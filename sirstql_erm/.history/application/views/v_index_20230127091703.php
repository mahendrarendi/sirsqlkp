<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>
  <title><?= APP_NAME; ?> <?php echo $title_page==='' ? ' - Beranda' : '- ' ;  echo $title_page; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/AdminLTE.min.css?v=1" >
  <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/skins/skin-yellow.min.css" >
  <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.css">
  <!-- Animation library for notifications   -->
  <link href="<?php echo base_url(); ?>assets/dist/css/animate.min.css" rel="stylesheet"/>
  <!--icon--> 
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon-16x16.png">
  <!-- jQuery 3 -->
  <script src="<?php echo base_url()?>assets/plugins/jquery/dist/jquery.min.js?n=1"></script>
  <?php 
  if(!empty($plugins))
  {
    foreach ($plugins as $i => $value) 
    {
      //<!-- Plugins css tambahan-->
      $this->load->view('plugins/'.$plugins[$i].'css');
    }
  }
  ?>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/sirstql.min.css?v=<?= APP_VERSION ?>" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font/webfontkit-geo1/stylesheet.css" />
</head>
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="skin-yellow sidebar-mini sidebar-collapse">
<!-- Site wrapper -->
<div class="wrapper">
    <div class="create_loading"></div>
    
    <!-- textbox ini digunakan untuk meelakukan pengecekan mode halaman di file javascript -->
    <input type="hidden" id="jsmode" value="<?= ((!empty($jsmode)) ? $jsmode : '' );  ?>">
    
    <!-- BEGIN HEADER -->
    <?php $this->load->view('v_header'); ?>
    
    <?php $this->load->view('v_sidebar'); ?>
    
    <div class="content-wrapper">
        
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1> <?php echo ucwords($title_page);?></h1>
        <ol class="breadcrumb" >
          <li><a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard "></i>Beranda</a></li>
          <?= (empty($title_page)) ? '<li class="active">'.ucwords($active_menu).'</li>' :'<li><a href="#">'.ucwords($active_menu).'</a></li>'; ?>
          <?= (!empty($title_page)) ? '<li class="active">'.ucwords($title_page).'</li>' :''; ?>
        </ol>
      </section>
      
      <!-- Set notifikasi -->
      <div id="notif"></div> 
      
      <input type="hidden" id="set_status" value="<?= ((isset($this->session->flashdata('message')['status'])) ? $this->session->flashdata('message')['status'] : '' ) ?>">
      <input type="hidden" id="set_message" value="<?= ((isset($this->session->flashdata('message')['message'])) ? $this->session->flashdata('message')['message'] : '' ) ?>">
      <input type="hidden" id="page_view" value="<?= $title_page; ?>" /> <!-- set isi page view -->
      <?php $this->load->view($content_view); ?>
    </div>
    <!-- END CONTENT -->
    
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('v_footer'); ?>
    <!-- END FOOTER -->

  <div class="control-sidebar-bg"></div>
  <a onclick="topFunction()" class="btn btn-warning btn-xs" id="menuBacktoTop" data-toggle="tooltip" data-original-title="Go to Top"><i class="fa fa-chevron-up fa-lg"></i></a>
</div>

<script src="<?php echo base_url();?>assets/dist/js/qrcode.min.js?n=1"></script><!-- QRCODE -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/dist/js/bootstrap.min.js?n=1"></script><!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js?n=1"></script><!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/lib/fastclick.js?n=1"></script><!-- FastClick -->
<script src="<?php echo base_url();?>assets/dist/js/adminlte.min.js?n=1"></script>
<script src="<?php echo base_url();?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.js?n=1"></script>
<script src="<?php echo base_url();?>assets/dist/js/bootstrap-notify.js?n=1"></script>

 <!--ubah versi js (.min.js?ver=...) ketika fitur sudah siap digunakan pelayanan, (...) diisi angka terbaru-->
<script src="<?php echo base_url();?>assets/dist/js/sirstql.min.js?ver=<?= APP_VERSION ?>"></script>

<script type="text/javascript">
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function()
    {
        scrollFunction()
    };
    var pr         = "<?php echo $this->session->userdata("peranperan"); ?>"; 
    var peranperan = pr.split(","); 
</script>

<?php
print_r($plugins);
die(); 
 if(!empty($plugins))
 {
    foreach ($plugins as $i => $value) 
    {
      //<!-- Plugins js tambahan-->
      $this->load->view('plugins/'.$plugins[$i].'js' );
    }
  }
  
  
//memanggil file js tambahan
  //ubah versi js (min.js?ver=5.4332") ketika fitur sudah siap digunakan pelayanan
if(isset($script_js))
{
  if(empty(!$script_js))
  {
      foreach ($script_js as $key => $value){
        ?>
        <script src="<?= base_url() .'assets/pages/'.$value ?>.min.js?ver=<?= APP_VERSION ?>"></script>
        <?php
      }
  }
}
?>

<script type="text/javascript">
    function ubahpassword()
    {
        var modalTitle = 'Ubah Password';
        var modalContent = '<form action="" id="FormUbahKataSandi">' +
                                '<div class="form-group">' +
                                    '<div class="col-sm-12">' +
                                    '<label>Kata Sandi Lama</label>' +
                                    '<input type="password" class="form-control" style="width:100%;" name="katasandilama" value="">'+
                                    '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<div class="col-sm-12">' +
                                    '<label>Kata Sandi Baru</label>' +
                                    '<input type="password" class="form-control" style="width:100%;" name="katasandibaru" value="">'+
                                    '</div>' +
                                '</div>' +
                                '<div class="form-group">' +
                                    '<div class="col-sm-12">' +
                                    '<label>Ulangi Kata Sandi Baru</label>' +
                                    '<input type="password" class="form-control" style="width:100%;" name="ulangikatasandibaru" value="">'+
                                    '</div>' +
                                '</div>' +
                            '</form>';
        $.confirm({ //aksi ketika di klik menu
            title: modalTitle,
            content: modalContent,
            columnClass: 'm',
            type:'orange',
            buttons: {
                //menu save atau simpan
                formSubmit: {
                    text: 'ubah kata sandi',
                    btnClass: 'btn-blue',
                    action: function () {
                        if ($('input[name="katasandilama"]').val() == '' || $('input[name="katasandibaru"]').val() == '' || $('input[name="ulangikatasandibaru"]').val() == '')
                        {
                            alert('Kata Sandi tidak boleh kosong');
                            return false;
                        }
                        else if ($('input[name="katasandibaru"]').val() != $('input[name="ulangikatasandibaru"]').val())
                        {
                            alert('Kata Sandi Baru tidak sama');
                            return false;
                        }
                        else
                        {
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: base_url + 'clogin/ubahpassword',
                                //data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icd"]').val(), ipd:$('select[name="idpegawaidokter"]').val(), td:$('input[name="jumlahtindakan"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
                                data: $("#FormUbahKataSandi").serialize(),
                                dataType: "JSON", //tipe data yang dikirim
                                success: function(result) { //jika  berhasil
                                    alert('Proses pengggantian password selesai'.result);
                                },
                                error: function(result) { //jika error
                                    alert('error');
                                }
                            });
                        }
                    }
                },
                //menu back
                formReset:{
                    text: 'batal',
                    btnClass: 'btn-danger'
                }
            },
            onContentReady: function () {
            }
        });
    }
    
    $(document).on('click','#gantishif',function(){
        $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: '',
        content: '<b>Konfirmasi Ganti Shif Kasir</b><br/><label class="label label-danger" id="infoshif"></label>',
        buttons: {
            ganti: function () {                
                $.ajax({ 
                    url: base_url + 'cdashboard/gantishifkasir',
                    type : "post",      
                    dataType : "json",
                    success: function(result) {
                        if(result.status=='success')
                        {
                            notif(result.status, result.message);
                            location.reload(); 
                        }
                        else
                        {
                            alert('Mohon Cek Shif Dengan Teliti Sebelum Mengakhiri Shif Jaga.!<br><label class="label label-danger">Minimal Waktu Ganti Shif : '+result.message+'</label>');
                        }
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            batal: function () {               
            }            
        },
        onContentReady: function () {
            $.ajax({ 
                url: base_url + 'cdashboard/tampilkanshifaktif',
                type : "post",      
                dataType : "json",
                success: function(result) {
                    $('#infoshif').empty();
                    $('#infoshif').text('Shif Aktif : '+result['label'] +', Tanggal Shif : '+result['mulaishif']);
                },
                error: function(result){               
                    fungsiPesanGagal();
                    return false;
                }
            }); 
        }
    });
    });
    
   
</script>
</body>
</html>

   
