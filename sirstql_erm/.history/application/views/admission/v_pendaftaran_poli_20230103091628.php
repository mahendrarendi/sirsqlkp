<style type="text/css">
  .label-daftar{padding: 0px;margin:0px;text-align: right;}
  .col-sm-9{margin:0px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="row ">
            <div class="col-md-12" style="margin-bottom:4px;">
                <input onchange="setLocalStorageTgl1(this.value)" id="tanggalawal" style="margin-right:6px;" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal">
                <input onchange="setLocalStorageTgl2(this.value)" id="tanggalakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggal"> 
                <a class="btn btn-info btn-sm" id="tampildata"><i class="fa fa-desktop"></i> Tampil</a>
                <a href="<?= base_url('cadmission/add_pendaftaran_poli')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Entri Pendaftaran</a>
                <a style="margin-right: 6px;" id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                <a class="btn btn-info btn-sm" onclick="gantiDokterPraktek()"><i class="fa fa-stethoscope"></i> Dokter Pengganti</a>
            </div>
            
        </div>
        <div class="box" style="padding-top:4px;">
            <div class="col-md-6 ">
                Tampil Data: 
                <select style="padding:5px;" id="jenistampilan">
                    <option value="1">Pasien Belum Diantrikan</option>
                    <option value="0">Semua Pasien</option>
                </select>
            </div>
            <table style="font-size: 10px;" id="dtpendaftaran" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>NO.RM</th>
                <th>NIK</th>
                <th>NAMA PASIEN</th>
                <th><i class="fa fa-calendar"></i> DAFTAR</th>
                <th><i class="fa fa-calendar"></i> PERIKSA</th>
                <th>CARADAFTAR</th>
                <th>POLIKLINIK</th>
                <th>CARABAYAR</th>
                <th>SKRINING</th>
                <th>PROLANIS</th>
                <th>Kunjungan Berikutnya</th>
                <th style="width:110px">CETAK</th>
                <th style="width:100px"> </th>
                <th class="none">NO.SEP</th>
                <th class="none">NO.RUJUKAN</th>
                <th class="none">Tgl Lahir</th>
                <th class="none">No.Telpon</th>
                <th class="none">Alamat</th>
                <th class="none">Status</th>
              </tr>
              </thead>
              <tbody>
            </table>
            
            <table class="table" style="font-size:12px;">
                    <tr class="bg bg-gray"><th colspan="3">#Keterangan</th></tr>   
                    <tr><td></td><td width="30"><a class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Pindah RANAP"><i class="fa fa-bed"></i></a></td><td> Menu untuk meranapkan pasien rawat jalan.</td></tr>                    
                    <tr><td></td><td><a class="btn btn-success btn-xs" data-original-title="Beri Antrian" data-toggle="tooltip"><i class="fa fa-check"></i></a></td><td> Menu untuk memberi antrian periksa pasien.</td></tr>
                    <tr><td></td><td><a class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Ubah"><i class="fa fa-pencil"></i></a></td><td> Menu untuk mengubah data (pendaftaran/pasien) periksa.</td></tr>
                    <tr><td></td><td><a class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Booking Jadwal Operasi"><i class="fa fa-calendar"></i></a></td><td> Menu untuk membuat jadwal operasi pasien.</td></tr>
                    <!--<tr><td></td><td><a class="btn btn-xs btn-danger" data-toggle="tooltip" data-original-title="Blacklist Pasien"><i class="fa fa-ban"></i></a></td><td> Menu untuk blacklist pasien di unit/poli periksa.</td></tr>-->
                    <tr><td></td><td><a data-toggle="tooltip" data-original-title="Tambah Layanan" class=" btn btn-primary btn-xs"><i class="fa fa-plus"></i></a></td><td> Menu untuk menambahkan layanan ke poli lain.</td></tr>
                    <tr><td></td><td><a class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Order RM"><i class="fa fa-search"></i></a></td><td> Menu untuk order berkas RM.</td></tr>
                    <tr><td></td><td><a class=" btn bg-purple btn-xs" data-original-title="Cetak Antrian"data-toggle="tooltip"><i class="fa fa-check"></i></a></td><td> Menu cetak antrian pemeriksaan / antrian periksa sudah diantrikan.</td></tr>                    
                    <tr><td></td><td><a class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Pindah Jadwal Periksa"><i class="fa fa-rocket"></i></a></td><td> Menu untuk memindahkan jadwal periksa pasien.</td></tr>                    
                </table>
        </div>
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <div style="border-top: 1px solid #f1f1f1;"></div>
          <div class="box-body">
            <form action="<?= base_url('cadmission/save_pendaftaran_poli');?>" method="POST" class="form-horizontal" id="FormPendaftaranPoli">
            
              <input type="hidden" name="idperson"/>
              <input type="hidden" name="idpendaftaran"/>
              <input type="hidden" name="norm" id="norm"/>
              <input type="hidden" id="noAntrian" name="hideNopesan"/>
              <input type="hidden" id="unitteripilih"/>
              <input type="hidden" name="modehalaman"/>
              <input type="hidden" name="isantri" id="isantri"/>
              <!-- set inputan bpjs, nosep dan rujukan -->
              <?php ( $this->session->userdata('isServerBPJS')=='true' ) ? $readonly='readonly' : $readonly='';?>
              
              <div class="form-group">
                  <div class="col-md-12">
                    <div class="col-md-2 col-xs-12"> 
                      <label> <i class="fa fa-clock-o"></i> Waktu Mulai</label>
                      <input type="text"  id="waktumulai" name="waktumulai" class="form-control" readonly/>
                    </div>
                    <div class="col-md-2 col-xs-12"> 
                      <label><i class="fa fa-clock-o"></i> Waktu Layanan</label>
                      <input type="text" id="waktulayanan" name="waktulayanan" class="form-control" readonly/>
                    </div>

                    <div class="col-md-2 col-xs-12"> 
                      <label> <i class="fa fa-clock-o"></i> Waktu Sekarang </label>
                      <input type="text" id="waktusekarang" name="waktusekarang" class="form-control" readonly/>
                    </div>

                    <?php if( $mode == 'edit' ): ?>
                      <div class="col-md-4 col-xs-12">
                        <div class="wrap-apm"></div>
                      </div>
                    <?php endif; ?>
                  </div>
              </div>
              
              
              <div style="background-color: #f5f5f5; padding: 8px 0;margin: 0 5px;">                
                <div class="form-group" id="pilihpendaftaran_poli">
                </div>
              </div>

              <!-- tampilkan jika mode add -->
              <!-- <div id="before_spesifiksearch" class="form-group" style="margin-top: 10px;margin-bottom: 10px;"> -->
                &nbsp;
              <?php if($mode=='add'){?>
              <fieldset class="bg bg-warning" id="pendaftaran_formcaripasien">
              <legend>Form Cari Pasien</legend>
                <div class="form-group">
                  <div style="margin-left:20px;" class="col-sm-2"> 
                    <label>No.RM</label>
                    <input type="text"  id="normcariipasienspesifik" autofocus placeholder="Norm" class="form-control"/>
                  </div>
                  <div class="col-sm-3"> 
                    <label>NamaPasien</label>
                    <input type="text" id="namacariipasienspesifik" placeholder="Nama pasien" class="form-control"/>
                  </div>
                  <div class="col-sm-2"> 
                    <label>TanggalLahir</label>
                    <input type="text" id="tanggallahircariipasienspesifik" placeholder="dd/mm/yyyy" class="form-control"/>
                  </div>
                  <div class="col-sm-3"> 
                    <label>Alamat</label>
                    <textarea class="form-control" id="alamatcariipasienspesifik" placeholder="Alamat" rows="1"></textarea>
                  </div>
                  <div class="col-sm-1">
                    <a style="margin-top: 15px; margin-left: 2px;" class="btn btn-warning btn-sm" onclick="caripasienspesifik()">Cari</a>
                  </div>
                </div>
            <br>
            <br>
            </fieldset>

              <?php } ?>

        <div class="row" style="margin-top:3px;">
        <!-- Left col -->
        <section class="col-sm-4">
          <div class="box box-solid box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Pasien</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!-- text box -->
              
              <!-- nomor identitas -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">NoIdentitas*</label>
                <div class="col-sm-9"><input type="text" class="form-control" id="noidentitas" name="noidentitas" placeholder="No. Identitas"/></div>
              </div>
              <!-- nama pasien -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Nama*</label>
                <div class="col-sm-9"><input type="text" id="namapasien" class="form-control" name="namapasien" placeholder="Nama Pasien"/></div>
              </div>
              <!-- identitas -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Identitas*</label>
                <div class="col-sm-9"><select class="form-control" name="identitas" id="identitas"><option value="0">Pilih</option><?php foreach ($dt_identitas as $row) { echo '<option value="'.$row.'">'.$row.'</option>'; } ?></select></div>
              </div>
              <!-- identitas -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">JenisPasien*</label>
                <div class="col-sm-9"><select class="form-control" name="jenispasien" id="jenispasien"><option value="0">Umum</option> <option value="1">Karyawan / Keluarga Karyawan</option></select></div>
              </div>
              <!-- jenis kelamin -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Kelamin*</label>
                <div class="col-sm-9"><select class="form-control" id="jeniskelamin" onchange="keydown_jeniskelamin()" name="jeniskelamin">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_jeniskelamin as $row) { echo '<option value="'.$row.'">'.$row.'</option>'; } ?></select>
                </div>
              </div>
              <!-- tanggal lahir -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Tgl Lahir*</label>
                <div class="col-sm-9"><input type="text" id="tanggallahir" class="form-control" name="tanggallahir" placeholder="Tanggal Lahir"/></div>
              </div>
              <!-- Tempat lahir -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">TempatLahir</label>
                <div class="col-sm-9"><input type="text" class="form-control" name="tempatlahir" placeholder="Tempat Lahir"/></div>
              </div>
              <!-- status -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Status</label>
                <div class="col-sm-9">
                  <select class="form-control" id="perkawinan" name="perkawinan">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_perkawinan as $row){echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>

              <!-- AGAMA -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Agama</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="agama" name="agama">
                    <?php foreach ($dt_agama as $row) {echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>
              <!-- golongan darah -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">GolDarah</label>
                <div class="col-sm-9"><select class="select2 form-control" style="width:100%" id="golongandarah" name="golongandarah">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_golongandarah as $row){ echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>
              <!-- rhesus darah -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Rhesus</label>
                <div class="col-sm-9"><select class="select2 form-control" style="width:100%" id="rhesus" name="rhesus"/>
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_rhesus as $row) {echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>
              <!-- pendidikan -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Pendidikan</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="pendidikan" name="pendidikan">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_pendidikan as $row) { echo '<option value="'.$row->idpendidikan.'">'.$row->namapendidikan.'</option>';}?></select>
                </div>
              </div>
              <!-- pekerjaan -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Pekerjaan</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="pekerjaan" name="pekerjaan">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_pekerjaan as $row){echo '<option value="'.$row->idpekerjaan.'">'.$row->namapekerjaan.'</option>';}?></select>
                </div>
              </div>
              <!-- no telepon -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">No. Telp</label>
                <div class="col-sm-9"><input type="text" class="form-control" name="notelepon" placeholder="No. Telepon"/></div>
              </div>
              <!-- alamat -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Alamat*</label>
               <div class="col-sm-9"><textarea class="form-control" rows="2" placeholder="Alamat sesuai identitas" name="alamat"></textarea></div>
              </div>
              <!-- kelurahan -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Kelurahan*</label>
                <div class="col-sm-7">
                  <select class="select2 form-control" style="width:100%" id="kelurahan" name="kelurahan">
                  <option value="0">Pilih</option>
                  <?php //foreach ($dt_kelurahan as $row) {echo '<option value="'.$row->iddesakelurahan.'">'.$row->namadesakelurahan.' '.$row->namakecamatan.'</option>';}?></select>
                </div> 
                 <a onclick="tambah_kelurahan()" data-toggle="tooltip" data-original-title="Tambah Kelurahan" class="btn btn-default btn-sm"><i class="fa fa-plus-square"></i></a>
              </div>
              
              <!-- Bahasa -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Bahasa*</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="idbahasa" name="idbahasa">
                  <option value="0">Pilih</option></select>
                </div> 
              </div>
              
              <!-- Bahasa -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Suku*</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="idsuku" name="idsuku">
                  <option value="0">Pilih</option></select>
                </div> 
              </div>

            </div>
          </div>
        </section>
        <!-- center col -->
        <section class="col-sm-4">
          <div class="box box-solid box-warning">
            <div class="box-header">
              <h3 class="box-title">Data Pendaftaran</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!-- text box -->

              <!-- nomor SEP -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label"><?= ($this->session->userdata('isServerBPJS_SEP')=='true') ? '' : 'No.SEP'; ?></label>
                <div class="col-sm-9">
                  <input type="<?= ($this->session->userdata('isServerBPJS_SEP')=='true') ? 'hidden' : 'text'; ?>" class="form-control" name="nosep" id="nosep" placeholder="No.SEP" onchange="bpjs_getdtpesertabysep();"/>
                </div>
              </div>
              <!-- nomor rm -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Norm</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="pilihpasien" placeholder="[Auto]" readonly />
                </div>
              </div>

              <!-- Pilih kelas -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Kelas</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="listkelaslayanan" name="kelaslayanan"><?php foreach ($dt_kelas as $row){echo '<option value="'.$row->idkelas.'">'.$row->kelas.'</option>';}?></select>
                </div>
              </div>

              <!-- jenis periksa -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Jenis Priksa</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" id="jenisperiksa" name="jenisperiksa"><?php foreach ($dt_jenisperiksa as $row){echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>
              
              <!-- Kategori Skrining -->
              <div class="form-group">
                  <label for="" class="col-sm-3 label-daftar control-label">K. Skrining* <br> <small style="font-size:8px;">Kategori Skrining</small></label>
                <div class="col-sm-9" id="skrining">
                    <?php 
                    if(isset($dt_skrining))
                    {
                        foreach ($dt_skrining as $value)
                        {
                            echo '<label class="label label-'.$value.'" style="padding:8px 9px;"><input type="radio"  name="kategoriskrining" value="'.$value.'"/> '.ucwords($value).'</label> ';
                        }
                    }
                    ?>
                </div>
              </div>

              <!-- cara bayar -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Cara Bayar</label>
                <div class="col-sm-9">
                  <!-- onchange="selisih15HariPasienBpjs(this.value)" -->
                  <select class="select2 form-control" style="width:100%" id="carabayar" name="carabayar"> 
                  <?php foreach ($dt_carabayar as $row) {echo '<option value="'.$row.'">'.$row.'</option>';}?></select>
                </div>
              </div>
              
              
              <!-- pendaftaran Online -->
              <div class="form-group">
                  <label class="col-sm-3"></label>
                  <input id="caradaftar" name="caradaftar" type="checkbox" class="flat-red">
                <label for="" classs="col-sm-9 label-daftar control-label">Pendaftaran Online</label>
              </div>

              <?php 
                /**
                 * Update [06/11/22]
                 * Developer By Ikhsan
                 * Cara Daftar APM
                 */
              ?>
              <div class="form-group">
                  <label class="col-sm-3"></label>
                  <input id="pendaftaranAPM" name="pendaftaranAPM" type="checkbox" class="flat-red">
                  <label for="" classs="col-sm-9 label-daftar control-label">Pendaftaran APM</label>
              </div>

              <?php /** end update caradaftar APM */ ?>

              <!-- Asal Rujukan -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Asal Rujukan</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" name="asalrujukan"><option value="">Pilih</option></select>
                </div>
              </div>
              <!-- jenis rujukan -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Jenis Rujukan</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" name="jenisrujukan">
                  <option value="">Pilih</option>
                  <?php if( !empty($dt_jenisrujukan) ) {
                    foreach ($dt_jenisrujukan as $row) {echo '<option value="'.$row.'">'.$row.'</option>';}
                  }?></select>
                </div>
                  </div>

              <!-- jenis kunjungan -->
              <div class="form-group">
                <label for="jeniskunjungan" class="col-sm-3 label-daftar control-label">JenisKunjungan</label>
                <div class="col-sm-9">
                  <select class="select2 form-control" style="width:100%" name="jeniskunjungan">
                    <option value="0">Pilih</option>
                  </select>
                </div>
              </div>

              <!-- Checkbox sudah print SEP -->
              <div class="form-group">
                  <label class="col-sm-3"></label>
                  <input id="sudahprintSEP" name="chckboxsudahprintSEP" type="checkbox" class="flat-red" value="1">
                  <label for="" classs="col-sm-9 label-daftar control-label"> SEP Sudah Terbit</label>
              </div>
            </div>
          </div>

          <!-- Data Penanggung -->
          <div class="box box-solid box-warning">
            <div class="box-header">
              <h3 class="box-title">Penanggung Pasien</h3>
              <div class="hide" id="jumlahpenanggungpasien">0</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" id="listpenanggung">

            </div>
          </div>
        </section>
        <!-- right col -->
        <section class="col-sm-4">
          <div class="box box-solid box-warning">
            <div class="box-header">
              <h3 class="box-title">Data BPJS</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <!-- text box -->

              <!-- nomor rujukan -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">No.Rujukan</label>
                <div class="col-sm-9">
                <input type="text" class="form-control" onchange="bpjs_getdtpesertabyrujukan()" name="norujukan" placeholder="No.Rujukan" style="padding: 6px 3px;"/>
                </div>
              </div>

              <!--  -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">NO.JKN</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" onchange="bpjs_getdtpesertabynojkn()" name="nojkn" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'No.Jkn' ; ?>" <?= $readonly; ?>/>
                </div>
              </div>

              <!-- jenis periksa -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Jenis Peserta</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="jenispeserta" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'JenisPeserta' ; ?>" <?= $readonly; ?> />
                  <input type="hidden" name="idjenispeserta"/>
                </div>
              </div>
              <!-- kelas anggota -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Hak Kelas</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control" name="kelas" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'Kelas' ; ?>" <?= $readonly; ?> />
                  <input type="hidden" name="idhakkelas"/>
                </div>
              </div>
              <!-- Status Peserta bpjs -->
              <div class="form-group">
              <label for="" class="col-sm-3 label-daftar control-label">Status Peserta</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="statuspeserta" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'Tanggal cetak' ; ?>" <?= $readonly; ?> />
                <input type="hidden" name="idstatuspeserta"/>
              </div>
              </div>
              <!-- Tanggal Cetak  -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Tgl Cetak</label>
                <div class="col-sm-9">
                  <input type="text"  name="tanggalcetak" class=" <?= (($this->session->userdata('isServerBPJS')=='true')? '':'datepicker') ?> form-control" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'StatusPeserta' ; ?>" <?= $readonly; ?> />
                </div>
              </div>
              <!-- INFORMASI -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Informasi</label>
                <div class="col-sm-9"><textarea name="informasi" class="form-control" rows="1" placeholder="<?= ($this->session->userdata('isServerBPJS')=='true') ? '[Auto]' : 'Informasi' ; ?>" <?= $readonly; ?>></textarea>
                <input type="hidden" name="tglKunjungan"/>
                </div>

              </div>

              <!-- Tanggal Rujukan  -->
              <div class="form-group">
                <label for="" class="col-sm-3 label-daftar control-label">Tgl Awal Rujukan</label>
                <div class="col-sm-9">
                  <input type="text"  name="tanggalrujukan" class="form-control" />
                </div>
              </div>

            </div>
          </div>


        </section>
      </div>
              
      <center id="menuPendaftaranPoliklinik">
        <a class="btn btn-primary btn-md" id="menusaveadmisi" onclick="entri_pendaftaran()">Save</a>
        <a class="btn btn-danger btn-md" onclick="location.reload();">Reset</a>
        <a class="btn btn-warning btn-md" onclick="back_pendaftaran()" >Back</a>
      </center>
      
    </form>
               
    </div>
     
    <?php } ?>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->

<script>
  $(document).ready(function(){

    list_apm_daftar();
    function list_apm_daftar(){
      
      var modependaftaran = localStorage.getItem('modependaftaran'); //ambil data modependaftaran dari local storage
      if( modependaftaran == 'edit' ){
          setTimeout(function() { 
            var norm = $('#FormPendaftaranPoli').find('#norm').val();
            var carabayar = $('#FormPendaftaranPoli #carabayar option').filter(':selected').val();
                        
            var textHtml;

            $.ajax({
              url:'tampil_apm', 
              type:'POST', 
              dataType:'JSON', 
              data:{'norm':norm,'carabayar':carabayar},
              success: function(response){
                if( response.msg == 'success' ){
                  if( response.cekpembayaran == carabayar ){
                    
                    let h_idasalrujukan = '';
                    let h_jenisrujukan ='';

                    if(response.idasalrujukan == null ){
                      h_idasalrujukan = '-';
                    }else{
                      h_idasalrujukan = response.idasalrujukan;
                    }

                    if(response.jenisrujukan == '' ){
                      h_jenisrujukan = '-';
                    }else{
                      h_jenisrujukan = response.jenisrujukan;
                    }

                    textHtml = `
                    <div class="apm-daftar-list box box-solid box-warning">
                      <div class="box-header">
                        Data APM
                      </div>
                      <div class="box-body">
                        <ul style="padding-left: 20px;">
                          <li>No BPJS : `+response.nobpjs+`</li>
                          <li>`+response.tipesurat+` : `+response.norujukan+`</li>
                          <li>No WA : `+response.nowa+`</li>
                          <li>Jenis Rujukan : `+h_jenisrujukan+`</li>
                          <li>Asal Rujukan : `+h_idasalrujukan+`</li>
                        </ul>
                      </div>
                    </div>
                    `;
                  }else{
                    textHtml='';
                  }

                  $('body').find('.wrap-apm').html(textHtml);
                
                }else{
                  textHtml = '';
                  $('body').find('.wrap-apm').html(textHtml);
                }
              },
              error: function(xhr){
                fungsiPesanGagal();
                return false;
              }
            }); //endajax

          }, 500); //end timeout
        
        } //end if
        
      }//end function


  });

</script>