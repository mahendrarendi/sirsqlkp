 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">

            <form class="form" id="frm-databulanan">
                <div class="col-md-6 row">
                    <input id="tglawal" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal">
                    <input id="tglakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggal">
                    <a class="btn btn-info btn-sm" id="tampil" ><i class="fa fa-desktop"></i> Tampil</a>
                    <a class="btn btn-warning btn-sm" id="reload"><i class="fa fa-refresh"></i> Refresh</a>
                </div>
            </form>
            
            <table id="listdatabulanan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                
                <thead>
                    <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>TGL PERIKSA</th>
                    <th>NO.Telepon</th>
                    <th>POLI</th>
                    <th>JENIS PERIKSA</th>
                    <th>CARA BAYAR</th>
                    <th>ALAMAT</th>
                    </tr>
                </thead>
                
                <tbody></tbody>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->