 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            
            <table id="listdatabulanan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                
                <thead>
                    <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>TGL PERIKSA</th>
                    <th>NO.Telepon</th>
                    <th>POLI</th>
                    <th>JENIS PERIKSA</th>
                    <th>CARA BAYAR</th>
                    <th>ALAMAT</th>
                    </tr>
                </thead>
                
                <tbody></tbody>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->