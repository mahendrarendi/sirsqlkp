<style>
  div.dataTables_wrapper {
  position: unset;
}
</style>
<!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
              <div class="filter">
              <div class="col-md-6 row">
                  <input id="tglawal" type="text" size="7" class="datepicker" name="tanggalawal" placeholder="Tanggal awal">
                  <input id="tglakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggalakhir">
                  <button class="btn btn-info btn-sm" id="tampil" ><i class="fa fa-desktop"></i> Tampil</button>
                  <a class="btn btn-warning btn-sm" id="reload"><i class="fa fa-refresh"></i> Refresh</a>
              </div>
              </div>
            
            <table id="listdatabulanan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                
                <thead>
                    <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>TGL PERIKSA</th>
                    <th>NO.Telepon</th>
                    <th>POLI</th>
                    <th>JENIS PERIKSA</th>
                    <th>CARA BAYAR</th>
                    <th>ALAMAT</th>
                    </tr>
                </thead>
                
                <tbody></tbody>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->