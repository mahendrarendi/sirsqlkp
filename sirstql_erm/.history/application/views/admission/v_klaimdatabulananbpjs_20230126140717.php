 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">

            <form class="form" id="frm-databulanan">
                <div class="form-group">
                    <div class="input-group input-daterange">
                        <input type="text" name="dari_tgl" autocomplete="off" class="form-control" value="2012-04-05">
                        <div class="input-group-addon">to</div>
                        <input type="text" name="sampai_tgl" autocomplete="off" class="form-control" value="2012-04-19">
                    </div>
                </div>
                <div class="form-inline">
                    <button class="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
                </div>
            </form>
            
            <table id="listdatabulanan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                
                <thead>
                    <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>TGL PERIKSA</th>
                    <th>NO.Telepon</th>
                    <th>POLI</th>
                    <th>JENIS PERIKSA</th>
                    <th>CARA BAYAR</th>
                    <th>ALAMAT</th>
                    </tr>
                </thead>
                
                <tbody></tbody>

            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->