<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-5">
                  <input type="text" class="datepicker" id="tahunbulan" size="7" value="<?= ((isset($get['tahunbulan'])) ? $get['tahunbulan'] : date('M Y') ) ?>" readonly>
                  &nbsp;
                  <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                  <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Cakupan</a>
                </div>
            </div>

            <div class="box box-default box-solid">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr class="header-table-ql"><th rowspan="2">NO</th><th rowspan="2">OPERATOR</th><th colspan="8" class="text-center">JAMINAN</th><th rowspan="2">JUMLAH</th></tr>
                            <tr class="header-table-ql"><th>UMUM</th><th>N-PBI</th><th>PBI</th><th>ASURANSI LAIN</th><th>JAMKESOS</th><th>JAMPERSAL</th><th>JASARAHARJA</th><th>BPJamsostek</th></tr>
                        </thead>
                        <tbody>
                            <?php
                            if(isset($carabayar))
                            {
                                $no=0;
                                foreach ($carabayar as $arr)
                                {
                                    
                                    echo "<tr><td>". ++$no."</td><td>".$arr['namadokter']."</td><td>".$arr['umum']."</td><td>".$arr['jknpbi']."</td><td>".$arr['jknnonpbi']."</td><td>".$arr['asuransilain']."</td><td>".$arr['jamkesos']."</td><td>".$arr['jampersal']."</td><td>".$arr['jasaraharja']."</td><td>".$arr['bpjstenagakerja']."</td><td>".$arr['total']."</td></tr>";
                                }
                            }
                            ?>
                        </tfoot>
                    </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                </div>
                <div class="box-body" style="width-max:1500px;overflow-x:auto;">
                    <table class="table table-bordered table-hover table-striped" style="width:60%;">
                        <thead>
                            <tr class="header-table-ql"><th rowspan="2">NO</th><th rowspan="2">JENIS</th><?php foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 1 ) {echo "<th colspan='5'>".$arr['jenisoperasi']."</th>";} } ?></tr>
                            <tr class="header-table-ql"><?php foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 1 ) { foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 0 ) {echo "<th>".$arr['jenisoperasi']."</th>";} } }}  ?></tr>
                        </thead>
                        <tbody>
                            <?php

                            $no = 0;
                            //loop jenis tindakan
                            foreach ($jenistindakan as $arr)
                            {
                                

                                echo "<tr><td>". ++$no."</td><td>".$arr['jenistindakan']."</td>";
                                
                                //set to array id jenis operasi
                                $idjenisoperasi = explode(',', $arr['jenisoperasi']);

                                echo '<pre>';
                                print_r( $jenisoperasi );
                                echo '</pre>';
                                
                                //loop jenisoperasi kategori 1
                                $n = 0;
                                foreach ($jenisoperasi as $arr1)
                                { 
                                    
                                //     if($arr1['jeniskategori'] == 1 )
                                //     {
                                //         //loop jenisoperasi kategori 0
                                        
                                //         foreach ($jenisoperasi as $arr2)
                                //         {
                                //             $jml_kategori0 = 0;
                                //             if($arr2['jeniskategori'] == 0 )
                                //             {
                                //                 if(in_array($arr2['idjenisoperasi'],$idjenisoperasi))
                                //                 {
                                //                    ++$jml_kategori0; 
                                //                 }
                                //                 echo "<td>".$jml_kategori0."</td>";
                                //             }
                                //         }
                                //     } 

                                    if( in_array( $arr1['idjenisoperasi'],$idjenisoperasi ) ){
                                        echo "<td>".$n++."</td>";
                                    }else{
                                        echo "<td>".$n."</td>";
                                    }

 
                                    
                                }
                            }
                            ?>
                        </tfoot>
                    </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                     <h5 class="text-bold" id="title1">Laporan Dr Anastesi</h5>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped" style="width:60%;">
                        <thead>
                            <tr class="header-table-ql"><th>Nama Dokter</th><th>Jumlah</th></tr>
                        </thead>
                        <tbody>
                            <?php
                                $total = 0;
                                foreach ($dranastesi as $arr)
                                {
                                    $total += intval($arr['jumlah']);
                                    echo "<tr><td>".$arr['namadokter']."</td><td>".$arr['jumlah']."</td></tr>";
                                }
                                echo "<tr><td>Total</td><td>".$total."</td></tr>";
                            ?>
                        </tfoot>
                    </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h5 class="text-bold" id="title1">Laporan Sc dengan Dr spesialis Anak/Umum</h5>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-hover table-striped" style="width:60%;">
                        <thead>
                            <tr class="header-table-ql"><th>Nama Dokter</th><th>Jumlah</th></tr>
                        </thead>
                        <tbody>
                            <?php
                                $total = 0;
                                foreach ($laporansc as $arr)
                                {
                                    $total += intval($arr['jumlah']);
                                    echo "<tr><td>".$arr['namadokter']."</td><td>".$arr['jumlah']."</td></tr>";
                                }
                                echo "<tr><td>Total</td><td>".$total."</td></tr>";
                            ?>
                        </tfoot>
                    </table>
                </div>
            </div>
            
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
        