  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <?php if( $mode=='viewlog'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>URL</th>
                  <th>Log</th>
                  <th>Expired</th>
                  <th>Waktu</th>  
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if( $mode=='viewlogperiksa'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <input id="tanggal" size="7" class="datepicker" placeholder="Tanggal"> <a class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a> <a style="margin-right: 6px;" id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
              <table id="dtlog" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>NO.RM</th>
                  <th>WAKTU</th>
                  <th>URL</th>
                  <th>LOG</th>  
                  <th>USER</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if( $mode=='viewpobat'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body" id="tampildataobat">
              <table id="viewpobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>Waktu</th>
                  <th>Nama Obat</th>
                  <th>Kode Obat</th>
                  <th>Jenis</th>  
                  <th>Satuan Obat</th>
                  <th>Jumlah</th>
                  <th>Dosis Racik</th>
                  <th>Harga</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if( $mode=='viewdtpasien'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>NoRM</th>
                  <th>nojkn</th> 
                  <th>nik</th> 
                  <th>namalengkap</th> 
                  <th>tempatlahir</th> 
                  <th>tanggallahir</th> 
                  <th>jeniskelamin</th> 
                  <th>agama</th> 
                  <th>statusmenikah</th> 
                  <th>golongandarah</th> 
                  <th>rh</th> 
                  <th>namapendidikan</th> 
                  <th>namapekerjaan</th> 
                  <th>alamat</th> 
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if($mode=='pelayananrs'){?>
          <style type="text/css">.small-box>.small-box-footer {background: #795548;}.btn-unduh{text-align: left; padding: none; cursor: pointer}</style>
          <div class="col-xs-6" style="margin-top: 15px;">
            <div class="box">
            <div class="box-body">
              <table class="table table-hover table-striped">
                  <tbody><tr style="font-size: 16px;">
                    <th>NAMA</th>
                    <th>DESKRIPSI</th>
                    <th></th>
                  </tr>
                  
                  <tr>
                    <td>LP.01</td>
                    <td>Laporan Pasien Bulanan</td>
                    <td><a href="<?= base_url('creport/laporanperiksapasien');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.02</td>
                    <td>Kunjungan Pasien RALAN Berdasarkan Asal Rujukan</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienbyasalrujukan');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.03</td>
                    <td>Kunjungan Pasien RANAP Berdasarkan Asal Rujukan</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienranapbyasalrujukan');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.04</td>
                    <td>Kunjungan Pasien RALAN Berdasarkan Status Layanan</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienbylayanan');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.05</td>
                    <td>Kunjungan Pasien Berdasarkan Cara Daftar</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienbycaradaftar');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.06</td>
                    <td>Kunjungan Pasien Baru</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienbaru');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.06.LB</td>
                    <td>Kunjungan Pasien Lama dan Pasien Baru per Poli</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienlamabaru');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.07</td>
                    <td>Kunjungan Pasien Berdasarkan Etnis/Suku dan Bahasa</td>
                    <td><a href="<?= base_url('creport/laporankunjunganpasienbyetnisdanbahasa');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.08</td>
                    <td>Kunjungan Pasien Antigen Positif</td>
                    <td><a href="<?= base_url('creport/kunjunganpasienantigenpositif');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.09</td>
                    <td>Kunjungan Pasien Antigen Negatif</td>
                    <td><a href="<?= base_url('creport/kunjunganpasienantigennegatif');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>LP.10</td>
                    <td>Laporan Rekam Medis Ganda</td>
                    <td><a href="<?= base_url('creport/laporanrmganda');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  
                  <tr>
                    <td>LP.11</td>
                    <td>Kunjungan Pasien Umum</td>
                    <td><a href="<?= base_url('creport/laporanpasienumum');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  
                  <tr>
                    <td>LP.12</td>
                    <td>Kunjungan Poli Kedokteran Jiwa</td>
                    <td><a href="<?= base_url('creport/laporanpolikedokteranjiwa');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  
                  <tr>
                    <td>LP.13</td>
                    <td>Cakupan Pasien Ralan Berdasarkan Cara Bayar</td>
                    <td><a href="<?= base_url('creport/laporanpasienbycarabayar');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>

                  <tr>
                    <td>LP.14</td>
                    <td>Laporan Kondisi Keluar Pasien RALAN Per Tahun</td>
                    <td><a href="<?= base_url('creport/laporanKondisiKeluar');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>

                </tbody>
              </table>
              </div>
            </div>
          </div>
          
          <div class="col-xs-6" style="margin-top: 15px;">
            <div class="box">
            <div class="box-body">
              <table class="table table-hover table-striped">
                  <tbody><tr style="font-size: 16px;">
                    <th>NAMA</th>
                    <th>DESKRIPSI</th>
                    <th></th>
                  </tr>
                  <tr>
                    <td>RL3.14</td>
                    <td>Data Rujukan</td>
                    <td><a href="<?= base_url('creport/rl314_rujukan');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL4A</td>
                    <td>Data Keadaan Morbiditas Pasien Rawat Inap</td>
                    <td><a href="<?= base_url('creport/rl4a_penyakitranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL4B</td>
                    <td>Data Keadaan Morbiditas Pasien Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl4b_penyakitrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.1</td>
                    <td>Kunjungan Pasien Rawat Inap</td>
                    <td><a href="<?= base_url('creport/rl51_kunjunganpasienranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.2</td>
                    <td>Kunjungan Pasien Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl52_kunjunganpasienrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.2</td>
                    <td>Kunjungan Ralan Berdasarkan Poli Tujuan</td>
                    <td><a href="<?= base_url('creport/rl52_kunjunganpasienrajalperpoli');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.3</td>
                    <td>10 Besar Penyakit Rawat Inap</td>
                    <td><a href="<?= base_url('creport/rl53_10besarpenyakitranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.4</td>
                    <td>10 Besar Penyakit Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl54_10besarpenyakitrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>ISPA/PNEUMONIA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/pneumonia_pelaporanview');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>BALITA PNEUMONIA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/balitapneumonia_pelaporanview');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>Surveilans Rajal</td>
                    <td>Surveilans Terpadu Penyakit Berbasis Rumah Sakit Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/report_stprsrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>Surveilans Ranap</td>
                    <td>Surveilans Terpadu Penyakit Berbasis Rumah Sakit Rawat Inap</td>
                    <td><a href="<?= base_url('creport/report_stprsranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>SIHA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/report_siha');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>CAKUPAN POLI</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/report_cakupanpoli');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>

                </tbody>
              </table>
              </div>
            </div>
          </div>
          
            <?php } else if($mode=='cakupancarabayarralan') { ?>

            <div class="row">
                <div class="col-xs-1" style="margin-bottom:6px;">
                  <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                </div>
                <div class="col-md-3">
                  <input type="text" class="datepicker" id="tahunbulan" size="7" value="" readonly>
                  <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                </div>
              </div>

              <div class="box box-default box-solid">
                  <div class="box-header with-border">
                    <h5 class="text-bold">Data Cakupan</h5>
                  </div>
                  <div class="box-body">
                    <div class="col-xs-12" id="tampildata"></div>
                  </div>
              </div>
          
            </div>
          </div>  
          <?php } else if($mode=='kunjungankedokteranjiwa') { ?>
          
          <div class="row">
              <div class="col-xs-1" style="margin-bottom:6px;">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              </div>
              <div class="col-md-2">
                  <select id="bulan" name="bulan" class="form-control">
                <?php 
                    $no=0;
                    foreach ($bulan as $arr)
                    {
                        ++$no;
                        echo '<option value="'. $no .'"  '.(( date('n') == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                    }
                ?>
              </select>
              </div>
              <div class="col-md-3">
                <input type="text" class="datepicker" id="tahun" size="7" value="<?= date('Y'); ?>" readonly>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
              </div>
            </div>
              
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold">Kunjungan Pasien Laki-laki</h5>
                </div>
                <div class="box-body">
                  <div class="col-xs-12" id="tampillakilaki"></div>
                </div>
            </div>
          
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold">Kunjungan Pasien Wanita</h5>
                </div>
                <div class="box-body">
                  <div class="col-xs-12" id="tampilwanita"></div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>    
          <?php } else if($mode=='kunjunganpasienumum') { ?>
          
          <div class="row">
              <div class="col-xs-1" style="margin-bottom:6px;">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              </div>
              <div class="col-md-2">
                  <select id="bulan" name="bulan" class="form-control">
                <?php 
                    $no=0;
                    foreach ($bulan as $arr)
                    {
                        ++$no;
                        echo '<option value="'. $no .'"  '.(( date('n') == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                    }
                ?>
              </select>
              </div>
              <div class="col-md-3">
                <input type="text" class="datepicker" id="tahun" size="7" value="<?= date('Y'); ?>" readonly>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <!--<a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>-->
              </div>
            </div>
              
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold">Kunjungan Pasien Umum Rawat Jalan</h5>
                </div>
                <div class="box-body">
                  <div class="col-xs-12" id="tampildataralan"></div>
                </div>
            </div>
          
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold">Kunjungan Pasien Umum Rawat Inap</h5>
                </div>
                <div class="box-body">
                  <div class="col-xs-12" id="tampildatainap"></div>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>    
            
          <?php } else if($mode=='laporanrmganda') { ?>
          
          <div class="row">
              <div class="col-xs-12" style="margin-bottom:6px;">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
              <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
              <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
            </div>
          </div>
              
            <div class="col-xs-12 row" >
                <div class="box" id="laporanrmganda">
                  <div class="box-body" id="tampildata">
                  </div>
                </div>
            </div>
                   
          <?php } else if($mode=='kunjunganpasienantigen') { ?>
          
          <div class="row">
              <div class="col-xs-12" style="margin-bottom:6px;">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tanggal" size="7" readonly>
              <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
              <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
              <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
            </div>
          </div>
              
            <div class="col-xs-12 row" >
                <div class="box" id="cakupan">
                  <div class="box-body" id="tampildata">
                  </div>
                </div>
            </div>
          
          <?php } else if($mode=='kunjunganpasienbyetnisdanbahasa') { ?>
          
          <div class="row">
              <div class="col-xs-12" style="margin-bottom:6px;">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="bulan" size="7" readonly>
              <a id="tampil" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <!--<a onclick="unduhlaporan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>-->
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-12 col-md-6" >
                <div class="box" id="cakupan">
                  <div class="box-header with-border">
                    <h3 class="box-title" style="font-size:13px;">Grafik Kunjungan Pasien Berdasarkan Suku</h3>
                  </div>
                  <div class="box-body" id="bodygrafikkunjungansuku">
                   
                  </div>
                </div>
            </div>
              
            <div class="col-xs-12 col-md-6" >
                <div class="box" id="cakupan">
                  <div class="box-header with-border">
                    <h3 class="box-title" style="font-size:13px;">Grafik Kunjungan Pasien Berdasarkan Bahasa</h3>
                  </div>
                  <div class="box-body" id="bodygrafikkunjunganbahasa">
                    <div class="chart">
                      <canvas id="grafikkunjunganbahasa" style="height:250px"></canvas>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-12 col-md-6" >
                <div class="box" id="cakupan">
                  <div class="box-header with-border">
                    <h3 class="box-title" style="font-size:13px;">Kunjungan Pasien Berdasarkan Suku</h3>
                  </div>
                  <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th width="30px;">No</th>  
                      <th width="400px;">Suku</th>
                      <th>Kunjungan</th>
                    </tr>
                    </thead>
                    <tbody id="tampilsuku">
                    </tfoot>
                  </table>
                  </div>
                </div>
            </div>
              
            <div class="col-xs-12 col-md-6" >
                <div class="box" id="cakupan">
                  <div class="box-header with-border">
                    <h3 class="box-title" style="font-size:13px;">Kunjungan Pasien Berdasarkan Bahasa</h3>
                  </div>
                  <div class="box-body">
                    <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                        <thead>
                        <tr class="bg bg-yellow-gradient">
                          <th width="30px;">No</th>  
                          <th width="400px;">Bahasa</th>
                          <th>Kunjungan</th>
                        </tr>
                        </thead>
                        <tbody id="tampilbahasa">
                        </tfoot>
                      </table>
                  </div>
                </div>
            </div>
          </div>
          
          <?php } else if($mode=='kunjunganpasienlamabaru') { ?>
          
          <div class="row">
            <div class="col-xs-12">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
              <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
              <a onclick="tampil_kunjunganpasien()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <a onclick="unduhlaporan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
            </div>
          </div>
          
          
          <div class="box box-gray box-solid" style="margin-top:10px;">                
                <div class="box-body">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="header-table-ql">
                      <th>No</th>  
                      <th>Poliklinik</th>
                      <th>Status</th>
                      <th>Pasien Lama</th>
                      <th>Pasien Baru</th>
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody id="tampilkunjungan">
                    </tfoot>
                  </table>
                </div>
              
          <?php } else if($mode=='kunjunganpasienbaru') { ?>
          
          <div class="row">
            <div class="col-xs-12">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tanggal" size="7" readonly>
              <a onclick="tampil_kunjunganpasienbaru()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <a onclick="unduhlaporan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
            </div>
          </div>
          
          
          <div class="box box-gray box-solid" style="margin-top:10px;">                
                <div class="box-body">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>No</th>  
                      <th>NORM</th>
                      <th>NIK</th>
                      <th>Nama Pasien</th>
                      <th>Tanggal Lahir</th>
                      <th>Tempat Lahir</th>
                      <th>Alamat</th>
                      <th>No.Telepon</th>
                      <th>Tanggal Periksa</th>
                      <th>Poli Tujuan</th>
                    </tr>
                    </thead>
                    <tbody id="tampilkunjungan">
                    </tfoot>
                  </table>
                </div>
          <?php } else if($mode=='kunjunganpasienbycaradaftar') { ?>
          
          <div class="row">
            <div class="col-xs-12">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
              <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
              <a onclick="tampil_kunjunganralanbycaradaftar()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
            </div>
          </div>
          
          
          <div class="box box-gray box-solid" style="margin-top:10px;">
                <div class="box-header with-border">
                  <h3 class="box-title">Jumlah Kunjungan Pasien </h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div class="box-body">
                  <small class="text text-red">#jumlah yang ditampilkan adalah keseluruhan pasien mendaftar, bukan hanya pasien yang sampai selesai periksa.</small>
                  <table id="defectaobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                        <tr class="bg bg-yellow-gradient">
                      <th>Tanggal Periksa</th>
                      <th>Pendaftar Online</th>
                      <th>Pendaftar Reguler</th>
                      <th>Pendaftar Anjungan</th>
                      <th>Pendaftar Mobile JKN</th>
                      <th>Total Pendaftar</th>
                    </tr>
                    </thead>
                    <tbody id="tampilkunjungan">
                    </tfoot>
                  </table>
                </div>
            </div>
          
          <?php } else if($mode=='kunjunganpasienbylayanan') { ?>
          <div class="box">
            <div class="box-body">
              <div class="col-xs-12">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
                <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
                <a onclick="tampil_kunjunganralanbylayanan()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              </div>
              <div id="listtable">
                <!--List data-->
              </div>
              </div>
            </div>
          
          <?php } else if($mode=='kunjunganranapbyrujukan') { ?>
              <div class="box">
                <div class="box-body">
                    <div class="col-xs-12 row" style="margin-bottom:10px;">
                    <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                    <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
                    <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
                    <a onclick="tampil_kunjunganranapbyrujukan()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                    <!--<a onclick="unduh_kunjunganranapbyrujukan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>-->
                  </div>
                    
                    <h4>Rekap Rujukan Rawat Inap</h4>
                    <div id="listtable"></div>
                  
                    <h4>Rekap Pasien Rawat Inap</h4>
                    <div id="listpasien"></div>
                  </div>
                </div>
          <?php } else if($mode=='kunjunganpasienasalrujukan') { ?>
          <div class="box">
            <div class="box-body">
              <div class="col-xs-12">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tanggal1" size="7" readonly>
                <input type="text" class="datepicker" id="tanggal2" size="7" readonly>
                <a onclick="tampil_kunjunganralanbyrujukan()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduh_kunjunganralanbyrujukan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
                <!--List data-->
              </div>
              </div>
            </div>
          <?php } else if($mode=='laporanpelayanan'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar" style="margin-bottom:5px;">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <!-- <select id="dokter"><option>Pilih Dokter</option></select> -->
                <input type="text" class="datepicker" id="bulan" size="4">
                <a onclick="tampil_lappelayanan()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhlpelayanan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <!--<div id="listtable">-->
              <table id="tabel"   class=" ql-table  ql-table-bordered" cellspacing="0" width="100%" style="overflow-x:auto;overflow-y:auto;">
                <thead>
                    <tr class="bgql-gray"><th rowspan="3">No</th>
                        <th rowspan="3">Dokter</th>
                        <th rowspan="3">Poliklinik</th>
                        <th rowspan="3">Jaminan</th>
                        <th colspan="6">Identitas Pasien</th>
                        <th rowspan="3">Pasien baru/lama</th>
                        <th colspan="5">Pemeriksaan</th>
                        <th rowspan="3">Diagnosa</th>
                        <th rowspan="3">Tindakan</th>
                        <th rowspan="3">Terapi</th>
                    </tr>
                <tr class="bgql-gray">
                  <th rowspan="2">No.RM</th>
                  <th rowspan="2">Nama</th>
                  <th rowspan="2">Tgl Lahir</th>
                  <th rowspan="2">Usia</th>
                  <th rowspan="2">Alamat</th>
                  <th rowspan="2">Jenis Kelamin</th>
                  <th colspan="2">Pemeriksaan Fisik</th> 
                  <th colspan="3">Pemeriksaan Penunjang</th>
                </tr>
                <tr class="bgql-gray">
                  <th>Vital Sign</th>
                  <th>Data Objektif</th>
                  <th>Laboratorium</th>
                  <th>Radiologi</th>
                  <th>USG</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          <?php } else if($mode=='rl4a'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl4a()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl4a()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="3">No. Urut</th>
                  <th rowspan="3">No. DTD</th> 
                  <th rowspan="3">No. Daftar Terperinci</th> 
                  <th rowspan="3">Golongan Sebab Penyakit</th> 
                  <th colspan="18">Jumlah Pasien (Hidup &amp; Mati) Menurut Golongan Umur &amp; Jenis Kelamin</th> 
                  <th colspan="3">Pasien Keluar (Hidup &amp; Mati) Menurut Jenis Kelamin</th>
                  <th rowspan="3">Jumlah Pasien Keluar Hidup</th>
                  <th rowspan="3">Jumlah Pasien Keluar Mati</th>
                </tr>
                <tr>
                  <th colspan="2">0-6 hr</th>
                  <th colspan="2">7-28 hr</th> 
                  <th colspan="2">29hr-&lt; 1 th</th>
                  <th colspan="2">1-4 th</th>
                  <th colspan="2">5-14 th</th>
                  <th colspan="2">15-24 th</th>
                  <th colspan="2">25-44 th</th>
                  <th colspan="2">45-64 th</th>
                  <th colspan="2">&gt; 65</th>
                  <th rowspan="2">Laki</th>
                  <th rowspan="2">Perempuan</th>
                </tr>
                <tr>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
            </div>
            </div>
          </div>
          <?php } else if($mode=='rl4b'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="7">
                <input type="text" class="datepicker" id="tgl2" size="7">
                <a onclick="tampilrl4b()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl4b()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr style="background:#edd38c;">
                  <th rowspan="3">No. Urut</th>
                  <th rowspan="3">No. DTD</th> 
                  <th rowspan="3">No. Daftar Terperinci</th> 
                  <th rowspan="3">Golongan Sebab Penyakit</th> 
                  <th colspan="18">JML Pasien Kasus Baru Menurut Golongan Umur &amp; Sex</th> 
                  <th colspan="2">Kasus Baru Menurut Jenis Kelamin</th>
                  <th rowspan="3">JML Kasus Baru</th>
                  <th rowspan="3">JML Kunjungan</th>
                </tr>
                <tr style="background:#b2ab96;">
                  <th colspan="2">0-6 hr</th>
                  <th colspan="2">7-28 hr</th> 
                  <th colspan="2">29hr-&lt; 1 th</th>
                  <th colspan="2">1-4 th</th>
                  <th colspan="2">5-14 th</th>
                  <th colspan="2">15-24 th</th>
                  <th colspan="2">25-44 th</th>
                  <th colspan="2">45-64 th</th>
                  <th colspan="2">&gt; 65</th>
                  <th rowspan="2">Laki</th>
                  <th rowspan="2">Perempuan</th>
                </tr>
                <tr class="bg bg-gray">
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                </tr>
                <tr style="background-color: #222d32;color:#fff;">
                  <th>1</th>
                  <th>2</th>
                  <th>3</th>
                  <th>4</th>
                  <th>5</th>
                  <th>6</th>
                  <th>7</th>
                  <th>8</th>
                  <th>9</th>
                  <th>10</th>
                  <th>11</th>
                  <th>12</th>
                  <th>13</th>
                  <th>14</th>
                  <th>15</th>
                  <th>16</th>
                  <th>17</th>
                  <th>18</th>
                  <th>19</th>
                  <th>20</th>
                  <th>21</th>
                  <th>22</th>
                  <th>23</th>
                  <th>24</th>
                  <th>25</th>
                  <th>26</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl51'){?>
          <div class="box">
            <div class="box-body" style="margin-top: 10px;">
                <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="7">
                <input type="text" class="datepicker" id="tgl2" size="7">
                <a onclick="tampil_rl51()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl51()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
                <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Kode RS</th>
                    <th>Nama RS</th>
                    <th>Bulan</th>
                    <th>KAB/KOTA</th>
                    <th>Tahun</th>
                    <th>KODE PROPINSI</th>
                    <th>No. Urut</th>
                    <th>Jenis Kegiatan</th>
                    <th>Laki-laki</th>
                    <th>Wanita</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl52'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tgl1" size="7">
              <input type="text" class="datepicker" id="tgl2" size="7">
              <a onclick="tampil_rl52()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <a onclick="unduhrl52()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
            </div>
            <div class="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>Kode RS</th>
                  <th>Nama RS</th>
                  <th>Bulan</th>
                  <th>KAB/KOTA</th>
                  <th>Tahun</th>
                  <th>KODE PROPINSI</th>
                  <th>No. Urut</th>
                  <th>Jenis Kegiatan</th>
                  <th>Laki-laki</th>
                  <th>Wanita</th>
                  <th>Jumlah</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl52b'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tgl1" size="7">
              <input type="text" class="datepicker" id="tgl2" size="7">
              <a onclick="tampil_rl52b()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <!--<a onclick="unduhrl52b()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>-->
            </div>
            <div class="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th>Nama Poli</th>
                  <th>Jumlah Kasus</th>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php else if( $mode  == 'rl314'){ ?>
            oke
          <?php } else if($mode=='rl53'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl53()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl53()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="2">KODE PROPINSI</th>
                  <th rowspan="2">KAB/KOTA</th>
                  <th rowspan="2">Kode RS</th>
                  <th rowspan="2">Nama RS</th>
                  <th rowspan="2">Bulan</th>
                  <th rowspan="2">Tahun</th>
                  <th rowspan="2">No. Urut</th>
                  <th rowspan="2">KODE ICD 10</th>
                  <th rowspan="2">Descripsi</th>
                  <th colspan="2" width="20%">Pasien Keluar hidup Menurut jenis Kelamin</th>
                  <th colspan="2" width="20%">Pasien Keluar Mati Menurut Jenis Kelamin</th>
                  <th rowspan="2">TOTAL ALL</th>
                </tr>
                <tr>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl54'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl54()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl54()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="2">KODE PROPINSI</th>
                  <th rowspan="2">KAB/KOTA</th>
                  <th rowspan="2">Kode RS</th>
                  <th rowspan="2">Nama RS</th>
                  <th rowspan="2">Bulan</th>
                  <th rowspan="2">Tahun</th>
                  <th rowspan="2">No. Urut</th>
                  <th rowspan="2">KODE ICD 10</th>
                  <th rowspan="2">Descripsi</th>
                  <th colspan="2">Kasus Baru menurut Jenis Kelamin</th>
                  <th rowspan="2">TOTAL ALL</th>
                </tr>
                <tr>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='pelaporanpneumonia'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="loaddt_pelaporanpneumonia()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhpelaporanpneumonia()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th class="bg bg-gray" rowspan="4">No</th>
                  <th class="bg bg-gray" rowspan="4">UNIT</th>
                  <th class="bg bg-gray" colspan="10" rowspan="1">REALISASI PENEMUAN PENDERITA</th>
                  <th class="bg bg-gray" rowspan="2" colspan="8">Jml Kematian Balita karena Pneumonia</th>
                  <th class="bg bg-gray" colspan="6">ISPA &ge; 5 Th</th>
                </tr>
                <tr>
                  <th class="bg bg-gray" colspan="5">Pneumonia</th>
                  <th class="bg bg-gray" colspan="5">Batuk Bukan Pneumonia</th>
                  <th class="bg bg-gray" rowspan="2" colspan="3">Bukan Pneumonia</th>
                  <th class="bg bg-gray" rowspan="2" colspan="3">Pneumonia</th>
                </tr>
                <tr>
                  <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
                  <th rowspan="2" class="bg bg-yellow">Total</th>
                  <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
                  <th rowspan="2" class="bg bg-yellow">Total</th>
                  <th colspan="3" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="3" class="bg bg-green">1 - 4 Th</th>
                  <th colspan="2" class="bg bg-yellow">Total</th>
                </tr>
                <tr>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-yellow">L</th>
                  <th class="bg bg-yellow">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='pelaporanbalitapneumonia'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="loaddt_pelaporanbalitapneumonia()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhpelaporanbalitapneumonia()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr class="bg-gray">
                    <th rowspan="2">No</th>
                    <th rowspan="2">NAMA LENGKAP</th>
                    <th rowspan="2">ALAMAT LENGKAP</th>
                    <th colspan="2">UMUR</th>
                    <th rowspan="2">Tanggal Masuk</th>
                    <th rowspan="2">Tanggal Keluar</th>
                    <th rowspan="2">dx</th>
                    <th rowspan="2">Kondisi Keluar (H/M)</th>
                  </tr>
                  <tr class="bg-gray"><th>L</th><th>P</th></tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='report_stprs'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="7">
                <input type="text" class="datepicker" id="tgl2" size="7">
                <a id="loaddt_report_stprs" jenis="<?= $jenispemeriksaan ;?>" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a id="unduhreport_stprs" jenis="<?= $jenispemeriksaan ;?>" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Excel</a>
                <a id="unduhtxtreport_stprs" jenis="<?= $jenispemeriksaan ;?>" class="btn btn-success btn-sm"><i class="fa  fa-file-text-o"></i> Txt</a>
              </div>
              <div id="listtable">
              <table  style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                   <tr class="bg bg-yellow-gradient">
                    <th rowspan="3">No</th>
                    <th rowspan="3">Kode</th>
                    <th rowspan="3">Kasus</th>
                    <th rowspan="3">Laki</th>
                    <th rowspan="3">Perempuan</th>
                    <th rowspan="3">Jml Kasus</th>
                    <th rowspan="3">Kunjungan</th>
                    <th colspan="24">RAWAT <?= strtoupper($jenispemeriksaan); ?></th>
                  </tr>
                  <tr class="bg bg-yellow-gradient">
                    <th colspan="2">0-7 Hr</th>
                    <th colspan="2">8-28 Hr</th>
                    <th colspan="2"> &lt; 1 Th</th>
                    <th colspan="2">1-4 Th</th>
                    <th colspan="2">5-9 Th</th>
                    <th colspan="2">10-14 Th</th>
                    <th colspan="2">15-19 Th</th>
                    <th colspan="2">20-44 Th</th>
                    <th colspan="2">45-54 Th</th>
                    <th colspan="2">55-59 Th</th>
                    <th colspan="2">60-69 Th</th>
                    <th colspan="2"> >=70 Th</th>
                  </tr>
                  <tr class="bg bg-yellow-gradient">
                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>

                    <th>L</th>
                    <th>P</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div><?php }else if($mode=='laporan_siha'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="siha_listdata()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="siha_downloadexcel()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table  style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>No</th><th>Nama Pasien</th><th>Jenis Kelamin</th><th>Tgl Lahir</th><th>Riwayat</th><th>Diagnosa</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='report_cakupanpoli'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <span class="bg bg-info" style="padding:8px;margin:2px;"><input type="radio" value="rajal" name="jenisperiksa" checked /> Rajal </span> 
                <span class="bg bg-info" style="padding:8px;margin:2px;"><input type="radio" value="rajalnap" name="jenisperiksa" /> Ranap </span>
                <a onclick="loadreport_cakupanpoli()"  class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhreport_cakupanpoli()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
                <h3>Laporan <?= $title_page ?></h3>
              <table  style="margin-top:5px;" class="table table-hover table-bordered table-striped" >
                <thead>
                  <tr class="bg bg-gray">
                    <td width="80px"></td>
                    <td>BULAN</td>
                    <td>JUMLAH</td>
                  </tr>
                  <tr id="waktu"></tr>
                  <tr ><td colspan="3">&nbsp;</td></tr>
                  <tr class="bg bg-gray">
                    <td colspan="2">PASIEN LAMA</td>
                    <td>PASIEN BARU</td>
                  </tr>
                  <tr id="jenispasien"></tr>
                  <tr class="bg bg-gray" id="carabayar"><td colspan="3">&nbsp;</td></tr>
                  <tr id="jumlahbayar"><td colspan="3">&nbsp;</td></tr>
                </table>
                
                <table class="table table-hover table-bordered table-striped" >
                  <thead>
                    <tr style="font-size: 15px;"><td colspan="4"><b>10 BESAR PENYAKIT</b></td></tr>
                  </thead>
                    <tr class="bg bg-gray"><td>NO</td><td>DIAGNOSIS</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
                  <tbody id="penyakit">
                  </tbody>
                </table>

                  <table class="table table-hover table-bordered table-striped" >
                  <thead>
                    <tr style="font-size: 15px;"><td colspan="4"><b>10 BESAR TINDAKAN TERSERING</b></td></tr>
                  </thead>
                    <tr class="bg bg-gray"><td>NO</td><td>TINDAKAN</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
                  <tbody id="tindakan">
                  </tbody>
                </table>
              </div>
            </div>  
          <?php } else if($mode=='kondisikeluar') { ?>

          <div class="row">
              <div class="col-xs-1" style="margin-bottom:6px;">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              </div>
              <div class="col-md-3">
                <input type="text" class="datepicker" id="tahun" size="7" value="" readonly>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
              </div>
            </div>

            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold">Data Cakupan</h5>
                </div>
                <div class="box-body">
                  <div class="col-xs-12" id="tampildata"></div>
                </div>
            </div>

          </div>
          </div> 
        <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        