<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Cadmission extends MY_controller
{
    function __construct()
    {
       parent::__construct();
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
       $this->load->model('mkombin');
       $this->load->model('mbpjs');
    }
    ///////////////////////ADMISSION PENDAFTARAN POLI///////////////////
    //-------------------------- vv Standar
    public function setting_pendaftaran_poli()
    {
        return [    'content_view'      => 'admission/v_pendaftaran_poli',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'pendaftaran_poli',
                    'active_menu_level' => '',
                    'plugins'           => [ PLUG_DATATABLE, PLUG_DATE ,PLUG_DROPDOWN]
               ];
    }
    public function pendaftaran_poli()//ke halaman list pendaftaran
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
         { 
            $data                   = $this->setting_pendaftaran_poli();
            $data['title_page']     = 'Pendaftaran Poliklinik';
            $data['mode']           = 'view';
            $data['table_title']    = 'List Pendaftaran Pasien ';
            $data['script_js']      = ['pendaftaran_pendaftaran','js_pendaftaranpoli','js_cetakresumepasien','js_report_resumeralan','js_rujukinternal','antrian_antrian'];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    // order BERKAS REKAM MEDIS
    public function arusrekammedis()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ARUSREKAMMEDIS)) //lihat define di atas
        {
        $data = [  'content_view'      =>'admission/v_pendaftaran_verifpasienbpjs',
                    'active_menu'      =>'admission',
                    'active_sub_menu'  =>'arusrekammedis',
                    'active_menu_level'=>'',
                    'plugins'          =>[ PLUG_DATATABLE, PLUG_DATE ,PLUG_DROPDOWN],
                    'title_page'       =>'Order Rekam Medis',
                    'table_title'      =>'List RM',
                    'script_js'        =>['js_pendaftaran_arusrekammedis']
                ];
        $this->load->view('v_index', $data);
        }else{aksesditolak();}
    }
    public function ubahstatus_arusrekamedis()
    {
        $status = $this->input->post("status");
        $id = $this->input->post("id");
        $q  = $this->ql->person_pendaftaran_insert_or_update($id,['isberkassudahada'=>$status]);
        pesan_success_danger($q,"Berkas berhasil di update..!","Berkas gagal di update..!","js");
    }
    // ambil data arus rekam medis
    public function loaddtarusrekamedis(){echo json_encode($this->mkombin->loaddtarusrekamedis($this->input->post('tgl1'),$this->input->post('tgl2'),$this->input->post('status')));}
    // data pasien bpjs baik skdp maupun pendaftaran online dan anjungan dimana berkas belum ditemukan
    public function klaimverifikasijaminan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_VERIFPASIENBPJS)) //lihat define di atas
        {
        $data = [  'content_view'      =>'admission/v_klaimverifikasijaminan',
                    'active_menu'      =>'admission',
                    'active_sub_menu'  =>'verifikasiklaimjaminan',
                    'active_menu_level'=>'',
                    'plugins'          =>[ PLUG_DATATABLE, PLUG_DATE ,PLUG_DROPDOWN],
                    'title_page'       =>'Klaim Verifikasi Jaminan BPJS',
                    'script_js'        =>['js_cetakresumepasien','js_klaimverifikasijaminan']
                ];
        $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function klaimdatabulananbpjs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_VERIFPASIENBPJS)) //lihat define di atas
        {
        $data = [  'content_view'      =>'admission/v_klaimdatabulananbpjs',
                    'active_menu'      =>'admission',
                    'active_sub_menu'  =>'klaimdatabulananbpjs',
                    'active_menu_level'=>'',
                    'plugins'          =>[ PLUG_DATATABLE, PLUG_DATE ,PLUG_DROPDOWN],
                    'title_page'       =>'Klaim Data Bulanan BPJS',
                    'script_js'        =>['js_klaimverifikasijaminan']
                ];
        $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function list_databulananbpjs()
    {
        $this->load->model('mbpjs');

        $response = $this->mbpjs->get_dt_bulanan(true);
        $no = 1;
        $data = [];
        foreach($response->result() as $res){
            $row = [
                $no++,
                $res->norm,
                $res->identitas.' '. $res->namalengkap,
                $res->tglperiksa,
                $res->notelpon,
                $res->poli,
                $res->jenispemeriksaan,
                $res->carabayar,
                'alamat'
            ];

            $data [] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => intval( $this->mbpjs->count_all() ),
            "recordsFiltered" => intval( $this->mbpjs->count_filtered()),
            "data" => $data,
        );

        echo json_encode($output);
    }

    //mahmud, clear
    public function dt_verifikasiklaim()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_verifikasiklaim_(true);
        $data=[]; $no=0;
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->norm;
                $row[] = $obj->identitas.' '.$obj->namalengkap;
                $row[] = $obj->waktudaftar;
                $row[] = $obj->waktuperiksa;
                $row[] = $obj->caradaftar;
                $row[] = $obj->poli;
                $row[] = $obj->jenispemeriksaan;
                $row[] = $obj->carabayar;
                $menu  = ' <a onclick="triaseasesmenmedisugd('.$obj->idpendaftaran.')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Triase & Asesmen Medis Gawat Darurat"><i class="fa fa-print"></i> RM.02</a>';
                $menu .= ' <a onclick="asasmenmedisrawatjalan('.$obj->idpendaftaran.')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Asesmen Medis Rawat Jalan"><i class="fa fa-print"></i> RM.03</a>';
                $menu .= ' <a onclick="formInacbg('.$obj->idpendaftaran.')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Formulir INA-CBG"><i class="fa fa-print"></i> INA-CBG</a>';
                $menu .= ' <a onclick="resumepasien('.$obj->idpendaftaran.')" class="btn  '.(($obj->resumeprint=='1')?'bg-black':'btn-primary').' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN"><i class="fa fa-print"></i> Resum</a>';
                $menu .= (($obj->idinap == 0) ? '' : ' <a onclick="print_ringkasanpasienpulang(' . $obj->idpendaftaran . ',' . $obj->idinap . ')" class=" btn bg-maroon btn-xs" '.ql_tooltip('Ringkasan Pasien Pulang').'><i class="fa fa-trello"></i></a>');
                $menu .= (($obj->idinap == 0) ? '' : ' <a onclick="print_forminacbgranap('.$obj->idpendaftaran.')" class=" btn btn-primary btn-xs" '.ql_tooltip('Formulir INA-CBG').'><i class="fa fa-delicious"></i></a>');
                $menu .= (($obj->idinap == 0) ? '' : ' <a onclick="cetak_catatan_terintegrasi('.$obj->idpendaftaran.')" class=" btn btn-info btn-xs" '.ql_tooltip('Catatan Terintegrasi').'><i class="fa fa-align-justify"></i></a>'); 
                $row[] = $menu;
                // note : norujukanKeluar dan norujukanKontrol perlu re-check
                $menuAction =   '<a id="addSep"
                    norujukan="'.$obj->norujukan.'"
                    idpendaftaran="'.$obj->idpendaftaran.'"
                    norm="'.$obj->norm.'"
                    notelepon="'.$obj->notelpon.'"
                    tglperiksa="'.$obj->tglperiksa.'"
                    namapasien="'.$obj->identitas.' '.$obj->namalengkap.'"
                    class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buat SEP">
                    <i class="fa fa-plus"></i>
                    SEP
                    </a>';

                $menuAction .= ' <a id="addKontrol"
                    idpendaftaran="'.$obj->idpendaftaran.'"
                    nosep="'.$obj->nosep.'"
                    tglperiksa="'.$obj->tglperiksa.'"
                    namapasien="'.$obj->identitas.' '.$obj->namalengkap.'"
                    class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buat Rencana Kontrol / Inap">
                    <i class="fa fa-plus"></i>
                    Kontrol/inap
                    </a>';

                $row[] = $menuAction;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->totaldt_verifikasiklaim(),
            "recordsFiltered" => $this->mdatatable->filterdt_verifikasiklaim(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    // mahmud, clear :: FORM INA-CBG
    public function verif_getidentitaspasien()
    {
       // $this->load->model('mviewql');
       $identitas = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$this->input->post('idp')])->row_array();
       $nominal = $this->db->query("select nominal from keu_tagihan where idpendaftaran='".$this->input->post('idp')."'")->row_array()['nominal'];
       $hp = $this->mkombin->get_dtdiagnosaskdp($this->input->post('idp'))->result();
       echo json_encode(['identitas'=>$identitas,'nominal'=>$nominal,'hp'=>$hp]);
    }
    
    public function cetak_asesmenmedisralan()
    {
       $data['identitas'] = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$this->input->post('idp')])->row_array();
       $data['asesmen']   = $this->db->query("SELECT *, date_format(rra.tanggalperiksa,'%d/%m/%Y') as tglperiksa from rs_ralan_asesmenmedis rra WHERE rra.idpendaftaran='".$this->input->post('idp')."'")->row_array();
       echo json_encode($data);
    }
    
    public function cetak_triaseasesmenmedisugd()
    {
       $idp = $this->input->post('idp');
       $data['identitas']     = $this->mkombin->get_identitaspasien($idp);
       $data['asesmen']       = $this->db->query("SELECT *, date_format(rra.tanggalmasuk,'%d/%m/%Y') as tglmasuk, date_format(rra.tanggalkeluar,'%d/%m/%Y') as tglkeluar  from rs_ugd_asesmenmedis rra WHERE rra.idpendaftaran='".$this->input->post('idp')."'")->row_array();
       $data['kstart']        = $this->db->query('SELECT uks.* FROM rs_ugd_asesmenmedis rua join ugd_kriteria_start uks on uks.idkriteriastart=rua.idkriteriastart where rua.idpendaftaran='.$idp)->row_array();
       $data['scalaflaccugd'] = $this->db->get_where('rs_ugd_scalaflaccdibawahenamtahun',['idpendaftaran'=>$idp])->result_array();
       $data['scalaflacc']    = $this->db->order_by('pengkajian,nilai','asc')->get('rs_scalaflaccdibawahenamtahun')->result_array();
       echo json_encode($data);
    }
    
    public function cetak_catatanintegrasiranap()
    {
        $catatan= [];
        $idp    = $this->input->post('idp');
        $data['pasien'] = $this->mkombin->get_identitaspasien($idp);
        $dtinap = $this->db->query("select idinap, namadokter(ri.idpegawaidokter) as dokterutama, namadokter(ri.iddokterrawatbersama) as dokterbersama, date_format(waktumasuk,'%d/%m/%Y') as tanggalmasuk, time(waktumasuk) as jammasuk, rb.nobed, rbs.namabangsal, rk.kelas
                        from rs_inap ri
                        join rs_bed rb on rb.idbed = ri.idbed
                        join rs_bangsal rbs on rbs.idbangsal = rb.idbangsal
                        join rs_kelas rk on rk.idkelas = ri.idkelas
                        where idpendaftaran = '".$idp."'");
        
        if($dtinap->num_rows() > 0)
        {
            $data['ranap']   = $dtinap->row_array();
            $dtranap= $this->mkombin->get_rencanamedisranaprowarray($data['ranap']['idinap']);
            $data['catatan'] = $this->mkombin->get_catatanterintegrasiranap($dtranap);
            $data['dtranap'] = $dtranap;
        }
        else
        {
            $data['ranap'] = 'null';
        }       
        
        echo json_encode($data);
    }
    
    public function cetak_forminacbgranap()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $sql = 'select pp.norm, namapasien(ppas.idperson) as namapasien, pper.jeniskelamin, usia(pper.tanggallahir) as usia, pper.tanggallahir, date(ri.waktumasuk) as waktumasuk, date(ri.waktukeluar) as waktukeluar, ri.carapulang, namadokter(ri.idpegawaidokter) as dokterdpjp, rid.icd,ric.namaicd, rid.jenis, rid.level, rb.namabangsal
        from rs_inap ri
        join person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson
        join rs_bed b on b.idbed = ri.idbed
        join rs_bangsal rb on rb.idbangsal = b.idbangsal
        left join rs_inapdetail rid on rid.idinap = ri.idinap
        left join rs_icd ric on ric.icd = rid.icd
        WHERE pp.idpendaftaran = '.$idpendaftaran.' order by rid.level';
        $data = $this->db->query($sql)->result_array();
        echo json_encode($data);
    }
    
    public function cetak_ringkasanpasienpulang()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $idinap = $this->input->post('idinap');

        $cekSignature = $this->db->query( 
                            "SELECT ri.* 
                            from rs_inap ri
                            join person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran 
                            JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter                           
                            LEFT join person_signature persign on persign.id_person = perpeg.idperson
                            WHERE pp.idpendaftaran = '".$idpendaftaran."'"
                        )->row_array();

        $cekBangsal = $this->db->query( 
                            "SELECT ri.* 
                            from rs_inap ri
                            join person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
                            join rs_bed b on b.idbed = ri.idbed
                            join rs_bangsal rb on rb.idbangsal = b.idbangsal
                            WHERE pp.idpendaftaran = '".$idpendaftaran."'"
                        )->row_array();

        $sql ="select ".((empty($cekSignature))?"'' as signature," : "persign.signature,")."ri.is_signature, if(ri.resikotinggi=0,'Tidak','Ya') as resikotinggi, pper.alamat, pp.norm, namapasien(ppas.idperson) as namapasien, pper.jeniskelamin, usia(pper.tanggallahir) as usia, pper.tanggallahir, date(ri.waktumasuk) as waktumasuk, date(ri.waktukeluar) as waktukeluar, ri.carapulang, namadokter(ri.idpegawaidokter) as dokterdpjp, rid.icd,ric.namaicd, rid.jenis, rid.level,"
                .((empty($cekBangsal))?"'' as namabangsal," : "rb.namabangsal,")."rk.kelas,
                ri.indikasirawatinap, ri.diagnosis, ri.komordibitaslain, ri.ringkasanriwayat_pemeriksaanfisik, ri.hasilpemeriksaanpenunjang, ri.terapiselamaranap, ri.terapipulang, ri.kondisisaatpulang, ri.carapulang, ri.anjuran, ri.tindaklanjut, ri.diagnosisDPJP
                from rs_inap ri
                join person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
                join person_pasien ppas on ppas.norm = pp.norm
                join person_person pper on pper.idperson = ppas.idperson"
                .((empty($cekBangsal))?"" : " join rs_bed b on b.idbed = ri.idbed join rs_bangsal rb on rb.idbangsal = b.idbangsal")
                ." join rs_kelas rk on rk.idkelas = ri.idkelas
                JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter"
                .((empty($cekSignature))?"" : " LEFT join person_signature persign on persign.id_person = perpeg.idperson")
                ." left join rs_inapdetail rid on rid.idinap = ri.idinap
                left join rs_icd ric on ric.icd = rid.icd
                WHERE pp.idpendaftaran = '".$idpendaftaran."' AND ri.idinap='".$idinap."' order by rid.level";
        
        $data = $this->db->query($sql)->result_array();
        echo json_encode($data);
    }

    // mahmud, clear :: FORM RESUME PASIEN PULANG RAJAL
    public function verif_resumepasien()
    {
        $idpendaftaran = $this->input->post('idp');
        $this->db->update('person_pendaftaran',['resumeprint'=>'1'],['idpendaftaran'=>$this->input->post('idp')]);
        $this->load->model('mviewql');
        $identitas = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$this->input->post('idp')])->row_array();
        $vitalsign =  $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
        $tindakan =  $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');
        $laboratorium =  $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
        $radiologi =  $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
        $obat = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
        $grup = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  FROM rs_barangpemeriksaan b 
        WHERE b.idpendaftaran='".$idpendaftaran."' GROUP by grup")->result();
        $diagnosa =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
        
        $getket = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, 
        if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, 
        if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, 
        if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa) anamnesaverifiklaim,
        if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa,  
       
        r.anamnesa as anamnesadpjp,
        r.keterangan as keterangandpjp,
        r.diagnosa as diagnosadpjp

        from person_pendaftaran p, rs_pemeriksaan r 
        where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();

        $ketradiologi = [];
        if(empty(!$getket))
        {
            $kkr='';$ks='';$kk='';$ka='';$kd='';
            foreach ($getket as $obj) {
                $kkr = $obj->keteranganradiologi;

                $ks  = $obj->saranradiologi;

                $kk .= $obj->keterangan;

                $ka .= $obj->anamnesaverifiklaim;

                $kd .= $obj->diagnosa; 

                $kj  = $obj->anamnesadpjp;
                $kz =  $obj->keterangandpjp;
                $kl = $obj->diagnosadpjp;
            }
            $ketradiologi = [
                                'keteranganradiologi'=>$kkr,
                                'saranradiologi'=>$ks,
                                'keterangan'=>$kk,
                                'anamnesaverifklaim'=>$ka,
                                'diagnosa'=>$kd,
                                'anamnesadpjp' => $kj,  
                                'keterangandpjp' => $kz,
                                'diagnosadpjp' => $kl
                            ];
        }

        $response = [
            'identitas'=>$identitas,
            'vitalsign'=>$vitalsign,
            'tindakan'=>$tindakan,
            'laboratorium'=>$laboratorium,
            'radiologi'=>$radiologi,
            'obat'=>$obat,
            'diagnosa'=>$diagnosa,
            'ketradiologi'=>$ketradiologi,
            'grup'=>$grup
        ];
        echo json_encode($response);
    }
    // -- ambil data pendaftaran
    public function loaddtpendaftaran(){ $this->search_by_date_range();}//echo json_encode($this->mkombin->loaddtpendaftaran(format_date_to_sql($this->input->post('tgl1')),format_date_to_sql($this->input->post('tgl2'))));}
      
    // ubah status berkas klaim 
    public function ubahstatusberkasklaim()
    {
        $post = $this->input->post();
        $arrMsg = $this->db->update("person_pendaftaran",['idstatusklaim'=>$post['ids']],['idpendaftaran'=>$post['idp']]);
        pesan_success_danger($arrMsg, "Ubah Status Berhasil.", "Ubah Status Gagal","js");
    }
//    public function ubahstatusklaimlayak()
//    {
//        $post = $this->input->post();
//        $status = $this->db->select('idstatusklaim')->where('isselesai',1)->get('bpjs_statusklaim')->row_array()['idstatusklaim'];
//        $arrMsg = $this->db->update("person_pendaftaran",['nominalklaimditerima'=>$post['nominal'],'idstatusklaim'=>$status],['idpendaftaran'=>$post['idp']]);
//        pesan_success_danger($arrMsg, "Ubah Status Berhasil.", "Ubah Status Gagal","js");
//    }
    
    // ubah status pengecekan berkas pasien bpjs baik skdp maupun pendaftaran online dan anjungan
    public function pendaftaran_updateverifpasienbpjs()
    {
        $update = $this->ql->person_pendaftaran_insert_or_update($this->input->post('id'),['isberkassudahada'=>1]);
        pesan_success_danger($update,"Ubah status pengecekan berkas berhasil..!","Ubah status pengecekan berkas gagal..!","js");
    }
    // cari berdasarkan idpendaftaran
    public function searchbypendaftaran()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("select rp.status, ppd.isberkassudahada,ppd.caradaftar,ppd.isantri, (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, rp.idpemeriksaan, pp.idperson, ppd.idunit, pp.namalengkap, `ppd`.`idpendaftaran`, `ppd`.`norm`, `ppd`.`waktu`, ppd.idstatuskeluar, `ppd`.`carabayar`, `ppd`.`norujukan`, `ppd`.`nosep`, `ppd`.`jenispemeriksaan`, `ru`.`namaunit`, ppd.isantri as noantrian  FROM person_pendaftaran ppd ,rs_unit ru ,person_pasien ppas ,person_person pp ,rs_pemeriksaan rp
                WHERE ru.idunit= ppd.idunit and pp.idperson = ppas.idperson and ppas.norm = ppd.norm and rp.idpendaftaran = ppd.idpendaftaran and ppd.idpendaftaran='$id'")->result();
        echo json_encode($query);
    }
    /// -- cari range pendaftaran
    public function search_by_date_range()
    {    
        $accesJadwalOp = ($this->pageaccessrightbap->checkAccessRight(V_JADWALOPERASI) ) ? ' 0 as isjadwaloperasi,' : ' 1 isjadwaloperasi, ';
        $post = $this->input->post();
        $tglawal = ((empty($post['tglawal']))? date("Y-m-d") : format_date_to_sql($post['tglawal']));
        $tglakhir= ((empty($post['tglakhir']))? date("Y-m-d") : format_date_to_sql($post['tglakhir']));
        $tahun1  = empty($post['tglawal']) ? date('Y') : date('Y', strtotime($tglawal));
        $tahun2  = empty($post['tglakhir']) ? date('Y') : date('Y', strtotime($tglakhir));
        $arrData = $this->db->query(""
                . "SELECT rp.idpemeriksaan, ppd.statuspulang, date_format(ppd.waktuperiksa,' %d/%m/%Y') as tgljadwal, 
                    ppd.pasienprolanis, ifnull(ppd.tanggal_kunjunganberikutnya,'') as tanggal_kunjunganberikutnya,  
                    ppd.idjadwalgrup, (select if(count(1) > 0,'-pesan ranap-','') from rs_inap inap where inap.idpendaftaran=ppd.idpendaftaran) isranap, 
                    pp.tanggallahir, pp.alamat,pp.notelpon, pp.cetakkartu, ppd.isberkassudahada,ppd.caradaftar,ppd.isantri, ppd.kategoriskrining,
                    (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, 
                    pp.idperson, ppd.idunit, pp.nik, 
                    concat(ifnull(pp.identitas,''),' ', pp.namalengkap) namalengkap, `ppd`.`idpendaftaran`, `ppd`.`norm`, `ppd`.`waktu`, 
                    ppd.waktuperiksa, ppd.idstatuskeluar, `ppd`.`carabayar`, `ppd`.`norujukan`, `ppd`.`nosep`, 
                    `ppd`.`jenispemeriksaan`, ppd.isantri as noantrian, `ru`.`namaunit`  
                    FROM  person_pendaftaran ppd 
                    join person_pasien ppas on ppas.norm=ppd.norm
                    join person_person pp on pp.idperson = ppas.idperson
                    join rs_pemeriksaan rp on rp.idpendaftaran = ppd.idpendaftaran
                    join rs_unit ru on ru.idunit = rp.idunit "
                . "WHERE ppd.tahunperiksa between '".$tahun1."' and '".$tahun2."' and ( date(ppd.waktu) between '".$tglawal."' and '".$tglakhir."' or date(ppd.waktuperiksa) between '".$tglawal."' and '".$tglakhir."' ) "
                . "ORDER by waktu desc")->result();
        echo json_encode($arrData);
    }
    
    public function dt_pendaftaran()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_pendaftaran_(true);
        $db = $this->db->last_query();
        $data=[]; $no=0;
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                
                $menucetak  = '<a id="cetakkartu" idp="'.$obj->idpendaftaran.'" norm="'.$obj->norm.'" namapasien="'.$obj->namalengkap.'" nik="'.$obj->nik.'" tanggallahir="'.$obj->tanggallahir.'" alamat="'.$obj->alamat.'" data-toggle="tooltip" data-original-title="Cetak kartu" class="btn btn-primary btn-xs" ><i class="fa fa-credit-card"> Kartu Pasien</i></a>';
                $menucetak .= ' <a id="listmenucetak" idp="'.$obj->idpendaftaran.'" norm="'.$obj->norm.'" class="btn btn-primary btn-xs"><i class="fa fa-print"></i> Berkas Pasien</a>';

                $menu  = ' '.(( $this->pageaccessrightbap->checkAccessRight(V_JADWALOPERASI) && ($obj->idstatuskeluar==1 || $obj->idstatuskeluar==2) )?' <a id="bookingjadwaloperasi"  class="btn btn-success btn-xs" '.ql_tooltip('Booking Jadwal Operasi').' idp="'.$obj->idpendaftaran.'"><i class="fa fa-calendar"></i></a>':'');
                $menu .= ' '.(($obj->jenispemeriksaan=='rajalnap' || $obj->jenispemeriksaan=='ranap' || $obj->isranap != '')? '': '<a onclick="pindahRanap('.$obj->norm.','.$obj->idpendaftaran.')" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Pindah RANAP"><i class="fa fa-bed"></i></a>');
                
                //jika status selesai / 2
                if($obj->idstatuskeluar != 2)
                {
                    $menu .= ' <a id="editPendaftaranPoli" alt="'.$obj->idpendaftaran.'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Ubah"><i class="fa fa-pencil"></i></a>';
                    $menu .= ' <a id="pindahPemeriksaanPasien" idp="'.$obj->idpendaftaran.'"  class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Pindah Jadwal Periksa"><i class="fa fa-rocket"></i></a>';
                }
                
                if($obj->idstatuskeluar == 2 && $this->pageaccessrightbap->checkAccessRight(MENU_UBAHDATAPENDAFTARAN_SELESAI) )
                {
                    $menu .= ' <a id="editPendaftaranPoli" alt="'.$obj->idpendaftaran.'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Ubah"><i class="fa fa-pencil"></i></a>';
                }
                
                $menu .= ' '.(($obj->isberkassudahada=='0')? '<a onclick="changeToProses('.$obj->idpendaftaran.',2)" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Order RM"><i class="fa fa-search"></i></a>' : '');

                $beriantrian = '';
                if($obj->isantri==null){
                    $beriantrian='<a id="pemeriksaanklinikBeriantrian" class=" btn btn-success btn-xs" data-original-title="Beri Antrian"';    
                }else{
                    $beriantrian='<a onclick="cetakAntrian('.$obj->idpemeriksaan.','.$obj->idperson.')" class=" btn bg-purple btn-xs" data-original-title="Cetak Antrian"';
                }

                // menu buka layanan
                if($obj->idstatuskeluar=='3')
                {
                    $menu .=' <a onclick="buka_pelayanan('.$obj->idpendaftaran.')" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buka Pelayanan"><i class="fa fa-stethoscope"></i></a> ';
                    $beriantrian='<a class=" btn bg-black btn-xs" style="cursor:not-allowed;" data-original-title="Terantrikan"';
                }

                $menu .= ' '.(($obj->isantri==null) ? '' : '<a id="pemeriksaanklinikRujuk" alt="'.$obj->idpemeriksaan.'"  alt2="'.$obj->waktuperiksa.'" alt3="'.$obj->tgljadwal.'" idp="'.$obj->idperson.'" data-toggle="tooltip" data-original-title="Tambah Layanan" class=" btn btn-primary btn-xs" ><i class="fa fa-plus"></i></a>' );
                if($obj->jenispemeriksaan!=='rajal' && $obj->jenispemeriksaan!=='homecare')
                { 
                    $beriantrian='<a class=" btn bg-black btn-xs" style="cursor:not-allowed;" data-original-title="Terantrikan"';
                }
                
                $menu .= ' '.$beriantrian.' alt="'.$obj->idpemeriksaan.'" alt2="'.$obj->idunit.'" alt3="'.$obj->idperson.'" alt4="'.$obj->idpendaftaran.'"  data-toggle="tooltip"><i class="fa fa-check"></i></a>';
//                $menu .= ' <a id="blacklistpasien" namaunit="'.$obj->namaunit.'" nama="'.$obj->namalengkap.'"  norm="'.$obj->norm.'" idunit="'.$obj->idunit.'" '. ((empty($obj->blacklist)) ? ' mode="Blacklist" class="btn btn-xs btn-danger" '.  ql_tooltip('Blacklist Pasien') : ' mode="Un Blacklist" class="btn btn-xs btn-success" '.  ql_tooltip('Un Blacklist Pasien') ) .'><i class="fa fa-ban"></i></a>' ;
                
                $db_apm = 'apm_daftar';
                $sqls = " SELECT * FROM ".$db_apm." WHERE norm='".$obj->norm."' AND DATE(NOW()) <= tglperiksa AND carabayar='".$obj->carabayar."'";
                $query_apm_daftar = $this->db->query($sqls)->row_array();
                
                $statuscetakSEP = '';
                if( !empty( $obj->sudahcetakSEP ) ){
                    $statuscetakSEP = '<label>  - SEP sudah terbit </label>';
                }
                
                $notif_anjunganonline='';
                if( !empty( $query_apm_daftar ) ){
                    $notif_anjunganonline = empty($obj->sudahcetakSEP) ? '<i style="color:red;" class="fa fa-info-circle" data-toggle="tooltip" data-placement="right" title="Belum di Validasi"></i> ' : ''; 
                }

                $row[] = $obj->norm;
                $row[] = $obj->nik;
                $row[] = $obj->namalengkap;                    
                $row[] = $obj->waktu;
                $row[] = $obj->waktuperiksa;
                $row[] = $notif_anjunganonline.$obj->caradaftar;
                $row[] = $obj->namaunit.' - '. $obj->jenispemeriksaan .(($obj->jenispemeriksaan=='rajal') ? '<br> '.$obj->isranap : '' ) .' <br><small class="text-bold">Status Pulang : '.$obj->statuspulang.'</small>';
                $row[] = $obj->carabayar.$statuscetakSEP; // status cetakSEP
                $row[] = '<div style="padding-top:6px;"><label class="label label-'.$obj->kategoriskrining.'" style="padding:4.5px 8.5px;font-size:9.5px;">'.(($obj->kategoriskrining==null)? '' : $obj->kategoriskrining ).'</label></div>';
                $row[] = (($obj->pasienprolanis==1) ? '<i class="fa fa-check fa-2x text text-green"></i>' : '<i class="fa fa-close fa-2x text text-red"></i>' );
                $row[] = $obj->tanggal_kunjunganberikutnya;
                $row[] = $menucetak;
                $row[] = $menu;
                $row[] = $obj->nosep;
                $row[] = $obj->norujukan;
                $row[] = $obj->tanggallahir;
                $row[] = $obj->notelpon;
                $row[] = $obj->alamat;
                $row[] = statuskeluar($obj->idstatuskeluar);
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            // "recordsTotal" => $this->mdatatable->total_dt_pendaftaran(),
            "recordsTotal" => 0,
            "recordsFiltered" => $this->mdatatable->filter_dt_pendaftaran(),
            // "recordsFiltered" => 0,
            "data" =>$data,
            "db"=>$db
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function edit_pindahjadwalperiksa()
    {
        $idpendaftaran = $this->input->post('idp');
        $sql = "select pp.norm, pper.identitas,pper.namalengkap,  pper.nik, pper.tanggallahir, pper.alamat, pper.jeniskelamin, pp.idunit, rj.tanggal, namadokter(rj.idpegawaidokter) as namadokter, al.namaloket, ru.namaunit
                from person_pendaftaran pp 
                join person_pasien ppas on ppas.norm = pp.norm
                join person_person pper on pper.idperson = ppas.idperson
                join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null
                join rs_jadwal rj on rj.idjadwal = rp.idjadwal
                join antrian_loket al on al.idloket = rj.idloket
                join rs_unit ru on ru.idunit = rj.idunit
                WHERE pp.idpendaftaran = '".$idpendaftaran."'";
        $dt = $this->db->query($sql)->row_array();
        echo json_encode($dt);
    }
    
    /// -- hapus pendaftaran dari js
    public function hapus_data_pendaftaran()
    {
        $id     = $this->input->post('i');
        $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id,['idstatuskeluar'=>'3']);
        $arrMsg = $this->rs_pemeriksaan_update_where(['status'=>'batal'],['idpendaftaran'=>$id]);
        $datan  = $this->db->query("select norm from rs_pemeriksaan where idpendaftaran='$id'")->row_array();
        $datal  = [
            'url'     => get_url(),
            'log'     => 'user:'.$this->session->userdata('username').';status:batal;idpendaftaran:'.$id.';norm:'.$datan['norm'],
            'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Delete Success.!', 'Delete Failed.!', 'js');
    }
    // -- buka_pelayanan dari js
    public function buka_pelayanan()
    {
        $id = $this->input->post('i');
        $antrian = $this->db->query("select IF(isantri>0, 'antrian', 'pesan') antrian from person_pendaftaran where idpendaftaran='$id'")->row_array();
        $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id,['idstatuskeluar'=>'0']);
        $arrMsg = $this->rs_pemeriksaan_update_where(['status'=>$antrian['antrian']],['idpendaftaran'=>$id]);
        $datan = $this->db->query("select norm from rs_pemeriksaan where idpendaftaran='$id'")->row_array();
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:'.$this->session->userdata('username').';status:buka pelayanan/'.$antrian['antrian'].';idpendaftaran:'.$id.';norm:'.$datan['norm'],
            'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Buka Pelayanan Success.!', 'Buka Pelayanan Failed.!', 'js');
    }

    public function add_pendaftaran_poli()//tambah pendaftaran poliklinik
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
         {
            $data                       = $this->setting_pendaftaran_poli(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page']         = 'Entri Pendaftaran Poliklinik';
            $data['mode']               = 'add';
            $data['dt_agama']           = $this->mkombin->get_data_enum('person_person','agama');
            $data['dt_identitas']       = $this->mkombin->get_data_enum('person_person','identitas');
            $data['dt_jenisperiksa']    = $this->mkombin->get_data_enum('person_pendaftaran','jenispemeriksaan');
            $data['dt_carabayar']       = $this->mkombin->get_data_enum('person_pendaftaran','carabayar');
            $data['dt_jeniskelamin']    = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            $data['dt_golongandarah']   = $this->mkombin->get_data_enum('person_person','golongandarah');
            $data['dt_rhesus']          = $this->mkombin->get_data_enum('person_person','rh');
            $data['dt_perkawinan']      = $this->mkombin->get_data_enum('person_person','statusmenikah');
            $data['dt_skrining']      = $this->mkombin->get_data_enum('person_pendaftaran','kategoriskrining');
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $data['dt_pekerjaan']       = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('demografi_pendidikan');
            $data['dt_pendidikan']      = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('rs_kelas');
            $data['dt_kelas']           = $this->mgenerikbap->select('*')->result();
            $data['data_edit']          = '';
            $data['plugins']            = [ PLUG_DROPDOWN , PLUG_DATE, PLUG_DATATABLE, PLUG_CHECKBOX ];
            $data['script_js']          = ['js_admission-pendaftaranpoli'];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    public function edit_pendaftaran_poli()//edit pendaftaran poliklinik
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
         {
            $data                       = $this->setting_pendaftaran_poli(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page']         = 'Edit Pendaftaran Poliklinik';
            $data['mode']               = 'edit';
            $data['data_edit']          = '';
            $data['plugins']            = [ PLUG_DROPDOWN , PLUG_DATE , PLUG_CHECKBOX];
            $data['script_js']          = ['js_admission-pendaftaranpoli'];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    
    public function pindahjadwal_pendaftaran_poli()//edit pendaftaran poliklinik
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
         {
            $data                       = $this->setting_pendaftaran_poli(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page']         = 'Pindah Jadwal Pemeriksaan';
            $data['mode']               = 'edit';
            $data['data_edit']          = '';
            $data['plugins']            = [ PLUG_DROPDOWN , PLUG_DATE , PLUG_CHECKBOX];
            $data['script_js']          = ['js_admission-pendaftaranpoli'];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    
    
    public function cari_pasien()//cari data pasien
    {
        if ($this->input->post('num') == 0)
        {
            echo json_encode(["data" => $this->mkombin->cari_norm_nojkn($this->input->post('id'), $this->input->post('lim')), "jumlah" => $this->mkombin->jumlah_norm_nojkn($this->input->post('id'))]);
        }
        else
        {
            echo json_encode(["data" => $this->mkombin->cari_norm_nojkn($this->input->post('id'), $this->input->post('lim'))]);
        }
    }
    public function cari_pasienspesifik()
    {
        if ($this->input->post('num') == 0)
        {
            echo json_encode(["data" => $this->mkombin->cari_pasienspesifik($this->input->post('id')), "jumlah" => $this->mkombin->jumlah_pasienspesifik()]);
        }
        else
        {
            echo json_encode(["data" => $this->mkombin->cari_pasienspesifik($this->input->post('id'), $this->input->post('lim'))]);
        }
    }
    public function cari_jadwalpoli() //cari poli di rs jadwal berdasarkan tanggal input
    {
        echo json_encode($this->mkombin->cari_jadwalpoli(date('Y-m-d', strtotime($this->input->post('date'))),$this->input->post('grup'))); 
    }

    # add code 
    # dev : Ikhsan MS
    # menampilkan info APM wa,bpjs,rujukan di atas edit_pendaftaranpoli
    public function tampil_apm()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
        {
            if( !empty( $_POST ) ){
                
                $norm       = $_POST['norm'];
                $carabayar  = $_POST['carabayar'];
                
                #query APM
                $db = 'apm_daftar';
                $sql = " SELECT * FROM ".$db." WHERE norm='".$norm."' AND DATE(NOW()) <= tglperiksa ORDER BY idapmdaftar DESC LIMIT 1";
                $query_apm_daftar = $this->db->query($sql)->row_array();
            
                $sql_asalrujukan    = $this->db->select("idklinik as id, concat(ifnull(kodeklinik,''),' ', ifnull(namaklinik,'') ) as text");
                $sql_asalrujukan    = $this->db->where("idklinik",$query_apm_daftar['idasalrujukan']);
                $sql_asalrujukan    = $this->db->get("bpjs_klinikperujuk")->row_array();
                
                $asalrujukan = '';
                if( !empty( $sql_asalrujukan)  ){
                    $asalrujukan = $sql_asalrujukan['text'];
                }
                
                if( !empty($query_apm_daftar) ){
                                    
                    $return = [
                        'msg'       => 'success',
                        'nobpjs'    => $query_apm_daftar['nobpjs'],
                        'norujukan' => $query_apm_daftar['norujukan'],
                        'nowa'      => $query_apm_daftar['nowa'],
                        'cekpembayaran'  => $query_apm_daftar['carabayar'], 
                        'jenisrujukan'  => $query_apm_daftar['jenisrujukan'], 
                        'tipesurat'  => ucfirst($query_apm_daftar['tipesurat']), 
                        'idasalrujukan'  => $asalrujukan, 
                    ]; 
                    
                }else{
                    $return = ['msg' => 'failed' ];
                }
                
                echo json_encode( $return );
            }else{
                echo json_encode( ['msg'=>'empty'] );
            }
        }
    }
    
    /**
     * Mahmud, clear
     * Menampilkan Data Pasien
     */
    public function tampil_pasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas
        {
            $mode = $this->input->post('mode');
            if($mode!='edit' && $mode != 'pindah') //jika mode add
            {
                $idperson = explode('-', $this->input->post('id')); //siapkan data  explode('-','norm-nojkn')
                $explodeNorm=$idperson[0];
                $dt_pendaftaran='';
                
                //---cek status periksa
                $periksa='';
                if(empty(!$idperson[0]) && $idperson[0] !='null')
                {                       
                    $periksa = $this->db->query("select pp.statustagihan, pp.idstatuskeluar,r.`status`, pp.tanggalrujukan, pp.waktuperiksa, pp.waktu as waktudaftar from person_pendaftaran pp, rs_pemeriksaan r, keu_tagihan k  where pp.norm='".$explodeNorm."' and pp.idstatuskeluar !=2 and pp.idstatuskeluar !=3 and pp.idstatuskeluar !=4 and r.idpendaftaran=pp.idpendaftaran and k.idpendaftaran=pp.idpendaftaran and r.status!='batal'");
                    if($periksa->num_rows() > 0)
                    {
                        $dtperiksa = $periksa->row_array();
                        echo json_encode(['cekstatus'=>'periksa','status'=>'danger','message'=>' Status Pemeriksaan Pasien [ '.statuspendaftaran($dtperiksa['idstatuskeluar']).' ], Status Tagihan ['.$dtperiksa['statustagihan'].']. Waktu Daftar ('.$dtperiksa['waktudaftar'].'), Periksa Tanggal ('.$dtperiksa['waktuperiksa'].'). <br> Mohon menghubungi bagian keuangan/kasir untuk menyelesaikan pembayaran terlebih dahulu.']);
                        return TRUE;
                    }
                }
            }
            else
            {   
                /**
                 * update 6/11/2022
                 * Developer By Ikhsan
                 * [+] pp.sudahcetakSEP
                 **/
                //---siapkan data id pendaftaran
                $id = $this->input->post('id');
                $this->mgenerikbap->setTable('person_pendaftaran');
                $dt_pendaftaran = $this->db->query("select pp.sudahcetakSEP,pp.kategoriskrining, pp.jenisrujukan,pp.idklinikperujuk, pp.caradaftar, pp.isantri, pp.tanggalrujukan,pp.idpendaftaran,pp.norm,pp.nosep,pp.norujukan,pp.idpasienpenanggungjawab,pp.idunit,rp.idjadwal, pp.idkelas,pp.nopesan, pp.carabayar, pp.jenispemeriksaan,pp.waktu, rp.waktu as waktuperiksa, pp.idjadwalgrup, pp.idstatuskeluar, pp.idjeniskunjungan from person_pendaftaran pp, rs_pemeriksaan rp where pp.idpendaftaran='".$id."' and rp.idpendaftaran = pp.idpendaftaran")->result_array();

                $explodeNorm = $dt_pendaftaran[0]['norm'];
                $tahunbulan  = date('Ym', strtotime($dt_pendaftaran[0]['waktu']));
            }
            
            $dt_bpjs            = $this->db->query("SELECT *,b.kelas, c.jenispeserta, d.statuspeserta  FROM bpjs_bpjs a, bpjs_hakkelas b, bpjs_jenispeserta c, bpjs_statuspeserta d WHERE norm='".$explodeNorm."' and b.idkelas=a.idkelas and c.idjenispeserta=a.idjenispeserta and d.idstatuspeserta=a.idstatuspeserta")->row_array();//,['norm'=>$explodeNorm]
            $this->mgenerikbap->setTable('person_pasien');
            $dt_pasien          = $this->mgenerikbap->select('*',['norm'=>$explodeNorm])->row_array();
            // $idp = empty($explodeNorm) ? 15 : $dt_pasien['idperson'] ;
            $idp = (empty($dt_pasien)) ? $idperson[2] : $dt_pasien['idperson'];
            $result             = $this->db->query("select *, date_format(p.tanggallahir,'%d/%m/%Y') as tgllahirview from person_person p where p.idperson='".$idp."'")->row_array();
            $dt_poliklinik      = $this->mkombin->cari_jadwalpoli(((empty($dt_pendaftaran[0]['waktuperiksa']))?date('Y-m-d'):date('Y-m-d', strtotime($dt_pendaftaran[0]['waktuperiksa']))));
            // admisi master person identitas
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $dt_pekerjaan       = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('demografi_pendidikan');
            $dt_pendidikan      = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('person_hubungan');
            $dt_hubungan        = $this->mgenerikbap->select('*')->result();
            $dt_agama           = $this->mkombin->get_data_enum('person_person','agama');
            $dt_identitas       = $this->mkombin->get_data_enum('person_person','identitas');
            $dt_jenisperiksa    = $this->mkombin->get_data_enum('person_pendaftaran','jenispemeriksaan');
            $dt_carabayar       = $this->mkombin->get_data_enum('person_pendaftaran','carabayar');
            $dt_jeniskelamin    = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            $dt_golongandarah   = $this->mkombin->get_data_enum('person_person','golongandarah');
            $dt_rhesus          = $this->mkombin->get_data_enum('person_person','rh');
            $dt_jenispasien     = [0=>['id'=>0,'txt'=>'Umum'], 1=>['id'=>1,'txt'=>'Karyawan/Keluarga Karyawan']];
            $dt_perkawinan      = $this->mkombin->get_data_enum('person_person','statusmenikah');
            $dt_skrining        = $this->mkombin->get_data_enum('person_pendaftaran','kategoriskrining');
            $dt_kelurahan       = $this->db->query("select gd.iddesakelurahan, gd.namadesakelurahan, gk.namakecamatan from geografi_desakelurahan gd left join geografi_kecamatan gk on gk.idkecamatan = gd.idkecamatan where gd.iddesakelurahan='".$result['iddesakelurahan']."'")->result();
            $dt_bahasa          = $this->db->query("select idbahasa as id, bahasa as text from person_bahasa pb where pb.idbahasa='".$result['idbahasa']."'")->result();
            $dt_suku            = $this->db->query("select idsuku as id, suku as text from person_suku ps where ps.idsuku='".$result['idsuku']."'")->result();
            $dt_kelaslayanan    = $this->mgenerikbap->select_multitable("*","rs_kelas")->result();
            $dt_jenisrujukan    = $this->mkombin->get_data_enum('person_pendaftaran','jenisrujukan');
            $dt_jeniskunjungan  = $this->db->select('kode as id, nama as text')->get('bpjs_bridging_jeniskunjungan')->result(); 
            $dt_klinikperujuk   = empty($dt_pendaftaran) ? '' : $this->db->select("idklinik as id, concat(ifnull(kodeklinik,''), ifnull(namaklinik,'')) as text")->get_where("bpjs_klinikperujuk",['idklinik'=>$dt_pendaftaran[0]['idklinikperujuk']])->result();

            $norm      = $explodeNorm;
            //tampilkan data dalam bentuk json
            echo json_encode([
                'nojkn'        =>$dt_pasien,
                'pasien'       =>$result,
                'jenisperiksa' =>$dt_jenisperiksa,
                'carabayar'    =>$dt_carabayar, 
                'agama'        =>$dt_agama,
                'identitas'    =>$dt_identitas,
                'jeniskelamin' =>$dt_jeniskelamin, 
                'golongandarah'=>$dt_golongandarah,
                'rhesus'       =>$dt_rhesus,
                'jenispasien'  =>$dt_jenispasien,
                'perkawinan'   =>$dt_perkawinan,
                'skrining'     =>$dt_skrining,
                'kelurahan'    =>$dt_kelurahan,
                'bahasa'       =>$dt_bahasa,
                'suku'         =>$dt_suku,
                'pendidikan'   =>$dt_pendidikan,
                'pekerjaan'    =>$dt_pekerjaan,
                'bpjs'         =>$dt_bpjs,
                'poliklinik'   =>$dt_poliklinik,
                'hubungan'     =>$dt_hubungan,
                'pendaftaran'  =>$dt_pendaftaran,
                'kelaslayanan' =>$dt_kelaslayanan,
                'jenisrujukan' =>$dt_jenisrujukan,
                'jeniskunjungan'=>$dt_jeniskunjungan,
                'klinikperujuk'=>$dt_klinikperujuk,
                'norm'         =>$norm,
                'grupjadwal'   =>$this->db->get('rs_jadwal_grup')->result()
            ]);
        }
    }
    
    //riwayat pasien prolanis bpjs
    public function bpjs_riwayatpasienprolanis()
    {
        $idunit = explode(',', $this->input->post('jadwal'));
        $tanggaldaftar = date('Y-m-d', strtotime(format_date_to_sql($this->input->post('tgldaftar'))));
        $selisihTanggalDaftar = dateDifference($tanggaldaftar,date('Y-m-d'),'%a');
        $riwayat = $this->db
                ->select('ru.namaunit, br.tanggal_kunjunganberikutnya, br.tanggalperiksa')
                ->join('rs_unit ru','ru.idunit=br.idunit')
                ->get_where('bpjs_riwayatpasienprolanis br',['norm'=>$this->input->post('norm'),'br.idunit'=>$idunit[0]]);
        if($riwayat->num_rows() > 0){
            $riwayat = $riwayat->row_array();
            $selisihsTanggalKunjungan = dateDifference($riwayat['tanggal_kunjunganberikutnya'],date('Y-m-d'),'%a');
            if($selisihsTanggalKunjungan <= $selisihTanggalDaftar){
                $data['status']  = 'success';
            }else{
                $data['status']  = 'warning';
                $data['message'] = 'Pasien dapat terlayani di Poli '.$riwayat['namaunit'].' <span class="label label-warning">mulai tanggal '.date('d/m/Y', strtotime($riwayat['tanggal_kunjunganberikutnya'])).'</span>, dikarenakan <b><i>pasien telah mendapatakan pelayanan Prolanis pada tanggal <span class="text text-red">'.date("d/m/Y", strtotime($riwayat['tanggalperiksa'])).'</span> di Poli '.$riwayat['namaunit'].'</i></b>. <br><br> Apabila Pasien Terdiagnosa Selain <b>Diagnosa PRB (Program Rujuk Balik) dari PPK1</b>, pasien bisa terlayani sesuai aturan BPJS yang berlaku. <br><br> <u>Centang list dibawah ini jika pasien non PRB:</u><br> <input type="checkbox" id="bpjs_pasiencholie"/> Pasien Non PRB (Program Rujuk Balik).';
            }
        }else{
            $data['status']  = 'success';
        }
        echo json_encode($data);
    }
    // ambil data penanggung
    public function pendaftaran_getdatapenanggung()
    {
        if(empty($this->input->post("b")) && empty(!$this->input->post("a")))
        {
        $sql=$this->db->query("SELECT p.idpasienpenanggungjawab,p.norm,pp.idperson, pp.namalengkap,  pp.jeniskelamin,dp.namapekerjaan , h.idhubungan, pp.idpekerjaan, h.namahubungan, pp.notelpon, pp.alamat FROM person_pasien_penanggungjawab p, person_person pp, person_hubungan h, demografi_pekerjaan dp where p.norm='".$this->input->post("a")."' and pp.idperson = p.idperson and h.idhubungan = p.idhubungan and dp.idpekerjaan = pp.idpekerjaan")->result();
        }
        else
        {
            $sql=$this->db->query("SELECT p.idpasienpenanggungjawab,p.norm,pp.idperson, pp.namalengkap,  pp.jeniskelamin,dp.namapekerjaan , h.idhubungan, pp.idpekerjaan, h.namahubungan, pp.notelpon, pp.alamat FROM person_pasien_penanggungjawab p, person_person pp, person_hubungan h, demografi_pekerjaan dp where p.norm='".$this->input->post("a")."' and pp.idperson = p.idperson and h.idhubungan = p.idhubungan and dp.idpekerjaan = pp.idpekerjaan and p.idperson='".$this->input->post("b")."'")->row_array();
        }
        echo json_encode($sql);
    }
    // ambil data enum person
    public function pendaftaran_getenumperson()
    {
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $pekerjaan= $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('person_hubungan');
            $hubungan= $this->mgenerikbap->select('*')->result();
            $kelamin= $this->mkombin->get_data_enum('person_person','jeniskelamin');
            echo json_encode(['pekerjaan'=>$pekerjaan, 'hubungan'=>$hubungan,'kelamin'=>$kelamin]);
    }

     // ambil semua data penanggung pasien 
    public function listpenanggungpasien(){echo json_encode( ((empty($this->input->post('rm'))) ? '' :  $this->db->query("SELECT ppp.idpasienpenanggungjawab, pp.idperson, ppp.norm,ph.idhubungan, pp.idpekerjaan, ppp.statuspenanggung, pp.namalengkap, ph.namahubungan FROM person_pasien_penanggungjawab ppp, person_hubungan ph, person_person pp where ph.idhubungan=ppp.idhubungan and pp.idperson=ppp.idperson and ppp.norm=".$this->input->post('rm'))->result_array() ) );}
    //save update status penanggung pasien
    public function saveupdatestatuspenanggung(){ echo json_encode($this->db->update('person_pasien_penanggungjawab',['statuspenanggung'=>$this->input->post('s')],['idpasienpenanggungjawab'=>$this->input->post('i')]));}
    //get enum penanggung
    public function pjgetenumperson(){ 
        $pekerjaan = $this->db->get('demografi_pekerjaan')->result();
        $jk= $this->mkombin->get_data_enum('person_person','jeniskelamin');
        $h =  $this->db->get('person_hubungan')->result();
        echo json_encode(['jeniskelamin'=>$jk,'hubungan'=>$h,'pekerjaan'=>$pekerjaan]);
        
    }
    
    public function cari_propinsi() // cari geografi propinsi
    {
        $this->mgenerikbap->setTable('geografi_propinsi');
        $result       = $this->mgenerikbap->select('*')->result();
        echo json_encode($result);
    }
    public function cari_kabupaten() // cari geografi kabupaten kota
    {
        $this->mgenerikbap->setTable('geografi_kabupatenkota');
        $result       = $this->mgenerikbap->select('*',['idpropinsi'=>$this->input->post('x')])->result();
        echo json_encode($result);
    }
    public function cari_kecamatan() // cari geografi kecamatan
    {
        $this->mgenerikbap->setTable('geografi_kecamatan');
        $result       = $this->mgenerikbap->select('*',['idkabupatenkota'=>$this->input->post('x')])->result();
        echo json_encode($result);
    }
    public function cari_kelurahan() // cari geografi kelurahan
    {
        $result = $this->mgenerikbap->select_multitable("dk.iddesakelurahan, dk.namadesakelurahan, kc.namakecamatan, kc.kodepos","geografi_desakelurahan, geografi_kecamatan")->result();
        echo json_encode($result);
    }
    public function cari_penanggung() // cari penanggung
    {
        $result = $this->mgenerikbap->select_multitable("pppj.idpasienpenanggungjawab, pper.namalengkap, ph.namahubungan ","person_pasien_penanggungjawab, person_person, person_hubungan",['pppj.norm'=>$this->input->post('norm')])->result();
        echo json_encode($result);
    }
    public function tampil_jadwalpoli()
    {
        $this->db->group_by('ru.idunit');
        $result = $this->mgenerikbap->select_multitable("ru.idunit, ru.namaunit","rs_jadwal, rs_unit")->result();
        echo json_encode($result);
    }
    
      /**
     * Update 16/11/2022
     * Developer Ikhsan
     * [+] caradaftar
     */
    public function save_pendaftaran_poli() // simpan data pendaftaran
    {   
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)) //lihat define di atas            
        {
             //Validasi Masukan
             $this->load->library('form_validation');
             $this->form_validation->set_rules('namapasien','','required');
             $this->form_validation->set_rules('jeniskelamin','','required');
             $this->form_validation->set_rules('tanggallahir','','required');
             $this->form_validation->set_rules('alamat','','required');
             //Jika Valid maka lanjutkan proses simpan
             if (validation_input())
             {
                $post = $this->input->post();
                // ---------persiapan data bpjs
                $tglrujukan     = format_date_to_sql($post['tglKunjungan']);
                $nojkn          = $this->input->post('nojkn');
                $kelas          = $this->input->post('idhakkelas');
                $statuspeserta  = $this->input->post('idstatuspeserta');
                $jenispeserta   = $this->input->post('idjenispeserta');
                $tanggalcetak   = format_date_to_sql($this->input->post('tanggalcetak'));
                $informasi      = $this->input->post('informasi');
                //----------persiapkan data person_pasien
                $jenispasien        = $this->input->post('jenispasien');
                $norm               = $this->input->post('norm');
                
                //----------Persiapkan data pendaftaran
                $idperson           = $this->input->post('idperson');
                $nik                = $this->input->post('noidentitas');
                $namalengkap        = qlreplace_quote($this->db->escape_str($this->input->post('namapasien')));
                $identitas          = $this->input->post('identitas');
                $tempatlahir        = $this->input->post('tempatlahir');
                $tanggallahir       = format_date_to_sql($this->input->post('tanggallahir'));
                $alamat             = qlreplace_quote($this->input->post('alamat'));
                $kelurahan          = $this->input->post('kelurahan');
                $agama              = $this->input->post('agama');
                $perkawinan         = $this->input->post('perkawinan');
                $jeniskelamin       = $this->input->post('jeniskelamin');
                $pendidikan         = $this->input->post('pendidikan');
                $pekerjaan          = $this->input->post('pekerjaan');
                $notelpon           = $this->input->post('notelepon');
                $golongandarah      = $this->input->post('golongandarah');
                $kategoriskrining   = $this->input->post('kategoriskrining');
                $rhesus             = $this->input->post('rhesus');
                $idsuku             = $this->input->post('idsuku');
                $idbahasa           = $this->input->post('idbahasa');
                $waktuperiksa       = (empty(format_date_to_sql($this->input->post('pilihtanggalantrian')))) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(format_date_to_sql($this->input->post('pilihtanggalantrian'))));
                $grupjadwal         = $this->input->post('grupjadwal');
             
                
                $waktumulai         = $this->input->post('waktumulai');
                $waktuselesai       = $this->input->post('waktusekarang');
                $waktulayanan       = $this->input->post('waktulayanan');
                
                //jika poliklinik tidak kosong simpan ke person_pendaftaran
                $jadwalpoli = $this->input->post('poliklinik');
                $poli_jadwal = ((!empty($grupjadwal))? explode(',', $jadwalpoli[0]) : explode(',', $jadwalpoli)); // set poli_jadwal
                $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
                //insert norm pasien
                $this->db->trans_start();
                if( !empty($idperson) )
                {
                    if($norm==null or $norm=='0' OR $norm=='null')
                    {   
                        $this->db->insert('person_pasien',['idperson'=>$idperson,'ispasienkeluargars'=>$jenispasien]);
                        $norm = $this->db->insert_id();
                    }
                }

                //simpan data ke tabel person_person
                $data = ['nik'=>$nik,'namalengkap'=>$namalengkap,'identitas'=>$identitas,'tempatlahir'=>$tempatlahir,'tanggallahir'=>$tanggallahir,'alamat'=>$alamat,'iddesakelurahan'=>$kelurahan,'agama'=>$agama,'statusmenikah'=>$perkawinan,'jeniskelamin'=>$jeniskelamin,'idpendidikan'=>$pendidikan,'idpekerjaan'=>$pekerjaan,'notelpon'=>$notelpon,'golongandarah'=>$golongandarah,'rh'=>$rhesus,'idsuku'=>$idsuku,'idbahasa'=>$idbahasa];
                $this->mgenerikbap->setTable('person_person');
                $save_person = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idperson);
                
                if(!$save_person){ return pesan_danger('Save Pendaftaran Person Failed..!'); } //jika save person gagal
                $idperson_aftersave = $idperson;
                
                if( empty($idperson) ) //jika idperson kosong atau baru mendaftar
                {
                    $isdaftar = 'pasienbaru'; //jika pasien baru mendaftar (untuk cek ketika akan mencetak kartu pasien)
                    $idperson_aftersave = $this->db->insert_id();
                    $this->db->insert('person_pasien',['idperson'=>$idperson_aftersave,'ispasienkeluargars'=>$jenispasien, 'nojkn'=>$nojkn,'insert_by'=>$iduser] );
                    $norm = $this->db->insert_id(); 
                    $this->resetuserapm($norm,$tanggallahir);
                    $this->pendaftaran_savebpjs($norm,$jenispeserta,$kelas,$informasi,$tanggalcetak,$statuspeserta);//save bpjs   
                }
                else
                {
                    $isdaftar='pasienlama'; //jika pasien lama (untuk cek ketika akan mencetak kartu pasien)
                    $this->db->update('person_pasien',['nojkn'=>$nojkn,'ispasienkeluargars'=>$jenispasien],['norm'=>$norm]);
                    $this->pendaftaran_savebpjs($norm,$jenispeserta,$kelas,$informasi,$tanggalcetak,$statuspeserta);//save bpjs   
                    $this->pendaftaran_updatebpjs($norm,$jenispeserta,$kelas,$informasi,$tanggalcetak,$statuspeserta);//save bpjs
                }
                $this->db->trans_complete();
                               
                //jika jadwal dipilih
                if(empty(!$jadwalpoli))
                {    
                    $idpoliklinik = $poli_jadwal[0];
                    $idjadwal = $poli_jadwal[1]; //siapkan data idjadwal    
                    
                    //cek apakah pasien di blacklist
                    $blacklist = $this->db->query("select *, (select namaunit from rs_unit ru where ru.idunit = ppb.idunit) as namaunit from person_pasien_blacklistunit ppb where idunit='".$idpoliklinik."' and norm='".$norm."'");
                    if( $blacklist->num_rows() > 0 )
                    {
                        $dt = $blacklist->row_array();
                        echo json_encode(['status'=>'blacklist','message'=>' Pasien Tidak Dapat Didaftarkan, Pasien <span class="label label-hitam">Tercatat Blacklist</span> Oleh Sistem di Poli '.$dt['namaunit'] ]);
                        return TRUE;
                    }
                }    
                
                //jika daftar || jadwal ada
                if(!empty($idpoliklinik) || !empty($this->input->post('isantri')))
                {
                    //siapkan data id pendaftaran
                    $idpendaftaran = $this->input->post('idpendaftaran');
                    
                    //jika mode pindah jadwal pemeriksaan pasien
                    if($this->input->post('modehalaman') == 'pindah')
                    {
                        //batalkan pendaftaran periksa yang lama
                        $this->db->update('person_pendaftaran',['idstatuskeluar'=>3],['idpendaftaran'=>$idpendaftaran]);
                        $this->db->update('rs_pemeriksaan',['status'=>'batal'],['idpendaftaran'=>$idpendaftaran]);
                        $idpendaftaran = '';
                    }
                    
                    # Update [11/6/22]
                    # Developer by ikhsan
                    $cek_caradaftar ='';
                    if( !empty( $idpendaftaran ) ){
                        $sql_caradaftar       = "SELECT caradaftar FROM `person_pendaftaran` WHERE idpendaftaran='".$idpendaftaran."'"; 
                        $query_cek_caradaftar = $this->db->query($sql_caradaftar)->row();
                        $cek_caradaftar = $query_cek_caradaftar->caradaftar;
                    }else{
                        $cek_caradaftar = 'reguler';
                    }
                    
                    if( $cek_caradaftar != 'anjunganoline' ) $cek_caradaftar = 'reguler';

                    /**
                     * Cek input POST cara daftar
                     * Cek input post pendaftaran APM
                     */
                    if($this->input->post('caradaftar') ){
                        $cek_caradaftar = 'online';
                    }elseif($this->input->post('pendaftaranAPM')){
                        $cek_caradaftar = 'anjunganonline';
                    }else{
                        $cek_caradaftar = 'reguler';
                    }
                    
                    # end update

                    if($this->input->post('modehalaman')=='edit')
                    {
                        if(empty($this->input->post('isantri')))
                        {
                            $simpan = (empty($grupjadwal)? $this->rs_pemeriksaan_update_where(['idjadwal'=>$idjadwal],['idpendaftaran'=>$idpendaftaran]):'');
                        }
                                                
                        //jika tanggal antrian  tidak disabled ditambahkan
                        if($this->input->post('pilihtanggalantrian') && !empty($this->input->post('pilihtanggalantrian')))
                        {
                            $data = [
                                'norm'             => $norm,
                                'nosep'            => $this->input->post('nosep'),
                                'idunit'           => $idpoliklinik,
                                'waktuperiksa'     => ql_formatdatetime($waktuperiksa),
                                'norujukan'        => $this->input->post('norujukan'),
                                'tanggalrujukan'   => (($tglrujukan=='0000-00-00')?'':$tglrujukan),
                                'idkelas'          => $this->input->post('kelaslayanan'),
                                'jenispemeriksaan' => $this->input->post('jenisperiksa'),
                                'carabayar'        => $this->input->post('carabayar'),
                                'idjadwalgrup'     => qlset_null($grupjadwal),
                                'caradaftar'       => $cek_caradaftar,
                                'idklinikperujuk'  => $this->input->post('asalrujukan'),
                                'jenisrujukan'     => $this->input->post('jenisrujukan'),
                                'idjeniskunjungan' => $this->input->post('jeniskunjungan'),
                                'kategoriskrining' => $kategoriskrining,
                                'sudahcetakSEP'    => $this->input->post('chckboxsudahprintSEP')   
                            ];
                            $jenistagihan =  (($post['carabayar']=='jknpbi' or $post['carabayar']=='jknnonpbi' ) ? $post['carabayar'] : 'tagihan' ); 
                            $this->db->update('keu_tagihan',['waktutagih'=>ql_formatdatetime($waktuperiksa),'jenistagihan'=>$jenistagihan],['idpendaftaran'=>$idpendaftaran]);
                            $this->db->update('rs_pemeriksaan',['waktu'=>ql_formatdatetime($waktuperiksa),'idjadwal'=>$idjadwal],['idpendaftaran'=>$idpendaftaran]);
                        }
                        else
                        {
                            $data = [
                            'norm'             => $norm,
                            'nosep'            => $this->input->post('nosep'),                            
                            'norujukan'        => $this->input->post('norujukan'),
                            'tanggalrujukan'   => (($tglrujukan=='0000-00-00')?'':$tglrujukan),
                            'idkelas'          => $this->input->post('kelaslayanan'),
                            'jenispemeriksaan' => $this->input->post('jenisperiksa'),
                            'carabayar'        => $post['carabayar'],
                            'idjadwalgrup'     => qlset_null($grupjadwal),
                            'caradaftar'       => $cek_caradaftar,
                            'idklinikperujuk'  => $this->input->post('asalrujukan'),
                            'jenisrujukan'     => $this->input->post('jenisrujukan'),
                            'idjeniskunjungan' => $this->input->post('jeniskunjungan'),
                            'kategoriskrining' => $kategoriskrining,
                            'sudahcetakSEP'    => $this->input->post('chckboxsudahprintSEP')  
                            ];
                            
                        }
                        //Ubah Jaminan Asuransi di Hasil Pemeriksaan dan Barang Pemeriksaan
                        $this->ubahJaminanAsuransiByPendaftaran($idpendaftaran,$post['carabayar']); 
                    }
                    else
                    {
                        $data = [
                            'norm'             => $norm,
                            'nosep'            => $this->input->post('nosep'),
                            'norujukan'        => $this->input->post('norujukan'),
                            'tanggalrujukan'   => (($tglrujukan=='0000-00-00')?'':$tglrujukan),
                            'idkelas'          => $this->input->post('kelaslayanan'),
                            'idunit'           => $idpoliklinik,
                            'waktu'            => date('Y-m-d H:i:s'),
                            'waktuperiksa'     => ql_formatdatetime($waktuperiksa),
                            'jenispemeriksaan' => $this->input->post('jenisperiksa'),
                            'carabayar'        => $this->input->post('carabayar'),
                            'idjadwalgrup'     => qlset_null($grupjadwal),
                            'caradaftar'       => $cek_caradaftar,
                            'idklinikperujuk'  => $this->input->post('asalrujukan'),
                            'jenisrujukan'     => $this->input->post('jenisrujukan'),
                            'idjeniskunjungan' => $this->input->post('jeniskunjungan'),
                            'kategoriskrining' => $kategoriskrining,
                            'sudahcetakSEP'    => $this->input->post('chckboxsudahprintSEP')  
                        ];
                    }
                    
                    $save_daftar = $this->ql->person_pendaftaran_insert_or_update($idpendaftaran,$data);
                    //simpan ke table helper_autonumber
                    if(empty($idpendaftaran)) //jika belum mendaftar
                    {   
                        $idpendaftaran = $this->db->insert_id();
                        $kegiatan = 'pendaftaran';
                        $callid = generaterandom(8);
                        $this->ql->antrianset_nopesan($waktuperiksa,$callid,$idpoliklinik); //buat no pesan
                        $this->mgenerikbap->setTable('helper_autonumber');
                        $nopesan = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array(); //siapkan data no antrian

                        // ======jika grup jadwal ada maka simpan pemeriksan sesuai banyaknya jadwal dipilih
                        if (empty($grupjadwal)) :
                            $save = $this->ql->pendaftaranpoli_savedaftarpemeriksaan($idpendaftaran,$idjadwal,$nopesan['number'],$norm,$callid,$this->input->post('pilihtanggalantrian'));
                        else :
                            $save = $this->pendaftaranpoli_savedaftarpemeriksaan_grupjadwal($idpendaftaran,$jadwalpoli,$nopesan['number'],$norm,$callid,qlset_null($grupjadwal));
                        endif;
                        // =====END====
                        $nosep = $this->db->query("select nosep from person_pendaftaran where idpendaftaran='$idpendaftaran'")->row_array()['nosep'];
                    }
                    else
                    {
                         $nopesan = $this->input->post('hideNopesan');
                         $nosep   = $this->input->post('nosep');
                         $norm    = $norm;
                         $kegiatan = 'editpendaftaran';
                    }
                    //input diskon otomatis
                    $this->mkombin->insertdelete_diskonpemeriksaan($idpendaftaran);
                    //input waktu layanan
                    $kegiatan = $kegiatan;
                    $this->insert_waktulayanan($norm,$idpendaftaran,$kegiatan,ql_formatdatetime($waktuperiksa),'pendaftaran',$waktumulai,$waktuselesai);
                    $idpemeriksaan = ((empty($this->input->post('idpendaftaran')))?$this->db->query("select idpemeriksaan from rs_pemeriksaan where idpendaftaran='".$idpendaftaran."'")->row_array()['idpemeriksaan']:0);
                    
                    $pesan = ($save_daftar) ? ['status'=>'success', 'message'=> (empty($idpendaftaran)) ? 'Pendaftaran Success.!' :'Update Pendaftaran Success.!'] : ['status'=>'danger', 'message'=>(empty($idpendaftaran)) ? 'Pendaftaran Failed.!' :'Update Pendaftaran Failed.!'] ;
                    //jika simpan pendaftaran status success selain itu status error
                    
                    
                    $json['pesan'] = $pesan;
                    $json['nopesan'] = $nopesan;
                    $json['nosep'] = $nosep;
                    $json['norm'] = $norm;
                    $json['idpemeriksaan'] = $idpemeriksaan;
                    if(empty($this->input->post('isantri')))
                    {
                        $json['idpoliklinik'] = $idpoliklinik;
                    }
                    else
                    {
                        $json['idpoliklinik'] = 1;
                    }
                    
                    echo json_encode($json);
                }
                else
                {
                    $dt_poliklinik = $this->mkombin->cari_jadwalpoli(date('Y-m-d'));
                    $pesan = ($save_person) ? ['status'=> 'success', 'message'=> empty($idperson) ? 'Entri Success.!' : 'Update Success.!'] 
                                       : ['status'=> 'danger', 'message'=>empty($idperson) ? 'Entri Failed.!' : 'Update Failed.!'];
                    $dt_jenisrujukan    = $this->mkombin->get_data_enum('person_pendaftaran','jenisrujukan');
                    $dt_jeniskunjungan  = $this->db->select('kode as id, nama as text')->get('bpjs_bridging_jeniskunjungan')->result(); 
                    echo json_encode([
                        
                        'pesan'     =>$pesan,
                        'norm'      =>$norm,
                        'poliklinik'=>$dt_poliklinik,
                        'person'    =>$idperson_aftersave,
                        'isdaftar'  =>$isdaftar,
                        'dt_jenisrujukan'   => $dt_jenisrujukan,
                        'dt_jeniskunjungan' => $dt_jeniskunjungan,
                        'grupjadwal'=>$this->db->get('rs_jadwal_grup')->result(),
                    ]);
                }
            }
        }   
    }
    /**
     * Ubah Jaminan Asuransi di Hasil Pemeriksaan dan Barang Pemeriksaan
     * @param type $idp Id Pendaftaran
     * @param type $carabayar Cara Bayar
     */
    //mahmud, clear
    public function ubahJaminanAsuransiByPendaftaran($idp,$carabayar)
    {
        $carabayar_old = $this->db->select('carabayar')->get('person_pendaftaran',['idpendaftaran'=>$idp])->row_array()['carabayar'];
        if($carabayar_old != $carabayar) // jika carabayar diubah
        {
            $jaminan = (($carabayar=='mandiri') ? 0 : 1 );
            $arrMsg = $this->db->update('rs_hasilpemeriksaan',['jaminanasuransi'=>$jaminan],['idpendaftaran'=>$idp]);
            $arrMsg = $this->db->update('rs_barangpemeriksaan',['jaminanasuransi'=>$jaminan],['idpendaftaran'=>$idp]);
        } 
    }
    
    public function refStatus()
    {
        $this->load->library('rsonline');
        return $this->rsonline->requestStatusRawat();
    }
    
    private function cekpendaftaranpasien()
    {
        //jika pasien dalam kondisi mendaftar
        $sql = $this->db->query("select rp.idpemeriksaan from rs_pemeriksaan rp join  person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran where pp.waktu='".date('Y-m-d')."' and rp.norm='".$this->input->post('norm')."' and rp.`status`!='selesai'")->row_array();
        if(!empty($sql))
        {   
             $pesan = ['status'=>'danger', 'message'=>'Pasien Sudah Mendaftar.!'];
             echo json_encode(['pesan'=>$pesan]);
             return false;
        }
        else
        {
            return true;
        }
    }
    
    public function pendaftaran_savekecamatan()
    {
        $data = ['namakecamatan'=>$this->input->post('kec'),'idkabupatenkota'=>$this->input->post('kab')];
        $save = $this->db->insert('geografi_kecamatan',$data);
        $id = $this->db->insert_id();
        echo json_encode(['status'=> (($save) ? 'success' : 'danger' ) ,'message'=> (($save) ? 'Berhasil.' : 'Gagal.' ) ,'id'=>$id]);

    }
    
    private function pendaftaranpoli_saveantrian($idunit,$callid)//simpan antrian
    {
        $this->mgenerikbap->setTable('rs_unit');
        $akronimunit = $this->mgenerikbap->select("idloket",['idunit'=>$idunit])->row_array();
        $this->mgenerikbap->setTable('helper_autonumber');
        $data = [
            'id'      =>date('Ymd').$akronimunit['idloket'],
            'number'  =>null,
            'callid'  =>$callid,
            'expired' =>date('Y-m-d')
        ];
        $simpan = $this->mgenerikbap->insert($data);
    }
    
    function paketicdperiksa($idpendaftaran,$icd)
    {
        return $this->db->query("select icd from rs_hasilpemeriksaan x where x.idpendaftaran='".$idpendaftaran."' and x.icd='".$icd."'");
    }
    public function save_geografikelurahan() //simpan geografi kelurahan
    {
        $this->mgenerikbap->setTable('geografi_desakelurahan');
        $data = [ 'idkecamatan' => $this->input->post('kecamatan'),'namadesakelurahan' => $this->input->post('kelurahan') ];
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data,'');
        pesan_success_danger($arrMsg, 'Save Kelurahan Success.!', 'Save Kelurahan Failed.!', 'js'); 
    }
    public function save_penanggungjawab() //simpan data penanggung jawab
    {
        $post = $this->input->post();
        $ipenanggung = $post['idpersonPenanggung'];
        $data = ['namalengkap'=> $post['namapenanggung'],'jeniskelamin'=> $post['jeniskelamin'],'idpekerjaan'=> $post['pekerjaan'],'notelpon'=> $post['notelpon'],'alamat'=> $post['alamatpenanggung']];
        if(empty($ipenanggung)){$arrMsg=$this->db->insert("person_person",$data);}
        else{$arrMsg=$this->db->update("person_person",$data,['idperson'=>$ipenanggung]);}
        if(!$arrMsg){ return pesan_success_danger($arrMsg, 'Save Penanggung Jawab Person Success.!', 'Save Penanggung Jawab Person Failed.!', 'js');}
        $data_hubungan = ['norm'=>$this->input->post('normPenanggung'),'idperson'    =>((empty($ipenanggung))? $this->db->insert_id() : $ipenanggung ),'idhubungan'  =>$this->input->post('hubungan')];
        if(empty($ipenanggung)){$arrMsg=$this->db->insert("person_pasien_penanggungjawab",$data_hubungan);}
        else{$arrMsg=$this->db->update("person_pasien_penanggungjawab",$data_hubungan,['idperson'=>$ipenanggung]);}
        pesan_success_danger($arrMsg, ((empty($ipenanggung))?'Save':'Update').' Penanggung Jawab Success.!', ((empty($ipenanggung))?'Save':'Update').' Penanggung Jawab Failed.!', 'js');
    }
    public function single_delete() // DELETE DATA
    {
        if ($this->pageaccessrightbap->checkAccessRight($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('hal'))))
        {
            $table = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('t'));
            $id = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('i'));
            $this->mgenerikbap->setTable($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('t')));
            if($table=='person_pendaftaran')
            {
                $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id,['idstatuskeluar'=>'3']);
                $arrMsg = $this->rs_pemeriksaan_update_where(['status'=>'batal'],['idpendaftaran'=>$id]);//panggil dari my_controller
                $datan  = $this->db->query("select norm from rs_pemeriksaan where idpendaftaran='$id'")->row_array();
                $datal  = [
                    'url'     => get_url(),
                    'log'     => 'user:'.$this->session->userdata('username').';status:batal;idpendaftaran:'.$id.';norm:'.$datan['norm'],
                    'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];
                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            }
            elseif($table=='rs_masterjadwal')
            {
                $arrMsg = $this->db->update($table,['ishide'=>'1'],['idmasterjadwal'=>$id]) ;
            }
            elseif($table=='rs_jadwal')
            {
                $arrMsg = $this->db->update($table,['status'=>'batal'],['idjadwal'=>$id]) ;
            }
            else
            {
                $arrMsg = $this->mgenerikbap->delete($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('i')));
            }
            pesan_success_danger($arrMsg, 'Delete Success.!', 'Delete Failed.!', 'js'); //jika hapus berhasil status success selain itu status error
        }
    }
    ///////////////////////ADMISSION PEGAWAI///////////////////
    //-------------------------- vv Standar
    public function setting_pegawai()
    {
        return [    'content_view'      => 'admission/v_pegawai',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'pegawai',
                    'active_menu_level' => '',
               ];
    }
    //list Pegawai
    public function pegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        { 
           $data                   = $this->setting_pegawai(); //letakkan di baris pertama
           $data['title_page']     = 'Pegawai';
           $data['mode']           = 'view';
           $data['jsmode']         = 'view';           
           $data['plugins']        = [PLUG_DATATABLE];
           $data['script_js']      = ['js_admission-pegawai'];
           $data['table_title']    = 'List Pegawai ';
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    // list data pegawai data table serverside
    public function dt_pegawai()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_pegawai_(true);
        $data=[]; 
        $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj)
            {
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $id           = $this->encryptbap->encrypt_urlsafe($obj->idpegawai, "json");
                $tabel        = $this->encryptbap->encrypt_urlsafe('person_pegawai', "json");
                $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_PEGAWAI, "json");
                
                (($obj->statuskeaktifan=='aktif') ? $label_status='label-success' : $label_status='label-danger');
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->nip;
                    $row[] = $obj->sip;
                    $row[] = $obj->bpjs_kodedokter;
                    $row[] = $obj->namalengkap;
                    $row[] = $obj->profesi;
                    $row[] = $obj->namagruppegawai;
                    $row[] = '<label class="label '.$label_status.'">'.$obj->statuskeaktifan.'</label>';
                    $row[] = '<a class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Ubah"  href="'.base_url('cadmission/edit_pegawai/'.$id).'"><i class="fa fa-pencil"></i></a>'
                           . ' <a id="delete_data" nobaris="'.$no.'" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Hapus"  href="#"><i class="fa fa-trash"></i></a>';
                    $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_pegawai(),
            "recordsFiltered" => $this->mdatatable->filter_dt_pegawai(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }


    //tambah data pegawai
    public function add_pegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $data                       = $this->setting_pegawai(); //letakkan di baris pertama
            $this->load->helper('form'); //load form helper
            $data['title_page']         = 'Add Pegawai'; //judul halaman
            $data['mode']               = 'add';
            $data['jsmode']         = 'view';
            $data['dt_statuskeaktifan'] = $this->mkombin->get_data_enum('person_pegawai','statuskeaktifan');
            $data['dt_gruppegawai']     = $this->db->get('person_grup_pegawai')->result();
            $data['dt_profesi']         = $this->db->get('person_grup_profesi')->result();
            $data['peran']              = $this->db->get('login_peran')->result();
            $data['unit']               = $this->db->get('rs_unit_farmasi')->result();
            $data['data_edit']          = '';
            $data['plugins']            = [ PLUG_DATATABLE, PLUG_DROPDOWN , PLUG_DATE, PLUG_CHECKBOX ];
            $data['script_js']          = ['js_admission-pegawai'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function edit_pegawai() //edit data pegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $data                       = $this->setting_pegawai(); //letakkan di baris pertama
            $this->load->helper('form'); //load form helper
            $data['title_page']         = 'Edit Pegawai'; //judul halaman
            $data['mode']               = 'edit';
            $data['jsmode']             = 'edit';
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['dt_statuskeaktifan'] = $this->mkombin->get_data_enum('person_pegawai','statuskeaktifan');
            $data['dt_gruppegawai']     = $this->db->get('person_grup_pegawai')->result();
            $data['dt_profesi']         = $this->db->get('person_grup_profesi')->result();
            $data['peran']              = $this->db->get('login_peran')->result();
            $data['unit']               = $this->db->select('idunit,namaunit')->get('rs_unit_farmasi')->result();
            $data['data_edit']          = $this->db->query("select DISTINCT pper.*, lu.iduser, lu.namauser, lu.password, lhu.idhakaksesuser, lhu.peranperan, lhu.unituser,ppeg.sip,ppeg.idprofesi, ppeg.idpegawai, ppeg.titeldepan, ppeg.titelbelakang, ppeg.idgruppegawai, ppeg.statuskeaktifan, ppeg.nip, ppeg.bpjs_kodedokter from person_pegawai ppeg join person_person pper on pper.idperson = ppeg.idperson left join login_user lu on lu.idperson=ppeg.idperson left join login_hakaksesuser lhu on lhu.iduser=lu.iduser where pper.idperson=ppeg.idperson and ppeg.idpegawai=".$id)->row_array();
            $data['plugins']            = [ PLUG_DROPDOWN , PLUG_DATE ,PLUG_CHECKBOX];
            $data['script_js']          = ['js_admission-pegawai'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function getdataedit_personpegawai(){echo json_encode($this->db->get_where('person_person',['idperson'=>$this->input->post('id')])->row_array());}
    public function uniqusername(){ $query=$this->db->get_where('login_user',['namauser'=>$this->input->post('user')]); echo json_encode((($query->num_rows()>0)?'Username sudah digunakan, silahkan ganti dengan yang lain.!':''));}
    public function save_pegawai() //simpan pegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $post=$this->input->post();
            
            //insert update login_user
            $datalu = ['namauser'=>$post['username'],'idperson'=>$post['listPerson']];//data login user
            if(empty(!$post['password']) && empty($post['iduser']) ){$datalu['password']=passwordhash($post['password']);}
            $this->mgenerikbap->setTable('login_user');
            $savelu = $this->mgenerikbap->update_or_insert_ignoreduplicate($datalu,$post['iduser']);
//            insert update login hakaksesuser
            $iduser = ((empty($post['iduser']))?$this->db->insert_id():$post['iduser']);
            
            $datalhu = ['iduser'=>$iduser,'peranperan'=>implode(',', $post['peranperan']),'unituser'=>implode(',', $post['unituser'])];
            $this->mgenerikbap->setTable('login_hakaksesuser');
            $savelhu = $this->mgenerikbap->update_or_insert_ignoreduplicate($datalhu,$post['idhakaksesuser']);
            
            //save person pegawai
            $data = [
                'titeldepan'=>$post['titeldepan'],
                'idperson'=>$post['listPerson'],
                'titelbelakang'=>$post['titelbelakang'],
                'idgruppegawai'=>$post['gruppegawai'],
                'statuskeaktifan'=>$post['statuskeaktifan'],
                'nip'=>$post['nip'],
                'sip'=>$post['sip'],
                'bpjs_kodedokter'=>$post['bpjs_kodedokter'],
                'idprofesi'=>$post['profesi']
            ];
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['idpegawai']));
            $this->mgenerikbap->setTable('person_pegawai');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data,$id);
            pesan_success_danger($arrMsg, (empty($id)) ? 'Save Pegawai Success.!' : 'Update Pegawai Success.!', (empty($id)) ? 'Save Pegawai Failed.!' : 'Update Pegawai Failed.!'); 
        }
        redirect(base_url('cadmission/pegawai'));
        
    }
    public function cari_gruppegawai() //cari data grup pegawai
    {
        $this->mgenerikbap->setTable('person_grup_pegawai');
        echo json_encode($this->mgenerikbap->select('*')->result());
    }
    public function cari_person()
    {      
        $this->mgenerikbap->setTable('person_person');
        $cari = $this->input->get('term');
        $this->db->like("namalengkap","$cari");
        $this->db->or_like("nik","$cari");
        $data = $this->mgenerikbap->select('idperson, namalengkap, nik')->result();
        echo json_encode($data);
    }
    public function tampil_enum_person()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $dt_agama           = $this->mkombin->get_data_enum('person_person','agama');
            $dt_jeniskelamin    = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            $dt_golongandarah   = $this->mkombin->get_data_enum('person_person','golongandarah');
            $dt_rhesus          = $this->mkombin->get_data_enum('person_person','rh');
            $dt_perkawinan      = $this->mkombin->get_data_enum('person_person','statusmenikah');
            $dt_kelurahan       = $this->mgenerikbap->select_multitable("dk.iddesakelurahan, dk.namadesakelurahan, kc.namakecamatan, kc.kodepos","geografi_desakelurahan, geografi_kecamatan")->result();
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $dt_pekerjaan       = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('demografi_pendidikan');
            $dt_pendidikan      = $this->mgenerikbap->select('*')->result();
            //tampilkan data dalam bentuk json
            echo json_encode([
                'agama'        =>$dt_agama,
                'jeniskelamin' =>$dt_jeniskelamin, 
                'golongandarah'=>$dt_golongandarah,
                'rhesus'       =>$dt_rhesus,
                'perkawinan'   =>$dt_perkawinan,
                'kelurahan'    =>$dt_kelurahan,
                'pendidikan'   =>$dt_pendidikan,
                'pekerjaan'    =>$dt_pekerjaan
            ]);
        }
    }
    public function tampil_enum_pegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {

            echo json_encode($this->mkombin->get_data_enum('person_pegawai','statuskeaktifan'));
        }
    }
    public function save_gruppegawai() //simpan gruppegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('person_grup_pegawai');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['namagruppegawai'=> $this->input->post('nama')],'');
            pesan_success_danger($arrMsg, 'Save Grup Pegawai Success.!', 'Save Grup Pegawai Failed.!', 'js'); 
        }  
    }
    public function save_person() //simpan data person pegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)) //lihat define di atas
        {
            $post = $this->input->post();
            $this->mgenerikbap->setTable('person_person');
            $data = [
                'nik'            => $post['noidentitas'],
                'namalengkap'    => $post['namalengkap'],
                'tempatlahir'    => $post['tempatlahir'],
                'tanggallahir'   => $post['tanggallahir'],
                'alamat'         => $post['alamat'],
                'iddesakelurahan'=> $post['kelurahan'],
                'agama'          => $post['agama'],
                'statusmenikah'  => $post['status'],
                'jeniskelamin'   => $post['jeniskelamin'],
                'idpendidikan'   => $post['pendidikan'],
                'idpekerjaan'    => $post['pekerjaan'],
                'notelpon'       => $post['notelpon'],
                'golongandarah'  => $post['golongandarah'],
                'rh'             => $post['rhesus']
            ];
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data,$post['idperson']);
            $idp=(($post['idperson']=='')?$this->db->insert_id():$post['idperson']);
                echo json_encode( ($arrMsg) ? ['status'=>'success','message'=>'Save Person Success.!','id'=>$idp,'nama'=>$this->input->post('namalengkap')] : ['status'=>'danger','message'=>'Save Person Failed.!','id'=>'','nama'=>$this->input->post('')]);
        }
    }
    ///////////////////////ADMISSION MASTERJADWAL///////////////////
    //-------------------------- vv Standar
    public function setting_masterjadwal()
    {
        return [    'content_view'      => 'admission/v_masterjadwal',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'masterjadwal',
                    'active_menu_level' => '',
                    'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TIME ],
                    'script_js'         => ['js_admission-masterjadwal']
               ];
    }
    //list master jadwal
    public function masterjadwal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        { 
           $data                   = $this->setting_masterjadwal(); //letakkan di baris pertama
//           $data['data_list']      = $this->db->query("SELECT * FROM `vrs_masterjadwal`")->result();
           $this->mgenerikbap->setTable('rs_unit');
           $data['title_page']     = 'Master Jadwal';
           $data['mode']           = 'view';
           $data['table_title']    = 'Master Jadwal';
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    // list data master jadwal data table serverside
    public function dt_masterjadwal()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_masterjadwal_(true);
        $data=[]; 
        $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj)
            {
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('rs_masterjadwal'));
                $idhalaman = $this->encryptbap->encrypt_urlsafe(V_MASTERJADWAL, "json");
                $row = [];
                $row[] = $obj->namaunit;
                $row[] = $obj->namalengkap;
                $row[] = ql_masterjadwal_editdelete($obj->ahad);
                $row[] = ql_masterjadwal_editdelete($obj->senin);
                $row[] = ql_masterjadwal_editdelete($obj->selasa);
                $row[] = ql_masterjadwal_editdelete($obj->rabu);
                $row[] = ql_masterjadwal_editdelete($obj->kamis);
                $row[] = ql_masterjadwal_editdelete($obj->jumat);
                $row[] = ql_masterjadwal_editdelete($obj->sabtu);
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_masterjadwal(),
            "recordsFiltered" => $this->mdatatable->filter_dt_masterjadwal(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function masterjadwal_save()
    {     
       if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $id = $this->input->post('i');
            $this->mgenerikbap->setTable('rs_masterjadwal');
            $data = [
                'idunit'          => $this->input->post('poli'),
                'idpegawaidokter' => $this->input->post('dokter'),
                'idloket'         => $this->input->post('loket'),
                'idjadwalgrup'    => ($this->input->post('jadwalgrup')==0)?null:$this->input->post('jadwalgrup'),
                'hari'            => $this->input->post('hari'),
                'jammulai'        => waktuindo($this->input->post('jammulai')),
                'jamakhir'        => waktuindo($this->input->post('jamakhir')),
                'quota'           => $this->input->post('quota'),
                'quota_jkn'       => $this->input->post('quotajkn'),
                'quota_nonjkn'    => $this->input->post('quotanonjkn'),
            ];
            $arrMsg = ($this->input->post('mode')=='salin') ? $this->db->insert('rs_masterjadwal',$data) : $this->mgenerikbap->update_or_insert_ignoreduplicate($data,$id);
            pesan_success_danger($arrMsg, (empty($id)) ? 'Save Jadwal Success.!' : 'Update Jadwal Success.!' , (empty($id)) ? 'Save Jadwal Failed.!' : 'Update Jadwal Failed.!', 'js'); 
        }
    }
    public function masterjadwal_caripoli()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('rs_unit');
            echo json_encode($this->mgenerikbap->select('idunit, namaunit')->result());
        }
    }
    public function masterjadwal_cariloket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $post = $this->input->post();
            $data = array();
            if(isset($post['idunit']))
            {
                $data = $this->db->select('idloket, namaloket')->get_where('antrian_loket',['idunit'=>$post['idunit'] ])->result();
            }
            else
            {
                $data = $this->db->select('idloket, namaloket')->get('antrian_loket')->result();
            }
            
            echo json_encode($data);
        }
    }
    public function masterjadwal_carijadwalgrup()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('rs_jadwal_grup');
            echo json_encode($this->mgenerikbap->select('idjadwalgrup, namagrup')->result());
        }
    }
    public function masterjadwal_caridokter()
    {
            $result = $this->mgenerikbap->select_multitable("pgp.namagruppegawai,ppg.idpegawai, concat(ifnull(ppg.titeldepan,''),' ',ifnull(pper.namalengkap,''),' ',ifnull(ppg.titelbelakang,'')) namalengkap","person_pegawai, person_person, person_grup_pegawai",['isdokter'=>'1','statuskeaktifan'=>'aktif'])->result();
            echo json_encode($result);
    }
    public function dropdown_listdokter()
    {
            $result = $this->mgenerikbap->select_multitable("ppg.idpegawai as id, concat(ifnull(ppg.titeldepan,''),' ',ifnull(pper.namalengkap,''),' ',ifnull(ppg.titelbelakang,'')) as txt ","person_pegawai, person_person, person_grup_pegawai",['isdokter'=>'1','ppg.statuskeaktifan'=>'aktif'])->result();
            echo json_encode($result);
    }
    public function masterjadwal_tampilhari()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            echo json_encode(hariindo());
        }
    }
    public function masterjadwal_edit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('rs_masterjadwal');
            echo json_encode($this->mgenerikbap->select("*",['idmasterjadwal'=>$this->input->post("x")])->row_array());
        }
    }
    public function masterdata_deletejadwal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)) //lihat define di atas
        {
            $arrMsg = $this->db->delete('rs_masterjadwal',['idmasterjadwal'=>$this->input->post('x')]) ;
            pesan_success_danger($arrMsg, 'Delete Jadwal Success.!', 'Delete Jadwal Failed.!', 'js');
        }
    }
    ///////////////////////ADMISSION INPUTJADWAL///////////////////
    //-------------------------- vv Standar
    public function setting_inputjadwal()
    {
        return [    'content_view'      => 'admission/v_inputjadwal',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'inputjadwal',
                    'active_menu_level' => '',
                    'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TIME, PLUG_DATE ],
                    'script_js'         => ['js_admission-inputjadwal']
               ];
    }
    //list input jadwal
    public function inputjadwal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)) //lihat define di atas
        { 
           $data                   = $this->setting_inputjadwal(); //letakkan di baris pertama\
           $this->db->order_by('namaunit','asc');
           // $data['data_list']      = $this->mkombin->get_inputjadwal();
           $data['title_page']     = 'Jadwal Pendaftaran';
           $data['mode']           = 'view';
           $data['table_title']    = 'Jadwal Pendaftaran';
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    //hapus jadwal by tanggal 
    public function hapusjadwal()
    {
        $idunit = $this->input->post('idunit');
        $iddokter = $this->input->post('iddokter');
        $tgl1   = $this->input->post('tglawal');
        $tgl2   = $this->input->post('tglakhir');
        $unit   = ((empty($idunit)) ? "" : "idunit = '".$idunit."' and " );
        $dokter = ((empty($iddokter)) ? "" : "idpegawaidokter = '".$iddokter."' and " );
        $sql    = "delete from rs_jadwal WHERE ".$unit." date(tanggal) BETWEEN '".$tgl1."' and '".$tgl2."'";        
        
        $arrMsg = $this->db->query($sql);        
        pesan_success_danger($arrMsg, 'Hapus Jadwal Berhasil.', 'Hapus Jadwal Gagal.','js');
        
    }
    
    public function load_datainputjadwal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)) //lihat define di atas
        {   
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_jadwalpoli_(true);
            $last = $this->db->last_query();
            $data=[]; $no=0;
            if($getData)
            {
                foreach ($getData->result() as $field) {
                    $no++;
                    $row = array();
                    $row[] = $field->namaunit;
                    $row[] = $field->namaloket;
                    $row[] = $field->namalengkap;
                    $row[] = tanggalwaktu($field->tanggal);
                    $row[] = waktuindo($field->jamakhir);
                    $row[] = $field->quota;
                    $row[] = $field->status;
                    $row[] = $field->catatan;
                    $row[] = $field->namagrup;
                    $row[] = '<div id="hapus'.$no.'"><a onclick="editJadwal('.$no.','.$field->idjadwal.')" class="btn btn-default btn-sm" data-toggle="tooltip" data-original-title="Ubah"><i class="fa fa-pencil"></i></a> <a onclick="hapusJadwal('.$no.','.$field->idjadwal.')" class="btn btn-default btn-sm" data-toggle="tooltip" data-original-title="Hapus"><i class="fa fa-trash"></i></a></div>';
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_jadwalpoli(),
                "recordsFiltered" => $this->mdatatable->filter_dt_jadwalpoli_(),
                "data" => $data,
                "last"=>$last
            );    
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }   
    }
    public function inputjadwal_generate()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)) //lihat define di atas
        {
            $tglmulai = $this->input->post('tglawal');
            $tglakhir = $this->input->post('tglakhir');
            $idunit   = $this->input->post('idunit');
            $result = $this->db->query("CALL pgeneratejadwal('$tglmulai','$tglakhir','$idunit')");
            pesan_success_danger($result,'Generate Success..!','Generate Failed..!','js');
        }
    }
    public function inputjadwal_editdata()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)) //lihat define di atas
        {
            // $id = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id'));
            $this->mgenerikbap->setTable('rs_jadwal');
            echo json_encode($this->mgenerikbap->select("*",["idjadwal"=>hilangkan_kutipganda($this->input->post('id'))])->row_array());
        }
    }
    public function inputjadwal_hapus()
    {
        $this->load->model('mviewql');
        $this->mgenerikbap->setTable('rs_jadwal');
        $arrMsg = $this->mgenerikbap->delete($this->input->post("i"));
        
        
        $idjadwal = $this->input->post('i');
        
        //data yang akan dihapus
        $dthapus = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('hapus_jadwal',0,$idjadwal);
        //data dikirim ke website
        $uploadedJSON = json_encode(['hapus' => $dthapus]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/hapusjadwal_where', $uploadedJSON);
        
        pesan_success_danger($arrMsg, 'Delete Jadwal Success.!' , 'Delete Jadwal Failed.!', 'js');
    }
    public function inputjadwal_update()
    {
        $this->load->model('mviewql');
        $idjadwal = $this->input->post('id');
        
        //data yang akan dihapus
        $dthapus = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('hapus_jadwal',0,$idjadwal);
        
        //ubah data jadwal
        $this->mgenerikbap->setTable('rs_jadwal');
        $data = [
            'idpegawaidokter' => $this->input->post('dokter'),
            'idloket'         => $this->input->post('loket'),
            'idjadwalgrup'    => ($this->input->post('jadwalgrup')==0)?null:$this->input->post('jadwalgrup'),
            'tanggal'         => $this->input->post('tanggal').' '.$this->input->post('mulai'),
            'jamakhir'        => $this->input->post('akhir'),
            'quota'           => $this->input->post('quota'),
            'status'          => $this->input->post('status'),
            'catatan'         => $this->input->post('catatan'),
            'singkronisasi'   => 0
        ];
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data,hilangkan_kutipganda($idjadwal));
        
        //data yang akan diubah
        $dtupdate = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('hapus_jadwal',0,$idjadwal);
        
        //data dikirim ke website
        $uploadedJSON = json_encode(['update' => $dtupdate, 'hapus' => $dthapus]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertjadwal_where', $uploadedJSON);

        //update singkronisasi jadwal jika berhasil
        if ($data->metadata->code == '200')
        {
            $this->db->update('rs_jadwal', ['singkronisasi' => 1], ['idjadwal' => $idjadwal]);
        }
        
        pesan_success_danger($arrMsg, 'Update Jadwal Success.!' , 'Update Jadwal Failed.!', 'js');
    }
    
    public function inputjadwal_tampilstatus()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)) //lihat define di atas
        {
            echo json_encode($this->mkombin->get_data_enum('rs_jadwal','status'));
        }
    }
    ///// --- cetak gelang
    public function cetak_gelang()
    {
        $arrMsg = $this->db->query("select concat('RM: ',ppas.norm,'/ TTL : ',ifnull(date_format(ppap.tanggallahir,'%d/%m/%Y'),'')) rmttl, concat(ifnull(ppap.identitas,''),' ',ifnull(ppap.namalengkap,'')) as namalengkap, concat('NIK : ', ppap.nik) nik, ppas.norm from person_pasien ppas, person_person ppap where ppas.idperson = ppap.idperson and ppas.norm='".$this->input->post('id')."'")->result_array();
        echo json_encode($arrMsg);
    }
    
    //// ---  cetak lembar rm
    public function cetak_lembarrm()
    {
        $norm = $this->input->post('id');
        $arrMsg = $this->db->query("select distinct pp.*, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, dp.namapekerjaan, dpn.namapendidikan, gd.namadesakelurahan from person_pasien p
            left join person_person pp on pp.idperson = p.idperson
            left join demografi_pekerjaan dp on dp.idpekerjaan = pp.idpekerjaan
            left join demografi_pendidikan dpn on dpn.idpendidikan = pp.idpendidikan
            left join geografi_desakelurahan gd on gd.iddesakelurahan = pp.iddesakelurahan 
            where p.norm='$norm' ")->row_array();
        echo json_encode(['pasien'=>$arrMsg,'penanggung'=>$this->getdata_penanggungpasien($norm)]);
    }
    
    //cetak laporan operasi
    public function cetak_laporanoperasi()
    {
        $sql = $this->db->query("SELECT ro.*, ppas.norm,pper.namalengkap, pper.nik, date_format(pper.tanggallahir,'%d/%m/%Y') as tanggallahir,
            if(ro.resikotinggi=0,'Tidak','Ya') as resikotinggi, 0 as lamaoperasi, 
            namadokter(rjo.iddokteroperator) as dokteroperator, 
            namadokter(ro.idpegawaiasisten) as asistenbedah, 
            ifnull(namadokter(ro.idpegawaiinstrumen),'') as instrumen,
            (select macamoperasi from rs_operasi_macamoperasi rom where rom.idmacamoperasi = ro.idmacamoperasi) as macamoperasi
                from rs_operasi ro
                join rs_jadwal_operasi rjo on rjo.idjadwaloperasi = ro.idjadwaloperasi
                join person_pendaftaran ppd on ppd.idpendaftaran = ro.idpendaftaran
                join person_pasien ppas on ppas.norm = ppd.norm
                join person_person pper on pper.idperson = ppas.idperson
                where ro.idjadwaloperasi = '".$this->input->post('idjadwaloperasi')."' and ro.statusoperasi='selesai' ");
        if($sql->num_rows() > 0)
        {
            $data['operasi'] = $sql->row_array();
            $data['jenisanestesi'] = ((empty($data['operasi']['jenisanestesi'])) ? '' : $this->db->query("select group_concat(jenisanestesi SEPARATOR ', ') as jenisanestesi from rs_jenisanestesi rj where rj.idjenisanestesi in (".$data['operasi']['jenisanestesi'].")")->row_array());
            $data['jenisoperasi']  = ((empty($data['operasi']['jenisoperasi'])) ? '' : $this->db->query("select group_concat(jenisoperasi SEPARATOR ', ') as jenisoperasi from rs_jenisoperasi rj where rj.idjenisoperasi in (".$data['operasi']['jenisoperasi'].")")->row_array());
            
            $post = $this->input->post();
            
            if(isset($post['tampilbarang']) && empty(!$post['tampilbarang'])) // tampil data penggunaan barang operasi
            {
                $data['barang'] = $barang = $this->db->query("SELECT rb.namabarang, rob.jumlahpemakaian , rs.namasatuan, rsd.namasediaan
                FROM rs_operasi_barangpemeriksaan rob 
                join rs_barang rb on rb.idbarang = rob.idbarang
                join rs_satuan rs on rs.idsatuan = rb.idsatuan
                join rs_sediaan rsd on rsd.idsediaan = rb.idsediaan
                WHERE rob.idoperasi = '".$data['operasi']['idoperasi']."' ORDER by rsd.namasediaan")->result_array();
            }
        }
        else
        {
            $data['operasi'] = 'null';
        }
        
        echo json_encode($data);
    }
    
    /**
     * Blacklist Pasien
     */
    
    public function blacklist()
    {
      if ($this->pageaccessrightbap->checkAccessRight(V_BLACKLIST)) //lihat define di atas
      {     
       $data = [
           'content_view'      => 'admission/v_blacklistpasien',
           'active_menu'       => 'admission',
           'active_sub_menu'   => 'blacklist',
           'active_menu_level' => ''
           ];
       $data['title_page']     = 'Blacklist Pasien';
       $data['mode']           = 'view';
       $data['script_js']      = ['js_blacklistpasien'];
       $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN ];
       $this->load->view('v_index', $data);
      }
      else
      {
        aksesditolak();
      }
    }
    
    public function dt_blacklistpasien()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_blacklistpasien_(true);
        $data=[]; 
        if($getData)
        {
            foreach ($getData->result() as $obj)
            {
                $row = [];
                $row[] = $obj->norm;
                $row[] = $obj->identitas.' '.$obj->namalengkap;
                $row[] = $obj->namaunit;
                $row[] = $obj->alasan;
                $row[] = '<a id="blacklistpasien" namaunit="'.$obj->namaunit.'" nama="'.$obj->namalengkap.'" norm="'.$obj->norm.'" idunit="'.$obj->idunit.'" mode="Un Blacklist" class="btn btn-xs btn-danger" data-toggle="tooltip" data-original-title="Un Blacklist Pasien"><i class="fa fa-ban"></i></a>';
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_blacklistpasien(),
            "recordsFiltered" => $this->mdatatable->filter_dt_blacklistpasien(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }    
    
    public function insert_blacklistpasien()
    {
        $idunit = $this->input->post('idunit');
        $norm   = $this->input->post('norm');
        $data   = [
            'idunit'=>$idunit,
            'norm'=>$norm,
            'alasan'=>$this->input->post('alasan')
        ];
        $arrMsg = '';
        if($this->input->post('mode') == 'Blacklist')
        {
            $arrMsg = $this->db->replace('person_pasien_blacklistunit',$data);
        }
        else
        {
            $arrMsg = $this->db->delete('person_pasien_blacklistunit',['idunit'=>$idunit,'norm'=>$norm]);
        }
        
        pesan_success_danger($arrMsg, $this->input->post('mode').' Pasien Berhasil.', $this->input->post('mode').' Pasien Gagal.', 'js');
    }


    /**
     * PERSON MEMBER
     */
    
    public function personmember()
    {
      if ($this->pageaccessrightbap->checkAccessRight(V_PERSONMEMBER)) //lihat define di atas
      {     
       $data = [
           'content_view'      => 'admission/v_personmember',
           'active_menu'       => 'admission',
           'active_sub_menu'   => 'personmember',
           'active_menu_level' => ''
           ];
       $data['title_page']     = 'Data Member Pasien';
       $data['mode']           = 'view';
       $data['script_js']      = ['js_personmember'];
       $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
       $this->load->view('v_index', $data);
      }
      else
      {
        aksesditolak();
      }
    }
    
    public function dt_personmember()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_personmember_(true);
        $data=[]; 
        $no = $_POST['start'];
        if($getData){
//            $norm_baru = '';
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->norm;
                $row[] = $obj->namalengkap;
                $row[] = $obj->tanggallahir;
                $row[] = $obj->alamat;
                $row[] = '<a id="detailmember" norm="'.$obj->norm.'" namalengkap="'.$obj->namalengkap.'" tgllahir="'.$obj->tanggallahir.'" alamat="'.$obj->alamat.'" class="btn btn-xs btn-info" '. ql_tooltip('detail member').'><i class="fa fa-eye"></i></a>';
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_personmember(),
            "recordsFiltered" => $this->mdatatable->filter_dt_personmember(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }    
    //-- end person member
    /**
     * Detail member Pasien
     */
    public function detailmember()
    {
        $detail = $this->db->select('pm.idmember, pm.tanggaldaftar, pm.daftarulang, if(pm.statusaktif=1,"Aktif","Tidak Aktif") as status, rm.member, rm.keterangan')
                ->join('rs_mastermember rm','rm.idmember=pm.idmember')
                ->get_where('person_pasien_member pm',['pm.norm'=>$this->input->post('norm'),'pm.isdelete'=>0])->result_array();
        echo json_encode($detail);
    }
    /**
     * Simpan Member Pasien
     */
    public function save_personmember()
    {
        $tanggaldaftar = $this->input->post('tanggaldaftar');
        $idmember      = explode(',', $this->input->post('idmember'))[0];
        $hariberlaku   = explode(' ',$this->input->post('hariberlaku'))[0];
        $mode          = $this->input->post('mode');
        
        $data = [
          'norm'            => $this->input->post('norm'),
          'idmember'        => $idmember,
          'tanggaldaftar'   => $tanggaldaftar,
          'daftarulang'     => date("Y-m-d", strtotime( $hariberlaku." Days", strtotime($tanggaldaftar))),
          'hariberlaku'     => $hariberlaku
        ];
        $this->mgenerikbap->setTable('person_pasien_member');
        $arrMsg = $this->mgenerikbap->insert_replaceonduplicate($data);
        pesan_success_danger($arrMsg, $mode.' Berhasil.', $mode.' Gagal.', 'js');
    }
    /**
     * formreadypersonmember
     */
    public function formreadypersonmember()
    {
        $post = $this->input->post();
        $data = $this->db->select('ppm.norm, (select concat("RM : ",a.norm," - ",ifnull(b.identitas,"")," ", b.namalengkap) from person_pasien a, person_person b where a.norm=ppm.norm and b.idperson=a.idperson) as pasien, concat(ppm.idmember,",",ppm.hariberlaku) as idmember,ppm.hariberlaku,'
                . '(select concat("<b>",rm.member,"</b> - ",rm.keterangan) from rs_mastermember rm where idmember=ppm.idmember) as member,'
                . 'ppm.tanggaldaftar')->get_where('person_pasien_member ppm',['ppm.idmember'=>$post['id'],'norm'=>$post['norm']])->row_array();
        echo json_encode($data);
    }
    
    //hapus member
    public function hapusmember()
    {
        $arrMsg = $this->db->update('person_pasien_member',['isdelete'=>1],['idmember'=>$this->input->post('idmember'),'norm'=>$this->input->post('norm')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }


    /// --- DATA INDUK
    public function setting_datainduk()
    {
        return [    'content_view'      => 'admission/v_datainduk',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'datainduk',
                    'active_menu_level' => ''
               ];
    }
    
    public function datainduk()
    {
      if ($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)) //lihat define di atas
      {     
       $data                   = $this->setting_datainduk(); //letakkan di baris pertama
       $data['title_page']     = 'Data Induk';
       $data['mode']           = 'view';
       $data['table_title']    = 'Data Induk';
       $data['jsmode']         = 'datainduk';
       $data['script_js']      = ['riwayat_periksa','datatable/js_datainduk'];
       $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
       $this->load->view('v_index', $data);
      }
      else
      {
        aksesditolak();
      }
    }
    
    public function dt_datainduk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)) //lihat define di atas
        {   
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_datainduk_(true);
            $data=[]; $no=$_POST['start'];
            if($getData){
                foreach ($getData->result() as $field) {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id   = $this->encryptbap->encrypt_urlsafe($field->idperson, "json");
                    $norm = $field->norm;
                    $no++;
                    
                    $menu  = '<a id="riwayatdtpasien" onclick="riwayatpasien('.$norm.')" norm="'.$norm.'" data-toggle="tooltip" title="" data-original-title="Riwayat Periksa" class="btn btn-info btn-xs" ><i class="fa fa-eye"></i></a>';
                    $menu .= ' <a onclick="buatQrPasien('.$norm.')" data-toggle="tooltip" data-original-title="Cetak kartu" class="btn btn-primary btn-xs" ><i class="fa fa-credit-card"> '.$field->cetakkartu.' </i></a>';
                    $menu .= (($this->pageaccessrightbap->checkAccessRight(V_MENUAKSIDATAINDUK)) ? ' <a data-toggle="tooltip" title="" data-original-title="Ubah data" class="btn btn-warning btn-xs" href="'.base_url('cadmission/edit_datainduk/'.$id).'" ><i class="fa fa-pencil"></i></a>' : '' );
                    $menu .= ' <a id="resetpassword"   norm="'.$norm.'" tanggal="'.$field->tanggallahir.'" pasien="'.$field->namalengkap.'" class="btn btn-danger btn-xs" '.  ql_tooltip('Reset Password').'><i class="fa fa-expeditedssl"></i></a>';
                    
                    $row = array();
                    $row[] = $field->nik;
                    $row[] = $field->norm;
                    $row[] = $field->namalengkap;
                    $row[] = $field->tanggallahir;
                    $row[] = $field->jeniskelamin;
                    $row[] = $field->notelpon;
                    $row[] = $field->alamat;
                    $row[] = $menu;
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" =>$data
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
          aksesditolak();
        }
    }
    
    //// --  edit data induk
    public function edit_datainduk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)) //lihat define di atas
        {
            $data                 = $this->setting_datainduk();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('person_person');      
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->db->query("select * from person_person pp, person_pasien p where p.idperson=pp.idperson and pp.idperson='$id'")->row_array();
            $data['data_bpjs'] = $this->db->query("select * from bpjs_bpjs b where b.norm='".$data['data_edit']['norm']."'")->row_array();
            $data['title_page'] = 'Edit Data Induk';
            $data['mode'] = 'edit';
            $data['dt_agama']           = $this->mkombin->get_data_enum('person_person','agama');
            $data['dt_jeniskelamin']    = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            $data['dt_golongandarah']   = $this->mkombin->get_data_enum('person_person','golongandarah');
            $data['dt_rhesus']          = $this->mkombin->get_data_enum('person_person','rh');
            $data['dt_perkawinan']      = $this->mkombin->get_data_enum('person_person','statusmenikah');
            $data['dt_kelurahan']       = $this->mgenerikbap->select_multitable("dk.iddesakelurahan, dk.namadesakelurahan, kc.namakecamatan, kc.kodepos","geografi_desakelurahan, geografi_kecamatan")->result();
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $data['dt_pekerjaan']       = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('demografi_pendidikan');
            $data['dt_pendidikan']      = $this->mgenerikbap->select('*')->result();
            $data['script_js']          = ['js_admission_datainduk'];
            $data['plugins']            = [ PLUG_DROPDOWN ];
            $this->load->view('v_index',$data);
        }
    }
    // add data induk
    public function add_datainduk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)) //lihat define di atas
        {
            $data                 = $this->setting_datainduk();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('person_person');      
            $data['title_page'] = 'Add Data Induk';
            $data['mode'] = 'add';
            $data['dt_agama']           = $this->mkombin->get_data_enum('person_person','agama');
            $data['dt_jeniskelamin']    = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            $data['dt_golongandarah']   = $this->mkombin->get_data_enum('person_person','golongandarah');
            $data['dt_rhesus']          = $this->mkombin->get_data_enum('person_person','rh');
            $data['dt_perkawinan']      = $this->mkombin->get_data_enum('person_person','statusmenikah');
            $data['dt_kelurahan']       = $this->mgenerikbap->select_multitable("dk.iddesakelurahan, dk.namadesakelurahan, kc.namakecamatan, kc.kodepos","geografi_desakelurahan, geografi_kecamatan")->result();
            $this->mgenerikbap->setTable('demografi_pekerjaan');
            $data['dt_pekerjaan']       = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('demografi_pendidikan');
            $data['dt_pendidikan']      = $this->mgenerikbap->select('*')->result();
            $data['script_js']          = ['js_admission_datainduk'];
            $data['plugins']            = [ PLUG_DROPDOWN ];
            $this->load->view('v_index',$data);
        }
    }
    // save data induk
    public function save_datainduk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)) //lihat define di atas
        {
                //Persiapkan data
                $idperson = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idperson')));
                $this->db->update('person_pasien',['nojkn'=>$this->input->post('nojkn')],['idperson'=>$idperson]);
                $data = [
                    'nik' => $this->input->post('noidentitas'),
                    'jeniskelamin' => $this->input->post('jeniskelamin'),
                    'namalengkap' => $this->input->post('namalengkap'),
                    'agama' => $this->input->post('agama'),
                    'namalengkappemanggilan' => $this->input->post('namalengkappemanggilan'),
                    'golongandarah' => $this->input->post('golongandarah'),
                    'tanggallahir' => $this->input->post('tanggallahir'),
                    'rh' => $this->input->post('rhesus'),
                    'tempatlahir' => $this->input->post('tempatlahir'),
                    'statusmenikah' => $this->input->post('statusmenikah'),
                    'notelpon' => $this->input->post('notelpon'),
                    'idpendidikan' => $this->input->post('pendidikan'),
                    'alamat' => $this->input->post('alamat'),
                    'idpekerjaan' => $this->input->post('pekerjaan')
                ];
                $this->mgenerikbap->setTable('person_person');//Simpan
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, hilangkan_kutipganda($idperson));
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idperson) ? 'Save Success.!' :'Update Success.!' , empty($idperson) ? 'Save  Failed.!' :'Update Failed.!' );
            // }
            redirect(base_url('cadmission/datainduk'));
        }
    }
    // pencarian data berdasarkan tanggal lahir
    public function get_dataindukbytgllahir()
    {
        echo json_encode($this->db->query("select * from person_person pp left join person_pasien p on p.idperson = pp.idperson where date(pp.tanggallahir)='".$this->input->post('tanggallahir')."' order by norm asc")->result());
    }
    ///// cari pasien spesifik
    public function caripasienspesifik()
    {
        (!empty($this->input->post('rm')) ? $rm="and ppas.norm like '%".$this->input->post('rm')."%'" : $rm="" );
        (!empty($this->input->post('nama')) ? $nama="and pp.namalengkap like '%".$this->input->post('nama')."%'" : $nama="" );
        (!empty($this->input->post('alamat')) ? $alamat="and pp.alamat like'%".$this->input->post('alamat')."%'" : $alamat="" );
        (!empty($this->input->post('tanggallahir')) ? $tanggallahir="and pp.tanggallahir like '%".$this->input->post('tanggallahir')."%'" : $tanggallahir="" );

        $arrMsg = $this->db->query("select pp.idperson ,pp.nik, pp.namalengkap, pp.tanggallahir, pp.alamat, ppas.norm, pp.cetakkartu from person_person pp left join person_pasien ppas on ppas.idperson = pp.idperson and ppas.status_pasien='A' where pp.status_person='A' ".$nama.$alamat.$tanggallahir.$rm)->result();
        echo json_encode($arrMsg);
    }
    // -- ranap list bangsal
    public function ranap_listbangsal()
    {
        $dtpasien = $this->db->query("select p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, pp.norm, pper.alamat, pp.nojkn, pper.nik, pper.namalengkap, pper.tanggallahir, pper.jeniskelamin, p.keteranganobat, p.keteranganradiologi, p.keteranganlaboratorium from  person_pasien pp, person_person pper, person_pendaftaran p where pp.norm='".$this->input->post('a')."'and pper.idperson = pp.idperson and p.idpendaftaran = '".$this->input->post('p')."' order by p.waktuperiksa desc limit 1")->row_array();
        $this->mgenerikbap->setTable("rs_bed_status");
        $dt_bedstatus = $this->mgenerikbap->select("*")->result();
        $this->db->where("( jeniskelaminbangsal = '".$dtpasien['jeniskelamin']."' OR jeniskelaminbangsal = 'campur' )", null, false);
        $dt_bangsal = $this->mgenerikbap->select_multitable("idbangsal, namabangsal, kelas","rs_bangsal, rs_kelas")->result();
        $dtkelas = $this->db->query("select * from rs_kelas where idkelas between 2 and 5")->result();
        $dtdokter = $this->db->query('SELECT ppeg.idpegawai, trim(concat(ifnull(titeldepan, ""), " ", namalengkap, " ", ifnull(titelbelakang, ""))) as namalengkap FROM person_pegawai ppeg join person_person pp on pp.idperson=ppeg.idperson join person_grup_pegawai pgp on pgp.idgruppegawai=ppeg.idgruppegawai and pgp.isdokter=1 order by namalengkap')->result();
        echo json_encode(['bangsal'=>$dt_bangsal, 'statusbed'=>$dt_bedstatus,'pasien'=>$dtpasien,'kelas'=>$dtkelas,'dokter'=>$dtdokter]);
    }
    // -- ranap list bed
    public function ranap_listbed()
    {
        $this->mgenerikbap->setTable("rs_bed");
        $idbangsal = $this->input->post('id');
        $data = $this->mgenerikbap->select_multitable("idbangsal, idbed, statusbed, nobed, file ","rs_bed, rs_bed_status",['idbangsal'=>$idbangsal])->result();
        // data bed yang perlu di prioritaskan di bangsal yang terpilih
        $bedprior = $this->db->query("select b.idbangsal, bs.idkelas, ps.norm, pp.namalengkap, concat(' dirawat di Bangsal ',bs.namabangsal,' kamar no ', b.nobed,' ', k.kelas) as bed from rs_inap i, person_pendaftaran p, person_pasien ps, person_person pp, rs_bed b 
            join rs_bangsal bs on bs.idbangsal = b.idbangsal  
            join rs_kelas k on k.idkelas = bs.idkelas and bs.idbangsal!='$idbangsal'
            where p.idstatuskeluar='0' and p.idpendaftaran = i.idpendaftaran and ps.norm = p.norm and pp.idperson = ps.idperson  and b.idbed = i.idbed and i.idkelas!= bs.idkelas")->result();
        echo json_encode(['bangsal'=>$data, 'info'=>$bedprior]);
    }
    public function beriAntrian()
    {
        $x = $this->input->post('id');
        $data =  $this->ql->person_pendaftaran_insert_or_update($x,['isantri'=>1]);
        echo json_encode($data);
    }
    // --- pendaftaran rawat inap dari rawat jalan
    public function pendaftaranranap_pilihbed()
    {
        $callid         = generaterandom(8);
        $idbed          = $this->input->post('bed');
        $idbangsal      = $this->input->post('bangsal');
        $israwatgabung  = $this->input->post('israwatgabung');
        $idpendaftaran  = $this->input->post('daftar');
        $nosep          = $this->input->post('nosep');
        $dokter         = $this->input->post('dokter');
        $kelasperawatan = $this->input->post('kelasperawatan');
        $kelasjaminan   = $this->input->post('kelasjaminan');
        #$this->mgenerikbap->setTable('person_pendaftaran');
        #update no sep
        $datapendaftaran = $this->db->select('nosep, idstatuskeluar')->where('idpendaftaran',$idpendaftaran)->get('person_pendaftaran')->row_array();
        
        //data rs_inap
        $dtranap = ['idpendaftaran'=>$idpendaftaran,'idbed'=>$idbed,'idpegawaidokter'=>$dokter,'idkelas'=>$kelasperawatan,'idkelasjaminan'=>$kelasjaminan,'israwatgabung'=>$israwatgabung, 'callid'=>$callid,'status'=>'pesan'];
        $arrMsg = $this->db->insert('rs_inap',$dtranap);
        if($datapendaftaran['idstatuskeluar']=='2'){
            $arrMsg = $this->mutasirajalkeranap($idpendaftaran);
            if($arrMsg)
            {
                /*update aplicare ketersediaan bad ranap*/
                $this->applicare_updatedata($idbangsal);
            }
        }else{
            $dtdaftar = ['nosep'=>$nosep,'noseplama'=>$datapendaftaran['nosep']];
            $arrMsg = $this->db->update('person_pendaftaran',$dtdaftar,['idpendaftaran'=>$idpendaftaran]);
            if($arrMsg)
            {
                /*update aplicare ketersediaan bad ranap*/
                $this->applicare_updatedata($idbangsal);
            }
        }
        pesan_success_danger($arrMsg,"Pindah rawat inap berhasil..!","Pindah rawat inap gagal..!","js");
    }
    
    public function applicare_updatedata($idbangsal)
    {
        $this->load->model('mviewql');
        $b = $this->mviewql->viewbangsalaplicare($idbangsal);
        if (array_key_exists(0, $b))
        {
            $bangsal = $b[0];
            $uploadedJSON = json_encode([   'koderuang'=>$bangsal["koderuangjkn"],
                                            'namaruang'=>$bangsal["namabangsal"],
                                            'kapasitas'=>$bangsal["kapasitas"],
                                            'tersedia'=>$bangsal["tersedialakilaki"]+$bangsal["tersediaperempuan"]+$bangsal["tersediacampur"],
                                            'tersediapria'=>$bangsal["tersedialakilaki"],
                                            'tersediawanita'=>$bangsal["tersediaperempuan"],
                                            'tersediapriawanita'=>$bangsal["tersediacampur"],
                                            'kodekelas'=>$bangsal["kodekelasjkn"]
                                        ]);
            $this->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON);
        }
    }
    // riwayat periksa pasien
    public function riwayatrekammedispasien_rajal()
    {   $post = $this->input->post();
        $mode = $this->input->post('mode');
        $norm = $post['norm'];
        $data['dtperiksa']   = $this->mkombin->getdatapendaftaranriwayatpasien($norm,$mode);
        $data['penanggung']  = $this->getdata_penanggungpasien($norm);
        $data['dtanamnesis'] = [];
        $data['planjutan']   = [];
        $detailperiksa=[];
        $data['dtdiagnosis'] = [];
        
        $data['dtresep'] = [];
        $data['dtgrup']  = [];
        foreach ($data['dtperiksa'] as $list) {
            // list anamnesis
            $data['dtanamnesis'][] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_anamnesis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, rs_pemeriksaan rp where rh.idpendaftaran='".$list->idpendaftaran."' and rh.idpemeriksaan='".$list->idpemeriksaan."' and rj.jenisicd='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
            // list periksa lanjutan
            $data['planjutan'][]   = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, person_pendaftaran pp, rs_pemeriksaan rp where rh.idpendaftaran='".$list->idpendaftaran."' and rh.idpemeriksaan='".$list->idpemeriksaan."' and rj.jenisicd!='tindakan' and rj.jenisicd!='diagnosa' and rj.jenisicd!='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and pp.idpendaftaran = rh.idpendaftaran and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
            // list diagnosis
            $data['dtdiagnosis'][] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpendaftaran='".$list->idpendaftaran."' and rh.idpemeriksaan='".$list->idpemeriksaan."' and rj.jenisicd!='radiologi' and rj.jenisicd!='vital sign' and rj.jenisicd!='laboratorium' and rj.jenisicd!='ECHOCARDIOGRAPHY' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
            // list pemakaian obat
            $data['dtresep'][] = $this->mkombin->pemeriksaan_listbhp($list->idpendaftaran,'riwayat','rajal');
            $data['dtgrup'][]  = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  FROM rs_barangpemeriksaan b WHERE b.idpendaftaran='".$list->idpendaftaran."' GROUP by grup")->result();
            // list detail periksa
            $data['detailperiksa'][] = $this->db->query("select rp.rekomendasi, pp.keteranganobat, pp.keteranganradiologi,pp.saranradiologi, pp.keteranganlaboratorium, rp.diagnosa,rp.anamnesa,rp.keterangan from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran=rp.idpendaftaran  where rp.idpendaftaran='".$list->idpendaftaran."' and rp.idpemeriksaan='".$list->idpemeriksaan."'")->result();
            //list subjective dan objective
            $data['subobj'][] = $this->db->query("SELECT * FROM `rs_pengkajian_awal_medis` inner join person_pendaftaran on rs_pengkajian_awal_medis.idpendaftaran = person_pendaftaran.idpendaftaran where person_pendaftaran.norm =".$norm)->result();
        }
        //data pasien
        $data['dtpasien']    = $this->mkombin->getdatapasienriwayatperiksa($norm);
        echo json_encode($data);
    }
    public function resumerawatjalanjson()
    { $post = $this->input->post();
        $dt = $this->db->query("select rp.idpemeriksaan, pp.norm, rp.anamnesa,rp.keterangan,pp.keteranganobat, pp.keteranganradiologi,pp.saranradiologi, pp.keteranganlaboratorium, pp.idpendaftaran, date_format( rp.waktu,'%d/%b/%Y') tanggal, date_format( rp.waktu,'%H:%i:%s') waktu,rp.idpegawaidokter, namadokter(rp.idpegawaidokter) as namadokter, (select namaunit from rs_unit where idunit = rp.idunit) as namaunit  from person_pendaftaran pp, rs_pemeriksaan rp, person_pegawai ppeg, person_person pper where  pp.idpendaftaran='".$post['idp']."' and rp.idpendaftaran = pp.idpendaftaran and ppeg.idpegawai = rp.idpegawaidokter and pper.idperson = ppeg.idperson" )->result_array();
        $vitalsign=[];
        $planjutan=[];
        $obat = [];
        $detailperiksa=[];
        $tindakan = [];
        $diagnosis = [];
        foreach ($dt as $list) {
            // list vitalsign
            $vitalsign[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, rs_pemeriksaan rp where rh.idpemeriksaan='".$list['idpemeriksaan']."' and rj.jenisicd='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
            // list periksa lanjutan
            $penunjang[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, person_pendaftaran pp, rs_pemeriksaan rp where rh.idpemeriksaan='".$list['idpemeriksaan']."' and rj.jenisicd!='tindakan' and rj.jenisicd!='diagnosa' and rj.jenisicd !='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and pp.idpendaftaran = rh.idpendaftaran and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
            // list diagnosis
            $diagnosis[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpemeriksaan='".$list['idpemeriksaan']."' and rj.jenisicd='diagnosa' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
            // list tindakan
            $tindakan[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpemeriksaan='".$list['idpemeriksaan']."' and rj.jenisicd='tindakan' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
            // list pemakaian obat
            $obat[] = $this->db->query("SELECT concat( ifnull(b.namabarang,''), ', dosis racik ', ifnull(rb.kekuatan,''),', jumlah ambil ', ifnull(rb.jumlahpemakaian,''), ' ',ifnull(s.namasatuan,'')) as obat FROM rs_barangpemeriksaan rb, rs_barang b, rs_satuan s WHERE rb.idpemeriksaan='".$list['idpemeriksaan']."' and b.idbarang = rb.idbarang and s.idsatuan = b.idsatuan")->result();
            // list detail periksa
            $detailperiksa[] = $this->db->query("select pp.keteranganobat, pp.keteranganradiologi,pp.saranradiologi, pp.keteranganlaboratorium, rp.diagnosa,rp.anamnesa,rp.keterangan from rs_pemeriksaan rp, person_pendaftaran pp where pp.idpendaftaran = rp.idpendaftaran and rp.idpendaftaran='".$list['idpendaftaran']."'")->result();
        }
        //data pasien
        $norm = $dt[0]['norm'];
        $dtpasien = $this->db->query("select p.norm, pp.tanggallahir,  concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap from person_pasien p left join person_person pp on pp.idperson = p.idperson where p.norm='".$norm."'")->row_array();
        echo json_encode(['dtperiksa'=>$dt,'penunjang'=>$penunjang, 'vitalsign'=>$vitalsign, 'dtdiagnosis'=>$diagnosis,'tindakan'=>$tindakan, 'dtpasien'=>$dtpasien, 'obat'=>$obat,'detailperiksa'=>$detailperiksa]);
    
    }
    public function riwayat_listdata()
    {
        $idpendaftaran = $this->input->post("id");
        $dtriwayat = $this->db->query("SELECT rj.jenisicd, rh.icd, rh.remark, rh.jumlah, rh.idpaketpemeriksaan, CASE WHEN ri.istext='numerik' THEN rh.nilai ELSE rh.nilaitext END AS nilai, ri.namaicd from rs_hasilpemeriksaan rh left join rs_icd ri on ri.icd = rh.icd left join rs_jenisicd rj on rj.idjenisicd = ri.idjenisicd where rh.idpendaftaran='$idpendaftaran' UNION ALL select 'Obat/BHP', b.namabarang, round(rb.jumlahpemakaian) , b.kekuatan, rb.kekuatan,round(((rb.kekuatan/b.kekuatan)*rb.jumlahpemakaian)),  round(rb.total) from rs_barangpemeriksaan rb left join rs_barang b on b.idbarang = rb.idbarang where rb.idpendaftaran='$idpendaftaran'")->result();
        $dtperiksa = $this->db->query("SELECT concat(if(ppe.titeldepan='' ,'',ppe.titeldepan ) ,' ',pper.namalengkap,' ',if(ppe.titelbelakang='','',ppe.titelbelakang)) as namadokter, ru.namaunit,al.namaloket, p.carabayar,rp.anamnesa, rp.keterangan, p.idstatuskeluar,  date_format(k.waktutagih,'%d/%b/%Y') as waktutagih, p.jenispemeriksaan, date_format(p.waktu,'%d/%b/%Y') as waktu, p.keteranganlaboratorium, p.keteranganradiologi, p.keteranganobat, p.kondisikeluar from person_pendaftaran p, rs_pemeriksaan rp, keu_tagihan k, rs_jadwal rj, rs_unit ru, antrian_loket al, person_pegawai ppe, person_person pper  where p.idpendaftaran='$idpendaftaran' and rp.idpendaftaran = p.idpendaftaran and k.idpendaftaran=p.idpendaftaran and rj.idjadwal = rp.idjadwal and ru.idunit = rj.idunit and al.idloket = ru.idloket and ppe.idpegawai = rj.idpegawaidokter and pper.idperson = ppe.idperson")->row_array();
        echo json_encode(['riwayat'=>$dtriwayat,'periksa'=>$dtperiksa]);
    }
    // dt pasien lengkap
    public function get_dtpasien()
    {
        $norm = 0;
        $norm = $this->db->query("SELECT distinct pp.*, p.norm, p.nojkn, p.alergi FROM  person_pasien p, person_person pp  WHERE p.norm = '$norm' AND pp.idperson = p.idperson ")->row_array();
    }
    // dt riwayatperiksa
    public function riwayat_listtanggal()
    {
        $rm = $this->input->post("norm");
        $dtwaktu = $this->db->query("SELECT date_format(waktu,'%d/%b/%Y') as waktu, idpendaftaran FROM person_pendaftaran where norm='$rm' ORDER BY date(waktu) DESC ")->result();
        $dtpasien=$this->db->query("SELECT pp.namalengkap, p.norm FROM  person_pasien p, person_person pp  WHERE p.norm = '$rm' AND pp.idperson = p.idperson ")->row_array();
        echo json_encode(['waktu'=>$dtwaktu,'pasien'=>$dtpasien]);
    }
    // ambil data kartu pasien
    public function getdt_kartupasien()
    {
        $norm = $this->input->post("id");
        echo json_encode($this->db->query("select p.norm, pp.namalengkap, pp.jeniskelamin, pp.tanggallahir, pp.idperson from person_pasien p, person_person pp where p.norm='$norm' and pp.idperson=p.idperson")->row_array()); // Andri [bug fix 0000543]
    }
    // hitung cetak kartu berobat pasien
    public function hitungcetakkartu_berobat()
    {
        $this_idperson = $this->input->post('id'); // Andri [bug fix 0000543]
        $q = $this->db->query("SELECT idperson from person_pasien where norm='".$this->input->post('id')."'")->row_array()['idperson'];
        $query = $this->db->query("UPDATE person_person SET cetakkartu=cetakkartu+1 WHERE idperson=$this_idperson"); // Andri [bug fix 0000543]
        echo json_encode($query);
    }
    // cek dataa verifikasi pendaftaran atau pemeriksaan pasien bpjs
    public function getverifpasienbpjs()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $query = $this->db->query("select pp.caradaftar, rp.idjadwal,date_format(rp.waktu,'%d/%m/%Y') as tanggal,rj.idunit, pp.nopesan, pper.idperson, pp.idpendaftaran, pp.carabayar, ppen.nojkn, pper.nik, pp.norm, pp.nokontrol, pp.idpendaftaran, rs.noskdpql, concat(ifnull(pper.identitas,''),' ',ifnull(pper.namalengkap,'')) as namalengkap from person_pendaftaran pp left join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran left join rs_skdp rs on rs.nokontrol = pp.nokontrol and pp.idpendaftaran='$idpendaftaran' left join person_pasien ppen on ppen.norm = pp.norm left join person_person pper on pper.idperson = ppen.idperson left join rs_jadwal rj on rj.idjadwal = rp.idjadwal where pp.idpendaftaran = '$idpendaftaran'")->row_array();
        echo json_encode($query);
    }
    // save aksi verifikasi pendaftaran pasien bpjs
    public function saveverifpasienbpjs()
    {
        // SIAPKAN DATA
        $carabayar = $this->input->post('carabayar');
        $nama = $this->input->post('nama');
        $nik = $this->input->post('nik');
        $tanggal = format_date_to_sql($this->input->post('tanggal'));
        $idjadwal = $this->input->post('politujuan');
        // DATA PASIEN BPJS
        $nojkn = $this->input->post('nojkn');
        $nosep = $this->input->post('nosep');
        $norujukan = $this->input->post('norujukan');
        // data pasien kontrol
        $noskdp = $this->input->post('noskdp');
        $nokontrol = $this->input->post('nokontrol');
        // data id / key
        $idperson = $this->input->post('idperson');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $nopesan = $this->input->post('nopesan');
        // simpan ke tabel person_person
        $dtperson = ['nik'=>$nik,'namalengkap'=>$nama,'namalengkappemanggilan'=>$nama];
        $this->mgenerikbap->setTable('person_person');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($dtperson,$idperson);
        // JIKA PEMBAYARAN JKN
        if($carabayar!='mandiri')
        {
            // simpan ke tabel person_pendaftaran
            $arrmsg = $this->ql->person_pendaftaran_insert_or_update($idpendaftaran,['nosep'=>$nosep,'norujukan'=>$norujukan,'isverif'=>'1']);
            $arrmsg = $this->db->query("update person_pasien set nojkn='$nojkn' where idperson='$idperson'");
            $arrmsg = $this->rs_pemeriksaan_update_where(['status'=>'antri'],['idpendaftaran'=>$idpendaftaran]);
            if(!$arrmsg){pesan_danger("Update person pasien gagal..! please contact developer..!","js");}
        }

        // ambil data idpemeriksaan dan tanggal periksa
        $dtperiksa = $this->db->query("select rp.idpemeriksaan, date_format(rp.waktu,'%Y-%m-%d') as tanggal from rs_pemeriksaan rp, person_pendaftaran pp where rp.idpendaftaran='$idpendaftaran' and pp.idpendaftaran = rp.idpendaftaran")->row_array();
        if(!empty($noskdp))// JIKA NOSKDP TIDAK KOSONG ATAU PASIEN KONTROL
        {
            //update tabel rs_pemeriksaan, idjadwal 
            if(!$arrmsg){pesan_danger("Update pemeriksaan gagal..! please contact developer..!","js");}
            //update tabel rs_pemeriksaan, ubah no antrian
            $callid = generaterandom(8);
            $arrmsg = $this->ql->antrianset_noantriperiksa($callid,$idjadwal[0],$dtperiksa['idpemeriksaan'],$idperson);
            $arrmsg = $this->ql->rs_pemeriksaan_insert_or_update($dtperiksa['idpemeriksaan'],['noantrian'=>$this->ql->antrian_get_number($callid),'callid'=>$callid]);
        }
        else
        {
            //update tabel rs_pemeriksaan, ubah status jadi antrian dan cetak nomor antrian periksa
            $arrmsg = $this->rs_pemeriksaan_update_where(['status'=>'antrian'],['idpendaftaran'=>$idpendaftaran]);
            if(!$arrmsg){pesan_danger("Update pemeriksaan gagal..! please contact developer..!","js");}
            if($tanggal!=$dtperiksa['tanggal'])
            {
                $arrmsg = $this->verifSetAntrian($idperson,$tanggal,$dtperiksa['idpemeriksaan']);
                if(!$arrmsg){pesan_danger("Update verif set antrian gagal..! please contact developer..!","js");}
            }

        }
        // cetak antrian anjungan pasien
        if($this->input->post('caradaftar')=='online' || !empty($noskdp))
        {
            $arrmsg = $this->rs_pemeriksaan_update_where(['status'=>'antrian'],['idpemeriksaan'=>$dtperiksa['idpemeriksaan']]);
            if(!$arrmsg){pesan_danger("Update pemeriksaan  gagal..! please contact developer..!","js");}
            return $arrmsg = $this->cetakantrianverif($dtperiksa['idpemeriksaan'],$idperson,$tanggal);
        }
        pesan_success_danger($arrmsg,"Verifikasi Berhasil...!","Verifikasi Gagal..!","js");
    }
    // update verif pendaftaran
    public function updateverifpasienbpjs()
    {
            $arrmsg = $this->ql->person_pendaftaran_insert_or_update($this->input->post('id'),['isverif'=>$this->input->post('status')]);
            pesan_success_danger($arrmsg,"Update Verifikasi Berhasil...!","Update Verifikasi Gagal..!","js");
    }
    // set antrian verifikasi ketika mengubah tanggal periksa
    public function verifSetAntrian($idperson,$tglperiksa,$idpemeriksaan)
    {
        $callid      = generaterandom(8);
        $akronimunit = $this->mgenerikbap->select_multitable("rj.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();
        $this->mgenerikbap->setTable('helper_autonumber');
        $data = [
            'id'      =>'Antrian'.date('Ymd',strtotime($tglperiksa)).'-'.$akronimunit['idstasiun'].'-'.$akronimunit['idloket'],
            'number'  =>null,
            'callid'  =>$callid,
            'expired' =>date('Y-m-d',strtotime($tglperiksa))
        ];
        $this->mgenerikbap->insert($data);
        $noantrian = $this->mgenerikbap->select_multitable('number','helper_autonumber',['callid'=>$callid])->row_array()['number'];//siapkan data no antrian
        $datau = ['noantrian' =>$noantrian,'status' =>'antrian'];
        $this->ql->rs_pemeriksaan_insert_or_update($idpemeriksaan,$datau);
        $dataa = ['idloket'=>$akronimunit['idloket'], 'tanggal'=>date('Y-m-d',strtotime($tglperiksa)), 'no'=>$noantrian, 'status'=>'antri', 'idperson'=>$idperson];
        $this->db->insert('antrian_antrian',$dataa);
    }
    // cetak data antrian verif pasien
    public function cetakantrianverif($idpemeriksaan,$idperson,$tglperiksa)
    {
        // amil data antrian
        $dtantrian = $this->db->query("select no,al.namaloket  from `antrian_antrian` aa, antrian_loket al where al.idloket = aa.idloket and idperson='$idperson' and tanggal='$tglperiksa' order by idantrian desc limit 1")->row_array();
        // ambil data pasien
        $dtpasien = $this->mgenerikbap->select_multitable("norm, namalengkap, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();
        // ambil data dokter
        $dtdokter = $this->db->query("select CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
        echo json_encode(['antrian'=>$dtantrian,'pasien'=>$dtpasien,'dokter'=>$dtdokter,'status'=>'success','message'=>'Verifikasi Berhasil..!']);
    }

    // BRIDGING BPJS DATA PENDAFARAN
    public function bpjs_getdtpesertabysep() //tampilkan data peserta by nosep
    {
        $grupjadwal = $this->db->get('rs_jadwal_grup')->result();
        $sep = $this->bpjsbap->requestSEP($this->input->post('nosep'));
        $norm = $this->db->query("select norm from person_pasien where nojkn='".$sep->peserta->noKartu."'")->row_array()['norm'];
        $rujukan = $this->bpjsbap->requestRujukan($sep->noRujukan);
        $this->bpjs_updatemasterpeserta($rujukan->rujukan->peserta);
        echo json_encode(['sep'=>$sep,'rujukan'=>$rujukan,'grup'=>$grupjadwal,'norm'=>$norm]);
    }
    public function bpjs_getdtpesertabyrujukan() //tampilkan data peserta by norujukan
    {
        $grupjadwal = $this->db->get('rs_jadwal_grup')->result();
        $rujukan = $this->bpjsbap->requestRujukan($this->input->post('norujukan'));
        // simpan data klinik perujuk
        // $this->bpjs_klinikperujuk_addupdate($rujukan->rujukan->provPerujuk);
        $ppas = $this->db->query("select * from person_pasien where nojkn='".$rujukan->rujukan->peserta->noKartu."'")->row_array();
        $this->bpjs_updatemasterpeserta($rujukan->rujukan->peserta);
        echo json_encode(['rujukan'=>$rujukan,'grup'=>$grupjadwal,'norm'=>$ppas['norm']]);
    }
    public function bpjs_getdtpesertabynojkn()
    {
        $peserta = $this->bpjsbap->requestRujukanSingle($this->input->post('nojkn'));
        // simpan data klinik perujuk
        // $this->bpjs_klinikperujuk_addupdate($peserta->provPerujuk);
        $this->bpjs_updatemasterpeserta($peserta->peserta);
        echo json_encode($peserta);
    }

    public function bpjs_getdtpesertabynojkn_COBA()
    {
        $peserta = $this->bpjsbap->requestRujukanSingle_COBA('0000095633313');
        // simpan data klinik perujuk
        // $this->bpjs_klinikperujuk_addupdate($peserta->provPerujuk);
        $this->bpjs_updatemasterpeserta($peserta->peserta);
        echo json_encode($peserta);
    }
    // tambah atau ubah data klinik perujuk pasien BPJS
    private function bpjs_klinikperujuk_addupdate($provPerujuk)
    {
        if( empty(!$provPerujuk->kode) && empty(!$provPerujuk->nama))
        {
            $this->mgenerikbap->setTable('bpjs_klinikperujuk');
            $this->mgenerikbap->update_or_insert_ignoreduplicate(['kodeklinik'=>$provPerujuk->kode,'namaklinik'=>$provPerujuk->nama]);
        }
    }
    private function bpjs_updatemasterpeserta($data) //update master hakkelas dan jenispeserta
    {
        // insert hakkelas
        $this->mgenerikbap->setTable('bpjs_hakkelas');
        $this->mgenerikbap->update_or_insert_ignoreduplicate(['idkelas'=>$data->hakKelas->kode,'kelas'=>$data->hakKelas->keterangan]);        
        // insert jenis peserta
        $this->mgenerikbap->setTable('bpjs_jenispeserta');
        $this->mgenerikbap->update_or_insert_ignoreduplicate(['idjenispeserta'=>$data->jenisPeserta->kode,'jenispeserta'=>$data->jenisPeserta->keterangan]);
    }
    
    public function gantiDokterPraktek()
    {
        $idjadwal = explode(',',$this->input->post('jadwaldokter'))[1];
        //update rs_pemeriksaan
        if($this->input->post('statusdokter')=='dokterdalam')
        {
            $arrMsg = $this->db->update('rs_jadwal',['idpegawaidokter'=>$this->input->post('dokterpengganti')],['idjadwal'=>$idjadwal]);
        }else{
            $arrMsg = $this->db->update('rs_pemeriksaan',['iddokterpengganti'=>$this->input->post('dokterpengganti')],['idjadwal'=>explode(',',$this->input->post('jadwaldokter'))[1]]);
        }
        pesan_success_danger($arrMsg, 'Ganti Dokter Berhasil..!', 'Ganti Dokter Gagal..!','js');
    }
    
    public function tampildokter()
    {
        $sql = "SELECT pg.idpegawai, concat(ifnull(pg.titeldepan,''),' ', ifnull(pp.namalengkap,'') ,' ',ifnull(pg.titelbelakang,'')) as namadokter FROM  person_pegawai pg, person_person pp where pp.idperson=pg.idperson and pg.statuskeaktifan='aktif'";
        echo json_encode($this->db->query($sql)->result());
    }
    
    public function selisih15hari_pasienbpjs()
    {
        $post = $this->input->post();
        echo json_encode($this->db->query(" select datediff('".$post['tanggal']."',date(pp.waktuperiksa)) selisih, date(pp.waktuperiksa) as riwayat  from person_pendaftaran pp where pp.norm=".$post['norm']." and pp.idunit=".$post['idunit']." and pp.carabayar='".$post['carabayar']."' and pp.idstatuskeluar=2 ORDER by pp.idpendaftaran DESC limit 1")->row_array());
    }
    public function get_enumcarabayar()
    {
         echo json_encode($this->mkombin->get_data_enum('person_pendaftaran','carabayar'));
    }
    
    public function perbaikandatapasien_rmganda()
    {
        $rmaktif   = $this->input->post('2');
        $rmdihapus =$this->input->post('1');
        $iduser    = $this->get_iduserlogin();
        $q=$this->db->query("update person_pendaftaran set norm='".$rmaktif."' where norm='".$rmdihapus."'");
        
        $q=$this->db->query("update person_person set status_person='D' where idperson= (select idperson from person_pasien where norm='".$rmdihapus."') ");
        $q=$this->db->query("update person_pasien set status_pasien='D' where norm='".$rmdihapus."'");
        $q=$this->db->query("update bpjs_bpjs set status_bpjs='D' where norm='".$rmdihapus."' ");
        
        $q=$this->db->query("update person_person set status_person='A' where idperson= (select idperson from person_pasien where norm='".$rmaktif."') ");
        $q=$this->db->query("update person_pasien set status_pasien='A' where norm='".$rmaktif."'");
        $q=$this->db->query("update bpjs_bpjs set status_bpjs='A' where norm='".$rmaktif."' ");
        
        $arrMsg=$this->db->query("update rs_pemeriksaan set norm='".$rmaktif."' where norm='".$rmdihapus."'");
        $periksa_asal = $this->db->query("SELECT idpemeriksaan, idpendaftaran, idunit, waktu  FROM rs_pemeriksaan  where norm = '".$rmdihapus."'")->result();
        $log = '';
        foreach ($periksa_asal as $obj){$log .= 'idpemeriksaan:'.$obj->idpemeriksaan.', idpendaftaran: '.$obj->idpendaftaran.', idunit: '.$obj->idunit.', waktu: '.$obj->waktu;}
        $arrMsg = $this->db->insert('log_pindahrekammedis',['normasal'=>$rmdihapus,'normtujuan'=>$rmaktif,'log'=>$log,'iduser'=>$iduser,'waktu'=>date('Y-m-d H:i:s')]);
        pesan_success_danger($arrMsg,'Pindah Rekam Medis Berhasil.!', 'Pindah Rekam Medis Gagal.!', 'js');
    }
    
    public function get_riwayatperiksapasienbynorm()
    {
        $norm = $this->input->post('norm');
        $dt   = $this->db->select('idpendaftaran as id, date(waktuperiksa) as text')->get_where('person_pendaftaran',['norm'=>$norm])->result();
        echo json_encode($dt);
    }
    public function update_riwayatperiksapasienbynorm()
    {
        $arrMsg = $this->db->update('person_pendaftaran',['norm'=>$this->input->post('norm')],['idpendaftaran'=>$this->input->post('idp')]);
        $arrMsg = $this->db->update('rs_pemeriksaan',['norm'=>$this->input->post('norm')],['idpendaftaran'=>$this->input->post('idp')]);
        pesan_success_danger($arrMsg, "Pindah Riwayat Periksa Berhasil.", "Pindah Riwayat Periksa Gagal.", "js");
    }
    //mahmud, clear
    public function bookingjadwaloperasi()
    {
        
        $post=$this->input->post();
        $sql="SELECT rp.idpemeriksaan, ppd.idpendaftaran, ru.idunit, ru.akronimunit, rj.idpegawaidokter FROM  person_pendaftaran ppd,rs_pemeriksaan rp,rs_unit ru , rs_jadwal rj WHERE ppd.idpendaftaran='".$post['idp']."' and rp.idpendaftaran=ppd.idpendaftaran and ru.idunit= ppd.idunit and rj.idjadwal=rp.idjadwal";
        $dtpasien = $this->db->query($sql)->row_array();
        $callid = generaterandom(8);
        $tanggalop = date('Ymd',strtotime($post['waktuoperasi']));
        $idantrian = 'OP'.$dtpasien['akronimunit'].$tanggalop;
        $this->ql->antrian_autonumber($idantrian,$callid,$post['waktuoperasi']);
        $kodebooking = $dtpasien['akronimunit'].$tanggalop.$this->ql->antrian_get_number($callid);//create code booking
        
        //data jadwal
        $datajo = [
            'idpemeriksaan'=>$dtpasien['idpemeriksaan'],
            'idpendaftaran'=>$post['idp'],
            'idunit'=>$dtpasien['idunit'],
            'iddokteroperator'=>$dtpasien['idpegawaidokter'],
            'idjenistindakan'=>$post['idjenistindakan'],
            'waktuoperasi'=>$post['waktuoperasi'],
            'kodebooking'=>$kodebooking,
            'idbed'=>$post['idbed']
        ];
        $arrMsg=$this->db->insert('rs_jadwal_operasi',$datajo);
        // if (!empty($post['idbangsal'])){aplicare_updatebed($post['idbangsal']);}
        pesan_success_danger($arrMsg, "Jadwal Operasi Berhasil Disimpan.!", "Jadwal Operasi Gagal Disimpan.!", "js");
    }
    
    //mahmud, clear -- tampil jenis tindakan operasi
    public function carijenistindakanoperasi(){echo json_encode($this->db->get('rs_jenistindakanoperasi')->result());}
    //mahmud, clear
    public function caribedoperasi()
    {
        $tanggal= "'".substr($this->input->post('waktu'),0,10)."'";
        $jam= "'".substr($this->input->post('waktu'),11,15)."'";
        echo json_encode($this->db->select("rb.idbed, concat('Bangsal:',rbs.namabangsal,' no.',rb.nobed) as bed, rjo.kodebooking")->join('rs_bangsal rbs','rbs.idbangsal=rb.idbangsal')->join('rs_jadwal_operasi rjo','rjo.idbed=rb.idbed and date(rjo.waktuoperasi) = '.$tanggal.' and time(rjo.waktuoperasi) = '.$jam.' and rjo.terlaksana="0"','left')->where('rb.idbangsal',$this->input->post('idbangsal'))->get('rs_bed rb')->result());
    }
    

    public function serchselect_klinikperujuk()
    {
        $cari = $this->input->post('cari');
        $sql = $this->db->like("namaklinik","$cari")->or_like("kodeklinik","$cari")->limit(20)->order_by('namaklinik','asc');
        $sql = $this->db->select("idklinik as id, concat(ifnull(kodeklinik,''),' ', ifnull(namaklinik,'') ) as text");
        $sql = $this->db->get("bpjs_klinikperujuk")->result_array();
        echo json_encode($sql);
    }

    public function serchselect_kelurahan()
    {
        $cari = $this->input->post('cari');
        $sql = $this->db->like("gd.namadesakelurahan","$cari")->or_like("gk.namakecamatan","$cari")->or_like("kab.namakabupatenkota","$cari")->limit(20);
        $sql = $this->db->select("gd.iddesakelurahan as id, concat(gd.namadesakelurahan,', ', gk.namakecamatan,', ',kab.namakabupatenkota) as text");
        $sql = $this->db->join("geografi_kecamatan gk","gk.idkecamatan=gd.idkecamatan");
        $sql = $this->db->join("geografi_kabupatenkota kab","kab.idkabupatenkota=gk.idkabupatenkota");
        $sql = $this->db->get("geografi_desakelurahan gd")->result_array();
        echo json_encode($sql);       
    }

    //setting tarif cetakkartu
    public function settarif_cetakkartu()
    {
        
        $post = $this->input->post();
        $spen = $this->db->query("select carabayar, idstatuskeluar from person_pendaftaran where idpendaftaran='".$post['idp']."'");
        $spas = $this->db->query("select ifnull(pp.cetakkartu,0) cetakkartu, ppas.norm, ifnull(ppeg.idperson,0) ispegawai from person_pasien ppas join person_person pp on pp.idperson=ppas.idperson left join person_pegawai ppeg on ppeg.idperson=pp.idperson where ppas.norm='".$post['norm']."'");
        //jika pasien  pegawai = Adm01  Cetak Kartu Karyawan
        //jika pasien mandiri dan bukan pegawai = Adm02   Cetak Kartu Baru atau Adm03   Cetak Kartu Lama
        //jika pasien bpjs tarif di kosongkan
        if( $spen->num_rows() > 0 && $spas->num_rows() > 0)
        {
            $pend = $spen->row_array();
            $pass = $spas->row_array();
            
            if( $pend['idstatuskeluar'] == 1 )
            {
                if(empty($pass['ispegawai'])) {
                    if($pend['carabayar']=='mandiri' && $pass['cetakkartu'] > 1){
                        $save = $this->cektarifcetakkartu('Adm03',$post['idp']);
                    }elseif($pend['carabayar']=='mandiri' && $pass['cetakkartu'] < 2){
                        $save = $this->cektarifcetakkartu('Adm02',$post['idp']);
                    }else{
                        $save = $this->cektarifcetakkartu('null',$post['idp']);
                    }
                }else{
                    if($pend['carabayar']=='mandiri'){
                        $save = $this->cektarifcetakkartu('Adm01',$post['idp']);
                    }else{
                        $save = $this->cektarifcetakkartu('null',$post['idp']);
                    }
                }
                if($save){
                    $respon=['status'=>'success','message'=>'Input Tarif Berhasil.'];
                }else{
                    $respon=['status'=>'danger','message'=>'Input Tarif Gagal.'];
                }
            }
            else
            {
                $respon=['status'=>'success','message'=>'Input Tarif Berhasil.'];
            }
        }
        else
        {
            $respon=['status'=>'danger','message'=>'Data pasien tidak ada'];
        }
        echo json_encode($respon);
    }

    private function cektarifcetakkartu($icd,$idp)
    {
        $cetakkartu = $this->db->select('icd')->get_where('rs_hasilpemeriksaan',['idpendaftaran'=>$idp,'icd'=>$icd])->num_rows();
        if($cetakkartu>0)
        {
            return true;
        }
        else
        {
            $dtmstrf = $this->mgenerikbap->select_multitable("*","rs_mastertarif",['icd'=>$icd, 'idkelas' => 1])->row_array();
            if( empty($dtmstrf) ) {
                $idjenistarif = 0;
                $jasaoperator = 0;
                $nakes=0;
                $jasars = 0;
                $bhp = 0;
                $akomodasi = 0;
                $margin = 0;
                $sewa=0;
                $total = 0; 
                $coapendapatan=0;    
            }else{
                $this->log_hasilpemeriksaan($idp,'input-kartu-',$icd);
                $idjenistarif = $dtmstrf['idjenistarif']; 
                $jasaoperator = $dtmstrf['jasaoperator']; 
                $jasars = $dtmstrf['jasars']; 
                $nakes = $dtmstrf['nakes']; 
                $bhp = $dtmstrf['bhp']; 
                $akomodasi = $dtmstrf['akomodasi']; 
                $margin = $dtmstrf['margin']; 
                $sewa = $dtmstrf['sewa']; 
                $total = $dtmstrf['total'];
                $coapendapatan = $dtmstrf['coapendapatan'];
            }
            $data = [
                'idpendaftaran'=>$idp,
                'icd'=>$icd,
                'idjenistarif'=>$idjenistarif,
                'jasaoperator'=>$jasaoperator,
                'nakes'=>$nakes,
                'jasars'=>$jasars,
                'bhp'=>$bhp,
                'akomodasi'=>$akomodasi,
                'margin'=>$margin,
                'sewa'=>$sewa,
                'total'=>$total,
                'coapendapatan'=>$coapendapatan
            ];
            $simpan = $this->db->insert('rs_hasilpemeriksaan',$data);
        }
        
        return $simpan;
    }
    
    public function reset_password_pasien()
    {
        $norm    = $this->input->post('norm');
        $tanggal = date('Ymd', strtotime($this->input->post('tanggal')));
        $arrMsg  = $this->resetuserapm($norm,$tanggal);
        pesan_success_danger($arrMsg, 'Reset Password Pasien Berhasil.', 'Reset Password Pasien Gagal.', 'js');
    }
    
    private function resetuserapm($norm,$date)
    {
        $tanggal = date('Ymd', strtotime($date));
        $result  = $this->db->replace('apm_login',['norm'=>$norm,'password'=>password_hash($tanggal, PASSWORD_DEFAULT)]);
        return $result;
    }
    
    
    //-- singkronisasi jadwal dokter ke website
    public function singkronisasijadwal()
    {
        
//        if ($this->pageaccessrightbap->checkAccessRight(V_SINGKRONISASIJADWAL)) //lihat define di atas
//        {
            $data['unit'] = $this->db->query("select idunit, namaunit, ifnull((select sum(1) from rs_jadwal rj where rj.idunit = ru.idunit and rj.singkronisasi = 0 and date(rj.tanggal) >= date(now()) ),0) as jumlah_jadwal, ifnull((select sum(1) from rs_jadwal rj where rj.idunit = ru.idunit and rj.singkronisasi = 1),0) as jumlah_singkron 
            from rs_unit ru 
            where ru.levelgudang = 'unit'")->result_array();
            $data['content_view']      = 'admission/v_singkronisasijadwal';
            $data['active_menu']       = 'admission';
            $data['active_sub_menu']   = 'pendaftaran_poli';
            $data['active_menu_level'] = '';
            $data['plugins']           = [PLUG_DATATABLE];
            $data['title_page']        = 'Singkronisasi Jadwal ke Website';
            $data['mode']              = 'view';
            $data['table_title']       = '';
            $data['script_js']         = ['js_singkronisasijadwal'];
            $this->load->view('v_index', $data);
//        }
//        else
//        {
//            aksesditolak();
//        }
        
    }
    
    public function singkronisasijadwal_uploadperunit()
    {
        $idunit = $this->input->post('idunit');
        $this->load->model('mviewql');
        $dtjadwal = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('upload_jadwal',$idunit);
        $uploadedJSON = json_encode(['datajadwal'=>$dtjadwal]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertjadwal',$uploadedJSON);
        
        //update singkronisasi jadwal jika berhasil
        if($data->metadata->code == '200')
        {
            foreach ($dtjadwal as $arr)
            {
                $this->db->update('rs_jadwal',['singkronisasi'=>1],['idjadwal'=>$arr['idjadwal'] ]);
            }
        }
        
        pesan_success_danger_ws($data->metadata->code,$data->metadata->message,'js');
    }
    
    public function singkronisasijadwal_hapusperunit()
    {
        $idunit = $this->input->post('idunit');
        $uploadedJSON = json_encode(['idunit'=>$idunit]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/hapusjadwal',$uploadedJSON);
        
        //update singkronisasi jadwal jika berhasil
        if($data->metadata->code == '200')
        {
            $this->db->query("update rs_jadwal set singkronisasi = '0' where idunit='".$idunit."' and singkronisasi='1' and date(tanggal) >= date(now()) ");
        }
        pesan_success_danger_ws($data->metadata->code,$data->metadata->message,'js');
    }
    
    public function get_kuotajadwalpemeriksaan()
    {
        $idjadwal = $this->input->post('idjadwal');
        $sql = "SELECT rj.quota, rj.quotawarning, (select namaloket from antrian_loket al WHERE al.idloket = rj.idloket ) as namaloket,ifnull((select max(rp.noantrian) from rs_pemeriksaan rp where rp.idjadwal = rj.idjadwal),0) as noantrian
                FROM rs_jadwal rj 
                WHERE rj.idjadwal = '".$idjadwal."'";
        $query = $this->db->query($sql)->row_array();
        
        if($query['noantrian'] >= $query['quotawarning'] )
        {
            $data['status'] = 1;
            $data['antrian_sisa']     = intval($query['quota']) - intval($query['noantrian']);
            $data['antrian_maksimal'] = $query['quota'];
            $data['antrian_terantrikan'] = $query['noantrian'];
            $data['namaloket']           = $query['namaloket'];
        }
        else
        {
            $data['status'] = 0;
        }
        echo json_encode($data);
        
    }
    
    public function hapuspemeriksaan()
    {
        $norm = $this->input->get('norm');
    
        $riwayat = $this->db->select('idpendaftaran')->get_where('person_pendaftaran',['norm'=>$norm])->result_array();
        foreach ($riwayat as $arr)
        {
            $this->db->delete("rs_inap_biayanonpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_inap_rencana_medis_barangpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_inap",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_barangpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_hasilpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_operasi",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_operasi_barangpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_operasi_biayanonpemeriksaan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_operasi_diagnosa",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_operasi_tarifoperasi",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("rs_jadwal_operasi",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("keu_tagihan",['idpendaftaran'=>$arr['idpendaftaran']]);
            $this->db->delete("person_pendaftaran",['idpendaftaran'=>$arr['idpendaftaran']]);
        }
    }
    
    public function riwayat_allpemeriksaan_pasien()
    {
        $norm = $this->input->post('norm');
        $tanggalperiksa = $this->input->post('tanggalperiksa');
        
        $whereidp = ((empty($tanggalperiksa)) ? "" : " and date(waktuperiksa) < '".$tanggalperiksa."' " );
        $response = [];
        
        //cari tanggal pendaftaran pasien
        $sql = "SELECT idpendaftaran, date(waktuperiksa) as waktuperiksa, date_format(waktuperiksa, '%d %M %Y') as tanggalperiksa, keteranganobat, saranradiologi,keteranganradiologi, keteranganlaboratorium, ifnull(keteranganekokardiografi,'') as keteranganekokardiografi
            FROM `person_pendaftaran` 
            WHERE norm = '".$norm."' ".$whereidp." and (idstatuskeluar = 1 or idstatuskeluar = 2 ) 
            ORDER by idpendaftaran desc limit 1";
        
        $query = $this->db->query($sql);
        
        if($query->num_rows() > 0)
        {
            $response['pendaftaran'] = $query->row_array();
            $idpendaftaran = $response['pendaftaran']['idpendaftaran'];
            //riwayat pasien rawat jalan
            $response['ralan_pemeriksaan'] = $this->db->query("select rp.idpemeriksaan, namadokter(rp.idpegawaidokter) as dokter_dpjp, namadokter(rp.iddokterpemeriksa) as dokter_pemeriksa, rp.idunit, rp.status, rp.catatanrujuk, rp.anamnesa, rp.diagnosa, rp.keterangan, rp.rekomendasi, ru.namaunit
            from rs_pemeriksaan rp
            join rs_unit ru on ru.idunit = rp.idunit
            WHERE idpendaftaran = '".$idpendaftaran."' ")->result_array();            
            
            $response['ralan_anamnesis'] = [];
            $response['ralan_planjutan'] = [];
            $response['ralan_diagnosis'] = [];
            $response['ralan_resep'] = [];
            $response['ralan_grup']  = [];
            $response['ralan_radiografi'] = [];
            $response['url_nas_simrs'] = URLNASSIMRS;
            
            foreach ($response['ralan_pemeriksaan'] as $arr) 
            {
                // list anamnesis
                $response['ralan_anamnesis'][] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_anamnesis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, rs_pemeriksaan rp where rh.idpendaftaran='".$idpendaftaran."' and rh.idpemeriksaan='".$arr['idpemeriksaan']."' and rj.jenisicd='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
                // list periksa lanjutan
                $response['ralan_planjutan'][]   = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, person_pendaftaran pp, rs_pemeriksaan rp where rh.idpendaftaran='".$idpendaftaran."' and rh.idpemeriksaan='".$arr['idpemeriksaan']."' and rj.jenisicd!='tindakan' and rj.jenisicd!='diagnosa' and rj.jenisicd!='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and pp.idpendaftaran = rh.idpendaftaran and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
                // list diagnosis
                $response['ralan_diagnosis'][] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ifnull(ri.satuan,'') ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpendaftaran='".$idpendaftaran."' and rh.idpemeriksaan='".$arr['idpemeriksaan']."' and rj.jenisicd!='radiologi' and rj.jenisicd!='vital sign' and rj.jenisicd!='laboratorium' and rj.jenisicd!='ECHOCARDIOGRAPHY' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
                // list pemakaian obat
                $response['ralan_resep'][] = $this->mkombin->pemeriksaan_listbhp_bypemeriksaan($arr['idpemeriksaan'],'rajal');
                $response['ralan_grup'][]  = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  FROM rs_barangpemeriksaan b WHERE b.idpemeriksaan='".$arr['idpemeriksaan']."' GROUP by grup")->result();
                $response['ralan_radiografi'][] = $this->db->query("SELECT idhasilpemeriksaan_efilm, idpendaftaran, efilm FROM `rs_hasilpemeriksaan_efilm` WHERE idpemeriksaan = '".$arr['idpemeriksaan']."'")->result();
            }
            $response['status']  = 'success';
            
            //riwayat pasien rawat inap
            $dtranap = $this->db->select("idinap, waktumasuk, waktukeluar, status ")->get_where('rs_inap',['status !='=>'batal','status !='=>'menunggu','status !='=>'pesan','idpendaftaran'=>$idpendaftaran]);
            
            $response['ranap_tindakan']    = [];
            $response['ranap_anamnesa']    = [];
            $response['ranap_lanjutan']    = [];
            $response['ranap_bhp']         = [];
            $response['ranap_radiografi']  = [];
            $response['ranap_catatanterintegrasi'] = [];
            $response['ranap_pemeriksaan'] = 'datakosong';
            
            if($dtranap->num_rows() > 0)
            {
                $response['ranap_pemeriksaan'] = $dtranap->row_array();
                $response['ranap_rencana']     = $this->mkombin->get_rencanamedisranap($response['ranap_pemeriksaan']['idinap']);
                
                foreach ( $response['ranap_rencana'] as $arr )
                {
                    $response['ranap_lanjutan'][] = $this->db
                            ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                            ->join('rs_icd b','b.icd=a.icd')
                            ->group_start()->where('b.idjenisicd',4)->or_where('b.idjenisicd',5)->group_end()
                            ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                            ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null])
                            ->result();
                    $response['ranap_tindakan'][] = $this->db
                            ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                            ->join('rs_icd b','b.icd=a.icd')
                            ->group_start()->where('b.idjenisicd',2)->or_where('b.idjenisicd',3)->group_end()
                            ->order_by('b.idjenisicd','asc')
                            ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                            ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null])
                            ->result();
                    $response['ranap_anamnesa'][] = $this->db
                            ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                            ->join('rs_icd b','b.icd=a.icd')
                            ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                            ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null,'b.idjenisicd'=>1])
                            ->result();
                    $response['ranap_bhp'][] = $this->db
                            ->select("concat (ifnull(c.namabarang,''),' <b>', ifnull(a.jumlah,0),'</b> ',ifnull(d.namasatuan,'')) as hasil")
                            ->join('rs_barang c','c.idbarang=a.idbarang')
                            ->join('rs_satuan d','d.idsatuan=c.idsatuan')
                            ->get_where('rs_inap_rencana_medis_barangpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana'])
                            ->result();
                    $response['ranap_radiografi'][] = $this->db
                            ->select('idhasilpemeriksaan_efilm,idpendaftaran,efilm,idrencanamedispemeriksaan')
                            ->get_where('rs_inap_hasilpemeriksaan_efilm',['idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan']])
                            ->result();    
                    $response['ranap_catatanterintegrasi'][]= $this->mkombin->get_catatanterintegrasiranap(['id'=>$arr['idrencanamedispemeriksaan']]);
                }
            }
            
            //riwayat operasi
            $dtoperasi = $this->db->query("SELECT ro.*, ppas.norm,pper.namalengkap, pper.nik, date_format(pper.tanggallahir,'%d/%m/%Y') as tanggallahir,
                    if(ro.resikotinggi=0,'Tidak','Ya') as resikotinggi, 0 as lamaoperasi, 
                    namadokter(rjo.iddokteroperator) as dokteroperator, 
                    namadokter(ro.idpegawaiasisten) as asistenbedah, 
                    ifnull(namadokter(ro.idpegawaiinstrumen),'') as instrumen,
                    (select macamoperasi from rs_operasi_macamoperasi rom where rom.idmacamoperasi = ro.idmacamoperasi) as macamoperasi
                        from rs_operasi ro
                        join rs_jadwal_operasi rjo on rjo.idjadwaloperasi = ro.idjadwaloperasi
                        join person_pendaftaran ppd on ppd.idpendaftaran = ro.idpendaftaran
                        join person_pasien ppas on ppas.norm = ppd.norm
                        join person_person pper on pper.idperson = ppas.idperson
                        where ro.idpendaftaran = '".$idpendaftaran."' and ro.statusoperasi='selesai' ");
            
            $response['operasi']  = 'datakosong';
            $response['operasi_jenisanestesi'] = [];
            $response['operasi_jenisoperasi']  = [];
            
            if($dtoperasi->num_rows() > 0)
            {
                $response['operasi']  = $dtoperasi->row_array();
                $response['operasi_jenisanestesi'] = ((empty($response['operasi']['jenisanestesi'])) ? '' : $this->db->query("select group_concat(jenisanestesi SEPARATOR ', ') as jenisanestesi from rs_jenisanestesi rj where rj.idjenisanestesi in (".$response['operasi']['jenisanestesi'].")")->row_array());
                $response['operasi_jenisoperasi']  = ((empty($response['operasi']['jenisoperasi'])) ? '' : $this->db->query("select group_concat(jenisoperasi SEPARATOR ', ') as jenisoperasi from rs_jenisoperasi rj where rj.idjenisoperasi in (".$response['operasi']['jenisoperasi'].")")->row_array());
            }
        }
        else
        {
            $response['status']  = 'info';
            $response['message'] = 'Riwayat Tidak Ditemukan';
        }
        
        echo json_encode($response);
    }
    
    public function lembar_pengkajian_ulang_status_jatuh()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');

        $data['dtpasien'] = $this->mkombin->get_identitaspasien($idpendaftaran);

        $data['dtpengkajianulangrisikojatuh'] = $this->db->query("SELECT * FROM `rs_asesmen_ulang_risiko_jatuh` 
        INNER JOIN rs_inap on rs_inap.idinap = rs_asesmen_ulang_risiko_jatuh.idinap
        INNER JOIN person_pendaftaran on person_pendaftaran.idpendaftaran = rs_inap.idpendaftaran
        WHERE person_pendaftaran.idpendaftaran=" . $idpendaftaran)->result_array();

        if (empty($data['dtpengkajianulangrisikojatuh'])) {
            $data['result'] = "failed";
            $data['msg'] = "Data Belum Diisikan!";

            echo json_encode($data);
        } else {
            $data['result']  = "success";

            $itempengkajianrisikojatuh = [
                "Humpty Dumpty",
                "Morse Fall Scale",
                "Ontario Modified Stratify Sidney Scoring"
            ];


            $iteminterpretasihasil = [
                "null",
                "Tidak Risiko",
                "Risiko Sedang (RS)",
                "Risiko Tinggi (RT)"
            ];

            $itemintervensirisikojatuh =
                [
                    "Pastikan pasien terpasang gelang kuning",
                    "Pastikan diorientasikan kondisi lingkungan / ruangan",
                    "Pengecekan 'BEL' mudah dijangkau",
                    "Naikkan Pagar pengaman tempat tidur",
                    "Handrail mudah dijangkau dan kokoh",
                    "Roda tempat tidur berada pada posisi terkunci",
                    "Pastikan tanda pasien risiko jatuh terpasang pada atas tempat tidur pasien",
                    "Pastikan lampu menyala pada saat malam hari",
                    "Berikan edukasi pencegahan jatuh pada pasien / keluarga",
                    "Lakukan pengkajian ulang setiap 1X24 jam"
                ];

            $itemintervensirisikotinggi =
                [
                    "Lakukan semua pencegahan jatuh risiko sedang",
                    "Libatkan keluarga untuk selalu menunggu dan mengawasi pasien",
                    "Beri penjelasan ulang tentang pencegahan jatuh untuk penunggu yang berbeda",
                    "Lakukan pengkajian ulang setiap 1X24 jam"
                ];

            $resultinterpretasihasil = "";
            foreach ($data['dtpengkajianulangrisikojatuh'] as $item) {
                if ($item['interpretasihasilhumptydumpty'] != NULL) {
                    $resultinterpretasihasil = $item['interpretasihasilhumptydumpty'];
                } else if ($item['intepretasihasilmfs'] != NULL) {
                    $resultinterpretasihasil = $item['intepretasihasilmfs'];
                } else if ($item['intepretasihasilomss'] != NULL) {
                    $resultinterpretasihasil = $item['intepretasihasilomss'];
                }
            }

            $data['pengkajianrisikojatuh'] = $iteminterpretasihasil[$resultinterpretasihasil];
            echo json_encode($data);
        }
    }

    public function lembar_pengkajian_ulang_status_fungsional()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');

        $data['dtpasien'] = $this->mkombin->get_identitaspasien($idpendaftaran);

        $data['dtpengkajianstatusfungsional'] = $this->db->query("SELECT * FROM `rs_asesmen_ulang_status_fungsional` 
        INNER JOIN rs_inap on rs_inap.idinap = rs_asesmen_ulang_status_fungsional.idinap
        INNER JOIN person_pendaftaran on person_pendaftaran.idpendaftaran = rs_inap.idpendaftaran
        WHERE person_pendaftaran.idpendaftaran=" . $idpendaftaran)->result_array();

        if (empty($data['dtpengkajianstatusfungsional'])) {
            $data['result'] = "failed";
            $data['msg'] = "Data Belum Diisikan!";
        } else {
            $data['result'] = "success";

            $iteminterpretasihasil = [
                "Mandiri",
                "Ketergantungan ringan",
                "Ketergantungan sedang",
                "Ketergantungan berat",
                "Ketergantungan total"
            ];

            $itemintervensistatusfungsional = [
                "Edukasi keluarga dan libatkan keluarga pasien dalam membantu pasien melakukan aktivitas sehari - hari",
                "Konsultasikan pada DPJP jika skor pada indeks Barthel ≤ 8",
                "Tidak ada intervensi",
                "Lainnya"
            ];

            foreach ($data['dtpengkajianstatusfungsional'] as $item) {
                $data['interpretasihasilib'] = $iteminterpretasihasil[$item['interpretasihasilib']];
                $data['txtisflainnya'] = $item['txtisflainnya'] != null ? $item['txtisflainnya']  : NULL;
                $data['isflainnya'] = $item['isflainnya'] != null ? $itemintervensistatusfungsional[$item['isflainnya']]  : NULL;
                $data['txtrencanapengkajianulang'] = $item['txtrencanapengkajianulang'] != null ?  $item['txtrencanapengkajianulang']  : null;
            }
        }
        echo json_encode($data);
    }

    public function lembar_pengkajian_ulang_nyeri()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $data['dtpasien'] = $this->mkombin->get_identitaspasien($idpendaftaran);

        $data['dtpengkajiannyeri'] = $this->db->query("SELECT * FROM `rs_asesmen_ulang_nyeri` 
        INNER JOIN rs_inap on rs_inap.idinap = rs_asesmen_ulang_nyeri.idinap
        INNER JOIN person_pendaftaran on person_pendaftaran.idpendaftaran = rs_inap.idpendaftaran WHERE person_pendaftaran.idpendaftaran=" . $idpendaftaran)->result_array();

        if (empty($data['dtpengkajiannyeri'])) 
        {
            $data['result'] = "failed";
            $data['msg'] = "Data Belum Diisikan!";
        }
        else 
        {
            $data['result'] = "success";
        }

        echo json_encode($data);
    }
    
}
/* End of file ${TM_FILENAME:${1/(.+)/Cadmission.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cadmission/:application/controllers/${1/(.+)/Cadmission.php/}} */






