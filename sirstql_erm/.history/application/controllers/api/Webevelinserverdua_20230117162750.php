<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Webevelinserverdua extends REST_Controller {
    private $Xconsid;
    private $Xconssecret;
    private $Xtimestamp;
    private $Xjeniskoneksi;
    private $Xsignature;
    
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function decodebase64()
    {
        $decode = base64_encode(hash_hmac('sha256',$this->Xconsid.'&'.$this->Xtimestamp,$this->Xconssecret, true));
        return ($decode==$this->Xsignature) ? true : false ;
    }

    private function cek_signature()
    {
        $headers = apache_request_headers();
        $this->Xconsid    = $headers['X-Cons-Id'];
        $this->Xtimestamp = $headers['X-Timestamp'];
        $this->Xsignature = $headers['X-Signature'];
        $this->Xjeniskoneksi = $headers['X-Jeniskoneksi'];

        if($this->Xsignature)
        {
            switch ($this->Xjeniskoneksi) {
                case 'EVELINQLYK': $this->Xconssecret='evelinQL'; break;
            }
            return ($this->decodebase64()) ? true : false ;
        }
        return false;
        
    }
    /**
     * Get Post 
     * @param type $id
     * @return type
     */
    private function get_post($id)
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }

    private function get_data(){
        $postdata = file_get_contents("php://input");
        $response = json_decode( $postdata );
        return $response; // stdobject
    }
    
    public function vclaimlist_post()
    {
        if($this->cek_signature()){ 
            
            $tgl_post  = $this->get_data()->tglperiksa;
            $jns_post = $this->get_data()->jnslayanan;

            $tgl_periksa = sql_clean($tgl_post);
            
            $jns_pemeriksaan = empty( $jns_post) ? 'rajal' : sql_clean( $jns_post);

            
            $query = $this->db->select("ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
            $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
            $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
            $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
            $query = $this->db->where('ppd.carabayar != "mandiri" ');
            $query = $this->db->where('ppd.carabayar != "asuransi lain" ');
            $query = $this->db->where('ppd.idstatuskeluar != "3" ');
            
            $query = $this->db->where('date(ppd.waktuperiksa) = "'.$tgl_periksa.'"');
            
            $query = $this->db->where('ppd.jenispemeriksaan = "'.$jns_pemeriksaan.'" ');

            $query = $this->db->group_by('ppd.idpendaftaran');
            $query = $this->db->get('person_pendaftaran ppd');

            $response = $query->result();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }
    }

    public function vclaimlistselesai_post()
    {
        if($this->cek_signature()){  

            $tgl_post  = $this->get_data()->tglperiksa;
            $jns_post = $this->get_data()->jnslayanan;

            $tgl_periksa = sql_clean($tgl_post);
            
            $jns_pemeriksaan = empty($jns_post) ? 'rajal' : sql_clean($jns_post);
                    
            $query = $this->db->select("ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
            $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
            $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
            $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
            $query = $this->db->where('ppd.carabayar != "mandiri" ');
            $query = $this->db->where('ppd.carabayar != "asuransi lain" ');
            
            $query = $this->db->where('ppd.idstatuskeluar = "2" ');
            
            $query = $this->db->where('date(ppd.waktuperiksa) = "'.$tgl_periksa.'"');
            
            $query = $this->db->where('ppd.jenispemeriksaan = "'.$jns_pemeriksaan.'" ');

            $query = $this->db->group_by('ppd.idpendaftaran');
            $query = $this->db->get('person_pendaftaran ppd');

            $response = $query->result();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }
    }

    public function vclaimsep_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            $idunit_post = $this->get_data()->idunit;
            
            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);
            $idunit     = sql_clean($idunit_post);

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);

            if( !empty($query->nosep) ){
                $nosep = str_replace(" ","",$query->nosep);
            }else{
                $nosep = '';
            }

            if( empty($nosep) ){
                $data_api = ['metaData'=>['code'=>404,'message'=>'No Sep Belum Di input']];
            }else{
                $api_sep   = $this->bpjsbap->get_vclaim_sep($nosep);
                if( $api_sep['metaData']->code == '200' ){
                    $norujukan      = $api_sep['response']->noRujukan; 
                    $api_rujukan    = $this->bpjsbap->get_vclaim_rujukan($norujukan);
                    $data_api = ['sep'=>$api_sep,'rujukan'=>$api_rujukan];
                }else{
                    $data_api = $api_sep;
                }
                
            }

            $response =  $data_api;
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);


        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function vclaimresume_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            $idunit_post = $this->get_data()->idunit;

            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);
            $idunit     = sql_clean($idunit_post);

            $query          = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            $idpendaftaran  = $query->idpendaftaran;
            $jenisrujukan  = $query->jenisrujukan;
            
            $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $vitalsign      = $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $tindakan       = $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $laboratorium   = $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $radiologi      = $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $obat           = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
            $grup           = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  
                                                FROM rs_barangpemeriksaan b 
                                                WHERE b.idpendaftaran = '".$idpendaftaran."' GROUP by grup")->result();
            $diagnosa       =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
            
            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $sip = $this->db->select('sip')->from('person_pegawai')->where('idpegawai',$identitas['idpegawai'])->get()->row_array();
            $identitas['sip_dokter'] = $sip['sip'];
    
            $response = [
                'datapendaftaran' => $query,
                'identitas'=>$identitas,
                'vitalsign'=>$vitalsign,
                'tindakan'=>$tindakan,
                'laboratorium'=>$laboratorium,
                'radiologi'=>$radiologi,
                'obat'=>$obat,
                'diagnosa'=>$diagnosa,
                'ketradiologi'=>$ketradiologi,
                'grup'=>$grup,
                'jenisrujukan'=>$jenisrujukan
            ];

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{

            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }

    }

    /**
     * TETSING nota
     */
    public function testnota_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            $idunit_post = $this->get_data()->idunit;

            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);
            $idunit = sql_clean($idunit_post);

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            
            $idpendaftaran     = $query->idpendaftaran;
            // $jenispemeriksaan  = $query->jenispemeriksaan;
            $jenispemeriksaan  = 'rajal';

            // if( $jenispemeriksaan == 'rajalnap' ){
                // $query_nota = $this->mevelin->vlcaim_cetakversiranap($idpendaftaran,$jenispemeriksaan);
            // }else{
            $query_nota = $this->mevelin->vclaim_cetaklangsung($idpendaftaran,$jenispemeriksaan);
            // }
            
            $response = $query_nota;

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function vclaimresumesuratkontrol_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;

            $norm               = sql_clean($norm_post);
            $tglperiksasekarang = sql_clean($tgl_post);
            
            $query_sekarang     = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksasekarang);

            $jenisrujukan       = $query_sekarang->jenisrujukan;

            // $cek_riwayat_pemeriksaan = $this->db->query("SELECT * 
            //                                              FROM `person_pendaftaran` 
            //                                              WHERE `norm` = $norm 
            //                                              AND `carabayar` != 'mandiri'
            //                                              AND `idunit` = $query_sekarang->idunit 
            //                                             --  AND `waktuperiksa` <= $query_sekarang->waktuperiksa
            //                                              ORDER BY `waktuperiksa` 
            //                                              DESC LIMIT 2 ")->result();

            $this->db->select('*');
            $this->db->from('person_pendaftaran');
            $this->db->where('norm',$norm);
            $this->db->where('carabayar !=', 'mandiri');
            $this->db->where('idunit',$query_sekarang->idunit);
            $this->db->where('idstatuskeluar',2);
            $this->db->where('statustagihan !=','terbuka');
            $this->db->where('date(waktuperiksa) <=',$tglperiksasekarang);
            $this->db->order_by('waktuperiksa', 'DESC');
            $this->db->limit(2);

            $cek_riwayat_pemeriksaan = $this->db->get()->result();

            $riwayat_terakhir = $cek_riwayat_pemeriksaan[1];
            $tglperiksa       = format_date($riwayat_terakhir->waktuperiksa,"Y-m-d"); 

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa);

            $idpendaftaran  = $query->idpendaftaran;

            $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $vitalsign      = $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $tindakan       = $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');            
            $laboratorium   = $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $radiologi      = $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $obat           = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
            $grup           = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  
                                                FROM rs_barangpemeriksaan b 
                                                WHERE b.idpendaftaran = '".$idpendaftaran."' GROUP by grup")->result();
            $diagnosa       =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
            
            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $sip = $this->db->select('sip')->from('person_pegawai')->where('idpegawai',$identitas['idpegawai'])->get()->row_array();
            $identitas['sip_dokter'] = $sip['sip'];
    
            $response = [
                'datapendaftaran' => $query,
                'identitas'=>$identitas,
                'vitalsign'=>$vitalsign,
                'tindakan'=>$tindakan,
                'laboratorium'=>$laboratorium,
                'radiologi'=>$radiologi,
                'obat'=>$obat,
                'diagnosa'=>$diagnosa,
                'ketradiologi'=>$ketradiologi,
                'grup'=>$grup,
                'jenisrujukan'=>$jenisrujukan
            ];

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{

            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }

    }

    /**
     * TESTING SURAT KONTROL
     */
    public function testvclaimresumesuratkontrol_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            
            $norm               = sql_clean($norm_post);
            $tglperiksasekarang = sql_clean($tgl_post);
            

            $query_sekarang     = $this->mevelin->testget_pendaftaran_bynorm_periksa($norm,$tglperiksasekarang);
            $response           = $query_sekarang;
            // // $postdata = file_get_contents("php://input");
            // // $response = json_decode( $postdata )->norm;
            // // $norm = $this->get_data()->norm
            // // $response = $query_sekarangs;
            // // $response = $json;
            // $jenisrujukan       = $query_sekarang->jenisrujukan;

            
            // $this->db->select('*');
            // $this->db->from('person_pendaftaran');
            // $this->db->where('norm',$norm);
            // $this->db->where('carabayar !=', 'mandiri');
            // $this->db->where('idunit',$query_sekarang->idunit);
            // $this->db->where('idstatuskeluar',2);
            // $this->db->where('date(waktuperiksa) <=',$tglperiksasekarang);
            // $this->db->order_by('waktuperiksa', 'DESC');
            // $this->db->limit(2);

            // $cek_riwayat_pemeriksaan = $this->db->get()->result();
            

            // $riwayat_terakhir = $cek_riwayat_pemeriksaan[1];
            // $tglperiksa       = format_date($riwayat_terakhir->waktuperiksa,"Y-m-d"); 

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa);

            // $idpendaftaran  = $query->idpendaftaran;

            // $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            // $vitalsign      = $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
            // $tindakan       = $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');            
            // $laboratorium   = $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
            // $radiologi      = $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
            // $obat           = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
            // $grup           = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  
            //                                     FROM rs_barangpemeriksaan b 
            //                                     WHERE b.idpendaftaran = '".$idpendaftaran."' GROUP by grup")->result();
            // $diagnosa       =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
            
            // $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            // $ketradiologi   = [];
            
            // if(empty(!$getket)){

            //     $kkr='';$ks='';$kk='';$ka='';$kd='';
            //     foreach ($getket as $obj) {
            //         $kkr = $obj->keteranganradiologi;
            //         $ks  = $obj->saranradiologi;
            //         $kk .= $obj->keterangan;
            //         $ka .= $obj->anamnesa;
            //         $kd .= $obj->diagnosa; 
            //     }

            //     $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            // }
    
            // $response = [
            //     'datapendaftaran' => $query,
            //     'identitas'=>$identitas,
            //     'vitalsign'=>$vitalsign,
            //     'tindakan'=>$tindakan,
            //     'laboratorium'=>$laboratorium,
            //     'radiologi'=>$radiologi,
            //     'obat'=>$obat,
            //     'diagnosa'=>$diagnosa,
            //     'ketradiologi'=>$ketradiologi,
            //     'grup'=>$grup,
            //     'jenisrujukan'=>$jenisrujukan
            // ];

            // $response = $response;

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{

            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }

    }


    public function vclaimrencanakontrol_post()
    {
        if( $this->cek_signature() ){
            $this->load->model('mevelin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            
            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa);

            $nosep = str_replace(" ","",$query->nosep);

            if( empty($nosep) ){
                $data_api = ['metaData'=>['code'=>404,'message'=>'No Sep Belum Di input']];
            }else{
                $api_sep   = $this->bpjsbap->get_vclaim_sep($nosep);
                if( $api_sep['metaData']->code == '200' ){
                    $nosurat        = $api_sep['response']->kontrol->noSurat; 
                    $api_rencanakontrol  = $this->bpjsbap->get_vclaim_rencanakontrol($nosurat);
                    
                    $data_api       = $api_rencanakontrol ;
                }else{
                    $data_api = $api_sep;
                }
                
            }

            $this->response(["metadata" =>  [ 'response'=>$data_api, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function vclaimradiologi_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            $idunit_post = $this->get_data()->idunit;

            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);
            $idunit     = sql_clean($idunit_post);

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);


            $idpendaftaran  = $query->idpendaftaran;
            $idunit         = $query->idunit;

             /**
             * cek nosep kosong
             */
            // $this->db->select('idpendaftaran'); 
            // $this->db->from('person_pendaftaran');
            // $this->db->where('idunit',$idunit);
            // $this->db->where('norm',$norm);
            // $this->db->where('date(waktuperiksa)',$tglperiksa);
            // $this->db->group_start();
            //     $this->db->where('nosep =','');
            //     $this->db->or_where('nosep','0');
            // $this->db->group_end();
            // $cek_pendafataran = $this->db->get()->row();
            
            // if(!empty( $cek_pendafataran )){
            //     $idpendaftaran = $cek_pendafataran->idpendaftaran;
            // }


            $data = [];
            $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $data['elektromedik']     = $this->mviewql->viewhasilpemeriksaan(5,$idunit,$idpendaftaran,'verifikasi');
            $data['diagnosa']         = $this->mviewql->viewhasilpemeriksaan(2,$idunit,$idpendaftaran,'verifikasi');
            $data['dokterpenilai']    = $this->mkombin->getKonfigurasi('drpenilairo');
            $data['sipdokterpenilai'] = $this->mkombin->getKonfigurasi('sippenilairo');
            $data['header']           = $this->mkombin->getKonfigurasi('headerro');

            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $data['ketradiologi'] = $ketradiologi;
            
            $this->response(["metadata" =>  [ 'response'=>$data, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function vclaimnota_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');

            $norm_post = $this->get_data()->norm;
            $tgl_post  = $this->get_data()->tglperiksa;
            $idunit_post = $this->get_data()->idunit;

            $norm       = sql_clean($norm_post);
            $tglperiksa = sql_clean($tgl_post);
            $idunit     = sql_clean($idunit_post);

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            
            $idpendaftaran     = $query->idpendaftaran;

             /**
             * cek nosep kosong
             */
            // $this->db->select('idpendaftaran'); 
            // $this->db->from('person_pendaftaran');
            // $this->db->where('idunit',$idunit);
            // $this->db->where('norm',$norm);
            // $this->db->where('date(waktuperiksa)',$tglperiksa);
            // $this->db->group_start();
            //     $this->db->where('nosep =','');
            //     $this->db->or_where('nosep','0');
            // $this->db->group_end();
            // $cek_pendafataran = $this->db->get()->row();
            
            // if(!empty( $cek_pendafataran )){
            //     $idpendaftaran = $cek_pendafataran->idpendaftaran;
            // }

            // $jenispemeriksaan  = $query->jenispemeriksaan;
            $jenispemeriksaan  = 'rajal';

            // if( $jenispemeriksaan == 'rajalnap' ){
                // $query_nota = $this->mevelin->vlcaim_cetakversiranap($idpendaftaran,$jenispemeriksaan);
            // }else{
            $query_nota = $this->mevelin->vclaim_cetaklangsung($idpendaftaran,$jenispemeriksaan);
            // }
            
            $response = $query_nota;

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function vclaimdatapoli_post(){
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            
            $query_unit = $this->mevelin->get_poli();

            $response   = $query_unit;
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    
    
}