<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Creport extends MY_controller {

    function __construct()
    {
       parent::__construct();
       // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
       $this->load->helper('numbers');
       $this->load->model('mviewql');
       $this->load->model('mkombin');
    }
    ///////////////////////Laporan Log///////////////////
    //-------------------------- vv Standar
    public function settinglaporan()
    {
        return [    'active_menu'       => 'report',
                    'active_menu_level' => '',
                    'script_js'  => ['js_datatable']
               ];
    }
    
    public function logpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOG)) //lihat define di atas
        {
            $data['active_menu']        = 'report';
            $data['active_menu_level']  = '';
            $data['script_js']          = ['log/js_log_pemeriksaan'];
            $data['content_view']       = 'report/v_laporan';
            $data['active_sub_menu']    = 'logpemeriksaan';
            $data['title_page']         = 'Log User';
            $data['table_title']        = 'Laporan Log User';
            $data['plugins']            = [ PLUG_DATATABLE , PLUG_DATE];
            $data['mode']               = 'viewlogperiksa';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function dt_logpemeriksaan()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_logpemeriksaan_(true);
        $data=[]; $no=0;
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->norm;
                    $row[] = $obj->waktuinput;
                    $row[] = $obj->url;
                    $row[] = $obj->deskripsi.' - icd:'.$obj->icd.' - Obat/Bhp:'.$obj->kode.'-'.$obj->namabarang;
                    $row[] = $obj->namauser;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_logpemeriksaan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_logpemeriksaan(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function log()
    {
    if ($this->pageaccessrightbap->checkAccessRight(V_LOG)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'log';
            $data['title_page']  = 'Log User';
            $data['table_title'] = 'Laporan Log User';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['jsmode']      = 'datalog';
            $data['mode']        = 'viewlog';            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function load_reportdatapasien()
        {
            if ($this->pageaccessrightbap->checkAccessRight(V_LDPASIEN)) //lihat define di atas
            {
                $this->load->model('mdatatable');
                $table="person_pasien";
                $order = array('norm' => 'desc'); // default order 
                $column_order = array('norm','nojkn','nik','namalengkap','tempatlahir'); //field yang ditampilkan
                $column_search = array('norm','nojkn','nik','namalengkap','tempatlahir'); //field  untuk pencarian 
                $arrQ_builder='person_pasien'; // di isi master yg diload ex: icd
                $list = $this->mdatatable->get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
                $data = array();
                $no = $_POST['start'];
                foreach ($list as $field) {
                    $row = array();
                        $row[] = $field->norm;
                        $row[] = $field->nojkn;
                        $row[] = $field->nik;
                        $row[] = $field->namalengkap;
                        $row[] = $field->tempatlahir;
                        $row[] = $field->tanggallahir;
                        $row[] = $field->jeniskelamin;
                        $row[] = $field->agama;
                        $row[] = $field->statusmenikah;
                        $row[] = $field->golongandarah;
                        $row[] = $field->rh;
                        $row[] = $field->namapendidikan;
                        $row[] = $field->namapekerjaan;
                        $row[] = $field->alamat;
                    $data[] = $row;
                }
                $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->mdatatable->count_all($order,$column_order,$column_search,$arrQ_builder,$table),
                    "recordsFiltered" => $this->mdatatable->count_filtered($order,$column_order,$column_search,$arrQ_builder,$table),
                    "data" => $data,
                );
                //output dalam format JSON

                echo json_encode($output);
            }
            else
            {
                aksesditolak();
            }
        }
        
    public function waktulayanan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_WAKTULAYANAN)) //lihat define di atas
        {
            $data                    = $this->settinglaporan();
            $data['content_view']    = 'report/v_waktulayanan';
            $data['active_sub_menu'] = 'waktulayanan';
            $data['title_page']      = 'Log Waktu Layanan';
            $data['plugins']         = [ PLUG_DATATABLE , PLUG_DATE, PLUG_DROPDOWN];
            $data['script_js']       = ['js_waktulayanan'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dt_waktulayanan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_WAKTULAYANAN)) //lihat define di atas
        {
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_waktulayanan_(true);
            $data=[]; $no=$_POST['start'];
            if($getData)
            {
                foreach ($getData->result() as $field) {
                    $row = array();
                        $row[] = $field->norm;
                        $row[] = $field->namapasien;
                        $row[] = $field->tanggalperiksa;
                        $row[] = $field->namaunit;
                        $row[] = '<ol>'.str_replace(',', '', $field->detail).'</ol>';
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_waktulayanan(),
                "recordsFiltered" => $this->mdatatable->filter_dt_waktulayanan(),
                "data" => $data,
            );
            //output dalam format JSON

            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function pemakaianobat()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPOBAT)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pemakaianobat';
            $data['title_page']  = 'Pemakaian Obat';
            $data['table_title'] = 'Laporan Pemakaian Obat';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'viewpobat';
            $data['mode']        = 'viewpobat';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function load_reportdataobat()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPOBAT)) //lihat define di atas
        {
            $this->load->model('mdatatable');
            $table="rs_barangpemeriksaan";
            $order = array('idbarangpemeriksaan' => 'desc'); // default order 
            $column_order = array('waktu','namabarang','kode','jenistarif','namasatuan'); //field yang ditampilkan
            $column_search = array('waktu','namabarang','kode','jenistarif','namasatuan'); //field  untuk pencarian 
            $arrQ_builder='rs_barangpemeriksaan'; // di isi master yg diload ex: icd
            $list = $this->mdatatable->get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $field) {
                $row = array();
                    $row[] = $field->waktu;
                    $row[] = $field->namabarang;
                    $row[] = $field->kode;
                    $row[] = $field->jenistarif;
                    $row[] = $field->namasatuan;
                    $row[] = $field->jumlahpemakaian;
                    $row[] = $field->harga;
                    $row[] = $field->total;
                    $row[] = $field->kekuatan;
                $data[] = $row;
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->count_all($order,$column_order,$column_search,$arrQ_builder,$table),
                "recordsFiltered" => $this->mdatatable->count_filtered($order,$column_order,$column_search,$arrQ_builder,$table),
                "data" => $data,
            );
            //output dalam format JSON

            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    public function get_lobat_rangedate()
    {
        echo json_encode($this->db->query("SELECT `person_pendaftaran`.`waktu`, `rs_barang`.`namabarang`, `rs_barang`.`kode`, `rs_satuan`.`namasatuan`, `rs_jenistarif`.`jenistarif`, `rs_barangpemeriksaan`.`jumlahpemakaian`, `rs_barangpemeriksaan`.`harga`, `rs_barangpemeriksaan`.`total`, `rs_barangpemeriksaan`.`kekuatan`, `rs_barangpemeriksaan`.`idbarangpemeriksaan` FROM `rs_barangpemeriksaan` LEFT JOIN `rs_barang` ON `rs_barang`.`idbarang` = `rs_barangpemeriksaan`.`idbarang` LEFT JOIN `person_pendaftaran` ON `person_pendaftaran`.`idpendaftaran` = `rs_barangpemeriksaan`.`idpendaftaran` LEFT JOIN `rs_jenistarif` ON `rs_jenistarif`.`idjenistarif` = `rs_barang`.`idjenistarif` LEFT JOIN `rs_satuan` ON `rs_satuan`.`idsatuan` = `rs_barang`.`idsatuan` WHERE  date(waktu) >='".$this->input->post('awal')."' AND date(waktu) <='".$this->input->post('akhir')."'  ORDER BY `idbarangpemeriksaan` DESC")->result());
    }

    public function load_reportlog()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOG)) //lihat define di atas
        {
            $this->load->model('mdatatable');
            $table="login_log";
            $order = array('idlog' => 'desc'); // default order 
            $column_order = array(null, 'url','log','expired','waktu'); //field yang ditampilkan
            $column_search = array('url','log','expired','waktu'); //field  untuk pencarian 
            $arrQ_builder=''; // di isi master yg diload ex: icd
            $list = $this->mdatatable->get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $field) {
                $row = array();
                    $row[] = $field->url;
                    $row[] = $field->log;
                    $row[] = $field->expired;
                    $row[] = $field->waktu;
                $data[] = $row;
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->count_all($order,$column_order,$column_search,$arrQ_builder,$table),
                "recordsFiltered" => $this->mdatatable->count_filtered($order,$column_order,$column_search,$arrQ_builder,$table),
                "data" => $data,
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    // PDF REPORT
    public function download_pdfkasir($id)
    {  
        $data['infotagihan'] = $this->mviewql->viewinfotagihan($id);
        $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($id,0);
        $this->load->view('report/pdf_report',$data);
    }
    
    // EXCEL REPORT
    // public function excel_inventaris()
    // {
    //     $data['titel']= "databarang".date('Y-m-d');
    //     $data['obat'] = $this->db->query("SELECT rb.namabarang, rb.kode,ifnull(rbs.stok,0) stok, rbp.batchno, rbp.kadaluarsa, rbp.hargabeli, rb.hargajual, rb.kekuatan,rb.jenis, rj.jenistarif, rs.namasatuan, rsd.namasediaan FROM rs_barang rb left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok rbs on rbs.idbarangpembelian=rbp.idbarangpembelian and rbs.idunit='".$this->session->userdata('idunitterpilih')."' left join rs_jenistarif rj on rj.idjenistarif=rb.idjenistarif left join rs_satuan rs on rs.idsatuan=rb.idsatuan left join rs_sediaan rsd on rsd.idsediaan=rb.idsediaan where rbs.stok > 0 ORDER by rb.idbarang asc")->result_array();
    //     $this->load->view('report/inventaris/databarang',$data);
    // }
    public function excel_inventaris($ttl)
    {
        $data['titel']= "databarang".date('Y-m-d');
        $data['total_kekayaan']= $ttl;
            $data['obat'] = $this->db->query("SELECT rb.namabarang, rb.kode,ifnull(rbs.stok,0) stok, rbp.batchno, rbp.kadaluarsa, rbp.hargabeli, rb.hargajual, rb.kekuatan,rb.jenis, rj.jenistarif, rs.namasatuan, rsd.namasediaan FROM rs_barang rb left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok rbs on rbs.idbarangpembelian=rbp.idbarangpembelian and rbs.idunit='".$this->session->userdata('idunitterpilih')."' left join rs_jenistarif rj on rj.idjenistarif=rb.idjenistarif left join rs_satuan rs on rs.idsatuan=rb.idsatuan left join rs_sediaan rsd on rsd.idsediaan=rb.idsediaan where rbs.stok > 0 ORDER by rb.idbarang asc")->result_array();
            $this->load->view('report/inventaris/databarang',$data);
    }
    public function excel_paketpemeriksaan()
    {
        $data['titel']= "paketpemeriksaan".date('Y-m-d');
        $data['paketpemeriksaan'] = $this->db->query("SELECT `a`.`idpaketpemeriksaan`, `a`.`namapaketpemeriksaan`, `a`.`namapaketpemeriksaancetak`, `d`.`namarule`, `e`.`jenis`, `c`.`jenisicd` FROM `rs_paket_pemeriksaan` `a` JOIN `rs_jenisicd` `c` ON `c`.`idjenisicd` = `a`.`idjenisicd` LEFT JOIN `rs_paket_pemeriksaan_rule` `d` ON `d`.`idpaketpemeriksaanrule` = `a`.`idpaketpemeriksaanrule` LEFT JOIN `rs_jeniscetaklaboratorium` `e` ON `e`.`idjeniscetak` = `a`.`idjeniscetak` WHERE ( `a`.`namapaketpemeriksaan` LIKE '%%' ESCAPE '!' OR `a`.`namapaketpemeriksaancetak` LIKE '%%' ESCAPE '!' OR `c`.`jenisicd` LIKE '%%' ESCAPE '!' ) AND `a`.`idpaketpemeriksaanparent` = 0 AND `a`.`isdelete` IS NULL ORDER BY `a`.`namapaketpemeriksaan` ASC")->result_array();
        $this->load->view('report/excel_paketpemeriksaan',$data);
    }
    public function downloadexcel_page($value)
    {
    // format $value -> dataatauid%20modehalaman -> 32%20kasir_versipasien || 2019-02-18%20pemakaianobat(2019-02-18 pemakaianobat , dll
        // var_dump($value);
        // return;
        $arr_dt = explode("%20", $value);
        $page=$arr_dt[1];
        switch ($page) {
            case 'laporandataindukpasien': // dari js datatable downloadexcel_page
                $dt = explode("%7C",$arr_dt[0]);
                $td2 = explode("%20", $dt[1]);
                $data['titel']   = $page.' '.$dt[0].'-'.$dt[1];
                $data['rmawal']  = $dt[0];
                $data['rmakhir'] = $dt[1];
                $data['datapasien'] = $this->db->query("select p.*, ps.nojkn, ps.norm, d.namapekerjaan, dp.namapendidikan  from person_person p left join demografi_pendidikan dp on dp.idpendidikan = p.idpendidikan left join demografi_pekerjaan d on d.idpekerjaan = p.idpekerjaan left join person_pasien ps on ps.idperson = p.idperson  and ps.norm <='".$dt[1]."' and ps.norm >='".$dt[0]."' where  ps.idperson = p.idperson")->result();
                return $this->load->view('report/excel_dataindukpasien',$data);
                break;
            case 'laporandataobat': // dari js datatable downloadexcel_page
                $dt = explode("%7C",$arr_dt[0]);
                $awal = $dt[0];
                $akhir = $dt[1];
                $data['dataobat'] = $this->report_laporandtobat($awal,$akhir);
                $data['titel'] = 'pemakaianobat'.$awal.'-'.$akhir;
                $data['mode']  = $page;
                break;
            case 'laporandataobatbebas': // dari js datatable downloadexcel_page
                $dt = explode("%7C",$arr_dt[0]);
                $awal  = $dt[0];
                $akhir = $dt[1];
                $data['dataobat'] = $this->report_laporandtobatbebas($awal,$akhir);
                $data['titel']    = 'pemakaianobatbebas'.$awal.'-'.$akhir;
                $data['mode']     = $page;
                return $this->load->view('report/excel_dataobatbebas',$data);
                break;
            case 'kasir_versipasien': // dari js kasir generatetoexcek_versipasien
                $dt = explode("%7C",$value);
                $nama = $this->db->query("select p.namalengkap from person_pasien pp, person_person p where p.idperson = pp.idperson and pp.norm='$dt[1]'")->row_array();
                $data['titel'] = $nama['namalengkap'];
                $data['infotagihan'] = $this->mviewql->viewinfotagihan($dt[0]);
                $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($dt[0],0);
                $data['mode']  = $page;
                break;
            case 'pelayananperiksa': // dari js js_pemeriksaanpasien -> get_exceldata
                $dt = explode("%7C",$arr_dt[0]);
                $data['titel'] = 'pemeriksanpasien '.$dt[0].'-'.$dt[1];
                $this->load->model('mkombin');
                $data['dtperiksa'] = $this->mkombin->rspemeriksaan_getdata($arr_dt[2],$dt[0],$dt[1]);
                $data['mode']  = $page;
                break;
            case 'rl4agetdata': 
                $data['titel'] = 'rl4agetdata '.$arr_dt[0];
                $data['titel'] = 'rl4bgetdata '.$arr_dt[0];
                $data['dtresult'] = $this->mviewql->rl4getdata('ranap',$dt[0],$dt[1])->result();
                return $this->load->view('report/excelrl4a',$data);
                break;
            case 'rl4bgetdata': 
                $data['titel'] = 'rl4bgetdata '.$arr_dt[0];
                $dt = explode("%7C",$arr_dt[0]);
                $data['jenis']   = $this->db->get_where('rs_icd_golongansebabpenyakit')->result();
                $data['laporan'] = $this->mviewql->rl4getdata('rajal',$dt[0],$dt[1])->result();
                return $this->load->view('report/excelrl4b',$data);
                break;
            case 'rl51getdata': 
                $data['titel'] = 'RL51'.$arr_dt[0];
                $dt = explode("%7C",$arr_dt[0]);
                $data['dtresult'] = $this->getdt_rl51($dt[0],$dt[1]);;
                return $this->load->view('report/excelrl51',$data);
                break;
            case 'rl52getdata': 
                $data['titel'] = 'RL52'.$arr_dt[0];
                $dt = explode("%7C",$arr_dt[0]);
                $data['dtresult'] = $this->getdt_rl52($dt[0],$dt[1]);
                return $this->load->view('report/excelrl52',$data);
                break;
            case 'rl53getdata': 
                $data['titel'] = 'RL53'.$arr_dt[0];
                $dt = explode("%7C",$arr_dt[0]);
                $data['dtresult'] = $this->mviewql->viewlaporanrl53($dt[0],$dt[1]);
                return $this->load->view('report/excelrl53',$data);
                break;
            case 'rl54getdata': 
                $data['titel'] = 'RL54'.$arr_dt[0];
                $dt = explode("%7C",$arr_dt[0]);
                $data['dtresult'] = $this->mviewql->viewlaporanrl54($dt[0],$dt[1]);
                return $this->load->view('report/excelrl54',$data);
                break;
            case 'lppelayanan': // dari js datatable downloadexcel_page
                $dt = explode("%7C",$value);
                $td2 = explode("%20", $dt[1]);
                // $this->load->model('mkombin');
                return $this->load_lap_pelayanan($dt[0],$td2[0],'unduh');
                break;
            case 'pneumoniaorispa':
                $dt = explode("%7C",$value);
                $td2 = explode("%20", $dt[1]);
                $data['rajal'] = $this->mviewql->getreport_pneumoniaorispa_rajal($dt[0],$td2[0])->row_array();
                $data['rajalbykab']=$this->mviewql->getreport_pneumoniaorispabykabupaten_rajal($dt[0],$td2[0])->result();
                $data['titel'] = 'Pelaporan Pneumonia/ISPA'.$dt[0].'-'.$td2[0];
                return $this->load->view('report/excel_pelaporanpneumoniaispa',$data);
                break;
            case 'balitapneumonia':
                $dt = explode("%7C",$value);
                $td2 = explode("%20", $dt[1]);
                $data['dtlaporan'] = $this->mviewql->getreport_balitapneumonia($dt[0],$td2[0])->result();
                $data['titel'] = 'Pelaporan Balita Pneumonia'.$dt[0].'-'.$td2[0];
                return $this->load->view('report/excel_pelaporanbalitapneumonia',$data);
                break;
            case 'stprs_rajal':
                $dt = explode("%7C",$value);
                $td2 = explode("%20", $dt[1]);
                $data['titel'] = 'STPRS'.strtoupper('RAJAL'.$dt[0].'-'.$td2[0]);
                $data['stprs'] = $this->mviewql->report_getdatastprs_rajal($dt[0],$td2[0])->result();
                $data['jenis'] = 'JALAN';
                $data['judullaporan'] = 'SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT JALAN';
                return $this->load->view('report/excel_stprs',$data);
                break;
            case 'stprs_rajaltxt':
                $dt = explode("%7C",$value);
                $td2 = explode("%20", $dt[1]);
                $data['titel'] = 'STPRS'.strtoupper('RAJAL'.$dt[0].'-'.$td2[0]);
                $data['stprs'] = $this->mviewql->report_getdatastprs_rajal($dt[0],$td2[0])->result();
                $data['jenis'] = 'JALAN';
                $data['judullaporan'] = 'SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT JALAN';
                return $this->load->view('report/txt_stprs',$data);
                break;
            case 'laporanpembelianobat':
                $get = explode("%7C",explode("%20", $value)[0]);
                $tanggal1 = $get[0];
                $tanggal2 = $get[1];
                $filtertanggal= $get[2];
                $data['filtertanggal'] = $filtertanggal;
                $data['data'] = $this->db->query("select rb.namabarang, pbf.namapbf, rbdr.distributor, ifnull(rbf.ppn,0) ppn, ifnull(rbd.jumlah,0) jumlah, ifnull(rbp.persendiskon,0) persendiskon, rbp.hargabeli, ifnull(rbp.nominaldiskon,0) as nominaldiskon 
                    from rs_barang_pembelian rbp 
                    left join rs_barang_distribusi rbd on rbd.idbarangpembelian=rbp.idbarangpembelian
                    left join rs_barang rb on rb.idbarang=rbp.idbarang
                    left join rs_barang_faktur rbf on rbf.idbarangfaktur = rbp.idbarangfaktur
                    left join rs_barang_distributor rbdr on rbdr.idbarangdistributor = rbf.idbarangdistributor
                    left join rs_barang_pbf pbf on pbf.idbarangpbf=rbdr.idbarangpbf
                    where ".(($filtertanggal=='tanggalfaktur') ? " date(rbf.tanggalfaktur) " :" date(rbf.tanggalinput) " )." between '".$tanggal1."' and '".$tanggal2."' and rbd.jenisdistribusi='masuk' order by pbf.idbarangpbf, rbdr.idbarangdistributor")->result_array();
                $data['title'] = 'LAPORAN PEMBELIAN OBAT '.$tanggal1.' - '.$tanggal2;
                return $this->load->view('report/excel_laporanpembelianobat',$data);
                break;
            case 'pelunasanhutangfarmasi':
                $data['filter'] = explode("%7C",$arr_dt[0]);
                $query = $this->db->select(' a.*, b.distributor,datediff(a.tanggaljatuhtempo,a.tanggalfaktur) masahutang, datediff(a.tanggaljatuhtempo,now()) tempo,pbf.namapbf')
                ->join('rs_barang_distributor b','b.idbarangdistributor = a.idbarangdistributor','left')
                ->join('rs_barang_pbf pbf','pbf.idbarangpbf=b.idbarangpbf','left');
                $query = $this->db->where("a.tanggalfaktur BETWEEN '".$data['filter'][0]."' AND '".$data['filter'][1]."'");
                $query = $this->db->get('rs_barang_faktur a')->result_array();
                $data['title'] = "pelunasanhutangfarmasi ".$data['filter'][0].' - '.$data['filter'][1];
                $data['dtpelunasan'] = $query;
                return $this->load->view('report/excel_pelunasanhutangfarmasi',$data);
                break;
            default:
                # code...
                break;
        }
        $this->load->view('report/excel_report',$data);
    }
    
    
    // start unduh excel
    //unduh excel terbaru, fungsi downloadexcel_page akan diganti ke unduhexcel
    public function unduhexcel()
    {
        $page = $this->uri->segment(3);
        switch ($page) {
            case 'fakturlunashutangfarmasi':
                if( $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASFARMASI) ) :
                    $data['tanggal1'] = $this->uri->segment(4);
                    $data['tanggal2'] = $this->uri->segment(5);
                    $data['title'] = "Faktur Lunas Farmasi - Tanggal Pelunasan ".$data['tanggal1'].' - '.$data['tanggal2'];
                    $data['dtpelunasan'] = $this->mkombin->getdata_fakturlunashutangfarmasi($data['tanggal1'],$data['tanggal2']);
                    return $this->load->view('report/excel_fakturlunashutangfarmasi',$data);
                else : 
                    aksesditolak();
                endif;
                break;
            case 'kunjunganpasienralanbyrujukan':
                $data['tanggal1'] = $this->uri->segment(4);
                $data['tanggal2'] = $this->uri->segment(5);
                $data['kunjungan']= $this->mkombin->getdata_kunjunganralan_byrujukan($data['tanggal1'],$data['tanggal2']);
                $data['title']    = 'Kunjungan Pasien RALAN Berdasarkan Asal Rujukan '.$data['tanggal1'].' - '.$data['tanggal2'];
                return $this->load->view('report/excel_kunjunganpasienralanbyrujukan',$data);
                break;
            case 'fakturlunashutangusaha':
                if( $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASUSAHA) ) :
                    $data['tanggal1']    = $this->uri->segment(4);
                    $data['tanggal2']    = $this->uri->segment(5);
                    $data['fakturlunas'] = $this->mkombin->getdata_fakturlunashutangusaha($data['tanggal1'],$data['tanggal2']);
                    $data['title']       = 'Faktur Lunas Usaha - Tanggal Pelunasan '.$data['tanggal1'].' - '.$data['tanggal2'];
                    return $this->load->view('report/excel_fakturlunashutangusaha',$data);
                else : 
                    aksesditolak();
                endif;
                break;
            case 'kunjunganpasienbaru':
                    $tanggal        = $this->uri->segment(4);
                    $data['data']   = $this->mkombin->laporankunjunganpasienbaru($tanggal);
                    $data['title']  = 'Kunjungan Pasien Baru Tanggal '.$tanggal;
                    return $this->load->view('report/excel_kunjunganpasienbaru',$data);
                break;
            case 'antigennegatif':
                    $tanggal        = explode('%20', $this->uri->segment(4));
                    $data['data']   = $this->mkombin->getdata_pasienantigen($tanggal[0],$tanggal[1],'negatif')->result();
                    $data['title']  = 'Kunjungan Pasien Antigen Negatif '.$tanggal[0].' - '.$tanggal[1];
                    return $this->load->view('report/excel_kunjunganpasienantigen',$data);
                break;
            case 'antigenpositif':
                    $tanggal        = explode('%20', $this->uri->segment(4));
                    $data['data']   = $this->mkombin->getdata_pasienantigen($tanggal[0],$tanggal[1],'positif')->result();
                    $data['title']  = 'Kunjungan Pasien Antigen Positif '.$tanggal[0].' - '.$tanggal[1];
                    return $this->load->view('report/excel_kunjunganpasienantigen',$data);
                break;
            case 'registerkamarbedah':
                    $data['title']  = 'Register Kamar Bedah '.$this->input->post('tanggal1').' - '.$this->input->post('tanggal2');
                    $this->load->model('mdatatable');
                    $getdata          = $this->mdatatable->dt_registerkamarbedah_();
                    $data['register'] = '';
                    if($getdata)
                    {
                        $data['register'] = $getdata->result();
                    }
                    
                    return $this->load->view('report/excel_registerkamarbedah',$data);
                break;
            case 'laporanpenngunaanobatbulanan': // laporan penngunaan obat bulanan Rawat Jalan
                    $data['tahunbulan'] = $this->input->post('tahunbulan');
                    $data['judul']      = 'Laporan Penggunaan Obat Rawat Jalan Bulan '.$data['tahunbulan'];
                    $data['laporan']    = $this->mviewql->view_laporanpenggunaanobatperbulan($data['tahunbulan']);
                    return $this->load->view('report/excel_laporanpengunaanobatbulanan',$data);
                break;
            case 'laporanpenngunaanobatbulananRanap':  // laporan penngunaan obat bulanan Rawat Inap
                        $data['tahunbulan'] = $this->input->post('tahunbulan');
                        $data['judul']      = 'Laporan Penggunaan Obat Rawat Inap Bulan '.$data['tahunbulan'];
                        $data['laporan']    = $this->mviewql->view_laporanpenggunaanobatperbulanRanap($data['tahunbulan']);
                        return $this->load->view('report/excel_laporanpengunaanobatbulananRanap',$data);
                    break;
            case 'laporandistribusigudang':
                    $data['tahunbulan'] = $this->input->post('tahunbulan');
                    $data['judul']      = 'Laporan Distribusi Gudang Bulan '.$data['tahunbulan'];
                    $data['laporan']    = $this->mviewql->view_laporandistribusigudang($data['tahunbulan']);
                    return $this->load->view('report/excel_farmasi_lap_distribusigudang',$data);
                break;
            case 'laporan_logwaktulayanan':
                    $data['tahunbulan'] = $this->input->post('tahunbulan');
                    $data['judul']      = 'Laporan Log Waktu Layanan Bulan '.$data['tahunbulan'];
                    
                    $this->load->model('mdatatable');
                    $getdata            = $this->mdatatable->dt_waktulayanan();
                    $data['laporan']    = '';
                    if($getdata)
                    {
                        $data['laporan'] = $getdata->result();
                    }
                    return $this->load->view('report/excel_waktulayanan',$data);
                break;
            case 'laporan_labarugi':
                    $tglawal    = $this->input->post('awal');
                    $tglsampai  = $this->input->post('sampai');
                    $data['judul']      = 'Laporan Laba Rugi '.$tglawal.' - '.$tglsampai;
                    $this->load->model('mkeuangan');
                    $data['laporan']    = $this->mkeuangan->dt_laporanlabarugi();
                    return $this->load->view('report/excel_keuangan_laporanlabarugi',$data);
                break;
            case 'penggunaanbarangincludetindakan':
                    $this->load->model('mdatatable');
                    
                    $tglawal    = $this->input->post('min');
                    $tglsampai  = $this->input->post('max');
                    $getdata    = $this->mdatatable->dt_penggunaanbarangincludetindakan();
                    
                    $data['barang']     = '';
                    if($getdata)
                    {
                        $data['barang'] = $getdata->result_array();
                    }
                    $data['judul']  = 'Laporan Penggunaan BHP/Alkes Include Tarif Tindakan Unit '.$this->input->post('namaunit').' '.$tglawal.' - '.$tglsampai;
                    return $this->load->view('report/excel_penggunaanbarangincludetindakan',$data);
                break;
            case 'rincian_akun':
                    $idakun             = $this->input->post('idakun');
                    $akun               = $this->input->post('akun');
                    $tglawal            = $this->input->post('awal');
                    $tglsampai          = $this->input->post('sampai');
                    $data['judulFile']  = 'Laporan Rincian Akun'.$akun.' - '.$tglawal.'-'.$tglsampai;
                    $data['judul']      = 'Laporan Rincian Akun';
                    $data['subJudul']   = $akun.' Periode '.$tglawal.' s/d '.$tglsampai;
                    $this->load->model('mkeuangan');
                    $data['riwayat']    = $this->mkeuangan->get_rincianakun($idakun,$tglawal,$tglsampai); //get_rincianakun($idakun,$awal,$akhir)
                    return $this->load->view('report/excel_keuangan_rincianakun',$data);
                    //echo json_encode($data); // debug mode
                break;
            case 'historyakun':
                    $akun_title     = $this->input->post('myID');
                    $idakun         = $this->input->post('idakun');
                    $tglawal        = $this->input->post('tanggal_mulai');
                    $tglakhir       = $this->input->post('tanggal_selesai');
                    $data['judulFile']  = 'Laporan History Akun '.$akun_title.' - '.$tglawal.'-'.$tglakhir;
                    $data['judul']      = 'Laporan History Akun '.$akun_title;
                    $data['subJudul']   = ' Periode '.$tglawal.' s/d '.$tglakhir;
                    $data['idakun']   = $idakun;
                    $this->load->model('mkeuangan');
                    
                    $data['dataHistory']   = $this->mkeuangan->get_historyakun($idakun, $tglawal, $tglakhir); //get_historyakun($idakun, $tglawal, $tglakhir, $periode)
                    return $this->load->view('report/excel_keuangan_historyakun',$data);
                    //echo json_encode($data); // debug mode
                break;
        }
    }
    // end unduh excel
    
    public function downloadexcel_datainventaris()
    {
        $this->load->model('mdatatable');
        $data['inventaris'] = $this->mdatatable->dt_inventariskantor_(false)->result();
        $data['title'] = 'Data Inventaris';
        $this->load->view('report/excel_datainventaris',$data);
    }

   
    // laporan data obat
    public function report_laporandtobat($awal,$akhir)
    {
        $ymin = date('Y', strtotime($awal));
        $ymax = date('Y',strtotime($akhir));
        $mmin = date('n',strtotime($awal));
        $mmax = date('n',strtotime($akhir));
        $query = $this->db->query("SELECT pp.waktuperiksa as waktu, rb.namabarang, rb.kode, rs.namasatuan, rj.jenistarif, rbp.jumlahpemakaian, rbp.harga, rbp.total, rbp.kekuatan, rbp.idbarangpemeriksaan FROM rs_barangpemeriksaan rbp LEFT JOIN rs_barang rb ON  rb.idbarang = rbp.idbarang LEFT JOIN person_pendaftaran pp ON pp.idpendaftaran = rbp.idpendaftaran LEFT JOIN rs_jenistarif rj ON rj.idjenistarif = rb.idjenistarif LEFT JOIN rs_satuan rs ON rs.idsatuan = rb.idsatuan where pp.tahunperiksa between '".$ymin."' and '".$ymax."' and pp.bulanperiksa between '".$mmin."' and '".$mmax."' and date(pp.waktuperiksa) between '".$awal."' and '".$akhir."' order by pp.waktuperiksa");
        return $query->result();
    }
    
    //laporan data obat bebas
    public function report_laporandtobatbebas($awal,$akhir)
    {
        $query = $this->db->query("SELECT rbp.waktuinput as waktu, rb.namabarang, rb.kode, rs.namasatuan, rj.jenistarif, rbp.jumlahpemakaian, rbp.harga, rbp.total, rbp.kekuatan 
                   FROM rs_barang_pembelibebas a
                   JOIN keu_tagihan_bebas ktb on ktb.idbarangpembelibebas = a.idbarangpembelibebas and ktb.status='selesai'
                   JOIN rs_barang_pembelianbebas rbp on rbp.idbarangpembelibebas = a.idbarangpembelibebas
                   LEFT JOIN rs_barang rb ON  rb.idbarang = rbp.idbarang 
                   LEFT JOIN rs_jenistarif rj ON rj.idjenistarif = rb.idjenistarif 
                   LEFT JOIN rs_satuan rs ON rs.idsatuan = rb.idsatuan 
                   WHERE date(a.waktu) between '".$awal."' and '".$akhir."' and ktb.status ='selesai'
                   GROUP BY rbp.waktuinput, rbp.idbarang");
                
                return $query->result();
    }
    // tampilan menu report pelayanan rs
    public function pelayananrs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Laporan Pelayanan Rumah Sakit';
            $data['table_title'] = 'Pelayanan Rumah Sakit';
            $data['plugins']     = [PLUG_DATE ];
            $data['jsmode']      = 'js_report';
            $data['mode']        = 'pelayananrs';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    /**
     * Laporan Kunjungan Kedokteran Jiwa
     */
    
    public function laporanpolikedokteranjiwa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Kedokteran Jiwa';
            $data['table_title'] = '';
            $data['bulan']       = ql_namabulan();
            $data['script_js']   = ['report/js_kunjungankedokteranjiwa'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['mode']        = 'kunjungankedokteranjiwa';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Tampil Laporan Kedokteran Jiwa
     */
    public function tampil_kunjungankedokteranjiwa()
    {
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $data  = $this->mviewql->view_kunjunganpasienkedokteranjiwa($tahun,$bulan)->result();
        echo json_encode($data);
    }
    
    /**
     * Laporan cakupan pasien berdasarkan cara bayar
     */
    
    public function laporanpasienbycarabayar()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Cakupan Pasien Ralan Berdasarkan Cara Bayar';
            $data['table_title'] = '';
            $data['script_js']   = ['report/js_cakupanpasiencarabayar'];
            $data['plugins']     = [PLUG_DATE ];
            $data['mode']        = 'cakupancarabayarralan';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Tampil Laporan Kedokteran Jiwa
     */
    public function tampil_laporanpasienbycarabayar()
    {
        $data  = $this->mviewql->view_cakupanpasiencarabayar();
        echo json_encode($data);
    }
    
    /**
     * Laporan data kondisi keluar pasien ralan per tahun
     */
    
    public function laporanKondisiKeluar()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Laporan Kondisi Keluar Pasien RALAN Per Tahun';
            $data['table_title'] = '';
            $data['script_js']   = ['report/js_kondisikeluar'];
            $data['plugins']     = [PLUG_DATE ];
            $data['mode']        = 'kondisikeluar';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Tampil data kondisi keluar pasien ralan per tahun
     */
    public function tampil_laporanKondisiKeluar()
    {
        $tahun = $this->input->post('tahun');
        $data  = $this->mviewql->view_laporankondisikeluar($tahun);
        echo json_encode($data);
    }
    
    
    
    
    /**
     * Laporan Kunjungan Pasien Umum
     */
    
    public function laporanpasienumum()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Umum';
            $data['table_title'] = '';
            $data['bulan']       = ql_namabulan();
            $data['script_js']   = ['report/js_kunjunganpasienumum'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['mode']        = 'kunjunganpasienumum';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Tampil Laporan Kunjungan Pasien Umum
     */
    public function tampil_kunjunganpasienumum()
    {
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $data  = $this->mviewql->view_kunjunganpasienumum($tahun,$bulan)->result();
        echo json_encode($data);
    }


    /**
     * Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Asal Rujukan
     */
    public function laporankunjunganpasienbyasalrujukan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Ralan Berdasarkan Asal Rujukan';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'kunjunganpasienasalrujukan';
            $data['mode']        = 'kunjunganpasienasalrujukan';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    /**
     * Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Asal Rujukan
     */
    public function laporankunjunganpasienranapbyasalrujukan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Ranap Berdasarkan Asal Rujukan';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'kunjunganranapbyrujukan';
            $data['mode']        = 'kunjunganranapbyrujukan';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    /**
     * Tampil Laporan Kunjungan Pasien Rawat Inap Berdasarkan Asal Rujukan 
     */
    public function tampil_kunjunganranapbyrujukan()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tangga2  = $this->input->post('tanggal2');
        $data['rekapranap']  = $this->mkombin->getdata_kunjungan_byrujukan('ranap',$tanggal1,$tangga2);
        $data['pasienranap'] = $this->mkombin->getdata_kunjungan_pasienranapbyrujukan($tanggal1,$tangga2);
        echo json_encode($data);
    }


    /**
     * Laporan Kunjungan laporan kunjungan pasien baru
     */
    public function laporankunjunganpasienbaru()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_kunjunganpasienbaru']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Baru';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'kunjunganpasienbaru';
            $data['mode']        = 'kunjunganpasienbaru';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    
    public function tampil_kanjunganpasienbaru()
    {
        $tanggal = $this->input->post('tanggal');
        $data = $this->mkombin->laporankunjunganpasienbaru($tanggal);
        echo json_encode($data);
    }
    
    /**
     * Tampil Laporan Kondisi Keluar Pasien
     */
    public function tampil_kondisikeluar()
    {
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun'); // jenis pasien RALAN / RAJAL
        $data  = $this->mviewql->view_kondisipasien($tahun,$bulan)->result();
        echo json_encode($data);
    }
    
    
    /**
     * Laporan Kunjungan laporan kunjungan pasien Lama baru
     */
    public function laporankunjunganpasienlamabaru()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_kunjunganpasienlamabaru']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Lama & Pasien Baru';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'kunjunganpasienlamabaru';
            $data['mode']        = 'kunjunganpasienlamabaru';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    
    public function tampil_kanjunganpasienlamabaru()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tanggal2 = $this->input->post('tanggal2');
        $data = $this->mkombin->laporankunjunganpasienlamabaru($tanggal1,$tanggal2);
        echo json_encode($data);
    }
    
    /**
     * Laporan Kunjungan Pasien By Etnis dan Bahasa
     */
    public function laporankunjunganpasienbyetnisdanbahasa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_kunjunganpasienbyetnisdanbahasa']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Berdasarkan Etnis/Suku & Bahasa';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE , PLUG_CHART];
            $data['jsmode']      = '';
            $data['mode']        = 'kunjunganpasienbyetnisdanbahasa';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function tampil_kunjunganpasienbyetnisdanbahasa()
    {
        $bulan = $this->input->post('bulan');
        $data = $this->mkombin->laporankunjunganpasienbyetnisdanbahasa($bulan);
        echo json_encode($data);
    }
    

    
    /**
     * Laporan Kunjungan Pasien antigen positif
     */
    public function kunjunganpasienantigenpositif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_reportpasienantigenpositif']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Antigen Positif';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = '';
            $data['mode']        = 'kunjunganpasienantigen';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function tampil_kunjunganpasienantigen()
    {
        $tanggal = $this->input->post('tanggal');
        $tanggal2= $this->input->post('tanggal2');
        $hasil  = $this->input->post('hasil');
        $data    = $this->mkombin->getdata_pasienantigen($tanggal,$tanggal2,$hasil)->result();
        echo json_encode($data);
    }
    
    /**
     * Laporan Kunjungan Pasien antigen negatif
     */
    public function kunjunganpasienantigennegatif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_reportpasienantigennegatif']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Antigen Negatif';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = '';
            $data['mode']        = 'kunjunganpasienantigen';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     * Tampil Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Asal Rujukan 
     */
    public function tampil_kunjunganralanbyrujukan()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tangga2  = $this->input->post('tanggal2');
        $data = $this->mkombin->getdata_kunjungan_byrujukan('rajal',$tanggal1,$tangga2);
        echo json_encode($data);
    }
    
    /**
     * Laporan RM GANDA
     */
    public function laporanrmganda()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS))
        {
            $data = [
                'active_menu'=> 'report',
                'active_menu_level' => '',
                'script_js'=> ['report/js_laporanrmganda']
            ];
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Laporan Rekam Medis Ganda';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = '';
            $data['mode']        = 'laporanrmganda';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     * Tampil Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Asal Rujukan 
     */
    public function tampil_laporanrmganda()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tanggal2  = $this->input->post('tanggal2');
        $data = $this->mkombin->getdata_laporanrmganda($tanggal1,$tanggal2);
        echo json_encode($data);
    }
    
    /**
     * Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Status Layanan
     */
    public function laporankunjunganpasienbylayanan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Kunjungan Pasien Ralan Berdasarkan Status Layanan';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'kunjunganpasienbylayanan';
            $data['mode']        = 'kunjunganpasienbylayanan';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    /**
     * Tampil Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Asal Rujukan 
     */
    public function tampil_kunjunganralanbylayanan()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tangga2  = $this->input->post('tanggal2');
        $data = $this->mkombin->getdata_kunjunganralan_bylayanan($tanggal1,$tangga2);
        echo json_encode($data);
    }
    
    /**
     * Laporan Kunjungan Pasien Berdasarkan Cara Daftar
     */
    public function laporankunjunganpasienbycaradaftar()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                   = $this->settinglaporan();
            $data['content_view']   = 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']     = 'Kunjungan Pasien Berdasarkan Cara Daftar';
            $data['plugins']        = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']         = 'kunjunganpasienbycaradaftar';
            $data['mode']           = 'kunjunganpasienbycaradaftar';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    
    /**
     * Tampil Laporan Kunjungan Pasien Berdasarkan Cara Daftar
     */
    public function tampil_kunjunganralanbycaradaftar()
    {
        $tanggal1 = $this->input->post('tanggal1');
        $tangga2  = $this->input->post('tanggal2');
        $data     = $this->mkombin->getdata_kunjunganralan_bycaradaftar($tanggal1,$tangga2);
        echo json_encode($data);
    }
    
    // laporan pemeriksaan pasien
    public function laporanperiksapasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Laporan Pasien Bulan '.date('M Y');
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'lap_pelayanan';
            $data['mode']        = 'laporanpelayanan';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    public function tampillaporanperiksapasien($ibulan='',$unduh='')
    {
        $tahun = date('Y', strtotime(((empty($unduh))?$this->input->post('bulan'):$ibulan)));
        $bulan = date('m', strtotime(((empty($unduh))?$this->input->post('bulan'):$ibulan)));
        $identitas = $this->db->query("select `ru`.`idunit` AS `idunit`,if(pp.ispasienlama='1','Lama','Baru') ispasienlama,`pp`.`jenispemeriksaan` AS `jenispemeriksaan`,`pp`.`kondisikeluar` AS `kondisikeluar`,`pp`.`carabayar` AS `carabayar`,`ppas`.`alergi` AS `alergi`,date_format(`rp`.`waktu`,'%d/%m/%Y') AS `tglperiksa`,date_format(`pper`.`tanggallahir`,'%d/%m/%Y') AS `tanggallahir`,`pp`.`keteranganradiologi` AS `keteranganradiologi`,`pp`.`saranradiologi` AS `saranradiologi`,if((`ru`.`idpegawaidokter` = '0'),concat(ifnull(`ppag`.`titeldepan`,''),' ',ifnull(`pper2`.`namalengkap`,''),' ',ifnull(`ppag`.`titelbelakang`,'')),concat(ifnull(`ppag2`.`titeldepan`,''),' ',ifnull(`pper3`.`namalengkap`,''),' ',ifnull(`ppag2`.`titelbelakang`,''))) AS `dokter`,`pp`.`idpendaftaran` AS `idpendaftaran`,`pp`.`norm` AS `norm`,`rp`.`idpemeriksaan` AS `idpemeriksaan`,cast(`rp`.`waktu` as date) AS `waktu`,concat(if((strcmp('POLI',`ru`.`namaunit`) = -(1)),'','POLI '),ifnull(`ru`.`namaunit`,'')) AS `namaunit`,`pper`.`identitas` AS `identitas`,`pper`.`namalengkap` AS `namalengkap`,`usia`(`pper`.`tanggallahir`) AS `usia`,`pper`.`alamat` AS `alamat`,`pper`.`jeniskelamin` AS `jeniskelamin`,`rp`.`keterangan` AS `keterangan`,`rp`.`anamnesa` AS `anamnesa` from (((((((((`person_pendaftaran` `pp` join `rs_pemeriksaan` `rp`) join `rs_unit` `ru`) join `person_pasien` `ppas`) join `person_person` `pper`) join `rs_jadwal` `rj`) join `person_pegawai` `ppag`) join `person_person` `pper2`) left join `person_pegawai` `ppag2` on((`ppag2`.`idpegawai` = `ru`.`idpegawaidokter`))) left join `person_person` `pper3` on((`pper3`.`idperson` = `ppag2`.`idperson`))) where ( pp.idstatuskeluar=2 and (`rp`.`idpendaftaran` = `pp`.`idpendaftaran`  and month(rp.waktu)='".$bulan."' and year(rp.waktu)='".$tahun."') and (`ppas`.`norm` = `pp`.`norm`) and (`pper`.`idperson` = `ppas`.`idperson`) and (`rj`.`idjadwal` = `rp`.`idjadwal`) and (`ru`.`idunit` = `rj`.`idunit`) and (`ppag`.`idpegawai` = `rj`.`idpegawaidokter`) and (`pper2`.`idperson` = `ppag`.`idperson`))")->result();
        $icd   = [];
        $terapi = [];
        if(!empty($identitas))
        {
            foreach ($identitas as $obj)
            {
                $icd[] = $this->db->query("select concat( ifnull(rh.nilai,''), ifnull(rh.nilaitext,'')) nilai, rh.icd, ri.namaicd, ri.idjenisicd from rs_hasilpemeriksaan rh, rs_icd ri where idpemeriksaan='".$obj->idpemeriksaan."' and ri.icd=rh.icd")->result();
                $terapi[] = $this->db->query("select rbp.idpemeriksaan, rb.kode, rb.namabarang, rbp.jumlahpemakaian, rs.namasatuan from rs_barangpemeriksaan rbp, rs_barang rb, rs_satuan rs where idpemeriksaan='".$obj->idpemeriksaan."' and rb.idbarang=rbp.idbarang and rs.idsatuan=rb.idsatuan")->result();     
            }
        }
        if(!empty($unduh))  //unduh excel
        {
            $data = ['identitas'=>$identitas,'icd'=>$icd,'terapi'=>$terapi];
            $data['titel'] = 'Laporan Pasien Bulan '.date('M Y', strtotime($ibulan));
            return $this->load->view('report/excel_laporanpasienbulanan',$data);
        }
        echo json_encode(['identitas'=>$identitas,'icd'=>$icd,'terapi'=>$terapi]); //tampilkan melalui js
    }
    // halaman rl4a_penyakitranap
    public function rl4a_penyakitranap()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL4A';
            $data['table_title'] = 'Penyakit Rawat Inap';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl4a';
            $data['mode']        = 'rl4a';
            $this->load->view('v_index',$data);
        }else{aksesditolak();}
    }
    // halaman rl4b_penyakitrajal
    public function rl4b_penyakitrajal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL4B';
            $data['table_title'] = 'Penyakit Rawat Jalan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl4b';
            $data['mode']        = 'rl4b';
            $this->load->view('v_index',$data);
        }
        else{aksesditolak();}
    }
    // halaman rl51_kunjunganpasienranap
    public function rl51_kunjunganpasienranap()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL5.1 - Kunjungan Pasien Ranap';
            $data['table_title'] = 'Kunjungan Pasien Ranap';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl51';
            $data['mode']        = 'rl51';
            $this->load->view('v_index',$data);
        }
        else{aksesditolak();}
    }
    // halaman rl52_kunjunganpasienrajal
    public function rl52_kunjunganpasienrajal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL5.2 - Kunjungan Pasien Rawat Jalan';
            $data['table_title'] = 'Kunjungan Pasien Rawat Jalan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl52';
            $data['mode']        = 'rl52';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    // halaman rl52_kunjunganpasienrajalperpoli
    public function rl52_kunjunganpasienrajalperpoli()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL5.2 - Kunjungan Ralan Berdasarkan Poli Tujuan';
            $data['table_title'] = 'Kunjungan Rawat Jalan Berdasarkan Poli Tujuan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl52b';
            $data['mode']        = 'rl52b';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    // halaman rl341_rujukan
    public function rl314_rujukan(){
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL3.14 - Rujukan';
            $data['table_title'] = 'Data Rujukan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl314';
            $data['mode']        = 'rl314';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    // halaman rl53_10besarpenyakitranap
    public function rl53_10besarpenyakitranap()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL5.3 - 10 Besar Penyakit Rawat Inap';
            $data['table_title'] = '10 Besar Penyakit Rawat Inap';
            $data['plugins']     = [ PLUG_DATATABLE ,PLUG_DATE ];
            $data['jsmode']      = 'js_rl53';
            $data['mode']        = 'rl53';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    // halaman rl54_10besarpenyakitrajal
    public function rl54_10besarpenyakitrajal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'RL5.4';
            $data['table_title'] = '10 Besar Penyakit Rawat Jalan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_rl54';
            $data['mode']        = 'rl54';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    // pelaporan SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT JALAN
    public function report_stprsrajal()
    {
        $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'STP RS RAJAL';
            $data['table_title'] = 'LAPORAN SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT JALAN (kasus baru)';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_report_stprsrajal';
            $data['jenispemeriksaan'] = 'jalan';
            $data['mode']        = 'report_stprs';
            $this->load->view('v_index',$data);
    }

    // pelaporan SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT INAP
    public function report_stprsranap()
    {
        $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'STP RS RAJAL';
            $data['table_title'] = 'LAPORAN SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT INAP  (kasus baru)';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_report_stprsranap';
            $data['jenispemeriksaan'] = 'inap';
            $data['mode']        = 'report_stprs';
            $this->load->view('v_index',$data);
    }

    // LIST STP RS
    public function report_stprslist_rajal()
    {
        $tgl1 = $this->input->post("tgl1");
        $tgl2 = $this->input->post("tgl2");
        echo json_encode( $this->mviewql->report_getdatastprs_rajal($tgl1,$tgl2)->result());
    }

    // pelaporan pneumonia atau ispa
    public function pneumonia_pelaporanview()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'ISPA/PNEUMONIA';
            $data['table_title'] = 'Laporan ISPA/PNEUMONIA';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_pelaporanpneumonia';
            $data['mode']        = 'pelaporanpneumonia';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }   
    }
    // pelaporan balitapneumonia atau ispa
    public function balitapneumonia_pelaporanview()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'BALITA PNEUMONIA';
            $data['table_title'] = 'Laporan BALITA PNEUMONIA';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'js_pelaporanbalitapneumonia';
            $data['mode']        = 'pelaporanbalitapneumonia';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }   
    }
    public function pneumonia_pelaporanlist()
    {
        $tgl1 = $this->input->post("tgl1");
        $tgl2 = $this->input->post("tgl2");
        $rajal = $this->mviewql->getreport_pneumoniaorispa_rajal($tgl1,$tgl2)->row_array();
        $rajalbykab=$this->mviewql->getreport_pneumoniaorispabykabupaten_rajal($tgl1,$tgl2)->result();
        echo json_encode(['rajal'=>$rajal,'rajalbykab'=>$rajalbykab]);
    }
    // mahmud, clear
    public function balitapneumonia_pelaporanlist()
    {
        echo json_encode($this->mviewql->getreport_balitapneumonia($this->input->post('tgl1'),$this->input->post('tgl2'))->result());
    }
    // -- generate report excel RL5.1
    public function generate_excelrl($x)
    {
        $tahun = $x;
        $data['title']        = 'laporanR51';
        $data ['kunjunganpasien'] = $this->db->query("SELECT month(waktu) as bulan, year(waktu) as tahun, sum(if(ispasienlama=0 ,1,0)) as pengunjungbaru, sum(if(ispasienlama=1 ,1,0)) as pengunjunglama from person_pendaftaran where year(waktu)='$tahun' group by month(waktu)")->result();
        $this->load->view('beranda/laporanR51',$data);
    }
    // dipanggil di js_datatable
    public function rl4a_jsondt()
    {
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->rl4getdata('ranap',$this->input->post('tgl1'),$this->input->post('tgl2'))->result());
    }
    // dipanggil di js_datatable
    public function rl4b_jsondt()
    {
        $this->load->model('mviewql');
        $data['jenis']   = $this->db->get_where('rs_icd_golongansebabpenyakit')->result();
        $data['laporan'] = $this->mviewql->rl4getdata('rajal',$this->input->post('tgl1'),$this->input->post('tgl2'))->result();
        echo json_encode($data);
    }
    // dipanggil di js_datatable
    public function rl51_jsondt()
    {
        echo json_encode($this->getdt_rl51($this->input->post('tgl1'),$this->input->post('tgl2')));
    }
    private function getdt_rl51($tgl1,$tgl2)
    {
        return $this->db->query("SELECT  month(waktuperiksa) as bulan, year(waktuperiksa) as tahun,sum(if(ispasienlama=0 and pp.jeniskelamin='laki-laki' ,1,0)) as pbpria,sum(if(ispasienlama=0 and pp.jeniskelamin='wanita' ,1,0)) as pbwanita, sum(if(ispasienlama=0 ,1,0)) as tpb, sum(if(ispasienlama=1 and pp.jeniskelamin='laki-laki' ,1,0)) as plpria,sum(if(ispasienlama=1 and pp.jeniskelamin='wanita' ,1,0)) as plwanita,sum(if(ispasienlama=1 ,1,0)) as tpl from person_pendaftaran p,person_pasien ps, person_person pp where p.idstatuskeluar='2'  and p.jenispemeriksaan='rajalnap' and date(waktuperiksa) BETWEEN '$tgl1' AND '$tgl2' and ps.norm = p.norm and pp.idperson = ps.idperson group by month(waktuperiksa)")->result();
    }
    // dipanggil di js_datatable
    public function rl52_jsondt()
    {
        echo json_encode($this->getdt_rl52($this->input->post('tgl1'),$this->input->post('tgl2')));
    }
    // ambil data laporan rl 52
    private function getdt_rl52($tgl1,$tgl2)
    {
        return $this->db->query("SELECT  month(waktuperiksa) as bulan, year(waktuperiksa) as tahun,sum(if(ispasienlama=0 and pp.jeniskelamin='laki-laki' ,1,0)) as pbpria,sum(if(ispasienlama=0 and pp.jeniskelamin='wanita' ,1,0)) as pbwanita, sum(if(ispasienlama=0 ,1,0)) as tpb, sum(if(ispasienlama=1 and pp.jeniskelamin='laki-laki' ,1,0)) as plpria,sum(if(ispasienlama=1 and pp.jeniskelamin='wanita' ,1,0)) as plwanita,sum(if(ispasienlama=1 ,1,0)) as tpl from person_pendaftaran p,person_pasien ps, person_person pp where p.idstatuskeluar='2' and ( p.jenispemeriksaan='rajal' or jenispemeriksaan='rajalnap') and date(waktuperiksa) BETWEEN '$tgl1' AND '$tgl2' and ps.norm = p.norm and pp.idperson = ps.idperson group by month(waktuperiksa)")->result();
    }
    // dipanggil di js_datatable
    public function rl52b_jsondt()
    {
        echo json_encode($this->getdt_rl52b($this->input->post('tgl1'),$this->input->post('tgl2')));
    }    
    // ambil data laporan rl 52b
    private function getdt_rl52b($tgl1,$tgl2)
    {
        return $this->db->query("select ru.idunit, ru.namaunit, 
        if(p.idunit='15',(select count(1) from rs_pemeriksaan where date(waktu) between '".$tgl1."' and '".$tgl2."'  and idunit='15' and status='selesai' ),COUNT(1)) as jumlahkasus,
        if(p.idunit='15',
           (select sum((select if(b.jeniskelamin='wanita', 1 ,0) from person_pasien a join person_person b on b.idperson=a.idperson where a.norm=rp.norm)) from rs_pemeriksaan rp where date(waktu) between '".$tgl1."' and '".$tgl2."'  and idunit='15' and status='selesai' ),
           sum((select if(b.jeniskelamin='wanita', 1 ,0) from person_pasien a join person_person b on b.idperson=a.idperson where a.norm=p.norm))
          ) as wanita,
        if(p.idunit='15',
           (select sum((select if(b.jeniskelamin!='wanita', 1 ,0) from person_pasien a join person_person b on b.idperson=a.idperson where a.norm=rp.norm)) from rs_pemeriksaan rp where date(waktu) between '".$tgl1."' and '".$tgl2."'  and idunit='15' and status='selesai' ),
           sum((select if(b.jeniskelamin!='wanita', 1 ,0) from person_pasien a join person_person b on b.idperson=a.idperson where a.norm=p.norm))
          ) as pria

        from
            person_pendaftaran p
            join rs_unit ru on ru.idunit = p.idunit
            WHERE date(p.waktuperiksa) BETWEEN '".$tgl1."' and '".$tgl2."' and p.idstatuskeluar='2'
            GROUP by p.idunit ORDER by ru.namaunit asc")->result();
    }

    // dipanggil di js_datatable
    public function rl314_jsondt()
    {
        // $this->load->model('mviewql');
        $tgl1 = $this->input->post('tgl1');
        $tgl2 = $this->input->post('tgl2');

        // $sql     = "SELECT ru.idunit,ru.namaunit,
        // (SELECT COUNT( pp.idpendaftaran ) 
        //  FROM person_pendaftaran pp 
        //  WHERE pp.idunit = ru.idunit
        //  AND date(pp.waktuperiksa) 
        //  BETWEEN '$tgl1' and '$tgl2'
        //  AND pp.jenispemeriksaan = 'rajal' 
        //  AND pp.carabayar != 'mandiri' 
        //  AND pp.idstatuskeluar = 2 
        //  AND pp.statustagihan != 'terbuka' 
        //  AND pp.jenisrujukan = 'rujukanbaru'
        //  AND pp.idklinikperujuk != 0
        //  AND (SELECT COUNT(bj.idjenisklinik) FROM bpjs_jenisklinik bj LEFT JOIN bpjs_klinikperujuk bk ON bj.idjenisklinik = bk.idjenisklinik  WHERE bk.idklinik=pp.idklinikperujuk AND bj.idjenisklinik=2 GROUP BY bk.idjenisklinik)
        
        // )  as rujukanpuskesmas,
        // (SELECT COUNT( pp.idpendaftaran ) 
        //  FROM person_pendaftaran pp 
        //  WHERE pp.idunit = ru.idunit
        //  AND date(pp.waktuperiksa) 
        //  BETWEEN '$tgl1' and '$tgl2'
        //  AND pp.jenispemeriksaan = 'rajal' 
        //  AND pp.carabayar != 'mandiri' 
        //  AND pp.idstatuskeluar = 2 
        //  AND pp.statustagihan != 'terbuka' 
        //  AND pp.jenisrujukan = 'rujukanbaru'
        //  AND pp.idklinikperujuk != 0
        //  AND (SELECT COUNT(bj.idjenisklinik) FROM bpjs_jenisklinik bj LEFT JOIN bpjs_klinikperujuk bk ON bj.idjenisklinik = bk.idjenisklinik  WHERE bk.idklinik=pp.idklinikperujuk AND bj.idjenisklinik=1 GROUP BY bk.idjenisklinik)
        
        // )  as rujukanrumahsakit,
        // (SELECT COUNT( pp.idpendaftaran ) 
        //  FROM person_pendaftaran pp 
        //  WHERE pp.idunit = ru.idunit
        //  AND date(pp.waktuperiksa) 
        //  BETWEEN '$tgl1' and '$tgl2'
        //  AND pp.jenispemeriksaan = 'rajal' 
        //  AND pp.carabayar != 'mandiri' 
        //  AND pp.idstatuskeluar = 2 
        //  AND pp.statustagihan != 'terbuka' 
        //  AND pp.jenisrujukan = 'rujukanbaru'
        //  AND pp.idklinikperujuk != 0
        //  AND (SELECT COUNT(bj.idjenisklinik) FROM bpjs_jenisklinik bj LEFT JOIN bpjs_klinikperujuk bk ON bj.idjenisklinik = bk.idjenisklinik  WHERE bk.idklinik=pp.idklinikperujuk AND ( bj.idjenisklinik != 1 OR bj.idjenisklinik!=2 ) GROUP BY bk.idjenisklinik)
        // )  as rujukanfaskeslain
        // FROM rs_unit ru 
        // WHERE jeniscetakresume = 'spesialis'";

        $sql = "SELECT a.idpendaftaran,a.idunit,a.norm,a.idklinikperujuk ,
        (SELECT namaunit FROM rs_unit WHERE idunit=a.idunit) as namaunitrs,
        (SELECT namaklinik FROM bpjs_klinikperujuk WHERE idklinik=a.idklinikperujuk) as namaklinik,
        (SELECT kodeklinik FROM bpjs_klinikperujuk WHERE idklinik=a.idklinikperujuk) as kodeklinik,
        (SELECT bj.jenisklinik FROM bpjs_jenisklinik bj LEFT JOIN bpjs_klinikperujuk bk ON bj.idjenisklinik = bk.idjenisklinik  WHERE bk.idklinik=a.idklinikperujuk) as jenisklinik,
        (SELECT bj.idjenisklinik FROM bpjs_jenisklinik bj LEFT JOIN bpjs_klinikperujuk bk ON bj.idjenisklinik = bk.idjenisklinik  WHERE bk.idklinik=a.idklinikperujuk) as idjenisklinik,
        a.tahunperiksa
        FROM person_pendaftaran a 
        WHERE date(a.waktuperiksa) BETWEEN '2022-08-01' and '2022-08-31'
        AND (a.jenispemeriksaan = 'rajal' OR a.jenispemeriksaan = 'rajalnap') 
        AND a.carabayar != 'mandiri' 
        AND a.idstatuskeluar = 2 
        AND a.statustagihan != 'terbuka' 
        -- AND a.jenisrujukan != 'umum'
        AND a.jenisrujukan = 'rujukanbaru' 
        AND a.idklinikperujuk != 0";

        $query = $this->db->query($sql)->result();
        
        $idunits         = [];
        $idklinikperujuk = [];
        $namaunits       = [];
        $tahuns       = [];
        foreach( $query as $key => $val ){
            $idunits[$val->idunit] = $val->idunit;
            $idklinikperujuk[$val->idklinikperujuk] = $val->idklinikperujuk;
            $namaunits[$val->idunit] = $val->namaunitrs;
            $tahuns[$val->idunit] = $val->tahunperiksa;
        }

        $idunits_reorder         = array_values($idunits);
        $idklinikperujuk_reorder = array_values($idklinikperujuk);
        $namaunitrs_reorder      = array_values($namaunits);
        $tahun_reorder           = array_values($tahuns);
            
        $data = [];
        foreach( $idunits_reorder as $key => $unit ){
            $count_puskesmas = 0;
            $count_rs = 0;
            $count_without_rs_puskesmas = 0;

            $puskesmas = [];
            $rs = [];
            $without_rs_puskesmas = [];
            foreach( $query as $rowquery ){
                // Puskesmas
                if( 
                    $unit == $rowquery->idunit 
                    // AND $idklinikperujuk_reorder[$key] == $rowquery->idklinikperujuk 
                    AND $rowquery->idjenisklinik == 2  
                ){
                    $puskesmas[$rowquery->idunit] = $count_puskesmas++;
                    // $puskesmas[] = $count_puskesmas++;
                }

                // // RS
                // if( $unit == $rowquery->idunit 
                //     AND $idklinikperujuk_reorder[$key] == $rowquery->idklinikperujuk 
                //     AND $rowquery->idjenisklinik == 1  ){
                //     $rs[$rowquery->idunit] = $count_rs++;
                // }

                // // Lain-lain
                // if( $unit == $rowquery->idunit 
                //     AND $idklinikperujuk_reorder[$key] == $rowquery->idklinikperujuk 
                //     AND $rowquery->idjenisklinik != 1 AND $rowquery->idjenisklinik != 2  ){
                //     $without_rs_puskesmas[$rowquery->idunit] = $count_without_rs_puskesmas++;
                // }
            }

            // $data[] = [
            //     'unit'=>$namaunitrs_reorder[$key],
            //     'tahun'=>$tahun_reorder[$key],
            //     'puskesmas'=> ( empty( $puskesmas ) ) ? 0 : $puskesmas[$unit],
            //     'rs'=> ( empty( $rs ) ) ? 0 : $rs[$unit],
            //     'without_rs_puskesmas'=> ( empty( $without_rs_puskesmas ) ) ? 0 : $without_rs_puskesmas[$unit],
            //     't' => $puskesmas 
            // ]; 

            $data[] = ['t'=>$puskesmas];
        }

        
        echo json_encode( $data );
    }
    
    // dipanggil di js_datatable
    public function rl53_jsondt()
    {
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->viewlaporanrl53($this->input->post('tgl1'),$this->input->post('tgl2')));
    }
    // dipanggil di js_datatable
    public function rl54_jsondt()
    {
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->viewlaporanrl54($this->input->post('tgl1'),$this->input->post('tgl2')));
    }


    // Cetak Hasil Laboratorium Rawat Jalan
    public function laboratorium_hasilpemeriksaan()
    {
        $post   = $this->input->post();
        $dtpost = ['idpemeriksaan'=>$post['z'],'idpendaftaran'=>$post['x'],'idunit'=>$post['y'],'idpaketpemeriksaan'=>$post['p'] ];
        $data   = $this->hasilpemeriksaanlab_getdata($dtpost);        
        echo json_encode($data);
    }
    
    //unduh hasil laborat
    public function download_pdflaborat($data)
    {  
        $post = explode('-', $data);
        $dtpost = [
            'idpemeriksaan'=>$post[0],
            'idpendaftaran'=>$post[1],
            'idunit'=>$post[2],
            'idpaketpemeriksaan'=>$post[3]
            ];
        $datapdf['paketlab'] = $post[3];
        $datapdf['data']     = $this->hasilpemeriksaanlab_getdata($dtpost);
        $this->load->view('report/pdf_laboratorium',$datapdf);
    }
    
    private function hasilpemeriksaanlab_getdata($dtpost)
    {
        $this->load->model('mviewql');        
        $data['waktu']     = $this->db->select('ifnull(waktusedangproses,"") periksa, ifnull(waktuok,"") selesai')->get_where('lab_waktu_pemeriksaan',['idpemeriksaan'=>$dtpost['idpemeriksaan']])->row_array(); 
        $data['identitas'] = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$dtpost['idpendaftaran']])->row_array();
        $data['hasillab']  = $this->mviewql->viewcetakhasilpemeriksaan(4,$dtpost['idunit'],$dtpost['idpendaftaran'],'periksa',$dtpost['idpemeriksaan'],$dtpost['idpaketpemeriksaan']);
        $data['namars']    = $this->mkombin->getKonfigurasi('namars');
        if($dtpost['idpaketpemeriksaan']=='00'){
         $data['formatcetak'] = $this->db->get_where('rs_jeniscetaklaboratorium',['kode'=>'DFLT'])->row_array();
        }else{
         $data['formatcetak'] = $this->db->select('rj.*, rpp.iscetakbarcode')->join('rs_jeniscetaklaboratorium rj','rj.idjeniscetak=rpp.idjeniscetak')->get_where('rs_paket_pemeriksaan rpp',['idpaketpemeriksaan'=>$dtpost['idpaketpemeriksaan']])->row_array();
        }
        return $data;
    }
    //end - Cetak Hasil Laboratorium Rawat Jalan
    
    //start - cetak hasil paket laboratorium ranap
    public function cetakpaketlaboratorium_rawatinap()
    {
        $this->load->model('mviewql');   
        $post = $this->input->post();
        $whererencana = (empty($post['y'])) ? ['idrencanamedispemeriksaan'=>$post['x'],'idpaketpemeriksaanparent'=>null] :  ['idrencanamedispemeriksaan'=>$post['x'],'idpaketpemeriksaan'=>$post['y']];
        $datarencana = $this->db->select('waktumulai, waktuselesai, idpendaftaran')->get_where('rs_inap_rencana_medis_hasilpemeriksaan',$whererencana)->row_array(); 
        $data['waktu'] = $datarencana;
        $data['identitas'] = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$datarencana['idpendaftaran']])->row_array();
        $data['hasillab']  = $this->mviewql->view_hasillaboratoriumrawatinap($post['x'],$post['mode'],$post['y']);
        $wherecetak = (empty($post['y'])) ? ['kode'=>'DFLT'] : ['idpaketpemeriksaan'=>$post['y']];
        $data['formatcetak'] = $this->db->select('rj.*')->join('rs_jeniscetaklaboratorium rj','rj.idjeniscetak=rpp.idjeniscetak')->get_where('rs_paket_pemeriksaan rpp',$wherecetak)->row_array();
        echo json_encode($data);
    }
    // end - cetak hasil laboratorium ranap
    

    // print elektromedik
    public function elektromedik_hasilpemeriksaan()
    {
        $this->load->model('mviewql');
        $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$this->input->post('x')])->row_array();
        $data['elektromedik']     = $this->mviewql->viewhasilpemeriksaan(5,$this->input->post('y'),$this->input->post('x'),'periksa');
        $data['diagnosa']         = $this->mviewql->viewhasilpemeriksaan(2,$this->input->post('y'),$this->input->post('x'),'periksa');
        $data['dokterpenilai']    = $this->mkombin->getKonfigurasi('drpenilairo');
        $data['sipdokterpenilai'] = $this->mkombin->getKonfigurasi('sippenilairo');
        $data['header'] = $this->mkombin->getKonfigurasi('headerro');
        echo json_encode($data);
    }
    
    public function ekokardiografi_hasilpemeriksaan()
    {
        $this->load->model('mviewql');
        $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$this->input->post('x')])->row_array();
        $data['ekokardiografi']   = $this->mviewql->viewhasilpemeriksaan(9,$this->input->post('y'),$this->input->post('x'),'periksa');
        $data['diagnosa']         = $this->mviewql->viewhasilpemeriksaan(2,$this->input->post('y'),$this->input->post('x'),'periksa');
        $data['header'] = $this->mkombin->getKonfigurasi('headerro');
        echo json_encode($data);
    }
    
//    SELECT * FROM `rs_hasilpemeriksaan` WHERE idpemeriksaan = 249389 and idpaketpemeriksaan is not null
    
    // print elektromedik ranap
    public function elektromedik_hasilpemeriksaanranap()
    {
        $this->load->model('mviewql');
        $idrencana       = $this->input->post('idrencana');
        $data['rencana'] = $this->db->query("SELECT b.idpendaftaran, a.keteranganradiologi, a.saranradiologi FROM rs_inap_rencana_medis_pemeriksaan a, rs_inap b WHERE b.idinap=a.idinap and a.idrencanamedispemeriksaan='".$idrencana."'")->row_array();
        $idpendaftaran = $data['rencana']['idpendaftaran'];
        
        $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
        $data['identitasranap']   = $this->mkombin->getIdentitasPasienRanap($idrencana);
        $data['elektromedik']     = $this->mviewql->view_hasilradiologiranap($idrencana);
        $data['diagnosa']         = $this->mviewql->view_hasildiagnosaranap($idrencana);
        $data['dokterpenilai']    = $this->mkombin->getKonfigurasi('drpenilairo');
        $data['sipdokterpenilai'] = $this->mkombin->getKonfigurasi('sippenilairo');
        $data['header'] = $this->mkombin->getKonfigurasi('headerro');
        echo json_encode($data);
    }

    // pelaporan SIHA ::  Sistem Informasi HIV AIDS (SIHA) 
    public function report_siha()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Laporan SIHA';
            $data['table_title'] = 'Pelayanan Rumah Sakit';
            $data['plugins']     = [PLUG_DATE ];
            $data['jsmode']      = 'laporan_siha';
            $data['mode']        = 'laporan_siha';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function siha_listdata()
    {
       $tgl1 = $this->input->post('tgl1');
       $tgl2 = $this->input->post('tgl2');
       echo json_encode($this->mviewql->siha_getdatapasien($tgl1,$tgl2));
    }

    // pelaporan CAKUPAN POLI
    public function report_cakupanpoli()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)) //lihat define di atas
        {
            $data                = $this->settinglaporan();
            $data['content_view']= 'report/v_laporan';
            $data['active_sub_menu']= 'pelayananrs';
            $data['title_page']  = 'Cakupan Poli';
            $data['table_title'] = '';
            $data['plugins']     = [PLUG_DATE ];
            $data['jsmode']      = 'report_cakupanpoli';
            $data['mode']        = 'report_cakupanpoli';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function cakupanpoli_listdata()
    {
       $jmlpasien = $this->mviewql->cakupanpoli_jumlahpasien($this->input->post('bulan'),$this->input->post('jenisperiksa'))->result();
       $bulan = ql_getmonth($this->input->post('bulan'));
       $tahun = ql_getyear($this->input->post('bulan'));
       $penyakit=$this->mviewql->get_diagnosatersering($tahun,null,$bulan,10,2)->result();
       $tindakan=$this->mviewql->get_diagnosatersering($tahun,null,$bulan,10,3)->result();
       echo json_encode(['jumlahpasien'=>$jmlpasien,'penyakit'=>'','tindakan'=>'']);
    }
    public function cakupanpoli_unduhexcel($value)
    {
       $value = qlreplace('%20', ' ', $value);
       $bulan = ql_getmonth($value);
       $tahun = ql_getyear($value);
       $data['waktu']     = date('M-Y', strtotime($value)); 
       $data['judul']     = 'CAKUPAN POLI '.$value; 
       $data['jmlpasien'] = $this->mviewql->cakupanpoli_jumlahpasien($bulan,$tahun)->result();
       $data['penyakit']  = $this->mviewql->get_diagnosatersering($tahun,null,$bulan,10,2)->result();
       $data['tindakan']  = $this->mviewql->get_diagnosatersering($tahun,null,$bulan,10,3)->result();
       return $this->load->view('report/cakupanpoli_unduhexcel',$data);
    }

    public function  cakupanperpoli()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORAN_CAKUPANPERPOLI)) //lihat define di atas
        {
            $data = [
                'active_menu'       => 'report',
                'active_menu_level' => '',
                'title_page'        =>'Cakupan Poli',
                'script_js'         => ['js_cakupanperpoli'],
                'content_view'      =>'report/v_cakupanperpoli',
                'active_sub_menu'   =>'cakupanperpoli',
                'plugins'           =>[PLUG_DATE, PLUG_CHECKBOX]
               ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function cakupanperpoli_listdata()
    {
       $tanggal = $this->input->post('t');
       $mode=$this->input->post('o');
       echo json_encode($this->mviewql->cakupanperpoli($mode,$tanggal)->result());
    }
    public function cakupanperpoli_unduhexcel($value)
    {
       $value = qlreplace('%20', ' ', $value);
       $bulan = ql_getmonth($value);
       $tahun = ql_getyear($value);
       $data['waktu']     = date('M-Y', strtotime($value)); 
       $data['judul']     = 'CAKUPAN POLI '.$value; 
       $data['jmlpasien'] = $this->mviewql->cakupanpoli_jumlahpasien($bulan,$tahun)->result();
       return $this->load->view('report/cakupanpoli_unduhexcel',$data);
    }

    // mahmud clear :: pelaporan demografikunjunganpasien
    public function demografikunjunganpasien()
    {
        $data['content_view'] = 'report/demografikunjunganpasien_grafik';
        $data['title_page']   = 'Demografi Kunjungan Pasien';
        $data['active_menu']  = 'report';
        $data['active_sub_menu']   = 'demografikunjunganpasien';
        $data['active_menu_level'] = '';
        $data['jsmode']   = 'chart_demografikunjunganpasien'; 
        $data['plugins']   = [PLUG_CHART, PLUG_DROPDOWN, PLUG_DATE];
        $data['script_js'] = ['report/demografikunjunganpasien_grafik'];
        $this->load->view('v_index',$data);
    }
    // mahmud clear::ambil data demografi kunjungan pasien pendaftaran
    public function getdata_demografipasien()
    {
        $this->load->model('mkombin');
        $mode= $this->input->post('mode');
        $jns = $this->input->post('jenistampilan');
        $jnsl = $this->input->post('jenislaporan');
        $jenistampilan = empty($jns) ? 'pasien'  : $jns ;
        $jenislaporan = empty($jnsl) ? 'Agama' : $jnsl ;
        $tanggal1 = date('Y-m-d', strtotime($this->input->post('tanggal1')));
        $tanggal2 = date('Y-m-d', strtotime($this->input->post('tanggal2')));
        $jointbl='';$wherejointbl='';$select='';
        $labels=[]; $dtchart=[]; $backgroundColor=[];$total=0;
        // jika jenis tampilan wilayah
        if($jenistampilan!=='pasien'){$this->getdata_demografiberdasarkanwilayah($tanggal1,$tanggal2,$jenislaporan);}
        else
        {
        // jika jenis tampilan pasien
        if($jenislaporan=='Agama'):
            $label='pper.agama';
        elseif($jenislaporan=='Jenis Kelamin'):
            $label='pper.jeniskelamin';
        elseif ($jenislaporan=='Status Pernikahan'):
            $label='pper.statusmenikah';
        elseif ($jenislaporan=='Pendidikan'):
            $select = ',dpen.namapendidikan as labels ';
            $label  ='pper.idpendidikan';
            $jointbl=',demografi_pendidikan dpen';
            $wherejointbl=' and dpen.idpendidikan = pper.idpendidikan';
        elseif ($jenislaporan=='Pekerjaan'):
            $select = ',dpek.namapekerjaan as labels ';
            $label='pper.idpekerjaan';
            $jointbl=',demografi_pekerjaan dpek';
            $wherejointbl=' and dpek.idpekerjaan = pper.idpekerjaan';
        endif;

         // chart data
        if($jenislaporan!='Usia'):
            $sql = $this->db->query("select count(1) as data, ".$label." as labels ".$select."from person_pendaftaran ppend, person_pasien ppas, person_person pper ".$jointbl." where ppend.idstatuskeluar='2' and date(ppend.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and ppas.norm=ppend.norm and pper.idperson=ppas.idperson ".$wherejointbl." GROUP by ".$label)->result_array();
            // jenis laporan selain usia 
            $no=0;
            foreach ($sql as $arry) {
                $labels[]=$arry['labels'];
                $dtchart[]=$arry['data'];
                $total += $arry['data'];
                $backgroundColor[]=qlwarna(++$no);
            }
           
            $result = ['tanggal1'=>date('d F Y', strtotime($tanggal1)),'tanggal2'=>date('d F Y', strtotime($tanggal2)),'labels'=>$labels,'dtchart'=>$dtchart,'backgroundColor'=>$backgroundColor,'total'=>$total];
        else :
            $sql = $this->demografipasien_byusia($tanggal1,$tanggal2)->row_array();
            
            for ($x=1; $x<=8;$x++){$dtchart[] = [$sql['data'.$x]];$backgroundColor[]=qlwarna($x);$total+=$sql['data'.$x];}
            $labels  = ['0 - 6 Hari', '7 - 28 Hari','28 - 364 Hari','1 - 4 Tahun', '5 - 14 Tahun','15 - 24 Tahun','45 - 64 Tahun','> 65 Tahun'];
            $result  = ['tanggal1'=>date('d F Y', strtotime($tanggal1)),'tanggal2'=>date('d F Y', strtotime($tanggal2)),'labels'=>$labels,'dtchart'=>$dtchart,'backgroundColor'=>$backgroundColor,'total'=>$total];
            // echo json_encode($result);
            // return;
        endif;
        echo json_encode($result);
        }
    }
    private function demografipasien_byusia($tanggal1,$tanggal2)
    {
        return $this->db->query("SELECT sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) < 7, 1, 0)) as data1,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) > 6 and DATEDIFF(now(),pper.tanggallahir) < 29, 1, 0)) as data2,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) > 27 and DATEDIFF(now(),pper.tanggallahir) < 365, 1, 0)) as data3,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 365 and DATEDIFF(now(),pper.tanggallahir) <= 1460, 1, 0)) as data4,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 1825 and DATEDIFF(now(),pper.tanggallahir) <= 5110, 1, 0)) as data5,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 5475 and DATEDIFF(now(),pper.tanggallahir) <= 8760, 1, 0)) as data6,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 9125 and DATEDIFF(now(),pper.tanggallahir) <= 16060, 1, 0)) as data7,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 16425 and DATEDIFF(now(),pper.tanggallahir) <= 23360, 1, 0)) as data8,
                "."sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and DATEDIFF(now(),pper.tanggallahir) >= 23725 , 1, 0)) as data9
                "."from person_pendaftaran pp, person_pasien ppas, person_person pper
                "."where pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."' and ppas.norm = pp.norm and pper.idperson = ppas.idperson");
    }

    private function getdata_demografiberdasarkanwilayah($tgl1,$tgl2,$jenislaporan)
    {
        $this->load->model('mkombin');
        $datasets = [];
        $data=[];
        if($jenislaporan=='Usia')
        {
            $sql=$this->demografiwilayahpasien_byusia($tgl1,$tgl2)->result_array();
            $label  = ['0 - 6 Hari', '7 - 28 Hari','28 - 364 Hari','1 - 4 Tahun', '5 - 14 Tahun','15 - 24 Tahun','25 - 44 Tahun','45 - 64 Tahun','> 65 Tahun'];
            $no=0;
            $total=0;
            $dtlabel=[];
            $labels=[];
            for($x=0;$x<count($sql);$x++){$labels[]=$sql[$x]['namakabupatenkota'];}
            for($a=1;$a<=count($label);$a++)
            {
                for ($c=0;$c<count($labels);$c++)
                {
                    for($b=0;$b<count($sql);$b++)
                    {
                            $data[]=$sql[$b]['data'.$a];
                            $total+=$sql[$b]['data'.$a];
                    }
                    
                }
                $datasets[]=['label'=>$label[$a-1],'backgroundColor'=>qlwarna(++$no),'data'=>$data];
                $data=[];
            }

        }
        else
        {
            // masih ada bugs ketika ada wilayah yang tidak ada datanya maka data tidak sesuai dengan wilayah yang seharusnya
            $dtwilayah = $this->db->query("select ifnull(gkab.namakabupatenkota,'Lainya') as namakabupatenkota
                from person_pendaftaran ppend
                join person_pasien ppas on ppas.norm=ppend.norm
                join person_person pper on pper.idperson=ppas.idperson
                left join geografi_desakelurahan gk on gk.iddesakelurahan = pper.iddesakelurahan
                left join geografi_kecamatan gkc on gkc.idkecamatan =  gk.idkecamatan
                left join geografi_kabupatenkota gkab on gkab.idkabupatenkota = gkc.idkabupatenkota
                
                where ppend.idstatuskeluar='2' and date(ppend.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and ppas.norm=ppend.norm and pper.idperson=ppas.idperson GROUP by gkab.idkabupatenkota")->result_array();
            // $dtwilayah[] = ['namakabupatenkota'=>'Lainya'];
            foreach ($dtwilayah as $arr) {$labels[]=$arr['namakabupatenkota'];}
            // jika  jenis laporan tidak berdasarkan usia
            $jointbl='';$select='';$label='';
            // ubah label sesuai dengan jenis laporan
            if($jenislaporan=='Agama'):
                $label='pper.agama';
                $select =$label;
                $dtlabel = $this->mkombin->get_data_enum('person_person','agama');
            elseif($jenislaporan=='Jenis Kelamin'):
                $label='pper.jeniskelamin';
                $select = $label;
                $dtlabel = $this->mkombin->get_data_enum('person_person','jeniskelamin');
            elseif ($jenislaporan=='Status Pernikahan'):
                $label='pper.statusmenikah';
                $select = $label;
                $dtlabel = $this->mkombin->get_data_enum('person_person','statusmenikah');
            elseif ($jenislaporan=='Pendidikan'):
                $select = ' dpen.namapendidikan ';
                $label  ='pper.idpendidikan';
                $jointbl='join demografi_pendidikan dpen on dpen.idpendidikan=pper.idpendidikan';
                $dtpekerjaan = $this->db->get('demografi_pendidikan')->result();
                $dtlabel=[];foreach ($dtpekerjaan as $obj){$dtlabel[]=$obj->namapendidikan;}
            elseif ($jenislaporan=='Pekerjaan'):
                $select = ' dpek.namapekerjaan ';
                $label='pper.idpekerjaan';
                $jointbl='join demografi_pekerjaan dpek on dpek.idpekerjaan=pper.idpekerjaan';
                $dtpekerjaan = $this->db->get('demografi_pekerjaan')->result();
                $dtlabel=[];foreach ($dtpekerjaan as $obj){$dtlabel[]=$obj->namapekerjaan;}
            endif;

            
            // , ifnull(gkab.namakabupatenkota,'Lainya') as namakabupatenkota
            // , gkab.idkabupatenkota
            $sql = $this->db->query("select count(1) as data, ".$select." as label, ifnull(gkab.namakabupatenkota,'Lainya') as namakabupatenkota
                "."from person_pendaftaran ppend
                "."join person_pasien ppas on ppas.norm=ppend.norm
                "."join person_person pper on pper.idperson=ppas.idperson
                "."left join geografi_desakelurahan gk on gk.iddesakelurahan = pper.iddesakelurahan
                "."left join geografi_kecamatan gkc on gkc.idkecamatan =  gk.idkecamatan
                "."left join geografi_kabupatenkota gkab on gkab.idkabupatenkota = gkc.idkabupatenkota
                ".$jointbl."
                "."where ppend.idstatuskeluar='2' and date(ppend.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and ppas.norm=ppend.norm and pper.idperson=ppas.idperson GROUP by ".$label.",gkab.idkabupatenkota")->result_array();
            $no=0;
            for($a=0; $a<count($dtlabel); $a++)
            {
                for($b=0; $b<count($labels);$b++)
                {   $total=0;
                    for ($x=0; $x<count($sql);$x++) {
                        if($sql[$x]['label']==$dtlabel[$a] && $sql[$x]['namakabupatenkota']==$labels[$b])
                        {
                            $data[]=$sql[$x]['data'];
                        }
                    $total+=$sql[$x]['data'];
                    }    
                }
                $datasets[]=['label'=>$dtlabel[$a],'backgroundColor'=>qlwarna(++$no),'data'=>$data];
                $data=[];
            }
        }
        $dtchart = [
            'labels'=>$labels,
            'datasets'=>$datasets
        ];
        $result  = ['tanggal1'=>date('d F Y', strtotime($tgl1)),'tanggal2'=>date('d F Y', strtotime($tgl2)),'dtchart'=>$dtchart,'total'=>$total];
        echo json_encode($result);

    }
    private function demografiwilayahpasien_byusia($tgl1,$tgl2)
    {
        return $this->db->query("SELECT sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) < 7, 1, 0)) as data1,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) > 6 and DATEDIFF(now(),pper.tanggallahir) < 29, 1, 0)) as data2,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) > 27 and DATEDIFF(now(),pper.tanggallahir) < 365, 1, 0)) as data3,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 365 and DATEDIFF(now(),pper.tanggallahir) <= 1460, 1, 0)) as data4,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 1825 and DATEDIFF(now(),pper.tanggallahir) <= 5110, 1, 0)) as data5,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 5475 and DATEDIFF(now(),pper.tanggallahir) <= 8760, 1, 0)) as data6,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 9125 and DATEDIFF(now(),pper.tanggallahir) <= 16060, 1, 0)) as data7,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 16425 and DATEDIFF(now(),pper.tanggallahir) <= 23360, 1, 0)) as data8,
                sum(if(pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and DATEDIFF(now(),pper.tanggallahir) >= 23725 , 1, 0)) as data9, ifnull(gkab.namakabupatenkota,'Lainya') as namakabupatenkota
                from person_pendaftaran pp
                join person_pasien ppas on ppas.norm=pp.norm
                join person_person pper on pper.idperson=ppas.idperson
                left join geografi_desakelurahan gk on gk.iddesakelurahan = pper.iddesakelurahan
                left join geografi_kecamatan gkc on gkc.idkecamatan =  gk.idkecamatan
                left join geografi_kabupatenkota gkab on gkab.idkabupatenkota = gkc.idkabupatenkota
                where pp.idstatuskeluar='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and ppas.norm = pp.norm and pper.idperson = ppas.idperson
                GROUP by gkab.idkabupatenkota");
    }

    // laporankeuangan
    
    public function settinglaporankeuangan(){
        return [
            'active_menu'      => 'report',
            'active_menu_level'=> '',
            'active_sub_menu'  => 'laporankeuangan',
            'script_js'        => ['js_laporankeuangan'],
            'content_view'     => 'report/v_laporankeuangan'
        ];
    }
    public function laporankeuangan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            
            $data = $this->settinglaporankeuangan();
            $data['title_page']       = 'Laporan Keuangan';
            $data['mode']             = 'laporankeuangan';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    public function keuangan_logsetelulangpembayaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $this->load->model('mkombin');
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Data Log <i>Reset</i> Pembayaran';
            $data['mode']        = 'logsetelulangpembayaran';
            $data['jsmode']      = 'logsetelulangpembayaran';
            $data['plugins']     = [PLUG_DATE, PLUG_DATATABLE ];
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function list_resetpembayaran()
    {
        $tahunbulan = date('Y-m', strtotime($this->input->post('tahunbulan')));
        $tgl1 = $tahunbulan.'-01';
        $tgl2 = $tahunbulan.'-31';
        $data = $this->db->query("SELECT a.*, b.norm, namapasien(d.idperson) as namapasien, c.namauser FROM log_resetpembayaran a join person_pendaftaran b on b.idpendaftaran = a.idpendaftaran join person_pasien d on d.norm = b.norm join login_user c on c.iduser = a.iduser where date(a.waktu) BETWEEN '".$tgl1."' and '".$tgl2."'")->result();        
        echo json_encode($data);   
    }
    
    /**
     * Pendapatan Potong Gaji
     */
    public function keuangan_pendapatanpotonggaji()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $this->load->model('mkombin');
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Pendapatan Potong Gaji';
            $data['mode']        = 'pendapatanpotonggaji';
            $data['jsmode']      = 'pendapatanpotonggaji';
            $data['plugins']     = [PLUG_DATE, PLUG_DATATABLE ];
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function list_keuangan_pendapatanpotonggaji()
    {
        $tgl1 = $this->input->post('tanggal1');
        $tgl2 = $this->input->post('tanggal2');
        $data['ralan']     = $this->mviewql->view_pendapatan_potonggaji_ralan($tgl1,$tgl2)->result();        
        $data['obatbebas'] = $this->mviewql->view_pendapatan_potonggaji_penjualanbebas($tgl1,$tgl2)->result();        
        echo json_encode($data);        
    }
    
    public function excel_pendapatan_tongjiralan()
    {
        $data['tgl1']   = $this->uri->segment(3);
        $data['tgl2']   = $this->uri->segment(4);
        $data['ralan']  = $this->mviewql->view_pendapatan_potonggaji_ralan($data['tgl1'],$data['tgl2'])->result();  
        $data['title']  = 'Pendapatan Potong Gaji Pasien Rawat Jalan - '.$data['tgl1'].' - '.$data['tgl2'];
        return $this->load->view('report/excel_potonggajipasienrawatjalan',$data);
    }
    
    public function excel_pendapatan_tongjiobatbebas()
    {
        $data['tgl1']   = $this->uri->segment(3);
        $data['tgl2']   = $this->uri->segment(4);
        $data['penjualanbebas']  = $this->mviewql->view_pendapatan_potonggaji_penjualanbebas($data['tgl1'],$data['tgl2'])->result();  
        $data['title']  = 'Pendapatan Potong Gaji Penjualan Obat Bebas - '.$data['tgl1'].' - '.$data['tgl2'];
        return $this->load->view('report/excel_potonggajipenjualanobatbebas',$data);
    }
    
    /**
     * Dashboard Pendapatan
     */
    
    public function dashboard_pendapatan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $this->load->model('mkombin');
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Dashboard Pendapatan';
            $data['mode']        = 'dashboardpendapatan';
            $data['jsmode']      = 'dashboardpendapatan';
            $data['plugins']     = [PLUG_DATE ];
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function list_dashboardpendapatan()
    {
        $tanggal = $this->input->post('tanggal');
        //pendapatan ralan by jenis tagihan
        $q_tagihanralan = "select pp.carabayar, k.statusbayar, concat('SHIFT ',k.levelshif) as shif ,(select namauser from login_user l WHERE l.iduser=k.iduserkasir) as kasir,sum(k.nominal) as tagihan, sum(k.nominal-k.potongan) as totaltagihan, sum(k.dibayar - k.kembalian) as dibayar, if(k.statusbayar='dibayar',k.jenispembayaran,k.jenistagihan) as jenistagihan  "
                . "FROM keu_tagihan k,person_pendaftaran pp "
                . "WHERE k.tanggalshif ='".$tanggal."' and k.jenistagihan!='batal' and k.jenisperiksa='rajal' and pp.idpendaftaran=k.idpendaftaran "
                . "GROUP by k.levelshif, k.jenispembayaran, k.statusbayar, k.iduserkasir, pp.carabayar "
                . "ORDER by k.levelshif asc, k.iduserkasir desc";
        
        $data['thr'] = $this->db->query($q_tagihanralan)->result_array();
        
        //pendapatan obat bebas 
        $q_obatbebas = "select k.levelshif, 
            (select namauser from login_user l where l.iduser=k.iduserkasir) as kasir, 
            sum(k.nominal - k.potongan) as tagihan, 
            sum(k.dibayar - k.kembalian) as dibayar,
            k.carabayar,
            k.jenistagihan
        FROM keu_tagihan_bebas k 
        where k.tanggalshif='".$tanggal."' and k.status='selesai'
        GROUP by k.iduserkasir, k.levelshif, k.jenistagihan, k.carabayar ORDER by k.levelshif asc";
        $data['tob'] = $this->db->query($q_obatbebas)->result_array();
        
        //pendapatan ralan by poliklinik
        $q_tagihanralanbypoli = "SELECT count(1) as jumlahpasien, kt.levelshif,(select namauser from login_user l where l.iduser=kt.iduserkasir) as kasir,
        ru.namaunit as namaunit, pp.carabayar,kt.jenistagihan,
        if(kt.jenistagihan='jknpbi' or kt.jenistagihan='jknnonpbi' ,sum(kt.nominal - kt.potongan) ,0) as bpjs,
        if(kt.jenistagihan!='jknpbi' and kt.jenistagihan!='jknnonpbi' , sum(kt.nominal - kt.potongan),0) as umum
        FROM keu_tagihan kt , person_pendaftaran pp , rs_unit ru
        WHERE kt.tanggalshif = '".$tanggal."' and kt.jenistagihan!='batal' and pp.idpendaftaran=kt.idpendaftaran and ru.idunit=pp.idunit
        GROUP BY pp.idunit , pp.carabayar, kt.levelshif ORDER by kt.levelshif, ru.namaunit asc";
        $data['thrp'] = $this->db->query($q_tagihanralanbypoli)->result_array();
        echo json_encode($data);
    }
    
    //-- status pemeriksaan pasien rawat jalan
    public function statuspemeriksaanpasienralan()
    {
        $this->load->model('mkombin');
        $data['active_menu']       = 'report';
        $data['active_menu_level'] = '';
        $data['active_sub_menu']   = 'statuspemeriksaanpasienralan';
        $data['script_js']         = ['report/js_statuspemeriksaanpasienralan'];
        $data['content_view']      = 'report/v_laporanstatuspemeriksaanralan';
        $data['title_page']        = 'Status Pemeriksaan Pasien RALAN';
        $data['carabayar']         = $this->mkombin->get_data_enum('person_pendaftaran','carabayar');
        $data['plugins']           = [PLUG_DATE, PLUG_DATATABLE, PLUG_DROPDOWN];
        $this->load->view('v_index',$data);
    }
    
    public function dt_statuspemeriksaanpasienralan()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_statuspemeriksaanpasienralan_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                    $row[] = $obj->norm;
                    $row[] = $obj->namapasien;
                    $row[] = $obj->nosep;
                    $row[] = $obj->waktu;
                    $row[] = $obj->waktuperiksa;
                    $row[] = $obj->namaunit;
                    $row[] = $obj->carabayar;
                    $row[] = $obj->jenispemeriksaan;
                    $row[] = $obj->status;
                    $row[] = $obj->detailperiksa;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_statuspemeriksaanpasienralan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_statuspemeriksaanpasienralan(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }


    // laporankeuangan :: jasa medis
    public function keuangan_jasamedis()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $this->load->model('mkeuangan');
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Laporan Jasa Medis';
            $data['mode']        = 'jasamedis';
            $data['jsmode']      = 'jasamedis';
            $data['dokter']      = $this->mkombin->getdatadokter();
            $data['laporan']     = $this->mkeuangan->getdata_jasamedisralan();
            $data['plugins']     = [ PLUG_DROPDOWN, PLUG_DATE ];
            $data['get']         = $this->input->get();
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function keuangan_jasamedis_excel()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $this->load->model('mkeuangan');
            $get = $this->input->get();
            $tanggal = (($get['tanggal1'] == $get['tanggal2']) ? $get['tanggal1'] : $get['tanggal1'].' sampai '.$get['tanggal2'] );
            $data['laporan']    = $this->mkeuangan->getdata_jasamedisralan();
            
            $namadokter = str_replace(',', '', str_replace('.', '', $get['namadokter']) );
            $data['fileName']   = 'Laporan Jasamedis '.$namadokter.' '.$tanggal;
            $this->load->view('report/excel_lapjasamedis',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Keuangan Pendapatan Rawat Jalan
     */
    public function keuangan_pendapatanrajal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Pendapatan Ralan';
            $data['mode']        = 'pendapatanrajal';
            $data['jsmode']      = 'pendapatanrajal';
            $data['dokter']      = $this->mkombin->getdatapegawai()->result();
            $data['carabayar']   = $this->mkombin->get_data_enum('person_pendaftaran','carabayar');
            $data['unit']        = $this->db->query("SELECT * FROM rs_unit where tipeunit='UTAMA' OR tipeunit='PENUNJANG'")->result();
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * kasir Pendapatan Rawat Jalan
     */
    public function kasir_pendapatanrajal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Rekap Pendapatan Ralan';
            $data['mode']        = 'kasirpendapatanrajal';
            $data['jsmode']      = 'kasirpendapatanrajal';
            $data['dokter']      = $this->mkombin->getdatapegawai()->result();
            $data['carabayar']   = $this->mkombin->get_data_enum('person_pendaftaran','carabayar');
            $data['unit']        = $this->db->query("SELECT * FROM rs_unit where tipeunit='UTAMA' OR tipeunit='PENUNJANG'")->result();
            $data['shif']        = [1=>'Shif 1',2=>'Shif 2',3=>'Shif 3'];
            $data['user']        = $this->db->query("SELECT x.namauser, x.iduser FROM login_hakaksesuser a, login_user x where find_in_set(9,a.peranperan) and x.iduser=a.iduser")->result();
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Keuangan Pendapatan Obat Bebas
     */
    public function keuangan_penjualanbebas()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)) //lihat define di atas
        {
            $data = $this->settinglaporankeuangan();
            $data['title_page']  = 'Pendapatan Obat Bebas';
            $data['mode']        = 'pendapatanobatbebas';
            $data['jsmode']      = 'pendapatanobatbebas';
            $data['user']        = $this->db->query("SELECT x.namauser, x.iduser FROM login_hakaksesuser a, login_user x where find_in_set(9,a.peranperan) and x.iduser=a.iduser")->result();
            $data['shif']        = [1=>'Shif 1',2=>'Shif 2',3=>'Shif 3'];
            $data['jenistagihan']= ['tagihan','dibayar','hutang','terbuka'];
            $data['plugins']     = [PLUG_DATE];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**Get Data Laporan Penjualan Obat Bebas**/
    public function datapendapatanobatbebas($unduh='')
    {
        $data['date1'] = ((empty($unduh))? ((empty($this->input->post('date1')))? date('Y-m-d'):$this->input->post('date1')) :$this->uri->segment(3));
        $data['date2'] = ((empty($unduh))? ((empty($this->input->post('date2')))? date('Y-m-d'):$this->input->post('date2')) :$this->uri->segment(4));
        $data['shif'] = ((empty($unduh))? $this->input->post('shif') :$this->uri->segment(6) );
        $data['user'] = ((empty($unduh))? $this->input->post('user') :$this->uri->segment(5) );
        $data['jenistagihan'] = ((empty($unduh)) ? $this->input->post('jenistagihan') : $this->uri->segment(7));
        $whereshif = empty($data['shif']) ? '' : " and ktb.levelshif='".$data['shif']."'" ;
        $whereuser = empty($data['user']) ? '' : " and ktb.iduserkasir='".$data['user']."'";
        $wherejenistagihan = empty($data['jenistagihan']) ? '' :" and ktb.jenistagihan='".$data['jenistagihan']."' " ;
        $data['laporan'] = $this->db->query("
            SELECT if(rbp.idpegawai is null,rbp.nama, namadokter(rbp.idpegawai)) as pembeli, ktb.waktubayar, ktb.nominal, ktb.potongan, ktb.dibayar, ktb.kekurangan, ((ktb.dibayar + ktb.potongan) - ktb.nominal) kembali, ktb.jenistagihan, ktb.keterangan, ktb.status 
            FROM keu_tagihan_bebas ktb 
            join rs_barang_pembelibebas rbp on rbp.idbarangpembelibebas=ktb.idbarangpembelibebas WHERE date(ktb.waktubayar) between '".$data['date1']."' and '".$data['date2']."' ". $whereshif. $whereuser. $wherejenistagihan." ORDER by ktb.idtagihan DESC")->result_array();
        
        if(!empty($unduh))  //unduh data (excel)
        {
            $data['petugas'] = $this->db->get_where('login_user',['iduser'=>$data['user']])->row_array();
            
            $data['title'] ='Laporan Pendapatan Penjualan Bebas - user'.$data['petugas']['namauser'].' '.$data['date1'].' - '.$data['date2'];
            return $this->load->view('report/excel_pendapatanobatbebas',$data);
        }
        echo json_encode($data['laporan']);//tampil data
    }
    
    public function unduh_pendapatanrawatjalan()
    {
        $this->pendapatanrajaljs('unduh');
    }
    
    public function pendapatanrajaljs($unduh='')
    {
        $data['date']   = ((empty($unduh))? ((empty($this->input->post('date')))? date('Y-m-d'):$this->input->post('date')) :$this->uri->segment(4));
        $data['date2']   = ((empty($unduh))? ((empty($this->input->post('date2')))? date('Y-m-d'):$this->input->post('date2')) :$this->uri->segment(5));
        
        $selisihtanggal =  dateDifference($data['date'],$data['date2'],'%d');
        if($data['date'] != $data['date2'] && $selisihtanggal > 6)
        {
            $data['date2'] = ql_tanggaltujuan('6 day',$data['date']);
        }
        
        $data['idunit'] = ((empty($unduh))? $this->input->post('unit') : $this->uri->segment(6));
        $pilihcarabayar = ((empty($unduh))? $this->input->post('carabayar') : $this->uri->segment(7));
        $carabayar      = [$pilihcarabayar];
        
        
        $data['unit']   = $this->db->get_where('rs_unit',['idunit'=>$data['idunit']])->row_array();
        
            $this->load->model('mmasterdata');
            $data['carabayar']  = empty($pilihcarabayar) ? $this->mmasterdata->get_data_enum('person_pendaftaran','carabayar') : $carabayar;
            $data['jenistarif'] = $this->db->get_where('rs_jenistarif',['istarifralan'=>1])->result();
            $data['pasien']   = [];
            $data['tindakan'] = [];
            
            for($i=0; $i < count($data['carabayar']);$i++)
            {
                $data['pasien'][] = $this->db->query("select pp.idpendaftaran, namapasien((select idperson from person_pasien where norm=pp.norm )) namalengkap, pp.carabayar, namadokter((select rp.idpegawaidokter from rs_pemeriksaan rp where rp.idpendaftaran=pp.idpendaftaran and idpemeriksaansebelum is null )) namadokter,ifnull(if(kt.jenistagihan!='transfer' and kt.jenistagihan!='batal' , sum(kt.dibayar - kt.kembalian) , 0 ),0) as dibayar,ifnull(if(kt.jenistagihan='transfer' and kt.jenistagihan!='batal' , sum(kt.dibayar - kt.kembalian) , 0 ),0) as transfer,ifnull(if(kt.jenistagihan!='batal' , sum(kt.potongan) , 0 ),0) as potongan from person_pendaftaran pp left join keu_tagihan kt on kt.idpendaftaran=pp.idpendaftaran and kt.statusbayar='dibayar' WHERE pp.idstatuskeluar=2 and date(pp.waktuperiksa) >= '".$data['date']."' and  date(pp.waktuperiksa) <= '".$data['date2']."' and (pp.jenispemeriksaan='rajal' or pp.jenispemeriksaan='rajalnap') and ".((empty($data['unit']))?"":" pp.idunit='".$data['idunit']."'  and ")." pp.carabayar='".$data['carabayar'][$i]."' GROUP by pp.idpendaftaran")->result(); // 
                $no=0;
                $tindakan2=[];
                foreach ($data['pasien'][$i] as $pasien)
                {
                    $tindakan=[];
                    foreach ($data['jenistarif'] as $tarif)
                    {
                        $tindakan[] = $this->db->query( (($tarif->jenislayanan=='farmasi')? "select idjenistarif, sum(total) as total, jaminanasuransi from rs_barangpemeriksaan where idpendaftaran='".$pasien->idpendaftaran."' and total > 0 and istagihan=1 and idjenistarif='".$tarif->idjenistarif."'" : "select idjenistarif, sum(total) as total, jaminanasuransi from rs_hasilpemeriksaan where idpendaftaran='".$pasien->idpendaftaran."' and total > 0 and istagihan=1 and idjenistarif='".$tarif->idjenistarif."'" ) )->result_array();
                    }
                  $tindakan2[] = $tindakan;
                }
                $data['tindakan'][] = $tindakan2;
            }
            
        if(!empty($unduh))  //unduh excel
        {
            $data['title']   = 'Laporan Pendapatan Ralan '. ((empty($data['idunit'])) ? '' : $data['unit']['namaunit'] ).$data['date'].' - '.$data['date2'];
            return $this->load->view('report/excel_pendapatanralan',$data);
        }
        echo json_encode($data);
    }
    
    public function unduh_kasir_pendapatanrajaljs()
    {
        $this->kasir_pendapatanrajaljs('unduh');
    }
    
    public function kasir_pendapatanrajaljs($unduh='')
    {
        $data['date']   = ((empty($unduh))? ((empty($this->input->post('date')))? date('Y-m-d'):$this->input->post('date')) :$this->uri->segment(4));
        $data['date2']   = ((empty($unduh))? ((empty($this->input->post('date2')))? date('Y-m-d'):$this->input->post('date2')) :$this->uri->segment(5));
        
        $selisihtanggal =  dateDifference($data['date'],$data['date2'],'%d');
        if($data['date'] != $data['date2'] && $selisihtanggal > 6)
        {
            $data['date2'] = ql_tanggaltujuan('6 day',$data['date']);
        }
        
        $data['user']   = ((empty($unduh))? ((empty($this->input->post('user')))? $this->input->post('user'):$this->input->post('user')) :$this->uri->segment(8));
        $data['shif']   = ((empty($unduh))? $this->input->post('shif') : $this->uri->segment(9));
        $data['idunit'] = ((empty($unduh))? $this->input->post('unit') : $this->uri->segment(6));
//        $data['mode']   = ((empty($unduh))? $this->input->post('mode') : $this->uri->segment(3));
        $pilihcarabayar = ((empty($unduh))? $this->input->post('carabayar') : $this->uri->segment(7));
        $carabayar      = [$pilihcarabayar];
        
        
        $data['unit']   = $this->db->get_where('rs_unit',['idunit'=>$data['idunit']])->row_array();
        
        $this->load->model('mmasterdata');
            $data['carabayar']  = empty($pilihcarabayar) ? $this->mmasterdata->get_data_enum('person_pendaftaran','carabayar') : $carabayar;
            $data['jenistarif'] = $this->db->get_where('rs_jenistarif',['istarifralan'=>1])->result();
            $wherebyuser =  ((empty($data['user'])) ? '' : " kt.iduserkasir='".$data['user']."' and");
            $whereshif   =  ((empty($data['shif'])) ? '' : " kt.levelshif='".$data['shif']."' and");
            $data['pasien']   = [];
            $data['tindakan'] = [];
            
            for($i=0; $i < count($data['carabayar']);$i++)
            {
                $data['pasien'][] = $this->db->query("select pp.idpendaftaran, namapasien((select idperson from person_pasien where norm=pp.norm )) namalengkap, pp.carabayar, namadokter((select rp.idpegawaidokter from rs_pemeriksaan rp where rp.idpendaftaran=pp.idpendaftaran and idpemeriksaansebelum is null )) namadokter, 
                    ifnull( (select sum(dibayar - kembalian) from keu_tagihan where idpendaftaran=kt.idpendaftaran and jenistagihan != 'transfer' and jenistagihan != 'batal'),0) as dibayar,
                    ifnull( (select sum(dibayar - kembalian) from keu_tagihan where idpendaftaran=kt.idpendaftaran and jenistagihan = 'transfer' and jenistagihan != 'batal'),0) as transfer,
                    ifnull( (select sum(potongan) from keu_tagihan where idpendaftaran=kt.idpendaftaran and jenistagihan != 'batal'),0) as potongan from keu_tagihan kt left join person_pendaftaran pp on pp.idpendaftaran = kt.idpendaftaran  WHERE kt.tanggalshif >= '".$data['date']."' and  kt.tanggalshif <= '".$data['date2']."' and jenisperiksa='rajal' and pp.idstatuskeluar=2 and  (pp.jenispemeriksaan='rajal' or pp.jenispemeriksaan='rajalnap') and ".((empty($data['unit']))?"":" pp.idunit='".$data['idunit']."'  and ") .$wherebyuser.$whereshif." pp.carabayar='".$data['carabayar'][$i]."' GROUP by pp.idpendaftaran")->result(); // 
                $no=0;
                $tindakan2=[];
                foreach ($data['pasien'][$i] as $pasien)
                {
                    $tindakan=[];
                    foreach ($data['jenistarif'] as $tarif)
                    {
                        $tindakan[] = $this->db->query( (($tarif->jenislayanan=='farmasi')? "select idjenistarif, sum(total) as total, jaminanasuransi from rs_barangpemeriksaan where idpendaftaran='".$pasien->idpendaftaran."' and total > 0 and istagihan=1 and idjenistarif='".$tarif->idjenistarif."'" : "select idjenistarif, sum(total) as total, jaminanasuransi from rs_hasilpemeriksaan where idpendaftaran='".$pasien->idpendaftaran."' and total > 0 and istagihan=1 and idjenistarif='".$tarif->idjenistarif."'" ) )->result_array();
                    }
                  $tindakan2[] = $tindakan;
                }
                $data['tindakan'][] = $tindakan2;
            }
            
        if(!empty($unduh))  //unduh excel
        {
            $data['petugas'] = $this->db->get_where('login_user',['iduser'=>$data['user']])->row_array();
            $data['title']   = 'Laporan Rekap Pendapatan Ralan ' . ((empty($data['idunit'])) ? '' : $data['unit']['namaunit'] ) .((empty(!$data['user'])) ? 'user'.$data['petugas']['namauser'] : '' ) .' '.$data['date'];
            return $this->load->view('report/excel_pendapatanralan',$data);
        }
        echo json_encode($data); //kirimkan kejs
    }


    // HALAMAN NOTIFIKASI BARANG BELUM TERJUAL
    public function setting_notifikasi_laporan()
    {
        return [    'active_menu'       => 'notifikasi',
                    'content_view'      => 'report/v_notifikasi',
                    'active_menu_level' => '',
                    'script_js'         => ['report/js_notifikasi']
               ];
    }
    public function notif_barangbelumterjual()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGNOTIFIKASI)) //lihat define di atas
        {
            $data                   = $this->setting_notifikasi_laporan();
            $data['active_sub_menu']= '';
            $data['title_page']     = 'Barang Belum terjual';
            $data['plugins']        = [ PLUG_DATATABLE];
            $data['jsmode']         = 'barangbelumterjual';
            $data['mode']           = 'barangbelumterjual';
            $data['listbarang']     = $this->db->get_where('rs_barang','tanggaljual <= "'.$this->session->userdata('notif')['tanggaljual'].'"')->result();
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function laporan_tbcexcelnew()
    {
        $data['tbc']=$this->db->query("select pper.namalengkap,pp.norm, concat(pper.tanggallahir,'/',FLOOR(DATEDIFF(CURRENT_DATE, pper.tanggallahir)/365.25)) as tanggallahir, pper.jeniskelamin,pper.alamat, concat(ri.icd, ri.namaicd) as diagnosa, date(pp.waktuperiksa) as masuk, date(pp.waktuperiksa) as keluar from rs_hasilpemeriksaan rh, rs_icd ri, person_pendaftaran pp,person_pasien ppas, person_person pper where ri.icd=rh.icd and  ri.idjenispenyakit=5 and pp.idpendaftaran=rh.idpendaftaran and pp.tahunperiksa='2019' and pp.bulanperiksa BETWEEN '01' and '09' and ppas.norm=pp.norm and pper.idperson = ppas.idperson")->result();
       return $this->load->view('report/excel_pasienbyjenispenyakit',$data);
    }
    
    /* Diagnosa dan tindakan 10 besar */
    public function diagnosatindakan10besar()
    {
        $data = [
            'active_menu'       => 'report',
            'active_menu_level' => '',
            'title_page'        =>'Diagnosa dan Tindakan 10 Besar',
            'script_js'         => ['js_diagnosatindakan10besar'],
            'content_view'      =>'report/v_diagnosatindakan10besar',
            'active_sub_menu'   =>'diagnosatindakan10besar',
            'plugins'           =>[PLUG_DATE, PLUG_DROPDOWN]
           ];
        $this->load->view('v_index',$data);
    }


    // *********** PELAPORAN FARMASI *************
    // mahmud, clear
    function settinglaporanfarmasi()
    {   
        return [    'active_menu'       => 'report',
                    'active_sub_menu'   => 'farmasi',
                    'active_menu_level' => '',
                    'script_js'    => ['report/js_farmasi'],
                    'plugins'      => [ PLUG_DATATABLE,  PLUG_DATE , PLUG_DROPDOWN ],
                    'content_view' =>'report/v_laporanfarmasi'
               ];
    }
    function laporanfarmasi()
    {
        $data                = $this->settinglaporanfarmasi();
        $data['unitfarmasi'] = $this->db->get('rs_unit_farmasi')->result_array();
        $data['title_page']  = 'Pelaporan Unit Farmasi';
        // $data['jsmode']      = 'farmasi';
        // $data['mode']        = 'farmasi';
        $this->load->view('v_index',$data);
    }
    
    
    function dt_penggunaanbarangincludetindakan()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_penggunaanbarangincludetindakan_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row=[];
                    $row[] = $obj->waktudibuat;
                    $row[] = $obj->namauser;
                    $row[] = '['.$obj->kode.'] '.$obj->namabarang;
                    $row[] = $obj->jumlah;
                    $row[] = $obj->namasatuan;
                    $row[] = $obj->jenis;
                    $row[] = $obj->kekuatan;
                    $row[] = convertToRupiah($obj->hargajual);
                    $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_penggunaanbarangincludetindakan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_penggunaanbarangincludetindakan(),
            "data" =>$data
        );
        echo json_encode($output);
    }
    
    function list_slowmovingobat()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_slowmovingobat_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row=[];
                    $row[] = $obj->kode;
                    $row[] = $obj->namabarang;
                    $row[] = $obj->batchno;
                    $row[] = $obj->kadaluarsa;
                    $row[] = $obj->stok;
                    $row[] = $obj->namasatuan;
                    $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_slowmovingobat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_slowmovingobat(),
            "data" =>$data
        );
        echo json_encode($output);
    }
    
    function list_belanjaobat()
    {
        $awal = $this->input->post('min');
        $akhir = $this->input->post('max');
        $post   = $this->input->post();
        $like   = (empty(!$post['search']['value']))?"and rb.namabarang like '%".$post['search']['value']."%'":'';
        $sql    = "SELECT rbp.idbarang,rb.kode, rb.namabarang,rs.namasatuan, rbf.tanggalfaktur,rbf.nopesan,rbf.nofaktur, rbp.batchno, rbp.kadaluarsa, rbd.distributor, rbdi.jumlah 
                    from rs_barang_faktur rbf, rs_barang_pembelian rbp, rs_barang rb, rs_satuan rs, rs_barang_distributor rbd, rs_barang_distribusi rbdi 
                    WHERE date(rbf.tanggalfaktur) between ' ".$awal." ' and ' ".$akhir. 
                        "' and rbp.idbarangfaktur=rbf.idbarangfaktur 
                        and isnull(rbp.parentidbarangpembelian) 
                        and rb.idbarang=rbp.idbarang 
                        and rs.idsatuan=rb.idsatuan 
                        and rbd.idbarangdistributor=rbf.idbarangdistributor 
                        and rbdi.idbarangpembelian=rbp.idbarangpembelian 
                        and rbdi.jenisdistribusi='masuk' ".$like.
                    "order by 
                        rbf.tanggalfaktur ASC,
                        rbf.idbarangdistributor";
        $numrows= $this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $data=[];
        foreach ($list as $obj) {
            $row=[];
            $row[] = '';
            $row[] = $obj->kode;
            $row[] = $obj->batchno;
            $row[] = $obj->kadaluarsa;
            $row[] = $obj->namabarang;
            $row[] = $obj->jumlah;
            $row[] = $obj->namasatuan;
            $row[] = $obj->nofaktur;
            $row[] = $obj->nopesan;
            $row[] = $obj->tanggalfaktur;
            $row[] = $obj->distributor;
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    function list_defectaobat()
    {
        $post   = $this->input->post();
        $like   = (empty(!$post['search']['value']))?" and rb.namabarang like '%".$post['search']['value']."%'":'';
        $sql    = "SELECT rb.idbarang, rb.namabarang, rb.kode,ifnull(sum(rbs.stok),0) stok, ifnull(rb.rop,0) rop, ifnull(rb.stokaman,0) stokaman, ifnull(rb.stokminimal,0) stokminimal, ifnull(rb.stokhabis,0) stokhabis  FROM rs_barang rb left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok rbs on rbs.idbarangpembelian=rbp.idbarangpembelian and rbs.idunit='".$this->session->userdata('idunitterpilih')."'  GROUP by rb.idbarang".$like; 
        $numrows =$this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $no=$post['start'];$data=[];
        foreach ($list as $obj) {
            $row=[];
            $row[] = ++$no;
            $row[] = '[ '.$obj->kode .'] '.$obj->namabarang;
            $row[] = $obj->stok ;
            $row[] = '<span class="rop-'.status_ROP($obj->stok,$obj->stokaman,$obj->stokminimal).'">'.$obj->rop.'</span>';
            $row[] ='<a data-toggle="tooltip" data-original-title="Pesan" class="btn btn-primary btn-xs" ><i class="fa fa-cart-plus"></i></a>';
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    function list_obatkadaluarsa()
    {
        $post   = $this->input->post();
        $like   = (empty(!$post['search']['value']))?" and rb.namabarang like '%".$post['search']['value']."%'":'';
        $sql    = "SELECT rbp.idbarang, rbp.batchno, rbp.kadaluarsa, rb.kode, rb.namabarang, rs.namasatuan, sum(rbs.stok) as stok FROM rs_barang_pembelian rbp, rs_barang rb, rs_satuan rs, rs_barang_stok rbs where rbs.idbarangpembelian=rbp.idbarangpembelian and rbp.kadaluarsa !='0000-00-00' and rbs.stok>0 and rb.idbarang=rbp.idbarang and rs.idsatuan=rb.idsatuan ".$like." GROUP by rbs.idbarangpembelian order by rbp.kadaluarsa asc"; 
        $numrows=$this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $no=$post['start'];$data=[];
        foreach ($list as $obj) {
            $row=[];
            $row[] = ++$no;
            $row[] = $obj->kode;
            $row[] = $obj->namabarang;
            $row[] = $obj->batchno ;
            $row[] = $obj->kadaluarsa;
            $row[] = convertToRupiah($obj->stok);
            $row[] = $obj->namasatuan;
            $row[] ='<a data-toggle="tooltip" data-original-title="Kembalikan" class="btn btn-primary btn-xs" ><i class="fa fa-reply"></i></a> <a data-toggle="tooltip" data-original-title="Musnahkan" class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i></a>';
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    //penjualan obat resep
    function list_pemakaianobat()
    {
        $awal = $this->input->post('min');
        $akhir = $this->input->post('max');
        $ymin = date('Y', strtotime($awal));
        $ymax = date('Y', strtotime($akhir));
        $mmin = date('n', strtotime($awal));
        $mmax = date('n', strtotime($akhir));
        $post = $this->input->post();
        $like = (empty(!$post['search']['value']))?" and rb.namabarang like '%".$post['search']['value']."%' ":'';

        $idobat   = empty($post['obat']) ? '' :explode(',', $post['obat']);
        $whereobat= empty($post['obat']) ? '' : "rbp.idbarang='".$idobat[1]."' and " ;
        $wherepasien= empty($post['pasien']) ? '' : "pp.norm='".$post['pasien']."' and " ;
        $sql    = "SELECT ppas.norm, namapasien(ppas.idperson) as namapasien, pp.waktuperiksa as waktu, rb.namabarang, rb.kode, rs.namasatuan, rj.jenistarif, rbp.jumlahpemakaian , rbp.harga, rbp.total, rbp.kekuatan, rbp.idbarangpemeriksaan 
                FROM  person_pendaftaran pp                  
                JOIN person_pasien ppas on ppas.norm = pp.norm
                JOIN rs_barangpemeriksaan rbp ON rbp.idpendaftaran = pp.idpendaftaran
                LEFT JOIN rs_barang rb ON  rb.idbarang = rbp.idbarang 
                LEFT JOIN rs_jenistarif rj ON rj.idjenistarif = rb.idjenistarif 
                LEFT JOIN rs_satuan rs ON rs.idsatuan = rb.idsatuan 
                WHERE ".$whereobat.$wherepasien." pp.tahunperiksa between '".$ymin."' and '".$ymax."' and pp.bulanperiksa between '".$mmin."' and '".$mmax."' and date(pp.waktuperiksa) between '".$awal."' and '".$akhir."' ".$like;
        
        $numrows=$this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $no=$post['start'];$data=[];
        foreach ($list as $obj){
            $row=[];
            $row[] = $obj->waktu;            
            $row[] = $obj->norm.' - '.$obj->namapasien;
            $row[] = '['.$obj->kode.'] '.$obj->namabarang;
            $row[] = convertToRupiah(substr($obj->jumlahpemakaian,0,-3));
            $row[] = $obj->namasatuan;
            $row[] = $obj->jenistarif;
            $row[] = $obj->kekuatan;
            $row[] = convertToRupiah(substr($obj->total,0,-3));
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    
    //penjualan obat bebas
    function list_penjualanobatbebas()
    {
        $awal = $this->input->post('min');
        $akhir = $this->input->post('max');
        $post = $this->input->post();
        $like = (empty(!$post['search']['value']))?" and rb.namabarang like '%".$post['search']['value']."%'":'';

        $idobat    = empty($post['obat']) ? '' :explode(',', $post['obat']);
        $whereobat = empty($post['obat']) ? '' : " and rbp.idbarang='".$idobat[1]."' " ;
        $sql       = "SELECT rbp.waktuinput as waktu, rb.namabarang, rb.kode, rs.namasatuan, rj.jenistarif, rbp.jumlahpemakaian, rbp.harga, rbp.total, rbp.kekuatan 
                   FROM rs_barang_pembelibebas a
                   JOIN keu_tagihan_bebas ktb on ktb.idbarangpembelibebas = a.idbarangpembelibebas and ktb.status='selesai'
                   JOIN rs_barang_pembelianbebas rbp on rbp.idbarangpembelibebas = a.idbarangpembelibebas
                   LEFT JOIN rs_barang rb ON  rb.idbarang = rbp.idbarang 
                   LEFT JOIN rs_jenistarif rj ON rj.idjenistarif = rb.idjenistarif 
                   LEFT JOIN rs_satuan rs ON rs.idsatuan = rb.idsatuan 
                   WHERE date(a.waktu) between '".$awal."' and '".$akhir."' " . $whereobat . " and ktb.status ='selesai'
                   GROUP BY rbp.waktuinput, rbp.idbarang";
        
        $numrows=$this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $no=$post['start'];$data=[];
        foreach ($list as $obj){
            $row=[];
            $row[] = $obj->waktu;
            $row[] = '['.$obj->kode.'] '.$obj->namabarang;
            $row[] = convertToRupiah(substr($obj->jumlahpemakaian,0,-3));
            $row[] = $obj->namasatuan;
            $row[] = $obj->jenistarif;
            $row[] = $obj->kekuatan;
            $row[] = convertToRupiah(substr($obj->total,0,-3));
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    function list_resepobat()
    {
        $post   = $this->input->post();
        $tahunbulan = ($post['tb']) ? date('Yn', strtotime($post['tb'])) : date('Yn');
        $wheredokter = ($post['dok']) ? ' and rj.idpegawaidokter="'.$post['dok'].'"' : '' ;
        $sql    = 'SELECT rb.idbarang,b.namabarang,b.hargajual, rj.idpegawaidokter, trim(concat(ifnull(titeldepan, ""), " ", namalengkap, " ", ifnull(titelbelakang, ""))) as dokter, (select namaunit from rs_unit where idunit=rj.idunit) as namaunit FROM rs_barangpemeriksaan rb join rs_pemeriksaan rp on rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan="'.$tahunbulan.'" join rs_jadwal rj on rj.idjadwal=rp.idjadwal join person_pegawai ppeg on ppeg.idpegawai = rj.idpegawaidokter '.$wheredokter.' join person_person pp on pp.idperson = ppeg.idperson '.$wheredokter.' join rs_barang b on b.idbarang=rb.idbarang join rs_sediaan s on s.idsediaan=b.idsediaan and s.jenissediaan="resep" where rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan="'.$tahunbulan.'"'.$wheredokter.' order by rj.idpegawaidokter, rj.idunit,rb.idbarang';
        echo json_encode($this->db->query($sql)->result());
    }
    function list_obatgenerikpaten()
    {
        $post   = $this->input->post();
        $tahunbulan = ($post['tahunbulan']) ? date('Yn', strtotime($post['tahunbulan'])) : date('Yn');
        $sql="SELECT count(case when b.tipeobat='generik' then 1 end) as generik,count(case when b.tipeobat='paten' then 1 end) as paten,ru.namaunit FROM rs_barangpemeriksaan rb join person_pendaftaran pp on pp.idpendaftaran=rb.idpendaftaran and pp.idstatuskeluar=2 and pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum' join rs_pemeriksaan rp on rp.idpendaftaran=pp.idpendaftaran and rp.tahunbulan='".$tahunbulan."' join rs_unit ru on ru.idunit = pp.idunit join rs_barang b on b.idbarang=rb.idbarang  join rs_sediaan s on s.idsediaan=b.idsediaan and s.jenissediaan='resep' where rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan='".$tahunbulan."' GROUP by ru.idunit";
        $numrows=$this->db->query($sql)->num_rows();
        $list   = $this->db->query($sql.' limit '.$post['start'].','.$post['length'])->result();

        $no=$post['start'];$data=[];
        foreach ($list as $obj) {
            $row=[];
            $row[] = ++$no;
            $row[] = convertToRupiah($obj->generik);
            $row[] = convertToRupiah($obj->paten);
            $row[] = $obj->namaunit;
            $data[] = $row;
        }
        echo json_encode(["draw" => $post['draw'],"recordsTotal" => $numrows,"recordsFiltered" =>$numrows,"data" => $data]);
    }
    
    //Laporan Resep Bulanan
    public function view_laporanpenggunaanresepbulanan()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $tahun = date('Y',  strtotime($tahunbulan));
        $bulan = date('n',  strtotime($tahunbulan));
        $dt = $this->db->select('*, date(waktuperiksa) as tanggal ')->order_by('waktuperiksa','asc')->get_where('vrs_laporanresepbulanan',['tahunperiksa'=>$tahun,'bulanperiksa'=>$bulan])->result();
        echo json_encode($dt);
    }
    
    //Laporan Penggunaan Obat Bulanan - Rawat Jalan
    public function view_laporanpenggunaanobatperbulan()
    {
        $dt = $this->mviewql->view_laporanpenggunaanobatperbulan($this->input->post('tahunbulan'));
        echo json_encode($dt);
    }
    
    //Laporan Penggunaan Obat Bulanan - Rawat Inap
    public function view_laporanpenggunaanobatperbulanRanap()
    {
        $dt = $this->mviewql->view_laporanpenggunaanobatperbulanRanap($this->input->post('tahunbulan'));
        echo json_encode($dt);
    }
    //Laporan Distribusi Gudang
    public function view_laporandistribusigudang()
    {
        $dt = $this->mviewql->view_laporandistribusigudang();
        echo json_encode($dt);
    }

    // ******* PELAPORAN RESEP
    public function pelaporanresepview()
    {
        $data                = $this->settinglaporanfarmasi();
        $data['title_page']  = 'Pelaporan Resep Bulanan';
        $data['jsmode']      = 'pelaporanresep';
        $data['mode']        = 'pelaporanresep';
        $data['dokter']      = $this->mkombin->getdatadokter()->result();
        $this->load->view('v_index',$data);
    }
    private function sql_pelaporanresep($bulan,$dokter=false)
    {
        $wheredokter = (($dokter==false)?'':' and rj.idpegawaidokter="'.$dokter.'"');
        $sql='select rb.idbarang,b.namabarang,b.hargajual, rj.idpegawaidokter, trim(concat(ifnull(titeldepan, ""), " ", namalengkap, " ", ifnull(titelbelakang, ""))) as dokter, (select namaunit from rs_unit where idunit=rj.idunit) as namaunit FROM rs_barangpemeriksaan rb join rs_pemeriksaan rp on rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan="'.$bulan.'" join rs_jadwal rj on rj.idjadwal=rp.idjadwal join person_pegawai ppeg on ppeg.idpegawai = rj.idpegawaidokter '.$wheredokter.' join person_person pp on pp.idperson = ppeg.idperson '.$wheredokter.' join rs_barang b on b.idbarang=rb.idbarang join rs_sediaan s on s.idsediaan=b.idsediaan and s.jenissediaan="resep" where rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan="'.$bulan.'"'.$wheredokter.' order by rj.idpegawaidokter, rj.idunit,rb.idbarang';
        return $this->db->query($sql);
    }
    public function pelaporanresepexcel()
    {
        $tb = explode("%20",$this->uri->segment(3));
        $data['tb']     = date('Yn', strtotime($tb[0].' '.$tb[1]));
        $dokter         = $this->uri->segment(4);
        $data['resep']  = $this->sql_pelaporanresep($data['tb'],$dokter)->result();
        $data['titel']  = 'resep'.$data['tb'].$dokter;
        $data['dokter'] = $dokter;
        $this->load->view('report/excel_farmasipelaporanresep',$data);
    }
    
    public function pelaporanresepjson()
    {
        $tb=date('Yn',strtotime($this->input->post('tb')));
        $dokter=$this->input->post('dokter');
        echo json_encode($this->sql_pelaporanresep($tb,$dokter)->result());
    }
    // ********* PELAPORAN OBAT GENERIK DAN OBAT PATEN
    public function pelaporanogpview()
    {
        $data                = $this->settinglaporanfarmasi();
        $data['title_page']  = 'Pelaporan Bulanan Obat Generik dan Obat Paten';
        $data['jsmode']      = 'pelaporanogp';
        $data['mode']        = 'pelaporanogp';
        $this->load->view('v_index',$data);
    }
    public function pelaporanogpjson()
    {
        $tb=date('Yn',strtotime($this->input->post('tb')));
        echo json_encode($this->sql_pelaporanogp($tb)->result());
    }
    public function pelaporanogpexcel()
    {
        $getdata        = explode("%20",$this->uri->segment(3));
        $data['tb']     = date('Yn', strtotime($getdata[0].' '.$getdata[1]));
        $data['obat']   = $this->sql_pelaporanogp($data['tb'])->result();
        $data['titel']  = 'Laporan obat generik dan paten '.$data['tb'];
        $this->load->view('report/excel_farmasiobatgenerikpaten',$data);
    }
    private function sql_pelaporanogp($tb)
    {
        $sql="select
        count(case when b.tipeobat='generik' then 1 end) as generik,
        count(case when b.tipeobat='paten' then 1 end) as paten,
        ru.namaunit
                FROM rs_barangpemeriksaan rb
                join person_pendaftaran pp on pp.idpendaftaran=rb.idpendaftaran and pp.idstatuskeluar=2 and pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum'
                join rs_pemeriksaan rp on rp.idpendaftaran=pp.idpendaftaran and rp.tahunbulan='".$tb."'
                join rs_unit ru on ru.idunit = pp.idunit
                join rs_barang b on b.idbarang=rb.idbarang 
                join rs_sediaan s on s.idsediaan=b.idsediaan and s.jenissediaan='resep'
                 where rp.idpemeriksaan=rb.idpemeriksaan and rp.tahunbulan='".$tb."' GROUP by ru.idunit";
    return $this->db->query($sql);
    }
    
    public function downloadexcel_buku()
    {
        $data['buku']  = $this->db->select('b.judulbuku,b.kodebuku, b.jumlahbuku, b.jumlahhalaman, b.isbn, b.tahunterbit, b.ringkasan, k.kategori ')->join('pp_kategori k','k.idkategori=b.idkategori')->get('pp_buku b')->result();
        $data['title'] = 'Data Buku';
        $this->load->view('report/excel_databuku',$data);
    }
    
    
    public function cakupanpolispesialis()
    {
        $data['active_menu']       = 'report';
        $data['active_sub_menu']   = 'cakupanpolispesialis';
        $data['active_menu_level'] = '';
        $data['script_js']         = ['report/js_cakupanpolispesialis'];
        $data['plugins']           = [ PLUG_DATATABLE,  PLUG_DATE , PLUG_DROPDOWN ];
        $data['content_view']      = 'report/v_cakupanpolispesialis';
        $data['title_page']        = 'Cakupan Poli Spesialis';
        $data['unit']              = $this->db->order_by('namaunit', 'ASC')->get_where('rs_unit',['polispesialis'=>1])->result_array();
        $this->load->view('v_index',$data);
    }

    /**
     * [+] data elektromedik
     */
    public function tampil_cakupanpolispesialis()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        if($idunit == '00')
        {
            $idunit = "20' OR pp.idunit='36";  // khusus spesialis gigi (endodonsi dan periodonti)
        }
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,2,10)->result();

        //poli obgyn id unit 10
        if( $idunit == '5' ){
            $q_tindakan           = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result();
            $q_elektomedik        = $this->mviewql->get_elektromediksering($tahun,$idunit,$bulan,0,5)->result();
            $data['tindakan']     = array_merge( $q_tindakan,$q_elektomedik );
        }else{
            $data['tindakan']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result();
        }
        $data['tindakanElektromedik']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,5,9)->result();
        echo json_encode($data);
    }
    
    public  function excel_cakupanpolispesialis()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        if($idunit == '00')
        {
            $idunit = "20' OR pp.idunit='36";  // khusus spesialis gigi (endodonsi dan periodonti)
        }
        $unit   = $this->input->post('unit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        $namabulan  = date('F', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,2,10)->result_array();
        $data['tindakan']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result_array();
        $data['tindakanElektromedik']= $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,5,9)->result_array();
        
        $data['fileName']     = 'Cakupan Poli Spesialis '.$unit.' '.$namabulan.' '.$tahun;        
        $data['namaPoli']     = 'Poli Spesialis '.$unit;  
        $this->load->view('report/excel_cakupanpolispesialis',$data);
    }
    
    // >> cakupan poli penunjang
    public function cakupanpolipenunjang()
    {
        $data['active_menu']       = 'report';
        $data['active_sub_menu']   = 'cakupanpolipenunjang';
        $data['active_menu_level'] = '';
        $data['script_js']         = ['report/js_cakupanpolipenunjang'];
        $data['plugins']           = [ PLUG_DATATABLE,  PLUG_DATE , PLUG_DROPDOWN ];
        $data['content_view']      = 'report/v_cakupanpolipenunjang';
        $data['title_page']        = 'Cakupan Penunjang Medik'; // rename by P.Bambang's request from 'Cakupan Poli Penunjang';
        $data['unit']              = $this->db->get_where('rs_unit',['tipeunit'=>"PENUNJANG"])->result_array();
        $this->load->view('v_index',$data);
    }
    
    public function tampil_cakupanpolipenunjang()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,2,10)->result();
        $data['tindakan']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result();
        echo json_encode($data);
    }
    
    public  function excel_cakupanpolipenunjang()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        $unit   = $this->input->post('unit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        $namabulan  = date('F', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,2,10)->result_array();
        $data['tindakan']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result_array();
        $data['fileName']     = 'Cakupan Penunjang Medik '.$unit.' '.$namabulan.' '.$tahun;  // rename by P.Bambang's request from 'Cakupan Poli Penunjang';      
        $this->load->view('report/excel_cakupanpolipenunjang',$data);
    }
    // << cakupan poli penunjang

    public function cakupanpolikhusus()
    {
        $data['active_menu']       = 'report';
        $data['active_sub_menu']   = 'cakupanpolikhusus';
        $data['active_menu_level'] = '';
        $data['script_js']         = ['report/js_cakupanpolikhusus'];
        $data['plugins']           = [ PLUG_DATATABLE,  PLUG_DATE , PLUG_DROPDOWN ];
        $data['content_view']      = 'report/v_cakupanpolikhusus';
        $data['title_page']        = 'Cakupan Poli Umum & UGD';
        $data['unit']              = $this->db->get_where('rs_unit',['polikhusus'=>1])->result_array();
        $this->load->view('v_index',$data);
    }
    
    public function tampil_cakupanpolikhusus()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolikhusus_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolikhusus_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolikhusus_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,2,10)->result();
        $data['tindakan']     = $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,3,9)->result();
        $data['tindakanElektromedik']= $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,5,9)->result();
        echo json_encode($data);
    }
    
    public  function excel_cakupanpolikhusus()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $idunit = $this->input->post('idunit');
        $unit   = $this->input->post('unit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        $namabulan  = date('F', strtotime($tahunbulan));
        
        $data['lamabaru']     = $this->mviewql->getdata_cakupanpolikhusus_pasienlamabaru($tahun,$bulan,$idunit);
        $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolikhusus_jenisperiksa($tahun,$bulan,$idunit);
        $data['carabayar']    = $this->mviewql->getdata_cakupanpolikhusus_carabayar($tahun,$bulan,$idunit);
        $data['diagnosis']    = $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,2,10)->result_array();
        $data['tindakan']     = $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,3,9)->result_array();
        $data['tindakanElektromedik']= $this->mviewql->get_diagnosaterseringpolikhusus($tahun,$idunit,$bulan,0,5,9)->result_array();
        $data['fileName']     = 'Cakupan '.$unit.' '.$namabulan.' '.$tahun;          
        $data['namaPoli']     = $unit;  
        $this->load->view('report/excel_cakupanpolikhusus',$data);
    }
    
    /**
     * tampil dan unduh cakupan kamar operasi
     */
    public function cakupankamaroperasi()
    {
        $data['active_menu']       = 'report';
        $data['active_sub_menu']   = 'cakupankamaroperasi';
        $data['active_menu_level'] = '';
        $data['script_js']         = ['report/js_cakupankamaroperasi'];
        $data['plugins']           = [ PLUG_DATE ];
        $data['content_view']      = 'report/v_cakupankamaroperasi';
        $data['title_page']        = 'Cakupan Instalasi Kamar Operasi';
        
        $tahunbulan = $this->input->get('tahunbulan');        
        $tahunbulan = ((empty($tahunbulan)) ? date('Ym') : date('Ym', strtotime( $tahunbulan)) );
        
        $data['get']               = $this->input->get();
        $data['jenisoperasi']      = $this->db->get('rs_jenisoperasi')->result_array();
        $data['carabayar']         = $this->mviewql->getdata_cakupankamaroperasi_carabayar($tahunbulan);
        $data['jenistindakan']     = $this->mviewql->getdata_cakupankamaroperasi_jenistindakan($tahunbulan);
        $data['dranastesi']        = $this->mviewql->getdata_cakupankamaroperasi_dranastesi($tahunbulan);
        $data['laporansc']         = $this->mviewql->getdata_cakupankamaroperasi_laporansc($tahunbulan);
        
        if(isset($data['get']['excel']) && $data['get']['excel'] == '1') 
        {
            $data['fileName'] = 'Cakupan Instalasi Kamar Operasi '.$this->input->get('tahunbulan');
            $this->load->view('report/excel_cakupankamaroperasi',$data);
        }
        else
        {
            $this->load->view('v_index',$data);
        }
    }
    
    public function cakupanrawatinap()
    {

        $data['active_menu']       = 'report';
        $data['active_sub_menu']   = 'cakupanrawatinap';
        $data['active_menu_level'] = '';
        $data['script_js']         = ['report/js_cakupanrawatinap'];
        $data['plugins']           = [ PLUG_DATATABLE, PLUG_DATE ];
        $data['content_view']      = 'report/v_cakupanrawatinap';
        $data['title_page']        = 'Cakupan Rawat Inap';
        $data['unit']              = $this->db->get_where('rs_unit',['polispesialis'=>1])->result_array(); // example from pli spesialis
        
        // $tahunbulan = $this->input->get('tahunbulan');        
        // $tahunbulan = ((empty($tahunbulan)) ? date('Y-m') : date('Y-m', strtotime( $tahunbulan)) );
        
        // $tahun  = date('Y', strtotime($tahunbulan));
        // $bulan  = date('n', strtotime($tahunbulan));
        
        // $data['get']        = $this->input->get();
        // $data['lamabaru']   = $this->mviewql->getdata_cakupanranap_pasienlamabaru($tahun,$bulan);
        // $data['carabayar']  = $this->mviewql->getdata_cakupanranap_pasiencarabayar($tahun,$bulan);
        
        // if(isset($data['get']['excel']) && $data['get']['excel'] == '1') 
        // {
        //     $data['fileName'] = 'Cakupan Rawat Inap '.$this->input->get('tahunbulan');
        //     $this->load->view('report/excel_cakupanrawatinap',$data);
        // }
        // else
        // {
        //     $this->load->view('v_index',$data);
        // }
        $this->load->view('v_index',$data);
    }

    public function tampil_cakupanrawatinap()
    {
        //$tahunbulan = $this->input->get('tahunbulan');   
        $tahunbulan = $this->input->post('tahunbulan');        
        //$tahunbulan = ((empty($tahunbulan)) ? date('Y-m') : date('Y-m', strtotime( $tahunbulan)) );
        
        //$idunit = $this->input->post('idunit');
        $tahun  = date('Y', strtotime($tahunbulan));
        $bulan  = date('n', strtotime($tahunbulan));
        
        $data['get']        = $this->input->get();
        $data['lamabaru']   = $this->mviewql->getdata_cakupanranap_pasienlamabaru($tahun,$bulan);
        $data['carabayar']  = $this->mviewql->getdata_cakupanranap_pasiencarabayar($tahun,$bulan);
        $data['diagnosis']    = $this->mviewql->get_diagnosatindakan_ranap($tahun,$bulan,0,2,10)->result();        
        $data['tindakan']     = $this->mviewql->get_diagnosatindakan_ranap($tahun,$bulan,0,3,9)->result();
        echo json_encode($data);
        
        // $tahunbulan = $this->input->post('tahunbulan');
        // $idunit = $this->input->post('idunit');
        // $tahun  = date('Y', strtotime($tahunbulan));
        // $bulan  = date('n', strtotime($tahunbulan));
        
        // $data['lamabaru']     = $this->mviewql->getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit);
        // $data['jenisperiksa'] = $this->mviewql->getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit);
        // $data['carabayar']    = $this->mviewql->getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit);
        // $data['diagnosis']    = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,2,10)->result();
        // $data['tindakan']     = $this->mviewql->get_diagnosatersering($tahun,$idunit,$bulan,0,3,9)->result();
        // echo json_encode($data);
        }

    public function getDataExcel() // get data
    {
        $thisMode  = $this->input->post("mode");
        $this->load->model('mdatatable');
        
        $data="";        
        if($thisMode =="barangslowmoving")
        {
            $data = $this->mdatatable->dt_slowmovingobat()->result();
        }
        else if($thisMode=="belanjaobat")
        {
            $data = $this->mdatatable->dt_belanjaObat()->result();
        } else {
            $data="";
        }
        echo json_encode($data);
    }

    public function cetak_excel_langsung()
    {
        $id  = $this->input->post("p");
        $listData = $this->input->post("listData");
        $title = $this->input->post("title");
        $data['fileName']     = $title.' - '.$id;
        $data['listData']     = $listData;
        $this->load->view('masterdata/excel_cetak_langsung',$data);
    }
}

