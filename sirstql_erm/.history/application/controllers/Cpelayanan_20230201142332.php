<?php

use MY_controller;

defined('BASEPATH') or exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
// define dan nilai di tabel login_halaman harus sama
// defined('V_PEMERIKSAANKLINIK') OR define('V_PEMERIKSAANKLINIK', 24);
// defined('V_ADMINISTRASIANTRIAN') OR define('V_ADMINISTRASIANTRIAN', 29);

class Cpelayanan extends MY_controller
{
    function __construct()
    {
        parent::__construct();
        // jika user belum login maka akses ditolak
        if ($this->session->userdata('sitiql_session') != 'aksesloginberhasil') {
            pesan_belumlogin();
        }
        $this->load->model('mkombin');
        $this->load->model('mviewql');
        $this->load->model('msqlbasic');
    }

    ///////////////////PEMBELIAN OBAT BEBAS////////////////////////
    public function setting_pembelianbebas()
    {
        return [
            'content_view'      => 'pelayanan/v_pembelianbebas',
            'active_menu'       => 'pelayanan',
            'active_menu_level' => ''
        ];
    }
    //---- basit
    public function pembelianbebas_bhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)) {
            $data                = $this->setting_pembelianbebas();
            $data['title_page']  = 'Tambah BHP/Farmasi Bebas';
            $data['mode']        = 'bhp';
            $data['active_sub_menu'] = 'obatbebas';
            $data['script_js']   = ['js_pembelianbebas-bhp'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    /**Halaman Kasir Pembelian Bebas**/
    public function kasirpembelianbebas()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KASIROBATBEBAS)) {
            $data                = $this->setting_pembelianbebas();
            $data['title_page']  = 'Kasir BHP/Farmasi Bebas';
            $data['mode']        = 'list';
            $data['active_sub_menu'] = 'kasirobatbebas';
            $data['script_js']   = ['js_pembelianbebas-cetak', 'js_pembelianbebas-listbhp'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE, PLUG_DROPDOWN];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    /**List Data Table Kasir Pembelian Bebas**/
    public function dt_pembelianbebas()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_pembelianbebas_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = (empty($obj->idpegawai)) ? $obj->namanonpeg :  $obj->titeldepan . ' ' . $obj->namalengkap . ' ' . $obj->titelbelakang;
                $row[] = $obj->waktu;
                $row[] = $obj->waktubayar;
                $row[] = $obj->idtagihan . '/' . $obj->jenistagihan . '/tagihan Rp ' . convertToRupiah($obj->kekurangan);
                $row[] = $obj->keterangan;
                $row[] = $obj->status;
                $row[] =
                    (($obj->jenistagihan != 'dibayar') ? '<a onclick="bayar(' . $obj->idbarangpembelibebas . ',' . $obj->kekurangan . ',' . $obj->idtagihan . ')" class="btn btn-primary btn-xs" ' . ql_tooltip('bayar') . '><i class="fa fa-money"></i></a>' : '')
                    . ' <a onclick="cetaknota(' . $obj->idbarangpembelibebas . ')" class="btn btn-primary btn-xs" ' . ql_tooltip('cetak') . '><i class="fa fa-print"></i></a>'
                    . (($obj->status != 'selesai') ? ' <a id="selesai" i="' . $obj->idbarangpembelibebas . '" class="btn btn-primary btn-xs" ' . ql_tooltip('selesai') . '><i class="fa fa-check"></i></a>'  : '')
                    . (($obj->status != 'selesai') ? ' <a id="ubah" i="' . $obj->idbarangpembelibebas . '" class="btn btn-warning btn-xs" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>' : '')
                    . (($obj->status != 'batal') ? ' <a id="batal" i="' . $obj->idbarangpembelibebas . '" class="btn btn-danger btn-xs" ' . ql_tooltip('batal') . '><i class="fa fa-minus-circle"></i></a>' : '');
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->totdt_pembelianbebas(),
            "recordsFiltered" => $this->mdatatable->filterdt_pembelianbebas(),
            "data" => $data
        );
        echo json_encode($output); //--output dalam format JSON
    }

    public function pembelianbebaslistjson()
    {
        $date = (empty($this->input->post('date'))) ? date('Y-m-d') :  $this->input->post('date');
        echo json_encode($this->db->query("SELECT ktb.idtagihan,ktb.idbarangpembelibebas, ktb.waktubayar, ktb.nominal, ktb.dibayar, ktb.potongan, ktb.kekurangan, ifnull(ktb.keterangan,'') keterangan,  if(rbp.ispegawai=0,rbp.nama,(select concat(ifnull(ppai.titeldepan,''),' ',pp.namalengkap,' ',ifnull(ppai.titelbelakang,'')) from person_pegawai ppai, person_person pp where ppai.idpegawai=rbp.idpegawai and pp.idperson=ppai.idperson )) as nama , rbp.waktu FROM keu_tagihan_bebas ktb, rs_barang_pembelibebas rbp where date(rbp.waktu)='" . $date . "' and rbp.idbarangpembelibebas=ktb.idbarangpembelibebas order by ktb.idbarangpembelibebas desc , ktb.waktubayar asc")->result_array());
    }

    //basit, clear
    public function pembelianbebas_caribhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)) {
            $like = $this->input->get('q');
            $query  = '';
            $idunit = $this->session->userdata('idunitterpilih');
            if (empty($idunit)) {
                $query = $this->db->select("idbarang as id, concat(namabarang,' | stok ',ifnull(totalstokbarang(idbarang),0),' ',s.namasatuan,' | harga ',hargaumum) as text");
                $query = $this->db->like("namabarang", $like);
                $query = $this->db->where("b.ishidden", 0);
                $query = $this->db->join("rs_satuan s", "s.idsatuan = b.idsatuan");
                $query = $this->db->get('rs_barang b')->result();
            } else {
                $query = $this->db->query("select concat(rbp.idbarang,',',rbp.idbarangpembelian) as id, 
                                            concat(rb.namabarang, 
                                                    ' | stok ',sum(rbs.stok),
                                                    ' ', rs.namasatuan,
                                                    ' | harga ', rb.hargaumum, 
                                                    ' | Exp ', rbp.kadaluarsa) as text 
                                            from rs_barang_pembelian rbp, 
                                                rs_barang_stok rbs, 
                                                rs_barang rb, 
                                                rs_satuan rs 
                                            WHERE rb.ishidden=0 
                                                and rbs.stok > 0
                                                and rb.namabarang like '%" . $like . "%' 
                                                and rbp.idbarang=rb.idbarang 
                                                and rbs.idunit = '" . $idunit . "' 
                                                and rbs.idbarangpembelian=rbp.idbarangpembelian 
                                                and rs.idsatuan=rb.idsatuan 
                                            GROUP by rbp.idbarang, rbp.idbarangpembelian
                                            ORDER BY rb.namabarang ASC, rbp.kadaluarsa ASC")->result();
            }

            echo json_encode($query);
        }
    }

    public function pembelianbebas_refreshpembeli()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)) {
            echo json_encode($this->db->query(""
                . "SELECT rbp.waktu, (select keterangan from keu_tagihan_bebas ktb where idbarangpembelibebas=rbp.idbarangpembelibebas and ktb.status!='batal' limit 1) as keterangan, "
                . "rbp.idpegawai, rbp.idbarangpembelibebas, rbp.jenisdiskon, if(rbp.jenisdiskon = 'diskon pegawai', "
                . "(select concat( ifnull(pper.nik,''),' | ', ifnull(ppeg.titeldepan,''),' ',pper.namalengkap,' ',ifnull(ppeg.titelbelakang,'')) "
                . "from person_pegawai ppeg "
                . "join person_person pper on pper.idperson=ppeg.idperson "
                . "where ppeg.idpegawai=rbp.idpegawai),rbp.nama) as namapembeli "
                . "FROM rs_barang_pembelibebas rbp where idbarangpembelibebas='" . $this->input->post('i') . "'")->row_array());
        } else {
            aksesditolak();
        }
    }

    public function pembelianbebas_refreshbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)) {
            $idpembelibebas = $this->input->post('i');
            echo json_encode($this->mkombin->pembelianbebas_listbhp($idpembelibebas));
        } else {
            aksesditolak();
        }
    }

    public function pembelianbebas_add()
    {
        $this->mgenerikbap->setTable('rs_barang_pembelibebas');
        $callid = generaterandom(4);
        $data = [
            'iduser'   => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
            'callid'   => $callid
        ];
        $this->mgenerikbap->insert($data);
        $ret = $this->mgenerikbap->select('idbarangpembelibebas', ['callid' => $callid])->row_array();
        $data = [
            'callid'  => ""
        ];
        $this->mgenerikbap->update_where($data, ['callid' => $callid]);
        echo json_encode(["idbarangpembelibebas" => $ret["idbarangpembelibebas"]]);
    }

    public function pembelianbebas_updatenama()
    {
        $post = $this->input->post();
        $this->mgenerikbap->setTable('rs_barang_pembelibebas');
        $data = ($post['jns'] != 'diskon pegawai') ? ['nama' => qlreplace_quote($post['n'])] : ['idpegawai' => $post['idp']];
        $data['ispasienranap'] = $post['ispasienranap'];

        $arrMsg = $this->mgenerikbap->update($data, $this->input->post('i'));
        $arrMsg = $this->db->update('keu_tagihan_bebas', ['keterangan' => qlreplace_quote($post['k'])], ['idbarangpembelibebas' => $this->input->post('i')]);
        pesan_success_danger($arrMsg, 'Update Nama Success.!', 'Update Nama Failed.!', 'js');
    }

    public function pembelianbebas_updatejenisdiskon()
    {
        $this->mgenerikbap->setTable('rs_barang_pembelibebas');
        $jenisdiskon = $this->input->post('jns');
        $ubahdata = ($jenisdiskon == 'diskon pegawai') ? ['jenisdiskon' => $jenisdiskon, 'idpegawai' => NULL] : ['jenisdiskon' => $jenisdiskon];
        $arrMsg = $this->mgenerikbap->update($ubahdata, $this->input->post('i'));
        pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
    }

    public function pembelianbebas_ispasienranap()
    {
        if (empty(!$this->input->post('i'))) {
            $this->mgenerikbap->setTable('rs_barang_pembelibebas');
            $arrMsg = $this->mgenerikbap->update(['ispasienranap' => $this->input->post('ispasienranap')], $this->input->post('i'));
            pesan_success_danger($arrMsg, 'Update Data Berhasil.', 'Update Data Gagal.', 'js');
        } else {
            pesan_success_danger(1, 'Data Tidak Ditemukan.', 'Update Data Gagal.', 'js');
        }
    }

    public function pembelianbebas_updateispatner()
    {
        $this->mgenerikbap->setTable('rs_barang_pembelibebas');
        $arrMsg = $this->mgenerikbap->update(['ispatner'   => $this->input->post('patner')], $this->input->post('i'));
        pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
    }
    public function pembelianbebas_inputbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)) {
            $idbarangpembelibebas = $this->input->post('i');

            $idbarang = 0;
            $idunit = $this->session->userdata('idunitterpilih');
            if ($idunit) {
                $dtidbarang = explode(',', $this->input->post('x'));
                $idbarang   = $dtidbarang[0];
            } else {
                $idbarang = $this->input->post('x'); //idbarang
            }

            $detail_bhp = $this->mgenerikbap->select_multitable("*", "rs_barang", ['idbarang' => $idbarang])->row_array();
            if (!empty($detail_bhp)) {
                $cek_rs_barang_pembelianbebas = $this->mkombin->cek_rs_barang_pembelianbebas($idbarang, $idbarangpembelibebas);
                if (!empty($cek_rs_barang_pembelianbebas)) {
                    $arrMsg =  $this->db->query("UPDATE rs_barang_pembelianbebas SET jumlahpemakaian=jumlahpemakaian + 1 WHERE idbarangpembelibebas='$idbarangpembelibebas' AND idbarang='$idbarang'");
                    pesan_success_danger($arrMsg, 'Update BHP Success.!', 'Update BHP Failed.!', 'js');
                } else {
                    $jenisdiskon = $this->input->post('jns');
                    $harga =  ($jenisdiskon == 'diskon pegawai') ? $detail_bhp['hargabeli'] + ($detail_bhp['hargabeli'] * 0.05)  : (($jenisdiskon == 'diskon patner') ? $detail_bhp['hargaumum'] - ($detail_bhp['hargaumum'] * 0.1) : $detail_bhp['hargaumum']);
                    empty($this->input->post('patner')) ? empty($this->input->post('ip')) ? $detail_bhp['hargaumum'] : $detail_bhp['hargabeli'] : $detail_bhp['hargaumum'] * 0.1;
                    $data = ['idbarangpembelibebas' => $idbarangpembelibebas, 'idbarang' => $idbarang, 'jumlahpemakaian' => '1', 'idjenistarif' => $detail_bhp['idjenistarif'], 'harga' => $harga, 'kekuatan' => $detail_bhp['kekuatan']];
                    if (empty(!$idunit)) {
                        $data['idunit'] = $idunit;
                        $data['idbarangpembelian'] = $dtidbarang[1];
                    }

                    $arrMsg =  $this->db->insert("rs_barang_pembelianbebas", $data);
                    pesan_success_danger($arrMsg, 'Add BHP Success.!', 'Add BHP Failed.!', 'js');
                }
            } else {
                pesan_danger("BHP Tidak Ditemukan..!", "js");
            }
        }
    }

    public function pembelianbebas_settbhp()
    {
        $id = $this->input->post('x');
        $y = $this->input->post('y');
        $type = $this->input->post('type');
        $d = $this->input->post('d');
        if ($type == 'grup') {
            $data = ['grup' => $d, 'jenisgrup' => ($d == 0) ? 'bukan racikan' : 'komponen racikan'];
        } else if ($type == 'del') {
            $this->mgenerikbap->setTable('rs_barang_pembelianbebas');
            $arrMsg = $this->mgenerikbap->delete($id);
            return pesan_success_danger($arrMsg, 'Delete BHP Success..!', 'Delete BHP Failed..!', 'js');
        } else if ($type == 'kekuatan') {
            $data = ['kekuatan' => $d];
        } else if ($type == 'aturanpakai') {
            $data = ['idbarangaturanpakai' => $d];
        } else if ($type == 'penggunaan') {
            $data = ['penggunaan' => $d];
        } else if ($type == 'kemasan') {
            $data = ['idkemasan' => $d];
        } else if ($type == 'jumlah') {
            $data = ['jumlahpemakaian' => $d];
        } else if ($type == 'catatan') {
            $data = ['catatan' => $d];
        }
        $arrMsg = $this->db->update('rs_barang_pembelianbebas', $data, ['idbarangpembelianbebas' => $id]);
        if ($type == 'grup') {
            $idbarangpembelibebas = $this->db->query("select idbarangpembelibebas from rs_barang_pembelianbebas where idbarangpembelianbebas=$id")->row_array()["idbarangpembelibebas"];
            //tambah parent grup, jika belum ada
            if ($d > 0) {
                $this->db->query("insert into rs_barang_pembelianbebas (idbarang, idbarangpembelibebas, idjenistarif, grup, jenisgrup, kekuatan) SELECT 0 as idbarang, idbarangpembelibebas, 0 as idjenistarif, grup, 'racikan' as jenisgrup, 1 as kekuatan FROM `rs_barang_pembelianbebas` irmb where idbarangpembelianbebas=$id and ifnull((select false from rs_barang_pembelianbebas where idbarangpembelibebas=irmb.idbarangpembelibebas and idbarang=0 and grup=irmb.grup and jenisgrup='racikan' limit 1), true)");
            }
            //hapus grup yang error
            $todelete = $this->db->query("select group_concat(distinct grup) as grup from rs_barang_pembelianbebas where idbarangpembelibebas=$idbarangpembelibebas and jenisgrup!='racikan'")->row_array();
            $this->db->query("delete from rs_barang_pembelianbebas where grup not in (" . $todelete["grup"] . ") and grup>0 and jenisgrup='racikan' and idbarangpembelibebas=$idbarangpembelibebas");
        }
        pesan_success_danger($arrMsg, 'Update BHP Success..!', 'Update BHP Failed..!', 'js');
    }

    // pemeriksan cetak aturan pakai obat
    public function pembelianbebas_cetak_aturanpakai()
    {
        echo json_encode($this->db->query("select pnb.grup, DATE_FORMAT(waktuinput, '%d/%m/%Y') as tglperiksa, pnb.penggunaan, concat(ifnull(pb.nama,''))as namalengkap, '-' as norm, if(jenisgrup='racikan',concat('racikan ', pnb.grup),(select b.namabarang from rs_barang b WHERE b.idbarang=pnb.idbarang)) as namaobat, aturanpakai from rs_barang_pembelianbebas pnb, rs_barang_pembelibebas pb, vrs_barang_aturanpakai vba where pnb.idbarangpembelibebas=pb.idbarangpembelibebas and vba.idbarangaturanpakai=pnb.idbarangaturanpakai and pnb.idbarangpembelianbebas='" . $this->input->post('idb') . "' order by idbarangpembelianbebas limit 1")->row_array());
    }

    // cetak nota penjualkan bebas
    public function cetak_notapenjualanbebas()
    {
        $arrdata = [
            'detailtagihan' => $this->db->query("SELECT namabarang as nama, harga + ifnull(potongan,0) as harga, sum(bp.jumlahpemakaian) as jumlah, bp.kekuatan, b.kekuatan as kekuatanbarang FROM `rs_barang_pembelianbebas` bp join rs_barang b on b.idbarang=bp.idbarang where idbarangpembelibebas='" . $this->input->post('i') . "' and total>0 group by bp.idbarang")->result_array(),
            'infotagihan' => $this->db->select(" *, ((ktb.nominal+ktb.pembulatan) - ktb.potongan) as totaltagihan, if(rbp.jenisdiskon!='diskon pegawai',rbp.nama,(select concat(ifnull(ppai.titeldepan,''),' ',pp.namalengkap,' ',ifnull(ppai.titelbelakang,'')) from person_pegawai ppai, person_person pp where ppai.idpegawai=rbp.idpegawai and pp.idperson=ppai.idperson )) as namacetak ")->join('keu_tagihan_bebas ktb', 'ktb.idbarangpembelibebas=rbp.idbarangpembelibebas')->where(['rbp.idbarangpembelibebas' => $this->input->post('i'), 'status!=' => 'batal'])->get('rs_barang_pembelibebas rbp')->result_array()
        ];
        echo json_encode($arrdata);
    }
    public function bayarpob() //bayar pembelian obat bebas
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $post = $this->input->post();
        $tunai = $post['tunai'];
        $transfer = $post['transfer'];
        $potonggaji = $post['potonggaji'];
        $totalbayar = $tunai + $transfer + $potonggaji;
        $idbank = $post['idbank'];
        $penanggung = $post['penanggung'];

        if ($tunai > 0) {
            //bayar tunai
            $databayar = ['iduserkasir' => $iduser, 'carabayar' => 'tunai', 'jenistagihan' => 'dibayar', 'dibayar' => $tunai, 'waktubayar' => date('Y-m-d H-i-s')];
            $bayar = $this->db->update('keu_tagihan_bebas', $databayar, ['idbarangpembelibebas' => $post['i'], 'waktubayar' => null]);
            $this->kekurangantagihanbebas($post, $totalbayar);
        }

        if ($transfer > 0) {
            //bayar transfer
            $databayar = [
                'iduserkasir' => $iduser,
                'carabayar' => 'transfer',
                'jenistagihan' => 'dibayar',
                'dibayar' => $transfer,
                'waktubayar' => date('Y-m-d H-i-s'),
                'idbanktujuan' => $idbank
            ];
            $bayar = $this->db->update('keu_tagihan_bebas', $databayar, ['idbarangpembelibebas' => $post['i'], 'waktubayar' => null]);
            $this->kekurangantagihanbebas($post, $totalbayar);
        }

        if ($potonggaji > 0) {
            //bayar potong gaji
            $databayar = [
                'iduserkasir' => $iduser,
                'carabayar' => 'potonggaji',
                'jenistagihan' => 'dibayar',
                'dibayar' => $potonggaji,
                'waktubayar' => date('Y-m-d H-i-s'),
                'penanggung' => $penanggung
            ];
            $bayar = $this->db->update('keu_tagihan_bebas', $databayar, ['idbarangpembelibebas' => $post['i'], 'waktubayar' => null]);
            $this->kekurangantagihanbebas($post, $totalbayar);
        }
        $this->distribusibarangobatbebas($post['i'], 'keluar');
        pesan_success_danger($bayar, 'Success.', 'Gagal.', 'js');
    }

    private function kekurangantagihanbebas($post, $totalbayar)
    {
        //cek data tagihan
        $tagihan = $this->db->where('idbarangpembelibebas', $post['i'])->where('kekurangan > 0')->get('keu_tagihan_bebas')->row_array();
        // buat keu_tagihan_bebas baru jika ada kekurangan 
        if (empty(!$tagihan) && ($totalbayar > 0)) {
            $nominal = $tagihan['kekurangan'];
            $bayar = $this->db->insert('keu_tagihan_bebas', ['idbarangpembelibebas' => $post['i'], 'nominal' => $nominal, 'potongan' => 0, 'dibayar' => 0, 'kekurangan' => $tagihan['kekurangan']]);
            $this->db->update('keu_tagihan_bebas', ['nominal' => $tagihan['dibayar'], 'kekurangan' => 0], ['idtagihan' => $tagihan['idtagihan']]);
        }
    }

    public function selesaipob()
    {

        $selesai = $this->db->update('keu_tagihan_bebas', ['status' => $this->input->post('status')], ['idbarangpembelibebas' => $this->input->post('i')]);
        //        $this->distribusibarangobatbebas($this->input->post('i'),'pembetulankeluar');
        pesan_success_danger($selesai, 'Success.', 'Gagal.', 'js');
    }

    //-------------------------- vv Standar
    public function setting_suratkelahiran()
    {
        return [
            'content_view'      => 'pelayanan/v_suratkelahiran',
            'active_menu'       => 'pelayanan',
            'active_sub_menu'   => 'suratkelahiran',
            'active_menu_level' => ''
        ];
    }
    public function suratkelahiran() //list pemeriksaanklinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) //lihat define di atas
        {
            $data                = $this->setting_suratkelahiran(); //letakkan di baris pertama
            $data['title_page']  = 'Surat Keterangan Kelahiran';
            $data['mode']        = 'view';
            $data['script_js']   = ['pelayanan/js_suratkelahiran'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_suratkelahiran()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_suratkelahiran_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->kodesurat;
                $row[] = $obj->norm;
                $row[] = $obj->namapasien;
                $row[] = $obj->tanggallahir;
                $row[] = $obj->jeniskelamin;
                $row[] = $obj->panjangbayi . ' cm';
                $row[] = $obj->beratbadan . ' Kg';
                $row[] = $obj->anakke;
                $row[] = $obj->catatan;
                $row[] = $obj->ayah;
                $row[] = $obj->ibu;
                $row[] = '<a class="btn btn-primary btn-xs" id="cetaksurat" norm="' . $obj->norm . '"><i class="fa fa-print"></i> cetak</a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_suratkelahiran(),
            "recordsFiltered" => $this->mdatatable->filter_dt_suratkelahiran(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function suratkelahiran_detailpasien()
    {
        $data = $this->db->query("select b.namalengkap, b.jeniskelamin, b.tanggallahir, (SELECT namapasien(ay.idperson) FROM person_pasien_penanggungjawab ay WHERE norm =a.norm and idhubungan=1) as ayah, (SELECT namapasien(ib.idperson) FROM person_pasien_penanggungjawab ib WHERE norm = a.norm and idhubungan=2) as ibu from person_pasien a join person_person b on b.idperson=a.idperson where a.norm='" . $this->input->post('norm') . "'")->row_array();
        echo json_encode($data);
    }

    public function suratkelahiran_ceknosurat()
    {
        $data = $this->db->query("select * from rs_suratkelahiran rs where kodesurat='" . $this->input->post('nosurat') . "'");
        if ($data->num_rows() > 0) {
            $data = 'sudahada';
        } else {
            $data = 'belumada';
        }
        echo json_encode($data);
    }

    public function suratkelahiran_ceknorm()
    {
        $data = $this->db->query("select * from rs_suratkelahiran rs where norm='" . $this->input->post('norm') . "'");
        if ($data->num_rows() > 0) {
            $data = 'sudahada';
        } else {
            $data = 'belumada';
        }
        echo json_encode($data);
    }

    public function buatsuratkelahiran()
    {
        $iduser = $this->get_iduserlogin();
        $data = [
            'kodesurat' => $this->input->post('nosurat'),
            'norm' => $this->input->post('norm'),
            'waktulahir' => $this->input->post('jamlahir'),
            'anakke' => $this->input->post('anakke'),
            'lahirkembar' => $this->input->post('lahirkembar'),
            'panjangbayi' => $this->input->post('panjangbayi'),
            'beratbadan' => $this->input->post('beratbadan'),
            'catatan' => $this->input->post('catatan'),
            'iduser' => $iduser
        ];
        $arrMsg =  $this->db->insert('rs_suratkelahiran', $data);
        pesan_success_danger($arrMsg, "Surat Kelahiran Berhasil Dibuat.", "Surat Kelahiran Gagal Dibuat.", "js");
    }

    public function insertriwayatsuratkelahiran()
    {
        $post   = $this->input->post();
        $iduser = $this->get_iduserlogin();
        $data = [
            'norm' => $post['norm'],
            'namapetugas' => $post['namapetugas'],
            'profesi' => $post['profesi'],
            'iduser' => $iduser
        ];
        $arrMsg = $this->db->insert('rs_suratkelahiran_riwayat', $data);
        pesan_success_danger($arrMsg, "Riwayat Cetak Berhasil Disimpan", "Riwayat Cetak Gagal Disimpan.", "js");
    }

    public function getdata_cetaksuratkelahiran()
    {
        $norm = $this->input->post('norm');
        $data['surat']   = $this->db->select('namapasien(p.idperson) as namalengkap,pp.jeniskelamin, fhariindo(DATE_FORMAT(pp.tanggallahir,"%w")) as harilahir,DATE_FORMAT(pp.tanggallahir,"%d/%m/%Y") as tanggallahir, r.*')
            ->join('person_pasien p', 'p.norm=r.norm')
            ->join('person_person pp', 'pp.idperson=p.idperson')
            ->get_where("rs_suratkelahiran r", ['r.norm' => $norm])
            ->row_array();
        $data['petugas'] = $this->db->select("profesi,namapetugas, DATE_FORMAT(waktucetak,'%d/%m/%Y') as tanggalcetak")
            ->order_by('waktucetak', 'desc')
            ->limit(1)
            ->get_where("rs_suratkelahiran_riwayat", ['norm' => $norm])
            ->row_array();
        $data['ibu']     = $this->db->select('namapasien(p.idperson) as namaibu, p.alamat, b.namapekerjaan')
            ->join('person_person p', 'p.idperson=a.idperson')
            ->join('demografi_pekerjaan b', 'b.idpekerjaan=p.idpekerjaan')
            ->get_where('person_pasien_penanggungjawab a', ['a.norm' => $norm, 'a.idhubungan' => 2])
            ->row_array();
        $data['ayah']     = $this->db->select('namapasien(p.idperson) as namaayah, p.alamat, b.namapekerjaan')
            ->join('person_person p', 'p.idperson=a.idperson')
            ->join('demografi_pekerjaan b', 'b.idpekerjaan=p.idpekerjaan')
            ->get_where('person_pasien_penanggungjawab a', ['a.norm' => $norm, 'a.idhubungan' => 1])
            ->row_array();
        echo json_encode($data);
    }

    ///////////////////////pemeriksaanklinik///////////////////
    //-------------------------- vv Standar
    public function setting_pemeriksaanklinik()
    {
        return [
            'content_view'      => 'pelayanan/v_pemeriksaanklinik',
            'active_menu'       => 'pelayanan',
            'active_sub_menu'   => 'pemeriksaanklinik',
            'active_menu_level' => ''
        ];
    }
    public function pemeriksaanklinik() //list pemeriksaanklinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) //lihat define di atas
        {
            $data                = $this->setting_pemeriksaanklinik(); //letakkan di baris pertama
            $data['title_page']  = 'Pemeriksaan Klinik';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Pasien';
            $data['script_js']   = ['js_report-pemeriksaan', 'js_pemeriksaanklinik'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function list_pemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $post    = $this->input->post();
            $sql     = $this->mkombin->select_listdatapemeriksaanpasien();
            $numrows = $this->db->query($sql)->num_rows();
            $list    = $this->db->query($sql . ' limit ' . $post['start'] . ',' . $post['length'])->result();
            $data = [];
            $no = $post['start'];
            $menuskdp = '';
            $menupanggil = '';
            $menu = '';
            $waktuperiksa = '';
            foreach ($list as $obj) {

                $waktuperiksa =  DATE('Y-m-d', strtotime($obj->waktuperiksa));
                // Mark for verifKlaim
                $VerifMark = '';
                $VerifMark = (($this->pageaccessrightbap->checkAccessRight(V_MENUVERIFIKATORRALAN)
                    && (($obj->carabayar == 'jknnonpbi') || ($obj->carabayar == 'jknpbi'))
                    && ($obj->status == 'selesai')
                    && ($obj->isverifklaim != '1')
                ) ?
                    '   <i style="color:red;" class="fa fa-cog fa-spin fa-lg fa-fw" data-toggle="tooltip" data-placement="top" title="Belum Verifikasi Klaim"></i> '
                    : '');
                $row = [];
                $row[] = $obj->idpemeriksaan;
                $row[] = $obj->norm;
                $row[] = $obj->namalengkap;
                $row[] = $obj->nik;
                $row[] = $obj->nojkn;
                $row[] = $obj->nosep;
                $row[] = $obj->notelpon;
                $row[] = $obj->waktu;
                $row[] = $obj->tglperiksa;
                $row[] = $obj->namadokter;
                $row[] = $obj->unitasal;
                $row[] = $obj->unittujuan . ' <br><small class="text text-info">' . $obj->catatanrujuk . '</small>';
                $row[] = '<div style="padding-top:5px;"><label class="label label-' . $obj->kategoriskrining . '" style="padding:4.5px 8.5px;font-size:9.5px;">' . (($obj->kategoriskrining == null) ? '' : $obj->kategoriskrining) . '</label></div>';
                $row[] = (($obj->pasienprolanis == 1) ? '<i class="fa fa-check fa-2x text text-green"></i>' : '<i class="fa fa-close fa-2x text text-red"></i>');
                $row[] = ( ( $obj->tglskdpberikutnya == '0000-00-00' || substr( $obj->jenisrujukan,0,-1) != 'skdp' ) ? $obj->tanggal_kunjunganberikutnya : '<strong>'.$obj->jenisrujukan.' : <br>'. $obj->tglskdpberikutnya.'</strong>' ) ;
                $row[] = $obj->status . $VerifMark;
                /*menu antrian farmasi*/
                $menu = (($this->pageaccessrightbap->checkAccessRight(V_FARMASIGENERATEANTRIAN) && $obj->status !== 'antrian') ? '<a ' . (($obj->statusantrianfarmasi !== null) ? 'class=" btn bg-black btn-xs"' : 'onclick="farmasi_buatantrian(' . $obj->norm . ')" class=" btn bg-maroon btn-xs" ') . ql_tooltip('Buat Antrian Farmasi') . '><i class="fa fa-street-view"></i></a> ' : '');
                /*menu pemanggilan hasil*/
                $menu .= ($this->pageaccessrightbap->checkAccessRight(V_PEMANGGILANHASIL)) ? ' <a onclick="pemanggilanhasil(' . $obj->idpemeriksaan . ')"  class=" btn btn-danger btn-xs" ' . ql_tooltip('Pemanggilan Hasil') . '><i class="fa fa-bullhorn"></i></a> ' : '';

                /* menu panggilan */
                $menupanggil = (($this->pageaccessrightbap->checkAccessRight(V_MENUPEMANGGILANPASIENDIPOLI)) ? (($obj->idantrian !== null) ? '<a onclick="panggilpasien(' . $obj->idantrian . ')"  class=" btn btn-warning btn-xs" ' . ql_tooltip('Panggil Pasien') . '><i class="fa fa-bullhorn"></i></a> ' : '') : '');
                if ($obj->carabayar !== 'mandiri') { //jika pasien bpjs
                    //jika 1 maka sudah dibuatkan 
                    //                if($obj->buatskdp!=='1'){$menuskdp ='<a id="pemeriksaanklinikBuatSKDP" nobaris="'.$no.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idpemeriksaan.'" class=" btn btn-info btn-xs" '.ql_tooltip('Buat SKDP').'><i class="fa fa-file-text"></i></a> ';}
                    //                else{$menuskdp ='<a nobaris="'.$no.'" onclick="pemeriksaanklinik_cetakskdp('.$obj->idpendaftaran.')" class=" btn btn-info btn-xs" '.ql_tooltip('CETAK SKDP '.$obj->noskdp).'><i class="fa fa-print"></i> '.$obj->noskdp.'</a> ';}
                } else {
                    $menuskdp = '';
                }

                if ($this->pageaccessrightbap->checkAccessRight(V_MENUVERIFIKATORRALAN)) {
                    // menu verifikasi pasien bpjs ralan //pemeriksaanklinikVerifikasi
                    $menu .= ' <a id="pemeriksaanklinikView" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-success btn-xs" ' . ql_tooltip('Hasil Periksa') . '><i class="fa fa-eye"></i></a> ' . (($obj->status == 'selesai') ?  ' <a id="pemeriksaanklinikVerifikasi" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-warning btn-xs" ' . ql_tooltip('Verifikasi RALAN') . '><i class="fa fa-stethoscope"></i></a> ' : '');
                } else {
                    if ($obj->status == 'selesai') {/*jika status pemeriksaan selesai*/
                        $menu .= '<a id="pemeriksaanklinikView" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-success btn-xs" ' . ql_tooltip('Hasil Periksa') . '><i class="fa fa-eye"></i></a> ';
                        $menu .= (($this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANRADIOLOGI) or $this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANLABORAT)) ? '<a id="pemeriksaanklinikUpdatePeriksa" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-success btn-xs" ' . ql_tooltip('Pemeriksaan Penunjang') . '><i class="fa fa-stethoscope"></i></a>' : '');
                        $menu .= (($this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANEKOKARDIOGRAFI)) ? ' <a id="pemeriksaanklinikUpdateEkokardiografi" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class="btn btn-primary btn-xs" ' . ql_tooltip('Input Hasil Echocardiography') . '><i class="fa fa-pencil"></i></a>' : '');
                        $menu .= ' <a id="pemeriksaanklinikBatalselesai" alt2="' . $obj->idpemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Buka Pemeriksaan" ><i class="fa fa-folder-open"></i></a>';
                    } else if ($obj->status == 'batal') {/*jika status pemeriksaan adalah batal */
                        $menu .= ' <a id="pemeriksaanklinikView" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-success btn-xs" ' . ql_tooltip('Hasil Periksa') . '><i class="fa fa-eye"></i></a> <a id="pemeriksaanklinikBatalselesai" nobaris="' . $no . '" alt2="' . $obj->idpemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Buka Pemeriksaan" ><i class="fa  fa-stethoscope"></i></a>';
                    } else {
                        $menu .= $menupanggil;

                        $menu .= (($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANDOKTER)) ? '<a id="pemeriksaanklinikPeriksaDokter" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" tanggalperiksa="' . $obj->tglperiksa . '" norm="' . $obj->norm . '" class=" btn bg-maroon btn-xs" ' . ql_tooltip('Pemeriksaan Dokter') . '><i class="fa fa-stethoscope"></i></a>' : ' <a id="pemeriksaanklinikPeriksa" alt2="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" norm="' . $obj->norm . '" class=" btn btn-success btn-xs" ' . ql_tooltip('Pemeriksaan') . '><i class="fa fa-stethoscope"></i></a>');

                        //menu rujuk
                        $menu .= ' <a id="pemeriksaanklinikRujuk" norm="' . $obj->norm . '" idpendaftaran="' . $obj->idpendaftaran . '" alt="' . $obj->idpemeriksaan . '" idp="' . $obj->idperson . '" alt2="' . ql_helpertanggal($obj->tanggal) . '" class=" btn btn-primary btn-xs" ' . ql_tooltip('Tambah Layanan') . '><i class="fa fa-plus"></i></a> ';
                        //menu selesai
                        $menu .= ' <a id="pemeriksaanklinikSelesai" nobaris="' . $no . '" alt2="' . $obj->idpemeriksaan . '" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Selesai" ><i class="fa fa-check"></i></a>';
                        //menu tunda
                        $menu .= ' <a id="pemeriksaanklinikTunda" nobaris="' . $no . '" alt2="' . $obj->idpemeriksaan . '" class=" btn btn-warning btn-xs" ' . ql_tooltip('Tunda') . '><i class="fa  fa-exclamation-circle"></i></a>';
                        //menu batal
                        $menu .= (($this->pageaccessrightbap->checkAccessRight(V_MENUBATALPEMERIKSAANRALAN)) ? ' <a id="pemeriksaanklinikBatal" nobaris="' . $no . '" alt2="' . $obj->idpemeriksaan . '" class=" btn btn-danger btn-xs" ' . ql_tooltip('Batal Periksa') . '><i class="fa fa-minus-circle"></i></a> ' : '');
                    }
                }


                $row[] = $menu . ' <a onclick="cetak_hasilechocardiography(' . $obj->idpendaftaran . ',' . $obj->idunit . ')" class="btn btn-info btn-xs" ' . ql_tooltip('Hasil Echocardiography') . '><i class="fa fa-heartbeat fa"></i></a> <a onclick="cetak_hasillab(' . $obj->idpemeriksaan . ',' . $obj->idpendaftaran . ',' . $obj->idunit . ')" class="btn btn-info btn-xs" ' . ql_tooltip('Hasil Laboratorium') . '><i class="fa fa-flask"></i></a> <a onclick="cetak_hasilelektromedik(' . $obj->idpendaftaran . ',' . $obj->idunit . ')" class="btn btn-info btn-xs" ' . ql_tooltip('Hasil Elektromedik') . '><img src="' . base_url('assets/images/icons/radiasi.svg') . '" /></i></a>';
                $data[] = $row;
            }
            echo json_encode(["draw" => $post['draw'], "recordsTotal" => $numrows, "recordsFiltered" => $numrows, "data" => $data]);
        }
    }

    // /SKDP MASIH KURANG SAVE PENDAFTARAN PERIKSA
    // dan masih kurang auto generate nomor kontrol
    public function pemeriksaanklinik_buatskdp() //-- buat/save skdp panggil dari js_pemeriksaanklinik
    {
        $idp = $this->input->post('idp');
        $jadwal = $this->input->post('idjadwal');
        $pertimbangan = $this->input->post('pertimbangan');
        $tanggal = format_date_to_sql($this->input->post("tanggal"));

        $dtskdp = $this->db->query("select rs.noskdpql+1 as noskdpql from rs_skdp rs where  rs.idpendaftaransebelum='" . $idp . "'")->row_array();
        $this->rs_pemeriksaan_update_where(['buatskdp' => 1], ['idpendaftaran' => $idp]);

        // jika skdp lebih dari 2 kali
        $cek_jumlah_skdp_dibuat = $this->db->query("SELECT noskdpql FROM rs_skdp WHERE idpendaftaransebelum='".$idp."' ORDER BY tanggal,jam DESC")->result(); 
        if (!empty($cek_jumlah_skdp_dibuat) and count( $cek_jumlah_skdp_dibuat ) == 2) {
            echo json_encode(['status' => 'danger', 'message' => 'SKDP Gagal Dibuat.<br>No skdp tidak boleh lebih dari 2 kali.']);
            return false;
        }
        // buat skdp
        $data = [
            'noskdpql'     => ((empty($dtskdp)) ? '1' : $dtskdp['noskdpql']),
            'pertimbangan' => $pertimbangan,
            'tanggalskdp' => $tanggal,
            'idpendaftaransebelum' => $idp,
            'idjadwal' => $jadwal,
            'idpetugas' => $this->input->post('idpetugas'),
            'iduser'   => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')))
        ];
        $arrMsg = $this->db->insert("rs_skdp", $data);
        $nokontrol = $this->db->insert_id();

        $data += ['nokontrol' => $nokontrol]; //tambah array noskdpql di skdp
        $data += ['waktuperiksa' => $tanggal]; //tambah array tanggal di data skdp
        if (!$arrMsg) {
            return json_encode(['status' => 'danger', 'message' => 'Buat SKDP Gagal..!']);
        }
        $datapasien = $this->mkombin->get_dtpasienskdp($idp)->row_array();
        $datadiagnosa = $this->mkombin->get_dtdiagnosaskdp($idp)->result();
        echo json_encode((($arrMsg) ? ['diagnosa' => $datadiagnosa, 'pasien' => $datapasien, 'skdp' => $data, 'status' => 'success', 'message' => 'Buat SKDP Success.!',] : ['diagnosa' => $datadiagnosa, 'pasien' => $datapasien, 'skdp' => $data, 'status' => 'danger', 'message' => 'Buat SKDP Failed.!',]));
    }
    // fungsi cetak skdp pasien
    public function pemeriksaanklinik_cetakskdp()
    {
        $idpp       = $this->input->post('id');
        $nokontrol  = $this->input->post('nokontrol');
        
        $dataskdp   = $this->mkombin->get_dtskdppasien($idpp,$nokontrol)->row_array();
        $datapasien = $this->mkombin->get_dtpasienskdp($dataskdp['idpendaftaransebelum'])->row_array();
        $datadiagnosa = $this->mkombin->get_dtdiagnosaskdp($idpp)->result();
        echo json_encode(['diagnosa' => $datadiagnosa, 'pasien' => $datapasien, 'skdp' => $dataskdp]);
    }
    public function pemeriksaanklinik_ubahstatus() // PEMERIKSANKLINIK UBAH STATUS
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $table = 'rs_pemeriksaan';
            $id = $this->input->post('i');
            $status = $this->input->post('status');

            if ($status == 'sedang ditangani') {
                $isselesai = $this->db->query('select a.idpemeriksaan from rs_pemeriksaan a join person_pendaftaran b on b.idpendaftaran = a.idpendaftaran where a.idpemeriksaan = "' . $id . '" and b.idstatuskeluar = 2');
                if ($isselesai->num_rows() > 0) {
                    pesan_danger('Pemeriksaan Sudah Selesai, Pasien sudah menyelesaikan pembayaran. Harap input pemeriksaan sebelum pasien ke kasir.!', 'js');
                    return;
                }
            }

            $arrMsg = $this->ql->rs_pemeriksaan_insert_or_update($id, ['status' => $status]);
            $data = $this->db->query("select idpendaftaran, norm from rs_pemeriksaan where idpemeriksaan='$id'")->row_array();

            $datal = [
                'url'     => get_url(),
                'log'     => 'user:' . $this->session->userdata('username') . ';status:' . $status . ';idpemeriksaan:' . $id . ';idpendaftaran:' . $data['idpendaftaran'] . ';norm:' . $data['norm'],
                'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];
            $this->mgenerikbap->setTable('login_log');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        }
    }
    public function pemeriksaanklinik_batalselesai()
    {
        $id = $this->input->post('i'); //id pendaftaran
        $data = $this->db->query("select idpemeriksaan, norm, waktu from rs_pemeriksaan where idpendaftaran='$id'")->row_array();
        $table = 'rs_pemeriksaan';
        $status = $this->input->post('status'); //status pemeriksaan
        $arrMsg = $this->rs_pemeriksaan_update_where(['status' => $status], ['idpendaftaran' => $id]);
        if ($status == 'selesai') {
            // update pengurangan stok
            // $this->load->model('mtriggermanual');//--ok
            // $this->mtriggermanual->aukeu_tagihan($id,$status);
            $arrMsg = $this->insert_waktulayanan($data['norm'], $id, 'selesaiperiksa', $data['waktu'], 'kasir');
            $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id, ['idstatuskeluar' => '2']);
        } else if ($status == 'sedang ditangani') {
            // update penambahan stok
            // $this->load->model('mtriggermanual'); //--ok
            // $this->mtriggermanual->aukeu_tagihan($id,$status);   
            $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id, ['idstatuskeluar' => '1']);
        } else {
            $arrMsg = $this->ql->person_pendaftaran_insert_or_update($id, ['idstatuskeluar' => '3']);
        }
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:' . $this->session->userdata('username') . ';status:' . $status . ';idpemeriksaan:' . $data['idpemeriksaan'] . ';idpendaftaran:' . $id . ';norm:' . $data['norm'],
            'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
    }

    public function tampilfarmasibelumlist() // cari pasien
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $unit = $this->input->post('unit');
            $tglawal = $this->input->post('tglawal');
            $tglakhir = $this->input->post('tglakhir');
            echo json_encode($this->mkombin->rspemeriksaan_getdata($unit, format_date_to_sql($tglawal), format_date_to_sql($tglakhir), true));
        }
    }

    public function caripoliklinik() // cari poliklinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            echo json_encode($this->mkombin->cari_jadwalpoli($this->input->post('date')));
        }
    }
    public function cari_by_unit() //cari data pemeriksaan berdasarkan unit
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            echo json_encode($this->mgenerikbap->select_multitable("*", "rs_unit")->result());
        }
    }
    public function pemeriksaanklinik_caribhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $idunit = $this->session->userdata('idunitterpilih');
            if ($idunit) {
                $obat = $this->mkombin->fillstokobat($idunit);
                echo json_encode($obat);
            } else {
                echo json_encode($this->db->query("SELECT 0 idbarangpembelian, idbarang, namabarang, ifnull(totalstokbarangperunit(idbarang,24),0) as stok, rs.namasatuan, rb.hargaumum as hargajual, 0 kadaluarsa FROM rs_barang rb, rs_satuan rs where rb.ishidden=0 and rs.idsatuan=rb.idsatuan and rb.namabarang like '%" . $this->input->get('q') . "%'")->result());
            }
        }
    }

    public function pemeriksaanklinik_cariobatbhprawatjalan()
    {

        $carabayar = $this->input->get('carabayar');
        if ($carabayar == 'jknpbi' or $carabayar == 'jknnonpbi' or $carabayar == 'bpjstenagakerja') {
            $selectfield = ' rb.hargajual as hargajual, '; //pasien bpjs
        } else {
            $selectfield = ' rb.hargaumum as hargajual, '; //pasien umum dan asuransi
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $idunit = $this->session->userdata('idunitterpilih');
            if ($idunit) {
                $obat = $this->db->query("select rbp.idbarangpembelian,
                                            rbs.idbarang,  
                                            rb.namabarang,
                                            ifnull(rbs.stok,0) as stok,
                                            rs.namasatuan,  
                                            " . $selectfield . " 
                                            rbp.kadaluarsa
                                        from rs_barang rb
                                            left join rs_satuan rs on rs.idsatuan = rb.idsatuan
                                            join rs_barang_stok rbs on rbs.idbarang = rb.idbarang
                                            left join rs_barang_pembelian rbp on rbp.idbarangpembelian = rbs.idbarangpembelian
                                        where rb.ishidden=0 
                                            and rbs.idunit = '".$idunit."'
                                            and rbs.stok > 0
                                            and rb.namabarang like  '%" .$this->input->get('q'). "%'  
                                        ORDER BY rb.idbarang ASC, rbp.kadaluarsa ASC")->result();
                
                 echo json_encode($obat);
            } else {
                echo json_encode($this->db->query("SELECT 0 idbarangpembelian, idbarang, namabarang, ifnull(totalstokbarangperunit(idbarang,24),0) as stok, rs.namasatuan, " . $selectfield . " 0 kadaluarsa FROM rs_barang rb, rs_satuan rs where rb.ishidden=0 and rs.idsatuan=rb.idsatuan and rb.namabarang like '%" . $this->input->get('q') . "%'")->result());
            }
        }
    }


    public function pemeriksaanklinik_diagnosa()
    {
        $query = '';
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $id = $this->input->get('q');
            $jenisicd = $this->input->get('jenisicd');
            $idicd    = $this->input->get('idicd');

            $query = $this->db->where('ri.idjenisicd', $jenisicd);
            // echo json_encode($query);
            if ($jenisicd == 2) {
                $query = $this->db->where('ri.idicd', $idicd);
            }

            $query = $this->db->group_start()->like('ri.icd', $id)->or_like('ri.namaicd', $id)->group_end();
            // echo json_encode($query);
            if ($jenisicd != 2) {
                if ($jenisicd != 8) {
                    $query = $this->db->join('rs_mastertarif rm', 'rm.icd = ri.icd and rm.idkelas=1');
                }
            }

            $query = $this->db->order_by('order_icd', 'asc')->limit('20')->get('rs_icd ri')->result();
        }
        echo json_encode($query);
    }
    public function pemeriksaanklinik_cariicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $id = $this->input->get('q');
            $icd = $this->input->get('icd');
            $this->mgenerikbap->setTable('rs_icd');
            $this->db->where("idjenisicd='$icd'");
            $this->db->like("namaicd", "$id");
            $this->db->or_like("icd", "$id");
            $this->db->or_like("aliasicd", "$id");
            echo json_encode($this->mgenerikbap->select("icd, namaicd, aliasicd")->result());
        }
    }
    public function diagnosa_pilihpaket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $id = $this->input->post('x');
            $this->mgenerikbap->setTable("rs_paket_pemeriksaan");
            $x = $this->mgenerikbap->select("idpaketpemeriksaan, namapaketpemeriksaan", ['idjenisicd' => $id, 'idpaketpemeriksaanparent' => null])->result();
            echo json_encode($x);
        }
    }
    public function tampilhargayangdiubah()
    {
        $id = $this->input->post('x');
        $data = $this->mgenerikbap->setTable('rs_hasilpemeriksaan');
        $data = $this->mgenerikbap->select('jasaoperator,nakes,jasars,bhp,akomodasi,margin,sewa,potongantagihan', ['idhasilpemeriksaan' => $id])->row_array();
        echo json_encode($data);
    }
    public function savepaketygdiubah()
    {
        if ($this->isselesaipemeriksaan($this->input->post('idp'), '')) {
            $data = [
                'jasaoperator' => $this->input->post('jasaoperator'),
                'nakes' => $this->input->post('nakes'),
                'jasars' => $this->input->post('jasars'),
                'bhp' => $this->input->post('bhp'),
                'akomodasi' => $this->input->post('akomodasi'),
                'margin' => $this->input->post('margin'),
                'sewa' => $this->input->post('sewa'),
                'potongantagihan' => $this->input->post('potongan'),
            ];
            $this->mgenerikbap->setTable('rs_hasilpemeriksaan');
            $arrMsg = $this->mgenerikbap->update($data, $this->input->post('id'));
            pesan_success_danger($arrMsg, "Update Harga Success", "Update Harga Failed", "js");
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }
    public function pemeriksaanklinik_caridetailpasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $post = $this->input->post();
            $idperiksa = $this->input->post('i');
            $norm = $this->input->post('norm');
            $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
            $arrData['skrining']      = $this->mkombin->get_data_enum('person_pendaftaran', 'kategoriskrining');
            $arrData['statuspasienpulang'] = $this->mkombin->get_data_enum('person_pendaftaran', 'statuspulang');
            $arrData['pasien']        = (($modesave == 'verifikasi') ? $this->mkombin->pemeriksaanverifklaim_tampilpasien($norm, hilangkan_kutipganda($idperiksa)) : $this->mkombin->pemeriksaan_tampilpasien($norm, hilangkan_kutipganda($idperiksa)));
            $arrData['periksa']       = (($modesave == 'verifikasi') ? $this->mkombin->get_cariverifpemeriksaanpasien(hilangkan_kutipganda($idperiksa)) : $this->mkombin->get_caripemeriksaanpasien(hilangkan_kutipganda($idperiksa)));
            $arrData['kondisikeluar'] = $this->mkombin->get_data_enum('person_pendaftaran', 'kondisikeluar');
            $arrData['penanggung']    = $this->db->query("SELECT b.namalengkap, b.notelpon, (SELECT namahubungan from person_hubungan c where c.idhubungan=a.idhubungan) hubungan from person_pasien_penanggungjawab a, person_person b where a.norm='" . $norm . "' and a.statuspenanggung='penanggung'  and b.idperson=a.idperson")->result_array();
            $arrData['grupjadwal']    = (($modesave == 'verifikasi') ? $this->db->query("SELECT p.idpemeriksaan, if(p.isverifklaim=1,p.anamnesa_verifklaim,p.anamnesa) as anamnesa, 0 as idjadwalgrup, u.namaunit FROM rs_pemeriksaan p,  rs_unit u where  p.idpendaftaran='" . $arrData['periksa']['idpendaftaran'] . "' and u.idunit = p.idunit")->result() : $this->db->query("SELECT p.idpemeriksaan, p.anamnesa, 0 as idjadwalgrup, u.namaunit, u.validasidatasubyektif, u.validasidataobyektif FROM rs_pemeriksaan p, rs_unit u where  p.idpendaftaran='" . $arrData['periksa']['idpendaftaran'] . "' and u.idunit = p.idunit")->result());
            $arrData['blacklist']     = $this->db->query("SELECT ppb.norm, ru.namaunit FROM person_pasien_blacklistunit ppb join rs_unit ru on ru.idunit = ppb.idunit WHERE norm = '" . $norm . "'")->result_array();
            $arrData['keterangan_ekokardiografi'] = $this->db->query("SELECT rpk.keterangan FROM rs_hasilpemeriksaan rp
                                join rs_paketpemeriksaan_keterangan rpk on rpk.idpaketpemeriksaan = rp.idpaketpemeriksaan
                                WHERE rp.idpemeriksaan = '" . $idperiksa . "' and rp.idpaketpemeriksaan is not null
                                group by rp.idpaketpemeriksaan")->result_array();

            echo json_encode($arrData);
        }
    }

    public function getIDPerson()
    {
        $idpegawai = $_POST['idpegawai'];

        $query = $this->db->query("SELECT * FROM person_pegawai WHERE idpegawai = '" . $idpegawai . "'")->result_array();

        echo json_encode($query);
    }

    public function get_is_kunjungan_30hari()
    {
        $norm = $_POST['norm'];
        $idpendaftaran = $_POST['idpendaftaran'];

        //Check Kunjungan
        $querkunjungan = $this->db->query("SELECT * FROM person_pasien WHERE norm =" . $norm)->result_array();

        //Check Poli
        $querpoli = $this->db->query("SELECT * FROM person_pendaftaran WHERE idpendaftaran = " . $idpendaftaran)->result_array();

        $arrmerge = array_merge($querkunjungan, $querpoli);

        echo json_encode($arrmerge);
    }



    public function simpan_kunjungan_30hari()
    {
        $norm = $_POST['norm'];
        $mode = $_POST['mode'];

        $query = "UPDATE person_pasien SET is_kunjungan30hari = " . $mode . " WHERE norm =" . $norm;
        $execquery = $this->db->query($query);

        $result = ["result" => "Success", "msg" => "Kunjungan Lebih Dari 30 Hari Sukses!"];
        echo json_encode($result);
    }



    public function pemeriksaanklinikpenunjang_caridetailpasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $idperiksa = $this->input->post('i');
            $norm = $this->input->post('norm');

            $arrData['pasien'] = $this->mkombin->pemeriksaan_tampilpasien($norm, hilangkan_kutipganda($idperiksa));
            $arrData['periksa'] = $this->mkombin->get_caripemeriksaanpasien(hilangkan_kutipganda($idperiksa));
            $arrData['penanggung'] = $this->db->query("SELECT b.namalengkap, b.notelpon, (SELECT namahubungan from person_hubungan c where c.idhubungan=a.idhubungan) hubungan from person_pasien_penanggungjawab a, person_person b where a.norm='" . $norm . "' and a.statuspenanggung='penanggung'  and b.idperson=a.idperson")->result_array();
            echo json_encode($arrData);
        }
    }
    public function pemeriksaanklinik_refreshbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $idperiksa  = $this->input->post('i');
            $penggunaan = $this->mkombin->get_data_enum('rs_barangpemeriksaan', 'penggunaan');
            $periksa    = $this->mkombin->get_caripemeriksaanpasien($idperiksa);
            $post = $this->input->post();
            $modelist   = ((isset($post['modelist'])) ? $post['modelist'] : '');
            $bphpemeriksaan = $this->mkombin->pemeriksaan_listbhp($periksa['idpendaftaran'], $modelist); //,$this->input->post('jenisrawat')
            echo json_encode(['bhpperiksa' => $bphpemeriksaan, 'penggunaan' => $penggunaan]);
        }
    }
    public function obatbhpralan_refresh()
    {
        $post = $this->input->post();
        $modelist   = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $penggunaan = $this->mkombin->get_data_enum('rs_barangpemeriksaan', 'penggunaan');
        $jenisrawat = $this->input->post('jenisrawat');
        $jenispemakaian = ((isset($post['jenispemakaian'])) ? $post['jenispemakaian'] : '');

        $bphpemeriksaan = $this->mkombin->pemeriksaan_listbhp($this->input->post('i'), $modelist, $jenisrawat, $jenispemakaian);
        echo json_encode(['bhpperiksa' => $bphpemeriksaan, 'penggunaan' => $penggunaan]);
    }
    public function pemeriksaan_tampilpasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $norm = $this->input->post('x');
            $idperiksa = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('y'));
            $pasien = $this->mkombin->pemeriksaan_tampilpasien($norm, hilangkan_kutipganda($idperiksa));
            $periksa = $this->mkombin->get_caripemeriksaanpasien(hilangkan_kutipganda($idperiksa));
            echo json_encode(['p' => $pasien, 'per' => $periksa]);
        }
    }
    public function pemeriksaan_detail_tampil_pasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            echo json_encode($this->mkombin->pemeriksaandetail_tampil_pasien($this->input->post('x')));
        }
    }
    //pemeriksaan klinik ralan
    public function pemeriksaanklinik_periksa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data = [
                'content_view'      => 'pelayanan/v_pemeriksaanklinik-periksa',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanklinik',
                'active_menu_level' => ''
            ];
            $data['data_paket'] = $this->db->select('*')
                ->where('idpaketpemeriksaanparent', '0')
                ->join('rs_mastertarif_paket_pemeriksaan rmp', 'rmp.idpaketpemeriksaan = rp.idpaketpemeriksaan and rmp.idkelas=1')
                ->where('rp.idjenisicd', 4)
                ->get('rs_paket_pemeriksaan rp')
                ->result();
            $data['data_paketvitalsign'] = $this->db->query("select * from rs_paket_pemeriksaan rp where rp.idjenisicd='1'")->result();
            $data['title_page']  = 'Pemeriksaan Klinik';
            $data['mode']        = 'periksa';
            $data['table_title'] = 'Periksa Pasien';
            $data['script_js']   = ['bhp_ralan', 'riwayat_periksa', 'js_report-pemeriksaan', 'js_pemeriksaanklinik-periksa', 'js_asesmenmedis', 'js_pemeriksaanklinik-hasilradiografi', 'js_riwayat_allpemeriksaan_pasien'];
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_DATE, PLUG_CHECKBOX, PLUG_TIME];
            $this->load->view('v_index', $data);
        }
    }

    //pemeriksaan klinik ralan
    public function pemeriksaanklinik_radiologiefilm()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data = [
                'content_view'      => 'pelayanan/v_pemeriksaanklinik_radiologiefilm',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanklinik',
                'active_menu_level' => ''
            ];

            $data['idefilm'] = $this->uri->segment(3);
            $data['idpendaftaran'] = $this->uri->segment(4);

            $pasien = $this->db->query("SELECT pp.norm, namapasien(ppas.idperson) as namapasien FROM person_pendaftaran pp 
            join person_pasien ppas on ppas.norm = pp.norm
            WHERE pp.idpendaftaran = '" . $data['idpendaftaran'] . "'")->row_array();

            $data['title_page']  = 'Hasil Radiografi ' . ucwords(strtolower($pasien['namapasien'])) . ', No.RM ' . $pasien['norm'];
            $data['mode']        = 'periksa';
            $data['table_title'] = 'Periksa Pasien';
            $data['script_js']   = [];
            $data['plugins']     = [];
            $data['efilmselected'] = $this->db->get_where('rs_hasilpemeriksaan_efilm', array('idpendaftaran' => $data['idpendaftaran'], 'idhasilpemeriksaan_efilm' => $data['idefilm']))->result_array();
            $data['efilm']   = $this->db->get_where('rs_hasilpemeriksaan_efilm', array('idpendaftaran' => $data['idpendaftaran']))->result_array();
            $this->load->view('v_index', $data);
        }
    }

    //upload file ke nas storage
    public function upload_file_radiografi()
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $norm = $this->input->post('norm');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $id_efilm  = time();

        $tmpName  = $_FILES['fileEfilm']['tmp_name'];
        $extension = pathinfo($_FILES['fileEfilm']['name'], PATHINFO_EXTENSION);
        $fileName = $norm . '_' . $idpendaftaran . '_' . $id_efilm . '.' . $extension;
        $newFileName = '/radiologi/' . $fileName;
        $config = $this->ftp_upload();

        $upload = $this->ftp->connect($config);
        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS . $newFileName, 'ascii', 0775);
        $upload = $this->ftp->close();

        //simpan file
        if ($upload) {
            $data['idhasilpemeriksaan_efilm'] = $id_efilm;
            $data['idpendaftaran'] = $idpendaftaran;
            $data['idpemeriksaan'] = $idpemeriksaan;
            $data['efilm']  = $newFileName;
            $data['iduser'] = $iduser;
            $data['userip'] = ' IP:' . $_SERVER['REMOTE_ADDR'] . ' Client-Info:' . exec('getmac');
            $upload = $this->db->replace('rs_hasilpemeriksaan_efilm', $data);
        }

        $uploaded =  (($upload) ? ['error' => false, 'status' => 'success', 'message' => 'Unggah Hasil Radiografi Berhasil.'] : ['error' => true, 'status' => 'danger', 'message' => 'Unggah Hasil Radiografi Berhasil.']);
        echo json_encode($uploaded);
    }

    //hapus file di nas storage
    public function delete_file_radiografi()
    {
        $id_efilm =  $this->input->post('idefilm');
        $idpendaftaran =  $this->input->post('idpendaftaran');
        $fileName = $this->input->post('filename');

        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);
        $delete = $this->ftp->delete_file(FOLDERNASSIMRS . $fileName);
        $delete = $this->ftp->close();

        //hapus file 
        if ($delete) {
            $where['idhasilpemeriksaan_efilm'] = $id_efilm;
            $where['idpendaftaran'] = $idpendaftaran;
            $delete = $this->db->delete('rs_hasilpemeriksaan_efilm', $where);
        }
        $deleted =  (($delete) ? ['error' => false, 'status' => 'success', 'message' => 'Hapus Hasil Radiografi Berhasil.'] : ['error' => true, 'status' => 'danger', 'message' => 'Hapus Hasil Radiografi Berhasil.']);
        echo json_encode($deleted);
    }

    //view file di dari nas storage
    public function view_file_radiografi()
    {
        $post = $this->input->post();
        $idpendaftaran  = $this->input->post('idpendaftaran');
        $viewmode = ((isset($post['viewmode'])) ? $post['viewmode'] : '');
        $dtefilm = $this->db->get_where('rs_hasilpemeriksaan_efilm', ['idpendaftaran' => $idpendaftaran])->result_array();
        $data = '';
        $no = 0;
        if (empty($dtefilm)) {
            $data = 'Hasil tidak ada.';
        } else {
            foreach ($dtefilm as $arr) {
                if ($no == 2) {
                    $data .= '<div class="col-md-12">&nbsp;</div>';
                    $no = 0;
                }
                $data .= '<div class="col-md-4">
                        <a href="' . base_url('cpelayanan/pemeriksaanklinik_radiologiefilm/' . $arr['idhasilpemeriksaan_efilm'] . '/' . $arr['idpendaftaran']) . '" target="_blank"><img class="img-responsive" style="max-height:195px;min-width:195px;display: inline;" src="' . URLNASSIMRS . $arr['efilm'] . '" alt="Hasil Radiografi"></a>
                    </div><div class="col-md-1">
                    ' . (($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI) && empty($viewmode)) ? '
                        <a id="delete_file_radiografi" filename="' . $arr['efilm'] . '" idpendaftaran="' . $arr['idpendaftaran'] . '" idefilm="' . $arr['idhasilpemeriksaan_efilm'] . '" class="btn btn-xs btn-danger" ' . ql_tooltip('Hapus Hasil') . '><i class="fa fa-trash"></i></a>
                    '  : '') . '</div>';
                $no++;
            }
        }
        echo json_encode($data);
    }

    //pemeriksaan klinik ralan
    public function pemeriksaanklinik_periksa_trial()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data['content_view']       = 'pelayanan/v_pemeriksaanklinik_trial';
            $data['active_menu']        = 'pelayanan';
            $data['active_sub_menu']    = 'pemeriksaanklinik';
            $data['active_menu_level']  = '';

            $data['data_paket'] = $this->db->select('*')->where('idpaketpemeriksaanparent', '0')->where('idjenisicd!=1')->get('rs_paket_pemeriksaan')->result();
            $data['data_paketvitalsign'] = $this->db->query("select * from rs_paket_pemeriksaan rp where rp.idjenisicd='1'")->result();
            $data['title_page']  = 'Pemeriksaan Klinik';
            $data['mode']        = 'periksa';
            $data['table_title'] = 'Periksa Pasien';
            $data['script_js']   = ['bhp_ralan', 'riwayat_periksa', 'js_report-pemeriksaan', 'js_pemeriksaanklinik-periksa-trial', 'js_asesmenmedis'];
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_DATE, PLUG_CHECKBOX, PLUG_TIME];
            $this->load->view('v_index', $data);
        }
    }
    // update pemeriksaan penunjang    
    public  function pemeriksaanklinik_update_periksa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data                = $this->setting_pemeriksaanklinik();
            $data['title_page']  = 'Edit Pemeriksaan';
            $data['mode']        = 'editpemeriksaan';
            $data['edithasillab'] = $this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANLABORAT);
            $data['script_js']   = ['riwayat_periksa', 'js_report-pemeriksaan', 'js_pemeriksaanklinik-editview', 'js_pemeriksaanklinik-hasilradiografi'];
            $data['plugins']     = [PLUG_TEXTAREA];
            $this->load->view('v_index', $data);
        }
    }
    //pemeriksaanklinik_update_ekokardiografi
    public function pemeriksaanklinik_update_ekokardiografi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANEKOKARDIOGRAFI)) {
            $data = [
                'content_view'  => 'pelayanan/v_pemeriksaanklinik',
                'active_menu'   => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanklinik',
                'active_menu_level' => ''
            ];

            $data['title_page']  = 'Edit Hasil Ekokardiografi';
            $data['mode']        = 'editekokardiografi';
            $data['script_js']   = ['riwayat_periksa', 'js_pemeriksaanklinik-editekokardiografi'];
            $data['plugins']     = [PLUG_TEXTAREA];
            $this->load->view('v_index', $data);
        }
    }

    public function save_inputhasilekokardiograpi()
    {
        $post = $this->input->post();
        $data['keteranganekokardiografi'] = $post['keteranganekokardiografi'];
        $arrMsg = $this->db->update('person_pendaftaran', $data, ['idpendaftaran' => $post['idpendaftaran']]);

        $datal = ['url' => get_url(), 'log' => 'user:' . $this->session->userdata('username') . ';status:inputhasilradiografi;idpendaftaran:' . $post['idpendaftaran'] . ';keteranganekokardiografi-' . $post['keteranganekokardiografi'], 'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Input Hasil Success.!', $arrMsg . 'Input Hasil Failed.!');
        redirect(base_url('cpelayanan/pemeriksaanklinik'));
    }


    //verifikasi pasien jaminan ralan
    public function  pemeriksaanklinik_verifikasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data                = $this->setting_pemeriksaanklinik();
            $data['data_paket'] = $this->db->select('*')->where('idpaketpemeriksaanparent', '0')->where('idjenisicd!=1')->get('rs_paket_pemeriksaan')->result();
            $data['data_paketvitalsign'] = $this->db->query("select * from rs_paket_pemeriksaan rp where rp.idjenisicd='1'")->result();
            $data['title_page']  = 'Verifikasi';
            $data['mode']        = 'verifikasi';
            $data['script_js']   = ['bhp_ralan', 'riwayat_periksa', 'js_report-pemeriksaan', 'js_pemeriksaanklinik-periksa', 'js_pemeriksaanklinik-hasilradiografi'];
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_DATE, PLUG_CHECKBOX];
            $this->load->view('v_index', $data);
        }
    }
    // view pemeriksaan klinik ralan
    public function pemeriksaanklinik_view()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $data = [
                'content_view'     => 'pelayanan/v_pemeriksaanklinik-view',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanklinik',
                'active_menu_level' => ''
            ];
            $data['title_page']  = 'Hasil Pemeriksaan';
            $data['mode']        = 'viewperiksa';
            $data['table_title'] = 'Periksa Pasien';
            $data['script_js']   = ['bhp_ralan', 'riwayat_periksa', 'js_report-pemeriksaan', 'js_pemeriksaanklinik-view', 'js_pemeriksaanklinik-hasilradiografi', 'js_riwayat_allpemeriksaan_pasien'];
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_TIME];
            $this->load->view('v_index', $data);
        }
    }
    public function pemeriksaan_uwaktuperiksadokter()
    {
        $update = $this->db->update('rs_pemeriksaan', ['waktuperiksadokter' => date('Y-m-d H:i:s')], ['idpemeriksaan' => $this->input->post('id'), 'waktuperiksadokter' => NULL]);
        pesan_success_danger($update, 'Update success.', 'Update success.', 'js');
    }
    public function generate_tindakanrujukan($icdlabpaket, $icdlab, $icdrad, $idpendaftaran, $idpemeriksaan)
    {
        //save tindakan laborat
        if (empty(!$icdlab)) {
            $save = 'awal';
            foreach ($icdlab as $value) {
                $save = $this->cekmastertarif('Laboratorium', $idpendaftaran, $value, $idpemeriksaan, 'periksa', '', 'hasil');
            }

            if ($save != 'awal' && $save == false) {
                echo json_encode(['status' => 'danger', 'message' => 'Rujuk Berhasil, Tindakan Laborat Gagal Ditambahkan.']);
                return;
            }
        }


        //save tindakan elektromedik                
        if (empty(!$icdrad)) {
            $save = 'awal';
            foreach ($icdrad as $value) {
                $save = $this->cekmastertarif('Radiologi', $idpendaftaran, $value, $idpemeriksaan, 'periksa', '', 'hasil');
            }

            if ($save != 'awal' && $save == false) {
                echo json_encode(['status' => 'danger', 'message' => 'Rujuk Berhasil, Tindakan Elektromedik Gagal Ditambahkan.']);
                return;
            }
        }



        //save tindakan paket laborat
        if (empty(!$icdlabpaket)) {
            $save = 'awal';
            foreach ($icdlabpaket as $value) {
                $save = $this->cekmasterpakettariflaborat('Laboratorium', $idpendaftaran, $value, $idpemeriksaan, 'periksa', 0, 'hasil');
            }

            if ($save != 'awal' && $save == false) {
                echo json_encode(['status' => 'danger', 'message' => 'Rujuk Berhasil, Tindakan Elektromedik Gagal Ditambahkan.']);
                return;
            }
        }
    }
    /**
     * Rujuk Internal Rawat Jalan
     * @return type
     */
    public function pemeriksaanklinik_saverujuk()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $jenisrujuk    = $this->input->post('jenis');
        $icdlab        = $this->input->post('icdlaborat');
        $icdrad        = $this->input->post('icdelektromedik');
        $icdlabpaket   = $this->input->post('paketlaborat');
        $norm          = $this->input->post('norm');
        $idjadwal      = $this->input->post('idjadwal');
        $tanggalperiksa = $this->input->post('tanggalperiksa');

        //rujuk internal dalam satu pendaftaran/billing
        if ($jenisrujuk == 0) {
            $result = $this->rujuk_addpemeriksaan();
            if ($result['status'] == 'success') {
                $this->generate_tindakanrujukan($icdlabpaket, $icdlab, $icdrad, $idpendaftaran, $result['idpemeriksaan']);
                echo json_encode(['status' => 'success', 'message' => 'Rujuk Berhasil.']);
            } else {
                echo json_encode($result);
            }
        } else //save rujuk pendaftaran baru
        {
            $waktuperiksa = ql_formatdatetime(format_date_to_sql($tanggalperiksa));
            $unit = $this->db->select('idunit')->get_where('rs_jadwal', ['idjadwal' => $idjadwal])->result_array();
            $idpoliklinik = $unit[0]['idunit'];

            $this->db->trans_start();
            $data = [
                'norm'             => $norm,
                'nosep'            => '',
                'norujukan'        => '',
                'tanggalrujukan'   => date('Y-m-d'),
                'idkelas'          => 1,
                'idunit'           => $idpoliklinik,
                'waktu'            => date('Y-m-d H:i:s'),
                'waktuperiksa'     => $waktuperiksa,
                'jenispemeriksaan' => 'rajal',
                'carabayar'        => 'mandiri',
                'caradaftar'       => 'rujukinternal',
                'idklinikperujuk'  => 6, // perujuk -> queen latifa
                'jenisrujukan'     => 'rujukanbaru',
            ];
            $savedaftar = $this->ql->person_pendaftaran_insert_or_update(0, $data);

            if (!$savedaftar) {
                echo json_encode(['status' => 'danger', 'message' => 'Gagal Mendaftarkan Pemeriksaan.']);
                return;
            }
            $idpendaftaran = $this->db->insert_id();
            $this->db->trans_complete();

            $callid = generaterandom(8);
            $this->ql->antrianset_nopesan($waktuperiksa, $callid, $idpoliklinik); //buat no pesan            
            $this->mgenerikbap->setTable('helper_autonumber');
            $nopesan = $this->mgenerikbap->select('number', ['callid' => $callid])->row_array(); //siapkan data no antrian
            //simpan pemeriksaan
            $saveperiksa = $this->ql->pendaftaranpoli_savedaftarpemeriksaan($idpendaftaran, $idjadwal, $nopesan['number'], $norm, $callid, $tanggalperiksa);

            $idpemeriksaan = $this->db->select('idpemeriksaan')->get_where('rs_pemeriksaan', ['idpendaftaran' => $idpendaftaran])->row_array()['idpemeriksaan'];
            $this->generate_tindakanrujukan($icdlabpaket, $icdlab, $icdrad, $idpendaftaran, $idpemeriksaan);
            echo json_encode(['status' => 'success', 'message' => 'Rujuk Pemeriksaan Terjadwal Berhasil.']);
        }
    }
    // mahmud, clear --> save nilai pemeriksaan (vital sign atau laborat)
    public function pemeriksaan_savenilaipemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $post = $this->input->post();
            if ($post['c'] == 'text') {
                $data = ['nilaitext' => $post['b']];
            } else {
                $data = ['nilai' => $post['b']];
            }
            $id = $post['a'];
            $arrMsg = $this->db->update('rs_hasilpemeriksaan', $data, ['idhasilpemeriksaan' => $id]);
            pesan_success_danger($arrMsg, 'Update hasil Success..!', 'Update hasil Failed..!', 'js');
        }
    }
    /**
     * add or edit data alergi pemeriksaan
     */
    public function pemeriksaan_savealergi()
    {
        $post = $this->input->post();
        $idbarang = ($post['alergi_idbarang']) ? explode(',', $post['alergi_idbarang'])[1] : null;
        $arrMsg = $this->db->insert('person_pasien_alergi', ['idperson' => $post['alergi_idperson'], 'jenisalergi' => $post['jenisalergi'], 'idbarang' => $idbarang, 'keterangan' => $post['keterangan'], 'reaksi' => $post['reaksi']]);
        pesan_success_danger($arrMsg, "Update Data Alergi Berhasil", "Update Data Alergi Gagal", "js");
    }
    /**
     * list data alergi pemeriksaan
     */
    public function get_dataalergi()
    {
        $idp = '';
        if ($this->input->post('mode') == 'e') {
            $idp = $this->input->post('idp');
        }
        //        else
        //        {
        //            $idp = $this->db->select('idperson')->get('person_pasien',['norm'=>$this->input->post('n')])->row_array()['idperson'];
        //        }
        $getData = $this->db->query("SELECT ppa.idperson,ppa.id, ppa.jenisalergi, ppa.reaksi, concat(ifnull(ppa.keterangan,''), ifnull(rb.kode,''), ifnull(rb.namabarang,'')) keterangan from person_pasien_alergi ppa left join rs_barang rb on rb.idbarang=ppa.idbarang where ppa.idperson='" . $idp . (($this->input->post('mode') == 'e') ? ' and ppa.id="' . $this->input->post('id') . '"' : '')  . "'");
        echo json_encode(($this->input->post('mode') == 'e') ? $getData->row_array() : $getData->result());
    }

    //insert update get kesimpulan hasil
    public function insert_update_get_kesimpulanhasil()
    {
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $idpaketpemeriksaan = $this->input->post('idpaketpemeriksaan');
        $gruppaketperiksa = $this->input->post('gruppaketperiksa');
        $data = [
            'idpemeriksaan' => $idpemeriksaan,
            'idpaketpemeriksaan' => $idpaketpemeriksaan,
            'gruppaketperiksa' => $gruppaketperiksa
        ];

        if ($this->input->post('mode') == 'getdata') {
            echo json_encode($this->db->select('c.idpaketpemeriksaanrule, a.*')
                ->join('rs_paket_pemeriksaan b', 'b.idpaketpemeriksaan=a.idpaketpemeriksaan', 'left')
                ->join('rs_paket_pemeriksaan_rule c', 'c.idpaketpemeriksaanrule=b.idpaketpemeriksaanrule', 'left')
                ->get_where('rs_keteranganhasilpemeriksaan a', ['idpemeriksaan' => $idpemeriksaan, 'a.idpaketpemeriksaan' => $idpaketpemeriksaan, 'gruppaketperiksa' => $gruppaketperiksa])->row_array());
        } else if ($this->input->post('mode') == 'insertdata') {
            $data['keteranganhasil'] = $this->input->post('kesimpulan');
            $arrMsg = $this->db->insert('rs_keteranganhasilpemeriksaan', $data);
            pesan_success_danger($arrMsg, 'Simpan Kesimpulan Berhasil.', 'Simpan Kesimpulan Gagal.', 'js');
        } else if ($this->input->post('mode') == 'updatedata') {
            $arrMsg = $this->db->update('rs_keteranganhasilpemeriksaan', ['keteranganhasil' => $this->input->post('kesimpulan')], $data);
            pesan_success_danger($arrMsg, 'Update Kesimpulan Berhasil.', 'Update Kesimpulan Gagal.', 'js');
        }
    }
    //

    //save update pemeeriksaan penunjang ralan 
    public function pemeriksaanklinik_updatepenunjang()
    {
        $post = $this->input->post();
        $arrMsg = $this->db->update('person_pendaftaran', ['keteranganradiologi' => $post['keteranganradiologi'], 'saranradiologi' => $post['saranradiologi'], 'keteranganlaboratorium' => $post['keteranganlaboratorium']], ['idpendaftaran' => $post['idpendaftaran']]);
        $datal = ['url' => get_url(), 'log' => 'user:' . $this->session->userdata('username') . ';status:selesai;idpendaftaran:' . $post['idpendaftaran'] . ';keteranganradiologi-' . $post['keteranganradiologi'] . ';saranradiologi-' . $post['saranradiologi'] . ';keteranganlaboratorium-' . $post['keteranganlaboratorium'], 'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Save Pemeriksaan Success.!', $arrMsg . 'Save Pemeriksaan Failed.!');
        redirect(base_url('cpelayanan/pemeriksaanklinik'));
    }

    public function pemeriksaanklinik_save_periksa_trial_trial()
    {
        echo '<pre>';
        print_r($_POST);
        echo '</pre>';
    }

    public function pemeriksaanklinik_save_periksa_trial() //simpan pemeriksaan klinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $post = $this->input->post();

            # Pengkajian Awal Keperawatan
            $datakeperawatan = [
                "idpendaftaran" => $post['idpendaftaran'],
                "keluhanutama" => $post['txtkeluhanutama'],
                "apakahpasiendengankeluhankebidanan" => isset($post['chckapdkt']) ?  $post['chckapdkt'] : null,
                "riwayatkehamilansekarangG" => !isset($post['rksg']) ? NULL : $post['rksg'],
                "riwayatkehamilansekarangP" => !isset($post['rksp']) ? NULL : $post['rksp'],
                "riwayatkehamilansekarangA" => !isset($post['rksga']) ? NULL : $post['rksga'],
                "riwayatkehamilansekarangAh" => !isset($post['rksah']) ? NULL : $post['rksah'],
                "riwayatkehamilansekarangHPHT" => !isset($post['rksghpht']) ? NULL : $post['rksghpht'],
                "riwayatkehamilansekarangHPL" => !isset($post['rksghpl']) ? NULL : $post['rksghpl'],
                "riwayatkehamilansekarangUK" => !isset($post['rksguk']) ? NULL : $post['rksguk'],
                "ANCTerpadu" => isset($post['chckanc']) ? $post['chckanc'] : null,
                "ANCTerpadudi" => !isset($post['txtanc']) ? NULL : $post['txtanc'],
                "imunisasitt" => !isset($post['chckimunisasitt']) ? null : $post['chckimunisasitt'],
                "imunisaittke" => !isset($post['txtitt']) ? NULL : $post['txtitt'],
                "pendarahanpervaginam" => isset($post['chckppvm']) ? $post['chckppvm'] : null,
                "pendarahanpervaginamml" => empty($post['txtppvm']) ? NULL : $post['txtppvm'],
                "objectiveairway" => empty($post['chckairway']) ? "-" : $post['chckairway'],
                "objectivebreathing" => empty($post['chckbre']) ? "-" : $post['chckbre'],
                "objectivesuaranafas" => empty($post['chcksuana']) ? "-" : $post['chcksuana'],
                "CirculationPulsasiPerifer" => isset($post['chckpulper']) ? $post['chckpulper'] : null,
                "CirculationPulsasiKarotis" => isset($post['chckpulsasikarotis']) ? $post['chckpulsasikarotis'] : null,
                "CirculationWarnaKulit" => isset($post['chckwarnakulit']) ? $post['chckwarnakulit'] : null,
                "CirculationAkral" => isset($post['chckakral']) ? $post['chckakral'] : null,
                "CirculationCRT" => isset($post['chckcrt']) ?  $post['chckcrt'] : null,
                "CirculationKesadaran" => empty($post['chckkesadaran']) ? "-" : $post['chckkesadaran'],
                "nipsekspresiwajah" => empty($post['chckekw']) ? null : $post['chckekw'],
                "nipstangisan" => empty($post['chcktangisan']) ? null : $post['chcktangisan'],
                "nipspolapernafasan" => empty($post['chckpolaperna']) ? null : $post['chckpolaperna'],
                "nipstangan" => empty($post['chcktangan']) ? null : $post['chcktangan'],
                "nipskesadaran" => empty($post['chcknipskesadaran']) ? null : $post['chcknipskesadaran'],
                "nipstotalskor" => empty($post['totalskornips']) ? 0 : $post['totalskornips'],
                "is_nips" => empty($post['chckppnnips']) ? 0 : 1,
                "nipsinterpretasihasil" =>  isset($post['chckbayimengalaminyeri']) ? $post['chckbayimengalaminyeri'] : null,

                "is_flacc" => empty($post['chckppnflacc']) ? 0 : 1,
                "flaccwajah" => !isset($post['chckflaccwajah']) ? NULL : $post['chckflaccwajah'],
                "flacckaki" => !isset($post['chckflacckaki']) ? NULL : $post['chckflacckaki'],
                "flaccaktivitas"  => !isset($post['chckaktivitaspengkajian']) ? NULL : $post['chckaktivitaspengkajian'],
                "flaccmenangis" => !isset($post['chckpengkajianmenangis']) ? NULL : $post['chckpengkajianmenangis'],
                "flaccbersuara" => !isset($post['chckpengkajianbersuara']) ? NULL : $post['chckpengkajianbersuara'],

                "flacctotalskor" => !isset($post['totalskorflacc']) ? NULL : $post['totalskorflacc'],
                "flaccinterprestasihasil" => !isset($post['chckinteprestrasihasilflacc']) ? NULL : $post['chckinteprestrasihasilflacc'],

                "is_wbfps" => empty($post['chckppnwongbaker']) ? 0 : 1,
                "wbfps" => !isset($post['chckwongbaker']) ? NULL : $post['chckwongbaker'],
                "wbfpsintepretasihasil" => !isset($post['chckinterpretasiwbfps']) ? NULL : $post['chckinterpretasiwbfps'],


                "faktoryangmemperberat" => !isset($post['chckfym']) ? NULL : $post['chckfym'],
                "faktoryangmeringankan" => !isset($post['chckfymer']) ? NULL : $post['chckfymer'],
                "kualitasnyeri" => !isset($post['chckkualitasnyeri']) ? NULL : $post['chckkualitasnyeri'],
                "lokasinyeri" => empty($post['txtlokasinyeri']) ? NULL : $post['txtlokasinyeri'],


                "menjalar" => !isset($post['chckmenjalar']) ? NULL : $post['chckmenjalar'],

                "skalanyeri" =>  !isset($post['chckskalanyeri']) ? NULL : $post['chckskalanyeri'],
                "lamanyanyeri" =>  !isset($post['chcklamanyanyeri']) ? null : $post['chcklamanyanyeri'],

                "frekuensinyeri" => !isset($post['chckfrekuensinyeri']) ? NULL : $post['chckfrekuensinyeri'],
                "kualitasnyeriolainnya" => empty($post['txtlainnyakualitasnyeri']) ? NULL : $post['txtlainnyakualitasnyeri'],
                "menjalarjelaskan" => empty($post['txtjelaskanmenjalar']) ? NULL : $post['txtjelaskanmenjalar'],

                "pengkajiantrisikojatuhcaraberjalanpasien" => isset($post['chckprjatuh']) ? $post['chckprjatuh'] : null,
                "pengkajianrisikojatuhmenopangsaatakanduduk" => isset($post['chckmsad']) ? $post['chckmsad'] : null,
                "pengkajianrisikojatuhinterpretasihasil" => isset($post['chckintepretasihasilpengkajianrisikojatuh']) ? $post['chckintepretasihasilpengkajianrisikojatuh'] : null,
                "pengkajianrisikonutrisionalberatbadan" => isset($post['chckprnberatbadan']) ? $post['chckprnberatbadan'] : null,
                "pengkajianrisikonutrisionalnafsumakan" => isset($post['chckprnnafsumakan']) ? $post['chckprnnafsumakan'] : null,
                "pengkajianrisikonutrisionalinterpretasihasil" => isset($post['chckinterpretasiprn']) ?  $post['chckinterpretasiprn'] : null,

                "pengkajianstatusfungsionalpenggunaanalatbantu" => isset($post['chckpealaban']) ? $post['chckpealaban'] : NULL,
                "pengkajianstatusfungsionalpenggunaanalatbantusebutkan" => empty($post['txtoestafu']) ? NULL : $post['txtoestafu'],

                "pengkajianstatusfungsionaladl" => isset($post['chckadl']) ? $post['chckadl'] : null,

                "pengkajianstatuspsikologi" =>  isset($post['chckpenstatuspsikologi']) ? $post['chckpenstatuspsikologi'] : null,
                "pengkajianstatuspsikologilainnya" => empty($post['txtpengstatuspsikologi']) ? null : $post['txtpengstatuspsikologi'],

                "pengkajianstatussosialstatuspernikahan" =>  !isset($post['chckpengstasoekkudspi']) ? null : $post['chckpengstasoekkudspi'],

                "pengkajianstatussosialkendalakomunikasi" => isset($post['chckkendalakomunikasi']) ? $post['chckkendalakomunikasi'] : null,
                "pengkajianstatussosialkendalakomunikasisebutkan" => empty($post['txtkendadakomunikasilainnya']) ? NULL : $post['txtkendadakomunikasilainnya'],


                "pengkajianstatussosialyangmerawatdirumah" => isset($post['chckyangmerawatdirumah']) ? $post['chckyangmerawatdirumah'] : null,


                "pengkajianstatussosialyangmerawatdirumahsebutkan" => empty($post['txtadasebutkanyangmerawatdirumah']) ? NULL : $post['txtadasebutkanyangmerawatdirumah'],

                "pengkajianstatussosialpekerjaan" => isset($post['chckpekerjaanlainnya']) ? $post['chckpekerjaanlainnya'] : NULL,

                "pengkajianstatussosialpekerjaanlainnya" => empty($post['txtlainnyapekerjaan']) ? NULL : $post['txtlainnyapekerjaan'],
                "pengkajainstatussosialjaminankesehatan" => isset($post['chckjaminankesehatan']) ? $post['chckjaminankesehatan'] : NULL,
                "pengkajainstatussosialjaminankesehatanlainnya" => empty($post['txtjaminankesehatanlainnya']) ? NULL : $post['txtjaminankesehatanlainnya'],
                "pengkajianstatusososialapakahaadatindakannbertentangankepercayaa" => isset($post['chcktidanakanyangbertentangandengankepercayaanpasien']) ? $post['chcktidanakanyangbertentangandengankepercayaanpasien'] : null,
                "pengkajianstatussosialjelaskantindakan" => empty($post['txtjikaadajelaskantindakantersebut']) ? NULL : $post['txtjikaadajelaskantindakantersebut'],
                "diagnosasdki" => empty($post['diagnosasdki']) ? 0 : $post['diagnosasdki'],
                "diagnosadiluarsdki" => empty($post['diagnosadiluarsdki']) ? NULL : $post['diagnosadiluarsdki'],
                "planintervensinyerimengajarkanteknikrelaksasi" => isset($post['planintervensinyerimengajarkanteknikrelaksasi']) ? 1 : 0,
                "planintervensinyerimegaturposisiyangnyamanbagipasien" => isset($post['planintervensinyerimegaturposisiyangnyamanbagipasien']) ? 1 : 0,
                "planintervensinyerimengontrollingkungan" => isset($post['planintervensinyerimengontrollingkungan']) ? 1 : 0,
                "planintervensinyerimemberikanedukasikepadapasiendankeluarga" => isset($post['planintervensinyerimemberikanedukasikepadapasiendankeluarga']) ? 1 : 0,
                "planintervensinyerikoloborasidengandokter" => isset($post['planintervensinyerikoloborasidengandokter']) ? 1 : 0,
                "planintervensinyerichcklainnya"  => isset($post['planintervensinyerichcklainnya']) ? 1 : 0,
                "planintervensinyeritidakadaintervensi" => isset($post['planintervensinyeritidakadaintervensi']) ? 1 : 0,
                "planintervensinyerilainnya" => empty($post['txtintervensinyerilainnya']) ? NULL : $post['txtintervensinyerilainnya'],


                "planintervensirisikojatuhpakaikanpitabewarnakuning"  => isset($post['planintervensirisikojatuhpakaikanpitabewarnakuning']) ? 1 : 0,
                "planintervensirisikojatuhberikanedukasipencegahanjatuh"  => isset($post['planintervensirisikojatuhberikanedukasipencegahanjatuh']) ? 1 : 0,
                "planintervensirisikojatuhchcklainnya"  => isset($post['planintervensirisikojatuhchcklainnya']) ? 1 : 0,
                "planintervensirisikojatuhtidakadaintervensi" => isset($post['planintervensirisikojatuhtidakadaintervensi']) ? 1 : 0,
                "planintervensirisikojatuhlainnya" => empty($post['txtintervensirisikojatuhtidakadaintervensi']) ? NULL : $post['txtintervensirisikojatuhtidakadaintervensi'],



                "planintervensirisikonutrisionalkonsultasikanpadaahligizi" => isset($post['planintervensirisikonutrisionalkonsultasikanpadaahligizi']) ? 1 : 0,
                "planintervensirisikonutrisionalchcklainnya" => isset($post['planintervensirisikonutrisionalchcklainnya']) ? 1 : 0,
                "planintervensirisikonutrisionaltidakadaintervensi" => isset($post['planintervensirisikonutrisionaltidakadaintervensi']) ? 1 : 0,
                "planintervensirisikonutrisionallainnya" => empty($post['txtintervensirisikonutrisionallainnya']) ? NULL : $post['txtintervensirisikonutrisionallainnya'],



                "intervensistatusfungsionallibatkanpartisipasikeluargadalam" => isset($post['intervensistatusfungsionallibatkanpartisipasikeluargadalam']) ? 1 : 0,
                "intervensistatusfungsionalchcklainnya" => isset($post['intervensistatusfungsionalchcklainnya']) ? 1 : 0,
                "intervensistatusfungsionaltidakadaintervensi" => isset($post['intervensistatusfungsionaltidakadaintervensi']) ? 1 : 0,
                "planintervesistatusfungsionallainnya" => empty($post['txtintervensistatusfungsionallainnya']) ? NULL : $post['txtintervensistatusfungsionallainnya'],


                "intervensipsikologimenenangkanpasien" => isset($post['intervensipsikologimenenangkanpasien']) ? 1 : 0,
                "intervensipsikologimengajarkanteknikrelaksasi"  => isset($post['intervensipsikologimengajarkanteknikrelaksasi']) ? 1 : 0,
                "intervensipsikologiberkoloborasidengandokter" => isset($post['intervensipsikologiberkoloborasidengandokter']) ? 1 : 0,
                "intervensipsikologichcklainnya"  => isset($post['intervensipsikologichcklainnya']) ? 1 : 0,
                "intervensipsikologitidakadaintervensi" => isset($post['intervensipsikologitidakadaintervensi']) ? 1 : 0,
                "planintevensipsikologilainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'],


                "planintervensisosialmenjelaskankepadapasiendankeluarga"  => isset($post['planintervensisosialmenjelaskankepadapasiendankeluarga']) ? 1 : 0,
                "planintervensisosialmemfasilitasibiladahambatan"  => isset($post['planintervensisosialmemfasilitasibiladahambatan']) ? 1 : 0,
                "planintervensisosialmemberikanalternatiftindakanlain"  => isset($post['planintervensisosialmemberikanalternatiftindakanlain']) ? 1 : 0,
                "planintervensisosialmenjelaskanterkaittindakan"  => isset($post['planintervensisosialmenjelaskanterkaittindakan']) ? 1 : 0,
                "planintervensisosialchcklainnya" => isset($post['planintervensisosialchcklainnya']) ? 1 : 0,
                "planintervensisosialtidakadaintervensi"  => isset($post['planintervensisosialtidakadaintervensi']) ? 1 : 0,
                "planintervensisosiallainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'],


                "planintervensialergimemberitahukankepadadokter" => isset($post['planintervensialergimemberitahukankepadadokter']) ? 1 : 0,
                "planintervensialergiedukasikepadapasiendankeluarga"  => isset($post['planintervensialergiedukasikepadapasiendankeluarga']) ? 1 : 0,
                "planintervensialergichcklainnya"  => isset($post['planintervensialergichcklainnya']) ? 1 : 0,
                "planintervensialergitidakadaintervensi" => isset($post['planintervensialergitidakadaintervensi']) ? 1 : 0,
                "planintervensialergilainnya" => empty($post['txtintervensialergilainnya']) ? NULL : $post['txtintervensialergilainnya'],


                "stabilisasijalannafas" => empty($post['chckintervensikeperawatanlainnyastabilisasijalannafas']) ? 0 : 1,
                "keluarkanbendaasing" => empty($post['chckintervensikeperawatanlainnyakeluarkanbendaasing']) ? 0 : 1,
                "pasangfripharingeal" => empty($post['chckintervensikeperawatanlainnyapasangiripharingeal']) ? 0 : 1,
                "berikanbantuannafas" => empty($post['chckintervensikeperawatanlainnyaberikanbantuannafas']) ? 0 : 1,
                "berikanoksigenmelaluinasal" => empty($post['chckintervensikeperawatanlainnyaberikan02']) ? 0 : 1,
                "delegatifnebulizer" => empty($post['chckintervensikeperawatanlainnyadelegatifnebulizer']) ? 0 : 1,
                "monitortandavitalsecararperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortandatandavital']) ? 0 : 1,
                "monitortingkatkesadaransecaraperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortingkatkesadaran']) ? 0 : 1,
                "monitorekg" => empty($post['chckintervensikeperawatanlainnyamontorekg']) ? 0 : 1,
                "berikanposisisemifownler" => empty($post['chckintervensikeperawatanlainnyaberikanposisisemifowler']) ? 0 : 1,
                "pasangdowercatheter" => empty($post['chckintervensikeperawatanlainnyapasangdowercatheter']) ? 0 : 1,
                "monitorintakedanoutputcairan" => empty($post['chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan']) ? 0 : 1,
                "pasanginfusintravena" => empty($post['chckintervensikeperawatanlainnyapasanginfus']) ? 0 : 1,
                "lakukanperawatanluka" => empty($post['chckintervensikeperawatanlainnyalakukanperawatanluka']) ? 0 : 1,
                "koloborasidengandokteruntukiv" => empty($post['chckintervensikeperawatanlainnyakoloborasidengandokter']) ? 0 : 1,
                "kajiturgorkulit" => empty($post['chckintervensikeperawatanlainnyakajirturgorkulit']) ? 0 : 1,
                "pasangpengamanspalk" => empty($post['chckintervensikeperawatanlainnyapasangpengaman']) ? 0 : 1,
                "mengobservasitandaadanyakompartemen" => empty($post['chckintervensikeperawatanlainnyamengobservasitandatanda']) ? 0 : 1,
                "pasangngt" => empty($post['chckintervensikeperawatanlainnyaoasangngt']) ? 0 : 1,
                "intervensikeperawatanlainnya1"  => empty($post['txtintervensikeperawatanlainnya1']) ? NULL : $post['txtintervensikeperawatanlainnya1'],
                "intervensikeperawatanlainnya2" => empty($post['txtintervensikeperawatanlainnya2']) ? NULL : $post['txtintervensikeperawatanlainnya2'],
                "intervensikeperawatanlainnya3" => empty($post['txtintervensikeperawatanlainnya3']) ? NULL : $post['txtintervensikeperawatanlainnya3'],
                "intervensikeperawatanlainnya4" => empty($post['txtintervensikeperawatanlainnya4']) ? NULL : $post['txtintervensikeperawatanlainnya4'],
                "evaluasikeperawatan" => isset($post['chckaevaluasikeperawatan']) ? $post['chckaevaluasikeperawatan'] : null,
                "evaluasikeperawatansebutkan" => empty($post['txtevaluasikeperawatan']) ? NULL : $post['txtevaluasikeperawatan'],
                "evaluasipasiendankeluargamenerimaedukasi" => isset($post['chckapakahpasienbersediamenerimaedukasi']) ? $post['chckapakahpasienbersediamenerimaedukasi'] : null,
                "evaluasipasiendankeluargahambatan" => isset($post['chckapakahterdapathambatandalamedukasi']) ? $post['chckapakahterdapathambatandalamedukasi'] : null,
                " evaluasipasienhambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1,
                "evaluasipasienhambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1,
                "evaluasipasienhambatankognitif" => empty($post['chckkognitif']) ? 0 : 1,
                "evaluasipasienhambatanfisik" => empty($post['chckfisik']) ? 0 : 1,
                "evaluasipasienhambatanemosi" => empty($post['chckemosi']) ? 0 : 1,
                "evaluasipasienhambatanbahasa" => empty($post['chckbahasa']) ? 0 : 1,
                "evaluasipasienhambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1,
                "evaluasipasienhambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1,
                "hambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1,
                "hambatanemosi" => empty($post['chckemosi']) ? 0 : 1,
                "hambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1,
                "hambatanbahasa"  => empty($post['chckbahasa']) ? 0 : 1,
                "hambatankognitif" => empty($post['chckkognitif']) ? 0 : 1,
                "hambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1,
                "hambatanfisik"  => empty($post['chckfisik']) ? 0 : 1,
                "hambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1,
                "edukasidiagnosapenyakit"  => empty($post['chckdiagnosapenyakit']) ? 0 : 1,
                "edukasirehabilitasimedik" => empty($post['chckrehabilitasimedik']) ? 0 : 1,
                "edukasihakdankewajibanpasien" => empty($post['chckhakdankewajibanpasien']) ? 0 : 1,
                "edukasitandabahayadanperawatanbahayibarulahir" => empty($post['chcktandabahayadanperawatanbayi']) ? 0 : 1,
                "edukasiobatobatan"  => empty($post['chckobatobatan']) ? 0 : 1,
                "edukasimanajemennyeri" => empty($post['chckmanajemennyeri']) ? 0 : 1,
                "edukasikb" => empty($post['chckkb']) ? 0 : 1,
                "edukasidietdannutrisi" => empty($post['chckdietdannutrisi']) ? 0 : 1,
                "edukasipenggunaanalatmedia" => empty($post['chckpenggunaanalatmedia']) ? 0 : 1,
                "edukasitandabahayamasanifas" => empty($post['chcktandabahayamasanifas']) ? 0 : 1,
                "tanggalpasienpulang" => empty($post['txttanggalpasienpulang']) ? NULL : $post['txttanggalpasienpulang'],
                "jampasienpulang" => empty($post['txtjampasienpulang']) ? NULL : $post['txtjampasienpulang'] . ':00',
                "perawatpelaksana" => empty($post['txtperawatpelaksana']) ? NULL : $post['txtperawatpelaksana'],
                "evaluasipasiendankeluargajikayahamabatan"  => empty($post['evaluasipasiendankeluargajikayahamabatan']) ? NULL : $post['evaluasipasiendankeluargajikayahamabatan'],
                "txtlainnyafaktoryangmemperberat" => empty($post['txtlainnyafaktoryangmemperberat']) ? NULL : $post['txtlainnyafaktoryangmemperberat'],
                "txtlainnyafaktoryangmeringankan" => empty($post['txtlainnyafaktoryangmeringankan']) ? NULL : $post['txtlainnyafaktoryangmeringankan']
            ];

            $checkquery = $this->db->query("SELECT * FROM rs_ralan_asesmen_awal_keperawatan WHERE idpendaftaran = " . $post['idpendaftaran'])->result_array();

            if (!empty($checkquery)) {
                $this->db->where('idpendaftaran', $post['idpendaftaran'])->update('rs_ralan_asesmen_awal_keperawatan', $datakeperawatan);
            } else {
                $this->db->insert('rs_ralan_asesmen_awal_keperawatan', $datakeperawatan);
            }

            # Pengkajian Awal Medis
            $queridpegawai = $this->db->query("SELECT person_pegawai.idpegawai FROM `person_pegawai` WHERE person_pegawai.idperson =" . $_POST['idpersondokter'])->result_array();
            $idpegawai = $queridpegawai[0]['idpegawai'];

            $isitextrencanaperawatandantindakanselanjutanya = '';
            $isikontroldi = '';
            $isipilihanrawatjalan = '';

            if (!isset($post['chckrencanaperawatandantindakanlainnya'])) {
                $isitextrencanaperawatandantindakanselanjutanya .= null;
            } else if ($post['chckrencanaperawatandantindakanlainnya'] == 0) {
                $isitextrencanaperawatandantindakanselanjutanya .= "Rawat Jalan <br>";

                if (!isset($post['chckrawatjalanitem'])) {
                    $isitextrencanaperawatandantindakanselanjutanya .= null;
                } else if ($post['chckrawatjalanitem'] == 0) {
                    $isitextrencanaperawatandantindakanselanjutanya .= "Kontrol Di <br>";

                    if (!isset($post['chckkontroldi'])) {
                        $isipilihanrawatjalan .= null;
                    } else if ($post['chckkontroldi'] == "0") {
                        $isipilihanrawatjalan .= "UGD, Tanggal  ";
                        $isipilihanrawatjalan .= empty($post['tglugd']) ? '' : $post['tglugd'];
                        $isipilihanrawatjalan .= "<br>";
                    } else if ($post['chckkontroldi'] == 1) {
                        $isipilihanrawatjalan .= "Poliklinik ";
                        $isipilihanrawatjalan .= empty($post['txtkontroldi']) ? '' : $post['txtkontroldi'] . "<br>";
                    } else if ($post['chckkontroldi'] == 2) {
                        $isipilihanrawatjalan .= "PPK 1 <br>";
                    }

                    $isitextrencanaperawatandantindakanselanjutanya .=  $isipilihanrawatjalan;
                } else if ($post['chckrawatjalanitem'] == 1) {
                    $isitextrencanaperawatandantindakanselanjutanya .= "<br> Kontrol Bila Ada Keluhan";
                }
            } else if ($post['chckrencanaperawatandantindakanlainnya'] == 1) {
                $isitextrencanaperawatandantindakanselanjutanya .= isset($post['txtobservasipasiendiugd']) ? "Observasi lanjutan di UGD <br>" . $post['txtobservasipasiendiugd'] : "Observasi lanjutan di UGD <br>";
            } else if ($post['chckrencanaperawatandantindakanlainnya'] == 2) {
                if (isset($post['advisedpjp']) && isset($post['dokteryangmenerimaadvise']) && isset($post['verifikasidpjpdokterdpjd'])) {
                    $query1 = $this->db->query("select * from person_person join person_pegawai on person_person.idperson = person_pegawai.idperson WHERE person_pegawai.idpegawai =" . $post['dokteryangmenerimaadvise'])->result_array();
                    $query2 =  $this->db->query("select * from person_person join person_pegawai on person_person.idperson = person_pegawai.idperson WHERE person_pegawai.idpegawai =" . $post['verifikasidpjpdokterdpjd'])->result_array();

                    $isitextrencanaperawatandantindakanselanjutanya .= "Rawat inap, bangsal &nbsp;" . (isset($post['txtrawatinapbangsal']) ? $post['txtrawatinapbangsal'] : null) . "<br><br><strong>Advise DPJP</strong><br>" . (isset($post['advisedpjp']) ? $post['advisedpjp'] : null) . "<br><br><strong>Dokter Yang Menerima Advise:</strong>&nbsp;" . $query1[0]['namalengkap'] . "<br><br><strong>DPJP:</strong>&nbsp;" . $query2[0]['namalengkap'];
                }
            } else if ($post['chckrencanaperawatandantindakanlainnya'] == 3) {
                $isitextrencanaperawatandantindakanselanjutanya .= "Rujuk di&nbsp;" . isset($post['alamatrujukan']) ? $post['alamatrujukan'] : null;
            } else if ($post['chckrencanaperawatandantindakanlainnya'] == 4) {
                $isitextrencanaperawatandantindakanselanjutanya .= "Menolak Tindakan Rawat Inap";
            }

            $dataawalmedis = [
                "idpendaftaran" => $post['idpendaftaran'],
                // "idpemeriksaan" => $post['idperiksa'], 
                "carapasiendatang" => isset($post['chckcarapasiendatang']) ? $post['chckcarapasiendatang'] : null,
                "diantarnama" => !isset($post['chckcarapasiendatangdiantarnama']) ? null : $post['chckcarapasiendatangdiantarnama'],
                "diantaralamat" => !isset($post['chckcarapasiendatangdiantaralamat']) ? null : $post['chckcarapasiendatangdiantaralamat'],
                "diantarnotelp" => !isset($post['chckcarapasiendatangdiantarnotelp']) ? null : $post['chckcarapasiendatangdiantarnotelp'],
                "rujukandari"  => !isset($post['chckcarapasiendatangrujukandari']) ? null : $post['chckcarapasiendatangrujukandari'],
                "rujukanalaasan" => !isset($post['chckcarapasiendatangrujukanalasan']) ? null : $post['chckcarapasiendatangrujukanalasan'],
                "kategorikasus" => !isset($post['chckkategorikasus']) ? null : $post['chckkategorikasus'],
                "triasemetodeesi" => !isset($post['chhcktme']) ? null : $post['chhcktme'],
                "riwayatpenyakitsekarang" => !isset($post['riwayatpenyakitsekarang']) ? null : $post['riwayatpenyakitsekarang'],
                "riwayatpenyakitdahulu" => isset($post['chckboxrpd']) ? $post['chckboxrpd'] : null,
                "txtriwayatpenyakitdahulu" => !isset($post['txtriwayatpenyakitdahulu']) ? null : $post['txtriwayatpenyakitdahulu'],
                "obatyangrutindikonsumsi" => isset($post['chckobyrd'])  ? $post['chckobyrd'] : null,
                "txtobatyangrutindikonsumsi" => !isset($post['txtobyrd']) ? null : $post['txtobyrd'],
                "riwayatalergiobatdanmakanan" => isset($post['chckraodm'])  ? $post['chckraodm'] : null,
                "txtriwayatalergiobatdanmakanan" => !isset($post['txtraodm']) ? null : $post['txtraodm'],
                "keadaanumum" => $post['keadaanumum'],
                "gcse" => !isset($post['gcse']) ? null : $post['gcse'],
                "gcsv" => !isset($post['gcsv']) ? null : $post['gcsv'],
                "gcsm" => !isset($post['gcsm']) ? null : $post['gcsm'],
                "skalanyeri" => !isset($post['skalanyeri']) ? null : $post['skalanyeri'],
                "txtpemeriksaanfisik" => !isset($post['pemeriksaanfisik']) ? null : $post['pemeriksaanfisik'],
                "diagnosadilauricd10" => !isset($post['diagnosa']) ? null : $post['diagnosa'],
                "pemeriksaanpenunjangmedik" => isset($post['chckppm']) ? $post['chckppm'] : null,
                "txtpemeriksaanpenunjangmedik" => !isset($post['txtppm']) ? null : $post['txtppm'],
                "terapi" => !isset($post['txtterapi']) ? null : $post['txtterapi'],
                "catatanalainnya" => isset($post['chckcl']) ? $post['chckcl'] : null,
                "txtcatatanlainnya" => !isset($post['txtcatatanlainnya']) ? null : $post['txtcatatanlainnya'],
                "observasipasiendiugd" => !isset($post['chckobservasipasiendiugd']) ? null : $post['chckobservasipasiendiugd'],
                "observasipasiendiugdcatatan" => !isset($post['txtobservasipasiendiugd']) ? null : $post['txtobservasipasiendiugd'],
                "rencanaperawatandantindakanaselanjutnya" => !isset($post['chckrencanaperawatandantindakanlainnya']) ? null : $post['chckrencanaperawatandantindakanlainnya'],
                "rawatjalankontroldi" => !isset($post['chckrawatjalanitem']) ? null : $post['chckrawatjalanitem'],
                "kontroldiitem" => !isset($post['chckkontroldi']) ? null : $post['chckkontroldi'],
                "tglugd" => !isset($post['tglugd']) ? null : $post['tglugd'],
                "namapoliklinik" => !isset($post['txtkontroldi']) ? null : $post['txtkontroldi'],
                "bangsalrawatinap" => !isset($post['txtrawatinapbangsal']) ? null : $post['txtrawatinapbangsal'],
                "kondisipasiensebelummeninggalkanUGD" => !isset($post['chckkpsmu']) ? null : $post['chckkpsmu'],
                "meninggalduniatanggal" => !isset($post['txttanggalmeninggaldunia']) ? null : $post['txttanggalmeninggaldunia'],
                "meninggalduniajam" => !isset($post['txtjammeninggaldunia']) ? null : $post['txtjammeninggaldunia'],
                "tglselesaiasawal" => isset($post['tglselesaiasesmen']) ? $post['tglselesaiasesmen'] : null,
                "jamselesaiasawal" => !isset($post['txtselesaiasesmenawal']) ? null : $post['txtselesaiasesmenawal'] . ':00',
                "idpegawaidokter" =>  $idpegawai,
                "isadaracikan" => empty($post['adaracikan']) ? 1 : 0,
                "keteranganobat" => !isset($post['keteranganobat']) ? null : $post['keteranganobat'],
                "tglpasienpulangugd" => !isset($post['tglpasienpulangugd']) ? null : $post['tglpasienpulangugd'],
                "jampasienpulangugd" => !isset($post['jampasienpulangugd']) ? null : $post['jampasienpulangugd'] . ':00',
                "iddokterugd" => !isset($post['namadokterugd']) ? null : $post['namadokterugd'],
                "advisedpjp" => isset($post['advisedpjp']) ? $post['advisedpjp'] : null,
                "iddoktermenerimaadvise"  => empty($post['dokteryangmenerimaadvise']) ? null : $post['dokteryangmenerimaadvise'],
                "iddokterdpjp" => empty($post['verifikasidpjpdokterdpjd']) ? null : $post['verifikasidpjpdokterdpjd'],
                "rencanaperawatandantindakanselanjutnyanamarumahsakit" => empty($post['alamatrujukan']) ? null : $post['alamatrujukan'],
                "textrencanaperawatandantindakanselanjutanya" => $isitextrencanaperawatandantindakanselanjutanya
            ];

            $querycheck2 = $this->db->query("SELECT * FROM rs_pengkajian_awal_medis WHERE idpendaftaran = " . $post['idpendaftaran'])->result_array();

            if (!empty($querycheck2)) {
                $save = $this->db->where('idpendaftaran', $post['idpendaftaran'])->update('rs_pengkajian_awal_medis', $dataawalmedis);
            } else {
                $save = $this->db->insert('rs_pengkajian_awal_medis', $dataawalmedis);
            }


            $idperiksa = $this->input->post('idperiksa');
            $idpendaftaran = $this->input->post('idpendaftaran');
            $adaracikan = $this->input->post('adaracikan');
            $norm = $this->input->post('norm');
            $prolanis = $this->input->post('prolanis');
            $kunjunganberikutnya = $this->input->post('kunjunganberikutnya');
            $idunit = $this->input->post('idunit');
            $kondisikeluar = $this->input->post('kondisikeluar');
            $statuspasienpulang = $this->input->post('statuspasienpulang');
            $keteranganobat = qlreplace_quote($this->input->post('keteranganobat'));
            $keteranganradiologi = qlreplace_quote($this->input->post('keteranganradiologi'));
            $saranradiologi = qlreplace_quote($this->input->post('saranradiologi'));
            $keteranganlaboratorium = qlreplace_quote($this->input->post('keteranganlaboratorium'));
            $kategoriskrining = $this->input->post('kategoriskrining');

            $jeniswabah = $this->input->post('jeniswabah');
            $cindikasipasien = $this->input->post('cindikasipasien');
            $penanganan = $this->input->post('penanganan');
            $sebabpenularan = $this->input->post('sebabpenularan');
            $grupanamnesa = $this->input->post('grupanamnesa');

            $modesave = $this->input->post('modesave');
            $isselesai = 0;
            $isselesai = (($modesave != 'verifikasi') ? (($this->isselesaipemeriksaan($idpendaftaran, $modesave)) ? 0 : 1) : 1);


            if ($modesave == 'verifikasi') {
                $dt_person_pendaftaran = [
                    'keteranganobat_verifklaim' => $keteranganobat,
                    'keteranganradiologi_verifklaim' => $keteranganradiologi,
                    'saranradiologi_verifklaim' => $saranradiologi,
                    'keteranganlaboratorium_verifklaim' => $keteranganlaboratorium,
                    'isverifklaim' => 1
                ];
            } else {
                $dt_person_pendaftaran = [
                    'keteranganlaboratorium' => $keteranganlaboratorium,
                    'idstatuskeluar'        => (($isselesai) ? 2 : (($this->input->post('status') == 2) ? '2' : '1')),
                    'adaracikan'            => ((isset($adaracikan)) ? '1' : null),
                    'kondisikeluar'         => $kondisikeluar,
                    'statuspulang'          => $statuspasienpulang
                ];
                //kategoriskrining
                if (isset($kategoriskrining)) {
                    $dt_person_pendaftaran['kategoriskrining'] = $kategoriskrining;
                }

                //input hasil expertise
                if ($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) {
                    $dt_person_pendaftaran['saranradiologi'] = $saranradiologi;
                    $dt_person_pendaftaran['keteranganradiologi'] = $keteranganradiologi;
                }

                //input hasil ECHOCARDIOGRAPHY
                if ($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI)) {
                    $keteranganekokardiografi = $this->input->post('keteranganekokardiografi');
                    $dt_person_pendaftaran['keteranganekokardiografi'] = $keteranganekokardiografi;
                }
                //input keterangan obat
                if ($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN)) {
                    $dt_person_pendaftaran['keteranganobat'] = $keteranganobat;
                }

                //inputprolanis
                if ($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS)) {
                    $setProlanis = ((isset($prolanis)) ? '1' : '0');
                    $dt_person_pendaftaran['pasienprolanis'] = $setProlanis;
                    $dt_person_pendaftaran['tanggal_kunjunganberikutnya'] = $kunjunganberikutnya;
                }
            }
            $this->db->update('person_pendaftaran', $dt_person_pendaftaran, ['idpendaftaran' => $idpendaftaran]);
            // simpan atau ubah anamnesa ::: grup atau tidak
            if ($this->pageaccessrightbap->checkAccessRight(V_INPUTDATASUBYEKTIFRALAN)) {
                $this->pemeriksaanklinik_grupanamnesa($grupanamnesa, $modesave);
            }

            if (isset($cindikasipasien) && $modesave != 'verifikasi') {
                $dataIPW = ['idjeniswabah' => (isset($jeniswabah)) ? $jeniswabah : '', 'idpenangananwabah' => (isset($penanganan)) ? $penanganan : '', 'idsebabpenularan' => (isset($sebabpenularan)) ? $sebabpenularan : '', 'idpemeriksaan' => $idperiksa, 'serviceintegrasi' => (($idperiksa != '') ? '2' : '1')];
                $this->ql->rs_pemeriksaanwabah_insertupdate($idperiksa, $dataIPW);
            }
            // $arrMsg = $this->db->update('rs_pemeriksaan',)

            $arrMsg = $this->pemeriksaanklinik_save_pemeriksaan($idperiksa, $modesave, $isselesai);
            if ($modesave != 'verifikasi') {
                $this->mkombin->hitungtotaldiskon($idpendaftaran);
            }
            pesan_success_danger($arrMsg, 'Save Pemeriksaan Success.!', $arrMsg . 'Save Pemeriksaan Failed.!');
            redirect(base_url('cpelayanan/pemeriksaanklinik'));
        }
    }

    public function pemeriksaanklinik_save_periksa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK))
        {
            $idperiksa = $this->input->post('idperiksa');
            $idpendaftaran = $this->input->post('idpendaftaran');
            $adaracikan = $this->input->post('adaracikan');
            $norm = $this->input->post('norm');
            $prolanis = $this->input->post('prolanis');
            $kunjunganberikutnya = $this->input->post('kunjunganberikutnya');
            $idunit = $this->input->post('idunit');
            $kondisikeluar = $this->input->post('kondisikeluar');
            $statuspasienpulang= $this->input->post('statuspasienpulang');
            $keteranganobat = qlreplace_quote($this->input->post('keteranganobat'));
            $keteranganradiologi = qlreplace_quote($this->input->post('keteranganradiologi'));
            $saranradiologi = qlreplace_quote($this->input->post('saranradiologi'));
            $keteranganlaboratorium = qlreplace_quote($this->input->post('keteranganlaboratorium'));
            $kategoriskrining = $this->input->post('kategoriskrining');

            $jeniswabah = $this->input->post('jeniswabah');
            $cindikasipasien = $this->input->post('cindikasipasien');
            $penanganan = $this->input->post('penanganan');
            $sebabpenularan = $this->input->post('sebabpenularan');
            $grupanamnesa = $this->input->post('grupanamnesa');
            
            $modesave = $this->input->post('modesave');
            $isselesai=0;
            $isselesai = (($modesave!='verifikasi')? (($this->isselesaipemeriksaan($idpendaftaran,$modesave)) ? 0 : 1 ) : 1 );
            
            
            if( $modesave=='verifikasi'){
                $dt_person_pendaftaran = [
                'keteranganobat_verifklaim'=>$keteranganobat,
                'keteranganradiologi_verifklaim'=>$keteranganradiologi,
                'saranradiologi_verifklaim'=>$saranradiologi,
                'keteranganlaboratorium_verifklaim'=>$keteranganlaboratorium, 
                'isverifklaim'=>1
                ];
            }else{
                $dt_person_pendaftaran = [
                'keteranganlaboratorium'=> $keteranganlaboratorium,
                'idstatuskeluar'        => (($isselesai) ? 2 : (($this->input->post('status')==2)? '2' :'1') ),
                'adaracikan'            => ((isset($adaracikan))?'1':null),
                'kondisikeluar'         => $kondisikeluar,
                'statuspulang'          => $statuspasienpulang
                ];
                //kategoriskrining
                if(isset($kategoriskrining))
                {
                    $dt_person_pendaftaran['kategoriskrining'] = $kategoriskrining;
                }
                
                //input hasil expertise
                if($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI))
                {
                    $dt_person_pendaftaran['saranradiologi'] = $saranradiologi;
                    $dt_person_pendaftaran['keteranganradiologi'] = $keteranganradiologi;
                }
                
                //input hasil ECHOCARDIOGRAPHY
                if($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI))
                {
                    $keteranganekokardiografi = $this->input->post('keteranganekokardiografi');
                    $dt_person_pendaftaran['keteranganekokardiografi'] = $keteranganekokardiografi;
                }
                //input keterangan obat
                if($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN))
                {
                    $dt_person_pendaftaran['keteranganobat'] = $keteranganobat;
                }
                
                //inputprolanis
                if($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS))
                {
                    $setProlanis = ((isset($prolanis))?'1': '0');
                    $dt_person_pendaftaran['pasienprolanis'] = $setProlanis;
                    $dt_person_pendaftaran['tanggal_kunjunganberikutnya'] = $kunjunganberikutnya;                    
                }
                
            }
            $this->db->update('person_pendaftaran',$dt_person_pendaftaran,['idpendaftaran'=>$idpendaftaran]);
            // simpan atau ubah anamnesa ::: grup atau tidak
            if($this->pageaccessrightbap->checkAccessRight(V_INPUTDATASUBYEKTIFRALAN))
            {
                $this->pemeriksaanklinik_grupanamnesa($grupanamnesa,$modesave);
            }
            
            if(isset($cindikasipasien) && $modesave!='verifikasi')
            {
                $dataIPW=['idjeniswabah'=>(isset($jeniswabah)) ? $jeniswabah : '','idpenangananwabah'=>(isset($penanganan)) ? $penanganan : '','idsebabpenularan'=>(isset($sebabpenularan)) ? $sebabpenularan : '','idpemeriksaan'=>$idperiksa,'serviceintegrasi'=>(($idperiksa!='')?'2':'1')];
                $this->ql->rs_pemeriksaanwabah_insertupdate($idperiksa,$dataIPW);
            }
            // $arrMsg = $this->db->update('rs_pemeriksaan',)
                        
            $arrMsg = $this->pemeriksaanklinik_save_pemeriksaan($idperiksa,$modesave,$isselesai);
            if($modesave!='verifikasi'){$this->mkombin->hitungtotaldiskon($idpendaftaran);}
            pesan_success_danger($arrMsg, 'Save Pemeriksaan Success.!', $arrMsg.'Save Pemeriksaan Failed.!'); 
            redirect(base_url('cpelayanan/pemeriksaanklinik'));
        }
    }


    //mahmud, clear
    //simpan pemeriksaan klinik
    // public function pemeriksaanklinik_save_periksa()//simpan pemeriksaan klinik
    // {
    //     if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK))
    //     {   
    //         $post = $this->input->post();

    //         # Pengkajian Awal Keperawatan
    //         $datakeperawatan = [
    //             "idpendaftaran" => $post['idpendaftaran'], 
    //             "idpemeriksaan" => $post['idperiksa'], 
    //             "keluhanutama" => $post['txtkeluhanutama'],
    //             "apakahpasiendengankeluhankebidanan" => empty($post['chckapdkt']) ? null : $post['chckapdkt'],
    //             "riwayatkehamilansekarangG" => !isset($post['rksg']) ? NULL : $post['rksg'],
    //             "riwayatkehamilansekarangP" => !isset($post['rksp']) ? NULL : $post['rksp'],
    //             "riwayatkehamilansekarangA" => !isset($post['rksga']) ? NULL : $post['rksga'],
    //             "riwayatkehamilansekarangAh" => !isset($post['rksah']) ? NULL : $post['rksah'],
    //             "riwayatkehamilansekarangHPHT" => !isset($post['rksghpht']) ? NULL : $post['rksghpht'],
    //             "riwayatkehamilansekarangHPL" => !isset($post['rksghpl']) ? NULL : $post['rksghpl'],
    //             "riwayatkehamilansekarangUK" =>!isset($post['rksguk']) ? NULL : $post['rksguk'],
    //             "ANCTerpadu" => empty($post['chckanctidak']) ? 1 : 0,
    //             "ANCTerpadudi" => !isset($post['txtanc']) ? NULL : $post['txtanc'],
    //             "imunisasitt" => empty($post['chcktdkitt']) ? 1 : 0,
    //             "imunisaittke" => !isset($post['txtitt']) ? NULL : $post['txtitt'],
    //             "pendarahanpervaginam" => empty($post['chcktdkppvm']) ? 1 : 0,
    //             "pendarahanpervaginamml" => empty($post['txtppvm']) ? NULL : $post['txtppvm'],
    //             "objectiveairway" => empty($post['chckairway']) ? "-" : $post['chckairway'],
    //             "objectivebreathing" => empty($post['chckbre']) ? "-" : $post['chckbre'],
    //             "objectivesuaranafas" => empty($post['chcksuana']) ? "-" : $post['chcksuana'],
    //             "CirculationPulsasiPerifer" => empty($post['chckpulpertidakteraba']) ? 1 : 0,
    //             "CirculationPulsasiKarotis" => empty($post['chckpulsasikarotistidakteraba']) ? 1 : 0,
    //             "CirculationWarnaKulit" => empty($post['chckwarnakulitpucat']) ? 1 : 0,
    //             "CirculationAkral" => empty($post['chckakraldingin']) ? 1 : 0,
    //             "CirculationCRT" => empty($post['chckcrtlebih2dtk']) ? 1 : 0,
    //             "CirculationKesadaran" => empty($post['chckkesadaran']) ? "-" : $post['chckkesadaran'],
    //             "nipsekspresiwajah" => empty($post['chckekw']) ? "-" : $post['chckekw'],
    //             "nipstangisan" => empty($post['chcktangisan']) ? "-" : $post['chcktangisan'],
    //             "nipspolapernafasan" => empty($post['chckpolaperna']) ? "-" : $post['chckpolaperna'],
    //             "nipstangan" => empty($post['chcktangan']) ? "-" : $post['chcktangan'],
    //             "nipskesadaran" => empty($post['chckkesadaran']) ? "-" : $post['chckkesadaran'],
    //             "nipstotalskor" => empty($post['totalskornips']) ? 0 : $post['totalskornips'],
    //             "is_nips" => empty($post['chckppnnips']) ? 0 : 1,
    //             "nipsinterpretasihasil" =>  empty($post['chckbayitidakmengalaminyeri']) ? 1: 0,
    //             "is_flacc" => empty($post['chckppnflacc']) ? 0 : 1,
    //             "flacckaki" => !isset($post['chckflacckaki']) ? NULL : $post['chckflacckaki'],
    //             "flaccwajah" => !isset($post['chckflaccwajah']) ? NULL : $post['chckflaccwajah'],
    //             "flaccaktivitas"  => !isset($post['chckaktivitaspengkajian']) ? NULL : $post['chckaktivitaspengkajian'],
    //             "flaccmenangis" => !isset($post['chckpengkajianmenangis']) ? NULL : $post['chckpengkajianmenangis'],
    //             "flaccbersuara" => !isset($post['chckpengkajianbersuara']) ? NULL : $post['chckpengkajianbersuara'],
    //             "flacctotalskor" => !isset($post['totalskorflacc']) ? NULL : $post['totalskorflacc'],
    //             "flaccinterprestasihasil" => !isset($post['chckinteprestrasihasilflacc']) ? NULL : $post['chckinteprestrasihasilflacc'],
    //             "is_wbfps" => empty($post['chckppnwongbaker']) ? 0 : 1,
    //             "wbfps" => !isset($post['chckwongbaker']) ? NULL : $post['chckwongbaker'],
    //             "wbfpsintepretasihasil" => !isset($post['chckinterpretasiwbfps']) ? NULL : $post['chckinterpretasiwbfps'],
    //             "faktoryangmemperberat" => !isset($post['chckfym']) ? NULL : $post['chckfym'],
    //             "faktoryangmeringankan" => !isset($post['chckfymer']) ? NULL : $post['chckfymer'],
    //             "kualitasnyeri" => !isset($post['chckkualitasnyeri']) ? NULL : $post['chckkualitasnyeri'],
    //             "lokasinyeri" => empty($post['txtlokasinyeri']) ? NULL : $post['txtlokasinyeri'],
    //             "menjalar" => empty($post['txtjelaskanmenjalar']) ? NULL : $post['txtjelaskanmenjalar'],
    //             "skalanyeri" =>  !isset($post['chckskalanyeri']) ? NULL : $post['chckskalanyeri'] ,
    //             "lamanyanyeri" =>  empty($post['chcklamanyanyeridiatas30menit']) ? 0 : 1,
    //             "frekuensinyeri" => !isset($post['chckfrekuensinyeri']) ? NULL : $post['chckfrekuensinyeri'],
    //             "kualitasnyeriolainnya" => empty($post['txtlainnyakualitasnyeri']) ? NULL : $post['txtlainnyakualitasnyeri'],
    //             "menjalarjelaskan" => empty($post['txtjelaskanmenjalar']) ? NULL : $post['txtjelaskanmenjalar'],
    //             "pengkajiantrisikojatuhcaraberjalanpasien" => isset($post['chckprjatuhcvptsjdmabyatidak']) ? $post['chckprjatuhcvptsjdmabyatidak'] : null,
    //             "pengkajianrisikojatuhmenopangsaatakanduduk" => isset($post['chckmsad']) ? $post['chckmsad'] : null,
    //             "pengkajianrisikojatuhinterpretasihasil" => !isset($post['chckintepretasihasilpengkajianrisikojatuh']) ? null : $post['chckintepretasihasilpengkajianrisikojatuh'],

    //             "pengkajianrisikonutrisionalberatbadan" => isset($post['chckprnberatbadan']) ? $post['chckprnberatbadan'] : null,
    //             "pengkajianrisikonutrisionalnafsumakan" => isset($post['chckprnnafsumakan']) ? $post['chckprnnafsumakan'] : null,
    //             "pengkajianrisikonutrisionalinterpretasihasil" => !isset($post['chckinterpretasiprn']) ? null : $post['chckinterpretasiprn'],


    //             "pengkajianstatusfungsionalpenggunaanalatbantu" => isset($post['chckpealaban']) ? $post['chckpealaban'] : null,
    //             "pengkajianstatusfungsionalpenggunaanalatbantusebutkan" => empty($post['txtoestafu']) ? NULL : $post['txtoestafu'],
    //             "pengkajianstatusfungsionaladl" => empty($post['chckadltidak']) ? 1 : 0,


    //             "pengkajianstatuspsikologi" =>  isset($post['chckpenstatuspsikologi']) ? $post['chckpenstatuspsikologi'] :null,



    //             "pengkajianstatuspsikologilainnya" => empty($post['txtpengstatuspsikologi']) ? 0 : $post['txtpengstatuspsikologi'],

    //             "pengkajianstatussosialstatuspernikahan" =>  isset($post['chckpengstasoekkudspi']) ? $post['chckpengstasoekkudspi'] :null,
    //             "pengkajianstatussosialkendalakomunikasi" => empty($post['chckkendalakomunikasitidakada']) ? 1: 0,
    //             "pengkajianstatussosialkendalakomunikasisebutkan" => empty($post['txtkendadakomunikasilainnya']) ? NULL : $post['txtkendadakomunikasilainnya'],
    //             "pengkajianstatussosialyangmerawatdirumah" => empty($post['chckyangmerawatdirumahtidakada""']) ? 1: 0,
    //             "pengkajianstatussosialyangmerawatdirumahsebutkan" => empty($post['txtadasebutkanyangmerawatdirumah']) ? NULL : $post['txtadasebutkanyangmerawatdirumah'],
    //             "pengkajianstatussosialpekerjaan" => isset($post['chckpekerjaanlainnya']) ? $post['chckpekerjaanlainnya'] : NULL , 
    //             "pengkajianstatussosialpekerjaanlainnya" => empty($post['txtlainnyapekerjaan']) ? NULL : $post['txtlainnyapekerjaan'], 
    //             "pengkajainstatussosialjaminankesehatan" => isset($post['chckjaminankesehatan']) ? $post['chckjaminankesehatan'] : NULL,  
    //             "pengkajainstatussosialjaminankesehatanlainnya" => empty($post['txtjaminankesehatanlainnya']) ? NULL : $post['txtjaminankesehatanlainnya'],  
    //             "pengkajianstatusososialapakahaadatindakannbertentangankepercayaa" => empty($post['chcktidanakanyangbertentangandengankepercayaanpasientidak']) ? 1 : 0,
    //             "pengkajianstatussosialjelaskantindakan" => empty($post['txtjikaadajelaskantindakantersebut']) ? NULL : $post['txtjikaadajelaskantindakantersebut'],
    //             "diagnosasdki" => empty($post['diagnosasdki']) ? 0 : $post['diagnosasdki'],
    //             "diagnosadiluarsdki" => empty($post['diagnosadiluarsdki']) ? NULL : $post['diagnosadiluarsdki'],
    //             "planintervensinyerimengajarkanteknikrelaksasi" => isset($post['planintervensinyerimengajarkanteknikrelaksasi']) ? 1 : 0,
    //             "planintervensinyerimegaturposisiyangnyamanbagipasien" => isset($post['planintervensinyerimegaturposisiyangnyamanbagipasien']) ? 1 : 0 ,
    //             "planintervensinyerimengontrollingkungan" => isset($post['planintervensinyerimengontrollingkungan']) ? 1 : 0,
    //             "planintervensinyerimemberikanedukasikepadapasiendankeluarga" => isset($post['planintervensinyerimemberikanedukasikepadapasiendankeluarga']) ? 1 : 0,
    //             "planintervensinyerikoloborasidengandokter" => isset($post['planintervensinyerikoloborasidengandokter']) ? 1 : 0,
    //             "planintervensinyerichcklainnya"  => isset($post['planintervensinyerichcklainnya']) ? 1 : 0,
    //             "planintervensinyeritidakadaintervensi" => isset($post['planintervensinyeritidakadaintervensi']) ? 1 : 0,
    //             "planintervensinyerilainnya" => empty($post['txtintervensinyerilainnya']) ? NULL : $post['txtintervensinyerilainnya'],


    //             "planintervensirisikojatuhpakaikanpitabewarnakuning"  => isset($post['planintervensirisikojatuhpakaikanpitabewarnakuning']) ? 1 : 0,
    //             "planintervensirisikojatuhberikanedukasipencegahanjatuh"  => isset($post['planintervensirisikojatuhberikanedukasipencegahanjatuh']) ? 1 : 0,
    //             "planintervensirisikojatuhchcklainnya"  => isset($post['planintervensirisikojatuhchcklainnya']) ? 1 : 0,
    //             "planintervensirisikojatuhtidakadaintervensi" => isset($post['planintervensirisikojatuhtidakadaintervensi']) ? 1 : 0,
    //             "planintervensirisikojatuhlainnya" => empty($post['txtintervensirisikojatuhtidakadaintervensi']) ? NULL : $post['txtintervensirisikojatuhtidakadaintervensi'],



    //             "planintervensirisikonutrisionalkonsultasikanpadaahligizi" => isset($post['planintervensirisikonutrisionalkonsultasikanpadaahligizi']) ? 1 : 0,
    //             "planintervensirisikonutrisionalchcklainnya" => isset($post['planintervensirisikonutrisionalchcklainnya']) ? 1 : 0,
    //             "planintervensirisikonutrisionaltidakadaintervensi" => isset($post['planintervensirisikonutrisionaltidakadaintervensi']) ? 1 : 0,
    //             "planintervensirisikonutrisionallainnya" => empty($post['txtintervensirisikonutrisionallainnya']) ? NULL : $post['txtintervensirisikonutrisionallainnya'],



    //             "intervensistatusfungsionallibatkanpartisipasikeluargadalam" => isset($post['intervensistatusfungsionallibatkanpartisipasikeluargadalam']) ? 1 : 0,
    //             "intervensistatusfungsionalchcklainnya" => isset($post['intervensistatusfungsionalchcklainnya']) ? 1 : 0,
    //             "intervensistatusfungsionaltidakadaintervensi" => isset($post['intervensistatusfungsionaltidakadaintervensi']) ? 1 : 0,
    //             "planintervesistatusfungsionallainnya" => empty($post['txtintervensistatusfungsionallainnya']) ? NULL : $post['txtintervensistatusfungsionallainnya'],


    //             "intervensipsikologimenenangkanpasien" => isset($post['intervensipsikologimenenangkanpasien']) ? 1 : 0,
    //             "intervensipsikologimengajarkanteknikrelaksasi"  => isset($post['intervensipsikologimengajarkanteknikrelaksasi']) ? 1 : 0,
    //             "intervensipsikologiberkoloborasidengandokter" => isset($post['intervensipsikologiberkoloborasidengandokter']) ? 1 : 0,
    //             "intervensipsikologichcklainnya"  => isset($post['intervensipsikologichcklainnya']) ? 1 : 0,
    //             "intervensipsikologitidakadaintervensi" => isset($post['intervensipsikologitidakadaintervensi']) ? 1 : 0,
    //             "planintevensipsikologilainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'],


    //             "planintervensisosialmenjelaskankepadapasiendankeluarga"  => isset($post['planintervensisosialmenjelaskankepadapasiendankeluarga']) ? 1 : 0,
    //             "planintervensisosialmemfasilitasibiladahambatan"  => isset($post['planintervensisosialmemfasilitasibiladahambatan']) ? 1 : 0,
    //             "planintervensisosialmemberikanalternatiftindakanlain"  => isset($post['planintervensisosialmemberikanalternatiftindakanlain']) ? 1 : 0,
    //             "planintervensisosialmenjelaskanterkaittindakan"  => isset($post['planintervensisosialmenjelaskanterkaittindakan']) ? 1 : 0,
    //             "planintervensisosialchcklainnya" => isset($post['planintervensisosialchcklainnya']) ? 1 : 0,
    //             "planintervensisosialtidakadaintervensi"  => isset($post['planintervensisosialtidakadaintervensi']) ? 1 : 0,
    //             "planintervensisosiallainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'], 


    //             "planintervensialergimemberitahukankepadadokter" => isset($post['planintervensialergimemberitahukankepadadokter']) ? 1 : 0,
    //             "planintervensialergiedukasikepadapasiendankeluarga"  => isset($post['planintervensialergiedukasikepadapasiendankeluarga']) ? 1 : 0,
    //             "planintervensialergichcklainnya"  => isset($post['planintervensialergichcklainnya']) ? 1 : 0,
    //             "planintervensialergitidakadaintervensi" => isset($post['planintervensialergitidakadaintervensi']) ? 1 : 0,
    //             "planintervensialergilainnya" => empty($post['txtintervensialergilainnya']) ? NULL : $post['txtintervensialergilainnya'], 


    //             "stabilisasijalannafas" => empty($post['chckintervensikeperawatanlainnyastabilisasijalannafas']) ? 0 : 1, 
    //             "keluarkanbendaasing" => empty($post['chckintervensikeperawatanlainnyakeluarkanbendaasing']) ? 0 : 1, 
    //             "pasangfripharingeal" => empty($post['chckintervensikeperawatanlainnyapasangiripharingeal']) ? 0 : 1, 
    //             "berikanbantuannafas" => empty($post['chckintervensikeperawatanlainnyaberikanbantuannafas']) ? 0 : 1, 
    //             "berikanoksigenmelaluinasal" => empty($post['chckintervensikeperawatanlainnyaberikan02']) ? 0 : 1, 
    //             "delegatifnebulizer" => empty($post['chckintervensikeperawatanlainnyadelegatifnebulizer']) ? 0 : 1, 
    //             "monitortandavitalsecararperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortandatandavital']) ? 0 : 1, 
    //             "monitortingkatkesadaransecaraperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortingkatkesadaran']) ? 0 : 1, 
    //             "monitorekg" => empty($post['chckintervensikeperawatanlainnyamontorekg']) ? 0 : 1, 
    //             "berikanposisisemifownler" => empty($post['chckintervensikeperawatanlainnyaberikanposisisemifowler']) ? 0 : 1, 
    //             "pasangdowercatheter" => empty($post['chckintervensikeperawatanlainnyapasangdowercatheter']) ? 0 : 1, 
    //             "monitorintakedanoutputcairan" => empty($post['chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan']) ? 0 : 1, 
    //             "pasanginfusintravena" => empty($post['chckintervensikeperawatanlainnyapasanginfus']) ? 0 : 1, 
    //             "lakukanperawatanluka" => empty($post['chckintervensikeperawatanlainnyalakukanperawatanluka']) ? 0 : 1, 
    //             "koloborasidengandokteruntukiv" => empty($post['chckintervensikeperawatanlainnyakoloborasidengandokter']) ? 0 : 1, 
    //             "kajiturgorkulit" => empty($post['chckintervensikeperawatanlainnyakajirturgorkulit']) ? 0 : 1, 
    //             "pasangpengamanspalk" => empty($post['chckintervensikeperawatanlainnyapasangpengaman']) ? 0 : 1, 
    //             "mengobservasitandaadanyakompartemen" => empty($post['chckintervensikeperawatanlainnyamengobservasitandatanda']) ? 0 : 1, 
    //             "pasangngt" => empty($post['chckintervensikeperawatanlainnyaoasangngt']) ? 0 : 1, 
    //             "intervensikeperawatanlainnya1"  => empty($post['txtintervensikeperawatanlainnya1']) ? NULL : $post['txtintervensikeperawatanlainnya1'], 
    //             "intervensikeperawatanlainnya2" => empty($post['txtintervensikeperawatanlainnya2']) ? NULL : $post['txtintervensikeperawatanlainnya2'],
    //             "intervensikeperawatanlainnya3" => empty($post['txtintervensikeperawatanlainnya3']) ? NULL : $post['txtintervensikeperawatanlainnya3'],
    //             "intervensikeperawatanlainnya4" => empty($post['txtintervensikeperawatanlainnya4']) ? NULL : $post['txtintervensikeperawatanlainnya4'],
    //             "evaluasikeperawatan" => empty($post['chckaevaluasikeperawatantidak']) ? 1 : 0, 
    //             "evaluasikeperawatansebutkan" => empty($post['txtevaluasikeperawatan']) ? NULL : $post['txtevaluasikeperawatan'], 
    //             "evaluasipasiendankeluargamenerimaedukasi" => empty($post['chckapakahpasienbersediamenerimaedukasitidak']) ? 1 : 0,
    //             "evaluasipasiendankeluargahambatan" => empty($post['chckapakahterdapathambatandalamedukasitidak']) ? 1 : 0,
    //             " evaluasipasienhambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1, 
    //             "evaluasipasienhambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1, 
    //             "evaluasipasienhambatankognitif" => empty($post['chckkognitif']) ? 0 : 1, 
    //             "evaluasipasienhambatanfisik" => empty($post['chckfisik']) ? 0 : 1, 
    //             "evaluasipasienhambatanemosi" => empty($post['chckemosi']) ? 0 : 1, 
    //             "evaluasipasienhambatanbahasa" => empty($post['chckbahasa']) ? 0 : 1, 
    //             "evaluasipasienhambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1, 
    //             "evaluasipasienhambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1, 
    //             "hambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1,
    //             "hambatanemosi" => empty($post['chckemosi']) ? 0 : 1,
    //             "hambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1,
    //             "hambatanbahasa"  => empty($post['chckbahasa']) ? 0 : 1,
    //             "hambatankognitif" => empty($post['chckkognitif']) ? 0 : 1,
    //             "hambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1,
    //             "hambatanfisik"  => empty($post['chckfisik']) ? 0 : 1,
    //             "hambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1,
    //             "edukasidiagnosapenyakit"  => empty($post['chckdiagnosapenyakit']) ? 0 : 1,
    //             "edukasirehabilitasimedik" => empty($post['chckrehabilitasimedik']) ? 0 : 1,
    //             "edukasihakdankewajibanpasien" => empty($post['chckhakdankewajibanpasien']) ? 0 : 1,
    //             "edukasitandabahayadanperawatanbahayibarulahir" => empty($post['chcktandabahayadanperawatanbayi']) ? 0 : 1,
    //             "edukasiobatobatan"  => empty($post['chckobatobatan']) ? 0 : 1,
    //             "edukasimanajemennyeri" => empty($post['chckmanajemennyeri']) ? 0 : 1,
    //             "edukasikb" => empty($post['chckkb']) ? 0 : 1,
    //             "edukasidietdannutrisi" => empty($post['chckdietdannutrisi']) ? 0 : 1,
    //             "edukasipenggunaanalatmedia" => empty($post['chckpenggunaanalatmedia']) ? 0 : 1,
    //             "edukasitandabahayamasanifas" => empty($post['chcktandabahayamasanifas']) ? 0 : 1,
    //             "tanggalpasienpulang" => empty($post['txttanggalpasienpulang']) ? NULL : $post['txttanggalpasienpulang'],
    //             "jampasienpulang" => empty($post['txtjampasienpulang']) ? NULL : $post['txtjampasienpulang'].':00',
    //             "perawatpelaksana" => empty($post['txtperawatpelaksana']) ? NULL : $post['txtperawatpelaksana'],
    //             "evaluasipasiendankeluargajikayahamabatan"  => empty($post['evaluasipasiendankeluargajikayahamabatan']) ? NULL : $post['evaluasipasiendankeluargajikayahamabatan']
    //         ];

    //         $this->db->where('idpendaftaran',$post['idpendaftaran'])->delete('rs_ralan_asesmen_awal_keperawatan');
    //         $this->db->replace('rs_ralan_asesmen_awal_keperawatan',$datakeperawatan);

    //         # Pengkajian Awal Medis

    //         $queridpegawai = $this->db->query("SELECT person_pegawai.idpegawai FROM `person_pegawai` WHERE person_pegawai.idperson =".$_POST['idpersondokter'])->result_array();
    //         $idpegawai = $queridpegawai[0]['idpegawai'];

    //         $dataawalmedis = [ 
    //             "idpendaftaran" => $post['idpendaftaran'], 
    //             "idpemeriksaan" => $post['idperiksa'], 
    //             "carapasiendatang" => isset($post['chckcarapasiendatang']) ? $post['chckcarapasiendatang'] : null,
    //             "diantarnama" => !isset($post['chckcarapasiendatangdiantarnama']) ? null : $post['chckcarapasiendatangdiantarnama'],
    //             "diantaralamat" => !isset($post['chckcarapasiendatangdiantaralamat']) ? null : $post['chckcarapasiendatangdiantaralamat'],
    //             "diantarnotelp" => !isset($post['chckcarapasiendatangdiantarnotelp']) ? null : $post['chckcarapasiendatangdiantarnotelp'],
    //             "rujukandari"  => !isset($post['chckcarapasiendatangrujukandari']) ? null : $post['chckcarapasiendatangrujukandari'],
    //             "rujukanalaasan" => !isset($post['chckcarapasiendatangrujukanalasan']) ? null : $post['chckcarapasiendatangrujukanalasan'],
    //             "kategorikasus" => !isset($post['chckkategorikasus']) ? null : $post['chckkategorikasus'],
    //             "triasemetodeesi" => !isset($post['chhcktme']) ? null : $post['chhcktme'],
    //             "riwayatpenyakitsekarang" => !isset($post['riwayatpenyakitsekarang']) ? null : $post['riwayatpenyakitsekarang'] , 
    //             "riwayatpenyakitdahulu" => empty($post['chcktidakriwayapenyakitdahulu']) ? 1 : 0 , 
    //             "txtriwayatpenyakitdahulu" => !isset($post['txtriwayatpenyakitdahulu']) ? null : $post['txtriwayatpenyakitdahulu'], 
    //             "obatyangrutindikonsumsi" => empty($post['chcktidakobyrd'])  ? 1 : 0, 
    //             "txtobatyangrutindikonsumsi" => !isset($post['txtobyrd']) ? null : $post['txtobyrd'], 
    //             "riwayatalergiobatdanmakanan" => empty($post['chcktidakraodm'])  ? 1 : 0,
    //             "txtriwayatalergiobatdanmakanan" => !isset($post['txtraodm']) ? null : $post['txtraodm'], 
    //             "keadaanumum" => $post['keadaanumum'], 
    //             "gcse" => !isset($post['gcse']) ? null : $post['gcse'], 
    //             "gcsv" => !isset($post['gcsv']) ? null : $post['gcsv'], 
    //             "gcsm" => !isset($post['gcsm']) ? null : $post['gcsm'], 
    //             "skalanyeri" => !isset($post['skalanyeri']) ? null : $post['skalanyeri'], 
    //             "txtpemeriksaanfisik" => !isset($post['pemeriksaanfisik']) ? null : $post['pemeriksaanfisik'], 
    //             "diagnosadilauricd10" => !isset($post['diagnosa']) ? null : $post['diagnosa'], 
    //             "pemeriksaanpenunjangmedik" => empty($post['chcktidakppm']) ? 1 : 0, 
    //             "txtpemeriksaanpenunjangmedik" => !isset($post['txtppm']) ? null : $post['txtppm'], 
    //             "terapi" => !isset($post['txtterapi']) ? null : $post['txtterapi'], 
    //             "catatanalainnya" => empty($post['chcktdkcatatanlainnya']) ? 1 : 0,
    //             "txtcatatanlainnya" => !isset($post['txtcatatanlainnya']) ? null : $post['txtcatatanlainnya'], 
    //             "observasipasiendiugd" => !isset($post['chckobservasipasiendiugd']) ? null : $post['chckobservasipasiendiugd'], 
    //             "observasipasiendiugdcatatan" => !isset($post['txtobservasipasiendiugd']) ? null : $post['txtobservasipasiendiugd'], 
    //             "rencanaperawatandantindakanaselanjutnya" => !isset($post['chckrencanaperawatandantindakanlainnya']) ? null : $post['chckrencanaperawatandantindakanlainnya'], 
    //             "rawatjalankontroldi" => !isset($post['chckrawatjalanitem']) ? null : $post['chckrawatjalanitem'],  
    //             "kontroldiitem" => !isset($post['chckkontroldi']) ? null : $post['chckkontroldi'],  
    //             "tglugd" => !isset($post['tglugd']) ? null : $post['tglugd'],  
    //             "namapoliklinik" => !isset($post['txtkontroldi']) ? null : $post['txtkontroldi'],  
    //             // "tglpoliklinik" => !isset($post['txttanggalpoliklinik']) ? null : $post['txttanggalpoliklinik'], 
    //             "bangsalrawatinap" => !isset($post['txtrawatinapbangsal']) ? null : $post['txtrawatinapbangsal'], 
    //             "kondisipasiensebelummeninggalkanUGD" => !isset($post['chckkpsmu']) ? null : $post['chckkpsmu'], 
    //             "meninggalduniatanggal" => !isset($post['txttanggalmeninggaldunia']) ? null : $post['txttanggalmeninggaldunia'], 
    //             "meninggalduniajam" => !isset($post['txtjammeninggaldunia']) ? null : $post['txtjammeninggaldunia'], 
    //             "tglselesaiasawal" => $post['tglselesaiasesmen'], 
    //             "jamselesaiasawal" => $post['txtselesaiasesmenawal'].':00', 
    //             "idpegawaidokter" =>  $idpegawai,  
    //             "isadaracikan" => empty($post['adaracikan']) ? 1 : 0, 
    //             "keteranganobat" => !isset($post['keteranganobat']) ? null : $post['keteranganobat'],
    //             "tglpasienpulangugd" => !isset($post['tglpasienpulangugd']) ? null : $post['tglpasienpulangugd'],
    //             "jampasienpulangugd"=> !isset($post['jampasienpulangugd']) ? null : $post['jampasienpulangugd'].':00',
    //             "iddokterugd" => !isset($post['namadokterugd']) ? null : $post['namadokterugd'],
    //             "advisedpjp" => empty($post['advisedpjp']) ? null : $post['advisedpjp'],
    //             "iddoktermenerimaadvise"  => empty($post['dokteryangmenerimaadvise']) ? null : $post['dokteryangmenerimaadvise'],
    //             "iddokterdpjp" => empty($post['verifikasidpjpdokterdpjd']) ? null : $post['verifikasidpjpdokterdpjd']
    //         ];

    //         $save = $this->db->replace('rs_pengkajian_awal_medis',$dataawalmedis);


    //         $idperiksa = $this->input->post('idperiksa');
    //         $idpendaftaran = $this->input->post('idpendaftaran');
    //         $adaracikan = $this->input->post('adaracikan');
    //         $norm = $this->input->post('norm');
    //         $prolanis = $this->input->post('prolanis');
    //         $kunjunganberikutnya = $this->input->post('kunjunganberikutnya');
    //         $idunit = $this->input->post('idunit');
    //         $kondisikeluar = $this->input->post('kondisikeluar');
    //         $statuspasienpulang= $this->input->post('statuspasienpulang');
    //         $keteranganobat = qlreplace_quote($this->input->post('keteranganobat'));
    //         $keteranganradiologi = qlreplace_quote($this->input->post('keteranganradiologi'));
    //         $saranradiologi = qlreplace_quote($this->input->post('saranradiologi'));
    //         $keteranganlaboratorium = qlreplace_quote($this->input->post('keteranganlaboratorium'));
    //         $kategoriskrining = $this->input->post('kategoriskrining');

    //         $jeniswabah = $this->input->post('jeniswabah');
    //         $cindikasipasien = $this->input->post('cindikasipasien');
    //         $penanganan = $this->input->post('penanganan');
    //         $sebabpenularan = $this->input->post('sebabpenularan');
    //         $grupanamnesa = $this->input->post('grupanamnesa');

    //         $modesave = $this->input->post('modesave');
    //         $isselesai=0;
    //         $isselesai = (($modesave!='verifikasi')? (($this->isselesaipemeriksaan($idpendaftaran,$modesave)) ? 0 : 1 ) : 1 );


    //         if( $modesave=='verifikasi'){
    //             $dt_person_pendaftaran = [
    //             'keteranganobat_verifklaim'=>$keteranganobat,
    //             'keteranganradiologi_verifklaim'=>$keteranganradiologi,
    //             'saranradiologi_verifklaim'=>$saranradiologi,
    //             'keteranganlaboratorium_verifklaim'=>$keteranganlaboratorium, 
    //             'isverifklaim'=>1
    //             ];
    //         }else{
    //             $dt_person_pendaftaran = [
    //             'keteranganlaboratorium'=> $keteranganlaboratorium,
    //             'idstatuskeluar'        => (($isselesai) ? 2 : (($this->input->post('status')==2)? '2' :'1') ),
    //             'adaracikan'            => ((isset($adaracikan))?'1':null),
    //             'kondisikeluar'         => $kondisikeluar,
    //             'statuspulang'          => $statuspasienpulang
    //             ];
    //             //kategoriskrining
    //             if(isset($kategoriskrining))
    //             {
    //                 $dt_person_pendaftaran['kategoriskrining'] = $kategoriskrining;
    //             }

    //             //input hasil expertise
    //             if($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI))
    //             {
    //                 $dt_person_pendaftaran['saranradiologi'] = $saranradiologi;
    //                 $dt_person_pendaftaran['keteranganradiologi'] = $keteranganradiologi;
    //             }

    //             //input hasil ECHOCARDIOGRAPHY
    //             if($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI))
    //             {
    //                 $keteranganekokardiografi = $this->input->post('keteranganekokardiografi');
    //                 $dt_person_pendaftaran['keteranganekokardiografi'] = $keteranganekokardiografi;
    //             }
    //             //input keterangan obat
    //             if($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN))
    //             {
    //                 $dt_person_pendaftaran['keteranganobat'] = $keteranganobat;
    //             }

    //             //inputprolanis
    //             if($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS))
    //             {
    //                 $setProlanis = ((isset($prolanis))?'1': '0');
    //                 $dt_person_pendaftaran['pasienprolanis'] = $setProlanis;
    //                 $dt_person_pendaftaran['tanggal_kunjunganberikutnya'] = $kunjunganberikutnya;                    
    //             }

    //         }
    //         $this->db->update('person_pendaftaran',$dt_person_pendaftaran,['idpendaftaran'=>$idpendaftaran]);
    //         // simpan atau ubah anamnesa ::: grup atau tidak
    //         if($this->pageaccessrightbap->checkAccessRight(V_INPUTDATASUBYEKTIFRALAN))
    //         {
    //             $this->pemeriksaanklinik_grupanamnesa($grupanamnesa,$modesave);
    //         }

    //         if(isset($cindikasipasien) && $modesave!='verifikasi')
    //         {
    //             $dataIPW=['idjeniswabah'=>(isset($jeniswabah)) ? $jeniswabah : '','idpenangananwabah'=>(isset($penanganan)) ? $penanganan : '','idsebabpenularan'=>(isset($sebabpenularan)) ? $sebabpenularan : '','idpemeriksaan'=>$idperiksa,'serviceintegrasi'=>(($idperiksa!='')?'2':'1')];
    //             $this->ql->rs_pemeriksaanwabah_insertupdate($idperiksa,$dataIPW);
    //         }
    //         // $arrMsg = $this->db->update('rs_pemeriksaan',)

    //         $arrMsg = $this->pemeriksaanklinik_save_pemeriksaan($idperiksa,$modesave,$isselesai);
    //         if($modesave!='verifikasi'){$this->mkombin->hitungtotaldiskon($idpendaftaran);}
    //         pesan_success_danger($arrMsg, 'Save Pemeriksaan Success.!', $arrMsg.'Save Pemeriksaan Failed.!'); 
    //         redirect(base_url('cpelayanan/pemeriksaanklinik'));
    //     }
    // }    

    private function setwaktulayananpemeriksaan($norm, $idpendaftaran, $tanggalperiksa)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANLABORAT)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaanlaboratorium', $tanggalperiksa, 'laboratorium', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANRADIOLOGI)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaanradiologi', $tanggalperiksa, 'radiologi', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANFARMASI)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaanfarmasi', $tanggalperiksa, 'farmasi', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANPOLI)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaanpoli', $tanggalperiksa, 'poli', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANDOKTER)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaandokter', $tanggalperiksa, 'poli', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }

        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANUGD)) {
            $this->insert_waktulayanan($norm, $idpendaftaran, 'pemeriksaanugd', $tanggalperiksa, 'ugd', $this->input->post('waktumulai'), $this->input->post('waktuselesai'));
        }
    }


    private function pemeriksaanklinik_grupanamnesa($grub, $modesave)
    {
        foreach ($grub as $value) {
            $update = (($modesave == 'verifikasi') ? $this->db->update('rs_pemeriksaan', ['isverifklaim' => 1, 'anamnesa_verifklaim' => qlreplace_quote($this->input->post('anamnesa' . $value))], ['idpemeriksaan' => $value]) : $this->db->update('rs_pemeriksaan', ['anamnesa' => qlreplace_quote($this->input->post('anamnesa' . $value))], ['idpemeriksaan' => $value]));
            $datal = [
                'url'     => get_url(),
                'log'     => 'user:' . $this->session->userdata('username') . ';status:sedang ditangani;idpemeriksaan:' . $value . ';anamnesa-' . qlreplace_quote($this->input->post('anamnesa' . $value)),
                'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];
            $this->mgenerikbap->setTable('login_log');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);

            if (!$update) {
                pesan_success_danger(0, "", "Ubah anamnesa tidak berhasil..!", "js");
            }
        }
    }
    private function pemeriksaanklinik_updateantrian()
    {
        $this->db->where('idperson', $this->input->post('idperson'));
        $this->db->order_by('idantrian', 'desc');
        $this->db->limit('1');
        $this->db->update('antrian_antrian', ['status' => 'ok']);
    }
    private function pemeriksaanklinik_save_pemeriksaan($id, $modesave, $isselesai)
    {
        $post = $this->input->post();
        $log_hasilexpertise = '';
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) {
            $log_hasilexpertise = '-keteranganradiologi' . qlreplace_quote($post['keteranganradiologi']) . '-saranradiologi' . qlreplace_quote($post['saranradiologi']);
        }
        $log_keteranganobat = '';
        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN)) {
            $log_keteranganobat = '-keteranganobat' . qlreplace_quote($post['keteranganobat']);
        }
        $datan = $this->db->query("select rp.idpendaftaran, rp.norm,rp.waktu, (select jenispemeriksaan from person_pendaftaran pp where pp.idpendaftaran=rp.idpendaftaran) jenispemeriksaan  from rs_pemeriksaan rp  where rp.idpemeriksaan='$id'")->row_array();
        $this->setwaktulayananpemeriksaan($datan['norm'], $datan['idpendaftaran'], $datan['waktu']);
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:' . $this->session->userdata('username') . ';status:sedang ditangani;idpemeriksaan:' . $id . ';idpendaftaran:' . $datan['idpendaftaran'] . ';norm:' . $datan['norm'] . '-keterangan:' . qlreplace_quote($post['keterangan']) . '-diagnosa' . qlreplace_quote($post['diagnosa']) . $log_keteranganobat . $log_hasilexpertise . '-keteranganlaboratorium' . qlreplace_quote($post['keteranganlaboratorium']),
            'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);

        if ($this->pageaccessrightbap->checkAccessRight(V_INPUTALERGIRALAN)) {
            $this->db->update('person_pasien', ['alergi' => $this->input->post('alergiobat')], ['norm' => $datan['norm']]);
        }
        $data = (($modesave == 'verifikasi') ?
            [
                'status' => (($isselesai) ? 'selesai' : 'sedang ditangani'),
                'diagnosa_verifklaim' => qlreplace_quote($this->input->post('diagnosa')),
                'isverifklaim' => 1,
                'keterangan_verifklaim' => qlreplace_quote($this->input->post('keterangan')),
                'rekomendasi_verifklaim' => qlreplace_quote($this->input->post('rekomendasi'))
            ]
            :

            [
                'status' => (($isselesai) ? 'selesai' : 'sedang ditangani'),
                'diagnosa' => qlreplace_quote($this->input->post('diagnosa'))
            ]);

        if ($modesave != 'verifikasi') {
            if ($this->pageaccessrightbap->checkAccessRight(V_INPUTDATASUBYEKTIFRALAN)) {
                $data['keterangan'] = qlreplace_quote($this->input->post('keterangan'));
            }

            if ($this->pageaccessrightbap->checkAccessRight(V_INPUTRENCANACATATANRALAN)) {
                $data['rekomendasi'] = qlreplace_quote($this->input->post('rekomendasi'));
            }
        }


        return $this->db->update('rs_pemeriksaan', $data, ['idpemeriksaan' => $id]);
    }


    ////////////////////DIAGNOSA////////////////////
    public function pemeriksaan_tambahdiagnosa() //tambah diagnosa
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $ispaket = ((isset($post['ispaket'])) ? $post['ispaket'] : '');
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $level = $post['l'];
        $icdparent = '';
        if ($level == 'primer') {
            $this->db->update('rs_hasilpemeriksaan', ['icdparent' => $icd], ['idpemeriksaan' => $idpemeriksaan, 'icdlevel' => 'sekunder']);
        } else {
            $icdparent = $this->mkombin->cekdiagnosaprimer($idpemeriksaan)->row_array()['icd'];
        }
        $data = ['idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'icd' => $icd, 'icdlevel' => $level,  'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
        if ($level != 'primer' && $icdparent != '') {
            $data['icdparent'] = $icdparent;
        }

        $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $icd);
        if ($cekData->num_rows() > 0) {
            pesan_success_danger(0, "danger", "Diagnosa Sudah Ada..!", "js");
        } else {
            $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
            $this->log_hasilpemeriksaan($idpendaftaran, 'insert-diagnosa- level-' . $level, $icd);
            pesan_success_danger($arrMsg, "Add Diagnosa Success..!", "Add Diagnosa Failed..!", "js");
        }
    }
    //list diskon
    public function pemeriksaan_listdiskon()
    {
        $post = $this->input->post();
        $x = $post['x'];
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $wheremodelist = (($modelist == 'verifikasi') ? 'and rh.isverifklaim=1' : 'and rh.istagihan=1');
        echo json_encode($this->db->query("SELECT rh.idhasilpemeriksaan, rh.icd,rh.icdlevel, ri.namaicd, rm.nominal,rm.jenisdiskon, rh.potongantagihan as total 
        FROM rs_hasilpemeriksaan rh, rs_icd ri , rs_masterdiskon rm
        WHERE rh.icd = ri.icd and rh.idpendaftaran='" . $x . "' and ri.idjenisicd='8' " . $wheremodelist . " and rm.icd=ri.icd order by rh.icd")->result());
    }
    //tambah diskon manual
    public function pemeriksaan_tambahdiskon()
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $idunit = $post['u'];

        if ($this->isselesaipemeriksaan($idpendaftaran, $modesave)) {
            $diskon = $this->db->select("idjenistarif, nominal")->get_where("rs_masterdiskon", ['icd' => $icd, 'idkelas' => 1, 'jenisinput' => 1])->row_array();
            if (empty($diskon)) {
                pesan_success_danger(1, "Diskon " . $icd . ". Tidak Tersedia.!", "Diskon " . $icd . ". Tidak Tersedia.!,", "js");
                return;
            } else //selain itu
            {
                $data = [
                    'idpemeriksaan' => $idpemeriksaan,
                    'idpendaftaran' => $idpendaftaran,
                    'icd' => $icd,
                    'idjenistarif' => $diskon['idjenistarif'],
                    'potongantagihan' => $diskon['nominal'],
                    'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                ];
                $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                $this->log_hasilpemeriksaan($idpendaftaran, 'insert', $icd);
                if ($modesave != 'verifikasi') {
                    $arrMsg = $this->mkombin->hitungtotaldiskon($idpendaftaran);
                    $this->load->model('mtriggermanual');
                    $this->mtriggermanual->airs_hasilpemeriksaan($idpendaftaran, $idpemeriksaan, $this->db->insert_id(), 'Tindakan', $icd);
                }
                pesan_success_danger($arrMsg, "Add Diskon Success..!", "Add Diskon Failed..!", "js");
            }
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }

    //list diagnosa
    public function pemeriksaan_listdiagnosa()
    {
        $post = $this->input->post();
        $x = $post['x'];
        $modelist      = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $wheremodelist = (($modelist == 'verifikasi') ? 'and rh.isverifklaim=1' : 'and rh.istagihan=1');
        $sql = $this->db->query("select rh.idhasilpemeriksaan, rh.icd,rh.icdlevel, ri.namaicd, ri.aliasicd 
            from rs_hasilpemeriksaan rh, rs_icd ri 
            where rh.icd = ri.icd and rh.idpendaftaran='$x' and ri.idjenisicd='2' and ri.idicd='" . $post['idicd'] . "' " . $wheremodelist . "
            order by rh.icdlevel, rh.icd")->result();

        echo json_encode($sql);
    }
    //hapus diagnosa
    public function pemeriksaan_hapusdiagnosa()
    {
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $this->log_hasilpemeriksaan($post['idp'], 'hapus/ubah-diagnosa- isverifikasi:' . $modesave, $post['icd']);
        $arrMsg = (($modesave == 'verifikasi') ? $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ['idhasilpemeriksaan' => $post['x']]) : $this->db->delete("rs_hasilpemeriksaan", ['idhasilpemeriksaan' => $post['x']]));

        pesan_success_danger($arrMsg, "Delete Diagnosa Success..!", "Delete Diagnosa Failed..!", "js");
    }
    public function pemeriksaan_cekdiagnosaprimer()
    {

        $hasil = $this->mkombin->cekdiagnosaprimer($this->input->post('x'));
        echo json_encode(['status' => $hasil->num_rows()]);
    }
    ///////////////////////END  DIAGNOSA//////////////////
    // ubah penerima jm ralan
    public function hasilpemeriksaan_update_penerimajm()
    {
        $arrMsg = $this->db->update('rs_hasilpemeriksaan', ['iddokterjasamedis' => $this->input->post('iddokterjasamedis')], ['idhasilpemeriksaan' => $this->input->post('idhasilpemeriksaan')]);
        pesan_success_danger($arrMsg, "Dokter Penerima JM berhasil diubah.", "Dokter Penerima JM gagal diubah.", "js");
    }
    ////////////////////TINDAKAN////////////////////
    public function pemeriksaan_tambahtindakan()
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $idunit = $post['u'];
        $this->cekmastertarif('Tindakan', $idpendaftaran, $icd, $idpemeriksaan, $modesave, $idunit);
    }
    public function pemeriksaan_listtindakan()
    {
        $post = $this->input->post();
        $x = $post['x'];
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $wheremodelist = (($modelist == 'verifikasi') ? 'and rh.isverifklaim=1' : 'and rh.istagihan=1');
        echo json_encode(['tindakan' => $this->db->query("select rh.iddokterjasamedis, namadokter(rh.iddokterjasamedis) as dokterpenerimajm, rh.idhasilpemeriksaan, rh.icd, ri.namaicd, ri.aliasicd, rh.total, rh.jumlah, rh.jaminanasuransi, rh.potongantagihan  from rs_hasilpemeriksaan rh, rs_icd ri
where rh.icd = ri.icd and rh.idpendaftaran='$x' and ri.idjenisicd='3'" . $wheremodelist)->result(), 'total' => $this->db->query("select (select COALESCE(SUM(total), 0) as total from rs_hasilpemeriksaan rh where rh.idpendaftaran='$x') as totalseluruh1, 
(select COALESCE(SUM(total), 0) as total from rs_barangpemeriksaan rh where rh.idpendaftaran='$x' " . $wheremodelist . " ) as totalseluruh2")->result()]);
    }
    public function pemeriksaan_hapustindakan()
    {
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $dtpasien = $this->db->select('idpendaftaran, icd')->get_where('rs_hasilpemeriksaan', ['idhasilpemeriksaan' => $post['x']])->row_array();
        $arrMsg = (($modesave == 'verifikasi') ? $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ['idhasilpemeriksaan' => $post['x']]) : '');
        if ($modesave != 'verifikasi') {
            if ($this->isselesaipemeriksaan($dtpasien['idpendaftaran'], $modesave)) {
                $arrMsg = $this->db->delete("rs_hasilpemeriksaan", ['idhasilpemeriksaan' => $post['x']]);
                if ($arrMsg) {
                    $this->log_hasilpemeriksaan($dtpasien['idpendaftaran'], 'hapus', $dtpasien['icd']);
                }
                $this->mkombin->hitungtotaldiskon($dtpasien['idpendaftaran']);
                pesan_success_danger($arrMsg, "Delete Tindakan Success..!", "Delete Tindakan Failed..!", "js");
            } else {
                echo json_encode(['status' => 'selesaiperiksa']);
            }
        } else {
            pesan_success_danger($arrMsg, "Delete Tindakan Success..!", "Delete Tindakan Failed..!", "js");
        }
    }
    //--set jumlah
    public function pemeriksaan_ubahjumlahtindakan()
    {
        $id = $this->input->post("id");
        $jumlah = $this->input->post("jumlah");
        $idp = $this->db->select('idpendaftaran')->get_where('rs_hasilpemeriksaan', ['idhasilpemeriksaan' => $id])->row_array()['idpendaftaran'];
        $modesave = '';
        if ($this->isselesaipemeriksaan($idp, $modesave)) {
            $arrMsg = $this->db->update("rs_hasilpemeriksaan", ['jumlah' => $jumlah], ['idhasilpemeriksaan' => $id]);

            //        hitung total diskon dalam persen
            $this->mkombin->hitungtotaldiskon($idp);
            pesan_success_danger($arrMsg, "Ubah Jumlah Tindakan Success..!", "Ubah Jumlah Tindakan Failed..!", "js");
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }

    ///////////////////////END  TINDAKAN//////////////////
    ////////////////////RADIOLOGI////////////////////
    public function pemeriksaan_tambahradiologi()
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $this->cekmastertarif('Radiologi', $idpendaftaran, $icd, $idpemeriksaan, $modesave, '');
    }
    public function pemeriksaan_listradiologi()
    {
        $post = $this->input->post();
        $x = $post['x'];
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $wheremodelist = (($modelist == 'verifikasi') ? 'and rh.isverifklaim=1' : 'and rh.istagihan=1');
        echo json_encode($this->db->query("select rh.idhasilpemeriksaan,rh.iddokterjasamedis, namadokter(rh.iddokterjasamedis) as dokterpenerimajm, rh.jumlah, rh.icd, ri.namaicd, ri.aliasicd, rh.total,rh.potongantagihan, rh.jaminanasuransi  from rs_hasilpemeriksaan rh, rs_icd ri where rh.icd = ri.icd and rh.idpendaftaran='$x' and ri.idjenisicd='5'" . $wheremodelist)->result());
    }
    public function pemeriksaan_hapusradiologi()
    {
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $dtpasien = $this->db->select('idpendaftaran, icd')->get_where('rs_hasilpemeriksaan', ['idhasilpemeriksaan' => $post['x']])->row_array();
        if ($this->isselesaipemeriksaan($dtpasien['idpendaftaran'], $modesave)) {
            $arrMsg = (($modesave == 'verifikasi') ? $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ['idhasilpemeriksaan' => $post['x']]) : $this->db->delete("rs_hasilpemeriksaan", ['idhasilpemeriksaan' => $post['x']]));
            if ($arrMsg) {
                $this->log_hasilpemeriksaan($dtpasien['idpendaftaran'], 'hapus-radiologi-', $dtpasien['icd']);
                $this->mkombin->hitungtotaldiskon($dtpasien['idpendaftaran']);
            }
            pesan_success_danger($arrMsg, "Delete Tindakan Success..!", "Delete Tindakan Failed..!", "js");
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }
    ///////////////////////END  RADIOLOGI//////////////////
    ////////////////////VITALSIGN////////////////////
    public function pemeriksaan_tambahvitalsign()
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $ispaket = $post['ispaket'];
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        if ($ispaket == 'ispaket') {
            $this->cekmasterpakettarif('Paket VitalSign', $idpendaftaran, $icd, $idpemeriksaan, $modesave);
        } else {
            $this->cekmastertarif('VitalSign', $idpendaftaran, $icd, $idpemeriksaan, $modesave, '');
        }
    }
    public function pemeriksaan_listvitalsign()
    {
        $post = $this->input->post();
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $periksa = $this->db->query("select a.idpendaftaran, b.idunit from rs_pemeriksaan a, person_pendaftaran b where a.idpemeriksaan='" . $post['y'] . "' and b.idpendaftaran = a.idpendaftaran")->row_array();
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->viewhasilpemeriksaan(1, $periksa['idunit'], $periksa['idpendaftaran'], $modelist));
    }
    public function pemeriksaan_hapusvitalsign()
    {
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        $arrMsg = (($modesave == 'verifikasi') ? $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ['idhasilpemeriksaan' => $post['x']]) : $this->db->delete("rs_hasilpemeriksaan", ['idhasilpemeriksaan' => $post['x']]));
        pesan_success_danger($arrMsg, "Delete VitalSign Success..!", "Delete VitalSign Failed..!", "js");
    }
    ///////////////////////END  VITALSIGN//////////////////
    /////////////// HAPUS PAKET ICD //////////
    public function pemeriksaan_hapuspaketpemeriksaan()
    {
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        if ($this->isselesaipemeriksaan($post['y'], $modesave)) {
            if ($modesave == 'verifikasi') {
                $result = $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ((empty($post['gpp'])) ? ['idgrup' => $post['x'], 'idpendaftaran' => $post['y']] : ['idgrup' => $post['x'], 'idpendaftaran' => $post['y'], 'gruppaketperiksa' => $post['gpp']]));
            } else {
                $result = $this->db->delete('rs_hasilpemeriksaan', ((empty($post['gpp'])) ? ['idgrup' => $post['x'], 'idpendaftaran' => $post['y']] : ['idgrup' => $post['x'], 'idpendaftaran' => $post['y'], 'gruppaketperiksa' => $post['gpp']]));
                $this->mkombin->hitungtotaldiskon($post['y']);
            }
            pesan_success_danger($result, "Hapus Paket Berhasil..!", "Hapus Paket Gagal..!", "js");
        }
    }
    ////////////////////LABORATORIUM////////////////////
    public function pemeriksaan_tambahlaboratorium()
    {
        $post = $this->input->post();
        $icd = $post['x'];
        $idpendaftaran = $post['y'];
        $idpemeriksaan = $post['z'];
        $ispaket = ((isset($post['ispaket'])) ? $post['ispaket'] : '');
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        if ($this->isselesaipemeriksaan($idpendaftaran, $modesave)) {
            if ($ispaket == 'ispaket') {
                $paketpaket = $this->db->query("select paket_paket from rs_paket_pemeriksaan where idpaketpemeriksaan ='" . $icd . "'")->row_array()['paket_paket'];
                if (empty($paketpaket)) {
                    $this->cekmasterpakettariflaborat('Laboratorium', $idpendaftaran, $icd, $idpemeriksaan, $modesave);
                } else {
                    //jika paket pemeriksaan memiliki paketpaket
                    $arrPaket = explode(',', $paketpaket);

                    $dtmstrf = $this->db->query("select * from rs_mastertarif_paket_pemeriksaan x  where x.idpaketpemeriksaan='" . $icd . "'");
                    if ($dtmstrf->num_rows() > 0) {
                        $dtmstrf = $dtmstrf->row_array();
                        $data = ['gruppaketperiksa' => 1, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $icd, 'idgrup' => $icd, 'idjenistarif' => $dtmstrf['idjenistarif'], 'coapendapatan' => $dtmstrf['coapendapatan'], 'jasaoperator' => $dtmstrf['jasaoperator'], 'nakes' => $dtmstrf['nakes'], 'jasars' => $dtmstrf['jasars'], 'bhp' => $dtmstrf['bhp'], 'akomodasi' => $dtmstrf['akomodasi'], 'margin' => $dtmstrf['margin'], 'total' => $dtmstrf['total'], 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                        $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data); //save paket
                        if ($modesave != 'verifikasi') {
                            $this->load->model('mtriggermanual');
                            $this->mtriggermanual->airs_hasilpemeriksaan($idpendaftaran, $idpemeriksaan, $this->db->insert_id(), 'Laboratorium'); //menambahkan idpegawaidokter di hasilpemeriksaan
                        }
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($icd); //add icd tindakan dalam paket
                        if (!empty($listpaket)) {
                            foreach ($listpaket as $key) {
                                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $key->icd);
                                if (empty($cekData->num_rows())) {
                                    $data = ['gruppaketperiksa' => 1, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'icd' => $key->icd, 'idpaketpemeriksaan' => $key->idpaketpemeriksaan, 'idgrup' => $icd, 'idjenistarif' => 0, 'jasaoperator' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'total' => 0, 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                                }
                            }
                        }
                        foreach ($arrPaket as $paket) {
                            $this->cekmasterpakettariflaborat('Laboratorium', $idpendaftaran, $paket, $idpemeriksaan, $modesave, $paketpaket);
                        }
                        if ($modesave != 'verifikasi') {
                            $this->mkombin->hitungtotaldiskon($idpendaftaran);
                        }
                        return pesan_success_danger(1, "Add Paket Laboratorium Success..!", "Add Paket Laboratorium Failed..!", "js");
                    } else {
                        pesan_danger("Paket Tidak Ada..!", "js");
                    }
                }
            } else {
                $this->cekmastertarif('Laboratorium', $idpendaftaran, $icd, $idpemeriksaan, $modesave, '');
            }
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }
    public function pemeriksaan_listlaboratorium()
    {
        $post = $this->input->post();
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $periksa = $this->db->query("select a.idpendaftaran, b.idunit from rs_pemeriksaan a, person_pendaftaran b where a.idpemeriksaan='" . $post['y'] . "' and b.idpendaftaran = a.idpendaftaran")->row_array();
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->viewhasilpemeriksaan(4, $periksa['idunit'], $periksa['idpendaftaran'], $modelist));
    }
    public function pemeriksaan_listekokardiografi()
    {
        $post = $this->input->post();
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $periksa = $this->db->query("select a.idpendaftaran, b.idunit from rs_pemeriksaan a, person_pendaftaran b where a.idpemeriksaan='" . $post['y'] . "' and b.idpendaftaran = a.idpendaftaran")->row_array();
        $this->load->model('mviewql');
        echo json_encode($this->mviewql->viewhasilpemeriksaan(9, $periksa['idunit'], $periksa['idpendaftaran'], $modelist));
    }
    public function pemeriksaan_hapuslaboratorium()
    {
        $x = $this->input->post("x");
        $dtpasien = $this->db->select('idpendaftaran, icd')->get_where('rs_hasilpemeriksaan', ['idhasilpemeriksaan' => $x])->row_array();
        $post = $this->input->post();
        $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '');
        if ($this->isselesaipemeriksaan($dtpasien['idpendaftaran'], $modesave)) {
            $arrMsg = (($modesave == 'verifikasi') ? $this->db->update('rs_hasilpemeriksaan', ['isverifklaim' => 0], ['idhasilpemeriksaan' => $post['x']]) : $this->db->delete("rs_hasilpemeriksaan", ['idhasilpemeriksaan' => $post['x']]));
            if ($arrMsg) {
                $this->log_hasilpemeriksaan($dtpasien['idpendaftaran'], 'hapus-laborat-', $dtpasien['icd']);
                $this->mkombin->hitungtotaldiskon($dtpasien['idpendaftaran']);
            }
            pesan_success_danger($arrMsg, "Delete Laboratorium Success..!", "Delete Laboratorium Failed..!", "js");
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }
    ///////////////////////END  LABORATORIUM//////////////////
    //////////CEK TARIF PER PAKET ICD 
    private function cekmasterpakettarif($pesan, $idpendaftaran, $idpaket, $idpemeriksaan, $modesave)
    {
        //siapkan data master tarif sesuai paket
        $dtmstrf = $this->db->query("select * from rs_mastertarif_paket_pemeriksaan x where x.idpaketpemeriksaan='" . $idpaket . "'");
        //save paket
        if ($dtmstrf->num_rows() > 0) {
            $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($idpaket); //cari data per tindakan tindakan 
            if (!empty($listpaket)) {
                foreach ($listpaket as $key) {
                    $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $key->icd);
                    if (empty($cekData->num_rows())) {
                        $data = [
                            'idpemeriksaan' => $idpemeriksaan,
                            'idpendaftaran' => $idpendaftaran,
                            'icd' => $key->icd,
                            'idpaketpemeriksaan' => $key->idpaketpemeriksaan,
                            'idgrup' => $idpaket,
                            'idjenistarif' => 0,
                            'jasaoperator' => 0,
                            'jasars' => 0,
                            'bhp' => 0,
                            'akomodasi' => 0,
                            'margin' => 0,
                            'total' => 0,
                            'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                        ];
                        $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                    } else {
                        return pesan_success_danger(0, "", $pesan . " Sudah Ada..!", "js");
                    }
                }
                // simpan paket parent
                $dtmstrf = $dtmstrf->row_array();
                $data = [
                    'idpemeriksaan' => $idpemeriksaan,
                    'idpendaftaran' => $idpendaftaran,
                    'idpaketpemeriksaan' => $idpaket,
                    'idgrup' => $idpaket,
                    'idjenistarif' => $dtmstrf['idjenistarif'],
                    'coapendapatan' => $dtmstrf['coapendapatan'],
                    'jasaoperator' => $dtmstrf['jasaoperator'],
                    'jasars' => $dtmstrf['jasars'],
                    'bhp' => $dtmstrf['bhp'],
                    'akomodasi' => $dtmstrf['akomodasi'],
                    'margin' => $dtmstrf['margin'],
                    'total' => $dtmstrf['total'],
                    'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                ];
                $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                pesan_success_danger($arrMsg, "Add " . $pesan . " Success..!", "Add " . $pesan . " Failed..!", "js");
            } else {
                $listpaketparent = $this->mkombin->diagnosa_getpaketdiagnosaparent($idpaket);
                if (!empty($listpaketparent)) {
                    foreach ($listpaketparent as $key) {
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($key->idpaketpemeriksaan);
                        if (!empty($listpaket)) {
                            foreach ($listpaket as $key) {
                                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $key->icd);
                                if (empty($cekData->num_rows())) {
                                    $data = [
                                        'idpemeriksaan' => $idpemeriksaan,
                                        'idpendaftaran' => $idpendaftaran,
                                        'icd' => $key->icd,
                                        'idpaketpemeriksaan' => $key->idpaketpemeriksaan,
                                        'idgrup' => $idpaket,
                                        'idjenistarif' => 0,
                                        'jasaoperator' => 0,
                                        'jasars' => 0,
                                        'bhp' => 0,
                                        'akomodasi' => 0,
                                        'margin' => 0,
                                        'total' => 0,
                                        'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                                    ];
                                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                                }
                            }
                        }
                    }
                    // simpan paket parent
                    $dtmstrf = $dtmstrf->row_array();
                    $data = [
                        'idpemeriksaan' => $idpemeriksaan,
                        'idpendaftaran' => $idpendaftaran,
                        'idpaketpemeriksaan' => $idpaket,
                        'idgrup' => $idpaket,
                        'idjenistarif' => $dtmstrf['idjenistarif'],
                        'coapendapatan' => $dtmstrf['coapendapatan'],
                        'jasaoperator' => $dtmstrf['jasaoperator'],
                        'jasars' => $dtmstrf['jasars'],
                        'bhp' => $dtmstrf['bhp'],
                        'akomodasi' => $dtmstrf['akomodasi'],
                        'margin' => $dtmstrf['margin'],
                        'total' => $dtmstrf['total'],
                        'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                    ];
                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                    return pesan_success_danger($arrMsg, "Add " . $pesan . " Success..!", "Add " . $pesan . " Failed..!", "js");
                } else {
                    pesan_danger($pesan . " Tidak Ada..!", "js");
                }
            }
        }
        // 'idjenistarif'=>$key->idjenistarif,'jasaoperator'=>$key->jasaoperator,'jasars'=>$key->jasars,'bhp'=>$key->bhp,'akomodasi'=>$key->akomodasi,'margin'=>$key->margin,'total'=>$key->total
        else {
            pesan_danger($pesan . " Tidak Ada", "js");
        }
    }
    // CEK GRUP PEMERIKSAAN 
    private function cekgrupperiksa($idp)
    {
        return $this->db->query("select idjadwalgrup from person_pendaftaran where idpendaftaran='$idp'")->row_array()['idjadwalgrup'];
    }
    /**
     * Cek Tarif Per ICD
     * @param type $pesan Pesan Notifikasi
     * @param type $idpendaftaran Id Pendaftaran
     * @param type $icd ICD
     * @param type $idpemeriksaan Id Pemeriksaan
     * @param type $modesave Mode Simpan
     * @param type $idunit ID Unit Periksa
     * @param type $response Jenis Response
     */
    private function cekmastertarif($pesan, $idpendaftaran, $icd, $idpemeriksaan, $modesave, $idunit, $response = '')
    {
        if ($this->isselesaipemeriksaan($idpendaftaran, $modesave)) {
            //seting coa pendapatan khusus ugd tindakan umum diambil dari konfigurasi
            $dtmstrf = $this->db->select("idjenistarif, "
                . "ifnull(ifnull(if(coapendapatan=0,(select b.kode from keu_tipeakun a, keu_akun b WHERE b.idtipeakun=a.idtipeakun and a.kode=tipecoapendapatan and b.idunit='" . $idunit . "'),coapendapatan),"
                . "if((select akronimunit from rs_unit where idunit='" . $idunit . "')='ugd', 
                                                  if(x.tipecoapendapatan='42000',getKonfigurasi('coaperiksaugd'),
                                                    if(x.tipecoapendapatan='43000',getKonfigurasi('coatindakanugd'), getKonfigurasi('coadiskonugd'))
                                                    ), 0
                                                 ) 
                                              ),0)as coapendapatan, "
                . "jasaoperator,jasars, nakes,bhp,akomodasi,margin,sewa,asuransi,total")
                ->get_where("rs_mastertarif x", ['icd' => $icd, 'idkelas' => 1])
                ->row_array();
            if (empty($dtmstrf) || !empty($this->cekgrupperiksa($idpendaftaran))) //jika tarif tidak ada atau GRUP pemeriksaan 
            {
                $idjenistarif = 0;
                $coapendapatan = 0;
                $jasaoperator = 0;
                $nakes = 0;
                $jasars = 0;
                $bhp = 0;
                $akomodasi = 0;
                $margin = 0;
                $sewa = 0;
                $asuransi = 0;
                $total = 0;
            } else //selain itu
            {
                $idjenistarif = $dtmstrf['idjenistarif'];
                $coapendapatan = $dtmstrf['coapendapatan'];
                $jasaoperator = $dtmstrf['jasaoperator'];
                $jasars = $dtmstrf['jasars'];
                $nakes = $dtmstrf['nakes'];
                $bhp = $dtmstrf['bhp'];
                $akomodasi = $dtmstrf['akomodasi'];
                $margin = $dtmstrf['margin'];
                $sewa = $dtmstrf['sewa'];
                $asuransi = $dtmstrf['asuransi'];
                $total = $dtmstrf['total'];
            }
            $data = [
                'idpemeriksaan' => $idpemeriksaan,
                'idpendaftaran' => $idpendaftaran,
                'icd' => $icd,
                'idjenistarif' => $idjenistarif,
                'coapendapatan' => $coapendapatan,
                'jasaoperator' => $jasaoperator,
                'nakes' => $nakes,
                'jasars' => $jasars,
                'bhp' => $bhp,
                'akomodasi' => $akomodasi,
                'margin' => $margin,
                'sewa' => $sewa,
                'asuransi' => $asuransi,
                'total' => $total,
                'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
            ];

            $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $icd);
            if ($cekData->num_rows() > 0 && $pesan != 'Tindakan' && $pesan != 'Laboratorium' && $pesan != 'Radiologi') {
                pesan_success_danger(0, "", $pesan . " Sudah Ada..!", "js");
            } else {
                $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                $this->log_hasilpemeriksaan($idpendaftaran, 'insert', $icd);
                if ($modesave != 'verifikasi') {
                    $arrMsg = $this->mkombin->hitungtotaldiskon($idpendaftaran);
                    $this->load->model('mtriggermanual');
                    $this->mtriggermanual->airs_hasilpemeriksaan($idpendaftaran, $idpemeriksaan, $this->db->insert_id(), $pesan, $icd);
                }
                if (empty($response)) {
                    pesan_success_danger($arrMsg, "Add " . $pesan . " Success..!", "Add " . $pesan . " Failed..!", "js");
                } else {
                    return $arrMsg;
                }
            }
        } else {
            echo json_encode(['status' => 'selesaiperiksa']);
        }
    }
    // SIMPAN DATA KETERANGAN OBAT SETELAH MENGUBAH text keteranganobat (RESEP OBAT)
    public function pemeriksaanklinik_saveketobat()
    {
        $arrMsg =  $this->db->update('person_pendaftaran', ['keteranganobat' => qlreplace_quote($this->input->post('data'))], ['idpendaftaran' => $this->input->post('id')]);
        $this->log_hasilpemeriksaan($this->input->post('id'), 'ubah keterangan obat/resep :' . qlreplace_quote($this->input->post('data')), 'RESEP');
        pesan_success_danger($arrMsg, 'Ubah keteranganobat berhasil.', 'Ubah keteranganobat gagal.', 'js');
    }
    // SIMPAN DTAA SUBYEKTIF SETELAH SELESAI INPUT DATA
    public function pemeriksaanklinik_savedatasubyektif()
    {
        $arrMsg = $this->rs_pemeriksaan_update_where(['anamnesa' => qlreplace_quote($this->input->post('dt'))], ['idpemeriksaan' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Ubah Data Subyektif Berhasil.', 'Ubah Data Subyektif Gagal.', 'js');
    }
    //-- AMBIL DATA PEMERIKSAAN INDIKASI PASIEN WABAH
    public function pemeriksaanklinik_getpemeriksaanindikasipasienwabah()
    {
        $arrData = [
            'wabah' => $this->db->get('rs_jeniswabah')->result(),
            'status' => $this->db->get('rs_statusrawatwabah')->result(),
            'penularan'  => $this->db->get('rs_sebabpenularan')->result(),
            'pw' => $this->db->query("select  rpw.*,rp.idstatusrawatwabah from rs_pemeriksaan_wabah rpw, rs_penangananwabah rp where rpw.idpemeriksaan='" . $this->input->post('idp') . "' and rp.idpenangananwabah=rpw.idpenangananwabah")->row_array()

        ];
        echo json_encode($arrData);
    }
    //-- AMBIL DATA penangananwabah, INDIKASI PASIEN WABAH
    public function pemeriksaanklinik_getpenangananIPW()
    {
        echo json_encode($this->db->get_where('rs_penangananwabah', ['idstatusrawatwabah' => $this->input->post('status')])->result());
    }

    //////////CEK TARIF PER PAKET ICD 
    /**
     * Insert Paket Laboratorium
     * @param type $pesan
     * @param type $idpendaftaran
     * @param type $idpaket
     * @param type $idpemeriksaan
     * @param type $modesave
     * @param type $paketpaket
     * @param type $response
     * @return type
     */
    private function cekmasterpakettariflaborat($pesan, $idpendaftaran, $idpaket, $idpemeriksaan, $modesave, $paketpaket = 0, $response = '')
    {
        //siapkan data master tarif sesuai paket
        $dtmstrf = $this->db->query("select * from rs_mastertarif_paket_pemeriksaan x  where x.idpaketpemeriksaan='" . $idpaket . "'");
        $dtgrup = $this->db->query("SELECT ifnull(gruppaketperiksa,'') + 1  as grup FROM `rs_hasilpemeriksaan` WHERE idgrup ='" . $idpaket . "' and idpendaftaran='" . $idpendaftaran . "' GROUP BY gruppaketperiksa")->result_array();
        $gruppaket = 1;
        if (empty(!$dtgrup)) {
            $gupppaket = $dtgrup[0]['grup'];
        }
        //save paket
        if ($dtmstrf->num_rows() > 0) {
            $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($idpaket);
            if (!empty($listpaket)) {
                $dtmstrf = $dtmstrf->row_array();
                if (!empty($this->cekgrupperiksa($idpendaftaran)) || $paketpaket != 0) {

                    $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $idpaket, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'asuransi' => 0, 'total' => 0, 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                } else {
                    $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $idpaket, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'coapendapatan' => $dtmstrf['coapendapatan'], 'jasaoperator' => $dtmstrf['jasaoperator'], 'nakes' => $dtmstrf['nakes'], 'jasars' => $dtmstrf['jasars'], 'bhp' => $dtmstrf['bhp'], 'akomodasi' => $dtmstrf['akomodasi'], 'margin' => $dtmstrf['margin'], 'asuransi' => $dtmstrf['asuransi'], 'total' => $dtmstrf['total'], 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                }

                $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data); //save paket
                $this->log_hasilpemeriksaan($idpendaftaran, 'insert-paketlaborat-', $idpaket);
                if ($modesave != 'verifikasi') {
                    $this->load->model('mtriggermanual');
                    $this->mtriggermanual->airs_hasilpemeriksaan($idpendaftaran, $idpemeriksaan, $this->db->insert_id(), $pesan);
                }
                foreach ($listpaket as $key) {

                    $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'icd' => $key->icd, 'idpaketpemeriksaan' => $key->idpaketpemeriksaan, 'idgrup' => $idpaket, 'idjenistarif' => 0, 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'asuransi' => 0, 'total' => 0, 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                }
                $this->mkombin->hitungtotaldiskon($idpendaftaran);
                if ($paketpaket == 0) {
                    if (empty($response)) {
                        pesan_success_danger($arrMsg, "Add " . $pesan . " Success..!", "Add " . $pesan . " Failed..!", "js");
                    } else {
                        return $arrMsg;
                    }
                }
            } else {
                $listpaketparent = $this->mkombin->diagnosa_getpaketdiagnosaparent($idpaket);
                if (!empty($listpaketparent)) {

                    // simpan paket parent
                    $dtmstrf = $dtmstrf->row_array();
                    echo json_encode($dtmstrf); // debug mode
                    if (!empty($this->cekgrupperiksa($idpendaftaran)) || $paketpaket != 0) {
                        $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $idpaket, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'asuransi' => 0, 'total' => 0, 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                    } else {
                        $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $idpaket, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'coapendapatan' => $dtmstrf['coapendapatan'], 'jasaoperator' => $dtmstrf['jasaoperator'], 'nakes' => $dtmstrf['nakes'], 'jasars' => $dtmstrf['jasars'], 'bhp' => $dtmstrf['bhp'], 'akomodasi' => $dtmstrf['akomodasi'], 'margin' => $dtmstrf['margin'], 'asuransi' => $dtmstrf['asuransi'], 'total' => $dtmstrf['total'], 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                    }

                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                    // untuk menambahkan idpegawai dokter di hasil pemeriksaan
                    if ($modesave != 'verifikasi') {
                        $this->load->model('mtriggermanual');
                        $this->mtriggermanual->airs_hasilpemeriksaan($idpendaftaran, $idpemeriksaan, $this->db->insert_id(), $pesan);
                    }
                    foreach ($listpaketparent as $key) {
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($key->idpaketpemeriksaan);
                        foreach ($listpaket as $key) {
                            $data = ['gruppaketperiksa' => $gruppaket, 'idpemeriksaan' => $idpemeriksaan, 'idpendaftaran' => $idpendaftaran, 'icd' => $key->icd, 'idpaketpemeriksaan' => $key->idpaketpemeriksaan, 'idgrup' => $idpaket, 'idjenistarif' => 0, 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'asuransi' => 0, 'total' => 0, 'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)];
                            $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                        }
                    }
                    $this->mkombin->hitungtotaldiskon($idpendaftaran);
                    if ($paketpaket == 0) {
                        if (empty($response)) {
                            return pesan_success_danger($arrMsg, "Add " . $pesan . " Success..!", "Add " . $pesan . " Failed..!", "js");
                        } else {
                            return $arrMsg;
                        }
                    }
                } else {
                    pesan_danger($pesan . " Tidak Ada..!", "js");
                }
            }
        }
        // 'idjenistarif'=>$key->idjenistarif,'jasaoperator'=>$key->jasaoperator,'jasars'=>$key->jasars,'bhp'=>$key->bhp,'akomodasi'=>$key->akomodasi,'margin'=>$key->margin,'total'=>$key->total
        else {
            pesan_danger($pesan . " Tidak Ada", "js");
        }
    }


    public function diagnosa_addsinglediagnosa() //tambah single diagnosa di hasil pemeriksaan
    {
        $icd = $this->input->post("x");
        $idpendaftaran = $this->input->post("y");
        $dtmstrf = $this->mgenerikbap->select_multitable("*", "rs_mastertarif", ['icd' => $icd, 'idkelas' => 1])->row_array();
        if (empty($dtmstrf)) //jika tafir tidak ada
        {
            $idjenistarif = 0;
            $jasaoperator = 0;
            $nakes = 0;
            $jasars = 0;
            $bhp = 0;
            $akomodasi = 0;
            $margin = 0;
            $sewa = 0;
            $total = 0;
        } else //selain itu
        {
            $idjenistarif = $dtmstrf['idjenistarif'];
            $jasaoperator = $dtmstrf['jasaoperator'];
            $nakes = $dtmstrf['nakes'];
            $jasars = $dtmstrf['jasars'];
            $bhp = $dtmstrf['bhp'];
            $akomodasi = $dtmstrf['akomodasi'];
            $margin = $dtmstrf['margin'];
            $sewa = $dtmstrf['sewa'];
            $total = $dtmstrf['total'];
        }
        //save data
        $data = ['idpendaftaran' => $idpendaftaran, 'icd' => $icd, 'idjenistarif' => $idjenistarif, 'jasaoperator' => $jasaoperator, 'nakes' => $nakes, 'jasars' => $jasars, 'bhp' => $bhp, 'akomodasi' => $akomodasi, 'margin' => $margin, 'sewa' => $sewa, 'total' => $total];

        $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $icd);
        if ($cekData->num_rows() > 0) {
            pesan_success_danger(0, "", "Diagnosa Tidak Dapat Ditambahkan..!", "js");
        } else {
            $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
            pesan_success_danger($arrMsg, "Add Diagnosa Success..!", "Add Diagnosa Failed..!", "js");
        }
    }

    public function diagnosa_addpaketdiagnosa()
    {
        $idpaket = $this->input->post("x");
        $idpendaftaran = $this->input->post("y");
        //siapkan data master tarif sesuai paket
        $dtmstrf = $this->mgenerikbap->select_multitable("*", "rs_mastertarif_paket_pemeriksaan", ['idpaketpemeriksaan' => $idpaket])->row_array();
        if (empty($dtmstrf)) {
            pesan_danger("Tarif Diagnosa Belum Ditambahkan..!", "js");
        } else {
            $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($idpaket);
            if (!empty($listpaket)) {
                foreach ($listpaket as $key) {
                    $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $key->icd);
                    if (empty($cekData->num_rows())) {
                        $data = ['idpendaftaran' => $idpendaftaran, 'icd' => $key->icd, 'idpaketpemeriksaan' => $key->idpaketpemeriksaan, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'sewa' => 0, 'total' => 0];
                        $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                    } else {
                        pesan_success_danger(0, "", "Paket Sudah Ada..!", "js");
                    }
                }
                $data = ['idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $dtmstrf['idpaketpemeriksaan'], 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => $dtmstrf['jasaoperator'], 'nakes' => $dtmstrf['nakes'], 'jasars' => $dtmstrf['jasars'], 'bhp' => $dtmstrf['bhp'], 'akomodasi' => $dtmstrf['akomodasi'], 'margin' => $dtmstrf['margin'], 'sewa' => $dtmstrf['sewa'], 'total' => $dtmstrf['total']];
                $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                pesan_success_danger($arrMsg, "Add Diagnosa Success..!", "Add Diagnosa Failed..!", "js");
            } else {
                $listpaketparent = $this->mkombin->diagnosa_getpaketdiagnosaparent($idpaket);
                if (!empty($listpaketparent)) {
                    foreach ($listpaketparent as $key) {
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($key->idpaketpemeriksaan);
                        if (!empty($listpaket)) {
                            foreach ($listpaket as $key) {
                                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran, $key->icd);
                                if (empty($cekData->num_rows())) {
                                    $data = ['idpendaftaran' => $idpendaftaran, 'icd' => $key->icd, 'idpaketpemeriksaan' => $key->idpaketpemeriksaan, 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => 0, 'nakes' => 0, 'jasars' => 0, 'bhp' => 0, 'akomodasi' => 0, 'margin' => 0, 'sewa' => 0, 'total' => 0];
                                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                                } else {
                                    pesan_success_danger(0, "", "Paket Sudah Ada..!", "js");
                                    return true;
                                }
                            }
                        } else {
                            pesan_success_danger(0, "", "Paket Diagnosa Tidakada..!", "js");
                            return true;
                        }
                    }

                    $data = ['idpendaftaran' => $idpendaftaran, 'idpaketpemeriksaan' => $dtmstrf['idpaketpemeriksaan'], 'idgrup' => $idpaket, 'idjenistarif' => $dtmstrf['idjenistarif'], 'jasaoperator' => $dtmstrf['jasaoperator'], 'nakes' => $dtmstrf['nakes'], 'jasars' => $dtmstrf['jasars'], 'bhp' => $dtmstrf['bhp'], 'akomodasi' => $dtmstrf['akomodasi'], 'margin' => $dtmstrf['margin'], 'sewa' => $dtmstrf['sewa'], 'total' => $dtmstrf['total']];
                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan", $data);
                    pesan_success_danger($arrMsg, "Add Diagnosa Success..!", "Add Diagnosa Failed..!", "js");
                    // echo json_encode($listpaket);
                } else {
                    pesan_success_danger(0, "", "Paket Diagnosa Tidakada..!", "js");
                    return true;
                }
            }
        }
    }
    public function ubah_harga_paket()
    {
        $harga = $this->input->post('x');
        $y = $this->input->post('y');
        $arrMsg = $this->db->update('rs_hasilpemeriksaan', ['total' => $harga], ['idhasilpemeriksaan' => $y]);
        pesan_success_danger($arrMsg, 'Update Harga Paket Success..!', 'Update Harga Paket Failed..!', 'js');
    }
    public function delete_hasil_periksa()
    {
        $idpendaftaran = $this->input->post('x');
        $icd = $this->input->post('y');
        $arrMsg = $this->db->delete('rs_hasilpemeriksaan', ['icd' => $icd, 'idpendaftaran' => $idpendaftaran]);
        pesan_success_danger($arrMsg, 'Delete Diagnosa Success..!', 'Delete Diagnosa Failed..!', 'js');
    }
    public function delete_paket_periksa()
    {
        $idpendaftaran = $this->input->post('x');
        $idgrup = $this->input->post('y');
        $arrMsg = $this->db->delete('rs_hasilpemeriksaan', ['idgrup' => $idgrup, 'idpendaftaran' => $idpendaftaran]);
        pesan_success_danger($arrMsg, 'Delete Diagnosa Success..!', 'Delete Diagnosa Failed..!', 'js');
    }
    //salin riwayat obat pasien ralan
    public function obat_salinriwayatobat()
    {
        $idbp   = $this->input->post('idbp'); // idbarangpemeriksaan
        $grup   = $this->input->post('grup'); // grup racik

        if ($grup == '0') {
            $sql    = "SELECT rb.idbarang, rb.penggunaan, rb.idjenistarif, rb.coapendapatan, rb.jumlahdiresepkan, rb.jumlahpemakaian, rb.kekuatan, rb.idbarangaturanpakai, rb.jenisrawat, rb.jenisinput , b.hargaumum as hargajual FROM rs_barangpemeriksaan rb join rs_barang b on b.idbarang=rb.idbarang where idbarangpemeriksaan='" . $idbp . "'";
            $query  = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $this->insert_salinriwayatobat($result);
            } else {
                echo json_encode(['status' => 'danger', 'message' => 'Riwayat Tidak Ditemukan.']);
            }
        } else {
            $query = $this->db->select('idpendaftaran')->get_where('rs_barangpemeriksaan', ['idbarangpemeriksaan' => $idbp]);
            if ($query->num_rows() > 0) {
                $query = $query->row_array();
                $idpendaftaran = $query['idpendaftaran'];
                $sql = "SELECT rb.idbarang, rb.penggunaan, rb.idjenistarif, rb.coapendapatan, rb.jumlahdiresepkan, rb.jumlahpemakaian, rb.kekuatan, rb.idbarangaturanpakai, rb.jenisrawat, rb.jenisinput , b.hargaumum as hargajual, rbg.grup, rbg.idkemasan, rbg.jumlah, rbg.jumlahdiberikan, rbg.jenisrawat
                        FROM rs_barangpemeriksaan rb 
                        join rs_barang b on b.idbarang=rb.idbarang 
                        join rs_barangpemeriksaan_grup rbg on rbg.idpendaftaran = rb.idpendaftaran and rbg.grup=rb.grup
                        where rb.idpendaftaran='" . $idpendaftaran . "' and rb.grup='" . $grup . "'";
                $query  = $this->db->query($sql);
                if ($query->num_rows() > 0) {
                    $result = $query->result_array();
                    $this->insert_salinriwayatobat($result);
                } else {
                    echo json_encode(['status' => 'danger', 'message' => 'Riwayat Tidak Ditemukan.']);
                }
            } else {
                echo json_encode(['status' => 'danger', 'message' => 'Riwayat Tidak Ditemukan.']);
            }
        }
    }

    private function insert_salinriwayatobat($result)
    {
        $idp    = $this->input->post('idp'); //idpemeriksaan
        $idpend = $this->input->post('idpend'); //idpendaftaran
        $grup   = $this->input->post('grup'); // grup racik
        if ($grup != '0') {
            //insert rs_barangpemeriksaan_grup
            $insert_grup = [
                'idpendaftaran' => $idpend,
                'grup' => $grup,
                'jumlah' => $result[0]['jumlah'],
                'idkemasan' => $result[0]['idkemasan'],
                'jumlahdiberikan' => $result[0]['jumlahdiberikan'],
                'jenisrawat' => $result[0]['jenisrawat']
            ];
            $this->db->replace('rs_barangpemeriksaan_grup', $insert_grup);
        }

        //insert rs_barangpemeriksaan
        foreach ($result as $data) {
            $datainsert = [
                'idpendaftaran' => $idpend,
                'idpemeriksaan' => $idp,
                'idbarang' => $data['idbarang'],
                'penggunaan' => $data['penggunaan'],
                'idjenistarif' => $data['idjenistarif'],
                'coapendapatan' => $data['coapendapatan'],
                'jumlahdiresepkan' => $data['jumlahdiresepkan'],
                'jumlahpemakaian' => $data['jumlahpemakaian'],
                'kekuatan' => $data['kekuatan'],
                'idbarangaturanpakai' => $data['idbarangaturanpakai'],
                'jenisrawat' => $data['jenisrawat'],
                'jenisinput' => $data['jenisinput'],
                'grup' => $grup,
                'harga' => $data['hargajual'],
                'istagihan' => 1
            ];
            $insert = $this->db->insert('rs_barangpemeriksaan', $datainsert);
        }
        pesan_success_danger($insert, 'Resep Berhasil Disalin.', 'Resep Gagal Disalin.', 'js');
    }


    /////////////SAVEBHP//////////
    public function pemeriksaan_inputbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $idperiksa  = $this->input->post('i');
            $periksa    = $this->mkombin->get_caripemeriksaanpasien($idperiksa); //idpemeriksaan
            $idbarang   = $this->input->post('x');

            $post       = $this->input->post();
            $jenispemakaian = ((isset($post['jenispemakaian'])) ? $post['jenispemakaian'] : 'resep');
            $modesave   = ((isset($post['modesave'])) ? $post['modesave'] : '');
            if ($this->isselesaipemeriksaan($periksa['idpendaftaran'], $modesave)) {
                $detail_bhp = $this->db->query("select idbarang, idjenistarif, hargaumum as hargajual,kekuatan,keluhan from rs_barang where idbarang = '" . $idbarang . "'")->row_array();
                if (!empty($detail_bhp)) {
                    $data = [
                        'idpemeriksaan' => $idperiksa,
                        'idbarang' => $detail_bhp['idbarang'],
                        'jumlahpemakaian' => '1',
                        'idjenistarif' => $detail_bhp['idjenistarif'],
                        'harga' => $detail_bhp['hargajual'],
                        'kekuatan' => $detail_bhp['kekuatan'],
                        'keluhan' => $detail_bhp['keluhan'],
                        'idpendaftaran' => $periksa['idpendaftaran'],
                        'jenispemakaian' => $jenispemakaian,
                        'istagihan' => (($modesave == 'verifikasi') ? 0 : 1)
                    ];

                    //                    if(empty(!$this->session->userdata('idunitterpilih')))
                    //                    {
                    //                        $data['idbarangpembelian'] = $postdata[0];
                    //                        $data['idunit'] = $this->session->userdata('idunitterpilih');
                    //                    }
                    $arrMsg = $this->db->insert("rs_barangpemeriksaan", $data);
                    $this->log_hasilpemeriksaan($periksa['idpendaftaran'], 'insert', '', $detail_bhp['idbarang']);
                    pesan_success_danger($arrMsg, 'Add BHP Success.!', 'Add BHP Failed.!', 'js');
                } else {
                    pesan_danger("BHP Tidak Ditemukan..!", "js");
                }
            } else {
                echo json_encode(['status' => 'selesaiperiksa']);
            }
        }
    }
    //////////////SETTING BHP/////////////////
    public function pemeriksaan_settbhp()
    {
        $id     = $this->input->post('x'); //idbarangpemeriksaan
        $y      = $this->input->post('y'); //idbarang
        $type   = $this->input->post('type'); //tipe yang diubah
        $d      = $this->input->post('d'); //data yang diubah
        $g      = $this->input->post('g'); // grup peracikan
        $i      = $this->input->post('i'); // idpendaftaran
        $jenisrawat = $this->input->post('jenisrawat');
        $idbarang   = $this->input->post('idbarang');
        $idinap     = $this->input->post('idinap');
        $post       = $this->input->post();
        $modesave   = ((isset($post['modesave'])) ? $post['modesave'] : '');

        if (
            $type == 'jumlah' or
            $type == 'kekuatan' or
            $type == 'grup' or
            $type == 'jumlahdiberikan' or
            $type == 'del' or
            $type == 'jumlahRacikan' or
            $type == 'kemasan' or
            $type == 'jumlahRacikanDiberikan'
        ) {
            if (!$this->isselesaipemeriksaan($i, $modesave)) {
                echo json_encode(['status' => 'selesaiperiksa']);
                return;
            }
        }

        if ($type == 'grup') {
            $data = ['grup' => $d];
            // simpan atau update ke barangpemeriksan_grup
            $cekgrup = $this->db->query("select grup from rs_barangpemeriksaan_grup  where idpendaftaran='" . $i . "' and grup='" . $d . "' and jenisrawat='" . $jenisrawat . "'");
            if ($cekgrup->num_rows() > 0) //jika grup sudah ada
            {
                $this->db->update('rs_barangpemeriksaan_grup', ['grup' => $d], ['idpendaftaran' => $i, 'grup' => $g, 'jenisrawat' => $jenisrawat]);
            } else {
                if ($d != 0) {
                    $this->db->insert('rs_barangpemeriksaan_grup', ['idpendaftaran' => $i, 'grup' => $d, 'jenisrawat' => $jenisrawat]);
                }
            }
        } else if ($type == 'jumlahRacikan') {
            $datagrup = ['jumlah' => $d, 'jumlahdiberikan' => $d];
            //jika jenis pemakaian copyresep
            if (isset($post['jenispemakaian']) && $post['jenispemakaian'] == 'copyresep') {
                $datagrup = ['jumlah' => $d, 'jumlahdiberikan' => 0];
            }

            $data = ['grup' => $g];
            $this->db->update('rs_barangpemeriksaan_grup', $datagrup, ['idpendaftaran' => $i, 'grup' => $g, 'jenisrawat' => $jenisrawat]);
        } else if ($type == 'jumlahRacikanDiberikan') {
            $data = ['grup' => $g];
            $this->db->update('rs_barangpemeriksaan_grup', ['jumlahdiberikan' => $d], ['idpendaftaran' => $i, 'grup' => $g, 'jenisrawat' => $jenisrawat]);
        } else if ($type == 'kemasan') {
            $data = ['grup' => $g];
            $this->db->update('rs_barangpemeriksaan_grup', ['idkemasan' => $d], ['idpendaftaran' => $i, 'grup' => $g, 'jenisrawat' => $jenisrawat]);
        } else if ($type == 'del') {
            if ($modesave == 'verifikasi') {
                $arrMsg = $this->db->update('rs_barangpemeriksaan', ['isverifklaim' => 0], ['idbarangpemeriksaan' => $id]);
            } else {
                $arrMsg = $this->db->delete('rs_barangpemeriksaan', ['idbarangpemeriksaan' => $id]);
            }
            $this->log_hasilpemeriksaan($i, 'hapus', '', $idbarang);
            $this->mkombin->hitungtotaldiskon($i);
            return pesan_success_danger($arrMsg, 'Delete BHP Success..!', 'Delete BHP Failed..!', 'js');
        } else if ($type == 'kekuatan') {
            $data = ['kekuatan' => $d];
        } else if ($type == 'penggunaan') {
            $data = ['penggunaan' => $d];
        } else if ($type == 'aturanpakai') {
            $data = ['idbarangaturanpakai' => $d];
        } else if ($type == 'jumlahdiberikan') {
            $data = ['jumlahpemakaian' => $d];
            if (empty(!$this->session->userdata('idunitterpilih'))) {
                $this->db->update('rs_barangpemeriksaan_detail', ['jumlah' => $d], ['idbarangpemeriksaan' => $id]);
            }
        } else {
            //jika jenis pemakaian => copy resep
            if (isset($post['jenispemakaian']) && $post['jenispemakaian'] == 'copyresep') {
                $data = ['jumlahdiresepkan' => $d, 'jumlahpemakaian' => 0];
            } else {
                $data = ['jumlahdiresepkan' => $d, 'jumlahpemakaian' => $d];
            }

            if (empty(!$this->session->userdata('idunitterpilih'))) {
                $this->db->update('rs_barangpemeriksaan_detail', ['jumlah' => $d], ['idbarangpemeriksaan' => $id]);
            }
        }

        $whereupdate = ['idbarangpemeriksaan' => $id];

        if (!empty($g)) {
            $whereupdate = ['idpendaftaran' => $i, 'grup' => $g, 'jenisrawat' => $jenisrawat];
        }

        $arrMsg = $this->db->update('rs_barangpemeriksaan', $data, $whereupdate);
        if ($modesave != 'verifikasi') {
            $arrMsg = $this->mkombin->hitungtotaldiskon($i);
        }
        pesan_success_danger($arrMsg, 'Update BHP Success..!', 'Update BHP Failed..!', 'js');
    }
    ///////////////////////ADMISSION ANTRIAN///////////////////
    //-------------------------- vv Standar
    public function setting_administrasiantrian()
    {
        return [
            'content_view'      => 'pelayanan/v_administrasiantrian',
            'active_menu'       => 'pelayanan',
            'active_sub_menu'   => 'administrasiantrian',
            'active_menu_level' => '',
            'script_js'         => ['js_administrasiantrian'],
            'plugins'           => [PLUG_DATATABLE, PLUG_DATE]
        ];
    }
    public function administrasiantrian() //list administrasiantrian
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIANTRIAN)) //lihat define di atas
        {
            $data                = $this->setting_administrasiantrian(); //letakkan di baris pertama
            $data['dt_unit']     = $this->mgenerikbap->select_multitable("idunit, namaunit", "rs_unit")->result();
            $data['data_list']   = array();
            $data['title_page']  = 'Administrasi Antrian';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Antrian';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    //dipanggil dari js_administrasiantrian - tampilkanantrian()
    public function loadantrian()
    {
        $idunit = !empty($this->input->post("i")) ? '`rj`.`idunit` = ' . $this->input->post("i") . '
            AND' : '';
        $tanggal    = $this->input->post("t");
        $this->session->set_userdata('tahunbulan', tahunbulan($tanggal));
        $arrMsg = $this->db->query("SELECT `idpemeriksaan`,ppd.idpendaftaran, pp.namalengkap, `rj`.`idunit`, `rprk`.`norm`, `nosep`, `noantrian`, `rprk`.`nopesan`, `tanggal`, `ru`.`namaunit`, pp.idperson
            FROM `rs_pemeriksaan` `rprk`, `person_pendaftaran` `ppd`, `rs_jadwal` `rj`, `rs_unit` `ru`, `person_pasien` `ps`, `person_person` `pp`
             
            WHERE  $idunit date(tanggal) = '$tanggal'
            AND `rprk`.`idjadwal` = `rj`.`idjadwal`
            AND `rprk`.`idpendaftaran` = `ppd`.`idpendaftaran`
            AND `ppd`.`idunit` = `ru`.`idunit`
            AND `rj`.`idunit` = `ru`.`idunit`
            AND ps.norm = rprk.norm
            AND pp.idperson = ps.idperson")->result();
        echo json_encode($arrMsg);
    }

    //dipanggil dari js_administrasiantrian - tampilkanantrian()
    public function loadantrianbypasien()
    {
        $idrmsep    = $this->input->post("q");
        $hasilantrian = $this->db->query("select idpemeriksaan, rj.idunit,rprk.norm, nosep, noantrian, rprk.nopesan, tanggal, ru.namaunit from rs_pemeriksaan rprk, person_pendaftaran ppd, rs_jadwal rj, rs_unit ru where rj.idjadwal=rprk.idjadwal and rprk.idpendaftaran=ppd.idpendaftaran and ru.idunit=rj.idunit and (rprk.norm='" . $idrmsep . "' or nosep='" . $idrmsep . "') order by idpemeriksaan desc limit 0, 50")->result();
        echo json_encode($hasilantrian);
    }

    //dipanggil dari js pendaftaranpoli, js administrasiantrian 
    public function setNomorantrian()
    {
        $idunit         = $this->input->post("vidu");
        $idpemeriksaan  = $this->input->post("vidp");
        $idperson       = $this->input->post("vidpr");
        $this->db->query("update person_pendaftaran set isantri=1 where idpendaftaran='" . $this->input->post('vidpend') . "'");
        $this->antrianset_antrianperiksa($idunit, $idpemeriksaan, $idperson);

    }
    public function antrian_cetakantrian()
    {
        $idperson = $this->input->post('idp');
        $idpemeriksaan = $this->input->post('idpr');
        $ap = $this->db->query("SELECT namadokter(b.idpegawaidokter) as dokter, b.idjadwal,b.noantrian, DATE_FORMAT(b.waktu, '%d/%m/%Y') waktu, b.waktu as tglperiksa , c.namaloket, d.namalengkap,usia(d.tanggallahir) as usia, date_format(d.tanggallahir,'%d %b %Y') as tanggallahir, d.alamat, b.norm, b.idpendaftaran FROM rs_pemeriksaan b, antrian_loket c, person_person d WHERE b.idpemeriksaan='" . $idpemeriksaan . "' and c.idloket=b.idloket and d.idperson='" . $idperson . "'")->row_array();
        $waktu = date('Y-m-d', strtotime($ap['tglperiksa']));
        // antrian sep
        $antrisep = $this->db->query("select a.no, l.namaloket from antrian_antrian a, antrian_loket l where a.idperson='" . $idperson . "' and date(a.tanggal)='" . $waktu . "' and a.idloket='39' and l.idloket = a.idloket")->row_array();
        
        //[Andri] - save log task antrean - taskId 3 start
        $idpendaftaran = $ap['idpendaftaran'];
        if($idpendaftaran)
        {
            $queryAntrian = $this->db->query("select * from antrian_antrian where idpemeriksaan ='".$idpemeriksaan."' AND idperson='".$idperson."'")->row_array();
            if($queryAntrian)
            {
                $this->bridging_save_log_antrean(1,$queryAntrian['idantrian'],'OK',$idpemeriksaan,$idpendaftaran); 
            }
        }        
        //[Andri] - save log task antrean - taskId 3 end
        
        echo json_encode(['ap' => $ap, 'antriansep' => $antrisep]);
    }

    ///////////////////////ADMISSION KASIR///////////////////
    //-------------------------- vv Standar
    public function setting_kasir()
    {
        return [
            'content_view'      => 'pelayanan/v_kasir',
            'active_menu'       => 'pelayanan',
            'active_sub_menu'   => 'kasir',
            'active_menu_level' => '',
            'script_js'         => ['js_kasir', 'js_cetakresumepasien'],
            'plugins'           => [PLUG_DATATABLE, PLUG_DATE, PLUG_DROPDOWN]
        ];
    }
    public function kasir() //list kasir
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KASIR)) //lihat define di atas
        {
            $data                = $this->setting_kasir(); //letakkan di baris pertama
            $data['data_list']   = array();
            $data['title_page']  = 'KASIR';
            $data['mode']        = 'view';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public  function pemeriksaanklinik_getdtpasien()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->query("select ppas.norm, pper.namalengkap from person_pendaftaran ppd, person_pasien ppas, person_person pper 
where ppas.norm=ppd.norm and ppd.isclosing=0 and pper.idperson=ppas.idperson and pper.namalengkap LIKE '%" . $cari . "%' 
UNION 
select ppas.norm, pper.namalengkap from person_pendaftaran ppd, person_pasien ppas, person_person pper 
where ppas.norm=ppd.norm and ppd.isclosing=0 and pper.idperson=ppas.idperson and ppas.norm LIKE '%" . $cari . "%' 
GROUP by norm limit 0, 10");
        $fetchData = $sql->result();
        $data = array();
        foreach ($fetchData as $obj) {
            $data[] = array("id" => $obj->norm, "text" => $obj->norm . ' | ' . $obj->namalengkap);
        }
        echo json_encode($data);
    }
    // ambil data pasien untuk cari by pasien
    public function kasir_getdtpasien()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->like("namalengkap", "$cari");
        $sql = $this->db->or_like("norm", "$cari");
        $sql = $this->db->limit(20);
        $sql = $this->db->offset($halaman * 30);
        $sql = $this->db->order_by('namalengkap', 'asc');
        $sql = $this->db->select(" person_person.idperson, norm, namalengkap");
        $sql = $this->db->join('person_pasien', 'person_pasien.idperson= person_person.idperson', 'left');
        $sql = $this->db->get("person_person");
        $fetchData = $sql->result();
        $data = array();
        foreach ($fetchData as $obj) {
            $data[] = array("id" => $obj->norm, "text" => $obj->norm . ' | ' . $obj->namalengkap);
        }
        echo json_encode($data);
    }
    //dipanggil dari js_kasir - tampilkankasir()

    public function dt_listkasir()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_kasir_(true);
        //        $last = $this->db->last_query();
        $data = [];
        $no = 0;
        $dibayar = '';
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $nominal = $obj->tagihan - $obj->dibayar;
                $menucetak = '';
                if ((($nominal > 0 or $nominal < 0) && $obj->isselesaisemua > 0) && ($this->pageaccessrightbap->checkAccessRight(V_MENUKASIR)) && $obj->idstatuskeluar == '2') {
                    if ($nominal > 0) {
                        /**
                         * OLD SCRIPT
                         */
                        // if ($obj->carabayar != "jknnonpbi" && $obj->carabayar != "jknpbi") {
                        //     $dibayar =  '<a id="bayar" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" alt2="' . convertToRupiah($nominal) . '" alt3="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Bayar" href="#"><i class="fa fa-money"></i></a>';
                        // } else if ($obj->carabayar == "jknnonpbi") {
                        //     $dibayar =  '<a id="jknnonpbi" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" nominal="' . $nominal . '"  alt2="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="JKN-non PBI" href="#"><i class="fa fa-handshake-o"></i></a>';
                        // } else if ($obj->carabayar == "jknpbi") {
                        //     $dibayar =  '<a id="jknpbi" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" nominal="' . $nominal . '" alt2="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class="  btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="JKN-PBI" href="#"><i class="fa fa-handshake-o"></i></a>';
                        // }

                        /**
                         * NEW SCRIPT
                         */
                        if($obj->carabayar == "jknnonpbi") {
                            $dibayar =  '<a id="jknnonpbi" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" nominal="' . $nominal . '"  alt2="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="JKN-non PBI" href="#"><i class="fa fa-handshake-o"></i></a>';
                        }else if($obj->carabayar == "jknpbi") {
                            $dibayar =  '<a id="jknpbi" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" nominal="' . $nominal . '" alt2="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class="  btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="JKN-PBI" href="#"><i class="fa fa-handshake-o"></i></a>';
                        }else{
                            $dibayar =  '<a id="bayar" alt="' . $obj->idtagihan . '" ispesanranap="' . $obj->statusinap . '" alt2="' . convertToRupiah($nominal) . '" alt3="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Bayar" href="#"><i class="fa fa-money"></i></a>';
                        }
                    }
                } else if ($nominal > 0 && $obj->isselesaisemua == "0") {
                    $dibayar = 'BLM SLS';
                } else if ($nominal > 0 && $obj->isselesaisemua == "1") {
                    $dibayar = 'BLM DIBAYAR';
                } else {
                    $dibayar = (($nominal > 0) ?  'BLM DIBAYAR' :  'DIBAYAR');
                }

                $statuscetakSEP = '';
                if( $obj->sudahcetakSEP == 1 ){
                    $statuscetakSEP = '<label test-dibayar="'.$obj->dibayar.'" test-tagihan="'.$obj->tagihan.'" test-nominal="'.$nominal.'"><i style="color:blue;" class="fa fa-check-square-o" fa-lg text text-red"></i> SEP sudah terbit </label>';
                }

                $row = [
                    ++$no,
                    $obj->waktutagih,
                    $obj->norm,
                    '<a id="pemanggilankasir" idp="' . $obj->idpendaftaran . '" idper="' . $obj->idperson . '" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Pemanggilan Kasir"><i class="fa fa-bullhorn"></i></a> ' . $obj->namalengkap,
                    $obj->nosep,
                    $obj->kelas,
                    $obj->carabayar.'</br>'.$statuscetakSEP,
                    $obj->statuspulang,
                    $obj->statustagihan . '<br><label class="label label-default">Dibayar:' . convertToRupiah($obj->dibayar) . ' / Kekurangan:' . convertToRupiah($nominal) . ' / Tot.Tagihan:' . convertToRupiah($obj->tagihan) . '</label>', $dibayar . '<br><label class="label label-default"> ' . ql_statusperiksa($obj->idstatuskeluar) . '</label>', $obj->statuspemeriksaan
                ];


                if ($obj->cekstatustagih !== 'terbuka') {

                    //                        $menucetak .=' <a id="cetakvpasiengabung" alt="'.$obj->idpendaftaran.'" alt4="'.$obj->jenispemeriksaan.'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Versi Pasien (Gabung)" href="#"><i class="fa fa-indent"></i></a>'; 
                    //                        $menucetak .=' <a onclick="generatetopdf_versipasien('.$obj->idpendaftaran.')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Versi Pasien"><i class="fa fa-file-pdf-o"></i></a>';
                    //                        $menucetak .=' <a id="generatetoexcek_versipasien" judul="'.$obj->idpendaftaran.'|'.$obj->norm.'|" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Versi Pasien"><i class="fa fa-file-excel-o"></i></a>';
                    if ($obj->carabayar != 'mandiri') {
                        $menucetak .= ' <a id="cetakvjaminan" alt="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Ralan Jaminan" href="#"><i class="fa fa-align-left"></i></a>';
                        $menucetak .= ' <a id="cetakvmandiri" alt="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" style="background-color:#ffeb3b;" class="btn btn-xs" data-toggle="tooltip" data-original-title="Ralan Non Jaminan" href="#"><i class="fa fa-align-right"></i></a>';
                    }
                }

                $menucetak .= ' <a id="cetakvpasienralan" alt="' . $obj->idpendaftaran . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Invoice (Ralan)" href="#"><i class="fa fa-align-center"></i></a>';
                $menucetak .= ' <a id="cetakvpasien" alt="' . $obj->idpendaftaran . '" alt4="rajalnap" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Invoice (Ranap)" href="#"><i class="fa fa-align-left"></i></a>';
                $menucetak .= ' <a id="cetakvdetail" alt="' . $obj->idpendaftaran . '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Detail Invoice Ranap" href="#"><i class="fa fa-align-justify"></i></a>';

                // cetak excel
                // $menucetak .= ' <a id="cetakExcelPasienRalan" alt="'.$obj->idpendaftaran.'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Excel Invoice (Ralan)" href="#"><i class="fa fa-align-center"></i></a>';
                $menucetak .= ' <a id="cetakExcelPasienRajalnap" alt="' . $obj->idpendaftaran . '" alt4="rajalnap" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Excel Invoice (Ranap)" href="#"><i class="fa fa-align-left"></i></a>';
                $menucetak .= ' <a id="cetakExcelDetail" alt="' . $obj->idpendaftaran . '" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Excel Detail Invoice Ranap" href="#"><i class="fa fa-align-justify"></i></a>';
                $menucetak .= ' <a id="cetakvpasienGabung" alt="' . $obj->idpendaftaran . '" alt4="invoiceGabung" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Invoice (GABUNG)" href="#"><i class="fa fa-align-left"></i></a>';
                $menucetak .= ' <a id="cetakExcelVersiClaim2" alt="' . $obj->idpendaftaran . '" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Excel Detail Versi GABUNG" href="#"><i class="fa fa-align-justify"></i></a>';
                //                    if($obj->jenispemeriksaan=='rajalnap')
                //                    {
                //                        
                //                    }

                //                    $menucetak .= ' <a id="cetakvjenistarif" alt="'.$obj->idpendaftaran.'" alt4="'.$obj->jenispemeriksaan.'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Versi Jenis Tarif" href="#"><i class="fa fa-list-ul"></i></a>';
                $menucetak .= ' <a onclick="buatQrPasien(' . $obj->norm . ')" data-toggle="tooltip" data-original-title="Cetak kartu" class="btn btn-primary btn-xs" ><i class="fa fa-credit-card">  ' .$obj->cetakkartu. ' </i></a>'; // Andri [bug fix 0000543]
                // var_dump($obj->cetakkartu);// die;
                // $menucetak .=' <a id="cetakvpasiengabung" alt="'.$obj->idpendaftaran.'" alt4="'.$obj->jenispemeriksaan.'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Versi Pasien (Gabung)" href="#"><i class="fa fa-indent"></i></a>'; 
                // $menucetak .=' <a id="generatetoexcek_versipasien" judul="'.$obj->idpendaftaran.'|'.$obj->norm.'|" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Versi Pasien"><i class="fa fa-file-excel-o"></i></a>';

                $row[] = $menucetak;


                $menuaksi = '';
                //menu aksi
                if ($obj->jenispemeriksaan != 'rajal' && $obj->status == 'ranap') {
                    $menuaksi = '<i data-toggle="tooltip" data-original-title="Belum diijinkan pulang" class="fa fa-bed fa-lg"></i>';
                } else {
                    if ($obj->idstatuskeluar == '2') {
                        if ($this->pageaccessrightbap->checkAccessRight(V_MENUKASIRBATALSELESAI)) {
                            if ($obj->carabayar == 'jknpbi' or $obj->carabayar == 'jknnonpbi') {
                                $menuaksi = '<a id="batalselesai" idp="' . $obj->idpendaftaran . '" idt="' . $obj->idtagihan . '" nama="' . $obj->namalengkap . '" norm="' . $obj->norm . '"  class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Selesai"><i class="fa fa-minus-circle"></i></a>';
                            }
                        }

                        if ($this->pageaccessrightbap->checkAccessRight(V_MENUKASIRBATALSELESAINONBPJS)) {
                            if ($obj->carabayar != 'jknpbi' and $obj->carabayar != 'jknnonpbi' and $obj->statusrekap == 0) {
                                $menuaksi = '<a id="batalselesai" idp="' . $obj->idpendaftaran . '" idt="' . $obj->idtagihan . '" nama="' . $obj->namalengkap . '" norm="' . $obj->norm . '"  class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Selesai"><i class="fa fa-minus-circle"></i></a>';
                            }
                        }

                        $menuaksi .= ' <a id="resetpembayaran" idp="' . $obj->idpendaftaran . '" class="btn btn-xs btn-danger" ' .  ql_tooltip('reset pembayaran ralan') . '><i class="fa fa-recycle"></i></a>';

                        if ($obj->carabayar == "tagihan" or $obj->carabayar == "hutang" or $obj->carabayar == "transfer") {
                            $menuaksi .= '';
                        } else {
                            $menuaksi .= ' <a id="cetakvklaim" alt="' . $obj->idpendaftaran . '" alt4="' . $obj->jenispemeriksaan . '" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Versi Klaim" href="#"><i class="fa fa-align-left"></i></a>';
                        }

                        $verif = "verif";
                        $dpjp = "dpjp";

                        // $menuaksi .= ' <a onclick="resumepasien('.$obj->idpendaftaran.',`'.$verif.'`)" class="btn  '.(($obj->resumeprint=='1')?'bg-black':'btn-primary') .' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN RALAN VERIF"><i class="fa fa-print"></i></a>'; //[Andri] remove this
                        $menuaksi .= ' <a onclick="resumepasien(' . $obj->idpendaftaran . ',`' . $dpjp . '`)" class="btn  ' . (($obj->resumeprint == '1') ? 'bg-black' : 'btn-primary') . ' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN RALAN DPJP"><i class="fa fa-print"></i></a>'; //[Andri] remove this
                    } else {
                        if ($obj->idstatuskeluar == '3') {
                            $menuaksi .= ' <a id="bukapemeriksaan" alt="' . $obj->idpendaftaran . '" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buka Pemeriksaan"><i class="fa fa-stethoscope"></i></a>';
                        } else {
                            if ($this->pageaccessrightbap->checkAccessRight(V_MENUKASIR)) {
                                if ($obj->idstatuskeluar == '0') {
                                    $menuaksi .= ' <a id="batalperiksa" alt="' . $obj->idpendaftaran . '"  class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa"><i class="fa fa-minus-circle"></i></a> ';
                                } else {
                                    if($obj->idstatuskeluar != '3'){ //jika pasien batal periksa tb person_pendafataran
                                    // if ($obj->status != 'batal') {/*jika status pemeriksaan adalah TIDAK batal */
                                        $menuaksi .= ' <a id="pemeriksaanklinikSelesai" nama="' . $obj->namalengkap . '"  norm="' . $obj->norm . '" alt2="' . $obj->idpendaftaran . '" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Selesai"  href="#"><i class="fa fa-check"></i></a>';
                                    }
                                }
                            }
                        }
                    }
                }

                $row[] = $menuaksi;
                $row[] = $obj->idstatuskeluar;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->totdt_kasir(),
            "recordsFiltered" => $this->mdatatable->filterdt_kasir(),
            "data" => $data
            //,"last"=>$last
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function resetpembayaran_ralan()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $alasan = $this->input->post('reset_alasan');
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));

        $idtagihan = $this->db->query("SELECT idtagihan FROM `keu_tagihan`  WHERE idpendaftaran ='" . $idpendaftaran . "' order by waktuinput asc limit 1")->row_array()['idtagihan'];
        //hapus tagihan selain yg pertama
        $delete = $this->db->delete('keu_tagihan', ['idpendaftaran' => $idpendaftaran, 'idtagihan !=' => $idtagihan, 'jenisperiksa' => 'rajal']);
        $dtkeu['jenistagihan']    = 'tagihan';
        $dtkeu['waktubayar']      = NULL;
        $dtkeu['jenispembayaran'] = 'belumbayar';
        $dtkeu['statusbayar']     = 'belumdibayar';
        $dtkeu['dibayar']         = 0;
        $dtkeu['idbankasal']      = NULL;
        $dtkeu['idbanktujuan']    = NULL;

        $delete = $this->db->update('keu_tagihan', $dtkeu, ['idpendaftaran' => $idpendaftaran, 'idtagihan' => $idtagihan, 'jenisperiksa' => 'rajal']);

        $nominal = $this->db->query("select totaltagihan(" . $idpendaftaran . ",'rajal') as total")->row_array();

        $save = $this->db->update('person_pendaftaran', ['statustagihan' => 'terbuka'], ['idpendaftaran' => $idpendaftaran]);

        $save = $this->db->update('keu_tagihan', ['nominal' => $nominal['total']], ['idpendaftaran' => $idpendaftaran, 'jenisperiksa' => 'rajal']);

        $save = $this->db->insert('log_resetpembayaran', ['idpendaftaran' => $idpendaftaran, 'iduser' => $iduser, 'alasan' => $alasan, 'jenisperiksa' => 'rajal']);
        pesan_success_danger($save, 'Reset Pembayaran Gagal.', 'Reset Pembayaran Gagal.', 'js');
    }

    public function batalselesai()
    {
        $idtagihan    = $this->input->post('idtagihan');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $this->distribusibarangkepasienrralan($idpendaftaran, 'pembetulankeluar');
        $arrMsg = $this->db->update('person_pendaftaran', ['idstatuskeluar' => 1], ['idpendaftaran' => $idpendaftaran]);
        $arrMsg = $this->db->update('rs_pemeriksaan', ['status' => 'sedang ditangani'], ['idpendaftaran' => $idpendaftaran]);
        $arrMsg = $this->db->update('keu_tagihan', ['waktubayar' => null, 'dibayar' => 0, 'jenistagihan' => 'tagihan', 'statusbayar' => 'belumdibayar', 'iduserkasir' => 0], ['idtagihan' => $idtagihan]);
        pesan_success_danger($arrMsg, 'Batal Selesai Pemeriksaan Berhasil.', 'Batal Selesai Pemeriksaan Gagal.', 'js');
    }
    /**
     * Batal Pelunasan Kasir
     */
    public function batal_pelunasan()
    {
        $arrMsg = $this->db->update('keu_tagihan', ['jenistagihan' => 'tagihan', 'dibayar' => 0, 'idbankasal' => 0, 'idbanktujuan' => 0, 'waktubayar' => null, 'statusbayar' => 'belumdibayar'], ['idtagihan' => $this->input->post('idt')]);
        pesan_success_danger($arrMsg, 'Batal Pelunasan Berhasil.', 'Batal Pelunasan Berhasil.', 'js');
    }
    //simpan pembayaran jkn
    public function simpan_pembayaran_jkn()
    {
        $idtagihan      = $this->input->post("t");
        $idpendaftaran  = $this->input->post("p");
        $jenistagihan   = $this->input->post("j");
        $post = $this->input->post();

        //ambil data untuk menentukan kekurangan jaminan dan nonjaminan
        $datatagihan = $this->db->query("SELECT 
        (select jenisperiksa from keu_tagihan where idpendaftaran='" . $idpendaftaran . "' and jenistagihan!='batal' limit 1) as jenisperiksa, 
        (select idkelas from person_pendaftaran where idpendaftaran='" . $idpendaftaran . "') as idkelas, 
        ifnull((select sum(rh.total) from rs_hasilpemeriksaan rh where rh.idpendaftaran='" . $idpendaftaran . "' and rh.jaminanasuransi=0),0) + 
        ifnull((select sum(rb.total) from rs_barangpemeriksaan rb where rb.idpendaftaran='" . $idpendaftaran . "' and rb.jaminanasuransi=0),0) as nonjaminan,
        ifnull((select sum(rh.total) from rs_hasilpemeriksaan rh where rh.idpendaftaran='" . $idpendaftaran . "' and rh.jaminanasuransi=1),0) + ifnull((select sum(rb.total) from rs_barangpemeriksaan rb where rb.idpendaftaran='" . $idpendaftaran . "' and rb.jaminanasuransi=1),0)
         as jaminan,
        (select sum(k.dibayar - k.kembalian) from keu_tagihan k where k.idpendaftaran='" . $idpendaftaran . "' and k.jenistagihan!='batal') as dibayar,
        (select sum(k.nominal - k.potongan - k.dibayar - k.kembalian) from keu_tagihan k where k.idpendaftaran='" . $idpendaftaran . "' and k.jenistagihan!='batal') as kekurangan,
        (select sum(k.dibayar - k.kembalian) from keu_tagihan k where k.idpendaftaran='" . $idpendaftaran . "' and k.jenistagihan!='batal' and (k.jenistagihan='jknpbi' or k.jenistagihan='jknnonpbi') ) as dibayarjaminan,
        (select sum(k.dibayar - k.kembalian) from keu_tagihan k where k.idpendaftaran='" . $idpendaftaran . "' and k.jenistagihan!='batal' and k.jenistagihan!='jknpbi' and k.jenistagihan !='jknnonpbi' ) as dibayarnonjaminan
        from rs_kelas limit 1")->row_array();

        $total = intval($datatagihan['jaminan']) + intval($datatagihan['nonjaminan']);
        $kekurangan = $total - intval($datatagihan['dibayar']);
        $kekurangan_jaminan = intval($datatagihan['jaminan']) -  intval($datatagihan['dibayarjaminan']);
        $kekurangan_nonjaminan = intval($datatagihan['nonjaminan']) -  intval($datatagihan['dibayarnonjaminan']);


        //--jika ada kekurangan non jaminan
        if (empty(!$kekurangan_nonjaminan)) {
            //--jika kekurangan non jaminan sama dengan total kekurangan
            if ($kekurangan_nonjaminan == $datatagihan['kekurangan']) {
                //siapkan data tagihan
                $datat = [
                    'waktubayar'   => date('Y-m-d H:i:s'),
                    'jenistagihan' => 'tagihan',
                ];
                $this->distribusibarangkepasienrralan($idpendaftaran, 'keluar');
                //update tagihan JKN
                $arrMsg = $this->db->update('keu_tagihan', $datat, ['idpendaftaran' => $idpendaftaran, 'statusbayar' => 'belumdibayar', 'jenistagihan!=' => 'batal']);
                echo json_encode(['status' => 'success', 'tagihan' => ['idtagihan' => $idtagihan, 'nominal' => $datatagihan['kekurangan']]]);
            } else {
                //jika ada kekurangan jaminan 
                if (empty(!$kekurangan_jaminan)) {
                    $datat = [
                        'waktubayar'   => date('Y-m-d H:i:s'),
                        'nominal' => $kekurangan_jaminan,
                        'dibayar' =>  pembulatan_limaratusan($kekurangan_jaminan),
                        'jenistagihan' => $jenistagihan,
                        'jenispembayaran' => $jenistagihan,
                        'iduserkasir'  => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
                        'statusbayar'  => 'dibayar'
                    ];
                    $arrMsg = $this->db->update('keu_tagihan', $datat, ['idpendaftaran' => $idpendaftaran, 'statusbayar' => 'belumdibayar', 'jenistagihan!=' => 'batal']);
                }
                //siapkan data non jaminan
                $callid = generaterandom(8);
                $datat = [
                    'waktutagih'   => date('Y-m-d H:i:s'),
                    'jenistagihan' => 'tagihan',
                    'iduserkasir'  => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
                    'statusbayar'  => 'belumdibayar',
                    'nominal'      => $kekurangan_nonjaminan,
                    'idpendaftaran' => $idpendaftaran,
                    'idkelas'      => $datatagihan['idkelas'],
                    'jenisperiksa' => $datatagihan['jenisperiksa'],
                    'callid' => $callid
                ];
                $this->mgenerikbap->setTable('keu_tagihan');
                $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($datat);

                $status = (($arrMsg) ? 'success' : 'danger');
                $tagihanbaru = $this->db->select('idtagihan, sum(nominal + pembulatan) as nominal')->get_where('keu_tagihan', ['callid' => $callid])->row_array();
                $this->db->update('keu_tagihan', ['callid' => NULL], ['idtagihan' => $tagihanbaru['idtagihan']]);
                $this->distribusibarangkepasienrralan($idpendaftaran, 'keluar');
                echo json_encode(['status' => $status, 'tagihan' =>  $tagihanbaru]);
            }
        } else {
            $this->updatekeu_pembayaranjkn($idpendaftaran, $jenistagihan, $post['dibayar']);
            $this->db->update('rs_pemeriksaan', ['status' => 'selesai'], ['idpendaftaran' => $idpendaftaran]);
            $this->mutasirajalkeranap($idpendaftaran);
            // ubah pendaftaran
            $this->ql->person_pendaftaran_insert_or_update($idpendaftaran, ['statustagihan' => 'lunas']); //panggil dari my_controller
            $this->distribusibarangkepasienrralan($idpendaftaran, 'keluar');
            echo json_encode($idpendaftaran);
        }
    }
    /**
     * Menampilkan Detail Tagihan
     */
    public  function keu_detailtagihan()
    {
        $value = $this->db->select("p.idpendaftaran, p.carabayar, date(p.waktuperiksa) as tanggalperiksa, p.statustagihan,(select namapasien(idperson) from person_pasien where norm=p.norm) as namapasien, p.norm, k.idtagihan, k.waktutagih, k.waktubayar, sum( (k.nominal - k.potongan + k.pembulatan) - if(k.statusbayar='dibayar',k.kekurangan,0)) as tagihan, sum(k.dibayar - k.kembalian) as dibayar, k.jenistagihan , (select namauser from login_user where iduser=k.iduserkasir) as user ")
            ->join('person_pendaftaran p', 'p.idpendaftaran=k.idpendaftaran')
            ->group_by('k.idtagihan')
            ->get_where('keu_tagihan k', ['k.idpendaftaran' => $this->input->post('p'), 'jenistagihan!=' => 'batal'])
            ->result_array();
        echo json_encode($value);
    }
    /**
     * Update Keu Tagihan, pembayaran dengan jkn
     * @param type $idtagihan
     * @param type $jenistagihan
     */
    private function updatekeu_pembayaranjkn($idpendaftaran, $jenistagihan, $dibayar)
    {
        //siapkan data tagihan
        $datat = [
            'waktubayar'   => date('Y-m-d H:i:s'),
            'dibayar' => $dibayar,
            'jenistagihan' => $jenistagihan,
            'jenispembayaran' => $jenistagihan,
            'iduserkasir'  => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
            'statusbayar'  => 'dibayar'
        ];
        $this->update_waktulayanan($idpendaftaran, 'kasir');
        $this->db->update('keu_tagihan', $datat, ['idpendaftaran' => $idpendaftaran, 'statusbayar' => 'belumdibayar', 'jenistagihan!=' => 'batal']);
    }

    public function simpan_returpembayaran()
    {
        $idpendaftaran  = $this->input->post("p");
        $dibayar        = $this->input->post("d");
        $datat = [
            'waktubayar'   => date('Y-m-d H:i:s'),
            'dibayar'      => $dibayar,
            'jenistagihan' => 'retur',
            'iduserkasir'  => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
            'statusbayar'  => 'dibayar',
            'kembalian'    => 0
        ];
        $this->db->update('keu_tagihan', $datat, ['idpendaftaran' => $idpendaftaran, 'statusbayar' => 'belumdibayar', 'jenistagihan!=' => 'batal']);
        echo json_encode($idpendaftaran);
    }

    public  function editjenistagihan()
    {
        $idtagihan      = $this->input->post("t");
        $jenistagihan   = $this->input->post("j");

        $this->mgenerikbap->setTable('keu_tagihan');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['jenistagihan' => $jenistagihan], $idtagihan);
        pesan_success_danger($arrMsg, 'Ubah Berhasil.', 'Ubah Gagal', 'js');
    }


    public function simpan_pembayaran()
    {
        $dibayar        = $this->input->post("b");
        $sisatagihan    = $this->input->post("s");
        $idtagihan      = $this->input->post("t");
        $idpendaftaran  = $this->input->post("p");
        $jenistagihan   = $this->input->post("j");
        $kembalian      = $this->input->post("k");

        $carabayar      = $this->input->post("carabayar");
        $idbanktujuan   = $this->input->post("bt");
        $penanggung     = $this->input->post("penanggung");
        $modebayar      = $this->input->post("modebayar");

        //jika modebayar ubahtagihan maka set belum bayar
        if ($modebayar == 'ubahtagihan') {
            $this->updatekeu_ubahpembayarannonjaminan($idtagihan);
        }

        $this->updatekeu_pembayarannonjaminan($dibayar, $kembalian, $idpendaftaran, $carabayar, $idbanktujuan, $penanggung);

        //jika ada sisa maka buat tagihan
        if (intval($sisatagihan) > 0) {
            $pendaftaran = $this->db->select('jenispemeriksaan, idkelas')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
            $datat = [
                'idpendaftaran' => $idpendaftaran,
                'nominal'       => $sisatagihan,
                'dibayar'       => 0,
                'jenistagihan'  => 'hutang',
                'jenisperiksa'  => (($pendaftaran['jenispemeriksaan'] == 'rajal') ? 'rajal' : 'ranap'),
                'statusbayar'   => 'belumdibayar',
                'idkelas'       => $pendaftaran['idkelas']
            ];
            $this->db->insert('keu_tagihan', $datat);
        }
        $this->db->update('rs_pemeriksaan', ['status' => 'selesai'], ['idpendaftaran' => $idpendaftaran]);
        $this->distribusibarangkepasienrralan($idpendaftaran, 'keluar');
        $this->mutasirajalkeranap($idpendaftaran);
        echo json_encode($idpendaftaran);
    }
    /**
     * Update Keu Tagihan pembayaran mandiri atau non jaminan
     * @param type $nominal
     * @param type $kembalian
     * @param type $idtagihan
     * @param type $jenistagihan
     * @param type $idbanktujuan
     */
    private function updatekeu_pembayarannonjaminan($nominal, $kembalian, $idpendaftaran, $carabayar, $idbanktujuan, $penanggung)
    {
        $datat = [
            'waktubayar'   => date('Y-m-d H:i:s'),
            'dibayar'      => $nominal,
            'jenistagihan' => 'tagihan',
            'jenispembayaran' => $carabayar,
            'iduserkasir'  => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
            'statusbayar'  => 'dibayar', //(($carabayar=='potonggaji') ? 'belumdibayar' : 'dibayar' ),
            'kembalian'    => ((intval($kembalian) > 0) ? $kembalian : 0),
            'idbanktujuan' => $idbanktujuan,
            'penanggung' => $penanggung
        ];
        $this->update_waktulayanan($idpendaftaran, 'kasir');
        $this->db->update('keu_tagihan', $datat, ['idpendaftaran' => $idpendaftaran, 'statusbayar' => 'belumdibayar', 'jenistagihan!=' => 'batal']);
    }

    private function updatekeu_ubahpembayarannonjaminan($idtagihan)
    {
        $datat = [
            'waktubayar'   => date('Y-m-d H:i:s'),
            'dibayar'      => 0,
            'jenistagihan' => 'tagihan',
            'statusbayar'  => 'belumdibayar',
            'kembalian'    => 0
        ];
        $this->db->update('keu_tagihan', $datat, ['idtagihan' => $idtagihan]);
    }

    //new cetak kasir
    public function cetak_versiranap()
    {
        $idpendaftaran  = $this->input->post("p");
        $data['lamaRanap']     = $this->db->query('SELECT * FROM rs_inap_biayanonpemeriksaan WHERE idpendaftaran=' . $idpendaftaran . ' AND idjenistarif=19')->num_rows(); // Andri - bugfix [0000590]
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran, 'ranap');
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
        $data['listtagihan']   = $this->mviewql->view_tarif_byjenistarifranap($idpendaftaran);
        $data['ttd'] = 'Yogyakarta, ' . date('d/m/Y');
        echo json_encode($data);
    }


    //new cetak kasir versi detail
    public function cetak_versidetail()
    {
        $idpendaftaran  = $this->input->post("p");
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran, 'ranap');
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
        $data['detail']        = $this->mviewql->viewdetailranap($idpendaftaran);

        echo json_encode($data);
    }

    public function cetak_excel_versidetail_getData()
    {
        $idpendaftaran  = $this->input->post("p");
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran, 'ranap');
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
        $data['detail']        = $this->mviewql->viewdetailranap($idpendaftaran);

        echo json_encode($data);
    }


    public function cetak_excel_versiranap_getData()
    {
        $idpendaftaran  = $this->input->post("p");
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran, 'ranap');
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
        $data['listtagihan']   = $this->mviewql->view_tarif_byjenistarifranap($idpendaftaran);
        $data['ttd'] = 'Yogyakarta, ' . date('d/m/Y');
        echo json_encode($data);
    }

    //invoice versi gabungprint_invoiceGabung
    public function cetak_invoiceGabung()
    {
        $idpendaftaran  = $this->input->post("p");
        $jenis    = $this->input->post("j");
        $modelist = $this->input->post("i");
        $data['lamaRanap']     = $this->db->query('SELECT * FROM rs_inap_biayanonpemeriksaan WHERE idpendaftaran=' . $idpendaftaran . ' AND idjenistarif=19')->num_rows(); // Andri - bugfix [0000590]
        // $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran,'ranap');
        $data['infotagihan']   = $this->mviewql->viewinfotagihanVersiCLAIM($idpendaftaran);
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran', ['idpendaftaran' => $idpendaftaran])->row_array();
        // $data['listtagihan']   = $this->mviewql->view_tarif_byjenistarifranap($idpendaftaran);
        $data['listtagihan'] = $this->mviewql->viewtotaltarifpemeriksaanCLAIM($idpendaftaran, $modelist, $jenis);
        $data['ttd'] = 'Yogyakarta, ' . date('d/m/Y');
        echo json_encode($data);
    }

    //detail versi gabung
    public function cetak_excel_VersiClaim_getData()
    {
        $idpendaftaran  = $this->input->post("p");
        $jenis    = $this->input->post("j");
        $modelist = $this->input->post("i");
        $data['infotagihan']   = $this->mviewql->viewinfotagihanVersiCLAIM($idpendaftaran);
        $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaanCLAIM($idpendaftaran, $modelist, $jenis);
        echo json_encode($data);

        // $idpendaftaran  = $this->input->post("p");
        // $data['infotagihan']   = $this->mviewql->viewinfotagihanVersiCLAIM($idpendaftaran);
        // $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran',['idpendaftaran'=>$idpendaftaran])->row_array();
        // // $data['detail']        = $this->mviewql->viewdetailranap($idpendaftaran);
        // // $data['detail']        = $this->mviewql->viewdetailrajalranap($idpendaftaran);        
        // $data['detail']        = $this->mviewql->viewdetailtarifralan($idpendaftaran); 
        // $data['detail']        = $this->mviewql->viewtotaltarifpemeriksaanCLAIM($idpendaftaran); 
        // echo json_encode($data);
    }

    //cetak excel
    public function cetak_excel_langsung()
    {
        $idpendaftaran  = $this->input->post("p");
        // $jenis    = $this->input->post("j");
        // $modelist = $this->input->post("i");
        $listkasir = $this->input->post("listkasir");
        $title = $this->input->post("title");
        // $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran);
        // $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($idpendaftaran,$modelist,$jenis);
        // echo json_encode($data);     
        $data['fileName']     = $title . ' - ' . $idpendaftaran;
        $data['listkasir']     = $listkasir;
        $this->load->view('pelayanan/excel_cetak_langsung', $data);
    }

    public function cetak_excel_langsung_getData()
    {
        $idpendaftaran  = $this->input->post("p");
        $jenis    = $this->input->post("j");
        $modelist = $this->input->post("i");
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran);
        $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($idpendaftaran, $modelist, $jenis);
        echo json_encode($data);
        // $this->load->view('pelayanan/excel_cetak_langsung',$data);
    }

    //old cetal kasir
    public function cetak_langsung()
    {
        $idpendaftaran  = $this->input->post("p");
        $jenis    = $this->input->post("j");
        $modelist = $this->input->post("i");
        $data['infotagihan']   = $this->mviewql->viewinfotagihan($idpendaftaran);
        $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($idpendaftaran, $modelist, $jenis);
        echo json_encode($data);
    }

    public function cetak_langsung_rajal()
    {
        $idpendaftaran  = $this->input->post("p");
        echo json_encode(["infotagihan" => $this->mviewql->viewinfotagihan($idpendaftaran), "detailtagihan" => $this->mviewql->viewtotaltarifpemeriksaan($idpendaftaran, 0, 'rajal')]);
    }

    public function cetak_langsung_lengkap()
    {
        $idpendaftaran  = $this->input->post("p");
        echo json_encode(["infotagihan" => $this->mviewql->viewinfotagihan($idpendaftaran), "detailtagihan" => $this->mviewql->viewtotaltarifpemeriksaanlengkap($idpendaftaran)]);
    }

    public function cetak_rekap_inap()
    {
        $idinap = $this->input->post("in");
        $idpendaftaran = $this->input->post("idp");
        echo json_encode(["infotagihan" => $this->mviewql->viewinfotagihan($idpendaftaran), "detailranap" => $this->mkombin->ranap_rekap($idinap)]);
    }

    // mahmud, clear
    public function list_riwayat_sattperiksa()
    {
        $norm = $this->db->query("select norm from rs_pemeriksaan where idpemeriksaan='" . $this->input->post('idperiksa') . "'")->row_array()['norm'];
        $idpemeriksaan = $this->db->query("select idpemeriksaan, status from rs_pemeriksaan where norm='$norm' and status='selesai'  order by idpemeriksaan desc limit 1")->row_array()['idpemeriksaan'];

        if (empty($idpemeriksaan)) {
            echo json_encode(['status' => 'empty']);
            return;
        }
        //mahmud, clear
        $periksa = $this->db->query("select ifnull(p.diagnosa,' ') as diagnosa, ifnull(pp.keteranganobat,' ') as keteranganobat, pp.waktu from rs_pemeriksaan p, person_pendaftaran pp where p.idpemeriksaan='" . $idpemeriksaan . "' and pp.idpendaftaran = p.idpendaftaran and  p.status='selesai' ORDER by idpemeriksaan desc limit 1")->row_array();
        // list diagnosa
        $diagnosa = $this->db->query("select ri.icd, ri.namaicd, ri.aliasicd from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpemeriksaan='" . $idpemeriksaan . "' and rj.jenisicd='diagnosa' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
        // list pemakaian obat
        $obat = $this->db->query("SELECT b.idbarang,b.namabarang,rb.kekuatan,rb.jumlahpemakaian,s.namasatuan, b.hargaumum as hargajual  FROM rs_barangpemeriksaan rb, rs_barang b, rs_satuan s WHERE rb.idpemeriksaan='$idpemeriksaan' and b.idbarang = rb.idbarang and s.idsatuan = b.idsatuan")->result();
        echo json_encode(['periksa' => $periksa, 'obat' => $obat, 'diagnosa' => $diagnosa, 'status' => 'ok']);
    }
    public function pemeriksaan_salinbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)) {
            $norm = $this->db->query("select norm from rs_pemeriksaan where idpemeriksaan='" . $this->input->post('i') . "'")->row_array()['norm'];
            // $riwayatidperiksa = $this->db->query("select idpemeriksaan from rs_pemeriksaan where norm='$norm' and status='selesai'  order by idpemeriksaan desc limit 1")->row_array()['idpemeriksaan'];
            $idperiksa = $this->input->post('i');
            $idbarang = $this->input->post('x');
            $idpendaftaran = $this->input->post('p');
            $detail_bhp = $this->db->query("select * from rs_barangpemeriksaan where idbarang='" . $idbarang . "' limit 1")->row_array();
            if (!empty($detail_bhp)) {
                $cek_rs_barangpemeriksaan = $this->mkombin->pemeriksaan_cek_rs_barangpemeriksaan($idbarang, $idpendaftaran, 'rajal');
                $data = ['idpemeriksaan' => $idperiksa, 'idbarang' => $detail_bhp['idbarang'], 'jumlahpemakaian' => $detail_bhp['jumlahpemakaian'], 'idjenistarif' => $detail_bhp['idjenistarif'], 'harga' => $detail_bhp['harga'], 'kekuatan' => $detail_bhp['kekuatan'], 'idpendaftaran' => $idpendaftaran];
                if (!empty($cek_rs_barangpemeriksaan)) {
                    $arrMsg =  $this->db->update("rs_barangpemeriksaan", $data, ['idpemeriksaan' => $idperiksa, 'idbarang' => $idbarang]);
                } else {
                    $arrMsg =  $this->db->insert("rs_barangpemeriksaan", $data);
                }
                pesan_success_danger($arrMsg, 'Salin Obat/BHP Berhasil.!', 'Salin Obat/BHP Gagal.!', 'js');
            } else {
                pesan_danger("Riwayat Obat/BHP Tidak Ditemukan..!", "js");
            }
        }
    }

    // pemeriksan cetak aturan pakai obat
    public function pemeriksaan_cetak_aturanpakai()
    {
        $data['aturan'] = $this->db->query("select DATE_FORMAT(ppp.tanggallahir, '%d/%m/%Y') as tanggallahir, ppp.tempatlahir, rb.grup, DATE_FORMAT(rp.waktu, '%d/%m/%Y') as tglperiksa, rb.penggunaan, concat(ifnull(ppp.identitas,''),' ',ppp.namalengkap)as namalengkap, p.norm, (select b.namabarang from rs_barang b WHERE b.idbarang='" . $this->input->post('idb') . "') as namaobat, vba.aturanpakai from person_pendaftaran p, person_pasien pp, person_person ppp,rs_barangpemeriksaan rb, vrs_barang_aturanpakai vba, rs_pemeriksaan rp where p.idpendaftaran='" . $this->input->post('idp') . "' and pp.norm = p.norm and ppp.idperson = pp.idperson and rp.idpendaftaran = p.idpendaftaran and rb.idbarang='" . $this->input->post('idb') . "' and rb.idpendaftaran='" . $this->input->post('idp') . "' and vba.idbarangaturanpakai = rb.idbarangaturanpakai")->row_array();
        $data['header'] = $this->mkombin->getKonfigurasi('headeraturanpakai');
        $data['apoteker'] = $this->mkombin->getKonfigurasi('aptname');
        echo json_encode($data);
    }
    // pemeriksaan cetak resep/racikan obat/bhp
    public function pemeriksaan_cetak_resepobatbhp()
    {
        $post = $this->input->post();
        $modelist      = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $data['pasien'] = $this->db->get_where('vrs_identitas_periksa_pasien', ['idpendaftaran' => $this->input->post('daftar')])->row_array();
        $data['resep'] = $this->mkombin->pemeriksaan_listbhp($this->input->post('daftar'), $modelist, $this->input->post('jenisrawat'));
        $data['grup']  = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  FROM rs_barangpemeriksaan b WHERE b.idpendaftaran='" . $this->input->post('daftar') . "' GROUP by grup")->result();
        $data['header'] = $this->mkombin->getKonfigurasi('headerresep');
        $data['sipa'] = $this->mkombin->getKonfigurasi('aptsipa');
        $data['apoteker']    = $this->mkombin->getKonfigurasi('aptname');
        $data['noteresep']   = $this->mkombin->getKonfigurasi('noteresep');
        echo json_encode($data);
    }

    // elektromedik
    public function elektromedik_getdtexpertise()
    {
        echo json_encode($this->db->query("select keteranganradiologi from person_pendaftaran where idpendaftaran='" . $this->input->post('id') . "'")->row_array()['keteranganradiologi']);
    }

    // get tanggal riwayat periksa
    public function get_riwayat_tglperiksa()
    {
        echo json_encode($this->db->query("select DATE_FORMAT(p.waktuperiksa,'%d/%m/%y') as waktuperiksa, p.idpendaftaran from person_pendaftaran p , rs_pemeriksaan rp  WHERE p.norm='" . $this->input->post('norm') . "' and rp.idpendaftaran = p.idpendaftaran and rp.status='selesai' GROUP by p.idpendaftaran DESC")->result());
    }
    // ambil data riwayat periksa
    public function get_riwayatperiksa()
    {
        $post = $this->input->post();
        $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '');
        $idpendaftaran = $this->input->post('id');
        $identitas = $this->db->query("select pp.keteranganobat, pp.keteranganlaboratorium, pp.keteranganradiologi, rp.anamnesa,rp.diagnosa, rp.keterangan, date(rp.waktu) as tanggalperiksa, rp.idpemeriksaan, rp.idunit, rp.idpegawaidokter, ru.namaunit, namadokter(rp.idpegawaidokter) as dokter
        from person_pendaftaran pp 
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran
        join rs_unit ru on ru.idunit = rp.idunit
        where pp.idpendaftaran = '" . $idpendaftaran . "'")->result();
        echo json_encode([
            'identitas' => $identitas,
            'vitalsign' => $this->riwayat_hasilperiksa('vital sign', ':', ', '),
            'radiologi' => $this->riwayat_hasilperiksa('radiologi', '', '<br>'),
            'laboratorium' => $this->riwayat_hasilperiksa('laboratorium', ':', ', '),
            'diagnosa' => $this->riwayat_hasilperiksa('diagnosa', '', ''),
            'tindakan' => $this->riwayat_hasilperiksa('tindakan', '', '<br> '),
            'obat' => $this->mkombin->pemeriksaan_listbhp($this->input->post('id'), $modelist),
            'grup' => $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  FROM rs_barangpemeriksaan b 
            WHERE b.idpendaftaran='" . $this->input->post('id') . "' GROUP by grup")->result()
        ]);
    }
    public function riwayat_hasilperiksa($jenisicd, $pemisahhasil, $pemisahicd)
    {
        return $this->db->query("select ri.icd, concat( ri.namaicd,'" . $pemisahhasil . " ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan,'" . $pemisahicd . "' ) as hasil_diagnosis, rh.icdlevel from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, person_pendaftaran pp, rs_pemeriksaan rp where rh.idpendaftaran='" . $this->input->post('id') . "' and rj.jenisicd='" . $jenisicd . "' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and pp.idpendaftaran = rh.idpendaftaran and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
    }

    public function notiffarmasi()
    {
        echo json_encode($this->db->query("SELECT count(1) as jumlah FROM person_pendaftaran p left join rs_barangpemeriksaan bp on p.idpendaftaran=bp.idpendaftaran left join rs_barang b on b.idbarang=bp.idbarang left join rs_pemeriksaan rp on rp.idpendaftaran=p.idpendaftaran and rp.status='sedang ditangani' where date(rp.waktu)=date(now()) and not isnull(keteranganobat) and trim(keteranganobat)<>'' and isnull(idbarangpemeriksaan) and (jenis='obat' or isnull(jenis)) and rp.status='sedang ditangani'")->result());
    }

    public function cekgolonganicdkns()
    {
        $sql = $this->db->query("select ri.icd from rs_hasilpemeriksaan rh, rs_icd ri  where idpemeriksaan='" . $this->input->post('i') . "'  and ri.icd=rh.icd and ri.golonganicd='KNS'");
        echo json_encode((($sql->num_rows() > 0) ? true : false));
    }

    //mahmud, clear
    public function jadwaloperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JADWALOPERASI)) //lihat define di atas
        {

            $data = [
                'content_view'      => 'pelayanan/v_pemeriksaanoperasi',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'operasi',
                'active_menu_level' => '',
                'plugins'           => [PLUG_DATATABLE, PLUG_DATE, PLUG_DROPDOWN]
            ];
            $data['title_page']     = 'Jadwal Operasi Pasien';
            $data['mode']           = 'view';
            $data['script_js']      = ['js_jadwaloperasi'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    //mahmud, clear
    public function listpo() //List Pasien Operasi
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_operasi_(true);
        $data = [];
        $total = 0;
        if ($getData) {

            $dataArr = $getData->result_array();
            //sort array desc by waktuoperasi
            function sortFunction($a, $b)
            {
                return strtotime($b["waktuoperasi"]) - strtotime($a["waktuoperasi"]);
            }
            usort($dataArr, "sortFunction");
            //end sort array

            foreach ($dataArr as $arr) {
                $registrationDate = date("Y-m-d", strtotime(date($arr['waktuperiksa'])));
                $row = [];
                $profilepasien = 'No.RM : ' . $arr['norm'] . '<br>Nama Pasien : ' . $arr['identitas'] . ' ' . $arr['namalengkap'] . '<br>Usia/Lahir : ' . $arr['usia'] . ' / ' . $arr['tanggallahir'] . '<br>Alamat : ' . $arr['alamat'];

                $menu  = ' <a onclick="cetak_laporan_operasi(' . $arr['idjadwaloperasi'] . ')" ' . ql_tooltip("Laporan Operasi") . ' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                $menu .= ' <a onclick="cetak_laporan_penggunaanobatbhp_operasi(' . $arr['idjadwaloperasi'] . ')" ' . ql_tooltip("Penggunaan Obat/BHP Operasi") . ' class="btn btn-xs btn-primary"><i class="fa fa-toggle-on"></i></a>';
                $menu .= (($arr['terlaksana'] != 1 && empty(!$arr['idoperasi'])) ? ' <a id="terlaksana" idjop="' . $arr['idjadwaloperasi'] . '" ur="setterlaksanaop" href="javascript:void(0);" ' . ql_tooltip("Terlaksana") . ' class="btn btn-xs btn-primary"><i class="fa fa-check"></i></a>' : '');
                $menu .= (($arr['terlaksana'] != 1) ? ' <a id="detail" href="javascript:void(0);" modeop="' . ((empty($arr['idoperasi'])) ? 'insert' : 'update') . '" idp="' . $arr['idpendaftaran'] . '" idop="' . $arr['idoperasi'] . '" profilepasien="' . $profilepasien . '" idjop="' . $arr['idjadwaloperasi'] . '" ' . ql_tooltip("Operasi") . ' class="btn btn-xs btn-warning"><i class="fa fa-heartbeat"></i></a> ' : '');

                $menu .= (($arr['terlaksana'] == 0) ? ' <a id="batal" idjop="' . $arr['idjadwaloperasi'] . '" ur="setbatalop" href="javascript:void(0);" ' . ql_tooltip("Batalkan Jadwal") . ' class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></a>' : '');
                $menu .= (($arr['terlaksana'] != 1) ? ' <a id="hapus" idjop="' . $arr['idjadwaloperasi'] . '" ur="sethapusop" href="javascript:void(0);" ' . ql_tooltip("Hapus Jadwal") . ' class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>' : '');
                $menu .= (($arr['terlaksana'] == 1 && empty(!$arr['idoperasi'])) ? ' <a id="batalselesai" idjop="' . $arr['idjadwaloperasi'] . '" ur="setbatalselesai" href="javascript:void(0);" ' . ql_tooltip("Batal Selesai") . ' class="btn btn-xs btn-warning"><i class="fa fa-minus-circle"></i></a>  <a id="indikatormutu" idjop="' . $arr['idjadwaloperasi'] . '" href="javascript:void(0);" ' . ql_tooltip("Indikator Mutu Kamar Bedah") . ' class="btn btn-xs btn-success"><i class="fa fa-dot-circle-o"></i></a>' : '');

                $menuverif = "";
                if ($arr['terlaksana'] == 1 and empty(!$arr['idoperasi']) and ($arr['jaminan'] == 'jknpbi' or $arr['jaminan'] == 'jknnonpbi') and $arr['verifjkn'] == 0) {
                    $menuverif = ' <a profil="' . $profilepasien . '" id="verif_tarifjkn" idoperasi="' . $arr['idoperasi'] . '" idjadwaloperasi="' . $arr['idjadwaloperasi'] . '" idpendaftaran="' . $arr['idpendaftaran'] . '" class="btn btn-xs btn-warning" ' . ql_tooltip('Verif Tarif JKN') . '><i class="fa fa-money"></i></a>';
                }

                $row[] = $arr['waktuoperasi'];
                $row[] = $arr['kodebooking'];
                $row[] = $arr['dokter'];
                $row[] = $arr['namaunit'];
                $row[] = $arr['jenistindakan'];
                $row[] = $arr['jaminan'] . $menuverif;
                $row[] = $arr['norm'];
                $row[] = $arr['identitas'] . ' ' . $arr['namalengkap'];
                $row[] = statusoperasi($arr['terlaksana']);
                $row[] = $menu;
                $row[] = $arr['jenispemeriksaan'] . ' --> ' . $registrationDate;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->totdt_operasi(),
            "recordsFiltered" => $this->mdatatable->filterdt_operasi(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    //mahmud, clear
    public function setterlaksanaop()
    {
        $arrMsg = $this->db->update('rs_jadwal_operasi', ['terlaksana' => '1'], ['idjadwaloperasi' => $this->input->post('id')]);
        $arrMsg = $this->db->update('rs_operasi', ['statusoperasi' => 'selesai'], ['idjadwaloperasi' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, "Ubah terlaksana berhasil.!", "Ubah terlaksana tidak berhasil.!", "js");
    }
    public function setbatalop()
    {
        $arrMsg = $this->db->update('rs_jadwal_operasi', ['terlaksana' => '2'], ['idjadwaloperasi' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, "Ubah batal berhasil.!", "Ubah batal tidak berhasil.!", "js");
    }

    public function setbatalselesai()
    {
        $arrMsg = $this->db->update('rs_jadwal_operasi', ['terlaksana' => '4'], ['idjadwaloperasi' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, "Ubah batal berhasil.!", "Ubah batal tidak berhasil.!", "js");
    }

    public function sethapusop()
    {

        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $arrMsg = $this->db->update('rs_jadwal_operasi', ['terlaksana' => '3', 'iduserhapus' => $iduser], ['idjadwaloperasi' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, "Hapus Jadwal berhasil.!", "Hapus Jadwal tidak berhasil.!", "js");
    }
    /**
     * Verifikasi Tarif Operasi Pasien JKN
     */

    public function onContentReady_FormVerif()
    {
        $data['tarifsc']       = $this->db->select('a.*, (select kelas from rs_kelas where idkelas = a.idkelas) as namakelas')->get('rs_mastertarif_operasi_bpjssc a')->result_array();
        $data['tarifnonsc']    = $this->db->get('rs_mastertarif_operasi_bpjsnonsc')->result_array();
        $data['tarifortopedi'] = $this->db->get('rs_mastertarif_operasi_bpjsorthopedi')->row_array();
        echo json_encode($data);
    }

    //save verif tarif operasi
    public function save_verifikasi()
    {
        $data['idoperasi']     = $this->input->post('idoperasi');
        $data['idpendaftaran'] = $this->input->post('idpendaftaran');
        $data['idjenistarif']  = $this->input->post('idjenistarif');
        $data['keterangan']    = $this->input->post('keterangan');
        $data['nominal']       = unconvertToRupiah($this->input->post('nominal_tarif'));
        $save = $this->db->insert('rs_operasi_tarifoperasi_bpjs', $data);

        $idjadwaloperasi = $this->input->post('idjadwaloperasi');
        if ($save) {
            $this->db->update('rs_jadwal_operasi', ['verifjkn' => 1], ['idjadwaloperasi' => $idjadwaloperasi]);
        }

        pesan_success_danger($save, 'Verif Tarif Operasi Berhasil.', 'Verif Tarif Operasi Gagal.', 'js');
    }

    /**
     * Halaman Operasi
     */
    public function pemeriksaanoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANOPERASI)) //lihat define di atas
        {

            $data = [
                'content_view'      => 'pelayanan/v_pemeriksaanoperasi',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'operasi',
                'active_menu_level' => '',
                'plugins'           => [PLUG_DATE, PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_DATATABLE, PLUG_TIME]
            ];
            $data['title_page']     = 'Pemeriksaan Operasi';
            $data['mode']           = 'operasi';
            $data['script_js']      = ['js_pemeriksaanoperasi'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    /**
     * Mahmud, clear
     * Ketika Halaman Pemeriksaan Operasi Diakses
     */
    public function formready_pemeriksaanoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANOPERASI)) //lihat define di atas
        {
            $data['jenisoperasi'] = $this->db->select('idjenisoperasi as id, jenisoperasi as txt')->get('rs_jenisoperasi')->result_array();
            $data['jaminan'] = $this->db->select('jaminan as id, jaminan as txt')->get('rs_jaminan')->result_array();
            $data['tarifoperasi'] = $this->db->select('idjenistarifoperasi as id, jenistarifoperasi as txt')->get('rs_jenistarif_operasi')->result_array();
            $data['tariftindakan'] = array(['id' => 'tunggal', 'txt' => 'Tunggal'], ['id' => 'ganda', 'txt' => 'Ganda'], ['id' => 'multiple', 'txt' => 'Multiple']);
            $data['biayaoperasi'] = $this->db->select('idjenistarifoperasi, jenistindakan')->get_where('rs_operasi_tarifoperasi', ['idoperasi' => $this->input->post('idop')])->row_array();
            $data['jenisanestesi'] = $this->db->select('idjenisanestesi as id, jenisanestesi as txt')->get('rs_jenisanestesi')->result_array();
            $data['macamoperasi'] = $this->db->select('idmacamoperasi as id, macamoperasi as txt')->get('rs_operasi_macamoperasi')->result_array();;
            $data['operasi'] = $this->db->select('ro.*, namadokter(ro.idpegawaidokteranestesi) as dokteranestesi, namadokter(ro.idpegawaidokteranak) as dokteranak, namadokter(ro.idpegawaipenata) as penata, namadokter(ro.idpegawaiinstrumen) as instrumen, namadokter(ro.idpegawaisirkuler) as sirkuler,namadokter(ro.idpegawaipenerimabayi) as penerimabayi, namadokter(ro.idpegawaiasisten) as asisten, pper.agama, pper.jeniskelamin, rjo.terlaksana, pp.norm, namadokter(rjo.iddokteroperator) as dokteroperator,pper.alamat, concat(ifnull(pper.identitas,"")," ",ifnull(pper.namalengkap,"")) as namalengkap, namaunit(rp.idunit)namaunit,rjio.jenistindakan, rjo.waktuoperasi, rjo.kodebooking, usia(pper.tanggallahir) as usia, pper.tanggallahir, pper.alamat, ppas.nojkn, rjo.jaminan, rjo.iddokteroperator')
                ->join('rs_jadwal_operasi rjo', 'rjo.idjadwaloperasi=ro.idjadwaloperasi')
                ->join('person_pendaftaran pp', 'pp.idpendaftaran=rjo.idpendaftaran')
                ->join('rs_pemeriksaan rp', 'rp.idpemeriksaan=rjo.idpemeriksaan')
                ->join('person_pasien ppas', 'ppas.norm=pp.norm')
                ->join('person_person pper', 'pper.idperson=ppas.idperson')
                ->join('rs_jenistindakanoperasi rjio', 'rjio.idjenistindakan=rjo.idjenistindakan')
                ->where('ro.idjadwaloperasi', $this->input->post('idjop'))
                ->where('ro.idoperasi', $this->input->post('idop'))
                ->get('rs_operasi ro')->row_array();
            echo json_encode($data);
        } else {
            aksesditolak();
        }
    }
    //mahmud, clear
    /**--Simpan Tindakan dan Diagnosa Operasi--**/
    public  function saveoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANOPERASI)) //lihat define di atas
        {
            $post          = $this->input->post();

            # ikhsan
            # done - cek validasi instrument pegawai
            if ($post['idpegawaiinstrumen'] == 0) {
                pesan_success_danger(false, "Simpan Data Operasi Berhasil.", "Mohon Pilih Perawat Instrumen !!!");
                redirect(base_url('cpelayanan/pemeriksaanoperasi'));
            } else {


                $idoperasi     = $post['idoperasi'];
                $tarifoperasi  = $post['tarifoperasi'];
                $tariftindakan = $post['tariftindakan'];
                $idpendaftaran = $post['idpendaftaran'];
                $jenisoperasi  = ((empty($post['jenisoperasi'])) ? '' : implode(',', $post['jenisoperasi']));
                $jenisanestesi = ((empty($post['jenisanestesi'])) ? '' : implode(',', $post['jenisanestesi']));

                //insert update tarif operasi
                $dttarifoperasi = [
                    'idoperasi'     => $idoperasi,
                    'idpendaftaran' => $idpendaftaran,
                    'idjenistarifoperasi' => $tarifoperasi,
                    'jenistindakan' => $tariftindakan
                ];
                $save = $this->db->replace('rs_operasi_tarifoperasi', $dttarifoperasi);


                //update operasi
                $data = [
                    'idpegawaidokteranak'     => $post['idpegawaidokteranak'],
                    'idpegawaiasisten'        => $post['idpegawaiasisten'],
                    'idpegawaiinstrumen'      => $post['idpegawaiinstrumen'],
                    'jenisoperasi'            => $jenisoperasi,
                    'idmacamoperasi'          => $post['idmacamoperasi'],
                    'idpegawaidokteranestesi' => $post['idpegawaidokteranestesi'],
                    'idpegawaipenata'         => $post['idpegawaipenata'],
                    'idpegawaisirkuler'       => $post['idpegawaisirkuler'],
                    'idpegawaipenerimabayi'   => $post['idpegawaipenerimabayi'],
                    'jenisanestesi'           => $jenisanestesi,
                    'obatanestesi'            => $post['obatanestesi'],
                    'resikotinggi'            => $post['resikotinggi'],
                    'isoperasisc'             => $post['isoperasisc'],
                    'jumlahperdarahan'        => $post['jumlahperdarahan'],
                    'sitologi'                => $post['sitologi'],
                    'patologianatomi'         => $post['patologianatomi'],
                    'asaljaringan'            => $post['asaljaringan'],
                    'pemasanganimplant'       => $post['pemasanganimplant'],
                    'uraianpembedahan'        => $post['uraianpembedahan'],
                    'diagnosapraoperasi'      => $post['diagnosapraoperasi'],
                    'diagnosapascaoperasi'    => $post['diagnosapascaoperasi'],
                    'tindakanoperasi'         => $post['tindakanoperasi'],
                    'jenistindakan'           => $post['jenistindakan'],
                    'tanggalmulai'            => $post['tanggalmulai'],
                    'tanggalselesai'          => $post['tanggalselesai'],
                    'jammulai'                => $post['jammulai'],
                    'jamselesai'              => $post['jamselesai']
                ];
                $save = $this->db->update('rs_operasi', $data, ['idoperasi' => $idoperasi]);

                $save = $this->db->update('rs_jadwal_operasi', ['iddokteroperator' => $post['iddokteroperator'], 'jaminan' => $post['jaminan'], 'terlaksana' => 4], ['idjadwaloperasi' => $post['idjadwaloperasi']]);


                pesan_success_danger($save, "Simpan Data Operasi Berhasil.", "Simpan Data Operasi Gagal.");
                redirect(base_url('cpelayanan/jadwaloperasi'));
            } // idpegawai instrument
        } else {
            aksesditolak();
        }
    }

    /**
     * Simpan Indikator Mutu Kamar Bedah
     */

    public function save_indikatormutukamarbedah()
    {
        $data['asesment_pra_anestesi']                          = $this->input->post('asesment_pra_anestesi');
        $data['kelengkapan_monitoring_pemulihan_psca_anestesi'] = $this->input->post('kelengkapan_monitoring_pemulihan_psca_anestesi');
        $data['konversi_anestesi_dari_lokalregionalkega']       = $this->input->post('konversi_anestesi_dari_lokalregionalkega');
        $data['asesmen_pra_bedah']                              = $this->input->post('asesmen_pra_bedah');
        $data['marking']                                        = $this->input->post('marking');
        $data['kelengkapan_scc']                                = $this->input->post('kelengkapan_scc');
        $data['diskrepasi_diagnosa_pre_dan_post']               = $this->input->post('diskrepasi_diagnosa_pre_dan_post');
        $data['kelengkapan_lap_anestesi']                       = $this->input->post('kelengkapan_lap_anestesi');

        $where['idjadwaloperasi'] = $this->input->post('idjadwaloperasi');
        $arrMsg = $this->db->update('rs_operasi', $data, $where);
        pesan_success_danger($arrMsg, 'Simpapn Indikator Mutu Berhasil.', 'Simpapn Indikator Mutu Gagal.', 'js');
    }

    public function formready_indikatormutukamarbedah()
    {
        $dt = $this->db->get_where('rs_operasi', ['idjadwaloperasi' => $this->input->post('idjadwaloperasi')])->row_array();
        echo json_encode($dt);
    }

    /**
     * List data operasi sewa alat
     */
    public function dt_operasisewaalat()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_operasisewaalat_(true);
        $data = [];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->namaalat;
                $row[] = $obj->alias;
                $row[] = (($obj->status == 'sewa') ? '<input id="setjumlah_sewaalat" alt="' . $obj->idoperasisewaalat . '" type="text" value="' . $obj->jumlah . '" size="3" /> ' : '');
                $row[] = 'Rp <input id="setharga_sewaalat" type="text" alt="' . $obj->idoperasisewaalat . '" alt2="' . $obj->idsewaalat . '" value="' . convertToRupiah($obj->harga) . '" size="8" /> ';
                $row[] = convertToRupiah($obj->jumlah * $obj->harga);
                $row[] = (($obj->status == 'sewa') ? '<a class="btn btn-xs btn-success">' . $obj->status . '</a>' : $obj->status);
                $row[] = (($obj->status == 'sewa') ? '<a id="batal_sewaalat" alt="' . $obj->idoperasisewaalat . '" class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i> Batal</a>' : '<a id="tambah_sewaalat" alt="' . $obj->idsewaalat . '" alt2="' . $obj->harga . '" class="btn btn-xs btn-primary"><i class="fa fa-plus-square"></i> Sewa</a>');
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_operasisewaalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_operasisewaalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    /**
     * List data operasi barangpemeriksaan
     */
    public function dt_operasibarangpemeriksaan()
    {
        $idunit = $this->session->userdata('idunitterpilih');
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_operasibarangpemeriksaan_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $menu  = '';
                if (empty(!$idunit)) {
                    if ($obj->selesai == 0) {
                        $menu .= ' <a id="pemberian_obat" idbarang="' . $obj->idbarang . '" namaobat = "' . $obj->kode . ' ' . $obj->namabarang . '" ' .  ql_tooltip('Pemberian Obat') . '  alt="' . $obj->idbarangoperasi . '" class="btn btn-success btn-xs"><i class="fa fa-plus-square"></i></a>';
                        $menu .= ' <a id="selesaipemberian_obat" alt2 = "' . $obj->kode . ' ' . $obj->namabarang . '" ' . ql_tooltip('Selesai Pemberian') . ' alt="' . $obj->idbarangoperasi . '" class="btn btn-primary btn-xs"><i class="fa fa-check-circle-o"></i></a>';
                    } else {
                        $menu .= ' <a id="inputulangpemberian_obat" alt2 = "' . $obj->kode . ' ' . $obj->namabarang . '" ' . ql_tooltip('Input Ulang Pemberian') . ' alt="' . $obj->idbarangoperasi . '" class="btn btn-warning btn-xs"><i class="fa fa-folder-open"></i></a>';
                    }
                }
                $menu .= (($obj->selesai == 0) ? ' <a id="hapus_obat" barang="' . $obj->kode . ' ' . $obj->namabarang . '" alt="' . $obj->idbarangoperasi . '" ' .  ql_tooltip('Hapus') . ' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>' : '');

                $row[] = ++$no;
                $row[] = $obj->kode . ' ' . $obj->namabarang;
                $row[] = ((empty($idunit)) ? '<input id="setjumlah_obat" alt="' . $obj->idbarangoperasi . '" value="' . $obj->jumlahpemakaian . '" size="5" />' : $obj->jumlahpemakaian);
                $row[] = convertToRupiah($obj->harga);
                $row[] = $obj->namasatuan;
                $row[] = convertToRupiah($obj->total);
                $row[] = $menu;

                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_operasibarangpemeriksaan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_operasibarangpemeriksaan(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    //menampilkan detail barang pemeriksaan operasi
    public function get_operasi_barangpemeriksaan_detail()
    {
        $idbp      = $this->input->post('idbp');
        $idunit   = $this->session->userdata('idunitterpilih');
        $idbarang = $this->input->post('idbarang');
        $query = "SELECT c.idunit, ifnull(a.jumlah,'0') as diberikan,b.idbarangpembelian, b.batchno, b.kadaluarsa, c.stok, b.idbarang
            FROM rs_barang_pembelian b
            join rs_barang_stok c on c.idbarangpembelian = b.idbarangpembelian and c.idunit = '" . $idunit . "'
            left join rs_operasi_barangpemeriksaan_detail a on a.idbarangpembelian = b.idbarangpembelian and a.idbarangoperasi = '" . $idbp . "'
            WHERE  b.idbarang = '" . $idbarang . "' and c.idunit = '" . $idunit . "' ";
        $result = $this->db->query($query)->result_array();
        echo json_encode($result);
    }
    //detail pemberian obat operasi
    public function detailpemberianobatoperasi()
    {
        $data['idbarangoperasi']     = $this->input->post('idbp');
        $data['idbarangpembelian']   = $this->input->post('idp');
        $data['idunit']              = $this->session->userdata('idunitterpilih');
        $data['jumlah']              = $this->input->post('jumlah');
        $arrMsg = $this->db->replace('rs_operasi_barangpemeriksaan_detail', $data);

        $diberikan = $this->db->query("select sum(jumlah) as jumlah from rs_operasi_barangpemeriksaan_detail where idbarangoperasi='" . $this->input->post('idbp') . "'")->row_array()['jumlah'];
        $arrMsg    = $this->db->update('rs_operasi_barangpemeriksaan', ['jumlahpemakaian' => $diberikan], ['idbarangoperasi' => $this->input->post('idbp')]);
        pesan_success_danger($arrMsg, 'Pemberian Obat Berhasil.', 'Pemberian Obat Gagal.', 'js');
    }

    //
    public function setdistribusibarangobatoperasi()
    {
        $idbarangoperasi = $this->input->post('i');
        $jenisdistribusi = $this->input->post('s');
        $arrMsg = $this->distribusibarangobatoperasi($idbarangoperasi, $jenisdistribusi);
        pesan_success_danger($arrMsg, 'Pemberian Obat Operasi Berhasil', 'Pemberian Obat Operasi Gagal', 'js');
    }
    //simpan barangpemeriksaan
    public function operasi_save_barangpemeriksaan()
    {
        $idbarang        = $this->input->post('idbarang');
        $mode            = $this->input->post("mode");
        $jumlah          = $this->input->post("jumlah");
        $idbarangoperasi = $this->input->post('idbarangoperasi');
        $idoperasi       = $this->input->post("idoperasi");
        $jaminan         = $this->input->post("jaminan");
        $idpendaftaran   = $this->input->post("idpendaftaran");
        $idunit          = $this->session->userdata('idunitterpilih');
        if ($mode == 'tambah') {
            //cek barang apakah sudah diinput
            $barang = $this->db->query("select idbarang from rs_operasi_barangpemeriksaan where idoperasi='" . $idoperasi . "' and idbarang='" . $idbarang . "'");
            if ($barang->num_rows() > 0) {
                echo json_encode(['status' => 'warning', 'message' => 'Obat/BHP Sudah Ada.']);
                return;
            }

            if ($jaminan == 'jknpbi' or $jaminan == 'jknnonpbi' or $jaminan == 'bpjstenagakerja') {
                $barang = $this->db->query("select hargajual, idjenistarif from rs_barang where idbarang='" . $idbarang . "'");
            } else {
                $barang = $this->db->query("select hargaumum as hargajual, idjenistarif from rs_barang where idbarang='" . $idbarang . "'");
            }


            if ($barang->num_rows() > 0) {
                $setbarang = $barang->row_array();
                $pesansuccess = 'Tambah Obat / BHP Berhasil.';
                $pesandanger  = 'Tambah Obat / BHP Gagal.';
                $dtinsert     = ['idbarang' => $idbarang, 'idoperasi' => $idoperasi, 'idpendaftaran' => $idpendaftaran, 'harga' => $setbarang['hargajual'], 'idjenistarif' => $setbarang['idjenistarif']];
                $dtinsert['jumlahpemakaian'] = ((empty($idunit)) ? 1 : 0);
                $arrMsg  = $this->db->insert('rs_operasi_barangpemeriksaan', $dtinsert);
            } else {
                echo json_encode(['status' => 'warning', 'message' => 'Obat/BHP Tidak Ada.']);
                return;
            }
        } else if ($mode == 'ubah') {
            $pesansuccess = 'Ubah Obat / BHP Berhasil.';
            $pesandanger  = 'Ubah Obat / BHP Gagal.';
            $arrMsg  = $this->db->update('rs_operasi_barangpemeriksaan', ['jumlahpemakaian' => $jumlah], ['idbarangoperasi' => $idbarangoperasi]);
        } else if ($mode == 'hapus') {
            $pesansuccess = 'Hapus Obat / BHP Berhasil.';
            $pesandanger  = 'Hapus Obat / BHP Gagal.';
            $arrMsg  = $this->db->delete('rs_operasi_barangpemeriksaan', ['idbarangoperasi' => $idbarangoperasi]);
        }
        pesan_success_danger($arrMsg, $pesansuccess, $pesandanger, 'js');
    }

    //add daftar alat
    public function operasi_add_sewaalat()
    {
        $arrMsg = $this->db->insert('rs_sewaalat', ['namaalat' => $this->input->post('namaalat'), 'alias' => $this->input->post('alias'), 'harga' => $this->input->post('harga')]);
        pesan_success_danger($arrMsg, 'Tambah Alat Berhasil.', 'Tambah Alat Gagal.', 'js');
    }

    //simpan sewa alat
    public function operasi_save_sewaalat()
    {
        $type  = $this->input->post('type');
        $arrMsg = '';
        $pesansuccess = '';
        $pesandanger = '';

        if ($type == 'ubahjumlah') {
            $pesansuccess = 'Jumlah Sewa ALat Berhasil Diubah.';
            $pesandanger  = 'Jumlah Sewa ALat Gagal Diubah.';
            $arrMsg = $this->db->update('rs_operasi_sewaalat', ['jumlah' => $this->input->post('jumlah')], ['idoperasisewaalat' => $this->input->post('idsewa')]);
        } elseif ($type == 'ubahharga') {
            $pesansuccess = 'Harga ALat Berhasil Diubah.';
            $pesandanger  = 'Harga ALat Gagal Diubah.';
            if (empty(!$this->input->post('idsewa'))) {
                $arrMsg = $this->db->update('rs_operasi_sewaalat', ['harga' => $this->input->post('harga')], ['idoperasisewaalat' => $this->input->post('idsewa')]);
            }
            $arrMsg = $this->db->update('rs_sewaalat', ['harga' => $this->input->post('harga')], ['idsewaalat' => $this->input->post('idalat')]);
        } elseif ($type == 'tambah') {
            $pesansuccess = 'ALat Berhasil Ditambahkan.';
            $pesandanger  = 'ALat Gagal Ditambahkan.';
            $arrMsg = $this->db->insert('rs_operasi_sewaalat', ['idsewaalat' => $this->input->post('idalat'), 'jumlah' => 1, 'idoperasi' => $this->input->post('idoperasi'), 'harga' => $this->input->post('harga'), 'idpendaftaran' => $this->input->post("idpendaftaran")]);
        } elseif ($type == 'batal') {
            $pesansuccess = 'Sewa ALat Berhasil Dibatalkan.';
            $pesandanger  = 'Sewa ALat Gagal Dibatalkan.';
            $arrMsg = $this->db->delete('rs_operasi_sewaalat', ['idoperasisewaalat' => $this->input->post('idsewa')]);
        }
        pesan_success_danger($arrMsg, $pesansuccess, $pesandanger, 'js');
    }

    //tampil penggunaan barang operasi
    public function getdata_barangoperasi()
    {
        $idoperasi = $this->input->post('idoperasi');
        $data = $this->db->query("SELECT rb.idbarang, rb.idjenistarif, rb.hargajual, ifnull(rob.idbarangoperasi,0) as idbarangoperasi, ifnull(rob.jumlahpemakaian,0) as jumlahpemakaian, rb.namabarang, rs.namasatuan, rs.singkatansatuan, rsd.namasediaan
        from rs_barang rb 
        left join rs_operasi_barangpemeriksaan rob on rob.idbarang = rb.idbarang and rob.idoperasi = '" . $idoperasi . "'
        left join rs_satuan rs on rs.idsatuan = rb.idsatuan
        left join rs_sediaan rsd on rsd.idsediaan = rb.idsediaan
        WHERE rb.isobatoperasi = 1 order by rsd.namasediaan")->result_array();
        echo json_encode($data);
    }

    //update barang operasi
    public function updatedata_barangoperasi()
    {
        $idoperasi = $this->input->post('idoperasi');
        $idbarangoperasi = $this->input->post('idbarangoperasi');
        $idbarang = $this->input->post('idbarang');
        $jumlah   = $this->input->post('jumlah');

        $mode = '';
        if (empty(!$idbarangoperasi) && empty($jumlah)) //hapus
        {
            $mode = 'Hapus';
            $arrMsg = $this->db->delete('rs_operasi_barangpemeriksaan', ['idbarangoperasi' => $idbarangoperasi]);
        }

        if (empty($idbarangoperasi) && empty(!$jumlah)) //insert
        {
            $mode = 'Input';

            $data['idoperasi']     = $this->input->post('idoperasi');
            $data['idjenistarif']  = $this->input->post('idjenistarif');
            $data['idbarang']      = $this->input->post('idbarang');
            $data['idpendaftaran'] = $this->input->post('idpendaftaran');
            $data['harga']         = $this->input->post('harga');
            $data['jumlahpemakaian'] = $jumlah;

            $arrMsg = $this->db->insert('rs_operasi_barangpemeriksaan', $data);
        }

        if (empty(!$idbarangoperasi) && empty(!$jumlah)) // update
        {
            $mode = 'Ubah';
            $arrMsg = $this->db->update('rs_operasi_barangpemeriksaan', ['jumlahpemakaian' => $jumlah], ['idbarangoperasi' => $idbarangoperasi]);
        }

        pesan_success_danger($arrMsg, $mode . ' Penggunaan Barang Berhasil.', $mode . ' Penggunaan Barang Gagal.', 'js');
    }

    /**
     * Mahmud, clear
     * Insert Operasi
     */
    public function insertoperasi()
    {
        $callid = generaterandom(7);
        $save   = $this->db->insert('rs_operasi', ['idjadwaloperasi' => $this->input->post('idjop'), 'idpendaftaran' => $this->input->post('idp'), 'callid' => $callid]);

        if ($save) {
            $op = $this->db->select('idoperasi')->get_where('rs_operasi', ['callid' => $callid])->row_array();
            $this->db->update('rs_operasi', ['callid' => NULL], ['idoperasi' => $op['idoperasi']]);
            echo json_encode(['status' => 'success', 'message' => 'Operasi Berhasil Dimulai.', 'idoperasi' => $op['idoperasi']]);
        } else {
            pesan_danger("Operasi Tidak Berhasil Dimulai.", "js");
        }
    }
    /**
     * Mahmud, clear
     * Barang Operasi
     */
    public function barangoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGOPERASI)) //lihat define di atas
        {

            $data = [
                'content_view'      => 'pelayanan/v_barangoperasi',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'barangoperasi',
                'active_menu_level' => '',
                'plugins'           => [PLUG_DROPDOWN]
            ];
            $data['title_page']     = 'Obat/BHP Operasi';
            $data['mode']           = '';
            $data['script_js']      = ['js_barangoperasi'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function insertupdate_barangoperasi()
    {
        $id = $this->input->post('idbarang');
        $arrMsg = $this->db->update('rs_barang', ['isobatoperasi' => 1], ['idbarang' => $id]);
        pesan_success_danger($arrMsg, 'Obat/BHP Berhasil Ditambahkan.', 'Obat/BHP Gagal Ditambahkan.', 'js');
    }

    public function hapus_barangoperasi()
    {
        $id = $this->input->post('idbarang');
        $arrMsg = $this->db->update('rs_barang', ['isobatoperasi' => 0], ['idbarang' => $id]);
        pesan_success_danger($arrMsg, 'Obat/BHP Berhasil Dihapus.', 'Obat/BHP Gagal Dihapus.', 'js');
    }

    public function view_barangoperasi()
    {
        $result = $this->db->query('SELECT rb.idbarang, rb.namabarang, rb.idsediaan, rs.namasatuan, rs.singkatansatuan, rsd.namasediaan
            FROM rs_barang rb 
            join rs_satuan rs on rs.idsatuan = rb.idsatuan
            join rs_sediaan rsd on rsd.idsediaan = rb.idsediaan
            WHERE rb.isobatoperasi = 1 order by rsd.namasediaan')->result_array();
        echo json_encode($result);
    }

    /**
     * Mahmud, clear
     * Register Kamar Bedah
     */
    public function registerkamarbedah()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_REGISTERKAMARBEDAH)) //lihat define di atas
        {

            $data = [
                'content_view'      => 'pelayanan/v_registerkamarbedah',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'registerkamarbedah',
                'active_menu_level' => '',
                'plugins'           => [PLUG_DATE, PLUG_DATATABLE]
            ];
            $data['title_page']     = 'Register Kamar Bedah';
            $data['mode']           = '';
            $data['script_js']      = ['js_operasi_registerkamarbedah'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_registerkamarbedah()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_registerkamarbedah_(true);
        $data = [];
        if ($getData) {
            $no = $_POST['start'];
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[]  = ++$no;
                $row[]  = $obj->norm;
                $row[]  = $obj->namalengkap;
                $row[]  = $obj->alamat;
                $row[]  = $obj->jaminan;
                $row[]  = $obj->ruang;
                $row[]  = $obj->waktuoperasi;
                $row[]  = $obj->tanggalmulai . ' ' . $obj->jammulai;
                $row[]  = $obj->jenisanestesi;
                $row[]  = $obj->obatanestesi;
                $row[]  = $obj->dokteroperator;
                $row[]  = $obj->dokteranestesi;
                $row[]  = $obj->dokteranak;
                $row[]  = $obj->penata;
                $row[]  = $obj->onloop;
                $row[]  = $obj->asisten;
                $row[]  = $obj->instrumen;
                $row[]  = $obj->penerimabayi;
                $row[]  = $obj->diagnosapascaoperasi;
                $row[]  = $obj->tindakanoperasi;
                $row[]  = $obj->jenistindakan;
                $row[]  = $obj->asesment_pra_anestesi;
                $row[]  = $obj->kelengkapan_lap_anestesi;
                $row[]  = $obj->kelengkapan_monitoring_pemulihan_psca_anestesi;
                $row[]  = $obj->konversi_anestesi_dari_lokalregionalkega;
                $row[]  = $obj->asesmen_pra_bedah;
                $row[]  = $obj->marking;
                $row[]  = $obj->kelengkapan_scc;
                $row[]  = $obj->diskrepasi_diagnosa_pre_dan_post;

                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_registerkamarbedah(),
            "recordsFiltered" => $this->mdatatable->filter_dt_registerkamarbedah(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    /////////////////////panel SKDP pasien BPJS///////////////
    public function settingpanelskdp()
    {
        return [
            'content_view'      => 'pelayanan/v_panelskdp',
            'active_menu'       => 'pelayanan',
            'active_sub_menu'   => 'panelskdp',
            'active_menu_level' => ''
        ];
    }
    public function panelskdp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_APLICARE)) //lihat define di atas
        {
            $data                = $this->settingpanelskdp();
            $data['datalist']    = '';
            $data['title_page']  = 'Data SKDP';
            $data['mode']        = 'view';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['script_js']   = ['pelayanan/js_panelskdp'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dtskdp()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_skdp_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                // $row = [];
                $row = [$obj->noskdpql, $obj->nokontrol, $obj->nojkn, $obj->norm, $obj->namalengkap, $obj->tanggalskdp, $obj->namaunit, $obj->pertimbangan, $obj->petugas, $obj->user, $obj->waktuinput];
                $row[] = '<a style="margin:3px;" id="ubahskdp" no="'.$obj->noskdpql.'" noskdp="' . $obj->nokontrol . '" class="btn btn-xs btn-warning" ' . ql_tooltip('Ubah No.SKDP') . ' ><i class="fa fa-edit"></i></a>'.'<a style="margin:3px;" id="pemeriksaanklinik_cetakskdp" nobaris="'.$obj->noskdpql.'" nokontrol="'.$obj->nokontrol.'" pendaftaran="'.$obj->idpendaftaransebelum.'" class=" btn btn-info btn-xs" '.ql_tooltip('CETAK SKDP '.$obj->noskdpql).'><i class="fa fa-print"></i> '.$obj->noskdpql.'</a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->totdt_skdp(),
            "recordsFiltered" => $this->mdatatable->filterdt_skdp(),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function setnoskdp()
    {
        $arrMsg = $this->db->update('rs_skdp', 
            ['noskdpql' => $this->input->post('noskdp'),'nokontrol' => $this->input->post('nokontrol')], 
            ['nokontrol' => $this->input->post('nokontrolsebelum')]
        );
        pesan_success_danger($arrMsg, 'Ubah berhasil.', 'Ubah Gagal.', 'js');
    }

    /**--Ubah Jaminan Asuransi di rs_hasil Pemeriksaan per tindakan*/
    //mahmud, clear
    public function ubahjaminanasuransi_ralan()
    {
        $idhp = $this->input->post('idp'); //idhasilpemeriksaan
        $jaminan = $this->input->post('jaminan'); //jaminan asuransi
        $idpendaftaran = $this->input->post('idpend'); //idpendaftaran
        $mode = $this->input->post('mode'); //mode
        if ($mode == 'obat') {
            $arrMsg = $this->db->update('rs_barangpemeriksaan', ['jaminanasuransi' => $jaminan], ['idbarangpemeriksaan' => $idhp]);
        } else if ($mode == 'grupobat') {
            $arrMsg = $this->db->update('rs_barangpemeriksaan', ['jaminanasuransi' => $jaminan], ['idpendaftaran' => $idpendaftaran, 'grup' => $idhp]);
        } else {
            $arrMsg = $this->db->update('rs_hasilpemeriksaan', ['jaminanasuransi' => $jaminan], ['idhasilpemeriksaan' => $idhp]);
        }
        pesan_success_danger($arrMsg, 'Ubah Status Jaminan Berhasil.', 'Ubah Status Jaminan Gagal.', 'js');
    }

    public function validation_icd10()
    {
        $data = $this->db->select('rh.icd')->join('rs_icd ri', 'ri.icd=rh.icd')->get_where('rs_hasilpemeriksaan rh', ['idpendaftaran' => $this->input->post('idp'), 'ri.idjenisicd' => 2])->num_rows();
        echo json_encode($data);
    }

    /** ubah dokter pemeriksaan poli**/
    public function pemeriksaanklinik_ubahdokter()
    {
        $save = $this->db->update('rs_pemeriksaan', ['idpegawaidokter' => $this->input->post('idok')], ['idpemeriksaan' => $this->input->post('ip')]);
        pesan_success_danger($save, 'Ubah DPJP Berhasil.', 'Ubah DPJP Gagal.', 'js');
    }

    public function pemeriksaanklinik_ubahdokterpemeriksa()
    {
        $save = $this->db->update('rs_pemeriksaan', ['iddokterpemeriksa' => $this->input->post('iddokterpemeriksa')], ['idpemeriksaan' => $this->input->post('idpemeriksaan')]);
        pesan_success_danger($save, 'Ubah Dokter Pemeriksa Berhasil.', 'Ubah Dokter Pemeriksa Gagal.', 'js');
    }

    /** ubah loket pemeriksaan poli**/
    public function pemeriksaanklinik_ubahloket()
    {
        $idlp          = $this->input->post('idlp');
        $idpemeriksaan = $this->input->post('ip');
        $idloket       = $this->input->post('idl');
        $idpendaftaran = $this->input->post('idp');

        $idunit = $this->db->select('idunit')->get_where('antrian_loket', ['idloket' => $idlp])->row_array()['idunit'];
        $save   = $this->db->update('person_pendaftaran', ['idunit' => $idunit], ['idpendaftaran' => $idpendaftaran]);
        $save   = $this->db->update('rs_pemeriksaan', ['idunit' => $idunit, 'idloketsebelum' => $idloket, 'idloket' => $idlp], ['idpemeriksaan' => $idpemeriksaan]);
        pesan_success_danger($save, 'Ubah Loket Berhasil.', 'Ubah Loket Gagal.', 'js');
    }

    //simpan asesmen medis rawat jalan
    // public function save_asesmenmedisralan()
    // {
    //     $post = $this->input->post();
    //     $data = [
    //         'idpendaftaran'=> $post['idpendaftaran'],
    //         'idpemeriksaan'=> $post['idpemeriksaan'],
    //         'resikotinggi' => $post['resikotinggi'],
    //         'keluhanutama'    => $post['keluhanutama'],
    //         'pemeriksaanfisik'=> $post['pemeriksaanfisik'],
    //         'pemeriksaanpenunjang' => $post['pemeriksaanpenunjang'],
    //         'diagnosa'             => $post['diagnosa'],
    //         'pengobatan'           => $post['pengobatan'],
    //         'pemeriksaanlanjutan'  => $post['pemeriksaanlanjutan'],
    //         'diet'                 => $post['diet'],
    //         'rencanaasuhan'        => $post['rencanaasuhan'],
    //         'kondisikeluar'        => $post['asesmen_kondisikeluar'],
    //         'dokterpemeriksa'      => $post['dokterpemeriksa'],
    //         'riwayatpenyakit'      => $post['riwayatpenyakit'],
    //         'riwayatalergi'        => $post['riwayatalergi'],
    //         'tensi'                => $post['tensi'],
    //         'nadi'                 => $post['nadi'],
    //         'respirasi'            => $post['respirasi'],
    //         'suhu'                 => $post['suhu'],
    //         'tanggalperiksa'       => $post['tanggalperiksa'],
    //         'jamperiksa'           => $post['jamperiksa']
    //     ];
    //     $save = $this->db->replace('rs_ralan_asesmenmedis',$data);
    //     pesan_success_danger($save, 'Update Asjamperiksaesmen Medis Berhasil.', 'Update Asesmen Medis Berhasil.','js');
    // }

    public function get_asesmenawalmedisralan()
    {
        $post = $this->input->post();

        $norm = $post['norm'];

        $data = [
            'idpendaftaran' => $post['idpendaftaran']
        ];

        $getdatapengkajian = $this->db->get_where('rs_pengkajian_awal_medis', $data)->result_array();

        $getdatasubjectivepengkajian = $this->db->query("select * from rs_pengkajian_awal_medis INNER JOIN person_pendaftaran ON rs_pengkajian_awal_medis.idpendaftaran = person_pendaftaran.idpendaftaran WHERE person_pendaftaran.norm = " . $norm . " GROUP BY rs_pengkajian_awal_medis.idpendaftaran order by rs_pengkajian_awal_medis.idpendaftaran desc LIMIT 1")->result();

        $datares = [
            "datapengkajian" => $getdatapengkajian,
            "datasubjective" => $getdatasubjectivepengkajian
        ];

        echo json_encode($datares);
    }

    public function save_asesmenawalmedisralan()
    {
        $post = $this->input->post();

        $queridpegawai = $this->db->query("SELECT person_pegawai.idpegawai FROM `person_pegawai` WHERE person_pegawai.idperson =" . $post['idpdokter'])->result_array();
        $idpegawai = $queridpegawai[0]['idpegawai'];

        $data = [
            "idpendaftaran" => $post['idpendaftaran'],
            "idpemeriksaan" => $post['idpemeriksaan'],
            "carapasiendatang" => isset($post['chckcarapasiendatang']) ? $post['chckcarapasiendatang'] : null,
            "diantarnama" => !isset($post['chckcarapasiendatangdiantarnama']) ? null : $post['chckcarapasiendatangdiantarnama'],
            "diantaralamat" => !isset($post['chckcarapasiendatangdiantaralamat']) ? null : $post['chckcarapasiendatangdiantaralamat'],
            "diantarnotelp" => !isset($post['chckcarapasiendatangdiantarnotelp']) ? null : $post['chckcarapasiendatangdiantarnotelp'],
            "rujukandari"  => !isset($post['chckcarapasiendatangrujukandari']) ? null : $post['chckcarapasiendatangrujukandari'],
            "rujukanalaasan" => !isset($post['chckcarapasiendatangrujukanalasan']) ? null : $post['chckcarapasiendatangrujukanalasan'],
            "kategorikasus" => !isset($post['chckkategorikasus']) ? null : $post['chckkategorikasus'],
            "triasemetodeesi" => !isset($post['chhcktme']) ? null : $post['chhcktme'],
            "riwayatpenyakitsekarang" => !isset($post['riwayatpenyakitsekarang']) ? null : $post['riwayatpenyakitsekarang'],
            "riwayatpenyakitdahulu" => empty($post['chcktidakriwayapenyakitdahulu']) ? 1 : 0,
            "txtriwayatpenyakitdahulu" => !isset($post['txtriwayatpenyakitdahulu']) ? null : $post['txtriwayatpenyakitdahulu'],
            "obatyangrutindikonsumsi" => empty($post['chcktidakobyrd'])  ? 1 : 0,
            "txtobatyangrutindikonsumsi" => !isset($post['txtobyrd']) ? null : $post['txtobyrd'],
            "riwayatalergiobatdanmakanan" => empty($post['chcktidakraodm'])  ? 1 : 0,
            "txtriwayatalergiobatdanmakanan" => !isset($post['txtraodm']) ? null : $post['txtraodm'],
            "keadaanumum" => $post['keadaanumum'],
            "gcse" => !isset($post['gcse']) ? null : $post['gcse'],
            "gcsv" => !isset($post['gcsv']) ? null : $post['gcsv'],
            "gcsm" => !isset($post['gcsm']) ? null : $post['gcsm'],
            "skalanyeri" => !isset($post['skalanyeri']) ? null : $post['skalanyeri'],
            "txtpemeriksaanfisik" => !isset($post['pemeriksaanfisik']) ? null : $post['pemeriksaanfisik'],
            "diagnosadilauricd10" => !isset($post['diagnosa']) ? null : $post['diagnosa'],
            "pemeriksaanpenunjangmedik" => empty($post['chcktidakppm']) ? 1 : 0,
            "txtpemeriksaanpenunjangmedik" => !isset($post['txtppm']) ? null : $post['txtppm'],
            "terapi" => !isset($post['txtterapi']) ? null : $post['txtterapi'],
            "catatanalainnya" => empty($post['chcktdkcatatanlainnya']) ? 1 : 0,
            "txtcatatanlainnya" => !isset($post['txtcatatanlainnya']) ? null : $post['txtcatatanlainnya'],
            "observasipasiendiugd" => !isset($post['chckobservasipasiendiugd']) ? null : $post['chckobservasipasiendiugd'],
            "observasipasiendiugdcatatan" => !isset($post['txtobservasipasiendiugd']) ? null : $post['txtobservasipasiendiugd'],
            "rencanaperawatandantindakanaselanjutnya" => !isset($post['chckrencanaperawatandantindakanlainnya']) ? null : $post['chckrencanaperawatandantindakanlainnya'],
            "rawatjalankontroldi" => !isset($post['chckrawatjalanitem']) ? null : $post['chckrawatjalanitem'],
            "kontroldiitem" => !isset($post['chckkontroldi']) ? null : $post['chckkontroldi'],
            "tglugd" => !isset($post['tglugd']) ? null : $post['tglugd'],
            "namapoliklinik" => !isset($post['txtkontroldi']) ? null : $post['txtkontroldi'],
            "tglpoliklinik" => !isset($post['txttanggalpoliklinik']) ? null : $post['txttanggalpoliklinik'],
            "bangsalrawatinap" => !isset($post['txtrawatinapbangsal']) ? null : $post['txtrawatinapbangsal'],
            "kondisipasiensebelummeninggalkanUGD" => !isset($post['chckkpsmu']) ? null : $post['chckkpsmu'],
            "meninggalduniatanggal" => !isset($post['txttanggalmeninggaldunia']) ? null : $post['txttanggalmeninggaldunia'],
            "meninggalduniajam" => !isset($post['txtjammeninggaldunia']) ? null : $post['txtjammeninggaldunia'] . ':00',
            "tglselesaiasawal" => $post['tglselesaiasesmen'],
            "jamselesaiasawal" => $post['txtselesaiasesmenawal'],
            "idpegawaidokter" =>  $idpegawai,
            "isadaracikan" => empty($post['adaracikan']) ? 1 : 0,
            "keteranganobat" => !isset($post['keteranganobat']) ? null : $post['keteranganobat'],
            "tglpasienpulangugd" => !isset($post['tglpasienpulangugd']) ? null : $post['tglpasienpulangugd'],
            "jampasienpulangugd" => !isset($post['jampasienpulangugd']) ? null : $post['jampasienpulangugd'],
            "iddokterugd" => !isset($post['iddokterugd']) ? null : $post['iddokterugd']
        ];

        $save = $this->db->replace('rs_pengkajian_awal_medis', $data);
        pesan_success_danger($save, 'Update Asesmen awal Medis Berhasil.', 'Update Asesmen awal Medis Berhasil.', 'js');
    }


    //edit asesmen medis rawat jalan
    public function edit_asesmenmedisralan()
    {
        $query = $this->db->get_where('rs_ralan_asesmenmedis', ['idpendaftaran' => $this->input->post('idp'), 'idpemeriksaan' => $this->input->post('ip')]);
        if ($query->num_rows() > 0) {
            $data['asesmen'] = $query->row_array();
        } else {
            $data['asesmen'] = null;
        }

        $data['identitas']      = $this->db->query("select pper.nik, namapasien(ppas.idperson) as pasien, date_format(pper.tanggallahir,'%d %M %Y') as tanggallahir, pp.norm, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.agama, pper.alamat from person_pendaftaran pp
            join person_pasien ppas on ppas.norm=pp.norm
            join person_person pper on pper.idperson=ppas.idperson
            where idpendaftaran='" . $this->input->post('idp') . "'")->row_array();
        echo json_encode($data);
    }


    // public function save_asesmenawalkeperawatan()
    // {
    //     $post = $this->input->post();

    //     // var_dump(empty($post['chckapakahpasienbersediamenerimaedukasitidak']) ? 0 : 1);
    //     // die;

    //     $data = [
    //         "idpendaftaran" => $post['idpendaftaran'],
    //         "idpemeriksaan" => $post['idperiksa'],
    //         "keluhanutama" => $post['txtkeluhanutama'],
    //         "apakahpasiendengankeluhankebidanan" => empty($post['chckapdkt']) ? null : $post['chckapdkt'],
    //         "riwayatkehamilansekarangG" => !isset($post['rksg']) ? NULL : $post['rksg'],
    //         "riwayatkehamilansekarangP" => !isset($post['rksp']) ? NULL : $post['rksp'],
    //         "riwayatkehamilansekarangA" => !isset($post['rksga']) ? NULL : $post['rksga'],
    //         "riwayatkehamilansekarangAh" => !isset($post['rksah']) ? NULL : $post['rksah'],
    //         "riwayatkehamilansekarangHPHT" => !isset($post['rksghpht']) ? NULL : $post['rksghpht'],
    //         "riwayatkehamilansekarangHPL" => !isset($post['rksghpl']) ? NULL : $post['rksghpl'],
    //         "riwayatkehamilansekarangUK" =>!isset($post['rksguk']) ? NULL : $post['rksguk'],
    //         "ANCTerpadu" => empty($post['chckanctidak']) ? 1 : 0,
    //         "ANCTerpadudi" => !isset($post['txtanc']) ? NULL : $post['txtanc'],
    //         "imunisasitt" => empty($post['chcktdkitt']) ? 1 : 0,
    //         "imunisaittke" => !isset($post['txtitt']) ? NULL : $post['txtitt'],
    //         "pendarahanpervaginam" => empty($post['chcktdkppvm']) ? 1 : 0,
    //         "pendarahanpervaginamml" => empty($post['txtppvm']) ? NULL : $post['txtppvm'],
    //         "objectiveairway" => empty($post['chckairway']) ? "-" : $post['chckairway'],
    //         "objectivebreathing" => empty($post['chckbre']) ? "-" : $post['chckbre'],
    //         "objectivesuaranafas" => empty($post['chcksuana']) ? "-" : $post['chcksuana'],
    //         "CirculationPulsasiPerifer" => empty($post['chckpulpertidakteraba']) ? 1 : 0,
    //         "CirculationPulsasiKarotis" => empty($post['chckpulsasikarotistidakteraba']) ? 1 : 0,
    //         "CirculationWarnaKulit" => empty($post['chckwarnakulitpucat']) ? 1 : 0,
    //         "CirculationAkral" => empty($post['chckakraldingin']) ? 1 : 0,
    //         "CirculationCRT" => empty($post['chckcrtlebih2dtk']) ? 1 : 0,
    //         "CirculationKesadaran" => empty($post['chckkesadaran']) ? "-" : $post['chckkesadaran'],
    //         "nipsekspresiwajah" => empty($post['chckekw']) ? "-" : $post['chckekw'],
    //         "nipstangisan" => empty($post['chcktangisan']) ? "-" : $post['chcktangisan'],
    //         "nipspolapernafasan" => empty($post['chckpolaperna']) ? "-" : $post['chckpolaperna'],
    //         "nipstangan" => empty($post['chcktangan']) ? "-" : $post['chcktangan'],
    //         "nipskesadaran" => empty($post['chckkesadaran']) ? "-" : $post['chckkesadaran'],
    //         "nipstotalskor" => empty($post['totalskornips']) ? 0 : $post['totalskornips'],
    //         "is_nips" => empty($post['chckppnnips']) ? 0 : 1,
    //         "nipsinterpretasihasil" =>  empty($post['chckbayitidakmengalaminyeri']) ? 1: 0,
    //         "is_flacc" => empty($post['chckppnflacc']) ? 0 : 1,
    //         "flacckaki" => empty($post['chckflacckaki']) ? NULL : $post['chckflacckaki'],
    //         "flaccwajah" => empty($post['chckflaccwajah']) ? NULL : $post['chckflaccwajah'],
    //         "flaccaktivitas"  => empty($post['chckaktivitaspengkajian']) ? NULL : $post['chckaktivitaspengkajian'],
    //         "flaccmenangis" => empty($post['chckpengkajianmenangis']) ? NULL : $post['chckpengkajianmenangis'],
    //         "flaccbersuara" => empty($post['chckpengkajianbersuara']) ? NULL : $post['chckpengkajianbersuara'],
    //         "flacctotalskor" => empty($post['totalskorflacc']) ? NULL : $post['totalskorflacc'],
    //         "flaccinterprestasihasil" => empty($post['chckinteprestrasihasilflacc']) ? NULL : $post['chckinteprestrasihasilflacc'],
    //         "is_wbfps" => empty($post['chckppnwongbaker']) ? 0 : 1,
    //         "wbfps" => !isset($post['chckwongbaker']) ? NULL : $post['chckwongbaker'],
    //         "wbfpsintepretasihasil" => empty($post['chckinterpretasiwbfps']) ? NULL : $post['chckinterpretasiwbfps'],
    //         "faktoryangmemperberat" => !isset($post['chckfym']) ? NULL : $post['chckfym'],
    //         "faktoryangmeringankan" => !isset($post['chckfymer']) ? NULL : $post['chckfymer'],
    //         "kualitasnyeri" => !isset($post['chckkualitasnyeri']) ? NULL : $post['chckkualitasnyeri'],
    //         "lokasinyeri" => empty($post['txtlokasinyeri']) ? NULL : $post['txtlokasinyeri'],
    //         "menjalar" => empty($post['txtjelaskanmenjalar']) ? NULL : $post['txtjelaskanmenjalar'],
    //         "skalanyeri" =>  !isset($post['chckskalanyeri']) ? NULL : $post['chckskalanyeri'] ,
    //         "lamanyanyeri" =>  empty($post['chcklamanyanyeridiatas30menit']) ? 0 : 1,
    //         "frekuensinyeri" => !isset($post['chckfrekuensinyeri']) ? NULL : $post['chckfrekuensinyeri'],
    //         "kualitasnyeriolainnya" => empty($post['txtlainnyakualitasnyeri']) ? NULL : $post['txtlainnyakualitasnyeri'],
    //         "menjalarjelaskan" => empty($post['txtjelaskanmenjalar']) ? NULL : $post['txtjelaskanmenjalar'],
    //         "pengkajiantrisikojatuhcaraberjalanpasien" => empty($post['chckprjatuhcvptsjdmabyatidak']) ? 1 : 0,
    //         "pengkajianrisikojatuhmenopangsaatakanduduk" => empty($post['chckmsadyatidak']) ? 1 : 0,
    //         "pengkajianrisikojatuhinterpretasihasil" => !isset($post['chckintepretasihasilpengkajianrisikojatuh']) ? 0 : $post['chckintepretasihasilpengkajianrisikojatuh'],
    //         "pengkajianrisikonutrisionalberatbadan" => empty($post['chckprnberatbadantidak']) ? 1 : 0,
    //         "pengkajianrisikonutrisionalnafsumakan" => empty($post['chckprnnafsumakantidak']) ? 1 : 0,
    //         "pengkajianrisikonutrisionalinterpretasihasil" => empty($post['chckinterpretasiprn']) ? 0 : $post['chckinterpretasiprn'],


    //         "pengkajianstatusfungsionalpenggunaanalatbantu" => isset($post['chckpealaban']) ? $post['chckpealaban'] : null,
    //         "pengkajianstatusfungsionalpenggunaanalatbantusebutkan" => empty($post['txtoestafu']) ? NULL : $post['txtoestafu'],
    //         "pengkajianstatusfungsionaladl" => isset($post['chckadl']) ? $post['chckadl'] : null,


    //         "pengkajianstatuspsikologi" =>  isset($post['chckpenstatuspsikologi']) ? $post['chckpenstatuspsikologi'] : null,
    //         "pengkajianstatuspsikologilainnya" => empty($post['txtpengstatuspsikologi']) ? null : $post['txtpengstatuspsikologi'],



    //         "pengkajianstatussosialstatuspernikahan" =>  !isset($post['chckpengstasoekkudspi']) ? null : $post['chckpengstasoekkudspi'],

    //         "pengkajianstatussosialkendalakomunikasi" => !isset($post['chckkendalakomunikasi']) ? null : $post['chckkendalakomunikasi'],
    //         "pengkajianstatussosialkendalakomunikasisebutkan" => empty($post['txtkendadakomunikasilainnya']) ? NULL : $post['txtkendadakomunikasilainnya'],

    //         "pengkajianstatussosialyangmerawatdirumah" => isset($post['chckyangmerawatdirumah']) ? $post['chckyangmerawatdirumah']: null,
    //         "pengkajianstatussosialyangmerawatdirumahsebutkan" => empty($post['txtadasebutkanyangmerawatdirumah']) ? NULL : $post['txtadasebutkanyangmerawatdirumah'],


    //         "pengkajianstatussosialpekerjaan" => isset($post['chckpekerjaanlainnya']) ? $post['chckpekerjaanlainnya'] : NULL , 
    //         "pengkajianstatussosialpekerjaanlainnya" => empty($post['txtlainnyapekerjaan']) ? NULL : $post['txtlainnyapekerjaan'], 
    //         "pengkajainstatussosialjaminankesehatan" => isset($post['chckjaminankesehatan']) ? $post['chckjaminankesehatan'] : NULL,  
    //         "pengkajainstatussosialjaminankesehatanlainnya" => empty($post['txtjaminankesehatanlainnya']) ? NULL : $post['txtjaminankesehatanlainnya'],  

    //         "pengkajianstatusososialapakahaadatindakannbertentangankepercayaa" => isset($post['chcktidanakanyangbertentangandengankepercayaanpasien']) ? $post['chcktidanakanyangbertentangandengankepercayaanpasien'] : null,


    //         "pengkajianstatussosialjelaskantindakan" => empty($post['txtjikaadajelaskantindakantersebut']) ? NULL : $post['txtjikaadajelaskantindakantersebut'],

    //         "diagnosasdki" => empty($post['diagnosasdki']) ? 0 : $post['diagnosasdki'],
    //         "diagnosadiluarsdki" => empty($post['diagnosadiluarsdki']) ? NULL : $post['diagnosadiluarsdki'],

    //         "planintervensinyerimengajarkanteknikrelaksasi" => isset($post['planintervensinyerimengajarkanteknikrelaksasi']) ? 1 : 0,
    //         "planintervensinyerimegaturposisiyangnyamanbagipasien" => isset($post['planintervensinyerimegaturposisiyangnyamanbagipasien']) ? 1 : 0 ,
    //         "planintervensinyerimengontrollingkungan" => isset($post['planintervensinyerimengontrollingkungan']) ? 1 : 0,
    //         "planintervensinyerimemberikanedukasikepadapasiendankeluarga" => isset($post['planintervensinyerimemberikanedukasikepadapasiendankeluarga']) ? 1 : 0,
    //         "planintervensinyerikoloborasidengandokter" => isset($post['planintervensinyerikoloborasidengandokter']) ? 1 : 0,
    //         "planintervensinyerichcklainnya"  => isset($post['planintervensinyerichcklainnya']) ? 1 : 0,
    //         "planintervensinyeritidakadaintervensi" => isset($post['planintervensinyeritidakadaintervensi']) ? 1 : 0,
    //         "planintervensinyerilainnya" => empty($post['txtintervensinyerilainnya']) ? NULL : $post['txtintervensinyerilainnya'],


    //         "planintervensirisikojatuhpakaikanpitabewarnakuning"  => isset($post['planintervensirisikojatuhpakaikanpitabewarnakuning']) ? 1 : 0,
    //         "planintervensirisikojatuhberikanedukasipencegahanjatuh"  => isset($post['planintervensirisikojatuhberikanedukasipencegahanjatuh']) ? 1 : 0,
    //         "planintervensirisikojatuhchcklainnya"  => isset($post['planintervensirisikojatuhchcklainnya']) ? 1 : 0,
    //         "planintervensirisikojatuhtidakadaintervensi" => isset($post['planintervensirisikojatuhtidakadaintervensi']) ? 1 : 0,
    //         "planintervensirisikojatuhlainnya" => empty($post['txtintervensirisikojatuhtidakadaintervensi']) ? NULL : $post['txtintervensirisikojatuhtidakadaintervensi'],



    //         "planintervensirisikonutrisionalkonsultasikanpadaahligizi" => isset($post['planintervensirisikonutrisionalkonsultasikanpadaahligizi']) ? 1 : 0,
    //         "planintervensirisikonutrisionalchcklainnya" => isset($post['planintervensirisikonutrisionalchcklainnya']) ? 1 : 0,
    //         "planintervensirisikonutrisionaltidakadaintervensi" => isset($post['planintervensirisikonutrisionaltidakadaintervensi']) ? 1 : 0,
    //         "planintervensirisikonutrisionallainnya" => empty($post['txtintervensirisikonutrisionallainnya']) ? NULL : $post['txtintervensirisikonutrisionallainnya'],



    //         "intervensistatusfungsionallibatkanpartisipasikeluargadalam" => isset($post['intervensistatusfungsionallibatkanpartisipasikeluargadalam']) ? 1 : 0,
    //         "intervensistatusfungsionalchcklainnya" => isset($post['intervensistatusfungsionalchcklainnya']) ? 1 : 0,
    //         "intervensistatusfungsionaltidakadaintervensi" => isset($post['intervensistatusfungsionaltidakadaintervensi']) ? 1 : 0,
    //         "planintervesistatusfungsionallainnya" => empty($post['txtintervensistatusfungsionallainnya']) ? NULL : $post['txtintervensistatusfungsionallainnya'],


    //         "intervensipsikologimenenangkanpasien" => isset($post['intervensipsikologimenenangkanpasien']) ? 1 : 0,
    //         "intervensipsikologimengajarkanteknikrelaksasi"  => isset($post['intervensipsikologimengajarkanteknikrelaksasi']) ? 1 : 0,
    //         "intervensipsikologiberkoloborasidengandokter" => isset($post['intervensipsikologiberkoloborasidengandokter']) ? 1 : 0,
    //         "intervensipsikologichcklainnya"  => isset($post['intervensipsikologichcklainnya']) ? 1 : 0,
    //         "intervensipsikologitidakadaintervensi" => isset($post['intervensipsikologitidakadaintervensi']) ? 1 : 0,
    //         "planintevensipsikologilainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'],


    //         "planintervensisosialmenjelaskankepadapasiendankeluarga"  => isset($post['planintervensisosialmenjelaskankepadapasiendankeluarga']) ? 1 : 0,
    //         "planintervensisosialmemfasilitasibiladahambatan"  => isset($post['planintervensisosialmemfasilitasibiladahambatan']) ? 1 : 0,
    //         "planintervensisosialmemberikanalternatiftindakanlain"  => isset($post['planintervensisosialmemberikanalternatiftindakanlain']) ? 1 : 0,
    //         "planintervensisosialmenjelaskanterkaittindakan"  => isset($post['planintervensisosialmenjelaskanterkaittindakan']) ? 1 : 0,
    //         "planintervensisosialchcklainnya" => isset($post['planintervensisosialchcklainnya']) ? 1 : 0,
    //         "planintervensisosialtidakadaintervensi"  => isset($post['planintervensisosialtidakadaintervensi']) ? 1 : 0,
    //         "planintervensisosiallainnya" => empty($post['txtintervensisosiallainnya']) ? NULL : $post['txtintervensisosiallainnya'], 


    //         "planintervensialergimemberitahukankepadadokter" => isset($post['planintervensialergimemberitahukankepadadokter']) ? 1 : 0,
    //         "planintervensialergiedukasikepadapasiendankeluarga"  => isset($post['planintervensialergiedukasikepadapasiendankeluarga']) ? 1 : 0,
    //         "planintervensialergichcklainnya"  => isset($post['planintervensialergichcklainnya']) ? 1 : 0,
    //         "planintervensialergitidakadaintervensi" => isset($post['planintervensialergitidakadaintervensi']) ? 1 : 0,
    //         "planintervensialergilainnya" => empty($post['txtintervensialergilainnya']) ? NULL : $post['txtintervensialergilainnya'], 

    //         "stabilisasijalannafas" => empty($post['chckintervensikeperawatanlainnyastabilisasijalannafas']) ? 0 : 1, 
    //         "keluarkanbendaasing" => empty($post['chckintervensikeperawatanlainnyakeluarkanbendaasing']) ? 0 : 1, 
    //         "pasangfripharingeal" => empty($post['chckintervensikeperawatanlainnyapasangiripharingeal']) ? 0 : 1, 
    //         "berikanbantuannafas" => empty($post['chckintervensikeperawatanlainnyaberikanbantuannafas']) ? 0 : 1, 
    //         "berikanoksigenmelaluinasal" => empty($post['chckintervensikeperawatanlainnyaberikan02']) ? 0 : 1, 
    //         "delegatifnebulizer" => empty($post['chckintervensikeperawatanlainnyadelegatifnebulizer']) ? 0 : 1, 
    //         "monitortandavitalsecararperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortandatandavital']) ? 0 : 1, 
    //         "monitortingkatkesadaransecaraperiodik" => empty($post['chckintervensikeperawatanlainnyamonitortingkatkesadaran']) ? 0 : 1, 
    //         "monitorekg" => empty($post['chckintervensikeperawatanlainnyamontorekg']) ? 0 : 1, 
    //         "berikanposisisemifownler" => empty($post['chckintervensikeperawatanlainnyaberikanposisisemifowler']) ? 0 : 1, 
    //         "pasangdowercatheter" => empty($post['chckintervensikeperawatanlainnyapasangdowercatheter']) ? 0 : 1, 
    //         "monitorintakedanoutputcairan" => empty($post['chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan']) ? 0 : 1, 
    //         "pasanginfusintravena" => empty($post['chckintervensikeperawatanlainnyapasanginfus']) ? 0 : 1, 
    //         "lakukanperawatanluka" => empty($post['chckintervensikeperawatanlainnyalakukanperawatanluka']) ? 0 : 1, 
    //         "koloborasidengandokteruntukiv" => empty($post['chckintervensikeperawatanlainnyakoloborasidengandokter']) ? 0 : 1, 
    //         "kajiturgorkulit" => empty($post['chckintervensikeperawatanlainnyakajirturgorkulit']) ? 0 : 1, 
    //         "pasangpengamanspalk" => empty($post['chckintervensikeperawatanlainnyapasangpengaman']) ? 0 : 1, 
    //         "mengobservasitandaadanyakompartemen" => empty($post['chckintervensikeperawatanlainnyamengobservasitandatanda']) ? 0 : 1, 
    //         "pasangngt" => empty($post['chckintervensikeperawatanlainnyaoasangngt']) ? 0 : 1, 
    //         "intervensikeperawatanlainnya1"  => empty($post['txtintervensikeperawatanlainnya1']) ? NULL : $post['txtintervensikeperawatanlainnya1'], 
    //         "intervensikeperawatanlainnya2" => empty($post['txtintervensikeperawatanlainnya2']) ? NULL : $post['txtintervensikeperawatanlainnya2'],
    //         "intervensikeperawatanlainnya3" => empty($post['txtintervensikeperawatanlainnya3']) ? NULL : $post['txtintervensikeperawatanlainnya3'],
    //         "intervensikeperawatanlainnya4" => empty($post['txtintervensikeperawatanlainnya4']) ? NULL : $post['txtintervensikeperawatanlainnya4'],

    //         "evaluasikeperawatan" => isset($post['chckaevaluasikeperawatan']) ? $post['chckaevaluasikeperawatan'] : null, 
    //         "evaluasikeperawatansebutkan" => empty($post['txtevaluasikeperawatan']) ? NULL : $post['txtevaluasikeperawatan'], 


    //         "evaluasipasiendankeluargamenerimaedukasi" => empty($post['chckapakahpasienbersediamenerimaedukasitidak']) ? 1 : 0,
    //         "evaluasipasiendankeluargahambatan" => empty($post['chckapakahterdapathambatandalamedukasitidak']) ? 1 : 0,
    //         " evaluasipasienhambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1, 
    //         "evaluasipasienhambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1, 
    //         "evaluasipasienhambatankognitif" => empty($post['chckkognitif']) ? 0 : 1, 
    //         "evaluasipasienhambatanfisik" => empty($post['chckfisik']) ? 0 : 1, 
    //         "evaluasipasienhambatanemosi" => empty($post['chckemosi']) ? 0 : 1, 
    //         "evaluasipasienhambatanbahasa" => empty($post['chckbahasa']) ? 0 : 1, 
    //         "evaluasipasienhambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1, 
    //         "evaluasipasienhambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1, 
    //         "hambatanpendengaran" => empty($post['chckpendengaran']) ? 0 : 1,
    //         "hambatanemosi" => empty($post['chckemosi']) ? 0 : 1,
    //         "hambatanpenglihatan" => empty($post['chckpenglihatan']) ? 0 : 1,
    //         "hambatanbahasa"  => empty($post['chckbahasa']) ? 0 : 1,
    //         "hambatankognitif" => empty($post['chckkognitif']) ? 0 : 1,
    //         "hambatanlainnya" => empty($post['chcklainnya']) ? 0 : 1,
    //         "hambatanfisik"  => empty($post['chckfisik']) ? 0 : 1,
    //         "hambatanbudaya" => empty($post['chckbudaya']) ? 0 : 1,
    //         "edukasidiagnosapenyakit"  => empty($post['chckdiagnosapenyakit']) ? 0 : 1,
    //         "edukasirehabilitasimedik" => empty($post['chckrehabilitasimedik']) ? 0 : 1,
    //         "edukasihakdankewajibanpasien" => empty($post['chckhakdankewajibanpasien']) ? 0 : 1,
    //         "edukasitandabahayadanperawatanbahayibarulahir" => empty($post['chcktandabahayadanperawatanbayi']) ? 0 : 1,
    //         "edukasiobatobatan"  => empty($post['chckobatobatan']) ? 0 : 1,
    //         "edukasimanajemennyeri" => empty($post['chckmanajemennyeri']) ? 0 : 1,
    //         "edukasikb" => empty($post['chckkb']) ? 0 : 1,
    //         "edukasidietdannutrisi" => empty($post['chckdietdannutrisi']) ? 0 : 1,
    //         "edukasipenggunaanalatmedia" => empty($post['chckpenggunaanalatmedia']) ? 0 : 1,
    //         "edukasitandabahayamasanifas" => empty($post['chcktandabahayamasanifas']) ? 0 : 1,
    //         "tanggalpasienpulang" => empty($post['txttanggalpasienpulang']) ? NULL : $post['txttanggalpasienpulang'],
    //         "jampasienpulang" => empty($post['txtjampasienpulang']) ? NULL : $post['txtjampasienpulang'],
    //         "perawatpelaksana" => empty($post['txtperawatpelaksana']) ? NULL : $post['txtperawatpelaksana']
    //     ];

    //     $this->db->where('idpendaftaran',$post['idpendaftaran'])->delete('rs_ralan_asesmen_awal_keperawatan');
    //     $this->db->replace('rs_ralan_asesmen_awal_keperawatan',$data);

    //     $response = ["result" => "Success", "msg" => "Update Asesmen Awal Keperawatan Sukses!"];
    //     echo json_encode($response);
    // }

    //simpan asesmen medis rawat UGD
    public function save_asesmenmedisugd()
    {
        $post = $this->input->post();
        $kesadaran = (empty($post['kesadaran']) ? '' : implode(',', $post['kesadaran']));
        $data = [
            'idpendaftaran'   => $post['idpendaftaran'],
            'idpemeriksaan'   => $post['idpemeriksaan'],
            'idkriteriastart' => $post['idkriteriastart'],
            'resikotinggi'    => $post['resikotinggi'],
            'sumberinformasi' => $post['sumberinformasi'],
            'caramasuk'       => $post['caramasuk'],
            'caradatang'      => $post['caradatang'],
            'kesadaran'       => $kesadaran,
            'keadaanumum'     => $post['keadaanumum'],
            'tekanandarah'    => $post['tekanandarah'],
            'nadi'            => $post['nadi'],
            'respirasi'       => $post['respirasi'],
            'suhu'            => $post['suhu'],
            'spo2'            => $post['spo2'],
            'beratbadan'      => $post['beratbadan'],
            'tinggibadan'     => $post['tinggibadan'],
            'keluhanutama'    => $post['keluhanutama'],
            'riwayatpenyakit' => $post['riwayatpenyakit'],
            'pemeriksaanfisik' => $post['pemeriksaanfisik'],
            'pemeriksaanlokalis' => $post['pemeriksaanlokalis'],
            'nilaiskalanyeri' => $post['nilaiskalanyeri'],
            'hasilobservasi'  => $post['hasilobservasi'],
            'hasilpemeriksaanpenunjang' => $post['hasilpemeriksaanpenunjang'],
            'hasilpemeriksaanekg' => $post['hasilpemeriksaanekg'],
            'rencanaasuhan'   => $post['rencanaasuhan'],
            'diagnosis'       => $post['diagnosis'],
            'dokterugd'       => $post['dokterugd'],
            'dpjp'            => $post['dpjp'],
            'tanggalmasuk'    => $post['tanggalmasuk'],
            'jammasuk'        => $post['jammasuk'],
            'kondisikeluar'   => $post['ugd_kondisikeluar'],
            'hasilobservasi'  => $post['hasilobservasi'],
            'tanggalkeluar'   => $post['tanggalkeluar'],
            'jamkeluar'       => $post['jamkeluar'],
            'carakeluar'      => $post['carakeluar']
        ];
        $save = $this->db->replace('rs_ugd_asesmenmedis', $data);

        $save = $this->db->delete('rs_ugd_scalaflaccdibawahenamtahun', ['idpendaftaran' => $post['idpendaftaran']]);
        //simpan rs_ugd_scalaflaccdibawahenamtahun
        $pengkajian = $this->input->post('nameradioflacc');
        for ($x = 0; $x < count($pengkajian); $x++) {

            if (isset($post["rad_" . $pengkajian[$x] . ""]) && $post["rad_" . $pengkajian[$x] . ""] != null) {
                $data = [
                    'idpendaftaran'   => $post['idpendaftaran'],
                    'pengkajian'      => $pengkajian[$x],
                    'nilai'           => $post["rad_" . $pengkajian[$x] . ""]
                ];

                $save = $this->db->replace('rs_ugd_scalaflaccdibawahenamtahun', $data);
            }
        }
        pesan_success_danger($save, 'Update Asesmen Medis Berhasil.', 'Update Asesmen Medis Berhasil.', 'js');
    }
    //edit asesmen medis ugd
    public function edit_asesmenmedisugd()
    {
        $idpendaftaran = $this->input->post('idp');
        $idpemeriksaan = $this->input->post('ip');
        $query = $this->db->get_where('rs_ugd_asesmenmedis', ['idpendaftaran' => $idpendaftaran, 'idpemeriksaan' => $idpemeriksaan]);

        if ($query->num_rows() > 0) {
            $data['asesmen'] = $query->row_array();
        } else {
            $data['asesmen'] = null;
        }

        $scalaflaccugd  = $this->db->get_where('rs_ugd_scalaflaccdibawahenamtahun', ['idpendaftaran' => $idpendaftaran]);
        if ($scalaflaccugd->num_rows() > 0) {
            $data['scalaflaccugd'] = $scalaflaccugd->result_array();
        } else {
            $data['scalaflaccugd'] = null;
        }
        $data['identitas']      = $this->db->query("select pper.nik, namapasien(ppas.idperson) as pasien, date_format(pper.tanggallahir,'%d %M %Y') as tanggallahir, pp.norm, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.agama, pper.alamat from person_pendaftaran pp
            join person_pasien ppas on ppas.norm=pp.norm
            join person_person pper on pper.idperson=ppas.idperson
            where idpendaftaran='" . $idpendaftaran . "'")->row_array();
        $data['kriteria_start'] = $this->db->get("ugd_kriteria_start")->result_array();
        $data['kesadaran']      = $this->db->get('rs_kesadaran')->result_array();
        $data['scalaflacc']     = $this->db->order_by('pengkajian,nilai', 'asc')->get('rs_scalaflaccdibawahenamtahun')->result_array();
        echo json_encode($data);
    }

    //menampilkan detail barang pemeriksaan
    public function get_barangpemeriksaan_detail()
    {
        $idb      = $this->input->post('idb');
        $idunit   = $this->session->userdata('idunitterpilih');
        $idbarang = $this->input->post('idbarang');
        $query = "SELECT c.idunit, ifnull(a.jumlah,'0') as diberikan,b.idbarangpembelian, b.batchno, b.kadaluarsa, c.stok, b.idbarang
            FROM rs_barang_pembelian b
            join rs_barang_stok c on c.idbarangpembelian = b.idbarangpembelian and c.idunit = '" . $idunit . "'
            left join rs_barangpemeriksaan_detail a on a.idbarangpembelian = b.idbarangpembelian and a.idbarangpemeriksaan = '" . $idb . "'
            WHERE  b.idbarang = '" . $idbarang . "' and c.idunit = '" . $idunit . "' and c.stok > 0 ";
        $result = $this->db->query($query)->result_array();
        echo json_encode($result);
    }



    //detail pemberian obat ralan
    public function detailpemberianobatralan()
    {
        $data['idbarangpemeriksaan'] = $this->input->post('idbp');
        $data['idbarangpembelian']   = $this->input->post('idp');
        $data['idunit']              = $this->session->userdata('idunitterpilih');
        $data['jumlah']              = $this->input->post('jumlah');
        $data['idpendaftaran']       = $this->db->query("select idpendaftaran from rs_barangpemeriksaan where idbarangpemeriksaan='" . $this->input->post('idbp') . "'")->row_array()['idpendaftaran'];
        $arrMsg = $this->db->replace('rs_barangpemeriksaan_detail', $data);
        pesan_success_danger($arrMsg, 'Pemberian Obat Berhasil.', 'Pemberian Obat Gagal.', 'js');
    }


    //buat qrcode keterangan hasil laborat
    public function generateqrcode()
    {
        $qr = $this->generateciqrcode('RSU Queen Latifa Yogyakarta/1209789/Antigen Positif/verifikasi hasil pada 20/03/2021 18:90:21');
        echo $qr;
    }

    //menampilkan detail barang obat bebas
    public function get_barangobatbebas_detail()
    {
        $idb      = $this->input->post('idb');
        $idunit   = $this->session->userdata('idunitterpilih');
        $idbarang = $this->input->post('idbarang');
        $query = "SELECT c.idunit, ifnull(a.jumlah,'0') as diberikan,b.idbarangpembelian, b.batchno, b.kadaluarsa, c.stok, b.idbarang
            FROM rs_barang_pembelian b
            join rs_barang_stok c on c.idbarangpembelian = b.idbarangpembelian and c.idunit = '" . $idunit . "'
            left join rs_barang_pembelianbebas_detail a on a.idbarangpembelian = b.idbarangpembelian and a.idbarangpembelianbebas = '" . $idb . "'
            WHERE  b.idbarang = '" . $idbarang . "' and c.idunit = '" . $idunit . "' ";
        $result = $this->db->query($query)->result_array();
        echo json_encode($result);
    }
    //detail pemberian obat bebas
    public function detailpemberianobatbebas()
    {
        $data['idbarangpembelianbebas'] = $this->input->post('idbp');
        $data['idbarangpembelian']   = $this->input->post('idp');
        $data['idunit']              = $this->session->userdata('idunitterpilih');
        $data['jumlah']              = $this->input->post('jumlah');
        $data['idbarangpembelibebas'] = $this->db->query("select idbarangpembelibebas from rs_barang_pembelianbebas_detail where idbarangpembelianbebas='" . $this->input->post('idbp') . "'")->row_array()['idbarangpembelibebas'];
        $arrMsg = $this->db->replace('rs_barang_pembelianbebas_detail', $data);
        pesan_success_danger($arrMsg, 'Pemberian Obat Bebas Berhasil.', 'Pemberian Obat Bebas Gagal.', 'js');
    }

    //get data waktu hasil laborat
    public function get_waktuhasillaborat()
    {
        $id = $this->input->post('idpemeriksaan');
        $query = $this->db->query("select waktusedangproses as proses, waktuok as selesai from antrian_antrian where idpemeriksaan = '" . $id . "'")->row_array();
        echo json_encode($query);
    }

    //set waktu hasil laborat
    public function set_waktuhasillaborat()
    {
        $id = $this->input->post('idpemeriksaan');
        $ok = $this->input->post('ok');
        $proses = $this->input->post('proses');

        $arrMsg = $this->db->update('antrian_antrian', ['waktusedangproses' => $proses, 'waktuok' => $ok, 'ubahwaktu' => 1], ['idpemeriksaan' => $id]);
        pesan_success_danger($arrMsg, 'Ubah Waktu Hasil Berhasil.', 'Ubah Waktu Hasil Gagal.', 'js');
    }



    // Data Blacklist Pasien
    public function pasiendiblacklist()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAPASIENDIBLACKLIST)) {
            $data['content_view'] = 'pelayanan/v_datapasiendiblacklist';
            $data['active_menu']  = 'pelayanan';
            $data['active_menu_level'] = '';
            $data['active_sub_menu']   = 'datapasienblacklist';

            $data['title_page']  = 'Data Pasien Diblacklist';
            $data['mode']        = 'view';
            $data['script_js']   = ['js_pelayanan-datapasienblacklist'];
            $data['plugins']     = [PLUG_DATATABLE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_pasiendiblacklist()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DATAPASIENDIBLACKLIST)) {
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_blacklistpasien_(true);
            $data = [];
            if ($getData) {
                $no = $_POST['start'];
                foreach ($getData->result() as $obj) {
                    $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->norm;
                    $row[] = $obj->namalengkap;
                    $row[] = $obj->namaunit;
                    $row[] = $obj->alasan;
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_blacklistpasien(),
                "recordsFiltered" => $this->mdatatable->filter_dt_blacklistpasien(),
                "data" => $data
            );
            //output dalam format JSON
            echo json_encode($output);
        } else {
            aksesditolak();
        }
    }

    public function cari_sdki()
    {
        $diagnosa = $this->input->get('q');

        $query = $this->db->query("
        SELECT rs_master_sdki_childkategori.id_sdki_childkategori, CONCAT(rs_master_sdki_kategori.nama_kategori_sdki,'||',rs_master_sdki_subkategori.nama_subkategori_sdki,'||', rs_master_sdki_childkategori.kode_childkategori,'-',rs_master_sdki_childkategori.nama_childkategori) as diagnosasdki 
        FROM `rs_master_sdki_childkategori` 
        INNER JOIN rs_master_sdki_subkategori ON rs_master_sdki_childkategori.id_subkategori_sdki = rs_master_sdki_subkategori.id_subkategori_sdki 
        INNER JOIN rs_master_sdki_kategori ON rs_master_sdki_subkategori.id_kategori_sdki = rs_master_sdki_kategori.id_kategori_sdki 
        WHERE rs_master_sdki_childkategori.nama_childkategori like '%" . $diagnosa . "%' OR rs_master_sdki_childkategori.kode_childkategori like '%" . $diagnosa . "%'")->result_array();

        echo json_encode($query);
    }

    public function get_data_asesmen_awal_keperawatan_ralan()
    {
        $idp = $this->input->post('idp');
        $idperiksa = $this->input->post('ip');

        $query = $this->db->query("SELECT * FROM rs_ralan_asesmen_awal_keperawatan WHERE idpendaftaran = " . $idp)->result_array();

        echo json_encode($query);
    }

    public function save_risiko_tinggi()
    {
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $valcondition = $this->input->post('thisval');

        $query = $this->db->query("UPDATE rs_pemeriksaan SET is_risikotinggi =" . $valcondition . " WHERE idpemeriksaan = " . $idpemeriksaan);

        $res = [
            "result" => "success",
            "msg" => "UPDATE resiko berhasil!"
        ];

        echo json_encode($res);
    }

    public function caridokterugd()
    {
        $q = $this->input->get('q');
        $query = $this->db->query("select ppg.bpjs_kodedokter, ppg.idpegawai,ppg.nip,ppg.sip, ppg.statuskeaktifan,pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai from person_pegawai ppg inner join person_person pper on pper.idperson = ppg.idperson inner join person_grup_pegawai pgp on pgp.idgruppegawai = ppg.idgruppegawai left join person_grup_profesi pgp2 on pgp2.idprofesi = ppg.idprofesi  where namalengkap like '%" . $q . "%' ORDER BY `pgp`.`namagruppegawai`")->result_array();

        echo json_encode($query);
    }

    public function caridokterugdbyid()
    {
        $q = $this->input->post('q');
        $query = $this->db->query("select ppg.bpjs_kodedokter, ppg.idpegawai,ppg.nip,ppg.sip, ppg.statuskeaktifan,pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai from person_pegawai ppg inner join person_person pper on pper.idperson = ppg.idperson inner join person_grup_pegawai pgp on pgp.idgruppegawai = ppg.idgruppegawai left join person_grup_profesi pgp2 on pgp2.idprofesi = ppg.idprofesi  where ppg.idpegawai = " . $q . " ORDER BY `pgp`.`namagruppegawai`")->result_array();

        echo json_encode($query);
    }

    public function simpanFotoPemeriksaanFisik()
    {
        $idpendaftaran = $_POST['idpendaftaran'];
        # Count each of uploaded file
        $count_uploaded_pemeriksaanfisik = empty($_FILES['pemeriksaanfisik']['name']) ? 0 : count($_FILES['pemeriksaanfisik']['name']);

        $files = $_FILES;
        $namaberkaspemeriksaanfisik = [];

        $check = false;
        if ($count_uploaded_pemeriksaanfisik != 0) {
            for ($i = 0; $i < $count_uploaded_pemeriksaanfisik; $i++) {
                $_FILES['pemeriksaanfisik'] =
                    [
                        "name" => $files['pemeriksaanfisik']['name'][$i],
                        'type'     => $files['pemeriksaanfisik']['type'][$i],
                        'tmp_name' => $files['pemeriksaanfisik']['tmp_name'][$i],
                        'error'    => $files['pemeriksaanfisik']['error'][$i],
                        'size'     => $files['pemeriksaanfisik']['size'][$i]
                    ];

                $tmpName        = $files['pemeriksaanfisik']['tmp_name'][$i];
                $fileName       = date('D') . rand(1000, 9999) . '-' . str_replace('"', '', str_replace("'", '', $files['pemeriksaanfisik']['name'][$i]));
                $newFileName    = '/pemeriksaan_fisik/' . $fileName;

                $namaberkaspemeriksaanfisik[] = $fileName;

                $config = $this->ftp_upload();

                # upload ke nas storage
                $upload = $this->ftp->connect($config);
                $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS . $newFileName, 'ascii', 0775);
                $upload = $this->ftp->close();


                $data = [
                    "id_pendaftaran" => $idpendaftaran,
                    "nama_file" => $fileName
                ];

                $query  = $this->db->insert('rs_pengkajian_awal_medis_file_pemeriksaan_fisik', $data);

                $check = true;
            }
        }

        $result = '';
        if ($check == true) {
            $result = [
                "status" => "Success",
                "msg" => "Upload Gambar Pemeriksaan File Sukses!"
            ];
        } else {
            $result = [
                "status" => "Error",
                "msg" => "Upload Gambar Pemeriksaan File Gagal!"
            ];
        }

        echo json_encode($result);
    }

    public function get_data_upload_pemeriksaan_fisik()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');

        $query = $this->db->query("SELECT * FROM `rs_pengkajian_awal_medis_file_pemeriksaan_fisik` WHERE id_pendaftaran =" . $idpendaftaran)->result_array();

        echo json_encode($query);
    }

    public function hapusfileuploadpemeriksaanklinik()
    {
        $idberkas = $this->input->post('idberkas');
        $namaberkas = $this->input->post('namaberkas');

        $query = $this->db->query("DELETE FROM rs_pengkajian_awal_medis_file_pemeriksaan_fisik WHERE id_file_pemeriksaan_fisik =" . $idberkas);


        $pathname = "/sirstql/pemeriksaan_fisik/";
        $urlnas = $pathname . $namaberkas;

        // hapus file di NAS
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);

        $delete = $this->ftp->delete_file($urlnas);
        $delete = $this->ftp->close();


        $result = [
            "status" => "Success",
            "msg" => "Hapus Berkas Selesai"
        ];

        echo json_encode($result);
    }

    public function pilihSDKI()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $valuediagnosa = $this->input->post('valuediagnosa');

        $data = [
            "id_pendaftaran" => $idpendaftaran,
            "id_sdki_childkategori" => $valuediagnosa
        ];

        $query = $this->db->insert("rs_pilih_sdki", $data);

        if ($query) {
            echo json_encode(["result" => "Sukses", "msg" => "Berhasil Menambahankan"]);
        } else {
            echo json_encode(["result" => "Gagal", "msg" => "Gagal Menambahkan"]);
        }
    }

    public function get_data_sdki()
    {
        $idp = $this->input->post('idp');

        $query = $this->db->query("SELECT * FROM rs_pilih_sdki
        INNER JOIN rs_master_sdki_childkategori ON rs_master_sdki_childkategori.id_sdki_childkategori = rs_pilih_sdki.id_sdki_childkategori
        INNER JOIN rs_master_sdki_subkategori ON rs_master_sdki_subkategori.id_subkategori_sdki = rs_master_sdki_childkategori.id_subkategori_sdki
        INNER JOIN rs_master_sdki_kategori ON rs_master_sdki_kategori.id_kategori_sdki = rs_master_sdki_subkategori.id_kategori_sdki
        WHERE rs_pilih_sdki.id_pendaftaran =" . $idp)->result_array();

        echo json_encode($query);
    }

    public function hapus_diagnosa_sdki()
    {
        $idpilihdiagnosa = $this->input->post('idpilihdiagnosa');

        $query = $this->db->query("DELETE FROM rs_pilih_sdki WHERE id_pilih_diagnosa = " . $idpilihdiagnosa);

        if ($query) {
            echo json_encode(["result" => "Sukses", "msg" => "Berhasil Menghapus"]);
        } else {
            echo json_encode(["result" => "Gagal", "msg" => "Gagal Menghapus"]);
        }
    }

    public function get_data_file_elektromedik()
    {
        $post = $this->input->post();

        $query = $this->msqlbasic->show_db('rs_pengkajian_awal_medis_file_elektromedik', ["id_pendaftaran" => $post['idpendaftaran']]);

        echo json_encode($query);
    }

    public function upload_gambar_elektromedik()
    {
        $idpendaftaran = $_POST['idpendaftaran'];
        # Count each of uploaded file
        $count_uploaded_fileelektromedik = empty($_FILES['fileelektomedik']['name']) ? 0 : count($_FILES['fileelektomedik']['name']);

        $files = $_FILES;
        $namaberkasfileelektromedik = [];

        $check = false;
        if ($count_uploaded_fileelektromedik != 0) {
            for ($i = 0; $i < $count_uploaded_fileelektromedik; $i++) {
                $_FILES['fileelektomedik'] =
                    [
                        "name" => $files['fileelektomedik']['name'][$i],
                        'type'     => $files['fileelektomedik']['type'][$i],
                        'tmp_name' => $files['fileelektomedik']['tmp_name'][$i],
                        'error'    => $files['fileelektomedik']['error'][$i],
                        'size'     => $files['fileelektomedik']['size'][$i]
                    ];

                $tmpName        = $files['fileelektomedik']['tmp_name'][$i];
                $fileName       = date('D') . rand(1000, 9999) . '-' . str_replace('"', '', str_replace("'", '', $files['fileelektomedik']['name'][$i]));
                $newFileName    = '/asesmen_elektromedik/' . $fileName;

                $namaberkasfileelektromedik[] = $fileName;

                $config = $this->ftp_upload();

                # upload ke nas storage
                $upload = $this->ftp->connect($config);
                $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS . $newFileName, 'ascii', 0775);
                $upload = $this->ftp->close();


                $data = [
                    "id_pendaftaran" => $idpendaftaran,
                    "nama_file" => $fileName
                ];

                $query  = $this->db->insert('rs_pengkajian_awal_medis_file_elektromedik', $data);

                $check = true;
            }
        }

        $result = '';
        if ($check == true) {
            $result = [
                "status" => "Success",
                "msg" => "Upload File Elektromedik Sukses!"
            ];
        } else {
            $result = [
                "status" => "Error",
                "msg" => "Upload File Elektromedik Gagal!"
            ];
        }

        echo json_encode($result);
    }

    public function delete_file_elektromedik()
    {
        $idberkas = $this->input->post('idberkas');
        $namaberkas = $this->input->post('namaberkas');

        $query = $this->db->query("DELETE FROM rs_pengkajian_awal_medis_file_elektromedik WHERE id_file_elektromedik  =" . $idberkas);


        $pathname = "/sirstql/asesmen_elektromedik/";
        $urlnas = $pathname . $namaberkas;

        // hapus file di NAS
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);

        $delete = $this->ftp->delete_file($urlnas);
        $delete = $this->ftp->close();


        $result = [
            "status" => "Success",
            "msg" => "Hapus Berkas Elektromedik Sukses"
        ];

        echo json_encode($result);
    }

    public function get_file_menolak_tindakan()
    {
        $post = $this->input->post();

        $query = $this->msqlbasic->show_db('rs_pengkajian_awal_medis_file_menolak_tindakan', ["id_pendaftaran" => $post['idpendaftaran']]);

        echo json_encode($query);
    }

    public function upload_file_menolak_tindakan()
    {
        $idpendaftaran = $_POST['idpendaftaran'];

        $updaterencanaperawatan = $this->msqlbasic->update_db('rs_pengkajian_awal_medis',["idpendaftaran" => $idpendaftaran],["textrencanaperawatandantindakanselanjutanya" => "Menolak Tindakan Rawat Inap", "rencanaperawatandantindakanaselanjutnya" => 4]);
        
        # Count each of uploaded file
        $count_uploaded_fileelektromedik = empty($_FILES['filemenolaktindakan']['name']) ? 0 : count($_FILES['filemenolaktindakan']['name']);

        $files = $_FILES;
        $namaberkasfileelektromedik = [];

        $check = false;
        if ($count_uploaded_fileelektromedik != 0) {
            for ($i = 0; $i < $count_uploaded_fileelektromedik; $i++) {
                $_FILES['filemenolaktindakan'] =
                    [
                        "name" => $files['filemenolaktindakan']['name'][$i],
                        'type'     => $files['filemenolaktindakan']['type'][$i],
                        'tmp_name' => $files['filemenolaktindakan']['tmp_name'][$i],
                        'error'    => $files['filemenolaktindakan']['error'][$i],
                        'size'     => $files['filemenolaktindakan']['size'][$i]
                    ];

                $tmpName        = $files['filemenolaktindakan']['tmp_name'][$i];
                $fileName       = date('D') . rand(1000, 9999) . '-' . str_replace('"', '', str_replace("'", '', $files['filemenolaktindakan']['name'][$i]));
                $newFileName    = '/asesmen_menolak_tindakan/' . $fileName;

                $namaberkasfileelektromedik[] = $fileName;

                $config = $this->ftp_upload();

                # upload ke nas storage
                $upload = $this->ftp->connect($config);
                $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS . $newFileName, 'ascii', 0775);
                $upload = $this->ftp->close();


                $data = [
                    "id_pendaftaran" => $idpendaftaran,
                    "nama_file" => $fileName
                ];

                $query  = $this->db->insert('rs_pengkajian_awal_medis_file_menolak_tindakan', $data);

                $check = true;
            }
        }

        $result = '';
        if ($check == true) {
            $result = [
                "status" => "Success",
                "msg" => "Upload File Menolak Tindakan Sukses!"
            ];
        } else {
            $result = [
                "status" => "Error",
                "msg" => "Upload File Menolak Tindakan Gagal!"
            ];
        }

        echo json_encode($result);
    }

    public function hapus_file_menolak_tindakan()
    {
        $post = $this->input->post();
        $idberkas = $this->input->post('idberkas');
        $namaberkas = $this->input->post('namaberkas');

        $conditiondelete = isset($post['idpendaftaran']) ? "keseluruhan": "peritem";

        if($conditiondelete == "peritem")
        {
            $query = $this->db->query("DELETE FROM rs_pengkajian_awal_medis_file_menolak_tindakan WHERE id_file_menolak_tindakan   =" . $idberkas);
    
            $pathname = "/sirstql/asesmen_menolak_tindakan/";
            $urlnas = $pathname . $namaberkas;
    
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect($config);
    
            $delete = $this->ftp->delete_file($urlnas);
            $delete = $this->ftp->close();
    
    
            $result = [
                "status" => "Success",
                "msg" => "Hapus Berkas Menolak Tindakan Sukses"
            ];
    
            echo json_encode($result);
        }
        else if($conditiondelete == "keseluruhan")
        {
            $getallnamefile = $this->msqlbasic->show_db('rs_pengkajian_awal_medis_file_menolak_tindakan',["id_pendaftaran" => $post['idpendaftaran']]);

            $updaterencanaperawatan = $this->msqlbasic->update_db('rs_pengkajian_awal_medis',["idpendaftaran" => $post['idpendaftaran']], ["textrencanaperawatandantindakanselanjutanya" => null, "rencanaperawatandantindakanaselanjutnya" => null]);

            foreach($getallnamefile as $item)
            {
                $namaberkas = $item['nama_file'];

                $pathname = "/sirstql/asesmen_menolak_tindakan/";
                $urlnas = $pathname . $namaberkas;
        
                // hapus file di NAS
                $config = $this->ftp_upload();
                $delete = $this->ftp->connect($config);
        
                $delete = $this->ftp->delete_file($urlnas);
                $delete = $this->ftp->close();
        
            }

            $querydelete = $this->db->query("DELETE FROM rs_pengkajian_awal_medis_file_menolak_tindakan WHERE id_pendaftaran   =" . $post['idpendaftaran']);
    
    
            $result = [
                "status" => "Success",
                "msg" => "Hapus Berkas Menolak Tindakan Sukses"
            ];
    
            echo json_encode($result);
        }
        
    }

    public function cek_skdp_terdaftar()
    {
        $post           = $this->input->post();
        $idperiksa      = $post['idperiksa'];
        $idpendaftaran  = $post['idp'];

        $tglperiksa = $this->db->query("SELECT date(waktuperiksa) as tglperiksa FROM person_pendaftaran WHERE idpendaftaran = '".$idpendaftaran."' ORDER BY idpendaftaran DESC ")->row()->tglperiksa;
        $query_skdp = $this->db->query("SELECT * FROM rs_skdp WHERE idpendaftaransebelum='".$idpendaftaran."' AND tanggal='".$tglperiksa."'" )->result();

        if( !empty($query_skdp) ){
            $skdp_yang_dibuat = '<p style="margin:10px 0px 0px 0px;font-size: 12px;"> SKDP Terbit ('.count($query_skdp).') : </p>';
            $skdp_yang_dibuat .= '<ul class="list-skdp" style="font-size: 12px;font-style: italic;font-weight: bold;">';
            foreach( $query_skdp as $key => $row ){
                $skdp_yang_dibuat .= '<li> No SKDP : '.$row->noskdpql.', No Kontrol : '.$row->nokontrol.' ('.$row->tanggalskdp.')</li>';
            }
            $skdp_yang_dibuat .= '</ul>';
        }else{
            $skdp_yang_dibuat = '';
        }

       echo json_encode(['hsl'=>$skdp_yang_dibuat]);

    }
}
/* End of file ${TM_FILENAME:${1/(.+)/Cpelayanan.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cpelayanan/:application/controllers/${1/(.+)/Cpelayanan.php/}} */
