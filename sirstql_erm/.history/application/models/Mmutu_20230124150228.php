<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmutu extends CI_Model 
{
    protected $tb_mutu_indikator            = 'mutu_indikator';
    protected $tb_mutu_pendataan_indikator  = 'mutu_pendataan_indikator';
    protected $tb_mutu_hasil                = 'mutu_hasil';
    protected $tb_mutu_analisarencana       = 'mutu_analisarencana';
    protected $tb_mutu_pengaturan           = 'mutu_pengaturan';

    public function get_listdaftarimutrs($bulan,$tahun)
    {
        $sql = "SELECT id_indikator,tanggal,judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        // $sql .= " WHERE Month(tanggal) = '".$bulan."' AND YEAR(tanggal) ='".$tahun."' ";

        return $this->db->query($sql)->result();
    }

    public function get_listrekapimutrs($tahun)
    {
        $sql = "SELECT id_indikator,tanggal,judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        // $sql .= " WHERE YEAR(tanggal) ='".$tahun."' ";

        return $this->db->query($sql)->result();
    }

    public function get_editindikator($id)
    {
        $sql = "SELECT id_indikator, judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        $sql .= " WHERE id_indikator = '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_editpendataan($id)
    {
        $sql = "SELECT id_pendataan,hasil, indikator_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,indikator,satuan ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator ;
        $sql .= " WHERE indikator_id = '".$id."'";

        return $this->db->query($sql)->result();
    }

    public function get_editpendataan_row($id)
    {
        $sql = "SELECT id_pendataan,hasil, indikator_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,indikator,satuan ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator ;
        $sql .= " WHERE indikator_id = '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_hasil_mutu($id_indikator,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND  (b.bulan BETWEEM 1 AND '".$bulan."') AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND (b.bulan BETWEEM 1 AND '".$bulan."') AND b.tahun='".$tahun."' ";
        }

        // return $sql;
        return $this->db->query($sql)->result();
    }

    public function get_hasil_mutu_row($id_pendataan,$id_indikator,$author)
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND b.pendataan_id ='".$id_pendataan."' ";
        
        return $this->db->query($sql)->row();
    }

    public function get_hasil_byidpendataan($idpendataan,$author,$bulan,$tahun)
    {
        $sql = " SELECT * FROM ".$this->tb_mutu_hasil ." WHERE pendataan_id ='".$idpendataan."' AND author='".$author."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";
        return $this->db->query($sql)->row();
    }

    public function get_hasil_rekap($id_indikator)
    {
        $sql = " SELECT * FROM ".$this->tb_mutu_hasil ." WHERE indikator_id ='".$id_indikator."' ";
        return $this->db->query($sql)->result();
    }

    public function get_analisarencana_row($id_indikator,$author,$bulan,$tahun)
    {   
        $where = [
            'indikator_id'=>$id_indikator,
            'author'    => $author,
            'bulan'     => $bulan,
            'tahun'     => $tahun
        ];

        $this->db->select('*');
        $this->db->from($this->tb_mutu_analisarencana);
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function get_list_detailanalisarencana()
    {
        $post           = $this->input->post();
        $id_indikator   = $this->security->sanitize_filename($post['id_indikator']);
        
        $filterindikatormutu = $post['filterindikatormutu'];
        $array_data = explode('-', $filterindikatormutu);
        
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        $this->db->select('*');
        $this->db->from($this->tb_mutu_analisarencana);
        $this->db->where(['indikator_id'=>$id_indikator,'bulan'=>$bulan,'tahun'=>$tahun]);
        return $this->db->get()->result();
        // return $this->db->last_query();

    }

    public function get_nama_unit_pengaturanmutu($userid)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturan);
        $this->db->where(['userid_akses'=>$userid]);
        $query = $this->db->get()->row();
        
        $nama_unit = ( !empty($query) ) ? $query->unit : '';

        return $nama_unit;
    }

    public function get_pengaturan_indikator_row($indikator_id)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturan);
        $query = $this->db->get()->result();
        
        foreach( $query as $row ){
            $indikator_arr = explode(",",$row->indikator_id);
            $key = array_search($indikator_id,$indikator_arr);
            
            unset($indikator_arr[$key]);

            $im_indikatorid = ( empty($indikator_arr) ) ? 0 : implode(',',$indikator_arr);
            
            $this->db->where(['id_pengaturan'=>$row->id_pengaturan]);
            $this->db->update($this->tb_mutu_pengaturan, ['indikator_id'=>$im_indikatorid]);
        }
    }


}
?>