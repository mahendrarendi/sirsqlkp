<?php
class Mevelin extends CI_Model
{
    public function get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit=""){
        $this->db->select('a.nosep,a.idunit,a.waktuperiksa,a.idpendaftaran,a.jenispemeriksaan,b.nojkn,a.jenisrujukan');
        $this->db->from('person_pendaftaran a');
        $this->db->join('person_pasien b', 'b.norm = a.norm');
        $this->db->where('a.norm',$norm);
        $this->db->where('date(a.waktuperiksa)',$tglperiksa);
        if( !empty($idunit) ){
            $this->db->where('a.idunit',$idunit);
        }
        $this->db->where('a.idstatuskeluar',2); //selesai
        $this->db->order_by("a.idpendaftaran", "desc");
        return $this->db->get()->row();
    }

    public function get_poli(){
        return $this->db->get_where('rs_unit',['levelgudang'=>'unit'])->result();
    }

    public function vlcaim_cetakversiranap($idpendaftaran,$jenis)
    {
        $CI = $this->CI();
        $this->load->model('mviewql');

        $data =[]; 
        $data['jenis']         = $jenis; 
        $data['lamaRanap']     = $this->db->query('SELECT * FROM rs_inap_biayanonpemeriksaan WHERE idpendaftaran='.$idpendaftaran.' AND idjenistarif=19')->num_rows(); // Andri - bugfix [0000590]
        $data['infotagihan']   = $CI->mviewql->viewinfotagihan($idpendaftaran,'ranap');
        $data['tanggalralan']  = $this->db->select('date(waktuperiksa) as tanggal')->get_where('person_pendaftaran',['idpendaftaran'=>$idpendaftaran])->row_array();
        $data['listtagihan']   = $CI->mviewql->view_tarif_byjenistarifranap($idpendaftaran);
        $data['ttd']           = 'Yogyakarta, '.date('d/m/Y'); 
        
        return $data;
    }

    public function vclaim_cetaklangsung($idpendaftaran,$jenis)
    {
        $CI = $this->CI();
        $this->load->model('mviewql');

        $modelist = 'klaim';
        $query_info = $this->mviewql->viewinfotagihan($idpendaftaran);

        $this->db->select('lu.namauser');
        $this->db->from('keu_tagihan kt');
        $this->db->join('login_user lu','lu.iduser = kt.iduserkasir','left');
        $this->db->where('kt.idtagihan',$query_info[0]->idtagihan);
        $query_user = $this->db->get()->row_array();
        
        
        $data = [];
        $data['jenis']         = $jenis;
        $data['infotagihan']   = $query_info;
        $data['detailtagihan'] = $this->mviewql->viewtotaltarifpemeriksaan($idpendaftaran,$modelist,$jenis);
        $data['userkasir']     = $query_user['namauser'];
        return $data;
    }

    public function CI()
    {
        return $CI =& get_instance();
    }

    public function testget_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit=""){
        $this->db->select('a.nosep,a.idunit,a.waktuperiksa,a.idpendaftaran,a.jenispemeriksaan,b.nojkn,a.jenisrujukan');
        $this->db->from('person_pendaftaran a');
        $this->db->join('person_pasien b', 'b.norm = a.norm');
        $this->db->where('a.norm',$norm);
        $this->db->where('date(a.waktuperiksa)',$tglperiksa);
        if( !empty($idunit) ){
            $this->db->where('a.idunit',$idunit);
        }
        $this->db->where('a.idstatuskeluar',2); //selesai
        return $this->db->get()->row();

        // $data = ['norm'=>$norm,'tglperiksa'=>$tglperiksa,'idunit'=>$idunit];
        // return $data;
    }
}

?>