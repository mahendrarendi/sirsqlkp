<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Mbpjs extends CI_Model
{
    public function _get_query_datatable_databulanan(){
      
        $post           = $this->input->post();
        $tanggalawal    = $post['tanggalawal'];
        $tanggalakhir   = $post['tanggalakhir'];

        $query = $this->db->select("ppd.jenisrujukan,ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
        $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
        $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
        $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
        $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
        $query = $this->db->group_start();
            $query = $this->db->where('ppd.carabayar','jknpbi');
            $query = $this->db->or_where('ppd.carabayar','jknnonpbi');
        $query = $this->db->group_end();
            
        $query = $this->db->where('ppd.idstatuskeluar = "2" ');
                    
        $query = $this->db->where('date(ppd.waktuperiksa) >=', $tanggalawal);
        $query = $this->db->where('date(ppd.waktuperiksa) <=', $tanggalakhir);

        $query = $this->db->group_by('ppd.idpendaftaran');
        $query = $this->db->get('person_pendaftaran ppd');

        return $query;
    }

    public function get_dt_bulanan($limit = false)
    {
    
        $post           = $this->input->post();

        $start          = $post['start'];
        $length         = $post['length'];

        $search         = $post['search'];
        $value          = $search['value'];
        $regex          = $search['regex'];

        $column = [
            'ppd.idpendaftaran',
            'ppd.norm',
            'pper.namalengkap', 
            'pper.notelpon',
            'ppd.waktu',
            'rp.waktuperiksadokter',
            'ppd.caradaftar',
            'ru.namaunit',
            'ppd.jenispemeriksaan',
            'ppd.carabayar',
            'tglperiksa',
        ];

        ($regex) ? $this->db->like($column,$search) : '';

        $this->db->order($column);
        
        if($limit){
            $this->db->limit($length,$start);
        }
        
        return $this->_get_query_datatable_databulanan();   


    }

    public function count_filtered()
    {
        $query = $this->_get_query_datatable_databulanan();
        return $query->num_rows();
    }

    public function count_all()
    {
        $query = $this->_get_query_datatable_databulanan();
        // return $this->db->count_all_results();
        $results = $query->result();
        return $query->num_rows();

    }
}
?>