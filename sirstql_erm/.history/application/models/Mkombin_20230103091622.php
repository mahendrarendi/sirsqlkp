<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mkombin extends CI_Model 
{ 

	public function like_datatable($column_search=[],$post_search)
    {
        $i = 0;
        if(empty(!$column_search))
        {
            foreach ($column_search as $item)
            {
                if(isset($post_search))
                {
                    if($i===0)
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $post_search);
                    }
                    else
                    {
                        $this->db->or_like($item, $post_search);
                    }

                    if(count($column_search) - 1 == $i) 
                    $this->db->group_end(); 
                }
                $i++;
            }
        }
    }

	public function order_datatable($column_order=[])
	{
		if(isset($_POST['order'])) 
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
	}

	public function limit_datatable($length, $start)
	{
		if($length != -1){$this->db->limit($length, $start);}
	}
	
	// INSERT REPLACE DUPLICATE DATA
	public function insert_replaceonduplicate($table,$data)
    {
        $this->mgenerikbap->setTable($table);
        return $this->mgenerikbap->insert_replaceonduplicate($data);
    }
	// --- query menampilkan data pendaftaran pasien bpjs dimana berkas belum ditemukan atau belum diverifikasi
	public function loaddtverifpasienbpjs($tanggal1='',$tanggal2='')
	{
		(empty($tanggal1)? $tanggal1= date('Y-m-d') : $tanggal1 = $tanggal1);
		(empty($tanggal2)? $tanggal2= date('Y-m-d') : $tanggal2 = $tanggal2);
		$query = $this->db->query("SELECT rp.status,ppd.isverif,ppd.resumeprint,ppd.caradaftar,(select ifnull((jenistagihan='jknnonpbi' || jenistagihan='jknpbi' || dibayar>0),0) from keu_tagihan where idpendaftaran=ppd.idpendaftaran limit 1) as isdibayar, (select ispemeriksaanselesaisemua(ppd.idpendaftaran) from keu_tagihan where idpendaftaran=ppd.idpendaftaran limit 1) as isselesaisemua, (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, rp.idpemeriksaan, pp.idperson, ppd.idunit, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, `ppd`.`idpendaftaran`, `ppd`.`norm`, `ppd`.`waktu`, ppd.waktuperiksa, ppd.idstatuskeluar, `ppd`.`carabayar`, `ppd`.`norujukan`, `ppd`.`nosep`, `ppd`.`jenispemeriksaan`, `ru`.`namaunit`, ppd.isantri as noantrian  FROM person_pendaftaran ppd ,rs_unit ru ,person_pasien ppas ,person_person pp ,rs_pemeriksaan rp WHERE ru.idunit= ppd.idunit and ru.tipeunit='UTAMA' and pp.idperson = ppas.idperson and ppas.norm = ppd.norm and rp.idpendaftaran = ppd.idpendaftaran and ppd.carabayar!='mandiri' and ppd.carabayar!='' and date(rp.waktu) between '$tanggal1' and '$tanggal2' order by waktu desc")->result();
		return $query;
	}
	// -- query menampilkan data pendaftaran pasien bpjs dimana berkas belum ditemukan atau belum diverifikasi
	public function loaddtarusrekamedis($tanggal1='',$tanggal2='',$status='')
	{ 
		(empty($tanggal1)? $tanggal1= date('Y-m-d') : $tanggal1 = $tanggal1);
		(empty($tanggal2)? $tanggal2= date('Y-m-d') : $tanggal2 = $tanggal2);
		(empty($status)? $status= '' : $status = "and ppd.isberkassudahada='".$status."'");
		$query = $this->db->query("SELECT rp.status,ppd.isverif,ppd.caradaftar,ppd.isberkassudahada, (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, rp.idpemeriksaan, pp.idperson, ppd.idunit, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, `ppd`.`idpendaftaran`, `ppd`.`norm`, `ppd`.`waktu`, ppd.idstatuskeluar, `ppd`.`carabayar`, `ppd`.`norujukan`, `ppd`.`nosep`, `ppd`.`jenispemeriksaan`, `ru`.`namaunit`, ppd.isantri as noantrian  FROM person_pendaftaran ppd ,rs_unit ru ,person_pasien ppas ,person_person pp ,rs_pemeriksaan rp WHERE date(ppd.waktuperiksa) BETWEEN '$tanggal1' and '$tanggal2' ".$status." and ru.idunit= ppd.idunit and pp.idperson = ppas.idperson and ppas.norm = ppd.norm and rp.idpendaftaran = ppd.idpendaftaran and date(rp.waktu) between '$tanggal1' and '$tanggal2' order by waktu desc")->result();
		return $query;
	}
	// -- query menampilkan data pendaftaran hari ini
	public function loaddtpendaftaran($tgl1,$tgl2)
	{
		((empty($tgl1))?$tglawal=date('Y-m-d'):$tglawal=$tgl1);
		((empty($tgl2))?$tglakhir=date('Y-m-d'):$tglakhir=$tgl2);
		$query = $this->db->query("SELECT pp.nik, pp.tanggallahir, pp.alamat, pp.notelpon, pp.cetakkartu, rp.status, ppd.isberkassudahada,ppd.caradaftar,ppd.isantri, (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, rp.idpemeriksaan, pp.idperson, ppd.idunit, concat(ifnull(pp.identitas,''),' ', ifnull(pp.namalengkap,'')) as namalengkap, `ppd`.`idpendaftaran`, `ppd`.`norm`, `ppd`.`waktu`,ppd.waktuperiksa, ppd.idstatuskeluar, `ppd`.`carabayar`, `ppd`.`norujukan`, `ppd`.`nosep`, `ppd`.`jenispemeriksaan`, if(ifnull(ppd.idjadwalgrup,'0')='0',`ru`.`namaunit`,(select namaunit from rs_unit where idjadwalgrup=rj.idjadwalgrup)) as namaunit, ppd.isantri as noantrian  FROM person_pendaftaran ppd ,rs_unit ru ,person_pasien ppas ,person_person pp ,rs_pemeriksaan rp, rs_jadwal rj
                WHERE ru.idunit= ppd.idunit and pp.idperson = ppas.idperson and ppas.norm = ppd.norm and rp.idpendaftaran = ppd.idpendaftaran  and rj.idjadwal=rp.idjadwal and  date(ppd.waktu) between '".$tglawal."' and '".$tglakhir."' OR ru.idunit= ppd.idunit and pp.idperson = ppas.idperson and ppas.norm = ppd.norm and rp.idpendaftaran = ppd.idpendaftaran and rj.idjadwal=rp.idjadwal and date(rp.waktu) between '".$tglawal."' and '".$tglakhir."' GROUP by ppd.idpendaftaran order by waktu desc")->result();
		return $query;
	}
	public function diagnosa_cekdiagnosa($idpendaftaran, $icd) //cek diagnosa sebelum simpan
	{
            $post = $this->input->post();
            $modesave = ((isset($post['modesave'])) ? $post['modesave'] : '' );
            $wheremodesave = (($modesave=='verifikasi') ? ' and isverifklaim=1 ' : ' and istagihan=1 ' );
            $query =  $this->db->query("SELECT icd FROM rs_hasilpemeriksaan WHERE idpendaftaran='$idpendaftaran' AND icd='$icd'".$wheremodesave);
            return $query;
	}
	public function diagnosa_getpaketdiagnosa($idpaket) //ambil data paket diagnosa
	{
		$query = $this->db->query("select * from rs_icd ri join rs_paket_pemeriksaan_detail rpd on rpd.icd = ri.icd WHERE ri.icd = rpd.icd and rpd.idpaketpemeriksaan='$idpaket'");
		if($query->num_rows() > 0){ return $query->result();}
		else{return NULL;}
	}
	public function diagnosa_getpaketdiagnosaparent($idpaket)
	{
		$query = $this->db->query("select rpp.idpaketpemeriksaan from rs_paket_pemeriksaan rpp where rpp.idpaketpemeriksaanparent='$idpaket'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        public function cekdiagnosaprimer($idpemeriksaan)
        {
           $post = $this->input->post();
           $modelist = ((isset($post['modelist'])) ? $post['modelist'] : '' );
           $wheremodelist = (($modelist='verifikasi') ? ' and isverifklaim=1' : ' and istagihan=1' );
           return $this->db->query("select * from rs_hasilpemeriksaan rh join rs_icd ri on ri.icd = rh.icd where idpemeriksaan ='".$idpemeriksaan."' and ri.idicd='".$post['idicd']."' and icdlevel='primer' ".$wheremodelist);
        }
	public function hasilperiksa_selecticd10($idpendaftaran)//pilih icd 10 hasil periksa
	{
		$query = $this->db->query("SELECT icd FROM rs_hasilpemeriksaan WHERE idpendaftaran='$idpendaftaran' AND remark='icd10'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function hasilperiksa_selecticd9($idpendaftaran)//pilih icd9 hasil periksa
	{
		$query = $this->db->query("SELECT icd FROM rs_hasilpemeriksaan WHERE idpendaftaran='$idpendaftaran' AND remark='icd9'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	
	public function pemeriksaan_cek_rs_barangpemeriksaan($idbarang,$idpendaftaran,$jenisrawat)
	{
		$query = $this->db->query("SELECT idbarang from rs_barangpemeriksaan WHERE jenisrawat='$jenisrawat' and idbarang='$idbarang' AND idpendaftaran='$idpendaftaran'");
                if($query->num_rows() > 0)
                {
                        return $query->row_array();
                }
                else
                {
                        return NULL;
                }
	}
	// set tarif otomatis grup pemeriksaan
	public function settarifgrupperiksa($idpr,$idg)
	{
		$sql = "insert into rs_hasilpemeriksaan (idpendaftaran, idpemeriksaan, icd, idjenistarif, jasaoperator, nakes, jasars, bhp, akomodasi, margin, sewa, jumlah) select idpendaftaran, idpemeriksaan, icd, idjenistarif, jasaoperator, nakes, jasars, bhp, akomodasi, margin, sewa, 1 as jumlah FROM rs_pemeriksaan p, rs_jadwal_grup g, rs_mastertarif m WHERE idpemeriksaan='$idpr' and g.idjadwalgrup='$idg' and m.icd=g.icdtarifpemeriksaan and m.idkelas='1'";
		return $this->db->query($sql);
	}
	public function pemeriksaan_listbhp($idpendaftaran,$modelist,$jenisrawat='',$jenispemakaian='')
	{
            $jenisrawat     = empty($jenisrawat) ? 'rajal' : $jenisrawat;
            $wheremodelist  = (($modelist=='verifikasi') ? '  and b.isverifklaim=1 ' : ' and b.istagihan=1 ' );
            $where_jenispemakaian = empty($jenispemakaian) ? "" :  " and b.jenispemakaian='".$jenispemakaian."' ";
            
            // $query = $this->db->query("SELECT "
            //         . "ifnull((select kadaluarsa from rs_barang_pembelian rbp where rbp.idbarangpembelian=b.idbarangpembelian),0) as kadaluarsa, "
            //         . "(select jumlahdiberikan from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikDiberikan,"
            //         . "(select jumlah from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikan,"
            //         . "(select g.idkemasan from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as idkemasan,"
            //         . "(select k.kemasan from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as kemasan, 
            //             rba.signa,rba.pemakaian, rb.kekuatan, b.penggunaan, b.kekuatan as kekuatanperiksa, rb.hargabeli, rb.idbarang, b.idbarangpemeriksaan, b.jumlahpemakaian, b.harga, b.total, b.grup, rb.namabarang, rb.jenis, b.jumlahaturanpakai, b.jumlahdiresepkan, b.idbarangaturanpakai, b.jaminanasuransi, b.jenisinput, b.idbarangpemeriksaan
            //             FROM rs_barangpemeriksaan b 
            //             JOIN rs_barang rb ON rb.idbarang = b.idbarang 
            //             LEFT JOIN rs_barang_aturanpakai rba on rba.idbarangaturanpakai = b.idbarangaturanpakai
            //             WHERE b.jenisrawat='$jenisrawat' and b.idpendaftaran='$idpendaftaran' ".$wheremodelist.$where_jenispemakaian." order by grup");
            
            $sql = "SELECT ";
            $sql .= " ifnull((select kadaluarsa from rs_barang_pembelian rbp where rbp.idbarangpembelian=b.idbarangpembelian),0) as kadaluarsa, ";
            $sql .= " (select MAX(jumlahdiberikan) from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikDiberikan, ";
            $sql .= " (select MAX(jumlah) from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikan, ";
            $sql .= " (select Max(g.idkemasan) from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as idkemasan, ";
            $sql .= " (select MAX(k.kemasan) from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as kemasan, ";
            $sql .= " rba.signa,rba.pemakaian, rb.kekuatan, b.penggunaan, b.kekuatan as kekuatanperiksa, rb.hargabeli, rb.idbarang, b.idbarangpemeriksaan, b.jumlahpemakaian, b.harga, b.total, b.grup, rb.namabarang, rb.jenis, b.jumlahaturanpakai, b.jumlahdiresepkan, b.idbarangaturanpakai, b.jaminanasuransi, b.jenisinput, b.idbarangpemeriksaan ";
            $sql .= " FROM rs_barangpemeriksaan b ";
            $sql .= " JOIN rs_barang rb ON rb.idbarang = b.idbarang ";
            $sql .= " LEFT JOIN rs_barang_aturanpakai rba on rba.idbarangaturanpakai = b.idbarangaturanpakai ";
            $sql .= " WHERE b.jenisrawat='$jenisrawat' and b.idpendaftaran='$idpendaftaran' ".$wheremodelist.$where_jenispemakaian." order by grup ";

            $query = $this->db->query($sql);
            
            if($query->num_rows() > 0)
            {
                    return $query->result();
            }
            else
            {
                    return NULL;
            }
	}
        
    public function pemeriksaan_listbhp_bypemeriksaan($idpemeriksaan,$jenisrawat='',$jenispemakaian='')
	{
            $jenisrawat     = empty($jenisrawat) ? 'rajal' : $jenisrawat;
            $where_jenispemakaian = empty($jenispemakaian) ? "" :  " and b.jenispemakaian='".$jenispemakaian."' ";
            
            $query = $this->db->query("SELECT "
                    . "(select jumlahdiberikan from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikDiberikan,"
                    . "(select jumlah from rs_barangpemeriksaan_grup g where g.idpendaftaran = b.idpendaftaran and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as jumlahRacikan,"
                    . "(select g.idkemasan from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as idkemasan,"
                    . "(select k.kemasan from rs_barangpemeriksaan_grup g, rs_barang_kemasan k where g.idpendaftaran = b.idpendaftaran and k.idkemasan = g.idkemasan and g.grup=b.grup and g.jenisrawat='".$jenisrawat."') as kemasan, 
                        rba.signa,rba.pemakaian, rb.kekuatan, b.penggunaan, b.kekuatan as kekuatanperiksa, rb.hargabeli, rb.idbarang, b.idbarangpemeriksaan, b.jumlahpemakaian, b.harga, b.total, b.grup, rb.namabarang, rb.jenis, b.jumlahaturanpakai, b.jumlahdiresepkan, b.idbarangaturanpakai, b.jaminanasuransi, b.jenisinput, b.idbarangpemeriksaan
                        FROM rs_barangpemeriksaan b 
                        JOIN rs_barang rb ON rb.idbarang = b.idbarang 
                        LEFT JOIN rs_barang_aturanpakai rba on rba.idbarangaturanpakai = b.idbarangaturanpakai
                        WHERE b.jenisrawat='".$jenisrawat."' and b.idpemeriksaan='".$idpemeriksaan."' and b.istagihan=1 ".$where_jenispemakaian." order by grup");
            if($query->num_rows() > 0)
            {
                    return $query->result();
            }
            else
            {
                    return NULL;
            }
	}
        
        public function pemeriksaanranap_listbhp($idinap)
	{
            $query =  $this->db->query( "SELECT rb.kekuatan, b.kekuatan as kekuatanperiksa, rb.hargabeli, b.idsatuanpemakaian, b.idkemasan, rb.idbarang, b.idrencanamedisbarang, b.jumlahpesan, b.jumlahpemakaian, b.jumlahsisa, b.harga, b.total, b.grup, b.jenisgrup, b.waktuinput, b.waktupengembalian, rb.namabarang, rb.jenis, b.jenisasal, catatan, ifnull((select count(1) from rs_inap_rencana_medis_barangpemeriksaan where idrencanamedisbarang=b.idrencanamedisbarang), 0) as jumlahplot, ifnull((select count(1) from rs_inap_rencana_medis_barangpemeriksaan where idrencanamedisbarang=b.idrencanamedisbarang and statuspelaksanaan='rencana'), 0) as jumlahrencana, penggunaan, 'ranap' statusranap FROM rs_inap_rencana_medis_barang b
                    left JOIN rs_barang rb ON rb.idbarang = b.idbarang
                    WHERE b.idinap='$idinap' order by jenisgrup asc,grup, isnull(namabarang), waktuinput desc, idrencanamedisbarang");
                    if($query->num_rows() > 0)
                    {
                            return $query->result();
                    }
                    else
                    {
                            return NULL;
                    }
	}
        public function pembelianbebas_listbhp($idpembelibebas)
	{
            $query = $this->db->query("SELECT rb.kekuatan, b.kekuatan as kekuatanperiksa, rb.hargabeli, b.idbarangaturanpakai, penggunaan, b.idkemasan, rb.idbarang, b.idbarangpembelianbebas, b.jumlahpemakaian, b.harga, b.total, b.grup, b.jenisgrup, b.waktuinput, rb.namabarang, rb.jenis, catatan FROM rs_barang_pembelianbebas b left JOIN rs_barang rb ON rb.idbarang = b.idbarang WHERE b.idbarangpembelibebas='".$idpembelibebas."' order by b.grup, isnull(rb.namabarang) desc, b.idbarangpembelianbebas");
                    if($query->num_rows() > 0)
                    {
                            return $query->result();
                    }
                    else
                    {
                            return NULL;
                    }
	}
	public function pemeriksaan_tampilpasien($norm,$idperiksa)//tampil data periksa pasien
	{
            $query = $this->db->query("SELECT "
                    . "(select isubahdokterpemeriksaan from rs_unit where idunit=rpp.idunit) as aksesubahdokter,"
                    . "(select count(1) from rs_pemeriksaan_wabah rpw where rpw.idpemeriksaan='".$idperiksa."' ) indikasipw, p.pasienprolanis, p.tanggal_kunjunganberikutnya, p.kondisikeluar, p.idstatuskeluar, p.tanggalrujukan, p.adaracikan, pper.idperson,pper.tempatlahir,  (select namapendidikan from demografi_pendidikan where idpendidikan = pper.idpendidikan)as pendidikan, date(now()) as tglsekarang, (select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ayah' limit 1) as ayah ,(select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ibu' limit 1) as ibu , pper.agama,pper.golongandarah, pper.rh, p.norujukan, p.nosep, p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, pp.norm, pper.alamat, pp.nojkn, pper.nik, concat(ifnull(pper.identitas,''),' ',ifnull(pper.namalengkap,'')) as namalengkap, pper.tanggallahir, pper.jeniskelamin, p.keteranganobat, p.keteranganradiologi,p.kategoriskrining, p.keteranganlaboratorium,p.saranradiologi,p.keteranganekokardiografi, usia(pper.tanggallahir) as usia, pp.alergi, p.statuspulang, al.ubahloketsaatperiksa,al.namaloket,al.idloket
                        from  person_pasien pp
                        join person_person pper on pper.idperson = pp.idperson
                        join person_pendaftaran p on p.norm = pp.norm
                        join rs_pemeriksaan rpp on rpp.idpendaftaran = p.idpendaftaran
                        left join antrian_loket al on al.idloket = rpp.idloket
            where pp.norm='".$norm."' and rpp.idpemeriksaan='$idperiksa'");
            if($query->num_rows() > 0)
            {
                    return $query->row_array();
            }
            else
            {
                    return NULL;
            }
	}
        public  function pemeriksaanverifklaim_tampilpasien($norm,$idperiksa)
        {
            $query = $this->db->query("SELECT (select count(1) from rs_pemeriksaan_wabah rpw where rpw.idpemeriksaan='".$idperiksa."' ) indikasipw, p.kondisikeluar, p.idstatuskeluar, p.tanggalrujukan, p.adaracikan, pper.idperson,pper.tempatlahir,  (select namapendidikan from demografi_pendidikan where idpendidikan = pper.idpendidikan)as pendidikan, date(now()) as tglsekarang, (select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ayah' limit 1) as ayah ,(select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ibu' limit 1) as ibu , pper.agama,pper.golongandarah, pper.rh, p.norujukan, p.nosep, p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, pp.norm, pper.alamat, pp.nojkn, pper.nik, concat(ifnull(pper.identitas,''),' ',ifnull(pper.namalengkap,'')) as namalengkap, pper.tanggallahir, pper.jeniskelamin, if(p.isverifklaim=1,p.keteranganobat_verifklaim,p.keteranganobat) as keteranganobat,p.kategoriskrining, if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) as keteranganradiologi,p.keteranganekokardiografi, if(p.isverifklaim=1,p.keteranganlaboratorium_verifklaim,p.keteranganlaboratorium) as keteranganlaboratorium ,if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) as saranradiologi,  usia(pper.tanggallahir) as usia, pp.alergi, p.statuspulang from  person_pasien pp, person_person pper, person_pendaftaran p, rs_pemeriksaan rpp
            where pp.norm='$norm' and pper.idperson = pp.idperson and p.norm = pp.norm and rpp.idpendaftaran = p.idpendaftaran and rpp.idpemeriksaan='$idperiksa'");
            if($query->num_rows() > 0)
            {
                    return $query->row_array();
            }
            else
            {
                    return NULL;
            }
        }
        public function pemeriksaanranap_tampilpasien_byinap($idinap)//tampil data periksa pasien
	{
		$query = $this->db->query("SELECT (select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm=pp.norm and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ayah' limit 1) as ayah ,(select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm=pp.norm and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ibu' limit 1) as ibu, (select namapendidikan from demografi_pendidikan where idpendidikan=pper.idpendidikan) as namapendidikan, usia(pper.tanggallahir) as usia, p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, p.idpendaftaran, pp.norm, pper.alamat, pp.nojkn, pper.nik, pper.namalengkap, pper.tanggallahir, pper.jeniskelamin,pper.agama, pper.rh, pper.tempatlahir, pper.golongandarah,  i.catatan, i.keteranganobat, i.keteranganradiologi,i.saranradiologi, i.keteranganlaboratorium, kelas 
                    from  person_pasien pp, person_person pper, person_pendaftaran p, rs_inap i, rs_kelas k
                    where pper.idperson = pp.idperson and p.norm = pp.norm and i.idpendaftaran=p.idpendaftaran and i.idkelas=k.idkelas and i.idinap='$idinap'");
		return $query->row_array();
	}
        public function rencanapemeriksaanranap($idrencanamedis)
        {
            $query = $this->db->select("*")->get_where('rs_inap_rencana_medis_pemeriksaan',['idrencanamedispemeriksaan'=>$idrencanamedis]);
            return $query->row_array();
            
        }
        public function get_rs_inap_soap($idrp,$waktu)
        {
            $wherer_waktu = ((empty($waktu)) ? "" : " and ris.waktu = '".$waktu."' " );
            $data = $this->db->query("SELECT ris.*, pgp.profesi, namadokter(ris.idpegawai) as pegawai FROM rs_inap_soap ris join person_grup_profesi pgp on pgp.idprofesi = ris.idprofesi where idrencanamedispemeriksaan = '".$idrp."' ".$wherer_waktu);
            return $data;
        }
        public function pemeriksaan_tampilpasien_bypendaftaran($norm,$idpendaftaran)//tampil data periksa pasien
	{
		$query = $this->db->query("SELECT p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, pp.norm, pper.alamat, pp.nojkn, pper.nik, pper.namalengkap, pper.tanggallahir, pper.jeniskelamin, p.keteranganobat, p.keteranganradiologi, p.keteranganlaboratorium from  person_pasien pp, person_person pper, person_pendaftaran p
		where pp.norm='$norm' and pper.idperson = pp.idperson and p.norm = pp.norm and p.idpendaftaran='$idpendaftaran'");
		if($query->num_rows() > 0)
		{
		 	return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
	public function pemeriksaandetail_tampil_pasien ($idpemeriksaan)// tampil data pasien pemeriksaan detail 
	{
		$query = $this->db->query("SELECT  p.alergi, rp.norm, rp.noantrian, p.nojkn, pp.nik, pp.namalengkap,pp.golongandarah, pp.rh, pp.agama, pp.statusmenikah,pp.notelpon as telponpasien, pp.idperson, pp.tempatlahir, pp.tanggallahir, pp.jeniskelamin, pp.alamat, ru.namaunit, ppp.carabayar, ppp.nosep, ppeg.titeldepan, ppeg.titelbelakang, pper.namalengkap as namadokter, pppjpper.namalengkap as namapenjawab, pppjpper.alamat as alamatpenjawab, pppjpper.notelpon
			from rs_pemeriksaan rp, person_pasien p, person_person pp, rs_jadwal rj, rs_unit ru, person_pendaftaran ppp, person_pegawai ppeg, person_person pper, person_pasien_penanggungjawab pppj, person_person pppjpper
			where rp.idpemeriksaan='$idpemeriksaan' and p.norm = rp.norm and pp.idperson = p.idperson and rj.idjadwal = rp.idjadwal and ru.idunit = rj.idunit and ppp.idpendaftaran = rp.idpendaftaran and ppeg.idpegawai = rj.idpegawaidokter and pper.idperson = ppeg.idperson and pppj.idpasienpenanggungjawab = ppp.idpasienpenanggungjawab and pppjpper.idperson = pppj.idperson 
			group by rp.idpemeriksaan");
		if($query->num_rows() > 0)
		{
		 	return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
	
	public function laboratorium_dataperiksa_tampilpaket($idpendaftaran)//tampil data paket lab periksa pasien
	{
		$query = $this->db->query("SELECT rh.idpendaftaran,  rpp.idpaketpemeriksaanparent, rpp2.namapaketpemeriksaan from rs_hasilpemeriksaan rh
		join rs_icd ricd on ricd.icd = rh.icd
		join rs_paket_pemeriksaan_detail rppd on rppd.icd = rh.icd
		join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rppd.idpaketpemeriksaan
		left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan = rpp.idpaketpemeriksaanparent
		WHERE rh.idpendaftaran='$idpendaftaran' AND ricd.jenisicd='laboratorium'
		group by rpp2.idpaketpemeriksaan");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function laboratorium_editperiksa($idpendaftaran, $idpaket)//tampil edit data lab periksa pasien
	{
		$query = $this->db->query("SELECT ricd.istext, ricd.icd,ricd.namaicd,rh.remark, rh.nilai, rh.nilaitext, ricd.satuan, ricd.nilaiacuanrendah, ricd.nilaiacuantinggi, rpp.idpaketpemeriksaan from rs_hasilpemeriksaan rh
			join rs_icd ricd on ricd.icd = rh.icd
			join rs_paket_pemeriksaan_detail rppd on rppd.icd = rh.icd
			join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rppd.idpaketpemeriksaan
			WHERE rh.idpendaftaran='$idpendaftaran' AND( rpp.idpaketpemeriksaan = '$idpaket' or rpp.idpaketpemeriksaanparent='$idpaket')");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function laboratorium_editperiksa_tampilpaket($idpendaftaran, $idpaket)//tampil edit data lab tampil paket
	{
		$query = $this->db->query("SELECT rpp.idpaketpemeriksaan, rpp.namapaketpemeriksaan from rs_hasilpemeriksaan rh
		join rs_icd ricd on ricd.icd = rh.icd
		join rs_paket_pemeriksaan_detail rppd on rppd.icd = rh.icd
		join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rppd.idpaketpemeriksaan
		WHERE rh.idpendaftaran='$idpendaftaran' AND ricd.jenisicd='laboratorium' and ( rpp.idpaketpemeriksaan = '$idpaket' or rpp.idpaketpemeriksaanparent='$idpaket')
		group by rpp.idpaketpemeriksaan");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function get_paketpemeriksaandetail()//tampil data paket pemeriksaan detail
	{
		$query = $this->db->query("SELECT rppd.idpaketpemeriksaandetail,rpp.namapaketpemeriksaan, ricd.icd, ricd.namaicd, ricd.satuan, ricd.nilaiacuanrendah, ricd.nilaiacuantinggi
			FROM rs_paket_pemeriksaan_detail rppd , rs_paket_pemeriksaan rpp, rs_icd ricd 
			WHERE rppd.icd = ricd.icd AND rppd.idpaketpemeriksaan = rpp.idpaketpemeriksaan
			order by rpp.idpaketpemeriksaan, rppd.idpaketpemeriksaandetail asc");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function get_caripemeriksaanpasien($idpemeriksaan)//cari data pemeriksaan pasien 
	{
		$query = $this->db->query("SELECT rp.idunit, idpendaftaran, rp.iddokterpemeriksa, namadokter(rp.iddokterpemeriksa) as dokterpemeriksa, anamnesa,diagnosa, keterangan,rekomendasi, rp.status, concat(ifnull(pp.titeldepan,''),' ',ifnull(pper.namalengkap,''),' ',ifnull(pp.titelbelakang,'')) as namalengkap 
		FROM rs_pemeriksaan  rp, person_pegawai pp, person_person pper
		WHERE idpemeriksaan='$idpemeriksaan' and pp.idpegawai = rp.idpegawaidokter and pper.idperson = pp.idperson");
		if($query->num_rows() > 0)
		{
		 	return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
        public function get_cariverifpemeriksaanpasien($idpemeriksaan)//cari data verifikasi pemeriksaan pasien 
	{
		$query = $this->db->query("
		SELECT ru.idunit, idpendaftaran, rp.iddokterpemeriksa, namadokter(rp.iddokterpemeriksa) as dokterpemeriksa, 
		if(rp.isverifklaim=1,rp.anamnesa_verifklaim,rp.anamnesa) as anamnesa,if(rp.isverifklaim=1,rp.diagnosa_verifklaim,rp.diagnosa) as diagnosa, 
		if(rp.isverifklaim=1,rp.keterangan_verifklaim,rp.keterangan) as keterangan,if(rp.isverifklaim=1,rp.rekomendasi_verifklaim, 
		rp.rekomendasi) as rekomendasi, rp.status, 
		concat(ifnull(pp.titeldepan,''),' ',ifnull(pper.namalengkap,''),' ',ifnull(pp.titelbelakang,'')) as namalengkap,
		pp.idperson as idpersonlogin 
		FROM rs_pemeriksaan  rp, person_pegawai pp, person_person pper, rs_unit ru
		WHERE idpemeriksaan='$idpemeriksaan' and pp.idpegawai = rp.idpegawaidokter and pper.idperson = pp.idperson");
		if($query->num_rows() > 0)
		{
		 	return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
        public function get_caripemeriksaanpasien_bypendaftaran($idpendaftaran)//cari data pemeriksaan pasien 
	{
		$query = $this->db->query("SELECT rp.norm, ru.idunit, rp.idpendaftaran, group_concat(concat(anamnesa, ' [', namaunit, ']')) as anamnesa, group_concat(concat(keterangan, ' [', namaunit, ']')) as keterangan, rp.status, group_concat(concat(pper.namalengkap, ' ', ifnull(pp.titeldepan, ''), ' ', ifnull(pp.titelbelakang, ''), ' [', namaunit, ']')) as dokter
		FROM rs_pemeriksaan  rp, rs_jadwal rj, person_pegawai pp, person_person pper, rs_unit ru
		WHERE rp.idpendaftaran='$idpendaftaran' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson and rj.idunit = ru.idunit group by rp.idpendaftaran");
                return $query->row_array();
	}
	public function get_pemeriksaanpasien($id)//cari data pemeriksaan
	{
		$query = $this->db->query("SELECT idpendaftaran, anamnesa, keterangan FROM rs_pemeriksaan WHERE idpemeriksaan='$id'");
		if($query->num_rows() > 0)
		{
		 	return $query->row_array();
		}
		else
		{
			return NULL;
		}
	}
	/// -- ambil data pemeriksaan
	public function rspemeriksaan_getdata($unit='', $tglawal='',$tglakhir='', $isfarmasi=false)
	{
		(!empty($unit) ? $whereunit="and ru.idunit='$unit'" : $whereunit="" );
		(empty($unit) && empty($tglakhir) ? $default="and date(rant.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : $default="" );
		(!empty($tglakhir) ? $wheredate="and date(rant.waktu) between '".$tglawal."' and '".$tglakhir."'" : $wheredate="" );
		(empty($tglakhir) && empty($tglawal) ? $wheredateempty= "and date(rant.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : $wheredateempty="" );
		$idgroup=((empty($unit))?'':$this->db->query("select idjadwalgrup from rs_unit where idunit='$unit'")->row_array()['idjadwalgrup']);
		$wheregroup = ((empty($unit))?'': (($idgroup==NULL)?'':" and ppd.idjadwalgrup = '$idgroup'")); 
		$wherefarmasi = ($isfarmasi) ? "and not isnull(keteranganobat) and trim(keteranganobat)<>'' and isnull(idbarangpemeriksaan) and (jenis='obat' or isnull(jenis))  and rant.status='sedang ditangani'" : "";
		
		$query=$this->db->query("SELECT pp.notelpon, rant.statusantrianfarmasi, (select noskdpql from rs_skdp where idpendaftaransebelum=rant.idpendaftaran) as noskdp , rj.idjadwalgrup, rant.noantrian ,if(ifnull(ppd.idjadwalgrup,'0')='0',concat(ifnull(al.namaloket,''),' (',rant.noantrian,')'),(select namaunit from rs_unit where idjadwalgrup=ppd.idjadwalgrup)) as unittujuan , al2.namaloket as unitasal, ppas.idperson, a.idantrian, ru.idunit, DATE_FORMAT(rant.waktu,'%d/%m/%Y %H:%i:%s') as tglperiksa, rant.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, rant.buatskdp, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, rant.idpemeriksaan, rant.norm, rant.`status`, rant.anamnesa, rant.keterangan,ppd.nosep, DATE_FORMAT(ppd.waktu,'%d/%m/%Y %H:%i:%s') as waktu ,rj.tanggal, ppd.carabayar
			FROM rs_pemeriksaan rant 
			left join person_pendaftaran ppd on ppd.idpendaftaran = rant.idpendaftaran and ppd.jenispemeriksaan!='ranap' and ppd.jenispemeriksaan!='belum'
			left join rs_jadwal rj on rj.idjadwal = rant.idjadwal 
			left join rs_unit ru on ru.idunit = rj.idunit 
			left join rs_pemeriksaan rant2 on rant2.idpemeriksaan = rant.idpemeriksaansebelum 
			left join rs_jadwal rj2 on rj2.idjadwal = rant2.idjadwal 
			left join rs_unit ru2 on ru2.idunit = rj2.idunit 
			left join person_pasien ppas on ppas.norm = ppd.norm 
			left join person_person pp on pp.idperson = ppas.idperson 
			left join person_pegawai ppai on ppai.idpegawai = rj.idpegawaidokter
			left join person_person pperppai on pperppai.idperson = ppai.idperson
			left join antrian_antrian a on a.idperson = ppas.idperson and a.idpemeriksaan=rant.idpemeriksaan and a.status!='ok' and a.idloket!='39' and date(a.tanggal) between '".$tglawal."' and '".$tglakhir."'
			left join antrian_loket al on al.idloket = rj.idloket
                        left join antrian_loket al2 on al2.idloket = rj2.idloket".(($isfarmasi)?" left join rs_barangpemeriksaan bp on ppd.idpendaftaran=bp.idpendaftaran left join rs_barang b on b.idbarang=bp.idbarang":"")."

			WHERE ppd.isclosing=0 and rant.status!='pesan' and rant.noantrian !=0 and ppd.jenispemeriksaan!='ranap' and ppd.jenispemeriksaan!='belum' ". $wheregroup.((empty($idgroup))? $whereunit : '').$wheredate.$default.$wheredateempty.$wherefarmasi." ORDER BY a.no DESC");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
    //menampilkan data pemeriksaan klinik
    /**
     * [+] ppd.statuspulang
     */
	public function select_listdatapemeriksaanpasien()
	{
		$order_field = ['rant.idpemeriksaan','rant.norm','pp.namalengkap',false,false,false,false,'ppd.waktu','rant.waktu','pperppai.namalengkap','al2.namaloket','a.no','ppd.kategoriskrining','ppd.pasienprolanis','ppd.tanggal_kunjunganberikutnya','rant.status'];
                
		$post = $this->input->post();
		$tglakhir    = format_date_to_sql($post['tglakhir']);
		$tglawal     = format_date_to_sql($post['tglawal']);
		$tahunbulan1 = empty($tglawal) ? date('Yj') : gettahunbulan(format_date_to_sql($post['tglawal']))  ;
		$tahunbulan2 = empty($tglakhir) ? date('Yj') : gettahunbulan(format_date_to_sql($post['tglakhir']))  ;

		$where_unit      = ( empty(!$post['unit']) ) ? " and ru.idunit='".$post['unit']."'" : "" ;
                $where_dokter    = ( empty($post['iddokter'])) ? "" : " and rant.idpegawaidokter ='".$post['iddokter']."' " ;
		$default         = ( empty($post['unit']) && empty($tglakhir) ) ? "and date(rant.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : "";
		$where_date      = ( empty(!$tglakhir) && empty(!$tglakhir) ) ? "and date(rant.waktu) between '".$tglawal."' and '".$tglakhir."'" : "";
		$where_date_empty= ( empty($tglakhir) && empty($tglawal) ) ? "and date(rant.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : "";
		
		$idgroup      = (( empty($post['unit']) )?'':$this->db->query("select idjadwalgrup from rs_unit where idunit='".$post['unit']."'")->row_array()['idjadwalgrup'] );
		$where_group  = (( empty($post['unit']) )?'': ( ($idgroup==NULL)?'':" and ppd.idjadwalgrup = '$idgroup'") ); 
		$where_farmasi= ($post['isfarmasi']=='on') ? "and not isnull(keteranganobat) and trim(keteranganobat)<>'' and isnull(idbarangpemeriksaan) and (jenis='obat' or isnull(jenis))  and rant.status='sedang ditangani'" : "";
                $where_jkn    = (($post['isjkn']=='on') ? " and ppd.carabayar in ('jknpbi','jknnonpbi','bpjstenagakerja') " : "");
		
		$order = "ORDER BY ".((isset($post['order'][0]['column']))? $order_field[$post['order'][0]['column']].' ' : " a.no "). ((isset($post['order'][0]['dir']))? $post['order'][0]['dir'] :" DESC");

                $where = " rant.tahunbulan between '".$tahunbulan1."' and '".$tahunbulan2."'  and ppd.isclosing=0 and rant.status!='pesan' and rant.noantrian !=0 and ppd.jenispemeriksaan!='ranap' and ppd.jenispemeriksaan!='belum' ". $where_group.((empty($idgroup))? $where_unit.$where_dokter : '').$where_date.$default.$where_date_empty.$where_farmasi.$where_jkn;
                $wherelike = (empty(!$post['search']['value']))? $where." and pp.namalengkap like '%".$post['search']['value']."%' OR ".$where." and rant.norm like '%".$post['search']['value']."%'": $where;
                
                $where_norm = isset($post['norm']) ? (empty($post['norm'])) ? '' : " rant.norm='".$post['norm']."' and ".$where  : '' ;
                
		$sql="SELECT  ppd.statuspulang,rant.buatskdp,ppas.nojkn, pp.nik, ppd.pasienprolanis, pp.notelpon, ppd.tanggal_kunjunganberikutnya, ppd.kategoriskrining, ppd.waktuperiksa, rant.statusantrianfarmasi, ppd.idjadwalgrup, rant.noantrian ,if(ifnull(ppd.idjadwalgrup,'0')='0',concat(ifnull(al.namaloket,''),' (',rant.noantrian,')'),(select namaunit from rs_unit where idjadwalgrup=ppd.idjadwalgrup)) as unittujuan , al2.namaloket as unitasal, ppas.idperson, a.idantrian, ru.idunit, DATE_FORMAT(rant.waktu,'%d/%m/%Y %H:%i:%s') as tglperiksa, rant.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter,  concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, rant.idpemeriksaan, rant.norm, rant.`status`, rant.anamnesa, rant.keterangan,ppd.nosep, DATE_FORMAT(ppd.waktu,'%d/%m/%Y %H:%i:%s') as waktu , date(rant.waktu) as tanggal, ppd.carabayar,rant.catatanrujuk, rant.isverifklaim 
			FROM rs_pemeriksaan rant 
			left join person_pendaftaran ppd on ppd.idpendaftaran = rant.idpendaftaran and ppd.jenispemeriksaan!='ranap' and ppd.jenispemeriksaan!='belum'
			left join rs_unit ru on ru.idunit = rant.idunit 
			left join rs_pemeriksaan rant2 on rant2.idpemeriksaan = rant.idpemeriksaansebelum 
			left join rs_unit ru2 on ru2.idunit = rant2.idunit 
			left join person_pasien ppas on ppas.norm = ppd.norm 
			left join person_person pp on pp.idperson = ppas.idperson 
			left join person_pegawai ppai on ppai.idpegawai = rant.idpegawaidokter
			left join person_person pperppai on pperppai.idperson = ppai.idperson
			left join antrian_antrian a on a.idperson = ppas.idperson and a.idpemeriksaan=rant.idpemeriksaan and a.idloket!='39' and date(a.tanggal) between '".$tglawal."' and '".$tglakhir."'
            left join antrian_loket al on al.idloket = rant.idloket
                        left join antrian_loket al2 on al2.idloket = rant2.idloket".(($post['isfarmasi']=='on')?" left join rs_barangpemeriksaan bp on ppd.idpendaftaran=bp.idpendaftaran left join rs_barang b on b.idbarang=bp.idbarang":"")."
			WHERE ".(($where_norm=='') ?  $wherelike.$order : $where_norm );
			return $sql;
	}
        
    //basit, clear
    public function cek_rs_barang_pembelianbebas($idbarang,$idbarangpembelibebas)
	{
            $query = $this->db->query("SELECT idbarangpembelianbebas, idbarang from rs_barang_pembelianbebas WHERE idbarang='$idbarang' AND idbarangpembelibebas='$idbarangpembelibebas'");
            if($query->num_rows() > 0)
            {
                    return $query->row_array();
            }
            else
            {
                    return NULL;
            }
	}
        
        //basit, clear
        public function pemeriksaan_cek_rs_inap_rencana_medis_barang($idbarang,$idinap)
	{
            $query = $this->db->query("SELECT idrencanamedisbarang, idbarang from rs_inap_rencana_medis_barang WHERE idbarang='$idbarang' AND idinap='$idinap'");
            if($query->num_rows() > 0)
            {
                    return $query->row_array();
            }
            else
            {
                    return NULL;
            }
	}
        
        //---------- basit, belum clear
	/// -- ambil data pemeriksaan ranap
	public function rsinap_getdata($bangsal='', $tglawal='',$tglakhir='')
	{
		(!empty($bangsal) ? $whereunit="and rb.idbangsal='$bangsal'" : $whereunit="" );
		(empty($bangsal) && empty($tglakhir) ? $default="and date(rip.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : $default="" );
		(!empty($tglakhir) ? $wheredate="and date(rip.waktu) between '".$tglawal."' and '".$tglakhir."'" : $wheredate="" );
		(empty($tglakhir) && empty($tglawal) ? $wheredateempty= "and date(rip.waktu) between '".date('Y-m-d')."' and '".date('Y-m-d')."'" : $wheredateempty="" );
		$query=$this->db->query("SELECT idrencanamedispemeriksaan, ppd.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, pp.namalengkap, ri.idinap, ppd.norm, ppd.nosep, rip.waktu, namabangsal, rip.status, ri.idkelas FROM 
                    rs_inap ri join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap
                    left join rs_bed rb on ri.idbed=rb.idbed
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join person_pegawai ppai on ppai.idpegawai = ri.idpegawaidokter 
                    left join person_person pperppai on pperppai.idperson = ppai.idperson 
			WHERE ppd.isclosing=0 ".$whereunit.$wheredate.$default.$wheredateempty."ORDER BY ppd.waktu desc");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        
//
    public function rsinap_getdata_checklist($post)
	{
            $status    = empty($post['status']) ? '' : " and rip.status = '".$post['status'] ."'" ;
            $idbangsal = empty($post['idbangsal']) ? '' : " and rb.idbangsal = '".$post['idbangsal'] ."'" ;
            $tanggal   = " and date(rip.waktu) between '".$post['tglawal']."' and '".$post['tglakhir']."' ";
            $idinap    = empty($post['idinap']) ? '' : " and ri.idinap = '".$post['idinap']."'" ;
            $sekarang  = " and(left(rip.waktu, 13)=left(now(),13) or (rip.status='rencana' and rip.waktu < now()))";
            $where     = ($post['sekarang']=='sekarang') ? $sekarang : $idinap.$tanggal.$idbangsal.$status ;
            
                //".$whereunit.$wheredate.$default.$wheredateempty."
                //".$whereunit.$wheredate.$default.$wheredateempty."
		$query=$this->db->query("(SELECT  
                    rpp2.idpaketpemeriksaan as idpaketpemeriksaanparent, 
                    rpp2.namapaketpemeriksaan as namapaketpemeriksaanparent, 
                    rpp.idpaketpemeriksaan, 
                    rpp.namapaketpemeriksaan, 
                    'tindakan' as jenistindakanobat, 
                    rmh.idrencanamedishasilpemeriksaan, 
                    0 as idrencanamedisbarangpemeriksaan, 
                    rip.idrencanamedispemeriksaan,
                    ppd.idpendaftaran, 
                    concat(pp.namalengkap, ' / ', ppd.norm) as pasien, 
                    ri.idinap, 
                    waktuterbilang(rip.waktu) as waktuterbilang, 
                    rip.waktu, 
                    namabangsal, 
                    concat(namaicd, ' [', jumlah, ' kali]') as tindakanobat, 
                    rmh.statuspelaksanaan, 
                    rip.status, 
                    ri.idkelas, 
                    rmh.icd 
                    FROM rs_inap ri 
                    join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap 
                    join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan 
                    left join rs_icd i on i.icd=rmh.icd 
                    left join rs_bed rb on ri.idbed=rb.idbed 
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal 
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rmh.idpaketpemeriksaan 
                    left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan=rpp.idpaketpemeriksaanparent 
                    WHERE ppd.isclosing=0 ".$where.")
                    
                    union all 
                    
                    (SELECT 'null','null','null','null',  'obat' jenistindakanobat, 
                    ifnull(rmbp.idrencanamedishasilpemeriksaan, 0) as idrencanamedishasilpemeriksaan, 
                    rmbp.idrencanamedisbarangpemeriksaan, 
                    rip.idrencanamedispemeriksaan, 
                    ppd.idpendaftaran, 
                    concat(pp.namalengkap, ' / ', ppd.norm) as pasien, 
                    ri.idinap, 
                    waktuterbilang(rip.waktu) as waktuterbilang, 
                    rip.waktu, 
                    namabangsal, 
                    concat(if(jenisgrup='racikan',concat('Racikan ', idkemasan), namabarang), ' [', jumlah, ' ', namasatuan, ' ', penggunaan,' makan]') as tindakanobat, 
                    rmbp.statuspelaksanaan, 
                    rip.status, 
                    ri.idkelas,
                    '' as icd 
                    FROM rs_inap ri 
                    join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap 
                    join rs_inap_rencana_medis_barangpemeriksaan rmbp on rmbp.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan 
                    join rs_inap_rencana_medis_barang rmb on rmb.idrencanamedisbarang=rmbp.idrencanamedisbarang 
                    join rs_satuan st on st.idsatuan=idsatuanpemakaian 
                    left join rs_barang b on b.idbarang=rmb.idbarang 
                    left join rs_bed rb on ri.idbed=rb.idbed 
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal 
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    WHERE jenisgrup<>'komponen racikan' and ppd.isclosing=0 ".$where.")
                    ORDER BY waktu desc, idrencanamedispemeriksaan, idrencanamedishasilpemeriksaan, idrencanamedisbarangpemeriksaan");
                
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        
        /// -- ambil data pemeriksaan ranap
	public function rsinap_getdata_sekarang()
	{
		$query=$this->db->query("SELECT idrencanamedispemeriksaan, ppd.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, pp.namalengkap, ri.idinap, ppd.norm, ppd.nosep, rip.waktu, namabangsal, rip.status, ri.idkelas FROM 
                    rs_inap ri join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap
                    left join rs_bed rb on ri.idbed=rb.idbed
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join person_pegawai ppai on ppai.idpegawai = ri.idpegawaidokter 
                    left join person_person pperppai on pperppai.idperson = ppai.idperson 
			WHERE ppd.isclosing=0 and (left(rip.waktu, 13)=left(now(),13) or (rip.status='rencana' and rip.waktu < now())) ORDER BY ppd.waktu desc");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        
        /// -- ambil data pemeriksaan ranap
	public function rsinap_getdata_pasien($idinap)
	{
		$query=$this->db->query("SELECT idrencanamedispemeriksaan, ppd.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, pp.namalengkap, ri.idinap, ppd.norm, ppd.nosep, rip.waktu, namabangsal, rip.status, ri.idkelas, idkelasjaminan FROM 
                    rs_inap ri join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap
                    left join rs_bed rb on ri.idbed=rb.idbed
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join person_pegawai ppai on ppai.idpegawai = ri.idpegawaidokter 
                    left join person_person pperppai on pperppai.idperson = ppai.idperson 
			WHERE ppd.isclosing=0 and ri.idinap=$idinap ORDER BY ppd.waktu desc");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        
        
	//cari hubungan keluarga sesuai norm
	public function cari_hubungankeluarga($id)
	{
		$query = $this->db->query("SELECT p1.idpasienpenanggungjawab, p3.namahubungan, p2.namalengkap FROM person_pasien_penanggungjawab p1
			LEFT JOIN person_person p2 ON p2.idperson = p1.idperson
			LEFT JOIN person_hubungan p3 ON p3.idhubungan = p1.idhubungan
			WHERE norm = '$id'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}

	//cari spesifik pasien
	public function cari_pasienspesifik($limit)
	{
		$nama = $this->input->post('nama');
		$tanggallahir = $this->input->post('tanggallahir');
		$tempatlahir = $this->input->post('tempatlahir');
		$query=$this->db->query("SELECT nojkn, pp.norm, nik, namalengkap FROM person_pasien pp join person_person p on p.idperson=pp.idperson WHERE p.namalengkap like '%$nama%' OR p.tempatlahir like '%$tempatlahir%' OR p.tanggallahir like '%$tanggallahir%' order by namalengkap limit 100, 100");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}

	public function jumlah_pasienspesifik()
	{
		$nama = $this->input->post('nama');
		$tanggallahir = $this->input->post('tanggallahir');
		$tempatlahir = $this->input->post('tempatlahir');
		$query=$this->db->query("SELECT count(1) as jumlah FROM person_pasien pp join person_person p on p.idperson=pp.idperson WHERE p.namalengkap like '%$nama%' OR p.tempatlahir like '%$tempatlahir%' OR p.tanggallahir like '%$tanggallahir%'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return 0;
		}
	}
	//cari norm,nojkn
	public function cari_norm_nojkn($id, $limit)
	{
		$query=$this->db->query("SELECT nojkn, pp.norm, nik, namalengkap FROM person_pasien pp join person_person p on p.idperson=pp.idperson WHERE pp.norm like '%$id%' OR nojkn like '%$id%' OR nik like '%$id%' OR namalengkap like '%$id%' order by namalengkap limit $limit, 100");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
        
    public function jumlah_norm_nojkn($id)
	{
		$query=$this->db->query("SELECT count(1) as jumlah FROM person_pasien pp join person_person p on p.idperson=pp.idperson WHERE pp.norm like '%$id%' OR nojkn like '%$id%' OR nik like '%$id%' OR namalengkap like '%$id%'");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return 0;
		}
	}
        
	public function cari_jadwalpoli($tanggal,$grup='')
	{
		$tahunbulan = gettahunbulan($tanggal);
		$query = $this->db->query("SELECT rj.idjadwal, rj.idunit, (select namaloket from antrian_loket where idloket=rj.idloket) as namaunit, rj.tanggal, concat(ifnull(pg.titeldepan,''),' ', ifnull(pp.namalengkap,'') ,' ',ifnull(pg.titelbelakang,'')) as namadokter FROM rs_jadwal rj
									join rs_unit ru on ru.idunit = rj.idunit 
                                    join person_pegawai pg on pg.idpegawai = rj.idpegawaidokter
                                    left join person_person pp on pp.idperson = pg.idperson
									where rj.tahunbulan='$tahunbulan' and date(rj.tanggal) = '$tanggal' and rj.idloket !='0' and pg.statuskeaktifan='aktif'".((empty($grup))?"": " and rj.idjadwalgrup=".$grup)."
									group by rj.idjadwal, rj.idunit");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	public function get_inputjadwal()
	{
		$query = $this->db->query("SELECT `ru`.`namaunit`, `pper`.`namalengkap`, `rj`.`idjadwal`, `rj`.`tanggal`, `rj`.`jamakhir`, `rj`.`quota`, `rj`.`status`, `rj`.`catatan` FROM `rs_jadwal` `rj`, `rs_unit` `ru`, `person_pegawai` `ppg`, `person_person` `pper`, `person_pegawai` `ppg2`, `person_person` `pper2` WHERE `rj`.`idunit` = `ru`.`idunit` AND `rj`.`idpegawaidokter` = `ppg`.`idpegawai` AND `ru`.`idpegawaika` = `ppg2`.`idpegawai` AND `ppg`.`idperson` = `pper`.`idperson` AND `ppg2`.`idperson` = `pper2`.`idperson` ORDER BY `rj`.`idjadwal` ASC");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	// ambil data enum dari database
	public function get_data_enum($table, $column,$dbname='db_sirstql') // ambil data enum dari database 
	{
        if(file_exists("./QLKP.php")){
            $dbname='db_sirstql_270721';
        } else {
            $dbname='db_sirstql';
        }
        
		$query = $this->db->query("SELECT COLUMN_TYPE  FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$column."'");

		if($query->num_rows() > 0)
		{
			
			foreach($query->result_array() AS $row) 
			{
    			$result = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE'])-6))));
    		}
		 	return $result;
		}
		else
		{
			return NULL;
		}
	}
	// QUERY LAPORAN PELAYANA DAN TINDAKAN GET DATA PASIEN, DOKTER Dll
	public function getlap_pelayanan($tgl1,$tgl2)
	{
		$q=$this->db->query("SELECT rp.norm,rji.jenisicd, rh.icd, ri.namaicd, ri.aliasicd, ri.idjenisicd,
		CASE WHEN ri.istext='numerik' THEN rh.nilai ELSE rh.nilaitext END AS nilai,
		CONCAT( IFNULL(ppeg.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(ppeg.titelbelakang,'')) as namadokter,
		rh.idpendaftaran, pp.waktu, rj.idunit, pper2.namalengkap, ru.namaunit
		from rs_hasilpemeriksaan rh
		left join rs_pemeriksaan rp on rp.idpendaftaran = rh.idpendaftaran
		left join rs_icd ri on ri.icd = rh.icd
		left join rs_jadwal rj on rj.idjadwal = rp.idjadwal
		left join person_pegawai ppeg on ppeg.idpegawai = rj.idpegawaidokter
		left join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran
		left join person_person pper on pper.idperson = ppeg.idperson
		left join person_pasien ppas on ppas.norm = rp.norm
		left join person_person pper2 on pper2.idperson = ppas.idperson
		left join rs_unit ru on ru.idunit = rj.idunit
		left join rs_jenisicd rji on rji.idjenisicd = ri.idjenisicd
		WHERE  date(pp.waktu) BETWEEN '$tgl1' and '$tgl2' order by rh.idpendaftaran, ri.idjenisicd asc")->result();
		return $q;
	}
	// start cetak skdp
	// ambil data pasien saat cetak skdp
	public function get_dtpasienskdp($idpendaftaran)
	{
		return $this->db->query("select concat(ifnull(ppeg.titeldepan,''),ifnull(pppeg.namalengkap,''), ifnull(ppeg.titelbelakang,'')) as namadokter, ppp.namalengkap, ppp.jeniskelamin, pp.norm, pp.nojkn, ppp.alamat, date_format(p.waktuperiksa,'%d/%m/%Y') as waktuperiksa from person_pendaftaran p 
        left join person_pasien pp on pp.norm = p.norm left join person_person ppp on ppp.idperson = pp.idperson 
        left join rs_pemeriksaan rp on rp.idpendaftaran = p.idpendaftaran
        left join rs_jadwal j on j.idjadwal = rp.idjadwal 
        left join person_pegawai ppeg on ppeg.idpegawai = j.idpegawaidokter 
        left join person_person pppeg on pppeg.idperson = ppeg.idperson
         where p.idpendaftaran='$idpendaftaran'");
}
	// ambil data diagnosa untuk dicetak
	public function get_dtdiagnosaskdp($idpendaftaran)
	{
		return $this->db->query("select hp.icd, i.namaicd, i.idjenisicd from  rs_hasilpemeriksaan hp join rs_icd i on i.icd = hp.icd and i.idjenisicd!=4 and i.idjenisicd!=1 and i.idjenisicd!=5 where hp.idpendaftaran='$idpendaftaran'");
	}

	// ambil data skdp untuk dicetak 
    /**
     * [+] nokontrol
     */
	public function get_dtskdppasien($idpendaftaran,$nokontrol)
	{
		return $this->db->query("SELECT distinct rs.*, date_format(rs.tanggalskdp,'%d/%m/%Y') as waktuperiksa  FROM rs_skdp rs, person_pendaftaran pp where rs.idpendaftaransebelum='$idpendaftaran' AND rs.nokontrol='".$nokontrol."'");
	}
	// end skdp cetak
	// mahmud, clear
	public function administrasiranap_getdata($data,$status,$ordermode=0)
	{
            $statusbed = ($status=="menunggu")  ? "and ri.idstatusbed='4'": '' ;
            $status = ($status=="menunggu")  ? 'selesai' : $status ;
            $whereunit= empty($data) ? "" : " and rb.idbangsal='$data'" ;
            $result = $this->db->query("SELECT sum(kk.nominal) as nominal, (ifnull(totalkekurangan(ri.idpendaftaran,'ranap'), 0)=0) as lunas, ri.idpendaftaran, ri.idbed,rb.idbangsal, p.status as statuspemeriksaan,sb.statusbed, sb.file, concat('Rencana: ',ifnull((select count(rip.idinap) from rs_inap_rencana_medis_pemeriksaan rip where rip.status='rencana' and rip.idinap=ri.idinap),''),'</br>Terlaksana: ',ifnull((select count(rip.idinap) from rs_inap_rencana_medis_pemeriksaan rip where rip.status='terlaksana' and rip.idinap=ri.idinap),'')) as pemeriksaan, ri.idinap, ppd.idpendaftaran, ppd.norm, concat('Masuk: ',ri.waktumasuk,if(ri.waktukeluar='0000-00-00 00:00:00','',concat('</br>Keluar: ',ri.waktukeluar))) as waktu, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, pp.namalengkap, ppd.nosep, concat(namabangsal,' (',rb.nobed,')') as namabangsal, ri.idkelas,ri.status FROM 
                    rs_inap ri 
                    left join rs_bed rb on ri.idbed=rb.idbed
                    left join rs_bed_status sb on sb.idstatusbed=ri.idstatusbed
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join rs_pemeriksaan p on p.idpendaftaran = ppd.idpendaftaran
                    left join keu_tagihan kk on kk.idpendaftaran = p.idpendaftaran
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join person_pegawai ppai on ppai.idpegawai = ri.idpegawaidokter 
                    left join person_person pperppai on pperppai.idperson = ppai.idperson 
            WHERE ri.status='$status' ".$statusbed.$whereunit." group by ri.idinap ORDER BY ".(($ordermode == 0)?"ppd.waktu desc":"namabangsal, norm, ppd.waktu desc"))->result();
            // ". (($ordermode == 0)?"ppd.waktu desc":"namabangsal, norm, ppd.waktu desc"))->result();
            return $result;
	}
        
        public function ranap_rekap($idinap)
        {
            $ret    = array();
            $jns    = array();
            $tgl    = array();
            $id     = array();
            $x = array();
            $result = $this->db->query  ("(select jenis, concat('b_', br.idbarang) as id, date(waktu) as tanggal, namabarang as nama, jumlah from rs_inap_rencana_medis_barangpemeriksaan bp join rs_inap_rencana_medis_barang b on b.idrencanamedisbarang=bp.idrencanamedisbarang join rs_barang br on br.idbarang=b.idbarang join rs_inap_rencana_medis_pemeriksaan p on p.idrencanamedispemeriksaan=bp.idrencanamedispemeriksaan join rs_inap i on i.idinap=p.idinap where i.idinap=$idinap and statuspelaksanaan='terlaksana') 
                                            union all 
                                            (select jenisicd as jenis, concat('i_', hp.icd) as id, date(waktu) as tanggal, namaicd as nama, jumlah from rs_inap_rencana_medis_hasilpemeriksaan hp join rs_inap_rencana_medis_pemeriksaan p on p.idrencanamedispemeriksaan=hp.idrencanamedispemeriksaan join rs_inap i on i.idinap=p.idinap join rs_icd ic on ic.icd=hp.icd join rs_jenisicd ji on ji.idjenisicd=ic.idjenisicd where i.idinap=$idinap and statuspelaksanaan='terlaksana') 
                                            union all 
                                            (select jenisicd as jenis, concat('i_', bnp.icd) as id, date(tanggal) as tanggal, namaicd as nama, 1 as jumlah from rs_inap_biayanonpemeriksaan bnp join rs_inap i on i.idinap=bnp.idinap join rs_icd ic on ic.icd=bnp.icd join rs_jenisicd ji on ji.idjenisicd=ic.idjenisicd where i.idinap=$idinap)"
                                        )->result();
            foreach ($result as $row)
            {
                $ret[$row->jenis][$row->id][$row->tanggal]  = $row->jumlah;
                $jns[$row->jenis][$row->id]                 = $row->jenis;
                $tgl[$row->tanggal]                         = $row->tanggal;
                $id[$row->id]                               = $row->nama;
            }
            ksort($tgl, SORT_NATURAL);
            ksort($id, SORT_NATURAL);
            foreach ($jns as $row)
            {
                foreach ($row as $idx => $row1)
                {
                    foreach ($tgl as $row2)
                    {
                        if (!isset($ret[$row1][$idx][$row2]))
                        {
                            $ret[$row1][$idx][$row2]   = " ";
                    }
                }
            }
            }
            foreach ($ret as &$subarray) 
            {
                ksort($subarray, SORT_NATURAL);
            }
            return ["data" => $ret, "grup" => $jns, "header" => $tgl, "id" => $id];
        }
        
        // ANRIAN FARMASI
        public function afr_cekadaresep($norm)
        {
        	return $this->db->query("select if(pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi',if(pp.adaracikan=1,'45','46'),if(pp.adaracikan=1,'43','44')) as idloket, date(now()) as expired, rp.idpemeriksaan, pp.carabayar, ppas.idperson, pp.idpendaftaran from person_pendaftaran pp, rs_pemeriksaan rp, person_pasien ppas where rp.idpendaftaran = pp.idpendaftaran and pp.norm='".$norm."' and ppas.norm=".$norm." and rp.status='sedang ditangani' limit 1");
        } 
        // mahmud clear :: PELAPORAN JASA MEDIS 
        public function getdatajasamedis($date,$order,$idpegawai='',$carabayar) // $tahunbulan -> yyyymm (201910)
        {
                $bulan=date('m', strtotime($date));
                $tahun=date('Y', strtotime($date));
                
                $sql = "SELECT pp.carabayar,
                            DATE_FORMAT(rp.waktu, '%d') waktu,
                            CONCAT(
                                IFNULL(ppai.titeldepan, ''),
                                ' ',
                                pper.namalengkap,
                                ' ',
                                IFNULL(ppai.titelbelakang, '')
                            ) namadokter,
                            ri.icd,
                            IFNULL(
                                ri.namaicd,
                                rpp.namapaketpemeriksaan
                            ) AS namaicd,
                            rh.jasaoperator,
                            SUM(rh.jasaoperator) sumjasaoperator,
                            rh.nakes,
                            SUM(rh.nakes) sumnakes,
                            rh.jasars,
                            SUM(rh.jasars) sumjasars,
                            rh.bhp,
                            SUM(rh.bhp) sumbhp,
                            rh.akomodasi,
                            SUM(rh.akomodasi) sumakomodasi,
                            rh.margin,
                            SUM(rh.margin) summargin,
                            rh.sewa,
                            SUM(rh.sewa) sumsewa,
                            SUM(rh.jumlah) jumlah
                        FROM
                            rs_hasilpemeriksaan rh
                        LEFT JOIN rs_icd ri ON
                            ri.icd = rh.icd AND ri.idjenisicd != '1' AND ri.idjenisicd != '2'
                        LEFT JOIN rs_pemeriksaan rp ON
                            rp.idpemeriksaan = rh.idpemeriksaan AND ".(($order=='hari')?"DATE(rp.waktu)='".$date."'":"YEAR(rp.waktu)='".$tahun."' AND MONTH(rp.waktu)='".$bulan."'")." AND rp.status != 'batal' AND rp.status != 'antrian' AND rp.status != 'tunda' AND rp.status != 'pesan'
                            LEFT JOIN person_pendaftaran pp ON
                            pp.idpendaftaran= rh.idpendaftaran AND ".(($order=='hari')?"DATE(pp.waktuperiksa)='".$date."'":"YEAR(pp.waktuperiksa)='".$tahun."' AND MONTH(pp.waktuperiksa)='".$bulan."'")."  AND pp.idstatuskeluar !=0 AND  pp.idstatuskeluar !=4 AND  pp.idstatuskeluar !=3 AND pp.carabayar='".$carabayar."'
                        LEFT JOIN rs_paket_pemeriksaan rpp ON
                            rpp.idpaketpemeriksaan = rh.idpaketpemeriksaan
                        LEFT JOIN person_pegawai ppai ON
                            ppai.idpegawai = rh.iddokterjasamedis
                        LEFT JOIN person_person pper ON
                            pper.idperson = ppai.idperson
                        WHERE
                            rh.total != 0 AND ".(($order=='hari')?"DATE(rp.waktu)='".$date."'":"YEAR(rp.waktu)='".$tahun."' AND MONTH(rp.waktu)='".$bulan."'")." AND rp.status != 'batal' AND rp.status != 'antrian' AND rp.status != 'tunda' AND rp.status != 'pesan' AND pp.carabayar='".$carabayar."' ".((empty($idpegawai))?" and rh.iddokterjasamedis!=0 GROUP BY ppai.idpegawai, rh.icd,pp.carabayar ": " and rh.iddokterjasamedis=".$idpegawai." GROUP BY  rh.icd, rp.waktu, pp.carabayar")." 
                        
                        ORDER BY
                            rp.waktu , pp.carabayar";
//        	$sql = "SELECT DATE_FORMAT(rp.waktu,'%d') waktu, concat(ifnull(ppai.titeldepan, ''), ' ',pper.namalengkap, ' ', ifnull(ppai.titelbelakang, '')) namadokter, ri.icd, ifnull(ri.namaicd,rpp.namapaketpemeriksaan) as namaicd, rh.jasaoperator, sum(rh.jasaoperator) sumjasaoperator , rh.nakes, sum(rh.nakes) sumnakes, rh.jasars,sum(rh.jasars) sumjasars, rh.bhp,sum(rh.bhp) sumbhp, rh.akomodasi, sum(rh.akomodasi) sumakomodasi, rh.margin, sum(rh.margin) summargin, rh.sewa,sum(rh.sewa) sumsewa, sum(rh.jumlah) jumlah FROM rs_hasilpemeriksaan rh left join rs_icd ri on ri.icd=rh.icd and ri.idjenisicd!='1' and ri.idjenisicd!='2' left join rs_pemeriksaan rp on rp.idpemeriksaan=rh.idpemeriksaan and ".(($order=='hari')?"date(rp.waktu)='".$date."'":"year(rp.waktu)='".$tahun."' and month(rp.waktu)='".$bulan."'")." and rp.status='selesai' left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan=rh.idpaketpemeriksaan left join person_pegawai ppai on ppai.idpegawai=rh.idpegawaidokter left join person_person pper on pper.idperson= ppai.idperson WHERE rh.total!=0 and ".(($order=='hari')?"date(rp.waktu)='".$date."'":"year(rp.waktu)='".$tahun."' and month(rp.waktu)='".$bulan."'")." and rp.status='selesai' ".((empty($idpegawai))?" and rh.idpegawaidokter!=0 GROUP BY ppai.idpegawai, rh.icd ": " and rh.idpegawaidokter=".$idpegawai." GROUP BY  rh.icd, rp.waktu")." order by rp.waktu";
        	return $this->db->query($sql);	
        }
        public function jasahadirjm($date,$dokter,$order)
        {
          $shift1=(($order=='bulan')?" and rp.tahunbulan=".date('Yn', strtotime($date))." and  time(rp.waktuperiksadokter) BETWEEN '07:00:01' AND  '21:00:00'":"and rp.waktuperiksadokter BETWEEN '".$date." 07:00:01' AND '".$date." 21:00:00'");
          $shift3=(($order=='bulan')?" and rp.tahunbulan=".date('Yn', strtotime($date))." and  time(rp.waktuperiksadokter) BETWEEN '21:00:01' AND  '07:00:00' ":"and rp.waktuperiksadokter BETWEEN '".$date." 21:00:01' AND '".ql_tanggaltujuan('1 day',$date)." 07:00:00'");
          return  $this->db->query("select 'Pagi/Sore' shift, COUNT(pp.carabayar) jumlahpasien , 40000 jm from person_pendaftaran pp, rs_pemeriksaan rp, rs_jadwal rj where pp.idstatuskeluar='2' and pp.jenispemeriksaan='rajal' and rp.idpendaftaran=pp.idpendaftaran ".$shift1." and rj.idjadwal=rp.idjadwal and rj.idpegawaidokter='".$dokter."' UNION ALL select 'Malam' shift, COUNT(pp.carabayar) jumlahpasien , 50000 jm from person_pendaftaran pp, rs_pemeriksaan rp, rs_jadwal rj where pp.idstatuskeluar='2' and pp.jenispemeriksaan='rajal' and rp.idpendaftaran=pp.idpendaftaran ".$shift3." and rj.idjadwal=rp.idjadwal and rj.idpegawaidokter='".$dokter."'");
        }
        // data pegawai
        public function getdatapegawai($dokter=null)
        {
        	return $this->db->query("select ppai.idpegawai, concat(ifnull(ppai.titeldepan, ''), ' ',pper.namalengkap, ' ', ifnull(ppai.titelbelakang, '')) as namadokter from person_pegawai ppai, person_person pper where ppai.statuskeaktifan='aktif' and ppai.idgruppegawai='5' and pper.idperson=ppai.idperson ".(($dokter!=null)?" and ppai.idpegawai='".$dokter."'":"")." order by pper.namalengkap asc");
        }
        
        public function getdatadokter()
        {
            $sql = "SELECT pp.idpegawai, pp.idperson, pper.namalengkap, pp.titeldepan, pp.titelbelakang FROM person_pegawai pp 
                    join person_grup_pegawai pgp on pgp.idgruppegawai = pp.idgruppegawai and pgp.isdokter = 1
                    join person_person pper on pper.idperson = pp.idperson
                    and pp.statuskeaktifan = 'aktif'";
            $data = $this->db->query($sql)->result_array();
            return $data;
        }


        public function getUnitByUsername()
        {
            $this->db->select("unituser");
            $this->db->from("login_hakaksesuser hau, login_user u");
            $this->db->where("hau.iduser = u.iduser");
            $this->db->where('namauser', sql_clean($this->input->post("u")));
            $query = $this->db->get()->result();
            foreach ($query as $row)
            {
                return $this->db->query("select idunit, namaunit, levelgudang from rs_unit_farmasi where idunit in (".$row->unituser.")")->result();
            }
        }
        
        
        public function getdata_pesananfarmasi()
        {
            $id     = ((empty($this->input->post('id')))?'':' and bp.idbarangpemesanan="'.$this->input->post('id').'" ');
            if(empty($this->input->post('id')))
            {
            	$unit   = ' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"';
            	$unitfarmasi = '';
            }
            else
            {
            	// jika id barang pemesanan tidak kosong atau approve permintaan
            	$pesanan= $this->db->query("select * from rs_barang_pemesanan where idbarangpemesanan='".$this->input->post('id')."'")->row_array();
            	$unit   = ' and bp.idunit="'.$pesanan['idunit'].'"  and bp.idunittujuan="'.$pesanan['idunittujuan'].'" and bp.jenistransaksi="'.$pesanan['jenistransaksi'].'" ';
            	$unitfarmasi = 'OR bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan '.$id.' and bp.idunit="'.$pesanan['idunit'].'"';


            }
            $bulan = $this->input->post('bulan');
            $mode   = (($this->input->post('mode')=='pembetulankirim') ? "bpd.jumlah - ifnull((select sum(if(db.jenisdistribusi='pembetulankeluar', -db.jumlah, +db.jumlah)) jumlah from rs_barang_distribusi db, rs_barang_pembelian a where a.idbarangpembelian=db.idbarangpembelian and db.idbarangpemesanan='".$this->input->post('id')."' and a.idbarang=bpd.idbarang),0) plot," : " bpd.jumlah plot," );
            $query = $this->db->query("select bp.idbarangpemesanan, ".$mode." bp.nofaktur, bp.waktu, "
                    . "(select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, "
                    . "(select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,"
                    . "bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan,bpd.jumlah, bpd.idbarang, "
                    . "bpd.keterangan,  bp.idpersonpetugas,b.hargajual, b.namabarang, b.kode, s.namasatuan  "
                    . "from rs_barang_pemesanan bp, rs_barang_pemesanan_detail bpd, rs_barang b, rs_satuan s "
                    . "where bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan".$id.$unit.$unitfarmasi);
            
            return $query;
        }
        
        public function getdata_pesanandepo()
        {
            $id     = ((empty($this->input->post('id')))?'':' and bp.idbarangpemesanan="'.$this->input->post('id').'" ');
            if(empty($this->input->post('id')))
            {
            	$unit   = ' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"';
            	$unitfarmasi = '';
            }
            else
            {
            	// jika id barang pemesanan tidak kosong atau approve permintaan
            	$pesanan= $this->db->query("select * from rs_barang_pemesanan where idbarangpemesanan='".$this->input->post('id')."'")->row_array();
            	$unit   = ' and bp.idunit="'.$pesanan['idunit'].'"  and bp.idunittujuan="'.$pesanan['idunittujuan'].'" and bp.jenistransaksi="'.$pesanan['jenistransaksi'].'" ';
            	$unitfarmasi = 'OR bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan '.$id.' and bp.idunit="'.$pesanan['idunit'].'"';


            }
            $bulan = $this->input->post('bulan');
            $mode   = (($this->input->post('mode')=='pembetulankirim') ? "bpd.jumlah - ifnull((select sum(if(db.jenisdistribusi='pembetulankeluar', -db.jumlah, +db.jumlah)) jumlah from rs_barang_distribusi db, rs_barang_pembelian a where a.idbarangpembelian=db.idbarangpembelian and db.idbarangpemesanan='".$this->input->post('id')."' and a.idbarang=bpd.idbarang),0) plot," : " bpd.jumlah plot," );
            $query = $this->db->query("select bp.idbarangpemesanan, ".$mode." bp.nofaktur, bp.waktu, "
                    . "(select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, "
                    . "(select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,"
                    . "bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan,bpd.jumlah, bpd.idbarang, "
                    . "bpd.keterangan,  bp.idpersonpetugas,b.hargajual, b.namabarang, b.kode, s.namasatuan  "
                    . "from rs_barang_pemesanan bp, rs_barang_pemesanan_detail bpd, rs_barang b, rs_satuan s "
                    . "where bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan".$id.$unit.$unitfarmasi);
            
            return $query;
        }


//        menampilkan data barang_pesanan
        public function jsonbarangpesanan()
        {   
            $id     = ((empty($this->input->post('id')))?'':' and bp.idbarangpemesanan="'.$this->input->post('id').'" ');
            if(empty($this->input->post('id')))
            {
            	$unit   = ($this->session->userdata('unitterpilih')=='GUDANG')?' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"':' and bp.idunittujuan="'.$this->session->userdata('idunitterpilih').'"';
            	$unitfarmasi = ($this->session->userdata('unitterpilih')=='FARMASI' OR $this->session->userdata('unitterpilih')=='RANAP') ?'OR bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan '.$id.' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"':'';
            }
            else
            {
            	// jika id barang pemesanan tidak kosong atau approve permintaan
            	$pesanan= $this->db->query("select * from rs_barang_pemesanan where idbarangpemesanan='".$this->input->post('id')."'")->row_array();
            	$unit   = ' and bp.idunit="'.$pesanan['idunit'].'"  and bp.idunittujuan="'.$pesanan['idunittujuan'].'" and bp.jenistransaksi="'.$pesanan['jenistransaksi'].'" ';
            	$unitfarmasi = 'OR bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan '.$id.' and bp.idunit="'.$pesanan['idunit'].'"';


            }
            $bulan = $this->input->post('bulan');
            $mode   = (($this->input->post('mode')=='pembetulankirim') ? "bpd.jumlah - ifnull((select sum(if(db.jenisdistribusi='pembetulankeluar', -db.jumlah, +db.jumlah)) jumlah from rs_barang_distribusi db, rs_barang_pembelian a where a.idbarangpembelian=db.idbarangpembelian and db.idbarangpemesanan='".$this->input->post('id')."' and a.idbarang=bpd.idbarang),0) plot," : " bpd.jumlah plot," );
            return $this->db->query("select bp.idbarangpemesanan, ".$mode." bp.nofaktur, bp.waktu, (select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, (select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan,bpd.jumlah, bpd.idbarang, bpd.keterangan,  bp.idpersonpetugas,b.hargajual, b.namabarang, b.kode, s.namasatuan  from rs_barang_pemesanan bp, rs_barang_pemesanan_detail bpd, rs_barang b, rs_satuan s where bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan".$id.$unit.$unitfarmasi); //(select namauser  from login_user where idperson=bp.idpersonpetugas) namalengkap,
//            return $this->db->query("select a.idbarangpemesanan, a.waktu, a.nofaktur, b.jumlah, ifnull((select sum(jumlah) from rs_barang_distribusi bi join rs_barang_pembelian bp on bp.idbarangpembelian=bi.idbarangpembelian join rs_barang bg on bg.idbarang=bp.idbarang where ". $id2 ." bg.idbarang=b.idbarang), 0) as jumlahsudah, b.keterangan, c.namalengkap, d.namaunit unitasal, e.namaunit unittujuan,f.kode,f.namabarang,f.idbarang, g.namasatuan, a.statusbarangpemesanan from rs_barang_pemesanan a, rs_barang_pemesanan_detail b, person_person c, rs_unit d, rs_unit e, rs_barang f, rs_satuan g where b.idbarangpemesanan=a.idbarangpemesanan and c.idperson=a.idpersonpetugas and d.idunit=a.idunit and e.idunit=a.idunittujuan and f.idbarang=b.idbarang and g.idsatuan=f.idsatuan");
        }

        public function jsonbarangpengembalian()
        {
        	$id     = ((empty($this->input->post('id')))?'':' and bp.idbarangpemesanan="'.$this->input->post('id').'" ');
            $unit   = ($this->session->userdata('unitterpilih')=='GUDANG')?' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"':' and bp.idunittujuan="'.$this->session->userdata('idunitterpilih').'"';
            $unitfarmasi = ($this->session->userdata('unitterpilih')=='FARMASI' OR $this->session->userdata('unitterpilih')=='RANAP') ?'OR bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan '.$id.' and bp.idunit="'.$this->session->userdata('idunitterpilih').'"':'';
            $bulan = $this->input->post('bulan');
            $mode   = (($this->input->post('mode')=='pembetulankirim') ? "bpd.jumlah - ifnull((select sum(if(db.jenisdistribusi='pembetulankeluar', -db.jumlah, +db.jumlah)) jumlah from rs_barang_distribusi db, rs_barang_pembelian a where a.idbarangpembelian=db.idbarangpembelian and db.idbarangpemesanan='".$this->input->post('id')."' and a.idbarang=bpd.idbarang),0) plot," : " bpd.jumlah plot," );
            return $this->db->query("select ".$mode." bpd.idbarangpembelian,bp.nofaktur, bp.waktu, (select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, (select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan,bpd.jumlah, bpd.idbarang, bpd.keterangan,  bp.idpersonpetugas, b.hargajual, b.namabarang, b.kode, s.namasatuan, (select batchno from rs_barang_pembelian a where a.idbarangpembelian=bpd.idbarangpembelian) as batchno, (select kadaluarsa from rs_barang_pembelian a where a.idbarangpembelian=bpd.idbarangpembelian) as kadaluarsa from rs_barang_pemesanan bp, rs_barang_pemesanan_detail bpd,  rs_barang b, rs_satuan s where bpd.idbarangpemesanan=bp.idbarangpemesanan and b.idbarang=bpd.idbarang and s.idsatuan=b.idsatuan".$id.$unit.$unitfarmasi); //(select namauser  from login_user where idperson=bp.idpersonpetugas) namalengkap,
        }

        public function jsondetailbelanjabarang()
        {
            $sql = "select rbf.*, rbp.batchno,rbp.hargaasli, rbp.kadaluarsa,rbp.hargabeli,rbp.kadaluarsa,rbp.batchno, rbd.distributor, rbd.alamat, rb.namabarang, rb.kode,rs.namasatuan, rdis.jumlah
                from rs_barang_faktur rbf, rs_barang_pembelian rbp, rs_barang_distributor rbd, rs_barang rb,rs_satuan rs, rs_barang_distribusi rdis
                where rbf.idbarangfaktur=".$this->input->post('idbf')." and rbp.idbarangfaktur=rbf.idbarangfaktur and rbd.idbarangdistributor=rbf.idbarangdistributor and rb.idbarang=rbp.idbarang and rs.idsatuan=rb.idsatuan and rdis.idbarangpembelian=rbp.idbarangpembelian and rdis.jenisdistribusi='masuk'";
            return $this->db->query($sql);
        }
        
        public function  jsoncetakbarangpesananunit()
        {
            $sql="SELECT
    bp.nofaktur,
    bp.waktu,
    (
    SELECT
        namaunit
    FROM
        rs_unit
    WHERE
        idunit = bp.idunit
) unitasal,
(
    SELECT
        namaunit
    FROM
        rs_unit
    WHERE
        idunit = bp.idunittujuan
) unittujuan,
bp.statusbarangpemesanan,
bp.idbarangpemesanan,
bpd.idbarang,
bpd.keterangan,
(
    SELECT
        namauser
    FROM
        login_user
    WHERE
        idperson = bp.idpersonpetugas
) namalengkap,
bp.idpersonpetugas,
b.hargajual,
b.namabarang,
b.kode,
s.namasatuan,
rbd.jumlah  jumlahdiberikan,
rbp.kadaluarsa,
rbp.batchno
FROM
    rs_barang_pemesanan bp,
    rs_barang_pemesanan_detail bpd,
    rs_barang b,
    rs_satuan s,
    rs_barang_distribusi rbd,
    rs_barang_pembelian rbp
    #,    rs_barang_distribusi rbd
WHERE
    bpd.idbarangpemesanan = bp.idbarangpemesanan AND b.idbarang = bpd.idbarang AND s.idsatuan = b.idsatuan AND bp.idbarangpemesanan = 11 AND bp.idunittujuan = '25' and rbd.idbarangpemesanan='11' and rbp.idbarangpembelian=rbd.idbarangpembelian GROUP by rbd.idbarangdistribusi";
        }

        public function jsonbarangpesananstok()
        {
            $idbarang   = $this->input->post('id');
            $idunit     = $this->input->post('idunit');
            $idpesan    = $this->input->post('idpm');
            //sum(if(rbd.jenisdistribusi='pembetulankeluar', -rbd.jumlah, +rbd.jumlah)) 
            $jumlah     = (($this->input->post('ht') != 0)?" ifnull((SELECT sum(if(rbd.jenisdistribusi='pembetulankeluar', -rbd.jumlah, +rbd.jumlah))  FROM rs_barang_distribusi rbd, rs_barang_pembelian rbp  where rbd.idbarangpemesanan='".$idpesan ."' and rbd.idbarangpembelian=b.idbarangpembelian and rbp.idbarangpembelian= rbd.idbarangpembelian  and rbp.idbarang='".$idbarang."' and rbd.idunit='".$idunit."' and rbd.jenisdistribusi!='masuk' and rbd.jenisdistribusi!='pembetulanmasuk' and rbd.jenisdistribusi!='pembetulantransformasihasil' and rbd.jenisdistribusi!='masuk'),0)  jumlah,":" 0 jumlah,");
//            $plot
//            if ($this->input->post('ht') == 0)
//            {
                return $this->db->query("SELECT ".$jumlah." b.idbarangpembelian, a.idbarangstok, b.idbarang, a.stok, b.batchno,b.kadaluarsa, c.kode, c.namabarang, d.namasatuan  FROM rs_barang_stok a, rs_barang_pembelian b, rs_barang c, rs_satuan d where a.stok > 0 and b.idbarangpembelian=a.idbarangpembelian and b.idbarang='".$idbarang."' and a.idunit='".$idunit."' and c.idbarang=b.idbarang and d.idsatuan=c.idsatuan order by b.kadaluarsa asc");
//                    . " UNION ALL "
//                    . "(SELECT 0 as idbarangpembelian, 0 as idbarangstok, 0 as idbarang, 0 as stok, jumlah, b.kadaluarsa, c.kode, c.namabarang, d.namasatuan FROM rs_barang_distribusi a, rs_barang_pembelian b, rs_barang c, rs_satuan d where b.idbarangpembelian=a.idbarangpembelian and a.idbarangpemesanan='".$idpesan."' and b.idbarang='".$idbarang."' and a.idunit='".$idunit."' and c.idbarang=b.idbarang and d.idsatuan=c.idsatuan order by b.kadaluarsa asc)");
//            }
//            else
//            {
////                SELECT a.jumlah FROM rs_barang_distribusi a, rs_barang_pembelian b  where a.idbarangpemesanan=6 and b.idbarangpembelian=a.idbarangpembelian and b.idbarang='5' and a.idunit=24 and a.jenisdistribusi='keluar'
////                SELECT a.idbarangpembelian, a.jumlah FROM rs_barang_distribusi a, rs_barang_pembelian b, rs_barang c where b.idbarangpembelian=a.idbarangpembelian and a.idbarangpemesanan='6' and b.idbarang='5' and a.idunit='24'
//                return $this->db->query("SELECT 0 as idbarangpembelian, 0 as idbarangstok, 0 as idbarang, 0 as stok, jumlah, b.kadaluarsa, c.kode, c.namabarang, d.namasatuan FROM rs_barang_distribusi a, rs_barang_pembelian b, rs_barang c, rs_satuan d where b.idbarangpembelian=a.idbarangpembelian and a.idbarangpemesanan='".$idpesan."' and b.idbarang='".$idbarang."' and a.idunit='".$idunit."' and c.idbarang=b.idbarang and d.idsatuan=c.idsatuan order by b.kadaluarsa asc");
////                return $this->db->query("SELECT  b.idbarangpembelian, a.idbarangstok, b.idbarang, a.stok,0 as jumlah, b.kadaluarsa, c.kode, c.namabarang, d.namasatuan  FROM rs_barang_stok a, rs_barang_pembelian b, rs_barang c, rs_satuan d where b.idbarangpembelian=a.idbarangpembelian and a.stok != 0 and b.idbarang='".$idbarang."' and a.idunit='".$idunit."' and c.idbarang=b.idbarang and d.idsatuan=c.idsatuan order by b.kadaluarsa asc");
//            }
        }
        
        /**
     * Insert Delete Diskon
     * @param $idp  Id Pendaftaran
     */
    function insertdelete_diskonpemeriksaan($idp)
    {
        $pendaftaran = $this->db->select("carabayar,  DATE_FORMAT(waktuperiksa,'%Y-%m-%d') waktuperiksa")->get_where('person_pendaftaran',['idpendaftaran'=>$idp])->row_array();
        $itemdiskon= $this->db->query("select icd, idjenistarif, nominal from rs_masterdiskon where jenisinput='0' and tanggalmulai <= '".$pendaftaran['waktuperiksa']."' and tanggalselesai >= '".$pendaftaran['waktuperiksa']."'");
        if($itemdiskon->num_rows() > 0)
        {
            foreach ($itemdiskon->result() as $obj)
            {
                $jumlahdiskon = $this->db->query("select count(1) as jumlah from rs_hasilpemeriksaan hp where hp.idpendaftaran = '".$idp."' and hp.icd='".$obj->icd."'")->row_array()['jumlah'];
                if($pendaftaran['carabayar'] == 'mandiri' and $jumlahdiskon == 0)
                {
                    $result = $this->db->insert('rs_hasilpemeriksaan',['idpendaftaran'=>$idp, 'icd'=>$obj->icd, 'idjenistarif'=>$obj->idjenistarif, 'potongantagihan'=>$obj->nominal]);
                }
                if($pendaftaran['carabayar'] != 'mandiri' and $jumlahdiskon > 0)
                {
                    $result = $this->db->delete('rs_hasilpemeriksaan',['idpendaftaran'=>$idp, 'icd'=>$obj->icd]);
                }
            }
            $result = $this->hitungtotaldiskon($idp);
            return $result;
        }
        return $pendaftaran;
    }
    /**
     * Menghitung total diskon persentase, kusus diskon otomatis dan diskon non member
     * @param $idp  Id Pendaftaran
     */
    public function hitungtotaldiskon($idp)
    {
        $jumlahdiskon = $this->db->query("select count(1) as jumlah, m.nominal, rh.icd, rh.idpendaftaran, totaltagihan(rh.idpendaftaran,'rajal') as total from rs_hasilpemeriksaan rh join rs_masterdiskon m on m.icd = rh.icd where rh.idpendaftaran='".$idp."' and m.jenisdiskon='persentase' and m.idmember=0");
        if($jumlahdiskon->num_rows() > 0)
        {
            foreach ($jumlahdiskon->result() as $obj)
            {
                $diskon = $obj->total * ($obj->nominal / 100);
                $totdiskon = pembulatanratusan( $diskon );
                $arrMsg = $this->db->update("rs_hasilpemeriksaan",['potongantagihan'=> $totdiskon],['idpendaftaran'=>$obj->idpendaftaran,'icd'=>$obj->icd]);
            }
        }
        
        return $arrMsg;
    }
    
    /**
     * Data Kunjungan Rawat Jalan Berdasarkan Rujukan
     * @param type $jenisperiksa Description
     * @param type $tanggal1 Tanggal Awal
     * @param type $tanggal2 Tanggal Akhir
     */    
    public function getdata_kunjungan_byrujukan($jenisperiksa,$tanggal1,$tanggal2)
    {
        $wherejp= (($jenisperiksa=='rajal') ? " and (pp.jenispemeriksaan='rajal' or pp.jenispemeriksaan='rajalnap') " : " and (pp.jenispemeriksaan='ranap' or pp.jenispemeriksaan='rajalnap') " );
        $query  = $this->db->query("
                SELECT count(1) as kunjungan, bk.kodeklinik, bk.namaklinik, pp.jenisrujukan, bj.jenisklinik
                FROM person_pendaftaran pp
                LEFT JOIN bpjs_klinikperujuk bk on bk.idklinik=pp.idklinikperujuk 
                LEFT JOIN bpjs_jenisklinik bj on bj.idjenisklinik = bk.idjenisklinik
                WHERE pp.waktuperiksa BETWEEN '".$tanggal1."' and '".$tanggal2."' ".$wherejp." 
                GROUP BY pp.idklinikperujuk, pp.jenisrujukan
                ORDER BY pp.idklinikperujuk, pp.jenisrujukan");
        $data = $query->result_array();
        return $data;
    }
    
    public function getdata_kunjungan_pasienranapbyrujukan($tanggal1,$tanggal2)
    {
        $query  = $this->db->query("SELECT fstatuspendaftaran(pp.idstatuskeluar) as statuskeluar, pper.notelpon, pper.alamat, pp.norm, namapasien(ppas.idperson) as namapasien, (select waktumasuk from rs_inap where idpendaftaran=pp.idpendaftaran limit 1) as waktumasuk, bk.kodeklinik, bk.namaklinik, pp.jenisrujukan
                FROM person_pendaftaran pp
                JOIN person_pasien ppas on ppas.norm=pp.norm
                JOIN person_person pper on pper.idperson=ppas.idperson
                LEFT JOIN bpjs_klinikperujuk bk on bk.idklinik=pp.idklinikperujuk 
                WHERE pp.waktuperiksa BETWEEN '".$tanggal1."' and '".$tanggal2."' and (pp.jenispemeriksaan='rajalnap' or pp.jenispemeriksaan='ranap' )
                ORDER BY pp.idklinikperujuk, pp.jenisrujukan");
        $data = $query->result_array();
        return $data;
    }

    /**
     * Data Kunjungan Rawat Jalan Berdasarkan Status Layanan
     * @param type $tanggal1
     * @param type $tanggal2
     */
    public function getdata_kunjunganralan_bylayanan($tanggal1,$tanggal2)
    {
        $tahun1 = date('Y', strtotime($tanggal1));
        $tahun2 = date('Y', strtotime($tanggal2));
        $bulan1 = date('j', strtotime($tanggal1));
        $bulan2 = date('j', strtotime($tanggal2));
        $wheretahunbulan = (tanggallebihdari_hari($tanggal1,$tanggal2,25)) ? " pp.tahunperiksa BETWEEN '".$tahun1."' and '".$tahun2."'  and pp.bulanperiksa BETWEEN '".$bulan1."' and '".$bulan2."' and" : "" ;
        $query = $this->db->query("
                SELECT sum(1) as kunjungan, if(idstatuskeluar=1,'Periksa',if(idstatuskeluar=3,'Batal',if(idstatuskeluar=2,'Selesai',if(idstatuskeluar=0,'Pesan','Kadaluarsa')))) as status 
                FROM person_pendaftaran pp
                WHERE ".$wheretahunbulan." date(pp.waktuperiksa) BETWEEN '".$tanggal1."' and '".$tanggal2."' 
                GROUP by pp.idstatuskeluar
                ORDER by pp.idstatuskeluar");
        $data = $query->result_array();
        return $data;
    }
    
    
    /**
     * Data Kunjungan Pasien Berdasarkan Cara Daftar
     * @param type $tanggal1
     * @param type $tanggal2
     */
    public function getdata_kunjunganralan_bycaradaftar($tanggal1,$tanggal2)
    {
        $tahun1 = date('Y', strtotime($tanggal1));
        $tahun2 = date('Y', strtotime($tanggal2));
        $bulan1 = date('j', strtotime($tanggal1));
        $bulan2 = date('j', strtotime($tanggal2));
        $wheretahunbulan = (tanggallebihdari_hari($tanggal1,$tanggal2,25)) ? " pp.tahunperiksa BETWEEN '".$tahun1."' and '".$tahun2."'  and pp.bulanperiksa BETWEEN '".$bulan1."' and '".$bulan2."' and" : "" ;
        $query = $this->db->query("SELECT DATE_FORMAT(pp.waktuperiksa,'%a, %d-%b-%Y') as tanggalperiksa , sum(if(pp.caradaftar='reguler',1,0)) as daftar_reguler, sum(if(pp.caradaftar='online',1,0)) as daftar_online, sum(if(pp.caradaftar='anjunganonline',1,0)) as daftar_anjungan, sum(if(pp.caradaftar='mobilejkn',1,0)) as daftar_mobilejkn, sum(1) as totalpendaftar FROM person_pendaftaran pp WHERE ".$wheretahunbulan." date(pp.waktuperiksa) BETWEEN '".$tanggal1."' and '".$tanggal2."' GROUP by pp.waktuperiksa ORDER BY pp.waktuperiksa asc");
        $data = $query->result_array();
        return $data;
    }
    
    /**
     * Faktur Lunas Hutang Farmasi
     * @param type $tanggal1 Tanggal awal
     * @param type $tanggal2 Tanggal akhir
     */
    public function getdata_fakturlunashutangfarmasi($tanggal1,$tanggal2)
    {
        $query = $this->db->select(' a.*, b.distributor, b.alamat, b.telepon')
                ->join('rs_barang_distributor b','b.idbarangdistributor = a.idbarangdistributor','left')
                ->where("a.tanggalpelunasan BETWEEN '".$tanggal1."' AND '".$tanggal2."'")
                ->order_by('a.idbarangdistributor','asc')
                ->get('rs_barang_faktur a')->result_array();
        return $query;
    }
    /**
     * Faktur Lunas Hutang Usaha
     * @param type $tanggal1 Tanggal Awal
     * @param type $tanggal2 Tanggal Akhir
     */
    public function getdata_fakturlunashutangusaha($tanggal1,$tanggal2)
    {
        $sql = "select rf.nofaktur, rf.tanggalfaktur, rf.tanggaljatuhtempo, rf.tanggalpelunasan, rf.tagihan, rf.totaltagihan, rf.potongan, rf.kekurangan, rf.dibayar, rf.ppn, rf.statusbayar, rf.keterangan, rbd.distributor, rbd.alamat, rbd.telepon
        FROM rs_fakturnonfarmasi rf, rs_barang_distributor rbd  
        WHERE rf.tanggalpelunasan BETWEEN '".$tanggal1."' and '".$tanggal2."' and rbd.idbarangdistributor=rf.idbarangdistributor 
        ORDER by rf.idbarangdistributor asc";
        return $this->db->query($sql)->result_array();
    }
    
    /**
     * Generate Hasil Laboratorium Otomatis
     * @param type $idrule Id Rule Paket Pemeriksaan
     * @param type $idperiksa Id Pemeriksaan
     * @param type $idpaket Id Paketpemeriksaan
     */
    public function generate_hasil_laboratorium($idrule,$idperiksa,$idpaket)
    {
        $data = $this->db->query("select pesan_paketpemeriksaan(GROUP_CONCAT(rp.idhasilpemeriksaan,".$idrule.") as kesimpulan from rs_hasilpemeriksaan rp where rp.idpemeriksaan='".$idperiksa."'  and rp.icd is not null and rp.idpaketpemeriksaan='".$idpaket."' and ri.idjenisicd=4")->row_array();
        return $data;
    }
    
    //ambil data pasien riwayat periksa
    public function getdatapasienriwayatperiksa($norm)
    {
        $data = $this->db->query("select distinct pp.*, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, dp.namapekerjaan, dpn.namapendidikan, gd.namadesakelurahan, (select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ayah' limit 1) as ayah ,(select alamat from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ayah' limit 1) as alamatayah, (select namalengkap from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ibu' limit 1) as ibu, (select alamat from person_pasien_penanggungjawab pj,person_person pjper, person_hubungan ph where pj.norm='$norm' and pjper.idperson = pj.idperson and pj.idhubungan = ph.idhubungan and ph.namahubungan='ibu' limit 1) as alamatibu
            from person_pasien p
            left join person_person pp on pp.idperson = p.idperson
            left join demografi_pekerjaan dp on dp.idpekerjaan = pp.idpekerjaan
            left join demografi_pendidikan dpn on dpn.idpendidikan = pp.idpendidikan
            left join geografi_desakelurahan gd on gd.iddesakelurahan = pp.iddesakelurahan 
            where p.norm='".$norm."'")->row_array();
        return $data;;
    }
    
    //ambil data pendaftaran pasien periksa
    public function getdatapendaftaranriwayatpasien($norm,$mode,$idpendaftaran='')
    {
        $data = $this->db->query("SELECT rp.anamnesa,rp.keterangan,pp.keteranganobat, pp.keteranganradiologi,pp.saranradiologi, pp.keteranganlaboratorium, rp.idpemeriksaan, pp.idpendaftaran, date_format( rp.waktu,'%d/%b/%Y') tanggal, date_format( rp.waktu,'%H:%i:%s') waktu,rp.idpegawaidokter, concat(if(ppeg.titeldepan='' ,'',ppeg.titeldepan ) ,' ',pper.namalengkap,' ',if(ppeg.titelbelakang='','',ppeg.titelbelakang)) as namadokter,ru.namaunit  from person_pendaftaran pp, rs_pemeriksaan rp, rs_unit ru, person_pegawai ppeg, person_person pper where ". ((empty($idpendaftaran))?'':" pp.idpendaftaran='".$idpendaftaran."' and ") ." pp.norm='".$norm."' and pp.idstatuskeluar!='3' and pp.idstatuskeluar!='0' and pp.idstatuskeluar!='4' and rp.idpendaftaran = pp.idpendaftaran and  ppeg.idpegawai = rp.idpegawaidokter and pper.idperson = ppeg.idperson and ru.idunit = rp.idunit ".((empty($mode)) ? "order by rp.waktu desc" : "order by rp.waktu asc" ) )->result();
        return $data;
    }
    
    //cek rencana perawatan rawat inap
    public function cekrencanaperawatan($idinap)
    {
        $sql = "SELECT b.waktu as waktuperencanaan, b.status as statusrencana, a.icd, a.statuspelaksanaan as statusperawatan
        from  rs_inap_rencana_medis_hasilpemeriksaan a
        join rs_inap_rencana_medis_pemeriksaan b  on b.idrencanamedispemeriksaan=a.idrencanamedispemeriksaan
        WHERE b.idinap='".$idinap."' and (a.statuspelaksanaan='rencana' or b.status='rencana') and b.waktu <= now() 

        UNION

        SELECT e.waktu as waktuperencanaan, e.status as statusrencana, ( select namabarang from rs_inap_rencana_medis_barang x , rs_barang y where x.idrencanamedisbarang=d.idrencanamedisbarang and y.idbarang=x.idbarang) as icd, d.statuspelaksanaan as statusperawatan
        from  rs_inap_rencana_medis_barangpemeriksaan d
        join rs_inap_rencana_medis_pemeriksaan e on e.idrencanamedispemeriksaan=d.idrencanamedispemeriksaan
        WHERE e.idinap='".$idinap."' and d.idrencanamedisbarang != '0' and (e.status='rencana' or d.statuspelaksanaan='rencana') and e.waktu <= now()";
        $query = $this->db->query($sql);
        return $query;
    }
    /**
     * Cek Status Hasil Perencanaan Perawatan
     * @param type $id  idrencanamedispemeriksaan
     */
    public function cek_statushasilperencanaanperawatan($id)
    {
        $sql = "SELECT a.icd, a.statuspelaksanaan from  rs_inap_rencana_medis_hasilpemeriksaan a WHERE a.idrencanamedispemeriksaan='".$id."' and a.statuspelaksanaan='rencana'
                UNION
                SELECT ( select namabarang from rs_barang y where y.idbarang=d.idbarang) as icd, d.statuspelaksanaan from  rs_inap_rencana_medis_barangpemeriksaan d WHERE d.idrencanamedispemeriksaan='".$id."' and d.statuspelaksanaan='rencana'";
        $query = $this->db->query($sql);
        return $query;
    }


    //hapus rencana yang belum terlaksana
    public function hapusrencanaperawatan($idinap)
    {
        $rencana = $this->db->query("select idrencanamedispemeriksaan from rs_inap_rencana_medis_pemeriksaan where idinap='".$idinap."' and status='rencana' and waktu > now() ");
        
        if($rencana->num_rows() > 0)
        {
            $dt =  $rencana->result_array();
            foreach ($dt as $arr)
            {
                $save = $this->db->delete('rs_inap_rencana_medis_hasilpemeriksaan',['idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan']]);
                $save = $this->db->delete('rs_inap_rencana_medis_barangpemeriksaan',['idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan']]);
                $save = $this->db->delete('rs_inap_rencana_medis_pemeriksaan',['idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan']]);
            }
            return $save;
        }
        return true;
    }
    
    //get nilai konfigurasi
    public function getKonfigurasi($nama)
    {
        $sql = $this->db->select('nilai')->get_where('konfigurasi',['nama'=>$nama])->row_array();
        $result = $sql['nilai'];
        return $result;
    }
    /**
     * get identitas pasien ranap
     * @param type $idrencana idrencanamedispasien
     */
    public function getIdentitasPasienRanap($idrencana)
    {
        $query = $this->db->query("SELECT a.status, d.nobed, e.namabangsal , c.keteranganradiologi, (select kelas from rs_kelas WHERE idkelas=e.idkelas) kelas, c.saranradiologi, namadokter(c.idpegawaidokter) as namadokter , date(a.waktu) as waktu "
                . "FROM rs_inap_rencana_medis_pemeriksaan a, rs_inap c, rs_bed d, rs_bangsal e "
                . "where a.idrencanamedispemeriksaan='".$idrencana."' and c.idinap=a.idinap and d.idbed=c.idbed and e.idbangsal=d.idbangsal");
        return $query->row_array();
    }
    
    /**
     * Laporan Kunjungan Pasien Baru
     */
    public function laporankunjunganpasienbaru($tanggal)
    {
        $result = $this->db->query("SELECT pp.norm, date(pp.waktuperiksa) as waktuperiksa, namapasien(pper.idperson) as namapasien, pper.tanggallahir, pper.nik, pper.tempatlahir, pper.jeniskelamin, pper.alamat, pper.notelpon, ru.namaunit FROM person_pendaftaran pp 
            join person_pasien ppas on ppas.norm=pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join rs_unit ru on ru.idunit = pp.idunit
            WHERE date(pp.waktuperiksa) ='".$tanggal."' and pp.ispasienlama = 0 and ru.tipeunit='UTAMA'")->result();
        return $result;
    }
    
    /**
     * Laporan Kunjungan Pasien Baru
     */
    public function laporankunjunganpasienlamabaru($tanggal1,$tanggal2)
    {
        $result = $this->db->query("SELECT rs.status, ru.namaunit, sum( if(pp.ispasienlama = 1,1, 0) ) as pasienlama, sum( if(pp.ispasienlama = 0,1, 0) ) as pasienbaru , sum(1) as totalpasien
        FROM person_pendaftaran pp 
        join rs_unit ru on ru.idunit = pp.idunit
        join rs_statuskeluar rs on rs.idstatuskeluar = pp.idstatuskeluar
        WHERE date(pp.waktuperiksa) BETWEEN '".$tanggal1."' and '".$tanggal2."' and ru.tipeunit='UTAMA'
        GROUP BY pp.idunit, pp.idstatuskeluar")->result();
        return $result;
    }
    
    /**
     * Laporan Kunjungan Pasien Berdasarkan Suku dan Bahasa
     */
    
    public function laporankunjunganpasienbyetnisdanbahasa($bulantahun)
    {
        $bulan = date('n', strtotime($bulantahun));
        $tahun = date('Y', strtotime($bulantahun));
        $suku = $this->db->query("SELECT sum(1) as jumlah, ps.suku, ps.sebutan FROM person_pendaftaran pp 
        JOIN person_pasien ppas on ppas.norm=pp.norm 
        JOIN person_person pper on pper.idperson=ppas.idperson 
        JOIN person_suku ps on ps.idsuku=pper.idsuku
        WHERE pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."'
        GROUP by pper.idsuku")->result();
        
        $bahasa=$this->db->query("SELECT sum(1) as jumlah, pb.bahasa FROM person_pendaftaran pp 
        JOIN person_pasien ppas on ppas.norm=pp.norm 
        JOIN person_person pper on pper.idperson=ppas.idperson 
        JOIN person_bahasa pb on pb.idbahasa=pper.idbahasa
        WHERE pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."'
        GROUP by pper.idbahasa")->result();        
        $data['suku']   = $suku;
        $data['bahasa'] = $bahasa;
        return $data;
    }
    
    /**
     * ambil barang depo berdasarkan idbarangpembelian
     * @param type $idp  idbarangpembelian
     */
    public function getbarangdepobyidbp($idp)
    {
        $sql = "select b.idbarang, rbs.idbarangpembelian, b.kode, b.namabarang, rbs.stok, s.namasatuan, rbp.batchno, rbp.kadaluarsa from 
        rs_barang_stok rbs
        join rs_barang_pembelian rbp on rbp.idbarangpembelian = rbs.idbarangpembelian
        join rs_barang b  on b.idbarang = rbp.idbarang
        join rs_satuan s on s.idsatuan = b.idsatuan
        where rbs.idunit = '".$this->session->userdata('idunitterpilih')."' and rbp.idbarangpembelian = '".$idp."'";
        $query = $this->db->query($sql)->row_array();
        return $query;
    }
    
    public function getdata_pasienantigen($tanggal,$tanggal2,$hasil)
    {
        $query = " SELECT pp.norm, pper.nik, pper.namalengkap, date(pp.waktuperiksa) as tanggalperiksa, pper.alamat, pper.notelpon, pper.tanggallahir, pper.jeniskelamin
        FROM rs_hasilpemeriksaan rh
        join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson
        WHERE icd = 'LD108' AND nilaitext ='".$hasil."' and (date(pp.waktuperiksa) BETWEEN '".$tanggal."' and '".$tanggal2."')and pp.idstatuskeluar='2'";
        $result = $this->db->query($query);
        return $result;
    }
    
    public function fillstokobat($idunit)
    {
        $query = $this->db->query("SELECT rbp.idbarangpembelian, rb.namabarang,rs.namasatuan, rb.hargajual, rbp.idbarang, sum(rbs.stok)stok, rbp.kadaluarsa from rs_barang_pembelian rbp, rs_barang_stok rbs, rs_barang rb, rs_satuan rs WHERE rb.ishidden=0 and rb.namabarang like '%".$this->input->get('q')."%' and  rbp.idbarang=rb.idbarang and rbs.idunit = ".$idunit." and rbs.idbarangpembelian=rbp.idbarangpembelian and rs.idsatuan=rb.idsatuan GROUP by rbp.idbarang, rbp.idbarangpembelian")->result();
        return $query;
        
    }
    
    public function getdata_laporanrmganda($tanggal1,$tanggal2)
    {
        $sql = "SELECT lp.*, namapasien(p.idperson) as pasienasal, namapasien(p2.idperson) as pasientujuan, u.namauser
                FROM log_pindahrekammedis lp
                join person_pasien p on p.norm =lp.normasal
                join person_pasien p2 on p2.norm = lp.normtujuan
                join login_user u on u.iduser = lp.iduser
                WHERE date(lp.waktu) BETWEEN '".$tanggal1."' and '".$tanggal2."'";
        $data = $this->db->query($sql)->result_array();
        return $data;
    }
    
    public function databarangslowmoving()
    {
        $sql = "select rb.kode, rb.namabarang, sum(rbs.stok) as stok, rs.namasatuan, rbp.batchno, rbp.kadaluarsa
        from rs_barang rb
        left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang
        left join rs_barang_stok rbs on rbs.idbarangpembelian = rbp.idbarangpembelian and rbs.idunit = 24 and rbs.stok > 0
        left join rs_satuan rs on rs.idsatuan = rb.idsatuan
        WHERE rb.terakhir_penjualan > '2021-08-01'  and rbs.stok > 0
        GROUP by rb.idbarang";
        $result = $this->db->query($sql);
        return $result;
    }
    
    public function databarangfastmoving()
    {
        $sql = "select rb.kode, rb.namabarang, sum(rbs.stok) as stok, rs.namasatuan, rbp.batchno, rbp.kadaluarsa
        from rs_barang rb
        left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang
        left join rs_barang_stok rbs on rbs.idbarangpembelian = rbp.idbarangpembelian and rbs.idunit = 24 and rbs.stok > 0
        left join rs_satuan rs on rs.idsatuan = rb.idsatuan
        WHERE rb.terakhir_penjualan > '2021-08-01'  and rbs.stok > 0
        GROUP by rb.idbarang";
        $result = $this->db->query($sql);
        return $result;
    }
    
    //get rencana medis ranap
    
    public function get_rencanamedisranap($idinap)
    {
        $data = $this->db->select('waktu, idrencanamedispemeriksaan, status')->order_by('waktu','desc')->get_where('rs_inap_rencana_medis_pemeriksaan',['idinap'=>$idinap,'status'=>'terlaksana'])->result_array();
        return $data;
    }
    
    public function get_rencanamedisranaprowarray($idinap)
    {
        $data = $this->db->select('waktu, GROUP_CONCAT(idrencanamedispemeriksaan) as id')
                    ->order_by('waktu','desc')
                    ->get_where('rs_inap_rencana_medis_pemeriksaan',['idinap'=>$idinap,'status'=>'terlaksana'])
                    ->row_array();
        return $data;
    }


    public function get_catatanterintegrasiranap($dtranap)
    {
        $idr = $dtranap['id'];
        $data = $this->db->query("
                        select 
                                date(a.waktu) as tanggal, 
                                time(a.waktu) as jam, 
                                b.profesi, 
                                soa as soa_soa, 
                                p as soa_p , 
                                a.sip, 
                                namadokter(a.idpegawai) as petugas 
                        from rs_inap_soap a 
                        join person_grup_profesi b on b.idprofesi = a.idprofesi 
                        where a.idrencanamedispemeriksaan  in ('". $idr ."') 
                        order by waktu asc")->result();
        return $data;
    }
    
    public function get_identitaspasien($idp)
    {
        $data = $this->db->query('select ppas.norm, pper.nik, pper.identitas, pper.namalengkap, pper.tanggallahir, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.agama, pper.alamat, ppas.alergi from person_pendaftaran pp 
           join person_pasien ppas on ppas.norm=pp.norm
           join person_person pper on pper.idperson=ppas.idperson
           WHERE idpendaftaran='.$idp)->row_array();
        return $data;
    }
    
}

/* End of file ${TM_FILENAME:${1/(.+)/Mkombin.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Mkombin/:application/models/${1/(.+)/Mkombin.php/}} */






