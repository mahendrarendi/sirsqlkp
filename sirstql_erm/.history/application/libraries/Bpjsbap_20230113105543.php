<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'LZCompressor/autoload.php'; // untuk decompress response dari vclaim v2

//copy define to helpers
//defined('BPJS_PESERTA') OR define('BPJS_PESERTA', 1);
//defined('BPJS_RUJUKAN') OR define('BPJS_RUJUKAN', 2);
//defined('BPJS_SEP') OR define('BPJS_SEP', 3);
//defined('BPJS_APLICARE') OR define('BPJS_APLICARE', 4);
//defined('KMK_SIRANAP') OR define('KMK_SIRANAP', 11);
//
//defined('JENISAPLIKASI_VCLAIM') OR define('JENISAPLIKASI_VCLAIM', 1);
//defined('JENISAPLIKASI_APLICARE') OR define('JENISAPLIKASI_APLICARE', 2);
//defined('JENISAPLIKASI_SIRANAP') OR define('JENISAPLIKASI_SIRANAP', 11);
//
//defined('JENISCONSID_DEVELOPMENT') OR define('JENISCONSID_DEVELOPMENT', 1);
//defined('JENISCONSID_PRODUCTION') OR define('JENISCONSID_PRODUCTION', 2);
/**
 * Bpjsbap.php
 * <br />BPJS Class / Indonesian National Health Insurance + Kemenkes / Indonesian Health Ministry
 * <br />All documentation will be in Bahasa Indonesia
 * 
 * @author Basit Adhi Prabowo, S.T. <basit@unisayogya.ac.id>
 * @access public
 * @link https://github.com/basit-adhi/MyCodeIgniterLibs/blob/master/libraries/Bpjsbap.php
 */
class Bpjsbap
{
    /**
     *
     * @var type merupakan kode consumer (pengakses web-service). Kode ini akan diberikan oleh BPJS Kesehatan/Kemenkes
     */
    private $Xconsid;
    /**
     *
     * @var type informasi Consumer Secret, hanya disimpan oleh service consumer. Tidak dikirim ke server web-service, hal ini untuk menjaga pengamanan yang lebih baik. Sedangkan kebutuhan Consumer Secret ini adalah untuk men-generate Signature (X-signature).
     */
    private $Xconssecret;
    /**
     *
     * @var type merupakan kode PPK yang diberikan oleh BPJS Kesehatan/Kemenkes
     */
    
    private $Userkey;
    
    private $Xkodeppk;
    private $norujukan  = "";
    private $response;
    private $idrequest;
    private $jenisaplikasi;
    private $jenisconsid;
    private $url;
    private $Xtimestamp;
    private $ContentType;

    //katalog VClaim & Aplicare: 
    //https://dvlp.bpjs-kesehatan.go.id/VClaim-Katalog/
    
    //katalog Sisrute:
    //http://sirs.yankes.kemkes.go.id/sirsservice/start/ts
    
    
    /*0179R014 RS Queen Latifa
                    user_key vclaim: a978286150a71bac147da294e2dfb14c
                    Consid - Secretkey : 22634 - 0fV130B0EF
     * cons-id dan secret key untuk vclaim,applicare,bpjs_antrian online
     */
    private function upass()
    {
        if(file_exists("./QLKP.php"))
        {
            // setting for QLKP - RSU QUEEN LATIFA KULON PROGO
            switch ($this->jenisaplikasi)
                {
                case JENISAPLIKASI_VCLAIM_V2:
                        $this->Xconsid      = 17874;
                        $this->Xconssecret  = '9mEE4477DC';
                        $this->Userkey      = 'bed19983b91783818cb549ec40c3364b';
                        $this->Xkodeppk     = "0176R009";
                break;
                    case JENISAPLIKASI_VCLAIM:
                        $this->Xconsid      = 32592;
                        $this->Xconssecret  = '8tS1047C5B';
                        $this->Xkodeppk     = "0226R005";
                    break;
                    case JENISAPLIKASI_BPJS_ANTROL:
                        $this->Xconsid      = 17874;
                        $this->Xconssecret  = '9mEE4477DC';
                        $this->Userkey      = 'edd9ced8910e5d24b2641cabfa0a0b0f';
                        $this->Xkodeppk     = "0176R009";
                    break;
                    case JENISAPLIKASI_APLICARE:
                        $this->Xconsid      = 17874;
                        $this->Xconssecret  = '9mEE4477DC';
                        $this->Xkodeppk     = "0176R009";
                        break;
                    case JENISAPLIKASI_SIRANAP:
                    case COVID19:
                        $this->Xconsid      = 3404045;
                        $this->Xconssecret  = '12345';
                        break;
                } 
        }
        else 
        {          
            // setting for QL-Sleman      
            switch ($this->jenisaplikasi)
                {
                    case JENISAPLIKASI_VCLAIM_V2:
                        $this->Xconsid      = 22634;
                        $this->Xconssecret  = '0fV130B0EF';
                        $this->Userkey      = '0726b0764b6b3b595e1e72817a231936';  //a978286150a71bac147da294e2dfb14c';
                        $this->Xkodeppk     = "0179R014";
                        break;
                    case JENISAPLIKASI_VCLAIM:
                        $this->Xconsid      = 22634;
                        $this->Xconssecret  = '0fV130B0EF';
                        $this->Xkodeppk     = "0179R014";
                        break;
                    case JENISAPLIKASI_BPJS_ANTROL:
                        $this->Xconsid      = 22634;
                        $this->Xconssecret  = '0fV130B0EF';
                        $this->Userkey      = 'b9a5273459fca3ebd728ffe6f0c14f6f';
                        $this->Xkodeppk     = "0179R014";
                        break;
                    case JENISAPLIKASI_APLICARE:
                        $this->Xconsid      = 22634;
                        $this->Xconssecret  = '0fV130B0EF';
                        $this->Xkodeppk     = "0179R014";
                        break;
                    case JENISAPLIKASI_SIRANAP:
                        // break;
                    case COVID19:
                        $this->Xconsid      = 3404045;
                        $this->Xconssecret  = 'S!rs2020!!';
                        break;
                    case JENISAPLIKASI_WEBQL:
                        $this->Xconsid      = 3404045;
                        $this->Xconssecret  = 'QLrs2021!!';
                        break;
                }   
        }
    }
    
    private function isBPJS()
    {
        return $this->jenisaplikasi == JENISAPLIKASI_VCLAIM || $this->jenisaplikasi == JENISAPLIKASI_APLICARE;
    }
    
    private function isKemenkes()
    {
        return $this->jenisaplikasi == JENISAPLIKASI_SIRANAP || $this->jenisaplikasi == COVID19;
    }
    
    function createSignature($requestParameter, $uploadedJSON = '', $method = 'POST')
    {
        $debug = "";
        $this->upass();
        if ($this->jenisconsid == JENISCONSID_DEVELOPMENT)
        {
            switch ($this->jenisaplikasi)
            {
                case JENISAPLIKASI_VCLAIM:      $this->url = 'https://dvlp.bpjs-kesehatan.go.id/vclaim-rest'; break;
                case JENISAPLIKASI_VCLAIM_V2:   $this->url = 'https://apijkn-dev.bpjs-kesehatan.go.id/vclaim-rest-dev'; break;
                case JENISAPLIKASI_BPJS_ANTROL: $this->url = 'https://apijkn-dev.bpjs-kesehatan.go.id/antreanrs_dev'; break;
                case JENISAPLIKASI_APLICARE:    $this->url = 'https://dvlp.bpjs-kesehatan.go.id:8888/aplicaresws'; break;
                case COVID19:                   $this->url = 'http://sirs.kemkes.go.id/fo/index.php'; break;
                case JENISAPLIKASI_SIRANAP:     $this->url = 'http://sirs.kemkes.go.id/fo/index.php'; break; //cek: http://yankes.kemkes.go.id/app/siranap/pages/rslain, entri: http://yankes.kemkes.go.id/app/siranap-yankes/login
                case JENISAPLIKASI_WEBQL:       $this->url = 'http://localhost/anjungan/api'; break;
            }
        }
        else if ($this->jenisconsid == JENISCONSID_PRODUCTION)
        {
            switch ($this->jenisaplikasi)
            {
                case JENISAPLIKASI_VCLAIM:      $this->url = 'https://new-api.bpjs-kesehatan.go.id:8080/new-vclaim-rest/'; break;
                case JENISAPLIKASI_VCLAIM_V2:   $this->url = 'https://apijkn.bpjs-kesehatan.go.id/vclaim-rest'; break;                
                case JENISAPLIKASI_BPJS_ANTROL: $this->url = 'https://apijkn.bpjs-kesehatan.go.id/antreanrs'; break;
                case JENISAPLIKASI_APLICARE:    $this->url = 'https://new-api.bpjs-kesehatan.go.id/aplicaresws'; break;
                case COVID19:                   $this->url = 'http://sirs.kemkes.go.id/fo/index.php'; break;
                case JENISAPLIKASI_SIRANAP:     $this->url = 'http://sirs.kemkes.go.id/fo/index.php'; break; //cek: http://yankes.kemkes.go.id/app/siranap/pages/rslain, entri: http://yankes.kemkes.go.id/app/siranap-yankes/login
                case JENISAPLIKASI_WEBQL:       $this->url = 'https://app.queenlatifa.co.id/api'; break;
                
            }
        }
        
        //menghitung timestamp
        date_default_timezone_set('UTC');
        $tStamp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $this->Xtimestamp = $tStamp;
        if ($this->isBPJS())
        {
            //menghitung tanda tangan dengan melakukan hash terhadap salt dengan kunci rahasia sebagai kunci
            $signature          = base64_encode(hash_hmac('sha256', $this->Xconsid."&".$tStamp, $this->Xconssecret, true));
            $headers = array(   "Accept: application/json",
                                "X-cons-id: ".$this->Xconsid, 
                                "X-timestamp: ".$tStamp, 
                                "X-signature: ".$signature,
                                "Content-Type: application/json"
                            );
        }
        else if ($this->isKemenkes())
        {
            $headers = array(   "X-rs-id: ".$this->Xconsid,
                                "X-pass: ".md5($this->Xconssecret),
                                "Content-Type: application/xml; charset=ISO-8859-1",
                                "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15"
                            );
            if ($this->jenisaplikasi == COVID19)
            {
                $headers[] = "X-Timestamp: ".$tStamp;
            }
        }
        
        if($this->jenisaplikasi == JENISAPLIKASI_WEBQL)
        {
            //menghitung tanda tangan dengan melakukan hash terhadap salt dengan kunci rahasia sebagai kunci
            $signature          = base64_encode(hash_hmac('sha256', $this->Xconsid."&".$tStamp, $this->Xconssecret, true));
            $headers = array(   "Accept: application/json",
                                "X-cons-id: ".$this->Xconsid, 
                                "X-timestamp: ".$tStamp, 
                                "X-signature: ".$signature,
                                "X-data: ".$uploadedJSON,
                                "Content-Type: application/json"
                            );
        }
        else if($this->jenisaplikasi == JENISAPLIKASI_VCLAIM_V2)
        {
            //menghitung tanda tangan dengan melakukan hash terhadap salt dengan kunci rahasia sebagai kunci
            $signature          = base64_encode(hash_hmac('sha256', $this->Xconsid."&".$tStamp, $this->Xconssecret, true));
            $headers = array(   "Accept: application/json",
                                "X-cons-id: ".$this->Xconsid, 
                                "X-timestamp: ".$this->Xtimestamp, 
                                "X-signature: ".$signature,
                                "user_key: ".$this->Userkey,
                                "Format:Json",
                                "Content-Type: ".$this->ContentType
                            );
        }
        else if($this->jenisaplikasi == JENISAPLIKASI_BPJS_ANTROL)
        {
            //menghitung tanda tangan dengan melakukan hash terhadap salt dengan kunci rahasia sebagai kunci
            $signature          = base64_encode(hash_hmac('sha256', $this->Xconsid."&".$tStamp, $this->Xconssecret, true));
            $headers = array(   "Accept: application/json",
                                "X-cons-id: ".$this->Xconsid, 
                                "X-timestamp: ".$this->Xtimestamp, 
                                "X-signature: ".$signature,
                                "user_key: ".$this->Userkey,
                                "Format:Json",
                                "Content-Type: application/json"
                            );
        }
        
        $debug .= json_encode($headers);
        
        $ch = curl_init($this->url.$requestParameter);
        $debug .= $this->url.$requestParameter;
        $debug .= $uploadedJSON;
                
        if($this->jenisaplikasi == JENISAPLIKASI_WEBQL)
        {
            if (empty(!$uploadedJSON)) 
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $uploadedJSON);
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);            
            if($this->jenisconsid == JENISCONSID_PRODUCTION)
            {
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            }
            
        }
        else if($this->jenisaplikasi == JENISAPLIKASI_VCLAIM_V2 OR $this->jenisaplikasi == JENISAPLIKASI_BPJS_ANTROL)
        {
           if ($uploadedJSON != '') 
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $uploadedJSON);
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        
        
        if ($this->isBPJS())
        {
            if ($uploadedJSON != '') 
            {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $uploadedJSON);
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        else if ($this->isKemenkes())
        {
            if ($uploadedJSON != '') 
            {
                if ($this->jenisaplikasi == COVID19)
                {
                    if($method!='GET')
                    {
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);                       
                    }
                }
                else
                {
                    curl_setopt($ch, CURLOPT_POST, 1);
                }
                curl_setopt($ch, CURLOPT_POSTFIELDS, $uploadedJSON);
            }
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_NOBODY, false);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            if ($this->jenisaplikasi != COVID19) 
            {
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            }
        }
        
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);

        if (empty($data) or $data == "null")
        {
            $data = curl_error($ch);
        }
        curl_close($ch);
        //debug mode
        // if($this->jenisaplikasi == JENISAPLIKASI_WEBQL)
        // { 
        //     var_dump("<<11>>");
        //     // var_dump($data);
        //     $dedata = json_decode($data); 
        //     var_dump($dedata);
        //     var_dump("<<11>>");
        // }
        return json_decode($data);
        
    }
    
    //vclaim
    function requestPropinsi()
    {
        $this->idrequest        = BPJS_APLICARE;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = json_decode(json_encode($this->createSignature("/referensi/propinsi")), true);
        if ($this->response["metaData"]["code"] == 200 && $this->idrequest == BPJS_APLICARE)
        {
            return  $this->response['response']['list'];
        }
    }
    
    function requestPeserta($nobpjs,$tglsep)
    {
        $this->idrequest        = BPJS_PESERTA;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION; //JENISCONSID_DEVELOPMENT;
        $this->response         = $this->createSignature("/Peserta/nokartu/$nobpjs/tglSEP/".$tglsep);
        if ($this->response->metaData->code== 200)
        {
            return  $this->response->response->peserta;
        }
    }
    
    function requestPoli()
    {
        $namaruang              = ["ivp", "int", "icu", "sar", "mat", "bed", "mat", "tht", "obg", "obs", "ana", "uro", "ort", "gig", "klt", "kul", "kel", "onk", "hem", "reh"];
        $ruangbpjs              = array();
        $this->idrequest        = BPJS_PESERTA;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION; //JENISCONSID_DEVELOPMENT;
        foreach ($namaruang as $r)
        {
            $this->response         = json_decode(json_encode($this->createSignature("/referensi/poli/".$r)), true);
            if ($this->response["metaData"]["code"] == 200)
            {
                foreach ($this->response['response']['poli'] as $p)
                {
                    $ruangbpjs[$p['kode']] = $p['nama'];
                }
            }
        }
        return $ruangbpjs;
    }
    
    function requestRujukan($norujukan)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/Rujukan/$norujukan");
      
        if($this->response->metaData->code == 200)
        {
            return $this->response->response;
        }  
    }
    
    function requestRujukanSingle($nokartu)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/Rujukan/Peserta/$nokartu");
        if($this->response->metaData->code == 200)
        {
            return $this->response->response->rujukan;
        }
    }
    
    function requestRujukanSingle_COBA($nokartu)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/Rujukan/List/Peserta/$nokartu");
        // if($this->response->metaData->code == 200)
        // {
        
            return $this->response;
        // }
        // $this->response         = $this->createSignature("/Rujukan/Peserta/".$nokartu);
        // // if($this->response->metaData->code == 200)
        // // {
        // //     return $this->response->response->rujukan;
        // // }
        // return  $this->response;
    }
    
        
        function cekrequestRujukanSingle($nokartu)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/Rujukan/Peserta/$nokartu");
//        if($this->response->metaData->code == 200)
//        {
//            return $this->response->response->rujukan;
//        }
        return  $this->response;
    }
        

    function requestRujukanMulti($nokartu)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/Rujukan/List/Peserta/$nokartu");
        if($this->response->metaData->code == 200)
        {
            return $this->response->response;
        }
    }
    
    function requestSEP($nosep)
    {
        $this->idrequest        = BPJS_SEP;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature("/SEP/$nosep");
        if ($this->response->metaData->code == 200 && $this->idrequest == BPJS_SEP)
        {
            return $this->response->response;
        }
    }

    function requestDataKunjungan($tglKunjungan,$jnsPelayanan)
    {
        $this->idrequest        = BPJS_MONITORING;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response = $this->createSignature("/Monitoring/Kunjungan/Tanggal/$tglKunjungan/JnsPelayanan/$jnsPelayanan");
        if($this->response->metaData->code== 200)
        {
            return $this->response->response;
        }
    }
    
    function DUMMY_requestPeserta($nobpjs)
    {
        $this->idrequest        = BPJS_PESERTA;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        if (substr($nobpjs, 0, 10) == '0011223344')
        {
            $kelas  = [["id" => 1, "nama" => "Kelas I"], ["id" => 2, "nama" => "Kelas II"], ["id" => 3, "nama" => "Kelas III"]];
            $pkelas = $kelas[array_rand($kelas, 1)];
            $jk     = ["L", "P"];
            $pjk    = $jk[array_rand($jk)];
            $this->response = json_decode('{"metaData":{"code":"200","message":"OK"},"response":{"peserta":{"cob":{"nmAsuransi":null,"noAsuransi":null,"tglTAT":null,"tglTMT":null},"hakKelas":{"keterangan":"'.$pkelas["nama"].'","kode":"'.$pkelas["id"].'"},"informasi":{"dinsos":null,"noSKTM":null,"prolanisPRB":null},"jenisPeserta":{"keterangan":"PEGAWAI SWASTA","kode":"13"},"mr":{"noMR":null,"noTelepon":null},"nama":"DUMMY'.$nobpjs.'","nik":"3311022033440001","noKartu":"'.$nobpjs.'","pisa":"1","provUmum":{"kdProvider":"0138U020","nmProvider":"KPRJ PALA MEDIKA"},"sex":"'.$pjk.'","statusPeserta":{"keterangan":"AKTIF","kode":"0"},"tglCetakKartu":"2016-02-12","tglLahir":"1981-10-10","tglTAT":"2014-12-31","tglTMT":"2008-10-01","umur":{"umurSaatPelayanan":"35 tahun ,1 bulan ,11 hari","umurSekarang":"35 tahun ,2 bulan ,10 hari"}}}}');
        }
        else
        {
            $this->response = json_decode('{"metaData":{"code":"401","message":"ERROR"}}');
        }
    }
    
    function DUMMY_requestRujukan($norujukan)
    {
        $this->idrequest        = BPJS_RUJUKAN;
        $this->jenisaplikasi    = JENISAPLIKASI_VCLAIM;
        if (substr($norujukan, 0, 13) == '001122334455Y')
        {
            $tglkunjungan   = date('Y-m-d');
            $nobpjstercatat = ['001122334455','001122334456','001122334457','001122334458'];
            $nobpjs         = $nobpjstercatat[array_rand($nobpjstercatat)];
            $this->response = json_decode('{"metaData":{"code":"200","message":"OK"},"response":{"rujukan":{"diagnosa":{"kode":"N40","nama":"Hyperplasia of prostate"},"keluhan":"kencing tidak puas","noKunjungan":"'.$norujukan.'","pelayanan":{"kode":"2","nama":"Rawat Jalan"},"peserta":{"cob":{"nmAsuransi":null,"noAsuransi":null,"tglTAT":null,"tglTMT":null},"hakKelas":{"keterangan":"KELAS I","kode":"1"},"informasi":{"dinsos":null,"noSKTM":null,"prolanisPRB":null},"jenisPeserta":{"keterangan":"PENERIMA PENSIUN PNS","kode":"15"},"mr":{"noMR":"298036","noTelepon":null},"nama":"MUSDIWAR,BA","nik":null,"noKartu":"'.$nobpjs.'","pisa":"2","provUmum":{"kdProvider":"03010701","nmProvider":"SITEBA"},"sex":"L","statusPeserta":{"keterangan":"AKTIF","kode":"0"},"tglCetakKartu":"2017-11-13","tglLahir":"1938-08-31","tglTAT":"2038-08-31","tglTMT":"1996-08-20","umur":{"umurSaatPelayanan":"78 tahun ,6 bulan ,6 hari","umurSekarang":"79 tahun ,3 bulan ,18 hari"}},"poliRujukan":{"kode":"URO","nama":"UROLOGI"},"provPerujuk":{"kode":"03010701","nama":"SITEBA"},"tglKunjungan":"'.$tglkunjungan.'"}}}');
        }
        else
        {
            $this->response = json_decode('{"metaData":{"code":"401","message":"ERROR"}}');
        }
    }
     
    function getInfoPrimer()
    {
        if ($this->response->metaData->code == 200 && ($this->idrequest == BPJS_PESERTA || $this->idrequest == BPJS_RUJUKAN || $this->idrequest == BPJS_SEP))
        {
            switch ($this->idrequest)
            {
                case BPJS_PESERTA:
                    $peserta = $this->response->response->peserta; break;
                case BPJS_RUJUKAN:
                    $peserta = $this->response->response->rujukan->peserta; break;
                case BPJS_SEP:
                    $this->requestRujukan($this->norujukan);
                    return $this->getInfoPrimer();
                default:
                    return array();
            }
            return  [   'nik'           => $peserta->nik,
                        'nokartu'       => $peserta->noKartu,
                        'namaLengkap'   => $peserta->nama,
                        'tglLahir'      => $peserta->tglLahir,
                        'jenisKelamin'  => $peserta->sex,
                        'noTelepon'     => ($this->idrequest == BPJS_SEP) ? "" : $peserta->mr->noTelepon
                    ];
        }
        else
        {
            return array();
        }
    }
    
    function getInfoSekunder()
    {
        if ($this->response->metaData->code == 200 && ($this->idrequest == BPJS_PESERTA || $this->idrequest == BPJS_RUJUKAN || $this->idrequest == BPJS_SEP))
        {
            switch ($this->idrequest)
            {
                case BPJS_PESERTA:
                    $peserta = $this->response->response->peserta; break;
                case BPJS_RUJUKAN:
                    $peserta = $this->response->response->rujukan->peserta; break;
                case BPJS_SEP:
                    $this->requestRujukan($this->norujukan);
                    return $this->getInfoSekunder();
                default:
                    return array();
            }
            return  [   'jenisPeserta'  => $peserta->jenisPeserta->kode,
                        'hakKelas'      => $peserta->hakKelas->kode,
                        'informasi'     => trim(str_replace(',,', '', implode(',', (array) $peserta->informasi))),
                        'tglCetakKartu' => $peserta->tglCetakKartu,
                        'statusPeserta' => $peserta->statusPeserta->kode,
                        'norujukan'     => $this->norujukan 
                    ];
        }
        else
        {
            return array();
        }
    }
    
    function getInfoRujukan()
    {
        if ($this->response->metaData->code == 200 && ($this->idrequest == BPJS_RUJUKAN))
        {
            $rujukan = $this->response->response->rujukan;
            return  $rujukan->poliRujukan->nama." [".$rujukan->diagnosa->kode."] ".$rujukan->diagnosa->nama." - ".$rujukan->keluhan." pada ".$rujukan->tglKunjungan;
}
        else
        {
            return array();
        }
    }
    
    //aplicares
    function requestKelas($jenisconsid)
    {
        $this->idrequest        = BPJS_APLICARE;
        $this->jenisaplikasi    = JENISAPLIKASI_APLICARE;
        $this->jenisconsid      = $jenisconsid;
        $this->response         = json_decode(json_encode($this->createSignature("/rest/ref/kelas")), true);
        if ($this->response["metadata"]["code"] == 1 && $this->idrequest == BPJS_APLICARE)
        {
            return  $this->response['response']['list'];
        }

    }
    
    function requestBedlist($jenisconsid)
    {
        $this->idrequest        = BPJS_APLICARE;  //4
        $this->jenisaplikasi    = JENISAPLIKASI_APLICARE;  //2
        $this->jenisconsid      = $jenisconsid; //2 //JENISCONSID_PRODUCTION
        $this->upass();
        $this->response         = json_decode(json_encode($this->createSignature("/rest/bed/read/".$this->Xkodeppk."/1/100",'','GET')), true);
        if ($this->response == null)
        {
            return $this->Xkodeppk;
        }
        else if (is_array($this->response))
        {
            $code                   = array_key_exists("code", $this->response["metadata"]) ? $this->response["metadata"]["code"] : (array_key_exists("Code", $this->response) ? $this->response["Code"] : 0); 
            if ($code == 1 && $this->idrequest == BPJS_APLICARE)
            {
                return $this->response["response"]["list"];
            }
            else
            {
                return $this->response["metadata"]["message"];
            }
        }
        else 
        {
            return $this->response;
        }
    }
    
    function updateBed($jenisconsid, $uploadedJSON)
    {
        $this->idrequest        = BPJS_APLICARE;
        $this->jenisaplikasi    = JENISAPLIKASI_APLICARE;
        $this->jenisconsid      = $jenisconsid;
        $this->upass();
        $this->response         = $this->createSignature("/rest/bed/update/".$this->Xkodeppk, $uploadedJSON);
        return $this->response;
    }
    
    function createBed($jenisconsid, $uploadedJSON)
    {
        $this->idrequest        = BPJS_APLICARE;
        $this->jenisaplikasi    = JENISAPLIKASI_APLICARE;
        $this->jenisconsid      = $jenisconsid;
        $this->upass();
        $this->response         = $this->createSignature("/rest/bed/create/".$this->Xkodeppk, $uploadedJSON);
        return $this->response;
    }
    
    function deleteBed($jenisconsid, $uploadedJSON)
    {
        $this->idrequest        = BPJS_APLICARE;
        $this->jenisaplikasi    = JENISAPLIKASI_APLICARE;
        $this->jenisconsid      = $jenisconsid;
        $this->upass();
        $this->response         = $this->createSignature("/rest/bed/delete/".$this->Xkodeppk, $uploadedJSON);
        return $this->response;
    }
    
    //sisrute
    function sisruteXMLfromJson($theJSON, $version = "1.0")
    {
        $theXML         = '';
        $uploadedArray  = json_decode($theJSON);
        foreach ($uploadedArray as $child)
        {
            $xml    = new SimpleXMLElement('<data/>');
            array_walk_recursive($child, function ($value, $key) use ($xml) { $xml->addChild($key, $value); });
            $theXML .= $xml->asXML();
        }
        return '<?xml version="'.$version.'" encoding="UTF-8"?><xml version="'.$version.'">'.str_ireplace('<>', '', str_ireplace('--', '', str_ireplace('?', '', str_ireplace('xml version="1.0"', '', $theXML)))).'</xml>';
    }
    
    function insertupdatedeleteBedSiranap($jenisconsid, $uploadedJSON)
    {
        $this->idrequest        = KMK_SIRANAP;
        $this->jenisaplikasi    = JENISAPLIKASI_SIRANAP;
        $this->jenisconsid      = $jenisconsid;
        $this->response         = $this->createSignature("/ranap", $this->sisruteXMLfromJson($uploadedJSON));
        return $this->response;
    }
    
    function insertupdatedeletePelayananSiranap($jenisconsid, $jenis, $tanggal, $uploadedJSON)
    {
        $this->idrequest        = KMK_SIRANAP;
        $this->jenisaplikasi    = JENISAPLIKASI_SIRANAP;
        $this->jenisconsid      = $jenisconsid;
        $this->response         = $this->createSignature("/".$jenis."/".$tanggal, $this->sisruteXMLfromJson($uploadedJSON));
        return $this->response;
    }
    
    function requestkabupatenCovid19($jenisconsid, $uploadedJSON='')
    {
        $this->idrequest        = COVID19;
        $this->jenisaplikasi    = JENISAPLIKASI_SIRANAP;
        $this->jenisconsid      = $jenisconsid;
        $this->response = $this->createSignature("/Referensi/Kabupaten", $uploadedJSON);
        return  $this->response;    
    }
//}
    function settingRequestCovid19($requestParameter,$uploadedJSON,$method)
    {
        $this->idrequest        = COVID19;
        $this->jenisaplikasi    = COVID19;
        $this->jenisconsid      = JENISCONSID_DEVELOPMENT;
        $this->response = $this->createSignature($requestParameter, $uploadedJSON,$method);
        return  $this->response;
    }
    function requestGetCovid19($requestParameter,$uploadedJSON='')
    {
        return $this->settingRequestCovid19($requestParameter,$uploadedJSON,'GET');
    }
    function requestPostCovid19($requestParameter,$uploadedJSON='')
    {
        return $this->settingRequestCovid19($requestParameter,$uploadedJSON,'POST');
    }
    function requestPutCovid19($requestParameter,$uploadedJSON='')
    {
        return $this->settingRequestCovid19($requestParameter,$uploadedJSON,'PUT');
    }
    function requestDeleteCovid19($requestParameter,$uploadedJSON='')
    {
        return $this->settingRequestCovid19($requestParameter,$uploadedJSON,'DELETE');
    }
    
    function crudWebql($requestParameter,$uploadedJSON)
    {
        $this->jenisaplikasi    = JENISAPLIKASI_WEBQL;
        $this->jenisconsid      = JENISCONSID_PRODUCTION;
        $this->response         = $this->createSignature($requestParameter, $uploadedJSON,'POST');
        return $this->response;
    }
    
    //========================================================================
    /**
     * web service vclaim v2
     */    

    // function decrypt
    function vclaimDecrypt($key, $string)
    {
        $encrypt_method = 'AES-256-CBC';
        // hash
        $key_hash = hex2bin(hash('sha256', $key));
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hex2bin(hash('sha256', $key)), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key_hash, OPENSSL_RAW_DATA, $iv);
        return $output;
    }

    // function lzstring decompress 
    // download libraries lzstring : https://github.com/nullpunkt/lz-string-php
    function vclaimDecompress($string)
    {
        return \LZCompressor\LZString::decompressFromEncodedURIComponent($string);
    }
    
    //mahmud, clear
    /**
     * Request Vclaim v2
     * @param type $url Url
     * @param type $uploadData data yang diupload
     * @param type $method method yang digunakan (GET/POST/DELETE)
     * @param type $contenType Content Type
     * @return type
     */
    function requestVclaim($url,$uploadData,$method,$contenType='')
    {
        $this->jenisaplikasi= JENISAPLIKASI_VCLAIM_V2;
        $this->jenisconsid  = JENISCONSID_PRODUCTION; //JENISCONSID_DEVELOPMENT;        
        $this->ContentType  = ((empty($contenType)) ? "application/json; charset=utf-8" : $contenType );
        $this->response     = $this->createSignature($url,$uploadData,$method);
        
        $result['url']      = $url;
        
        //jika gagal --> permintaan telah diterima untuk diproses, tetapi pemrosesan belum selesai
        if(empty($this->response))
        {
            $result['metaData']['code'] = '202';
            $result['metaData']['message'] = 'Bridging Sedang Bermasalah, Silakan Coba Lagi.!';
            return $result;
        }
        
        //jika berhasil -->permintaan telah diterima untuk diproses, dan pemrosesan selesai
        $result['metaData'] = ((isset($this->response->metaData)) ? $this->response->metaData : $this->response );
        if($this->response->metaData->code == 200 && empty(!$this->response->metaData))
        {
            //decrypt hasil response
            $decrypt    = $this->vclaimDecrypt($this->Xconsid.$this->Xconssecret.$this->Xtimestamp,$this->response->response);
            //decompress hasil response
            $decompress = $this->vclaimDecompress($decrypt);            
            $result['response'] = json_decode($decompress);
        }
        return $result;
    }
    //end Vclaim v2 web service
    
    
    
    //========================================================================
    /**
     * mahmud, clear
     * web service Antrian Online BPJS v2
     * @param type $url Url
     * @param type $uploadData data yang diupload
     * @param type $method method yang digunakan (GET/POST/DELETE)
     * @param type $contenType Content Type
     * @return type
     */ 
    
    function requestBpjsAntrol($url,$uploadData,$method,$contenType='')
    {
        $this->jenisaplikasi= JENISAPLIKASI_BPJS_ANTROL;
        $this->jenisconsid  = JENISCONSID_PRODUCTION; //JENISCONSID_DEVELOPMENT;        
        $this->ContentType  = ((empty($contenType)) ? "application/json; charset=utf-8" : $contenType );
        $this->response     = $this->createSignature($url,$uploadData,$method);
        
        $result['url']      = $url;
        //jika gagal --> permintaan telah diterima untuk diproses, tetapi pemrosesan belum selesai
        if(empty($this->response))
        {
            $result['metaData']['code'] = '202';
            $result['metaData']['message'] = 'Bridging Sedang Bermasalah, Silakan Coba Lagi.!';
            return $result;
        }
        
        //jika berhasil -->permintaan telah diterima untuk diproses, dan pemrosesan selesai
        $result['metaData'] = ((isset($this->response->metadata)) ? $this->response->metadata : $this->response );
        if( ($this->response->metadata->code == 200 OR $this->response->metadata->code == 1) && empty(!$this->response->metadata))
        {
            if(isset($this->response->response))
            {
                 //decrypt hasil response
                $decrypt    = $this->vclaimDecrypt($this->Xconsid.$this->Xconssecret.$this->Xtimestamp,$this->response->response);
                //decompress hasil response
                $decompress = $this->vclaimDecompress($decrypt);            
                $result['response'] = json_decode($decompress);
            }
            
        }
        return $result;
    }

    function requestBpjsAntrolDashboard($url,$uploadData,$method,$contenType='')
    {
        $this->jenisaplikasi= JENISAPLIKASI_BPJS_ANTROL;
        $this->jenisconsid  = JENISCONSID_PRODUCTION; //JENISCONSID_DEVELOPMENT;        
        $this->ContentType  = ((empty($contenType)) ? "application/json; charset=utf-8" : $contenType );
        $this->response     = $this->createSignature($url,$uploadData,$method);
        
        $result['url']      = $url;
        //jika gagal --> permintaan telah diterima untuk diproses, tetapi pemrosesan belum selesai
        if(empty($this->response))
        {
            $result['metaData']['code'] = '202';
            $result['metaData']['message'] = 'Bridging Sedang Bermasalah, Silakan Coba Lagi.!';
            return $result;
        }
        
        //jika berhasil -->permintaan telah diterima untuk diproses, dan pemrosesan selesai
        $result['metaData'] = ((isset($this->response->metadata)) ? $this->response->metadata : $this->response );
        if( ($this->response->metadata->code == 200 OR $this->response->metadata->code == 1) && empty(!$this->response->metadata))
        {
            if(isset($this->response->response))
            {            
                $result['response'] = $this->response->response;
            }
        }
        return $result;
    }

    function get_vclaim_sep($nosep)
    {
        $url        = '/SEP/'.$nosep;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }

    function get_vclaim_rujukan($norujukan)
    {
        $url        = '/Rujukan/'.$norujukan;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }

    function get_vclaim_rujukan_by_RS($norujukan)
    {
        $url        = '/Rujukan/RS/'.$norujukan;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }

    function get_vclaim_rujukan_bykartu($nokartu)
    {
        $url        = '/Rujukan/Peserta/'.$nokartu;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }

    function get_vclaim_rujukan_bykartu_multi($nokartu)
    {
        $url        = '/Rujukan/List/Peserta/'.$nokartu;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }

    function get_vclaim_rencanakontrol($nosurat)
    {
        $url        = '/RencanaKontrol/noSuratKontrol/'.$nosurat;
        $uploadData = '';
        $method     = 'GET';
        $request = $this->requestVclaim($url,$uploadData,$method,$contenType='');

        return  $request;
    }
     
}