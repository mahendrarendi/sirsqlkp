var table;
$(document).ready(function () {
//includejs('/assets/pages/riwayat_periksa.js');// include file js riwayat pasien

  switch($('#jsmode').val()) {
//    case 'datainduk':
//      js_datainduk();
//      break;
    case 'propinsi':
      js_propinsi();
      break;
    case 'icd':
      js_icd();
      break;
    case 'datalog':
      js_datalog();
      break;
    case 'masterbarang':
      js_masterbarang();
      break;
    case 'aksesuser':
      js_aksesuser();
      break;
    case 'masteruser':
      js_user();
      break;
    case 'mastertarif':
      js_mastertarif();
      break;
    case 'viewpobat':
      laporanPemakaianObat();
      break;
    case 'viewdtpasien':
      js_viewdtpasien();
      break;
    case 'js_rl51':
      js_rl51();
      break;
    case 'js_rl52':
      js_rl52();
      break;
    case 'js_rl52b':
      js_rl52b();
      break;
    case 'js_rl314':
      js_rl314();
      break;
    case 'js_rl53':
      js_rl53();
      break;
    case 'js_rl54':
      js_rl54();
      break;
    case 'js_rl4b':
      js_rl4b();
      break;
    case 'js_rl4a':
      js_rl4a();
      break;
    case 'lap_pelayanan':
      init_datemonth();
      break;
    case 'js_pelaporanpneumonia':
      js_pelaporanpneumonia();
      break;
    case 'js_pelaporanbalitapneumonia':
      js_pelaporanbalitapneumonia();
      break;
    case 'js_report_stprsrajal':
      js_report_stprsrajal();
      break;
    case 'js_report_stprsranap':
      js_report_stprsranap();
      break;
    case 'laporan_siha':
      pelaporan_siha();
      break;
    case 'report_cakupanpoli':
      report_cakupanpoli();
      break;    
    case 'kunjunganpasienasalrujukan':
      $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
      tampil_kunjunganralanbyrujukan();
      break;
    case 'kunjunganpasienbylayanan':
      $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
      tampil_kunjunganralanbylayanan();
      break;
    case 'kunjunganranapbyrujukan':
      $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
      tampil_kunjunganranapbyrujukan();
      break;
    case 'kunjunganpasienbycaradaftar':
      $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
      tampil_kunjunganralanbycaradaftar();
      break;
    default:
      // code block
  }
});


/*Tampil Kunjungan Pasien By Cara Daftar*/
function tampil_kunjunganralanbycaradaftar() 
{
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"creport/tampil_kunjunganralanbycaradaftar",
        data:{
            tanggal1:$('#tanggal1').val(),
            tanggal2:$('#tanggal2').val()
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            var rows='';
            if(result.length > 0)
            {
                var totalkeseluruhan = 0;
                var totalonline=0;
                var totalreguler=0;
                var totalanjungan=0;
                var totalmobilejkn=0;
                for(var i in result)
                {
                    totalkeseluruhan += parseInt(result[i].totalpendaftar);
                    totalonline   += parseInt(result[i].daftar_online);
                    totalreguler  += parseInt(result[i].daftar_reguler);
                    totalanjungan += parseInt(result[i].daftar_anjungan);
                    totalmobilejkn += parseInt(result[i].daftar_mobilejkn);
                    rows += '<tr><td>'+ result[i].tanggalperiksa +'</td><td>'+ convertToRupiah(result[i].daftar_online) +' </td><td>'+ convertToRupiah(result[i].daftar_reguler) +' </td> <td>'+ convertToRupiah(result[i].daftar_anjungan) +'</td> <td>'+ convertToRupiah(result[i].daftar_mobilejkn) +'</td> <td>'+ convertToRupiah(result[i].totalpendaftar) +'</td> </tr>';
                }
                rows += '<tr><td>Total</td><td>'+convertToRupiah(totalonline)+'</td><td>'+convertToRupiah(totalreguler)+'</td><td>'+convertToRupiah(totalanjungan)+'</td><td>'+convertToRupiah(totalmobilejkn)+'</td><td>'+convertToRupiah(totalkeseluruhan)+'</td></tr>';
            }
            else
            {
                rows += '<tr><td colspan="4">Data Tidak Ada</td></tr>';
            }
            $('#tampilkunjungan').html(rows);
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
          return false;
        }
      });
}

/*Tampil Kunjungan Pasien Ralan By Status Layanan*/
function tampil_kunjunganralanbylayanan() 
{
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"creport/tampil_kunjunganralanbylayanan",
        data:{tanggal1:$('#tanggal1').val(),tanggal2:$('#tanggal2').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            var rows='';
            var no=0;
            for(var i in result)
            {
                rows += '<tr><td>'+ ++no +'</td><td>'+ convertToRupiah(result[i].kunjungan) +'</td><td>'+ result[i].status +'</td></tr>';
            }
            var tabel = '<table id="tabel" class="table table-bordered table-hover table-striped dt-responsive" cellspacing="0" width="100%" style="overflow-x:auto;overflow-y:auto;">'
                  +'<thead>'
                  +'<tr class="bgql-gray"><th>No</th>'
                      +'<th>KUNJUNGAN</th>'
                      +'<th>STAUS LAYANAN</th>'
                  +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    + rows
                  +'</tfoot>'
                +'</table>';
            $('#listtable').html(tabel);
            $('#tabel').dataTable();
//          notif(result.status, result.message);
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
          return false;
        }
      });
}


/*Tampil Kunjungan Pasien Ranap By Rujukan*/
function tampil_kunjunganranapbyrujukan() 
{
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"creport/tampil_kunjunganranapbyrujukan",
        data:{
            tanggal1:$('#tanggal1').val(),
            tanggal2:$('#tanggal2').val()
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            var rows='';
            var no=0;
            for(var i in result.rekapranap)
            {
                var rekap = result.rekapranap[i];
                rows += '<tr>\n\
                    <td>'+ ++no +'</td>\n\
                    <td>'+ rekap.kodeklinik +'</td>\n\
                    <td>'+ rekap.namaklinik +'</td>\n\
                    <td>'+ rekap.jenisklinik +'</td>\n\
                    <td>'+ convertToRupiah(rekap.kunjungan) +'</td>\n\
                    <td>'+ rekap.jenisrujukan +'</td>\n\
                    </tr>';
            }
            var tabel = '<table id="tabel" class="table table-bordered table-hover table-striped dt-responsive" cellspacing="0" width="100%" style="overflow-x:auto;overflow-y:auto;">'
                  +'<thead>'
                  +'<tr class="bgql-gray"><th>No</th>'
                      +'<th>KODE FASKES</th>'
                      +'<th>NAMA FASKES</th>'
                      +'<th>JENIS FASKES</th>'
                      +'<th>KUNJUNGAN</th>'
                      +'<th>JENIS RUJUKAN</th>'
                  +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    + rows
                  +'</tfoot>'
                +'</table>';
            $('#listtable').html(tabel);
            $('#tabel').dataTable();
            
            
            var rowspasien='';
            for(var x in result.pasienranap)
            {
                var pasien = result.pasienranap[x];
                rowspasien += '<tr>\n\
                    <td>'+ ++no +'</td>\n\
                    <td>'+ pasien.norm +'</td>\n\
                    <td>'+ pasien.namapasien +'</td>\n\
                    <td>'+ pasien.notelpon +'</td>\n\
                    <td>'+ pasien.alamat +'</td>\n\
                    <td>'+ pasien.waktumasuk +'</td>\n\
                    <td>'+ pasien.statuskeluar +'</td>\n\
                    <td>'+ pasien.kodeklinik +'</td>\n\
                    <td>'+ pasien.namaklinik +'</td>\n\
                    <td>'+ pasien.jenisrujukan +'</td>\n\
                    </tr>';
            }
            var pasienranap = '<table id="tabelpasienranap" class="table table-bordered table-hover table-striped dt-responsive" cellspacing="0" width="100%" style="overflow-x:auto;overflow-y:auto;">'
                  +'<thead>'
                  +'<tr class="bgql-gray"><th>No</th>'
                      +'<th>NORM</th>'
                      +'<th>NAMA PASIEN</th>'
                      +'<th>NOTELPON</th>'
                      +'<th>ALAMAT</th>'
                      +'<th>WAKTU MASUK</th>'
                      +'<th>STATUS</th>'
                      +'<th>KODE KLINIK</th>'
                      +'<th>NAMA KLINIK</th>'
                      +'<th>JENIS RUJUKAN</th>'
                  +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    + rowspasien
                  +'</tfoot>'
                +'</table>';
            $('#listpasien').html(pasienranap);
            $('#tabelpasienranap').dataTable();
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
          return false;
        }
      });
}

/*Tampil Kunjungan Pasien Ralan By Faskes Rujukan*/
function tampil_kunjunganralanbyrujukan() 
{
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"creport/tampil_kunjunganralanbyrujukan",
        data:{tanggal1:$('#tanggal1').val(),tanggal2:$('#tanggal2').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            var rows='';
            var no=0;
            for(var i in result)
            {
                rows += '<tr><td>'+ ++no +'</td><td>'+ result[i].kodeklinik +'</td><td>'+ result[i].namaklinik +'</td><td>'+ result[i].jenisklinik +'</td><td>'+ convertToRupiah(result[i].kunjungan) +'</td><td>'+ result[i].jenisrujukan +'</td></tr>';
            }
            var tabel = '<table id="tabel" class="table table-bordered table-hover table-striped dt-responsive" cellspacing="0" width="100%" style="overflow-x:auto;overflow-y:auto;">'
                  +'<thead>'
                  +'<tr class="bgql-gray"><th>No</th>'
                      +'<th>KODE FASKES</th>'
                      +'<th>NAMA FASKES</th>'
                      +'<th>JENIS FASKES</th>'
                      +'<th>KUNJUNGAN</th>'
                      +'<th>JENIS RUJUKAN</th>'
                  +'</tr>'
                  +'</thead>'
                  +'<tbody>'
                    + rows
                  +'</tfoot>'
                +'</table>';
            $('#listtable').html(tabel);
            $('#tabel').dataTable();
//          notif(result.status, result.message);
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
          return false;
        }
      });
}
/*Unduh Kunjungan Pasien Ralan By Faskes Rujukan*/
function unduh_kunjunganralanbyrujukan()
{
    $.confirm({ //aksi ketika di klik menu
        title: 'Unduh Data Kunjungan',
        content: '',
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: '<i class="fa fa-file-excel-o"> unduh</i>',
                btnClass: 'btn-success',
                action: function () {
                    window.location.href=base_url+"creport/unduhexcel/kunjunganpasienralanbyrujukan/"+ $('#tanggal1').val() +"/"+ $('#tanggal2').val();
                }
            },
            //menu back
            formReset:{
                text: '<i class="fa fa-minus-circle"> batal</i>',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//
//function js_datainduk()
//{
//  table = $('#datainduk').DataTable({
//      "dom": '<"toolbar">frtip',
//      "stateSave": true,
//      "processing": true, 
//      "serverSide": true, 
//      "paging":false,
//      "false":false,
//      "info":false,
//      "order": [], 
//       
//      "ajax": {
//          "url": base_url +"cadmission/dt_datainduk",
//          "type": "POST"
//      },
//      "columnDefs": [
//      { 
//          "targets": [ 5,6,7 ], 
//          "orderable": false, 
//      },
//      ],
//      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
//          $('[data-toggle="tooltip"]').tooltip();
//      },
//
//  });
//  
//  $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"});
//}
function js_propinsi()
{
  table = $('#dtpropinsi').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_dtpropinsi",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ 0 ], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
      },
  });
  var menu='<a href="'+base_url+'cmasterdata/add_propinsi" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a> ';
      menu += ' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
      menu += ' <a href="'+base_url+'cmasterdata/kabupaten" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Kabupaten</a>';
      menu += ' <a href="'+base_url+'cmasterdata/kecamatan" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Kecamatan</a>';
      menu += ' <a href="'+base_url+'cmasterdata/kelurahan" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Kelurahan</a>';
  $("div.toolbar").html(menu);
}

function js_icd()
{
  table = $('#example').DataTable({
      "dom": '<"toolbar">frtip', //-- 
      "processing": true, 
      "serverSide": true,
      "stateSave": true, 
      "order": [], 
       
      "ajax": {
          "url": base_url+"cmasterdata/dt_icd",
          "type": "POST"
      },
      language : {
        sLoadingRecords : '<span style="width:100%;"><img src="'+base_url+'assets/ajax-load.gif"></span>'
      },
      "columnDefs": [
      { 
          "targets": [ 3, -1 ], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu='<a href="'+base_url+'cmasterdata/add_icd" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Entri ICD</a>';
      menu += ' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
      $("div.toolbar").html(menu);
}
function js_datalog()
{
  table = $('.table').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_reportlog",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [0], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu =' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>';
  $("div.toolbar").html(menu);
}
function js_masterbarang()
{
  table = $('#listdatatable').DataTable({
      "dom": '<"toolbar">frtip',
      "processing": true, 
      "serverSide": true,
      "stateSave": true, 
      "order": [], 
      "ajax": {
          "url": "load_dt_masterbarang", //-- url
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ 0,2,3,5,6,7,8,9], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },
  });
  $("div.toolbar").html('<a href="'+base_url+'cmasterdata/add_barang" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> '+
          '<a href="'+base_url+'cmasterdata/sediaan" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Sediaan</a> '+
          '<a href="'+base_url+'cmasterdata/satuan" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Satuan</a> '+
          '<a href="'+base_url+'cmasterdata/golongan" class="btn btn-success btn-sm"><i class="fa  fa-file-o"></i> Golongan</a> ');
}
function js_aksesuser()
{
  $('[data-toggle="tooltip"]').tooltip();
  table = $('#login_hakaksesuser').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_dtaksesuser",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [0,3], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu = '<a href="'+base_url+'cadministrasi/add_akses_user" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>'; 
      menu +=' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>';
  $("div.toolbar").html(menu);
}
function js_user()
{
  table = $('#login_user').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_datauser",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [-1, -2], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
}

function js_mastertarif()
{
  table = $('#example').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "dt_mastertarif",
          "type": "POST",
          // "data":{'total':''},
      },
      "columnDefs": [
      { 
          "targets": [12], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu ='<a href="'+base_url+'cmasterdata/add_mastertarif" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>';
      menu +=' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
  $("div.toolbar").html(menu);
}
function laporanPemakaianObat()
{
  var min = (($('#min').val())?ambiltanggal($('#min').val()) : '');
  var max = (($('#max').val())?ambiltanggal($('#max').val()) : '');
  var setmin = (($('#min').val())?$('#min').val() : '');
  var setmax = (($('#max').val())?$('#max').val() : '');
  table = $('#viewpobat').DataTable({
    "dom": '<"toolbar">frtip',
    "stateSave": true,
    "processing": true,
    "serverSide": true, 
    "order": [], 
    "ajax": {
        "url": "load_reportdataobat",
        "type": "POST",
        "data":{'awal':min, 'akhir':max},
    }, "columnDefs": [{"targets": [5,6,7],"orderable": false,},
    ],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
      $(nRow).attr('id', 'row' + iDataIndex);
    },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
    },
    "destroy" : true
  });
  menuLaporanPObat(setmin,setmax);
}

function menuLaporanPObat(min='',max='')
{
  var menu = '<input type="text" size="6" class="form-control datepicker" id="min" /> <input size="6" type="text" class="form-control datepicker" id="max" />';
  menu +=' <a class="btn btn-info btn-sm" onclick="getreport_laporanPemakaianObat()"><i class="fa fa-desktop"></i> Tampil</a>';
  menu +=' <a onclick="downloadexcel_page()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Excel</a>';
  menu +=' <a onclick="refresh_laporanPemakaianObat()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
      
  $("div.toolbar").html(menu);
  $('#min').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((min=='')? 'now' : min ) );
  $('#max').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((max=='')? 'now' : max ) );
}

// display data pemekaian obat sesuai tanggal yang dipilih
function getreport_laporanPemakaianObat()
{
  return laporanPemakaianObat();
}
function refresh_laporanPemakaianObat()
{
  menuLaporanPObat();
  $('#min').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now' );
  $('#max').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now' );
  table.state.clear();//menghapus stateSave di datatable 
  laporanPemakaianObat();
}
// download data pemaiakain obat sesuai dengan tanggal
function downloadexcel_page()
{
  var tgl1 = $('#min').val();
  var tgl2 = $('#max').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' laporandataobat';
}
function js_viewdtpasien()
{
  table = $('.table').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_reportdatapasien",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [0], 
          "orderable": false, 
      },
      ],

  });
  var  menu = '<select id="tampilpoli" name="unit" onchange="testing(this.value)" class="form-control" style="margin-right:4px;"></select>';
  menu += '<input type="text" size="6" class="form-control datepicker" id="min" /> <input size="6" type="text" class="form-control datepicker" id="max" />';
  menu +=' <a class="btn btn-info btn-sm" onclick="testing()"><i class="fa fa-desktop"></i> Tampil</a>';
  menu +=' <a onclick="testing()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Excel</a>';
  menu +=' <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>';
  $("div.toolbar").html(menu);
  $('#min').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", (($('#min').val()=='')? 'now' : $('#min').val() ) );
  $('#max').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", (($('#max').val()=='')? 'now' : $('#max').val() ) );
  get_datapoli('#tampilpoli');
}
function testing()
{
  $.alert("masih dalam pengembangan.!");
}
function refresh_page()
{
  $('input[type="search"]').val('').keyup();
  table.state.clear();//menghapus stateSave di datatable 
  table.ajax.reload();//reload ajax di datatable
  
}
function initializeDate()
{
  $('.datepicker').datepicker({
  autoclose: true,
  // minViewMode:2,
  format: "dd/mm/yyyy",
  orientation: "bottom",
  }).datepicker("setDate",'now'); //Initialize Date picker
}
function js_rl51()
{
  initializeDate();
  loaddt_rl51();
}
function tampil_rl51()
{
  var dttable = '<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                +'<thead>'
                  +'<tr>'
                    +'<th>Kode RS</th>'
                    +'<th>Nama RS</th>'
                    +'<th>Bulan</th>'
                    +'<th>KAB/KOTA</th>'
                    +'<th>Tahun</th>'
                    +'<th>KODE PROPINSI</th>'
                    +'<th>No. Urut</th>'
                    +'<th>Jenis Kegiatan</th>'
                    +'<th>Laki-laki</th>'
                    +'<th>Wanita</th>'
                    +'<th>Jumlah</th>'
                  +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot>'
              +'</table>';
      $('#listtable').empty();
      $('#listtable').html(dttable);
      loaddt_rl51();
}
// load data laporan RL51
function loaddt_rl51()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl51_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='', no=0;
      for(x in result)
      {
        dt+='<tr><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+qlbulan(result[x].bulan)+'</td><td>'+result[x].tahun+'</td><td>kab sleman</td><td>34Prop</td><td>'+ ++no +'</td><td>Pengunjung Baru</td><td>'+result[x].pbpria+'</td><td>'+result[x].pbwanita+'</td><td>'+result[x].tpb+'</td></tr>';
        dt+='<tr><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+qlbulan(result[x].bulan)+'</td><td>'+result[x].tahun+'</td><td>kab sleman</td><td>34Prop</td><td>'+ ++no +'</td><td>Pengunjung Lama</td><td>'+result[x].plpria+'</td><td>'+result[x].plwanita+'</td><td>'+result[x].tpl+'</td></tr>';
      }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      $('.table').DataTable({
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
        // "scrollY": 200,
        "scrollX": true
      });
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
// RL 52
function js_rl52()
{
  initializeDate();
  loaddt_rl52();
}
function tampil_rl52()
{
  var t52='<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
      +'<thead>'
      +'<tr>'
        +'<th>Kode RS</th>'
        +'<th>Nama RS</th>'
        +'<th>Bulan</th>'
        +'<th>KAB/KOTA</th>'
        +'<th>Tahun</th>'
        +'<th>KODE PROPINSI</th>'
        +'<th>No. Urut</th>'
        +'<th>Jenis Kegiatan</th>'
        +'<th>Laki-laki</th>'
        +'<th>Wanita</th>'
        +'<th>Jumlah</th>'
      +'</tr>'
      +'</thead>'
      +'<tbody id="tampildata">'
      +'</tfoot>'
    +'</table>';
    $('#listtable').empty();
    $('#listtable').html(t52);
    loaddt_rl52();
}
// load data laporan RL51
function loaddt_rl52()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl52_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='', no=0;
      for(x in result)
      {
        dt+='<tr><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+qlbulan(result[x].bulan)+'</td><td>'+result[x].tahun+'</td><td>kab sleman</td><td>34Prop</td><td>'+ ++no +'</td><td>Pengunjung Baru</td><td>'+result[x].pbpria+'</td><td>'+result[x].pbwanita+'</td><td>'+result[x].tpb+'</td></tr>';
        dt+='<tr><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+qlbulan(result[x].bulan)+'</td><td>'+result[x].tahun+'</td><td>kab sleman</td><td>34Prop</td><td>'+ ++no +'</td><td>Pengunjung Lama</td><td>'+result[x].plpria+'</td><td>'+result[x].plwanita+'</td><td>'+result[x].tpl+'</td></tr>';
      }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      // $('.table').DataTable({
      //   'lengthChange': false,
      //   'searching'   : false,
      //   'ordering'    : false,
      //   'info'        : true,
      //   'autoWidth'   : false,
      //   // "scrollY": 200,
      //   "scrollX": true
      // });
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

// RL 3.14 Rujukan
function js_rl314()
{
  initializeDate();
  loaddt_rl314();
  
}

// load data laporan RL314 Rujukan
function loaddt_rl314()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl314_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      // var dt='';

        // var no=0;
        // for(i in result)
        // {
          // dt+='<tr><td>34Prop</td><td>kab sleman</td><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+result[i].bulan+'</td><td>'+result[i].tahun+'</td><td>'+ ++no +'</td><td>'+result[i].icd+'</td><td>'+result[i].namaicd+'</td><td>'+result[i].lakilakihidup+'</td><td>'+result[i].wanitahidup+'</td><td>'+result[i].lakilakimeninggal+'</td><td>'+result[i].wanitameninggal+'</td><td>'+result[i].total+'</td></tr>';
        // }
      
      // $('#tampildata').empty();
      // $('#tampildata').html(dt);
      // $('.table').DataTable({
      //   'lengthChange': false,
      //   'searching'   : false,
      //   'ordering'    : false,
      //   'info'        : true,
      //   'autoWidth'   : false,
      // });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

function tampilrl314()
{
  var data = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                +'<thead>'
                +'<tr>'
                  +'<th rowspan="2">KODE PROPINSI</th>'
                  +'<th rowspan="2">KAB/KOTA</th>'
                  +'<th rowspan="2">Kode RS</th>'
                  +'<th rowspan="2">Nama RS</th>'
                  +'<th rowspan="2">Bulan</th>'
                  +'<th rowspan="2">Tahun</th>'
                  +'<th rowspan="2">No. Urut</th>'
                  +'<th rowspan="2">KODE ICD 10</th>'
                  +'<th rowspan="2">Descripsi</th>'
                  +'<th colspan="2" width="20%">Pasien Keluar hidup Menurut jenis Kelamin</th>'
                  +'<th colspan="2" width="20%">Pasien Keluar Mati Menurut Jenis Kelamin</th>'
                  +'<th rowspan="2">TOTAL ALL</th>'
                +'</tr>'
                +'<tr>'
                  +'<th>Laki-laki</th>'
                  +'<th>Perempuan</th>'
                  +'<th>Laki-laki</th>'
                  +'<th>Perempuan</th>'
                +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot>'
             +'</table>';
             $('#listtable').empty();
             $('#listtable').html(data);
             loaddt_rl314();
}

// RL 52b
function js_rl52b()
{
  initializeDate();
  loaddt_rl52b();
}
function tampil_rl52b()
{
  var t52='<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
      +'<thead>'
      +'<tr class="bg bg-yellow-gradient">'
        +'<th>Nama Poli</th>'
        +'<th>Jumlah Kasus</th>'
        +'<th>Laki-laki</th>'
        +'<th>Perempuan</th>'
      +'</tr>'
      +'</thead>'
      +'<tbody id="tampildata">'
      +'</tfoot>'
    +'</table>';
    $('#listtable').empty();
    $('#listtable').html(t52);
    loaddt_rl52b();
}
// load data laporan RL51
function loaddt_rl52b()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl52b_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='', no=0;
      for(x in result)
      {
        dt+='<tr><td>'+result[x].namaunit+'</td><td>'+convertToRupiah(result[x].jumlahkasus)+'</td><td>'+convertToRupiah(result[x].pria)+'</td><td>'+convertToRupiah(result[x].wanita)+'</td></tr>';
      }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      // $('.table').DataTable({
      //   'lengthChange': false,
      //   'searching'   : false,
      //   'ordering'    : false,
      //   'info'        : true,
      //   'autoWidth'   : false,
      //   // "scrollY": 200,
      //   "scrollX": true
      // });
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
// RL 53
function js_rl53()
{
  initializeDate();
  loaddt_rl53();
}
function tampilrl53()
{
  var data = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                +'<thead>'
                +'<tr>'
                  +'<th rowspan="2">KODE PROPINSI</th>'
                  +'<th rowspan="2">KAB/KOTA</th>'
                  +'<th rowspan="2">Kode RS</th>'
                  +'<th rowspan="2">Nama RS</th>'
                  +'<th rowspan="2">Bulan</th>'
                  +'<th rowspan="2">Tahun</th>'
                  +'<th rowspan="2">No. Urut</th>'
                  +'<th rowspan="2">KODE ICD 10</th>'
                  +'<th rowspan="2">Descripsi</th>'
                  +'<th colspan="2" width="20%">Pasien Keluar hidup Menurut jenis Kelamin</th>'
                  +'<th colspan="2" width="20%">Pasien Keluar Mati Menurut Jenis Kelamin</th>'
                  +'<th rowspan="2">TOTAL ALL</th>'
                +'</tr>'
                +'<tr>'
                  +'<th>Laki-laki</th>'
                  +'<th>Perempuan</th>'
                  +'<th>Laki-laki</th>'
                  +'<th>Perempuan</th>'
                +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot>'
             +'</table>';
             $('#listtable').empty();
             $('#listtable').html(data);
             loaddt_rl53();
}
// load data laporan RL51
function loaddt_rl53()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl53_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='';
      // for(i in result)
      // {
        var no=0;
        for(i in result)
        {
          dt+='<tr><td>34Prop</td><td>kab sleman</td><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+result[i].bulan+'</td><td>'+result[i].tahun+'</td><td>'+ ++no +'</td><td>'+result[i].icd+'</td><td>'+result[i].namaicd+'</td><td>'+result[i].lakilakihidup+'</td><td>'+result[i].wanitahidup+'</td><td>'+result[i].lakilakimeninggal+'</td><td>'+result[i].wanitameninggal+'</td><td>'+result[i].total+'</td></tr>';
        }
      // }
      
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      $('.table').DataTable({
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
      });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
function js_rl54()
{
  initializeDate();
  loaddt_rl54();
}
function tampilrl54()
{
  var x = '<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                +'<thead>'
                +'<tr>'
                  +'<th rowspan="2">KODE PROPINSI</th>'
                  +'<th rowspan="2">KAB/KOTA</th>'
                  +'<th rowspan="2">Kode RS</th>'
                  +'<th rowspan="2">Nama RS</th>'
                  +'<th rowspan="2">Bulan</th>'
                  +'<th rowspan="2">Tahun</th>'
                  +'<th rowspan="2">No. Urut</th>'
                  +'<th rowspan="2">KODE ICD 10</th>'
                  +'<th rowspan="2">Descripsi</th>'
                  +'<th colspan="2">Kasus Baru menurut Jenis Kelamin</th>'
                  +'<th rowspan="2">TOTAL ALL</th>'
                +'</tr>'
                +'<tr>'
                  +'<th>Laki-laki</th>'
                  +'<th>Perempuan</th>'
                +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot>'
              +'</table>';
              $('#listtable').empty();
              $('#listtable').html(x);
              loaddt_rl54();
}
// load data laporan RL54
function loaddt_rl54()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl54_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='';
      var dt='';
      // for(i in result)
      // {
        var no=0;
        for(i in result)
        {
          dt+='<tr><td>34Prop</td><td>kab sleman</td><td>3404045</td><td>RSU QUEEN LATIFA</td><td>'+result[i].bulan+'</td><td>'+result[i].tahun+'</td><td>'+ ++no +'</td><td>'+result[i].icd+'</td><td>'+result[i].namaicd+'</td><td>'+result[i].lakilakibaru+'</td><td>'+result[i].wanitabaru+'</td><td>'+result[i].total+'</td></tr>';
        }
      // }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      $('.table').DataTable({
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
      });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
function js_rl4a()
{
  initializeDate();
  loaddt_rl4a();
}
function tampilrl4a()
{
  var dt='<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                +'<thead>'
                +'<tr>'
                  +'<th rowspan="3">No. Urut</th>'
                  +'<th rowspan="3">No. DTD</th> '
                  +'<th rowspan="3">No. Daftar Terperinci</th> '
                  +'<th rowspan="3">Golongan Sebab Penyakit</th> '
                  +'<th colspan="18">Jumlah Pasien (Hidup &amp; Mati) Menurut Golongan Umur &amp; Jenis Kelamin</th> '
                  +'<th colspan="3">Pasien Keluar (Hidup &amp; Mati) Menurut Jenis Kelamin</th>'
                  +'<th rowspan="3">Jumlah Pasien Keluar Hidup</th>'
                  +'<th rowspan="3">Jumlah Pasien Keluar Mati</th>'
                +'</tr>'
                +'<tr>'
                  +'<th colspan="2">0-6 hr</th>'
                  +'<th colspan="2">7-28 hr</th> '
                  +'<th colspan="2">29hr-&lt; 1 th</th>'
                  +'<th colspan="2">1-4 th</th>'
                  +'<th colspan="2">5-14 th</th>'
                  +'<th colspan="2">15-24 th</th>'
                  +'<th colspan="2">25-44 th</th>'
                  +'<th colspan="2">45-64 th</th>'
                  +'<th colspan="2">&gt; 65</th>'
                  +'<th rowspan="2">Laki</th>'
                  +'<th rowspan="2">Perempuan</th>'
                +'</tr>'
                +'<tr>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot></table>';
                $('#listtable').empty();
                $('#listtable').html(dt);
                loaddt_rl4a();
}
function loaddt_rl4a()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl4a_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      var dt='', no=0, total=0;
      if(result=='')
      {
        dt+='<tr class="text-center"><td colspan="27">No data to display</td></tr>';
      }
      stopLoading();
      for(x in result)
      {
        total += parseInt(result[x].kbwanita) + parseInt(result[x].kbpria);
          dt+='<tr><td>'+ ++no +'</td><td>'+ no +'</td><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ result[x].apria +'</td><td>'+ result[x].awanita +'</td><td>'+ result[x].bpria +'</td><td>'+ result[x].bwanita +'</td><td>'+ result[x].cpria +'</td><td>'+ result[x].cwanita +'</td><td>'+ result[x].dpria +'</td><td>'+ result[x].dwanita +'</td><td>'+ result[x].epria +'</td><td>'+ result[x].ewanita +'</td><td>'+ result[x].fpria +'</td><td>'+ result[x].fwanita +'</td><td>'+ result[x].gpria +'</td><td>'+ result[x].gwanita +'</td><td>'+ result[x].hpria +'</td><td>'+ result[x].hwanita +'</td><td>'+ result[x].ipria +'</td><td>'+ result[x].iwanita +'</td><td>'+ result[x].kbpria +'</td><td>'+ result[x].kbwanita +'</td><td>'+total +'</td><td>'+result[x].kunjungan+'</td></tr>';
        
      }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      $('.table').DataTable({
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
        // "scrollY": 200,
        "scrollX": true
      });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
function js_rl4b()
{
  initializeDate();
  loaddt_rl4b();
}
function tampilrl4b()
{
  var data='<table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">'
                +'<thead>'
                +'<tr style="background:#edd38c;">'
                  +'<th rowspan="3">No. Urut</th>'
                  +'<th rowspan="3">No. DTD</th> '
                  +'<th rowspan="3">No. Daftar Terperinci</th> '
                  +'<th rowspan="3">Golongan Sebab Penyakit</th> '
                  +'<th colspan="18">JML Pasien Kasus Baru Menurut Golongan Umur &amp; Sex</th> '
                  +'<th colspan="2">Kasus Baru Menurut Jenis Kelamin</th>'
                  +'<th rowspan="3">JML Kasus Baru</th>'
                  +'<th rowspan="3">JML Kunjungan</th>'
                +'</tr>'
                +'<tr style="background:#b2ab96;">'
                  +'<th colspan="2">0-6 hr</th>'
                  +'<th colspan="2">7-28 hr</th> '
                  +'<th colspan="2">29hr-&lt; 1 th</th>'
                  +'<th colspan="2">1-4 th</th>'
                  +'<th colspan="2">5-14 th</th>'
                  +'<th colspan="2">15-24 th</th>'
                  +'<th colspan="2">25-44 th</th>'
                  +'<th colspan="2">45-64 th</th>'
                  +'<th colspan="2">&gt; 65</th>'
                  +'<th rowspan="2">Laki</th>'
                  +'<th rowspan="2">Perempuan</th>'
                +'</tr>'
                +'<tr class="bg bg-gray">'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                  +'<th>L</th>'
                  +'<th>P</th>'
                +'</tr>'
                +'<tr style="background-color: #222d32;color:#fff;">'
                  +'<th>1</th>'
                  +'<th>2</th>'
                  +'<th>3</th>'
                  +'<th>4</th>'
                  +'<th>5</th>'
                  +'<th>6</th>'
                  +'<th>7</th>'
                  +'<th>8</th>'
                  +'<th>9</th>'
                  +'<th>10</th>'
                  +'<th>11</th>'
                  +'<th>12</th>'
                  +'<th>13</th>'
                  +'<th>14</th>'
                  +'<th>15</th>'
                  +'<th>16</th>'
                  +'<th>17</th>'
                  +'<th>18</th>'
                  +'<th>19</th>'
                  +'<th>20</th>'
                  +'<th>21</th>'
                  +'<th>22</th>'
                  +'<th>23</th>'
                  +'<th>24</th>'
                  +'<th>25</th>'
                  +'<th>26</th>'
                +'</tr>'
                +'</thead>'
                +'<tbody id="tampildata">'
                +'</tfoot>'
              +'</table>';
    $('#listtable').empty();
    $('#listtable').html(data);
    loaddt_rl4b();
}
function loaddt_rl4b()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/rl4b_jsondt",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='', no=0, total=0;
      if(result=='')
      {
        dt+='<tr class="text-center"><td colspan="26">No data to display</td></tr>';
      }
      var no=0;
      var apria = 0;
      var awanita = 0;
      var bpria = 0;
      var bwanita = 0;
      var cpria = 0;
      var cwanita = 0;
      var dpria = 0;
      var dwanita = 0;
      var epria = 0;
      var ewanita = 0;
      var fpria = 0;
      var fwanita = 0;
      var gpria = 0;
      var gwanita = 0;
      var hpria = 0;
      var hwanita = 0;
      var ipria = 0;
      var iwanita = 0;
      var kbpria = 0;
      var kbwanita = 0;
      var kunjunganbaru = 0;
      var kunjungan = 0;
      
      for(var x in result.jenis)
      {
          var data = result.jenis[x];
          for(var j in result.laporan)
          {
              var lap = result.laporan[j];
              if(lap.idgolongansebabpenyakit == data.idgolongansebabpenyakit)
              {
                apria += parseInt(lap.apria);
                awanita += parseInt(lap.awanita);
                bpria += parseInt(lap.bpria);
                bwanita += parseInt(lap.bwanita);
                cpria += parseInt(lap.cpria);
                cwanita += parseInt(lap.cwanita);
                dpria += parseInt(lap.dpria);
                dwanita += parseInt(lap.dwanita);
                epria += parseInt(lap.epria);
                ewanita += parseInt(lap.ewanita);
                fpria += parseInt(lap.fpria);
                fwanita += parseInt(lap.fwanita);
                gpria += parseInt(lap.gpria);
                gwanita += parseInt(lap.gwanita);
                hpria += parseInt(lap.hpria);
                hwanita += parseInt(lap.hwanita);
                ipria += parseInt(lap.ipria);
                iwanita += parseInt(lap.iwanita);
                kbpria += parseInt(lap.kbpria);
                kbwanita += parseInt(lap.kbwanita);
                kunjunganbaru += parseInt(lap.kunjunganbaru);
                kunjungan += parseInt(lap.kunjungan);
              }
                
          }
          dt+='<tr><td>'+ ++no +'</td><td>'+data.nodtd+'</td><td>'+data.daftar_terperinci+'</td><td>'+data.golongansebabpenyakit+'</td>\n\
            <td>'+ apria +'</td>\n\
            <td>'+ awanita +'</td>\n\
            <td>'+ bpria +'</td>\n\
            <td>'+ bwanita +'</td>\n\
            <td>'+ cpria +'</td>\n\
            <td>'+ cwanita +'</td>\n\
            <td>'+ dpria +'</td>\n\
            <td>'+ dwanita +'</td>\n\
            <td>'+ epria +'</td>\n\
            <td>'+ ewanita +'</td>\n\
            <td>'+ fpria +'</td>\n\
            <td>'+ fwanita +'</td>\n\
            <td>'+ gpria +'</td>\n\
            <td>'+ gwanita +'</td>\n\
            <td>'+ hpria +'</td>\n\
            <td>'+ hwanita +'</td>\n\
            <td>'+ ipria +'</td>\n\
            <td>'+ iwanita +'</td>\n\
            <td>'+ kbpria +'</td>\n\
            <td>'+ kbwanita +'</td><\n\
            <td>'+kunjunganbaru +'</td>\n\
            <td>'+kunjungan+'</td></tr>';
            
            apria = 0;
            awanita = 0;
            bpria = 0;
            bwanita = 0;
            cpria = 0;
            cwanita = 0;
            dpria = 0;
            dwanita = 0;
            epria = 0;
            ewanita = 0;
            fpria = 0;
            fwanita = 0;
            gpria = 0;
            gwanita = 0;
            hpria = 0;
            hwanita = 0;
            ipria = 0;
            iwanita = 0;
            kbpria = 0;
            kbwanita = 0;
            kunjunganbaru = 0;
            kunjungan = 0;
      }     
      $('#tampildata').empty();
      $('#tampildata').html(dt);
      $('.table').DataTable({
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false,
        // "scrollY": 200,
        "scrollX": true
      });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
function unduhrl4a()
{
  var tgl1 = ambiltanggal($('#tgl1').val());
  var tgl2 = ambiltanggal($('#tgl2').val());
  var value = tgl1 +'|'+tgl2;
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl4agetdata';
}
function unduhrl4b()
{
  var tgl1 = ambiltanggal($('#tgl1').val());
  var tgl2 = ambiltanggal($('#tgl2').val());
  var value = tgl1 +'|'+tgl2;
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl4bgetdata';
}
function unduhrl51()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl51getdata';
}
function unduhrl52()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl52getdata';
}
function unduhrl53()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl53getdata';
}
function unduhrl54()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' rl54getdata';
}
function unduhpelaporanpneumonia()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' pneumoniaorispa';
}
function unduhpelaporanbalitapneumonia()
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  window.location.href=base_url+"creport/downloadexcel_page/"+value+' balitapneumonia';
}
function init_datemonth()
{
  $('.datepicker').datepicker({
  autoclose: true,
  format: "yyyy-mm",
  viewMode: "months", 
  minViewMode: "months",
  orientation: "bottom",
  }).datepicker("setDate",'now'); //Initialize Date picker
  lap_pelayanan();
}
function unduhlpelayanan()
{
  window.location.href=base_url+"creport/tampillaporanperiksapasien/"+$('#bulan').val()+'/unduh';
}
// unduh stprs excel file
function unduh_report_stprs(jenis)
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  if(jenis=='jalan')
  {
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' stprs_rajal';
  }
  else
  {
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' stprs_ranap';
  }
}
// unduh stprs txt file
function unduhtxt_report_stprs(jenis)
{
  var tgl1 = $('#tgl1').val();
  var tgl2 = $('#tgl2').val();
  var value= ambiltanggal(tgl1)+'|'+ambiltanggal(tgl2);
  if(jenis=='jalan')
  {
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' stprs_rajaltxt';
  }
  else
  {
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' stprs_ranaptxt';
  }
}
function unduhreport_cakupanpoli()
{
  window.location.href=base_url+"creport/cakupanpoli_unduhexcel/"+$('#tgl1').val();
}
function tampil_lappelayanan()
{
  var data = '<table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%"><thead><tr><th>No</th><th>Norm</th> <th>NamaPasien</th><th>WaktuLayanan</th><th>DokterDPJP</th><th>Unit</th><th>Diagnosa</th><th>Tindakan</th></tr></thead><tbody id="tampildata"></tfoot></table>';
    $('#listtable').empty();
    $('#listtable').html(data);
    lap_pelayanan();
}
function lap_pelayanan()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/tampillaporanperiksapasien",
    data:{bulan:$('#bulan').val()},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var html='', no=0;var identitas=result.identitas;
      for (x in identitas)
      {
          var vitalsign='',icd10='',icd9='',laborat='', diagnosa='', tindakan='', terapi='';
          for(i in result.icd[x])
          {
             var icd = result.icd[x][i];
              vitalsign += ((icd.idjenisicd=='1')? ''+icd.icd+' '+icd.namaicd +' '+icd.nilai+'<br>':'');
              laborat += ((icd.idjenisicd=='4')? ''+icd.icd+' '+icd.namaicd +' '+icd.nilai+'<br>':'');
              diagnosa += ((icd.idjenisicd=='2')? ''+icd.icd+' '+icd.namaicd +' '+icd.nilai+'<br>':'');
              tindakan += ((icd.idjenisicd=='3')? ''+icd.icd+' '+icd.namaicd +' '+icd.nilai+'<br>':'');
          }
          for(a in result.terapi[x])
          {
              terapi+= result.terapi[x][a].kode+' '+result.terapi[x][a].namabarang+' '+result.terapi[x][a].jumlahpemakaian+' '+result.terapi[x][a].namasatuan+'<br>';
          }
          html+='<tr><td>'+ ++no +'</td><td>'+ identitas[x].dokter +'</td><td>'+ identitas[x].namaunit +'</td><td>'+ identitas[x].carabayar +'</td><td>'+ identitas[x].norm +'</td><td>'+ identitas[x].namalengkap +'</td><td>'+ identitas[x].tanggallahir +'</td><td>'+ identitas[x].usia +'</td><td>'+ identitas[x].alamat +'</td><td>'+ identitas[x].jeniskelamin +'</td><td>'+ identitas[x].ispasienlama +'</td><td>'+vitalsign+'</td><td>'+ identitas[x].anamnesa +'</td><td>'+laborat+'</td><td>'+identitas[x].keteranganradiologi+'<br>'+((identitas[x].saranradiologi=='')?'':'Saran:<br>') + identitas[x].saranradiologi+'</td><td></td><td>'+diagnosa+'</td><td>'+tindakan+'</td><td>'+terapi+'</td></tr>';
      }
      $('#tampildata').empty();
      $('#tampildata').html(html);
      $('#tabel').DataTable({
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'paging'      : false,
        'info'        : true,
        'autoWidth'   : false,
         "scrollY": 480,
        "scrollX": true
      });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
// pelaporan pneumonia
function js_pelaporanpneumonia()
{
  initializeDate();
  loaddt_pelaporanpneumonia();
}
// load data laporan pelaporanpneumonia
function loaddt_pelaporanpneumonia()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/pneumonia_pelaporanlist",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt='',rajal=result.rajal;
      dt+='<tr><td>1</td><td>RAWAT JALAN</td><td>'+rajal['pneumonia_k1L']+'</td><td>'+rajal['pneumonia_k1W']+'</td><td>'+rajal['pneumonia_k5L']+'</td><td>'+rajal['pneumonia_k5W']+'</td><td>'+rajal['pneumonia_k5T']+'</td><td>'+rajal['K1L']+'</td><td>'+rajal['K1W']+'</td><td>'+rajal['K5L']+'</td><td>'+rajal['K5W']+'</td><td>'+rajal['K5T']+'</td><td>'+rajal['meninggal_k1L']+'</td><td>'+rajal['meninggal_k1W']+'</td><td>'+rajal['meninggal_k1T']+'</td><td>'+rajal['meninggal_4L']+'</td><td>'+rajal['meninggal_4W']+'</td><td>'+rajal['meninggal_4T']+'</td><td>'+rajal['meninggal_4TL']+'</td><td>'+rajal['meninggal_4TW']+'</td><td>'+rajal['L5L']+'</td><td>'+rajal['L5W']+'</td><td>'+rajal['L5T']+'</td><td>'+rajal['pneumonia_5L']+'</td><td>'+rajal['pneumonia_5W']+'</td><td>'+rajal['pneumonia_5T']+'</td></tr>';
      dt+='<tr><td>2</td><td>RAWAT INAP</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
      dt+='<tr><td></td><td>Jumlah</td><td>'+rajal['pneumonia_k1L']+'</td><td>'+rajal['pneumonia_k1W']+'</td><td>'+rajal['pneumonia_k5L']+'</td><td>'+rajal['pneumonia_k5W']+'</td><td>'+rajal['pneumonia_k5T']+'</td><td>'+rajal['K1L']+'</td><td>'+rajal['K1W']+'</td><td>'+rajal['K5L']+'</td><td>'+rajal['K5W']+'</td><td>'+rajal['K5T']+'</td><td>'+rajal['meninggal_k1L']+'</td><td>'+rajal['meninggal_k1W']+'</td><td>'+rajal['meninggal_k1T']+'</td><td>'+rajal['meninggal_4L']+'</td><td>'+rajal['meninggal_4W']+'</td><td>'+rajal['meninggal_4T']+'</td><td>'+rajal['meninggal_4TL']+'</td><td>'+rajal['meninggal_4TW']+'</td><td>'+rajal['L5L']+'</td><td>'+rajal['L5W']+'</td><td>'+rajal['L5T']+'</td><td>'+rajal['pneumonia_5L']+'</td><td>'+rajal['pneumonia_5W']+'</td><td>'+rajal['pneumonia_5T']+'</td></tr>';
      dt+='<tr><td colspan="26"></td></tr>';
      dt+='<tr><th colspan="26">RINCIAN ASAL PASIEN</th></tr>';
      var no=0;
      for(x in result.rajalbykab)
      {
        var tampil=result.rajalbykab;
        no++;
        dt+='<tr><td>'+no+'</td><td>'+tampil[x].kabupatenkota+'</td><td>'+tampil[x].pneumonia_k1L+'</td><td>'+tampil[x].pneumonia_k1W+'</td><td>'+tampil[x].pneumonia_k5L+'</td><td>'+tampil[x].pneumonia_k5W+'</td><td>'+tampil[x].pneumonia_k5T+'</td><td>'+tampil[x].K1L+'</td><td>'+tampil[x].K1W+'</td><td>'+tampil[x].K5L+'</td><td>'+tampil[x].K5W+'</td><td>'+tampil[x].K5T+'</td><td>'+tampil[x].meninggal_k1L+'</td><td>'+tampil[x].meninggal_k1W+'</td><td>'+tampil[x].meninggal_k1T+'</td><td>'+tampil[x].meninggal_4L+'</td><td>'+tampil[x].meninggal_4W+'</td><td>'+tampil[x].meninggal_4T+'</td><td>'+tampil[x].meninggal_4TL+'</td><td>'+tampil[x].meninggal_4TW+'</td><td>'+tampil[x].L5L+'</td><td>'+tampil[x].L5W+'</td><td>'+tampil[x].L5T+'</td><td>'+tampil[x].pneumonia_5L+'</td><td>'+tampil[x].pneumonia_5W+'</td><td>'+tampil[x].pneumonia_5T+'</td></tr>';
      }

      $('#tampildata').empty();
      $('#tampildata').html(dt);

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

// pelaporan balitapneumonia
function js_pelaporanbalitapneumonia()
{
  initializeDate();
  loaddt_pelaporanbalitapneumonia();
}
// load data laporan balitapelaporanpneumonia
function loaddt_pelaporanbalitapneumonia()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/balitapneumonia_pelaporanlist",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      // console.log(result);
      stopLoading();
      
      var no=0, dt='';
      for(x in result)
      {
        no++;
        dt+='<tr><td>'+no+'</td><td>'+result[x].namalengkap+'</td><td>'+result[x].alamat+'</td><td>'+result[x].laki+'</td><td>'+result[x].wanita+'</td><td>'+result[x].masuk+'</td><td>'+result[x].keluar+'</td><td>'+result[x].icd+'</td><td>'+result[x].kondisikeluar+'</td></tr>';
      }

      $('#tampildata').empty();
      $('#tampildata').html(dt);
      // $('.table').DataTable({
      //   'lengthChange': false,
      //   'searching'   : false,
      //   'ordering'    : false,
      //   'info'        : false,
      //   'autoWidth'   : false,
      //   // "scrollY": 200,
      //   "scrollX": true
      // });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

// STP RS RAJAL
function js_report_stprsrajal()
{
  initializeDate();
  loaddt_report_stprs('jalan');
}
// STP RS Ranap
function js_report_stprsranap()
{
  initializeDate();
  loaddt_report_stprs('ranap');
}
$(document).on("click","#loaddt_report_stprs", function(){loaddt_report_stprs($(this).attr("jenis"));}); // ketika menu tampil stp rs di klik
$(document).on("click","#unduhreport_stprs", function(){unduh_report_stprs($(this).attr("jenis"));}); // ketika menu unduh stp rs di klik
$(document).on("click","#unduhtxtreport_stprs", function(){unduhtxt_report_stprs($(this).attr("jenis"));}); // UNDUH FILE TXT STPRS
// load data laporan report_stprs
function loaddt_report_stprs(jenis)
{
  var url=((jenis=='jalan')?'creport/report_stprslist_rajal':'creport/report_stprslist_ranap');
  startLoading();
  $.ajax({
    url:base_url+url,
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val()),jenispemeriksaan:jenis},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt = '';
      for(x in result)
      {
        var no = parseInt(x)+1;
        dt += '<tr><td>'+ no +'</td><td>'+ if_empty(result[x].icd) +'</td><td>'+ result[x].jenispenyakit +'</td>';
        dt += '<td>'+result[x].L+'</td><td>'+result[x].P+'</td><td>'+result[x].J+'</td><td>0</td><td>'+result[x].L1+'</td><td>'+result[x].P1+'</td><td>'+result[x].L2+'</td><td>'+result[x].P2+'</td><td>'+result[x].L3+'</td><td>'+result[x].P3+'</td><td>'+result[x].L4+'</td><td>'+result[x].P4+'</td><td>'+result[x].L5+'</td><td>'+result[x].P5+'</td><td>'+result[x].L6+'</td><td>'+result[x].P6+'</td><td>'+result[x].L7+'</td><td>'+result[x].P7+'</td><td>'+result[x].L8+'</td><td>'+result[x].P8+'</td><td>'+result[x].L9+'</td><td>'+result[x].P9+'</td><td>'+result[x].L10+'</td><td>'+result[x].P10+'</td><td>'+result[x].L11+'</td><td>'+result[x].P11+'</td><td>'+result[x].L12+'</td><td>'+result[x].P12+'</td>';
      }
      $('#tampildata').empty();
      $('#tampildata').html(dt);
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}
// PELAPORAN SIHA
function pelaporan_siha()
{
  initializeDate();
  siha_listdata();
}

function siha_listdata()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/siha_listdata",
    data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var no=0, dt='';
      for(x in result.dtperiksa)
      {
        dt+='<tr><td>'+ ++no +'</td><td>'+result.dtperiksa[x].namalengkap+'</td><td>'+result.dtperiksa[x].tanggallahir+'</td><td>'+result.dtperiksa[x].jeniskelamin+'</td><td><a target="_blank" class="btn" href="'+result.dtperiksa[x].riwayat+'">Link</a></td></tr>';
      }

      $('#tampildata').empty();
      $('#tampildata').html(dt);
      // $('.table').DataTable({
      //   'lengthChange': false,
      //   'searching'   : false,
      //   'ordering'    : false,
      //   'info'        : false,
      //   'autoWidth'   : false,
      //   // "scrollY": 200,
      //   "scrollX": true
      // });

    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

// PELAPORAN CAKUPAN POLI

function report_cakupanpoli()
{
  // initializeDate();
  $('.datepicker').datepicker({
   autoclose: true,
            // endDate: new Date(),
            format: "M yyyy",
            viewMode: "months", 
            minViewMode: "months",
            orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
  loadreport_cakupanpoli();
}

// load data laporan report_stprs
function loadreport_cakupanpoli()
{
  startLoading();
  $.ajax({
    url:base_url+"creport/cakupanpoli_listdata",
    data:{bulan:$('#tgl1').val(),jenisperiksa:$('input[name="jenisperiksa"]:checked').val()},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var dt = '', dtwaktu='', dtjenis='',dtjumlahbayar='',dtcarabayar='',dtpenyakit='',dttindakan='', pasienlama=0, pasienbaru=0, jumlahpasien=0;
      for(x in result.jumlahpasien)
      {
        dtcarabayar+='<td>'+result.jumlahpasien[x].carabayar+'</td>';
        dtjumlahbayar+='<td>'+result.jumlahpasien[x].jumlahbayar+'</td>';
        pasienlama += parseInt(result.jumlahpasien[x].pasienlama);
        pasienbaru += parseInt(result.jumlahpasien[x].pasienbaru);
        jumlahpasien += parseInt(result.jumlahpasien[x].jumlahpasien); 
      }
      dtwaktu = ((result.jumlahpasien.length==0)?'<td colspan="3">Data Tidak ada</td>':'<td></td><td>'+result.jumlahpasien[0].waktu+'</td><td>'+convertToRupiah(jumlahpasien)+'</td>');
      dtjenis = ((result.jumlahpasien.length==0)?'<td colspan="3">Data Tidak ada</td>':'<td></td><td>'+convertToRupiah(pasienlama)+'</td><td>'+convertToRupiah(pasienbaru)+'</td>');
      
      if(result.jumlahpasien.length==0){
        dtcarabayar='<td colspan="3">Data Tidak ada</td>';
        dtjumlahbayar='<td colspan="3">Data Tidak ada</td>';
      }
      var no =0;
      for(i in result.penyakit){dtpenyakit+='<tr><td>'+ ++no +'</td><td>'+result.penyakit[i].icd+' '+result.penyakit[i].namaicd+'</td><td>'+result.penyakit[i].total+'</td><td>'+angkadesimal(result.penyakit[i].percentase,2)+' &#37;</td></tr>';}
      var no =0;
      for(i in result.tindakan){dttindakan+='<tr><td>'+ ++no +'</td><td>'+result.tindakan[i].icd+' '+result.tindakan[i].namaicd+'</td><td>'+result.tindakan[i].total+'</td><td>'+angkadesimal(result.tindakan[i].percentase,2)+' &#37;</td></tr>';}
      $('#waktu').empty();
      $('#waktu').html(dtwaktu);
      $('#jenispasien').empty();
      $('#jenispasien').html(dtjenis);
      $('#carabayar').empty();
      $('#carabayar').html(dtcarabayar);
      $('#jumlahbayar').empty();
      $('#jumlahbayar').html(dtjumlahbayar);
      $('#penyakit').empty();
      $('#penyakit').html(dtpenyakit);
      $('#tindakan').empty();
      $('#tindakan').html(dttindakan);
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}

// pelaporan balitapneumonia
// function js_pelaporanbalitapneumonia()
// {
//   initializeDate();
//   loaddt_pelaporanbalitapneumonia();
// }
// load data laporan balitapelaporanpneumonia
// function loaddt_pelaporanbalitapneumonia()
// {
//   startLoading();
//   $.ajax({
//     url:base_url+"creport/balitapneumonia_pelaporanlist",
//     data:{tgl1:ambiltanggal($('#tgl1').val()),tgl2:ambiltanggal($('#tgl2').val())},
//     type:"POST",
//     dataType:"JSON",
//     success: function(result){
//       // console.log(result);
//       stopLoading();
      
//       var no=0, dt='';
//       for(x in result)
//       {
//         no++;
//         dt+='<tr><td>'+no+'</td><td>'+result[x].namalengkap+'</td><td>'+result[x].alamat+'</td><td>'+result[x].laki+'</td><td>'+result[x].wanita+'</td><td>'+result[x].masuk+'</td><td>'+result[x].keluar+'</td><td>'+result[x].icd+'</td><td>'+result[x].kondisikeluar+'</td></tr>';
//       }

//       $('#tampildata').empty();
//       $('#tampildata').html(dt);
//       // $('.table').DataTable({
//       //   'lengthChange': false,
//       //   'searching'   : false,
//       //   'ordering'    : false,
//       //   'info'        : false,
//       //   'autoWidth'   : false,
//       //   // "scrollY": 200,
//       //   "scrollX": true
//       // });

//     },
//     error:function()
//     {
//       stopLoading();
//       fungsiPesanGagal();
//       return false;
//     }
//   });
// }