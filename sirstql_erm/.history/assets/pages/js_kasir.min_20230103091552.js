var dtkasir = "",
    dtpenanggung = [];
function getpenanggung() {
    $.ajax({
        url: base_url + "cmasterdata/fillpenanggungkasir",
        type: "POST",
        dataType: "JSON",
        success: function (a) {
            dtpenanggung = a;
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function reloadPemeriksaanByUnit() {
    return localStorage.setItem("idunit", $("#idunit").val()), dtkasir.ajax.reload(null, !1);
}

function reload_pemerisaan_by_bangsal(){
    localStorage.setItem('idbangsal',$('#idbangsal').val());
    return dtkasir.ajax.reload(null,false);
}

function reloadKasir() {
    dtkasir.ajax.reload();
}
function settanggal() {
    $(".datepicker").datepicker({ autoclose: !0, format: "yyyy-mm-dd", orientation: "bottom" }).datepicker("setDate", "now");
}
function listdatakasir() {
    dtkasir = $("#dtkasir").DataTable({
        dom: '<"toolbar">frtip',
        processing: !0,
        serverSide: !0,
        stateSave: !0,
        order: [],
        ajax: {
            data: {
                isbelumdibayar: function () {
                    return $('input[name="isbelumdibayar"]:checked').val();
                },
                tgl: function () {
                    return $('input[name="tanggal"]').val();
                },
                tgl2: function () {
                    return $('input[name="tanggal2"]').val();
                },
                norm: function () {
                    return $("#caripasien").val();
                },
                idunit: function () {
                    return localStorage.getItem("idunit");
                },
                idbangsal:function(){
                    return localStorage.getItem('idbangsal')
                },
            },
            url: base_url + "cpelayanan/dt_listkasir",
            type: "POST",
        },
        columnDefs: [{ targets: [9, 10, 11, 12], orderable: !1 }],
        fnCreatedRow: function (a, t, n) {
            $(a).attr("style", qlstatuswarnapp(t[13]));
        },
        drawCallback: function (a, t) {
            $('[data-toggle="tooltip"]').tooltip();
        },
    });
}
function select2_serachmultidata() {
    var a = 0;
    $("#caripasien").select2({
        allowClear: !0,
        ajax: {
            url: base_url + "cpelayanan/kasir_getdtpasien",
            type: "post",
            dataType: "json",
            delay: 250,
            data: function (t) {
                return "" == t.term ? (a += 1) : (a = 0), { searchTerm: t.term, page: a };
            },
            processResults: function (a) {
                return { results: a };
            },
            cache: !0,
        },
    });
}
function formBayarNonJaminan(a, t, n, i, e, r, l = 0, o = "") {
    var d =
        '<form action="" id="formBayar"><div class="form-group col-md-12 bg bg-info" style="padding:10px;border-radius:4px;"><div class="pull-left" style="font-size:16px;padding-top:0px;margin-top:0px;"><label>TOTAL TAGIHAN : </label></div><div class="pull-right"><label id="labeltotaltagihan" class="" style="font-size:30px;margin-left:8px;">Rp ' +
        t +
        '</label></div><input type="hidden" name="altt" value="' +
        a +
        '" /><input type="hidden" name="altp" value="' +
        n +
        '" /><input type="hidden" name="modebayar" value="' +
        (l > 0 ? "ubahtagihan" : "tagihan") +
        '" /></div><div class="form-group"><div class="col-md-6"><label>TAGIHAN</label><input style="margin-bottom:8.5px;" type="text" name="txttagihan" value="' +
        t +
        '" id="txttagihan" class="form-control" onkeyup="hitungkembalian(event)" disabled /><label>NOMINAL BAYAR</label><input style="margin-bottom:8.5px;" type="text" name="txtnominalbayar" value="' +
        ("" != o && "tagihan" == o ? l : 0) +
        '" placeholder="nominal tunai" autocomplete="off" id="txtnominalbayar" class="form-control" onkeyup="hitungkembalian(event)" /></div><div class="col-md-6"><label>CARA BAYAR</label><div">\n                                        <select id="carabayar" onchange="optioncarabayar(this.value)" class="form-control" style="margin-bottom:6px;"></select><span id="optioncarabayar"></span></div></div><div class="col-md-12"></div><div class="form-group" style="margin-top:9px;"><div class="col-md-6"><label>KEKURANGAN</label><input type="text" name="txtkekurangan" placeholder="kekurangan" class="form-control" disabled/></div></div><div class="col-md-12"></div><div class="form-group" style="margin-top:9px;"><div class="col-md-6"><label>KEMBALI</label><input type="text" name="txtkembali"  placeholder="kembali" class="form-control" disabled/></div></div>' +
        r +
        "</form>";
    $.confirm({
        title: "Pelunasan Biaya",
        content: d,
        closeIcon: !0,
        type: "orange",
        columnClass: "medium",
        buttons: {
            formSubmit: {
                text: "simpan",
                btnClass: "btn-blue",
                action: function () {
                    return "" == $("#txtnominalbayar").val() || "0" == $("#txtnominalbayar").val() || 0 == $("#txtnominalbayar").val()
                        ? ($("#txtnominalbayar").focus(), alert_empty("Nominal Pembayaran."), !1)
                        : 0 == $("#carabayar").val() || "0" == $("#carabayar").val()
                        ? (alert_empty("Cara Pembayaran."), !1)
                        : 0 == $("#selbanktujuan").val() && "transfer" == $("#carabayar").val()
                        ? (alert_empty("bank tujuan"), !1)
                        : (simpanpembayaran(), void cetakvpasien($('input[name="altp"]').val(), i, e));
                },
            },
            formReset: { text: "batal", btnClass: "btn-danger" },
        },
        onContentReady: function () {
            dropdown_getdata($("#carabayar"), "cmasterdata/get_enumjenispembayaran", "", '<option value="0">Pilih</option>');
            var a = this;
            this.$content.find("form").on("submit", function (t) {
                t.preventDefault(), a.$$formSubmit.trigger("click");
            });
        },
    });
}
function optioncarabayar(a) {
    var t = "";
    "potonggaji" == a
        ? ((t =
              '<label>Penanggung</label>\n                  <input list="listpenanggung" id="penanggung" name="penanggung" type="text" class="form-control" placeholder="penanggung"/>\n                  <datalist id="listpenanggung"></datalist>'),
          $("#optioncarabayar").html(t),
          listpenanggung())
        : "transfer" == a
        ? ((t = '<label>Bank Tujuan</label>\n                  <select id="selbanktujuan" onchange="hitungkembalian(event)" name="selbanktujuan" class="select2" style="width:100%;"><option value="">Pilih</option></select></div>'),
          $("#optioncarabayar").html(t),
          dropdown_getdata($("#selbanktujuan"), "cmasterdata/fillkeuakunbank", "", '<option value="0">Bank Tujuan</option>'),
          $(".select2").select2())
        : $("#optioncarabayar").html(t);
}
function listpenanggung() {
    var a = dtpenanggung,
        t = "";
    for (var n in a) t += '<option value="' + a[n].text + '">';
    $("#listpenanggung").empty(), $("#listpenanggung").html(t);
}
function hitungkembalian(a) {
    if (13 == a.keyCode) simpanpembayaran();
    else {
        $("#selbanktujuan").val();
        var t = unconvertToRupiah($('input[name="txttagihan"]').val()),
            n = unconvertToRupiah($('input[name="txtnominalbayar"]').val()),
            i = parseInt(n);
        $('input[name="txtnominalbayar"]').val(n < 1 ? 0 : convertToRupiah(n)), $('input[name="txtkembali"]').val(convertToRupiah(i - t));
        var e = t - i;
        e < 0 && (e = 0), $('input[name="txtkekurangan"]').val(convertToRupiah(e));
        var r = convertToRupiahBulat(parseInt(t));
        $("#labeltotaltagihan").text("Rp " + r);
    }
}
function simpanpembayaran() {
    if ("" == $("#txtnominalbayar").val()) return $("#txtnominalbayar").focus(), alert_empty("Nominal Pembayaran"), !1;
    var a = unconvertToRupiah($('input[name="txtkekurangan"]').val()),
        t = unconvertToRupiah($('input[name="txtnominalbayar"]').val()),
        n = unconvertToRupiah($('input[name="txtkembali"]').val()),
        i = $('select[name="selbanktujuan"]').val(),
        e = $("#carabayar").val(),
        r = $("#penanggung").val();
    startLoading(),
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/simpan_pembayaran",
            data: { k: n, s: a, b: t, j: "tagihan", t: $('input[name="altt"]').val(), p: $('input[name="altp"]').val(), modebayar: $('input[name="modebayar"]').val(), carabayar: e, bt: i, penanggung: r },
            dataType: "JSON",
            success: function (a) {
                stopLoading(), dtkasir.ajax.reload(null, !1);
            },
            error: function (a) {
                stopLoading(), console.log(a.responseText);
            },
        });
}

    // cetakvpasienralan => cetakvpasien($(this).attr("alt"), "rajal", 3);
    // cetakvpasienranap => cetakvpasien($(this).attr("alt"), "rajalnap", 1);
    
function cetakvpasien(a, t, n, i = "") {
    startLoading();
    var e = a,
        r = ((t = t), 3 == n ? "rajal" : 4 == n ? "ranap" : ""),
        l = "",
        o = "",
        d = "",
        s = "",
        g = "",
        p = "",
        u = "",
        c = 0,
        h = 0,
        _ = 0;
    $.ajax({
        type: "POST",
        url: base_url + ("rajalnap" == t ? "cpelayanan/cetak_versiranap" : "cpelayanan/cetak_langsung"),
        data: { p: e, j: r, i: i, jns: 0 },
        dataType: "JSON",
        success: function (a) {
            if ((stopLoading(), "rajalnap" == t)) return print_vpasienranap(a), !0;
            (l = (g = "rajal" != t ? "width:19cm;" : "width:6cm;") + ("rajal" != t ? "font-size:normal;" : "font-size:small;")),
                (o = "rajal" != t ? "font-size:normal;" : "font-size:x-small;"),
                (d = "rajal" != t ? "20" : "7.6"),
                (s = "rajal" != t ? "2" : "0.2"),
                (p +=
                    '<div style="' +
                    g +
                    'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' +
                    ("rajal" != t ? "" : g) +
                    '" src="' +
                    base_url +
                    "assets/images/headerkuitansi" +
                    ("rajal" != t ? "" : "small") +
                    '.jpg" />\n                          <table border="0" style="' +
                    l +
                    'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                null != a.infotagihan &&
                    ("rajal" != t
                        ? ((p += '<tr><td>Nama</td><td colspan="3">' + a.infotagihan[0].namalengkap + "</td></tr>"),
                          (p += "<tr><td>Umur</td><td>" + a.infotagihan[0].usia + "</td><td>No RM</td><td>" + a.infotagihan[0].norm + "</td></tr>"),
                          (p += '<tr><td>Alamat</td><td colspan="3">' + a.infotagihan[0].alamat + "</td></tr>"),
                          (p += '<tr><td>Ruang/Kelas</td><td colspan="3">' + a.infotagihan[0].ruangkelas + "</td></tr>"),
                          (p += "<tr><td>Tanggal Masuk</td><td>" + a.infotagihan[0].waktumasuk + "</td><td>Tanggal Keluar</td><td>" + a.infotagihan[0].waktukeluar + "</td></tr>"),
                          (p += '<tr><td>Dirawat Selama</td><td colspan="3">' + a.infotagihan[0].dirawatselama + "</td></tr>"),
                          (p += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">' + a.infotagihan[0].dokterdbjp + "</td></tr>"),
                          (p += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">' + a.infotagihan[0].kelas + "</td></tr>"))
                        : ((p += "<tr><td>Nama</td><td>" + a.infotagihan[0].namalengkap + "</td></tr>"),
                          (p += "<tr><td>Alamat</td><td>" + a.infotagihan[0].alamat + "</td></tr>"),
                          (p += "<tr><td>Penanggung</td><td>" + a.infotagihan[0].penanggung + "</td></tr>"),
                          (p += "<tr><td>No RM</td><td>" + a.infotagihan[0].norm + "</td></tr>"))),
                (p += '</table><table border="0" style="' + l + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                (p += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>');
            for (x in a.detailtagihan)
                (y = x - 1),
                    u != a.detailtagihan[x].jenistarif &&
                        ("" != u && (p += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">' + convertToRupiah(Math.round(c)) + "</td></tr>"), (p += '<tr><td colspan="3">' + a.detailtagihan[x].jenistarif + "</td></tr>"), (c = 0)),
                    (c +=
                        (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) *
                        parseFloat(a.detailtagihan[x].jumlah) *
                        (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                    (h +=
                        "Diskon" == a.detailtagihan[x].jenistarif
                            ? 0
                            : (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) *
                              parseFloat(a.detailtagihan[x].jumlah) *
                              (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                    1 == a.detailtagihan[x].iskenapajak
                        ? ((p +=
                              "<tr><td>" +
                              ++_ +
                              " " +
                              a.detailtagihan[x].nama +
                              (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1
                                  ? " [" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatanbarang, 10) + "]")
                                  : "") +
                              '</td><td align="right" style="font-size:' +
                              o +
                              '">' +
                              hapus3digitkoma(a.detailtagihan[x].jumlah) +
                              '</td><td align="right">' +
                              convertToRupiah(parseInt(a.detailtagihan[x].jasaoperator, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))) +
                              "</td></tr>"),
                          (p +=
                              '<tr><td style="font-size:11px;">' +
                              ++_ +
                              " Akomodasi " +
                              a.detailtagihan[x].nama +
                              (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1
                                  ? " [" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatanbarang, 10) + "]")
                                  : "") +
                              '</td><td align="right" style="font-size:' +
                              o +
                              '">' +
                              hapus3digitkoma(a.detailtagihan[x].jumlah) +
                              '</td><td align="right">' +
                              convertToRupiah(parseInt(a.detailtagihan[x].akomodasi, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))) +
                              "</td></tr>"))
                        : (p +=
                              "<tr><td>" +
                              ++_ +
                              " " +
                              a.detailtagihan[x].nama +
                              (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1
                                  ? " [" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatanbarang, 10) + "]")
                                  : "") +
                              '</td><td align="right" style="font-size:' +
                              o +
                              '">' +
                              hapus3digitkoma(a.detailtagihan[x].jumlah) +
                              '</td><td align="right">' +
                              convertToRupiah(Math.round(
                                  (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) *
                                      parseFloat(a.detailtagihan[x].jumlah) *
                                      (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)))
                              ) +
                              "</td></tr>"),
                    (u = a.detailtagihan[x].jenistarif);
            if (
                ((p += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">' + convertToRupiah(Math.round(c)) + "</td></tr>"),
                (p += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">' + convertToRupiahBulat(h) + "</td></tr>"),
                (p += '</table><table border="0" style="' + l + 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                "jaminan" == i || "mandiri" == i || "klaim" == i)
            )
                (p += '<tr><td>No Nota</td><td align="right">' + a.infotagihan[0].idtagihan + "</td></tr>"),
                    (p += '<tr><td>Waktu Tagih</td><td align="right">' + a.infotagihan[0].waktutagih + "</td></tr>"),
                    (p += '<tr><td>Waktu Bayar</td><td align="right">' + a.infotagihan[0].waktubayar + "</td></tr>"),
                    (p += '<tr><td>Nominal</td><td align="right">' + convertToRupiahBulat(h) + "</td></tr>"),
                    (p += '<tr><td>Pembulatan</td><td align="right">' + convertToRupiah(bulatkanLimaRatusan(h) - h) + "</td></tr>"),
                    (p += '<tr><td>Tot.Tagihan</td><td align="right">' + convertToRupiah(bulatkanLimaRatusan(h)) + "</td></tr>"),
                    (p += '<tr><td>Dibayar</td><td align="right">' + convertToRupiah(bulatkanLimaRatusan(h)) + "</td></tr>"),
                    (p += '<tr><td>Jenis Tagihan</td><td align="right">' + ("mandiri" == i ? "tagihan" : a.infotagihan[0].jenistagihan) + "</td></tr>");
            else if (1 == n || 3 == n)
                for (x in a.infotagihan)
                    parseInt(3 == n ? h : a.infotagihan[x].nominal, 10) - (parseInt(a.infotagihan[x].dibayar, 10) + parseInt(a.infotagihan[x].potongan, 10)),
                        (p += '<tr><td>No Nota</td><td align="right">' + a.infotagihan[x].idtagihan + "</td></tr>"),
                        (p += '<tr><td>Waktu Tagih</td><td align="right">' + a.infotagihan[x].waktutagih + "</td></tr>"),
                        (p += '<tr><td>Waktu Bayar</td><td align="right">' + a.infotagihan[x].waktubayar + "</td></tr>"),
                        (p += '<tr><td>Nominal</td><td align="right">' + convertToRupiah(a.infotagihan[x].nominal) + "</td></tr>"),
                        (p += '<tr><td>Potongan</td><td align="right">' + convertToRupiahBulat(a.infotagihan[x].potongan) + "</td></tr>"),
                        (p += '<tr><td>Pembulatan</td><td align="right">' + convertToRupiahBulat(a.infotagihan[x].pembulatan) + "</td></tr>"),
                        (p += '<tr><td>Tot.Tagihan</td><td align="right">' + convertToRupiahBulat(parseInt(a.infotagihan[x].nominal) + parseInt(a.infotagihan[x].pembulatan) - parseInt(a.infotagihan[x].potongan)) + "</td></tr>"),
                        ("jknnonpbi" == a.infotagihan[x].jenistagihan && "jknpbi" == a.infotagihan[x].jenistagihan) || (p += '<tr><td>Dibayar</td><td align="right">' + convertToRupiah(a.infotagihan[x].dibayar) + "</td></tr>"),
                        (p += a.infotagihan[x].kekurangan > 0 ? '<tr><td>Kekurangan</td><td align="right">' + convertToRupiah(a.infotagihan[x].kekurangan) + "</td></tr>" : ""),
                        (p += a.infotagihan[x].kembalian > 0 ? '<tr><td>Kembalian</td><td align="right">' + convertToRupiah(a.infotagihan[x].kembalian) + "</td></tr>" : ""),
                        (p += '<tr><td>Jenis Tagihan</td><td align="right">' + a.infotagihan[x].jenistagihan + "</td></tr>");
            else {
                var e,
                    r = "",
                    m = "",
                    f = "",
                    b = 0,
                    k = 0,
                    v = 0,
                    T = 0,
                    $ = 0;
                for (x in a.infotagihan)
                    (r += a.infotagihan[x].idtagihan + " | "),
                        (m += a.infotagihan[x].waktutagih + " | "),
                        (f += a.infotagihan[x].waktubayar + " | "),
                        (b += parseInt(a.infotagihan[x].nominal, 10)),
                        (k += parseInt(a.infotagihan[x].dibayar, 10)),
                        (v += parseInt(a.infotagihan[x].potongan, 10)),
                        (T += parseInt(a.infotagihan[x].pembulatan, 10)),
                        ($ += parseInt(a.infotagihan[x].kekurangan, 10)),
                        (e = a.infotagihan[x].jenistagihan);
                (b -= $),
                    (p += '<tr><td>No Nota</td><td align="right">' + r + "</td></tr>"),
                    (p += '<tr><td>Waktu Tagih</td><td align="right">' + m + "</td></tr>"),
                    (p += '<tr><td>Waktu Bayar</td><td align="right">' + f + "</td></tr>"),
                    (p += '<tr><td>Nominal</td><td align="right">' + convertToRupiahBulat(b) + "</td></tr>"),
                    v > 0
                        ? ((p += '<tr><td>Potongan</td><td align="right">' + convertToRupiahBulat(v) + "</td></tr>"),
                          (p += '<tr><td>Pembulatan</td><td align="right">' + convertToRupiahBulat(T) + "</td></tr>"),
                          (p += '<tr><td>Tot.Tagihan</td><td align="right">' + convertToRupiahBulat(b + T - v) + "</td></tr>"))
                        : ((p += '<tr><td>Pembulatan</td><td align="right">' + convertToRupiahBulat(T) + "</td></tr>"), (p += '<tr><td>Tot.Tagihan</td><td align="right">' + convertToRupiahBulat(b + T) + "</td></tr>")),
                    "rajal" != t
                        ? (p += "jknnonpbi" == a.infotagihan[x].jenistagihan || "jknpbi" == a.infotagihan[x].jenistagihan ? "" : '<tr><td>Dibayar</td><td align="right">' + convertToRupiah(k) + "</td></tr>")
                        : ((p += '<tr><td>Dibayar</td><td align="right">' + convertToRupiah(k) + "</td></tr>"),
                          (p += a.infotagihan[x].kekurangan > 0 ? '<tr><td>Kekurangan</td><td align="right">' + convertToRupiah(a.infotagihan[x].kekurangan) + "</td></tr>" : ""),
                          (p += a.infotagihan[x].kembalian > 0 ? '<tr><td>Kembalian</td><td align="right">' + convertToRupiah(a.infotagihan[x].kembalian) + "</td></tr>" : "")),
                    (p += '<tr><td>Jenis Tagihan</td><td align="right">' + e + "</td></tr>");
            }
            (p +=
                "jknpbi" !== a.infotagihan[0].jenispembayaran && "jknnonpbi" != a.infotagihan[0].jenispembayaran && "asuransi" != a.infotagihan[0].jenispembayaran
                    ? '<tr><td>Cara Bayar</td><td align="right">' + a.infotagihan[0].jenispembayaran + "</td></tr>"
                    : ""),
                (p += '<tr><td colspan="2" align="center">dicetak oleh ' + sessionStorage.user + ", pada " + new Date().toLocaleString("id") + "</td></tr>"),
                (p += "</table></div>"),
                fungsi_cetaktopdf(p, "<style>@page {size:" + d + "cm 100%;margin:" + s + "cm;}</style>");
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function print_vpasienranap(a) {
    var t = "";
    (t += '<div style="float: none;"><img style="opacity:0.9;margin-bottom:-2px;width:67%;"  src="' + base_url + 'assets/images/kwitansibig.svg" />'),
        (t += '<div style="margin-top:-10px;">__________________________________________________________________________________________________________________________________</div>'),
        (t += '<div style="margin-top:-14px;margin-bottom:6px;">__________________________________________________________________________________________________________________________________</div>'),
        (t += '<div style="text-align:center;font-size:24px;margin-bottom:5px;">KUITANSI RAWAT INAP</div>'),
        (t += '<table border="0" style="width:100%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
        (t += '<tr><td>Nama Pasien</td><td colspan="3">: ' + a.infotagihan[0].namalengkap + "</td><td>&nbsp;&nbsp; RM</td><td>: " + a.infotagihan[0].norm + "</td></tr>"),
        (t += '<tr><td>Umur</td><td colspan="3">: ' + a.infotagihan[0].usia + "</td></tr>"),
        (t += '<tr><td>Alamat</td><td colspan="4" style="font-size:13px;">: ' + a.infotagihan[0].alamat + "</td></tr>"),
        (t += '<tr><td>Ruang/Kelas</td><td colspan="3">: ' + a.infotagihan[0].ruangkelas + "</td></tr>"),
        (t += '<tr><td>Tanggal Masuk</td><td colspan="3">: ' + a.infotagihan[0].waktumasuk + "</td><td>&nbsp;&nbsp; Tanggal Keluar</td><td>: " + a.infotagihan[0].waktukeluar + "</td></tr>"),
        (t += '<tr><td>Dirawat Selama</td><td colspan="3">: ' + a.lamaRanap + " hari</td></tr>"), // Andri - bugfix [0000590]
        (t += '<tr><td>Dokter Penanggungjawab</td><td colspan="3">: ' + a.infotagihan[0].dokterdbjp + "</td></tr>"),
        (t += '<tr><td>Kelas Perawatan</td><td colspan="3">: ' + a.infotagihan[0].kelas + "</td></tr>"),
        (t += "</table>"),
        (t += "<div>__________________________________________________________________________________________________________________________________</div>"),
        (t += '<table border="0" style="width:100%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">');
    var n = 0,
        i = 0,
        e = "",
        r = 0;
    for (var l in a.listtagihan)
        "" != e && e != a.listtagihan[l].jenistarif && ((t += "<tr><td>" + ++n + "</td><td>" + e + '</td><td style="text-align:right">Rp </td><td style="text-align:left">' + convertToRupiah(r) + "</td></tr>"), (r = 0)),
            (e = a.listtagihan[l].jenistarif),
            (r += parseInt(a.listtagihan[l].nominal)),
            (i += parseInt(a.listtagihan[l].nominal));
    (t += "<tr><td>" + ++n + "</td><td>" + e + '</td><td style="text-align:right">Rp </td><td style="text-align:left">' + convertToRupiah(r) + "</td></tr>"),
        (t += '<tr><td colspan="4">__________________________________________________________________________________________________________________________________</td></tr>'),
        (t += '<tr><td></td><td style="text-align:right">Total Biaya</td><td style="text-align:right">Rp </td><td style="text-align:left">' + convertToRupiah(i) + "</td></tr>"),
        (t += '<tr><td colspan="4">&nbsp;</td></tr>'),
        (t += '<tr><td></td><td colspan="2"><i>Terbilang : ' + Terbilang(i) + " Rupiah</i></td><td></td></tr>"),
        (t += '<tr><td colspan="4">&nbsp;</td></tr>'),
        (t += '<tr><td colspan="4">&nbsp;</td></tr>'),
        (t += '<tr><td colspan="2"></td><td style="text-align:right;">' + a.ttd + "</td><td></td></tr>"),
        (t += '<tr><td colspan="2"></td><td style="text-align:right; padding-right:20px;">Bagian Keuangan</td><td></td></tr>'),
        (t += "</table>"),
        (t += "</div>"),
        fungsi_cetaktopdf(t, "<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>");
}

function print_vpasienralan() {
    fungsi_cetaktopdf(listkasir, "<style>@page {size:" + paper + "cm 100%;margin:" + margin + "cm;}</style>");
}

function print_invoiceGabung(a) {
    var t = "",
        title = "Invoice Pasien Versi GABUNG";
    
    t += '<div style="float: none;">\n\
                    <img style="opacity:0.9;margin-bottom:-2px;width:67%;"  src="'+ base_url + 'assets/images/headerkuitansismall.jpg" />';

    t += '<table border="0" style="width:10%;">\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td></td><td style="width:5px;"></td><td></td></tr>\n\
                    </table>';

    t += '<table border="0" style="width:100%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
    t += '<div style="border-top: double;text-align:center;font-size:18px;">KUITANSI RAWAT INAP</div>';
    t += '<tr><td colspan="2">Nama Pasien</td><td colspan="2">: ' + a.infotagihan[0].namalengkap + "</td><td>&nbsp;&nbsp; RM</td><td>: " + a.infotagihan[0].norm + "</td></tr>";
    t += '<tr><td colspan="2">Umur</td><td colspan="2">: ' + a.infotagihan[0].usia + "</td></tr>";
    t += '<tr><td colspan="2">Alamat</td><td colspan="3" style="font-size:13px;">: ' + a.infotagihan[0].alamat + "</td></tr>";
    t += '<tr><td colspan="2">Ruang/Kelas</td><td colspan="2">: ' + a.infotagihan[0].ruangkelas + "</td></tr>";
    t += '<tr><td colspan="2">Tanggal Masuk</td><td colspan="2">: ' + a.infotagihan[0].waktumasuk + "</td><td>&nbsp;&nbsp; Tanggal Keluar</td><td>: " + a.infotagihan[0].waktukeluar + "</td></tr>";
    t += '<tr><td colspan="2">Dirawat Selama</td><td colspan="2">: ' + a.lamaRanap + " hari</td></tr>"; // Andri - bugfix [0000590]
    t += '<tr><td colspan="2">Dokter Penanggungjawab</td><td colspan="2">: ' + a.infotagihan[0].dokterdbjp + "</td></tr>";
    t += '<tr><td colspan="2">Kelas Perawatan</td><td colspan="2">: ' + a.infotagihan[0].kelas + "</td></tr>";
    t += '</table>';

    t += '<table border="0" style="width:50%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
    t += '<tr><td colspan="6" style="border-bottom:1px solid;"></td></tr>';

    var n = 0,
        i = 0,
        e = "",
        r = 0;

    for (var l in a.listtagihan) {
        if (e != '' && e != a.listtagihan[l].jenistarif) {
            t += '<tr><td>' + ++n + '</td><td style="width:50%;">' + e + '</td><td colspan="3" style="text-align:right">Rp </td><td style="text-align:right">' + r + '</td></tr>'; //convertToRupiah(subtotal)
            r = 0;
        }
        e = a.listtagihan[l].jenistarif;
        r +=
                (parseInt(a.listtagihan[l].sewa, 10) + parseInt(a.listtagihan[l].nonsewa, 10)) *
                parseFloat(a.listtagihan[l].jumlah) *
                (parseInt(a.listtagihan[l].kekuatan, 10) / parseInt(a.listtagihan[l].kekuatanbarang, 10));
        i +=
            "Diskon" == a.listtagihan[l].jenistarif
                ? 0
                : (parseInt(a.listtagihan[l].sewa, 10) + parseInt(a.listtagihan[l].nonsewa, 10)) *
                parseFloat(a.listtagihan[l].jumlah) *
                (parseInt(a.listtagihan[l].kekuatan, 10) / parseInt(a.listtagihan[l].kekuatanbarang, 10));
    }

    
    t += '<tr><td>' + ++n + '</td><td style="width:50%;">' + e + '</td><td colspan="3" style="text-align:right">Rp </td><td style="text-align:right">' + r + '</td></tr>'; //convertToRupiah(subtotal)
            
        
        t += '<tr><td colspan="6" style="border-bottom:1px solid;"></td></tr>'; // border
        (t += '<tr> <td></td> <td></td> <td></td> <td style="text-align:right">Total Biaya</td><td style="text-align:right">Rp </td><td style="text-align:right">' + i + "</td></tr>"),
        (t += '<tr><td colspan="4">&nbsp;</td></tr>'),
        (t += '<tr><td></td><td colspan="4"><i>Terbilang : ' + Terbilang(i) + " Rupiah</i></td><td></td></tr>"),
        (t += '<tr><td colspan="6">&nbsp;</td></tr>'),
        (t += '<tr><td colspan="6">&nbsp;</td></tr>'),
        (t += '<tr><td colspan="4"></td><td colspan="2" style="text-align:center;">' + a.ttd + "</td><td></td></tr>"),
        (t += '<tr><td colspan="4"></td><td colspan="2" style="text-align:center; padding-right:20px;">Bagian Keuangan</td><td></td></tr>'),
        (t += "</table>"),
        (t += "</div>"),

        // fungsi_cetaktopdf(t, "<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>");
        cetak_excel_langsung(a.infotagihan[0].norm, t, title);
}

function fungsi_pemeriksaanklinikUbahStatus(a, t, n, i) {
    $.confirm({
        icon: "fa fa-question",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Konfirmasi",
        content: t,
        buttons: {
            ok: function () {
                $.ajax({
                    url: base_url + "cpelayanan/pemeriksaanklinik_batalselesai",
                    type: "post",
                    dataType: "json",
                    data: { i: n, status: a },
                    success: function (a) {
                        dtkasir.ajax.reload(null, !1), notif(a.status, a.message);
                    },
                    error: function (a) {
                        return fungsiPesanGagal(), !1;
                    },
                });
            },
            batal: function () {},
        },
    });
}
function generatetopdf_versipasien(a) {
    window.open(base_url + "creport/download_pdfkasir/" + a);
}
function Terbilang(a) {
    var t = parseInt(a);
    return t < parseInt(12)
        ? " " + ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"][t]
        : t < parseInt(20)
        ? Terbilang(t - parseInt(10)) + " Belas"
        : t < parseInt(100)
        ? Terbilang(t / parseInt(10)) + " Puluh" + Terbilang(t % parseInt(10))
        : t < parseInt(200)
        ? " Seratus" + Terbilang(t - parseInt(100))
        : t < parseInt(1e3)
        ? Terbilang(t / parseInt(100)) + " Ratus" + Terbilang(t % parseInt(100))
        : t < parseInt(2e3)
        ? " Seribu" + Terbilang(t - parseInt(1e3))
        : t < parseInt(1e6)
        ? Terbilang(t / parseInt(1e3)) + " Ribu" + Terbilang(t % parseInt(1e3))
        : t < parseInt(1e9)
        ? Terbilang(t / parseInt(1e6)) + " Juta" + Terbilang(t % parseInt(1e6))
        : void 0;
}
$(function () {
    settanggal(), select2_serachmultidata(), get_datapoli("#idunit", localStorage.getItem("idunit") ? localStorage.getItem("idunit") : ""), $("#idunit").select2(),$("#idbangsal").select2(), listdatakasir(), get_databangsal('#idbangsal', ( (localStorage.getItem('idbangsal')) ? localStorage.getItem('idbangsal') : '' ) ), getpenanggung();
    // console.log(localStorage);
}),
    $(document).on("click", "#tampilkasir", function () {
        dtkasir.ajax.reload();
    }),
    $(document).on("click", "#refreshkasir", function () {
        settanggal(), $("#caripasien").val(""), $('input[type="search"]').val("").keyup(), dtkasir.state.clear(), dtkasir.ajax.reload();
    }),
    $(document).on("change", "#caripasien", function () {
        dtkasir.ajax.reload();
    }),
    $(document).on("click", "#detailtagihan", function () {
        var a = $(this).attr("alt");
        startLoading(),
            $.ajax({
                url: base_url + "cpelayanan/keu_detailtagihan",
                type: "POST",
                dataType: "JSON",
                data: { p: a },
                success: function (a) {
                    stopLoading();
                    var t = '<table class="table table-striped table-bordered">';
                    (t += '<tr><td class="bg-info">No.RM</td><td colspan="5">: ' + a[0].norm + "</td></tr>"),
                        (t += '<tr><td class="bg-info">Nama Pasien</td><td colspan="5">: ' + a[0].namapasien + "</td></tr>"),
                        (t += '<tr><td class="bg-info">Tgl.Periksa</td><td colspan="5">: ' + a[0].tanggalperiksa + "</td></tr>"),
                        (t += '<tr><td class="bg-info">Carabayar</td><td colspan="5">: ' + a[0].carabayar + "</td></tr>"),
                        (t += '<tr><td class="bg-info">StatusTagihan</td><td colspan="5">: ' + a[0].statustagihan + "</td></tr>"),
                        (t += '<tr class="bg-yellow"><th>Waktu Tagih</th><th>Tagihan</th><th>Kekurangan</th><th>Dibayar</th><th>Jenis Tagihan</th><th></th></tr>');
                    var n = "";
                    for (var i in a)
                        n +=
                            "<tr><td>" +
                            a[i].waktutagih +
                            "</td><td>" +
                            convertToRupiah(a[i].tagihan) +
                            "</td><td>" +
                            convertToRupiah(parseInt(a[i].tagihan) - parseInt(a[i].dibayar)) +
                            "</td><td>" +
                            convertToRupiah(a[i].dibayar) +
                            "</td><td>" +
                            a[i].jenistagihan +
                            '</td><td><a id="ubahjenistagihan" alt="' +
                            a[i].idtagihan +
                            '" ispesanranap="' +
                            a[i].statusinap +
                            '" alt2="' +
                            convertToRupiah(a[i].tagihan) +
                            '" alt3="' +
                            a[i].idpendaftaran +
                            '" alt4="' +
                            a[i].jenispemeriksaan +
                            '" alt5="' +
                            convertToRupiah(a[i].dibayar) +
                            '" alt6="' +
                            a[i].jenistagihan +
                            '" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Bayar" href="#"><i class="fa fa-edit"></i></a> <a id="batallunas" waktu="' +
                            a[i].waktutagih +
                            '" nominal="' +
                            convertToRupiah(a[i].dibayar) +
                            '" jenistagihan="' +
                            a[i].jenistagihan +
                            '" idt="' +
                            a[i].idtagihan +
                            '" class="btn btn-xs btn-danger" ' +
                            tooltip("Batal Lunas") +
                            ' ><i class="fa fa-money"></i></a> </td></tr>';
                    var e = t + n + "</table>";
                    $.confirm({ title: "Ubah Jenis Tagihan", content: e, closeIcon: !0, columnClass: "medium", buttons: { formReset: { text: "kembali", btnClass: "btn-danger" } } });
                },
                error: function (a) {
                    return stopLoading(), fungsiPesanGagal(), !1;
                },
            });
    }),
    $(document).on("click", "#ubahjenistagihan", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt2"),
            n = $(this).attr("alt3"),
            i = $(this).attr("alt4"),
            e = $(this).attr("alt5"),
            r = "pesan" == $(this).attr("ispesanranap") ? 3 : "",
            l = $(this).attr("alt6");
        formBayarNonJaminan(
            a,
            t,
            n,
            i,
            r,
            "pesan" == $(this).attr("ispesanranap") ? '<div class="bg bg-danger" style="padding-left:none;">*Pasien Sudah Booking RANAP. <br> Setelah menyelesaikan pembayaran, status pasien dimutasi ke RANAP.</div>' : "",
            e,
            l
        );
    }),
    $(document).on("click", "#bayar", function () {
        formBayarNonJaminan(
            $(this).attr("alt"),
            $(this).attr("alt2"),
            $(this).attr("alt3"),
            $(this).attr("alt4"),
            "pesan" == $(this).attr("ispesanranap") ? 3 : "",
            "pesan" == $(this).attr("ispesanranap")
                ? '<label class="col-xs-12" style="padding-left:none;"><small class="text text-red">*Pasien Sudah Booking RANAP. <br> Setelah menyelesaikan pembayaran, status pasien dimutasi ke RANAP.</small></label>'
                : ""
        );
    }),
    $(document).on("click", "#jknnonpbi", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt2"),
            n = ($(this).attr("nobaris"), $(this).attr("alt4")),
            i = "pesan" == $(this).attr("ispesanranap") ? 3 : "",
            e = $(this).attr("nominal");
        $.confirm({
            title: "Konfirmasi Pembayaran",
            content: "Bayar dengan JKN NONPBI.?",
            closeIcon: !0,
            columnClass: "medium",
            buttons: {
                formSubmit: {
                    text: "Bayar",
                    btnClass: "btn-warning",
                    action: function () {
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/simpan_pembayaran_jkn",
                                data: { t: a, p: t, j: "jknnonpbi", dibayar: e },
                                dataType: "JSON",
                                success: function (a) {
                                    if ((stopLoading(), null != a.status)) return formBayarNonJaminan(a.tagihan.idtagihan, a.tagihan.nominal, t, n, "mandiri", "");
                                    cetakvpasien(t, n, i), dtkasir.ajax.reload(null, !1);
                                },
                                error: function (a) {
                                    stopLoading(), console.log(a.responseText);
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger" },
            },
        });
    }),
    $(document).on("click", "#jknpbi", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt2"),
            n = ($(this).attr("nobaris"), $(this).attr("alt4")),
            i = "pesan" == $(this).attr("ispesanranap") ? 3 : "",
            e = $(this).attr("nominal");
        $.confirm({
            title: "Konfirmasi Pembayaran",
            content: "Bayar dengan JKNPBI.?",
            closeIcon: !0,
            columnClass: "medium",
            buttons: {
                formSubmit: {
                    text: "Bayar",
                    btnClass: "btn-warning",
                    action: function () {
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/simpan_pembayaran_jkn",
                                data: { t: a, p: t, j: "jknpbi", dibayar: e },
                                dataType: "JSON",
                                success: function (a) {
                                    if ((stopLoading(), null != a.status)) return formBayarNonJaminan(a.tagihan.idtagihan, a.tagihan.nominal, t, n, "mandiri", "");
                                    cetakvpasien(t, n, i), dtkasir.ajax.reload(null, !1);
                                },
                                error: function (a) {
                                    stopLoading(), console.log(a.responseText);
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger" },
            },
        });
    }),
    $(document).on("click", "#retur", function () {
        $(this).attr("alt");
        var a = $(this).attr("alt2"),
            t = $(this).attr("alt3"),
            n = $(this).attr("alt4");
        startLoading(),
            $.ajax({
                type: "POST",
                url: base_url + "cpelayanan/simpan_returpembayaran",
                data: { p: t, d: a },
                dataType: "JSON",
                success: function (a) {
                    stopLoading(), cetakvpasien(t, n, "3"), dtkasir.ajax.reload(null, !1);
                },
                error: function (a) {
                    stopLoading(), console.log(a.responseText);
                },
            });
    }),
    $(document).on("click", "#batalperiksa", function () {
        var a = $(this).attr("alt");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "Batal Pemeriksaan.?",
            buttons: {
                ok: function () {
                    startLoading(),
                        $.ajax({
                            type: "POST",
                            url: base_url + "cadmission/hapus_data_pendaftaran",
                            data: { i: a },
                            dataType: "JSON",
                            success: function (a) {
                                stopLoading(), dtkasir.ajax.reload(null, !1);
                            },
                            error: function (a) {
                                stopLoading(), console.log(a.responseText);
                            },
                        });
                },
                batal: function () {},
            },
        });
    }),
    $(document).on("click", "#bukapemeriksaan", function () {
        var a = $(this).attr("alt");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "Buka Pemeriksaan.?",
            buttons: {
                ok: function () {
                    startLoading(),
                        $.ajax({
                            type: "POST",
                            url: base_url + "cadmission/buka_pelayanan",
                            data: { i: a },
                            dataType: "JSON",
                            success: function (a) {
                                stopLoading(), dtkasir.ajax.reload(null, !1);
                            },
                            error: function (a) {
                                stopLoading(), console.log(a.responseText);
                            },
                        });
                },
                batal: function () {},
            },
        });
    }),

    // >>>>> cetak excel
    // $(document).on("click","#cetakExcelPasienRalan", function(){ //cetak kasir pasien rajal
    //     cetakExcelPasien($(this).attr("alt"), 'rajal', 3);
    // });


    $(document).on("click","#cetakExcelPasienRajalnap", function(){ //cetak kasir pasien rajalnap
        cetakExcelPasien($(this).attr("alt"), 'rajalnap', 3);
    });

    function cetakExcelPasien(ip, jenispemeriksaan, mode,modelist='')
    {
        startLoading();
        var idp = ip;
        var jenispemeriksaan = jenispemeriksaan;
        var jenisnota = (mode==3) ? 'rajal' : (mode==4) ? 'ranap' : '' ;
        var font = '', fontx = '', paper = '', margin = '', lebar = '', lebargambar;
        var listkasir='',tipesebelum='',subtotal=0,total=0,no=0, selisih = 0;
        // var p = idp;
        // var j = jenisnota;
        // var i = modelist; 
        // var jns = 0;
        // var url = base_url + ((jenispemeriksaan == 'rajalnap') ? 'cpelayanan/cetak_versiranap' : 'cpelayanan/cetak_excel_langsung' );
        //     
        $.ajax({
            type: "POST",
            url: base_url + ((jenispemeriksaan == 'rajalnap') ? 'cpelayanan/cetak_excel_versiranap_getData' : 'cpelayanan/cetak_excel_langsung_getData' ),
            data: {p:idp, j:jenisnota,i:modelist, jns:0},
            dataType: "JSON",
            success: function(result) {
                stopLoading();
                
                if(jenispemeriksaan == 'rajalnap')
                {
                    print_Excelpasienranap(idp, result);
                    
                return true;
                }
                
                lebar = (jenispemeriksaan!='rajal') ? 'width:19cm;' : 'width:6cm;';
                lebargambar = (jenispemeriksaan!='rajal') ? '' : lebar;
                font = lebar + ((jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:small;');
                fontx = (jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:x-small;';
                paper = (jenispemeriksaan!='rajal') ? '20' : '7.6';
                margin = (jenispemeriksaan!='rajal') ? '2' : '0.2';
                // listkasir += '<div style @page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}>';
                listkasir += '<div style="' + lebar + 'float: none;">\n\
                                <img style="opacity:0.5;filter:alpha(opacity=50);' + lebargambar + '" src="'+base_url+'assets/images/headerkuitansi' + ((jenispemeriksaan!='rajal') ? '' : 'small') + '.jpg" />';
                            //   </div>;
                listkasir += '<table border="0" style=" width:10%; ' + font + 'font-family: Palatino Linotype, Book Antiqua, Palatino, serif;color: #333333;">\n\
                                <tr>\n\
                                    <td></td>\n\
                                    <td></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td></td>\n\
                                    <td></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td></td>\n\
                                    <td></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td></td>\n\
                                    <td></td>\n\
                                </tr>';
                            
                                // <tr><td colspan="2" rowspan="4"></td></tr>';
                // listkasir += '<div style="width:19cm; float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);" src="http://172.16.200.2/sirstql_dev/assets/images/headerkuitansi' + 'small' + '.jpg" />\n\
                            // <table border="0" style="width:19cm; font-size:normal; font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            
                if (result['infotagihan'] != null)
                {
                    if (jenispemeriksaan!='rajal')
                    {
                        listkasir += '<tr><td>Nama</td><td colspan="3">'+result['infotagihan'][0].namalengkap+'</td></tr>';
                        listkasir += '<tr><td>Umur</td><td>'+result['infotagihan'][0].usia+'</td><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                        listkasir += '<tr><td>Alamat</td><td colspan="3">'+result['infotagihan'][0].alamat+'</td></tr>';
                        listkasir += '<tr><td>Ruang/Kelas</td><td colspan="3">'+result['infotagihan'][0].ruangkelas+'</td></tr>';
                        listkasir += '<tr><td>Tanggal Masuk</td><td>'+result['infotagihan'][0].waktumasuk+'</td><td>Tanggal Keluar</td><td>'+result['infotagihan'][0].waktukeluar+'</td></tr>';
                        listkasir += '<tr><td>Dirawat Selama</td><td colspan="3">'+result['infotagihan'][0].dirawatselama+'</td></tr>';
                        listkasir += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">'+result['infotagihan'][0].dokterdbjp+'</td></tr>';
                        listkasir += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">'+result['infotagihan'][0].kelas+'</td></tr>';
                    }
                    else
                    {
                        listkasir += '<tr>\n\
                                        <td>Nama</td>\n\
                                        <td>'+result['infotagihan'][0].namalengkap+'</td>\n\
                                    </tr>';
                        listkasir += '<tr>\n\
                                        <td>Alamat</td>\n\
                                        <td>'+result['infotagihan'][0].alamat+'</td>\n\
                                    </tr>';
                        listkasir += '<tr>\n\
                                        <td>Penanggung</td>\n\
                                        <td>'+result['infotagihan'][0].penanggung+'</td>\n\
                                    </tr>';
                        listkasir += '<tr>\n\
                                        <td>No RM</td>\n\
                                        <td>'+result['infotagihan'][0].norm+'</td>\n\
                                    </tr>';
                    }
                }
                listkasir += '</table>\n\
                            <table border="0" style="width:10%;' + font + 'font-family: Palatino Linotype, Book Antiqua, Palatino, serif;color: #333333;">';
                listkasir += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>';
                var akomodasi=0;
                for(x in result['detailtagihan'])
                {
                    y = x-1;
                    if (tipesebelum != result['detailtagihan'][x].jenistarif)
                    {
                        // if (tipesebelum != '') listkasir += '<tr><td>'+ ++no +' Akomodasi ' +result['detailtagihan'][y].jenistarif  + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '"></td><td align="right">'+ convertToRupiah(parseInt(akomodasi,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                        if (tipesebelum != '') listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+subtotal+'</td></tr>'; //convertToRupiah(subtotal)
                        listkasir += '<tr><td colspan="3">'+result['detailtagihan'][x].jenistarif+'</td></tr>';
                        // akomodasi = 0;
                        subtotal  = 0;
                    }
                    subtotal += (parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                    total    += (result['detailtagihan'][x].jenistarif=='Diskon') ? 0 :  (parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)) ;
                    if(result['detailtagihan'][x].iskenapajak==1)
                    {//jika iskenapajak maka tarif ditampilkan terpisah antara jasamedis dengan akomodasi
                        listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].jasaoperator,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                        listkasir += '<tr><td style="font-size:11px;">'+ ++no +' Akomodasi '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].akomodasi,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                    }else{
                        listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                    }
                    tipesebelum = result['detailtagihan'][x].jenistarif;
                }
                listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+subtotal+'</td></tr>';  //convertToRupiah(subtotal)
                listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+total+'</td></tr>';  //convertToRupiahBulat((total))
                listkasir += '</table><table border="0" style="width:10%;' + font + 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
                
                //jika mode 1,3 atau biaya admin > 0 tampilkan riwayat transaksi
                if(modelist=='jaminan' || modelist=='mandiri' || modelist=='klaim' )
                {       
                    listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][0].idtagihan+'</td></tr>';
                    listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][0].waktutagih+'</td></tr>';
                    listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][0].waktubayar+'</td></tr>';
                    listkasir += '<tr><td>Nominal</td><td align="right">'+ total +'</td></tr>';  //convertToRupiahBulat( total )
                    listkasir += '<tr><td>Pembulatan</td><td align="right">'+ bulatkanLimaRatusan(total) - total +'</td></tr>'; //convertToRupiah( bulatkanLimaRatusan(total) - total )
                    listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+ bulatkanLimaRatusan(total) +'</td></tr>';  //convertToRupiah( bulatkanLimaRatusan(total) )              
                    listkasir += '<tr><td>Dibayar</td><td align="right">'+bulatkanLimaRatusan(total)+'</td></tr>' ;  //convertToRupiah(bulatkanLimaRatusan(total))
                    listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+ ((modelist=='mandiri')? 'tagihan' : result['infotagihan'][0].jenistagihan )+'</td></tr>';
                }
                else
                {
                    if (mode == 1 || mode == 3)
                    {
                        for(x in  result['infotagihan'])
                        {
                            selisih   += parseInt( ((mode==3) ? total : result['infotagihan'][x].nominal ) , 10) - ( parseInt(result['infotagihan'][x].dibayar, 10) + parseInt(result['infotagihan'][x].potongan,10));
                            listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                            listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                            listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                            listkasir += '<tr><td>Nominal</td><td align="right">'+ result['infotagihan'][x].nominal +'</td></tr>';  //convertToRupiah( result['infotagihan'][x].nominal )  //convertToRupiah(result['infotagihan'][x].nominal)
                            listkasir += '<tr><td>Potongan</td><td align="right">'+result['infotagihan'][x].potongan+'</td></tr>'; //convertToRupiahBulat(result['infotagihan'][x].potongan) 
                            listkasir += '<tr><td>Pembulatan</td><td align="right">'+result['infotagihan'][x].pembulatan+'</td></tr>';  //convertToRupiahBulat(result['infotagihan'][x].pembulatan)
                            listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+ parseInt(result['infotagihan'][x].nominal + parseInt(result['infotagihan'][x].pembulatan) - parseInt(result['infotagihan'][x].potongan ) ) +'</td></tr>';  //convertToRupiahBulat( parseInt(result['infotagihan'][x].nominal)
                                if(result['infotagihan'][x].jenistagihan!='jknnonpbi' ||  result['infotagihan'][x].jenistagihan!='jknpbi' )
                                {
                                    listkasir += '<tr><td>Dibayar</td><td align="right">'+result['infotagihan'][x].dibayar+'</td></tr>' ;  //convertToRupiah(result['infotagihan'][x].dibayar)
                                }
                                listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+result['infotagihan'][x].kekurangan+'</td></tr>' : '' ); //convertToRupiah(result['infotagihan'][x].kekurangan)
                                listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+result['infotagihan'][x].kembalian+'</td></tr>' : '' );  //convertToRupiah(result['infotagihan'][x].kembalian)                  
                            listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
                        }
                    }
                    else
                    {
                        var nonota = '', waktutagih='', waktubayar='', nominal = 0, dibayar = 0, potongan = 0,pembulatan=0, jenistagihan, kekurangan=0;
                        for(x in result['infotagihan'])
                        {
                            nonota          += result['infotagihan'][x].idtagihan + ' | ';
                            waktutagih      += result['infotagihan'][x].waktutagih + ' | ';
                            waktubayar      += result['infotagihan'][x].waktubayar + ' | ';
                            nominal         += parseInt(result['infotagihan'][x].nominal, 10);
                            dibayar         += parseInt(result['infotagihan'][x].dibayar, 10);
                            potongan        += parseInt(result['infotagihan'][x].potongan, 10);
                            pembulatan      += parseInt(result['infotagihan'][x].pembulatan, 10);
                            kekurangan      += parseInt(result['infotagihan'][x].kekurangan, 10);
                            jenistagihan    = result['infotagihan'][x].jenistagihan;
                        }
                        nominal = nominal - kekurangan;
                        listkasir += '<tr><td>No Nota</td><td align="right">'+nonota+'</td></tr>';
                        listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+waktutagih+'</td></tr>';
                        listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+waktubayar+'</td></tr>';
                        listkasir += '<tr><td>Nominal</td><td align="right">'+nominal+'</td></tr>';  //convertToRupiahBulat(nominal)  //convertToRupiah(result['infotagihan'][x].nominal)
                        if (potongan > 0)
                        {
                            listkasir += '<tr><td>Potongan</td><td align="right">'+potongan+'</td></tr>'; //convertToRupiahBulat(potongan)
                            listkasir += '<tr><td>Pembulatan</td><td align="right">'+pembulatan+'</td></tr>'; //convertToRupiahBulat(pembulatan)                         
                            listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+nominal + pembulatan - potongan+'</td></tr>'; //convertToRupiahBulat(nominal + pembulatan - potongan)
                        }
                        else
                        {
                            listkasir += '<tr><td>Pembulatan</td><td align="right">'+pembulatan+'</td></tr>'; //convertToRupiahBulat(pembulatan)
                            listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+nominal+pembulatan+'</td></tr>';  //convertToRupiahBulat(nominal+pembulatan)
                        }
                        
                        if (jenispemeriksaan!='rajal')
                        {
                            listkasir += ((result['infotagihan'][x].jenistagihan=='jknnonpbi' ||  result['infotagihan'][x].jenistagihan=='jknpbi' ) ? '' : '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(dibayar)+'</td></tr>') ;

                        }
                        else
                        {
                            listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(dibayar)+'</td></tr>';
                            listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                            listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                        }
                        listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+jenistagihan+'</td></tr>';
                    }
                }
                listkasir += ((result['infotagihan'][0].jenispembayaran !=='jknpbi' && result['infotagihan'][0].jenispembayaran != 'jknnonpbi' && result['infotagihan'][0].jenispembayaran !='asuransi') ? '<tr><td>Cara Bayar</td><td align="right">'+result['infotagihan'][0].jenispembayaran+'</td></tr>' : '' ) ;
                listkasir += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
                listkasir += '<tr><td colspan="2" align="center">--Harga obat sudah termasuk ppn--</td></tr>';
                listkasir += '</table></div>';
                var title = "Pasien Ralan";
                // var p = idp;
                // var j = jenisnota;
                // var i = modelist; 
                // var jns = 0;
                // var url = base_url + ((jenispemeriksaan == 'rajalnap') ? 'cpelayanan/cetak_excel_versiranap' : 'cpelayanan/cetak_excel_langsung' );
                
                // cetak_excel_langsung(idp, jenisnota, modelist, jns,listkasir);
                cetak_excel_langsung(idp, listkasir, title);
        //         fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
            },
            error: function(result) { //jika error
                stopLoading();
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
    }

    function cetak_excel_langsung(id='', content='', title=''){
        var url = base_url+'cpelayanan/cetak_excel_langsung';
        var form = $('<form action="' + url + '" method="post">' +
        '<input type="hidden" name="p" value="'+ id +'" />' +
        '<textarea hidden="hidden" name="listkasir" >'+ content +'</textarea>'+
        '<input type="hidden" name="title" value="'+ title +'" />' +
        '</form>');
        $('body').append(form);
        form.submit();
    }

    function print_Excelpasienranap(idp, result)
    {
        var listkasir = '';
        
        listkasir += '<div style="float: none;">\n\
                    <img style="opacity:0.9;margin-bottom:-2px;width:67%;"  src="'+base_url+'assets/images/headerkuitansismall.jpg" />';
        
        listkasir += '<table border="0" style="width:10%;">\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td colspan="3"></td></tr>\n\
                    <tr><td></td><td style="width:5px;"></td><td></td></tr>\n\
                    </table>';
        // listkasir += '<div style="border-top: double;"></div>';
        // listkasir += '<div>__________________________________________________________________________________________________________________________________</div>';
        
        // listkasir += '<div style="margin-top:-10px;">__________________________________________________________________________________________________________________________________</div>';
        // listkasir += '<div style="margin-top:-14px;margin-bottom:6px;">__________________________________________________________________________________________________________________________________</div>';
        
        listkasir += '<div style="border-top: double;text-align:center;font-size:18px;margin-bottom:5px;">KUITANSI RAWAT INAP</div>';
        
        listkasir += '<table border="0" style="width:10%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
        listkasir += '<tr><td colspan="2">Nama Pasien</td><td colspan="2">: '+result['infotagihan'][0].namalengkap+'</td><td>&nbsp;&nbsp; RM</td><td>: '+result['infotagihan'][0].norm+'</td></tr>';
        listkasir += '<tr><td colspan="2">Umur</td><td colspan="2">: '+result['infotagihan'][0].usia+'</td></tr>';
        listkasir += '<tr><td colspan="2">Alamat</td><td colspan="3" style="font-size:13px;">: '+result['infotagihan'][0].alamat+'</td></tr>';
        listkasir += '<tr><td colspan="2">Ruang/Kelas</td><td colspan="2">: '+result['infotagihan'][0].ruangkelas+'</td></tr>';
        listkasir += '<tr><td colspan="2">Tanggal Masuk</td><td colspan="2">: '+result['infotagihan'][0].waktumasuk+'</td><td>&nbsp;&nbsp; Tanggal Keluar</td><td>: '+result['infotagihan'][0].waktukeluar+'</td></tr>';
        listkasir += '<tr><td colspan="2">Dirawat Selama</td><td colspan="2">: '+result['infotagihan'][0].dirawatselama+' hari</td></tr>';
        listkasir += '<tr><td colspan="2">Dokter Penanggungjawab</td><td colspan="2">: '+result['infotagihan'][0].dokterdbjp+'</td></tr>';
        listkasir += '<tr><td colspan="2">Kelas Perawatan</td><td colspan="2">: '+result['infotagihan'][0].kelas+'</td></tr>';
        listkasir += '</table>';

        listkasir += '<table border="0" style="width:50%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
        listkasir += '<tr><td colspan="6" style="border-bottom:1px solid;"></td></tr>';
        var no = 0;
        var total = 0;
        var jenistarif = '';
        var subtotal = 0;
        
        for(var x in result.listtagihan)
        {
            if(jenistarif != '' && jenistarif != result.listtagihan[x].jenistarif)
            {
                listkasir += '<tr><td>'+ ++no +'</td><td style="width:50%;">'+jenistarif+'</td><td colspan="3" style="text-align:right">Rp </td><td style="text-align:left">'+subtotal+'</td></tr>'; //convertToRupiah(subtotal)
                subtotal = 0;
            }
            jenistarif = result.listtagihan[x].jenistarif;
            subtotal += parseInt(result.listtagihan[x].nominal);        
            total += parseInt(result.listtagihan[x].nominal);
        }    
        listkasir += '<tr><td>'+ ++no +'</td><td>'+jenistarif+'</td><td colspan="3" style="text-align:right">Rp </td><td style="text-align:left">'+subtotal+'</td></tr>'; //convertToRupiah(subtotal)
        listkasir += '<tr><td colspan="6" style="border-bottom:1px solid;"></td></tr>';
        listkasir += '<tr><td></td><td style="text-align:right">Total Biaya</td><td colspan="3" style="text-align:right">Rp </td><td style="text-align:left">'+total+'</td></tr>';  //convertToRupiah(total)
        
        listkasir += '<tr><td colspan="6">&nbsp;</td></tr>';
        listkasir += '<tr><td></td><td colspan="5"><i>Terbilang : '+Terbilang(total)+' Rupiah</i></td></tr>';
        
        listkasir += '<tr><td colspan="6">&nbsp;</td></tr>';
        listkasir += '<tr><td colspan="6">&nbsp;</td></tr>';
        
        listkasir += '<tr><td colspan="3"></td><td colspan="2" style="text-align:right;">'+result.ttd+'</td><td></td></tr>';
        listkasir += '<tr><td colspan="4"></td><td style="text-align:right; padding-right:20px;">Bagian Keuangan</td><td></td></tr>';
        
        listkasir += '</table>'; 
        
        
        listkasir += '</div>';

        var idp = idp;
        var title = "Pasien Ranap"
        cetak_excel_langsung(idp,listkasir, title);
        // fungsi_cetaktopdf(listkasir,'<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>');
    }

    // <<<<< cetak excel

    $(document).on("click", "#cetakvpasienralan", function () {
        cetakvpasien($(this).attr("alt"), "rajal", 3);
    }),
    $(document).on("click", "#cetakvjaminan", function () {
        cetakvpasien($(this).attr("alt"), "rajal", 3, "jaminan");
    }),
    $(document).on("click", "#cetakvmandiri", function () {
        cetakvpasien($(this).attr("alt"), "rajal", 3, "mandiri");
    }),
    $(document).on("click", "#cetakvpasienranap", function () {
        cetakvpasien($(this).attr("alt"), "ranap", 4);
    }),
    $(document).on("click", "#cetakvpasien", function () {
        cetakvpasien($(this).attr("alt"), $(this).attr("alt4"), 1);
    }),
    $(document).on("click", "#cetakvpasiengabung", function () {
        cetakvpasien($(this).attr("alt"), $(this).attr("alt4"), 2);
    }),
    $(document).on("click", "#cetakvklaim", function () {
        cetakvpasien($(this).attr("alt"), "rajal", 1, "klaim");
    }),
    $(document).on("click", "#cetakvjenistarif", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt4"),
            n = "",
            i = "",
            e = "",
            r = "",
            l = "",
            o = "",
            d = "",
            s = 0,
            g = 0,
            p = 0,
            u = 0;
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_langsung",
            data: { p: a },
            dataType: "JSON",
            success: function (a) {
                for (x in ((n = (l = "rajal" != t ? "width:19cm;" : "width:6cm;") + ("rajal" != t ? "font-size:normal;" : "font-size:small;")),
                (i = "rajal" != t ? "font-size:normal;" : "font-size:x-small;"),
                (e = "rajal" != t ? "20" : "7.6"),
                (r = "rajal" != t ? "2" : "0.2"),
                (o +=
                    '<div style="' +
                    l +
                    'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' +
                    ("rajal" != t ? "" : l) +
                    '" src="' +
                    base_url +
                    "assets/images/headerkuitansi" +
                    ("rajal" != t ? "" : "small") +
                    '.jpg" />\n                          <table border="0" style="' +
                    n +
                    'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                null != a.infotagihan &&
                    ("rajal" != t
                        ? ((o += '<tr><td>Nama</td><td colspan="3">' + a.infotagihan[0].namalengkap + "</td></tr>"),
                          (o += "<tr><td>Umur</td><td>" + a.infotagihan[0].usia + "</td><td>No RM</td><td>" + a.infotagihan[0].norm + "</td></tr>"),
                          (o += '<tr><td>Alamat</td><td colspan="3">' + a.infotagihan[0].alamat + "</td></tr>"),
                          (o += '<tr><td>Ruang/Kelas</td><td colspan="3">' + a.infotagihan[0].ruangkelas + "</td></tr>"),
                          (o += "<tr><td>Tanggal Masuk</td><td>" + a.infotagihan[0].waktumasuk + "</td><td>Tanggal Keluar</td><td>" + a.infotagihan[0].waktukeluar + "</td></tr>"),
                          (o += '<tr><td>Dirawat Selama</td><td colspan="3">' + a.infotagihan[0].dirawatselama + "</td></tr>"),
                          (o += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">' + a.infotagihan[0].dokterdbjp + "</td></tr>"),
                          (o += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">' + a.infotagihan[0].kelas + "</td></tr>"))
                        : ((o += "<tr><td>Nama</td><td>" + a.infotagihan[0].namalengkap + "<br/>" + a.infotagihan[0].alamat + "</td></tr>"),
                          (o += "<tr><td>Penanggung</td><td>" + a.infotagihan[0].penanggung + "</td></tr>"),
                          (o += "<tr><td>No RM</td><td>" + a.infotagihan[0].norm + "</td></tr>"))),
                (o += '</table><table border="0" style="' + n + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                (o += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>'),
                a.detailtagihan))
                    d != a.detailtagihan[x].jenistarif &&
                        ("" != d && (o += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">' + convertToRupiah(s) + "</td></tr>"), (o += '<tr><td colspan="3">' + a.detailtagihan[x].jenistarif + "</td></tr>"), (s = 0)),
                        (p += parseInt(a.detailtagihan[x].sewa, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                        (s += parseInt(a.detailtagihan[x].nonsewa, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                        (g +=
                            "Diskon" == a.detailtagihan[x].jenistarif
                                ? 0
                                : parseInt(a.detailtagihan[x].nonsewa, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                        (o +=
                            "<tr><td>" +
                            ++u +
                            " " +
                            a.detailtagihan[x].nama +
                            (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1
                                ? " [" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detailtagihan[x].kekuatanbarang, 10) + "]")
                                : "") +
                            '</td><td align="right" style="font-size:' +
                            i +
                            '">' +
                            hapus3digitkoma(a.detailtagihan[x].jumlah) +
                            '</td><td align="right">' +
                            convertToRupiah(parseInt(a.detailtagihan[x].nonsewa, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))) +
                            "</td></tr>"),
                        (d = a.detailtagihan[x].jenistarif);
                for (x in ((o += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">' + convertToRupiah(s) + "</td></tr>"),
                p > 0 && ((o += '<tr><td colspan="3">Sewa Alat</td></tr>'), (o += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">' + convertToRupiah(p) + "</td></tr>"), (g += p)),
                (o += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">' + convertToRupiahBulat(bulatkanRatusan(g)) + "</td></tr>"),
                (o += '</table><table border="0" style="' + n + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                a.infotagihan))
                    (o += '<tr><td>No Nota</td><td align="right">' + a.infotagihan[x].idtagihan + "</td></tr>"),
                        (o += '<tr><td>Waktu Tagih</td><td align="right">' + a.infotagihan[x].waktutagih + "</td></tr>"),
                        (o += '<tr><td>Waktu Bayar</td><td align="right">' + a.infotagihan[x].waktubayar + "</td></tr>"),
                        (o += '<tr><td>Nominal</td><td align="right">' + convertToRupiahBulat(a.infotagihan[x].nominal) + "</td></tr>"),
                        (o += '<tr><td>Potongan</td><td align="right">' + convertToRupiahBulat(a.infotagihan[x].potongan) + "</td></tr>"),
                        "rajal" != t && (o += '<tr><td>Biaya</td><td align="right">' + convertToRupiahBulat(a.infotagihan[x].nominal - a.infotagihan[x].potongan) + "</td></tr>"),
                        (o += '<tr><td>Dibayar</td><td align="right">' + convertToRupiah(a.infotagihan[x].dibayar) + "</td></tr>"),
                        (o += a.infotagihan[x].kekurangan > 0 ? '<tr><td>Kekurangan</td><td align="right">' + convertToRupiah(a.infotagihan[x].kekurangan) + "</td></tr>" : ""),
                        (o += a.infotagihan[x].kembalian > 0 ? '<tr><td>Kembalian</td><td align="right">' + convertToRupiah(a.infotagihan[x].kembalian) + "</td></tr>" : ""),
                        (o += '<tr><td>Jenis Tagihan</td><td align="right">' + a.infotagihan[x].jenistagihan + "</td></tr>");
                (o += '<tr><td colspan="2" align="center">dicetak oleh ' + sessionStorage.user + ", pada " + new Date().toLocaleString("id") + "</td></tr>"),
                    (o += "</table></div>"),
                    fungsi_cetaktopdf(o, "<style>@page {size:" + e + "cm 100%;margin:" + r + "cm;}</style>");
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
    }),
    $(document).on("click", "#cetakvsemua", function () {
        var a = $(this).attr("alt4"),
            t = $(this).attr("alt"),
            n = "",
            i = "",
            e = "",
            r = 0,
            l = 0,
            o = 0;
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_langsung_lengkap",
            data: { p: t },
            dataType: "JSON",
            success: function (t) {
                for (x in ((n += '<div style="width:29.7cm;float: none;"><table border="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                null != t.infotagihan &&
                    ("rajal" != a
                        ? ((n += '<tr><td>Nama</td><td colspan="3">' + t.infotagihan[0].namalengkap + "</td></tr>"),
                          (n += "<tr><td>Umur</td><td>" + t.infotagihan[0].usia + "</td><td>No RM</td><td>" + t.infotagihan[0].norm + "</td></tr>"),
                          (n += '<tr><td>Alamat</td><td colspan="3">' + t.infotagihan[0].alamat + "</td></tr>"),
                          (n += '<tr><td>Ruang/Kelas</td><td colspan="3">' + t.infotagihan[0].ruangkelas + "</td></tr>"),
                          (n += "<tr><td>Tanggal Masuk</td><td>" + t.infotagihan[0].waktumasuk + "</td><td>Tanggal Keluar</td><td>" + t.infotagihan[0].waktukeluar + "</td></tr>"),
                          (n += '<tr><td>Dirawat Selama</td><td colspan="3">' + t.infotagihan[0].dirawatselama + "</td></tr>"),
                          (n += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">' + t.infotagihan[0].dokterdbjp + "</td></tr>"),
                          (n += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">' + t.infotagihan[0].kelas + "</td></tr>"))
                        : ((n += "<tr><td>Nama</td><td>" + t.infotagihan[0].namalengkap + "<br/>" + t.infotagihan[0].alamat + "</td></tr>"),
                          (n += "<tr><td>Penanggung</td><td>" + t.infotagihan[0].penanggung + "</td></tr>"),
                          (n += "<tr><td>No RM</td><td>" + t.infotagihan[0].norm + "</td></tr>"))),
                t.detailtagihan))
                    i != t.detailtagihan[x].jenisicd && (n += '<tr><td colspan="13" style="font-size:small;"><strong>' + t.detailtagihan[x].jenisicd + "</strong></td></tr>"),
                        e != t.detailtagihan[x].jenistarif &&
                            ((n += '<tr><td colspan="13" style="font-size:small;"><u>' + t.detailtagihan[x].jenistarif + "</u></td></tr>"),
                            (n +=
                                '<tr><td>Nama</td><td>Nilai</td><td align="right">Jasa Op.</td><td align="right">Nakes</td><td align="right">Jasa RS</td><td align="right">BHP</td><td align="right">Akomodasi</td><td align="right">Margin</td><td align="right">Sewa</td><td align="right">Kekuatan</td><td align="right">Resep</td><td align="right">Jumlah</td><td align="right">Harga</td><td align="right">Pot.</td><td align="right">Total</td></tr>'),
                            (o = 0)),
                        0 != t.detailtagihan[x].idgroup && 0 != t.detailtagihan[x].idpaketpemeriksaan && null == t.detailtagihan[x].icd && t.detailtagihan[x].idpaketpemeriksaan != t.detailtagihan[x].idgrup
                            ? (n += '<tr><td colspan="13">' + t.detailtagihan[x].nama + "</td></tr>")
                            : (n +=
                                  "<tr><td>" +
                                  ++o +
                                  " " +
                                  t.detailtagihan[x].nama +
                                  "</td><td>" +
                                  t.detailtagihan[x].nilai +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].jasaoperator, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].nakes, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].jasars, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].bhp, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].akomodasi, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].margin, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].sewa, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].kekuatan, 10)) +
                                  "/" +
                                  convertToRupiah(parseInt(t.detailtagihan[x].kekuatanbarang, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].jumlahdiresepkan, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].jumlahpemakaian, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].harga, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].potongan, 10)) +
                                  '</td><td align="right">' +
                                  convertToRupiah(parseInt(t.detailtagihan[x].total - t.detailtagihan[x].potongan, 10)) +
                                  "</td></tr>"),
                        (e = t.detailtagihan[x].jenistarif),
                        (i = t.detailtagihan[x].jenisicd),
                        (r += parseInt(t.detailtagihan[x].total, 10)),
                        (l += parseInt(t.detailtagihan[x].potongan, 10));
                for (x in ((n += '<tr><td align="right" colspan="11">== Total ==</td><td align="right">' + convertToRupiahBulat(bulatkanRatusan(r - l)) + "</td></tr>"), t.infotagihan))
                    parseInt(t.infotagihan[x].nominal, 10) - parseInt(t.infotagihan[x].potongan, 10) - parseInt(t.infotagihan[x].dibayar, 10),
                        (n += '<tr><td>No Nota</td><td colspan="2">' + t.infotagihan[x].idtagihan + "</td></tr>"),
                        (n += '<tr><td>Waktu Tagih</td><td colspan="2">' + t.infotagihan[x].waktutagih + "</td></tr>"),
                        (n += '<tr><td>Waktu Bayar</td><td colspan="2">' + t.infotagihan[x].waktubayar + "</td></tr>"),
                        "rajal" != a
                            ? ((n += '<tr><td>Nominal</td><td align="right" colspan="2">' + convertToRupiah(t.infotagihan[x].nominal) + "</td></tr>"),
                              (n += '<tr><td>Potongan</td><td align="right" colspan="2">' + convertToRupiah(t.infotagihan[x].potongan) + "</td></tr>"),
                              (n += '<tr><td>Biaya</td><td align="right" colspan="2">' + convertToRupiahBulat(t.infotagihan[x].nominal - t.infotagihan[x].potongan) + "</td></tr>"))
                            : (n += '<tr><td>Nominal</td><td align="right" colspan="2">' + convertToRupiahBulat(t.infotagihan[x].nominal) + "</td></tr>"),
                        (n += '<tr><td>Dibayar</td><td align="right" colspan="2">' + convertToRupiah(t.infotagihan[x].dibayar) + "</td></tr>"),
                        (n += t.infotagihan[x].kekurangan > 0 ? '<tr><td>Kekurangan</td><td align="right">' + convertToRupiah(t.infotagihan[x].kekurangan) + "</td></tr>" : ""),
                        (n += t.infotagihan[x].kembalian > 0 ? '<tr><td>Kembalian</td><td align="right">' + convertToRupiah(t.infotagihan[x].kembalian) + "</td></tr>" : ""),
                        (n += '<tr><td>Jenis Tagihan</td><td align="right" colspan="2">' + t.infotagihan[x].jenistagihan + "</td></tr>");
                (n += '<tr><td colspan="12" align="center">dicetak oleh ' + sessionStorage.user + ", pada " + new Date().toLocaleString("id") + "</td></tr>"),
                    (n += "</table></div>"),
                    fungsi_cetaktopdf(n, "<style>@page {size:29.7cm 21cm;margin:0.2cm;}</style>");
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
    }),
    $(document).on("click", "#cetakrekapinap", function () {
        var a = $(this).attr("alt5"),
            t = $(this).attr("alt"),
            n = "";
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_rekap_inap",
            data: { in: a, idp: t },
            dataType: "JSON",
            success: function (a) {
                for (k in ((n += '<div style="width:29.7cm;float: none;">'),
                (n += '<table cellpadding="0" cellspacing="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                null != a.infotagihan &&
                    ((n += '<tr><td width="200px">Nama</td><td width="500px">' + a.infotagihan[0].namalengkap + "<br/>" + a.infotagihan[0].alamat + "</td></tr>"),
                    (n += "<tr><td>Penanggung</td><td>" + a.infotagihan[0].penanggung + "</td></tr>"),
                    (n += "<tr><td>No RM</td><td>" + a.infotagihan[0].norm + "</td></tr>")),
                (n += "</table>"),
                (n += '<table border="1" cellpadding="0" cellspacing="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                (n += "<tr><th></th>"),
                a.detailranap.header))
                    n += "<th>" + a.detailranap.header[k] + "</th>";
                for (g in ((n += "</tr>"), a.detailranap.grup))
                    for (b in ((n += "<tr><td>" + g.toUpperCase() + "</td></tr><tr>"), a.detailranap.grup[g])) {
                        for (k in ((n += "<tr><td>" + a.detailranap.id[b] + "</td>"), a.detailranap.header)) n += "<td>" + a.detailranap.data[g][b][k] + "</td>";
                        n += "</tr>";
                    }
                (n += '<tr><td colspan="12" align="center">dicetak oleh ' + sessionStorage.user + ", pada " + new Date().toLocaleString("id") + "</td></tr>"),
                    (n += "</table></div>"),
                    fungsi_cetaktopdf(n, "<style>@page {size:29.7cm 21cm;margin:0.2cm;}</style>");
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
    }),
    $(document).on("click", "#pemeriksaanklinikSelesai", function () {
        var a = $(this).attr("alt2"),
            t = $(this).attr("nobaris"),
            n = $(this).attr("norm");
        fungsi_pemeriksaanklinikUbahStatus(
            "selesai",
            '<p><label class="label label-primary"><b>Apakah Pemeriksaan Sudah Selesai.?</b></label><br> Nama Pasien : ' +
                $(this).attr("nama") +
                "<br>Norm : " +
                n +
                ' <br> <small class="text text-red">*Pemeriksaan yang sudah diselesikan tidak dapat dibuka kembali.</small></p>',
            a,
            t
        );
    }),
    $(document).on("click", "#pemeriksaanklinikBatalselesai", function () {
        fungsi_pemeriksaanklinikUbahStatus("sedang ditangani", "Batal Selesai?", $(this).attr("alt2"), $(this).attr("nobaris"));
    }),
    $(document).on("click", "#generatetoexcek_versipasien", function () {
        var a = $(this).attr("judul");
        console.log(a), (window.location.href = base_url + "creport/downloadexcel_page/" + a + " kasir_versipasien");
    }),
    $(document).on("click", "#batallunas", function () {
        var a = $(this).attr("idt"),
            t = $(this).attr("nominal"),
            n = $(this).attr("waktu"),
            i = $(this).attr("jenistagihan");
        $.confirm({
            title: "Konfirmasi Batal Pelunasan",
            content: "<b>Detail Tagihan</b>\n        <br> Waktu Tagih : " + n + " <br> Nominal  : " + t + " <br> JenisTagihan : " + i,
            closeIcon: !0,
            columnClass: "medium",
            buttons: {
                formSubmit: {
                    text: "Konfirmasi",
                    btnClass: "btn-warning",
                    action: function () {
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/batal_pelunasan",
                                data: { idt: a },
                                dataType: "JSON",
                                success: function (a) {
                                    stopLoading(), dtkasir.ajax.reload(null, !1);
                                },
                                error: function (a) {
                                    stopLoading(), console.log(a.responseText);
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger" },
            },
        });
    }),
    $(document).on("click", "#pemanggilankasir", function () {
        var a = $(this).attr("idp"),
            t = $(this).attr("idper");
        $.ajax({
            type: "POST",
            url: base_url + "cantrian/buatantriankasir",
            data: { idp: a, idper: t },
            dataType: "JSON",
            success: function (a) {
                notif(a.status, a.message);
            },
            error: function (a) {
                console.log(a.responseText);
            },
        });
    }),
    $(document).on("click", "#batalselesai", function () {
        var a = $(this).attr("idp"),
            t = $(this).attr("idt"),
            n = $(this).attr("norm"),
            i = $(this).attr("nama");
        $.confirm({
            title: "Batal Selesai",
            content: "Batal Penyelesaian Pasien BPJS.? <br> Nama: " + i + " <br> NORM: " + n,
            closeIcon: !0,
            columnClass: "small",
            buttons: {
                formSubmit: {
                    text: "konfirmasi",
                    btnClass: "btn-primary",
                    action: function () {
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/batalselesai",
                                data: { idpendaftaran: a, idtagihan: t },
                                dataType: "JSON",
                                success: function (a) {
                                    stopLoading(), dtkasir.ajax.reload(null, !1);
                                },
                                error: function (a) {
                                    stopLoading(), console.log(a.responseText);
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger" },
            },
        });
    }),
    $(document).on("click", "#resetpembayaran", function () {
        var a =
            '<form action="" id="formReset"><input type="hidden" name="idpendaftaran" value="' +
            $(this).attr("idp") +
            '" /><div class="form-group"><label>Alasan </label><textarea name="reset_alasan" id="reset_alasan" class="form-control" rows="3" word-limit="true" min-words="5"></textarea></div><div class="form-group"><label>Petugas </label><p>' +
            sessionStorage.user +
            '</p></div><div class="form-group bg bg-red" style="padding:4px;"><label>Peringatan..! </label><p class="bg bg-red">\n                                <ol><li>Aktivitas <i>reset</i> pembayaran akan disimpan oleh sistem.</li><li>Kasir harus menyelesaikan ulang pelunasan biaya.</li></ol></p></div></form>';
        $.confirm({
            title: "Reset Pembayaran.",
            content: a,
            closeIcon: !0,
            type: "orange",
            columnClass: "medium",
            buttons: {
                formSubmit: {
                    text: "simpan",
                    btnClass: "btn-blue",
                    action: function () {
                        var a = $("#reset_alasan").val().split(/\s+/).filter(Boolean).length;
                        return "" == $("#reset_alasan").val()
                            ? (alert_empty("Alasan."), !1)
                            : a < 3
                            ? ($.alert({ icon: "fa  fa-warning", theme: "modern", closeIcon: !0, animation: "scale", type: "orange", title: "Warning!", content: "Alasan minimal 3 kata." }), !1)
                            : (startLoading(),
                              void $.ajax({
                                  type: "POST",
                                  url: base_url + "cpelayanan/resetpembayaran_ralan",
                                  data: $("#formReset").serialize(),
                                  dataType: "JSON",
                                  success: function (a) {
                                      stopLoading(), notif(a.status, a.message), dtkasir.ajax.reload(null, !1);
                                  },
                                  error: function (a) {
                                      stopLoading(), console.log(a.responseText);
                                  },
                              }));
                    },
                },
                formReset: { text: "batal", btnClass: "btn-danger" },
            },
            onContentReady: function () {
                var a = this;
                this.$content.find("form").on("submit", function (t) {
                    t.preventDefault(), a.$$formSubmit.trigger("click");
                });
            },
        });
    }),
    $(document).on("click", "#cetakvdetail", function () {
        var a = $(this).attr("alt");
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_versidetail",
            data: { p: a, j: "detail", i: "kasir", jns: 0 },
            dataType: "JSON",
            success: function (a) {
                stopLoading();
                var t = "";
                (t += '<div style="float: none;">'),
                    (t += '<table border="0" style="font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                    (t += '<tr><td>NORM</td><td colspan="3">: ' + a.infotagihan[0].norm + "</td></tr>"),
                    (t += '<tr><td>Nama</td><td colspan="3">: ' + a.infotagihan[0].namalengkap + "</td></tr>"),
                    (t += "</table>"),
                    (t += '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;font-family: padding:1px;"Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">');
                var n = "",
                    i = 0,
                    e = "border-bottom:1px solid #000;",
                    r = "",
                    l = 0;
                for (var o in a.detail) {
                    (n == a.detail[o].jenistarif && "" != n) ||
                        ((t += '<tr><td colspan="5">&nbsp;</td>'), (t += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;"> Rekap ' + a.detail[o].jenistarif + "</td></tr>")),
                        (r == a.detail[o].jenistarif + "" + a.detail[o].tanggal && "" != r) ||
                            (n == a.detail[o].jenistarif && (t += '<tr><td colspan="5">&nbsp;</td>'),
                            (t +=
                                '<tr>\n                    <td style="' +
                                e +
                                '" width="100px;"> <i>' +
                                a.detail[o].tanggal +
                                '</i></td>\n                    <td style="' +
                                e +
                                '"> Nama</td>\n                    <td style="' +
                                e +
                                '"> jumlah</td>\n                    <td style="' +
                                e +
                                '"> @harga</td>\n                    <td style="' +
                                e +
                                '"> Subtotal</td></tr>'),
                            (i = 0)),
                        (t +=
                            "<tr>\n                <td> </td>\n                <td> " +
                            ++i +
                            ". " +
                            a.detail[o].nama +
                            "</td>\n                <td> " +
                            a.detail[o].jumlah +
                            "</td>\n                <td> " +
                            convertToRupiah(a.detail[o].subtotal) +
                            "</td>\n                <td> " +
                            convertToRupiah(parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah), ".") +
                            "</td>\n                </tr>"),
                        (l += parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah)),
                        (r = a.detail[o].jenistarif + "" + a.detail[o].tanggal),
                        (n = a.detail[o].jenistarif);
                }
                (t += '<tr><td colspan="5">&nbsp;</td>'),
                    (t += '<tr><td colspan="5">&nbsp;</td>'),
                    (t += '<tr>\n                    <td colspan="4" style="' + e + '"> Total Biaya</td>\n                    <td style="' + e + '"> ' + convertToRupiah(l, ".") + "</td></tr>"),
                    (t += "</table>"),
                    (t += "</div>"),
                    fungsi_cetaktopdf(t, "<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>");
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
    });
    
    $(document).on("click", "#cetakExcelDetail", function () {
        var a = $(this).attr("alt");
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_excel_versidetail_getData",
            data: { p: a, j: "detail", i: "kasir", jns: 0 },
            dataType: "JSON",
            success: function (a) {
                stopLoading();
                var title = "Detail Pasien Ranap";
                var t = "";
                (t += '<div style="float: none;">'),
                    (t += '<table border="0" style="font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                    (t += '<tr><td>NORM</td><td colspan="3">: ' + a.infotagihan[0].norm + "</td></tr>"),
                    (t += '<tr><td>Nama</td><td colspan="3">: ' + a.infotagihan[0].namalengkap + "</td></tr>"),
                    (t += "</table>"),
                    (t += '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;font-family: padding:1px;"Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">');
                var n = "",
                    i = 0,
                    e = "border-bottom:1px solid #000;",
                    r = "",
                    l = 0;
                for (var o in a.detail) {
                    (n == a.detail[o].jenistarif && "" != n) ||
                        ((t += '<tr><td colspan="5">&nbsp;</td>'), (t += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;"> Rekap ' + a.detail[o].jenistarif + "</td></tr>")),
                        (r == a.detail[o].jenistarif + "" + a.detail[o].tanggal && "" != r) ||
                            (n == a.detail[o].jenistarif && (t += '<tr><td colspan="5">&nbsp;</td>'),
                            (t +=
                                '<tr>\n                    <td style="' +
                                e +
                                '" width="100px;"> <i>' +
                                a.detail[o].tanggal +
                                '</i></td>\n                    <td style="' +
                                e +
                                '"> Nama</td>\n                    <td style="' +
                                e +
                                '"> jumlah</td>\n                    <td style="' +
                                e +
                                '"> @harga</td>\n                    <td style="' +
                                e +
                                '"> Subtotal</td></tr>'),
                            (i = 0)),
                        (t +=
                            "<tr>\n                <td> </td>\n                <td> " +
                            ++i +
                            ". " +
                            a.detail[o].nama +
                            "</td>\n                <td> " +
                            a.detail[o].jumlah +
                            "</td>\n                <td> " +
                            a.detail[o].subtotal +//convertToRupiah(a.detail[o].subtotal) +
                            "</td>\n                <td> " +
                            parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah), "." +//convertToRupiah(parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah), ".") +
                            "</td>\n                </tr>"),
                        (l += parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah)),
                        (r = a.detail[o].jenistarif + "" + a.detail[o].tanggal),
                        (n = a.detail[o].jenistarif);
                }
                (t += '<tr><td colspan="5">&nbsp;</td>'),
                    (t += '<tr><td colspan="5">&nbsp;</td>'),
                    (t += '<tr>\n                    <td colspan="4" style="' + e + '"> Total Biaya</td>\n                    <td style="' + e + '"> ' + l + "</td></tr>"), //convertToRupiah(l, ".")
                    (t += "</table>"),
                    (t += "</div>"),
                    
                    cetak_excel_langsung(a.infotagihan[0].norm, t, title);
                    // fungsi_cetaktopdf(t, "<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>");
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
    });

    $(document).on("click", "#cetakExcelVersiClaim", function () {
        var a = $(this).attr("alt");
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/cetak_excel_VersiClaim_getData",
            data: { p: a, j: "detail", i: "kasir", jns: 0 },
            dataType: "JSON",
            success: function (a) {
                console.log(a);
                stopLoading();
                var title = "Detail Pasien Versi Claim",
                    t = "",
                    c = 0,
                    h = 0,
                    _ = 0,
                    n = "",
                    i = 0,
                    e = "border-bottom:1px solid #000;",
                    r = "",
                    l = 0;
                // header information
                (t += '<div style="float: none;">'),
                    (t += '<table border="0" style="font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                    (t += '<tr><td>NORM</td><td colspan="3">: ' + a.infotagihan[0].norm + '</td></tr>'),
                    (t += '<tr><td>Nama</td><td colspan="3">: ' + a.infotagihan[0].namalengkap + '</td></tr>'),
                    (t += '</table>'),
                    (t += '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;font-family: padding:1px;"Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">');
                for (var o in a.detail) {
                    // text configuration
                    (n == a.detail[o].jenistarif && "" != n) ||
                    (   (t += '<tr><td colspan="5">&nbsp;</td>'), 
                        (t += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;"> Rekap ' + a.detail[o].jenistarif + "</td></tr>")
                    ),
                    
                    // table title
                    (r == a.detail[o].jenistarif + "" + a.detail[o].tanggaltagih && "" != r) ||
                    (n == a.detail[o].jenistarif && (t += '<tr><td colspan="5">&nbsp;</td>'),
                        (t +=
                            '<tr>'+
                                '<td style="' + e + '" width="100px;"> <i>' + a.detail[o].tanggaltagih +'</i></td>'+
                                '<td style="' + e + '"> Nama</td>'+
                                '<td style="' + e + '"> jumlah</td>'+
                                '<td style="' + e + '"> @harga</td>'+
                                '<td style="' + e + '"> Subtotal</td>'+
                            '</tr>'
                        ),
                        (i = 0)
                    ),

                    // counting value
                    (c +=
                        (parseInt(a.detail[o].sewa, 10) + parseInt(a.detail[o].nonsewa, 10)) *
                        parseFloat(a.detail[o].jumlah) *
                        (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10))),
                    (h +=
                        "Diskon" == a.detail[o].jenistarif
                            ? 0
                            : (parseInt(a.detail[o].sewa, 10) + parseInt(a.detail[o].nonsewa, 10)) *
                              parseFloat(a.detail[o].jumlah) *
                              (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10))),
                    1 == a.detail[o].iskenapajak ? 
                        ((t +=
                            "<tr><td>" +
                            ++_ +
                            " " +
                            a.detail[o].nama +
                            (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10) > 1
                                ? " [" + convertToRupiah(parseInt(a.detail[o].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detail[o].kekuatanbarang, 10) + "]")
                                : "") +
                            '</td><td align="right" style="font-size:' +
                            o +
                            '">' +
                            hapus3digitkoma(a.detail[o].jumlah) +
                            '</td><td align="right">' +
                            convertToRupiah(parseInt(a.detail[o].jasaoperator, 10) * parseFloat(a.detail[o].jumlah) * (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10))) +
                            "</td></tr>"),
                        (t +=
                            '<tr><td style="font-size:11px;">' +
                            ++_ +
                            " Akomodasi " +
                            a.detail[o].nama +
                            (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10) > 1
                                ? " [" + convertToRupiah(parseInt(a.detail[o].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detail[o].kekuatanbarang, 10) + "]")
                                : "") +
                            '</td><td align="right" style="font-size:' +
                            o +
                            '">' +
                            hapus3digitkoma(a.detail[o].jumlah) +
                            '</td><td align="right">' +
                            convertToRupiah(parseInt(a.detail[o].akomodasi, 10) * parseFloat(a.detail[o].jumlah) * (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10))) +
                            "</td></tr>"))
                        : (t +=
                            "<tr><td>" +
                            ++_ +
                            " " +
                            a.detail[o].nama +
                            (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10) > 1
                                ? " [" + convertToRupiah(parseInt(a.detail[o].kekuatan, 10)) + "/" + convertToRupiah(parseInt(a.detail[o].kekuatanbarang, 10) + "]")
                                : "") +
                            '</td><td align="right" style="font-size:' +
                            o +
                            '">' +
                            hapus3digitkoma(a.detail[o].jumlah) +
                            '</td><td align="right">' +
                            convertToRupiah(
                                (parseInt(a.detail[o].sewa, 10) + parseInt(a.detail[o].nonsewa, 10)) *
                                    parseFloat(a.detail[o].jumlah) *
                                    (parseInt(a.detail[o].kekuatan, 10) / parseInt(a.detail[o].kekuatanbarang, 10))
                            ) +
                            "</td></tr>"),

                    // table content
                    // (t +=
                    //     "<tr>"+
                    //         "<td> </td>"+
                    //         "<td> " + ++i + ". " + a.detail[o].nama + "</td>"+
                    //         "<td> " + a.detail[o].jumlah + "</td>"+
                    //         "<td> " + a.detail[o].subtotal +/*convertToRupiah(a.detail[o].subtotal) +*/ "</td>"+
                    //         "<td> " + parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah), "." +/*convertToRupiah(parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah), ".") +*/"</td>"+
                    //     "</tr>"
                    // ),

                    // additional info
                    (l += parseInt(a.detail[o].subtotal) * parseFloat(a.detail[o].jumlah)),
                    (r = a.detail[o].jenistarif + "" + a.detail[o].tanggaltagih),
                    (n = a.detail[o].jenistarif);
                }

                // footer information
                (t += '<tr><td colspan="5">&nbsp;</td>'),
                (t += '<tr><td colspan="5">&nbsp;</td>'),
                (t +=   '<tr>'+
                            '<td colspan="4" style="' + e + '"> Total Biaya</td>'+
                            '<td style="' + e + '"> ' + l + '"</td>'+
                        '</tr>"'), //convertToRupiah(l, ".")
                (t += "</table>"),
                (t += "</div>"),
                    
                // the result
                // cetak_excel_langsung(a.infotagihan[0].norm, t, title);
                fungsi_cetaktopdf(t, "<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>");
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
    });

    // excel detail gabung
    $(document).on("click", "#cetakExcelVersiClaim2", function () {
        cetakvpasienClaim($(this).attr("alt"), "rajal", 3);
    });

    // excel invoice gabung
    $(document).on("click", "#cetakvpasienGabung", function () {
        cetakvpasienClaim($(this).attr("alt"), "invoiceGabung", );
    });

    // cetakvpasienralan => cetakvpasien($(this).attr("alt"), "rajal", 3);    
    // cetakvpasienranap => cetakvpasien($(this).attr("alt"), "rajalnap", 1);
    function cetakvpasienClaim(a, t, n, i = "") {
        startLoading();
        var e = a,
            r = ((t = t), 3 == n ? "rajal" : 4 == n ? "invoiceGabung" : ""),
            l = "",
            o = "",
            d = "",
            s = "",
            g = "",
            p = "",
            u = "",
            c = 0,
            h = 0,
            _ = 0;
        $.ajax({
            type: "POST",
            url: base_url + ("invoiceGabung" == t ? "cpelayanan/cetak_invoiceGabung" : "cpelayanan/cetak_excel_VersiClaim_getData"),
            data: { p: e, j: r, i: i, jns: 0 },
            dataType: "JSON",
            success: function (a) {
                var line = "border-bottom:1px solid #000; border-top:1px solid #000;",
                    r2 = "",
                    n = "";
                tgl_new = "";
                tgl = "";
                // console.log(a);
                
                var title = "Detail Pasien Versi GABUNG";
                        
                if ((stopLoading(), "invoiceGabung" == t)) return print_invoiceGabung(a), !0;                
                tgl = (a.detailtagihan[0].tanggal == '' ? a.infotagihan[0].waktutagih : a.detailtagihan[0].tanggal);
                (l = (g = "rajal" != t ? "width:19cm;" : "width:6cm;") + ("rajal" != t ? "font-size:normal;" : "font-size:small;")),
                    (o = "rajal" != t ? "font-size:normal;" : "font-size:x-small;"),
                    (d = "rajal" != t ? "20" : "7.6"),
                    (s = "rajal" != t ? "2" : "0.2"),
                    (p += '<div style="float: none;">'),
                    (p += '<table border="0" style="font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'),
                    (p += '<tr><td>NORM</td><td colspan="3">: ' + a.infotagihan[0].norm + '</td></tr>'),
                    (p += '<tr><td>Nama</td><td colspan="3">: ' + a.infotagihan[0].namalengkap + '</td></tr>'),
                    (p += '<tr><td colspan="5">&nbsp;</td></tr>'),
                    (p += '</table>'),
                    (p += '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;font-family: padding:1px;"Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">');

                    (p += '<tr><td colspan="5">&nbsp;</td>'),
                    (p += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;">' +
                        ' Rekap ' + a.detailtagihan[0].jenistarif +
                        "</td></tr>"),

                    (p +=
                        '<tr>' +
                        '<td align="left" style="' + line + '" width="100px;"> <i>' + (a.detailtagihan[0].tanggal == '' ? a.infotagihan[0].waktutagih : a.detailtagihan[0].tanggal) + '</i></td>' +
                        '<td align="left" style="' + line + '"> Nama</td>' +
                        '<td align="right" style="' + line + '"> jumlah</td>' +
                        '<td align="right" style="' + line + '"> @harga</td>' +
                        '<td align="right" style="' + line + '"> Subtotal</td>' +
                        '</tr>'
                    );

                for (x in a.detailtagihan)
                {  
                    if (u != "" && u != a.detailtagihan[x].jenistarif) {
                        (p += '<tr><td colspan="5">&nbsp;</td>'),
                        (p += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;">' +
                            ' Rekap ' + a.detailtagihan[x].jenistarif +
                            "</td></tr>");
                        (c = 0);
                    }
                     
                    tgl_new = (a.detailtagihan[x].tanggal == '' ? a.infotagihan[0].waktutagih : a.detailtagihan[x].tanggal);
                    if ((u != "" && u != a.detailtagihan[x].jenistarif) || (tgl != "" && tgl != tgl_new))
                    {
                        (p +=
                            '<tr>' +
                            '<td align="left" style="' + line + '" width="100px;"> <i>' + tgl_new + '</i></td>' +
                            '<td align="left" style="' + line + '"> Nama</td>' + '</td>' +
                            '<td align="right" style="' + line + '"> jumlah</td>' +
                            '<td align="right" style="' + line + '"> @harga</td>' +
                            '<td align="right" style="' + line + '"> Subtotal</td>' +
                            '</tr>'
                        ),
                        (_ = 0)
                    };
                    (tgl = tgl_new);

                    u = a.detailtagihan[x].jenistarif;
    
                        // counting value
                        (c +=
                            (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) *
                            parseFloat(a.detailtagihan[x].jumlah) *
                            (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                        (h +=
                            "Diskon" == a.detailtagihan[x].jenistarif
                                ? 0
                                : (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) *
                                parseFloat(a.detailtagihan[x].jumlah) *
                                (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))),
                        1 == a.detailtagihan[x].iskenapajak
                            ? ((p +=
                                "<tr><td style='text-align:right'>" + ++_ +"</td><td>" +
                                a.detailtagihan[x].nama +
                                (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1 ?
                                    " [" +
                                    parseInt(a.detailtagihan[x].kekuatan, 10) +
                                    "/" +
                                    parseInt(a.detailtagihan[x].kekuatanbarang, 10) +
                                        "]"
                                    :
                                    "") +
                                '</td><td align="right" style="font-size:' +
                                o +
                                '">' +
                                hapus3digitkoma(a.detailtagihan[x].jumlah) +
                                '</td><td align="right">' +
                                parseInt(a.detailtagihan[x].jasaoperator, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10))+
                                '</td><td align="right">' +
                                parseInt(a.detailtagihan[x].jumlah) * parseInt(a.detailtagihan[x].jasaoperator, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)) +
                                "</td></tr>"),
                                (p +=
                                    '<tr><td style= "text-align:right font-size:12px;">' + ++_ +'</td><td style="font-size:12px;">'+
                                    " Akomodasi " +
                                    a.detailtagihan[x].nama +
                                    (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1 ?
                                        " [" +
                                        parseInt(a.detailtagihan[x].kekuatan, 10) +
                                        "/" +
                                        parseInt(a.detailtagihan[x].kekuatanbarang, 10) +
                                            "]"
                                        :
                                        "") +
                                    '</td><td align="right" style="font-size:' +
                                    o +
                                    '">' +
                                    hapus3digitkoma(a.detailtagihan[x].jumlah) +
                                    '</td><td align="right">' +
                                    parseInt(a.detailtagihan[x].akomodasi, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)) +
                                    '</td><td align="right">' +
                                    parseInt(a.detailtagihan[x].jumlah) * parseInt(a.detailtagihan[x].akomodasi, 10) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)) +
                                    "</td></tr>"))
                            : (p +=
                                "<tr><td style='text-align:right'>" + ++_ +"</td><td>" +
                                a.detailtagihan[x].nama +
                                (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10) > 1 ?
                                    " [" +
                                    parseInt(a.detailtagihan[x].kekuatan, 10) +
                                    "/" +
                                    parseInt(a.detailtagihan[x].kekuatanbarang, 10) +
                                        "]"
                                    :
                                    "") +
                                '</td><td align="right" style="font-size:' +
                                o +
                                '">' +
                                hapus3digitkoma(a.detailtagihan[x].jumlah) +
                                '</td><td align="right">' +
                                (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) /* * parseFloat(a.detailtagihan[x].jumlah) */ * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)) +
                                '</td><td align="right">' +
                                // (parseInt(a.detailtagihan[x].jumlah)) * 
                                (parseInt(a.detailtagihan[x].sewa, 10) + parseInt(a.detailtagihan[x].nonsewa, 10)) * parseFloat(a.detailtagihan[x].jumlah) * (parseInt(a.detailtagihan[x].kekuatan, 10) / parseInt(a.detailtagihan[x].kekuatanbarang, 10)) +
                                "</td></tr>");
                }
               (p += '<tr><td colspan="5">&nbsp;</td>'),
                    (p += '<tr><td align="right" colspan="4">== TOTAL ==</td><td align="right">' + h + "</td></tr>"),
                    (p += "</table></div>"),
                    
                    // fungsi_cetaktopdf(p, "<style>@page {size:" + d + "cm 100%;margin:" + s + "cm;}</style>");
                    cetak_excel_langsung(a.infotagihan[0].norm, p, title);
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
    }
    