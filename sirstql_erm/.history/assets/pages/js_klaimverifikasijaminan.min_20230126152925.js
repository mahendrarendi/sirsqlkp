"use strict";
var dtverif='';
$(function(){
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    //loaddtpendaftaran();// tampilkan data pendaftaran sekarang/hari ini
    listdata();
});

function listdata(){
    dtverif = $('#listdata').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
   "ajax": {
       "data":{
           tglawal:function(){return $('#tglawal').val();},
           tglakhir:function(){return $('#tglakhir').val();} 
       },
   "url": base_url+'cadmission/dt_verifikasiklaim',
   "type": "POST"
   },"columnDefs": [{ "targets": [],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', qlstatuswarna(aData[10]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//reload data
$(document).on('click','#reload',function(){
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    $('input[type="search"]').val('').keyup();
    dtverif.state.clear();
    dtverif.ajax.reload();
});
//tampil data
$(document).on('click','#tampil',function(){
    dtverif.ajax.reload();
});
////status layak
//$(document).on('click','#statusklaimlayak',function(){
//    var idpendaftaran = $(this).attr('i');
//    var selectid = $(this).attr('sts');
//    var modalTitle = 'Status Klaim Layak';
//    var modalContent = '<form action="" id="klaimlayak">' +
//                            '<div class="form-group">' +
//                                '<label>Nominal Diterima</label>' +
//                                '<input type="text" name="nominal" class="form-control" placeholder="Input nominal klaim diterima" />'+
//                            '</div>'+
//                        '</form>';
//                $.confirm({
//                    title: modalTitle,
//                    content: modalContent,
//                    columnClass: 'small',
//                    buttons: {
//                        formSubmit: { //menu submit
//                            text: 'Simpan',
//                            btnClass: 'btn-blue',
//                            action: function () {
//                                if($('input[name="nominal"]').val()==0){
//                                    $.alert('Nominal Tidak Boleh Kosong.');
//                                    return false;
//                                }else{
//                                    $.ajax({
//                                        type: "POST", //tipe pengiriman data
//                                        url: base_url + 'cadmission/ubahstatusklaimlayak', //alamat controller yang dituju (di js base url otomatis)
//                                        data: {idp:idpendaftaran,nominal:$('input[name="nominal"]').val()}, //
//                                        dataType: "JSON", //tipe data yang dikirim
//                                        success: function(result) { //jika  berhasil
//                                            notif(result.status,result.message);
//                                            dtverif.ajax.reload();
//                                        },
//                                        error: function(result) { //jika error
//                                            fungsiPesanGagal(); // console.log(result.responseText);
//                                            return false;
//                                        }
//                                    });
//                                }
//                            }
//                        },
//                        formReset:{ //menu batal
//                            text: 'batal',
//                            btnClass: 'btn-danger'
//                        }
//                    },
//                    onContentReady: function () {
//                    var jc = this;
//                    this.$content.find('form').on('submit', function (e) {
//                        // if the user submits the form by pressing enter in the field.
//                        e.preventDefault();
//                        jc.$$formSubmit.trigger('click'); // reference the button and click it
//                    });
//                    }
//                });
//});
//edit status
$(document).on('click','#editstatus',function(){
    var idpendaftaran = $(this).attr('i');
    var selectid = $(this).attr('sts');
    var modalTitle = 'Ubah Status';
    var modalContent = '<form action="" id="VerifikasiBerkas">' +
                            '<div class="form-group">' +
                                '<label>Status Klaim</label>' +
                                '<select id="idstatusklaim" class="form-control"></select>'+
                            '</div>'+
                        '</form>';
                $.confirm({
                    title: modalTitle,
                    content: modalContent,
                    columnClass: 'small',
                    buttons: {
                        formSubmit: { //menu submit
                            text: 'Simpan',
                            btnClass: 'btn-blue',
                            action: function () {
                                    $.ajax({
                                        type: "POST", //tipe pengiriman data
                                        url: base_url + 'cadmission/ubahstatusberkasklaim', //alamat controller yang dituju (di js base url otomatis)
                                        data: {idp:idpendaftaran, ids:$('#idstatusklaim').val()}, //
                                        dataType: "JSON", //tipe data yang dikirim
                                        success: function(result) { //jika  berhasil
                                            notif(result.status,result.message);
                                            dtverif.ajax.reload();
                                        },
                                        error: function(result) { //jika error
                                            fungsiPesanGagal(); // console.log(result.responseText);
                                            return false;
                                        }
                                    });
                            }
                        },
                        formReset:{ //menu batal
                            text: 'batal',
                            btnClass: 'btn-danger'
                        }
                    },
                    onContentReady: function () {
                        $.ajax({url:base_url+"cmasterdata/get_combostatusklaim",type:"POST",dataType:"JSON", success: function(result){ editdataoption($('#idstatusklaim'),result,selectid); }, error: function() {} });
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                    }
                });
});

$(document).on("click","#verifDaftarPasienBpjs", function(){
    var idpendaftaran = $(this).attr("alt");
    var nobaris = $(this).attr("nobaris");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Yakin berkas sudah ditemukan.?',
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/pendaftaran_updateverifpasienbpjs",
                    type:"POST",
                    dataType:"JSON",
                    data:{id:idpendaftaran},
                    success:function(result){
                        notif(result.status, result.message);//panggil fungsi popover message
                        setTimeout(function(){window.location.reload(true);},2000);

                    },
                    error:function(result)
                    {
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    });
});
//
///////////FUNGSI VERIFIKASI PENDAFTARAN//////
//// $(document).on("click","#verifpasienbpjs", function(){
//function setVerifPendaftaran(id,status,no,stsprintverif){
//    var menuVerif='', mode=$('#mode'+no).val();
//    menuVerif='<a onclick="formInacbg('+id+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Formulir INA-CBG"><i class="fa fa-print"></i></a> ';
//    menuVerif+='<a onclick="resumepasien('+id+')" class="btn '+((stsprintverif=='1')?'bg-black':'btn-primary')+' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN"><i class="fa fa-print"></i></a> ';
//    if(mode!=='asuransi lain' && status!='1')
//    {
//        menuVerif+='<a onclick="setVerifPendaftaran('+id+',1,'+no+','+stsprintverif+')" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Verif BPJS"><i class="fa fa-check"></i></a>';
//        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
//    }
//    else if(mode=='asuransi lain' && status!='1')//selain itu atau jika pasien mempunyai jaminan selain jkn
//    {
//        menuVerif+='<a onclick="setVerifPendaftaran('+id+',1,'+no+','+stsprintverif+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Verif Asuransi Lain"><i class="fa fa-check"></i></a>';
//        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
//    }
//    else
//    {
//        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',2,'+no+','+stsprintverif+')" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Batal Verif"><i class="fa fa-warning"></i></a>';
//        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
//    }
//    if(status=='2' || status=='3')
//    {
//        $.ajax({
//            type: "POST",
//            url: base_url+'cadmission/updateverifpasienbpjs',
//            data:{id:id, status:status},
//            dataType: "JSON",
//            success: function(result) {
//                // deleteRow('row'+nobaris);
//                if(status=='2'){bg='#f3df96';}//selain itu atau gagal verif
//                else if(status=='3'){bg='#f0D8D8';}
//                $('#row'+no).css('background-color',bg);
//                $('#menu'+no).empty();
//                $('#menu'+no).html(menuVerif);
//                notif(result.status, result.message);
//            },
//            error: function(result) {
//                fungsiPesanGagal();
//                return false;
//            }
//        });
//    }
//    else
//    {
//    // FORM VERIFIKASI
//    var modalTitle = 'Verifikasi Pendaftaran Pasien';
//    var modalContent = '<form action="" id="Formverifpasienbpjs">' +
//                            '<input type="hidden" name="carabayar" class="form-control"/>' +
//                            '<input type="hidden" name="caradaftar" class="form-control"/>' +
//                            '<input type="hidden" name="idperson" class="form-control"/>' +
//                            '<input type="hidden" name="idpendaftaran" class="form-control"/>' +
//                            '<input type="hidden" name="nopesan" class="form-control"/>' +
//                            '<input type="hidden" name="unit" id="unitterpilih"/>'+
//                            '<div class="form-group">' +
//                                '<label class="label-control col-sm-3">Nama Pasien</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="nama" class="form-control" readonly/>' +
//                                '</div>'+
//                                '<div>&nbsp;</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group">' +
//                                '<label class="label-control col-sm-3">No.Identitas(KTP)</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="nik" placeholder="No Kartu Tanda Penduduk" class="form-control"/>' +
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group">' +
//                                '<label class="label-control col-sm-3">TanggalPeriksa</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" id="tanggalverif" onchange="cari_poliklinik(this.value)" name="tanggal" class="datepicker form-control"/>' +
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group">' +
//                                '<label class="label-control col-sm-3">PoliTujuan</label>' +
//                                '<div class="col-sm-8">' +
//                                    '<select id="poliklinik" name="politujuan" class="form-control select2" style="width:100%;"><option value="0">Pilih</option></select>'+
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group jkn">' +
//                                '<label class="label-control col-sm-3">No.JKN</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="nojkn" placeholder="No Jaminan Kesehatan Nasional" class="form-control"/>' +
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group jkn">' +
//                                '<label class="label-control col-sm-3">No.SEP</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="nosep" placeholder="No Surat Eligibilitas Peserta" class="form-control"/>' +
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group jkn">' +
//                                '<label class="label-control col-sm-3">No.Rujukan</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="norujukan" placeholder="No Rujukan " class="form-control"/>' +
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group skdp">' +
//                                '<label class="label-control col-sm-3">No.SKDP</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="noskdp" placeholder="No SKDP " class="form-control" readonly/>' +                                
//                                '</div>'+
//                            '</div>' +
//                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
//                            '<div class="form-group skdp">' +
//                                '<label class="label-control col-sm-3">No.Kontrol</label>' +
//                                '<div class="col-sm-8">' +
//                                '<input type="text" name="nokontrol" placeholder="No Kontrol " class="form-control" readonly/>' +                                
//                                '</div>'+
//                            '</div>' +
//                        '</form>';
//    $.confirm({
//        title: modalTitle,
//        content: modalContent,
//        columnClass: 'm',
//        buttons: {
//            formSubmit: {
//                text: 'Verif',
//                btnClass: 'btn-blue',
//                action: function () {
//                    if($('input[name="nik"]').val()==='') //jika no namapenanggung kosong
//                    {
//                        alert_empty('nik');
//                        return false;
//                    }
//                    else //selain itu
//                    {
//                        $.ajax({
//                            type: "POST",
//                            url: base_url+'cadmission/saveverifpasienbpjs',
//                            data: $("#Formverifpasienbpjs").serialize(),
//                            dataType: "JSON",
//                            success: function(result) {
//                                // deleteRow('row'+nobaris);
//                                $('#row'+no).css('background-color','#e0f0d8');
//                                $('#menu'+no).empty();
//                                $('#menu'+no).html(menuVerif);
//                                notif(result.status, result.message);
//                                if($('input[name="caradaftar"]').val()=='online' || $('input[name="noskdp"]').val()!==''){setTimeout(function(){cetakAntrianAnjungan(result.antrian, result.dokter,result.pasien);},500);}
//                                // pendaftaranListRow(id,nobaris);//tampil per baris
//                            },
//                            error: function(result) {
//                                console.log(result.responseText);
//                            }
//                        });
//                    }
//                }
//            },
//            formReset:{
//                text: 'Batal',
//                btnClass: 'btn-danger'
//            }
//        },
//        onContentReady: function () { 
//        //saat form dijalankan
//        // tambahkan kode yang diinginkan
//        getverifpasienbpjs(id);//ambil data verifikasi
//        $('[data-toggle="tooltip"]').tooltip();
//        $('.datepicker').datepicker({'autoclose':true,'format': "dd/mm/yyyy",'startDate':new Date(),'orientation':"bottom"});
//        $('.select2').select2();
//        // bind to events
//        var jc = this;
//        this.$content.find('form').on('submit', function (e) {
//            // if the user submits the form by pressing enter in the field.
//            e.preventDefault();
//            jc.$$formSubmit.trigger('click'); // reference the button and click it
//        });
//        }
//    });
//    }
//}
// fungsi ambil data pasien yang akan diverifikasi
function getverifpasienbpjs(idpendaftaran)
{
    $.ajax({
        type:"POST",
        dataType:"JSON",
        url:base_url+"cadmission/getverifpasienbpjs",
        data:{idpendaftaran:idpendaftaran},
        success:function(result){
            // jika jenis pembayaran mandiri
            $('input[name="nik"]').val(result.nik);
            $('input[name="idperson"]').val(result.idperson);
            $('input[name="unit"]').val(result.idjadwal);
            $('input[name="idpendaftaran"]').val(result.idpendaftaran);
            $('input[name="nopesan"]').val(result.nopesan);
            $('input[name="nama"]').val(result.namalengkap);
            $('input[name="carabayar"]').val(result.carabayar);
            $('input[name="caradaftar"]').val(result.caradaftar);
            $('input[name="noskdp"]').val(result.noskdpql);
            $('#tanggalverif').val(result.tanggal);
            cari_poliklinik($('#tanggalverif').val());
            // inputan JKN
            $('input[name="nojkn"]').val(result.nojkn);
            $('input[name="nosep"]').val();
            $('input[name="norujukan"]').val();
            // jika noskdpl tidak kosong atau pasien kunjungan kedua
            if(result.noskdpql=='0'){$('.skdp').hide();}
            else{$('.skdp').show();$('input[name="nokontrol"]').val(result.nokontrol);}
        },
        error:function(){

        }
    });
}

// cetak antrian pasien anjungan
function cetakAntrianAnjungan(antrian,dokter,pasien)
{
    var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                  <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                      '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ antrian['no'] +'</span></td></tr>'+
                      '<tr><td><span>'+ dokter['namadokter'] +'</span></td></tr>'+
                      '<tr><td><span>'+ antrian['namaloket'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['norm'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['namalengkap'] +'</span></td></tr>'+
                      '<tr><td><div>'+ pasien['alamat'] +'</td></tr>'+
                    '<tr><td></div>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
}
// cari jadwal poli
function cari_poliklinik(value) //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    $.ajax({
        type: "POST",
        url: 'cari_jadwalpoli',
        data:{date: ambiltanggal(value)},
        dataType: "JSON",
        success: function(result) {
            // console.log(result);
             edit_dropdown_poliklinik($('#poliklinik'),result,$('#unitterpilih').val());//menampilkan poliklinik
             $('#unitterpilih').val('');
        },
        error: function(result) {
            console.log(result.responseText);
        }
    });
}

function edit_dropdown_poliklinik(column,data,selected_data) //fungsi edit poliklinik
{
    // console.log(name);
    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(i in data)
        {
            select = select + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" '+ if_select(data[i].idjadwal,selected_data) +' >' + data[i].namaunit +' '+ data[i].namadokter +' ' +if_empty(data[i].tanggal)+' </option>';
        }
        column.html('<option value="0">Pilih</option>'+ select );
        $('.select2').select2();
    }   
}


/**
 * buat sep dari halaman Klaim Verifikasi Jaminan BPJS
 * saat menu buat sep di klaim verifikasi di klik
 */
 $(document).on('click','#addSep',function(){
    var norujukan     = $(this).attr('norujukan');
    var idpendaftaran = $(this).attr('idpendaftaran');
    var tglperiksa= $(this).attr('tglperiksa');
    var namapasien = $(this).attr('namapasien');
    window.location.href= base_url+"cbpjs/sep?tabActive=pembuatan_sep&buatsepotomatis=1&norujukan="+norujukan+"&idpendaftaran="+idpendaftaran+"&tglperiksa="+tglperiksa+"&namapasien="+namapasien;
})
/**end */



/**
 * buat rencana kontrol/inap dari halaman Klaim Verifikasi Jaminan BPJS
 * saat menu buat kontrol/inap di klaim verifikasi di klik
 */
 $(document).on('click','#addKontrol',function(){
    var nosep     = $(this).attr('nosep');
    var idpendaftaran = $(this).attr('idpendaftaran');
    window.location.href= base_url+"cbpjs/rencanakontrol?tabActive=rencana_kontrol&nosep="+nosep+"&idpendaftaran="+idpendaftaran;
})
/**end */

/** 
 * Data Klaim BPJS
 */
$(document).ready(function(){
    var tbl = null;
    function table_bulananbpjs(formdata){
        tbl = $('#listdatabulanan').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.24/i18n/Indonesian.json"
            },
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                //panggil method ajax list dengan ajax
                "url": base_url+ 'cadmission/list_databulananbpjs',
                "type": "POST",
                "dataType": "JSON",
                "data" : function(){
                    formdata
                }
            }
        });
    }

    button_tampil();
    function button_tampil(){
        $('body').on('submit','#frm-databulanan',function(e){
            e.preventDefault();

            let root = $(this);
            let formdata = root.serializeArray();
            console.log(formdata[0].name);
            // table_bulananbpjs(formdata);
            
            // $.ajax({
            //     url: base_url+ 'cadmission/list_databulananbpjs',
            //     data: formdata,
            //     type:'POST',
            //     // dataType:'JSON',
            //     success: function(response){
            //         console.log(response);
            //     },
            //     error:function(xhr){
            //         alert('Contact From Developer : '+ xhr.status);
            //         return false;
            //     }

            // });
        });
        
    }

});