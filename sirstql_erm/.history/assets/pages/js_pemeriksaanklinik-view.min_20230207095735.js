var norm = localStorage.getItem("norm"),
    idperiksa = localStorage.getItem("idperiksa"),
    idpendaftaran = localStorage.getItem("idp");
function cariBhpFarmasi(a) {
    a.select2({
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: "pemeriksaanklinik_caribhp",
            dataType: "json",
            delay: 100,
            cache: !1,
            data: function (a) {
                return { q: a.term, page: a.page || 1 };
            },
            processResults: function (a, t) {
                var n = t.page || 1;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.idbarang, text: a.namabarang };
                    }),
                    pagination: { more: 10 * n <= a[0].total_count },
                };
            },
        },
    });
}
function inputBhpFarmasi(a) {
    "" == $('textarea[name="keteranganobat"]').val()
        ? ($('textarea[name="keteranganobat"]').focus(), alert_empty("keterangan resep"))
        : $.ajax({
              type: "POST",
              url: "pemeriksaan_inputbhp",
              data: { x: a, i: idperiksa },
              dataType: "JSON",
              success: function (a) {
                  notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshBhp();
              },
              error: function (a) {
                  return fungsiPesanGagal(), !1;
              },
          });
}
function pemeriksaanklinikRefreshBhp() {
    $.ajax({
        type: "POST",
        url: "pemeriksaanklinik_refreshbhp",
        data: { i: idperiksa },
        dataType: "JSON",
        success: function (a) {
            a.penggunaan, (a = a.bhpperiksa);
            var t = "",
                n = 0,
                e = 0,
                i = 0,
                s = 0;
            for (n in ((jumlahambil = 0), a))
                (s = (s = a[n].total.toString()).slice(0, -3)),
                    (s = parseInt(s)),
                    (jumlahambil = (a[n].kekuatanperiksa / a[n].kekuatan) * a[n].jumlahpemakaian),
                    (i += parseInt(a[n].total)),
                    (t +=
                        '<tr id="listBhp' +
                        ++e +
                        '"><td>' +
                        e +
                        "</td><td>" +
                        a[n].namabarang +
                        "</td><td>" +
                        a[n].jumlahpemakaian +
                        "</td><td>" +
                        a[n].kekuatan +
                        "</td><td>" +
                        a[n].kekuatanperiksa +
                        "</td><td>" +
                        jumlahambil +
                        "</td><td>" +
                        convertToRupiah(s) +
                        "</td><td>" +
                        a[n].penggunaan +
                        "</td><td>" +
                        a[n].signa +
                        '</td><td><input type="hidden" id="idbarang_ralan' +
                        e +
                        '" value="' +
                        a[n].idbarang +
                        '"><a onclick="cetakBhpAturanpakaiRajal(' +
                        e +
                        ')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a></td>');
            $("#viewDataBhp").empty(),
                (t += '<tr class="bg bg-info"><td colspan="6">Subtotal</td><td colspan="1">' + convertToRupiah(i) + '</td><td colspan="2"></td></tr>'),
                (t += '<tr><td colspan="8"> &nbsp;</td></tr><tr class="bg bg-info"><td colspan="6">Rekap Total</td><td colspan="1" id="totalPemeriksaan"></td><td colspan="2"></td></tr>'),
                $("#viewDataBhp").html(t),
                $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function editBhpGrup(a) {
    settingBhp($("#grup" + a).val(), "grup", $("#idbarangpemeriksaan" + a).val(), $("#idbarang" + a).val());
}
function editBhpJumlah(a) {
    settingBhp($("#jumlah" + a).val(), "jumlah", $("#idbarangpemeriksaan" + a).val(), $("#harga" + a).val());
}
function editBhpSetHarga(a) {
    settingBhp($("#harga" + a).val(), "harga", $("#idbarangpemeriksaan" + a).val(), $("#idbarang" + a).val());
}
function settingBhp(a, t, n, e) {
    $.ajax({
        url: "pemeriksaan_settbhp",
        type: "post",
        dataType: "json",
        data: { d: a, type: t, x: n, y: e },
        success: function (a) {
            if ("success" != a.status) return notif(a.status, a.message), !1;
            notif(a.status, a.message), pemeriksaanklinikRefreshBhp();
        },
        error: function (a) {
            return notif(a.status, a.message), !1;
        },
    });
}
function hapusBhp(a) {
    $.confirm({
        icon: "fa fa-question",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Confirmation!",
        content: "Delete data?",
        buttons: {
            confirm: function () {
                settingBhp("", "del", $("#idbarangpemeriksaan" + a).val(), "");
            },
            cancel: function () {},
        },
    });
}
function pilihvitalsign(a, t) {
    var n = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahvitalsign",
        data: { x: a, y: n, ispaket: t },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshVitalsign(n);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshVitalsign(a) {
    var t = 0,
        n = "",
        e = "";
    $("#listVitalsign").empty(),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_listvitalsign",
            data: { x: a, y: idperiksa },
            dataType: "JSON",
            success: function (a) {
                for (t in a) {
                    (e = i != a[t].namapaketpemeriksaan ? '<tr height="30px"><td class="bg-warning" colspan="5">' + a[t].namapaketpemeriksaan + ' </td><td class="bg-warning"></td><tr>' : ""),
                        (n +=
                            e +
                            "<tr><td>" +
                            a[t].icd +
                            " - " +
                            a[t].namaicd +
                            '</td><td> <input type="hidden" name="icdvitalsign[]" value="' +
                            a[t].icd +
                            '"/><input type="hidden" name="istextvitalsign[]" value="' +
                            a[t].istext +
                            '"/><input readonly type="text" class="form-control" value="' +
                            (null == a[t].nilai ? "" : a[t].nilai) +
                            '" placeholder="Input nilai" name="nilaivitalsign[]" size="1" /></td><td>' +
                            (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                            "</td><td>" +
                            (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                            ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                            "</td><td>" +
                            a[t].satuan +
                            "</td></tr>");
                    var i = a[t].namapaketpemeriksaan;
                }
                $("#listVitalsign").html(n), $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function hapusvitalsign(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapusvitalsign",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            console.log(a), notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshVitalsign(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihdiagnosa(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahdiagnosa",
        data: { x: a, y: t },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshDiagnosa(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshDiagnosa(a, t) {
    var n = 0,
        e = "";
    $("#listDiagnosa").empty(),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_listdiagnosa",
            data: { x: a, idicd: t },
            dataType: "JSON",
            success: function (a) {
                for (n in a) e += "<tr><td>" + a[n].icd + "</td><td>" + a[n].namaicd + "</td><td>" + a[n].aliasicd + "</td><td>" + a[n].icdlevel + "</td></tr>";
                $("#listDiagnosa" + t).html(e), $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function hapusdiagnosa(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapusdiagnosa",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshDiagnosa(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihtindakan(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahtindakan",
        data: { x: a, y: t },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshTindakan(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshTindakan(a) {
    var t = 0,
        n = "",
        e = 0;
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listtindakan",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            for (t in a.tindakan)
                (e += parseInt(a.tindakan[t].total)),
                    (n +=
                        "<tr>\n                <td>" +
                        a.tindakan[t].icd +
                        "</td>\n                <td>" +
                        a.tindakan[t].namaicd +
                        "</td>\n                <td>" +
                        a.tindakan[t].dokterpenerimajm +
                        "</td>\n                <td>" +
                        a.tindakan[t].jumlah +
                        "</td>\n                <td>" +
                        a.tindakan[t].total +
                        "</td></tr>");
            $("#listTindakan").empty(),
                (n += '<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">' + convertToRupiah(e) + "</td></tr>"),
                $("#listTindakan").html(n),
                $("#totalPemeriksaan").empty(),
                $("#totalPemeriksaan").html(convertToRupiah(bulatkanRatusan(parseInt(a.total[0].totalseluruh1) + parseInt(a.total[0].totalseluruh2)))),
                $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapustindakan(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapustindakan",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            console.log(a), notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshTindakan(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihradiologi(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahradiologi",
        data: { x: a, y: t },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshRadiologi(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshRadiologi(a) {
    var t = 0,
        n = "",
        e = 0;
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listradiologi",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            for (t in a) (e += parseInt(a[t].total)), (n += "<tr><td>" + a[t].icd + "</td><td>" + a[t].namaicd + "</td><td>" + a[t].total + "</td></tr>");
            $("#listRadiologi").empty(), (n += '<tr class="bg bg-info"><td colspan="2">Subtotal</td><td colspan="2">' + convertToRupiah(e) + "</td></tr>"), $("#listRadiologi").html(n), $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapusradiologi(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapusradiologi",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshRadiologi(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihlaboratorium(a, t) {
    var n = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahlaboratorium",
        data: { x: a, y: n, ispaket: t },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshLaboratorium(n);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshLaboratorium(a) {
    var t = 0,
        n = "",
        e = "",
        i = 0,
        s = "";
    $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listlaboratorium",
        data: { x: a, y: idperiksa },
        dataType: "JSON",
        success: function (a) {
            for (t in a)
                if ("4" == a[t].idjenisicd2) {
                    a[t].idgrup != r && null != r && ((n += '<tr ><td colspan="7" style="padding-left:15px;">' + s + "</td></tr>"), (s = "")),
                        (e =
                            l == a[t].namapaketpemeriksaan && o == a[t].gruppaketperiksa
                                ? ""
                                : null == a[t].namapaketpemeriksaan
                                ? ""
                                : '<tr height="30px" ' +
                                  (a[t].idgrup == a[t].idpaketpemeriksaan ? 'class="bg-info"' : "") +
                                  '><td colspan="6" class="text-bold">' +
                                  a[t].namapaketpemeriksaan +
                                  "</td><td>" +
                                  (1 == a[t].isubahwaktuhasil ? '<a class="btn btn-warning btn-xs"  id="ubahwaktuhasil"><i class="fa fa-clock-o"></i> Ubah Waktu Hasil</a>' : "") +
                                  "</td></tr>"),
                        (i += parseInt(a[t].total) - parseInt(a[t].potongantagihan)),
                        (n +=
                            e +
                            (null == a[t].icd
                                ? ""
                                : "<tr " +
                                  (null !== a[t].idgrup ? "" : 'class="bg-warning"') +
                                  "><td> &nbsp;&nbsp;&nbsp;" +
                                  a[t].icd +
                                  " - " +
                                  a[t].namaicd +
                                  '</td><td><input type="hidden" name="icd[]" value="' +
                                  a[t].icd +
                                  '"/><input type="hidden" id="istypehasil' +
                                  a[t].idhasilpemeriksaan +
                                  '" value="' +
                                  a[t].istext +
                                  '"/>' +
                                  a[t].nilai +
                                  "</td><td>" +
                                  (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                                  "</td><td>" +
                                  (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                                  ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                                  "</td><td>" +
                                  a[t].satuan +
                                  "</td><td>" +
                                  (null == a[t].idgrup ? convertToRupiah(a[t].total) : "") +
                                  "</td><td>" +
                                  (null == a[t].idgrup ? convertToRupiah(a[t].potongantagihan) : "") +
                                  "</td><td></td></tr>"));
                    a[t].idpaketpemeriksaan;
                    null != a[t].keteranganhasil && a[t].keteranganhasil != s && (s = a[t].keteranganhasil);
                    var r = a[t].idgrup,
                        l = a[t].namapaketpemeriksaan,
                        o = a[t].gruppaketperiksa;
                }
            (n += '<tr ><td colspan="7" style="padding-left:15px;">' + s + "</td></tr>"),
                $("#listLaboratorium").empty(),
                $("#listLaboratorium").html(n + '<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">' + convertToRupiah(i) + "</td></tr>"),
                $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapuslaboratorium(a) {
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapuslaboratorium",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            console.log(a), notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshLaboratorium(t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapuspaketparent(a, t) {
    var n = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapuspaketpemeriksaan",
        data: { x: a, y: n },
        dataType: "JSON",
        success: function (a) {
            console.log(a), notif(a.status, a.message), "success" == a.status && ("laboratorium" == t ? pemeriksaanRefreshLaboratorium(n) : pemeriksaanRefreshVitalsign(n));
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function diagnosa_or_tindakan(a, t) {
    a.select2({
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: "pemeriksaanklinik_diagnosa",
            dataType: "json",
            delay: 150,
            cache: !1,
            data: function (a) {
                return { q: a.term, jenisicd: t, page: a.page || 1 };
            },
            processResults: function (a, t) {
                var n = t.page || 1;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.icd, text: a.icd + " | " + a.namaicd + ("" != a.aliasicd ? " / " : "") + a.aliasicd };
                    }),
                    pagination: { more: 10 * n <= a[0].total_count },
                };
            },
        },
    });
}
function pilihJenisIcd(a) {
    pilihPaketDiagnosa($('select[name="paket"]'), a);
}
function singleDiagnosa(a) {
    $.ajax({
        type: "POST",
        url: "diagnosa_addsinglediagnosa",
        data: { x: a, y: $('input[name="idpendaftaran"]').val() },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function paketDiagnosa(a) {
    $.ajax({
        type: "POST",
        url: "diagnosa_addpaketdiagnosa",
        data: { x: a, y: $('input[name="idpendaftaran"]').val() },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihSingleDiagnosa(a, t) {
    a.select2({
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: "pemeriksaanklinik_cariicd",
            dataType: "json",
            delay: 150,
            cache: !1,
            data: function (a) {
                return { q: a.term, icd: t, page: a.page || 1 };
            },
            processResults: function (a, t) {
                var n = t.page || 1;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.icd, text: a.namaicd + ("" != a.aliasicd ? " >>> " : "") + a.aliasicd };
                    }),
                    pagination: { more: 10 * n <= a[0].total_count },
                };
            },
        },
    });
}
function pilihPaketDiagnosa(a, t) {
    $.ajax({
        url: "diagnosa_pilihpaket",
        type: "POST",
        dataType: "JSON",
        data: { x: t },
        success: function (n) {
            var e = '<option value="0" >Pilih</option>';
            for (i in (a.empty(), n)) e = e + '<option value="' + n[i].idpaketpemeriksaan + '" >' + n[i].namapaketpemeriksaan + "</option>";
            a.html(e), $(".select2").select2(), pilihSingleDiagnosa($('select[name="single"]'), t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanklinikDetailPasien() {
    startLoading(),
        $.ajax({
            type: "POST",
            url: "pemeriksaanklinik_caridetailpasien",
            data: { norm: norm, i: idperiksa },
            dataType: "JSON",
            success: function (a) {
                stopLoading(),
                    pemeriksaan_inputAnamnesa(a.grupjadwal),
                    $('input[name="dokter"]').val(a.periksa.namalengkap),
                    $('textarea[name="alergiobat"]').val(a.pasien.alergi),
                    $('textarea[name="diagnosa"]').val(a.periksa.diagnosa),
                    $('textarea[name="keterangan"]').val(a.periksa.keterangan),
                    $('textarea[name="rekomendasi"]').val(a.periksa.rekomendasi),
                    $('textarea[name="keteranganradiologi"]').val(a.pasien.keteranganradiologi),
                    $('textarea[name="saranradiologi"]').val(a.pasien.saranradiologi),
                    a.keterangan_ekokardiografi.length > 0 &&
                        (null == a.pasien.keteranganekokardiografi
                            ? $('textarea[name="keteranganekokardiografi"]').val(a.keterangan_ekokardiografi[0].keterangan)
                            : $('textarea[name="keteranganekokardiografi"]').val(a.pasien.keteranganekokardiografi)),
                    $('textarea[name="keteranganobat"]').val(a.pasien.keteranganobat),
                    $('textarea[name="keteranganlaboratorium"]').val(a.pasien.keteranganlaboratorium),
                    $("#kondisikeluar").val(a.pasien.kondisikeluar);
                    
                    /**
                     * [+] SKDP Tanggal
                     *  
                     **/ 
                    if( a.pasien.statuspulang == "SKDP" ){
                        // let = button_formskdp = `<a id="pemeriksaanklinikBuatSKDP" nobaris="`+parseInt('0')+`" alt2="`+id_pendaftaran+`" alt3="`+id_pemeriksaan+`" class=" btn btn-danger".'><i class="fa fa-file-text"></i> Formulir SKDP</a>`;
                        // if( cekMasaSkdp().code == '200' ){
                        //     $('#statuspasienpulang').after('<div id="group_form_skdp" style="margin-top: 20px;">'+button_formskdp+'</div>');
                        // }else{
                        //     $('#statuspasienpulang').after('<div id="group_form_skdp" style="border-radius:4px;margin-top: 20px;background:red;color:white;padding:4px;">'+cekMasaSkdp().msg+'</div>');
                        // }
                        // ajax_ceksdkp();
                        console.log(a);
                        // let html = `<div class="group-ql"><label style="margin-top: 10px;">Tanggal Kontrol Berikutnya</label> <input value="`+a.tglskdpberikutnya+`" type="text" readonly placeholder="Pilih Tanggal" autocomplate="off" class="dateql form-control tglkunjuanberikutnya" name="tglskdpberikutnya" required disabled></div>`;
                        // $('#statuspasienpulang').after(html);
                    }
                    /** end skdp tanggal */

                    $("#statuspasienpulang").val(a.pasien.statuspulang),
                    viewIdentitasPasien(a.pasien, a.penanggung),
                    $('input[name="idpendaftaran"]').val(a.periksa.idpendaftaran),
                    $('input[name="norm"]').val(a.pasien.norm),
                    pemeriksaanRefreshDiagnosa(a.periksa.idpendaftaran, 10),
                    pemeriksaanRefreshDiagnosa(a.periksa.idpendaftaran, 9),
                    pemeriksaanRefreshTindakan(a.periksa.idpendaftaran),
                    pemeriksaanRefreshRadiologi(a.periksa.idpendaftaran),
                    tampilHasilRadiografi(a.periksa.idpendaftaran, "view"),
                    pemeriksaanRefreshLaboratorium(a.periksa.idpendaftaran),
                    pemeriksaanRefreshVitalsign(a.periksa.idpendaftaran),
                    pemeriksaanRefreshEkokardiografi(a.periksa.idpendaftaran),
                    $('input[name="idunit"]').val(a.periksa.idunit);
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaan_inputAnamnesa(a) {
    var t = "";
    for (x in a) {
        var n = 1 == a.length ? "" : a[x].namaunit;
        (t += '<input id="idanamnesa' + x + '" type="hidden" name="grupanamnesa[]" value="' + a[x].idpemeriksaan + '">'),
            (t +=
                '<div class="form-group"><label for="" class="col-sm-2 control-label">Data Subyektif ' +
                n +
                '</label><div class="col-sm-9"><textarea id="namaanamnesa' +
                x +
                '" disabled class="form-control textarea" name="anamnesa' +
                a[x].idpemeriksaan +
                '" placeholder="Anamnesa ' +
                n +
                '" rows="4">' +
                a[x].anamnesa +
                "</textarea></div></div>");
    }
    $("#pemeriksaanInputAnamnesa").empty(), $("#pemeriksaanInputAnamnesa").html(t), $(".textarea").wysihtml5({ toolbar: !1 });
}
function viewIdentitasPasien(a, t) {
    var n,
        e = "";
    for (var i in t) e += "<tr><td>" + t[i].namalengkap + "</td><td>" + t[i].hubungan + "</td><td>" + t[i].notelpon + "</td></tr>";
    var s = '<table class="table table-bordered"><tr><th>Nama Penanggung</th><th>Hubungan </th><th> No.Telp</th></tr>' + e + "</table>";
    n =
        '<div class="bg-info"><div class="box-body box-profile"><div class="login-logo"><b class="text-yellow"><i class="fa ' +
        ("laki-laki" == a.jeniskelamin ? "fa-male" : "fa-female") +
        ' fa-2x"></i></b></div><h3 class="profile-username text-center">' +
        a.namalengkap +
        '</h3><p class="text-muted text-center">' +
        a.ispasienlama +
        '</p><h5 class="text-muted text-center">Usia ' +
        ambilusia(a.tanggallahir, a.tglsekarang) +
        " </h5></div></div>";
    var r;
    (r =
        '<div class="bg-info"><div class="box-body box-profile" ><table class="table table-striped" ><tbody><tr><td><strong><i class="fa fa-circle-o margin-r-5"></i>NIK </strong></td><td class="text-muted">: &nbsp; ' +
        a.nik +
        '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td><td class="text-muted">: &nbsp; ' +
        a.norm +
        '</td></tr><tr><td><strong><i class="fa fa-calendar margin-r-5"></i>Tanggal Lahir</strong></td><td class="text-muted">: &nbsp;' +
        a.tanggallahir +
        '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td><td class="text-muted">: &nbsp; ' +
        a.nojkn +
        '</td></tr><tr><td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td><td class="text-muted">: &nbsp; ' +
        a.jeniskelamin +
        '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor SEP </strong></td><td class="text-muted">: &nbsp; ' +
        a.nosep +
        '</td></tr><tr><td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td><td class="text-muted">: &nbsp; ' +
        a.golongandarah +
        " " +
        a.rh +
        ' </td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RUJUKAN </strong></td><td class="text-muted">: &nbsp; ' +
        a.norujukan +
        '</td></tr><tr><td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td><td class="text-muted">: &nbsp; ' +
        a.pendidikan +
        "</td>" +
        ("0000-00-00" == a.tanggalrujukan || "" == a.tanggalrujukan
            ? '<td colspan="2"></td>'
            : '<td class="bg bg-red"><strong><i class="fa fa-circle-o margin-r-5"></i>Rujukan BPJS Selanjutnya</strong></td><td class="bg bg-red text-muted">: &nbsp; ' + getNextDate(a.tanggalrujukan, 90) + "</td>") +
        '</tr><tr><td><strong><i class="fa fa-map-marker margin-r-5"></i> Agama</strong></td><td class="text-muted" >: &nbsp;' +
        a.agama +
        '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Penjamin </strong></td><td class="text-muted">: &nbsp; ' +
        a.carabayar +
        '</td></tr><tr><td><strong><i class="fa fa-circle-o margin-r-5"></i>Alamat </strong></td><td class="text-muted" width="250px">: &nbsp; ' +
        a.alamat +
        '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ayah </strong></td><td class="text-muted">: &nbsp; ' +
        a.ayah +
        '</td></tr><tr><td colspan="2"><strong><i class="fa fa-circle-o margin-r-5"></i>Penanggung </strong></td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ibu </strong></td><td class="text-muted">: &nbsp; ' +
        a.ibu +
        '</td></tr><tr><td colspan="2">' +
        s +
        "</td></tr></tbody></table></div></div>"),
        $("#pemeriksaanklinik_profile").html(n),
        $("#pemeriksaanklinik_detailprofile").html(r);
}
function updateBiayahasilperiksa(a, t) {
    var n = a,
        e =
            '<form action="" id="Formubahhargapaket"><div class="form-group"><input type="hidden" name="id" value="' +
            n +
            '" class="form-control"/><div class="col-md-3"><label>Jasa Operator</label><input type="text" name="jasaoperator" class="form-control"/></select></div><div class="col-md-3"><label>Nakes</label><input type="text" name="nakes" class="form-control"/></select></div><div class="col-md-3"><label>Jasars</label><input type="text" name="jasars" class="form-control"/></select></div><div class="col-md-3"><label>Bhp</label><input type="text" name="bhp" class="form-control"/></select></div><div class="col-md-3"><label>Akomodasi</label><input type="text" name="akomodasi" class="form-control"/></select></div><div class="col-md-3"><label>Margin</label><input type="text" name="margin" class="form-control"/></select></div><div class="col-md-3"><label>Sewa</label><input type="text" name="sewa" class="form-control"/></select></div></div></form>';
    $.confirm({
        title: "Ubah Biaya",
        content: e,
        columnClass: "medium",
        buttons: {
            formSubmit: {
                text: "Update",
                btnClass: "btn-blue",
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "savepaketygdiubah",
                        data: $("#Formubahhargapaket").serialize(),
                        dataType: "JSON",
                        success: function (a) {
                            notif(a.status, a.message),
                                "success" == a.status &&
                                    ("Tindakan" == t
                                        ? pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val())
                                        : "Radiologi" == t
                                        ? pemeriksaanRefreshRadiologi($('input[name="idpendaftaran"]').val())
                                        : pemeriksaanRefreshLaboratorium($('input[name="idpendaftaran"]').val()));
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });
                },
            },
            formReset: { text: "batal", btnClass: "btn-danger" },
        },
        onContentReady: function () {
            tampilhargayangdiubah(n);
            var a = this;
            this.$content.find("form").on("submit", function (t) {
                t.preventDefault(), a.$$formSubmit.trigger("click");
            });
        },
    });
}
function tampilhargayangdiubah(a) {
    $.ajax({
        type: "POST",
        url: "tampilhargayangdiubah",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            $('input[name="jasaoperator"]').val(a.jasaoperator),
                $('input[name="nakes"]').val(a.nakes),
                $('input[name="jasars"]').val(a.jasars),
                $('input[name="bhp"]').val(a.bhp),
                $('input[name="akomodasi"]').val(a.akomodasi),
                $('input[name="margin"]').val(a.margin),
                $('input[name="sewa"]').val(a.sewa);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapusPaketPeriksa(a) {
    var t = $('input[name="idpendaftaran"]').val(),
        n = a;
    $.ajax({
        type: "POST",
        url: "delete_paket_periksa",
        data: { x: t, y: n },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function deleteHasilPeriksa(a) {
    var t = $('input[name="idpendaftaran"]').val(),
        n = $("#deleteHasilPeriksa" + a).val();
    $.ajax({
        type: "POST",
        url: "delete_hasil_periksa",
        data: { x: t, y: n },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaan_tampilpasien() {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tampilpasien",
        data: { x: norm, y: idperiksa },
        dataType: "JSON",
        success: function (a) {
            $("#labNormPasien").html(": " + a.p.norm),
                $("#labNamaPasien").html(": " + a.p.namalengkap),
                $("#labPengirim").html(": " + a.per.titeldepan + " " + a.per.namalengkap + " " + a.per.titelbelakang),
                $("#labKelaminPasien").html(": " + a.p.jeniskelamin),
                $("#labTgllahirPasien").html(": " + a.p.tanggallahir);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaandetailTampilPasien() {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_detail_tampil_pasien",
        data: { x: idperiksa },
        dataType: "JSON",
        success: function (a) {
            pemeriksaandetailTampilPasienListData(a);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaandetailTampilPasienListData(a) {
    var t =
        '<div class="col-sm-6"><table class="table table-striped" ><tr><th>Nama</th><th style="font-weight:normal;">: ' +
        a.namalengkap +
        '</th></tr><tr><th>No.RM</th><th style="font-weight:normal;">: ' +
        a.norm +
        '</th></tr><tr><th>Alamat</th><th style="font-weight:normal;">: ' +
        a.alamat +
        '</th></tr><tr><th>Telpon</th><th style="font-weight:normal;">: ' +
        a.telponpasien +
        '</th></tr><tr><th>Kelamin</th><th style="font-weight:normal;">: ' +
        a.jeniskelamin +
        '</th></tr><tr><th>TanggalLahir</th><th style="font-weight:normal;">: ' +
        a.tanggallahir +
        '</th></tr><tr><th>Agama</th><th style="font-weight:normal;">: ' +
        a.agama +
        '</th></tr><tr><th>Status</th><th style="font-weight:normal;">: ' +
        a.statusmenikah +
        '</th></tr><tr><th>Golongan Darah</th><th style="font-weight:normal;">: ' +
        a.golongandarah +
        '</th></tr><tr><th>Rhesus</th><th style="font-weight:normal;">: ' +
        a.rh +
        '</th></tr><tr><th>Alergi</th><th style="font-weight:normal;">: ' +
        a.alergi +
        '</th></tr></table></div><div class="col-sm-6"><table class="table table-striped" ><tr><th>Penanggung</th><th style="font-weight:normal;">: ' +
        a.namapenjawab +
        '</th></tr><tr><th>Alamat</th><th style="font-weight:normal;">: ' +
        a.alamatpenjawab +
        '</th></tr><tr><th>No.Telpon</th><th style="font-weight:normal;">: ' +
        a.notelpon +
        '</th></tr><tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr><tr><th>Klinik</th><th style="font-weight:normal;">: ' +
        a.namaunit +
        '</th></tr><tr><th>No.Antri</th><th style="font-weight:normal;">: ' +
        a.noantrian +
        '</th></tr><tr><th>Dokter</th><th style="font-weight:normal;">: ' +
        a.titeldepan +
        " " +
        a.namadokter +
        " " +
        a.titelbelakang +
        '</th></tr><tr><th>Carabayar</th><th style="font-weight:normal;">: ' +
        a.carabayar +
        '</th></tr><tr><th>No.SEP</th><th style="font-weight:normal;">: ' +
        a.nosep +
        "</th></tr></table></div>";
    $.alert({ title: '<div class="col-sm-12 center">Detail Pasien</div>', content: t, columnClass: "xl" });
}
function pemeriksaanRefreshEkokardiografi(a) {
    var t = 0,
        n = "",
        e = "",
        i = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listekokardiografi",
        data: { x: a, y: idperiksa, modelist: i },
        dataType: "JSON",
        success: function (a) {
            for (t in a) {
                (e =
                    i == a[t].namapaketpemeriksaan && s == a[t].gruppaketperiksa
                        ? ""
                        : null == a[t].namapaketpemeriksaan
                        ? ""
                        : '<tr height="30px" ' +
                          (a[t].idgrup == a[t].idpaketpemeriksaan ? 'class="bg-info"' : "") +
                          '><td colspan="3" class="text-bold">' +
                          a[t].namapaketpemeriksaan +
                          '</td><td class="">' +
                          (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].total : "") +
                          "</td></tr>"),
                    parseInt(a[t].total) - parseInt(a[t].potongantagihan),
                    (n +=
                        e +
                        (null == a[t].icd
                            ? ""
                            : "<tr " +
                              (null !== a[t].idgrup ? "" : 'class=""') +
                              "><td> &nbsp;&nbsp;&nbsp;" +
                              a[t].icd +
                              " - " +
                              a[t].namaicd +
                              "</td><td>" +
                              a[t].nilai +
                              "</td><td>" +
                              (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                              ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                              " " +
                              a[t].satuan +
                              "</td></tr>"));
                a[t].idpaketpemeriksaan, a[t].idgrup;
                var i = a[t].namapaketpemeriksaan,
                    s = a[t].gruppaketperiksa;
            }
            $("#listEkokardiografi").empty(), $("#listEkokardiografi").html(n), $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
localStorage.setItem("jenisrawat", "rajal"),
    $(function () {
        $(".select2").select2(),
            pemeriksaanklinikDetailPasien(),
            $('input[name="idperiksa"]').val(idperiksa),
            (peranperan.includes("12") && 1 == peranperan.length) || (pemeriksaanklinikRefreshBhp(), cariBhpFarmasi($('select[name="caribhp"]')));
    }),
    $(document).on("click", "#ubahwaktuhasil", function () {
        $.confirm({
            title: "Ubah Waktu Hasil Laboratorium",
            content:
                '<form action="" id="formUbah"><div class="form-group"><label>Waktu Proses</label><input type="text" id="txtwaktuproses" name="txtwaktuproses" class="form-control"/></div><div class="form-group"><label>Waktu Selesai</label><input type="text"id="txtwaktuselesai" name="txtwaktuselesai"  class="form-control"/></div></form>',
            closeIcon: !0,
            type: "orange",
            columnClass: "small",
            buttons: {
                formSubmit: {
                    text: "simpan",
                    btnClass: "btn-blue",
                    action: function () {
                        $.ajax({
                            type: "POST",
                            url: base_url + "cpelayanan/set_waktuhasillaborat",
                            data: { idpemeriksaan: idperiksa, ok: $("#txtwaktuselesai").val(), proses: $("#txtwaktuproses").val() },
                            dataType: "JSON",
                            success: function (a) {
                                notif(a.status, a.message);
                            },
                            error: function (a) {
                                return fungsiPesanGagal(), !1;
                            },
                        });
                    },
                },
                formReset: { text: "batal", btnClass: "btn-danger" },
            },
            onContentReady: function () {
                $.ajax({
                    type: "POST",
                    url: base_url + "cpelayanan/get_waktuhasillaborat",
                    data: { idpemeriksaan: idperiksa },
                    dataType: "JSON",
                    success: function (a) {
                        $("#txtwaktuproses").val(a.proses), $("#txtwaktuselesai").val(a.selesai);
                    },
                    error: function (a) {
                        return fungsiPesanGagal(), !1;
                    },
                });
                var a = this;
                this.$content.find("form").on("submit", function (t) {
                    t.preventDefault(), a.$$formSubmit.trigger("click");
                });
            },
        });
    });
