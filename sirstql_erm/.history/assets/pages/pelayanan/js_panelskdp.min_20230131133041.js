"use strict";
var panelskdp = "";
function listdata() {
    panelskdp = $("#panelskdp").DataTable({
        processing: !0,
        serverSide: !0,
        lengthChange: !1,
        searching: !1,
        stateSave: !0,
        order: [],
        ajax: {
            data: {
                tanggal: function () {
                    return $('input[name="tanggal"]').val();
                },
                cari: function () {
                    return $('input[name="cari"]').val();
                },
            },
            url: base_url + "cpelayanan/dtskdp",
            type: "POST",
        },
        columnDefs: [{ targets: [7, 8, 9], orderable: !1 }],
        fnCreatedRow: function (a, n, t) {},
        drawCallback: function (a, n) {},
    });
}
function refresh() {
    setdate(), $('input[type="search"]').val("").keyup(), panelskdp.state.clear(), panelskdp.ajax.reload();
}
function cari() {
    panelskdp.ajax.reload();
}
function setdate() {
    $('input[name="tanggal"]').datepicker({ autoclose: !0, format: "yyyy-mm-dd", orientation: "bottom" }).datepicker("setDate", "now");
}
$(function () {
    setdate(), listdata();
}),
    $(document).on("click", "#ubahskdp", function () {
        var b = $(this).attr("no");
        var a = $(this).attr('noskdp');
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Ubah SKDP",
            content: `<div class="form-group">
                <label>No SKDP</label>
                <input type="input" required class="form-control" name="noskdp" value="`+b+`" placeholder="input No.SKDP" />
            </div>
            <div class="form-group">
            <label>No Kontrol</label>
                <input type="input" required class="form-control" name="nokontrol" value="`+a+`" placeholder="input No Kontrol" />
            </div>`,
            buttons: {
                simpan: function () {

                    if( $('input[name="nokontrol"]').val().length == '' ){
                        alert('Nomor Kontrol Wajib diisi!');
                        return false;
                    }else if( $('input[name="noskdp"]').val().length == '' ){
                        alert('Nomor SKDP Wajib diisi!');
                        return false;
                    }

                    $.ajax({
                        url: base_url + "cpelayanan/setnoskdp",
                        type: "POST",
                        dataType: "JSON",
                        data: { 
                            // nokontrol: a, 
                            nokontrolsebelum: a, 
                            nokontrol: $('input[name="nokontrol"]').val(), 
                            noskdp: $('input[name="noskdp"]').val() 
                        },
                        success: function (a) {
                            // console.log(a);
                            notif(a.status, a.message), panelskdp.ajax.reload();
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });
                },
                batal: function () {},
            },
        });
    });

$(document).ready(function(){
    pemeriksaanklinik_cetakskdp();
    function pemeriksaanklinik_cetakskdp() {
        $('body').on('click','#pemeriksaanklinik_cetakskdp',function(e){
            let a = $(this).attr('pendaftaran');
            let c = $(this).attr('nokontrol');
            $.ajax({
                url: base_url + "cpelayanan/pemeriksaanklinik_cetakskdp",
                type: "post",
                dataType: "json",
                data: { id: a,nokontrol:c },
                success: function (a) {
                    console.log(a);
                    // cetak_skdp(a.diagnosa, a.skdp, a.pasien);
                },
                error: function (a) {
                    return fungsiPesanGagal(), !1;
                },
            });
        });
    }
});
