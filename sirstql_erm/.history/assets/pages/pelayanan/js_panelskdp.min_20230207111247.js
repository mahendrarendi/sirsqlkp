"use strict";
var panelskdp = "";
function listdata() {
    panelskdp = $("#panelskdp").DataTable({
        processing: !0,
        serverSide: !0,
        lengthChange: !1,
        searching: !1,
        stateSave: !0,
        order: [],
        ajax: {
            data: {
                tanggal: function () {
                    return $('input[name="tanggal"]').val();
                },
                cari: function () {
                    return $('input[name="cari"]').val();
                },
            },
            url: base_url + "cpelayanan/dtskdp",
            type: "POST",
        },
        columnDefs: [{ targets: [7, 8, 9], orderable: !1 }],
        fnCreatedRow: function (a, n, t) {},
        drawCallback: function (a, n) {},
    });
}
function refresh() {
    setdate(), $('input[type="search"]').val("").keyup(), panelskdp.state.clear(), panelskdp.ajax.reload();
}
function cari() {
    panelskdp.ajax.reload();
}
function setdate() {
    $('input[name="tanggal"]').datepicker({ autoclose: !0, format: "yyyy-mm-dd", orientation: "bottom" }).datepicker("setDate", "now");
}
$(function () {
    setdate(), listdata();
}),
    $(document).on("click", "#ubahskdp", function () {
        var b = $(this).attr("no");
        var a = $(this).attr('noskdp');
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Ubah SKDP",
            content: `<div class="form-group">
                <label>No SKDP</label>
                <input type="input" required class="form-control" name="noskdp" value="`+b+`" placeholder="input No.SKDP" />
            </div>
            <div class="form-group">
            <label>No Kontrol</label>
                <input type="input" required class="form-control" name="nokontrol" value="`+a+`" placeholder="input No Kontrol" />
            </div>`,
            buttons: {
                simpan: function () {

                    if( $('input[name="nokontrol"]').val().length == '' ){
                        alert('Nomor Kontrol Wajib diisi!');
                        return false;
                    }else if( $('input[name="noskdp"]').val().length == '' ){
                        alert('Nomor SKDP Wajib diisi!');
                        return false;
                    }

                    $.ajax({
                        url: base_url + "cpelayanan/setnoskdp",
                        type: "POST",
                        dataType: "JSON",
                        data: { 
                            // nokontrol: a, 
                            nokontrolsebelum: a, 
                            nokontrol: $('input[name="nokontrol"]').val(), 
                            noskdp: $('input[name="noskdp"]').val() 
                        },
                        success: function (a) {
                            // console.log(a);
                            notif(a.status, a.message), panelskdp.ajax.reload();
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });
                },
                batal: function () {},
            },
        });
    });

$(document).ready(function(){
    pemeriksaanklinik_cetakskdp();
    function pemeriksaanklinik_cetakskdp() {
        $('body').on('click','#pemeriksaanklinik_cetakskdp',function(e){
            let a = $(this).attr('pendaftaran');
            let c = $(this).attr('nokontrol');
            $.ajax({
                url: base_url + "cpelayanan/pemeriksaanklinik_cetakskdp",
                type: "post",
                dataType: "json",
                data: { id: a,nokontrol:c },
                success: function (a) {
                    console.log(a);
                    cetak_skdp(a.diagnosa, a.skdp, a.pasien);
                },
                error: function (a) {
                    return fungsiPesanGagal(), !1;
                },
            });
        });
    }
    function cetak_skdp(a, t, e) {
        var i = "",
            n = "";
            for (var o in a) (i += 2 == a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : ""), (n += 2 != a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : "");
        
            let diagnosa_skdp = (t.diagnosa != '') ? t.diagnosa.replace(/,/g, '<br>') : i;
            let terapi_skdp = (t.terapi != '') ? t.terapi.replace(/,/g, '<br>') : n;
            
            var l = '<img src="' + base_url + '/assets/images/headerlembarrm.svg"><img src="' + base_url + '/assets/images/garishitam.svg">';
        (l +=
            '<table style="margin-top:8px;margin-bottom:8px;"><tr ><td width="35%"></td><td><h4 style="margin:0;">SURAT KETERANGAN DALAM KEPERAWATAN</h4></td></tr></table><table><tr><td><b>SKDP </b></td><td>:</td><td> <b>' +
            t.noskdpql +
            "</b> </td></tr><tr><td><b>No. KONTROL </b></td><td>:</td><td> <b>" +
            t.nokontrol +
            "</b></td></tr><tr><td>Nama Pasien</td><td>:</td><td> " +
            e.namalengkap +
            " </td></tr><tr><td>Jenis Kelamin</td><td>:</td><td> " +
            e.jeniskelamin +
            " </td></tr><tr><td>No RM</td><td>:</td><td> " +
            if_empty(e.norm) +
            " </td></tr><tr><td>Nomor Kartu BPJS</td><td>:</td><td> " +
            if_empty(e.nojkn) +
            " </td></tr><tr><td>Alamat</td><td>:</td><td> " +
            if_empty(e.alamat) +
            ' </td></tr><tr style="margin-top:2px; margin-bottom:3px;"><td>Diagnosa</td><td>:</td><td><div>' +
            diagnosa_skdp +
            "</div></td></tr><tr><td>Terapi</td><td>:</td><td><div>" +
            terapi_skdp +
            "</div></td></tr><tr><td>Kontrol Kembali Tanggal</td><td>:</td><td>" +
            t.waktuperiksa +
            "</td></tr><tr><td>Tanggal Surat Rujukan</td><td>:</td><td>"+
            t.tglawalrujukan+
            "</td></tr></table><table><tr><td>Pasien tersebut diatas masih dalam perawatan <b>Dokter Spesialis</b> di RSU Queen Latifa dengan pertimbangan<br> sebagai berikut : <b>" +
            t.pertimbangan +
            "</b></td></tr><tr><td>Surat keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien<br> dengan diagnosa sebagai mana diatas pada tanggal <b>" +
            t.waktuperiksa +
            '</b><td></tr></table><table style="text-align:center;width:100%;" ><tr><td style="padding-left:50%;">Yogyakarta, ' +
            e.waktuperiksa +
            '</td></tr><tr><td style="padding-left:50%;">Dokter</td></tr><tr><td style="text-align:center;">'+e.ttd+'</td></tr><tr><td style="padding-left:50%;"><b>' +
            e.namadokter +
            "</b></td></tr></table>"),
            fungsi_cetaktopdf(l, "<style>@page {size:A5; margin:0.2cm;}</style>");
    }
});
