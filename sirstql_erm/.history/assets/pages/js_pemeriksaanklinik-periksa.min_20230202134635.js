var norm = localStorage.getItem("norm"),
    idperiksa = localStorage.getItem("idperiksa"),
    idpendaftaran = localStorage.getItem("idp");
localStorage.setItem("jenisrawat", "rajal");
var modehalamanjs = $('input[name="modehalaman"]').val();
function cariBhpFarmasi(a) {
    a.select2({
        placeholder: "Pilih Obat/Bhp",
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: base_url + "cpelayanan/pemeriksaanklinik_cariobatbhprawatjalan",
            dataType: "json",
            delay: 100,
            cache: !1,
            data: function (a) {
                return { q: a.term, carabayar: $('input[name="carabayar"]').val(), page: a.page || 1 };
            },
            processResults: function (a, t) {
                t.page;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.idbarang, text: a.namabarang + "  | stok " + convertToRupiah(a.stok) + " " + a.namasatuan + " | harga " + convertToRupiah(a.hargajual) + (0 == a.kadaluarsa ? "" : " | Exp:" + a.kadaluarsa) };
                    }),
                    pagination: { more: !0 },
                };
            },
        },
    });
}
function inputBhpFarmasi(a, t = "") {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_inputbhp",
        data: { x: a, i: idperiksa, jenispemakaian: t, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            isselesaiperiksa(a.status) && "success" == a.status && (pemeriksaanklinikRefreshBhp(idpendaftaran), $('select[name="caribhp"]').empty());
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihvitalsign(a, t) {
    startLoading();
    var e = $('input[name="idpendaftaran"]').val(),
        n = $('input[name="idperiksa"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahvitalsign",
        data: { x: a, y: e, ispaket: t, z: n, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshVitalsign(e));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshVitalsign(a) {
    var t = 0,
        e = "",
        n = "";
    $("#listVitalsign").empty(),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_listvitalsign",
            data: { x: a, y: idperiksa, modelist: $('input[name="modesave"]').val() },
            dataType: "JSON",
            success: function (a) {
                for (t in a) {
                    (n =
                        i != a[t].namapaketpemeriksaan
                            ? '<tr height="30px"><td class="text-bold" colspan="5">' +
                              a[t].namapaketpemeriksaan +
                              ' </td><td class=""><a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="hapuspaketparent(' +
                              a[t].idgrup +
                              ',\'vitalsign\')" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td><tr>'
                            : ""),
                        (e +=
                            n +
                            "<tr><td>" +
                            a[t].icd +
                            " - " +
                            a[t].namaicd +
                            '</td><td> <input type="hidden" name="icdvitalsign[]" value="' +
                            a[t].icd +
                            '"/><input type="hidden" id="istypehasil' +
                            a[t].idhasilpemeriksaan +
                            '" value="' +
                            a[t].istext +
                            '"/><input id="' +
                            a[t].idhasilpemeriksaan +
                            '" type="text" class="form-pelayanan" value="' +
                            a[t].nilai +
                            '" onchange="savenilaipemeriksaan_vitalsign(' +
                            a[t].idhasilpemeriksaan +
                            ')" placeholder="Input ' +
                            a[t].istext +
                            '" name="nilaivitalsign[]" size="1" /></td><td>' +
                            (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                            "</td><td>" +
                            (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                            ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                            "</td><td>" +
                            a[t].satuan +
                            '</td><td><a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusvitalsign" icd="' +
                            a[t].icd +
                            " - " +
                            a[t].namaicd +
                            '" vid="' +
                            a[t].idhasilpemeriksaan +
                            '" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>');
                    var i = a[t].namapaketpemeriksaan;
                }
                $("#listVitalsign").html(e), $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function pilihdiagnosa(a, t, e) {
    var n = $('input[name="idpendaftaran"]').val(),
        i = $('input[name="idperiksa"]').val();
    "primer" == e
        ? $.ajax({
              type: "POST",
              url: "pemeriksaan_cekdiagnosaprimer",
              data: { x: i, modelist: $('input[name="modesave"]').val(), idicd: t },
              dataType: "JSON",
              success: function (s) {
                  s.status > 0 ? notif("warning", "ICD " + t + " dengan level primer sudah ada!") : simpandiagnosa(a, e, t, n, i);
              },
              error: function (a) {
                  return fungsiPesanGagal(), !1;
              },
          })
        : simpandiagnosa(a, e, t, n, i);
}
function simpandiagnosa(a, t, e, n, i) {
    startLoading(),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_tambahdiagnosa",
            data: { x: a, y: n, z: i, idicd: e, l: t, modesave: $('input[name="modesave"]').val() },
            dataType: "JSON",
            success: function (a) {
                stopLoading(), notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshDiagnosa(n, e);
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaanRefreshDiagnosa(a, t) {
    var e = 0,
        n = "";
    $("#listDiagnosa").empty(),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_listdiagnosa",
            data: { x: a, idicd: t, modelist: $('input[name="modesave"]').val() },
            dataType: "JSON",
            success: function (a) {
                for (e in a)
                    n +=
                        "<tr><td>" +
                        a[e].icd +
                        "</td><td>" +
                        a[e].namaicd +
                        "</td><td>" +
                        a[e].icdlevel +
                        "</td><td>" +
                        a[e].aliasicd +
                        '</td><td><a id="hapusdiagnosa" class="btn btn-default btn-xs" data-toggle="tooltip" idicd="' +
                        t +
                        '" namaicd="' +
                        a[e].namaicd +
                        '"  alt2="' +
                        a[e].icd +
                        '" alt="' +
                        a[e].idhasilpemeriksaan +
                        '" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>';
                $("#listDiagnosa" + t).html(n), $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function hapusdiagnosa(a, t, e) {
    startLoading();
    var n = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapusdiagnosa",
        data: { x: a, modesave: $('input[name="modesave"]').val(), icd: t, idp: $('input[name="idpendaftaran"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshDiagnosa(n, e);
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function pilihtindakan(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val(),
        e = $('input[name="idperiksa"]').val(),
        n = $('input[name="idunit"]').val(),
        i = a;
    $('select[name="caritindakan"]').val(""),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_tambahtindakan",
            data: { x: i, y: t, z: e, modesave: $('input[name="modesave"]').val(), u: n },
            dataType: "JSON",
            success: function (a) {
                stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshTindakan(t));
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaanRefreshTindakan(a) {
    var t = $('input[name="modesave"]').val(),
        e = 0,
        n = 0,
        i = "",
        s = 0;
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listtindakan",
        data: { x: a, modelist: t },
        dataType: "JSON",
        success: function (a) {
            for (e in a.tindakan)
                ++n,
                    (s += parseInt(a.tindakan[e].total) - parseInt(a.tindakan[e].potongantagihan)),
                    (i +=
                        "verifikasi" == t
                            ? "<tr><td>" +
                              a.tindakan[e].icd +
                              "</td><td>" +
                              a.tindakan[e].namaicd +
                              '</td><td><select class="form-control iddokterpenerimajm" idhasilpemeriksaan="' +
                              a.tindakan[e].idhasilpemeriksaan +
                              '"><option value="' +
                              a.tindakan[e].iddokterjasamedis +
                              '">' +
                              a.tindakan[e].dokterpenerimajm +
                              '</option></select></td><td class="text-center">' +
                              a.tindakan[e].jumlah +
                              "</td><td>" +
                              (0 == a.tindakan[e].total ? "" : convertToRupiah(a.tindakan[e].total)) +
                              "</td><td>" +
                              (0 == a.tindakan[e].potongantagihan ? "" : convertToRupiah(a.tindakan[e].potongantagihan)) +
                              "</td><td></td></tr>"
                            : "<tr><td>" +
                              a.tindakan[e].icd +
                              "</td><td>" +
                              a.tindakan[e].namaicd +
                              '</td><td><select class="form-control iddokterpenerimajm" idhasilpemeriksaan="' +
                              a.tindakan[e].idhasilpemeriksaan +
                              '"><option value="' +
                              a.tindakan[e].iddokterjasamedis +
                              '">' +
                              a.tindakan[e].dokterpenerimajm +
                              '</option></select></td><td class="text-center">' +
                              (0 == a.tindakan[e].total
                                  ? ""
                                  : '<input id="idTindakan' +
                                    n +
                                    '" type="hidden" value="' +
                                    a.tindakan[e].idhasilpemeriksaan +
                                    '"><input type="text" class="form-pelayanan text-center" id="setjmlTindakan' +
                                    n +
                                    '" value="' +
                                    a.tindakan[e].jumlah +
                                    '" onchange="set_jmltindakan(' +
                                    n +
                                    ",'Tindakan')\" />") +
                              "</td><td>" +
                              (0 == a.tindakan[e].total ? "" : convertToRupiah(a.tindakan[e].total)) +
                              "</td><td>" +
                              (0 == a.tindakan[e].potongantagihan ? "" : convertToRupiah(a.tindakan[e].potongantagihan)) +
                              "</td><td> " +
                              menuUbahAsuransiHasilPemeriksaan(a.tindakan[e].idhasilpemeriksaan, a.tindakan[e].jaminanasuransi, "tindakan") +
                              ' <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa(' +
                              a.tindakan[e].idhasilpemeriksaan +
                              ',\'Tindakan\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapustindakan" tid="' +
                              a.tindakan[e].idhasilpemeriksaan +
                              '" icd="' +
                              a.tindakan[e].icd +
                              '" namaicd="' +
                              a.tindakan[e].namaicd +
                              '"  data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>');
            (i += '<tr class="bg bg-info"><td colspan="4">Subtotal</td><td colspan="2">' + convertToRupiah(s) + "</td></tr>"),
                $("#listTindakan").empty(),
                $("#listTindakan").html(i),
                $("#totalPemeriksaan").empty(),
                $("#totalPemeriksaan").html(convertToRupiah(bulatkanRatusan(parseInt(a.total[0].totalseluruh1) + parseInt(a.total[0].totalseluruh2)))),
                select2serachmulti($(".iddokterpenerimajm"), "cmasterdata/fillcaridokter"),
                $('[data-toggle="tooltip"]').tooltip(),
                $(".dokselect2").select2();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshDiskon(a) {
    var t = 0,
        e = "",
        n = 0;
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listdiskon",
        data: { x: a, modelist: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            for (t in a)
                0,
                    (n += parseInt(a[t].total)),
                    (e +=
                        "<tr><td>" +
                        a[t].icd +
                        "</td><td>" +
                        a[t].namaicd +
                        "</td><td>" +
                        convertToRupiah(a[t].nominal) +
                        ("nominal" == a[t].jenisdiskon ? "" : "%") +
                        "</td><td>" +
                        convertToRupiah(a[t].total) +
                        '</td><td> <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="hapusdiskon(' +
                        a[t].idhasilpemeriksaan +
                        ')" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>');
            (e += '<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">' + convertToRupiah(n) + "</td></tr>"), $("#listDiskon").empty(), $("#listDiskon").html(e), $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihdiskon(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val(),
        e = $('input[name="idperiksa"]').val(),
        n = $('input[name="idunit"]').val(),
        i = a;
    $('select[name="caridiskon"]').val(""),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_tambahdiskon",
            data: { x: i, y: t, z: e, modesave: $('input[name="modesave"]').val(), u: n },
            dataType: "JSON",
            success: function (a) {
                stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshDiskon(t));
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function hapusdiskon(a) {
    hapustindakan(a), pemeriksaanRefreshDiskon($('input[name="idpendaftaran"]').val());
}
function updatePegawaiDokter(a) {
    $.ajax({
        url: "hasilpemeriksaan_updatePegawaiDokter",
        type: "post",
        dataType: "json",
        data: { idhp: $("#idTindakan" + a).val(), idpd: $("#comboPegawaiDokter" + a).val() },
        success: function (a) {
            if ("success" != a.status) return notif(a.status, a.message), !1;
            notif(a.status, a.message), pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val());
        },
        error: function (a) {
            return notif(a.status, a.message), !1;
        },
    });
}
function isicombopegawaidokter(a, t) {
    is_empty(localStorage.getItem("localStoragePegawaiDokter"))
        ? $.ajax({
              url: base_url + "cadmission/masterjadwal_caridokter",
              type: "POST",
              dataType: "JSON",
              success: function (e) {
                  localStorage.setItem("localStoragePegawaiDokter", JSON.stringify(e)), fillpegawaidokter(a, t);
              },
              error: function (a) {
                  return notif(a.status, a.message), !1;
              },
          })
        : $.ajax({
              url: "../cmasterdata/kosongan",
              type: "post",
              dataType: "json",
              succcess: function (e) {
                  fillpegawaidokter(a, t);
              },
              error: function (e) {
                  fillpegawaidokter(a, t);
              },
          });
}
function fillpegawaidokter(a, t) {
    var e = JSON.parse(localStorage.getItem("localStoragePegawaiDokter")),
        n = 0;
    for (n in e) $("#" + a).append("<option " + (e[n].idpegawai === t ? "selected" : "") + ' value="' + e[n].idpegawai + '">' + e[n].namalengkap + "</option>");
}
function set_jmltindakan(a, t) {
    var e = $("#id" + t + a).val(),
        n = $("#setjml" + t + a).val(),
        i = $('input[name="idpendaftaran"]').val();
    "" == n
        ? ($("#setjmltindakan").val(1), $("#setjmltindakan").focus(), alert_empty("jumlah"))
        : (startLoading(),
          $.ajax({
              type: "POST",
              url: "pemeriksaan_ubahjumlahtindakan",
              data: { id: e, jumlah: n },
              dataType: "JSON",
              success: function (a) {
                  stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "Radiologi" !== t ? pemeriksaanRefreshTindakan(i) : pemeriksaanRefreshRadiologi(i));
              },
              error: function (a) {
                  return stopLoading(), fungsiPesanGagal(), !1;
              },
          }));
}
function hapustindakan(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapustindakan",
        data: { x: a, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshTindakan(t));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function isselesaiperiksa(a) {
    return "selesaiperiksa" != a || ($.alert('<b>Pemeriksaan Sudah Selesai</b><div class="text text-red">Perubahan Tidak Tersimpan.</div><br> Mohon Melakukan Tambah/Ubah/Hapus Tindakan Sebelum Pasien Menyelesaikan Pembayaran.'), !1);
}
function pilihradiologi(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val(),
        e = $('input[name="idperiksa"]').val(),
        n = a;
    $('select[name="cariradiologi"]').val(""),
        $.ajax({
            type: "POST",
            url: "pemeriksaan_tambahradiologi",
            data: { x: n, y: t, z: e, modesave: $('input[name="modesave"]').val() },
            dataType: "JSON",
            success: function (a) {
                stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshRadiologi(t));
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaanRefreshRadiologi(a) {
    var t = 0,
        e = "",
        n = 0;
    $("#listRadiologi").empty();
    var i = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listradiologi",
        data: { x: a, modelist: i },
        dataType: "JSON",
        success: function (a) {
            var s = 0;
            for (t in a)
                s++,
                    (n += parseInt(a[t].total) - parseInt(a[t].potongantagihan)),
                    (e +=
                        "verifikasi" == i
                            ? "<tr><td>" +
                              a[t].icd +
                              "</td><td>" +
                              a[t].namaicd +
                              "</td><td>" +
                              a[t].jumlah +
                              "</td><td>" +
                              convertToRupiah(a[t].total) +
                              "</td><td>" +
                              convertToRupiah(a[t].potongantagihan) +
                              '</td><td> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusradiologi" rid="' +
                              a[t].idhasilpemeriksaan +
                              '" icd="' +
                              a[t].icd +
                              " - " +
                              a[t].namaicd +
                              '" data-original-title="Hapus"><i class="fa fa-trash"></i></a> </td></tr>'
                            : "<tr><td>" +
                              a[t].icd +
                              "</td><td>" +
                              a[t].namaicd +
                              '</td><td><select class="form-control iddokterpenerimajm" idhasilpemeriksaan="' +
                              a[t].idhasilpemeriksaan +
                              '"><option value="' +
                              a[t].iddokterjasamedis +
                              '">' +
                              a[t].dokterpenerimajm +
                              '</option></select></td><td><input id="idRadiologi' +
                              ++s +
                              '" type="hidden" value="' +
                              a[t].idhasilpemeriksaan +
                              '"><input type="text" class="form-pelayanan text-center" id="setjmlRadiologi' +
                              s +
                              '" value="' +
                              a[t].jumlah +
                              '" onchange="set_jmltindakan(' +
                              s +
                              ",'Radiologi')\"/></td><td>" +
                              convertToRupiah(a[t].total) +
                              "</td><td>" +
                              convertToRupiah(a[t].potongantagihan) +
                              "</td><td> " +
                              menuUbahAsuransiHasilPemeriksaan(a[t].idhasilpemeriksaan, a[t].jaminanasuransi, "radiologi") +
                              ' <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa(' +
                              a[t].idhasilpemeriksaan +
                              ',\'Radiologi\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusradiologi" rid="' +
                              a[t].idhasilpemeriksaan +
                              '" icd="' +
                              a[t].icd +
                              " - " +
                              a[t].namaicd +
                              '" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>');
            $("#listRadiologi").html(e + '<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">' + convertToRupiah(n) + "</td></tr>"),
                $('[data-toggle="tooltip"]').tooltip(),
                select2serachmulti($(".iddokterpenerimajm"), "cmasterdata/fillcaridokter");
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function simpanhasilradiologi(a) {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_simpanhasilradiologi",
        data: { i: a, h: $("#" + a).val() },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapusradiologi(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapusradiologi",
        data: { x: a, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshRadiologi(t));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function pilihlaboratorium(a, t) {
    startLoading(), $('select[name="carilaboratorium"]').val(""), $('select[name="paketlaboratorium"]').val("");
    var e = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tambahlaboratorium",
        data: { x: a, y: e, ispaket: t, z: idperiksa, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshLaboratorium(e));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshEkokardiografi(a) {
    var t = 0,
        e = "",
        n = "",
        i = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listekokardiografi",
        data: { x: a, y: idperiksa, modelist: i },
        dataType: "JSON",
        success: function (a) {
            for (t in a) {
                (n =
                    i == a[t].namapaketpemeriksaan && s == a[t].gruppaketperiksa
                        ? ""
                        : null == a[t].namapaketpemeriksaan
                        ? ""
                        : '<tr height="30px" ' +
                          (a[t].idgrup == a[t].idpaketpemeriksaan ? 'class="bg-info"' : "") +
                          '><td colspan="3" class="text-bold">' +
                          a[t].namapaketpemeriksaan +
                          '</td><td class="">' +
                          (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].total : "") +
                          '</td><td class=""></td></tr>'),
                    parseInt(a[t].total) - parseInt(a[t].potongantagihan),
                    (e +=
                        n +
                        (null == a[t].icd
                            ? ""
                            : "<tr " +
                              (null !== a[t].idgrup ? "" : 'class=""') +
                              "><td> &nbsp;&nbsp;&nbsp;" +
                              a[t].icd +
                              " - " +
                              a[t].namaicd +
                              '</td><td><input type="hidden" name="icd[]" value="' +
                              a[t].icd +
                              '"/><input type="hidden" id="istypehasil' +
                              a[t].idhasilpemeriksaan +
                              '" value="' +
                              a[t].istext +
                              '"/>' +
                              formInputHasilLaborat(a[t]) +
                              "</td><td>" +
                              (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                              ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                              " " +
                              a[t].satuan +
                              "</td></tr>"));
                a[t].idpaketpemeriksaan, a[t].idgrup;
                var i = a[t].namapaketpemeriksaan,
                    s = a[t].gruppaketperiksa;
            }
            $("#listEkokardiografi").empty(), $("#listEkokardiografi").html(e), $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanRefreshLaboratorium(a) {
    var t = 0,
        e = "",
        n = "",
        i = 0,
        s = "",
        r = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_listlaboratorium",
        data: { x: a, y: idperiksa, modelist: r },
        dataType: "JSON",
        success: function (a) {
            for (t in a)
                if ("4" == a[t].idjenisicd2) {
                    a[t].idgrup != l && null != l && ((e += '<tr ><td colspan="7" style="padding-left:15px;">' + s + "</td></tr>"), (s = "")),
                        "verifikasi" == r
                            ? ((n =
                                  o == a[t].namapaketpemeriksaan && d == a[t].gruppaketperiksa
                                      ? ""
                                      : null == a[t].namapaketpemeriksaan
                                      ? ""
                                      : '<tr height="30px" ' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? 'class="bg-info"' : "") +
                                        '><td colspan="5" class="text-bold">' +
                                        a[t].namapaketpemeriksaan +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].total : "") +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].potongantagihan : "") +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan
                                            ? menuKesimpulanHasil(a[t]) +
                                              ' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuspaketlaborat" plidgrup="' +
                                              a[t].idgrup +
                                              '" plgruppaket="' +
                                              a[t].gruppaketperiksa +
                                              '" icd="' +
                                              a[t].namapaketpemeriksaan +
                                              '"  data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                                            : "") +
                                        "</td></tr>"),
                              (i += parseInt(a[t].total) - parseInt(a[t].potongantagihan)),
                              (e +=
                                  n +
                                  (null == a[t].icd
                                      ? ""
                                      : "<tr " +
                                        (null !== a[t].idgrup ? "" : 'class="bg-warning"') +
                                        "><td> &nbsp;&nbsp;&nbsp;" +
                                        a[t].icd +
                                        " - " +
                                        a[t].namaicd +
                                        "</td><td>" +
                                        formInputHasilLaborat(a[t]) +
                                        "</td><td>" +
                                        (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                                        "</td><td>" +
                                        (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                                        ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                                        "</td><td>" +
                                        a[t].satuan +
                                        "</td><td>" +
                                        (null == a[t].idgrup ? convertToRupiah(a[t].total) : "") +
                                        "</td><td>" +
                                        (null == a[t].idgrup ? convertToRupiah(a[t].potongantagihan) : "") +
                                        "</td><td>" +
                                        (null == a[t].idgrup
                                            ? ' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuslaborat" lid="' +
                                              a[t].idhasilpemeriksaan +
                                              '" icd="' +
                                              a[t].icd +
                                              " - " +
                                              a[t].namaicd +
                                              '" data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                                            : "") +
                                        "</td></tr>")))
                            : ((n =
                                  o == a[t].namapaketpemeriksaan && d == a[t].gruppaketperiksa
                                      ? ""
                                      : null == a[t].namapaketpemeriksaan
                                      ? ""
                                      : '<tr height="30px" ' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? 'class="bg-info"' : "") +
                                        '><td colspan="5" class="text-bold">' +
                                        a[t].namapaketpemeriksaan +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].total : "") +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan ? a[t].potongantagihan : "") +
                                        '</td><td class="">' +
                                        (a[t].idgrup == a[t].idpaketpemeriksaan
                                            ? menuKesimpulanHasil(a[t]) +
                                              " " +
                                              menuUbahAsuransiHasilPemeriksaan(a[t].idhasilpemeriksaan, a[t].jaminanasuransi, "laboratorium") +
                                              ' <a class="btn btn-xs btn-default" onclick="print_laboratorium(' +
                                              $('input[name="idperiksa"]').val() +
                                              "," +
                                              $('input[name="idpendaftaran"]').val() +
                                              "," +
                                              $('input[name="idunit"]').val() +
                                              "," +
                                              a[t].idgrup +
                                              ')" ' +
                                              tooltip("Cetak") +
                                              ' ><i class="fa fa-print"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa(' +
                                              a[t].idhasilpemeriksaan +
                                              ',\'Laboratorium\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuspaketlaborat" plidgrup="' +
                                              a[t].idgrup +
                                              '" plgruppaket="' +
                                              a[t].gruppaketperiksa +
                                              '" icd="' +
                                              a[t].namapaketpemeriksaan +
                                              '"  data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                                            : "") +
                                        "</td></tr>"),
                              (i += parseInt(a[t].total) - parseInt(a[t].potongantagihan)),
                              (e +=
                                  n +
                                  (null == a[t].icd
                                      ? ""
                                      : "<tr " +
                                        (null !== a[t].idgrup ? "" : 'class="bg-warning"') +
                                        "><td> &nbsp;&nbsp;&nbsp;" +
                                        a[t].icd +
                                        " - " +
                                        a[t].namaicd +
                                        '</td><td><input type="hidden" name="icd[]" value="' +
                                        a[t].icd +
                                        '"/><input type="hidden" id="istypehasil' +
                                        a[t].idhasilpemeriksaan +
                                        '" value="' +
                                        a[t].istext +
                                        '"/>' +
                                        formInputHasilLaborat(a[t]) +
                                        "</td><td>" +
                                        (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                                        "</td><td>" +
                                        (null == a[t].nilaiacuanrendah ? "" : a[t].nilaiacuanrendah) +
                                        ("" == a[t].nilaiacuantinggi ? "" : " - " + a[t].nilaiacuantinggi) +
                                        "</td><td>" +
                                        a[t].satuan +
                                        "</td><td>" +
                                        (null == a[t].idgrup ? convertToRupiah(a[t].total) : "") +
                                        "</td><td>" +
                                        (null == a[t].idgrup ? convertToRupiah(a[t].potongantagihan) : "") +
                                        "</td><td> " +
                                        (null == a[t].idgrup
                                            ? menuUbahAsuransiHasilPemeriksaan(a[t].idhasilpemeriksaan, a[t].jaminanasuransi, "laboratorium") +
                                              '<a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa(' +
                                              a[t].idhasilpemeriksaan +
                                              ',\'Laboratorium\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuslaborat" lid="' +
                                              a[t].idhasilpemeriksaan +
                                              '" icd="' +
                                              a[t].icd +
                                              " - " +
                                              a[t].namaicd +
                                              '" data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                                            : "") +
                                        "</td></tr>")));
                    a[t].idpaketpemeriksaan;
                    null != a[t].keteranganhasil && a[t].keteranganhasil != s && (s = a[t].keteranganhasil);
                    var l = a[t].idgrup,
                        o = a[t].namapaketpemeriksaan,
                        d = a[t].gruppaketperiksa;
                }
            (e += '<tr ><td colspan="7" style="padding-left:15px;">' + s + "</td></tr>"),
                $("#listLaboratorium").empty(),
                $("#listLaboratorium").html(e + '<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">' + convertToRupiah(i) + "</td></tr>"),
                $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function formInputHasilLaborat(a) {
    return "text" == a.istext || "textnumerik" == a.istext
        ? "<textarea " +
              (a.isbolehinput > 0 ? "" : "readonly") +
              ' onchange="savenilaipemeriksaan_laborat(' +
              a.idhasilpemeriksaan +
              ",'laborat')\" id=\"" +
              a.idhasilpemeriksaan +
              '" class="form-control" placeholder="Input ' +
              a.istext +
              '" rows="2">' +
              a.nilai +
              "</textarea>"
        : "numerik" == a.istext
        ? '<input id="' +
          a.idhasilpemeriksaan +
          '" type="text" class="form-pelayanan" value="' +
          a.nilai +
          '" onchange="savenilaipemeriksaan_laborat(' +
          a.idhasilpemeriksaan +
          ",'laborat')\"  placeholder=\"Input " +
          a.istext +
          '" name="nilai[]" size="1" ' +
          (a.isbolehinput > 0 ? "" : "readonly") +
          " />"
        : "";
}
function menuKesimpulanHasil(a) {
    return (
        '<a id="inputeditkesimpulanhasil" nama="' +
        a.namapaketpemeriksaan +
        '" idgrup="' +
        a.idgrup +
        '" gruppaket="' +
        a.gruppaketperiksa +
        '" class="btn btn-xs btn-default" ' +
        tooltip("Input/Edit Kesimpulan Hasil") +
        '><i class="fa fa-file-text"></i></a>'
    );
}
function hapuslaboratorium(a) {
    startLoading();
    var t = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapuslaboratorium",
        data: { x: a, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && pemeriksaanRefreshLaboratorium(t));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function hapuspaketparent(a, t, e = "") {
    startLoading();
    var n = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_hapuspaketpemeriksaan",
        data: { x: a, y: n, gpp: e, modesave: $('input[name="modesave"]').val() },
        dataType: "JSON",
        success: function (a) {
            stopLoading(), isselesaiperiksa(a.status) && (notif(a.status, a.message), "success" == a.status && ("laboratorium" == t ? pemeriksaanRefreshLaboratorium(n) : pemeriksaanRefreshVitalsign(n)));
        },
        error: function (a) {
            return stopLoading(), fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaan_savenilaipemeriksaan(a, t, e = "") {
    var n = $("#istypehasil" + a).val(),
        i = $("#" + a).val();
    $.ajax({
        type: "POST",
        url: "pemeriksaan_savenilaipemeriksaan",
        data: { a: a, b: i, c: n, d: idperiksa, u: e },
        dataType: "JSON",
        success: function (a) {
            if ((notif(a.status, a.message), "success" == a.status)) $('input[name="idpendaftaran"]').val();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function savenilaipemeriksaan_laborat(a, t = "") {
    pemeriksaan_savenilaipemeriksaan(a, "laboratorium", t);
}
function savenilaipemeriksaan_vitalsign(a, t = "") {
    pemeriksaan_savenilaipemeriksaan(a, "vitalsign", t);
}
function diagnosa_or_tindakan(a, t, e = "") {
    startLoading(),
        a.select2({
            minimumInputLength: 3,
            allowClear: !0,
            ajax: {
                url: "pemeriksaanklinik_diagnosa",
                dataType: "json",
                delay: 150,
                cache: !1,
                data: function (a) {
                    return stopLoading(), { q: a.term, jenisicd: t, idicd: e, page: a.page || 1 };
                },
                processResults: function (a, t) {
                    t.page;
                    return {
                        results: $.map(a, function (a) {
                            return { id: a.icd, text: a.icd + " | " + a.namaicd + ("" != a.aliasicd ? " / " : "") + a.aliasicd };
                        }),
                    };
                },
            },
        });
}
function pilihJenisIcd(a) {
    pilihPaketDiagnosa($('select[name="paket"]'), a);
}
function singleDiagnosa(a) {
    $.ajax({
        type: "POST",
        url: "diagnosa_addsinglediagnosa",
        data: { x: a, y: $('input[name="idpendaftaran"]').val() },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function paketDiagnosa(a) {
    $.ajax({
        type: "POST",
        url: "diagnosa_addpaketdiagnosa",
        data: { x: a, y: $('input[name="idpendaftaran"]').val() },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pilihSingleDiagnosa(a, t) {
    a.select2({
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: "pemeriksaanklinik_cariicd",
            dataType: "json",
            delay: 150,
            cache: !1,
            data: function (a) {
                return { q: a.term, icd: t, page: a.page || 1 };
            },
            processResults: function (a, t) {
                t.page;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.icd, text: a.namaicd + ("" != a.aliasicd ? " >>> " : "") + a.aliasicd };
                    }),
                    pagination: { more: !0 },
                };
            },
        },
    });
}
function pilihPaketDiagnosa(a, t) {
    $.ajax({
        url: "diagnosa_pilihpaket",
        type: "POST",
        dataType: "JSON",
        data: { x: t },
        success: function (e) {
            var n = '<option value="0" >Pilih</option>';
            for (i in (a.empty(), e)) n = n + '<option value="' + e[i].idpaketpemeriksaan + '" >' + e[i].namapaketpemeriksaan + "</option>";
            a.html(n), $(".select2").select2(), pilihSingleDiagnosa($('select[name="single"]'), t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanklinikDetailPasien() {
    startLoading(),
        $.ajax({
            type: "POST",
            url: "pemeriksaanklinik_caridetailpasien",
            data: { norm: norm, i: idperiksa, modesave: $('input[name="modesave"]').val() },
            dataType: "JSON",
            success: function (a) {
                stopLoading(),
                    "1" == a.pasien.aksesubahdokter && $("#viewMenuUbahDokter").html('<a id="pemeriksaanklinik_ubahdokter" class="btn btn-warning btn-xs"><i class="fa fa-user-md"></i> Ubah Dokter</a>'),
                    "1" == a.pasien.ubahloketsaatperiksa && $("#viewMenuUbahLoket").html('<a id="pemeriksaanklinik_ubahloket" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Ubah Loket</a>'),
                    pemeriksaan_inputAnamnesa(a.grupjadwal),
                    1 == a.pasien.adaracikan ? $("#adaracikan").attr("checked", !0) : $("#adaracikan").attr("checked", !1),
                    $('input[name="dokter"]').val(if_empty(a.periksa.namalengkap)),
                    $("#iddokterpemeriksa").html('<option value="' + a.periksa.iddokterpemeriksa + '">' + a.periksa.dokterpemeriksa + "</option>"),
                    $('textarea[name="alergiobat"]').val(a.pasien.alergi),
                    edit_skrining($("#skrining"), a.skrining, a.pasien.kategoriskrining),
                    $('textarea[name="diagnosa"]').val(a.periksa.diagnosa),
                    $('textarea[name="keterangan"]').val(a.periksa.keterangan),
                    $('textarea[name="rekomendasi"]').val(a.periksa.rekomendasi),
                    $('textarea[name="keteranganobat"]').val(a.pasien.keteranganobat),
                    $('textarea[name="keteranganlaboratorium"]').val(a.pasien.keteranganlaboratorium),
                    $('textarea[name="keteranganradiologi"]').val(a.pasien.keteranganradiologi),
                    $('textarea[name="saranradiologi"]').val(a.pasien.saranradiologi),
                    a.keterangan_ekokardiografi.length > 0 &&
                        (null == a.pasien.keteranganekokardiografi
                            ? $('textarea[name="keteranganekokardiografi"]').val(a.keterangan_ekokardiografi[0].keterangan)
                            : $('textarea[name="keteranganekokardiografi"]').val(a.pasien.keteranganekokardiografi)),
                    0 != a.pasien.indikasipw && ($("#cindikasipasien").attr("checked", !0), setFormIndikasiPasienWabah(!0)),
                    viewIdentitasPasien(a.pasien, a.blacklist, a.penanggung),
                    $('input[name="idpendaftaran"]').val(a.periksa.idpendaftaran),
                    $('input[name="idperson"]').val(a.pasien.idperson),
                    $('input[name="status"]').val(a.pasien.idstatuskeluar),
                    $('input[name="carabayar"]').val(a.pasien.carabayar),
                    pemeriksaanRefreshDiagnosa(a.periksa.idpendaftaran, 9),
                    pemeriksaanRefreshDiagnosa(a.periksa.idpendaftaran, 10),
                    pemeriksaanRefreshDiskon(a.periksa.idpendaftaran),
                    pemeriksaanRefreshTindakan(a.periksa.idpendaftaran),
                    pemeriksaanRefreshRadiologi(a.periksa.idpendaftaran),
                    tampilHasilRadiografi(a.periksa.idpendaftaran),
                    pemeriksaanRefreshLaboratorium(a.periksa.idpendaftaran),
                    pemeriksaanRefreshVitalsign(a.periksa.idpendaftaran),
                    pemeriksaanRefreshEkokardiografi(a.periksa.idpendaftaran),
                    $('input[name="idunit"]').val(a.periksa.idunit),
                    $('input[name="idloket"]').val(a.pasien.idloket),
                    $('input[name="namaloket"]').val(a.pasien.namaloket),
                    "jknpbi" != a.pasien.carabayar && "jknnonpbi" != a.pasien.carabayar && "bpjstenagakerja" != a.pasien.carabayar && $(".pasien-prolanis").addClass("hide"),
                    1 == a.pasien.pasienprolanis ? $("#prolanis").attr("checked", !0) : $("#prolanis").attr("checked", !1),
                    $("#kunjunganberikutnya").datepicker({ autoclose: !0, format: "yyyy-mm-dd", orientation: "bottom" }).datepicker("setDate", a.pasien.tanggal_kunjunganberikutnya),
                    list_enum("kondisikeluar", a.kondisikeluar, a.pasien.kondisikeluar),
                    list_enum("statuspasienpulang", a.statuspasienpulang, a.pasien.statuspulang);
                    
                    /**
                     * [+] SKDP Tanggal
                     *  
                     **/ 
                     if( a.statuspulang == "SKDP" ){
                        let html = `<div class="group-ql"><label style="margin-top: 10px;">Tanggal Kontrol Berikutnya</label> <input value="`+a.tglskdpberikutnya+`" type="text" placeholder="Pilih Tanggal" autocomplate="off" class="dateql form-control tglkunjuanberikutnya" readonly name="tglskdpberikutnya" required></div>`;
                        $('#statuspasienpulang').after(html);

                        let date = new Date();
                        let today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                        let startDate = new Date(today.getFullYear(), date.getMonth(), 1);
                
                        $('.dateql').datepicker({
                            format: 'dd/mm/yyyy',
                            todayHighlight : true,
                            autoclose:true,
                            startDate: startDate,
                        });
                    }
                    /** end skdp tanggal */
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaan_inputAnamnesa(a) {
    var t = sessionStorage.v_inputdatasubyektifralan,
        e = "";
    for (x in a) {
        var n = 1 == a.length ? "" : a[x].namaunit;
        (e += '<input id="idanamnesa' + x + '" type="hidden" name="grupanamnesa[]" value="' + a[x].idpemeriksaan + '">'),
            (e +=
                '<div class="form-group"><label for="" class="col-sm-2 control-label">Data Subyektif ' +
                n +
                '</label><div class="col-sm-9"> <a class="btn btn-warning" onclick="pemeriksaanklinik_savedtsubyektif(' +
                x +
                ')">Simpan</a><textarea id="namaanamnesa' +
                x +
                '" class="form-control textarea" name="anamnesa' +
                a[x].idpemeriksaan +
                '" placeholder="Anamnesa ' +
                n +
                '" rows="6" ' +
                ("" == t ? "disabled" : "") +
                ">" +
                a[x].anamnesa +
                "</textarea></div></div>");
    }
    $("#pemeriksaanInputAnamnesa").empty(), $("#pemeriksaanInputAnamnesa").html(e), $(".textarea").wysihtml5({ toolbar: !1 });
}
function viewIdentitasPasien(a, t, e) {
    localStorage.setItem("namapasien", a.namalengkap), localStorage.removeItem("namapasien"), localStorage.setItem("namapasien", a.namalengkap);
    var n = "",
        i = "",
        s = "";
    for (var r in e) s += "<tr><td>" + e[r].namalengkap + "</td><td>" + e[r].hubungan + "</td><td>" + e[r].notelpon + "</td></tr>";
    var l = '<table class="table table-bordered"><tr><th>Nama Penanggung</th><th>Hubungan </th><th> No.Telp</th></tr>' + s + "</table>";
    "pemeriksaanklinik2" !== modehalamanjs
        ? ((n =
              '<div class="bg-info"><div class="box-body box-profile" ><div class="login-logo" style="margin-bottom:0px;"><b class="text-yellow"><i class="fa ' +
              ("laki-laki" == a.jeniskelamin ? "fa-male" : "fa-female") +
              ' fa-2x"></i></b></div><h3 class="profile-username text-center">' +
              a.namalengkap +
              '</h3><p class="text-muted text-center">' +
              (a.norm < "10083900" ? "Pasien Lama" : a.ispasienlama) +
              '</p><h5 class=" text-center">Usia ' +
              ambilusia(a.tanggallahir, a.tglsekarang) +
              " </h5></div></div>"),
          (i =
              '<div class="bg-info"><div class="box-body box-profile" ><table class="table table-striped" ><tbody><tr><td><strong><i class="fa fa-circle-o margin-r-5"></i>NIK </strong></td><td class="text-muted">: &nbsp; ' +
              a.nik +
              '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td><td class="text-muted">: &nbsp; ' +
              a.norm +
              '</td></tr><tr><td><strong><i class="fa fa-calendar margin-r-5"></i>Tanggal Lahir</strong></td><td class="text-muted">: &nbsp;' +
              a.tanggallahir +
              '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td><td class="text-muted">: &nbsp; ' +
              a.nojkn +
              '</td></tr><tr><td><strong><i class="fa fa-map-marker margin-r-5"></i>Tempat Lahir</strong></td><td class="text-muted">: &nbsp;' +
              a.tempatlahir +
              '</td></tr><tr><td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td><td class="text-muted">: &nbsp; ' +
              a.jeniskelamin +
              '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor SEP </strong></td><td class="text-muted">: &nbsp; ' +
              a.nosep +
              '</td></tr><tr><td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td><td class="text-muted">: &nbsp; ' +
              a.golongandarah +
              " " +
              a.rh +
              ' </td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RUJUKAN </strong></td><td class="text-muted">: &nbsp; ' +
              a.norujukan +
              '</td></tr><tr><td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td><td class="text-muted">: &nbsp; ' +
              a.pendidikan +
              "</td>" +
              ("0000-00-00" == a.tanggalrujukan || "" == a.tanggalrujukan
                  ? '<td colspan="2"></td>'
                  : '<td class="bg bg-red"><strong><i class="fa fa-circle-o margin-r-5"></i>Rujukan BPJS Selanjutnya</strong></td><td class="bg bg-red text-muted">: &nbsp; ' + getNextDate(a.tanggalrujukan, 90) + "</td>") +
              '</tr><tr><td><strong><i class="fa fa-circle-o margin-r-5"></i> Agama</strong></td><td class="text-muted" >: &nbsp;' +
              a.agama +
              '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Penjamin </strong></td><td class="text-muted">: &nbsp; ' +
              a.carabayar +
              '</td></tr><tr><td><strong><i class="fa fa-map-marker margin-r-5"></i>Alamat </strong></td><td class="text-muted" width="250px">: &nbsp; ' +
              a.alamat +
              '</td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ayah </strong></td><td class="text-muted">: &nbsp; ' +
              a.ayah +
              '</td></tr><tr><td colspan="2"><strong><i class="fa fa-circle-o margin-r-5"></i>Penanggung </strong></td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ibu </strong></td><td class="text-muted">: &nbsp; ' +
              a.ibu +
              '</td></tr><tr><td colspan="2">' +
              l +
              "</td></tr></tbody></table></div></div>"))
        : ((n =
              '<div class="login-logo" style="margin-bottom:0px;"><b class="text-yellow"><i class="fa ' +
              ("laki-laki" == a.jeniskelamin ? "fa-male" : "fa-female") +
              ' fa-md"></i></b></div><p class="text-muted text-center">' +
              (a.norm < "10083900" ? "Pasien Lama" : a.ispasienlama) +
              '</p><p class="text-center">' +
              a.norm +
              "</p>"),
          (i =
              '<h4 >Identitas Pasien </h4>   <table class="tbl"><tr><td>Nama</td><td>: ' +
              a.namalengkap +
              '</td><td class="padLeft">J.Kelamin</td><td>: ' +
              a.jeniskelamin +
              '</td><td class="padLeft">Ayah</td><td>: ' +
              a.ayah +
              '</td><td class="padLeft">No.RM</td><td>: ' +
              a.norm +
              '</td><td class="padLeft">No.SEP</td><td>: ' +
              a.nosep +
              "</td></tr><tr><td>Tgl.Lahir (Usia)</td><td>: " +
              a.tanggallahir +
              " (" +
              a.usia +
              ')</td><td class="padLeft">Gol.Darah</td><td>: ' +
              a.golongandarah +
              " " +
              a.rh +
              '</td><td class="padLeft">Ibu</td><td>: ' +
              a.ibu +
              '</td><td class="padLeft">No.JKN</td><td>: ' +
              a.nojkn +
              '</td><td class="padLeft">Penjamin</td><td>: ' +
              a.carabayar +
              "</td></tr><tr><td>Alamat</td><td>: " +
              a.alamat +
              '</td><td class="padLeft">Agama</td><td>: ' +
              a.agama +
              '</td><td class="padLeft">Penanggung</td><td>: ' +
              a.penanggung +
              '</td><td class="padLeft">No.Rujukan</td><td>: ' +
              a.norujukan +
              "</td></tr></table>"));
    var o = "",
        d = 0;
    for (var p in t) o += "<tr><td>" + ++d + "</td><td>" + t[p].namaunit + "</td></tr>";
    "" != o &&
        (n +=
            '<div class="box box-solid box-info" style="margin-top:10px;">\n                   <div class="box-header ">\n                           <h3 class="box-title ">Pasien ini diblacklist di Poli</h3>\n                   </div>\n                   <div class="box-body no-padding">\n                     <table class="table table-striped">\n                       <tbody><tr><th style="width: 10px">#</th><th>Unit/Poli</th></tr>' +
            o +
            "</tbody>\n                     </table>\n                   </div>\n                 </div>"),
        $("#pemeriksaanklinik_profile").html(n),
        $("#pemeriksaanklinik_detailprofile").html(i);
}
function updateBiayahasilperiksa(a, t) {
    var e = a,
        n =
            '<form action="" id="Formubahhargapaket"><div class="form-group"><input type="hidden" name="idp" value="' +
            $('input[name="idpendaftaran"]').val() +
            '" class="form-control"/><input type="hidden" name="id" value="' +
            e +
            '" class="form-control"/><div class="col-md-3"><label>Jasa Operator</label><input type="text" name="jasaoperator" class="form-control"/></select></div><div class="col-md-3"><label>Nakes</label><input type="text" name="nakes" class="form-control"/></select></div><div class="col-md-3"><label>Jasars</label><input type="text" name="jasars" class="form-control"/></select></div><div class="col-md-3"><label>Bhp</label><input type="text" name="bhp" class="form-control"/></select></div><div class="col-md-3"><label>Akomodasi</label><input type="text" name="akomodasi" class="form-control"/></select></div><div class="col-md-3"><label>Margin</label><input type="text" name="margin" class="form-control"/></select></div><div class="col-md-3"><label>Sewa</label><input type="text" name="sewa" class="form-control"/></select></div><div class="col-md-3"><label>Potongan</label><input type="text" name="potongan" class="form-control"/></select></div></div></form>';
    $.confirm({
        title: "Ubah Biaya",
        content: n,
        columnClass: "medium",
        buttons: {
            formSubmit: {
                text: "Update",
                btnClass: "btn-blue",
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "savepaketygdiubah",
                        data: $("#Formubahhargapaket").serialize(),
                        dataType: "JSON",
                        success: function (a) {
                            isselesaiperiksa(a.status) &&
                                (notif(a.status, a.message),
                                "success" == a.status &&
                                    ("Tindakan" == t
                                        ? pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val())
                                        : "Radiologi" == t
                                        ? pemeriksaanRefreshRadiologi($('input[name="idpendaftaran"]').val())
                                        : pemeriksaanRefreshLaboratorium($('input[name="idpendaftaran"]').val())));
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });
                },
            },
            formReset: { text: "batal", btnClass: "btn-danger" },
        },
        onContentReady: function () {
            tampilhargayangdiubah(e);
            var a = this;
            this.$content.find("form").on("submit", function (t) {
                t.preventDefault(), a.$$formSubmit.trigger("click");
            });
        },
    });
}
function tampilhargayangdiubah(a) {
    $.ajax({
        type: "POST",
        url: "tampilhargayangdiubah",
        data: { x: a },
        dataType: "JSON",
        success: function (a) {
            $('input[name="jasaoperator"]').val(a.jasaoperator),
                $('input[name="nakes"]').val(a.nakes),
                $('input[name="jasars"]').val(a.jasars),
                $('input[name="bhp"]').val(a.bhp),
                $('input[name="akomodasi"]').val(a.akomodasi),
                $('input[name="margin"]').val(a.margin),
                $('input[name="sewa"]').val(a.sewa),
                $('input[name="potongan"]').val(a.potongantagihan);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function hapusPaketPeriksa(a) {
    var t = $('input[name="idpendaftaran"]').val(),
        e = a;
    $.ajax({
        type: "POST",
        url: "delete_paket_periksa",
        data: { x: t, y: e },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function deleteHasilPeriksa(a) {
    var t = $('input[name="idpendaftaran"]').val(),
        e = $("#deleteHasilPeriksa" + a).val();
    $.ajax({
        type: "POST",
        url: "delete_hasil_periksa",
        data: { x: t, y: e },
        dataType: "JSON",
        success: function (a) {
            notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshDiagnosa();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaan_tampilpasien() {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_tampilpasien",
        data: { x: norm, y: idperiksa },
        dataType: "JSON",
        success: function (a) {
            $("#labNormPasien").html(": " + a.p.norm),
                $("#labNamaPasien").html(": " + a.p.namalengkap),
                $("#labPengirim").html(": " + a.per.titeldepan + " " + a.per.namalengkap + " " + a.per.titelbelakang),
                $("#labKelaminPasien").html(": " + a.p.jeniskelamin),
                $("#labTgllahirPasien").html(": " + a.p.tanggallahir);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaandetailTampilPasien() {
    $.ajax({
        type: "POST",
        url: "pemeriksaan_detail_tampil_pasien",
        data: { x: idperiksa },
        dataType: "JSON",
        success: function (a) {
            pemeriksaandetailTampilPasienListData(a);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaandetailTampilPasienListData(a) {
    var t =
        '<div class="col-sm-6"><table class="table table-striped" ><tr><th>Nama</th><th style="font-weight:normal;">: ' +
        if_empty(a.namalengkap) +
        '</th></tr><tr><th>No.RM</th><th style="font-weight:normal;">: ' +
        if_empty(a.norm) +
        '</th></tr><tr><th>Alamat</th><th style="font-weight:normal;">: ' +
        a.alamat +
        '</th></tr><tr><th>Telpon</th><th style="font-weight:normal;">: ' +
        a.telponpasien +
        '</th></tr><tr><th>Kelamin</th><th style="font-weight:normal;">: ' +
        a.jeniskelamin +
        '</th></tr><tr><th>TanggalLahir</th><th style="font-weight:normal;">: ' +
        a.tanggallahir +
        '</th></tr><tr><th>Agama</th><th style="font-weight:normal;">: ' +
        a.agama +
        '</th></tr><tr><th>Status</th><th style="font-weight:normal;">: ' +
        a.statusmenikah +
        '</th></tr><tr><th>Golongan Darah</th><th style="font-weight:normal;">: ' +
        a.golongandarah +
        '</th></tr><tr><th>Rhesus</th><th style="font-weight:normal;">: ' +
        a.rh +
        '</th></tr><tr><th>Alergi</th><th style="font-weight:normal;">: ' +
        a.alergi +
        '</th></tr></table></div><div class="col-sm-6"><table class="table table-striped" ><tr><th>Penanggung</th><th style="font-weight:normal;">: ' +
        a.namapenjawab +
        '</th></tr><tr><th>Alamat</th><th style="font-weight:normal;">: ' +
        a.alamatpenjawab +
        '</th></tr><tr><th>No.Telpon</th><th style="font-weight:normal;">: ' +
        a.notelpon +
        '</th></tr><tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr><tr><th>Klinik</th><th style="font-weight:normal;">: ' +
        a.namaunit +
        '</th></tr><tr><th>No.Antri</th><th style="font-weight:normal;">: ' +
        a.noantrian +
        '</th></tr><tr><th>Dokter</th><th style="font-weight:normal;">: ' +
        a.titeldepan +
        " " +
        a.namadokter +
        " " +
        a.titelbelakang +
        '</th></tr><tr><th>Carabayar</th><th style="font-weight:normal;">: ' +
        a.carabayar +
        '</th></tr><tr><th>No.SEP</th><th style="font-weight:normal;">: ' +
        a.nosep +
        "</th></tr></table></div>";
    $.alert({ title: '<div class="col-sm-12 center">Detail Pasien</div>', content: t, columnClass: "xl" });
}
function pemeriksaanklinik_adddiagnosa() {
    $.confirm({
        title: "Form Tambah ICD X",
        content:
            '<form action="" id="FormAddIcd"><div class="form-group"><label>Kode ICD</label><input type="text" name="kodeicd" placeholder="kode icd" class="form-control"/></div><div class="form-group"><label>Nama ICD</label><input type="text" name="namaicd" placeholder="nama icd" class="form-control"/></div><div class="form-group"><label>Alias ICD</label><input type="text" name="aliasicd" placeholder="alias icd" class="form-control"/></div></form>',
        columnClass: "small",
        buttons: {
            formSubmit: {
                text: "simpan",
                btnClass: "btn-blue",
                action: function () {
                    return "" === $('input[name="kodeicd"]').val()
                        ? (alert_empty("kode icd"), !1)
                        : "" === $('input[name="namaicd"]').val()
                        ? (alert_empty("nama icd"), !1)
                        : "" === $('input[name="aliasicd"]').val()
                        ? (alert_empty("alias icd"), !1)
                        : void $.ajax({
                              type: "POST",
                              url: base_url + "cmasterdata/pemeriksaan_saveicd",
                              data: $("#FormAddIcd").serialize(),
                              dataType: "JSON",
                              success: function (a) {
                                  notif(a.status, a.message), pilihdiagnosa($('input[name="kodeicd"]').val());
                              },
                              error: function (a) {
                                  return fungsiPesanGagal(), !1;
                              },
                          });
                },
            },
            formReset: { text: "Tutup", btnClass: "btn-danger" },
        },
        onContentReady: function () {
            var a = this;
            this.$content.find("form").on("submit", function (t) {
                t.preventDefault(), a.$$formSubmit.trigger("click");
            });
        },
    });
}
function tutupRiwayatPeriksa() {
    $("#listRiwayatSaatperiksa").empty(),
        $("#listRiwayatSaatperiksa").html(
            '<div class="box-body text-center" style="overflow-y: scroll;overflow-x: scroll; height: 120px;" ><div class="error-content"><h4><i class="fa fa-warning text-aqua"></i> Riwayat belum ditampilkan.</h4><a class="btn btn-primary" onclick="tampilRiwayatSaatPeriksa()">Tampil Riwayat</a></div></div>'
        ),
        $("#listTglRiwayatPeriksa").empty();
}
function tampilRiwayatSaatPeriksa() {
    startLoading(),
        $.ajax({
            type: "POST",
            url: base_url + "cpelayanan/list_riwayat_sattperiksa",
            data: { idperiksa: $('input[name="idperiksa"]').val() },
            dataType: "JSON",
            success: function (a) {
                if ((stopLoading(), "empty" == a.status)) return notif("info", "Riwayat tidak ditemukan...!");
                var t = '<div class="box-body" style="overflow-y: scroll;overflow-x: scroll; height: 350px;">';
                for (x in ((t +=
                    '<div class="pull-right"><a class="btn btn-danger" onclick="tutupRiwayatPeriksa()">Tutup Riwayat</a></div><br><div class="form-group"><div class="col-sm-12"><span class="label label-default">Diagnosa diluar icd 10</span> <a onclick="salinRiwayatDiagnosa()" class="btn btn-primary btn-xs"  data-toggle="tooltip" data-original-title="Salin Diagnosa diluar ICD 10"><i class="fa fa-copy"></i>Salin</a><textarea class="form-control" name="riwayatdiagnosa" rows="1" disabled>' +
                    a.periksa.diagnosa +
                    '</textarea></div></div><div class="form-group"><div class="col-sm-12"><label><h3>Diagnosa</h3></label> <table class="table"><thead><tr class="bg-info"><th>ICD</th><th>Nama ICD</th><th>Alias</th><th></th></tr></thead><tbody>'),
                a.diagnosa))
                    t +=
                        "<tr><td>" +
                        a.diagnosa[x].icd +
                        "</td><td>" +
                        a.diagnosa[x].namaicd +
                        "</td><td>" +
                        a.diagnosa[x].aliasicd +
                        '</td><td><a id="salinRiwayatIcd" icdlevel="' +
                        a.diagnosa[x].icdlevel +
                        '" isiSalinRiwayatIcd="' +
                        a.diagnosa[x].icd +
                        '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin ' +
                        a.diagnosa[x].icd +
                        '"><i class="fa fa-copy"></i> Salin</a></td></tr>';
                for (i in ((t +=
                    '</tbody></table></div></div><div class="form-group"><div class="col-sm-12"><span class="label label-default">Keterangan BHP/Obat</span> <a onclick="salinRiwayatKetObat()" class="btn btn-xs btn-primary" data-toggle="tooltip" data-original-title="Salin Keterangan Obat/BHP"><i class="fa fa-copy"></i> Salin </a><textarea disabled class="form-control" name="riwayatketeranganobat" rows="1">' +
                    a.periksa.keteranganobat +
                    '</textarea></div></div><div class="form-group"><div class="col-sm-12"><label><h3>Obat/BHP</h3></label><table class="table"><thead><tr class="bg-info"><th>NamaObat/BHP</th><th>Kekuatan</th><th>JumlahAmbil</th><th>Tatacara</th><th></th></tr></thead><tbody>'),
                a.obat))
                    t +=
                        "<tr><td>" +
                        a.obat[i].namabarang +
                        "</td><td>" +
                        a.obat[i].kekuatan +
                        "</td><td>" +
                        a.obat[i].jumlahpemakaian +
                        "</td><td>" +
                        a.obat[i].tatacara +
                        '</td><td><a onclick="salinRiwayatObat(' +
                        a.obat[i].idbarang +
                        ')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin ' +
                        a.obat[i].namabarang +
                        '"><i class="fa fa-copy"></i> Salin</a></td></tr>';
                (t += "</tbody></table></div></div>"),
                    (t += "</div>"),
                    $("#listRiwayatSaatperiksa").empty(),
                    $("#listRiwayatSaatperiksa").html(t),
                    $("#listTglRiwayatPeriksa").empty(),
                    $("#listTglRiwayatPeriksa").text(a.periksa.waktu),
                    $('[data-toggle="tooltip"]').tooltip();
            },
            error: function (a) {
                return fungsiPesanGagal(), !1;
            },
        });
}
function salinRiwayatDiagnosa() {
    return $('textarea[name="diagnosa"]').val($('textarea[name="riwayatdiagnosa"]').val()), notif("success", "Menyalin Diagnosa Berhasil...!");
}
function salinRiwayatKetObat() {
    return $('textarea[name="keteranganobat"]').val($('textarea[name="riwayatketeranganobat"]').val()), notif("success", "Menyalin Keterangan Obat/BHP Berhasil...!");
}
function salinRiwayatObat(a) {
    "" == $('textarea[name="keteranganobat"]').val()
        ? ($('textarea[name="keteranganobat"]').focus(), alert_empty("keterangan resep"))
        : $.ajax({
              type: "POST",
              url: "pemeriksaan_salinbhp",
              data: { x: a, i: idperiksa, p: $('input[name="idpendaftaran"]').val() },
              dataType: "JSON",
              success: function (a) {
                  notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshBhp();
              },
              error: function (a) {
                  return fungsiPesanGagal(), !1;
              },
          });
}
function isicomboaturanpakai(a, t) {
    is_empty(localStorage.getItem("localStorageAP"))
        ? $.ajax({
              url: base_url + "cmasterdata/get_barangaturanpakai",
              type: "POST",
              dataType: "JSON",
              success: function (e) {
                  localStorage.setItem("localStorageAP", JSON.stringify(e)), fillaturanpakai(a, t);
              },
              error: function (a) {
                  return notif(a.status, a.message), !1;
              },
          })
        : $.ajax({
              url: "../cmasterdata/kosongan",
              type: "post",
              dataType: "json",
              succcess: function (e) {
                  fillaturanpakai(a, t);
              },
              error: function (e) {
                  fillaturanpakai(a, t);
              },
          });
}
function fillaturanpakai(a, t) {
    var e = JSON.parse(localStorage.getItem("localStorageAP")),
        n = 0;
    for (n in e) $("#" + a).append("<option " + (e[n].idbarangaturanpakai === t ? "selected" : "") + ' value="' + e[n].idbarangaturanpakai + '">' + e[n].signa + "</option>");
}
function isicombokemasan(a, t) {
    is_empty(localStorage.getItem("localStorageKEMASAN"))
        ? $.ajax({
              url: base_url + "cmasterdata/get_kemasan",
              type: "POST",
              dataType: "JSON",
              success: function (e) {
                  localStorage.setItem("localStorageKEMASAN", JSON.stringify(e)), fillkemasan(a, t);
              },
              error: function (a) {
                  return notif(a.status, a.message), !1;
              },
          })
        : $.ajax({
              url: "../cmasterdata/kosongan",
              type: "post",
              dataType: "json",
              succcess: function (e) {
                  fillkemasan(a, t);
              },
              error: function (e) {
                  fillkemasan(a, t);
              },
          });
}
function fillkemasan(a, t) {
    var e = JSON.parse(localStorage.getItem("localStorageKEMASAN")),
        n = 0;
    for (n in e) $("#" + a).append("<option " + (e[n].idkemasan === t ? "selected" : "") + ' value="' + e[n].idkemasan + '">' + e[n].kemasan + "</option>");
}
function setfocusVitalsign() {
    $("#focusVitalsign").focus();
}
function setfocusElektromedik() {
    $("#focusElektromedik").focus();
}
function setfocusEchocardiography() {
    $("#focusEchocardiography").focus();
}
function setfocuslaboratorium() {
    $("#focusLaboratorium").focus();
}
function setfocusdiagnosa() {
    $("#focusDiagnosa").focus();
}
function setfocustindakan() {
    $("#focusTindakan").focus();
}
function setfocusobatbhp() {
    $("#focusObat").focus();
}
function FormPemeriksaanKlinikSubmit() {
    return (
        startLoading(),
        "" == $("#namaanamnesa0").val() && 0 != $("#validasidatasubyektif").val()
            ? (stopLoading(), alert("Harap Melengkapi Data Subyektif.!"), !1)
            : "" == $("#dataobyektif").val() && 0 != $("#validasidataobyektif").val()
            ? (stopLoading(), alert("Harap Melengkapi Data Obyektif.!"), !1)
            : void $("#FormPemeriksaanKlinik").submit()
    );
}
function pemeriksaanklinik_gettglriwayat(a = "") {
    startLoading(),
        (tgl1 = "" == a ? "" : $('input[name="riwayat_tgl1"]').val()),
        $.ajax({
            url: base_url + "cpelayanan/get_riwayat_tglperiksa",
            data: { tgl1: tgl1, tgl2: a, norm: norm },
            type: "POST",
            dataType: "JSON",
            success: function (a) {
                if ((stopLoading(), 0 == a.length))
                    return $("#r_hasilPeriksa").empty(), $("#r_hasilPeriksa").html('<div class="error-content text-center"><h3><i class="fa fa-warning text-yellow"></i> Riwayat pemeriksaan belum ada..!.</h3></div>'), !0;
                var t = "";
                for (x in a) t += '<button onclick="pemeriksaanklinik_getriwayatperiksa(' + a[x].idpendaftaran + ')" type="button" class="btn btn-default">' + a[x].waktuperiksa + "</button>";
                $("#r_tanggalPeriksa").empty(), $("#r_tanggalPeriksa").html(t), pemeriksaanklinik_getriwayatperiksa(a[0].idpendaftaran);
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaanklinik_getriwayatperiksa(a) {
    $.ajax({
        url: base_url + "cpelayanan/get_riwayatperiksa",
        data: { id: a },
        type: "POST",
        dataType: "JSON",
        success: function (a) {
            var t = a.identitas,
                e = "",
                n = "",
                s = "",
                r = "",
                l = "",
                o = "";
            for (x in a.vitalsign) n += a.vitalsign[x].hasil_diagnosis;
            for (x in a.radiologi) s += a.radiologi[x].hasil_diagnosis;
            for (x in a.laboratorium) r += a.laboratorium[x].hasil_diagnosis;
            for (x in a.diagnosa)
                l +=
                    a.diagnosa[x].icd +
                    " " +
                    a.diagnosa[x].hasil_diagnosis +
                    ' <a id="salinRiwayatIcd" icdlevel="' +
                    a.diagnosa[x].icdlevel +
                    '" isisalinriwayaticd="' +
                    a.diagnosa[x].icd +
                    '" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin"><i class="fa fa-copy"></i> Salin</a><br>';
            for (x in a.tindakan) o += a.tindakan[x].icd + " " + a.tindakan[x].hasil_diagnosis;
            for (i in t)
                e +=
                    '<table style="margin-left: 16px;" class="col-sm-12 unselectable">' +
                    (0 != i ? '<tr class="bg bg-gray"><th>&nbsp;</th><td></td></tr>' : "") +
                    '<tr><th style="width: 10%;">DOKTER</th><td>: ' +
                    t[i].dokter +
                    "</td></tr><tr><th>WAKTU</th><td>: " +
                    t[i].tglperiksa +
                    "</td></tr><tr><th>KLINIK</th><td>: " +
                    t[i].namaunit +
                    '</td></tr></table><table style="margin: 16px;margin-top: 10px;" class="col-sm-11"><tr style="border-bottom:1px solid #000;"><th colspan="2">ANAMNESIS </th><th></tr><tr><td colspan="2" class="unselectable">' +
                    t[i].anamnesa +
                    "<br>" +
                    n +
                    '</td></tr><tr style="border-bottom:1px solid #000;"><th colspan="2">Elektromedik </th><th></tr><tr><td class="unselectable">' +
                    s +
                    '</td><td class="unselectable">' +
                    t[i].keteranganradiologi +
                    '</td></tr><tr style="border-bottom:1px solid #000;"><th colspan="2">LABORATORIUM </th><th></tr><tr><td colspan="2" class="unselectable">' +
                    r +
                    '</td></tr><tr style="border-bottom:1px solid #000;"><th colspan="2">DIAGNOSA</th><th></tr><tr><td colspan="2" class="unselectable">' +
                    l +
                    '</td></tr><tr style="border-bottom:1px solid #000;"><th colspan="2">TINDAKAN</th><th></tr><tr><td colspan="2" class="unselectable">' +
                    o +
                    '</td></tr><tr style="border-bottom:1px solid #000;"><th colspan="2">OBAT/BHP</th><th></tr><tr><td colspan="2" class="unselectable">' +
                    t[i].keteranganobat +
                    ' <a class="btn btn-primary btn-xs" id="periksa_salinketeranganobat" ket="' +
                    t[i].keteranganobat +
                    '"><i class="fa fa-copy"></i> Salin Keterang Obat</a><br><br>' +
                    periksa_cetakresep(a.grup, a.obat, "1", a.obat) +
                    "</td></tr></table>";
            $("#r_hasilPeriksa").empty(), $("#r_hasilPeriksa").html(e), $('[data-toggle="tooltip"]').tooltip();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function pemeriksaanklinik_saveketobat(a) {
    startLoading(),
        $.ajax({
            url: "pemeriksaanklinik_saveketobat",
            type: "post",
            dataType: "json",
            data: { id: $('input[name="idpendaftaran"]').val(), data: a },
            success: function (a) {
                if ((stopLoading(), "success" != a.status)) return notif(a.status, a.message), !1;
                notif(a.status, a.message);
            },
            error: function (a) {
                return stopLoading(), fungsiPesanGagal(), !1;
            },
        });
}
function pemeriksaanklinik_savedtsubyektif(a) {
    $.ajax({
        url: "pemeriksaanklinik_savedatasubyektif",
        type: "post",
        dataType: "json",
        data: { dt: $("#namaanamnesa" + a).val(), id: $("#idanamnesa" + a).val() },
        success: function (a) {
            if ("success" != a.status) return notif(a.status, a.message), !1;
            notif(a.status, a.message);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function setFormIndikasiPasienWabah(a) {
    "" !=
    $("input:checkbox[name='cindikasipasien']:checked")
        .map(function () {
            return $(this).val();
        })
        .get()
        ? $.ajax({
              url: base_url + "cpelayanan/pemeriksaanklinik_getpemeriksaanindikasipasienwabah",
              type: "post",
              dataType: "json",
              data: { idp: idperiksa },
              success: function (a) {
                  var t = a;
                  $("#formIndikasiPasienWabah").removeClass("hide").fadeIn("slow"),
                      dropdownlist_pemeriksaanpasienwabah("jeniswabah", t.wabah, null == t.pw ? "" : t.pw.idjeniswabah),
                      dropdownlist_pemeriksaanpasienwabah("statusrawatwabah", t.status, null == t.pw ? "" : t.pw.idstatusrawatwabah),
                      dropdownlist_pemeriksaanpasienwabah("sebabpenularan", t.penularan, null == t.pw ? "" : t.pw.idsebabpenularan),
                      change_statusrawatIPW(null == t.pw ? "" : t.pw.idstatusrawatwabah, null == t.pw ? "" : t.pw.idpenangananwabah);
              },
              error: function (a) {
                  return fungsiPesanGagal(), !1;
              },
          })
        : $("#formIndikasiPasienWabah").addClass("hide").fadeIn("slow");
}
function dropdownlist_pemeriksaanpasienwabah(a, t, e) {
    var n = '<option value="">Pilih</option>',
        s = "";
    for (i in ($("#" + a).empty(), t))
        n =
            n +
            '<option value="' +
            (s = "jeniswabah" == a ? t[i].idjeniswabah : "statusrawatwabah" == a ? t[i].idstatusrawatwabah : "sebabpenularan" == a ? t[i].idsebabpenularan : "penanganan" == a ? t[i].idpenangananwabah : "") +
            '" ' +
            (s == e ? "selected" : "") +
            " >" +
            ("jeniswabah" == a ? t[i].jeniswabah : "statusrawatwabah" == a ? t[i].statusrawat : "sebabpenularan" == a ? t[i].sebabpenularan : "penanganan" == a ? t[i].penanganan : "") +
            "</option>";
    $("#" + a).html(n);
}
function change_statusrawatIPW(a, t) {
    $.ajax({
        url: base_url + "cpelayanan/pemeriksaanklinik_getpenangananIPW",
        type: "post",
        dataType: "json",
        data: { status: a },
        success: function (a) {
            dropdownlist_pemeriksaanpasienwabah("penanganan", a, t);
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function formAlergi(a = "", t = "") {
    $.confirm({
        title: "Tambah Data Alergi",
        content:
            '<form action="" id="FormAddAlergi"> <input type="hidden" name="alergi_idperson" /><input type="hidden" name="mode" /><div class="form-group"><label>Jenis Alergi</label><select onchange="onchange_jenisalergi(this.value)" name="jenisalergi" class="form-control" ><option value="obat">Obat</option><option>Makanan</option><option>Minuman</option><option>Lainya</option></select></div><div class="form-group"><label class="obatalergi">Pilih Obat</label><select class="obatalergi form-control" name="alergi_idbarang"><option value="">Pilih Obat</option></select><label class="keteranganalergi hide">Keterangan</label><textarea class="keteranganalergi hide form-control" name="keterangan" rows="3"></textarea></div><div class="form-group"><label>Reaksi</label><textarea name="reaksi" rows="3" class="form-control"></textarea></div></form>',
        columnClass: "small",
        buttons: {
            formSubmit: {
                text: "Simpan",
                btnClass: "btn-blue",
                action: function () {
                    return "obat" == $('select[name="jenisalergi"]').val() && "" == $('select[name="alergi_idbarang"]').val()
                        ? (alert_empty(" (obat belum ada)"), !1)
                        : "" === $('input[name="keterangan"]').val()
                        ? (alert_empty("keterangan"), !1)
                        : ($(".select2").removeClass("hide"),
                          void $.ajax({
                              type: "POST",
                              url: base_url + "cpelayanan/pemeriksaan_savealergi",
                              data: $("#FormAddAlergi").serialize(),
                              dataType: "JSON",
                              success: function (a) {
                                  notif(a.status, a.message), pemeriksaanklinikRefreshAlergi(norm);
                              },
                              error: function (a) {
                                  return fungsiPesanGagal(), !1;
                              },
                          }));
                },
            },
            formReset: {
                text: "Batal",
                btnClass: "btn-danger",
                action: function () {
                    $(".select2").removeClass("hide");
                },
            },
        },
        onContentReady: function () {
            $('input[name="alergi_idperson"]').val($('input[name="idperson"]').val()),
                "" != a && tampilUbahAlergi(a, t),
                $.ajax({
                    type: "POST",
                    url: base_url + "cmasterdata/getdataenumjson",
                    dataType: "JSON",
                    data: { t: "ppas_a" },
                    success: function (a) {
                        var t = "";
                        for (x in a) t += '<option value="' + a[x] + '">' + a[x] + "</option>";
                        $('select[name="jenisalergi"]').empty(), $('select[name="jenisalergi"]').html(t), onchange_jenisalergi();
                    },
                });
            var e = this;
            this.$content.find("form").on("submit", function (a) {
                a.preventDefault(), e.$$formSubmit.trigger("click");
            });
        },
    });
}
function tampilUbahAlergi(a, t) {
    $.ajax({
        url: base_url + "cpelayanan/get_dataalergi",
        data: { idp: a, id: t, mode: "e" },
        dataType: "JSON",
        type: "POST",
        success: function (a) {
            console.log(a.keterangan), $('textarea[name="keteranganalergi"]').val(a.keterangan);
        },
        error: function (a) {
            return !1;
        },
    });
}
function onchange_jenisalergi() {
    "obat" == $('select[name="jenisalergi"]').val()
        ? ($(".obatalergi").removeClass("hide"), $(".select2").removeClass("hide"), $(".keteranganalergi").addClass("hide"), cariBhpFarmasi($('select[name="alergi_idbarang"]')))
        : ($(".select2").addClass("hide"), $(".obatalergi").addClass("hide"), $(".keteranganalergi").removeClass("hide"));
}
function pemeriksaanklinikRefreshAlergi(a) {
    $.ajax({
        url: base_url + "cpelayanan/get_dataalergi",
        data: { n: a, mode: "a" },
        dataType: "JSON",
        type: "POST",
        success: function (a) {
            var t = "";
            for (var e in a)
                t +=
                    '<tr><td><a onclick="formAlergi(' +
                    a[e].idperson +
                    "," +
                    a[e].id +
                    ')" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> <a onclick="hapusAlergi(' +
                    a[e].idperson +
                    "," +
                    a[e].id +
                    ')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a></td><td>' +
                    a[e].jenisalergi +
                    "</td><td>" +
                    a[e].keterangan +
                    "</td><td>" +
                    a[e].reaksi +
                    "</td></tr>";
            $("#listAlergi").empty(),
                $("#listAlergi").html(
                    '<table id="tblalergi" class="table table-striped"><thead> <tr class="bg bg-yellow"><th><a i="" id="addalergi" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Tambah Alergi"><i class="fa fa-plus-circle"></i></a></th><th>Jenis</th><th>Obat/Keterangan</th><th>Reaksi</th></tr></thead><tbody>' +
                        t +
                        "</tfoot></table>"
                ),
                $("#tblalergi").DataTable({ columnDefs: [{ orderable: !1, targets: 0 }] }),
                $("#addalergi").on("click", function () {
                    formAlergi();
                });
        },
        error: function (a) {
            return !1;
        },
    });
}
function menuUbahAsuransiHasilPemeriksaan(a, t, e) {
    return (
        '<a class="btn ' +
        ("0" == t ? "btn-default" : "btn-info") +
        ' btn-xs" data-toggle="tooltip" id="ubahjaminanasuransi" mode="' +
        e +
        '" alt="' +
        a +
        '" jaminan="' +
        t +
        '" data-original-title="jaminan ' +
        ("0" == t ? "Mandiri" : "Asuransi") +
        '"><i class="fa fa-flag"></i></a>'
    );
}
$(function () {
    $('input[name="idperiksa"]').val(idperiksa),
        $("#riwayatdtpasien").attr("norm", norm),
        $(".select2").select2(),
        $(".datepicker").datepicker({ autoclose: !0, format: "yyyy-mm-dd", orientation: "bottom" }),
        pemeriksaanklinikDetailPasien(),
        $('input[type="checkbox"].flat-red').iCheck({ checkboxClass: "icheckbox_flat-green", radioClass: "iradio_flat-green" }),
        cariBhpFarmasi($('select[name="caribhp"]')),
        diagnosa_or_tindakan($('select[name="carivitalsign"]'), 1),
        diagnosa_or_tindakan($('select[name="caridiagnosa"]'), 2, 10),
        diagnosa_or_tindakan($('select[name="caridiagnosa9"]'), 2, 9),
        diagnosa_or_tindakan($('select[name="caritindakan"]'), 3),
        diagnosa_or_tindakan($('select[name="carilaboratorium"]'), 4),
        diagnosa_or_tindakan($('select[name="cariradiologi"]'), 5),
        diagnosa_or_tindakan($('select[name="caridiskon"]'), 8),
        select2serachmulti($("#iddokterpemeriksa"), "cmasterdata/fillcaridokter"),
        pemeriksaanklinikRefreshBhp(idpendaftaran, "rajal"),
        $('input[name="norm"]').val(localStorage.getItem("norm")),
        $('[data-toggle="tooltip"]').tooltip(),
        startTimeLayanan();
    var a = $("#waktusekarang").val();
    $("#waktumulai").val(a);
}),
    $(document).on("click", "#hapusvitalsign", function () {
        var a = $(this).attr("icd"),
            t = $(this).attr("vid");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + a + " </b> <br>Hapus Dari Vital Sign.?",
            buttons: {
                Hapus: function () {
                    startLoading();
                    var a = $('input[name="idpendaftaran"]').val();
                    $.ajax({
                        type: "POST",
                        url: "pemeriksaan_hapusvitalsign",
                        data: { x: t, modesave: $('input[name="modesave"]').val() },
                        dataType: "JSON",
                        success: function (t) {
                            stopLoading(), notif(t.status, t.message), "success" == t.status && pemeriksaanRefreshVitalsign(a);
                        },
                        error: function (a) {
                            return stopLoading(), fungsiPesanGagal(), !1;
                        },
                    });
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#hapusdiagnosa", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt2"),
            e = $(this).attr("namaicd"),
            n = $(this).attr("idicd");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + t + " - " + e + " </b> <br>Hapus Dari ICD " + n + (10 == n ? " (Diagnosa) " : " (Tindakan) ") + ".?",
            buttons: {
                Hapus: function () {
                    hapusdiagnosa(a, t, n);
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#hapustindakan", function () {
        var a = $(this).attr("tid"),
            t = $(this).attr("icd"),
            e = $(this).attr("namaicd");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + t + " - " + e + " </b> <br>Hapus Dari Tindakan.?",
            buttons: {
                Hapus: function () {
                    hapustindakan(a);
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#hapusradiologi", function () {
        var a = $(this).attr("rid"),
            t = $(this).attr("icd");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + t + " </b> <br>Hapus Dari Radiologi.?",
            buttons: {
                Hapus: function () {
                    hapusradiologi(a);
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#inputeditkesimpulanhasil", function () {
        var a = $('input[name="idperiksa"]').val(),
            t = $(this).attr("nama"),
            e = $(this).attr("idgrup"),
            n = $(this).attr("gruppaket"),
            i =
                "<b>Paket Laborat : " +
                t +
                ' </b> </br><b>Kesimpulan</b> : <span id="menugeneratekesimpulan"></span><input type="hidden" id="mode_kesimpulanhasil"/> <textarea id="kesimpulanlaborat" class="form-control textkesimpulan" rows="12"></textarea>';
        $.confirm({
            icon: "fa fa-check-square-o",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            columnClass: "l",
            title: "Kesimpulan Hasil",
            content: i,
            buttons: {
                simpan: function () {
                    $.ajax({
                        url: base_url + "cpelayanan/insert_update_get_kesimpulanhasil",
                        type: "POST",
                        dataType: "JSON",
                        data: { idpemeriksaan: a, idpaketpemeriksaan: e, gruppaketperiksa: n, kesimpulan: $("#kesimpulanlaborat").val(), mode: $("#mode_kesimpulanhasil").val() },
                        success: function (a) {
                            notif(a.status, a.message), pemeriksaanRefreshLaboratorium($('input[name="idpendaftaran"]').val());
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });
                },
                batal: function () {},
            },
            onContentReady: function () {
                startLoading(),
                    $.ajax({
                        url: base_url + "cpelayanan/insert_update_get_kesimpulanhasil",
                        type: "POST",
                        dataType: "JSON",
                        data: { idpemeriksaan: a, idpaketpemeriksaan: e, gruppaketperiksa: n, mode: "getdata" },
                        success: function (a) {
                            stopLoading(),
                                $("#mode_kesimpulanhasil").val(null != a ? "updatedata" : "insertdata"),
                                $("#kesimpulanlaborat").val(null != a ? a.keteranganhasil : ""),
                                $('[data-toggle="tooltip"]').tooltip(),
                                $(".textkesimpulan").wysihtml5();
                        },
                        error: function (a) {
                            return stopLoading(), fungsiPesanGagal(), !1;
                        },
                    });
            },
        });
    }),
    $(document).on("click", "#hapuslaborat", function () {
        var a = $(this).attr("lid"),
            t = $(this).attr("icd");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + t + " </b> <br>Hapus Dari Laboratorium.?",
            buttons: {
                Hapus: function () {
                    hapuslaboratorium(a);
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#hapuspaketlaborat", function () {
        var a = $(this).attr("plgruppaket"),
            t = $(this).attr("plidgrup"),
            e = $(this).attr("icd");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi",
            content: "<b> " + e + " </b> <br>Hapus Dari Laboratorium.?",
            buttons: {
                Hapus: function () {
                    hapuspaketparent(t, "laboratorium", a);
                },
                Batal: function () {},
            },
        });
    }),
    $(document).on("click", "#salinRiwayatIcd", function () {
        pilihdiagnosa($(this).attr("isiSalinRiwayatIcd"), 10, $(this).attr("icdlevel"));
    }),
    $(document).on("click", "#periksa_salinketeranganobat", function () {
        $("#keteranganobat").val(""), $("#keteranganobat").val($(this).attr("ket")), notif("success", "Salin ketetangan obat berhasil..!");
    }),
    $(document).on("click", "#salinriwayatresep", function () {
        var a = $(this).attr("grup"),
            t = $(this).attr("idbp"),
            e = $(this).attr("resep") + "<br>" + ("0" != a ? $(this).attr("kemasangrup") + "<br>" + $(this).attr("signagrup") : $(this).attr("signa"));
        $.confirm({
            icon: "fa fa-question",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Salin Resep",
            content: e,
            buttons: {
                formSubmit: {
                    text: "salin",
                    btnClass: "btn-blue",
                    action: function () {
                        startLoading(),
                            $.ajax({
                                url: base_url + "cpelayanan/obat_salinriwayatobat",
                                type: "post",
                                dataType: "json",
                                data: { grup: a, idbp: t, idp: localStorage.getItem("idperiksa"), idpend: localStorage.getItem("idp") },
                                success: function (a) {
                                    stopLoading(), notif(a.status, a.message), "success" == a.status && pemeriksaanklinikRefreshBhp(idpendaftaran, "rajal");
                                },
                                error: function (a) {
                                    return stopLoading(), fungsiPesanGagal(), !1;
                                },
                            });
                    },
                },
                formReset: { text: "batal", btnClass: "btn-danger" },
            },
        });
    }),
    $(document).on("click", "#ubahjaminanasuransi", function () {
        var a = $(this).attr("alt"),
            t = 0 == $(this).attr("jaminan") ? 1 : 0,
            e = $(this).attr("mode");
        $.confirm({
            icon: "fa fa-question",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Konfirmasi!",
            content: "<b>Ubah Jaminan Tindakan</b> <br/> " + (1 == t ? "Dari <u>Mandiri</u> ke Asuransi" : "Dari Asuransi ke <u>Mandiri</u>"),
            buttons: {
                ubah: function () {
                    startLoading(),
                        $.ajax({
                            url: base_url + "cpelayanan/ubahjaminanasuransi_ralan",
                            type: "post",
                            dataType: "json",
                            data: { idp: a, jaminan: t, mode: e, idpend: idpendaftaran },
                            success: function (a) {
                                if ((stopLoading(), "success" != a.status)) return notif(a.status, a.message), !1;
                                switch (e) {
                                    case "tindakan":
                                        pemeriksaanRefreshTindakan(idpendaftaran);
                                        break;
                                    case "radiologi":
                                        pemeriksaanRefreshRadiologi(idpendaftaran);
                                        break;
                                    case "laboratorium":
                                        pemeriksaanRefreshLaboratorium(idpendaftaran);
                                        break;
                                    case "obat":
                                    case "grupobat":
                                        pemeriksaanklinikRefreshBhp(idpendaftaran);
                                }
                                notif(a.status, a.message);
                            },
                            error: function (a) {
                                return stopLoading(), notif(a.status, a.message), !1;
                            },
                        });
                },
                batal: function () {},
            },
        });
    }),
    $(document).on("click", "#pemeriksaanklinik_ubahdokter", function () {
        var a =
            '<form action="" id="FormUbahDokter"><div class="form-group"><input type="hidden" value="' +
            idperiksa +
            '" /><label>Dokter Pengganti</label><select id="iddokterpengganti" name="iddokterpengganti" class="form-control select2"><option value="0">Pilih Dokter</option></select></div></form>';
        $.confirm({
            title: "Ubah DPJP",
            content: a,
            columnClass: "small",
            buttons: {
                formSubmit: {
                    text: "Simpan",
                    btnClass: "btn-blue",
                    action: function () {
                        if (0 == $("#iddokterpengganti").val()) return alert("Dokter Belum Dipilih."), !1;
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/pemeriksaanklinik_ubahdokter",
                                data: { ip: idperiksa, idok: $("#iddokterpengganti").val() },
                                dataType: "JSON",
                                success: function (a) {
                                    if ((notif(a.status, a.message), "success" == a.status)) {
                                        var t = $("#iddokterpengganti option:selected").text();
                                        $('input[name="dokter"]').val(t);
                                    }
                                    stopLoading();
                                },
                                error: function (a) {
                                    return stopLoading(), fungsiPesanGagal(), !1;
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger", action: function () {} },
            },
            onContentReady: function () {
                select2serachmulti($("#iddokterpengganti"), "cmasterdata/fillcaridokter");
                var a = this;
                this.$content.find("form").on("submit", function (t) {
                    t.preventDefault(), a.$$formSubmit.trigger("click");
                });
            },
        });
    }),
    $(document).on("click", "#pemeriksaanklinik_ubahloket", function () {
        var a =
            '<form action="" id="FormUbahLoket"><div class="form-group"><input type="hidden" value="' +
            idperiksa +
            '" /><label>Loket Pengganti</label><select id="idloketpengganti" name="idloketpengganti" class="form-control select2"><option value="0">Pilih Loket</option></select></div></form>';
        $.confirm({
            title: "Ubah Loket",
            content: a,
            columnClass: "small",
            buttons: {
                formSubmit: {
                    text: "Simpan",
                    btnClass: "btn-blue",
                    action: function () {
                        if (0 == $("#idloketpengganti").val()) return alert("Loket Belum Dipilih."), !1;
                        startLoading(),
                            $.ajax({
                                type: "POST",
                                url: base_url + "cpelayanan/pemeriksaanklinik_ubahloket",
                                data: { ip: idperiksa, idl: $("#idloket").val(), idlp: $("#idloketpengganti").val(), idp: $('input[name="idpendaftaran"]').val() },
                                dataType: "JSON",
                                success: function (a) {
                                    if ((stopLoading(), notif(a.status, a.message), "success" == a.status)) {
                                        var t = $("#idloketpengganti option:selected").text();
                                        $('input[name="namaloket"]').val(t);
                                    }
                                },
                                error: function (a) {
                                    return stopLoading(), fungsiPesanGagal(), !1;
                                },
                            });
                    },
                },
                formReset: { text: "Batal", btnClass: "btn-danger", action: function () {} },
            },
            onContentReady: function () {
                select2serachmulti($("#idloketpengganti"), "cmasterdata/fillcariloket");
                var a = this;
                this.$content.find("form").on("submit", function (t) {
                    t.preventDefault(), a.$$formSubmit.trigger("click");
                });
            },
        });
    }),
    $(document).on("change", "#iddokterpemeriksa", function () {
        var a = this.value;
        startLoading(),
            $.ajax({
                type: "POST",
                url: base_url + "cpelayanan/pemeriksaanklinik_ubahdokterpemeriksa",
                data: { idpemeriksaan: $('input[name="idperiksa"]').val(), iddokterpemeriksa: a },
                dataType: "JSON",
                success: function (a) {
                    stopLoading(), notif(a.status, a.message);
                },
                error: function (a) {
                    return stopLoading(), fungsiPesanGagal(), !1;
                },
            });
    }),
    $(document).on("change", ".iddokterpenerimajm", function () {
        var a = this.value,
            t = $(this).attr("idhasilpemeriksaan");
        startLoading(),
            $.ajax({
                type: "POST",
                url: base_url + "cpelayanan/hasilpemeriksaan_update_penerimajm",
                data: { iddokterjasamedis: a, idhasilpemeriksaan: t },
                dataType: "JSON",
                success: function (a) {
                    stopLoading(), notif(a.status, a.message);
                },
                error: function (a) {
                    return stopLoading(), fungsiPesanGagal(), !1;
                },
            });
    });

    /**
     * Mr.Ganteng - DEV
     */
    $(document).ready(function(){
        /**
         * Status Pasien  Pulang
         * SKDP
         */
        skdp_status_pasien_pulang();
        function skdp_status_pasien_pulang(){
            let default_skdp = 'SKDP';
            let id_pendaftaran = localStorage.getItem("idp"); 
            let id_pemeriksaan = localStorage.getItem("idperiksa"); 
            let button_formskdp;
            
            button_formskdp = `<a id="pemeriksaanklinikBuatSKDP" nobaris="`+parseInt('0')+`" alt2="`+id_pendaftaran+`" alt3="`+id_pemeriksaan+`" class=" btn btn-danger".'><i class="fa fa-file-text"></i> Formulir SKDP</a>`;

            /**
             * change
             */
            $('body').on('change','#statuspasienpulang',function(e){
                e.preventDefault();
                let root = $(this);
                if( root.val() == default_skdp ){
                    $('#statuspasienpulang').after('<div id="group_form_skdp" style="margin-top: 20px;">'+button_formskdp+'</div>');
                }else{
                    $('#group_form_skdp').remove();
                }
            });

            /**
             * first load
             */
            setTimeout(function() {      
                if( $('#statuspasienpulang').find(':selected').val() == default_skdp ) {
                    $('#statuspasienpulang').after('<div id="group_form_skdp" style="margin-top: 20px;">'+button_formskdp+'</div>');
                    
                    $.ajax({
                        type: "POST",
                        url: base_url + "cpelayanan/cek_skdp_terdaftar",
                        data: { 
                            'idperiksa': localStorage.getItem("idperiksa"), 
                            'idp': localStorage.getItem("idp")  
                        },
                        dataType: "JSON",
                        success: function (a) {
                            $('#pemeriksaanklinikBuatSKDP').after(a.hsl);
                        },
                        error: function (a) {
                            return fungsiPesanGagal(), !1;
                        },
                    });

                }else{
                   
                    $('#group_form_skdp').remove();
                }
            }, 500);

        }

        /**
         * handle Click skdp
         */
        click_formulir_skdp();
        function click_formulir_skdp(){
            $('body').on('click','#pemeriksaanklinikBuatSKDP',function(e){
                e.preventDefault();
                let root = $(this);

                let id_pendafatran = root.attr('alt2');
                let id_pemeriksaan = root.attr('alt3');
                
                let tes = get_skdp_lebih_dari_dua(id_pendafatran,id_pemeriksaan);
                console.log(tes);
                startLoading(); 

                isGolonganicdKNS( id_pemeriksaan, id_pendafatran );
            });
        }

        function get_skdp_lebih_dari_dua(a,b){
            let idpendaftaran = a;
            let idpemeriksaan = b;
            let data = {idpendaftaran,idpemeriksaan};

            var return_data = {};
            $.ajax({
                url:base_url + 'cpelayanan/get_skdp_lebih_dari_dua',
                type:'POST',
                async: false,
                data:data,
                dataType:'JSON',
                success:function(response){
                    // console.log(response.iscek);
                    return_data = response;
                },
                error:function(xhr){
                    return fungsiPesanGagal(), !1;
                }
            });

            return return_data;
        }

        function isGolonganicdKNS(a, t) {
            
            // let data_skdp = get_skdp_lebih_dari_dua(t,a);
            // let cek_skdp = data_skdp.iscek;


            $.ajax({
                type: "POST",
                url: base_url + "cpelayanan/cekgolonganicdkns",
                data: { i: a,b:t },
                dataType: "JSON",
                success: function (a) {
                    console.log(a);
                    if (true == a.cekkns) return notif("danger", "Terdapat diagnosa KNS<br> Pasien tidak dapat dibuatkan SKDP dan hanya dapat dibuatkan Surat Rujuk Balik dari Dokter.!"), $(".jconfirm").hide(), !1;
                    var tg = ( ( a.tglskdpberikutnya == '' ) ? 'now' : a.tglskdpberikutnya ),
                        e = t;

                    let form_skdp = `<form action="" id="FormRujuk"><input type="hidden" value="`+ e + `" name="idp">
                                <div class="form-group">
                                    <label>Tanggal Kontrol <i class="fa fa-question"></i></label>
                                    <input type="text"  id="tglskdp" class="datepicker form-control" name="tanggal" /> 
                                </div>
                                <div class="form-group">
                                    <label>Poliklinik</label>
                                    <select class="select2 form-control" style="width:100%;" id="skdp_poliklinik" name="idjadwal">
                                        <option value="0">Pilih</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Petugas</label>
                                    <select class="select2 form-control" style="width:100%;" id="skdp_petugas" name="idpetugas"></select>
                                </div>`;
                    
                                // if( cek_skdp == 'true' ){
                                //     form_skdp += `<div class="form-group">
                                //         <label>Diagnosa</label>
                                //         <textarea class="form-control" name="diagnosa" required rows="3"></textarea>
                                //     </div>
                                //     <div class="form-group">
                                //         <label>Terapi</label>
                                //         <textarea class="form-control" name="terapi" required rows="3"></textarea>
                                //     </div>`;
                                // }
                                
                                form_skdp += `<div class="form-group">
                                    <label>Pertimbangan Dokter</label>
                                    <textarea name="pertimbangan" class="form-control" rows="3"></textarea>
                                </div>
                                <div class="form-group">
                                    <label><input type="checkbox" name="daftar" /> Include Pendaftaran</label>
                                </div>
                            </form>`;

                    let i = ($(this).attr("nobaris"),form_skdp);

                    $.confirm({
                        title: "SKDP <br><h4>Surat Keterangan Dalam Perawatan</h4>",
                        content: i,
                        columnClass: "medium",
                        buttons: {
                            formSubmit: {
                                text: "Cetak",
                                btnClass: "btn-blue",
                                action: function () {
                                    return "" == $('input[name="tanggal"]').val()
                                        ? (alert_empty("tanggal"), !1)
                                        : "0" === $('select[name="idjadwal"]').val()
                                        ? (alert_empty("poliklinik"), !1)
                                        // : cek_skdp == 'true' ? "" == $('textarea[name="diagnosa"]') ? (alert_empty("diagnosa"), !1) : ''
                                        // :  cek_skdp == 'true' ? "" == $('textarea[name="terapi"]') ? (alert_empty("terapi"), !1) : ''
                                        : void $.ajax({
                                              type: "POST",
                                              url: "pemeriksaanklinik_buatskdp",
                                              data: $("#FormRujuk").serialize(),
                                              dataType: "JSON",
                                              success: function (a) {
                                                  "danger" == a.status ? $.alert(a.message) : (notif(a.status, a.message), cetak_skdp(a.diagnosa, a.skdp, a.pasien));
                                              },
                                              error: function (a) {
                                                  return fungsiPesanGagal(), !1;
                                              },
                                          });
                                },
                            },
                            formReset: { text: "batal", btnClass: "btn-danger" },
                        },
                        onContentReady: function () {
                            stopLoading(), $("#tglskdp").datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" }).datepicker("setDate", tg);
                            var a = this;
                            $('body').find('.select2').select2();
        
                            this.$content.find("form").on("submit", function (t) {
                                t.preventDefault(), a.$$formSubmit.trigger("click");
                            });
                        },
                    });
        
                },
                error: function (a) {
                    stopLoading(), fungsiPesanGagal();
                },
            });
        }

        cariskdp_petugas();
        function cariskdp_petugas() {
            $("select[name='idpetugas']").select2({
                placeholder: "NIK | Nama",
                allowClear: !0,
                ajax: {
                    url: base_url + "cmasterdata/caridatapegawai",
                    type: "post",
                    dataType: "json",
                    delay: 200,
                    data: function (a) {
                        return { searchTerm: a.term };
                    },
                    processResults: function (a) {
                        return { results: a };
                    },
                    cache: !0,
                },
            });
        }

        function cetak_skdp(a, t, e) {
            var i = "",
                n = "";
            for (var o in a) (i += 2 == a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : ""), (n += 2 != a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : "");
            var l = '<img src="' + base_url + '/assets/images/headerlembarrm.svg"><img src="' + base_url + '/assets/images/garishitam.svg">';
            (l +=
                '<table style="margin-top:8px;margin-bottom:8px;"><tr ><td width="35%"></td><td><h4 style="margin:0;">SURAT KETERANGAN DALAM KEPERAWATAN</h4></td></tr></table><table><tr><td><b>SKDP </b></td><td>:</td><td> <b>' +
                t.noskdpql +
                "</b> </td></tr><tr><td><b>No. KONTROL </b></td><td>:</td><td> <b>" +
                t.nokontrol +
                "</b></td></tr><tr><td>Nama Pasien</td><td>:</td><td> " +
                e.namalengkap +
                " </td></tr><tr><td>Jenis Kelamin</td><td>:</td><td> " +
                e.jeniskelamin +
                " </td></tr><tr><td>No RM</td><td>:</td><td> " +
                if_empty(e.norm) +
                " </td></tr><tr><td>Nomor Kartu BPJS</td><td>:</td><td> " +
                if_empty(e.nojkn) +
                " </td></tr><tr><td>Alamat</td><td>:</td><td> " +
                if_empty(e.alamat) +
                ' </td></tr><tr style="margin-top:2px; margin-bottom:3px;"><td>Diagnosa</td><td>:</td><td><div>' +
                i +
                "</div></td></tr><tr><td>Terapi</td><td>:</td><td><div>" +
                n +
                "</div></td></tr><tr><td>Kontrol Kembali Tanggal</td><td>:</td><td>" +
                t.waktuperiksa +
                "</td></tr><tr><td>Tanggal Surat Rujukan</td><td>:</td><td></td></tr></table><table><tr><td>Pasien tersebut diatas masih dalam perawatan <b>Dokter Spesialis</b> di RSU Queen Latifa dengan pertimbangan<br> sebagai berikut : <b>" +
                t.pertimbangan +
                "</b></td></tr><tr><td>Surat keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien<br> dengan diagnosa sebagai mana diatas pada tanggal <b>" +
                t.waktuperiksa +
                '</b><td></tr></table><table style="text-align:center;width:100%;" ><tr><td style="padding-left:50%;">Yogyakarta, ' +
                e.waktuperiksa +
                '</td></tr><tr><td style="padding-left:50%;">Dokter<br><br><br><br></td></tr><tr><td style="padding-left:50%;"><b>' +
                e.namadokter +
                "</b></td></tr></table>"),
                fungsi_cetaktopdf(l, "<style>@page {size:A5; margin:0.2cm;}</style>");
        }

        skdp_getjadwal();
        function skdp_getjadwal() {
            $('body').on('change','#tglskdp',function(){
                let root = $(this);
                tampilDataPoliklinik($('select[name="idjadwal"]'), tglubah_format1(root.val())),
                
                setTimeout(function () {
                    cariskdp_petugas();
                }, 500);
            });
        }

      
    });