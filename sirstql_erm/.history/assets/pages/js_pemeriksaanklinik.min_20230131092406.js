"use strict";
var tablepemeriksaan;
function setLocalStorageTgl1(a) {
    return localStorage.setItem("tglawal", a);
}
function setLocalStorageTgl2(a) {
    return localStorage.setItem("tglakhir", a);
}
function select2_serachmultidata() {
    var a = 0;
    $("#norm").select2({
        allowClear: !0,
        ajax: {
            url: base_url + "cpelayanan/pemeriksaanklinik_getdtpasien",
            type: "post",
            dataType: "json",
            delay: 250,
            data: function (t) {
                return "" == t.term ? (a += 1) : (a = 0), { searchTerm: t.term, page: a };
            },
            processResults: function (a) {
                return { results: a };
            },
            cache: !0,
        },
    });
}
function listPemeriksaan() {
    $("#tglawal")
        .datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" })
        .datepicker("setDate", null == localStorage.getItem("tglawal") ? "now" : localStorage.getItem("tglawal")),
        $("#tglakhir")
            .datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" })
            .datepicker("setDate", null == localStorage.getItem("tglakhir") ? "now" : localStorage.getItem("tglakhir")),
        (tablepemeriksaan = $("#tablepemeriksaan").DataTable({
            processing: !0,
            serverSide: !0,
            stateSave: !0,
            order: [],
            ajax: {
                data: {
                    isfarmasi: function () {
                        return $('input[name="isfarmasi"]:checked').val();
                    },
                    isjkn: function () {
                        return $('input[name="isjkn"]:checked').val();
                    },
                    unit: function () {
                        return $("#idunit").val();
                    },
                    iddokter: function () {
                        return $("#idpegawaidokter").val();
                    },
                    tglawal: function () {
                        return $("#tglawal").val();
                    },
                    tglakhir: function () {
                        return $("#tglakhir").val();
                    },
                    norm: function () {
                        return $("#norm").val();
                    },
                },
                url: base_url + "cpelayanan/list_pemeriksaan",
                type: "POST",
            },
            columnDefs: [{ targets: [-1], orderable: !1 }],
            fnCreatedRow: function (a, t, e) {
                $(a).attr("style", qlstatuswarna(t[15]));
            },
            drawCallback: function (a, t) {
                $('[data-toggle="tooltip"]').tooltip();
            },
        }));
}
function refreshPemeriksaan() {
    $(".datepicker").datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" }).datepicker("setDate", "now"),
        $('input[type="search"]').val("").keyup(),
        tablepemeriksaan.state.clear(),
        setLocalStorageTgl1($("#tglawal").val()),
        setLocalStorageTgl2($("#tglakhir").val()),
        tablepemeriksaan.ajax.reload();
}
function reloadPemeriksaan() {
    setLocalStorageTgl1($("#tglawal").val()), setLocalStorageTgl2($("#tglakhir").val()), tablepemeriksaan.ajax.reload(null, !1);
}
function reloadPemeriksaanByUnit() {
    return localStorage.setItem("idunit", $("#idunit").val()), reloadPemeriksaan();
}
function reloadPemeriksaanByDokter() {
    return localStorage.setItem("idpegawaidokter", $("#idpegawaidokter").val()), reloadPemeriksaan();
}
function reloadPemeriksaanByNorm() {
    tablepemeriksaan.ajax.reload(), $("#norm").empty();
}
function get_exceldata() {
    var a = ambiltanggal($("#tglawal").val()) + "|" + ambiltanggal($("#tglakhir").val());
    window.location.href = base_url + "creport/downloadexcel_page/" + a + " pelayananperiksa " + $("#idunit").val();
}
function if_undefined(a, t) {
    return "undefined" === a ? t : a;
}
function panggilpasien(a) {
    $.ajax({
        type: "POST",
        url: base_url + "cantrian_1/panggilulang",
        data: { a: a },
        dataType: "JSON",
        success: function (t) {
            notif(t.status, t.message + a);
        },
        error: function (a) {
            console.log(a.responseText);
        },
    });
}
function pemanggilanhasil(a) {
    $.ajax({
        type: "POST",
        url: base_url + "cantrian_1/pemanggilanhasil",
        data: { i: a },
        dataType: "JSON",
        success: function (t) {
            notif(t.status, t.message + a);
        },
        error: function (a) {
            console.log(a.responseText);
        },
    });
}
function isGolonganicdKNS(a, t) {
    $.ajax({
        type: "POST",
        url: base_url + "cpelayanan/cekgolonganicdkns",
        data: { i: a,b:t },
        dataType: "JSON",
        success: function (a) {
            console.log(a);
            if (true == a.cekkns) return notif("danger", "Terdapat diagnosa KNS<br> Pasien tidak dapat dibuatkan SKDP dan hanya dapat dibuatkan Surat Rujuk Balik dari Dokter.!"), $(".jconfirm").hide(), !1;
            var tg = ( ( a.tglskdpberikutnya == '' ) ? 'now' : a.tglskdpberikutnya ),
                e = t,
                i =
                    ($(this).attr("nobaris"),
                    '<form action="" id="FormRujuk"><input type="hidden" value="' +
                        e +
                        '" name="idp"><div class="form-group"><label>Tanggal Kontrol <i class="fa fa-question"></i></label><input type="text"  id="tglskdp" class="datepicker form-control" name="tanggal" onchange="skdp_getjadwal(this.value)"/> </div><div class="form-group"><label>Poliklinik</label><select class="select2 form-control" style="width:100%;" id="skdp_poliklinik" name="idjadwal"><option value="0">Pilih</option></select></div><div class="form-group"><label>Petugas</label><select class="select2 form-control" style="width:100%;" id="skdp_petugas" name="idpetugas"></select></div><div class="form-group"><label>Pertimbangan Dokter</label><textarea name="pertimbangan" class="form-control" rows="3"></textarea></div><div class="form-group"><label><input type="checkbox" name="daftar" /> Include Pendaftaran</label></div></form>');
            $.confirm({
                title: "SKDP <br><h4>Surat Keterangan Dalam Perawatan</h4>",
                content: i,
                columnClass: "medium",
                buttons: {
                    formSubmit: {
                        text: "Cetak",
                        btnClass: "btn-blue",
                        action: function () {
                            return "" == $('input[name="tanggal"]').val()
                                ? (alert_empty("tanggal"), !1)
                                : "0" === $('select[name="idjadwal"]').val()
                                ? (alert_empty("poliklinik"), !1)
                                : void $.ajax({
                                      type: "POST",
                                      url: "pemeriksaanklinik_buatskdp",
                                      data: $("#FormRujuk").serialize(),
                                      dataType: "JSON",
                                      success: function (a) {
                                          "danger" == a.status ? $.alert(a.message) : (notif(a.status, a.message), cetak_skdp(a.diagnosa, a.skdp, a.pasien), reloadPemeriksaan());
                                      },
                                      error: function (a) {
                                          return fungsiPesanGagal(), !1;
                                      },
                                  });
                        },
                    },
                    formReset: { text: "batal", btnClass: "btn-danger" },
                },
                onContentReady: function () {
                    stopLoading(), $("#tglskdp").datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" }).datepicker("setDate", tg);
                    var a = this;
                    $('body').find('.select2').select2();

                    this.$content.find("form").on("submit", function (t) {
                        t.preventDefault(), a.$$formSubmit.trigger("click");
                    });
                },
            });

        },
        error: function (a) {
            stopLoading(), fungsiPesanGagal();
        },
    });
}
function cariskdp_petugas() {
    $("select[name='idpetugas']").select2({
        placeholder: "NIK | Nama",
        allowClear: !0,
        ajax: {
            url: base_url + "cmasterdata/caridatapegawai",
            type: "post",
            dataType: "json",
            delay: 200,
            data: function (a) {
                return { searchTerm: a.term };
            },
            processResults: function (a) {
                return { results: a };
            },
            cache: !0,
        },
    });
}
function cetak_skdp(a, t, e) {
    var i = "",
        n = "";
    for (var o in a) (i += 2 == a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : ""), (n += 2 != a[o].idjenisicd ? a[o].icd + " " + a[o].namaicd + "<br>" : "");
    var l = '<img src="' + base_url + '/assets/images/headerlembarrm.svg"><img src="' + base_url + '/assets/images/garishitam.svg">';
    (l +=
        '<table style="margin-top:8px;margin-bottom:8px;"><tr ><td width="35%"></td><td><h4 style="margin:0;">SURAT KETERANGAN DALAM KEPERAWATAN</h4></td></tr></table><table><tr><td><b>SKDP </b></td><td>:</td><td> <b>' +
        t.noskdpql +
        "</b> </td></tr><tr><td><b>No. KONTROL </b></td><td>:</td><td> <b>" +
        t.nokontrol +
        "</b></td></tr><tr><td>Nama Pasien</td><td>:</td><td> " +
        e.namalengkap +
        " </td></tr><tr><td>Jenis Kelamin</td><td>:</td><td> " +
        e.jeniskelamin +
        " </td></tr><tr><td>No RM</td><td>:</td><td> " +
        if_empty(e.norm) +
        " </td></tr><tr><td>Nomor Kartu BPJS</td><td>:</td><td> " +
        if_empty(e.nojkn) +
        " </td></tr><tr><td>Alamat</td><td>:</td><td> " +
        if_empty(e.alamat) +
        ' </td></tr><tr style="margin-top:2px; margin-bottom:3px;"><td>Diagnosa</td><td>:</td><td><div>' +
        i +
        "</div></td></tr><tr><td>Terapi</td><td>:</td><td><div>" +
        n +
        "</div></td></tr><tr><td>Kontrol Kembali Tanggal</td><td>:</td><td>" +
        t.waktuperiksa +
        "</td></tr><tr><td>Tanggal Surat Rujukan</td><td>:</td><td></td></tr></table><table><tr><td>Pasien tersebut diatas masih dalam perawatan <b>Dokter Spesialis</b> di RSU Queen Latifa dengan pertimbangan<br> sebagai berikut : <b>" +
        t.pertimbangan +
        "</b></td></tr><tr><td>Surat keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien<br> dengan diagnosa sebagai mana diatas pada tanggal <b>" +
        t.waktuperiksa +
        '</b><td></tr></table><table style="text-align:center;width:100%;" ><tr><td style="padding-left:50%;">Yogyakarta, ' +
        e.waktuperiksa +
        '</td></tr><tr><td style="padding-left:50%;">Dokter<br><br><br><br></td></tr><tr><td style="padding-left:50%;"><b>' +
        e.namadokter +
        "</b></td></tr></table>"),
        fungsi_cetaktopdf(l, "<style>@page {size:A5; margin:0.2cm;}</style>");
}
function pemeriksaanklinik_cetakskdp(a,c) {
    $.ajax({
        url: "pemeriksaanklinik_cetakskdp",
        type: "post",
        dataType: "json",
        data: { id: a,nokontrol:c },
        success: function (a) {
            cetak_skdp(a.diagnosa, a.skdp, a.pasien), reloadPemeriksaan();
        },
        error: function (a) {
            return fungsiPesanGagal(), !1;
        },
    });
}
function skdp_getjadwal(a) {
    tampilDataPoliklinik($('select[name="idjadwal"]'), tglubah_format1(a)),
        setTimeout(function () {
            cariskdp_petugas();
        }, 500);
}
function fungsi_pemeriksaanklinikUbahStatus(a, t, e, i) {
    $.confirm({
        icon: "fa fa-question",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Confirmation!",
        content: t,
        buttons: {
            confirm: function () {
                $.ajax({
                    url: "pemeriksaanklinik_ubahstatus",
                    type: "post",
                    dataType: "json",
                    data: { i: e, status: a },
                    success: function (a) {
                        notif(a.status, a.message), reloadPemeriksaan();
                    },
                    error: function (a) {
                        return fungsiPesanGagal(), !1;
                    },
                });
            },
            cancel: function () {},
        },
    });
}
function rujuktampilpoli(a) {
    tampilDataPoliklinik($('select[name="idjadwal"]'), ambiltanggal(a));
}
function diagnosa_or_tindakan(a, t, e = "") {
    a.select2({
        minimumInputLength: 3,
        allowClear: !0,
        ajax: {
            url: base_url + "cpelayanan/pemeriksaanklinik_diagnosa",
            dataType: "json",
            delay: 150,
            cache: !1,
            data: function (a) {
                return { q: a.term, jenisicd: t, idicd: e, page: a.page || 1 };
            },
            processResults: function (a, t) {
                t.page;
                return {
                    results: $.map(a, function (a) {
                        return { id: a.icd, text: a.icd + " | " + a.namaicd + ("" != a.aliasicd ? " / " : "") + a.aliasicd };
                    }),
                };
            },
        },
    });
}
$(function () {
    "undefined" == typeof Storage && pesanUndefinedLocalStorage(),
        localStorage.removeItem("idpendaftaran"),
        localStorage.removeItem("modependaftaran"),
        localStorage.removeItem("localStorageAP"),
        localStorage.removeItem("localStoragePegawaiDokter"),
        $("#idunit").select2(),
        $("#idpegawaidokter").select2(),
        get_datapoli("#idunit", localStorage.getItem("idunit") ? localStorage.getItem("idunit") : ""),
        dropdown_getdata($("#idpegawaidokter"), "cadmission/dropdown_listdokter", localStorage.getItem("idpegawaidokter") ? localStorage.getItem("idpegawaidokter") : "", '<option value="0">Pilih Dokter</option>'),
        setTimeout(function () {
            listPemeriksaan();
        }, 100);
}),
    $(document).on("click", "#pemeriksaanklinikBuatSKDP", function () {
        startLoading(), isGolonganicdKNS($(this).attr("alt3"), $(this).attr("alt2"));
    }),
    $(document).on("click", "#pemeriksaanklinikSelesai", function () {
        fungsi_pemeriksaanklinikUbahStatus("selesai", "Selesai Pemeriksaan?", $(this).attr("alt2"), $(this).attr("nobaris"));
    }),
    $(document).on("click", "#pemeriksaanklinikBatal", function () {
        fungsi_pemeriksaanklinikUbahStatus("batal", "Batal Pemeriksaan?", $(this).attr("alt2"), $(this).attr("nobaris"));
    }),
    $(document).on("click", "#pemeriksaanklinikTunda", function () {
        fungsi_pemeriksaanklinikUbahStatus("tunda", "Tunda Pemeriksaan?", $(this).attr("alt2"), $(this).attr("nobaris"));
    }),
    $(document).on("click", "#pemeriksaanklinikBatalselesai", function () {
        fungsi_pemeriksaanklinikUbahStatus("sedang ditangani", "Batal Selesa?", $(this).attr("alt2"), $(this).attr("nobaris"));
    }),
    $(document).on("click", "#pemeriksaanklinikUpdatePeriksa", function () {
        "undefined" != typeof Storage
            ? (localStorage.removeItem("idperiksa"),
              localStorage.removeItem("norm"),
              localStorage.removeItem("idp"),
              localStorage.setItem("idperiksa", $(this).attr("alt")),
              localStorage.setItem("norm", $(this).attr("norm")),
              localStorage.setItem("idp", $(this).attr("alt2")),
              (window.location.href = base_url + "cpelayanan/pemeriksaanklinik_update_periksa"))
            : pesanUndefinedLocalStorage();
    }),
    $(document).on("click", "#pemeriksaanklinikUpdateEkokardiografi", function () {
        "undefined" != typeof Storage
            ? (localStorage.removeItem("idperiksa"),
              localStorage.removeItem("norm"),
              localStorage.removeItem("idp"),
              localStorage.setItem("idperiksa", $(this).attr("alt")),
              localStorage.setItem("norm", $(this).attr("norm")),
              localStorage.setItem("idp", $(this).attr("alt2")),
              (window.location.href = base_url + "cpelayanan/pemeriksaanklinik_update_ekokardiografi"))
            : pesanUndefinedLocalStorage();
    }),
    $(document).on("click", "#pemeriksaanklinikRujuk", function () {
        var a = $(this).attr("alt"),
            t = $(this).attr("alt2"),
            e =
                '<form action="" id="FormRujuk"><input type="hidden" value="' +
                a +
                '" name="idperiksa"><input type="hidden" value="' +
                $(this).attr("idp") +
                '" name="idperson"><input type="hidden" value="' +
                $(this).attr("idpendaftaran") +
                '" name="idpendaftaran"><input type="hidden" value="' +
                $(this).attr("norm") +
                '" name="norm"><div class="form-group col-md-5"><label>Tanggal Periksa</label><input type="text" class="datepicker form-control" onchange="rujuktampilpoli(this.value)" name="tanggalperiksa" /></select></div><div class="form-group col-md-7"><label>Poliklinik</label><select class="form-control select_jadwalralan" style="width:100%;" name="idjadwal"><option value="0">Pilih</option></select></div><div class="form-group col-md-5"><label>Status</label><select class="form-control" style="width:100%;" name="status"><option value="0">Pilih</option><option value="rujuk internal kembali">Rujuk Internal Kembali</option><option value="rujuk internal tidak kembali">Rujuk Internal Tidak Kembali</option><option value="rujuk eksternal">Rujuk Internal Eksternal</option></select></div><div class="form-group col-md-7"><label>Pemeriksaan Terjadwal</label><div class="form-control" style="border:0px;"><label><input type="radio" name="jenis" value="0" checked/> Tidak &nbsp;&nbsp;&nbsp;</label><label><input type="radio" name="jenis" value="1"/> Ya</label></div></div><div class="form-group col-md-5"><label>Laboratorium</label><select class="form-control" style="width:100%;" id="laboratorium"><option value="">Pilih Laboratorium</option></select></div><div class="form-group col-md-7"><label>Paket Laboratorium</label><select class="form-control" style="width:100%;" id="paketlaboratorium"><option value="">Pilih Paket Laboratorium</option></select></div><div class="forom-group col-md-12" style="margin-top:-12px;"><table class="table table-striped non-margin" style="margin-bottom:10px;"><tbody id="list_laboratorium"></tbody></table></div><div class="form-group col-md-12"><label>Elektromedik</label><select class="form-control" style="width:100%;" id="elektromedik"><option value="">Pilih Elektromedik</option></select></div><div class="forom-group col-md-12" style="margin-top:-12px;"><table class="table table-striped non-margin" style="margin-bottom:8px;"><tbody id="list_elektromedik"></tbody></table></div></form>';
        $.confirm({
            title: "Rujuk Pemeriksaan Pasien",
            content: e,
            columnClass: "l",
            buttons: {
                formSubmit: {
                    text: "Rujuk",
                    btnClass: "btn-blue",
                    action: function () {
                        return "0" === $('select[name="poliklinik"]').val()
                            ? (alert_empty("poliklinik"), !1)
                            : "0" === $('select[name="status"]').val()
                            ? (alert_empty("status"), !1)
                            : (startLoading(),
                              void $.ajax({
                                  type: "POST",
                                  url: "pemeriksaanklinik_saverujuk",
                                  data: $("#FormRujuk").serialize(),
                                  dataType: "JSON",
                                  success: function (a) {
                                      stopLoading(), "success" == a.status ? (notif(a.status, a.message), qlReloadPage(600)) : $.alert(a.message);
                                  },
                                  error: function (a) {
                                      return stopLoading(), fungsiPesanGagal(), !1;
                                  },
                              }));
                    },
                },
                formReset: { text: "batal", btnClass: "btn-danger" },
            },
            onContentReady: function () {
                $(".datepicker").datepicker({ autoclose: !0, format: "dd/mm/yyyy", orientation: "bottom" }).datepicker("setDate", "now"),
                    tampilDataPoliklinik($('select[name="idjadwal"]'), t),
                    diagnosa_or_tindakan($("#laboratorium"), 4),
                    diagnosa_or_tindakan($("#elektromedik"), 5),
                    select2serachmulti($("#paketlaboratorium"), "cmasterdata/fill_paket_laboratorium_ralan");
                var a = this;
                this.$content.find("form").on("submit", function (t) {
                    t.preventDefault(), a.$$formSubmit.trigger("click");
                });
            },
        });
    }),
    $(document).on("change", "#laboratorium", function () {
        var a = new Date().getUTCMilliseconds(),
            t =
                '<tr id="' +
                a +
                '"><td><input type="hidden" name="icdlaborat[]" value="' +
                this.value +
                '" >&nbsp;' +
                $("#laboratorium option:selected").text() +
                '</td><td><a id="hapuslaborat" valid="' +
                a +
                '" ' +
                tooltip("hapus") +
                ' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
        $("#list_laboratorium").append(t), $("#laboratorium").html('<option value="">Pilih Laboratorium</option>'), $('[data-toggle="tooltip"]').tooltip();
    }),
    $(document).on("change", "#paketlaboratorium", function () {
        var a = new Date().getUTCMilliseconds(),
            t =
                '<tr id="' +
                a +
                '"><td><input type="hidden" name="paketlaborat[]" value="' +
                this.value +
                '" >&nbsp;' +
                $("#paketlaboratorium option:selected").text() +
                '</td><td><a id="hapuslaborat" valid="' +
                a +
                '" ' +
                tooltip("hapus") +
                ' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
        $("#list_laboratorium").append(t), $("#paketlaboratorium").html('<option value="">Pilih Paket Laboratorium</option>'), $('[data-toggle="tooltip"]').tooltip();
    }),
    $(document).on("click", "#hapuslaborat", function () {
        var a = $(this).attr("valid");
        $("#" + a).remove();
    }),
    $(document).on("change", "#elektromedik", function () {
        var a = new Date().getUTCMilliseconds(),
            t =
                '<tr id="' +
                a +
                '"><td><input type="hidden" name="icdelektromedik[]" value="' +
                this.value +
                '" >&nbsp;' +
                $("#elektromedik option:selected").text() +
                '</td><td><a id="hapuselektromedik" valid="' +
                a +
                '" ' +
                tooltip("hapus") +
                ' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
        $("#list_elektromedik").append(t), $("#elektromedik").html('<option value="">Pilih Elektromedik</option>'), $('[data-toggle="tooltip"]').tooltip();
    }),
    $(document).on("click", "#hapuselektromedik", function () {
        var a = $(this).attr("valid");
        $("#" + a).remove();
    }),
    $(document).on("click", "#pemeriksaanklinikPeriksa", function () {
        "undefined" != typeof Storage
            ? (localStorage.removeItem("idperiksa"),
              localStorage.removeItem("norm"),
              localStorage.removeItem("idp"),
              localStorage.setItem("idperiksa", $(this).attr("alt")),
              localStorage.setItem("norm", $(this).attr("norm")),
              localStorage.setItem("idp", $(this).attr("alt2")),
              (window.location.href = base_url + "cpelayanan/pemeriksaanklinik_periksa"))
            : pesanUndefinedLocalStorage();
    }),
    $(document).on("click", "#pemeriksaanklinikVerifikasi", function () {
        "undefined" != typeof Storage
            ? (localStorage.removeItem("idperiksa"),
              localStorage.removeItem("norm"),
              localStorage.removeItem("idp"),
              localStorage.setItem("idperiksa", $(this).attr("alt")),
              localStorage.setItem("norm", $(this).attr("norm")),
              localStorage.setItem("idp", $(this).attr("alt2")),
              (window.location.href = base_url + "cpelayanan/pemeriksaanklinik_verifikasi"))
            : pesanUndefinedLocalStorage();
    }),
    $(document).on("click", "#pemeriksaanklinikPeriksaDokter", function () {
        if ("undefined" != typeof Storage) {
            localStorage.removeItem("idp"), localStorage.removeItem("idperiksa"), localStorage.removeItem("norm");
            var a = $(this).attr("alt"),
                t = $(this).attr("norm"),
                e = $(this).attr("alt2");
            localStorage.setItem("idperiksa", a),
                localStorage.setItem("norm", t),
                localStorage.setItem("idp", e),
                $.ajax({
                    type: "POST",
                    url: base_url + "cpelayanan/pemeriksaan_uwaktuperiksadokter",
                    data: { id: a },
                    dataType: "JSON",
                    success: function (a) {
                        window.location.href = base_url + "cpelayanan/pemeriksaanklinik_periksa";
                    },
                    error: function (a) {
                        return fungsiPesanGagal(), !1;
                    },
                });
        } else pesanUndefinedLocalStorage();
    }),
    $(document).on("click", "#pemeriksaanklinikView", function () {
        if ("undefined" != typeof Storage) {
            localStorage.removeItem("idperiksa"), localStorage.removeItem("norm");
            var a = $(this).attr("alt"),
                t = $(this).attr("norm"),
                e = $(this).attr("alt2");
            localStorage.setItem("idperiksa", a), localStorage.setItem("norm", t), localStorage.setItem("idp", e), (window.location.href = base_url + "cpelayanan/pemeriksaanklinik_view");
        } else pesanUndefinedLocalStorage();
    });