var styleInput='style="height: 34px; padding:0 4px; border-radius:5px; font-size: 14px; line-height: 1.42857143; color: #555; background-color: #fff; background-image: none; border: 1px solid #ccc;"';
///--saat file js di panggil
$(function()
{ 
    
    catatanpengembang();
    $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
    var set_status = $('#set_status').val();//set status message
    var set_message = $('#set_message').val();//set pesan message
    $('#generate_barcode').val('');
     ///--- jika status tidak kosong panggil fungsi notif
    if(set_status !==''){notif(set_status, set_message);}
    // jika pesan order ugd 1 maka panggil antrian_orderugd
    if(sessionStorage.pesanorderugd==='1'){setInterval('panggilOrderUGD()', 100000);}
    // jika pesan order rm 1 maka tampil notifikasi data order rm
    if(sessionStorage.pesanorderrm==='1'){setInterval('panggilOrderRM()', 100000);}
    
    // jika pesan notif farmasi 1 maka tampil notifikasi farmasi
    if(sessionStorage.pesannotiffarmasi==='1'){setInterval('panggilNotifFarmasi()', 60000);}
    if(sessionStorage.pesannotiffarmasi==='1'){panggilNotifFarmasi();}
    
    // jika pesan notif farmasi 1 maka tampil notifikasi GUDANG
    // if(sessionStorage.pesannotifgudang==='1'){setInterval('panggilNotifGudang()', 70000);}
})
function catatanpengembang()
{
    consoleWithNoSource("%cSTOP!", "color: red; text-shadow: 0 0 3px #FF0000, 0 0 5px #0000FF;font-weight:bold;font-size:75px;");
    consoleWithNoSource("%c Ini adalah fitur browser yang ditujukan untuk pengembang. Jika seseorang meminta Anda untuk menyalin dan menempelkan sesuatu di sini untuk mengaktifkan fitur Sitiql atau meretas akun seseorang, itu adalah penipuan dan akan memberi mereka akses ke akun Sitiql Anda.", "font-size:15px;");
}
function consoleWithNoSource(...params) {
  setTimeout(console.log.bind(console, ...params));
}



///************tambah colspan dan rowspan otomatis*****************
function dynamicallyMergeCells()
{
//    dynamically adds colspans and / or rowspans to a table cell, when it contains certain content that is the same as the one in the next cell
//    https://stackoverflow.com/questions/29162783/dynamically-add-colspans-and-or-rowspans-to-a-table-cell
    var i = 0;
    var $trs = $('.table tr');
    $trs.each(function() {
        var $tds = $(this).find('td');
        var width = $tds.length;
        var num = 2;
        for(i = width - 2; i >= 0; i--) {
            if($($tds[i]).html() == $($tds[i + 1]).html() && $($tds[i]).html() != ""){
                $($tds[i]).attr('colspan', num);
                num++;
                $($tds[i + 1]).remove();
            } else {
                num = 2;
            }
        }
        $tds = $(this).find('td');
        width = $tds.length;
        $($tds[0]).attr('seq', 0);
        for(i = 1; i < width; i++) {
            $($tds[i]).attr('seq', parseInt($($tds[i - 1]).attr('seq')) + $($tds[i - 1]).prop('colspan'));
        }
    });

    var height = $trs.length;
    var j = 0;

    for(i = height - 2; i >= 0; i--){
        $($trs[i]).find('td').each(function() {
            var seq = parseInt($(this).attr('seq'));
            var $tdUnder = $($trs[i + 1]).find('td[seq="' + seq + '"]');
            if($tdUnder.length && ($tdUnder.html() != "") && ($tdUnder.html() == $(this).html()) && ($tdUnder.prop('colspan') == $(this).prop('colspan'))) {
                $(this).prop('rowspan', $tdUnder.prop('rowspan') + 1);
                $tdUnder.remove();
            }
        });
    }
}

//Get URL Parameter Using jQuery
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};

function ucwords(string)
{
    var result = string.toLowerCase().replace(/\b[a-z]/g, function(letter) { return letter.toUpperCase();});
    return result;
}
function edit_skrining(id,dtskrining,check)
{
    var radio='';
    for(var x in dtskrining)
    {
        radio+='<label class="label label-'+dtskrining[x]+'" style="padding:8px 9px;"><input type="radio" '+ ((dtskrining[x]==check) ? 'checked' : '') +'  name="kategoriskrining" value="'+dtskrining[x]+'"/> '+ ucwords(dtskrining[x]) +'</label> ';
//        console.log(radio);q
    }
    id.html(radio);
}

function ql_namabulan(bulan='')
{
  var namabulan = {1:"Januari",2:"Februari",3:"Maret",4:"April",5:"Mei",6:"Juni",7:"Juli",8:"Agustus",9:"September",10:"Oktober",11:"November",12:"Desember"};
  if(bulan == ''){
    return namabulan;
  }else{
    return  namabulan[bulan];
  }
}

function minus_sign_akuntansi(nominal)
{
    var number = parseInt(nominal);
    var result = 0;
    if(number<0)
    {
        var angka = number * -1;
        result = '('+convertToRupiah2(angka)+')';
    }
    else
    {
        result = convertToRupiah2(number);
    }
    return result;
}

function startTimeLayanan() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  m = checkTimeLayanan(m);
  s = checkTimeLayanan(s);
  
  var now = h + ":" + m + ":" + s;
//  $('#'+htmlElementId).val(now);
//  document.getElementById(htmlElementId).innerHTML =
//  h + ":" + m + ":" + s;
    //waktu sekarang
    var waktusekarang = h + ":" + m + ":" + s;
    $('#waktusekarang').val(waktusekarang);
    
    //waktulayanan
    var sekarang = waktusekarang;
    var mulai    = $('#waktumulai').val(); 
    hitungwaktu(mulai,sekarang);
    
  var t = setTimeout(startTimeLayanan, 1000);
}
function checkTimeLayanan(i) {
  if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
  return i;
}

function hitungwaktu(mulai,sekarang)
{
    var time1 = mulai;
    var time2 = sekarang;

    var hours=0;
    var minutes=0;
    var second=0;
//
    var splitTime1= time1.split(':');
    var splitTime2= time2.split(':');
//
    hours = parseInt(splitTime2[0]) - parseInt(splitTime1[0]) ;
    minutes = parseInt(splitTime2[1]) - parseInt(splitTime1[1]);
    second = parseInt(splitTime2[2]) - parseInt(splitTime1[2]);
    
    
//    hour = hour + minute/60;
//    minute = minute%60;
//    minute = minute + second/60;
//    second = second%60;
   
    var a = 0;
    var b = 0;
    
    if (mulai <= "12:00:00" && sekarang >= "13:00:00"){
       a = 1;
    }else {
       a = 0;
    }
      
    minutes = minutes.toString().length<2?'0'+minutes:minutes;
    if(minutes<0){ 
        hours--;
        minutes = 60 + minutes;        
    }
      
    second = second.toString().length<2?'0'+second:second;
    if(second<0){ 
        minutes--;
        second = 60 + second;        
    }
      
      hours = hours.toString().length<2?'0'+hours:hours;
      minutes = minutes.toString().length<2?'0'+minutes:minutes;
    
    var layanan = hours-a + ':' + minutes  + ':' + second;
    $('#waktulayanan').val( ((layanan=='NaN:NaN:NaN') ? '0:00:00' : layanan ) );
}


function setinputbyid(id,value)
{
    $('#'+id).val(value);
}

/**
 * Mencari Banyak Data Dengan select2
 * @param {type} selectId Select Id
 * @param {type} url Url
 */
function select2serachmulti(selectId,url)
{
  var hal=0;
  selectId.select2({
      allowClear: true,
      ajax: { 
       url: base_url+url, 
       type: "post", 
       dataType: 'json', 
       delay: 250,
       data: function (params) {
        ((params.term=='')?hal +=1:hal=0);
        return { searchTerm: params.term, page: hal, }; // serch value
       },
       
       processResults: function (response) {
         return { results: response,}; // display data
       },
       cache: true
      },
      escapeMarkup: function(markup) { // set html render
        return markup;
      },
     });
}

function setDatepicker(htmlElement,setDate='')
{
    htmlElement.datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((setDate=='') ? 'now' : setDate ) );
}

function setDatepickerMonth(htmlElement)
{
    htmlElement.datepicker({autoclose: true, format: "M yyyy", viewMode: "months", minViewMode: "months",orientation: "bottom"}).datepicker("setDate",'now');
}

function edit_dropdown(column,data,selected_data)
{
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].id==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].id +'" '+ selected +' >' + data[i].text +'</option>';
    }
    column.html(select);
}


//memberikan nilai form inputan berdasarkan nama 
function setvalinput(name,value){$('input[name="'+name+'"]').val(value);}
//memberikan nilai  berdasarkan id 
function setvaluebyid(id,value){id.val(value);}
/**Memberikan Isi di form input Select **/
function setselectvalue(id,idValue,textValue){id.empty();id.html('<option value="'+idValue+'">'+ ((textValue==null)?'Pilih..':textValue)+'</option>')};
// fungsi edit data enum (idhtml,dataenum,datayangterpilih)
function editdataenum(column,data,selected_data,addoption='') 
{
    var select= ((addoption=='')?'':'<option value="0">Pilih</option>');
    column.empty(); 
    for(var i in data){ select = select + '<option value="'+ data[i] +'" '+ ((data[i]==selected_data)? 'selected' : '' ) +' >' + data[i] +'</option>';}
    column.html(select);
}

// mahmud, clear
function nextdate(nextday,startDate='')
{
    var d = (startDate=='') ? new Date() : new Date(startDate);
    var numberMonth = [1,2,3,4,5,6,7,8,9,10,11,12];
    var textMonth = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    var nextime = new Date(d)
    var next= nextime.setDate(d.getDate() + nextday);
    var newdate = new Date(next);
    var result = newdate.getFullYear() + '-' + numberMonth[newdate.getMonth()] +'-'+ newdate.getDate();
    return result;
}

function editdataoption(column,data,selected_data,addoption='') // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{
    var select= addoption;
    column.empty();    
    for(var i in data){ select = select + '<option value="'+ data[i].id +'" '+ ((data[i].id==selected_data) ? 'selected' : '' ) +' >' + data[i].txt +'</option>';}
    column.html(select);
}
function startLoading(){ $("body").addClass("loading"); }
function stopLoading(){ $("body").removeClass("loading"); }
////---seting notifikasi
function notif(status, message){$.notify({icon: 'fa fa-bell fa-lg',message: '<strong>&nbsp;'+message+'</strong>'},{type: status,delay: 1100,timer: 400});return true;}
////---notifikasi field kosong / empty
function alert(message){$.alert({icon: 'fa fa-warning',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Warning!',content:message});}
function info(message){$.alert({icon: 'fa fa-info',theme: 'modern',closeIcon: true,animation: 'scale',type: 'blue',title: 'Info!',content:message});}
function alert_empty(value){$.alert({icon: 'fa  fa-warning',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Warning!',content: 'Harap isi dengan benar '+value+'.!'});}
function notif_confirminfo(message){$.confirm({icon: 'fa fa-info',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Informasi',content: message,buttons: {confirm: function () { } } });}
////---QRCODE
function buatqrCode () 
{      
var qrcode = new QRCode(document.getElementById("qrcode"),{width : 200,height : 200});
    var elText = document.getElementById("textqrcode");
    if (!elText.value) 
    {
        alert_empty('code to generate');
    }
    else
    {
        qrcode.makeCode(elText.value);
        setTimeout(function(){
            fungsi_cetaktopdf( document.getElementById('qrcode').innerHTML ,'<style>body{  margin: auto;}</style>');
            $("#textqrcode").val('');
            $("#textqrcode").focus();
        },2); 
    }
}
$("#textqrcode").
on("keydown", function (e) {
    if (e.keyCode == 13) {
        buatqrCode();
    }
});
/////////////////END QRCODE///////////////////////
///---print to pdf
function fungsi_cetaktopdf(content,style=''){
    w = window.open();
    w.document.write(style);
    w.document.write(content);
    w.document.write('<script type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</script>');
    w.document.close();
    w.focus(); 
}
///--jika terpilih (if select) pada dropdown
function if_select(id,selected){  var selectoption=''; if(id==selected){return selectoption='selected';}else{return selectoption='';}}
function formatTgltoSql(value)//fungsi tampil tanggal, format value: dd/mm/yyyy
{   
    var v = value.split("/");
    var tanggal = v[2]+'-'+v[1]+'-'+v[0];
    return tanggal;
}
function ambiltanggal(value)//fungsi tampil tanggal, format value: dd/mm/yyyy
{   
    var v = value.split("/");
    var tanggal = v[2]+'-'+v[1]+'-'+v[0];
    return tanggal;
}
function ambiltanggal2(value)//fungsi tampil tanggal, format value: dd-mm-yyyy
{   
    var tanggal = new Date(value);
    return tanggal.getFullYear()+'-'+ (tanggal.getMonth()+1) +'-'+ tanggal.getDate();
}
function tglformatindo1(value)
{
    var array = value.split("-"); // format awal yyyy-mm-dd
    tanggal = array[2]+'/'+array[1]+'/'+array[0]; //// format ditampilkan dd/mm/yyyy
    return tanggal; 
}
///////---fungsi tampil waktu
function ambilwaktu(value){tanggal = new Date(value); tanggal = tanggal.getHours()+':'+ tanggal.getMinutes(); return tanggal;}
//---Notifikasi
///////---fungsi pesan gagal
function fungsiPesanGagal(){stopLoading();return notif('danger', 'Error while get or update data, Please contact developer..!');}
function pesanUndefinedLocalStorage(){return $.alert('<p style="text-align:center"><br><b>Your browser does not support Web Storage</b><br><br> Please update your browser..!</p>');}
function pesanBrowserLocalStorage(){if (typeof(Storage) === "undefined") {return $.alert('<p style="text-align:center"><br><b>Your browser does not support Web Storage</b><br><br> Please update your browser..!</p>');}}
function notifExlamation(msg,script)
{
    $.confirm({icon: 'fa fa-exclamation',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Peringatan!',content: msg,buttons: {confirm: function () { script }}});
}
function notifConfirmation(msg,script)
{
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi!',content: msg,buttons: {confirm: function () { script }}});
}
function qlReloadPage(time){return setTimeout(function(){ window.location.reload(true); },time);}
/////---tanda penghubung
function tandaPenghubung(nilai,tanda){var x = '';if(nilai!=''){return x = tanda;}else{return x = '';}}
//////---cek jika kosong
function if_empty(nilai){var x = '';if(nilai > 0 || nilai !=null){return x = nilai;}else{return x = '';}}
//////--- Seting Warna
function qlwarna(x){var color = ['gray','green','red','maroon','blue','purple','aqua','orange','olive','yellow'] ; while ( x > 9) {x = x-9;} return  color[x];}
//// -- ambil data poli atau unit
function qlstatuswarna(status)
{ 
    var color = {diterima:'background-color:#03a9f442;',selesai:'background-color:#e0f0d8;',batal:'background-color:#f0D8D8;',tunda:'background-color:#F7E59E;'};
    return  color[""+status+""];
}
function qlstatuswarna2(status)
{ 
    var color = {1:'background-color:#ffc107;',2:'background-color:#ffeb3b;',3:'background-color:#cddc39;'};
    return  color[""+status+""];
}
function qlstatuswarnapp(status)
{ 
    var color = {2:'background-color:#e0f0d8;',3:'background-color:#f0D8D8;',4:'background-color:#b7d4ed;' } ; return  color[status];
}
function get_datapoli(getId,x='')
{
    var tampildt='<option value="0">Farmasi</option>';
    $.ajax({ 
        url: base_url+'cmasterdata/get_datapoli',type : "post", dataType : "json",
        success: function(result) {
            $(getId).empty();
            for (i in result){
                tampildt +='<option value="'+result[i].idunit+'" '+if_select(result[i].idunit,x)+'>'+result[i].namaunit+'</option>';
            }
            $(getId).html(tampildt);
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    }); 
}
function dropdown_getdata(htmlSelectId,url,valueId='',option='')
{
    var tampildt=((option=='') ? '<option value="0">Pilih</option>' : option);
    $.ajax({ 
        url: base_url + url,type : "post", dataType : "json",
        success: function(result) {
            $(htmlSelectId).empty();
            for (i in result){
                tampildt +='<option value="'+result[i].id+'" '+if_select(result[i].id,valueId)+'>'+result[i].txt+'</option>';
            }
            $(htmlSelectId).html(tampildt);
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    }); 
}
//// -- ambil data poli atau unit
function get_databangsal(getId,x='',tipe='')
{
    var tampildt='<option value="0">Bangsal</option>';
    $.ajax({ 
        url: base_url+'cmasterdata/get_databangsal',
        type : "post",
        data:{tipe:tipe},
        dataType : "json",
        success: function(result) {
            $(getId).empty();
            for (i in result)
            {
                tampildt +='<option value="'+result[i].idbangsal+'" '+if_select(result[i].idbangsal,x)+'>'+result[i].namabangsal+'</option>';
            }
            $(getId).html(tampildt);
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    }); 
}
//// -- ambil data poli atau unit
function get_dataasalobatbhp(getId,x='')
{
    //ini memang begini karena ada bugs
    //tapi entah kenapa tidak ngisi select
    $.ajax({
        url: '../cmasterdata/kosongan',
        type : "post",      
        dataType : "json",
        succcess: function(result){
            $(getId).empty();
            $("#"+getId).append('<option value="dalam" '+if_select("dalam",x)+'>Tagihan</option>');
            $("#"+getId).append('<option value="luar" '+if_select("luar",x)+'>Mandiri, Tagihan</option>');
            $("#"+getId).append('<option value="luarlunas" '+if_select("luarlunas",x)+'>Mandiri, Non Tagihan</option>');
        },
        error: function(result){
            $(getId).empty();
            $("#"+getId).append('<option value="dalam" '+if_select("dalam",x)+'>Tagihan</option>');
            $("#"+getId).append('<option value="luar" '+if_select("luar",x)+'>Mandiri, Tagihan</option>');
            $("#"+getId).append('<option value="luarlunas" '+if_select("luarlunas",x)+'>Mandiri, Non Tagihan</option>');
        }
    });
   
}
/// -- BUTTON PANE DISPLAY WHEN HOVER TABLE ROW
function button_pane()
{
    $('#table tbody tr').hover(function() {
    $(this).addClass('hover');

    var rowP = $(this).offset();
    var rowH = $(this).height();
    var offset = rowP.top + (rowH - 132);
    var left = $('td:first', $(this)).width() + 30;

    $('td:first', $(this)).prepend($('.button-pane'));
    $('.button-pane', $(this)).css({
      'margin-top': offset + 'px',
      'margin-left': left + 'px'
    }).show();

  }, function(event) {
    $(this).remove('.button-pane');
    $(this).removeClass('hover');
    $('.button-pane').hide();
  });
}
/**
 * Convert To Rupiah Input Number
 * @param idName HTML <input> element id and HTML <input> element name
 */
function converToRupiahInput(idName)
{
    $(document).on('input','#'+idName,function(){
      $('input[name="'+idName+'"]').val(convertToRupiah(unconvertToRupiah(this.value)));
    })
}

// - Fungsi replace 4 angka dari belakang
function hapus3digitkoma(value)
{
    return value.replace(/.000/g,"");
}
function convertToRupiahBulat(angka)
{
    var rupiah = '';        
    var angkarev = bulatkanRatusan(angka).toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    var result =  rupiah.split('',rupiah.length-1).reverse().join('');
    return result;
}

/* Fungsi formatRupiah */
function convertToRupiah(angka,split='')
{
    var rupiah = '';

    if(split!=''){angka = angka.toString().split('.')[0]; }
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    var result= rupiah.split('',rupiah.length-1).reverse().join('');
    result = ((result==0) ? '' : result);
    return result;
}

/* Fungsi formatRupiah */
function convertToRupiah2(angka,split='')
{
    var rupiah = '';

    if(split!=''){angka = angka.toString().split(',')[0]; }
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
    var result= rupiah.split('',rupiah.length-1).reverse().join('');
    result = ((result==0) ? '' : result);
    return result;
}

function unconvertToRupiah2(rupiah)
{
    return ifnull(rupiah, '0').split(',').join('');
}

/* Fungsi formatRupiah */
function convertToRupiahDesimal(angka,split='')
{
    var rupiah = '', joinAngka='';

    if(split!=''){joinAngka = ','+angka.toString().split('.')[1]; angka = angka.toString().split('.')[0]; }
    var angkarev = angka.toString().split('').reverse().join('');
    for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
    var result= rupiah.split('',rupiah.length-1).reverse().join('');
    return result +joinAngka;
}

function unconvertToRupiah(rupiah)
{
    return ifnull(rupiah, '0').split('.').join('');
}

function bulatkanRatusan(angka)
{
    return Math.ceil(angka / 100) * 100;
}

function bulatkanLimaRatusan(angka)
{
    return Math.ceil(angka / 500) * 500;
}
// is NaN
function isnan(x) {
  if (isNaN(x)) x = 0;
  return x * 1;
}
// pembulatan angka
function bulatkan(angka)
{
    return Math.round(angka);
}
// format angka desimal 
function angkadesimal(angka,digit)
{
    return parseFloat(Math.round(angka * 100) / 100).toFixed(digit);
}
////////////////////////// -- change input number text field
function NumberInput(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    if(textbox){
        textbox.addEventListener(event, function() {
          if (inputFilter(this.value)) {
            this.oldValue = this.value;
            this.oldSelectionStart = this.selectionStart;
            this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
            this.value = this.oldValue;
            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          }
        });
    }
  });
}
// -- hanya angka
function setInputAngka(getId){ NumberInput(document.getElementById(getId), function(value) {return /^\d*[.,]?\d*$/.test(value); });}
///////////////////////// -- end change input number text field 

// -- ambil data dokter
function tampilDataDokter(htmlTagName)
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url+'cadmission/masterjadwal_caridokter', //load controller
        type:'POST',
        dataType:'JSON',
        success: function(result){
            htmlTagName.empty();
            for(i in result){ select = select + '<option value="'+ result[i].idpegawai +'" >' +result[i].namalengkap +'</option>'; }
            htmlTagName.html(select);
            $('.select2').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}

// -- ambil data icd
function tampilDataIcd(htmlTagName, jenisicd,idinap)
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url+'cpelayananranap/cariicdranap', //load controller
        type:'POST',
        dataType:'JSON',
        data:{ji:jenisicd,idinap:idinap},
        success: function(result){
            htmlTagName.empty();
            for(i in result) { select = select + '<option value="'+ result[i].icd +'" >' + result[i].namaicd + '</option>'; }
            htmlTagName.html(select);
            $('.selectDataICD').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}

// -- ambil data icd
function tampilDataIcdpaket(htmlTagName, jenisicd)
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url+'cpelayananranap/pemeriksaanranap_cariicdpaket', //load controller
        type:'POST', dataType:'JSON', data:{ji:jenisicd},
        success: function(result){
            htmlTagName.empty();
            for(i in result){ select = select + '<option value="'+ result[i].idpaketpemeriksaan +'" >' + result[i].namapaketpemeriksaan + '</option>'; }
            htmlTagName.html(select);
            $('.select2').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
// -- ambil data bangsal
function tampilDataBangsal(htmlTagName, selected)
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url+'cmasterdata/get_databangsal', //load controller
        type:'POST', dataType:'JSON',
        success: function(result){
            htmlTagName.empty();
            for(i in result){ select = select + '<option value="'+ result[i].idbangsal +'" ' + ((result[i].idbangsal===selected)?'SELECTED':'') + '>' + result[i].namabangsal + '</option>'; }
            htmlTagName.html(select);
            $('.select2').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}

// -- ambil data pasien rawat inap
function tampilDataRawatinap(htmlTagName, selected)
{
    var select='<option value="0">Pilih Pasien</option>';
    $.ajax({
        url:base_url+'cpelayananranap/carirawatinap', //load controller
        type:'POST', dataType:'JSON',
        success: function(result){
            htmlTagName.empty();
            for(i in result){select = select + '<option value="'+ result[i].idinap +'" ' + ((result[i].idinap===selected)?'SELECTED':'') + '>' + result[i].datainap + '</option>';}
            htmlTagName.html(select);
            $('.select2').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
// -- ambil data jadwal poli
function tampilDataPoliklinik(htmlTagName,date)
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url+'cpelayanan/caripoliklinik', //load controller
        type:'POST',dataType:'JSON',data:{date:date},
        success: function(result){
            htmlTagName.empty();
            for(i in result){ select = select + '<option value="'+ result[i].idjadwal +'" >' + result[i].namaunit +' '+ result[i].namadokter +' (' +result[i].tanggal+')</option>'; }
            htmlTagName.html(select);
            $('.select_jadwalralan').select2();
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
// -- ubah format tanggal di js dari 20/12/2019  >>> 2019-12-20
function tglubah_format1(value)
{
    var tgl = value.substr(6), tgl2= value.substr(3,2), tgl3 = value.substr(0,2);
    return tgl+'-'+tgl2+'-'+tgl3;
}
function is_null(value){ return is_undefined(value); }
function is_empty(value){ return is_undefined(value); }
function is_undefined(value){ return (value == undefined) || (value == '') || (value == null) || (value == 'null'); }
function if_null(value, alternative=''){ return if_undefined(value, alternative); }
function if_empty(value, alternative=''){ return if_undefined(value, alternative); }
function if_undefined(value, alternative=''){if(is_undefined(value)){return alternative;}else{return value;}}
function startLoading(){ $("body").addClass("loading"); }
function stopLoading(){ $("body").removeClass("loading"); }

function fungsi_tampilloket(selected,idunit='') //panggil fungsi tampil loket
{
    var select='';
    $.ajax({
        url:base_url+'cadmission/masterjadwal_cariloket',
        type:'POST',
        data:{idunit:idunit},
        dataType:'JSON',
        success: function(result){
            $('select[name="loket"]').empty();
            for(i in result){select = select + '<option '+if_select(result[i].idloket,selected)+' value="'+ result[i].idloket +'" >'+ result[i].namaloket +'</option>';}
            $('select[name="loket"]').html('<option value="0">Pilih</option>' + select + '</select>');
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }
    }); 
}
function fungsi_tampiljadwalgrup(selected) //panggil fungsi tampil loket
{
    var select='';
    $.ajax({
        url:base_url+'cadmission/masterjadwal_carijadwalgrup',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="jadwalgrup"]').empty();
            for(i in result){select = select + '<option '+if_select(result[i].idjadwalgrup,selected)+' value="'+ result[i].idjadwalgrup +'" >'+ result[i].namagrup +'</option>';}
            $('select[name="jadwalgrup"]').html('<option value="0">Tidak Ada Grup</option>' + select + '</select>');
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }
    }); 
}
function window_open(url){return window.open(base_url+url);}
function buatQrPasien(data)
{
    var code = new QRCode(document.getElementById("qrcode"),{width : 88,height : 88});
    $.ajax({
        url:base_url+"cadmission/getdt_kartupasien",
        data:{id:data},
        dataType:"JSON",
        type:"POST",
        success:function(result){
            code.makeCode(result['norm']);
            setTimeout(function(){
            var qrPasien = document.getElementById('qrcode').innerHTML;
            
            var cetak = '<div style="position:relative;text-align:left;height:5,350cm;widht:9,500cm">'//
            +'<img style="position:absolute;left:0;top:0;" >'// /src="'+base_url+'/assets/images/krtpasien.svg"
            +'<br><br>'
            +'<table width="50%" padding="0" style="z-index:100; margin-top:20px; position:absolute;margin-top:3px;margin-left:10px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'
            +'<tr><td width="20%;" >'+qrPasien+'</td><td colspan="3" align="left"><div style="width:100%; padding-left:5px;font-size:13px;" ><b>'+result['namalengkap']+'</b><br><span style="font-size:11.7px;"><b>'+result['tanggallahir']+'</b></span><br><span style="font-size:11px;">'+result['jeniskelamin']+'</span></div></td></tr>'
            +'<tr><td width="20%;" align="center"><b>'+result['norm']+'</b></td><td></td><td></td></tr>'
            +'</table></div>';
            fungsi_cetaktopdf(cetak,'<style>/*@page {size:9cm;margin:none;}*/</style>');
            },90);
//            hitungcetakkartu_berobat(result['norm']);//fungsi hitung cetak kartu pasien
            setTimeout(function(){$('#qrcode').empty();},100);
        }, 
        error:function(){ }
    });
}
function hitungcetakkartu_berobat(norm)
{
    $.ajax({
        url:base_url+"cadmission/hitungcetakkartu_berobat",
        data:{id:norm},dataType:"JSON",type:"POST",
        success:function(result){ }, 
        error:function(){ }
    });
}
function qlbulan(x)
{
    var hasil = x-1;
    var dt = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
    return dt[hasil];
}
// explode dengan mengambil array tertentu
function explode_getdt(str,split,getnoarray='')
{
    var myStr = str;
    var strArray = myStr.split(split);
    var result = strArray[getnoarray];
    return result;
}
/*Split to array*/
function split_to_array(data){return data.split(',');}
/*explode dengan mengganti tanda*/
function explode_replace(str,split,replace='')
{
    var myStr = str;
    var split = ((str==='')?'':split);
    var strArray = myStr.split(split);
    var result ='';
    for(var i = 0; i < strArray.length; i++){ result+= replace + strArray[i];}
    return result;
}
// ambil usia
function ambilusia(date1, date2)
{
    var dt1 = new Date(date1);
    var dt2 = new Date(date2);
    var datediff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    var tahun = parseInt(datediff/360);
    // var bulan = parseInt(tahun%360);
    return tahun+' tahun ';
}
// 
function datediff(date1, date2)
{
    var dt1 = new Date(date1);
    var dt2 = new Date(date2);
    var datediff = Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
    return datediff;
}
// buat antrian farmasi
function farmasi_buatantrian(value)
{
    if(value!='')
    {
        $.ajax({
            url:base_url+"cantrian/farmasi_buatantrian",
            type:"POST",dataType:"JSON",data:{norm:value},
            success: function(result){
                if(result.status=='danger'){notif(result.status,result.message);return;}
                var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                      <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                      '<tr><td><b style="font-size:16px;">'+result.loket['namaloket']+' </b></td></tr>'+
                      '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ result.no +'</span></td></tr>'+
                      '<tr><td><span>'+ result.datapasien['norm'] +'</span></td></tr>'+
                      '<tr><td><span>'+ result.datapasien['namalengkap'] +'</span></td></tr>'+
                      '<tr><td><div>'+ result.datapasien['alamat'] +'</td></tr>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.1cm;}</style>');
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
// fungsi panggil antrian order ugd
function panggilOrderUGD()
{
    $.ajax({
        url:base_url+"cnotifikasi/notif_antrianugd",
        type:"POST",dataType:"JSON",
        success: function(result){
            var dt = '', antri=0;
            for(var x in result){ antri += parseInt(result[x].antri); dt+= '<div>Loket '+result[x].namaloket+' antri '+result[x].antri+' total '+result[x].antriandiambil+' </div>'; }
            if(antri!==0){ $('#infopanggilan').empty();$('#infopanggilan').html('<audio  src="'+base_url+'assets/departure.mp3" autoplay></audio>'); notif('info',dt); }
        },
        error:function(result){
            fungsiPesanGagal(); return false;
        }
    })
}
// panggil order rm
function panggilOrderRM()
{
    var tglawal = new Date(), tglakhir = new Date();
    $.ajax({
        url: base_url+"cadmission/loaddtarusrekamedis",
        type: "POST",dataType: "JSON",data : {tgl1:tanggaljs(),tgl2:tanggaljs(), status:'2' },
        success: function(result) {
            var no=0, data='';
            for(x in result){ no++; }
            data+= no+' Berkas RM dalam status order..! '; 
            if(no!==0){ $('#infopanggilan').empty();$('#infopanggilan').html('<audio  src="'+base_url+'assets/departure.mp3" autoplay></audio>'); notif('info',data); }
        },
        error: function(result) {
            fungsiPesanGagal(); return false;
        }
  });
}


//hari nasional
function harinasional(getDay)
{
    var Days = {
        1:'Senin', 
        2:'Selasa', 
        3:'Rabu',
        4:'Kamis',
        5:'Jumat',
        6:'Sabtu',
        7: 'Minggu',
        8: 'Hari Libur Nasional'
    };
    var result = Days[getDay];
    return result;
}


$(document).on("click","#pemeriksaanklinikPelayananFarmasi", function(){ //pemeriksaan klinik
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        localStorage.removeItem("idp");
        
        var idperiksa = $(this).attr("alt");
        $.ajax({
            url:base_url+"cnotifikasi/update_pelayananfarmasi",
            type:"POST",
            dataType:"JSON",
            data:{i:idperiksa},
            success: function(result){
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
        
        localStorage.setItem("idperiksa",$(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idp", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_periksa';
    } else {
         pesanUndefinedLocalStorage();
    }
});

// fungsi notifikasi farmasi
function panggilNotifFarmasi()
{
    $.ajax({
        url:base_url+"cnotifikasi/notif_pelayananfarmasi",
        type:"POST",
        dataType:"JSON",
        success: function(result){
            var no=0, dt = '';
            for(x in result)
            {
                no++;
                dt += '<li><a id="pemeriksaanklinikPelayananFarmasi" alt="'+result[x].idpemeriksaan+'" idp="'+result[x].idpendaftaran+'" norm="'+result[x].norm+'" href="#" class="small" '+tooltip('Klik untuk konfirmasi dilayani.')+'><i class="fa fa-check-square text-primary"></i> '+ result[x].norm +' '+ result[x].namapasien +'</a></li>';
            }
            
            if(no!==0){
                $('#header_notiflayananfarmasi').text(no +' pasien telemedicine belum dilayani.');
                $('#label_notiflayananfarmasi').text(no);
                $('#body_notiflayananfarmasi').html(dt);

                $('#infopanggilan').empty();
                $('#infopanggilan').html('<audio  src="'+base_url+'assets/farmasi.wav" autoplay></audio>');
            }
            else
            {
                $('#header_notiflayananfarmasi').text('');
                $('#label_notiflayananfarmasi').text('');
                $('#body_notiflayananfarmasi').html('');
                $('#infopanggilan').empty();
            }
            $('[data-toggle="tooltip"]').tooltip();
            // setInterval($('#infopanggilan').empty(), 5000);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

// fungsi panggil notif Gudang
function panggilNotifGudang()
{
    $.ajax({
        url:base_url+"cnotifikasi/notif_gudang",
        type:"POST",
        dataType:"JSON",
        success: function(result){
            var no=0, dt = '';
            for(x in result)
            {
                dt = result[x].jumlah + ' Keterangan Farmasi Dokter untuk hari ini belum di List ';
                no +=parseInt(result[x].jumlah);
            }
            if(no!==0){
                $('#infopanggilan').empty();$('#infopanggilan').html('<audio  src="'+base_url+'assets/departure.mp3" autoplay></audio>');
                notif('info',dt);
            }
            // setInterval($('#infopanggilan').empty(), 5000);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    })
}

function waktujs()
{
    var clientTime = new Date();
    //buat object date dengan menghitung selisih waktu client dan server
    var time = new Date(clientTime.getTime());
    //ambil nilai jam
    var sh = time.getHours().toString();
    //ambil nilai menit
    var sm = time.getMinutes().toString();
    //ambil nilai detik
    var ss = time.getSeconds().toString();
    //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
    var clock = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
    return clock;
   
}
function tanggaljs()
{
    var clientTime = new Date();
    var tanggal =  clientTime.getFullYear() + '-' + parseInt(clientTime.getMonth()+1) +'-'  + clientTime.getDate();
    return tanggal;
   
}
function ql_identitas(nama)
{
    var result = nama.toLowerCase();
    var left = result.substr(0, 3);
    var right = result.substr(-3);
    if(left=='an ' || left=='nn ' || left=='ny ' || left=='sdr ' || left=='tn ') { result = result.replace(left,''); }
    if(right==' an' || right==' nn' || right==' ny' || right==' sdr' || right==' tn') { result = result.replace(right,''); }
    // if(right==' an' || right==' nn' || right==' ny' || right==',sdr' || right==',tn' || ' an' || right==',nn' || right==',ny' || right==',sdr' || right==',tn') { result =  result.replace(right,'');}
    result = result;
    return result.toLocaleUpperCase();
}
function toCamelCase(str)
{
    var arr = str.toLowerCase().split(' ');
    var words = arr.filter(v=>v!='');
    words.forEach((w, i)=>{
        words[i] = w.replace(/\w\S*/g, function(txt){
            return txt.charAt(0).toUpperCase() + txt.substr(1);
        });
    });
    return words.join(' ');
}
function toSubstringRight(string,delimiter)
{
    var str = string.substring(parseInt(string.indexOf(delimiter))+1,string.length);
    return str;
}
// mahmud, clear
function tampilpilihloket(selectId,idstasiun)
{
    $.ajax({
        type: "POST",
        url: base_url+"cmasterdata/tampil_loketbystasiun",
        dataType: "JSON",
        data : {stasiun:idstasiun},
        success: function(result) {
            var dtloket = '';
            for(x in result)
            {
                dtloket+='<option value="'+result[x].idloket+'">'+result[x].namaloket+'</option>';
            }
            selectId.empty();
            selectId.html(dtloket);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}
function getAge(dob) {
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }

    return age;
}
function getNextDate(date,valDay)
{   
    if(date=='0000-00-00' || date==null || date==undefined || date== '')
    {return '';}
    else
    {
    var nextDate = new Date(date);
    var result = new Date(nextDate.setDate(nextDate.getDate() + valDay));
        result = result.getDate()+'/'+ (result.getMonth()+1) +'/'+result.getFullYear();
    return result;
    }
}
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("menuBacktoTop").style.display = "block";
  } else {
    document.getElementById("menuBacktoTop").style.display = "none";
  }
}
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
//mahmud, clear :: verifikasi pasien
function verifikasi_pendaftaran(value='')
{
    if(!value=='')
    {
        verifpendaftaran(value);
    }
    else
    {
        notif('danger','No.Identitas harus diisi..!');
        $('#noidentitas').focus();
        $('#noidentitas').css('background-color','#ffedeb');
    }
}

function verifpendaftaran($norm)
{
    $.ajax({
        type: "POST",
        url: base_url+"cantrian/cek_verifdaftarperiksa",
        dataType: "JSON",
        data : {rm:norm},
        success: function(result) {
            console.log(result);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}
function ql_replacebr(str)
{
    if(str!==''){

    // var str = ;
    var regex = /<br\s*[\/]?>/gi;
    return str.replace(regex, "");
    }
}
function includejs(namafile)
{
    var data = document.createElement('script');
    data.src = base_url+namafile;
    return document.head.appendChild(data);
}
function tooltip(title){return 'data-toggle="tooltip" data-original-title="'+title+'"';}
function cetakEtiket(result, value)
{
      var cetak = '<div style="width:5.5cm;height:5cm;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;float: center;text-align:center;"><img style="width:4cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n\
                    <table border="0" style="font-size:small;text-align:center;float:center;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
      cetak += "<tr style='margin-bottom:2px;font-size:10.8px;'><td colspan='4' align='left'><b>Apoteker : Nurun Ni'mah B,.S. Farm,.Apt.</b></td></tr>";
      cetak += "<tr style='font-size:8px;'><td colspan='4'>=============================================</td></tr>";
      cetak += "<tr style='font-size:10px;solid #000;'><td colspan='2' align='left'>"+result['norm']+"</td><td align='right'>Tgl :"+result['tglperiksa']+"</td><td></td></tr>";
      cetak += "<tr style='font-size:11px;'><td colspan='4' align='left'>"+toCamelCase(result['namalengkap'])+"<br></td></tr>";
      cetak += "<tr style='font-size:8px;'><td colspan='4'>=============================================</td></tr>";
      if(value!==''){
      cetak += "<tr style='font-size:12px;'><td colspan='4'>"+ ((result['grup']=='0')? result['namaobat'] : '') +"</td></tr>";
      cetak += "<tr style='font-size:14.5px;'><td colspan='4'><b>"+result['aturanpakai']+"</b></td></tr>";
      cetak += "<tr style='font-size:12px;'><td colspan='4'>"+((result['penggunaan']!=='')?result['penggunaan']+' Makan':'')+"</td></tr>";
      }else{
          cetak += "<tr style='font-size:12px;'><td colspan='4'>"+ $('textarea[name="aturanpakai"]').val() +"</td></tr>";
      }
      cetak +='</table></div><div style="width:5.5cm;height:1cm;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000; float: center;text-align:center;"><table style="font-size:small;text-align:center;float:center;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
      cetak += "<tr style='font-size:12px;'><td style='padding-left:30px;'>Pagi / Siang / Sore / Malam</td></tr>";
      cetak += "<tr style='font-size:12px;'><td style='padding-left:30px;'>Semoga Lekas Sembuh</td></tr>";
      cetak += '</table></div>';
      fungsi_cetaktopdf(cetak,'<style>@page {size:6cm 7cm;margin:0.1cm;}</style>');
}

/**
 * Cetak Resep 
 * @type grupresep
 * @type obat
 * @type fontstyle
 * @type aslin
 */
function periksa_cetakresep(grupresep,obat, fontstyle='',salin='')
{
    var cetakresep='';
    for(x in obat){
        var resep = obat[x];
        totaldiresepkan = (resep.kekuatanperiksa/resep.kekuatan) * resep.jumlahdiresepkan;
        ((resep.grup==0 || grup!==resep.grup)? no=1 : no +=1  );
        
        var txtresep = ((resep.grup==0 || grup!==resep.grup)?'R/':'&nbsp;&nbsp;&nbsp;')+"  "+resep.namabarang+ ', jumlah: ' +((resep.jumlahRacikan!=null)? (totaldiresepkan/resep.jumlahRacikan).toFixed(3) : totaldiresepkan ) + ' '+ ((resep.grup==='0')?((resep.pemakaian===null)?'': resep.pemakaian ):'');
        var txtsigna = "&nbsp;&nbsp;&nbsp;&nbsp;"+((resep.signa===null)?'':resep.signa);
        var txtkemasangrup ="&nbsp;&nbsp;&nbsp;&nbsp;<i>"+resep.kemasan+" dtd no "+resep.jumlahRacikan;
        var txtsignagrup = "&nbsp;&nbsp;&nbsp;&nbsp;"+((resep.signa===null)?'':resep.signa);
        
        cetakresep +=  "<tr "+((fontstyle==='')?"style='font-size:10px;'":'')+"><td colspan='2'>"+ txtresep + (( (resep.grup==0 || grup!==resep.grup) && salin !='')?"&nbsp;&nbsp;<a id='salinriwayatresep' grup='"+resep.grup+"' idbp='"+resep.idbarangpemeriksaan+"' resep='"+txtresep+"' signa='"+txtsigna+"' kemasangrup='"+txtkemasangrup+"' signagrup='"+txtsignagrup+"' class='btn btn-primary btn-xs'> Salin Resep</a>" : "")  +" </td></tr>";
        
        if (resep.grup==0)
        {
            cetakresep +="<tr "+((fontstyle==='')?"style='font-size:10px;'":'')+"><td colspan='2'>"+txtsigna+"</td></tr>";
        }
        
        for(i in grupresep)
        {
            if(resep.grup!=0 && grupresep[i].jumlahgrup==no && resep.grup==grupresep[i].grup )
            {
                cetakresep +="<tr "+((fontstyle==='')?"style='font-size:10px;'":'')+"><td colspan='2'>"+txtkemasangrup+"</i></td></tr>";
                cetakresep +="<tr "+((fontstyle==='')?"style='font-size:10px;'":'')+"><td colspan='2'>"+txtsignagrup+"</td></tr>";
            }
        }
        var grup = resep.grup;
    }

    return cetakresep;
}
function ql_readonlytxt(param,status)
{
    return ((param == status)?'readonly':'');
}

// 
function tanggaldatepicker(format=null,date=null)
{
    $('.datepicker').datepicker({autoclose: true,format: ((format==null)? "yyyy-mm-dd" : format ),orientation: "bottom"}).datepicker("setDate", ((date==null)? 'now' : date ) );
}

// isi dropdown / select box
//  id= id tag html, list = data yg ditampilkan, pilih = data yang terpilih, fo = first option label
function ql_isicombo(id,list,pilih,fo)
{
    var html ='<option value="0">'+fo+'</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idcombo==pilih){ selected='selected'; }
        else{selected='';}
        html = html + '<option value="'+ data[i].idcombo +'" '+ selected +' >' + data[i].namacombo +'</option>';
    }
    column.html(html);
}
//menambahkan data ke localstorage
function addlocalstorage(name,add)
{
//    contoh menambahkan data di local storage
//    var arrdata = split_to_array(value);
//    var dataobat = JSON.stringify({
//        idbarang  :arrdata[0],
//        kode      :arrdata[1],
//      });
//  addlocalstorage('lsbarangpembelian',dataobat);  
//  notif('success','Tambah barang berhasil..!');
//  tampilbarangpembelian(getlocalstorage('lsbarangpembelian'));
  var datalama = JSON.parse(localStorage.getItem(name));//converts string to object
  if(datalama == null){datalama = [];} //jika data null
  datalama.push(add);
  return localStorage.setItem(name, JSON.stringify(datalama));
}
//mengubah data ke localstorage
function updatelocalstorage(name,edit)
{
//    function ubahdata(index)
//    {
//    contoh mengubah data local storage
//    var dataobat = getlocalstorage('lsbarangpembelian');
//        dataobat[index] = JSON.stringify({
//        idbarang  :$('#idbarang'+index).val(),
//        kode      :$('#kode'+index).val(),
//        kadaluarsa:$('#kadaluarsa'+index).val(),
//      });
//    updatelocalstorage('lsbarangpembelian',dataobat);
//    tampilbarangpembelian(getlocalstorage('lsbarangpembelian'));
//    }
  return localStorage.setItem(name, JSON.stringify(edit));
}
/*ambil data localstorage*/
function getlocalstorage(name){ return JSON.parse(localStorage.getItem(name));/*converts string to object*/}
/*hapus data local storage*/
function removelocalstorage(name){return localStorage.removeItem(name);}
/*copy atau salin local storage by index key*/
function copylocalstorage(name,index){return addlocalstorage(name,getlocalstorage(name)[index]);}
function ifempty(value,alternatif)
{
    if(value=='' || value==undefined || is_empty(value) || is_null(value)){return alternatif;}
    else{ return value;}
}
function ifnull(value, alternative){return ifempty(value, alternative);}
/* mahmud, clear -> LIST ENUM DATA*/
function list_enum(column,data,selected,option='')
{
    var option= ((option=='')?'':'<option value="">Pilih</option>');
    $('#'+column).empty();
    for(i in data){option = option + '<option value="'+ data[i] +'" '+ ((data[i]==selected)?'selected':'') +' >' + data[i] +'</option>';}
    $('#'+column).html(option);
}



function generateUUID()
{
    return UUID.genV1();
}

function generateShortUUID()
{
    return UUID.genV1().toString().substring(0, 8);
}

/**
 * UUID.js - RFC-compliant UUID Generator for JavaScript
 *
 * @file
 * @author  LiosK
 * @version v4.2.5
 * @license Apache License 2.0: Copyright (c) 2010-2020 LiosK
 */

/**
 * @class
 * @classdesc {@link UUID} object.
 * @hideconstructor
 */
var UUID;

UUID = (function(overwrittenUUID) {
"use strict";

// Core Component {{{

/**
 * Generates a version 4 UUID as a hexadecimal string.
 * @returns {string} Hexadecimal UUID string.
 */
UUID.generate = function() {
  var rand = UUID._getRandomInt, hex = UUID._hexAligner;
  return  hex(rand(32), 8)          // time_low
        + "-"
        + hex(rand(16), 4)          // time_mid
        + "-"
        + hex(0x4000 | rand(12), 4) // time_hi_and_version
        + "-"
        + hex(0x8000 | rand(14), 4) // clock_seq_hi_and_reserved clock_seq_low
        + "-"
        + hex(rand(48), 12);        // node
};

/**
 * Returns an unsigned x-bit random integer.
 * @private
 * @param {number} x Unsigned integer ranging from 0 to 53, inclusive.
 * @returns {number} Unsigned x-bit random integer (0 <= f(x) < 2^x).
 */
UUID._getRandomInt = function(x) {
  if (x < 0 || x > 53) { return NaN; }
  var n = 0 | Math.random() * 0x40000000; // 1 << 30
  return x > 30 ? n + (0 | Math.random() * (1 << x - 30)) * 0x40000000 : n >>> 30 - x;
};

/**
 * Converts an integer to a zero-filled hexadecimal string.
 * @private
 * @param {number} num
 * @param {number} length
 * @returns {string}
 */
UUID._hexAligner = function(num, length) {
  var str = num.toString(16), i = length - str.length, z = "0";
  for (; i > 0; i >>>= 1, z += z) { if (i & 1) { str = z + str; } }
  return str;
};

/**
 * Retains the value of 'UUID' global variable assigned before loading UUID.js.
 * @since 3.2
 * @type {any}
 */
UUID.overwrittenUUID = overwrittenUUID;

// }}}

// Advanced Random Number Generator Component {{{

(function() {

  var mathPRNG = UUID._getRandomInt;

  /**
   * Enables Math.random()-based pseudorandom number generator instead of cryptographically safer options.
   * @since v3.5.0
   * @deprecated This method is provided only to work around performance drawbacks of the safer algorithms.
   */
  UUID.useMathRandom = function() {
    UUID._getRandomInt = mathPRNG;
  };

  var crypto = null, cryptoPRNG = mathPRNG;
  if (typeof window !== "undefined" && (crypto = window.crypto || window.msCrypto)) {
    if (crypto.getRandomValues && typeof Uint32Array !== "undefined") {
      // Web Cryptography API
      cryptoPRNG = function(x) {
        if (x < 0 || x > 53) { return NaN; }
        var ns = new Uint32Array(x > 32 ? 2 : 1);
        ns = crypto.getRandomValues(ns) || ns;
        return x > 32 ? ns[0] + (ns[1] >>> 64 - x) * 0x100000000 : ns[0] >>> 32 - x;
      };
    }
  } else if (typeof require !== "undefined" && (crypto = require("crypto"))) {
    if (crypto.randomBytes) {
      // nodejs
      cryptoPRNG = function(x) {
        if (x < 0 || x > 53) { return NaN; }
        var buf = crypto.randomBytes(x > 32 ? 8 : 4), n = buf.readUInt32BE(0);
        return x > 32 ? n + (buf.readUInt32BE(4) >>> 64 - x) * 0x100000000 : n >>> 32 - x;
      };
    }
  }
  UUID._getRandomInt = cryptoPRNG;

})();

// }}}

// UUID Object Component {{{

/**
 * Names of UUID internal fields.
 * @type {string[]}
 * @constant
 * @since 3.0
 */
UUID.FIELD_NAMES = ["timeLow", "timeMid", "timeHiAndVersion",
                    "clockSeqHiAndReserved", "clockSeqLow", "node"];

/**
 * Sizes of UUID internal fields.
 * @type {number[]}
 * @constant
 * @since 3.0
 */
UUID.FIELD_SIZES = [32, 16, 16, 8, 8, 48];

/**
 * Creates a version 4 {@link UUID} object.
 * @returns {UUID} Version 4 {@link UUID} object.
 * @since 3.0
 */
UUID.genV4 = function() {
  var rand = UUID._getRandomInt;
  return new UUID()._init(rand(32), rand(16), // time_low time_mid
                          0x4000 | rand(12),  // time_hi_and_version
                          0x80   | rand(6),   // clock_seq_hi_and_reserved
                          rand(8), rand(48)); // clock_seq_low node
};

/**
 * Converts a hexadecimal UUID string to a {@link UUID} object.
 * @param {string} strId Hexadecimal UUID string ("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx").
 * @returns {UUID} {@link UUID} object or null.
 * @since 3.0
 */
UUID.parse = function(strId) {
  var r, p = /^\s*(urn:uuid:|\{)?([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{2})([0-9a-f]{2})-([0-9a-f]{12})(\})?\s*$/i;
  if (r = p.exec(strId)) {
    var l = r[1] || "", t = r[8] || "";
    if (((l + t) === "") ||
        (l === "{" && t === "}") ||
        (l.toLowerCase() === "urn:uuid:" && t === "")) {
      return new UUID()._init(parseInt(r[2], 16), parseInt(r[3], 16),
                              parseInt(r[4], 16), parseInt(r[5], 16),
                              parseInt(r[6], 16), parseInt(r[7], 16));
    }
  }
  return null;
};

/**
 * Initializes a {@link UUID} object.
 * @private
 * @constructs UUID
 * @param {number} [timeLow=0] time_low field (octet 0-3, uint32).
 * @param {number} [timeMid=0] time_mid field (octet 4-5, uint16).
 * @param {number} [timeHiAndVersion=0] time_hi_and_version field (octet 6-7, uint16).
 * @param {number} [clockSeqHiAndReserved=0] clock_seq_hi_and_reserved field (octet 8, uint8).
 * @param {number} [clockSeqLow=0] clock_seq_low field (octet 9, uint8).
 * @param {number} [node=0] node field (octet 10-15, uint48).
 * @returns {UUID} this.
 */
UUID.prototype._init = function() {
  var names = UUID.FIELD_NAMES, sizes = UUID.FIELD_SIZES;
  var bin = UUID._binAligner, hex = UUID._hexAligner;

  /**
   * UUID internal field values as an array of integers.
   * @type {number[]}
   */
  this.intFields = new Array(6);

  /**
   * UUID internal field values as an array of binary strings.
   * @type {string[]}
   */
  this.bitFields = new Array(6);

  /**
   * UUID internal field values as an array of hexadecimal strings.
   * @type {string[]}
   */
  this.hexFields = new Array(6);

  for (var i = 0; i < 6; i++) {
    var intValue = parseInt(arguments[i] || 0);
    this.intFields[i] = this.intFields[names[i]] = intValue;
    this.bitFields[i] = this.bitFields[names[i]] = bin(intValue, sizes[i]);
    this.hexFields[i] = this.hexFields[names[i]] = hex(intValue, sizes[i] >>> 2);
  }

  /**
   * UUID version number.
   * @type {number}
   */
  this.version = (this.intFields.timeHiAndVersion >>> 12) & 0xF;

  /**
   * 128-bit binary string representation.
   * @type {string}
   */
  this.bitString = this.bitFields.join("");

  /**
   * Non-delimited hexadecimal string representation ("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx").
   * @type {string}
   * @since v3.3.0
   */
  this.hexNoDelim = this.hexFields.join("");

  /**
   * Hexadecimal string representation ("xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx").
   * @type {string}
   */
  this.hexString = this.hexFields[0] + "-" + this.hexFields[1] + "-" + this.hexFields[2]
                 + "-" + this.hexFields[3] + this.hexFields[4] + "-" + this.hexFields[5];

  /**
   * URN string representation ("urn:uuid:xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx").
   * @type {string}
   */
  this.urn = "urn:uuid:" + this.hexString;

  return this;
};

/**
 * Converts an integer to a zero-filled binary string.
 * @private
 * @param {number} num
 * @param {number} length
 * @returns {string}
 */
UUID._binAligner = function(num, length) {
  var str = num.toString(2), i = length - str.length, z = "0";
  for (; i > 0; i >>>= 1, z += z) { if (i & 1) { str = z + str; } }
  return str;
};

/**
 * Returns the hexadecimal string representation.
 * @returns {string} {@link UUID#hexString}.
 */
UUID.prototype.toString = function() { return this.hexString; };

/**
 * Tests if two {@link UUID} objects are equal.
 * @param {UUID} uuid
 * @returns {boolean} True if two {@link UUID} objects are equal.
 */
UUID.prototype.equals = function(uuid) {
  if (!(uuid instanceof UUID)) { return false; }
  for (var i = 0; i < 6; i++) {
    if (this.intFields[i] !== uuid.intFields[i]) { return false; }
  }
  return true;
};

/**
 * Nil UUID object.
 * @type {UUID}
 * @constant
 * @since v3.4.0
 */
UUID.NIL = new UUID()._init(0, 0, 0, 0, 0, 0);

// }}}

// UUID Version 1 Component {{{

/**
 * Creates a version 1 {@link UUID} object.
 * @returns {UUID} Version 1 {@link UUID} object.
 * @since 3.0
 */
UUID.genV1 = function() {
  if (UUID._state == null) { UUID.resetState(); }
  var now = new Date().getTime(), st = UUID._state;
  if (now != st.timestamp) {
    if (now < st.timestamp) { st.sequence++; }
    st.timestamp = now;
    st.tick = UUID._getRandomInt(4);
  } else if (Math.random() < UUID._tsRatio && st.tick < 9984) {
    // advance the timestamp fraction at a probability
    // to compensate for the low timestamp resolution
    st.tick += 1 + UUID._getRandomInt(4);
  } else {
    st.sequence++;
  }

  // format time fields
  var tf = UUID._getTimeFieldValues(st.timestamp);
  var tl = tf.low + st.tick;
  var thav = (tf.hi & 0xFFF) | 0x1000;  // set version '0001'

  // format clock sequence
  st.sequence &= 0x3FFF;
  var cshar = (st.sequence >>> 8) | 0x80; // set variant '10'
  var csl = st.sequence & 0xFF;

  return new UUID()._init(tl, tf.mid, thav, cshar, csl, st.node);
};

/**
 * Re-initializes the internal state for version 1 UUID creation.
 * @since 3.0
 */
UUID.resetState = function() {
  UUID._state = new UUIDState();
};

function UUIDState() {
  var rand = UUID._getRandomInt;
  this.timestamp = 0;
  this.sequence = rand(14);
  this.node = (rand(8) | 1) * 0x10000000000 + rand(40); // set multicast bit '1'
  this.tick = rand(4);  // timestamp fraction smaller than a millisecond
}

/**
 * Probability to advance the timestamp fraction: the ratio of tick movements to sequence increments.
 * @private
 * @type {number}
 */
UUID._tsRatio = 1 / 4;

/**
 * Persistent internal state for version 1 UUID creation.
 * @private
 * @type {UUIDState}
 */
UUID._state = null;

/**
 * @private
 * @param {Date|number} time ECMAScript Date Object or milliseconds from 1970-01-01.
 * @returns {any}
 */
UUID._getTimeFieldValues = function(time) {
  var ts = time - Date.UTC(1582, 9, 15);
  var hm = ((ts / 0x100000000) * 10000) & 0xFFFFFFF;
  return  { low: ((ts & 0xFFFFFFF) * 10000) % 0x100000000,
            mid: hm & 0xFFFF, hi: hm >>> 16, timestamp: ts };
};

// }}}

// create local namespace
function UUID() {}

// for nodejs
if (typeof module === "object" && typeof module.exports === "object") {
  module.exports = UUID;
}

return UUID;

})(UUID);

// vim: fdm=marker fmr&
