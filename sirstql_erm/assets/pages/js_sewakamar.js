///Mahmud, clear
// DATA LOCALSTORAGE
// -idpoli
// -listmode
// -poliselected
// === getItem->ambil data localstorage
// === setItem->isi data localstorage
$(function(){ 
    var idpoli = localStorage.getItem('poliselected');
    search_by_bangsal(0);
    // listsewakamar();
});
//refresh page
function refresh_page()
{
    // table.state.clear();
    // get_datapoli('#tampilpoli','');
    window.location.reload(true);
}
// dropdown poli
function setpoli_afterselect(x)
{
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("poliselected",x);

    } else {
         pesanUndefinedLocalStorage();
    }
}
// menubar tabel
function menubar_table()
{ //tambahSewaKamar
    // <a onclick="tambahRawatGabung()" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Rawat Gabung</a> &nbsp;| 
    $("div.toolbar").html('<select id="tampilbangsal" name="bangsal" onchange="search_by_bangsal(this.value)" class="form-control" style="margin-right:4px;"></select> <input id="tanggalawal" size="5" class="datepicker form-control" placeholder="Tanggal awal" > <input id="tanggalakhir" size="5" class="datepicker form-control" placeholder="Tanggal akhir"> <a class="btn '+((localStorage.getItem('listmode')=='sewakamar')?"btn-default":"btn-info")+' btn-sm" onclick="search_by_daterange()"><i class="fa fa-desktop"></i> Sewa Kamar</a> <a onclick="liststatusbed()" class="btn '+((localStorage.getItem('listmode')=='statusbed')?"btn-default":"bg-purple")+' btn-sm"><i class="fa fa-desktop"></i> Status Bed</a> <a onclick="listsrawatgabung()" class="btn '+((localStorage.getItem('listmode')=='rawatgabung')?"btn-default":"btn-primary")+' btn-sm"><i class="fa fa-desktop"></i> Rawat Gabung</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a onclick="tambahSewaKamar()" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i> Sewa Kamar</a>');
    // $("div.dataTables_filter").after('<table style="font-size: 11.4px;" cellspacing="0" width="100%"><tr><td></td></tr></table>');
}
/// --  cari berdasarkan bangsal
function search_by_bangsal(x)
{
    var modelistdata = localStorage.getItem('listmode');
    if(modelistdata==='sewakamar' || modelistdata==null){listsewakamar(x);}
    else if(modelistdata==='statusbed'){liststatusbed(x);}
    else if(modelistdata==='rawatgabung'){listsrawatgabung(x);}
}
/// --  cari berdasarkan poli
function search_by_daterange()
{
    listsewakamar();
}
// form tambah sewa kamar
function tambahSewaKamar()
{
    var modalTitle = 'Sewa Kamar';
    var modalContent = '<form action="" id="FormSewaKamar">' +
                            '<div class="form-group">' +
                                '<div class="col-sm-12"><label>Waktu Masuk (tahun-bulan-tanggal jam)</label>' +
                                '<input type="text" name="tglmasuk" class="datetimepicker form-control"/></div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-12"><label>Waktu Keluar (tahun-bulan-tanggal jam)</label>' +
                                '<input type="text" name="tglkeluar" class="datetimepicker form-control"/></div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-12"><label>Keluarga Dari Pasien</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select></div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-4"><label>Nama Penyewa</label>' +
                                '<input type="text" name="namapenyewa" placeholder="Nama penyewa" class="form-control"/></div>'+
                                '<div class="col-sm-4"><label>Jenis Kelamin</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="jeniskelamin">'+
                                '<option value="0">Pilih</option>'+
                                '<option value="2">Laki-laki</option>'+
                                '<option value="3">Wanita</option>'+
                                '</select></div>' +
                                '<div class="col-sm-4"><label>Bangsal</label>' +
                                '<select class="select2 form-control" style="width:100%;" onchange="sewakamarcaribed(this.value)" name="idbangsal">'+
                                '<option value="0">Pilih</option>'+
                                '</select></div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-4"><label>Bed</label><select onchange="sewakamar_setbiayasewa(this.value)" class="select2 form-control" style="width:100%;" name="idbed">'+
                                '<option value="0">Pilih</option>'+
                                '</select></div>' +
                                '<div class="col-sm-8"><label>Catatan</label>' +
                                '<textarea class="form-control" rows="1" name="catatan" placeholder="Catatan sewa"></textarea></div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-3"><label>Biaya Sewa</label><input type="text" name="biayasewa" readonly class="form-control" placeholder="[Ototmatis]" /></div>' +
                                '<div class="col-sm-3"><label>Bayar</label><input type="text" name="bayar" onkeyup="sewakamar_setkembalisewa(this.value)" class="form-control" placeholder="Uang Bayar" /></div>' +
                                '<div class="col-sm-3"><label>Kembali</label><input type="text" name="kembali" readonly class="form-control" placeholder="[Ototmatis]"/></div>' +
                                '<div class="col-sm-3"><label>Kekurangan</label><input type="text" name="kekurangan" readonly class="form-control" placeholder="[Ototmatis]"/></div>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'sewa kamar',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tglmasuk"]').val() == '')
                    {
                        alert_empty('tanggal masuk');
                        return false;
                    }
                    else if ($('input[name="tglkeluar"]').val() == '')
                    {
                        alert_empty('tanggal keluar');
                        return false;
                    }
                    else if ($('input[name="tglkeluar"]').val() <= $('input[name="tglmasuk"]').val())
                    {
                        alert_empty('ubah tanggal keluar');
                        return false;
                    }
                    else if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('pasien rawat inap');
                        return false;
                    }
                    else if ($('input[name="namapenyewa"]').val() == 0)
                    {
                        alert_empty('penyewa');
                        return false;
                    }
                    else if ($('select[name="jeniskelamin"]').val() == 0)
                    {
                        alert_empty('jenis kelamin');
                        return false;
                    }
                    else if ($('select[name="idbangsal"]').val() == 0)
                    {
                        alert_empty('bangsal');
                        return false;
                    }
                    else if ($('select[name="idbed"]').val() == 0)
                    {
                        alert_empty('bed');
                        return false;
                    }
                    else if ($('textarea[name="catatan"]').val() == '')
                    {
                        alert_empty('catatan');
                        return false;
                    }
                    else
                    {
                        simpansewakamar();
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinapselected')); //ambil data inap
        tampilDataBangsal($('select[name="idbangsal"]'),''); //ambil data bangsal
        // tampilDataBed($('select[name="idbed"]','kosong')); //ambil data bangsal
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
// mahmud, clear
function sewakamar_setbiayasewa(value)
{
    var kekurangan=0;
    $('input[name="biayasewa"]').val(explode_getdt(value,',','2'));
    (($('input[name="bayar"]').val()<=$('input[name="biayasewa"]').val()) ? kekurangan = $('input[name="bayar"]').val() - $('input[name="biayasewa"]').val() : kekurangan='0');
    $('input[name="kekurangan"]').val(kekurangan);
    $('input[name="bayar"]').focus();
}
// mahmud, clear
function sewakamar_setkembalisewa(value)
{
    var kembali=0,kekurangan=0;
    ((parseInt($('input[name="bayar"]').val())<=parseInt($('input[name="biayasewa"]').val())) ? kekurangan = $('input[name="bayar"]').val() - $('input[name="biayasewa"]').val() : kekurangan='0');
    $('input[name="kekurangan"]').val(kekurangan);
    ((parseInt($('input[name="bayar"]').val())>=parseInt($('input[name="biayasewa"]').val())) ? kembali = $('input[name="bayar"]').val() - $('input[name="biayasewa"]').val() : kembali='0');
    $('input[name="kembali"]').val(kembali);
}
// mahmud, clear --- cari bed sewa kamar by bangsal
function sewakamarcaribed(idbangsal)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/sewakamar_getdtbed',
        data: {i:idbangsal}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            var html='<option value="0">Pilih</option>';
            for(i in result)
            {
                html +='<option value="'+result[i].idbed+','+result[i].idkelas+','+result[i].sewakamar+'">'+result[i].nobed+'</option>';
            }
            $('input[name="biayasewa"]').val('');
            $('input[name="bayar"]').val('');
            $('input[name="kembali"]').val('');
            $('input[name="kekurangan"]').val('');
            $('select[name="idbed"]').empty();
            $('select[name="idbed"]').html(html);
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}
//mahmud, clear === simpan rencana
function simpansewakamar()
{
    var cetakBukti='';
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_sewakamar',
        data: $("#FormSewaKamar").serialize(), //data ambil dari form
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status, result.message);
            cetakBukti += '<div style="width:6cm;float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);width:6cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n\
                          <table border="0" style="font-size:small;float:left;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetakBukti += '<tr><td>Nama</td><td>:</td><td>'+result.dtsewa['namapenyewa']+'</td></tr>';
            cetakBukti += '<tr><td>Catatan</td><td>:</td><td>'+result.dtsewa['catatan']+'</td></tr>';
            cetakBukti += '<tr><td>Waktu Masuk</td><td>:</td><td>'+result.dtsewa['waktumasuk']+'</td></tr>';
            cetakBukti += '<tr><td>Waktu Keluar</td><td>:</td><td>'+result.dtsewa['waktukeluar']+'</td></tr>';
            cetakBukti += '</table><table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetakBukti += '<tr><td>No.Kamar</td><td>::</td><td align="right" >'+result.dtsewa['namabangsal']+' No '+result.dtsewa['nobed']+'</td></tr>';
            cetakBukti += '<tr><td>Biaya Sewa</td><td>::</td><td align="right" >'+result.dtsewa['biayasewa']+'</td></tr>';
            cetakBukti += '</table><table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetakBukti += '<tr><td>No Nota</td><td>'+result.dtsewa['idsewakamar']+'</td></tr>';
            cetakBukti += '<tr><td>Waktu Tagih</td><td>'+result.dtsewa['waktutagih']+'</td></tr>';
            cetakBukti += '<tr><td>Waktu Bayar</td><td>'+result.dtsewa['waktubayar']+'</td></tr>';
            cetakBukti += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat(result.dtsewa['biayasewa'])+'</td></tr>';
            cetakBukti += '<tr><td>Dibayar</td><td align="right">'+convertToRupiahBulat(result.dtsewa['dibayar'])+'</td></tr>';
            cetakBukti += '<tr><td>Selisih</td><td align="right">'+convertToRupiahBulat(parseInt(result.dtsewa['dibayar']) - parseInt(result.dtsewa['biayasewa']))+'</td></tr>';
            cetakBukti += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            cetakBukti += '</table></div>';
            fungsi_cetaktopdf(cetakBukti,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
            listsewakamar();
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}
// mahmud, clear
/// -- list data sewa kamar
function listsewakamar(x='')
{
    localStorage.setItem('listmode','sewakamar');
    $('.box-title').empty();
    $('.box-title').html('<i class="fa fa-table"></i> List Sewa Kamar');
    startLoading();
    setpoli_afterselect(x); 
    var backgroundinfo ='';
    var bangsal     = ((x=='') ? $('select[name="bangsal"]').val() : x );
    var tglawal     = (is_undefined($('#tanggalawal').val()) && !is_undefined(localStorage.getItem("tanggalawal")))?localStorage.getItem("tanggalawal"):$('#tanggalawal').val();
    var tglakhir    = (is_undefined($('#tanggalakhir').val()) && !is_undefined(localStorage.getItem("tanggalakhir")))?localStorage.getItem("tanggalakhir"):$('#tanggalakhir').val();
    var x=0, no=0, html = '<table id="table" class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">'+
                  '<thead>'+
                  '<tr>'+
                    '<th>No</th>'+
                    '<th>PENYEWA</th>'+
                    '<th>BANGSAL (No.Bed)</th>'+
                    '<th>STATUS BED</th>'+
                    '<th>BIAYA</th>'+
                    '<th>TERBAYAR</th>'+
                    '<th>KURANG</th>'+
                    '<th>WAKTU TAGIH</th>'+
                    '<th>WAKTU MASUK</th>'+
                    '<th>WAKTU KELUAR</th>'+
                    '<th>CATATAN</th>'+
                    '<th>STATUS</th>'+
                    '<th style="width:100px;">AKSI</th>'+
                  '</tr>'+
                  '</thead>'+
                  '<tbody>';
                  /////////cari dan tampilkan data
                  $.ajax({
                    url:base_url + 'cpelayananranap/getdtsewakamar',
                    type:'POST',
                    dataType:'JSON',
                    data:{bangsal:bangsal,awal:((tglawal!=undefined)? formatTgltoSql(tglawal) : tglawal),akhir:((tglakhir!=undefined)? formatTgltoSql(tglakhir) : tglakhir)},
                    success : function(result){
                        stopLoading();
                        for (x in result)
                        {
                        if(result[x].status=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                        else if(result[x].status=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                        else if(result[x].status=='pesan'){backgroundinfo='background-color:#c4d8e8';}
                        else {backgroundinfo='';}
                        html += '<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                           '<td>'+ no +'</td>'+
                           '<td>'+result[x].namapenyewa+'</td>'+
                           '<td>'+result[x].namabangsal+' ( '+result[x].nobed+' )</td>'+
                           '<td>'+result[x].statusbed+'</td>'+
                           '<td>'+convertToRupiahBulat(result[x].biayasewa)+'</td>'+
                           '<td>'+convertToRupiahBulat(result[x].dibayar)+'</td>'+
                           '<td>'+convertToRupiahBulat(result[x].kekurangan)+'</td>'+
                           '<td>'+result[x].waktutagih+'</td>'+
                           '<td>'+result[x].waktumasuk+'</td>'+
                           '<td>'+result[x].waktukeluar+'</td>'+
                           '<td>'+result[x].catatan+'</td>'+
                           '<td>'+result[x].status+'</td>'+
                           '<td>';
                           if(result[x].status!=='dipesan'){html+='<a id="ubahstatussewakamar" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Dipesan" isewa="'+result[x].idsewakamar+'" stat1="'+result[x].status+'" stat2="dipesan"><i class="fa fa-shopping-cart"></i></a> ';}
                           if(result[x].status!=='selesai'){html+='<a id="ubahstatussewakamar" kekurangan="'+result[x].kekurangan+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Selesai" isewa="'+result[x].idsewakamar+'" stat1="'+result[x].status+'" stat2="selesai"><i class="fa fa-check"></i></a> ';}
                           if(result[x].status!=='dipakai'){html+='<a id="ubahstatussewakamar" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Dipakai" isewa="'+result[x].idsewakamar+'" stat1="'+result[x].status+'" stat2="dipakai"><i class="fa fa-bed"></i></a> ';}
                           if(result[x].status!=='batal'){html+='<a id="ubahstatussewakamar" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal" isewa="'+result[x].idsewakamar+'" stat1="'+result[x].status+'" stat2="batal"><i class="fa fa-minus-circle"></i></a> ';}
                           html+='</td></tr>';
                        }
                        get_databangsal('#tampilbangsal',bangsal);
                        $('#listkamar').empty();
                        $('#listkamar').html(html+'</tfoot></table>');
                        table = $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,}); //Initialize Data Tables
                        $('[data-toggle="tooltip"]').tooltip();
                        menubar_table();
                        // loadbelumdiplot();
                        $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglawal, 'now')));
                        $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglakhir, 'now')));
                        if (!is_undefined(tglawal)) localStorage.setItem("tanggalawal",tglawal);
                        if (!is_undefined(tglakhir)) localStorage.setItem("tanggalakhir",tglakhir);
                    },
                    error : function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                  });   
}
// mahmud, clear ==== UBAH STATUS SEWA KAMAR ===
$(document).on("click","#ubahstatussewakamar", function(){
    var isewa = $(this).attr("isewa");
    var stat1 = $(this).attr("stat1");
    var stat2 = $(this).attr("stat2");
    var kekurangan = $(this).attr("kekurangan");
    ubahstatussewakamar(isewa,stat1,stat2,kekurangan);
});
function ubahstatussewakamar(isewa,stat1,stat2,kekurangan)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status dari '+stat1+' ke '+stat2+'..? ',
        buttons: {
            confirm: function () {
            if(stat2=='selesai')
            {
                $.confirm({
                    title: 'Kekurangan biaya sewa',
                    content: '<div class="form-group"><div class="col-sm-12"><label>Kekurangan</label><input id="kekurangan_kekurangan" type="text" class="form-control" disabled value="'+explode_replace(kekurangan,'-')+'"></div></div><div class="form-group"><div class="col-sm-12"><label>Bayar</label><input id="kekurangan_bayar" type="text" class="form-control" placeholder="bayar" onkeyup="setkekuranganselisih(this.value)" /></div></div><div class="form-group"><div class="col-sm-12"><label>Selisih</label><input id="kekurangan_selisih" type="text" class="form-control" disabled placeholder="[otomatis]"></div></div>',
                    buttons: {
                        simpan: function () {
                            if(parseInt($('#kekurangan_bayar').val()) < parseInt($('#kekurangan_kekurangan').val()))
                            {
                                $.alert('Uang bayar masih kurang..!');
                                return false;
                            }
                            else
                            {
                                saveubahstatussewakamar(isewa,stat2,explode_replace(kekurangan,'-'));
                            }
                        },
                        batal: function () {               
                        }            
                    }
                });
            }
            else
            {
                saveubahstatussewakamar(isewa,stat2,kekurangan);
            }                
                 
            },
            cancel: function () {               
            }            
        }
    });
}
function setkekuranganselisih(value)
{
    $('#kekurangan_selisih').val($('#kekurangan_bayar').val() - $('#kekurangan_kekurangan').val());
}
function saveubahstatussewakamar(isewa,stat2,kekurangan)
{
    $.ajax({ 
        url: base_url +'cpelayananranap/ubahstatussewakamar',
        type : "post",      
        dataType : "json",
        data : {i:isewa, status:stat2,kekurangan:kekurangan},
        success: function(result) {
            notif(result.status, result.message);
            return listsewakamar();
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    });
}
// mahmud, clear
/// -- list data status BED
function liststatusbed(x='')
{
    localStorage.setItem('listmode','statusbed');
    $('.box-title').empty();
    $('.box-title').html('<i class="fa fa-table"></i> List Status Bed');
    startLoading();
    setpoli_afterselect(x); 
    var backgroundinfo ='';
    var bangsal     = ((x=='') ? $('select[name="bangsal"]').val() : x );
    var x=0, no=0, html = '<table id="table" class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">'+
                  '<thead>'+
                  '<tr>'+
                    '<th>No</th>'+
                    '<th>BANGSAL</th>'+
                    '<th>KELAS</th>'+
                    '<th>STATUS BED</th>'+
                    '<th>NO.BED</th>'+
                    '<th style="width:150px;">AKSI</th>'+
                  '</tr>'+
                  '</thead>'+
                  '<tbody>';
                  /////////cari dan tampilkan data
                  $.ajax({
                    url:base_url + 'cpelayananranap/getdtstatusbed',
                    type:'POST',
                    dataType:'JSON',
                    data:{bangsal:bangsal},
                    success : function(result){
                        stopLoading();
                        for (x in result)
                        {
                        if(result[x].status=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                        else if(result[x].status=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                        else if(result[x].status=='pesan'){backgroundinfo='background-color:#c4d8e8';}
                        else {backgroundinfo='';}
                            html += '<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                                        '<td>'+ no +'</td>'+
                                       '<td>'+result[x].namabangsal+'</td>'+
                                       '<td>'+result[x].kelas+'</td>'+
                                       '<td>'+result[x].statusbed+'</td>'+
                                       '<td>'+result[x].nobed+'</td>'+
                                       '<td>';
                                       if(result[x].statusbed!=='Bed Dipesan'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Pesan" ibed="'+result[x].idbed+'" istatus="5" stat2="Bed Dipesan" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/beddipesan.png" width="25px"/></a> ';}
                                       if(result[x].statusbed!=='Bed Kosong'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Kosongkan" ibed="'+result[x].idbed+'" istatus="1" stat2="Bed Kosong" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/bedkosong.png" width="25px"/></a> ';}
                                       if(result[x].statusbed!=='Bed Rusak'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Rusak" ibed="'+result[x].idbed+'" istatus="6" stat2="Bed Rusak" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/bedrusak.png" width="25px"/></a> ';}
                                       if(result[x].statusbed!=='Bed Menunggu'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Menunggu" ibed="'+result[x].idbed+'" istatus="4" stat2="Bed Menunggu" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/bedmenunggu.png" width="25px"/></a> ';}
                                       if(result[x].statusbed!=='Bed Laki-laki'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Laki-laki" ibed="'+result[x].idbed+'" istatus="2" stat2="Bed Laki-laki" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/bedlaki.png" width="25px"/></a> ';}
                                       if(result[x].statusbed!=='Bed Wanita'){html+='<a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Wanita" ibed="'+result[x].idbed+'" istatus="3" stat2="Bed Wanita" stat1="'+result[x].statusbed+'"><img src="'+base_url+'assets/images/bedwanita.png" width="25px"/></a> ';}
                                       html+='</td></tr>';      
                        }
                        get_databangsal('#tampilbangsal',bangsal);
                        $('#listkamar').empty();
                        $('#listkamar').html(html+'</tfoot></table>');
                        table = $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,}); //Initialize Data Tables
                        $('[data-toggle="tooltip"]').tooltip();
                        menubar_table();
                        $('.datepicker').hide();
                    },
                    error : function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                  });   
}
// mahmud, clear ==== UBAH STATUS BED ===
$(document).on("click","#ubahstatusbed", function(){
    var idbed = $(this).attr("ibed");
    var idstatusbed = $(this).attr("istatus");
    var stat1 = $(this).attr("stat1");
    var stat2 = $(this).attr("stat2");
    ubahstatusbed(idbed,idstatusbed,stat1,stat2);
});
function ubahstatusbed(idbed,idstatusbed,stat1,stat2)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status dari '+stat1+' ke '+stat2+'..? ',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url +'cpelayananranap/ubahstatusbed',
                    type : "post",      
                    dataType : "json",
                    data : {i:idbed, status:idstatusbed},
                    success: function(result) {
                        notif(result.status, result.message);
                        return liststatusbed();
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
// mahmud, clear
/// -- list data rawat gabung
function listsrawatgabung(x='')
{
    localStorage.setItem('listmode','rawatgabung');
    $('.box-title').empty();
    $('.box-title').html('<i class="fa fa-table"></i> List Rawat Gabung');
    startLoading();
    setpoli_afterselect(x); 
    var backgroundinfo ='';
    var bangsal     = ((x=='') ? $('select[name="bangsal"]').val() : x );
    var tglawal     = (is_undefined($('#tanggalawal').val()) && !is_undefined(localStorage.getItem("tanggalawal")))?localStorage.getItem("tanggalawal"):$('#tanggalawal').val();
    var tglakhir    = (is_undefined($('#tanggalakhir').val()) && !is_undefined(localStorage.getItem("tanggalakhir")))?localStorage.getItem("tanggalakhir"):$('#tanggalakhir').val();
    var x=0, no=0, html = '<table id="table" class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">'+
                  '<thead>'+
                  '<tr>'+
                    '<th>No</th>'+
                    '<th>NO.RM</th>'+
                    '<th>NAMA PASIEN</th>'+
                    '<th>NO.SEP</th>'+
                    '<th>WAKTU MASUK</th>'+
                    '<th>DOKTER DPJP</th>'+
                    '<th>BANGSAL</th>'+
                    '<th>KELAS</th>'+
                    '<th>NO.BED</th>'+
                    '<th>STATUS</th>'+
                  '</tr>'+
                  '</thead>'+
                  '<tbody>';
                  /////////cari dan tampilkan data
                  $.ajax({
                    url:base_url + 'cpelayananranap/getdtrawatgabung',
                    type:'POST',
                    dataType:'JSON',
                    data:{bangsal:bangsal,awal:((tglawal!=undefined)? formatTgltoSql(tglawal) : tglawal),akhir:((tglakhir!=undefined)? formatTgltoSql(tglakhir) : tglakhir)},
                    success : function(result){
                        stopLoading();
                        for (x in result)
                        {
                        if(result[x].status=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                        else if(result[x].status=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                        else if(result[x].status=='pesan'){backgroundinfo='background-color:#c4d8e8';}
                        else {backgroundinfo='';}
                            html += '<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                                        '<td>'+ no +'</td>'+
                                       '<td>'+result[x].norm+'</td>'+
                                       '<td>'+result[x].namalengkap+'</td>'+
                                       '<td>'+result[x].nosep+'</td>'+
                                       '<td>'+result[x].waktumasuk+'</td>'+
                                       '<td>'+result[x].namadokter+'</td>'+
                                       '<td>'+result[x].namabangsal+'</td>'+
                                       '<td>'+result[x].kelas+'</td>'+
                                       '<td>'+result[x].nobed+'</td>'+
                                       '<td>'+result[x].israwatgabung+'</td></tr>';
                                       
                        }
                        get_databangsal('#tampilbangsal',bangsal);
                        $('#listkamar').empty();
                        $('#listkamar').html(html+'</tfoot></table>');
                        table = $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,}); //Initialize Data Tables
                        $('[data-toggle="tooltip"]').tooltip();
                        menubar_table();
                        $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglawal, 'now')));
                        $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglakhir, 'now')));
                        if (!is_undefined(tglawal)) localStorage.setItem("tanggalawal",tglawal);
                        if (!is_undefined(tglakhir)) localStorage.setItem("tanggalakhir",tglakhir);
                    },
                    error : function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                  });   
}
// getdtrawatgabung
//---------- basit, clear
$(document).on("click","#pemeriksaanranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('terlaksana','Pemeriksaan Terlaksana?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('batal','Batal Pemeriksaan?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanranapUbahStatus('rencana','Batal Terlaksana?',id,nobaris);
});

//---------- basit, clear
function fungsi_pemeriksaanranapUbahStatus(status,content,id,nobaris) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: content,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'pemeriksaanranap_ubahstatus',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                        notif(result.status, result.message);
                        listsewakamar('');
                        if(result.status=='success')
                        {
                            $('#status'+nobaris).empty();
                            $('#status'+nobaris).text(status);
                        }
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

$(document).on("click","#pemeriksaanranapBHP", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt4"));
        localStorage.setItem("idinap", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        localStorage.setItem("idkelas", $(this).attr("alt3"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapView", function(){ //view pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idinap");
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_view';
    } else {
         pesanUndefinedLocalStorage();
    }
});
