var diskon='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  diskon = $('#diskon').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
      "ajax": {
          "data":{},
          "url": base_url+'cmasterdata/dt_masterdiskon',
          "type": "POST"
      },
      "columnDefs": [{ "targets": [9],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  diskon.state.clear();
  diskon.ajax.reload();
});
function reloaddata(){diskon.ajax.reload(null,false);}
//tambah aset
$(document).on("click",'#add',function(){
    formtambahedit('','Tambah Diskon');
});
//ubah aset
$(document).on("click",'#edit',function(){
    formtambahedit($(this).attr('alt'),'Ubah Diskon');
});

function formtambahedit(iddiskon,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formdiskon" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<input type="hidden" name="idmasterdiskon" value="'+iddiskon+'" />'+
                                '<div class="col-md-6">' +
                                    '<label>ICD</label>' +
                                    '<select class="form-control" name="icd" id="icd" ><option value="">-Pilih-</option></select>'+
                                '</div>'+
                                '<div class="col-md-6">' +
                                    '<label>Kelas</label>' +
                                    '<select class="form-control" name="idkelas" id="idkelas" ><option value="">-Pilih-</option></select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                    '<label>Jenis Tarif</label>' +
                                    '<select class="form-control" id="idjenistarif" name="idjenistarif"><option value="">-Pilih-</option></select>'+
                                '</div>'+
                                '<div class="col-md-6">' +
                                    '<label>Jenis Diskon</label>' +
                                    '<select class="form-control" name="jenisdiskon" id="jenisdiskon"><option>Persentase</option><option>Nominal</option></select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                    '<label>Tanggal Mulai</label>' +
                                    '<input type="text" id="tglmulai" class="datepicker form-control" name="tglmulai" value="" />' +  
                                '</div>'+
                                '<div class="col-md-6">' +
                                    '<label>Tanggal Berakhir</label>' +
                                    '<input type="text" id="tglakhir" class="datepicker form-control" name="tglakhir" value="" />' +  
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                    '<label>Cara Input</label>' +
                                    '<div>'+
                                        '<input type="radio" id="manual" name="jenisinput" checked value="1" /> Manual ' +
                                        '<input type="radio" id="otomatis" name="jenisinput" value="0" /> Otomatis</div>' +  
                                    '</div>'+
                                '</div>'+
                                
                                '<div class="col-md-6">' +
                                    '<label>Nominal Diskon</label>' +
                                    '<input type="text" id="nominal" class="form-control" name="nominal" value="" />' +  
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6">' +
                                    '<label>Member</label>' +
                                    '<select class="form-control" name="idmember" id="idmember"></select>'+
                                '</div>'+
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="nama"]').val()==''){
                        alert('Nama  harap diisi.');
                        return false;
                    }else if($('input[name="namabarang"]').val()==''){
                        alert('Nilai harap diisi.');
                        return false;
                    }else if($('textarea[name="keterangan"]').val()==''){
                        alert('Keterangan harap diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formready(iddiskon);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function inputedit(id,value)
{
    id.empty();
    id.val(value);
}
function formready(iddiskon)
{
    
    select2serachmulti($("#icd"),"cmasterdata/fillIcd");
    select2serachmulti($("#idmember"),"cmasterdata/fillcarimember");
    
    startLoading();
    $.ajax({ 
        url:  base_url+'cmasterdata/formreadymasterdiskon',type:"POST",dataType:"JSON",data:{id:iddiskon},
        success: function(result){
            stopLoading();
            var diskon = result.diskon;
            editdataenum($('#jenisdiskon'),result.jenisdiskon,((diskon!=null)? diskon.jenisdiskon:''));
            editdataoption($('#idjenistarif'),result.jenistarif, ((diskon!=null)? diskon.idjenistarif:'') );
            editdataoption($('#idkelas'),result.kelas,((diskon!=null)? diskon.idkelas:''));
            $('#tglmulai').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((diskon==null) ? 'now' : diskon.tanggalmulai ));
            $('#tglakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((diskon==null) ? 'now' : diskon.tanggalselesai ));
            if(diskon!=null){
                
                $('#icd').html('<option value="'+diskon.icd+'">'+diskon.namaicd+'</option>');
                $('#idmember').html('<option value="'+diskon.idmember+'">'+diskon.member+'</option>');
                inputedit($('#nominal'),diskon.nominal);
                (diskon.jenisinput==1) ? $('#manual').prop('checked', true) : $('#otomatis').prop('checked', true) ;
            }
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    });
}

function simpan()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_masterdiskon',type:"POST",dataType:"JSON",data:$('#formdiskon').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
