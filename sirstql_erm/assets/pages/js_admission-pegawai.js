$(function () {
    
    switch($('#jsmode').val()){
        case 'view':
                list_data_pegawai();
            break;
        case 'add':
                $('.select2').select2();
                $('#datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"});
                $('input[type="checkbox"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green'});
                fungsicari_person();
            break;
        case 'edit':
                $('.select2').select2();
                $('#datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"});
                $('input[type="checkbox"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green'});
                fungsicari_person();
            break;
        default:
    }
})

function list_data_pegawai(){
    var dtpegawai = $('#dtpegawai').DataTable({
    "dom": '<"toolbar">frtip',
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "url": base_url+'cadmission/dt_pegawai',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [-1],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
    });
}



function fungsicari_person()
{
    $('#listPerson').select2({//cari data person pegawai
    placeholder: "Cari Pegawai...",
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url + "cadmission/cari_person",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                term: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idperson, text: item.nik + ' - ' + item.namalengkap}}),
                pagination: {
                    more: true//(page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
function refreshGrupPegawai() // menampilkan data grup pegawai
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:base_url + 'cadmission/cari_gruppegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            console.log(result);
            $('#gruppegawai').empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idgruppegawai +'" >' + result[i].namagruppegawai +'</option>';
            }
            $('#gruppegawai').html(select);
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }
    }); 
}
function tambah_gruppegawai() //form tambah grup pegawai
{
    var modalSize = 'small';
    var modalTitle = 'Add Grup Pegawai';
    var modalContent = '<form action="" id="FormGrupPegawai">' +
                            '<div class="form-group">' +
                                '<label>Nama Grup</label>' +
                                '<input type="text" name="nama" placeholder="Nama Grup" id="grup" class="form-control"/>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    if( ! $('#grup').val() )  //jika data grup pegawai kosong
                    {
                            alert_empty('nama grup');
                            return false;
                    }
                    else //selain itu
                    {
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: base_url + 'cadmission/save_gruppegawai', //alamat controller yang dituju (di js base url otomatis)
                                data: $("#FormGrupPegawai").serialize(), //
                                dataType: "JSON", //tipe data yang dikirim
                                success: function(result) { //jika  berhasil
                                    notif(result.status, result.message);
                                    return refreshGrupPegawai();
                                },
                                error: function(result) { //jika error
                                    console.log(result.responseText);
                                }
                            });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function ubah_person(id)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cadmission/getdataedit_personpegawai', //alamat controller yang dituju (di js base url otomatis)
        data: {id:id}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            tambah_person(result);
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}
function tambah_person(dataperson='') ///tambah person
{
    var modalTitle = 'Add Data Person';
    var modalContent = '<form action="" id="FormPerson">' +
                            '<input value="'+((dataperson=='')?'':dataperson.idperson)+'" type="hidden" name="idperson" />' +
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                    '<label>No.Identitas*</label>' +
                                    '<input value="'+((dataperson=='')?'':dataperson.nik)+'" type="text" name="noidentitas" id="noidentitas" placeholder="No.Identitas" class="form-control"/>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Agama</label>' +
                                '<select name="agama" id="listAgama" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Nama Lengkap*</label>' +
                                '<input type="text" value="'+((dataperson=='')?'':dataperson.namalengkap)+'" name="namalengkap" id="namalengkap" placeholder="Nama lengkap" class="form-control"/>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>GolDarah</label>' +
                                '<select name="golongandarah" id="listGolDarah" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-3" >' +
                                '<label>JnsKelamin</label>' +
                                '<select name="jeniskelamin" id="listJeniskelamin" class="form-control select2">' +
                                '</select>' +
                                '</div>'+
                                '<div class="col-sm-5">' +
                                '<label>TanggalLahir*</label>' +
                                '<input type="text" name="tanggallahir" placeholder="Tanggal lahir" id="tanggallahir" class="form-control"/>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Rhesus</label>' +
                                '<select name="rhesus" id="listRhesus" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-4">' +
                                '<label>Tempat Lahir</label>' +
                                '<input type="text" value="'+((dataperson=='')?'':dataperson.tempatlahir)+'" name="tempatlahir" placeholder="Tempat Lahir" class="form-control"/>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Pekerjaan</label>' +
                                '<select name="pekerjaan" id="listPekerjaan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Pendidikan</label>' +
                                '<select name="pendidikan" id="listPendidikan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>No.Telpon</label>' +
                                '<input type="text" name="notelpon" value="'+((dataperson=='')?'':dataperson.notelpon)+'" placeholder="No.Telpon" class="form-control"/>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Status</label>' +
                                '<select name="status" id="listStatus" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Alamat</label>' +
                                '<textarea class="form-control" rows="1" placeholder="Alamat sesuai identitas" name="alamat">'+((dataperson=='')?'':dataperson.alamat)+'</textarea>' +
                                '</div>'+
                                '<div class="col-sm-4" >' +
                                '<label>Kelurahan</label>' +
                                '<select name="kelurahan" id="listKelurahan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                        '</form>';
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#noidentitas').val()==='') //jika no identitas kosong
                    {
                        alert_empty('noidentitas');
                        return false;
                    }
                    else if($('#namalengkap').val()==='') //jika no namalengkap kosong
                    {
                        alert_empty('nama lengkap');
                        return false;
                    }
                    else if($('#tanggallahir').val()==='') //jika no tanggallahir kosong
                    {
                        alert_empty('tanggal lahir');
                        return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST",
                            url: base_url + 'cadmission/save_person',
                            data: $("#FormPerson").serialize(),
                            dataType: "JSON",
                            success: function(result) {
                                $('select[name="listPerson"]').empty();
                                $('select[name="listPerson"]').html('<option value="'+result.id+'">'+result.nama+'</option>');
                                notif(result.status, result.message);
                            },
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'back',
                btnClass: 'btn-danger',
                action:function(){
                    fungsicari_person();
                }
            }
        },
        onContentReady: function () { 
        //tampilkan data enum 
        tampil_data_enum_person(dataperson);
        $('.select2').select2();
        $('#tanggallahir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate',((dataperson=='')?'now':dataperson.tanggallahir))
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function tampil_data_enum_person(data) //menampilkan data enum person
{
    $.ajax({
        url: base_url + 'cadmission/tampil_enum_person',
        type:'POST',
        dataType:'JSON',
        data:{id:''},
        success: function(result){
            edit_data_enum($('#listAgama'),result.agama,((data=='')?'':data.agama)); //menampilkan data agama pasien
            edit_data_enum($('#listJeniskelamin'),result.jeniskelamin,((data=='')?'':data.jeniskelamin)); //menampilkan data jeniskelamin pasien
            edit_data_enum($('#listGolDarah'),result.golongandarah,((data=='')?'':data.golongandarah)); //menampilkan data golongandarah pasien
            edit_data_enum($('#listRhesus'),result.rhesus,((data=='')?'':data.rh)); //menampilkan data rhesus pasien
            edit_data_enum($('#listStatus'),result.perkawinan,((data=='')?'':data.statusmenikah)); //menampilkan data perkawinan pasien
            edit_dropdown_kelurahan($('#listKelurahan'),result.kelurahan,((data=='')?'':data.iddesakelurahan)); //menampilkan data kelurahan
            edit_dropdown_pendidikan($('#listPendidikan'),result.pendidikan,((data=='')?'':data.idpendidikan)); //menampilkan data pendidikan
            edit_dropdown_pekerjaan($('#listPekerjaan'),result.pekerjaan,((data=='')?'':data.idpekerjaan)); //menampilkan data pekerjaan             
        },
        error: function(result){
            console.log(result);
        }
    });
}
function uniqusername(username)
{
    $.ajax({
        url: base_url + 'cadmission/uniqusername',
        type:'POST',
        dataType:'JSON',
        data:{user:username},
        success: function(result){
            if(result!=''){
                notif('danger', result);
                $('input[name="username"]').val('');
                $('input[name="username"]').focus();
            }
        },
        error: function(result){
            console.log(result);
        }
    });
}
function edit_data_enum(column,data,selected_data) // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{
    var select='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i]==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i] +'" '+ selected +' >' + data[i] +'</option>';
    }
    column.html(select);
}
function edit_dropdown_pendidikan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idpendidikan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpendidikan +'" '+ selected +' >' + data[i].namapendidikan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_pekerjaan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idpekerjaan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpekerjaan +'" '+ selected +' >' + data[i].namapekerjaan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_kelurahan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].iddesakelurahan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].iddesakelurahan +'" '+ selected +' >' + data[i].namadesakelurahan +' '+ data[i].namakecamatan +' '+ data[i].kodepos+'</option>';
    }
    column.html(select);
}
function back_pegawai() //kembali ke halaman list pegawai poli
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list pegawai?',
        buttons: {
            confirm: function () { 
                window.location.href='pegawai';
            },
            cancel: function () {               
            }            
        }
    });   
}
function save_pegawai() //save pegawai
{
    if($('input[name="nip"]').val()==='')
    {   
        $('input[name="nip"]').focus();
        alert_empty('nip');
    }
    else if($('input[name="namapegawai"]').val()==='')
    {   
        $('input[name="namapegawai"]').focus();
        alert_empty('nama pegawai');
    }
    else if($('select[name="statuskeaktifan"]').val()==='0')
    {   
        $('select[name="statuskeaktifan"]').focus();
        alert_empty('status');
    }
    else if($('select[name="gruppegawai"]').val()==='0')
    {   
        $('select[name="gruppegawai"]').focus();
        alert_empty('grup pegawai');
    }
    else
    {
        $('#FormPegawai').submit();
    }
}
