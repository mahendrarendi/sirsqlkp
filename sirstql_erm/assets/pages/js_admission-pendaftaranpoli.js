///'use strict';
var modependaftaran = localStorage.getItem('modependaftaran'); //ambil data modependaftaran dari local storage
var idpendaftaran = localStorage.getItem('idpendaftaran'); //ambil data idpendaftaran dari local storage
var limitcaripasien = -100;
var numcaripasien = 0;

$(function () {    
    setwaktulayanan();
    $('.selectpoliklinik').select2();
    caridesakelurahan();
    select2serachmulti($('#idbahasa'),'cmasterdata/fillcaribahasa');    
    select2serachmulti($('#idsuku'),'cmasterdata/fillcarisuku');
    cariklinikrujukan();
    $('input[name="modehalaman"]').val('');
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    $('#tanggallahir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"});
    
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red').iCheck({checkboxClass:'icheckbox_flat-green'});
    
    if(modependaftaran==='edit')
    {
        function_getdatapasien(idpendaftaran,'edit'); 
        $('input[name="modehalaman"]').val(modependaftaran);
    }else if(modependaftaran ==='pindah')
    {
        function_getdatapasien(idpendaftaran,'pindah'); 
        $('input[name="modehalaman"]').val(modependaftaran);
    }
})


function cetak_gelang(x,y,z) //fungsi cetak gelang pasien
{
   var namaPasien = $('input[name="namapasien"]').val();
   var noRm = $('input[name="pilihpasien"]').val();
   var noAntrian = $('#noAntrian').val();
   var printContents = '<div>'+
                       '<label>Nama Pasien</label> : '+namaPasien+'<br>'+
                       '<label>No.RM</label> : '+noRm+'<br>'+
                       '<label>No.Antrian</label> : '+noAntrian+
                       '</div>';
   var style = '<style rel="stylesheet" media="print"> @media print { div{ background-color:#000; } } </style>';
   fungsi_cetaktopdf(printContents,style); //panggil fungsi cetak 
}
// list data penanggung
function pendaftaran_getdatapenanggung()
{
    var modalTitle = 'Data Penanggung a/n pasien '+$('input[name="namapasien"]').val();
    var modalContent = '<a class="btn btn-primary btn-xs" onclick="tambah_penanggung()"><i class="fa fa-plus"></i>Tambah Penanggung</a><br><table class="table table-bordered table-striped table-hover"><thead><tr><th>No</th><th>Nama Lengkap</th><th>Jenis Kelamin</th><th>Pekerjaan</th><th>Hubungan</th><th>No.Telepon</th><th>Alamat</th><th> Tombol</th></tr></thead><tbody id="pendaftaran_listalldatapenanggung"></tfoot></table>';
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'lg',
        buttons: {
            formReset:{
                text: 'close',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { 
        // ambil data penanggung
        // pendaftaran_listtabledtpenanggung();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
    
}

function tambah_penanggung(x='',y='',h='',p='',k='') ///tambah penanggung jawab
{
    var modalTitle = ((x==='')? 'Add' : 'Edit' )+' Data Penanggung';
    var modalContent = '<form action="" id="FormPenanggungJawab">' +
                            '<input type="hidden" name="normPenanggung" id="normPenanggung" />'+
                            '<input type="hidden" name="idpersonPenanggung" id="idpersonPenanggung" value="'+x+'" />'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">Nama</label>' +
                                '<div class="col-sm-9">' +
                                '<input type="text" name="namapenanggung" id="namapenanggung" placeholder="Nama penanggung" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">JnsKelamin</label>' +
                                '<div class="col-sm-9" >' +
                                '<select name="jeniskelamin" id="listJeniskelamin" class="form-control">' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">Pekerjaan</label>' +
                                '<div class="col-sm-9" >' +
                                '<select name="pekerjaan" id="listPekerjaan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="col-sm-3">No.Telpon</label>' +
                                '<div class="col-sm-9">' +
                                '<input type="text" name="notelpon" id="notelponpenanggung" placeholder="No.Telpon" class="form-control"/>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="col-sm-3">Hubungan</label>' +
                                '<div class="col-sm-9" >' +
                                '<select name="hubungan" id="listHubungan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">Alamat</label>' +
                                '<div class="col-sm-9">' +
                                    '<textarea class="form-control" placeholder="Alamat sesuai identitas" name="alamatpenanggung" id="alamatpenanggung"></textarea>' +
                                '</div>'+
                                '<a class="btn" onclick="copy_alamat_pasien()">Salin sesuai alamat pasien</a>'+
                            '</div>' +
                        '</form>';
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',
        buttons: {
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#namapenanggung').val()==='') //jika no namapenanggung kosong
                    {
                        alert_empty('nama penanggung');
                        return false;
                    }
                    else if($('#tanggallahir2').val()==='') //jika no tanggallahir kosong
                    {
                        alert_empty('tanggal lahir');
                        return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST",
                            url: 'save_penanggungjawab',
                            data: $("#FormPenanggungJawab").serialize(),
                            dataType: "JSON",
                            success: function(result) {
                                notif(result.status, result.message);
                                listpenanggungpasien();
                            },
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { 
        $('#normPenanggung').val($('#norm').val());
        $('#tanggallahir2').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"});
        if(x!=='')
        {
            startLoading();
            $.ajax({
                url:base_url+"cadmission/pendaftaran_getdatapenanggung",
                data:{a:y,b:x},
                type:"POST",
                dataType:"JSON",
                success:function(result){
                    stopLoading();
                   $('#namapenanggung').val(result['namalengkap']);
                   $('#listJeniskelamin').empty();
                   $('#listJeniskelamin').html('<option value="'+result['jeniskelamin']+'">'+result['jeniskelamin']+'</option>');
                   $('#notelponpenanggung').val(result['notelpon']);
                   $('#alamatpenanggung').val(result['alamat']);
                    // pendaftaran_getenumperson(h,p);
                    $('.select2').select2();
                    return true;
                },
                error:function(result){fungsiPesanGagal();return false;}
            });
        }
        //tampilkan data enum = add penanggung jawab 
        pendaftaran_getenumperson(h,p);
        $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function refreshPenanggung() // menampilkan data penanggung
{
    var norm = $('#norm').val();
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_penanggung',
        type:'POST',
        dataType:'JSON',
        data:{norm:norm},
        success: function(result){
            $('#penanggung').empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idpasienpenanggungjawab +'" >' + result[i].namalengkap +' (' + result[i].namahubungan +')';
            }
            $('#penanggung').html(select);
            $('.select2').select2();
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    }); 
}
function copy_alamat_pasien() //copy alamat pasien
{
    var alamat = $('textarea[name="alamat"]').val();
    $('textarea[name="alamatpenanggung"]').val(alamat);
}
function pendaftaran_getenumperson(h,p) //menampilkan data enum pasien = add penanggungjawab
{
    $.ajax({
        url:'pjgetenumperson',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            edit_data_enum($('#listJeniskelamin'),result.jeniskelamin,$('#listJeniskelamin').val()); //menampilkan data jeniskelamin pasien
            edit_dropdown_pekerjaan($('#listPekerjaan'),result.pekerjaan,p); //menampilkan data pekerjaan
            edit_dropdown_hubungan($('#listHubungan'),result.hubungan,h); //menampilkan data hubungan                  
        },
        error: function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function tambah_kelurahan() //form tambah geografi kelurahan
{
    var modalSize = 'small';
    var modalTitle = 'Add Data Kelurahan';
    var modalContent = '<form action="" id="FormKelurahan">' +
                            '<div class="form-group">' +
                                '<label>Provinsi</label>' +
                                '<div id="ListProvinsi" style="display:relative;">' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" id="propinsi" name="propinsi">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kabupaten</label>' +
                                '<div id="ListKabupaten">' +
                                '<select name="kabupaten" class="kabupaten form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kecamatan</label>' +
                                '<div id="ListKecamatan">' +
                                '<select name="kecamatan" class="kecamatan form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select> ' +
                                '</div>'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kelurahan</label>' +
                                '<input type="text" name="kelurahan" placeholder="Kelurahan" id="Kelurahan" class="form-control"/>' +
                            '</div>' +
                        '</form> <a id="tambah_kecatamatan" data-toggle="tooltip" class="btn btn-warning btn-sm"><i class="fa fa-plus-square"></i> kecamatan</a>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: { //menu save atau simpan
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#propinsi').val()==='' || $('#propinsi').val()==='0') //jika data provinsi kosong
                    {
                            alert_empty('Propinsi');
                            return false;
                    }
                    else if($('#kabupaten').val()==='' || $('#kabupaten').val()==='0') //jika data kabupaten kosong
                    {
                            alert_empty('Kabupaten Kota');
                            return false;
                    }
                    else if($('#kecamatan').val()==='' || $('#kecamatan').val()==='0') //jika data kecamatan kosong
                    {
                            alert_empty('Kecamatan');
                            return false;
                    }
                    else if( ! $('#Kelurahan').val() ) //jika data kelurahan kosong
                    {
                            alert_empty('Kelurahan');
                            return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: 'save_geografikelurahan', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormKelurahan").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                return refreshKelurahan();
                            },
                            error: function(result) { //jika error
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{ //menu back
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        $('.select2').select2(); //inisialisasi dropdown select2 
        get_data_propinsi(); //ambil data provinsi
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
$(document).on('click','#tambah_kecatamatan',function(){
    if($('#propinsi').val()==='' || $('#propinsi').val()==='0') //jika data provinsi kosong
    {
            $.alert('Propinsi harap diisi.');
            return false;
    }
    else if($('#kabupaten').val()==='' || $('#kabupaten').val()==='0') //jika data kabupaten kosong
    {
            $.alert('Kabupaten harap diisi.');
            return false;
    }
    else
    {
        $.confirm({
            title: 'Tambah Kecamatan',
            content: '<input class="form-control" placeholder="Kecamatan" type="text" name="inputkecamatan"/>',
            buttons: {
                simpan: {
                    text: 'SIMPAN',
                    btnClass: 'btn-orange',
                    action: function(){
                        var inputname = $('input[name="inputkecamatan"]').val();
                        if(inputname==''){
                            $.alert({
                                content: "Harap input kecatamatan.",
                                type: 'red'
                            });
                            return false;
                        }else{
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'pendaftaran_savekecamatan', //alamat controller yang dituju (di js base url otomatis)
                                data: {kab:$('#kabupaten').val(),kec:inputname}, //
                                dataType: "JSON", //tipe data yang dikirim
                                success: function(result) { //jika  berhasil
                                    notif(result.status, result.message);
                                    if(result.status=='success'){ $('#kecamatan').empty(); $('#kecamatan').html('<option value="'+result.id+'">'+inputname+'</option>'); }
                                },
                                error: function(result) {
                                    fungsiPesanGagal();
                                    return false;
                                }
                            });
                        }
                    }
                },
                batal: function(){
                    // do nothing.
                }
            }
        });
    }

});
function get_data_propinsi() //fungsi ambil data geografi
{
    var select='';
    $.ajax({
        url:'cari_propinsi',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#ListProvinsi').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idpropinsi +'" >' + result[i].namapropinsi +'</option>';
            }
            $('#ListProvinsi').html('<select onchange="get_data_kabupaten(this.value)" class="select2 form-control" style="width:100%" id="propinsi" name="propinsi">'+
                            '<option value="0">Pilih</option>' +
                            select +
                            '</select>');           
            $('.select2').select2();
        },
        error: function(result){
            stopLoading();fungsiPesanGagal();return false;
        }
    }); 
}
function get_data_kabupaten(value) //fungsi ambil data geografi
{
    var select='';
    $.ajax({url:'cari_kabupaten',type:'POST',dataType:'JSON',data:{x:value},
        success: function(result){
            $('#ListKabupaten').empty();
            for(i in result){select = select + '<option value="'+ result[i].idkabupatenkota +'" >' + result[i].namakabupatenkota +'</option>';}
            $('#ListKabupaten').html('<select onchange="get_data_kecamatan(this.value)" class="select2 form-control" style="width:100%" id="kabupaten" name="kabupaten"><option value="0">Pilih</option>' +select+'</select>');
            $('#ListKecamatan').empty(); 
            $('#ListKecamatan').html('<select onchange="get_data_kabupaten(this.value)" class="select2 form-control" style="width:100%" id="propinsi" name="propinsi"><option value="0">Pilih</option></select>');
            $('.select2').select2();
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    }); 
}
function get_data_kecamatan(value) //fungsi ambil data geografi
{
    var select='';
    $.ajax({
        url:'cari_kecamatan',
        type:'POST',
        dataType:'JSON',
        data:{x:value},
        success: function(result){
            $('#ListKecamatan').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idkecamatan +'" >' + result[i].namakecamatan +' '+ result[i].kodepos +'</option>';
            }
            $('#ListKecamatan').html('<select class="select2 form-control" style="width:100%" id="kecamatan" name="kecamatan">'+
                             '<option value="0">Pilih</option>' +
                             select +
                             '</select>');
            $('.select2').select2();
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    }); 
}
function refreshKelurahan() // menampilkan data kelurahan
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_kelurahan',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#kelurahan').empty();
            for(var i in result) { select = select + '<option value="'+ result[i].iddesakelurahan +'" >' + result[i].namadesakelurahan +' '+ result[i].namakecamatan +' '+ result[i].namakabupatenkota +' '+ result[i].namapropinsi +' '+ result[i].kodepos+'</option>'; }
            $('#kelurahan').html(select); $('.select2').select2();
        },
        error: function(result){
            stopLoading(); fungsiPesanGagal(); return false;
        }
    }); 
}
function entri_pendaftaran() // entri pendaftaran
{
    var idjadwal = $('#poliklinik').val();
    
    startLoading();
    if($('input[name="noidentitas"]').val()==='')
    {   
        $('input[name="noidentitas"]').focus(); 
        alert('NIK pasien hara dilengkapi');
        stopLoading();
    }else if($('input[name="namapasien"]').val()==='')
    {   
        $('input[name="namapasien"]').focus();
        alert('nama pasien harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="identitas"]').val()==='0')
    {   
        $('select[name="identitas"]').focus();
        alert('identitas harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="jeniskelamin"]').val()=='0')
    {   
        alert('jenis kelamin harap dilengkapi');
        stopLoading();
    }
    else if($('input[name="tanggallahir"]').val()==='')
    {   
        $('input[name="tanggallahir"]').focus();
        alert('tanggal lahir harap dilengkapi');
        stopLoading();
    }
    else if($('textarea[name="alamat"]').val()==='')
    {   
        $('textarea[name="alamat"]').focus();
        alert('alamat harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="kelaslayanan"]').val()==='')
    {   
        alert('kelas layanan harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="jenisperiksa"]').val()==='')
    {   
        alert('jenis periksa harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="carabayar"]').val()==='')
    {   
        alert('carabayar harap dilengkapi');
        stopLoading();
    }
    else if($('select[name="carabayar"]').val()==='jknpbi' && $('input[name="nojkn"]').val()=='' || $('select[name="carabayar"]').val()==='jknnonpbi' && $('input[name="nojkn"]').val()==''){
        alert('nojkn sesuai pada identitas kartu pasien'); 
        stopLoading();
    }
    else if($('select[name="pendidikan"]').val()==='0')
    {   
        alert('pendidikan harap dilengkapi'); 
        stopLoading();
    }
    else if($('select[name="pekerjaan"]').val()==='0')
    {   
        alert('pekerjaan harap dilengkapi'); 
        stopLoading();
    }
    else if($('select[name="kelurahan"]').val()==='0')
    {   
        alert('kelurahan harap dilengkapi');
        stopLoading();
    }
    else if($('#isantri').val()=='' && $('select[name="idbahasa"]').val()==='0')
    {   
        alert('bahasa harap dilengkapi'); 
        stopLoading();
    }
    else if($('#isantri').val()=='' && $('select[name="idsuku"]').val()==='0')
    {   
        alert('suku harap dilengkapi.');
        stopLoading();
    }
    else if(idjadwal!==undefined && idjadwal !=0 && $('select[name="asalrujukan"]').val()=="0")
    {
        alert('asal rujukan harap dilengkapi');
        stopLoading();
    }
    else if(idjadwal!==undefined && idjadwal !=0 && $('select[name="jeniskunjungan"]').val()=="0")
    {
        alert('jenis kunjungan harap dilengkapi.');
        stopLoading();
    }
    //jika pasien bpjs dan poliklinik dipilih, cek status tanggal kunjungan
    else if( $('#isantri').val()=='' && idjadwal!==undefined && idjadwal !=0 && ( $('#carabayar').val()=="jknnonpbi"  || $('#carabayar').val()=="jknpbi" || $('#carabayar').val()=="bpjstenagakerja" ) )
    {
//        alert_empty('asal rujukan'); stopLoading();
        $.ajax({
            url:'bpjs_riwayatpasienprolanis', //url yang dituju
            type:'POST', //type pengiriman data
            dataType:'JSON', //type data yang dikirim
            data:{jadwal:idjadwal,norm:$('#norm').val(),tgldaftar:$('#tgldaftarpoli').val()},//set data yang dikirim dari form pendaftaran
            success: function(result){ // jika simpan berhasil    
            if(result.status=='success'){
                simpandatapendaftaran();
            }else{
                stopLoading();
                $.alert(result.message);
            }
            },
            error: function(result){
                stopLoading();
                notif(result.pesan.status,result.pesan.message);
            }
        });
    }
    else
    {
        simpandatapendaftaran();
    $('.selectpoliklinik').select2();
    }
}
//jika pasien non prb
$(document).on('click','#bpjs_pasiencholie',function(){
    var pilih = $('#bpjs_pasiencholie:checked').val();
    if(pilih=='on'){
        simpandatapendaftaran();
    }
   
});

function simpandatapendaftaran()
{
    $.ajax({
        url:'save_pendaftaran_poli', //url yang dituju
        type:'POST', //type pengiriman data
        dataType:'JSON', //type data yang dikirim
        data:$('#FormPendaftaranPoli').serialize(),//set data yang dikirim dari form pendaftaran
        success: function(result){ // jika simpan berhasil
            stopLoading();
            if(result.status == 'blacklist')
            {
                $.alert(result.message);
            }
            else
            {
                $('#pendaftaran_formcaripasien').hide();//sembunyikan form cari pasien
                $('#pilihpendaftaran_poli').show();//tampilkan form pilih jadwal poli
                notif(result.pesan.status,result.pesan.message);//tampil notifikasi
                if(result.norm) //jika setelah di simpan atau data norm pasien ada
                {
                    $('input[name="pilihpasien"]').val(result.norm);
                    $('input[name="norm"]').val(result.norm);
                    $('input[name="idperson"]').val(result.person);
                    pilihpendaftaran_poli('');//list form pilih poli
                    // yang maudiedit
                    edit_dropdown_poliklinik($('#poliklinik'),result.poliklinik,'');//menampilkan poliklinik
                    // edit_dropdown_penanggung($('#penanggung'),result.penanggung,'');//menampilkan penanggung
                    edit_data_enum($('select[name="jenisrujukan"]'),result.dt_jenisrujukan,'');//menampilkan data pekerjaan
                    edit_dropdown($('select[name="jeniskunjungan"]'),result.dt_jeniskunjungan,'');//menampilkan data pekerjaan
                }
                // tampilkan antrian jika dapat nomor antrian
                ((result.nopesan)? tampil_data_antrian($('#listAntrian'),result.nopesan.number):'');
                // mengarahkan ke halaman list pendaftaran jika simpan data pendaftaran berhasil jika tidak tampilkan grup jadwal
                ((result.idpoliklinik)? window.location.href=base_url +"cadmission/pendaftaran_poli" : edit_dropdown_jadwal_grup($('#grupjadwal'),result.grupjadwal));                
                caridesakelurahan();
                cariklinikrujukan();
            }
        },
        error: function(result){
            stopLoading();
            console.log(result);
            notif(result.pesan.status,result.pesan.message);
            // console.log(result);
        }
    });
}
function kosongkanFormPendaftaran(value='') //fungsi kosongkan inputan
{
    startLoading();
    $('input[name="idperson"]').val(''); 
    $('input[name="norm"]').val(''); 
    $('input[name="noidentitas"]').val(''); 
    $('input[name="namapasien"]').val(''); 
    $('input[name="tempatlahir"]').val(''); 
    $('input[name="tanggallahir"]').val(''); 
    $('input[name="notelepon"]').val(''); 
    $('textarea[name="alamat"]').val('');
    $.ajax({
        url:'tampil_pasien', 
        type:'POST', 
        dataType:'JSON', 
        data:{id:value},
        success: function(result){
            stopLoading();
            if(result.bpjs!=null) //jika data bpjs tidak kosong
            {
                $('input[name="idjenispeserta"]').val(result.bpjs['idjenispeserta']);
                $('input[name="jenispeserta"]').val(result.bpjs['jenispeserta']);
                $('input[name="idkelas"]').val(result.bpjs['idkelas']);
                $('input[name="kelas"]').val(result.bpjs['kelas']);
                $('input[name="idstatuspeserta"]').val(result.bpjs['idstatuspeserta']);
                $('input[name="statuspeserta"]').val(result.bpjs['statuspeserta']);
                $('input[name="tanggalcetak"]').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", new Date(result.bpjs['tglcetakkartu']));
                $('textarea[name="informasi"]').val(result.bpjs['informasi']);
                edit_data_enum($('#jeniskelamin'),result.jeniskelamin,result.bpjs['jeniskelamin']); //menampilkan data jeniskelamin pasien
            } 
            else //selain itu
            {
                //menampilkan data bpjs
                $('input[name="nojkn"]').val('');
                $('input[name="jenispeserta"]').val('');
                $('input[name="kelas"]').val('');
                $('input[name="statuspeserta"]').val('');
                $('input[name="tanggalcetak"]').val('');
                $('textarea[name="informasi"]').val('');
                edit_data_enum($('#jeniskelamin'),result.jeniskelamin,''); //menampilkan data jeniskelamin pasien
            }
            //menampilkan jenis pasien
            editdataoption($('#jenispasien'),result.jenispasien, '');
            $('input[name="noidentitas"]').val(((result.bpjs!=null)? result.bpjs['nik'] :''));
            $('input[name="namapasien"]').val((result.bpjs!=null)?ql_identitas( result.bpjs['namalengkap']):'');
            $('input[name="notelepon"]').val(((result.bpjs!=null)? result.bpjs['notelpon'] :''));
            $('input[name="tanggallahir"]').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", new Date(((result.bpjs!=null)?result.bpjs['tanggallahir'] : '') )); //Initialize Date picker

            edit_data_enum($('#agama'),result.agama,''); //menampilkan data agama pasien
            edit_data_enum($('#identitas'),result.identitas,'','add'); //menampilkan data agama pasien
            edit_data_enum($('#jenisperiksa'),result.jenisperiksa,''); //menampilkan data jenis peserta
            edit_data_enum($('#carabayar'),result.carabayar,''); //menampilkan data cara bayar
            edit_data_enum($('#golongandarah'),result.golongandarah,'','add'); //menampilkan data golongandarah pasien
            edit_data_enum($('#rhesus'),result.rhesus,'','add'); //menampilkan data rhesus pasien
            edit_data_enum($('#perkawinan'),result.perkawinan,''); //menampilkan data perkawinan pasien
            edit_dropdown_kelurahan($('#kelurahan'),result.kelurahan,''); //menampilkan data kelurahan
            edit_dropdown($('#idbahasa'),result.bahasa,'');//menampilkan data bahasa
            edit_dropdown($('#idsuku'),result.suku,'');//menampilkan data suku
            edit_dropdown_kelaslayanan($('#listkelaslayanan'),result.kelaslayanan,''); //menampilkan data kelas layanan
            edit_dropdown_pendidikan($('#pendidikan'),result.pendidikan,''); //menampilkan data pendidikan
            edit_dropdown_pekerjaan($('#pekerjaan'),result.pekerjaan,''); //menampilkan data pekerjaan
            edit_data_enum($('select[name="jenisrujukan"]'),result.jenisrujukan,'');
            edit_skrining($('#skrining'),result.skrining,'');            
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
    
    //yangmaudiedit
    pilihpendaftaran_poli('');//list form pilih poli
    edit_dropdown_poliklinik($('#poliklinik'),'',''); //menampilkan poliklinik
    tampil_data_antrian($('#listAntrian')); //menampilkan data antrian
    // $('.select2').select2();
}
// tampilkan data pasien
$("#pilihpasien").on("keydown", function (e) {
    if (e.keyCode == 13) {
        var id = $('#pilihpasien').val();
        if(!id) //jika tidak ada data inputan
        {
            kosongkanFormPendaftaran();
        }
        else //selain itu
        {
            function_getdatapasien(id,'add'); //panggil fungsi menampilkan data pasien yang sudah ada
        }
    }
});

/**
 * Mahmud, clear
 * fungsi menampilkan data pasien yang sudah ada
 */
function function_getdatapasien(id,mode)
{
    $('#pendaftaran_formcaripasien').hide();
    startLoading();
    $.ajax({
        url:'tampil_pasien',
        type:'POST',
        dataType:'JSON',
        data:{id:id,mode:mode},
        success: function(result){
            stopLoading();
            
            if(result.cekstatus)
            {
                $.alert(result.message);
            }
            
            if(result.pasien==null) // jika data pasien kosong
            {
                kosongkanFormPendaftaran(id);
            }
            else // selain itu
            {
                $('input[name="pilihpasien"]').val(result.norm);
                $('input[name="idperson"]').val(result.pasien['idperson']);
                $('input[name="norm"]').val(result.norm);
                $('input[name="noidentitas"]').val(result.pasien['nik']);
                $('input[name="namapasien"]').val(ql_identitas(result.pasien['namalengkap']));
                if(result.pendaftaran.length!==0)
                {
                    $('input[name="tglKunjungan"]').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", new Date(result.pendaftaran[0]['tanggalrujukan']));
                }
                else
                {
                    $('input[name="tglKunjungan"]').val('');
                }
                $('input[name="tempatlahir"]').val(result.pasien['tempatlahir']);
                $('#tanggallahir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", new Date(result.pasien['tanggallahir']));
                $('input[name="notelepon"]').val(result.pasien['notelpon']);
                $('textarea[name="alamat"]').val(result.pasien['alamat']);
                edit_data_enum($('#agama'),result.agama,result.pasien['agama']);//menampilkan data agama pasien
                edit_data_enum($('#identitas'),result.identitas,result.pasien['identitas'],'add');//menampilkan data identitas pasien
                edit_data_enum($('#jenisperiksa'),result.jenisperiksa,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['jenispemeriksaan'])); //menampilkan data jenis peserta
                edit_data_enum($('#carabayar'),result.carabayar,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['carabayar'])); //menampilkan data cara bayar
                edit_data_enum($('#jeniskelamin'),result.jeniskelamin,result.pasien['jeniskelamin']);//menampilkan data jeniskelamin pasien
                edit_data_enum($('#golongandarah'),result.golongandarah,result.pasien['golongandarah'],'add');//menampilkan data golongandarah pasien
                edit_data_enum($('#rhesus'),result.rhesus,result.pasien['rh'],'add');//menampilkan data rhesus pasien
                edit_data_enum($('#perkawinan'),result.perkawinan,result.pasien['statusmenikah']);//menampilkan data perkawinan pasien
                edit_dropdown_kelurahan($('#kelurahan'),result.kelurahan,result.pasien['iddesakelurahan']);//menampilkan data kelurahan
                edit_dropdown_kelaslayanan($('#listkelaslayanan'),result.kelaslayanan,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['idkelas'])); //menampilkan data kelas layanan
                edit_dropdown_pendidikan($('#pendidikan'),result.pendidikan,result.pasien['idpendidikan']);//menampilkan data pendidikan
                edit_dropdown_pekerjaan($('#pekerjaan'),result.pekerjaan,result.pasien['idpekerjaan']);//menampilkan data pekerjaan
                edit_dropdown($('select[name="asalrujukan"]'),result.klinikperujuk,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['idklinikperujuk']));//menampilkan data pekerjaan
                edit_dropdown($('select[name="jeniskunjungan"]'),result.jeniskunjungan,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['idjeniskunjungan']));//menampilkan data pekerjaan
                edit_data_enum($('select[name="jenisrujukan"]'),result.jenisrujukan,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['jenisrujukan']));
                edit_skrining($('#skrining'),result.skrining,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['kategoriskrining']));
                
                //menampilkan jenis pasien
                editdataoption($('#jenispasien'),result.jenispasien, result.nojkn['ispasienkeluargars'],'');
                // cariklinikrujukan();
                //menampilkan data bpjs
                $('input[name="nojkn"]').val(result.nojkn['nojkn']);
                $('input[name="idjenispeserta"]').val(((result.bpjs!=null)?result.bpjs['idjenispeserta']:''));
                $('input[name="jenispeserta"]').val(((result.bpjs!=null)?result.bpjs['jenispeserta']:''));
                $('input[name="idhakkelas"]').val(((result.bpjs!=null)?result.bpjs['idkelas']:''));
                $('input[name="kelas"]').val(((result.bpjs!=null)?result.bpjs['kelas']:''));
                $('input[name="idstatuspeserta"]').val(((result.bpjs!=null)?result.bpjs['idstatuspeserta']:''));
                $('input[name="statuspeserta"]').val(((result.bpjs!=null)?result.bpjs['statuspeserta']:''));
                $('input[name="tanggalcetak"]').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((result.bpjs!=null)?  new Date(result.bpjs['tglcetakkartu']):'' ));
                $('textarea[name="informasi"]').val(((result.bpjs!=null)?result.bpjs['informasi']:'')); 
                pilihpendaftaran_poli(((result.pendaftaran.length==0)?'':result.pendaftaran[0]['waktuperiksa']));//list form pilih poli

                if(result.pendaftaran.length>1)
                {  
                    var unit='';
                    for(x in result.pendaftaran)
                    {
                        unit+=result.pendaftaran[x].idjadwal+',';
                    }    
                }
                
                if(mode=='edit' || mode =='pindah')
                {
                    if(result.pendaftaran[0]['caradaftar']=='online')
                    {
                        $('#caradaftar').prop('checked',true);
                        $('input[type="checkbox"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green'})
                    }
                }
                $('#unitteripilih').val(((result.pendaftaran.length==0)?'':((result.pendaftaran.length>1)?unit:result.pendaftaran[0]['idjadwal'])));
                pendaftaran_gruppemeriksaan(((result.pendaftaran.length==0)?'0':result.pendaftaran[0]['idjadwalgrup']));
                edit_dropdown_jadwal_grup($('#grupjadwal'),result.grupjadwal,((result.pendaftaran.length==0)?'':result.pendaftaran[0]['idjadwalgrup']));
                tampil_data_antrian($('#listAntrian'),((result.pendaftaran.length==0)?'':result.pendaftaran[0]['nopesan']));//menampilkan data antrian
                $('input[name="nosep"]').val(((result.pendaftaran.length==0)?'':result.pendaftaran[0]['nosep']));//menampilkan data nosep
                $('input[name="norujukan"]').val(((result.pendaftaran.length==0)?'':result.pendaftaran[0]['norujukan']));//menampilkan data norujukan
                $('input[name="idpendaftaran"]').val(((result.pendaftaran.length==0)?'':result.pendaftaran[0]['idpendaftaran']));//menampilkan data idpendaftaran
                //--SET disabled tanggal periksa
                if(mode=='edit' && result.pendaftaran[0]['isantri']==1)
                {
                    $('#tgldaftarpoli').attr('disabled',true);
                    $('#poliklinik').attr('disabled',true);
                }
                $('input[name="isantri"]').val( ((result.pendaftaran.length==0)?'': result.pendaftaran[0]['isantri'] ) );
                edit_dropdown($('#idbahasa'),result.bahasa,result.pasien['idbahasa']);//menampilkan data bahasa
                edit_dropdown($('#idsuku'),result.suku,result.pasien['idsuku']);//menampilkan data suku
                
                setTimeout(function(){
                    listpenanggungpasien();
                },800);
            }
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
    $('.selectpoliklinik').select2();
}



function cariklinikrujukan()
{
    $('select[name="asalrujukan"]').select2({
      allowClear: true,
      ajax: { url: base_url+"cadmission/serchselect_klinikperujuk", type: "post", dataType: 'json', delay: 250,
       data: function (params) { return { cari: params.term }; },
       processResults: function (response) { return { results: response, }; },
      }
     });
}
//
//function edit_dropdown(column,data,selected_data)
//{
//    var select ='<option value="0">Pilih</option>';
//    var selected;
//    column.empty();
//    for(var i in data)
//    {
//        if(data[i].dataid==selected_data){ selected='selected'; }
//        else{selected='';}
//        select = select + '<option value="'+ data[i].dataid +'" '+ selected +' >' + data[i].datatext +'</option>';
//    }
//    column.html(select);
//}

function caridesakelurahan()
{
    $('select[name="kelurahan"]').select2({
      allowClear: true,
      ajax: { url: base_url+"cadmission/serchselect_kelurahan", type: "post", dataType: 'json', delay: 250,
       data: function (params) { return { cari: params.term }; },
       processResults: function (response) { return { results: response, }; },
      }
     });
}

$("#pilihpasien").on("keyup", function (e) { // cari data pasien
    var value = $('#pilihpasien').val();
    if ((e.keyCode == 16 || e.keyCode == 17) && value.length >= 3) 
    { //&& value.length > 3
        var datalist='';
        var i=0;
        if(value)
        {   
            limitcaripasien = (e.keyCode == 16) ? limitcaripasien + 100 : limitcaripasien - 100;
            if (limitcaripasien < 0 || limitcaripasien > numcaripasien) limitcaripasien = 0;
            $.ajax({
                url:'cari_pasien',
                type:'POST',
                dataType:'JSON',
                data:{id:value,lim:limitcaripasien,num:numcaripasien},
                success: function(result){
                // console.log(result);
                    $('#ListPasien').empty();
                    for(i in result['data'])
                    {
                        datalist = datalist + '<option value="'+ result['data'][i].norm+'-'+ result['data'][i].nojkn+'-'+ result['data'][i].nik+'-'+ result['data'][i].namalengkap+'">';
                    }
                    if (numcaripasien == 0) numcaripasien = result['jumlah'][0].jumlah;
                    $('#ListPasien').html(datalist);
                },
                error: function(result){
                    stopLoading();
                    console.log(result);
                }
            });
        }
    }
    else
    {
        limitcaripasien = -100;
        numcaripasien   = 0;
    }
});
function edit_data_enum(column,data,selected_data,addoption='') // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{
    var select= ((addoption=='')?'':'<option value="0">Pilih</option>');
    var selected='';
    column.empty();
    
    for(var i in data)
    {
        if(data[i]==selected_data){ selected='selected';}
        else{selected='';}
        select = select + '<option value="'+ data[i] +'" '+ selected +' >' + data[i] +'</option>';
    }
    column.html(select);
}
function tampil_data_antrian(column,no='') //fungsi menampilkan data antrian
{
    column.empty();
    column.html('No.Pesan:' + no);
    $('#noAntrian').val(no);
}
function pilihpendaftaran_poli(tanggalpoli)
{
    var htmlTag = ''+
                  '<div class="col-sm-1" style="margin-left:8px; width:10%;"> <label>Tanggal</label> <input id="tgldaftarpoli" class=" form-control" type="text" name="pilihtanggalantrian" onchange="cari_poliklinik(this.value)"></div>'+
                  '<div class="col-sm-1" style="width:12%;"> <label>Grup</label> <select class="form-control" name="grupjadwal" id="grupjadwal" onchange="pendaftaran_gruppemeriksaan(this.value)"></select></div>'+
                  
                  '<div class="col-sm-5" id="gruppemeriksaan">'+
                  '<label >Poliklinik</label>'+
                  '<select class="selectpoliklinik form-control" style="width:100%" id="poliklinik" name="poliklinik" >'+
                  '</select>'+
                  '</div>'+
                    '<div style="margin-left:2px;padding-top:25px;"><label style="padding-left:10px;" class="text text-blue" id="listAntrian"></label></div>';
    $('#pilihpendaftaran_poli').empty();
    $('#pilihpendaftaran_poli').html(htmlTag);
    $('#tgldaftarpoli').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((tanggalpoli=='' || tanggalpoli==undefined)? 'now' : new Date(tanggalpoli) )); //Initialize Date picker

$('.selectpoliklinik').select2();
}
function pendaftaran_gruppemeriksaan(value)
{
    // jika ada grup poli maka nama poliklonik dalam array selain itu tidak
    var data = '<label >Poliklinik</label><select class="selectpoliklinik form-control" style="width:100%" id="poliklinik" name="'+((value!=='0' && value!==null) ? 'poliklinik[]' : 'poliklinik')+'" '+((value!=='0' && value!==null) ? 'multiple="multiple"' : '')+'></select>';
    $('#gruppemeriksaan').empty();
    $('#gruppemeriksaan').html(data);
    cari_poliklinik($('#tgldaftarpoli').val(),value);
    // mencegah urut berdasarkan tag || urut berdasarkan yang dipilih 
    $("select").on("selectpoliklinik:select", function (evt) {
      var element = evt.params.data.element;
      var $element = $(element);  
      $element.detach();
      $(this).append($element);
      $(this).trigger("change");
    });
                  
}
function cari_poliklinik(value,grup='') //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    startLoading();
    $.ajax({
        type: "POST",
        url: 'cari_jadwalpoli',
        data:{date: ambiltanggal(value),grup:grup},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            edit_dropdown_poliklinik($('#poliklinik'),result,$('#unitteripilih').val());//menampilkan poliklinik
            $('.selectpoliklinik').select2();
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function edit_dropdown_poliklinik(column,data,selected_data) //fungsi edit poliklinik
{
    // data jadwal yang dipilih split menjadi array
    var selectarray = split_to_array(selected_data);

    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(var i in data)
        {
            
            select = select + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" '+ ((selectarray.includes(data[i].idjadwal))?'selected':'') +' >' + data[i].namaunit +' '+ data[i].namadokter +' ' +if_empty(data[i].tanggal,'')+' </option>';
        }
        column.html('<option value="0">Pilih</option>'+ select );
    }   
        $('.selectpoliklinik').select2();
}
function edit_dropdown_jadwal_grup(column,data,selected_data)
{
    var select ='';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].idjadwalgrup==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idjadwalgrup +'" '+ selected +' >' + data[i].namagrup +'</option>';
    }
    column.html(select);
}
function edit_dropdown_hubungan(column,data,selected_data) //fungsi edit dropdown
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idhubungan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idhubungan +'" '+ selected +' >' + data[i].namahubungan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_pendidikan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].idpendidikan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpendidikan +'" '+ selected +' >' + data[i].namapendidikan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_pekerjaan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].idpekerjaan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpekerjaan +'" '+ selected +' >' + data[i].namapekerjaan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_kelurahan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].iddesakelurahan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].iddesakelurahan +'" '+ selected +' >' + data[i].namadesakelurahan +' '+ data[i].namakecamatan +'</option>';
    }
    column.html(select);
}
function edit_dropdown_kelaslayanan(column,data,selected_data)
{
    // console.log(name);
    var select ='';
    var selected;
    column.empty();
    for(var i in data)
    {
        if(data[i].idkelas==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idkelas +'" '+ selected +' >' + data[i].kelas+'</option>';
    }
    column.html(select);
}
function reset_pendaftaran() //reset data pendaftaran
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset Form?',
        buttons: {
            confirm: function () { 
                $('#pilihpasien').val('');
                kosongkanFormPendaftaran();
            },
            cancel: function () {               
            }            
        }
    }); 
}
function back_pendaftaran() //kembali ke halaman list pendaftaran poli
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list pendaftaran?',
        buttons: {
            confirm: function () { 
                window.location.href='pendaftaran_poli';
            },
            cancel: function () {               
            }            
        }
    });   
}

//////////////// SETING INPUTAN ketika tekan enter ganti inputan selanjutnya /////////////////////
$("#nosep").on("keydown",function(e){if(e.keyCode == 13){$("input[name='noidentitas']").focus();}});
$("input[name='noidentitas']").on("keydown",function(e){if(e.keyCode == 13){$("input[name='namapasien']").focus();}});
$("input[name='namapasien']").on("keydown",function(e){if(e.keyCode == 13){$("select[name='jeniskelamin']").focus();}});
function keydown_jeniskelamin(){ $("input[name='tanggallahir']").focus();}
$("input[name='tanggallahir']").on("keydown",function(e){if(e.keyCode == 13){$("input[name='tempatlahir']").focus();}});
$("input[name='tempatlahir']").on("keydown",function(e){if(e.keyCode == 13){$("input[name='notelepon']").focus();}});
$("input[name='notelepon']").on("keydown",function(e){if(e.keyCode == 13){$("textarea[name='alamat']").focus();}});
$("textarea[name='alamat']").on("keydown",function(e){if(e.keyCode == 13){$("select[name='kelurahan']").focus();}});
// /// --  pencarian pasien secara spesifik
// function pencarian_spesifik()
// {
//     var formContent='<div class="form-group">' +
//                         '<div class="col-sm-4">'+
//                         '<label>Nama Pasien</label>' +
//                         '<input type="text" id="namacariipasienspesifik" placeholder="Nama pasien" class="form-control"/>' +
//                         '</div>' +
//                     '<div class="col-sm-2">' +
//                         '<label>Tanggal Lahir</label>' +
//                         '<input type="text" id="tanggallahircariipasienspesifik" placeholder="dd/mm/yyyy" class="form-control"/>' +
//                     '</div>' +
//                     '<div class="col-sm-5">' +
//                         '<label>Alamat</label>' +
//                         '<textarea class="form-control" id="alamatcariipasienspesifik" placeholder="Alamat" rows="1"></textarea>'+
//                         // '<input type="text" name="alamat" placeholder="Alamat " class="form-control"/>' +
//                     '</div>' +
//                     '<div style="padding-top:24px;"><a class="btn btn-success" onclick="caripasienspesifik()">Cari</a></div>'+
//                     '</div>'+
//                     '<div class="form-group"><div class="col-sm-12"><br>'+
//                     '<table id="caripasien" class="table table-striped table-borderet">'+
//                     '<thead>'+
//                     '<tr class="bg-warning"><th width="5%">No</th><th>NIK</th><th>NORM</th><th>NAMA</th><th>ALAMAT</th><th width="3%"></th></tr>'+
//                     '</thead>'+
//                     '<tbody id="tampilpencarianpasien">'+
//                     '<tr class="text-center"><td colspan="6">No data available in table</td></tr>'+
//                     '</tbody>'+
//                     '</table></div>';
//     $('#formpencarianspesifik').empty();
//     var modalTitle = 'Cari Pasien';
//     var modalContent = '<form id="formpencarianspesifik">'+formContent+'</form>';
//     $.confirm({ //aksi ketika di klik menu
//         title: modalTitle,
//         content: modalContent,
//         columnClass: 'xl',
//         buttons: {
//             formReset:{ //menu back
//                 text: 'Kembali',
//                 btnClass: 'btn-danger'
//             }
//         },
//         onContentReady: function () { //ketika form di tampilkan
//         var jc = this;
//         this.$content.find('form').on('submit', function (e) {
//             // if the user submits the form by pressing enter in the field.
//             e.preventDefault();
//             jc.$$formSubmit.trigger('click'); // reference the button and click it
//         });
//         }
//     });
// }
/// --- cari pasien spesifik
function caripasienspesifik()
{   if($('#namacariipasienspesifik').val()==='' && $('#normcariipasienspesifik').val()==='' && $('#alamatcariipasienspesifik').val()==='' && $('#tanggallahircariipasienspesifik').val()==='') //jika no identitas kosong
    {
        alert_empty('data pencarian');
        return false;
    }
    else
    {
        var formContent ='<table id="caripasien" class="table table-striped table-borderet">'+
        '<thead>'+
        '<tr class="bg-warning"><th width="5%">No</th><th>NIK</th><th>NORM</th><th>NAMA</th><th width="80px;">TANGGAL LAHIR</th><th>ALAMAT</th><th><i class="fa fa-credit-card"></i></th><th width="3%"></th></tr>'+
        '</thead>'+
        '<tbody id="tampilpencarianpasien">';
        var html='', no=0;
        $.ajax({
            type: "POST",
            url: 'caripasienspesifik',
            data:{rm:$('#normcariipasienspesifik').val(), nama:$('#namacariipasienspesifik').val(), alamat:$('#alamatcariipasienspesifik').val(),tanggallahir:$('#tanggallahircariipasienspesifik').val()},
            dataType: "JSON",
            success: function(result) {
                for(i in result){html = html + '<tr><td>'+ ++no +'</td><td>'+result[i].nik+'</td><td>'+result[i].norm+'</td><td>'+result[i].namalengkap+'</td><td>'+result[i].tanggallahir+'</td><td>'+result[i].alamat+'</td><td>'+result[i].cetakkartu+'</td><td><a class="btn btn-default btn-xs" id="pendaftaranpasienbaru" rm="'+result[i].norm+'" idp="'+result[i].idperson+'" data-toggle="tooltip" data-original-title="Pilih"><i class="fa fa-check"></i></a></td></tr>';}
                formContent+=html;
                var modalContent = '<form id="formpencarianspesifik">'+formContent+'</tbody></table></div></form>';
                var modalTitle = 'Hasil Pencarian Pasien';
                $.confirm({ //aksi ketika di klik menu
                    title: modalTitle,
                    content: modalContent,
                    columnClass: 'xl',
                    buttons: {
                        formReset:{ //menu back
                            text: 'Kembali',
                            btnClass: 'btn-danger'
                        }
                    },
                    onContentReady: function () { //ketika form di tampilkan
                    $('[data-toggle="tooltip"]').tooltip();
                    $('#caripasien').DataTable();
                    $('#pendaftaran_formcaripasien').hide();
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                    }
                });
        },
            error: function(result) {
                console.log(result.responseText);
            }
        });
    }
}
$(document).on('click','#pendaftaranpasienbaru',function(){
    $('.jconfirm').hide();
    var rm  = $(this).attr('rm');
    var idp = $(this).attr('idp');
    var value = rm+'-'+'0'+'-'+idp
    function_getdatapasien(value,'add');
});

// pencarian pasien dengan norm enter keyboard
$("#normcariipasienspesifik").on("keydown", function (e) {
    var norm = $("#normcariipasienspesifik").val();
    if(e.keyCode==13)
    {
        if(norm.length<8)
        {
            alert_empty('norm');
            return false;
        }
        else
        {
            function_getdatapasien(norm,'add');
        }

    }
});

// BRIDGING BPJS
// mahmud clear :: pencarian data pasien berdasarkan nomor sep bpjs
function bpjs_getdtpesertabysep()
{
    startLoading();
    var nosep = $('input[name="nosep"]').val();
    if(nosep=='')
    {
        $('input[name="nosep"]').focus();
        alert_empty('nosep');
        return false;
    }
    else
    {
        $.ajax({
            url:base_url+"cadmission/bpjs_getdtpesertabysep",
            data:{nosep:nosep},
            type:"POST",
            dataType:"JSON",
            success: function(data){
            stopLoading();
            function_getdatapasien(data.norm,'add');
            setTimeout(function() {
                $('input[name="nosep"]').val(nosep);
                bpjs_listdtpeserta(data);
                }, 100);
            },
            error: function(data)
            {
                stopLoading();
                fungsiPesanGagal();
                return false;
            } 
        });
    }
}

// mahmud clear :: pencarian data pasien berdasarkan nomor rujukan bpjs
function bpjs_getdtpesertabyrujukan()
{
    startLoading();
    var norujukan = $('input[name="norujukan"]').val();
    if(norujukan=='')
    {
        $('input[name="norujukan"]').focus();
        alert_empty('norujukan');
        return false;
    }
    else
    {
        $.ajax({
            url:base_url+"cadmission/bpjs_getdtpesertabyrujukan",
            data:{norujukan:norujukan},
            type:"POST",
            dataType:"JSON",
            success: function(data){
            stopLoading();
            function_getdatapasien(data.norm,'add');
            setTimeout(function(){bpjs_listdtpeserta(data);}, 100);
            },
            error: function(data)
            {
                stopLoading();
                fungsiPesanGagal();
                return false;
            } 
        });
    }
}
// mahmud clear :: pencarian data pasien berdasarkan nomor jkn
function bpjs_getdtpesertabynojkn()
{
    startLoading();
    var nojkn = $('input[name="nojkn"]').val();
    if(nojkn=='')
    {
        $('input[name="nojkn"]').focus();
        alert_empty('nojkn');
        return false;
    }
    else
    {
        $.ajax({url:base_url+"cadmission/bpjs_getdtpesertabynojkn",data:{nojkn:nojkn},type:"POST",dataType:"JSON",
            success: function(data){ stopLoading(); $('input[name="norujukan"]').val(data.noKunjungan); $('input[name="tglKunjungan"]').datepicker("setDate",tglformatindo1(data.tglKunjungan)); $('input[name="noidentitas"]').val(data.peserta.nik); $('input[name="kelas"]').val(data.peserta.hakKelas.keterangan); $('input[name="idhakkelas"]').val(data.peserta.hakKelas.kode); $('input[name="namapasien"]').val(data.peserta.nama); $('input[name="jenispeserta"]').val(data.peserta.jenisPeserta.keterangan); $('input[name="idjenispeserta"]').val(data.peserta.jenisPeserta.kode); $('input[name="nojkn"]').val(data.peserta.noKartu); $('input[name="statuspeserta"]').val(data.peserta.statusPeserta.keterangan); $('input[name="idstatuspeserta"]').val(data.peserta.statusPeserta.kode); $('input[name="tanggalcetak"]').val(tglformatindo1(data.peserta.tglCetakKartu)); $('input[name="tanggallahir').datepicker("setDate",tglformatindo1(data.peserta.tglLahir)); $('select[name="jeniskelamin"]').empty(); $('select[name="jeniskelamin"]').html('<option value="laki-laki" '+((data.peserta.sex=='L')? 'selected':'')+' >laki-laki</option><option '+((data.peserta.sex!=='L')? 'selected':'')+' value="wanita">wanita</option>'); },
            error: function(data){ stopLoading(); fungsiPesanGagal(); return false; } 
        });
    }
}
// mahmud clear :: menampilkan data pasien dari hasil bridging
function bpjs_listdtpeserta(data)
{
    var peserta = data.rujukan.rujukan.peserta;
    $('input[name="pilihpasien"]').val(((data.norm==null)?'':data.norm)); $('input[name="norujukan"]').val(data.rujukan.rujukan.noKunjungan); $('input[name="noidentitas"]').val(peserta.nik); $('input[name="kelas"]').val(peserta.hakKelas.keterangan); $('input[name="idhakkelas"]').val(peserta.hakKelas.kode); $('input[name="namapasien"]').val(peserta.nama); $('input[name="notelepon"]').val(peserta.mr.noTelepon); $('input[name="jenispeserta"]').val(peserta.jenisPeserta.keterangan); $('input[name="idjenispeserta"]').val(peserta.jenisPeserta.kode); $('input[name="nojkn"]').val(peserta.noKartu); $('input[name="statuspeserta"]').val(peserta.statusPeserta.keterangan); $('input[name="idstatuspeserta"]').val(peserta.statusPeserta.kode); $('input[name="tanggalcetak"]').val(tglformatindo1(peserta.tglCetakKartu)); $('input[name="tanggallahir').datepicker("setDate",tglformatindo1(peserta.tglLahir)); $('input[name="tglKunjungan"]').datepicker("setDate",tglformatindo1(data.rujukan.rujukan.tglKunjungan)); $('select[name="jeniskelamin"]').empty(); $('select[name="jeniskelamin"]').html('<option value="laki-laki" '+((peserta.sex=='L')? 'selected':'')+' >laki-laki</option><option '+((peserta.sex!=='L')? 'selected':'')+' value="wanita">wanita</option>');
    if(data.norm==null){$('#pilihpendaftaran_poli').hide();}
        else{$('#pilihpendaftaran_poli').show();}
}

function selisih15HariPasienBpjs(value)
{
//    if(value=='jknpbi' || value=='jknnonpbi'){
//        var idunit_=split_to_array($('select[name="poliklinik"]').val())[0];
//        if(idunit_==0){
//            notif('danger','Jadwal Periksa Belum Dipilih.!');
//            setCarabayarMandiri();
//        }else{
//            var tanggal=$('#poliklinik option:selected').text().substr(-18,8);
//            $.ajax({
//                url:base_url+"cadmission/selisih15hari_pasienbpjs",
//                data:{norm:$('#norm').val(),idunit:idunit_,carabayar:value,tanggal:tanggal},
//                type:"POST",
//                dataType:"JSON",
//                success:function(result){
//                    if(result.selisih < 15 && idunit_!=2){
//                        notif('danger','Pasien tidak bisa dilayani dengan jaminan '+value+' silahkan pilih cara pembayaran yang lain, terakhir pasien periksa pada tanggal '+result.riwayat+' pada poli yang sama!');
//                        setCarabayarMandiri();
//                    }
//                    if(result.selisih < 3 && idunit_==2){
//                        notif('danger','Pasien tidak bisa dilayani dengan jaminan '+value+' silahkan pilih cara pembayaran yang lain, terakhir pasien periksa pada tanggal '+result.riwayat+' pada poli yang sama!');
//                        setCarabayarMandiri();
//                    }
//                },
//                error:function(result){fungsiPesanGagal();return false;}
//            });
//        }
//    }
}
function setCarabayarMandiri()
{
    $.ajax({
        url:base_url+"cadmission/get_enumcarabayar",
        type:"POST",
        dataType:"JSON",
        success:function(result){
            var option='';
            for(x in result){
                option+='<option value="'+result[x]+'">'+result[x]+'</option>';
            }
            $('select[name="carabayar"]').empty();
            $('select[name="carabayar"]').html(option);
            
        },
        error:function(result){fungsiPesanGagal();return false;}
    });
}


// menampilkan data penanggung pasien
// start
function listpenanggungpasien()
{
    setTimeout(function(){
    $.ajax({url:base_url+"cadmission/listpenanggungpasien",type:"POST",dataType:"JSON",data:{rm:$('#norm').val()},
        success:function(result){
                var dt='',no=0;
                for(x in result){ ++no; dt+='<tr><td>'+result[x].namalengkap+'</td><td>'+result[x].namahubungan+'</td><td><span class="label '+((result[x].statuspenanggung=='bukan')?'label-danger':'label-success')+'">'+result[x].statuspenanggung+'</label> </td><td>'+addupdatestatuspenanggung(result[x].idpasienpenanggungjawab,result[x].statuspenanggung)+' <a onclick="tambah_penanggung('+result[x].idperson+','+result[x].norm+','+result[x].idhubungan+','+result[x].idpekerjaan+')" class="btn btn-warning btn-xs" '+tooltip('Ubah')+'><i class="fa fa-edit"></i></a> <a class="btn btn-danger btn-xs" '+tooltip('Hapus')+'><i class="fa fa-trash"></i></a></td></tr>';}
                $('#jumlahpenanggungpasien').empty(); $('#jumlahpenanggungpasien').text(no);
                $('#listpenanggung').empty(); $('#listpenanggung').html('<table id="tbllistpenanggung" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%"><thead><tr><th>Nama</th><th>Hubungan</th><th>Status</th><th><a class="btn btn-primary btn-xs" '+tooltip('Tambah')+' onclick="tambah_penanggung()"><i class="fa fa-plus"></i></a></th></tr></thead><tbody>'+dt+'</tbody></table>');
                $('[data-toggle="tooltip"]').tooltip();
        },
        error:function(result){fungsiPesanGagal();return false;}
    });
},1000);
}
// tambah ubah status penanggung
function addupdatestatuspenanggung(id,status){ return (status=='penanggung') ? '<a id="setstatuspenanggnung" idp="'+id+'" status="'+status+'" '+tooltip('Set Bukan Penanggung')+' class="btn btn-xs btn-danger"><i class="fa fa-minus-circle"></i></a>' : '<a id="setstatuspenanggnung" idp="'+id+'" status="'+status+'" '+tooltip('Set Penanggung')+' class="btn btn-xs btn-success"><i class="fa fa-plus-circle"></i></a>' ; }
// saveupdatestatuspenanggung
$(document).on('click','#setstatuspenanggnung',function(){
    var id = $(this).attr('idp');
    var status = ($(this).attr('status')=='bukan') ? 'penanggung' : 'bukan' ;
    $.ajax({url:base_url+"cadmission/saveupdatestatuspenanggung",type:"POST",dataType:"JSON",data:{i:id,s:status},
        success:function(result){
            notif('success','Ubah status penanggung berhasil.!');
            return listpenanggungpasien();
        },
        error:function(result){fungsiPesanGagal();return false;}
    });

});
// end


function setwaktulayanan()
{
    startTimeLayanan();
    var mulai = $('#waktusekarang').val();
    $('#waktumulai').val(mulai);
}


//cek kuota pendaftaran pasien
$(document).on('change','#poliklinik',function(){
    var idjadwal = this.value.split(",")[1];
    startLoading();
    $.ajax({
        url:base_url+"cadmission/get_kuotajadwalpemeriksaan",
        data:{idjadwal:idjadwal},
        type:"POST",
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(result.status == 1)
            {
                var sisa        = result.antrian_sisa;
                var maksimal    = result.antrian_maksimal;
                var terantrikan = result.antrian_terantrikan;
                
                alert('Kuota '+result.namaloket+' kurang <label class="label label-success">'+sisa+' antrian</label>. \n\
                        <br> Kuota maksimal <label class="label label-warning">'+maksimal+' antrian</label>. \n\
                        <br> Antrian '+result.namaloket+' sudah sampai <label class="label label-info">'+terantrikan+' antrian</label>.'
                    );
            }
            
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
   
});