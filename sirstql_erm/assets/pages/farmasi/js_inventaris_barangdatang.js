var listbarangdatang='';
$(function () {
    removelocalstorage('lsdistribusibarang');
    removelocalstorage('modebeli');
    removelocalstorage('fakturbelanjabarang');
    setdate();
    tabelBelanjaBarang();
})

function setdate()
{
    var tgl1 = localStorage.getItem('tanggal1');
    var tgl2 = localStorage.getItem('tanggal2');
    $('#tanggal1').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",  ((tgl1==null) ? 'now' : tgl1) ); //Initialize Date picker
    $('#tanggal2').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",((tgl2==null) ? 'now' : tgl2)); //Initialize Date picker
}

//menampilkan data belanja barang
function tabelBelanjaBarang()
{ 
  listbarangdatang = $('#listbarangdatang').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : false,"stateSave": true,"order": [],"ajax": {"data":{filtertanggal:function(){return $('input[name="filtertanggal"]:checked').val();},cari:function(){return $('input[name="cari"]').val();},tanggal1:function(){return $('input[name="tanggal1"]').val();}, tanggal2:function(){return $('input[name="tanggal2"]').val();} },"url": base_url+'cmasterdata/dt_barangdatang',"type": "POST"},"columnDefs": [{ "targets": [ 0,5,11,12 ],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', qlstatuswarna(aData[7]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#tampilbarangdatang',function(){
    localStorage.setItem('tanggal1',$('#tanggal1').val());
    localStorage.setItem('tanggal2',$('#tanggal2').val());
    reloaddata();
});
$(document).on('keyup','#cari',function(){reloaddata();});

$(document).on('click','#unduh',function(){
    var tanggal1 = $('#tanggal1').val();
    var tanggal2 = $('#tanggal2').val();
    var filtertanggal = $('input[name="filtertanggal"]:checked').val();
    var value = tanggal1+'|'+tanggal2+'|'+filtertanggal;
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' laporanpembelianobat'; 
});

$(document).on('click','#reloadbarangdatang',function(){
  localStorage.removeItem('tanggal1');
  localStorage.removeItem('tanggal2');
  setdate();
  $('#cari').val('').keyup();
  listbarangdatang.state.clear();
  reloaddata();
});
$(document).on('click','#detailfaktur',function(){ cetakbarangbelanja($(this).attr('idbf'),'cetak'); });
$(document).on('click','#radio1',function(){reloaddata();});
$(document).on('click','#radio2',function(){reloaddata();});
function reloaddata(){listbarangdatang.ajax.reload();}

function cetakbarangbelanja(idbf,mode='')
{
    $.ajax({ 
        url:  base_url+'cmasterdata/jsondetailbelanjabarang',type : "POST",dataType : "JSON",data:{idbf:idbf},
        success: function(result){
            var no=0, list='';for(var x in result){ list+='<tr><td class="padding">'+ ++no +'</td><td class="padding">'+ result[x].kode +', '+result[x].namabarang +'</td><td class="padding">'+result[x].kadaluarsa +'</td><td class="padding">'+result[x].batchno +'</td><td class="padding">'+convertToRupiah(parseFloat(result[x].hargabeli))+'</td><td class="padding">'+ convertToRupiah(parseFloat(result[x].hargaasli)) +'</td><td class="padding">'+convertToRupiah(parseFloat(result[x].jumlah))+'</td><td class="padding">'+result[x].namasatuan+'</td></tr>';}
            var cetak='<table width="100%"><thead></thead><tbody><tr><td rowspan="3"><img style="opacity:0.8;filter:alpha(opacity=50);" src="'+base_url+'assets/images/headerkuitansismall.jpg" /></td><td class="right" style="font-size:20px;">BARANG DATANG<br/><span style="font-size:16px;">NF: '+result[0].nofaktur+'</span></td></tr></tfooter></table>';
                cetak+='<table width="100%"><thead>';
                cetak+='<tr><td class="padding">No.Pesan</td><td class="padding">: '+result[0].nopesan+'</td><td class="split">&nbsp;</td><td class="padding">Tagihan</td><td class="padding">: '+convertToRupiah(result[0].tagihan)+'</td> <td class="split2">&nbsp;</td> <td class="padding">Total Tagihan</td><td class="padding">: '+convertToRupiah(result[0].totaltagihan)+'</td></tr>';
                cetak+='<tr><td class="padding">Tgl.Faktur</td><td class="padding">: '+result[0].tanggalfaktur+'</td> <td class="split">&nbsp;</td><td class="padding">Potongan</td><td class="padding">: '+convertToRupiah(result[0].potongan)+'</td> <td class="split">&nbsp;</td><td class="padding">Dibayar</td><td class="padding">: '+convertToRupiah(result[0].dibayar)+'</td> </tr>';
                cetak+='<tr><td class="padding">Tgl.Tempo</td><td class="padding">: '+result[0].tanggaljatuhtempo+'</td> <td class="split">&nbsp;</td><td class="padding">PPN</td><td class="padding">: '+convertToRupiah(result[0].ppn)+'</td> <td class="split">&nbsp;</td><td class="padding">Hutang</td><td class="padding">: '+convertToRupiah(result[0].kekurangan)+'</td></tr></tfooter></table>';
                cetak+='<table cellpadding="0" cellspacing="0" width="100%" border="1" ><thead><tr class="bg bg-yellow"><td class="padding">No</td><td class="padding">Barang</td><td class="padding">Expired</td><td class="padding">Batch</td><td class="padding">HargaBeli@</td><td class="padding">HargaAsli@</td><td class="padding">Jumlah</td><td class="padding">Satuan</td></tr></thead><tbody>'+list+'<tr></tfooter></table><br/>';
                // cetak+='<table class="table" style="width:100%;"><tr><td width="76%">Mengetahui</td><td>Penanggung Jawab</td></tr></table>';
                // cetak+='<table class="table" style="width:100%;margin-top:50px;"><tr><td width="76%">( .... )</td><td>(.......)</td></tr></table>';
                if(mode==''){
                    fungsi_cetaktopdf(cetak,'<style>@page { size:A4 lanscape; margin:0.2cm 0.5cm;}.padding{padding:4px;}th{text-align:left;}.split{width:18%;} .split2{width:2%;} .left{text-align:left;}.right{text-align:right;}.block{display:block;}</style>');
                }else{
                    $.dialog({ title: '',content: cetak,columnClass:'lg'});
                }
        },error: function(result){fungsiPesanGagal();return false;}
      });
}

//mahmud, clear
$(document).on('click','#ubahstatusfaktur',function(){
    var id = $(this).attr('alt');
    var stat = $(this).attr('stat');
    $.ajax({ 
        url:  base_url+'cmasterdata/updatefaktur',type : "POST",dataType : "JSON",data:{id:id, stat:stat},
        success: function(r){notif(r.status,r.message);window.location.reload(true);},
        error: function(result){fungsiPesanGagal();return false;}
      });
});

function detailbelanja(idfakturbarang)
{
    localStorage.setItem('idfakturbarangdetailbelanja',idfakturbarang); // buat storage idpesan
    localStorage.setItem('modebeli', 'pembetulan');
    window.location.href=base_url + 'cmasterdata/distribusibelanjabarang';
}

//bayar inkaso
$(document).on("click",'#bayarinkaso',function(){
    var idf = $(this).attr("alt");//infaktur
    var nom = $(this).attr("alt2");//nominal;
    var nomk= $(this).attr("alt3");//kekurangan;
    var nomp= $(this).attr("alt4");//Potongan;
    var modalSize = 'medium';
    var modalTitle = 'INKASO';
    var modalContent = '<form action="" id="formBayar">' +
                            '<div class="form-group">' +
                                '<label>Nominal Inkaso</label><br/>' +
                                '<input type="text" name="ttagihan" placeholder="Tagihan" class="form-control" value="'+nom+'" disabled/>' +
                                '<input type="hidden" name="altt" value="'+idf+'" />' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Potongan</label>' +
                                '<input type="text" name="tpotongan" value="'+convertToRupiah(nomp)+'" placeholder="Potongan" class="form-control" disabled/>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Hutang</label><br/>' +
                                '<input type="text"  name="thutang" value="'+convertToRupiah(nomk)+'" placeholder="Hutang" class="form-control" disabled/>' +
                            '</div>' +
                            '<div class="form-group">' +
                            '<label>Bayar</label> '+
                                '<input type="text" name="tbayar" placeholder="Bayar" id="tbayar" class="form-control" onkeyup="hitungkekuranganinkaso(event)" />' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Kekurangan</label><br/>' +
                                '<input type="text"  name="tkekurangan" value="'+convertToRupiah(nomk)+'" placeholder="Kekurangan" class="form-control" disabled/>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#tbayar').val()==''){
                        $('#tbayar').focus();
                        alert_empty('inputan bayar');
                        return false;
                    }else{
                        simpanpembayaraninkaso();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
//bayar lunas inkaso
$(document).on("click",'#bayarlunasinkaso',function(){
    var nofaktur=$(this).attr('alt3');
    var totaltagihan=$(this).attr('alt2');
    var form = '<form action="" id="formInput" autocomplete="off">' +
                '<div class="form-group">' +
                    '<label>No.Faktur</label> : ' + nofaktur +
                    '<br><label>N.Inkaso</label> : Rp ' + convertToRupiah(totaltagihan) +
                '</div>'+
                '<div class="form-group">' +
                    '<label>Tgl.Pelunasan</label> : ' +
                    '<input type="text" name="tglpelunasan" class="datepicker" size="7" value=""/>' +
                '</div>'+
                        '</form>';
    var idf = $(this).attr("alt");//infaktur
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Pelunasan Inkaso',
        content: form,
        buttons: {
            formSubmit: {
                text: 'Bayar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({ 
                        url:  base_url+'cmasterdata/setbayarinkaso',type:"POST",dataType:"JSON",data:{id:idf, sb:'lunas', tgl:$('input[name="tglpelunasan"]').val()},
                        success: function(r){notif(r.status,r.message);listbarangdatang.ajax.reload(null,false);},
                        error: function(result){fungsiPesanGagal();return false;}
                    });
                }
            },
            //menu reset
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }         
        },
        onContentReady: function () {
            $('input[name="tglpelunasan"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", 'now' ); //Initialize Date picker
        }
    });
});

//Batal lunas inkaso
$(document).on("click",'#batallunasinkaso',function(){
    var idf = $(this).attr("alt");//infaktur
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Konfirmasi Batal Pelunasan Inkaso.',
        buttons: {
            formSubmit: {
                text: 'Bayar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({ 
                        url:  base_url+'cmasterdata/setbayarinkaso',type:"POST",dataType:"JSON",data:{id:idf, sb:'belumdibayar'},
                        success: function(r){notif(r.status,r.message);listbarangdatang.ajax.reload(null,false);},
                        error: function(result){fungsiPesanGagal();return false;}
                    });
                }
            },
            //menu reset
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }         
        }
    });
});
//mahmud, clear
function hitungkekuranganinkaso(event)
{
    if (event.keyCode == 13){simpanpembayaraninkaso();}
    else{converToRupiahInput('tbayar');$('input[name="tkekurangan"]').val(convertToRupiah(unconvertToRupiah($('input[name="thutang"]').val()) - unconvertToRupiah($('input[name="tbayar"]').val())));}
}
//mahmud, clear
function simpanpembayaraninkaso()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/setbayarinkaso',type:"POST",dataType:"JSON",data:{nom:unconvertToRupiah($('#tbayar').val()),id:$('input[name="altt"]').val()},
        success: function(r){notif(r.status,r.message);window.location.reload(true);},
        error: function(result){fungsiPesanGagal();return false;}
    });
}
