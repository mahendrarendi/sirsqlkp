/**
 * Cetak Hasil Laboratorium RANAP
 * @param {type} idpemeriksaan
 * @param {type} idpendaftaran
 * @param {type} idunit
 * @param {type} paketlab
 * @returns {undefined}
 */
function print_laboratorium(idrencanamedispemeriksaan,mode,idpaketpemeriksaan)
{
    var header='', style='', i=0, printData='';
    $.ajax({
        type:"POST",
        url:base_url+"creport/cetakpaketlaboratorium_rawatinap",
        data:{
            mode:mode,
            x:idrencanamedispemeriksaan,
            y:idpaketpemeriksaan
        },
        dataType:"JSON",
        success:function(value){
            if(value.hasillab.length > 0)
            {
             
            var identitas = value.identitas; var gruppaket='';
            var waktu = value.waktu;
            var pesan = value.pesan;
//            var jeniscetak = value.jeniscetak;
            var formatcetak = value.formatcetak;
//            var ttdokter = value.ttdokter;
            var value = value.hasillab;
//            header += ((ttdokter==null || ttdokter=='' || ttdokter=='0') ? '' : '<img style="width:20cm;" src="'+base_url+'assets/images/headerlaboratorium.jpg" />' );
            header += '<table style="page-break-before: always;margin-top:4.5cm";width:100%; text-align:left; font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            header += '<tr><td>No.Periksa/No.RM </td><td>:</td> <td >'+identitas.idpemeriksaan+'/'+identitas.norm+'</td><td>Dokter</td> <td>:</td><td style="height:auto;width:30%;">'+identitas.dokter+'</td><tr>';
            header += '<tr><td>Nama</td>             <td>:</td> <td style="height:auto;width:40%;">'+identitas.namalengkap+'</td><td>Asal</td>   <td>:</td><td style="height:auto;width:30%;">Laboratorium</td><tr>';
            header += '<tr><td>J.Kelamin</td>        <td>:</td> <td>'+identitas.jeniskelamin+'</td><td>Periksa</td><td>:</td><td>'+((waktu==null) ? '' : waktu.waktumulai )+'</td><tr>';
            header += '<tr><td>Usia</td>             <td>:</td> <td>'+identitas.usia+'</td><td>Selesai</td> <td>:</td><td>'+((waktu==null) ? '' : waktu.waktuselesai )+'</td><tr>';
            header += '<tr><td>Alamat</td>           <td>:</td><td style="height:auto;font-size:12px;" colspan="4">'+identitas.alamat+'</td><tr>';
            header += '</table>';

            header += '<table cellpadding="5" cellspacing="0" style=" width:18cm;font-size:14px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"">';
            // jenis cetak kosong default, selain itu lab hiv
            header += formatcetak.judul;//((jeniscetak==null || jeniscetak=='') ? '' : '<tr><td colspan="5">IMUNOSEROLOGI</td></tr>');
            header += formatcetak.header;//((jeniscetak==null || jeniscetak=='') ? '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">PEMERIKSAAN</td><td style="border:1px solid #000;border-left:0px;">HASIL</td><td style="border:1px solid #000;border-left:0px;min-width:80px;">N DEFAULT</td><td style="border:1px solid #000;border-left:0px;min-width:90px;">N RUJUKAN</td><td style="border:1px solid #000;border-left:0px;min-width:60px;" >SATUAN</td></tr>' : '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">Pemeriksaan</td><td style="border:1px solid #000;border-left:0px;">Hasil</td><td style="border:1px solid #000;border-left:0px;">Nilai Rujukan</td><td style="border:1px solid #000;border-left:0px;">Metode</td><td style="border:1px solid #000;border-left:0px;">Hasil</td></tr>');
            printData += header;
            var printPaket='';
            var  i=0, nilai='', jenisicd='', paketpemeriksaanparent='', paketpemeriksaan='';
            for(i in value)
            {   
               
                    if(mode=='cetakpaket'){
                        //CETAK HASIL LABORATORIUM SESUAI PAKET YANG DIPILIH
                        printData='';
                        
                        //tampilkan keterangan hasil laborat jika satu grup dan satu paket
                        if(gruppaket!==value[i].gruppaketperiksa && idgrup!= undefined)
                        {
                             printPaket += '<tr ><td colspan="5" style="padding-left:7px;">tes keterangan hasil</td></tr>'; 
//                             keteranganhasil = '';
                        }
                        
                        if( idpaketpemeriksaan==value[i].idpaketpemeriksaan || idpaketpemeriksaan == value[i].idpaketpemeriksaanparent)
                        { 
                            if(gruppaket!==value[i].gruppaketperiksa){printPaket+=header;}
                            paketpemeriksaan = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum && gruppaket==value[i].gruppaketperiksa)?'': '<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>');
                            printPaket       = printPaket +paketpemeriksaan;// + ((jeniscetak==null || jeniscetak=='') ?  paketpemeriksaan : '');//jika jenis cetak null  nama paket dicetak
                            if(icd != value[i].icd && value[i].icd!=null)//jika icd sama lewati
                            {
                            printPaket = printPaket  +'<tr>'+
                                      '<td>'+value[i].namaicd+'</td>'+
                                      '<td>'+ ((value[i].istext=='text') ? value[i].nilaitext : value[i].nilai) +'</td>'+
                                      '<td>'+ ((formatcetak.kode=='DFLT') ? ((value[i].nilaidefault==null || value[i].nilaidefault=='') ? '' : value[i].nilaidefault  ) : value[i].nilairujukan )+'</td>'+
                                      '<td>'+ ((formatcetak.kode=='DFLT') ? value[i].nilairujukan : value[i].metode) +'</td>'+
                                      '<td>'+ ((formatcetak.kode=='DFLT') ? value[i].satuan : value[i].kesimpulanhasil)+'</td>'+
                                        '</tr>';
                            }
                                    var icd = value[i].icd;
                                    // var jenisicdsebelum=value[i].jenisicd;
                                    var namapaketpemeriksaansebelum = value[i].namapaketpemeriksaan;
                                    var gruppaket=value[i].gruppaketperiksa;
                        }
//                        if(value[i].keteranganhasil !=null && value[i].keteranganhasil != keteranganhasil)
//                        {
//                            var keteranganhasil = value[i].keteranganhasil;
//                        }
                        var idgrup = value[i].idgrup;
                    }
                    else
                    {
                        if(idpaketpemeriksaan==='')
                        {
                            //CETAK HASIL LABORATORIUM - KETIKA JENIS PAKET YANG DIPILIH BUKAN PAKET
                            if(value[i].idpaketpemeriksaan < 1 && value[i].idpaketpemeriksaanparent < 1)
                            {
                                printData = printData  +'<tr>'+
                                            '<td>'+value[i].namaicd+'</td>'+
                                            '<td>'+ value[i].nilai +'</td>'+
                                            '<td>'+((value[i].nilaidefault==null) ? '' : value[i].nilaidefault  )+'</td>'+
                                            '<td>'+value[i].nilairujukan+'</td>'+
                                            '<td>'+value[i].satuan+'</td>'+
                                            '</tr>';
                            }
                        }
                        else
                        {
                           //CETAK HASIL LABORAT TANPA MEMILIH JENIS PAKET
                           // paketpemeriksaanparent = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum)?'': '<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>');
                           paketpemeriksaan = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum && gruppaket==value[i].gruppaketperiksa)?'': ((value[i].namapaketpemeriksaan==null)?'':'<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>'));
                            printData = printData  + paketpemeriksaanparent + paketpemeriksaan;
                            if(icd != value[i].icd && value[i].icd!=null)//jika icd sama lewati
                            {
                                printData = printData  +'<tr>'+
                                '<td>'+value[i].namaicd+'</td>'+
                                '<td>'+ value[i].nilai +'</td>'+
                                '<td>'+((value[i].nilaidefault==null || value[i].nilaidefault=='') ? '' : value[i].nilaidefault  )+'</td>'+
                                '<td>'+value[i].nilairujukan+'</td>'+
                                '<td>'+value[i].satuan+'</td>'+
                                '</tr>';
                            }
                            var icd = value[i].icd;
                            var namapaketpemeriksaansebelum = value[i].namapaketpemeriksaan;
                            var gruppaket=value[i].gruppaketperiksa;
                        }
                    }
            }
            printData += printPaket;
            
            //tampilkan keterangan hasil laborat terakhir
//            printData += '<tr><td colspan="5" style="padding-left:7px;">'+((keteranganhasil==undefined)?'':keteranganhasil)+'</td></tr> </table> ';
            
//            printData += ((ttdokter==null || ttdokter=='' || ttdokter=='0') ? '' : '<table cellpadding="0" cellspacing="0" style="text-align:center;margin-left:-2cm; width:21cm;'+((nilaiantigen=='positif') ? 'margin-top:8cm;' : 'margin-top:7cm;' )+'"><tr><td>Penanggung Jawab</td><td style="margin-left:-1cm;">Validasi Hasil</td><td>Petugas Pemeriksa</td></tr> <tr><td><img style="width:4cm;" src="'+base_url+'assets/images/dokter/laborat.jpg" /></td><td></td><td></td></tr> <tr><td>dr.Riat El Khair, M.Sc, Sp.PK</td><td>..........................</td><td>..........................</td></tr></table>' );
            var style = '<style>@page {size:21cm 29.7cm;padding-bottom:0px;margin-bottom:0px;}</style>';
            fungsi_cetaktopdf(printData,style);
            
            }
            else
            {
                $.alert('Belum Ada Tindakan Laboratorium.');
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
// ELEKTRO MEDIK
function cetak_hasilelektromedik(idpendaftaran='',idunit='')//download hasil laboratorium
{
    elektromedik_cetakexpertise(idpendaftaran,idunit);    
}
function elektromedik_cetakexpertise(idpendaftaran,idunit)
{
    var printData = '<div style="float: none;"><img style="opacity:0.6;filter:alpha(opacity=60);width:9cm" src="'+base_url+'assets/images/headerkuitansi.jpg" />\n', style='', i=0;
    var idpendaftaran = ((idpendaftaran=='')?$('input[name="idpendaftaran"]').val():idpendaftaran) , idunit = ((idunit=='')? $('input[name="idunit"]').val() : idunit); 
    $.ajax({type:"POST",url:base_url+"creport/elektromedik_hasilpemeriksaan",data:{x:idpendaftaran,y:idunit},dataType:"JSON",
        success:function(value){
            stopLoading();
            var keteranganradiologi=value.identitas.keteranganradiologi+((value.identitas.saranradiologi=='')?'':'<b>Saran:</b>')+value.identitas.saranradiologi;
            var diagnosa='';
            var elektromedik = '';
            for (var x in value.diagnosa) { diagnosa += '<span style="display:block;">'+value.diagnosa[x].icd+' '+value.diagnosa[x].namaicd;+'</span>';}
            for (var y in value.elektromedik) { elektromedik += '<span style="display:block;">'+value.elektromedik[y].icd+' '+value.elektromedik[y].namaicd;+'</span>';}
            var identitas = value.identitas;
            var garis= '<div style="border-top:1px solid #3a3a3a;"></div>';
            var borderlr= 'border-left:1px solid #3a3a3a;border-right:1px solid #3a3a3a;';
            var borderbt= 'border-bottom:1px solid #3a3a3a;border-top:1px solid #3a3a3a;';
            var style1='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            var style2='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            // printData +=garis;
            printData += '<table '+style1+'>';
            printData += '<tr><td>N.Periksa/N.RM </td><td>:</td> <td >'+identitas.idpemeriksaan+'/'+identitas.norm+'</td><td>D. Pengirim</td> <td>:</td><td style="height:auto;width:35%;">'+identitas.dokter+'</td><tr>';
            printData += '<tr><td>Nama</td>             <td>:</td> <td style="height:auto;width:35%;">'+ identitas.identitas +' '+identitas.namalengkap+'</td><td>Asal</td>   <td>:</td><td style="height:auto;width:35%;">'+identitas.namaunit+'</td><tr>';
            printData += '<tr><td>J. Kelamin</td>        <td>:</td> <td>'+identitas.jeniskelamin+'</td><td>T. Periksa</td><td>:</td><td>'+identitas.waktu+'</td><tr>';
            printData += '<tr><td>Usia</td>             <td>:</td> <td>'+identitas.usia+'</td><td>Alamat</td> <td>:</td><td style="height:auto;width:35%;">'+identitas.alamat+'</td><tr>';
            printData += '</table>';
            printData +=garis;
            printData += '<table '+style1+'><tr ><th>Diagnosis Klinis</th><th>Pemeriksaan Radiologis</th></tr>';
            printData += '<tr><td style="height:auto;width:50%;">'+ diagnosa  +'</td><td style="height:auto;width:50%;">'+elektromedik+'</td></tr></table>';
            printData +=garis;
            printData += '<table '+style2+'><tr><td style="padding:5px;text-align:justify;">'+keteranganradiologi+'</td></tr></table>';
            printData += '<table style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br><br><br><br><br></td></tr><tr><td>dr. Amri Wicaksono Pribadi, Sp. Rad<br>SIP : 446/6744/501/VIII-23</td></tr></table>';
            var style = '<style>@page {size:21cm 29.7cm;}</style>';
            fungsi_cetaktopdf(printData,style);
        },
        error:function(result){
            stopLoading(); fungsiPesanGagal(); return false;
        }
    });
}