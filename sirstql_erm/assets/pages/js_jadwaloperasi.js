'use strict';
var dttable;
$(function(){
    localStorage.removeItem('idoperasi');
    localStorage.removeItem('idpendaftaranop');
    localStorage.removeItem('idjadwaloperasi');
    settanggal();   
    
    dttable = $('#listpo').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
        "ajax": {
            "data":{
                tanggal1:function(){return $('#tanggalawal').val()},                
                tanggal2:function(){return $('#tanggalakhir').val()}
            },
            "url": base_url+'cpelayanan/listpo',
            "type": "POST"
        },
        "columnDefs": [
            { "targets": [ -1 ],"orderable": false,},
        ],
   "fnCreatedRow": function (nRow, aData, iDataIndex) {
       $(nRow).attr('style', statusoperasi(aData[8]));
    }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {
       $('[data-toggle="tooltip"]').tooltip();
   },// -- Function that is called every time DataTables performs a draw.
   });
   
  
});

function statusoperasi(status)
{
    var color = {Terlaksana:'background-color:#e0f0d8;',Batal:'background-color:#f0D8D8;',Rencana:''};
    return  color[""+status+""];
}

function settanggal()
{
    var tgl1 = localStorage.getItem('tanggal1');
    var tgl2 = localStorage.getItem('tanggal2');
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((tgl1==undefined) ? 'now' : tgl1 ) );
    $('#tanggalakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((tgl2==undefined) ? 'now' : tgl2 ) );    
}

$(document).on('click','#tampil',function(){ 
    dttable.ajax.reload(); 
   localStorage.setItem('tanggal1',$('#tanggalawal').val());
   localStorage.setItem('tanggal2',$('#tanggalakhir').val());
});

$(document).on('click','#refresh',function(){
    $('input[type="search"]').val('').keyup();
    dttable.state.clear();
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    $('#tanggalakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    dttable.ajax.reload(); 
});

$(document).on('click','#terlaksana',function(){
    var ur = $(this).attr('ur');var data = $(this).attr('idjop');
    var pesan = 'Apakah Anda Yakin Akan Menyelesaikan Jadwal Operasi.?';
    confirm(ur,data,'terlaksana',pesan);
});

//verif tarif jkn
$(document).on('click','#verif_tarifjkn',function(){
    var idoperasi = $(this).attr('idoperasi');
    var idpendaftaran = $(this).attr('idpendaftaran');
    var idjadwaloperasi = $(this).attr('idjadwaloperasi');
    var profil = $(this).attr('profil');
    
    var modalSize = 'm';
    var modalTitle = 'Verif Tarif Operasi Pasien JKN';
    var modalContent = '<form action="" id="Formverifpasienjkn">' +
                            '<input type="hidden" name="idoperasi" value="'+idoperasi+'" />' +
                            '<input type="hidden" name="idpendaftaran" value="'+idpendaftaran+'" />' +
                            '<input type="hidden" name="idjadwaloperasi" value="'+idjadwaloperasi+'" />' +
                            '<input type="hidden" name="idjenistarif"/>'+ 
                            '<input type="hidden" name="keterangan"/>'+ 
                            '<div class="form-group bg bg-info" style="padding:6px; border-radius:2px;">'+profil+'</div>'+
                            '<div class="form-group">' +
                                '<label>Jenis Tarif Operasi</label>' +
                                '<select class="form-control" name="jenistarif" id="jenistarif">\n\
                                    <option value="0">Pilih</option>\n\
                                    <option value="1">Tarif Operasi SC</option>\n\
                                    <option value="2">Tarif Operasi Non SC</option>\n\
                                    <option value="3">Tarif Operasi Ortopedi</option>\n\
                                </select>'+
                            '</div>'+
                            '<div class="form-group" data-form="1">' +
                                '<label>Kelas</label>' +
                                '<select class="form-control" name="idkelas" id="idkelas"></select>'+
                            '</div>'+
                            '<div class="form-group" data-form="2">' +
                                '<label>Plafon</label>' +
                                '<select class="form-control" name="idplafon" id="idplafon"></select>'+
                            '</div>'+
                            '<div class="form-group" data-form="3">' +
                                '<label>Persentase</label>' +
                                '<div class="input-group" id="formtarifplafon">\n\
                                    <input type="text" name="persentase" id="persentase" class="form-control" readonly>\n\
                                    <div class="input-group-addon">\n\
                                      <i class="fa fa-percent"></i>\n\
                                    </div>\n\
                                </div>'+
                            '</div>'+
                            '<div class="form-group" data-form="3">' +
                                '<label>Input Plafon</label>' +
                                '<input type="text" name="plafon" id="plafon" class="form-control" placeholder="ketik besarnya plafon"/>'+
                            '</div>'+
                            '<div class="form-group" data-form="1,2,3">' +
                                '<label>Tarif</label>' +
                                '<input type="text" name="nominal_tarif" class="form-control" readonly />'+
                            '</div>'+
                        '</form>';
            $.confirm({ //aksi ketika di klik menu
                title: modalTitle,
                content: modalContent,
                closeIcon: true,
                type:'orange',
                columnClass: 'medium',
                buttons: {
                    //menu save atau simpan
                    formSubmit: {
                        text: 'simpan',
                        btnClass: 'btn-blue',
                        action: function () {
                            //save_verifikasi
                            if($('#jenistarif').val() == '')
                            {
                                alert('Jenis Tarif Operasi Harap Dipilih.');
                                return false;
                            }
                            else if( $('#jenistarif').val() == 1 && $('#idkelas').val() == '' )
                            {
                                alert('Kelas Harap Dipilih.');
                                return false;
                            }
                            else if( $('#jenistarif').val() == 2 && $('#idplafon').val() == '' )
                            {
                                alert('Plafon Harap Dipilih.');
                                return false;
                            }
                            else if( $('#jenistarif').val() == 3 && $('#plafon').val() == '' )
                            {
                                alert('Plafon Harap Diisi.');
                                return false;
                            }
                            else
                            {
                                $.ajax({
                                    url: base_url+'cpelayanan/save_verifikasi',
                                    type : "post",      
                                    dataType : "json",
                                    data:$('#Formverifpasienjkn').serialize(),
                                    success: function(result){
                                        stopLoading();
                                        notif(result.status, result.message);
                                        dttable.ajax.reload(false);
                                    },
                                    error: function(result){   
                                        stopLoading();
                                        notif(result.status, result.message);
                                        return false;
                                    }
                                }); 
                            }
                        }
                    },
                    //menu back
                    formReset:{
                        text: 'batal',
                        btnClass: 'btn-danger'
                    }
                },
                onContentReady: function () {
                // bind to events
                $('[data-form]').hide();
                onContentReady_FormVerif();
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
                }
            });
});

//hitung taris SC
$(document).on('change','#idkelas',function(){
    var nominal = $('option:selected', this).attr('tarif');
    var idjenistarif = this.value;
    var keterangan = $('option:selected', this).attr('keterangan');
    
    $('input[name="nominal_tarif"]').val(convertToRupiah(nominal));
    $('input[name="idjenistarif"]').val(idjenistarif);
    $('input[name="keterangan"]').val(keterangan);
});

//hitung tarif non SC
$(document).on('change','#idplafon',function(){
    var nominal = $('option:selected', this).attr('tarif');
    var idjenistarif = this.value;
    var keterangan = $('option:selected', this).attr('keterangan');
    
    $('input[name="nominal_tarif"]').val(convertToRupiah(nominal));
    $('input[name="idjenistarif"]').val(idjenistarif);
    $('input[name="keterangan"]').val(keterangan);
});

//set tarif ortopedi
$(document).on('change','#jenistarif',function(){
    $('[data-form]').hide();
    $('[data-form*='+this.value+']').show(); 
    $('input[name="nominal_tarif"]').val('');
    var jenis = this.value;
    if(jenis == 3)
    {
        var idjenistarif = $('#persentase').attr('idjenistarif');
        var keterangan = $('#persentase').attr('keterangan');
        $('input[name="idjenistarif"]').val(idjenistarif);
        $('input[name="keterangan"]').val(keterangan);
    }
});

//hitung tarif ortopedi
$(document).on('keyup','#plafon',function(){
    var plafon = unconvertToRupiah($('#plafon').val());
    $('#plafon').val(convertToRupiah(plafon));
    
    var persentase = $('#persentase').val();
    var nominal = parseFloat(plafon) * (parseInt(persentase)/parseInt(100));
    $('input[name="nominal_tarif"]').val(convertToRupiah(nominal,'.'));
});

//saat form verifikasi operasi dimuat
function onContentReady_FormVerif(){
    $.ajax({
        url: base_url+'cpelayanan/onContentReady_FormVerif',
        type : "post",      
        dataType : "json",
        success: function(result){
            stopLoading();
            //set tarif SC
            var tarifsc = '<option value="">Pilih</option>';
            for(var x in result.tarifsc)
            {
                tarifsc += '<option value="'+result.tarifsc[x].idjenistarif+'" keterangan="'+result.tarifsc[x].keterangan+'" tarif="'+result.tarifsc[x].tarif+'">'+result.tarifsc[x].namakelas+'</option>';
            }
            $('#idkelas').html(tarifsc);
            
            //set tarif non sc
            var tarifnonsc = '<option value="">Pilih</option>';
            for(var x in result.tarifnonsc)
            {
                tarifnonsc += '<option value="'+result.tarifnonsc[x].idjenistarif+'" keterangan="'+result.tarifnonsc[x].keterangan+'" tarif="'+result.tarifnonsc[x].tarif+'">'+result.tarifnonsc[x].plafon+'</option>';
            }
            $('#idplafon').html(tarifnonsc);
            
            //set tarif ortopedi
            $('#formtarifplafon').html('<input type="text" name="persentase" id="persentase" class="form-control" value="'+result.tarifortopedi.persentase+'" keterangan="'+result.tarifortopedi.keterangan+'" idjenistarif="'+result.tarifortopedi.idjenistarif+'" readonly><div class="input-group-addon"><i class="fa fa-percent"></i></div>');
        },
        error: function(result){   
            stopLoading();
            notif(result.status, result.message);
            return false;
        }
    }); 
}


//input tindakan operasi
$(document).on('click','#detail',function(){
    var idjadwaloperasi = $(this).attr('idjop');
    var idoperasi       = $(this).attr('idop');
    var idpendaftaran   = $(this).attr('idp');
    var profile  = $(this).attr('profilepasien');
    var modeoperasi = $(this).attr('modeop');
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Mulai Operasi',
        content: ((modeoperasi=='insert') ? 'Apakah Pasien Akan Dimulai Operasi Sekarang.?' : 'Apakah Tindakan & Diagnosa Operasi Pasien Akan Diubah.?' ) +' <hr><div class="bg-info" style="padding:6px; border-radius:2px;">'+profile+'</div>',
        buttons: {
            formSubmit: {
                text: ((modeoperasi=='insert') ? 'mulai' : 'ubah' ),
                btnClass: 'btn-blue',
                action: function () {
                    startLoading();
                    if(modeoperasi=='insert')
                    {
                        $.ajax({ 
                            url: base_url+'cpelayanan/insertoperasi',
                            type : "post",      
                            dataType : "json",
                            data : {idjop:idjadwaloperasi,idp:idpendaftaran},
                            success: function(result) {   
                                stopLoading();
                                notif(result.status, result.message);
                                if(result.status=='success'){
                                    
                                    localStorage.setItem('idpendaftaranop',idpendaftaran);
                                    localStorage.setItem('idjadwaloperasi',idjadwaloperasi);
                                    localStorage.setItem('idoperasi',result.idoperasi);
                                    window.location.href=base_url + 'cpelayanan/pemeriksaanoperasi';
                                }else{
                                    return false;
                                }   
                            },
                            error: function(result){   
                                stopLoading();
                                notif(result.status, result.message);
                                return false;
                            }
                        }); 
                    }
                    else
                    {
                        startLoading();
                        
                        localStorage.setItem('idpendaftaranop',idpendaftaran);
                        localStorage.setItem('idjadwaloperasi',idjadwaloperasi);
                        localStorage.setItem('idoperasi',idoperasi);
                        window.location.href=base_url + 'cpelayanan/pemeriksaanoperasi';
                    }
                    
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    });
});
$(document).on('click','#batal',function(){
    var ur = $(this).attr('ur');
    var data = $(this).attr('idjop');
    var pesan = 'Anda Yakin Akan Membatalkan Jadwal Operasi.?';
    confirm(ur,data,'batal',pesan);
});

$(document).on('click','#batalselesai',function(){
    var ur = $(this).attr('ur');
    var data = $(this).attr('idjop');    
    var pesan = 'Anda Yakin Akan Membatalkan Selesai Jadwal Operasi.?';
    confirm(ur,data,'batalselesai',pesan);
});



$(document).on('click','#hapus',function(){
    var ur = $(this).attr('ur');
    var data = $(this).attr('idjop');
    var pesan = 'Anda Yakin Akan Menghapus Jadwal Operasi.?';
    confirm(ur,data,'hapus',pesan);
});
function confirm(ur,data,mode,pesan)
{
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: pesan ,
        buttons: {
            
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    ajax('cpelayanan/'+ur,{id:data});
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }      
        }
    });
}
function ajax(url,datapost)
{
    $.ajax({ 
        url: base_url+url,
        type : "post",      
        dataType : "json",
        data : datapost,
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                window.location.reload();
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                    
            notif(result.status, result.message);
            return false;
        }

    }); 
}
//mahmud, clear
function cetak_laporan_penggunaanobatbhp_operasi(idjadwaloperasi)
{
    var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
    var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
    var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
    var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
    var bordernotleft2='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var left = "border-left:0.7px solid; padding:0.7px 5px;";
    var right= "border-right:0.7px solid; padding:0.7px 5px;";
    var leftright  = "border-left:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;";
    var leftbottom = "border-left:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var rightbottom= "border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var leftbottomright= "border-left:0.7px solid;border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";


    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_laporanoperasi',
        data:{
            idjadwaloperasi:idjadwaloperasi,
            tampilbarang:'ok'
        },
        dataType: "JSON",
        success: function(result){
        stopLoading();
        if(result.operasi == 'null')
        {
            alert('Data Operasi Tidak Ada.');
            return;
        }
//        <tr><td colspan="2" style="text-align:right;">RM ...</td></tr>\n\
        var jenisanestesi  =  result.jenisanestesi.jenisanestesi;
        var jenisoperasi  =  result.jenisoperasi.jenisoperasi;
        var barang  = result.barang;
        var result = result.operasi;
            var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr><td colspan="3" style="text-align:right;">RM 06.16</td></tr>\n\
                <tr>\n\
                    <td '+bordertd+' width="100"><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/logocetak_operasi.svg" />\n</td>\n\
                    <td '+bordernotleft+' width="300"> \n\
                        <table '+styleFont+'>\n\
                        <tr><td>No.RM</td><td>: '+result.norm+'</td></tr>\n\
                        <tr><td>NIK</td><td>: '+result.nik+'</td></tr>\n\
                        <tr><td>Nama</td><td>: '+result.namalengkap+'</td></tr>\n\
                        <tr><td>Tanggal Lahir</td><td>: '+result.tanggallahir+'</td></tr>\n\
                        </table>\n\
                    </td>\n\
                    <td '+bordernotleft+' width="100px;">Resiko Tinggi : '+result.resikotinggi+' </td>\n\
                </tr>\n\
                <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>LEMBAR BHP & OBAT OPERASI</b></td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">';
                var sediaan = '';
                var td = '';
                var no =0 ;
                for(var x in barang)
                {
                    if(barang[x].namasediaan != sediaan)
                    {
                        cetak += '<tr><td colspan="4" style="'+bordernottop+'"><b>'+ barang[x].namasediaan +'</b></th></tr>';
                        no=0;
                    }
                    td += '<td style="'+bordernottop+'">'+ ++no +' </td>';
                    td += '<td style="'+rightbottom+'">'+barang[x].namabarang+' </td>';
                    td += '<td style="'+rightbottom+'">'+barang[x].jumlahpemakaian+'</td>';
                    td += '<td style="'+rightbottom+'">'+barang[x].namasatuan+'</td>';
                    
                    cetak += '<tr>'+td+'</tr>';
                    td = '';
                    
                    sediaan = barang[x].namasediaan;
                    
                }
                cetak += '</table>\n\
                    </div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.3cm 0.3cm 0.3cm 2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//mahmud clear
function cetak_laporan_operasi(idjadwaloperasi)
{
    var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
    var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
    var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
    var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
    var bordernotleft2='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var left = "border-left:0.7px solid; padding:0.7px 5px;";
    var right= "border-right:0.7px solid; padding:0.7px 5px;";
    var leftright  = "border-left:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;";
    var leftbottom = "border-left:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var rightbottom= "border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var leftbottomright= "border-left:0.7px solid;border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";


    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_laporanoperasi',
        data:{idjadwaloperasi:idjadwaloperasi},
        dataType: "JSON",
        success: function(result){
        stopLoading();
        if(result.operasi == 'null')
        {
            alert('Data Operasi Tidak Ada.');
            return;
        }
        var jenisanestesi  =  result.jenisanestesi.jenisanestesi;
        var jenisoperasi  =  result.jenisoperasi.jenisoperasi;
        var result = result.operasi;
            var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr><td colspan="3" style="text-align:right;">RM 06.12</td></tr>\n\
                <tr>\n\
                    <td '+bordertd+' width="100"><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/logocetak_operasi.svg" />\n</td>\n\
                    <td '+bordernotleft+' width="300"> \n\
                        <table '+styleFont+'>\n\
                        <tr><td>No.RM</td><td>: '+result.norm+'</td></tr>\n\
                        <tr><td>NIK</td><td>: '+result.nik+'</td></tr>\n\
                        <tr><td>Nama</td><td>: '+result.namalengkap+'</td></tr>\n\
                        <tr><td>Tanggal Lahir</td><td>: '+result.tanggallahir+'</td></tr>\n\
                        </table>\n\
                    </td>\n\
                    <td '+bordernotleft+' width="100px;">Resiko Tinggi : '+result.resikotinggi+' </td>\n\
                </tr>\n\
                <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>LAPORAN OPERASI</b></td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Nama Dokter Operator <br>'+result.dokteroperator+'</td>\n\
                    <td style="'+bordernotleft2+'">Nama Asisten Bedah <br>'+result.asistenbedah+'</td>\n\
                    <td style="'+borderbottomright+'">Nama Perawat Instrument <br>'+result.instrumen+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Diagnosis Pra Operasi <br> '+result.diagnosapraoperasi+'</td>\n\
                    <td style="'+bordernotleft2+'">Diagnosis Post Operasi <br> '+result.diagnosapascaoperasi+'</td>\n\
                    <td style="'+borderbottomright+'">Tindakan Operasi <br>'+result.tindakanoperasi+'</td>\n\
                </tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr><td style="'+leftbottom+'" width="20%">Jenis Operasi</td><td style="'+rightbottom+'">: '+jenisoperasi+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Macam Operasi</td><td style="'+rightbottom+'">: '+result.macamoperasi+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Jenis Anestesi</td><td style="'+rightbottom+'">: '+jenisanestesi+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Jumlah Perdarahan</td><td style="'+rightbottom+'">: '+result.jumlahperdarahan+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Patologi Anatomi</td><td style="'+rightbottom+'">: '+result.patologianatomi+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Asal Jaringan</td><td style="'+rightbottom+'">: '+ifnull(result.asaljaringan,'')+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Sitologi</td><td style="'+rightbottom+'">: '+result.sitologi+' </td></tr>\n\
                <tr><td style="'+leftbottom+'">Pemasangan Implant</td><td style="'+rightbottom+'">: '+result.pemasanganimplant+' </td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td colspan="2" style="'+bordernottop+'">Tanggal Operasi <br>'+result.tanggalmulai+'</td>\n\
                    <td style="'+bordernotleft2+'">Jam Mulai Operasi <br>'+result.jammulai+'</td>\n\
                    <td style="'+bordernotleft2+'">Jam Selesai Operasi <br>'+result.tanggalselesai+' '+result.jamselesai+'</td>\n\
                </tr>\n\
                <tr><td colspan="4" style="'+bordernottop+'"> Uraian Pembedahan (Prosedure operasi yang dilakukan dan rincian temuan, ada dan tidak adanya komplikasi)<br>'+result.uraianpembedahan+'</td></tr>\n\
                <tr>\n\
                    <td colspan="2" style="'+leftbottom+'">&nbsp:</td>\n\
                    <td colspan="2" style="'+rightbottom+' text-align:center;">Yogyakarta,  '+result.tanggalselesai+', Jam:'+result.jamselesai+' WIB <br>Dokter Operator <br><br><br><br>'+result.dokteroperator+'</td>\n\
                </tr>\n\
                </table>\n\
                </div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.3cm 0.3cm 0.3cm 2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}


$(document).on('click','#indikatormutu',function(){
    var idjadwaloperasi = $(this).attr('idjop');
    var modalTitle = 'Indikator Mutu Kamar Bedah';
    var modalContent = '<form action="" id="formindikator" autocomplete="off" class="col-md-12">' +
                            '<input type="hidden" value="'+idjadwaloperasi+'" name="idjadwaloperasi"  />' +
                            '<div class="form-group">' +
                                '<label>Asesment Pra Anestesi</label>' +
                                '<input type="text" name="asesment_pra_anestesi" id="asesment_pra_anestesi" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kelengkapan Lap Anestesi</label>' +
                                '<input type="text" name="kelengkapan_lap_anestesi" id="kelengkapan_lap_anestesi" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kelengkapan Monitoring Pemilihan PSCA Anestesi</label>' +
                                '<input type="text" name="kelengkapan_monitoring_pemulihan_psca_anestesi" id="kelengkapan_monitoring_pemulihan_psca_anestesi" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Konversi Anestesi dari Lokal/Regional ke GA</label>' +
                                '<input type="text" name="konversi_anestesi_dari_lokalregionalkega" id="konversi_anestesi_dari_lokalregionalkega" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Asesmen Pra Bedah</label>' +
                                '<input type="text" name="asesmen_pra_bedah" id="asesmen_pra_bedah" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Marking</label>' +
                                '<input type="text" name="marking" id="marking" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Kelengkapan SCC</label>' +
                                '<input type="text" name="kelengkapan_scc" id="kelengkapan_scc" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Diskrepasi Diagnosa Pre dan Post</label>' +
                                '<input type="text" name="diskrepasi_diagnosa_pre_dan_post" id="diskrepasi_diagnosa_pre_dan_post" class="form-control"/>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 's',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url+'cpelayanan/save_indikatormutukamarbedah', //alamat controller yang dituju (di js base url otomatis)
                        data: $("#formindikator").serialize(), //
                        dataType: "JSON", //tipe data yang dikirim
                        //jika  berhasil
                        success: function(result) {
                            notif(result.status, result.message);
                        },
                        //jika error
                        error: function(result) {
                            console.log(result.responseText);
                        }
                    });
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            $.ajax({
                type: "POST", //tipe pengiriman data
                url: base_url+'cpelayanan/formready_indikatormutukamarbedah', //alamat controller yang dituju (di js base url otomatis)
                data: {idjadwaloperasi:idjadwaloperasi}, //
                dataType: "JSON", //tipe data yang dikirim
                //jika  berhasil
                success: function(result) {
                    setvalformid($('#asesment_pra_anestesi'),result.asesment_pra_anestesi);
                    setvalformid($('#kelengkapan_monitoring_pemulihan_psca_anestesi'),result.kelengkapan_monitoring_pemulihan_psca_anestesi);
                    setvalformid($('#konversi_anestesi_dari_lokalregionalkega'),result.konversi_anestesi_dari_lokalregionalkega);
                    setvalformid($('#asesmen_pra_bedah'),result.asesmen_pra_bedah);
                    setvalformid($('#marking'),result.marking);
                    setvalformid($('#kelengkapan_scc'),result.kelengkapan_scc);
                    setvalformid($('#diskrepasi_diagnosa_pre_dan_post'),result.diskrepasi_diagnosa_pre_dan_post);
                    setvalformid($('#kelengkapan_lap_anestesi'),result.kelengkapan_lap_anestesi);
                },
                //jika error
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});


function setvalformid(id,value){id.val(value);}