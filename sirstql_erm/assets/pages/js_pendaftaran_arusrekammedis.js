$(function(){
    loaddtpendaftaran();// tampilkan data pendaftaran sekarang/hari ini
    // if(sessionStorage.pesanorderugd==='1')
    // {
    //     setInterval('loaddtpendaftaran()', 5000);
    // }
});
function loaddtpendaftaran() //fungsi panggil data pendaftaran hari ini
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtarusrekamedis",
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}
function pendaftaranlistdt(result) // -- menampilkan data pendaftaran ke html
{   
    var menuBerkas = '' ;
    var bgberkas='';
    var beriantrian='',no=0;
    var tampildt = '<table style="font-size: 12px;" id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'+
              '<thead>'+
              '<tr >'+
                '<th>No</th>'+
                '<th>NO.RM</th>'+
                '<th>NAMA PASIEN</th>'+
                '<th>NO.SEP</th>'+
                '<th>NO.RUJUKAN</th>'+
                '<th>WAKTU DAFTAR</th>'+
                '<th>CARA DAFTAR</th>'+
                '<th>KLINIK TUJUAN</th>'+
                '<th>PEMERIKSAAN</th>'+
                '<th>CARA BAYAR</th>'+
                '<th width="8%"></th>'+
              '</tr>'+
              '</thead>'+
              '<tbody >';
    for(i in result)
    {   
        ++no ;
        // berkas di proses
        if(result[i].isberkassudahada=='1')
        {
            bgberkas='background-color:#e0f0d8;';
            menuBerkas='<a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+3+')" nobaris="'+no+'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Status Sementara"><i class="fa fa-paper-plane"></i></a>';
        }
        // berkas dalam pencarian
        else if(result[i].isberkassudahada=='2')
        {
            bgberkas='background-color:#3baeff82';
            menuBerkas='<a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+1+')" nobaris="'+no+'"  class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Proses Berkas"><i class="fa fa-sign-in"></i></a>';
            menuBerkas+=' <a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+3+')" nobaris="'+no+'"  class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Status Sementara"><i class="fa fa-paper-plane"></i></a>';
        }
        // berkas status sementara
        else if(result[i].isberkassudahada=='3')
        {
            bgberkas='background-color:#ffeb3b82';
            menuBerkas='<a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+1+')" nobaris="'+no+'" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Proses Berkas"><i class="fa fa-sign-in"></i></a>';
        }
        else
        {
            bgberkas='';
            menuBerkas='<a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+2+')" nobaris="'+no+'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Tambah ke pencarian"><i class="fa fa-search"></i> Order</a>';
        }
        tampildt += '<tr id="row'+no+'" style="'+bgberkas+'">'+ //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
           '<td>'+no+'</td>'+
           '<td>'+result[i].norm+'</td>'+
           '<td>'+result[i].namalengkap+'</td>'+
           '<td>'+result[i].nosep+'</td>'+
           '<td>'+result[i].norujukan+'</td>'+
           '<td>'+result[i].waktu+'</td>'+
           '<td>'+result[i].caradaftar+'</td>'+
           '<td>'+result[i].namaunit+'</td>'+
           '<td>'+result[i].jenispemeriksaan+'</td>'+
           '<td>'+result[i].carabayar+'</td>'+
           '<td>'+menuBerkas+'</td></tr>';
    }
    $('#tampildt').empty();
    $('#tampildt').html(tampildt+'</tfoot></table>');//
    $('#table').DataTable({"dom": '<"toolbar col-md-6">frtip',"stateSave": true,});
    menubar_table();//panggil fungsi tambahkan menu di atas tabel
    $('[data-toggle="tooltip"]').tooltip();//inisialisasi tooltips
}
function menubar_table()//tambah menu
{
    var menu = '<input id="tanggalawal" style="margin-right:6px;" type="text" size="7" class="datepicker form-control" name="tanggal" placeholder="Tanggal awal" >';
    menu+='<input id="tanggallahir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker form-control" name="tanggal" >';
    menu+=' <a class="btn btn-info btn-sm" onclick="cari_berkasbytanggal()"><i class="fa fa-desktop"></i> Tampil</a>';
    menu+=' <a onclick="listDaftarCariBerkas();" class="btn btn-primary btn-sm" data-toggle="tooltip" data-original-title="Status Cari"><i class="fa fa-search"></i> Order</a>';
    menu+=' <a onclick="listDaftarSementaraBerkas();" class="btn btn-warning btn-sm" data-toggle="tooltip" data-original-title="Status Sementara"><i class="fa fa-paper-plane"></i> Sementara</a>';
    menu+=' <a onclick="listDaftarProsesBerkas();" class="btn btn-success btn-sm" data-toggle="tooltip" data-original-title="Status Proses"><i class="fa fa-sign-in"></i> Proses</a>';
    menu+=' <a onclick="window.location.reload(true);" class="btn btn-default btn-sm" data-toggle="tooltip" data-original-title="Muat Ulang Halaman"><i class="fa  fa-refresh"></i> Refresh</a>';
    $("div.toolbar").html(menu);
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
}
// tampilkan data arus berkas yang status nya telah proses atau 1
function listDaftarProsesBerkas()
{
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggallahir').val();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtarusrekamedis",
        dataType: "JSON",
        data : { tgl1:ambiltanggal(tglawal), tgl2:ambiltanggal(tglakhir), status:'1' },
        success: function(result) {
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
            $('#tanggalawal').val(tglawal);
            $('#tanggallahir').val(tglakhir);
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}
// tampilkan data arus berkas yang status nya pencarian berkas atau 2
function listDaftarCariBerkas()
{
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggallahir').val();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtarusrekamedis",
        dataType: "JSON",
        data : { tgl1:ambiltanggal(tglawal), tgl2:ambiltanggal(tglakhir), status:'2' },
        success: function(result) {
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
            $('#tanggalawal').val(tglawal);
            $('#tanggallahir').val(tglakhir);
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}
// tampilkan data arus berkas yang status nya status sementara atau 3
function listDaftarSementaraBerkas()
{
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggallahir').val();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtarusrekamedis",
        dataType: "JSON",
        data : { tgl1:ambiltanggal(tglawal), tgl2:ambiltanggal(tglakhir), status:'3' },
        success: function(result) {
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
            $('#tanggalawal').val(tglawal);
            $('#tanggallahir').val(tglakhir);
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}
function cari_berkasbytanggal() /// -- cari berdasarkan range tanggal
{   
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggallahir').val();
    if(tglawal=='')
    {
        $('#tanggallahir').val('')
        alert_empty('tanggal awal');
    }
    else
    {
        $.ajax({ 
            url: base_url+'cadmission/loaddtarusrekamedis',
            type : "post",      
            dataType : "json",
            data : { tgl1:ambiltanggal(tglawal), tgl2:ambiltanggal(tglakhir), status:'' },
            success: function(result) {
                stopLoading();
                pendaftaranlistdt(result);
                $('#tanggalawal').val(tglawal);
                $('#tanggallahir').val(tglakhir);
            },
            error: function(result){
                stopLoading();                 
                fungsiPesanGagal();
                return false;
            }
        });
    }  
}
// fungsi ubah status isberkas sudah ada-> 0,1,2,3
function changeToProses(no,idpendaftaran,status)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status berkas rekam medis.?',
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/ubahstatus_arusrekamedis",
                    type:"POST",
                    dataType:"JSON",
                    data:{status:status,id:idpendaftaran},
                    success:function(result){
                        // var background='';
                        // if(status=='1')
                        // {
                        //     background='#e0f0d8';
                        // }
                        // else  if(status=='2')
                        // {
                        //     background='#3baeff82';
                        // }
                        // else if(status='3')
                        // {
                        //     background='#ffeb3b82';
                        // }
                        // $("#row"+no).css("background-color",background);
                        $("#row"+no).remove();
                        notif(result.status, result.message);//panggil fungsi popover message

                    },
                    error:function(result)
                    {
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    });
}

/////////FUNGSI VERIFIKASI PENDAFTARAN//////
$(document).on("click","#verifpasienbpjs", function(){
    var nobaris = $(this).attr("nobaris");
    var id = $(this).attr("alt");//idpendaftaran
    // FORM VERIFIKASI
    var modalTitle = 'Verifikasi Pendaftaran Pasien';
    var modalContent = '<form action="" id="Formverifpasienbpjs">' +
                            '<input type="hidden" name="carabayar" class="form-control"/>' +
                            '<input type="hidden" name="caradaftar" class="form-control"/>' +
                            '<input type="hidden" name="idperson" class="form-control"/>' +
                            '<input type="hidden" name="idpendaftaran" class="form-control"/>' +
                            '<input type="hidden" name="nopesan" class="form-control"/>' +
                            '<input type="hidden" name="unit" id="unitterpilih"/>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">Nama Pasien</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nama" class="form-control" readonly/>' +
                                '</div>'+
                                '<div>&nbsp;</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">No.Identitas(KTP)</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nik" placeholder="No Kartu Tanda Penduduk" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">TanggalPeriksa</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" id="tanggalverif" onchange="cari_poliklinik(this.value)" name="tanggal" class="datepicker form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">PoliTujuan</label>' +
                                '<div class="col-sm-8">' +
                                    '<select id="poliklinik" name="politujuan" class="form-control select2" style="width:100%;"><option value="0">Pilih</option></select>'+
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.JKN</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nojkn" placeholder="No Jaminan Kesehatan Nasional" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.SEP</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nosep" placeholder="No Surat Eligibilitas Peserta" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.Rujukan</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="norujukan" placeholder="No Rujukan " class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
                            '<div class="form-group skdp">' +
                                '<label class="label-control col-sm-3">No.SKDP</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="noskdp" placeholder="No SKDP " class="form-control" readonly/>' +                                
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
                            '<div class="form-group skdp">' +
                                '<label class="label-control col-sm-3">No.Kontrol</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nokontrol" placeholder="No Kontrol " class="form-control" readonly/>' +                                
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',
        buttons: {
            formSubmit: {
                text: 'Verif',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="nik"]').val()==='') //jika no namapenanggung kosong
                    {
                        alert_empty('nik');
                        return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST",
                            url: base_url+'cadmission/saveverifpasienbpjs',
                            data: $("#Formverifpasienbpjs").serialize(),
                            dataType: "JSON",
                            success: function(result) {
                                // deleteRow('row'+nobaris);
                                // notif(result.status, result.message);
                                if($('input[name="caradaftar"]').val()=='online' || $('input[name="noskdp"]').val()!=''){setTimeout(function(){cetakAntrianAnjungan(result.antrian, result.dokter,result.pasien);},500);}
                                // pendaftaranListRow(id,nobaris);//tampil per baris
                            },
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { 
        //saat form dijalankan
        // tambahkan kode yang diinginkan
        getverifpasienbpjs(id);//ambil data verifikasi
        $('[data-toggle="tooltip"]').tooltip();
        $('.datepicker').datepicker({'autoclose':true,'format': "dd/mm/yyyy",'startDate':new Date(),'orientation':"bottom"});
        $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
// fungsi ambil data pasien yang akan diverifikasi
function getverifpasienbpjs(idpendaftaran)
{
    $.ajax({
        type:"POST",
        dataType:"JSON",
        url:base_url+"cadmission/getverifpasienbpjs",
        data:{idpendaftaran:idpendaftaran},
        success:function(result){
            // jika jenis pembayaran mandiri
            $('input[name="nik"]').val(result.nik);
            $('input[name="idperson"]').val(result.idperson);
            $('input[name="unit"]').val(result.idunit);
            $('input[name="idpendaftaran"]').val(result.idpendaftaran);
            $('input[name="nopesan"]').val(result.nopesan);
            $('input[name="nama"]').val(result.namalengkap);
            $('input[name="carabayar"]').val(result.carabayar);
            $('input[name="caradaftar"]').val(result.caradaftar);
            $('input[name="noskdp"]').val(result.noskdpql);
            $('#tanggalverif').val(result.tanggal);
            cari_poliklinik($('#tanggalverif').val());
            // inputan JKN
            $('input[name="nojkn"]').val(result.nojkn);
            $('input[name="nosep"]').val();
            $('input[name="norujukan"]').val();
            // jika noskdpl tidak kosong atau pasien kunjungan kedua
            if(result.noskdpql=='0'){$('.skdp').hide();}
            else{$('.skdp').show();$('input[name="nokontrol"]').val(result.nokontrol);}
        },
        error:function(){

        }
    });
}

// cetak antrian pasien anjungan
function cetakAntrianAnjungan(antrian,dokter,pasien)
{
    var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                  <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                      '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ antrian['no'] +'</span></td></tr>'+
                      '<tr><td><span>'+ dokter['namadokter'] +'</span></td></tr>'+
                      '<tr><td><span>'+ antrian['namaloket'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['norm'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['namalengkap'] +'</span></td></tr>'+
                      '<tr><td><div>'+ pasien['alamat'] +'</td></tr>'+
                    '<tr><td></div>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
}
// cari jadwal poli
function cari_poliklinik(value) //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    $.ajax({
        type: "POST",
        url: 'cari_jadwalpoli',
        data:{date: ambiltanggal(value)},
        dataType: "JSON",
        success: function(result) {
            // console.log(result);
             edit_dropdown_poliklinik($('#poliklinik'),result,$('#unitterpilih').val());//menampilkan poliklinik
             $('#unitterpilih').val('');
        },
        error: function(result) {
            console.log(result.responseText);
        }
    });
}

function edit_dropdown_poliklinik(column,data,selected_data) //fungsi edit poliklinik
{
    // console.log(name);
    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(i in data)
        {
            select = select + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" '+ if_select(data[i].idunit,selected_data) +' >' + data[i].namaunit +' '+ if_empty(data[i].titeldepan) +' '+ if_empty(data[i].namalengkap) +' '+ if_empty(data[i].titelbelakang) +' ' +if_empty(data[i].tanggal)+' </option>';
        }
        column.html('<option value="0">Pilih</option>'+ select );
        $('.select2').select2();
    }   
}