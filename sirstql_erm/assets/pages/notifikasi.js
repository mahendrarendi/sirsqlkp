$(function () 
{ 
    var set_status = $('#set_status').val();
    var set_message = $('#set_message').val();
    // jika status tidak kosong panggil fungsi notif
    if(set_status !=='')
    {
        notif(set_status, set_message);
    }
})


function notif(status, message)
{
    // console.log(status);
    $.notify({
        icon: 'fa fa-bell fa-lg',
        message: '<strong>&nbsp;'+message+'</strong>'
    },{
        type: status,
        timer: 1000
    });
    return true;
}