var norm = localStorage.getItem('norm');//siapkan norm -> ambil dari localstorage norm
var idpendaftaran = localStorage.getItem('idpendaftaranranap');
localStorage.setItem('jenisrawat','ranap');

$(function(){
    localStorage.removeItem('dataSatuan');
    $('#riwayatdtpasien').attr('norm',norm);
    $('.select2').select2();
    pemeriksaanranapDetailPasien();//panggil fungsi (cari data pasien)
    $('input[name="idinap"]').val(localStorage.getItem('idinap'));
    $('input[name="idrencanamedispemeriksaan"]').val(localStorage.getItem('idrencanamedispemeriksaan'));
    localStorage.setItem('jenisrawat','ranap');
    setTimeout(function(){
        pemeriksaanklinikRefreshBhp(localStorage.getItem('idpendaftaranranap'));        
        detailpemberianobatbhp();
    },1000);
    
    cariBhpFarmasi($('select[name="caribhppasienpulang"]'));
});

function detailpemberianobatbhp()
{
    $.ajax({
        type:"POST",
        url:"riwayat_pemberianobatranap",
            data:{idp:idpendaftaran},
        dataType:"JSON",
        success:function(result){
            var row = '';
            var waktu = '';
            var tdwaktu = '';
            var no  = 0;
            for(var x in result)
            {
                if(waktu != result[x].waktu)
                {
                    waktu = result[x].waktu;                    
                    ++no 
                    tdwaktu = '<td>'+ no +'</td><td>'+result[x].waktu+' / <span class="label" style="'+statuspemberian(result[x].status)+'">'+result[x].status+'</span></td>';
                }
                else
                {
                    tdwaktu = '<td></td><td></td>';
                }
                row += '<tr>'+tdwaktu+'<td>'+result[x].namabarang+'</td><td>'+result[x].jumlah+'</td><td>'+result[x].namasatuan+'</td><td>'+convertToRupiah(result[x].harga)+'</td><td>'+convertToRupiah(result[x].total)+'</td><td><span class="label" style="'+statuspemberian(result[x].statuspelaksanaan)+'">'+result[x].statuspelaksanaan+'</span></td><td></td></tr>';
            }
            
            $('#viewDetailObatBHP').html(row);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

function statuspemberian(status)
{ 
    var color = {rencana:'background-color:#a3a3a3;',terlaksana:'background-color:#00a65a;',batal:'background-color:#dd4b39;'};
    return  color[""+status+""];
}

function cariBhpFarmasi(selectNameTag)//cari bhp
{
    startLoading();
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
        stopLoading();
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data) {
            return {results: data};
        },              
    }
    });
}

function inputBhpFarmasi(value,kondisi='')
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_inputbhp",
        data:{x:value, i:localStorage.getItem('idinap'), ip: idpendaftaran,kondisi:kondisi},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            
        pemeriksaanklinikRefreshBhp(idpendaftaran);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

function cetakBhpAturanpakai(value='')//cetak aturan pakai
{
    var idb = $('#idrencanamedisbarang'+value).val();
    $.ajax({
        dataType:"JSON",
        url:base_url+"cpelayananranap/pemeriksaan_cetak_aturanpakai",
        data:{idb:idb},
        type:"POST",
        success:function(result){
            cetakEtiket(result, value);
        },
        error:function(result)
        {
            fungsiPesanGagal();
            return false;
        }
    });
}

//basit, clear
function pemeriksaanranapRefreshBhp()//refreshDataBHP
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_refreshbhp',
        data: {i:localStorage.getItem('idinap')},
        dataType: "JSON",
        success: function(result) {
            var lisBhp = '', i=0, no=0, subtotal=0, total=0;jumlahambil=0;
            for (i in result)
            {
                total = result[i].total.toString();
                total = total.slice(0, -3);
                total = parseInt(total);
                jumlahambil = (result[i].kekuatanperiksa/result[i].kekuatan) * result[i].jumlahpemakaian;
                
                subtotal += parseInt(result[i].total);
                if (result[i].jenisgrup == 'racikan')
                {
                    lisBhp += '<tr id="listBhp'+ ++no +'" class="bg bg-gray"><td>'+ no +'</td>'+
                              '<td colspan="3"><input type="hidden" id="idrencanamedisbarang'+no+'" size="5" class="form-control"  value="'+result[i].idrencanamedisbarang+'">[ RACIKAN '+result[i].grup+' ]</td>' +
                              '<td>'+(!is_empty(result[i].waktupengembalian)?result[i].jumlahpemakaian:'<input type="text" onchange="editBhpJumlah('+no+')" id="jumlah'+no+'"size="5" class="form-control"  name="jumlah[]" value="'+result[i].jumlahpemakaian+'">')+'</td>'+
                              '<td colspan="2" align="right" class="bg bg-aqua">Kemasan Racik</td>'+
                              '<td class="bg bg-aqua"><select class="select2 form-control" style="width:100%" onchange="editKemasan('+no+')" id="idkemasan'+no+'" name="idkemasan'+no+'"><option value="0">Pilih</option></select></td>'+
                              '<td><select class="select2 form-control" style="width:100%" onchange="editSatuanpemakaian('+no+')" id="idsatuan'+no+'" name="idsatuan'+no+'"><option value="0">Pilih</option></select></td>'+
                              '<td><select class="select2 form-control" style="width:100%" onchange="editPenggunaan('+no+')" id="penggunaan'+no+'" name="penggunaan'+no+'"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select></td>'+
                              '<td></td>'+
                              '<td></td>'+
                              '<td></td>'+
                              '<td></td>';
                }
                else
                {
                    lisBhp += '<tr id="listBhp'+ ++no +'"><td>'+ no +'</td>'+
                              '<td '+((result[i].grup==0)?'colspan="2"':'')+'><input type="hidden" id="idrencanamedisbarang'+no+'" size="5" class="form-control"  value="'+result[i].idrencanamedisbarang+'">'+((result[i].grup==0)?"":"|-</td><td>")+result[i].namabarang+'</td>' +
                              '<td>'+(!is_empty(result[i].waktupengembalian)?result[i].jumlahpesan:'<input type="text" onchange="editBhpJumlahpesan('+no+')" id="jumlahpesan'+no+'"size="5" class="form-control"  name="jumlahpesan[]" value="'+result[i].jumlahpesan+'">')+'</td>'+
                              '<td>'+(!is_empty(result[i].waktupengembalian)?result[i].jumlahpemakaian:'<input type="text" onchange="editBhpJumlah('+no+')" id="jumlah'+no+'"size="5" class="form-control"  name="jumlah[]" value="'+result[i].jumlahpemakaian+'">')+'</td>'+
                              '<td>'+result[i].kekuatan+'</td>'+
                              '<td><input type="text" onchange="editBhpKekuatan('+no+')" id="kekuatan'+no+'" size="5" class="form-control" name="kekuatan[]" value="'+result[i].kekuatanperiksa+'"></td>'+
                              '<td><input type="text" onchange="editBhpGrup('+no+')" id="grup'+no+'" size="5" class="form-control"  name="grup[]" value="'+result[i].grup+'" /></td>'+
                              '<td>'+((result[i].grup==0)?'<select class="select2 form-control" style="width:100%" onchange="editSatuanpemakaian('+no+')" id="idsatuan'+no+'" name="idsatuan'+no+'"><option value="0">Pilih</option></select>':'')+'</td>'+
                              '<td>'+((result[i].grup==0)?'<select class="select2 form-control" style="width:100%" onchange="editPenggunaan('+no+')" id="penggunaan'+no+'" name="penggunaan'+no+'"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select>':'')+'</td>'+
                              '<td><input type="text" onchange="editBhpCatatan('+no+')" id="catatanperobat'+no+'"size="5" class="form-control"  name="catatanperobat[]" value="'+result[i].catatan+'"></td>'+
                              '<td>'+jumlahambil+'</td>'+
                              '<td>'+convertToRupiah(total)+'</td>'+
                              // '<td>'+result[i].tatacara+'</td>'+
                              '<td><select class="select2 form-control" style="width:100%" onchange="editJenisasal('+no+')" id="jenisasal'+no+'" name="jenisasal'+no+'"><option value="0">Pilih</option></select></td>';
                }
                lisBhp +=
                          
                          '<td>'+((!is_empty(result[i].waktupengembalian))?'| Sisa '+ result[i].jumlahsisa +' diretur tanggal '+result[i].waktupengembalian:((result[i].jumlahsisa > 0)?'| Sisa '+ result[i].jumlahsisa +' dapat diretur':''))+((result[i].jumlahplot == 0)?'':' | plot:'+result[i].jumlahplot)+'</td>'+
                          // '<td><input type="text" onchange="editBhpSetHarga('+no+')" id="harga'+no+'" size="5" class="form-control"  value="'+result[i].hargabeli+'"></td>'+
                          '<td>';
                get_dataasalobatbhp("jenisasal"+no, result[i].jenisasal);
                isicombosatuan('idsatuan'+no, result[i].idsatuanpemakaian);
                isicombokemasan('idkemasan'+no, result[i].idkemasan);
                if (result[i].jenisgrup != 'komponen racikan' && result[i].jumlahplot != 0)
                {
                    lisBhp += '<a onclick="cetakBhpAturanpakai('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a> ';
                }
                if (result[i].jenisgrup != 'komponen racikan' && result[i].jumlahplot == 0)
                {
                    lisBhp += '<a id="tombolplot'+no+'" onclick="plotBhp('+result[i].idrencanamedisbarang+','+no+')" data-toggle="tooltip" data-original-title="Buat Rencana Pemakaian" class="btn btn-success btn-xs"><i class="fa fa-medkit"></i></a> ';
                }
                if (result[i].jumlahsisa > 0 && is_empty(result[i].waktupengembalian))
                {
                    lisBhp += '<a onclick="retursisaBhp('+no+')" data-toggle="tooltip" data-original-title="Retur Sisa" class="btn btn-primary btn-xs"><i class="fa fa-reply-all"></i></a> ';
                    lisBhp += '<a id="pindahsisakeobatpulangBhp" no="'+no+'" jumlahsisa="'+result[i].jumlahsisa+'" jumlahpakai="'+result[i].jumlahpemakaian+'" data-toggle="tooltip" data-original-title=" sisa untuk obat pulang" class="btn btn-warning btn-xs"><i class="fa fa-share"></i></a>';
                }
                
                if (result[i].jumlahrencana > 0)
                {
                    lisBhp += '<a id="tombolbatal'+no+'" onclick="batalPlotBhp('+result[i].idrencanamedisbarang+','+no+')" data-toggle="tooltip" data-original-title="Batalkan Rencana Pemakaian (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a> ';
                }
                if (result[i].jumlahplot == 0 && result[i].jenisgrup != 'racikan')
                {
                    lisBhp += '<a  onclick="hapusBhp('+no+')" data-toggle="tooltip" data-original-title="Hapus" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a> </td></tr>';
                }
                lisBhp += '<input type="hidden" id="idbarang'+no+'" size="5" class="form-control" value="'+result[i].idbarang+'">';//<td>'+result[i].jenis+'</td>
            }
            $('#viewDataBhp').empty();
            lisBhp +='<tr class="bg bg-info"><td colspan="8">Subtotal</td><td colspan="1">'+convertToRupiah(subtotal)+'</td><td colspan="2"></td></tr>';
            $('#viewDataBhp').html(lisBhp);
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
$(document).on("click","#pindahsisakeobatpulangBhp",function(){
    var a = $(this).attr("no");
    $.confirm({
        icon:"fa fa-question",
        theme:"modern",
        closeIcon:!0,
        animation:"scale",
        type:"orange",
        title:"Konfirmasi",
        content:"Pindahkan Sisa Obat/BHP untuk Obat Pulang.?",
        buttons:{
            confirm:function(){
                settingBhp("","sisauntukobatpulang",$("#idrencanamedisbarang"+a).val(),"",a)
            },
            cancel:function(){}
        }
    })
});

function plotBhp(value, no)
{
    var modalTitle = 'Tambah Rencana Pemakaian BHP/Obat';
    var modalContent = '<form action="" id="FormRencanaPemakaian">' +
                            '<div class="form-group">' +
                                '<label>Waktu Mulai Rencana (tahun-bulan-tanggal jam)*</label>' +
                                '<input type="hidden" id="idrencanamedisbarang" name="idrencanamedisbarang" class="form-control"  value="'+value+'"><input type="hidden" id="idinap" name="idinap" class="form-control"  value="'+localStorage.getItem('idinap')+'"><input type="text" name="tgl" class="datetimepicker form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Jumlah Pemakaian</label>' +
                                '<input type="text" class="form-control" style="width:50%;" name="jumlahpemakaian" value="'+localStorage.getItem('jumlahpemakaian')+'">'+ $('select[name="idsatuan'+no+'"]').find('option:selected').html() +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Jumlah Per Hari</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahperhari" value="'+localStorage.getItem('jumlahperhari')+'">'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Selama (Hari)</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="selama" value="'+localStorage.getItem('selama')+'">'+
                            '</div>' +
                            '<div class="form-group">' +
                                '* Geser kotak jika pemilihan tanggal tidak nampak' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        draggable: true,
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buat rencana pemakaian BHP/Obat',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tgl"]').val() == '')
                    {
                        alert_empty('tgl');
                        return false;
                    }
                    else if ($('input[name="jumlahperhari"]').val() == '')
                    {
                        alert_empty('jumlahperhari');
                        return false;
                    }
                    else if ($('input[name="selama"]').val() == '')
                    {
                        alert_empty('selama');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("jumlahpemakaian",$('input[name="jumlahpemakaian"]').val());
                        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
                        localStorage.setItem("selama",$('input[name="selama"]').val());
//                        alert($('input[name="idrencanamedisbarang"]').val());
                        simpanrencanabhp(no);
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function simpanrencanabhp(no)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencanabhp',
        data: {t:$('input[name="tgl"]').val(), in:$('input[name="idinap"]').val(), ir:$('input[name="idrencanamedisbarang"]').val(), jm:$('input[name="jumlahpemakaian"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            if(result.status=='success'){
                notif(result.status, result.message);                
                $('#tombolplot'+no).remove();
            }else{
                notif(result.status, result.message);
                return false;
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//basit, clear
function batalPlotBhp(value, no)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Batalkan Rencana Pemakaian? (Tidak Termasuk Yang Terlaksana)',
        buttons: {
            confirm: function () {             
                batalkanrencanabhp(value, no);
            },
            cancel: function () {               
            }            
        }
    });
}

//basit, clear
function batalkanrencanabhp(value, no)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/batalkan_rencanabhp',
        data: {i:value}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            if(result.status=='success'){
                notif(result.status, result.message);
                $('#tombolbatal'+no).remove();
            }else{
                notif(result.status, result.message);
                return false;
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//basit, clear
function isicombosatuan(id, selected)
{
    if (is_empty(localStorage.getItem('dataSatuan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_satuan',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataSatuan', JSON.stringify(result));
                fillcombosatuan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombosatuan(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombosatuan(id, selected);
            },
            error: function(result){
                fillcombosatuan(id, selected);
            }
        });
    }
}

//basit, clear
function fillcombosatuan(id, selected)
{
    var datasatuan = JSON.parse(localStorage.getItem('dataSatuan'));
    var i=0;
    for (i in datasatuan)
    {
      $("#"+id).append('<option ' + ((datasatuan[i].idsatuan===selected)?'selected':'') + ' value="'+datasatuan[i].idsatuan+'">'+datasatuan[i].namasatuan+'</option>');
    }
}

//basit, clear
function isicombokemasan(id, selected)
{
    if (is_empty(localStorage.getItem('dataKemasan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_kemasan',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataKemasan', JSON.stringify(result));
                fillcombokemasan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombosatuan(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombokemasan(id, selected);
            },
            error: function(result){
                fillcombokemasan(id, selected);
            }
        });
    }
}

//basit, clear
function fillcombokemasan(id, selected)
{
    var datakemasan = JSON.parse(localStorage.getItem('dataKemasan'));
    var i=0;
    for (i in datakemasan)
    {
      $("#"+id).append('<option ' + ((datakemasan[i].idkemasan===selected)?'selected':'') + ' value="'+datakemasan[i].idkemasan+'">'+datakemasan[i].kemasan+'</option>');
    }
}

//basit, clear
function editPenggunaan(value)
{
    $.ajax({ 
        url: 'pemeriksaanranap_ubahpenggunaan',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedisbarang'+value).val(), is:$('#penggunaan'+value).val() },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//basit, clear
function editSatuanpemakaian(value)
{
    $.ajax({ 
        url: 'pemeriksaanranap_ubahsatuanpemakaian',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedisbarang'+value).val(), is:$('#idsatuan'+value).val() },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//basit, clear
function editKemasan(value)
{
    $.ajax({ 
        url: 'pemeriksaanranap_ubahkemasan',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedisbarang'+value).val(), is:$('#idkemasan'+value).val() },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//basit, clear
function editJenisasal(value)
{
    $.ajax({ 
        url: 'pemeriksaanranap_ubahjenisasal',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedisbarang'+value).val(), ja:$('#jenisasal'+value).val() },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//basit, sementara dinonaktifkan
function editBhpGrup(value)//seting atau ubah grup bhp
{
    settingBhp($('#grup'+value).val(),'grup',$('#idrencanamedisbarang'+value).val(),null,null,'#idsatuan'+value);
}

//settingBhp(d,field,x,y,no)
//basit, clear
function editBhpJumlah(value)//seting atau ubah grup bhp
{
    settingBhp($('#jumlah'+value).val(),'jumlah',$('#idrencanamedisbarang'+value).val(),null,null,'#kekuatan'+value);
}
//basit, clear
function editBhpCatatan(value)//seting atau ubah grup bhp
{
    settingBhp($('#catatanperobat'+value).val(),'catatan',$('#idrencanamedisbarang'+value).val(),null,null,'#jenisasal'+value);
}
//basit, clear
function editBhpJumlahpesan(value)//seting atau ubah grup bhp
{
    settingBhp($('#jumlahpesan'+value).val(),'pesan',$('#idrencanamedisbarang'+value).val(),null,null,'#jumlah'+value);
}
//basit, clear
function editBhpKekuatan(value)//seting atau ubah grup bhp
{
    settingBhp($('#kekuatan'+value).val(),'kekuatan',$('#idrencanamedisbarang'+value).val(),null,null,'#grup'+value);
}
//basit, sementara dinonaktifkan
function editBhpSetHarga(value)//seting atau ubah grup bhp
{
    var d = $('#harga'+value).val();
    var x = $('#idrencanamedisbarang'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'harga',x,y,value);
}

//basit, clear
function settingBhp(d,field,x,y,no,nextfocus)
{
    $.ajax({ 
        url: 'pemeriksaan_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:field, x:x,y:y },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                pemeriksaanranapRefreshBhp();
                setTimeout(function(){
                    $(nextfocus).focus();
                },300);
                
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}


//basit, clear
function hapusBhp(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                var x = $('#idrencanamedisbarang'+value).val();
                var d='', y='';
                settingBhp(d,'del',x,y);
                pemeriksaanranapRefreshBhp();
            },
            cancel: function () {               
            }            
        }
    });
}

//basit, clear
function retursisaBhp(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Retur Sisa?',
        buttons: {
            confirm: function () {
                settingBhp('','retur',$('#idrencanamedisbarang'+value).val(),'');
            },
            cancel: function () {               
            }            
        }
    });
}

//basit, clear
function pemeriksaanranapDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_caridetailpasien',
        data: {i:localStorage.getItem('idinap')},
        dataType: "JSON",
        success: function(result)
        {
            stopLoading();
            if (is_null(norm) || is_null(idpendaftaran))
            {
                norm            = result.pasien.norm;
                localStorage.setItem('norm', norm);
                idpendaftaran   = result.pasien.idpendaftaran;
                localStorage.setItem('idpendaftaranranap', idpendaftaran);
            }
            $('textarea[name="dokter"]').val(if_empty(result.rajal.dokter));
            $('textarea[name="anamnesa"]').val(result.rajal.anamnesa);
            $('textarea[name="keterangan"]').val(result.rajal.keterangan);
            $('textarea[name="catatan"]').val(result.pasien.catatan);
            $('textarea[name="keteranganobat"]').val(result.pasien.keteranganobat);
            viewIdentitasPasien(result.pasien);//panggil view identitas
            $('input[name="idpendaftaran"]').val(result.rajal.idpendaftaran);
            $('input[name="idunit"]').val(result.rajal.idunit);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//basit, clear
function viewIdentitasPasien(value)//tampilidentitas
{
    var profile = '<div class="bg bg-gold">'
                +'<div class="box-body box-profile" >'
                 +'<div class="login-logo" style="margin-bottom:0px;">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-center">NIK : '+value.nik+'</p>'
                  +'<h5 class=" text-center"> <i class="fa fa-calendar margin-r-5"></i> '+value.tanggallahir+' /  <i>'+value.usia+'</i> </h5>'
                  +'<p class="text-center"><i>Ayah : '+value.ayah+' , Ibu : '+value.ibu+'</i></p>'
                +'</div>'
              +'</div>';
    var detail_profile = '<div class="bg bg-gold"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td>: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                 +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td>: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Tempat Lahir</strong></td>'
                  +'<td>: &nbsp;'+value.tempatlahir+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td>: &nbsp; '+value.jeniskelamin+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td>: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' 
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td>: &nbsp; '+value.namapendidikan+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i> Agama</strong></td>'
                  +'<td>: &nbsp;'+value.agama+'</td>'
                  
                +'</tr>'
        
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Alamat </strong></td>'
                  +'<td width="250px">: &nbsp; '+value.alamat+'</td>'

                  
                +'</tr>'
                +'</tbody>'
             +'</table></div></div>';
    $('#ranap_detailprofile').html(detail_profile);
    $('#ranap_profile').html(profile);
}

//pemesanan obat ke farmasi
$(document).on('click','#pesankefarmasi',function(){
    formpemesananobat('Pesan Obat/BHP','Pesan');
});


function formpemesananobat(judul,mode)
{
    var idinap     = localStorage.getItem('idinap');
    var pilihobat  = ((mode == 'Pesan') ? '<select class="select2 col-md-4" id="fillpesanreturobat"><option value="0">Input Obat/BHP</option></select>' : '' );
    var modalTitle = judul;
    var modalContent = '<form action="" id="formpemesananobat" autocomplete="off">' +
                            '<input type="hidden" name="idinap" value="'+idinap+'"/>' +
                            '<input type="hidden" name="modesave" value="ranap"/>' +
                            '<div class="form-group">' +
                                '<label class="col-md-2">Tanggal '+ mode +'</label>' +    
                                '<div class="col-md-2"><input type="text" id="tanggalfaktur" class="datepicker form-control" name="tanggalfaktur" value="" /></div>'+
                            '</div>'+
                            
                            '<div class="col-md-12" style="margin:-5px;">&nbsp;</div>'+
                            
                            '<div class="form-group">' +
                                '<label class="col-md-2">Unit Asal</label>' +
                                '<div class="col-md-2"><input type="text" id="unitasal" class="form-control" name="unitasal" value="" readonly /></div>'+
                            '</div>'+
                            
                            '<div class="col-md-12" style="margin:-5px;">&nbsp;</div>'+
                            
                            '<div class="form-group">' +
                                '<label class="col-md-2">Unit Tujuan</label>' +
                                '<div class="col-md-2"><select id="idunitasal" class="form-control" name="idunitasal"></select></div>'+
                            '</div>'+
                            
                            '<div class="col-md-12">&nbsp;</div>'+
                            
                            
                            '<div class="form-group">' +
                                pilihobat +
                                '<table class="table table-hover table-bordered table-striped" style="width: 100%; margin-top: 6px;">'+
                                '<thead>'+
                                  '<tr class="bg bg-yellow">'+
                                    '<th width="50">Kode</th>'+
                                    '<th>Obat/BHP</th>'+
                                    ((mode=='Pesan') ? '' : '<th>BatchNo</th><th>Kadaluarsat</th><th>Sisa Obat</th>'  ) +
                                    '<th>Jumlah '+mode+'</th>'+
                                    '<th>Satuan</th>'+
                                    '<th></th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                    '<tr id="rowspesananobat"><td colspan="'+ ((mode=='Pesan') ? '5' : '8'  ) +'"></td></tr>'+
                                '</tbody>'+
                              '</table>'+
                            '</div>'+
                        '</form>';
                
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'lg',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    
                    
                    var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
                    for(var z in arrIndex)
                    {
                        var jumlahpesan = $('#jumlahpesan_'+arrIndex[z]).val();
                        if(jumlahpesan == '')
                        {
                            
                            
                            pesan_inputjumlahpesanan(arrIndex[z],'kosong');
                            return false;
                            
                        }
                    }
                    //simpan pesanan
                    simpanpesanobatfarmasi(mode);
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            $('#tanggalfaktur').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now'); //Initialize Date picker
            ((mode == 'Pesan') ? select2serachmulti($('#fillpesanreturobat'),'cpelayananranap/fillpesanbarangkefarmasi') : '' );
            onReadyPemesanandanpengembalianobat(mode);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function pesan_inputjumlahpesanan(index,mode)
{
    $('#jumlahpesan_'+index).focus();
    $('#jumlahpesan_'+index).after('<br/><i id="pesan'+index+'" class="text text-red"> '+((mode=='kosong') ? 'Jumlah Pesan Harap Diisi' : 'Jumlah melebihi sisa obat/bhp' )+'.</i>');
}


function isselesaiperiksa(status)
{
    if(status=='selesaiperiksa')
    {
        $.alert('<b>Pemeriksaan Sudah Selesai</b><div class="text text-red">Perubahan Tidak Tersimpan.</div><br> Mohon Melakukan Tambah/Ubah/Hapus Tindakan Sebelum Pasien Menyelesaikan Pembayaran.');
        return false;
    }
    return true;
    
}
$(document).on('click','#splitbhp',function(){
   var value = '';
   var modalTitle = 'Tambah Rencana Pemakaian BHP/Obat';
    var modalContent = '<form action="" id="FormRencanaPemakaian">' +
                            '<div class="form-group">' +
                                '<label>Waktu Mulai Rencana (tahun-bulan-tanggal jam)*</label>' +
                                '<input type="hidden" id="idrencanamedisbarang" name="idrencanamedisbarang" class="form-control"  value="'+value+'"><input type="hidden" id="idinap" name="idinap" class="form-control"  value="'+localStorage.getItem('idinap')+'"><input type="text" name="tgl" class="datetimepicker form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Jumlah Pemakaian</label>' +
                                '<input type="text" class="form-control" style="width:50%;" name="jumlahpemakaian" value="'+localStorage.getItem('jumlahpemakaian')+'">'+ $('select[name="idsatuan'+no+'"]').find('option:selected').html() +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Jumlah Per Hari</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahperhari" value="'+localStorage.getItem('jumlahperhari')+'">'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Selama (Hari)</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="selama" value="'+localStorage.getItem('selama')+'">'+
                            '</div>' +
                            '<div class="form-group">' +
                                '* Geser kotak jika pemilihan tanggal tidak nampak' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        draggable: true,
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buat rencana pemakaian BHP/Obat',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tgl"]').val() == '')
                    {
                        alert_empty('tgl');
                        return false;
                    }
                    else if ($('input[name="jumlahperhari"]').val() == '')
                    {
                        alert_empty('jumlahperhari');
                        return false;
                    }
                    else if ($('input[name="selama"]').val() == '')
                    {
                        alert_empty('selama');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("jumlahpemakaian",$('input[name="jumlahpemakaian"]').val());
                        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
                        localStorage.setItem("selama",$('input[name="selama"]').val());
                        simpanrencanabhp(no);
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    }); 
});