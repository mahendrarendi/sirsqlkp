var listbarang='';
$(function () {
    removelocalstorage('barangpesanan');
      removelocalstorage('modeubah');
      removelocalstorage('lsdistribusibarang');
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    tableReturbarang();
})

function tableReturbarang()
{ 
  listbarang = $('#listretur').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : false,"stateSave": true,"order": [],"ajax": {"data":{mode:'retur',cari:function(){return $('input[name="cari"]').val();},tanggal1:function(){return $('input[name="tanggal1"]').val();}, tanggal2:function(){return $('input[name="tanggal2"]').val();} },"url": base_url+'cmasterdata/dt_returbarang',"type": "POST"},"columnDefs": [{ "targets": [ 1,2,4 ],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#tampil',function(){reloaddata();});
$(document).on('keyup','#cari',function(){reloaddata();});
$(document).on('click','#reload',function(){
  $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
  $('input[name="cari"]').val('').keyup();
  listbarang.state.clear();
  reloaddata();
});
function reloaddata(){listbarang.ajax.reload();}

$(document).on('click','#view',function(){
  var w = $(this).attr('waktu');
  var g = $(this).attr('grupwaktu');
  var s = $(this).attr('setundo');
  viewaddtransformasibarang(w,g,s);
});
//form persetujuan distribusi
function pembetulanpengembalian(idpesan)
{
    startLoading();
    localStorage.setItem('idpesan',idpesan);
    localStorage.setItem('modeubah', 'pembetulanpesan');
    window.location.href=base_url + 'cmasterdata/distribusireturbarang';
}
function cetakbarangpesanan(idpesan)
{
    $.ajax({ 
        url:  base_url+'cmasterdata/jsontampilpesananbarang',type : "POST",dataType : "JSON",data:{id:idpesan},
        success: function(result){
            var no=0, list='', total=0;for(var x in result){ total+=(parseFloat(result[x].hargajual)*parseFloat(result[x].jumlah)); list+='<tr><td class="padding">'+ ++no +'</td><td class="padding">'+ result[x].kode +', '+result[x].namabarang +'</td><td class="padding"></td><td class="padding">'+convertToRupiah(result[x].hargajual)+'</td><td class="padding">'+convertToRupiah(result[x].jumlah)+'</td><td class="padding">'+result[x].namasatuan+'</td><td class="padding">'+convertToRupiah(parseFloat(result[x].hargajual) * parseFloat(result[x].jumlah))+'</td></tr>';}
            var cetak='<table width="100%"><thead></thead><tbody><tr><td rowspan="3"><img style="opacity:0.8;filter:alpha(opacity=50);" src="'+base_url+'assets/images/headerkuitansismall.jpg" /></td><td class="right" style="font-size:20px;">BELANJA UNIT '+result[0].unittujuan+'</td></tr><tr><td style="font-size:16px;" class="right">No.Faktur : '+result[0].nofaktur+'</td></tr><tr><td class="right" style="font-size:12.5px;">Waktu Belanja : '+result[0].waktu+'</td></tr></tfooter></table><table cellpadding="0" cellspacing="0" width="100%" border="1" ><thead><tr><td class="padding">No</td><td class="padding">Nama Barang</td><td class="padding">ED</td><td class="padding">Harga@</td><td class="padding">Jumlah</td><td class="padding">Satuan</td><td class="padding">Nominal</td></tr></thead><tbody>'+list+'<tr><td colspan="6" class="left">&nbsp;Total</td><td>&nbsp;'+convertToRupiah(total)+'</td></tr></tfooter></table><table class="table" style="width:100%;"><tr><td width="80%">Pemesan</td><td>Pengirim</td></tr></table>';
            fungsi_cetaktopdf(cetak,'<style>@page { size:A4 lanscape; margin:0.2cm 0.5cm;}.padding{padding:4px;}th{text-align:left;}.left{text-align:left;}.right{text-align:right;}.block{display:block;}</style>');
        },error: function(result){fungsiPesanGagal();return false;}
      });
}

function terimapengembalian(idpesan)
{
  $.confirm({
    icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type:'orange',title:'Konfirmasi',content:'Periksa Kembali Pengembalian Barang',
        buttons: {
            terima: function () {                
              $.ajax({
                url:base_url+'cmasterdata/save_terimaretur',
                type:'POST',dataType:'JSON',data:{ip:idpesan},
                success: function(result){notif(result.status, result.message);reloaddata();},
                error: function(result){fungsiPesanGagal();return false;}
            });
            },
            batal: function () {               
            }            
        }
    });
}