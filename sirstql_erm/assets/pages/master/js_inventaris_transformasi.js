var listbarang='';
$(function () {
    removelocalstorage('lstransformasiasal');
    removelocalstorage('waktu');
    removelocalstorage('grupwaktu');
    localStorage.setItem('undotransformasi','');
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    tabelTransformasi();
})

function tabelTransformasi()
{ 
  listbarang = $('#transformasi').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : false,"stateSave": true,"order": [],"ajax": {"data":{cari:function(){return $('input[name="cari"]').val();},tanggal1:function(){return $('input[name="tanggal1"]').val();}, tanggal2:function(){return $('input[name="tanggal2"]').val();} },"url": base_url+'cmasterdata/dt_transformasibarang',"type": "POST"},"columnDefs": [{ "targets": [ 1,2,4 ],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#tampil',function(){reloaddata();});
$(document).on('keyup','#cari',function(){reloaddata();});
$(document).on('click','#reload',function(){
  $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
  $('input[name="cari"]').val('').keyup();
  listbarang.state.clear();
  reloaddata();
});
function reloaddata(){listbarang.ajax.reload();}

$(document).on('click','#view',function(){
  var w = $(this).attr('waktu');
  var g = $(this).attr('grupwaktu');
  var s = $(this).attr('setundo');
  viewaddtransformasibarang(w,g,s);
});
function viewaddtransformasibarang(waktu, grupwaktu, setundo)
{
    localStorage.setItem('undotransformasi',setundo);
    localStorage.setItem('waktu',waktu);
    localStorage.setItem('grupwaktu',grupwaktu);
    window.location.href=base_url + 'cmasterdata/addtransformasibarang';
}