var dtinventaris='';
var dtinventarisdetail='';

$(function () {
    listdata();
})

//menampilkan data aset
function listdata()
{ 
  dtinventaris = $('#dtinventarisnonmedis').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cmasterdata/dt_inventarisnonmedis',
     "type": "POST"
 },
 "columnDefs": [{ "targets": [0,-1],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', statusinventaris(aData[10]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtinventaris.state.clear();
  dtinventaris.ajax.reload();
});

function reloaddata(){dtinventaris.ajax.reload(null,false);}

//detail inventaris non medis
$(document).on('click','#detailinventaris',function(){
    var idruang   = $(this).attr('alt');
    var namaruang = $(this).attr('namaruang');
    var cetak ='';
        cetak+='<table id="dtdetailinventaris" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
               +'<thead>'
                    +'<tr class="bg bg-yellow-gradient">'
                      +'<th>Nama Alat</th>'
                      +'<th>Merek</th>'
                      +'<th>Tipe</th>'
                      +'<th>No.Seri</th>'
                      +'<th>No.Inventaris</th>'
                      +'<th>No.Sertifikat</th>'
                      +'<th>Tanggal Pembelian</th>'
                      +'<th>Nilai Aset</th>'
                      +'<th>Penyusutan</th>'
                      +'<th>Status</th>'
                      +'<th></th>'
                    +'</tr>'
                +'</thead>'
                +'<tbody>'
                +'</tfoot>'
              +'</table>';
    tampildetail(cetak,namaruang,idruang);
});
function tampildetail(html,namaruang,idruang)
{
    $.confirm({ //aksi ketika di klik menu
        title: 'Detail Inventaris '+namaruang,
        content: html,
        columnClass: 'lg',
        //menu reset
        buttons: {   
            formReset:{
                text: 'Close',
                btnClass: 'btn-danger'
            }  
        },
        onContentReady: function () {
            dtinventarisdetail = $('#dtdetailinventaris').DataTable({
                "processing": true,
                "serverSide": true,
                "lengthChange": false,
                "searching" : true,
                "stateSave": true,
                "order": [],
           "ajax": {
               "data":{idruang:idruang},
               "url": base_url+'cmasterdata/dt_detailinventarisnonmedis',
               "type": "POST"
           },
           "columnDefs": [{ "targets": [-1],"orderable": false,},],
           "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', statusinventaris(aData[10]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
           "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
           });
        }
    });
}



//tambah aset
$(document).on("click",'#addinventaris',function(){formtambahedit('','Tambah Inventaris');});
//ubah aset
$(document).on("click",'#editinventaris',function(){formtambahedit($(this).attr('alt'),'Ubah Inventaris');});
//unduh
$(document).on("click",'#unduh',function(){
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: '',
        content: '<b>Unduh Data Inventaris.?</b>',
        buttons: {
            formSubmit: {/*menu save atau simpan*/
                text: 'unduh',
                btnClass: 'btn-success',
                action: function () {
                    window.location.href=base_url+"creport/downloadexcel_datainventaris";
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }          
        }
    });
});

function formtambahedit(idinventaris,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formInventaris" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Pilih Barang/Alat </label>' + //<a class="btn btn-xs btn-primary" '+tooltip('Tambah Alat')+' ><i class="fa fa-plus"></i></a>
                                    '<input type="hidden" name="idinventaris"  value="'+idinventaris+'"/>' +
                                    '<input type="hidden" name="idbarangdetailsebelum" id="idbarangdetailsebelum"/>' +
                                    '<select  id="idbarang" onchange="pilihbarang(this.value)" name="idbarangdetail" class="select2 form-control"><option value="">-Pilih-</option></select>' + 
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<div class="col-md-12">' +
                                    '<label>Lokasi/Ruang </label>' + //<a class="btn btn-xs btn-primary" '+tooltip('Tambah Alat')+' ><i class="fa fa-plus"></i></a>
                                    '<select class="form-control select2" id="idlokasi" name="idruanginventaris"><option value="">-Pilih_</option></select>' +
                                '</div>'+
                            '</div>'+
                            ((idinventaris==0)?'': 
                            '<div class="form-group">'+
                                '<div class="col-md-12">' +
                                    '<label>Pindah Lokasi/Ruang </label>' + //<a class="btn btn-xs btn-primary" '+tooltip('Tambah Alat')+' ><i class="fa fa-plus"></i></a>
                                    '<select class="form-control select2" id="idpindahlokasi" name="idpindahruanginventaris"><option value="">-Pilih-</option></select>' +
                                '</div>'+
                            '</div>'
                            ) +
                            '<div class="col-xs-12" style="margin-top:10px;"></div>' +
                            '<div class="form-group">' +
                                '<div class="col-md-2">' +
                                    '<label>No.Seri</label>' +
                                    '<input type="text" id="noseri" placeholder="[otomatis]" class="form-control" value="" disabled />' +
                                '</div>'+
                                '<div class="col-md-2">' +
                                    '<label>No.Sertifikat</label>' +
                                    '<input type="text" id="nosertifikat" placeholder="[otomatis]" class="form-control" value="" disabled/>' +
                                '</div>'+
                                '<div class="col-md-2">' +
                                    '<label>Nilai Aset</label>' +
                                    '<input type="text" id="nilaiaset"  placeholder="[otomatis]" class="form-control" value="" disabled/>' +
                                '</div>'+
                                '<div class="col-md-2">' +
                                    '<label>Penyusutan</label>' +
                                    '<input type="text" id="penyusutan" placeholder="[otomatis]" class="form-control" value="" disabled/>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-xs-12" style="margin-top:10px;"></div>' +
                            '<div class="form-group">' +
                                
                                '<div class="col-md-2">' +
                                    '<label>Tanggal Pembelian</label>' +
                                    '<input type="text" id="tanggalpembelian"  class="form-control" placeholder="[otomatis]" value="" disabled/>' +
                                '</div>'+
                                '<div class="col-md-2">' +
                                    '<label>Tanggal Pengujian</label>' +
                                    '<input type="text" id="tanggalpengujian"  placeholder="[otomatis]" class="form-control" value="" disabled/>' +
                                '</div>'+
                                
                                '<div class="col-md-2">' +
                                    '<label>Jumlah</label>' +
                                    '<input type="text" id="jumlah" name="jumlah" placeholder="Jumlah" class="form-control" value="" '+((idinventaris==0)?'':'disabled')+'/>' +
                                '</div>'+
                                '<div class="col-md-2">' +
                                    '<label>Satuan</label>' +
                                    '<input type="text" id="satuan"  placeholder="[otomatis]" class="form-control" value="" disabled/>' +
                                '</div>'+
                                
                                '<div class="col-md-2">' +
                                    '<label>Status</label>' +
                                    '<select class="form-control" id="status" name="status"></select>' +
                                '</div>'+
                                
                            '</div>' + 
                            '<div class="col-xs-12" style="margin-top:10px;"></div>' +
                            '<div class="form-group">' +
                                '<div class="col-md-10">' +
                                    '<label>Keterangan</label>' +
                                    '<textarea class="form-control" rows="4" name="keterangan" id="keterangan"></textarea>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'lg',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="idbarangdetail"]').val()==''){
                        alert('Barang harap diisi.');
                        return false;
                    }else if($('select[name="idruanginventaris"]').val()==''){
                        alert('Ruang harap diisi.');
                        return false;
                    }else if( idinventaris==0 && $('input[name="jumlah"]').val()==''){
                        alert('Jumlah harap diisi.');
                        return false;
                    }else if($('select[name="status"]').val()==''){
                        $.alert('Staus harap diisi');
                        return false;
                    }else if($('select[name="status"]').val()=='pindah' && $('select[name="idpindahruanginventaris"]').val()==''){
                        $.alert('Pilih Lokasi terbaru');
                        return false;
                    }
                    else{
                        simpanaset();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            forminventarisready(idinventaris);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//status inventaris
function statusinventaris(status)
{
    var color = {
        baik:'',
        pindah:'background-color:#ffeb3b;',
        hilang:'background-color:#03a9f4;',
        rusak:'background-color:#ff1a1a;color:#fff;',
        recall:'background-color:#ff9800;'
    };
    return  color[""+status+""];
}
//onchange pindahlokasi
$(document).on('change','#idpindahlokasi',function(){
   $('#status').empty();
   $('#status').html('<option value="pindah">Pindah</option>');
});
//menampilkan data barang saat barang dipilih
function pilihbarang(value)
{
    var databarang = value.split(',');
    inputedit($('#tanggalpembelian'),databarang[5]);
    inputedit($('#penyusutan'),convertToRupiah(databarang[4]));
    inputedit($('#nilaiaset'),convertToRupiah(databarang[3]));
    inputedit($('#tanggalpengujian'),databarang[6]);
    inputedit($('#noseri'),databarang[1]);
    inputedit($('#nosertifikat'),databarang[2]);
    inputedit($('#satuan'),databarang[7]);
}

function inputedit(id,value)
{
    id.empty();
    id.val(value);
}
function forminventarisready(idinventaris)
{
    startLoading();
    $.ajax({ 
        url:  base_url+'cmasterdata/onreadyform_inventaris',type:"POST",dataType:"JSON",data:{idv:idinventaris},
        success: function(result){
            stopLoading();
            editdataenum($('#status'),result.status, ((result.dataedit==null)? '' : result.dataedit.status ) );
            //disable pilih ruang
            if(idinventaris!=0){$('#idlokasi').prop('disabled', true);}
            //tampilkan ruang yang dipilih
            editdataoption($('#idlokasi'),result.ruang, ((result.dataedit==null)? '' : result.dataedit.idruanginventaris ),'<option value=""> Pilih Ruang </option>');
            //pencarian ruang
            select2serachmulti($('#idlokasi'),'cmasterdata/fillinventarisruang');
            //disable pilih barang
            if(idinventaris!=0){$('#idbarang').prop('disabled', true);}
            //tampilkan barang yang dipilih
            editdataoptionbarang($('#idbarang'),result.barang, ((result.dataedit==null)? '' : result.dataedit.idbarangdetail ),'<option value=""> Pilih Alat/Barang </option>');
            //pencarian barang
            select2serachmulti($('#idbarang'),'cmasterdata/fillinventarisbarang');
            
            
            //pencarian pindah lokasi
            select2serachmulti($('#idpindahlokasi'),'cmasterdata/fillinventarisruang');
            
            editdataoption($('#idsatuan'),result.satuan, ((result.dataedit==null)? '' : result.dataedit.idsatuan ));
            if(result.dataedit!=null){
                inputedit($('#idbarangdetailsebelum'),result.dataedit.idbarangdetail);
                inputedit($('#tanggalpembelian'),result.dataedit.tanggalpembelian);
                inputedit($('#penyusutan'),convertToRupiah(result.dataedit.nilaipenyusutan));
                inputedit($('#nilaiaset'),convertToRupiah(result.dataedit.nilaiaset));
                inputedit($('#tanggalpengujian'),result.dataedit.tanggalpengujian);
                inputedit($('#noseri'),result.dataedit.nomorseri);
                inputedit($('#nosertifikat'),result.dataedit.nomorsertifikat);
                inputedit($('#keterangan'),result.dataedit.keterangan);
                inputedit($('#noinventaris'),result.dataedit.nomorinventaris);
                inputedit($('#satuan'),result.dataedit.namasatuan);
            }
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    });
}

function editdataoptionbarang(column,data,selected_data,addoption) // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{
    var select= addoption;
    column.empty();    
    for(var i in data)
    {
        var idbarang = data[i].id.split(',');select = select + '<option value="'+ data[i].id +'" '+ ((idbarang[0]==selected_data) ? 'selected' : '' ) +' >' + data[i].txt +'</option>';
    }
    column.html(select);
}

function simpanaset()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_inventaris',type:"POST",dataType:"JSON",data:$('#formInventaris').serialize(),
        success: function(r){
            notif(r.status,r.message);
            dtinventarisdetail.ajax.reload(null,false);
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
