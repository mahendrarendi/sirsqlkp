var dtmodel='';
$(function () {
    listdata();
})
function listdata()
{ 
  dtmodel = $('#dtmodel').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
      "ajax": {
          "data":{},
          "url": base_url+'cmasterdata/dt_modelinvalat',
          "type": "POST"
      },
      "columnDefs": [{ "targets": [3],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtmodel.state.clear();
  reloaddata();
});
function reloaddata(){dtmodel.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Model/Tipe Alat');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Model/Tipe Alat');});

function formtambahedit(idmodel,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formModel" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Merek Alat</label>' +
                                '<select name="idmerek" class="form-control select2" id="idmerek"></select>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Model/Tipe Alat</label>' +
                                '<input type="hidden" name="idmodel"  value="'+idmodel+'"/>' +
                                '<input type="text" name="modelbarang" class="form-control" value=""/>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="idmerek"]').val()==''){
                        $.alert('Merek Harap Diisi.');
                        return false;
                    }else if($('input[name="modelbarang"]').val()==''){
                        $.alert('Model Harap Diisi.');
                        return false;
                    }
                    else{
                        simpanmodel();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idmodel);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idmodel)
{
//    if(idruang!=''){
        $.ajax({ 
            url:  base_url+'cmasterdata/onreadyformmodelinvalat',
            type:"POST",
            dataType:"JSON",
            data:{i:idmodel},
            success: function(result){
                if(result.model!=null){
                    $('input[name="modelbarang"]').val(result.model.modelbarang);
                }
                editdataoption($('#idmerek'),result.merek, ((result.model==null) ? '' : result.model.idmerekbarang ) );
                
            },
            error: function(result){fungsiPesanGagal();return false;}
        });
//    }
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_modelinvalat',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpanmodel()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_modelinvalat',type:"POST",dataType:"JSON",data:$('#formModel').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}