var dtruang="";
function listdata(){
    dtruang=$("#dtruang").DataTable({
        processing:!0,
        serverSide:!0,
        lengthChange:!1,
        searching:!0,
        stateSave:!0,
        order:[],
        ajax:{
            data:{},
            url:base_url+"cmasterdata/dt_ruanginvalat",
            type:"POST"
        },
        columnDefs:[{targets:[3],orderable:!1}],
        fnCreatedRow:function(a,t,n){},
        drawCallback:function(a,t){
            $('[data-toggle="tooltip"]').tooltip()
        }
    })
}
        
function reloaddata(){dtruang.ajax.reload()}
function formtambahedit(a,t){
    var n=t,e='<form action="" id="formRuang" autocomplete="off"><div class="form-group"><label>Nama Blok</label><select name="idblok" class="form-control select2" id="idblok"></select></div><div class="form-group"><label>Kode Ruang</label><input type="hidden" name="idruang"  value="'+a+'"/><input type="text" name="koderuang" class="form-control" value=""/></div><div class="form-group"><label>Nama Ruang</label><input type="text" name="namaruang" class="form-control" value=""/></div></form>';
    $.confirm({
        title:n,
        content:e,
        columnClass:"s",
        closeIcon:!0,
        animation:"scale",
        type:"orange",
        buttons:{
            formSubmit:{
                text:"simpan",btnClass:"btn-blue",
                action:function(){
                    if(""==$('input[name="namaruangs"]').val())return $.alert("Nama Ruang Harap Diisi."),!1;simpanruang();
                }
            },
            formReset:{text:"batal",btnClass:"btn-danger"}
        },
        onContentReady:function(){
            formReady(a),$(".select2").select2();
            var t=this;
            this.$content.find("form").on("submit",function(a){a.preventDefault(),t.$$formSubmit.trigger("click")})
        }
    })
}

function formReady(a){
    $.ajax({
        url:base_url+"cmasterdata/onreadyformruanginvalat",
        type:"POST",
        dataType:"JSON",
        data:{i:a},
        success:function(a){
            null!=a.ruang&&($('input[name="koderuang"]').val(a.ruang.koderuang),$('input[name="namaruang"]').val(a.ruang.namaruanginventaris)),editdataoption($("#idblok"),a.blok,null==a.ruang?"":a.ruang.idblokinventaris)},error:function(a){return fungsiPesanGagal(),!1}})}function simpanruang(){$.ajax({url:base_url+"cmasterdata/save_ruanginvalat",type:"POST",dataType:"JSON",data:$("#formRuang").serialize(),success:function(a){notif(a.status,a.message),reloaddata()},error:function(a){return fungsiPesanGagal(),!1}})}

    $(function(){
        listdata()
    });

    $(document).on("click","#reload",function(){
        $('input[type="search"]').val("").keyup();
        dtruang.state.clear();
        reloaddata();
    });
    $(document).on("click","#add",function(){
        formtambahedit("","Tambah Lokasi");
    });
    $(document).on("click","#edit",function(){
        formtambahedit($(this).attr("alt"),"Ubah Ruang");
    });
    $(document).on("click","#delete",function(){
        var a=$(this).attr("alt");
        $.confirm({
            icon:"fa fa-question",
            theme:"modern",
            closeIcon:!0,
            animation:"scale",
            type:"orange",
            title:"Konfirmasi!",
            content:"Hapus Data?",
            buttons:{
                confirm:function(){
                    $.ajax({
                        url:base_url+"cmasterdata/hapus_ruanginvalat",
                        type:"post",
                        dataType:"json",
                        data:{a:a},
                        success:function(a){
                            notif(a.status,a.message),reloaddata()
                        },
                        error:function(a){
                            return notif(a.status,a.message),!1
                        }
                    })
                },cancel:function(){}
            }
        })
    });