var dtitemalat='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtitemalat = $('#dtitemalat').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
      "ajax": {
          "data":{},
          "url": base_url+'cmasterdata/dt_iteminvalat',
          "type": "POST"
      },
      "columnDefs": [{ "targets": [11],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', warningrekalibrasi(aData[7]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtitemalat.state.clear();
  dtitemalat.ajax.reload();
});
function reloaddata(){dtitemalat.ajax.reload(null,false);}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Barang/Alat');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Barang/Alat');});
function warningrekalibrasi(tanggalrekalibrasi)
{
    var aktif='';
    if(tanggalrekalibrasi=='0000-00-00' || tanggalrekalibrasi=='')
    {
        aktif = 1 ;
    }
    else
    {
        var newdate = new Date();
        var date = newdate.getFullYear()+'-'+ bulanindo(newdate.getMonth()+1) +'-'+tanggalindo(newdate.getDate());
            aktif = ((tanggalrekalibrasi <= date) ? 2 : 1 );
    }
    var color = {1:'',2:'background-color:#f44336;  color:#fff;'};
    return color[aktif];
}
function tanggalindo(date)
{
    if(date < 10)
    {
        var tanggal = {1:'01',2:'02',3:'03',4:'04',5:'05',6:'06',7:'07',8:'08',9:'09'};
        return tanggal[date];
    }
    else
    {
        return date;
    }
}
function bulanindo(month)
{
    if(month < 10)
    {
        var bulan = {1:'01',2:'02',3:'03',4:'04',5:'05',6:'06',7:'07',8:'08',9:'09'};
        return bulan[month];
    }
    else
    {
        return month;
    }
}
function formtambahedit(idbarangdetail,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formItemALat" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<input type="hidden" name="idbarangdetail" value="'+idbarangdetail+'" />'+
                                '<div class="col-md-6 col-xs-12">' +
                                    '<label>Barang/Alat</label>' +
                                    '<select name="idbaranginventaris" id="idbaranginventaris" class="form-control select2"></select>' +
                                '</div>' +
                                '<div class="col-md-6 col-xs-12">'+
                                    '<label>Model/Tipe Alat</label>' +
                                    '<select name="idmodelbarang" id="idmodelbarang" class="form-control select2"></select>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12" style="margin-top:8px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6 col-xs-12">' +
                                    '<label>No.Seri</label>' +
                                    '<input name="nomorseri" class="form-control" type="text"/>' +
                                '</div>' +
                                '<div class="col-md-6 col-xs-12">'+
                                    '<label>No.Sertifikat</label>' +
                                    '<input name="nomorsertifikat" class="form-control" type="text"/>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12" style="margin-top:8px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-6 col-xs-12">' +
                                    '<label>Jumlah</label>' +
                                    '<input name="jumlah" id="jumlah" class="form-control" type="text"/>' +
                                '</div>' +
                                '<div class="col-md-6 col-xs-12">'+
                                    '<label>Satuan</label>' +
                                    '<select name="idsatuan" id="idsatuan" class="form-control select2"></select>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12" style="margin-top:8px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-4 col-xs-12">' +
                                    '<label>Nilai Aset</label>' +
                                    '<input id="nilaiaset" name="nilaiaset" class="form-control" type="text"/>' +
                                '</div>' +
                                '<div class="col-md-4 col-xs-12">'+
                                    '<label>Nilai Penyusutan</label>' +
                                    '<input name="nilaipenyusutan" id="nilaipenyusutan" class="form-control" type="text"/>' +
                                '</div>'+
                                '<div class="col-md-4 col-xs-12">'+
                                    '<label>Batas Penyusutan</label>' +
                                    '<input id="bataspenyusutan" name="bataspenyusutan" class="form-control" type="text" readonly/>' +
                                '</div>'+
                            '</div>'+
                            '<div class="col-md-12" style="margin-top:8px;"></div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-4 col-xs-12">' +
                                    '<label>Tgl. Pembelian</label>' +
                                    '<input id="tglpembelian" name="tglpembelian" class="form-control" type="text" readonly/>' +
                                '</div>' +
                                '<div class="col-md-4 col-xs-12">'+
                                    '<label>Tgl. Pengujian</label>' +
                                    '<input id="tglpengujian" name="tglpengujian" class="form-control" type="text" readonly/>' +
                                '</div>'+
                                '<div class="col-md-4 col-xs-12">'+
                                    '<label>Tgl. Rekalibrasi</label>' +
                                    '<input id="tglrekalibrasi" name="tglrekalibrasi" class="form-control" type="text" readonly/>' +
                                '</div>'+
                            '</div>'+
                            
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="idbaranginventaris"]').val()==''){
                        alert('Barang/Alat Harap Diisi..');
                        return false;
                    }
                    else{
                        simpanitem();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idbarangdetail);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idbarangdetail)
{
    $.ajax({ 
        url:  base_url+'cmasterdata/onreadyformiteminvalat',type:"POST",dataType:"JSON",data:{i:idbarangdetail},
        success: function(result){
            
            if(result.detail!=null){
                $('input[name="nomorseri"]').val(result.detail.nomorseri);
                $('input[name="nomorsertifikat"]').val(result.detail.nomorsertifikat);
                $('input[name="jumlah"]').val(convertToRupiah( (result.detail.jumlah=='') ? 0 : result.detail.jumlah ) );
                $('input[name="nilaiaset"]').val( convertToRupiah( (result.detail.nilaiaset=='') ? 0 : result.detail.nilaiaset ) );
                $('input[name="nilaipenyusutan"]').val( convertToRupiah( (result.detail.nilaipenyusutan=='') ? 0 : result.detail.nilaipenyusutan ));
                //disable pilih barang/alat
                $('#idbaranginventaris').prop('disabled', true);
            }
            $('#tglpembelian').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "top"}).datepicker("setDate",((result.detail==null) ? '' : result.detail.tanggalpembelian ) );
            $('#tglpengujian').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "top"}).datepicker("setDate",((result.detail==null) ? '' : result.detail.tanggalpengujian ) );
            $('#tglrekalibrasi').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "top"}).datepicker("setDate",((result.detail==null) ? '' : result.detail.tanggalrekalibrasi ) );
            $('#bataspenyusutan').datepicker({autoclose: true,format: "yyyy",viewMode: "years",minViewMode: "years",orientation: "bottom",}).datepicker("setDate",((result.detail==null) ? '' : result.detail.bataspenyusutan ));
            editdataoption($('#idbaranginventaris'),result.barang, ((result.detail==null) ? '' : result.detail.idbaranginventaris ) );
            editdataoption($('#idmodelbarang'),result.model, ((result.detail==null) ? '' : result.detail.idmodelbarang ) );
            editdataoption($('#idsatuan'),result.satuan, ((result.detail==null) ? '' : result.detail.idsatuan ) );
        },
        error: function(result){fungsiPesanGagal();return false;}
    });   
}
$(document).on('keyup','#nilaiaset',function(){
    converToRupiahInput('nilaiaset');
})
$(document).on('keyup','#nilaipenyusutan',function(){
    converToRupiahInput('nilaipenyusutan');
})
$(document).on('keyup','#jumlah',function(){
    converToRupiahInput('jumlah');
})

$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_iteminvalat',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpanitem()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_iteminvalat',type:"POST",dataType:"JSON",data:$('#formItemALat').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
