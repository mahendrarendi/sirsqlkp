var dtmerek='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtmerek = $('#dtmerek').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cmasterdata/dt_merekinvalat',
     "type": "POST"
 },
 "columnDefs": [{ "targets": [2],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtmerek.state.clear();
  reloaddata();
});
function reloaddata(){dtmerek.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Merek');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Merek');});

function formtambahedit(idmerek,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formMerek" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Merek</label>' +
                                '<input type="hidden" name="idmerek"  value="'+idmerek+'"/>' +
                                '<input type="text" name="merek" class="form-control" value=""/>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="merek"]').val()==''){
                        $.alert('Merek Harap Diisi.');
                        return false;
                    }
                    else{
                        simpanmerek();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idmerek);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idmerek)
{
    if(idmerek!=''){
        $.ajax({ 
            url:  base_url+'cmasterdata/onreadyformmerekinvalat',type:"POST",dataType:"JSON",data:{i:idmerek},
            success: function(result){
                $('input[name="merek"]').val(result.merekbarang);
            },
            error: function(result){fungsiPesanGagal();return false;}
        });
    }
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_merekinvalat',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpanmerek()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_merekinvalat',type:"POST",dataType:"JSON",data:$('#formMerek').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
