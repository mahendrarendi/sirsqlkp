var dtkategori='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtkategori = $('#dtkategori').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : true,"stateSave": true,"order": [],
 "ajax": {"data":{},"url": base_url+'cmasterdata/dt_inv_barangkategori',"type": "POST"},"columnDefs": [{ "targets": [],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtkategori.state.clear();
  reloaddata();
});
function reloaddata(){dtkategori.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Kategori');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Kategori');});

function formtambahedit(idkategori,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formKategori" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Nama Kategori</label>' +
                                '<input type="hidden" name="idkategori"  value="'+idkategori+'"/>' +
                                '<input type="text" name="namakategori" class="form-control" value=""/>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="namakategori"]').val()==''){
                        $.alert('Kategori harap diisi.');
                        return false;
                    }
                    else{
                        simpankategori();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idkategori);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idkategori)
{
    if(idkategori!=''){
        $.ajax({ 
            url:  base_url+'cmasterdata/onreadyformkategori',type:"POST",dataType:"JSON",data:{i:idkategori},
            success: function(result){
                $('input[name="namakategori"]').val(result.kategori);
            },
            error: function(result){fungsiPesanGagal();return false;}
        });
    }
}
$(document).on("click","#delete", function(){ // DELETE USER
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_inv_barangkategori',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpankategori()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_inv_barangkategori',type:"POST",dataType:"JSON",data:$('#formKategori').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
