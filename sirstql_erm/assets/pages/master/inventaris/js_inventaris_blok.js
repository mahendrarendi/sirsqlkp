var dtblok='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtblok = $('#dtblok').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cmasterdata/dt_blokinvalat',
     "type": "POST"
 },"columnDefs": [{ "targets": [3],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtblok.state.clear();
  reloaddata();
});
function reloaddata(){dtblok.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Blok');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Blok');});

function formtambahedit(idblok,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formBlok" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Kode Blok</label>' +
                                '<input type="hidden" name="idblok"  value="'+idblok+'"/>' +
                                '<input type="text" name="kodeblok" class="form-control" value=""/>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Nama Blok</label>' +
                                '<input type="text" name="namablok" class="form-control" value=""/>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="kodeblok"]').val()=='')
                    {
                        $.alert('Kode Blok Harap Diisi.');
                        return false;
                    }
                    else if($('input[name="namablok"]').val()=='')
                    {
                        $.alert('Nama Blok Harap Diisi.');
                        return false;
                    }
                    else
                    {
                        simpanblok();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idblok);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idblok)
{
    if(idblok!=''){
        $.ajax({ 
            url:  base_url+'cmasterdata/onreadyformblokinvalat',type:"POST",dataType:"JSON",data:{i:idblok},
            success: function(result){
                $('input[name="kodeblok"]').val(result.kodeblok);
                $('input[name="namablok"]').val(result.namablok);
            },
            error: function(result){fungsiPesanGagal();return false;}
        });
    }
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_blokinvalat',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpanblok()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_blokinvalat',type:"POST",dataType:"JSON",data:$('#formBlok').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
