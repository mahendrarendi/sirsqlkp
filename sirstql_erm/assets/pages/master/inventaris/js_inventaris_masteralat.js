var dtmasteralat='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtmasteralat = $('#dtmasteralat').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
      "ajax": {
          "data":{},
          "url": base_url+'cmasterdata/dt_masterinvalat',
          "type": "POST"
      },
      "columnDefs": [{ "targets": [3],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtmasteralat.state.clear();
  reloaddata();
});
function reloaddata(){dtmasteralat.ajax.reload();}
$(document).on("click",'#add',function(){formtambahedit('','Tambah Alat/Barang');});
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Alat/Barang');});

function formtambahedit(idalat,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formbarang" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Kode Barang</label>' +
                                '<input type="hidden" name="idbarang"  value="'+idalat+'"/>' +
                                '<input type="text" name="kodebarang" class="form-control" value="" '+((idalat!='')?'readonly':'')+' />' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Nama Barang</label>' +
                                '<input type="text" name="namabarang" class="form-control" value=""/>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Jenis</label>' +
                                '<select class="form-control" name="jenis" id="jenis"></select>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Kategori</label>' +
                                '<select class="form-control select2" name="kategori" id="kategori"></select>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="kodebarang"]').val()==''){
                        $.alert('Kode Barang Harap Diisi.');
                        return false;
                    }else if($('input[name="namabarang"]').val()==''){
                        $.alert('Nama Barang Harap Diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idalat);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idalat)
{  
    $.ajax({ 
        url:base_url+'cmasterdata/onreadyformmasterinvalat',type:"POST",dataType:"JSON",data:{i:idalat},
        success: function(result){
            $('input[name="kodebarang"]').val( ((result.barang==null)? '' : result.barang.kodebaranginventaris ));
            $('input[name="namabarang"]').val( ((result.barang==null)? '' : result.barang.namabaranginventaris ));
            editdataenum($('#jenis'),result.jenis, ((result.barang==null)? '' : result.barang.jenisalat ) );
            editdataoption($('#kategori'),result.kategori, ((result.barang==null)? '' : result.barang.idbarangkategori ),'<option value=""> - Pilih - </option>');
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_masterinvalat',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        dtmasteralat.ajax.reload(null,false);
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpan()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_masterinvalat',type:"POST",dataType:"JSON",data:$('#formbarang').serialize(),
        success: function(r){
            notif(r.status,r.message);
            dtmasteralat.ajax.reload(null,false);
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
