var dtbarang='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dtbarang = $('#dtbarang').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : true,"stateSave": true,"order": [],
 "ajax": {"data":{},"url": base_url+'cmasterdata/dt_masterbarangpengeluaran',"type": "POST"},"columnDefs": [{ "targets": [3],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dtbarang.state.clear();
  reloaddata();
});
function reloaddata(){dtbarang.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Barang');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Barang');});

function formtambahedit(idbarang,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formBarang" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Nama Lokasi</label>' +
                                '<input type="hidden" name="idbarang"  value="'+idbarang+'"/>' +
                                '<input type="text" name="namabarang" class="form-control" value=""/>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Satuan</label>' +
                                '<select id="idsatuan" name="idsatuan" class="form-control select2"></select>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="namabarang"]').val()==''){
                        $.alert('Barang harap diisi.');
                        return false;
                    }else if($('select[name="idsatuan"]').val()==''){
                        $.alert('Satuan harap diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(idbarang);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(idbarang)
{
    $.ajax({ 
        url:  base_url+'cmasterdata/onreadyformmasterbarangpengeluaran',type:"POST",dataType:"JSON",data:{i:idbarang},
        success: function(result){
            editdataoption($('#idsatuan'),result.satuan, ((result.edit==null)? '' : result.edit.idsatuan ));
            if(result.edit!=null){$('input[name="namabarang"]').val(result.edit.namabarang);}
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_masterbarangpengeluaran',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpan()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_masterbarangpengeluaran',type:"POST",dataType:"JSON",data:$('#formBarang').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
