var dttable='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  dttable = $('#dttable').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : true,"stateSave": true,"order": [],
 "ajax": {"data":{},"url": base_url+'cmasterdata/dt_subkategoribiaya',"type": "POST"},"columnDefs": [{ "targets": [3],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dttable.state.clear();
  reloaddata();
});
function reloaddata(){dttable.ajax.reload();}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Sub Kategori Biaya');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Sub Kategori Biaya');});

function formtambahedit(id,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formInput" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<label>Kategori</label>' +
                                '<select id="idkategoribiaya" name="idkategoribiaya" class="form-control select2"></select>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Sub Kategori</label>' +
                                '<input type="hidden" name="idsubkategoribiaya"  value="'+id+'"/>' +
                                '<input type="text" name="subkategoribiaya" class="form-control" value=""/>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="idkategoribiaya"]').val()==''){
                        $.alert('Kategori harap diisi.');
                        return false;
                    }else if($('input[name="subkategoribiaya"]').val()==''){
                        $.alert('Sub Kategori harap diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formReady(id);
            $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function formReady(id)
{
    
        $.ajax({ 
            url:  base_url+'cmasterdata/onreadyform_subkategoribiaya',type:"POST",dataType:"JSON",data:{i:id},
            success: function(result){
                editdataoption($('#idkategoribiaya'),result.kategori, ((result.edit==null)? '' : result.edit.idkategoribiaya ));
                if(result.edit!=null){$('input[name="subkategoribiaya"]').val(result.edit.subkategoribiaya);}
            },
            error: function(result){fungsiPesanGagal();return false;}
        });
    
}
$(document).on("click","#delete", function(){
    var id = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_subkategoribiaya',
                    type : "post",      
                    dataType : "json",
                    data : {a:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
function simpan()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_subkategoribiaya',type:"POST",dataType:"JSON",data:$('#formInput').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
