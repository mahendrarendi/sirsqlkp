//dipake, ok
$(function(){
    diskonpembeli();
    $('.select2').select2();
    pembelianbebasRefreshBhp();//list bhp
    cariBhpFarmasi($('select[name="caribhp"]'));
    if (is_empty(localStorage.getItem('idbarangpembelibebas'))) {
        $('#inputbhp').hide();
        $('#simpan').hide();
    }  
});

$(document).on('change','#nama',function(){
   simpanpembelianbebas();
});

$(document).on('click','#kembali',function(){
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Yakin kembali ke halaman Kasir BHP/Farmasi Bebas?',
        buttons: {
            kembali: function () { 
                window.location.href=base_url+"cpelayanan/kasirpembelianbebas";
            },
            batal: function () {               
            }            
        }
    });
});

//dipake, ok
function cariBhpFarmasi(selectNameTag)//cari bhp
{
    startLoading();
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pembelianbebas_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
        stopLoading();
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(response) { 
            return { results: response,}; // display data
        },              
    }
    });
}

function setjenisdiskon(jenisdiskon)
{
    $.ajax({
        type: "POST",
        url:"pembelianbebas_updatejenisdiskon",
        data: {i:localStorage.getItem('idbarangpembelibebas'), jns:jenisdiskon},
        dataType: "JSON",
        success: function(result) {
             pembelianbebasRefreshBhp(false); 
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

function setpasienranap(value)
{
    $.ajax({
        type: "POST",
        url:"pembelianbebas_ispasienranap",
        data: {i:localStorage.getItem('idbarangpembelibebas'), ispasienranap:value},
        dataType: "JSON",
        success: function(result) {
            if(result.status=='danger')
            {
                $.alert(result.message);
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//dipake, ok
function inputBhpFarmasi(value)//input bhp
{
    {
        $.ajax({
            type:"POST",
            url:"pembelianbebas_inputbhp",
            data:{x:value, i:localStorage.getItem('idbarangpembelibebas'), jns:$('#jenisdiskon').val()},
            dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                $('select[name="caribhp"]').empty();
                $('select[name="caribhp"]').html('<option value="">Pilih</option>');
                if(result.status=='success')
                {
                    pembelianbebasRefreshBhp(); //refresh diagnosa
                }
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
    }
}

function cetakBhpAturanpakai(value='')//cetak aturan pakai
{
    var idb = $('#idbarangpembelianbebas'+value).val();
    $.ajax({
        dataType:"JSON",
        url:"pembelianbebas_cetak_aturanpakai",
        data:{idb:idb},
        type:"POST",
        success:function(result){
            cetakEtiket(result, value, true);
        },
        error:function(result)
        {
            fungsiPesanGagal();
            return false;
        }
    });
}

//dipake
function pembelianbebasRefreshBhp(reloadispegawai=true)//refreshDataBHP
{
    $.ajax({
        type: "POST",
        url: 'pembelianbebas_refreshbhp',
        data: {i:localStorage.getItem('idbarangpembelibebas')},
        dataType: "JSON",
        success: function(result) {
            var lisBhp = '', i=0, no=0, subtotal=0, total=0;jumlahambil=0;
            localStorage.setItem('jumlahitem', 0);
            //rb.kekuatan, b.kekuatan as kekuatanperiksa, rb.hargabeli, b.idaturanpakaipemakaian, b.idkemasan, rb.idbarang, b.idbarangpembelianbebas, 
            //b.jumlahpemakaian, b.harga, b.total, b.grup, b.jenisgrup, b.waktuinput, rb.namabarang, rb.jenis, catatan, penggunaan
            for (i in result)
            {
                total = result[i].total.toString();
                total = total.slice(0, -3);
                total = parseInt(total);
                jumlahambil = (result[i].kekuatanperiksa/result[i].kekuatan) * result[i].jumlahpemakaian;
                
                subtotal += parseInt(result[i].total);
                if (result[i].jenisgrup == 'racikan')
                {
                    lisBhp += '<tr id="listBhp'+ ++no +'" class="bg bg-gray"><td>'+ no +'</td>'+
                              '<td colspan="2"><input type="hidden" id="idbarangpembelianbebas'+no+'" size="5" class="form-control"  value="'+result[i].idbarangpembelianbebas+'">[ RACIKAN '+result[i].grup+' ]</td>' +
                              '<td><input type="text" onchange="editBhpJumlah('+no+')" id="jumlah'+no+'"size="5" class="form-control"  name="jumlah[]" value="'+result[i].jumlahpemakaian+'"></td>'+
                              '<td colspan="2" align="right" class="bg bg-aqua">Kemasan Racik</td>'+
                              '<td class="bg bg-aqua"><select class="select2 form-control" style="width:100%" onchange="editKemasan('+no+')" id="idkemasan'+no+'" name="idkemasan'+no+'"><option value="0">Pilih</option></select></td>'+
                              '<td><select class="select2 form-control" style="width:100%" onchange="editAturanpakai('+no+')" id="idaturanpakai'+no+'" name="idaturanpakai'+no+'"><option value="0">Pilih</option></select></td>'+
                              '<td><select class="select2 form-control" style="width:100%" onchange="editPenggunaan('+no+')" id="penggunaan'+no+'" name="penggunaan'+no+'"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select></td>'+
                              '<td></td>'+
                              '<td></td>'+
                              '<td></td>';  
                    isicombokemasan('idkemasan'+no, result[i].idkemasan);
                    isicomboaturanpakai('idaturanpakai'+no, result[i].idbarangaturanpakai);
                }
                else
                {
                    
                    localStorage.setItem('jumlahitem', localStorage.getItem('jumlahitem') + 1);
                    lisBhp += '<tr id="listBhp'+ ++no +'"><td>'+ no +'</td>'+
                              '<td '+((result[i].grup==0)?'colspan="2"':'')+'><input type="hidden" id="idbarangpembelianbebas'+no+'" size="5" class="form-control"  value="'+result[i].idbarangpembelianbebas+'">'+((result[i].grup==0)?"":"|-</td><td>")+result[i].namabarang+'</td>' +
                              '<td><input type="text" onchange="editBhpJumlah('+no+')" id="jumlah'+no+'"size="5" class="form-control"  name="jumlah[]" value="'+result[i].jumlahpemakaian+'"></td>'+
                              '<td>'+result[i].kekuatan+'</td>'+
                              '<td><input type="text" onchange="editBhpKekuatan('+no+')" id="kekuatan'+no+'" size="5" class="form-control" name="kekuatan[]" value="'+result[i].kekuatanperiksa+'"></td>'+
                              '<td><input type="text" onchange="editBhpGrup('+no+')" id="grup'+no+'" size="5" class="form-control"  name="grup[]" value="'+result[i].grup+'" /></td>'+
                              '<td>'+((result[i].grup==0)?'<select class="sel2 form-control" style="width:100%" onchange="editAturanpakai('+no+')" id="idaturanpakai'+no+'" name="idaturanpakai'+no+'"><option value="0">Pilih</option></select>':'')+'</td>'+
                              '<td>'+((result[i].grup==0)?'<select class="form-control" style="width:100%" onchange="editPenggunaan('+no+')" id="penggunaan'+no+'" name="penggunaan'+no+'"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select>':'')+'</td>'+
                              '<td><input type="text" onchange="editBhpCatatan('+no+')" id="catatanperobat'+no+'"size="5" class="form-control"  name="catatanperobat[]" value="'+result[i].catatan+'"></td>'+
                              '<td>'+jumlahambil+'</td>'+
                              '<td>'+convertToRupiah(total)+'</td>';  
                    isicomboaturanpakai('idaturanpakai'+no, result[i].idbarangaturanpakai);
                }
                lisBhp += '<td>';
                if (result[i].jenisgrup != 'komponen racikan')
                {
                    lisBhp += '<a onclick="cetakBhpAturanpakai('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a> ';
                }
                if (result[i].jenisgrup != 'racikan')
                {
                    lisBhp += '<a  onclick="hapusBhp('+no+')" data-toggle="tooltip" data-original-title="Hapus" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a> ';
                }
                
                //menu untuk input obat diberikan (unit ugd,farmasi)
                if(sessionStorage.idunitterpilih!='0')
                {
                    lisBhp += ' <a id="detailpemberian" idbarang="'+result[i].idbarang+'" jumlah="'+jumlahambil+'" idp="'+result[i].idbarangpembelianbebas+'" namaobat="'+result[i].namabarang +'" data-toggle="tooltip" data-original-title="Detail Pemberian" class="btn btn-primary btn-xs"><i class="fa fa-arrows-alt"></i></a>';
                } 
                    
                lisBhp += '</td></tr>';
                lisBhp += '<input type="hidden" id="idbarang'+no+'" size="5" class="form-control" value="'+result[i].idbarang+'">';//<td>'+result[i].jenis+'</td>
            }
            refreshPembeli();
            lisBhp +='<tr class="bg bg-info"><td colspan="10">Subtotal</td><td colspan="1" id="subtotal">'+convertToRupiah(subtotal)+'</td><td></td></tr>';
            $('#viewDataBhp').empty();
            $('#viewDataBhp').html(lisBhp);
           
            if (localStorage.getItem('jumlahitem') == 0)
            {
                $('#baru').show();
                // $('#barusimpan').show();
                $('#selesai').hide();
            }
            else
            {
                $('#baru').hide();
                // $('#barusimpan').hide();
                $('#selesai').show();
            }
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
            $('.sel2').select2();
            stopLoading();
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
//dipake
function refreshPembeli()
{
    $.ajax({
        type: "POST",
        url:"pembelianbebas_refreshpembeli",
        data: {i:localStorage.getItem('idbarangpembelibebas')},
        dataType: "JSON",
        success: function(result) {
            if(result!=null)
            {   
                diskonpembeli(result['jenisdiskon']);//reload label diskon
                $('#view_identitasPasien').html('No Nota: '+localStorage.getItem('idbarangpembelibebas')+'<br/>'+'Waktu: '+result['waktu']);
                var nama = result['namapembeli'];
                // jika pegawai
                $('#datapembeli').empty();
                $('#keterangan').val(result['keterangan']);
                if(result['jenisdiskon']=='diskon pegawai')//jika pembeli pegawai
                {   
                    // $('#labelispegawai').html('Pegawai');
                    $('#datapembeli').html('<label for="" class="col-xs-12 control-label" >Pilih Pegawai</label><div class="col-md-12"><select id="idpegawai" name="idpegawai" class="select2 form-control" onchange="ubahnama()"></select><br></div>');
                    $('select[name="idpegawai"]').html('<option value="'+ result['idpegawai'] +'">'+ result['namapembeli'] +'</option>')
                    $("select[name='idpegawai']").select2({
                    placeholder: "NIK | Nama",
                    allowClear: true,
                      ajax: {
                        url: base_url+"cmasterdata/caridatapegawai",type: "post",dataType: 'json',delay: 200,
                        data: function (params) {return {searchTerm: params.term};},
                        processResults: function (response) {return {results: response};},
                        cache: true
                      }
                     });
                    
                }
                else
                {
                    $('#datapembeli').html('<label for="" class="col-xs-12 control-label" >Nama Pembeli</label><div class="col-xs-12"><textarea class="form-control" id="nama" name="nama" rows="2">'+result['namapembeli']+'</textarea></div>');
                }

            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//dipake, ok
$(document).on("click","#baru", function(){
    ubahnama();
    baru();
});

//dipake, ok
$(document).on("click","#simpan", function(){
    simpanpembelianbebas();
});

function simpanpembelianbebas()
{
    var jenisdiskon = $('#jenisdiskon').val();
    if(jenisdiskon!='diskon pegawai'){
        if( $('#nama').val()==null || $('#nama').val()=='')
        {
            cekpembeli('Nama Pembeli Belum Diisi.!');
            return false;
        }
        else
        {
            ubahnama();
            refreshPembeli();
            $('#inputbhp').show();
        }

    }else{
        if( $('#idpegawai').val()==0 )
        {
            cekpembeli('Nama Pembeli Belum Dipilih.!');
            return false;
        }
        else
        {
            ubahnama();
            refreshPembeli();
            $('#inputbhp').show();
        }
    }
}

function cekpembeli(konten)
{
    $.confirm({
        icon: 'fa fa-warning',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Peringatan',content: konten,
        buttons: {
          ok: function(){}            
        }
    });
}
//dipake, ok
// cetak nota dan selesaikan transaksi
$(document).on("click","#selesai",function()
{
    var id = localStorage.getItem('idbarangpembelibebas');
    $.confirm({
        icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi',content: 'Selesai Input.?',
        buttons: {
          simpan: function(){
            $.ajax({
                type: "POST",url: base_url + 'cpelayanan/selesaipob',data:{i:id, status:'tagihan'},dataType: "JSON",
                success: function(result){
                    localStorage.clear();
                    window.location.reload(true);
                },
                error: function(result){fungsiPesanGagal();return false;}
              });
          },
          batal: function(){}            
        }
    });
});


//dipake, ok
function baru()
{
    $.ajax({
        type: "POST",
        url:"pembelianbebas_add",
        dataType: "JSON",
        success: function(result) {
            localStorage.clear();
            localStorage.setItem('idbarangpembelibebas', result['idbarangpembelibebas']);
            localStorage.setItem('jumlahitem', 0);
            window.location.reload(true);

        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//dipake, ok
function barusimpan()
{
    ubahnama(true);
    $.ajax({
        type: "POST",
        url:"pembelianbebas_add",
        dataType: "JSON",
        success: function(result) {
            localStorage.setItem('idbarangpembelibebas', result['idbarangpembelibebas']);
            localStorage.setItem('jumlahitem', 0);
            $('#inputbhp').hide();
            $('#viewDataBhp').empty();
            $('textarea[name="nama"]').val('');
            $('textarea[name="nama"]').focus();
            $('#baru').hide();
            $('#barusimpan').hide();
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//dipake, ok
function ubahnama(isreset = false)
{
    $.ajax({
        type: "POST",
        url:"pembelianbebas_updatenama",
        data: {
            i:localStorage.getItem('idbarangpembelibebas'), 
            n:$('textarea[name="nama"]').val(), 
            jns: $('#jenisdiskon').val(),
            idp:$('#idpegawai').val(),
            k:$('#keterangan').val(),
            ispasienranap:$('#ispasienranap').val()
        },
        dataType: "JSON",
        success: function(result) {
            $('textarea[name="nama"]').focus();
            if (isreset) $('textarea[name="nama"]').val('');
            $('input[name="labelid"]').val($('#idpegawai').val());
            $('input[name="labelname"]').val($('#idpegawai option:selected').text());
            notif(result.status, result.message);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

function simpanrencanabhp(no)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencanabhp',
        data: {t:$('input[name="tgl"]').val(), in:$('input[name="idinap"]').val(), ir:$('input[name="idbarangpembelianbebas"]').val(), jm:$('input[name="jumlahpemakaian"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            if(result.status=='success'){
                notif(result.status, result.message);                
                $('#tombolplot'+no).remove();
            }else{
                notif(result.status, result.message);
                return false;
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//dipake, ok
function isicomboaturanpakai(id, selected)
{
    if (is_empty(localStorage.getItem('dataAturanpakai')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_barangaturanpakai',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataAturanpakai', JSON.stringify(result));
                fillcomboaturanpakai(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcomboaturanpakai(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcomboaturanpakai(id, selected);
            },
            error: function(result){
                fillcomboaturanpakai(id, selected);
            }
        });
    }
}

//dipake, ok
function fillcomboaturanpakai(id, selected)
{
    var dataaturanpakai = JSON.parse(localStorage.getItem('dataAturanpakai'));
    var i=0;
    for (i in dataaturanpakai)
    {
      $("#"+id).append('<option ' + ((dataaturanpakai[i].idbarangaturanpakai===selected)?'selected':'') + ' value="'+dataaturanpakai[i].idbarangaturanpakai+'">'+dataaturanpakai[i].signa+'</option>');
    }
}

//dipake, ok
function isicombokemasan(id, selected)
{
    if (is_empty(localStorage.getItem('dataKemasan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_kemasan',
            type : "post",      
            dataType : "json",
            success: function(result) {
                localStorage.setItem('dataKemasan', JSON.stringify(result));
                fillcombokemasan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcomboaturanpakai(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombokemasan(id, selected);
            },
            error: function(result){
                fillcombokemasan(id, selected);
            }
        });
    }
}

//dipake, ok
function fillcombokemasan(id, selected)
{
    var datakemasan = JSON.parse(localStorage.getItem('dataKemasan'));
    var i=0;
    for (i in datakemasan)
    {
      $("#"+id).append('<option ' + ((datakemasan[i].idkemasan===selected)?'selected':'') + ' value="'+datakemasan[i].idkemasan+'">'+datakemasan[i].kemasan+'</option>');
    }
}

//dipake
function editKemasan(value)
{
    settingBhp($('#idkemasan'+value).val(),'kemasan',$('#idbarangpembelianbebas'+value).val(),null,null,'#idaturanpakai'+value);
}



//dipake, ok
function editAturanpakai(value)
{
    settingBhp($('#idaturanpakai'+value).val(),'aturanpakai',$('#idbarangpembelianbebas'+value).val(),null,null,'#penggunaan'+value);
}

function editPenggunaan(value)
{
    settingBhp($('#penggunaan'+value).val(),'penggunaan',$('#idbarangpembelianbebas'+value).val(),null,null,'#catatan'+value);
}

//basit, sementara dinonaktifkan
function editBhpGrup(value)//seting atau ubah grup bhp
{
    settingBhp($('#grup'+value).val(),'grup',$('#idbarangpembelianbebas'+value).val(),null,null,'#idaturanpakai'+value);
}

//dipake
function editBhpJumlah(value)//seting atau ubah grup bhp
{
    settingBhp($('#jumlah'+value).val(),'jumlah',$('#idbarangpembelianbebas'+value).val(),null,null,'#kekuatan'+value);
}

//dipake
function editBhpCatatan(value)//seting atau ubah grup bhp
{
    settingBhp($('#catatanperobat'+value).val(),'catatan',$('#idbarangpembelianbebas'+value).val(),null,null,null);
}

//dipake
function editBhpJumlahpesan(value)//seting atau ubah grup bhp
{
    settingBhp($('#jumlahpesan'+value).val(),'pesan',$('#idbarangpembelianbebas'+value).val(),null,null,'#jumlah'+value);
}

//dipake
function editBhpKekuatan(value)//seting atau ubah grup bhp
{
    settingBhp($('#kekuatan'+value).val(),'kekuatan',$('#idbarangpembelianbebas'+value).val(),null,null,'#grup'+value);
}

//dipake, ok
function settingBhp(d,field,x,y,no,nextfocus)
{
    $.ajax({ 
        url: 'pembelianbebas_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:field, x:x,y:y },
        success: function(result) {                                                                   
            if(result.status=='success'){
                // notif(result.status, result.message);
                pembelianbebasRefreshBhp();
                setTimeout(function(){
                    $(nextfocus).focus();
                },300);
                
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

function hapusBhp(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                var x = $('#idbarangpembelianbebas'+value).val();
                var d='', y='';
                settingBhp(d,'del',x,y);
                pembelianbebasRefreshBhp();
            },
            cancel: function () {               
            }            
        }
    });
}

function diskonpembeli(selected='')
{
    var jenis = ["tidak ada","diskon pegawai","diskon patner"];
    var opt   ='';
    for(x in jenis){ opt+='<option value="'+jenis[x]+'" '+((selected==jenis[x]) ? 'selected' : '') +' >'+jenis[x]+'</option>';}
    $('#jenisdiskon').empty();
    $('#jenisdiskon').html(opt);
}


$(document).on('click','#detailpemberian',function(){
    var idb      = $(this).attr('idp');
    var namaobat = $(this).attr('namaobat');
    var jumlah   = $(this).attr('jumlah');
    var idbarang = $(this).attr('idbarang');
    var modalSize   = 'lg';
    var modalTitle  = 'Detail Pemberian Obat';
    var modalContent= '<div class="col-xs-12">\n\
                        <table class="">\n\
                            <tr><th>Obat/BHP</th><td> : ' + namaobat + '</td></tr>\n\
                            <tr><th>Jumlah Diresepkan</th><td> : ' + jumlah + '</td></tr>\n\
                            <tr><th>Jumlah Diberikan</th><td> : <span id="detailpemberian_jumlahdiberikan"></span></td></tr>\n\
                        </table>\n\
                        <div id="detailpemberian_barangdiberikan"></div>\n\
                       </div>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'lg',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {    
            //menampilkan detail pemberian obat
            $.ajax({ 
                url: base_url+'cpelayanan/get_barangobatbebas_detail',
                type : "POST",      
                dataType : "JSON",
                data:{idb:idb,idbarang:idbarang},
                success: function(result){
//                    $('.select2').select2();
                    var dthtml = '<tr class="bg bg-yellow-gradient"><th>Batch.No</th><th>Kadaluarsa</th><th>Stok Unit</th><th>Diberikan</th></tr>';
                    for(var x in result)
                    {
                        dthtml += '<tr>\n\
                                    <input type="hidden" id="idbp_'+x+'" value="'+ idb +'"/>\n\
                                    <input type="hidden" id="idp_'+x+'" value="'+ result[x].idbarangpembelian +'"/>\n\
                                    <input type="hidden" name="index[]" value="'+x+'"/>\n\
                                    <td>'+result[x].batchno+'</td>\n\
                                    <td>'+result[x].kadaluarsa+'</td>\n\
                                    <td><input type="hidden" id="detailpemberian_stok'+x+'" value="'+result[x].stok+'" />'+result[x].stok+'</td>\n\
                                    <td><input onchange="cekjumlahstok(this.value,'+x+','+jumlah+')" id="detailpemberian_jumlah'+x+'" type="text" value="'+result[x].diberikan+'"/></td>\n\
                                   </tr>';
                    }
                    
                    //hitung diberikan
                    setTimeout(function(){ 
                        var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
                        var jumlah = 0;
                        for(var z in arrIndex)
                        {
                            jumlah += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
                        }
                        $('#detailpemberian_jumlahdiberikan').text(jumlah);
                    },1000);
                    
                    
                    $('#detailpemberian_barangdiberikan').html('<table class="table table-hover table-bordered table-striped" id="detailpemberian_barangdiberikan">'+dthtml+'</table>');
                },
                error: function(result){                  
                    notif(result.status, result.message);
                    return false;
                }
            });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});


function cekjumlahstok(jumlah,index,resep)
{
    var stok   = parseFloat($('#detailpemberian_stok'+index).val());
    var jumlah = parseFloat($('#detailpemberian_jumlah'+index).val());
    var diberikan  = jumlah;
    //totaldiberikan
    var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
    var totdiberikan = 0;
    
    for(var z in arrIndex)
    {
        totdiberikan += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
    }
    
    //set diberikan
    if( totdiberikan > resep){
        notif('danger', 'Jumlah diberikan tidak boleh melebihi resep dokter.!');
        $('#detailpemberian_jumlah'+index).val( 0 );
        return;
    }else{               
        diberikan  = jumlah;
        if(jumlah > stok)
        {
            notif('danger', 'Jumlah diberikan tidak boleh melebihi stok.!');
            diberikan  = ((stok > resep) ? resep : stok );
        }        
        
        var idbp = $('#idbp_'+index).val();
        var idp  = $('#idp_'+index).val();        
        startLoading();
        $.ajax({ 
            url: base_url+'cpelayanan/detailpemberianobatbebas',
            type : "post",      
            dataType : "json",
            data : {idbp:idbp,idp:idp,jumlah:diberikan},
            success: function(result) {                                                                   
                stopLoading();                
                $('#detailpemberian_jumlah'+index).val( diberikan );
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }

        }); 
    }
}