// 'use strict';
var dtpendaftaran='';
$(function(){
    // localStorage.clear();
    localStorage.removeItem("idpendaftaran"); //hapus localstorage idpendaftaran
    localStorage.removeItem("modependaftaran"); //hapus localstorage modependaftaran
    $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",((localStorage.getItem('tglawaldaftar'))?localStorage.getItem('tglawaldaftar'):'now')); //Initialize Date picker;
    $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",((localStorage.getItem('tglakhirdaftar'))?localStorage.getItem('tglakhirdaftar'):'now')); //Initialize Date picker;
    dt_pendaftaran();
});

$(document).on('change','#jenistampilan',function(){
    dtpendaftaran.ajax.reload(null,false);
});

function dt_pendaftaran()
{
    dtpendaftaran = $('#dtpendaftaran').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,        
        "paging":false,
        "info":false,
        "order": [],
        "ajax": {
            "data":{
                tglawal:function(){ return $('#tanggalawal').val()},
                tglakhir:function(){ return $('#tanggalakhir').val()},
                jenistampilan:function(){ return $('#jenistampilan').val()},
            },
            "url": base_url+'cadmission/dt_pendaftaran',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [5,6,7,8,9,10,11,12],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
         $(nRow).attr('style', qlstatuswarna(aData[18]));
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}

$(document).on('click','#tampildata',function(){
    dtpendaftaran.ajax.reload(null,false);
});

$(document).on('click','#reload',function(){
    $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker;
    $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker;
    $('input[type="search"]').val('').keyup();
    dtpendaftaran.state.clear();
    dtpendaftaran.ajax.reload();
});

function onchange_checkboxisrawatgabung()
{
    $('#israwatgabung').click(function(){
        if($(this).is(":checked")){//saat ceklis is rawat gabung dipilih
            $('#isirawatgabung').val('1');
            $('#listBed').empty();
        }
        else if($(this).is(":not(:checked)")){//saat ceklis is rawat gabung tidak dipilih
            $('#isirawatgabung').val('0');
            $('#listBed').empty();
        }
    });
}
function setLocalStorageTgl1(date){return localStorage.setItem('tglawaldaftar',date);}
function setLocalStorageTgl2(date){return localStorage.setItem('tglakhirdaftar',date);}

$(document).on('click','#cetakkartu',function(){
    var norm = $(this).attr('norm');
    var idp  = $(this).attr('idp');
    var namapasien = $(this).attr('namapasien');
    var alamat = $(this).attr('alamat');
    var tanggallahir = $(this).attr('tanggallahir');
    var nik = $(this).attr('nik');
    
    var konten = '<table class="table table-bordered table-striped">\n\
                    <tr class="bg bg-gray"><td colspan="2">DATA PASIEN</td></tr>\n\
                    <tr><td>NORM</td><td>'+norm +'</td></tr>\n\
                    <tr><td>Nama Pasien</td><td>'+namapasien +'</td></tr>\n\
                    <tr><td>NIK</td><td>'+nik +'</td></tr>\n\
                    <tr><td>Tanggal Lahir</td><td>'+tanggallahir +'</td></tr>\n\
                    <tr><td>Alamat</td><td>'+alamat +'</td></tr></table>'
    $.confirm({
        icon: 'fa fa-question',
//        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi Cetak Kartu',
        content: konten,
        buttons: {
            cetak: function () { 
                $.ajax({
                    url:base_url+"cadmission/settarif_cetakkartu",
                    type:"POST",
                    dataType:"JSON",
                    data:{norm:norm, idp:idp},
                    success:function(result){if(result.status=='success'){ buatQrPasien(norm);}else{notif(result.status, result.message);}},
                    error:function(result){ fungsiPesanGagal(); return false;}
                });            
            },
            batal: function () {               
            }            
        }
    });
});
function menubar_table()//tambah menu
{
    $("div.toolbar").html('<input onchange="setLocalStorageTgl1(this.value)" id="tanggalawal" style="margin-right:6px;" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal" ><input onchange="setLocalStorageTgl2(this.value)" id="tanggalakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggal" > <a class="btn btn-info btn-sm" onclick="search_by_date_range()"><i class="fa fa-desktop"></i> Tampil</a> <a href="'+base_url+'cadmission/add_pendaftaran_poli" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Entri Pendaftaran</a> <a style="margin-right: 6px;" onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a class="btn btn-info btn-sm" onclick="gantiDokterPraktek()"><i class="fa fa-stethoscope"></i> Dokter Pengganti</a>');
    $("div.toolbar").css('padding-top','3px');
    
//    button_pane();//panggil fungsi button pane
}
function refresh_page()
{
    // table.state.clear();
    $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now' );
    $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now' );
    window.location.reload(true);
}

function changeToProses(no,idpendaftaran,status)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status berkas rekam medis.?',
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/ubahstatus_arusrekamedis",
                    type:"POST",
                    dataType:"JSON",
                    data:{status:status,id:idpendaftaran},
                    success:function(result){
                    notif(result.status, result.message);//panggil fungsi popover message
                    },
                    error:function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    });
}

/// -- cetak gelang
function cetak_gelang(id)
{
  var style = '<style rel="stylesheet" media="print"> @media print { div{ background-color:#000; } } </style>';
  $.ajax({
        type: "POST",
        url: "cetak_gelang",
        data:{id},
        dataType: "JSON",
        success: function(result) {
            var printContents = '<div><label>Nama Pasien</label> : '+result[0].namalengkap+'<br>'+
                       '<label>No.RM</label> : '+result[0].norm+'<br>'+
                       '</div>';
            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}
// Cetak Label Pasien Ranap
function cetak_label(id)
{
  var style = '<style>@page{size:15.55cm 19cm; margin:0px; font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;}</style>';
  $.ajax({
        type: "POST",
        url: "cetak_gelang",
        data:{id},
        dataType: "JSON",
        success: function(result) {
            var label='<div style="font-size:11px;text-align:center;padding-top:5.5px;border-radius:4px;display:inline-block;height:1.6cm; width:5cm;border:0.01px solid #f0f0f0;margin:-4px 12px 8px -3px;">'+result[0].rmttl+'<br>'+result[0].namalengkap+'<br>'+result[0].nik+'</div>';
            var printContents = '', down=0, right=0;
            for(down=0; down<10;down++)
            {
                for(right=0; right<3; right++)
                {
                    printContents += label;
                }
                printContents+='<br/>';
            }

            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}
/// -- cetak lembar rm
function cetak_lembarrm(id)
{
  var split='&nbsp;&nbsp;&nbsp;';
  var style = '<style rel="stylesheet" media="print"> @media print { .borderleft{border-left:1px solid #2D2D2D;border-top:1px solid #2D2D2D;} .border{border-right:1px solid #2D2D2D; border-left:1px solid #2D2D2D; border-top:1px solid #2D2D2D; padding:4px; margin:0;} .jarak{padding-top:8px;}  } </style>';
  $.ajax({
        type: "POST",
        url: "cetak_lembarrm",
        data:{id:id},
        dataType: "JSON",
        success: function(result) {
        	var pasien = result.pasien;
        	var ortu = result.ortu;
            var printContents = '<div style="padding:8px;">'+
            '<img src="'+base_url+'/assets/images/headerlembarrm.svg" >'+
            '<hr><span style="font-size:30px;">Lembar Rekam Medik &nbsp;&nbsp;&nbsp;&nbsp;  No. RM.:'+id+'</span><hr>'+
            '<table>'+
            '<tr><td>NAMA LENGKAP '+ split +'</td><td>'+ pasien.namalengkap +'</td></tr>'+
            '<tr><td>JENIS KELAMIN '+ split +'</td><td>'+ pasien.jeniskelamin  +'</td></tr>'+
            '<tr><td>TGL LAHIR '+ split +'</td><td>'+ pasien.tanggallahir  +'</td></tr>'+
            '<tr><td>AGAMA '+ split +'</td><td>'+ pasien.agama  +'</td></tr>'+
            '<tr><td>PEKERJAAN '+ split +'</td><td>'+ pasien.namapekerjaan+'</td></tr>'+
            '<tr><td>ALAMAT '+ split +'</td><td>'+ pasien.alamat +'</td></tr>'+
            '<tr><td>KELURAHAN '+ split +'</td><td>'+  pasien.namadesakelurahan +'</td></tr>'+
            '<tr><td>NO. TELP '+ split +'</td><td>'+ pasien.notelpon +'</td></tr>'+
            '<tr><td>GOL DARAH '+ split +'</td><td>'+ pasien.golongandarah +'</td></tr>'+
            '<tr><td>PENDIDIKAN '+ split +'</td><td>'+ pasien.namapendidikan +'</td></tr>'+
            '<tr><td>PEKERJAAN '+ split +'</td><td>'+ pasien.namapekerjaan +'</td></tr>'+
            '<tr><td>STATUS KAWIN '+ split +'</td><td>'+ pasien.statusmenikah +'</td></tr>'+
            '<tr><td colspan="2">&nbsp;</td></tr>'+
            '<tr><td colspan="2">Data Penanggung</td></tr>'+
            '</table><table cellspacing="0"><tr><td class="borderleft">Nama</td><td class="borderleft">Hubungan</td><td class="borderleft">Telpon</td><td class="border">Alamat</td></tr>';
            for(var p in result.penanggung){ printContents+= '<tr><td class="borderleft">'+result.penanggung[p].namalengkap+'</td><td class="borderleft">'+result.penanggung[p].namahubungan+'</td><td class="borderleft">'+result.penanggung[p].notelpon+'</td><td class="border">'+result.penanggung[p].alamat+'</td></tr>'; }
            printContents +=
            '<table width="100%" style="margin-top:5px;">'+
            '<hr>'+
            '<tr style="text-align:center;">'+
            '<td>Tanggal & Jam</td><td>Anamnesis & Pemeriksaan</td><td>Diagnosis & Terapi</td><td>ICD</td><td>DOKTER</td>'+
            '</tr>'+
            '<tr style="height:420px;">'+
            '<td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="border">&nbsp;</td>'+
            '</tr>'+
            '<table>'+
            '</div>';
            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}

function buka_pelayanan(x,y) // buka pemeriksaan
{
    var id = x;var nobaris = y;
    $.confirm({
        icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Confirmation!',content: 'Buka Pelayanan?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'buka_pelayanan',
                    type : "post",      
                    dataType : "json",
                    data : { i:id },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            // tablependaftaran.ajax.reload();
                            pendaftaranListRow(id,nobaris);//get data auto load beri antrian
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
function hapus_data_pendaftaran(x,y){ //hapus atau batal pelayanan/pemerirksan
    var id = x;var nobaris = y;
    $.confirm({
        icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Confirmation!',content: 'Delete data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'hapus_data_pendaftaran',
                    type : "post",      
                    dataType : "json",
                    data : { i:id },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            // tablependaftaran.ajax.reload();
                             pendaftaranListRow(id,nobaris);//get data auto load beri antrian
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

// -- pindah rawat inap
function info_sep(){ $.alert("Input No SEP untuk bisa dipindahkan ke rawat Inap, untuk pasien umum/mandiri No SEP tidak di input..!");}
// -- list bed
function ranap_listbangsal(norm,idpendaftaran,nobaris)
{ 
    $.ajax({ 
        url: base_url+'cadmission/ranap_listbangsal',type : "post",dataType : "json",data: {a:norm, p:idpendaftaran},
        success: function(result) {
            var dthtml='', dthtmlbangsal='', dtkelasrawat='<option value="0">Kelas Perawatan</option>',dtkelasjamin='<option value="jaminan">Kelas Jaminan</option>', dtdokter='<option value="0">Dokter DPJP</option>';
            $('#ranap_carabayar').val(result.pasien['carabayar']);// list cara bayar
            $('#pindahranap_identitasPasien').html('<b>No.RM:</b> '+result.pasien['norm']+' ,<b> No.JKN:</b>  '+result.pasien['nojkn']+',<b> Penjamin:</b>  '+result.pasien['carabayar']+' ,<b> NIK:</b>  '+result.pasien['nik']+', <b>Nama:</b>  '+result.pasien['namalengkap']+', <b> TanggalLahir:</b> '+result.pasien['tanggallahir']+', <b> Alamat:</b> '+result.pasien['alamat']+' [<b style="color:#000;">'+ ((result.pasien['norm']<'10083900') ? 'Pasien Lama' : result.pasien['ispasienlama'] ) +'</b>]');// list identitas pasien
            // dokter
            for(var b in result.dokter){dtdokter+='<option value='+result.dokter[b].idpegawai+'>'+result.dokter[b].namalengkap+'</option>'}
            $('#ranap_dokterdpjp').empty();
            $('#ranap_dokterdpjp').html(dtdokter);
            // kelas perawatan
            for ( var a in result.kelas){dtkelasrawat+='<option value='+result.kelas[a].idkelas+'>'+result.kelas[a].kelas+'</option>';}
            $('#ranap_kelasperawatan').empty();
            $('#ranap_kelasperawatan').html(dtkelasrawat);
            // kelas jaminan
            if (result.pasien['carabayar'] == 'mandiri')
            {
                dtkelasjamin+='<option value="0">Mandiri</option>';
            }
            else
            {
                for ( var a in result.kelas)
                {
                    ((result.kelas[a].kelas=='Kelas VIP' || result.kelas[a].kelas=='Mandiri')?dtkelasjamin+='':dtkelasjamin+='<option value='+result.kelas[a].idkelas+'>'+result.kelas[a].kelas+'</option>');
                }
            }
            $('#ranap_kelasjaminan').empty();$('#ranap_kelasjaminan').html(dtkelasjamin);
            // bed
            for( var i in result.statusbed){dthtml += '<td style="padding:10px;"><img src="'+base_url+'assets/images/'+ result.statusbed[i].file +'" width="45"/><br> '+ result.statusbed[i].statusbed +'</td> &nbsp;';}
            $('#listStatusBangsal').empty(); $('#listStatusBangsal').html(dthtml);
            // tampil bangsal
            for( var x in result.bangsal){dthtmlbangsal += '<a class="btn btn-primary btn-sm" id="ranaplistbed" nobaris="'+nobaris+'" idbangsal="'+result.bangsal[x].idbangsal+'" daftar="'+idpendaftaran+'" bangsal="'+result.bangsal[x].namabangsal+' '+result.bangsal[x].kelas+'" cursor:pointer;">'+result.bangsal[x].namabangsal+'</a>&nbsp;';}
            $('#listBangsal').empty(); $('#listBangsal').html(dthtmlbangsal); $('.select2').select2();
            return;
        },
        error: function(result){         
            fungsiPesanGagal();
            return false;
        }
    });

}
// list bed
$(document).on("click","#ranaplistbed", function(){
    startLoading();
    var value = $(this).attr("idbangsal");
    var idpendaftaran = $(this).attr("daftar");
    var bangsal = $(this).attr("bangsal");
    var cekrawatgabung = $('#isirawatgabung').val();
    localStorage.removeItem('bangsal');
    localStorage.setItem('bangsal',bangsal);
    var nobaris = $(this).attr("nobaris");
    $('#judul_listbed').empty();
    $('#judul_listbed').text('List Bed '+bangsal);
    $.ajax({ 
        url: base_url+'cadmission/ranap_listbed',
        type : "post",      
        dataType : "json",
        data:{id:value},
        success: function(result) {
            stopLoading();
            var dtinfo = '';
            for( var x in result.info){dtinfo += ' nomor rekam medis '+ result.info[x].norm+' - a/n pasien '+result.info[x].namalengkap+result.info[x].bed+'<br>';}
            $('#infobangsal').empty();
            $('#infobangsal').html(dtinfo);
            if(dtinfo!=''){$('#infobangsal').css("padding","8 px");}
            var dthtml='<tr>', no=0;
            for(var i in result.bangsal)
            {
               var allowmenu = '';
               var dt_tr='';  ++no; if(no>7){no=0;dt_tr='</tr><tr>';}
               // cekrawatgabung
               if(result.bangsal[i].statusbed=='Bed Kosong' && cekrawatgabung!=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else if(result.bangsal[i].statusbed=='Bed Wanita' && cekrawatgabung=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else if(result.bangsal[i].statusbed=='Bed Laki-laki' && cekrawatgabung=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else if(result.bangsal[i].statusbed=='Bed Dipesan' && cekrawatgabung=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else
               {
                allowmenu = 'style="cursor:not-allowed;" ';
               }
               dthtml += '<td style="text-align:center; padding:4px;"><a '+ allowmenu + '><img style="border-radius:0px; width:120px;height:120px;" src="'+base_url+'assets/images/'+result.bangsal[i].file+'" alt="'+result.bangsal[i].statusbed+'"><span class="users-list-name"  style="font-size:30px;">'+result.bangsal[i].nobed+'</span></a></td>'+dt_tr;
            }
            $('#listBed').empty();
            $('#listBed').html(dthtml+'</tr>');
        },
        error: function(result){          
            fungsiPesanGagal();
            return false;
        }
    });
    
});
//  -- rawat inap pilih bed
function ranap_pilihbed(idbangsal,idbed,idpendaftaran,nobaris,nobed)
{
    
    if($('#nosep').val()=='' && $('#ranap_carabayar').val()!=='mandiri')
    {
        alert_empty('nosep');
    }
    else if($('#ranap_dokterdpjp').val()=='0')
    {
        alert_empty('dokter dpjp');
    }
    else if($('#ranap_kelasperawatan').val()=='0')
    {
        alert_empty('kelas perawatan');
    }
    else if($('#ranap_kelasjaminan').val()=='jaminan')
    {
        alert_empty('kelas jaminan');
    }
    else
    {
        $.confirm({
            icon: 'fa fa-question',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Konfirmasi!',
            content: 'Pastikan bed yang dipilih sudah sesuai...!<br> <b>Bed '+localStorage.getItem('bangsal')+' nomor '+nobed+'</b>',
            buttons: {
                confirm: function () { 
                startLoading();
                $.ajax({ 
                    url: 'pendaftaranranap_pilihbed',
                    type : "post",      
                    dataType : "json",
                    data : {israwatgabung:$('#isirawatgabung').val(), bangsal:idbangsal, bed:idbed, nosep:$('#nosep').val(), daftar:idpendaftaran,kelasperawatan:$('#ranap_kelasperawatan').val(),kelasjaminan:$('#ranap_kelasjaminan').val(),dokter:$('#ranap_dokterdpjp').val() },
                    success: function(result) {
                        stopLoading();
                        $('.jconfirm').hide();
                        notif(result.status, result.message);
                        // tablependaftaran.ajax.reload();
                         pendaftaranListRow(idpendaftaran,nobaris);//get data auto load beri antrian

                    },
                    error: function(result){          
                        notif(result.status, result.message);
                        return false;
                    }
                    
                });
            },
                cancel: function () {               
                }            
            }
        });
    }
}
// -- pindah rawat inap
function pindahRanap(value,idpendaftaran,nobaris)
{
    startLoading();
    var mSize='lg'
    var mTitle=' Pindah Rawat Inap';
    var mContent='';
    var menusave='simpan';
    var menubatal='batal';
    var url= base_url+'cadmission/pendaftaran_pindahranap';
    var data = $("#formPindahRanap").serialize();
    
    mContent +='<form action="" id="formPindahRanap">'+
    '<input type="hidden" name="norm" value="'+ value +'" />'+
    '<input type="hidden" name="idpendaftaran" value="'+ idpendaftaran +'" />'+

    '<div class="pad margin ">'+
      '<div class="callout callout-warning" style="margin-bottom: 0!important;">'+
        '<h4><i class="fa fa-default"></i> Identitas Pasien:</h4>'+
        '<div id="pindahranap_identitasPasien"></div>'+
      '</div>'+
    '</div>'+

    '<div class="col-sm-6" style="margin:none;">'+
        '<div class="box no-border" style="margin:none;">'+
                '<div class="box-body">'+
                '<div class="chart">'+
                    '<table border="1"><tbody style="display:block;overflow:auto;"><tr id="listStatusBangsal"></tr></tfoot></table>'+
                '</div>'+
            '</div>'+
        '</div>'+    
    '</div>'+

    '<div class="col-sm-4">'+
      '<div class="input-group">'+
        '<input type="text" id="nosep" class="form-control" placeholder="No.SEP">'+
        '<input type="hidden" id="ranap_carabayar"/>'+
        '<div class="input-group-addon" style="padding:4px;">'+
          '<a class="btn btn-xs btn-info" onclick="info_sep()"><i class="fa fa-question-circle"></i></a>'+
        '</div>'+
      '</div>'+
      
      '<div class="form-group" style="margin-top:4px;">'+
        '<select id="ranap_dokterdpjp" class="form-control select2"><option>Dokter DPJP</option></select>'+
        '<select id="ranap_kelasperawatan" class="form-control" style="margin-top:4px;"><option>Kelas Perawatan</option></select>'+
        '<select id="ranap_kelasjaminan" class="form-control" style="margin-top:4px;"><option>Kelas Jaminan</option></select>'+
      '</div>'+
    '</div>'+

    '<div class="col-sm-2">'+
        '<input type="checkbox" id="israwatgabung" class="flat-red"> Pasien Rawat Gabung'+
        '<input type="hidden" id="isirawatgabung"/>'+
    '</div>'+


    '<div class="col-sm-12" style="margin:none;">'+
    '<h4>Pilih Bangsal :</h4>'+
        '<table><tbody style="display:block;overflow:auto;"><tr id="listBangsal"></tr></tfoot></table>'+
        '</div>'+  

    '<div class="col-md-12">'+
        '<style type="text/css">.fixed_header tbody{display:block;overflow:auto;height:200px;width:100%.fixed_header thead tr{display:block}</style>'+
        '<div class="box no-border">'+
            '<div class="with-border">'+
                '<h3 class="box-title" id="judul_listbed"> List Bed</h3>'+
                '</div>'+
                '<div class="box-body">'+
                '<div class="lablel label-warning" id="infobangsal"></div>'+
                '<div class="chart">'+
                '<table" id="listBed"></table>'+
                '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+    
    '</div>'+
'</div>'+
'<div class="col-sm-12" style="margin:4px 0px;"></div>';
$.confirm({
    title: mTitle,
    content: mContent,
    columnClass: mSize,
    autoRefresh:true,
    buttons: {
        formReset:{
            text: menubatal,
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    onchange_checkboxisrawatgabung();
    stopLoading();
    ranap_listbangsal(value,idpendaftaran,nobaris);
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
});
}
//// --- generate no antrian
//dipanggil dari a id pemeriksaanklinikBeriantrian - di dalam fungsi tampilkanantrian()
$(document).on("click","#pemeriksaanklinikBeriantrian", function(){
    var idp = $(this).attr("alt");
    var nobaris = $(this).attr("nobaris");
    var idu = $(this).attr("alt2");
    var idpr = $(this).attr("alt3");
    var id = $(this).attr("alt4");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Beri Antrian?',
        buttons: {
            confirm: function () { 
            // setTimeout(function(){
            beriAntrian(idp,idu,idpr,id,nobaris);           
            dtpendaftaran.ajax.reload(null,false);
        },
            cancel: function () {               
            }            
        }
    });
});
function gantiDokterPraktek()
{
    startLoading();
    var mTitle=' Ganti Dokter Praktek';
    var mContent='';
    var url= base_url+'cadmission/gantiDokterPraktek';
    var data = $("#form").serialize();
    
    mContent +='<form action="" id="form">'+
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
        '<label>Tanggal</label>'+
        '<input id="tanggalgantidokter" class="form-control" onchange="cari_poliklinik(this.value)" >'+
      '</div>'+
    '</div>'+
    
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
        '<label>Jadwal Dokter</label>'+
        '<select id="poliklinik" name="jadwaldokter" class="form-control select2"><option>Pilih Jadwal</option></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
      '<label>Dokter Pengganti</label><br>'+
      '<input type="radio" name="statusdokter" value="dokterdalam" checked /> '+
      '<span>Dokter Dalam</span> '+
      ' <input type="radio" name="statusdokter" value="dokterluar" /> '+
      '<span>Dokter Luar</span>'+
      '<select id="pilihdokterpengganti" name="dokterpengganti" class="form-control select2"><option>Pilih Dokter</option></select>'+
      '</div>'+
    '</div></form>';
$.confirm({
    title: mTitle,content: mContent,columnClass: 'small',autoRefresh:true,
    buttons: {
        formSubmit:{
            text:'Simpan',
            btnClass: 'btn-primary',
            action: function (){
                    
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url +'cadmission/gantiDokterPraktek', //alamat controller yang dituju (di js base url otomatis)
                    data: $("#form").serialize(), //
                    dataType: "JSON", //tipe data yang dikirim
                    //jika  berhasil
                    success: function(result) {
                        notif(result.status, result.message);
                    },
                    //jika error
                    error: function(result) {
                        console.log(result.responseText);
                    }
                });
            }
            
        },
        formReset:{
            text:'Batal',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    stopLoading();
    //panggil dokter
    $('#tanggalgantidokter').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    tampildokter();
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
   });
}

function tampildokter()
{
    $.ajax({
        type: "POST",url: base_url +'cadmission/tampildokter',data: $("#form").serialize(),dataType: "JSON",
        success: function(result) { //jika  berhasil
            var html='<option value="0">Pilih</option>';
            for(var x in result){html+='<option value="'+result[x].idpegawai+'">'+result[x].namadokter+'</option>';}
            $('#pilihdokterpengganti').empty(); $('#pilihdokterpengganti').html(html);
        },
        error: function(result) { //jika error
            fungsiPesanGagal();return false;
        }
    });
}

function pendaftaranListRow(id,nobaris)
{
    search_by_date_range();
}

$(document).on('click','#bookingjadwaloperasi',function(){
    var baris = $(this).attr('nobar');
    var mContent='<form action="" id="formbookingjadwalop">'+

    '<div class="col-xs-12">'+
      '<div class="bg-info"><p class="small">Perhatikan Waktu Rencana Operasi,<br> Bed tidak bisa dipilih jika pada waktu yang direncanakan sudah dipesan.</p></div>'+
    '</div>'+
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Waktu Rencana Operasi (Y-M-D H:m)</label>'+
        '<input type="hidden" name="idp" value="'+$(this).attr('idp')+'" />'+
        '<input id="setwaktuoperasi" class="form-control datetime" type="text" name="waktuoperasi"/>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Bangsal</label>'+
        '<select id="idbangsalop" onchange="caribedoperasi(this.value)" name="idbangsal" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Bed/Ruang</label>'+
        '<select id="idbedop" name="idbed" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Jenis Tindakan</label>'+
        '<select id="idjenistindakanop" name="idjenistindakan" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '</form>';
$.confirm({
    title: 'Booking Jadwal Operasi',content: mContent,closeIcon: true,closeIconClass: 'fa fa-close',columnClass: 'col-xs-offset-2 col-xs-8',autoRefresh:true,
    buttons: {
        formSubmit:{
            text:'Booking',
            btnClass: 'btn-primary',
            action: function (){
                if($('select[name="idbangsal"]').val()=='0'){
                    alert_empty('Bangsal belum dipilih.!');
                    return false;
                }else if($('select[name="idbed"]').val()=='0'){
                    alert_empty('Bed belum dipilih.!');
                    return false;
                }else if($('select[name="idjenistindakan"]').val()=='0'){
                    alert_empty('Jenis tindakan belum dipilih.!');
                    return false;
                }else{
                    startLoading();
                    $.ajax({type: "POST",url: base_url +'cadmission/bookingjadwaloperasi',data: $("#formbookingjadwalop").serialize(), dataType: "JSON",
                    success: function(result){/*jika  berhasil*/
                        stopLoading();
                        (result.status=='success') ? $('.nobar'+baris).addClass('hide') : '';
                        notif(result.status, result.message);
                    },
                    //jika error
                    error: function(result) {
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
                }
            }
            
        },
        formReset:{text:'Batal',btnClass: 'btn-danger'}
    },
    onContentReady: function () {
    $('input[name="waktuoperasi"]').datetimepicker({format:"YYYY-MM-DD HH:mm", sideBySide: true,defaultDate:new Date()});
    get_databangsal($('#idbangsalop'),'','bangsaloperasi');
    $.ajax({type: "POST",url: base_url +'cadmission/carijenistindakanoperasi',dataType: "JSON",
        success: function(result) {
            var opt='<option value="0">Pilih</option>';
            for(x in result){opt+='<option value="'+result[x].idjenistindakan+'">'+result[x].jenistindakan+'</option>';}
            $('#idjenistindakanop').empty(); $('#idjenistindakanop').html(opt);
        },
        error: function(result) {
            fungsiPesanGagal(); return false;
        }
    });
    
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
   });
});
function caribedoperasi(idbangsal)
{
    if(idbangsal==0){
        alert_empty('Bangsal Harap Dipilih');
    }else{
        $.ajax({ type: "POST", url: base_url +'cadmission/caribedoperasi', data:{idbangsal:idbangsal,waktu:$('#setwaktuoperasi').val()}, dataType: "JSON", //tipe data yang dikirim
            success: function(result) {
                var opt='<option value="0">Pilih</option>';
                for(x in result){opt+='<option '+((result[x].kodebooking==null) ? '' : 'disabled') +' value="'+result[x].idbed+'">'+result[x].bed+ ((result[x].kodebooking==null) ? '' : ' - bed sudah dipesan, mohon ubah waktu rencana operasi untuk bisa membuat jadwal.!') +'</option>';}
                $('#idbedop').empty(); $('#idbedop').html(opt);
            },
            error: function(result) {
                fungsiPesanGagal(); return false;
            }
        });
    }
}

//list menu cetak
$(document).on('click','#listmenucetak',function(){
    var norm = $(this).attr('norm');
    var idp  = $(this).attr('idp');
    startLoading();
    var mTitle=' Cetak Berkas Pasien';
    var mContent='';
    mContent +='<form action="" id="form">'
          +'<div class="form-group">'
            +'<table class="table table-bordered table-hover table-striped" font-size: 25px; width: 100%;>'
                +'<tr><td>1. </td><td>CETAK LABEL</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_label('+norm+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>2. </td><td>LEMBAR RM</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_lembarrm('+norm+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>3. </td><td>Formulir INA-CBG</td><td><a class="btn btn-xs btn-primary" href="#" onclick="formInacbg('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>4. </td><td>RESUME</td><td><a class="btn btn-xs btn-primary" href="#" onclick="resumepasien('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>5. </td><td>CETAK GELANG</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_gelang('+norm+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>6. </td><td><b>RM 02</b> Triase & Asesmen Medis Gawat Darurat</td><td><a class="btn btn-xs btn-primary" href="#" onclick="triaseasesmenmedisugd('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>7. </td><td><b>RM 03</b> Asesmen Medis Rawat Jalan</td><td><a class="btn btn-xs btn-primary" href="#" onclick="asasmenmedisrawatjalan('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>8. </td><td><b>RM 03.4</b> Riwayat Kunjungan Rawat Jalan</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_resumralan('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>9. </td><td><b>RM 05.3</b> Catatan Terintegrasi</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_catatan_terintegrasi('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
                +'<tr><td>10. </td><td><b>RM 06.12</b> Laporan Operasi</td><td><a class="btn btn-xs btn-primary" href="#" onclick="cetak_laporan_operasi('+idp+')"> <i class="fa fa-print"></i> cetak</a></td></tr>'
            +'</table>'
          +'</div></form>';
$.confirm({
    title: mTitle,content: mContent,columnClass: 'm', type: 'orange',autoRefresh:true,
    buttons: {
        formReset:{
            text:'Tutup',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    stopLoading();
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
   });
});


$(document).on('click','#blacklistpasien',function(){
   var namaunit = $(this).attr('namaunit');
   var norm     = $(this).attr('norm');
   var idunit   = $(this).attr('idunit');
   var nama     = $(this).attr('nama');
   var mode     = $(this).attr('mode');
   $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: mode+' Pasien Untuk Poli/Unit '+namaunit+' .? <br> NORM : '+norm+'<br> NAMA : '+nama,
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/insert_blacklistpasien",
                    type:"POST",
                    dataType:"JSON",
                    data:{norm:norm,idunit:idunit,mode:mode},
                    success:function(result){
                        notif(result.status, result.message);//panggil fungsi popover message
                        dtpendaftaran.ajax.reload(null,false);
                    },
                    error:function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    }); 
});


//pindah hari periksa pasien
$(document).on('click','#pindahPemeriksaanPasien',function(){
    var idpendaftaran = $(this).attr("idp");
    
    var modalTitle = 'Pindah Jadwal Pemeriksaan Pasien';
    var modalContent = '<form action="" id="FormPindah" class="form-horizontal col-md-12">' +
                            '<input type="hidden" value="'+idpendaftaran+'" name="idpendaftaranpin">'+
                            
                              '<div class="box box-default box-solid">'+
                                '<div class="box-header with-border">'+
                                  '<h3 class="box-title">Data Pasien</h3>'+
                                '</div>'+
                                '<div class="box-body" style="padding:6px 0px;" id="dtpasien">'+
                                    
                                '</div>'+
                              '</div>'+
                              
                              '<div class="box box-default box-solid">'+
                                '<div class="box-header with-border">'+
                                  '<h3 class="box-title">Jadwal Periksa Sekarang</h3>'+
                                '</div>'+
                                '<div class="box-body" style="padding:6px 0px;" id="dtjadwalperiksa">'+
                                    
                                '</div>'+
                              '</div>'+
                            
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Pindah',
                btnClass: 'btn-blue',
                action: function () {
                    
                    if($('select[name="poliklinik"]').val()==='0')
                    {
                        alert_empty('poliklinik');
                        return false;
                    }
                    else
                    {
                        //jika browser tidak support webstorage
                        if (typeof(Storage) !== "undefined") {
                            var id = $(this).attr("alt");
                            localStorage.setItem("idpendaftaran", idpendaftaran);
                            localStorage.setItem("modependaftaran", "pindah");
                            window.location.href='pindahjadwal_pendaftaran_poli';
                        }
                        else
                        {
                            pesanUndefinedLocalStorage();
                        }
                    }
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan            
        startLoading();
        $.ajax({
            type: "POST", //tipe pengiriman data
            url: base_url+'cadmission/edit_pindahjadwalperiksa', //alamat controller yang dituju (di js base url otomatis)
            data: {idp:idpendaftaran}, //
            dataType: "JSON", //tipe data yang dikirim
            success: function(result) { //jika  berhasil
                stopLoading();
                var dtpasien = '<table class="table">'+
                                    '<tr><td width="120">Nama Pasien</td><td>: '+result.namalengkap+'</td></tr>'+
                                    '<tr><td>NORM</td><td>: '+result.norm+'</td></tr>'+
                                    '<tr><td>NIK</td><td>: '+result.nik+'</td></tr>'+
                                    '<tr><td>Tgl.Lahir</td><td>: '+result.tanggallahir+'</td></tr>'+
                                    '<tr><td>Alamat</td><td>: '+result.alamat+'</td></tr>'+
                                    '<tr><td>Jenis Kelamin</td><td>: '+result.jeniskelamin+'</td></tr>'+
                                '</table>';
                $('#dtpasien').html(dtpasien);
                
                var dtjadwalperiksa = '<table class="table">'+
                                        '<tr><td width="120">Nama Dokter</td><td>: '+result.namadokter+'</td></tr>'+
                                        '<tr><td>Poli/Unit</td><td>: '+result.namaunit+'</td></tr>'+
                                        '<tr><td>Loket</td><td>: '+result.namaloket+'</td></tr>'+
                                        '<tr><td>Jadwal</td><td>: '+result.tanggal+'</td></tr>'+
                                    '</table>';
                            
                $('#dtjadwalperiksa').html(dtjadwalperiksa);
                
            },
            error: function(result) { //jika error
                stopLoading();
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
