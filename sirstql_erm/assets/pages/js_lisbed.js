///Mahmud, clear
var dtbed = '';
$(function(){ 
//    search_by_bangsal(0);
    get_databangsal('#idbangsal');
    $('.select2').select2();
    dt_bed();
});

function dt_bed()
{
    dtbed = $('#dtbed').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
   "ajax": {
       "data":{idbangsal:function(){return $('#idbangsal').val();},},
   "url": base_url+'cpelayananranap/dt_bed',
   "type": "POST"
   },"columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) {}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

$(document).on('change','#idbangsal',function(){
    dtbed.ajax.reload(null,false);
})

//refresh page
function refresh_page()
{
    window.location.reload(true);
}
// dropdown poli
function setpoli_afterselect(x)
{
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("poliselected",x);

    } else {
         pesanUndefinedLocalStorage();
    }
}

// mahmud, clear ==== UBAH STATUS BED ===
$(document).on("click","#ubahstatusbed", function(){
    var idbangsal = $(this).attr("ibang");
    var idbed = $(this).attr("ibed");
    var idstatusbed = $(this).attr("istatus");
    var stat1 = $(this).attr("stat1");
    var stat2 = $(this).attr("stat2");
    ubahstatusbed(idbangsal, idbed,idstatusbed,stat1,stat2);
});
function ubahstatusbed(idbangsal, idbed,idstatusbed,stat1,stat2)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status dari '+stat1+' ke '+stat2+'..? ',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url +'cpelayananranap/ubahstatusbed',
                    type : "post",      
                    dataType : "json",
                    data : {ib: idbangsal, i:idbed, status:idstatusbed},
                    success: function(result) {
                        notif(result.status, result.message);
                        dtbed.ajax.reload(null,false);
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
