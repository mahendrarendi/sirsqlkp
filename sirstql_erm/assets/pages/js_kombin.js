$(function () 
{ 
    $("body").addClass("loading");
    $("body").removeClass("loading");
    $('#table').DataTable({
        "dom": '<"toolbar">frtip',
        "stateSave": true,
    }
    ); //Initialize Data Tables
    localStorage.removeItem("idpendaftaran"); //hapus localstorage idpendaftaran
    localStorage.removeItem("modependaftaran"); //hapus localstorage modependaftaran
})
function deleteRow(rowid) //DELETE ROW
{   
    var row = document.getElementById(rowid);
    var table = row.parentNode;
    while ( table && table.tagName != 'TABLE' )
        table = table.parentNode;
    if ( !table )
        return;
    table.deleteRow(row.rowIndex);
}
$(document).on("click","#delete_data", function(){ // DELETE USER
    var table = $(this).attr("alt");
    var id = $(this).attr("alt2");
    var halaman = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Apakah Data Akan Dihapus.?',
        buttons: {
            hapus: function () {                
                $.ajax({ 
                    url: base_url + 'cmasterdata/single_delete',
                    type : "post",      
                    dataType : "json",
                    data : { t:table, i:id, hal:halaman },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            deleteRow('row'+nobaris);
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            batal: function () {               
            }            
        }
    });
});
$(document).on("click","#reset_password", function(){ //reset password
    var username = $(this).attr("alt");
    var id = $(this).attr("alt2");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset password?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'reset_password',
                    type : "post",      
                    dataType : "json",
                    data : { i:id, u:username },
                    success: function(result) {
                    // console.log(result);        
                        notif(result.status, result.message);
                        table.ajax.reload(null,false);
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
$(document).on("click","#editPendaftaranPoli", function(){ //edit pendaftaran
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        var id = $(this).attr("alt");
        localStorage.setItem("idpendaftaran", id);
        localStorage.setItem("modependaftaran", "edit");
        window.location.href='edit_pendaftaran_poli';
    } else {
         pesanUndefinedLocalStorage();
    }
});
// $(document).on("click","#cetakRegister", function(){ //cetak pendaftaran
//     if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
//         var id = $(this).attr("alt");
//         localStorage.setItem("idpendaftaran", id);
//         localStorage.setItem("modependaftaran", "cetak");
//         window.location.href='view_pendaftaran_poli';
//     } else {
//          $.alert('<p style="text-align:center"><br><b>Your browser does not support Web Storage</b><br><br> Please update your browser..!</p>');
//     }
// });

function cetaktopdf_peran()//cetak peran
{
    var listperan='',x=0,no=0;
    $.ajax({
        type: "POST",
        url: base_url + 'creport/cetaktopdf_peran',
        dataType: "JSON",
        success: function(result) {
            listperan += '<h3>Data Peran</h3><table style="width:100%"><tr><td>No</td><td>Peran</td></tr>'; 
            for(x in result)
            {
                listperan += '<tr><td>'+ ++no +'</td><td>'+result[x].namaperan+'</td></tr>'; 
            }
            listperan += '</table>'; 
            fungsi_cetaktopdf(listperan,'');
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
    
}
function downloadtopdf_peran()//download peran
{
   window.location.href = base_url + "creport/downloadtopdf_peran";
    // var listperan='',x=0,no=0;
    // $.ajax({
    //     type: "POST",
    //     url: base_url + 'creport/cetaktopdf_peran',
    //     dataType: "JSON",
    //     success: function(result) {
    //         listperan += '<h3>Data Peran</h3><table style="width:100%"><tr><td>No</td><td>Peran</td></tr>'; 
    //         for(x in result)
    //         {
    //             listperan += '<tr><td>'+ ++no +'</td><td>'+result[x].namaperan+'</td></tr>'; 
    //         }
    //         listperan += '</table>'; 
    //         fungsi_cetaktopdf(listperan,'');
    //     },
    //     error: function(result) { //jika error
    //         fungsiPesanGagal(); // console.log(result.responseText);
    //         return false;
    //     }
    // });
    
}
