
var pagemode=$('#jsmode').val();
  switch(pagemode) 
  {
    case 'jasamedis':
      lap_jasamedis();
      $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
      break;
    case 'pendapatanrajal':
      startLoading();
      pendapatanrajal();
      $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
      break;
    case 'kasirpendapatanrajal':
      startLoading();
      rekap_pendapatanrajal();
      $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
      break;
    case 'pendapatanobatbebas':
      startLoading();
     $('input[name="date1"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now'); //Initialize Date picker
     $('input[name="date2"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now'); //Initialize Date picker
      pendapatanobatbebas();
      $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
      break;
    case 'dashboardpendapatan':
      resettanggal_dashboardpendapatan();
      dashboardpendapatan();
      break;
    case 'pendapatanpotonggaji':
        $('input[name="tanggal1"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now'); //Initialize Date picker
        $('input[name="tanggal2"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now'); //Initialize Date picker
        list_tongjiralan();
        break;
    case 'logsetelulangpembayaran':
        $('.tahunbulan').datepicker({autoclose: true, endDate: new Date(), format: "M yyyy",viewMode: "months",minViewMode: "months",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
        list_resetpembayaran();
        break;    default:
        //run code
  }
  
  $(document).on('click','#cari_dtlogsetelulangpembayaran',function(){
      list_resetpembayaran();
  });
  
  function list_resetpembayaran()
{
    startLoading();
    $.ajax({
        url: base_url+"creport/list_resetpembayaran",
        data:$('#formcari_resetpembayaran').serialize(),
        dataType:"JSON",
        type:"POST",
        success:function(result){
            stopLoading();
            var no=0;
            var dtrows = '';
            for(var x in result)
            {
                dtrows += '<tr><td>'+ ++no +'</td><td>'+ result[x].norm +'</td><td>'+ result[x].namapasien +'</td><td>'+ result[x].waktu +'</td><td>'+ result[x].namauser +'</td><td>'+ result[x].alasan +'</td><td>'+ result[x].jenisperiksa +'</td></tr>';
                
            }
            var table = '<table class="table table-hover table-bordered table-striped">'
                  +'<thead>'
                      +'<tr class="bg bg-gray">'
                        +'<th>NO</th>'
                        +'<th>NORM</th>'
                        +'<th>Nama Pasien</th>'
                        +'<th>Waktu Reset</th>'
                        +'<th>Petugas</th>'
                        +'<th>Alasan</th>'
                        +'<th>Pemeriksaan</th>'
                      +'</tr>'
                  +'</thead>'
                  +'<tbody>'+dtrows+'</tbody>'
                +'</table>';
            $('#dtresetpembayaran').empty();
            $('#dtresetpembayaran').html(table);
            $('.table').dataTable();
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
        }
    });
}
  
  
$(document).on('click','#caritongjiralan',function(){
    list_tongjiralan();
});
function list_tongjiralan()
{
    startLoading();
    $.ajax({
        url: base_url+"creport/list_keuangan_pendapatanpotonggaji",
        data:$('#formcari_tongji_ralan').serialize(),
        dataType:"JSON",
        type:"POST",
        success:function(result){
            stopLoading();
            var dtralan = '';
            var no=0;
            var total = 0;
            for(var x in result.ralan)
            {
                total   += parseInt(result.ralan[x].nominal);
                dtralan += '<tr><td>'+ ++no +'</td><td><a class="btn" idp="'+result.ralan[x].idpendaftaran+'" id="detailtongjiralan">'+ result.ralan[x].norm +'</a></td><td><a class="btn" idp="'+result.ralan[x].idpendaftaran+'" id="detailtongjiralan">'+ result.ralan[x].pasien +'</a></td><td>'+ result.ralan[x].penanggung +'</td><td>'+ result.ralan[x].tanggaltagih +'</td><td>'+ convertToRupiah(result.ralan[x].nominal) +'</td></tr>';
                
            }
            var table = '<table class="table dtlaporan table-hover table-bordered table-striped">'
                  +'<thead>'
                      +'<tr class="bg bg-gray">'
                        +'<th>NO</th>'
                        +'<th>NORM</th>'
                        +'<th>Nama Pasien</th>'
                        +'<th>Penanggung</th>'
                        +'<th>Tanggal Periksa</th>'
                        +'<th>Pendapatan</th>'
                      +'</tr>'
                  +'</thead>'
                  +'<tbody>'+dtralan+'</tbody>'
                  +'<tr><th></th><th></th><th>Total Pendapatan</th><th></th><th></th><th>'+ convertToRupiah(total) +'</th></tr>'
                +'</table>';
            $('#dtralan').empty();
            $('#dtralan').html(table);
            
            var dtobatbebas = '';
            var total = 0;
            var no = 0;
            for(var z in result.obatbebas)
            {
                total   += parseInt(result.obatbebas[z].nominal);
                dtobatbebas += '<tr><td>'+ ++no +'</td><td><a class="btn" id="detailpenjualanbebas" idbp="'+result.obatbebas[z].idbarangpembelibebas+'">'+ result.obatbebas[z].pegawai +'</a></td><td>'+ result.obatbebas[z].penanggung +'</td><td>'+ result.obatbebas[z].tanggal +'</td><td>'+ convertToRupiah(result.obatbebas[z].nominal) +'</td></tr>';
                
            }
            var table = '<table class="table dtlaporan2 table-hover table-bordered table-striped">'
                  +'<thead>'
                      +'<tr class="bg bg-gray">'
                        +'<th>NO</th>'
                        +'<th>Nama Pembeli</th>'
                        +'<th>Penanggung</th>'
                        +'<th>Tanggal Beli</th>'
                        +'<th>Pendapatan</th>'
                      +'</tr>'
                  +'</thead>'
                  +'<tbody>'+dtobatbebas+'</tbody>'
                  +'<tr><th></th><th>Total Pendapatan</th><th></th><th></th><th>'+ convertToRupiah(total) +'</th></tr>'
                +'</table>';
            $('#dtobatbebas').empty();
            $('#dtobatbebas').html(table);
            $('.dtlaporan').dataTable({"dom": '<"toolbar tool1 col-md-6">frtip',"paging":false});
            $('.dtlaporan2').dataTable({"dom": '<"toolbar tool2 col-md-6">frtip',"paging":false});
            $('.tool1').html('<a id="unduhtongjiralan" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> unduh</a>');
            $('.tool2').html('<a id="unduhtongjiobatbebas" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> unduh</a>');
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

$(document).on('click','#unduhtongjiralan',function(){
   window.location.href = base_url+"creport/excel_pendapatan_tongjiralan/"+$('input[name="tanggal1"]').val()+'/'+$('input[name="tanggal2"]').val();
});
$(document).on('click','#unduhtongjiobatbebas',function(){
   window.location.href = base_url+"creport/excel_pendapatan_tongjiobatbebas/"+$('input[name="tanggal1"]').val()+'/'+$('input[name="tanggal2"]').val();
});

// saat menu tampil di dashboard pendapatan di klik
$(document).on('click','#tampil_dashboardpendapatan',function(){
    dashboardpendapatan();
});

// saat menu refresh di dashboard pendapatan di klik
$(document).on('click','#refresh_dashboardpendapatan',function(){
    resettanggal_dashboardpendapatan();
    dashboardpendapatan();
});

//reset tanggal dashboard pendapatan
function resettanggal_dashboardpendapatan()
{
    $('#tanggal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', 'now' );      
}
//tampil data dashboard pendapatan
function dashboardpendapatan()
{
    startLoading();
    $.ajax({
        url: base_url+"creport/list_dashboardpendapatan",
        data:{tanggal:$('#tanggal').val()},
        dataType:"JSON",
        type:"POST",
        success:function(result){
            list_pendapatanralanbyjenistagihan(result.thr);
            list_pendapatanobatbebas(result.tob); 
            list_pendapatanralanbypoli(result.thrp);
            list_pendapatanralanall(result.thrp);
            stopLoading();
            
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
        }
    });
}
//list pendapatan ralan all
function list_pendapatanralanall(result)
{
    //mengurutkan data berdasarkan namaunit
    result.sort(function(a, b) {
        var keyA = a.namaunit,keyB = b.namaunit;
        // bandingkan 2 namaunit
        if (keyA < keyB) return -1;
        if (keyA > keyB) return 1;
        return 0;
      });
          
    var isibaris='', totbpjs=0, totumum=0, namaunit='', bpjs=0,umum=0;
    
    for ( var t in result)
    {
        if(result[t].namaunit != namaunit && namaunit !='' )
        {
            isibaris += '<tr><td>'+ namaunit +'</td>\n\
            <td>'+ convertToRupiah(bpjs) +'</td>\n\
            <td>'+ convertToRupiah(umum) +'</td></tr>';
            bpjs=0;
            umum=0;
        }
        bpjs += parseInt(result[t].bpjs);
        umum += parseInt(result[t].umum);
        totbpjs += bpjs;
        totumum += umum;
        namaunit = result[t].namaunit;
    }
//    //tampil row terakhir
    isibaris += '<tr><td>'+ namaunit +'</td><td>'+ convertToRupiah(bpjs) +'</td><td>'+ convertToRupiah(umum) +'</td></tr>';
//    //tampil total terakhir
    isibaris += '<tr class="bg bg-gray"><th>Subtotal</th><th>'+convertToRupiah(totbpjs)+'</th><th>'+convertToRupiah(totumum)+'</th></tr>';
    isibaris += '<tr class="bg bg-gray"><th>Total</th><th colspan="2">'+convertToRupiah(totbpjs + totumum)+'</th></tr>';
//    
    var listdata = '<thead><tr class="bg bg-gray">\n\
                   <th rowspan="2">POLIKLINIK</th>\n\
                   <th colspan="2">SETORAN</th>\n\
                   </tr>'
                   +'<tr class="bg bg-gray"><th>BPJS</th><th>UMUM</th></tr>'
                   +'</thead>\n\
                   <tbody>'+isibaris+'</tbody>';
    $('#list_pendapatanall').html(listdata);
}

//list pendapatan ralan berdasarkan poliklinik
function list_pendapatanralanbypoli(result)
{
    var isibaris='', shif='', bpjs=0, umum=0, totbpjs=0, totumum=0, totpasien=0, namaunit='',totpasienunit=0;
    for ( var t in result)
    {
        //tampilkan per unit
        if(result[t].namaunit != namaunit && namaunit !='' )
        {
            isibaris += '<tr><td>'+ namaunit +'</td>\n\
            <td>'+ convertToRupiah(totpasienunit) +'</td>\n\
            <td>'+ convertToRupiah(bpjs) +'</td>\n\
            <td>'+ convertToRupiah(umum) +'</td></tr>';
            totpasienunit=0;
            bpjs=0;
            umum=0;
        }
        // hitung per unit
        bpjs += parseInt(result[t].bpjs);
        umum += parseInt(result[t].umum);
        totpasienunit += parseInt(result[t].jumlahpasien);
        namaunit = result[t].namaunit;
        //tampilkan total
        if(result[t].levelshif != shif ) {
            isibaris += ((result[t].levelshif != shif && shif != '') ?  '<tr class="bg bg-gray"><th>Total</th><th>'+convertToRupiah(totpasien)+'</th><th>'+convertToRupiah(totbpjs)+'</th><th>'+convertToRupiah(totumum)+'</th></tr>' : '' );
            isibaris += '<tr class="bg bg-yellow-gradient"><td colspan="4"> SHIF '+result[t].levelshif+' , '+result[t].kasir+'</td></tr>';
            totbpjs=0;
            totumum=0;
            totpasien=0;
        } 
        //set shif
        shif = result[t].levelshif;
        
        //hitung total bpjs
        totbpjs += parseInt(result[t].bpjs);
        totumum += parseInt(result[t].umum);
        totpasien += parseInt(result[t].jumlahpasien);
    }
    //tampil row terakhir
    isibaris += '<tr><td>'+ namaunit +'</td><td>'+ convertToRupiah(totpasienunit) +'</td><td>'+ convertToRupiah(bpjs) +'</td><td>'+ convertToRupiah(umum) +'</td></tr>';
    //tampil total terakhir
    isibaris += '<tr class="bg bg-gray"><th>Total</th><th>'+convertToRupiah(totpasien)+'</th><th>'+convertToRupiah(totbpjs)+'</th><th>'+convertToRupiah(totumum)+'</th></tr>';            
    $('#list_tagihanralanpoli').html(isibaris);
}

//                    <tr class="bg bg-gray"><th colspan="4">SHIF 1 , Kasir</th></tr>
//list tagihan ralan berdasarkan jenistagihan
function list_pendapatanralanbyjenistagihan(result)
{
    var isibaris='', shif='', kasir = '', tagihan=0, dibayar=0;
    for ( var t in result)
    {
        if(result[t].shif != shif || result[t].kasir != kasir && kasir != '')
        {
            isibaris += (( result[t].shif != shif && shif != '' || result[t].kasir != kasir && kasir != '') ?  '<tr><th>Total</th><th>'+convertToRupiah(tagihan)+'</th><th>'+convertToRupiah(dibayar)+'</th></tr>' : '' );
            isibaris += '<tr class="bg bg-yellow"><td colspan="6">'+result[t].shif+', '+result[t].kasir+'</td></tr><tr class="bg bg-gray"><th>Cara Bayar</th><th>Nominal</th><th>Dibayar</th><th>Jenis Tagihan</th><th>Status Bayar</th><tr>';
            dibayar=0;
            tagihan=0;
        } 
        shif = result[t].shif;
        kasir = result[t].kasir;
        isibaris += '<tr style="'+statustagihan(result[t].jenistagihan)+'"><td class="text-bold">'+ result[t].carabayar +'</td>\n\
                    <td>'+ convertToRupiah(result[t].totaltagihan.replace('-', '')) +'</td>\n\
                    <td>'+ convertToRupiah(result[t].dibayar.replace('-', '')) +'</td>\n\
                    <td>'+ result[t].jenistagihan +'</td>\n\
                    <td>'+ result[t].statusbayar +'</td></tr>';
        //hitung tagihan dan dibayar
        tagihan += parseInt(result[t].totaltagihan);
        dibayar += parseInt(result[t].dibayar);

    }
    isibaris += '<tr><th>Total</th><th>'+convertToRupiah(tagihan)+'</th><th>'+convertToRupiah(dibayar)+'</th></tr>';            
    $('#list_tagihanralan').html(isibaris);
}
function statustagihan(jenistagihan)
{
    var warna = {tagihan:'background-color:#00bcd4;',retur:'background-color:#ff5722;',transfer:'background-color:#cddc39;'}
    return warna[jenistagihan];
}
//list pendapatan obat bebas
function list_pendapatanobatbebas(result)
{
    var isibaris='', shif='', tottagihan=0, totdibayar=0;
    for ( var t in result)
    {
        //tampilkan total
        if(result[t].levelshif != shif && shif!='')
        {
            isibaris += '<tr class="bg bg-gray"><td>Total</td><td>'+convertToRupiah(tottagihan)+'</td><td>'+convertToRupiah(totdibayar)+'</td></tr>';
            tottagihan=0;
            totdibayar=0;
        }
        //tampilkan shif
        if(result[t].levelshif != shif )
        {
            isibaris += '<tr class="bg bg-yellow-gradient"><td colspan="6"> SHIFT '+result[t].levelshif+'</td></tr>';
        } 
        shif = result[t].levelshif;
        isibaris += '<tr><td>'+ result[t].kasir +'</td>\n\
                    <td>'+ convertToRupiah(result[t].tagihan.replace('-', '')) +'</td>\n\
                    <td>'+ convertToRupiah(result[t].dibayar.replace('-', '')) +'</td>\n\
                    <td>'+ result[t].jenistagihan +'</td>\n\
                    <td>'+ result[t].carabayar +'</td></tr>';
        tottagihan += parseInt(result[t].tagihan);
        totdibayar = ((result[t].carabayar=='potonggaji') ? totdibayar + parseInt(0) : totdibayar + parseInt(result[t].dibayar) ) ;

    }
    //tampilkan total
    isibaris += '<tr class="bg bg-gray"><td>Total</td><td>'+convertToRupiah(tottagihan)+'</td><td>'+convertToRupiah(totdibayar)+'</td></tr>';
    $('#list_tagihanobatbebas').html(isibaris);
}

$(document).on('click','#tampilObatBebas',function(){
    return pendapatanobatbebas();
});

$(document).on('click','#unduhObatBebas',function(){
  window.location.href=base_url+"creport/datapendapatanobatbebas/"+$('input[name="date1"]').val()+"/"+$('input[name="date2"]').val()+"/"+$('select[name="user"]').val()+"/"+$('select[name="shif"]').val()+"/"+$('select[name="jenistagihan"]').val();
});
/**Pendapatan Obat Bebas**/
function pendapatanobatbebas()
{
  startLoading();
  $.ajax({
    url: base_url+"creport/datapendapatanobatbebas",
    data:{
        date1:$('input[name="date1"]').val(),
        date2:$('input[name="date2"]').val(),
        user:$('select[name="user"]').val(),
        shif:$('select[name="shif"]').val(),
        jenistagihan:$('select[name="jenistagihan"]').val()
    },
    dataType:"JSON",
    type:"POST",
    success:function(result){
    stopLoading();
      var dtpembelian = '', no=0, totnominal=0,totpotongan=0,totdibayar=0,totkekurangan=0, totkembali=0;
      for(var x in result){ totnominal += parseInt(result[x].nominal); totpotongan += parseInt(result[x].potongan); totdibayar += parseInt(result[x].dibayar); totkekurangan += parseInt(result[x].kekurangan); totkembali += parseInt(result[x].kembali); dtpembelian +='<tr><td>'+ ++no +'</td><td>'+result[x].pembeli+'</td><td>'+result[x].waktubayar+'</td><td>'+convertToRupiah(result[x].nominal)+'</td><td>'+convertToRupiah(result[x].potongan)+'</td><td>'+convertToRupiah(result[x].dibayar)+'</td><td>'+ convertToRupiah( ((result[x].kekurangan < 0)? 0 : result[x].kekurangan ) ) +'</td><td>'+ convertToRupiah( ((result[x].kembali < 0) ? 0 : result[x].kembali ) ) +'</td></tr>'; }
        dtpembelian += '<tr><td colspan="3">Total</td><td>'+ convertToRupiah(totnominal) +'</td><td>'+ convertToRupiah(totpotongan) +'</td><td>'+ convertToRupiah(parseInt(totdibayar) - parseInt(totkembali)) +'</td><td>'+ convertToRupiah(totkekurangan) +'</td><td>'+ convertToRupiah(totkembali) +'</td></tr>';
        var html='<table class="table table-bordered table-striped table-hover" cellspacing="0">'
              +'<thead>'
              +'<tr class="bg-yellow">'
                +'<th>No</th>'
                +'<th>Pembeli</th>'
                +'<th>Waktu Bayar</th>'
                +'<th>Nominal</th>'
                +'<th>Potongan</th>'
                +'<th>Dibayar</th>'
                +'<th>Kekurangan</th>'
                +'<th>Kembali</th>'
              +'</tr>'
              +'</thead><body>'+dtpembelian+'</body></table>';
      $('[data-toggle="tooltip"]').tooltip(); 
      $('#tampil').empty();
      $('#tampil').html(html);
     
    },
    error:function(result){
      stopLoading();
      fungsiPesanGagal();
    }
  });
}

/**Laporan Data Medis**/
function lap_jasamedis()
{
  $('input[name="tanggal1"]').datepicker({autoclose: true, /* endDate: new Date(), */ format: "yyyy-mm-dd", /*viewMode: "months", minViewMode: "months",*/orientation: "bottom"}); //Initialize Date picker
  $('input[name="tanggal2"]').datepicker({autoclose: true, format: "yyyy-mm-dd",orientation: "bottom"}); //Initialize Date picker
  $('.select2').select2();
}

function getjasamedis()
{
   var url =  base_url+'creport/keuangan_jasamedis?idpegawaidokter='+$('#nmdokter').val()+'&tanggal1='+$('input[name="tanggal1"]').val()+'&tanggal2='+$('input[name="tanggal2"]').val();
   window.location.href= url;
}

function unduhjasamedis()
{
    var url =  base_url+'creport/keuangan_jasamedis_excel?idpegawaidokter='+$('#nmdokter').val()+'&tanggal1='+$('input[name="tanggal1"]').val()+'&tanggal2='+$('input[name="tanggal2"]').val()+'&namadokter='+$('#nmdokter option:selected').text();
   window.location.href= url;
}

/**Unduh Laporan Pendapatan Ralan Excel**/
function unduhpendapatanrajal()
{
  var carabayar = $('select[name="carabayar"]').val();
  var unit = $('select[name="unit"]').val();
  
  if(carabayar!='' && unit==''){
    alert('Nama Poli Harus diisi.');
  }else{
    window.location.href=base_url+"creport/unduh_pendapatanrawatjalan/unduh/"+$('input[name="hari"]').val()+"/"+$('input[name="hari2"]').val()+"/"+$('select[name="unit"]').val()+"/"+$('select[name="carabayar"]').val();
  }
}

function unduh_kasir_pendapatanrajaljs()
{
    var carabayar = $('select[name="carabayar"]').val();
    var unit = $('select[name="unit"]').val();

    if(carabayar!='' && unit==''){
      alert('Nama Poli Harus diisi.');
    }else{
      window.location.href=base_url+"creport/unduh_rekappendapatanrawatjalan/unduh/"+$('input[name="hari"]').val()+"/"+$('input[name="hari2"]').val()+"/"+$('select[name="unit"]').val()+"/"+$('select[name="carabayar"]').val()+"/"+$('select[name="user"]').val()+"/"+$('select[name="shif"]').val();
    }
}

//reload pages
function refresh_page()
{
    // table.state.clear();
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now' );
    window.location.reload(true);
}
function modedata(mode)
{
   var generatemode = localStorage.getItem('generatemode');
  if(generatemode==null){
    localStorage.setItem('generatemode',mode);
    generatemode = localStorage.getItem('generatemode');
  }else{
    if(mode==''){
      localStorage.setItem('generatemode',localStorage.getItem('generatemode'));
      generatemode = localStorage.getItem('generatemode');
    }else{
      localStorage.setItem('generatemode',mode);
      generatemode = localStorage.getItem('generatemode');
    }   
    
    return generatemode;
  }
}
function pendapatanrajal()
{
  startLoading();
  $('.select2').select2();
  var tanggal  = $('input[name="hari"]').val();
  var tanggal2 = $('input[name="hari2"]').val();
  var shift    = $('#shift option:selected').text();
  $.ajax({
    url: base_url+"creport/pendapatanrajaljs",
    data:{
        date:tanggal, 
        date2:tanggal2,
        dokter:$('select[name="dokter"]').val(),
        unit:$('select[name="unit"]').val(),
        user:$('select[name="user"]').val(), 
        shif:$('select[name="shif"]').val(),
        carabayar:$('select[name="carabayar"]').val()
    },
    dataType:"JSON",
    type:"POST",
    success:function(result){
    stopLoading();
        var data='', sumjasaoperator=0,sumjasars=0, carabayar='';
        var total=0, colspan=(parseInt(result.jenistarif.length)+ parseInt(10));
        for(a in result.carabayar)
        {
            data += '<tr class="bg bg-yellow-gradient"><th colspan="'+ colspan +'">'+result.carabayar[a]+'</th></tr>'; 
            data += '<tr class="bg bg-gray"><th rowspan="2">No</th><th rowspan="2">PASIEN</th><th rowspan="2">DOKTER</th><th colspan='+ (colspan-3) +'>BIAYA</th></tr>';
            var biaya='';
            for(x in result.jenistarif){  biaya+='<td>'+result.jenistarif[x].jenistarif+'</td>';}
            data+='<tr class="bg bg-gray">'+biaya+'<td>TotJaminan</td><td>TotNonJaminan</td><td>Potongan</td><td>TotTagihan</td><td>Dibayar</td><td>Transfer</td><td>Selisih</td></tr>';
            var no=0, totJaminan=0,totNonjaminan=0, totTransfer=0, totSelisih=0,totPotongan=0,totTagihan=0,totDibayar=0,totJaminan=0,totNonjaminan=0;
            
            for(b in result.pasien[a])
            {
                var  tarif='', totaltarif=0, selisih=0 , jaminan=0, nonjaminan=0;
                for( d in result.tindakan[a][b])
                {
                    
                    var subtotal = ((result.tindakan[a][b][d][0].total==null)? 0 : result.tindakan[a][b][d][0].total );
                    jaminan += parseInt( ((result.tindakan[a][b][d][0].jaminanasuransi=='1') ? subtotal : 0 ) );
                    nonjaminan += parseInt( ((result.tindakan[a][b][d][0].jaminanasuransi=='0') ? subtotal : 0 ) );
                    tarif += '<td>'+((result.tindakan[a][b][d][0].total==null)?0: convertToRupiah(subtotal,2) ) +'</td>' ;
                    totaltarif += parseInt(subtotal);
                }
                selisih = (parseFloat(totaltarif) - (parseFloat(jaminan) + parseFloat(nonjaminan)));                
                totTagihan += (parseFloat(jaminan) + parseFloat(nonjaminan) ) - parseFloat(result.pasien[a][b].potongan);
                totDibayar += parseFloat(result.pasien[a][b].dibayar);
                totPotongan += parseFloat(result.pasien[a][b].potongan);
                totTransfer += parseFloat(result.pasien[a][b].transfer);
                totJaminan += parseFloat(jaminan);
                totNonjaminan += parseFloat(nonjaminan);
                totSelisih += parseFloat(selisih);
               
                data+='<tr><td>'+ ++no +'</td>'
                     +'<td> <a '+tooltip('Detail Transaksi')+' onclick="detailTransaksi('+result.pasien[a][b].idpendaftaran+')" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a> '+result.pasien[a][b].namalengkap+'</td>'
                     +'<td>'+result.pasien[a][b].namadokter+'</td>'
                     +tarif
                     +'<td>'+convertToRupiah(jaminan)+'</td>'
                     +'<td>'+convertToRupiah(nonjaminan)+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].potongan)+'</td>'
                     +'<td>'+convertToRupiah( (parseFloat(jaminan) + parseFloat(nonjaminan) ) - parseFloat(result.pasien[a][b].potongan))+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].dibayar)+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].transfer)+'</td>'
                     +'<td>'+convertToRupiah(selisih)+'</td></tr>';           
            }
            
            data+='<tr class="bg bg-gray">'
                +'<td colspan="'+ (colspan - 7) +'">Total</td>'
                +'<td>'+convertToRupiah(totJaminan)+'</td>'
                +'<td>'+convertToRupiah(totNonjaminan)+'</td>'
                +'<td>'+convertToRupiah(totPotongan)+'</td>'
                +'<td>'+convertToRupiah(totTagihan)+'</td>'
                +'<td>'+convertToRupiah(totDibayar)+'</td>'
                +'<td>'+convertToRupiah(totTransfer)+'</td>'
                +'<td>'+convertToRupiah(totSelisih)+'</td></tr>';
        }
            
        var html = '<h3>PENDAPATAN RALAN '+shift+'</h3><table style="margin-top: 5px;" class="table table-bordered table-striped table-hover" cellspacing="0"><body style="max-width:200px; overflow-y:auto;overflow-x:auto;">'+data+'</body></table>';
        html=html;
      $('#tampil').empty();
      $('#tampil').html(html);
      $('[data-toggle="tooltip"]').tooltip(); 
      $('input[name="hari"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((tanggal=="")? 'now' : tanggal ) ); //Initialize Date picker
      $('input[name="hari2"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((tanggal2=="")? 'now' : tanggal2 ) ); //Initialize Date picker
//      setdaterange_pendapatanralan();
    },
    error:function(result){
      stopLoading();
      fungsiPesanGagal();
    }
  });
}

function rekap_pendapatanrajal(mode='')
{
//  var generatemode = modedata(mode);
//  $('h1').text(((generatemode=='pendapatan')?'Pendapatan Ralan':((generatemode=='detail')?'Detail Pendapatan RALAN':'Rekap Laporan RALAN')));
  startLoading();
  $('.select2').select2();
  var tanggal  = $('input[name="hari"]').val();
  var tanggal2 = $('input[name="hari2"]').val();
  var shift    = $('#shift option:selected').text();
  $.ajax({
    url: base_url+"creport/kasir_pendapatanrajaljs",
    data:{
        date:tanggal, 
        date2:tanggal2,
        dokter:$('select[name="dokter"]').val(),
        unit:$('select[name="unit"]').val(),
//        mode:generatemode, 
        user:$('select[name="user"]').val(), 
        shif:$('select[name="shif"]').val(),
        carabayar:$('select[name="carabayar"]').val()
    },
    dataType:"JSON",
    type:"POST",
    success:function(result){
    stopLoading();
        var data='', sumjasaoperator=0,sumjasars=0, carabayar='';
        var total=0, colspan=(parseInt(result.jenistarif.length)+ parseInt(10));
        for(a in result.carabayar)
        {
            data += '<tr class="bg bg-yellow-gradient"><th colspan="'+ colspan +'">'+result.carabayar[a]+'</th></tr>'; 
            data += '<tr class="bg bg-gray"><th rowspan="2">No</th><th rowspan="2">PASIEN</th><th rowspan="2">DOKTER</th><th colspan='+ (colspan-3) +'>BIAYA</th></tr>';
            var biaya='';
            for(x in result.jenistarif){  biaya+='<td>'+result.jenistarif[x].jenistarif+'</td>';}
            data+='<tr class="bg bg-gray">'+biaya+'<td>TotJaminan</td><td>TotNonJaminan</td><td>Potongan</td><td>TotTagihan</td><td>Dibayar</td><td>Transfer</td><td>Selisih</td></tr>';
            var no=0, totJaminan=0,totNonjaminan=0, totTransfer=0, totSelisih=0,totPotongan=0,totTagihan=0,totDibayar=0,totJaminan=0,totNonjaminan=0;
            
            for(b in result.pasien[a])
            {
                var  tarif='', totaltarif=0, selisih=0 , jaminan=0, nonjaminan=0;
                for( d in result.tindakan[a][b])
                {
                    
                    var subtotal = ((result.tindakan[a][b][d][0].total==null)? 0 : result.tindakan[a][b][d][0].total );
                    jaminan += parseInt( ((result.tindakan[a][b][d][0].jaminanasuransi=='1') ? subtotal : 0 ) );
                    nonjaminan += parseInt( ((result.tindakan[a][b][d][0].jaminanasuransi=='0') ? subtotal : 0 ) );
                    tarif += '<td>'+((result.tindakan[a][b][d][0].total==null)?0: convertToRupiah(subtotal,2) ) +'</td>' ;
                    totaltarif += parseInt(subtotal);
                }
                selisih = (parseFloat(totaltarif) - (parseFloat(jaminan) + parseFloat(nonjaminan)));                
                totTagihan += (parseFloat(jaminan) + parseFloat(nonjaminan) ) - parseFloat(result.pasien[a][b].potongan);
                totDibayar += parseFloat(result.pasien[a][b].dibayar);
                totPotongan += parseFloat(result.pasien[a][b].potongan);
                totTransfer += parseFloat(result.pasien[a][b].transfer);
                totJaminan += parseFloat(jaminan);
                totNonjaminan += parseFloat(nonjaminan);
                totSelisih += parseFloat(selisih);
               
                data+='<tr><td>'+ ++no +'</td>'
                     +'<td> <a '+tooltip('Detail Transaksi')+' onclick="detailTransaksi('+result.pasien[a][b].idpendaftaran+')" class="btn btn-xs btn-primary"><i class="fa fa-list-ul"></i></a> '+result.pasien[a][b].namalengkap+'</td>'
                     +'<td>'+result.pasien[a][b].namadokter+'</td>'
                     +tarif
                     +'<td>'+convertToRupiah(jaminan)+'</td>'
                     +'<td>'+convertToRupiah(nonjaminan)+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].potongan)+'</td>'
                     +'<td>'+convertToRupiah( (parseFloat(jaminan) + parseFloat(nonjaminan) ) - parseFloat(result.pasien[a][b].potongan))+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].dibayar)+'</td>'
                     +'<td>'+convertToRupiah(result.pasien[a][b].transfer)+'</td>'
                     +'<td>'+convertToRupiah(selisih)+'</td></tr>';           
            }
            
            data+='<tr class="bg bg-gray">'
                +'<td colspan="'+ (colspan - 7) +'">Total</td>'
                +'<td>'+convertToRupiah(totJaminan)+'</td>'
                +'<td>'+convertToRupiah(totNonjaminan)+'</td>'
                +'<td>'+convertToRupiah(totPotongan)+'</td>'
                +'<td>'+convertToRupiah(totTagihan)+'</td>'
                +'<td>'+convertToRupiah(totDibayar)+'</td>'
                +'<td>'+convertToRupiah(totTransfer)+'</td>'
                +'<td>'+convertToRupiah(totSelisih)+'</td></tr>';
        }
            
        var html = '<h3>PENDAPATAN RALAN '+shift+'</h3><table style="margin-top: 5px;" class="table table-bordered table-striped table-hover" cellspacing="0"><body style="max-width:200px; overflow-y:auto;overflow-x:auto;">'+data+'</body></table>';
        html=html;
      $('#tampil').empty();
      $('#tampil').html(html);
      $('[data-toggle="tooltip"]').tooltip(); 
      $('input[name="hari"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((tanggal=="")? 'now' : tanggal ) ); //Initialize Date picker
      $('input[name="hari2"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((tanggal2=="")? 'now' : tanggal2 ) ); //Initialize Date picker
//      setdaterange_pendapatanralan();
    },
    error:function(result){
      stopLoading();
      fungsiPesanGagal();
    }
  });
}


function detailTransaksi(idp)
{
    startLoading();
     $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_langsung',
        data: {p:idp},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            var tipesebelum='', subtotal=0,totalsewa=0, total=0, no=0;
            var listkasir = '<div style="float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);" src="'+base_url+'assets/images/headerkuitansismall.jpg" /><table border="0" style="font-size:normal;font-family: padding:8px; "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                listkasir += '<tr><td>Nama</td><td>'+result['infotagihan'][0].namalengkap+'</td></tr>';
                listkasir += '<tr><td>Alamat</td><td>'+result['infotagihan'][0].alamat+'</td></tr>';
                listkasir += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                listkasir += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
               
            }
            listkasir += '</table><table border="0" style="padding:8px; font-size:normal;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333; width:"100%">';
            listkasir += '<tr><td>No/Item</td><td align="center" >Jml</td><td align="right">Nominal</td></tr>';
            for(x in result['detailtagihan'])
            {
                if (tipesebelum != result['detailtagihan'][x].jenistarif)
                {
                    if (tipesebelum != '') listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
                    listkasir += '<tr><td colspan="3"><b>'+result['detailtagihan'][x].jenistarif+'</b></td></tr>';
                    subtotal  = 0;
                }
                totalsewa+= parseInt(result['detailtagihan'][x].sewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                subtotal += parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                total    += (result['detailtagihan'][x].jenistarif=='Diskon') ? 0 : parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama+ ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="center" style="font-size:normal;">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].nonsewa,10))* parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                tipesebelum = result['detailtagihan'][x].jenistarif;
            }
            listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
            if (totalsewa > 0)
            {
                listkasir += '<tr><td colspan="3">Sewa Alat</td></tr>';
                listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(totalsewa)+'</td></tr>';
                total     += totalsewa;
                
            }
            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiahBulat(bulatkanRatusan(total))+'</td></tr>';
            listkasir += '</table><table border="0" style="padding:8px; font-size:normal;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            for(x in result['infotagihan'])
            {
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].nominal)+'</td></tr>';
                listkasir += '<tr><td>Potongan</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].potongan)+'</td></tr>';
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][x].dibayar)+'</td></tr>';
                listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
                listkasir += '<tr><td colspan="2">&nbsp;</td></tr>';
            }
            listkasir += '</table></div>';
            
            
            $.confirm({ //aksi ketika di klik menu
            title: 'Detail Transaksi',
            content: listkasir,
            columnClass: 'l',closeIcon: true,animation: 'scale',type: 'orange',
            buttons: {
                formReset:{
                    text: 'Tutup',
                    btnClass: 'btn-danger'
                }
            },
            onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
            }
        });
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

function detail_penjualanbebas()
{
    
}

$(document).on('click','#detailtongjiralan',function(){
    var idp = $(this).attr('idp');
    startLoading();
     $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_langsung',
        data: {p:idp},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            var tipesebelum='', subtotal=0,totalsewa=0, total=0, no=0;
            var listkasir = '<div style="float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);" src="'+base_url+'assets/images/headerkuitansismall.jpg" /><table border="0" style="font-size:normal;font-family: padding:8px; "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                listkasir += '<tr><td>Nama</td><td>'+result['infotagihan'][0].namalengkap+'</td></tr>';
                listkasir += '<tr><td>Alamat</td><td>'+result['infotagihan'][0].alamat+'</td></tr>';
                listkasir += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                listkasir += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
               
            }
            listkasir += '</table><table border="0" style="padding:8px; font-size:normal;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333; width:"100%">';
            listkasir += '<tr><td>No/Item</td><td align="center" >Jml</td><td align="right">Nominal</td></tr>';
            for(x in result['detailtagihan'])
            {
                if (tipesebelum != result['detailtagihan'][x].jenistarif)
                {
                    if (tipesebelum != '') listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
                    listkasir += '<tr><td colspan="3"><b>'+result['detailtagihan'][x].jenistarif+'</b></td></tr>';
                    subtotal  = 0;
                }
                totalsewa+= parseInt(result['detailtagihan'][x].sewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                subtotal += parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                total    += (result['detailtagihan'][x].jenistarif=='Diskon') ? 0 : parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama+ ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="center" style="font-size:normal;">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].nonsewa,10))* parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                tipesebelum = result['detailtagihan'][x].jenistarif;
            }
            listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
            if (totalsewa > 0)
            {
                listkasir += '<tr><td colspan="3">Sewa Alat</td></tr>';
                listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(totalsewa)+'</td></tr>';
                total     += totalsewa;
                
            }
            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiahBulat(bulatkanRatusan(total))+'</td></tr>';
            listkasir += '</table><table border="0" style="padding:8px; font-size:normal;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            for(x in result['infotagihan'])
            {
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].nominal)+'</td></tr>';
                listkasir += '<tr><td>Potongan</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].potongan)+'</td></tr>';
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][x].dibayar)+'</td></tr>';
                listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
                listkasir += '<tr><td colspan="2">&nbsp;</td></tr>';
            }
            listkasir += '</table></div>';
            
            
            $.confirm({ //aksi ketika di klik menu
            title: 'Detail Transaksi',
            content: listkasir,
            columnClass: 'l',closeIcon: true,animation: 'scale',type: 'orange',
            buttons: {
                formReset:{
                    text: 'Tutup',
                    btnClass: 'btn-danger'
                }
            },
            onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
            }
        });
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
});

$(document).on('click','#detailpenjualanbebas',function(){
   var idbp = $(this).attr('idbp');
   startLoading();
    var font = '', fontx = '', paper = '', margin = '', lebar = '', lebargambar='',total=0;
    var listkasir='',tipesebelum='',subtotal=0,total=0,no=0, selisih = 0, jenispemeriksaan='rajal';
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_notapenjualanbebas',
        data: {i:idbp},
        dataType: "JSON",
        success: function(result) {
            lebar = (jenispemeriksaan!='rajal') ? 'width:19cm;' : 'width:6cm;';
            lebargambar = (jenispemeriksaan!='rajal') ? '' : lebar;
            font = lebar + ((jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:small;');
            fontx = (jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:x-small;';
            paper = (jenispemeriksaan!='rajal') ? '20' : '7.6';
            margin = (jenispemeriksaan!='rajal') ? '2' : '0.2';
            listkasir += '<div style="' + lebar + 'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' + lebargambar + '" src="'+base_url+'assets/images/headerkuitansi' + ((jenispemeriksaan!='rajal') ? '' : 'small') + '.jpg" />\n\
                          <table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            // INFO TAGIHAN
            listkasir += '<tr><td>Nama</td><td>: '+ result['infotagihan'][0].namacetak +'</td></tr><tr><td>Keterangan</td><td>: '+ result['infotagihan'][0].keterangan +'</td></tr></table>';

            // list item
            listkasir += '<table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            listkasir += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>';
            for(x in result['detailtagihan']){ total += parseInt(result['detailtagihan'][x].jumlah) * result['detailtagihan'][x].harga; listkasir+='<tr><td>'+result['detailtagihan'][x].nama+'</td><td align="right">'+parseInt(result['detailtagihan'][x].jumlah)+'</td><td align="right">'+convertToRupiah(result['detailtagihan'][x].harga)+'</td></tr>';}

            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiah(total)+'</td></tr>';
            listkasir += '</table><table border="0" style="' + font + 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            
            // DETAIL TAGIHAN
            for(var u in result['infotagihan'])
            {
                listkasir += '';
                selisih   += (parseInt( result['infotagihan'][u].nominal , 10) + parseInt( result['infotagihan'][u].pembulatan , 10)) - ( parseInt(result['infotagihan'][u].dibayar, 10) + parseInt(result['infotagihan'][u].potongan,10));
                kembali   = (selisih < 0) ? selisih * -1 : 0 ;
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][u].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][u].waktu+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][u].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].nominal)+'</td></tr>';
                listkasir += (result['infotagihan'][u].potongan>0) ? '<tr><td>Potongan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].potongan)+'</td></tr>' : '';
                listkasir += (result['infotagihan'][u].pembulatan > 0) ? '<tr><td>Pembulatan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].pembulatan)+'</td></tr>' : '';
                listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].totaltagihan)+'</td></tr>';
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][u].dibayar)+'</td></tr>';
                listkasir += (result['infotagihan'][u].kekurangan>0) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][u].kekurangan)+'</td></tr>' : '';
                listkasir += (kembali>0) ? '<tr><td>Kembali</td><td align="right">'+convertToRupiah(kembali)+'</td></tr>' : '';
                listkasir += '<tr><td>Jenistagihan</td><td align="right">'+result['infotagihan'][u].jenistagihan+'</td></tr>';
                listkasir += '<tr><td>Carabayar</td><td align="right">'+result['infotagihan'][u].carabayar+'</td></tr>';
            }
            
            listkasir += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            listkasir += '</table></div>';
            stopLoading();
            fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
});