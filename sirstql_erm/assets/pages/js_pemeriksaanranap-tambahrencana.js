var table='';
var jumlahicdtindakan = 0;
var val_rencanabarangobatbhp = [];
var dtranap='';
var jumlahrencanaperawatan=0;
var jumlahrencanaobat=0;
var jumlahrencanapenggunaanobat = 0;
$(function(){ 
    //jika browser tidak support webstorage
    if(typeof(Storage)==undefined){pesanUndefinedLocalStorage();}
    var dateNow = new Date();
    $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
    tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinap')); //ambil data inap
    tampilDataIcd($('#icd0'), "1,3,4,5",$('select[name="idinap"]').val()); //ambil data icd
    tampilDataDokter($('select[name="idpegawaidokter"]')); //ambil data icd
});


function menusimpanrencana()
{
    if ($('input[name="tgl"]').val() == '')
    {
        empty('waktu perencanaan');
        return false;
    }
    else if ($('select[name="idinap"]').val() == 0)
    {
        alert('Pasien Rawat Inap Belum Dipilih.');
        return false;
    }
    else if ($('select[name="icd"]').val() == 0)
    {
        alert_empty('icd');
        return false;
    }
    else if ($('input[name="jumlahperhari"]').val() == '')
    {
        alert_empty('jumlahperhari');
        return false;
    }
    else if ($('input[name="selama"]').val() == '')
    {
        alert_empty('selama');
        return false;
    }
    else
    {
        localStorage.setItem("idinapselected",$('select[name="idinap"]').val());
        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
        localStorage.setItem("jumlahtindakan",$('input[name="jumlahtindakan"]').val());
        localStorage.setItem("selama",$('input[name="selama"]').val());
        simpanrencana();
    }
}

//basit, clear
function simpanrencana()
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencana',
        //data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icd"]').val(), ipd:$('select[name="idpegawaidokter"]').val(), td:$('input[name="jumlahtindakan"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        data: $("#FormRencanaPerawatan").serialize(),
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            notif(result.status,result.message);
            setTimeout(function(){ window.location.href = $('#refferurl').val()}, 60);
           
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

$(document).on("click","#hapusrencana", function(){
    
    var rencana = $(this).attr('jumlah');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Hapus Rencana Perawatan.?',
        buttons: {
            hapus: function () {   
                $('#tr_rencana'+rencana).remove();
                $('.tr_rencana'+rencana).remove();
            },
            batal: function () {               
            }            
        }
    });
});

$(document).on("click","#hapusrencanaobat",function(){
    var jumlahrencana = $(this).attr("jumlahrencanaobat");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Hapus Rencana Obat/BHP.?',
        buttons: {
            hapus: function () {   
                $('.jumlahrencanaobat'+jumlahrencana).remove();
            },
            batal: function () {               
            }            
        }
    });
});


function menukembali(){
    
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Data Rencana Tidak Tersimpan.!',
        buttons: {
            kembali: function () {   
                window.location.href = $('#refferurl').val();
            },
            batal: function () {               
            }            
        }
    });
}

//rencana penggunaan obat/bhp

$(document).on('click','#tambahrencanapenggunaanobat',function(){
  $.alert('Fitur Sedang dibuat., belum bisa digunakan..');
  return;

    if($('select[name="idinap"]').val() == 0)
    {
        alert('Silakan memilih pasien terlebih dahulu.');
    }
    else
    {
        jumlahrencanapenggunaanobat++;
        var jeda = '<tr class="tr_rencana'+jumlahrencanapenggunaanobat+'obatbhp"><td>&nbsp;</td></tr>';
        var rencana='<tr style="margin-top:10px;border-top:3px solid #f39c10;background-color:#f1f1f1;" id="tr_rencana'+jumlahrencanapenggunaanobat+'obatbhp">'
         +'<td style="padding: 14px 0px 10px 0px;">'
             +'<div class="col-md-12"><a id="hapusrencana" jumlah="'+jumlahrencanaperawatan+'" '+tooltip(' Hapus Rencana ')+' class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash-o"></i></a></div>'
             +'<div class="form-group" style="margin: 1px;">'
                 +'<div class="col-sm-5"><label>ICD Rencana Perawatan</label><select class="select2 form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'"  name="icd[]"><option value="0">Pilih ICD Perawatan</option></select></div>'
                 +'<div class="col-sm-2"><label>Jumlah Tindakan</label><input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value=""><small class="text text-red">#jumlah setiap kali perawatan</small></div>'
                 +'<div class="col-sm-3"><label>Jumlah Per Hari</label><input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value=""><small class="text text-red">#jumlah tindakan / hari. Ex: 1 hari 3 x, maka diisi 3</small></div> '
                 +'<div class="col-sm-2"><label>Selama [ Hari ]</label><input type="text" class="form-control" style="width:100%;" name="selama[]" value=""><small class="text text-red">#berapa hari tindakan dilakukan</small></div>'
             +'</div>'

             +'<a style="margin-left:15px;" id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="BHP/Obat Include Perawatan"><i class="fa fa-plus-circle"></i> Tambah Obat/Bhp</a>'
             +'<div class="form-group" style="margin: 2px;">'
                 +'<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label></div> '
                 +'<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jumlah Pesan</label></div> '
                 +'<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label></div>'
                 +'<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-3"><label>Jumlah Pakai (Per Tindakan)</label></div>'
             +'</div>'
         +'</td>'
     +'</tr>'+jeda;
        $('#rencanaperawatan').append(rencana);
       tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5",$('select[name="idinap"]').val()); //ambil data icd
       $('[data-toggle="tooltip"]').tooltip();
    }
});

//-- rencana obat.bhp include perawatan
$(document).on("click","#tambahrencanabarang", function(){
    var jumlahrencana = $(this).attr("jumlahrencana");
    jumlahicdtindakan++;
    jumlahrencanaobat++;
    $('#rencanabarangobatbhp'+jumlahrencana).append('<div class="jumlahrencanaobat'+jumlahrencanaobat+'"><select class="select2 form-control jumlahrencanaobat'+jumlahrencanaobat+'" style="width:100%;" id="rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangobatbhp'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select></div>');
    $('#rencanabarangjumlahpesan'+jumlahrencana).append('<input type="text" class="form-control jumlahrencanaobat'+jumlahrencanaobat+'" style="width:100%;" id="rencanabarangjumlahpesan'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpesan'+jumlahrencana+'['+jumlahicdtindakan+']" />');
    $('#rencanabarangsatuanpakai'+jumlahrencana).append('<div class="jumlahrencanaobat'+jumlahrencanaobat+'"><select class="select2 form-control" style="width:100%;" id="rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangsatuanpakai'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select></div>');
    $('#rencanabarangjumlahpakai'+jumlahrencana).append('<div class="input-group jumlahrencanaobat'+jumlahrencanaobat+'"><input type="text" class="form-control" style="width:100%;" id="rencanabarangjumlahpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpakai'+jumlahrencana+'['+jumlahicdtindakan+']" /> <a id="hapusrencanaobat" jumlahrencanaobat="'+jumlahrencanaobat+'" class="btn input-group-addon btn-xs btn-danger" '+tooltip('Hapus Rencana Obat/BHP')+'><i class="fa fa-trash"></i></a></div>');
    isicombobhp('rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan);
    isicombosatuan('rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'', '');
    $('[data-toggle="tooltip"]').tooltip();
});
//rencana perawatan
$(document).on("click","#tambahrencanaperawatan", function(){
    
    if($('select[name="idinap"]').val() == 0)
    {
        alert('Silakan memilih pasien terlebih dahulu.');
    }
    else
    {
        jumlahrencanaperawatan++;
        var jeda = '<tr class="tr_rencana'+jumlahrencanaperawatan+'"><td>&nbsp;</td></tr>';
        var rencana='<tr style="margin-top:10px;border-top:3px solid #f39c10;background-color:#f1f1f1;" id="tr_rencana'+jumlahrencanaperawatan+'">'
         +'<td style="padding: 14px 0px 10px 0px;">'
             +'<div class="col-md-12"><a id="hapusrencana" jumlah="'+jumlahrencanaperawatan+'" '+tooltip(' Hapus Rencana ')+' class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash-o"></i></a></div>'
             +'<div class="form-group" style="margin: 1px;">'
                +'<input type="hidden" name="index_perencanaan[]" value="'+jumlahrencanaperawatan+'" />'
                 +'<div class="col-sm-5"><label>ICD Rencana Perawatan</label><select class="select2 form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'"  name="icd[]"><option value="0">Pilih ICD Perawatan</option></select></div>'
                 +'<div class="col-sm-2"><label>Jumlah Tindakan</label><input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value=""><small class="text text-red">#jumlah setiap kali perawatan</small></div>'
                 +'<div class="col-sm-3"><label>Jumlah Per Hari</label><input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value=""><small class="text text-red">#jumlah tindakan / hari. Ex: 1 hari 3 x, maka diisi 3</small></div> '
                 +'<div class="col-sm-2"><label>Selama [ Hari ]</label><input type="text" class="form-control" style="width:100%;" name="selama[]" value=""><small class="text text-red">#berapa hari tindakan dilakukan</small></div>'
             +'</div>'

             +'<a style="margin-left:15px;" id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="BHP/Obat Include Perawatan"><i class="fa fa-plus-circle"></i> Tambah Obat/Bhp</a>'
             +'<div class="form-group" style="margin: 2px;">'
                 +'<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label></div> '
                 +'<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jumlah Pesan</label></div> '
                 +'<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label></div>'
                 +'<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-3"><label>Jumlah Pakai (Per Tindakan)</label></div>'
             +'</div>'
         +'</td>'
     +'</tr>'+jeda;
        $('#rencanaperawatan').append(rencana);
       tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5",$('select[name="idinap"]').val()); //ambil data icd
       $('[data-toggle="tooltip"]').tooltip();
    }
});

function isicombobhp(id)//cari bhp
{
    $('select[id="'+id+'"]').select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
        stopLoading();
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            // var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return { results: data }
            
        },              
    }
    });
}

//basit, clear
function isicombosatuan(id, selected)
{
    if (is_empty(localStorage.getItem('dataSatuan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_satuan',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataSatuan', JSON.stringify(result));
                fillcombosatuan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombosatuan(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombosatuan(id, selected);
            },
            error: function(result){
                fillcombosatuan(id, selected);
            }
        });
    }
}

//basit, clear
function fillcombosatuan(id, selected)
{
    var datasatuan = JSON.parse(localStorage.getItem('dataSatuan'));
    var i=0;
    for (i in datasatuan)
    {
      $("#"+id).append('<option ' + ((datasatuan[i].idsatuan===selected)?'selected':'') + ' value="'+datasatuan[i].idsatuan+'">'+datasatuan[i].namasatuan+'</option>');
    }
    $("#"+id).select2();
}