var dttable='';
$(function () {
    listdata();
})

function listdata()
{ 
  dttable = $('#dttable').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : true,"stateSave": true,"order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cadmission/dt_blacklistpasien',
     "type": "POST"},
     "columnDefs": [{ "targets": [],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {
    $('[data-toggle="tooltip"]').tooltip(); 
  },
// -- Function that is called every time DataTables performs a draw.
 });
}
//muat ulang data pasien member 
$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  dttable.state.clear();
  dttable.ajax.reload();
});
//load data pasien member
function reloaddata()
{
    dttable.ajax.reload(null,false);
}
//tambah member
$(document).on("click",'#add',function(){formtambahedit();});
//hapus member
$(document).on('click','#delete',function(){
    var norm = $(this).attr('norm');
    var idmember = $(this).attr('idmember');
    $.confirm({
        title: 'Hapus Member',
        content: '.',
        closeIcon: true,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Hapus',
                btnClass: 'btn-warning',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cadmission/hapusmember', //alamat controller yang dituju (di js base url otomatis)
                        data: {norm:norm, idmember:idmember}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            location.reload();
                            return false;
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
    
});

function formtambahedit()
{
    var modalTitle = 'Blacklist Pasien';
    var modalContent = '<form action="" id="forminput" autocomplete="off">' +
                            '<input type="hidden" name="mode" value="Blacklist" />'+
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Pilih Pasien</label>' +
                                    '<select id="norm" name="norm" class="form-control select2"><option value="">-Pilih-</option></select>' +
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group">' +
                                '<div class="col-md-12"  style="padding-top:8px;">' +
                                    '<label>Pilih Poli / Unit</label>' +
                                    '<select  id="idunit" name="idunit"  class="form-control select2"><option value="">-Pilih-</option></select>' +
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group">' +
                                '<div class="col-md-12"  style="padding-top:8px;">' +
                                    '<label>Alasan</label>' +
                                    '<textarea class="form-control" name="alasan"></textarea>' +
                                '</div>'+
                            '</div>'+
                            
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="norm"]').val()==''){
                        alert('Pilih Pasien.');
                        return false;
                    }else if( $('select[name="idunit"]').val()=='' || $('select[name="idunit"]').val()=='0'){
                        alert('Pilih Poli.');
                        return false;
                    }else if( $('textarea[name="alasan"]').val()==''){
                        alert('Alasan Blacklist.');
                        return false;
                    }
                    else{
                        $.ajax({ 
                            url:  base_url+'cadmission/insert_blacklistpasien',type:"POST",dataType:"JSON",data:$('#forminput').serialize(),
                            success: function(r){
                                notif(r.status,r.message);
                                reloaddata();
                            },
                            error: function(result){fungsiPesanGagal();return false;}
                        });  
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        get_datapoli('#idunit', '');
        $('.select2').select2();
        select2serachmulti($('#norm'),'cpelayanan/kasir_getdtpasien');
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}


$(document).on('click','#blacklistpasien',function(){
   var namaunit = $(this).attr('namaunit');
   var norm     = $(this).attr('norm');
   var idunit   = $(this).attr('idunit');
   var nama     = $(this).attr('nama');
   var mode     = $(this).attr('mode');
   $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: mode+' Pasien Untuk Poli/Unit '+namaunit+' .? <br> NORM : '+norm+'<br> NAMA : '+nama,
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/insert_blacklistpasien",
                    type:"POST",
                    dataType:"JSON",
                    data:{norm:norm,idunit:idunit,mode:mode},
                    success:function(result){
                        notif(result.status, result.message);//panggil fungsi popover message
                        reloaddata();
                    },
                    error:function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    }); 
});