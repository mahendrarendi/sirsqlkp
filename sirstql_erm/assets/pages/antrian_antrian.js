'use strict';
function beriAntrian(idp,idu,idpr,id,nobaris='')
{
    $.ajax({ 
        url: base_url+'cpelayanan/setNomorantrian',
        type : "post",      
        dataType : "json",
        data : { vidp:idp, vidu:idu, vidpr:idpr,vidpend:id },
        success: function(result) {
            if (result.err == 'ERR_LOKET')
            {
                notif('danger', 'Maaf, antrian tidak dapat diproses karena Loket untuk ' + result.namaunit + ' belum didefinisikan. Silakan kontak Admin.')
            }
            else
            {
                var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                      <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                          '<tr><td><b>Antrian  Periksa</b></td></tr>'+
                          '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ result.noantrian +'</span></td></tr>'+
                          '<tr><td><span>'+ result.dokter['namadokter'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.namaloket +'</span></td></tr>'+
                          '<tr><td>Periksa: <span>'+ result.dokter['waktu'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.datapasien['norm'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.datapasien['namalengkap'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.datapasien['tanggallahir'] +'</span>/<small>'+ result.datapasien['usia'] +'</small></td></tr>'+
                          '<tr><td><div>'+ result.datapasien['alamat'] +'</td></tr>';
                    fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
                    
                    dtpendaftaran.ajax.reload();
            }
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    });
}

function cetakAntrian(idpr,idp)
{
    $.ajax({ 
        url: base_url+'cpelayanan/antrian_cetakantrian',
        type : "post",      
        dataType : "json",
        data : { idpr:idpr, idp:idp},
        success: function(result) {
                var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                      <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                          '<tr><td><b>Antrian  Periksa</b></td></tr>'+
                          '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ result.ap['noantrian'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.ap['dokter'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.ap['namaloket'] +'</span></td></tr>'+
                          '<tr><td>Periksa: <span>'+ result.ap['waktu'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.ap['norm'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.ap['namalengkap'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.ap['tanggallahir'] +'</span>/<small>'+ result.ap['usia'] +'</small></td></tr>'+
                          '<tr><td><div>'+ result.ap['alamat'] +'</td></tr>';

                    fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
                    
                    // if(nobaris!==''){pendaftaranListRow(id,nobaris);}
            // }
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    });
}

