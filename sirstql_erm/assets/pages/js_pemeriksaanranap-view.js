var norm = localStorage.getItem('norm');//siapkan norm -> ambil dari localstorage norm
var idpendaftaran = localStorage.getItem('idpendaftaranranap');

$(function(){
    
    localStorage.removeItem('dataDokter');
    $('#riwayatdtpasien').attr('norm',norm);
    $('.select2').select2();
    pemeriksaanranapDetailPasien();//panggil fungsi (cari data pasien)
    $('input[name="idrencanamedispemeriksaan"]').val(localStorage.getItem('idrencanamedispemeriksaan'));
    pemeriksaanRefreshVitalsign();//list vital sign
    pemeriksaanRefreshRadiologi(); //list radiologi
    
    tampilHasilRadiografi(idpendaftaran,localStorage.getItem('idrencanamedispemeriksaan'),'view');
    diagnosa_or_tindakan($('select[name="carivitalsign"]'),1);
    diagnosa_or_tindakan($('select[name="cariradiologi"]'),5);
    if (!(peranperan.includes("12") && peranperan.length == 1))
    {
        pemeriksaanranapRefreshBhp();//list bhp
        pemeriksaanRefreshTindakan(); //list tindakan
        pemeriksaanRefreshLaboratorium();//list laboratorium
        cariRanapRencanaBhpFarmasi($('select[name="caribhp"]'));
        //load dropdown diagnosa/tindakan/radiologi/laboratorium 
        diagnosa_or_tindakan($('select[name="caridiagnosa"]'),2);
        diagnosa_or_tindakan($('select[name="caritindakan"]'),3);
        diagnosa_or_tindakan($('select[name="carilaboratorium"]'),4);
    }
    
});
function cetak_hasillab()//download hasil laboratorium
{
    var printData='', style='', i=0;
    var idpendaftaran = $('input[name="idpendaftaran"]').val(), idunit = $('input[name="idunit"]').val(); 
    $.ajax({
        type:"POST",
        url:"laboratorium_hasilpemeriksaan",
        data:{x:idpendaftaran,y:idunit},
        dataType:"JSON",
        success:function(value){
            printData = '<img src="'+base_url+'/assets/images/headerlaboratorium.svg">';
            printData += '<img src="'+base_url+'/assets/images/garis.svg"><table>';
            printData += '<table border="0" style="width: 100%"><thead><tr style="text-align:left; background-image:url('+base_url+'/assets/images/background-tabel.svg)"><th>Jenis</th><th>Parameter</th><th>Hasil</th><th>NilaiDefault</th><th>NilaiRujukan</th><th>Satuan</th></thead><tbody>';
            var  i=0, nilai='', jenisicd='', paketpemeriksaan='';
            for(i in value)
            {   
            if(value[i].istext==='1') { nilai = value[i].nilaitext; }
            else{ nilai = value[i].nilai;}

            jenisicd = ((value[i].jenisicd==jenisicdsebelum)?'':'<tr><td class="bg-warning" colspan="6" ><b>'+value[i].jenisicd+'</b></td></tr>');
            paketpemeriksaan = ((value[i].namapaketpemeriksaanparent==null) ? '' : '<tr><td colspan="6">'+ ((value[i].namapaketpemeriksaanparent==namapaketpemeriksaanparent) ? '': '<b>'+value[i].namapaketpemeriksaanparent+'</b></td>') +'</tr>');
            var namapaketpemeriksaanparent = value[i].namapaketpemeriksaanparent;
            printData = printData + jenisicd + paketpemeriksaan;
            if(icd != value[i].icd && value[i].icd!=null)//jika icd sama lewati
            {
            printData = printData  +'<tr>'+
                        '<td >'+((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum || value[i].namapaketpemeriksaan==namapaketpemeriksaanparent)?'': value[i].namapaketpemeriksaan)+'</td>'+
                        '<td>'+value[i].namaicd+'</td>'+
                        '<td>'+ ((nilai==null) ? '' : nilai) +'</td>'+
                        '<td>'+((value[i].nilaidefault==null) ? '' : value[i].nilaidefault  )+'<input type="hidden" name="icd[]" value="'+value[i].icd+'"><input type="hidden" name="istext[]" value="'+value[i].istext+'"></td>'+
                        '<td>'+value[i].nilaiacuanrendah+tandaPenghubung(value[i].nilaiacuantinggi,'-')+value[i].nilaiacuantinggi+'</td>'+
                        '<td>'+value[i].satuan+'</td>'+
                        '</tr>';
            }
                    var icd = value[i].icd;
                    var jenisicdsebelum=value[i].jenisicd;
                    var namapaketpemeriksaansebelum = value[i].namapaketpemeriksaan;
            }
            printData +='</tbody></table>';
            fungsi_cetaktopdf(printData,style);
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function cariRanapRencanaBhpFarmasi(select)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_listrencanabhp",
        data:{
            i:localStorage.getItem('idrencanamedispemeriksaan')
        },
        dataType:"JSON",
        success:function(result){
            var html='<option>Pilih</option>';
            for (x in result) {
                html+='<option value="'+result[x].idrencanamedisbarang+'">'+result[x].namabarang+'</option>';
            }
            select.empty();
            select.html(html);
        },
        error:function(result){fungsiPesanGagal();return false;}
    });
}
// mahmud, clear
// pemeriksaan_settrencanainputbhp
function rencanaInputBhp(value)
{
    if(value!='0')
    {
        $.ajax({
            type:"POST",
            url:"pemeriksaanranap_ubahstatusrencanabhp",
            data:{x:value, i:localStorage.getItem('idrencanamedispemeriksaan')},
            dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                if(result.status=='success'){pemeriksaanranapRefreshBhp();}
            },
            error:function(result){fungsiPesanGagal();return false;}
        });
    }
}
// mahmud, clear
function pemeriksaanranapRefreshBhp()//refreshDataBHP
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listrencanabhp',
        data: {i:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            var html = '', i=0, no=0, subtotal=0, total=0;jumlahambil=0;
            for (i in result)
            {
                html += '<tr id="listrencanabhp'+ ++no +'"><td>'+ no +'</td><td>'+result[i].namabarang+'</td><td>'+ result[i].jumlah+'</td><td>'+result[i].namasatuan+'</td><td>'+convertToRupiah(result[i].harga)+'</td><td>'+convertToRupiah(result[i].total)+'</td><td>'+result[i].statuspelaksanaan+'</td></tr>';
            }
            $('#viewDataBhp').empty();
            $('#viewDataBhp').html(html);
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
        },
        error: function(result) {fungsiPesanGagal();return false;}
    });
}
// mahmud, clear
function batalPlotBhp(no)
{
    $('#tombolbatal'+no+'').remove();
    $('.tooltip').hide();
}
// mahmud, clear
function update_jumlahrencanabhp(x,y)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatejumlahrencanabhp",
        data:{y:y, z:$('#rencanabhp_jumlah'+x).val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function ubahStatusRencanaBhp(barang, status)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_ubahstatusrencanabhp",
        data:{b:barang, s:status},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanranapRefreshBhp();} //refresh rencana bhp
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusRencanaBhp(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus rencana Bhp/Obat?',
        buttons: {
            confirm: function () {//konfirm         
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusrencanabhp',
                    data: {x:value},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanranapRefreshBhp();}
                    },
                    error: function(result){fungsiPesanGagal();return false;}
                });
            },
            cancel: function () {//cancel            
            }            
        }
    });
}
function editBhpGrup(value)//seting atau ubah grup bhp
{
    var d = $('#grup'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'grup',x,y,value);
}
function editBhpJumlah(value)//seting atau ubah grup bhp
{
    var d = $('#jumlah'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#harga'+value).val();
    settingBhp(d,'jumlah',x,y,value);
}
function editBhpKekuatan(value)//seting atau ubah grup bhp
{
    var d = $('#kekuatan'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#harga'+value).val();
    settingBhp(d,'kekuatan',x,y,value);
}
function editBhpSetHarga(value)//seting atau ubah grup bhp
{
    var d = $('#harga'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'harga',x,y,value);
}
function settingBhp(d,field,x,y,no)
{
    $.ajax({ 
        url: 'pemeriksaan_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:field, x:x,y:y },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                pemeriksaanranapRefreshBhp();
                setTimeout(function(){
                    // if(field=='grup'){$('#jumlah'+no).focus();}
                    // if(field=='jumlah'){$('#kekuatan'+no).focus();}
                    if(field=='jumlah'){$('#kekuatan'+no).focus();}
                    else{$('#jumlah'+(no+1)).focus();}
                },300);
                
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

// mahmud, clear
//////////////////////////Vital Sign////////////////
function pilihvitalsign(value,ispaket='')
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilvitalsign",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran,ispaket:ispaket, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshVitalsign();} //refresh vitalsign
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function pemeriksaanRefreshVitalsign()//refreshDataVitalsign
{
    var x=0, html='', no=0 ;
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listvitalsign',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            
            for(x in result)
            {
                no++;
                html += ((namapaket!=result[x].namapaketpemeriksaan && result[x].idpaketpemeriksaan!=null)? '<tr height="30px"><td class="bg-warning text-bold" colspan="5">'+result[x].namapaketpemeriksaan+'</td><td class="bg-warning">'+result[x].statuspelaksanaan+'</td><tr>' : ''); 
                if(result[x].icd!=null)
                {
                    html += '<tr id="listrencanavitalsign'+no+'"><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                    '<td>'+ ((result[x].istext=='text')? result[x].nilaitext : result[x].nilai )+'</td>'+ 
                    '<td>'+ result[x].nilaidefault +'</td>'+
                    '<td>'+ result[x].nilairujukan +'</td>'+
                    '<td>'+ result[x].satuan +'</td>'+
                    '<td>'+ result[x].statuspelaksanaan+'</td></tr>';
                }
                var namapaket = result[x].namapaketpemeriksaan;
            }
            $('#listVitalsign').empty();
            $('#listVitalsign').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); return false;
        }
    });
}
// mahmud, clear
function ubahStatusVitalsign(v1, v2, v3)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilvitalsign",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshVitalsign();} //refresh vitalsign
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusVitalsign(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan vitalSign?',
        buttons: {
            confirm: function () {//konfirm         
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapusvitalsign',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshVitalsign();}
                    },
                    error: function(result){fungsiPesanGagal();return false;}
                });
            },
            cancel: function () {//cancel            
            }            
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////TINDAKAN////////////////
//basit, clear
function ubahStatusTindakan(v1, v2, v3)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpemeriksaan",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

//basit, clear
function editDokter(value)
{
    $.ajax({ 
        url: 'pemeriksaanranap_ubahdokterhasilpemeriksaan',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedishasilpemeriksaan'+value).val(), id:$('#iddokter'+value).val() },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//basit, clear
function pilihtindakan(value)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpemeriksaan",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

//---basit, clear
function pemeriksaanRefreshTindakan()//refreshDataDiagnosa
{
    var x=0,no=0, html='',nobhp=0, subtotal=0;
    var idrencanamedishasilpemeriksaansebelum = '';
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listtindakan',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {

            for(x in result.tindakan)
            {
                //isi tindakan
                if (result.tindakan[x].idrencanamedishasilpemeriksaan != idrencanamedishasilpemeriksaansebelum)
                {
                    ++no;
                    subtotal += parseInt(result.tindakan[x].total);
                    html += '<tr id="listrencanatindakan'+no+'">'+
                            '<td>'+ result.tindakan[x].icd +'</td>'+
                            '<td>'+ result.tindakan[x].namaicd +'</td>'+
                            '<td>'+result.tindakan[x].namadokter+'</td>'+
                            '<td class="text-center">'+ result.tindakan[x].jumlah +'</td>'+
                            '<td>'+ convertToRupiah(result.tindakan[x].total) +'</td>'+
                            '<td>'+ result.tindakan[x].statuspelaksanaan +'</td></tr>';
                }
                
                //isi obat untuk tindakan (jika ada)
                if (!is_empty(result.tindakan[x].idrencanamedisbarangpemeriksaan))
                {
                    var idrencana = result.tindakan[x].idrencanamedisbarangpemeriksaan;
                    html += '<tr class="bg bg-warning">\n\
                            <td align="right">|-----------&nbsp;</td>\n\
                            <td>'+result.tindakan[x].namabarang+'</td>\n\
                            <td></td>\n\</td>\n\
                            <td class="text-center">'+ result.tindakan[x].jumlahbhp+'</td>\n\
                            <td>'+result.tindakan[x].namasatuan+'</td>\n\
                            <td>'+result.tindakan[x].statuspelaksanaanbhp+'</td></tr>';
                }
                idrencanamedishasilpemeriksaansebelum = result.tindakan[x].idrencanamedishasilpemeriksaan;
            }
            html+='<tr class="bg bg-info"><td colspan="4">Subtotal</td><td colspan="3">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listTindakan').empty();
            $('#listTindakan').html(html);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}

// mahmud, clear
function pemeriksaanRefreshRadiologi()//refreshDataRadiologi
{
    var x=0, html='', subtotal=0, no=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listradiologi',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                no++;
                subtotal += parseInt(result[x].total);
                html += '<tr id="listrencanaradiologi'+no+'"><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ convertToRupiah(result[x].total) +'</td><td>'+result[x].statuspelaksanaan+'</td></tr>';
            }
            $('#listRadiologi').empty();
            $('#listRadiologi').html(html+'<tr class="bg bg-info"><td colspan="2">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusRadiologi(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan elektromedik?',
        buttons: {
            confirm: function () {//konfirm
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusradiologi',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshRadiologi();}/*tampil hasil radiologi*/
                    },
                    error: function(result) { 
                        fungsiPesanGagal();return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
// mahmud, clear
function ubahStatusRadiologi(v1, v2, v3)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilradiologi",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshRadiologi();}//tampil hasil radiologi
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
//////////////////////////END RADIOLOGI///////////
// mahmud, clear
//////////////////////////START LABORATORIUM//////
function pilihlaboratorium(value,ispaket='')
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasillaboratorium",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran,ispaket:ispaket, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshLaboratorium();} //refresh laboratorium
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function ubahStatusLaboratorium(v1, v2, v3)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasillaboratorium",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshLaboratorium();}//tampil hasil laboratorium
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function ubahStatusPakettindakan(mode,id,status,idrencanamedishasilpemeriksaan)
{
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpaket",
        data:{x:mode, y:id, z:status,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idrencanamedishasilpemeriksaan, k:localStorage.getItem("idkelas")},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){
                if(mode=='Laboratorium'){pemeriksaanRefreshLaboratorium();}
                else{pemeriksaanRefreshVitalsign();}
            }//tampil hasil laboratorium
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}



//cetak hasil laboratorium per paket
function cetak_hasillab(idpaketpemeriksaan=null)
{
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var mode='cetakpaket';
    print_laboratorium(idrencanamedispemeriksaan,mode,idpaketpemeriksaan);
}
//cetak hasil laboratorium non paket
$(document).on('click','#cetakhasillaboratnonpaket',function(){
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var mode='cetaknonpaket';
    print_laboratorium(idrencanamedispemeriksaan,mode,null);
});


// mahmud, clear
function pemeriksaanRefreshLaboratorium()//refreshDataLaboratorium
{
    var x=0, html='', paket='', paketparent='', subtotal=0,menu='', no=0, idpaket=0, keteranganhasil='';
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listlaboratorium',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan'),m:'view'},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {   
                no++;
                menu='';
                
                //tampilkan keterangan hasil laborat
                if(result[x].idpaketpemeriksaanparent != idpaketpemeriksaanparent && idpaketpemeriksaanparent!=null)
                {
                     html += '<tr class="bg bg-info"><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
                     keteranganhasil='';
                }
                    
                idpaket = ((result[x].idpaketpemeriksaanparent==null)? result[x].idpaketpemeriksaan : result[x].idpaketpemeriksaanparent );
//                menu += ' <a id="inputeditkesimpulanhasil" namapaket="'+result[x].namapaketpemeriksaanparent+'" idpaketpemeriksaan="'+result[x].idpaketpemeriksaanparent+'" class="btn btn-primary btn-xs" '+tooltip('Input/Edit Kesimpulan Hasil')+'><i class="fa fa-file-text"></i></a> ';
//                menu += ' <a id="waktupengerjaanlabranap" mode="paketl" idpaket="'+idpaket+'" namapaket="'+result[x].namapaketpemeriksaan+'" class="btn btn-info btn-xs" '+tooltip('Waktu Pengerjaan')+'><i class="fa fa-clock-o"></i></a> ';
                menu += ' <a onclick="cetak_hasillab('+idpaket+')" class="btn btn-info btn-xs" '+tooltip('Cetak Hasil')+'><i class="fa fa-print"></i></a> ';

                
                // end menu paket
                subtotal += parseInt(result[x].total);
                var paketlain = result[x].namapaketpemeriksaanparent;
                html += ((namapaket!=result[x].namapaketpemeriksaan & result[x].idpaketpemeriksaan!=null ) ? '<tr class="bg bg-gray" height="30px"><td colspan="5">'+result[x].namapaketpemeriksaan+'</td>'+  result[x].statuspelaksanaan +'</td>'+ ((result[x].namapaketpemeriksaanparent==null)? '<td>'+  convertToRupiah(result[x].total) +'</td><td><td>'+menu + '<input type="hidden" id="idrencanapaketlaboratorium'+no+'" value="'+result[x].idpaketpemeriksaan+'"/>'+((result[x].jumlahrencana)>0 ?  ' <a id="tombolbatalrencanapaketlaboratorium'+no+'" onclick="batalRencana('+no+',\'rencanapaketlaboratorium\',\'Rencana Paket Laboratorium\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Laboratorium (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : '' ) +'</td>' :'<td ></td><td >'+  result[x].statuspelaksanaan +'</td>') +'</tr>' : '');

               if(result[x].icd!=null)
               {
                    html += '<tr><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                    '<td>'+ ((result.istext=='text' || result.istext=='textnumerik') ? result[x].nilaitext : result[x].nilai ) +'</td>'+ 
                    '<td>'+ result[x].nilaidefault +'</td>'+
                    '<td>'+ result[x].nilairujukan +'</td>'+
                    '<td>'+ result[x].satuan +'</td>'+
                    '<td>'+ ((result[x].namapaketpemeriksaan!=null) ? '' : convertToRupiah(result[x].total))+'</td>'+
                    '<td>'+ result[x].statuspelaksanaan +'</td>';
                    html +='</tr>';
                }
                var namapaket=result[x].namapaketpemeriksaan;
                var namaparent=result[x].namapaketpemeriksaanparent;
                if(result[x].keteranganhasil !=null && result[x].keteranganhasil != keteranganhasil)
                {
                    keteranganhasil = result[x].keteranganhasil;
                }
                var idpaketpemeriksaanparent = result[x].idpaketpemeriksaanparent;
            }
            //tampilkan keterangan hasil laborat
            html += '<tr class="bg bg-info"><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
            
            html+='<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listLaboratorium').empty();
            $('#listLaboratorium').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal();return false;
        }
    });
    
}
// mahmud, clear
function simpanHasil_icdPeriksa(mode,no,idnilai,idhasil)
{
    var nilai = $('#'+idnilai+no).val();
    var jenisnilai = $('#idjenis'+idnilai+no).val();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_simpanHasil_icdPeriksa",
        data:{w:jenisnilai,x:mode,y:idhasil,z:nilai},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){
                if(mode=='Laboratorium'){pemeriksaanRefreshLaboratorium();}
                else{pemeriksaanRefreshVitalsign();}
            }//tampil hasil laboratorium
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear 
function hapusPakettindakan(mode,idpaket)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus paket tindakan '+mode+'?',
        buttons: {
            confirm: function () {//konfirm
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusPakettindakan',
                    data: {x:idpaket,y:mode,z:localStorage.getItem('idrencanamedispemeriksaan')},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){
                            if(mode=='Laboratorium'){pemeriksaanRefreshLaboratorium();}/*tampil hasil Laboratorium*/
                            else{pemeriksaanRefreshVitalsign();}/*tampil hasil Vitalsign*/
                        }
                    },
                    error: function(result) { 
                        fungsiPesanGagal();return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
// mahmud, clear
function hapusLaboratorium(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan laboratorium?',
        buttons: {
            confirm: function () {//konfirm
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapuslaboratorium',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshLaboratorium();}/*tampil hasil Laboratorium*/
                    },
                    error: function(result) { 
                        fungsiPesanGagal();return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
function hapuspaketparent(x,z)
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuspaketpemeriksaan',
        data: {x:x, y:idpendaftaran},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                if(z=='laboratorium')
                {
                    pemeriksaanRefreshLaboratorium(idpendaftaran);
                }
                else
                {
                    pemeriksaanRefreshVitalsign(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////diagnosa_or_tindakan//////////////////
function diagnosa_or_tindakan(x,y)
{
    startLoading();
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_diagnosa",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            stopLoading();
            return {
                q: params.term,
                jenisicd: y,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
///////////////////////////////////////////////
function pilihJenisIcd(value)//pilih jenis diagnosa
{
    pilihPaketDiagnosa($('select[name="paket"]'),value);
}
function singleDiagnosa(value)//tambah single diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addsinglediagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function paketDiagnosa(value)//tambah paket diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addpaketdiagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pilihSingleDiagnosa(selectNameTag,icd) //pilih diagnosa 
{  
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_cariicd",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                icd:icd,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.namaicd + ((item.aliasicd!='') ? ' >>> ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
function pilihPaketDiagnosa(htmlTagName,icd) //pilih diagnosa 
{  
    $.ajax({
        url:'diagnosa_pilihpaket',
        type:'POST',
        dataType:'JSON',
        data:{x:icd},
        success: function(result){
            var select='<option value="0" >Pilih</option>';
            htmlTagName.empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idpaketpemeriksaan +'" >' + result[i].namapaketpemeriksaan +'</option>';
            }
            htmlTagName.html(select);
            $('.select2').select2();
            pilihSingleDiagnosa($('select[name="single"]'),icd);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
// mahmud, clear
function updateBiayahasilperiksa(y,z)//seting hahasilpemeriksaanpaket laboratorium
{
    var idpaketygdiubah = y;
    var modalTitle = 'Ubah Biaya';
    var modalContent = '<form action="" id="Formubahhargapaket">' +
                            '<div class="form-group">' +
                            '<input type="hidden" name="id" value="'+idpaketygdiubah+'" class="form-control"/>'+
                            //jasa operator
                                '<div class="col-md-3">'+
                                    '<label>Jasa Operator</label>' +
                                    '<input type="text" name="jasaoperator" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //nakes
                                '<div class="col-md-3">'+
                                    '<label>Nakes</label>' +
                                    '<input type="text" name="nakes" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //jasars
                                '<div class="col-md-3">'+
                                    '<label>Jasars</label>' +
                                    '<input type="text" name="jasars" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //bhp
                                '<div class="col-md-3">'+
                                    '<label>Bhp</label>' +
                                    '<input type="text" name="bhp" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //akomodasi
                                '<div class="col-md-3">'+
                                    '<label>Akomodasi</label>' +
                                    '<input type="text" name="akomodasi" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //margin
                                '<div class="col-md-3">'+
                                    '<label>Margin</label>' +
                                    '<input type="text" name="margin" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //sewa
                                '<div class="col-md-3">'+
                                    '<label>Sewa</label>' +
                                    '<input type="text" name="sewa" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Update',
                btnClass: 'btn-blue',
                action: function () {
                    
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: 'savepaketygdiubah', //alamat controller yang dituju
                        data: $("#Formubahhargapaket").serialize(), //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            notif(result.status, result.message);
                            if(result.status=='success')
                            {
                                if(z=='Tindakan')
                                {
                                    pemeriksaanRefreshTindakan();
                                }
                                else if(z=='Radiologi')
                                {
                                    pemeriksaanRefreshRadiologi();
                                }
                                else
                                {
                                    pemeriksaanRefreshLaboratorium();
                                }
                            }
                        },
                        error: function(result) { //jika error
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        //tampilkan harga yang akan diubah
        tampilhargayangdiubah(idpaketygdiubah);
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function tampilhargayangdiubah(x)
{
    $.ajax({
        type:"POST",
        url:"tampilhargayangdiubah",
        data:{x:x},
        dataType:"JSON",
        success: function(result){
            $('input[name="jasaoperator"]').val(result.jasaoperator);
            $('input[name="nakes"]').val(result.nakes);
            $('input[name="jasars"]').val(result.jasars);
            $('input[name="bhp"]').val(result.bhp);
            $('input[name="akomodasi"]').val(result.akomodasi);
            $('input[name="margin"]').val(result.margin);
            $('input[name="sewa"]').val(result.sewa);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusPaketPeriksa(value)//hapus paket pemeriksaan
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = value;
    $.ajax({
        type:"POST",
        url:"delete_paket_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function deleteHasilPeriksa(value)//hapus hasil periksa per icd
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = $('#deleteHasilPeriksa'+value).val();
    $.ajax({
        type:"POST",
        url:"delete_hasil_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaan_tampilpasien() //tampil data pasien
{
	$.ajax({
		type:"POST",
		url:"pemeriksaan_tampilpasien",
		data:{x:norm, y:localStorage.getItem('idrencanamedispemeriksaan')},
		dataType:"JSON",
		success: function(result){
			$('#labNormPasien').html(': '+result.p.norm);
			$('#labNamaPasien').html(': '+result.p.namalengkap);
			$('#labPengirim').html(': '+result.per.titeldepan+' '+result.per.namalengkap+' '+result.per.titelbelakang);
			$('#labKelaminPasien').html(': '+result.p.jeniskelamin);
			$('#labTgllahirPasien').html(': '+result.p.tanggallahir);
		},
		error:function(result){
			fungsiPesanGagal();
			return false;
		}
	});
}

//////////////MENAMPILKAN DATA DETAIL PASIEN//////////////////
function pemeriksaandetailTampilPasien()
{
	$.ajax({
		type:"POST",
		url:"pemeriksaan_detail_tampil_pasien",
		data:{x:localStorage.getItem('idrencanamedispemeriksaan')},
		dataType:"JSON",
		success:function(result){
			// console.log(result);
			pemeriksaandetailTampilPasienListData(result);
		},
		error:function(result){
			fungsiPesanGagal();
			return false;
		}
	});
}
function pemeriksaandetailTampilPasienListData(data)
{
	var modalTitle = '<div class="col-sm-12 center">Detail Pasien</div>';
    var modalContent = '<div class="col-sm-6">'+
                            	'<table class="table table-striped" >'+
                            		'<tr><th>Nama</th><th style="font-weight:normal;">: '+if_empty(data.namalengkap)+'</th></tr>'+
                            		'<tr><th>No.RM</th><th style="font-weight:normal;">: '+if_empty(data.norm)+'</th></tr>'+
                            		'<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamat+'</th></tr>'+
                            		'<tr><th>Telpon</th><th style="font-weight:normal;">: '+data.telponpasien+'</th></tr>'+
                            		'<tr><th>Kelamin</th><th style="font-weight:normal;">: '+data.jeniskelamin+'</th></tr>'+
                            		'<tr><th>TanggalLahir</th><th style="font-weight:normal;">: '+data.tanggallahir+'</th></tr>'+
                            		'<tr><th>Agama</th><th style="font-weight:normal;">: '+data.agama+'</th></tr>'+
                            		'<tr><th>Status</th><th style="font-weight:normal;">: '+data.statusmenikah+'</th></tr>'+
                            		'<tr><th>Golongan Darah</th><th style="font-weight:normal;">: '+data.golongandarah+'</th></tr>'+
                            		'<tr><th>Rhesus</th><th style="font-weight:normal;">: '+data.rh+'</th></tr>'+
                            		'<tr><th>Alergi</th><th style="font-weight:normal;">: '+data.alergi+'</th></tr>'+
                            	'</table>'+
                        	'</div>'+
                        	'<div class="col-sm-6">'+
                            	'<table class="table table-striped" >'+
                            		'<tr><th>Penanggung</th><th style="font-weight:normal;">: '+data.namapenjawab+'</th></tr>'+
                            		'<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamatpenjawab+'</th></tr>'+
                            		'<tr><th>No.Telpon</th><th style="font-weight:normal;">: '+data.notelpon+'</th></tr>'+
                            		'<tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr>'+
                            		'<tr><th>Klinik</th><th style="font-weight:normal;">: '+data.namaunit+'</th></tr>'+
                            		'<tr><th>No.Antri</th><th style="font-weight:normal;">: '+data.noantrian+'</th></tr>'+
                            		'<tr><th>Dokter</th><th style="font-weight:normal;">: '+data.titeldepan+' '+data.namadokter+' '+data.titelbelakang+'</th></tr>'+
                            		'<tr><th>Carabayar</th><th style="font-weight:normal;">: '+data.carabayar+'</th></tr>'+
                            		'<tr><th>No.SEP</th><th style="font-weight:normal;">: '+data.nosep+'</th></tr>'+
                            	'</table>'+
                        	'</div>';
    $.alert({
        title: modalTitle,
        content: modalContent,
        columnClass: 'xl',
    });
}
//basit, clear
function pemeriksaanranapDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_caridetailpasien',
        data: {
            i:localStorage.getItem('idinap'),
            x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if (is_null(norm) || is_null(idpendaftaran))
            {
                norm            = result.pasien.norm;
                localStorage.setItem('norm', norm);
                idpendaftaran   = result.pasien.idpendaftaran;
                localStorage.setItem('idpendaftaranranap', idpendaftaran);
            }
            $('#waktupemeriksaan').html(result.rencana.waktu);
            $('textarea[name="anamnesa"]').val(result.rajal.anamnesa);
            $('textarea[name="keterangan"]').val(result.rajal.keterangan);
            $('textarea[name="catatan"]').val(result.pasien.catatan);            
            $('textarea[name="keteranganelektromedik"]').val(result.pasien.keteranganradiologi);
            $('textarea[name="saranelektromedik"]').val(result.pasien.saranradiologi);
            $('textarea[name="keteranganlaboratorium"]').val(result.pasien.keteranganlaboratorium);
            $('textarea[name="keteranganobat"]').val(result.pasien.keteranganobat);            
            $('textarea[name="dokter"]').val(if_empty(result.rajal.dokter));
            $('.textarea').wysihtml5({toolbar:false});
            viewIdentitasPasien(result.pasien);//panggil view identitas
            $('input[name="idpendaftaran"]').val(result.rajal.idpendaftaran);
            $('input[name="idunit"]').val(result.rajal.idunit);
            $('input[name="norm"]').val(result.pasien.norm);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
//basit, clear
function viewIdentitasPasien(value)//tampilidentitas
{
    var profile = '<div class="bg bg-gold">'
                +'<div class="box-body box-profile" >'
                 +'<div class="login-logo" style="margin-bottom:0px;">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-center">NIK : '+value.nik+'</p>'
                  +'<h5 class=" text-center"> <i class="fa fa-calendar margin-r-5"></i> '+value.tanggallahir+' /  <i>'+value.usia+'</i> </h5>'
                  +'<p class="text-center"><i>Ayah : '+value.ayah+' , Ibu : '+value.ibu+'</i></p>'
                +'</div>'
              +'</div>';
    var detail_profile = '<div class="bg bg-gold"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td>: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                 +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td>: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Tempat Lahir</strong></td>'
                  +'<td>: &nbsp;'+value.tempatlahir+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td>: &nbsp; '+value.jeniskelamin+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td>: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' 
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td>: &nbsp; '+value.namapendidikan+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i> Agama</strong></td>'
                  +'<td>: &nbsp;'+value.agama+'</td>'
                  
                +'</tr>'
        
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Alamat </strong></td>'
                  +'<td width="250px">: &nbsp; '+value.alamat+'</td>'

                  
                +'</tr>'
                +'</tbody>'
             +'</table></div></div>';
    $('#ranap_profile').html(profile);
    $('#ranap_detailprofile').html(detail_profile);
}
// mahmud, clear
function batalRencana(no,mode,pesan)
{
    var ispaket='', value = $('#id'+mode+no).val();
    if(mode=='rencanapaketlaboratorium' || mode=='rencanapaketvitalsign'){ispaket='paket';}
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Batalkan Rencana '+pesan+'? (Tidak Termasuk Yang Terlaksana)',
        buttons: {
            confirm: function () {             
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url + 'cpelayananranap/batalrencana_periksaranap',
                    data: {i:value, k:localStorage.getItem('idrencanamedispemeriksaan'),mode:mode,pesan:pesan,ispaket:ispaket}, //
                    dataType: "JSON", //tipe data yang dikirim
                    success: function(result) { //jika  berhasil
                        if(result.status=='success')
                        {
                            notif(result.status, result.message);
                            $('#tombolbatal'+mode+no).remove();
                            // $('#list'+mode+no).remove();
                        }
                        else
                        {
                            notif(result.status, result.message);
                            return false;
                        }
                    },
                    error: function(result) { //jika error
                        fungsiPesanGagal(); // console.log(result.responseText);
                        return false;
                    }
                });
            },
            cancel: function () {               
            }            
        }
    });
}