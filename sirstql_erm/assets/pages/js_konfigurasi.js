var konfigurasi='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  konfigurasi = $('#konfigurasi').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cadministrasi/dt_konfigurasi',
     "type": "POST"
 },
 "columnDefs": [{ "targets": [0,4],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  konfigurasi.state.clear();
  konfigurasi.ajax.reload();
});
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Data');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Data');});

function formtambahedit(idunik,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formkonfigurasi" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Nama</label>' +
                                    '<input type="hidden" id="id"  class="form-control" name="id" value="'+idunik+'" />' +
                                    '<input type="text" id="nama" placeholder="nama konfigurasi"  class="form-control" name="nama" value="" '+((idunik!='')?'readonly':'')+' />' +  
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Nilai</label>' +
                                    '<input type="text" id="nilai"  class="form-control" placeholder="nilai konfigurasi" name="nilai" value="" />' +  
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Keterangan</label>' +
                                    '<textarea class="form-control" id="keterangan" rows="5" name="keterangan" placeholder="Keterangan"></textarea>' +  
                                '</div>'+
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="nama"]').val()==''){
                        alert('Nama  harap diisi.');
                        return false;
                    }else if($('input[name="namabarang"]').val()==''){
                        alert('Nilai harap diisi.');
                        return false;
                    }else if($('textarea[name="keterangan"]').val()==''){
                        alert('Keterangan harap diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formready(idunik);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function inputedit(id,value)
{
    id.empty();
    id.val(value);
}
function formready(idunik)
{
    startLoading();
    $.ajax({ 
        url:  base_url+'cadministrasi/formreadykonfigurasi',type:"POST",dataType:"JSON",data:{id:idunik},
        success: function(result){
            stopLoading();
            if(result!=null){
                inputedit($('#nama'),result.nama);
                inputedit($('#nilai'),result.nilai);
                $('#keterangan').val(result.keterangan);
            }
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    });
}

function simpan()
{
    $.ajax({ 
        url:  base_url+'cadministrasi/save_konfigurasi',type:"POST",dataType:"JSON",data:$('#formkonfigurasi').serialize(),
        success: function(r){
            notif(r.status,r.message);
            konfigurasi.ajax.reload(null,false);
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}
