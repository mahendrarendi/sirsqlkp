// mahmud, clear == dipanggil dari pemeriksaan
function riwayatpasien_rajal(mode='')
{
  var norm = (($(this).attr('norm'))==undefined? localStorage.getItem('norm') : $(this).attr('norm') );
  $.ajax({url:base_url+"cadmission/riwayatrekammedispasien_rajal",dataType:"JSON",type:"POST",data:{norm:norm, mode:mode},success:function(result){lembarriwayatpasien(result,norm,mode);},error:function(){}});
}
// mahmud, clear == dipanggil dari data induk
function riwayatpasien(value)
{
  var mode='';
  localStorage.setItem('norm',value);
  var norm = localStorage.getItem('norm');
  $.ajax({url:base_url+"cadmission/riwayatrekammedispasien_rajal",dataType:"JSON",type:"POST",data:{norm:norm},success:function(result){lembarriwayatpasien(result,norm,mode);},error:function(){}});
}
// mahmud, clear
function lembarriwayatpasien(result,id,mode)
{
  var akses_salinriwayat = (sessionStorage.akses_salinriwayatperiksaralan) ? '' : 'unselectable' ;
  
  var penanggung = result.penanggung;
  var periksa = result.dtperiksa;
  var planjutan = result.planjutan;
  var anamnesis = result.dtanamnesis;
  var diagnosis = result.dtdiagnosis;
//  var obat = result.dtobat;
  var resep     = result.dtresep;
  var grup      = result.dtgrup;
  var detailperiksa = result.detailperiksa;
	var result = result.dtpasien;
	var split='&nbsp;&nbsp;&nbsp;';
	var style = '<style rel="stylesheet" media="print"> @media print { .borderleft{border-left:1px solid #2D2D2D;border-top:1px solid #2D2D2D;} .border{border-right:1px solid #2D2D2D; border-left:1px solid #2D2D2D; border-top:1px solid #2D2D2D; padding:4px; margin:0;} .jarak{padding-top:8px;}  } </style>';
	var printContents = '<div style="padding:8px;">'+
	'<img src="'+base_url+'/assets/images/headerlembarrm.svg" >'+
	'<hr><span style="font-size:30px;">Lembar Rekam Medik &nbsp;&nbsp;&nbsp;&nbsp;  No. RM.:'+id+'</span><hr>'+
	'<table>'+
		'<tr><td>NAMA LENGKAP '+ split +'</td><td>'+ result.namalengkap +'</td></tr>'+
		'<tr><td>JENIS KELAMIN '+ split +'</td><td>'+ result.jeniskelamin  +'</td></tr>'+
		'<tr><td>TGL LAHIR '+ split +'</td><td>'+ result.tanggallahir  +'</td></tr>'+
		'<tr><td>AGAMA '+ split +'</td><td>'+ result.agama  +'</td></tr>'+
		'<tr><td>PEKERJAAN '+ split +'</td><td>'+ result.namapekerjaan+'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamat +'</td></tr>'+
		'<tr><td>KELURAHAN '+ split +'</td><td>'+  result.namadesakelurahan +'</td></tr>'+
		'<tr><td>NO. TELP '+ split +'</td><td>'+ result.notelpon +'</td></tr>'+
		'<tr><td>GOL DARAH '+ split +'</td><td>'+ result.golongandarah +'</td></tr>'+
		'<tr><td>PENDIDIKAN '+ split +'</td><td>'+ result.namapendidikan +'</td></tr>'+
		'<tr><td>PEKERJAAN '+ split +'</td><td>'+ result.namapekerjaan +'</td></tr>'+
		'<tr><td>STATUS KAWIN '+ split +'</td><td>'+ result.statusmenikah +'</td></tr>'+
		'<tr><td colspan="2">&nbsp;</td></tr>'+
		'<tr><td>NAMA AYAH '+ split +'</td><td>'+ result.ayah +'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamatayah +'</td></tr>'+
		'<tr><td>NAMA IBU '+ split +'</td><td>'+ result.ibu +'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamatibu +'</td></tr>'+
		'<tr><td colspan="2">&nbsp;</td></tr>'+
        '<tr><td colspan="2">Data Penanggung</td></tr>'+
        '</table>'+
        '<table class="" border="1" cellspacing="0"><thead><tr class="bg-info"><td>&nbsp;Nama</td><td>&nbsp;Hubungan</td><td>Telpon</td><td>Alamat</td></tr></thead>';
		for(var p in penanggung){ printContents+= '<tr><td>&nbsp;'+penanggung[p].namalengkap+'</td><td>'+penanggung[p].namahubungan+'</td><td>'+penanggung[p].notelpon+'</td><td>'+penanggung[p].alamat+'</td></tr>'; }
                printContents +=
		'<table class="'+akses_salinriwayat+'" width="100%" style="margin-top:2px;" border="1" cellspacing="0" cellpadding="0"><thead>'+
		'<tr class="bg-info"><td>&nbsp;Waktu </td><td>&nbsp;Anamnesis</td><td> &nbsp;Pemeriksaan </td>'+((mode=='print')?'':'<td> &nbsp;Catatan </td>')+'<td>&nbsp;Diagnosa & Terapi </td><td>&nbsp;BHP/OBAT </td><td>&nbsp;DOKTER </td></tr>';
                    for( i in periksa)
                    {
			  var list = periksa;
			  printContents +='<tr class="bg-warning"><td>&nbsp;'+list[i].tanggal+'<br> &nbsp;'+list[i].waktu+'</td>'+'<td style="padding:2px;">';
			  
        
                        // list anamnesis
                        for( var ii in detailperiksa[i])
                        {
                            printContents+=if_null(detailperiksa[i][ii].anamnesa)+'<br>';
                        } 
                          
                        for( x in anamnesis[i])
                        {
                          var listanamnesis = anamnesis[i];
                          printContents +=listanamnesis[x].hasil_anamnesis+'<br>';
                        }
		    printContents +='</td><td style="padding:2px;">';
		    // list pemeriksaan
                    for( var xx in  detailperiksa[i])
                    {
                        printContents += if_null(detailperiksa[i][xx].keterangan)+'<br>';
                    }
                        printContents += if_null(detailperiksa[i][xx].keteranganradiologi)+((detailperiksa[i][0].saranradiologi=='')?'':'<b>Saran:</b>')+detailperiksa[i][0].saranradiologi+'<br>'+if_null(detailperiksa[i][0].keteranganlaboratorium)+'<br>';


		    for( z in planjutan[i])
		    {
		      var listplanjutan = planjutan[i];
		      printContents +=if_null(listplanjutan[z].hasil_diagnosis)+'<br>';
		    }
		    printContents += ((mode=='print')?'':'</td><td style="padding:2px;">'+if_null(detailperiksa[i][0].rekomendasi)+'<br>');
		    // <td style="padding:2px;">''</td>
                    
			  printContents +='</td><td style="padding:2px;">';
			  
                          for(var zz in detailperiksa[i])
                          {
                              printContents+=if_null(detailperiksa[i][zz].diagnosa);
                          }
                          
                          printContents += '<ol style="padding:14px;">';
			  // list diagnosis
			  for( a in diagnosis[i])
			  {
			    var listdiagnosis = diagnosis[i];
			    printContents += '<li>'+listdiagnosis[a].hasil_diagnosis+'</li>';
			  }
                          printContents += '</ol>';
			  printContents +='</td><td style="padding:2px;">';
			  // list obat
			  printContents+='<b>Keterangan Obat/BHP :</b><br>'+if_null(detailperiksa[i][0].keteranganobat)+'<br> <b>Resep Dokter:</b><br/>';
//			  for( y in obat[i])
//			  {
//			    var listobat = obat[i];
        if (detailperiksa[i][0].keteranganobat !='-') {
			    printContents += listresep(resep[i],grup[i]); 
        }
//                            listobat[y].obat+'<br>';
//			  }
			  printContents+=' </td><td style="padding:2px;">'+list[i].namadokter+' <br> <i style="font-size:10px;"> Poli '+list[i].namaunit+'</i></td></tr>';
			}
			printContents += 
	'<table>'+
	'</div>';

	if(mode=='print')
	{
	  fungsi_cetaktopdf(printContents,style);
	}
	else
	{
	$('#displayRiwayatPasien').empty();
	$('#displayRiwayatPasien').html(printContents);
	}
	$.alert({
        title: '',content: printContents,columnClass: 'xlarge',buttons: {formReset:{text: "Kembali",btnClass: 'btn-danger btn-xs'},
        formSubmit:{text: "Print",btnClass: 'btn-primary btn-xs',action: function () {riwayatpasien_rajal('print');}}},
	});
}

function listresep(result_resep,result_grup)
{
    var cetakresep='<table>';
    var no=0,no2=1;
    var totaldiresepkan = 0;
    for(var x in result_resep)
    {
        var resep = result_resep[x];
        totaldiresepkan = (resep.kekuatanperiksa/resep.kekuatan) * resep.jumlahdiresepkan;
        ((resep.grup==0 || grup!==resep.grup)? no=1 : no +=1  );
        cetakresep +=  "<tr style='font-size:11.5px;'><td colspan='2'>"+((resep.grup==0 || grup!==resep.grup)?'<b>R/</b>':'&nbsp;&nbsp;&nbsp;')+"  "+resep.namabarang+ ' ' +((resep.jumlahRacikan!=null)? (totaldiresepkan/resep.jumlahRacikan).toFixed(3) : totaldiresepkan ) + ' '+ ((resep.grup==='0')?(( resep.pemakaian===null)?'': resep.pemakaian):'') +" </td></tr>";
        if (resep.grup==0)
        {
            cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+((resep.signa===null)?'':resep.signa)+"</td></tr>";
            cetakresep += ((resep.signa===null)?'':"<tr  style='font-size:6px;' ><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>") ;
        }
        for(var i in result_grup)
        {
            if(resep.grup!=0 && result_grup[i].jumlahgrup==no && resep.grup==result_grup[i].grup )
            {
                cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<i>"+resep.kemasan+" dtd no "+resep.jumlahRacikan+"</i></td></tr>";
                cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+resep.signa+"</td></tr>";
                cetakresep +="<tr style='font-size:6px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
            }
        }
        var grup = resep.grup;
    }
    cetakresep += '</table>';
    return cetakresep;
}