function elektromedik_cetakexpertiseranap(idrencana)
{
    var  style='', i=0;
    $.ajax({
        type:"POST",
        url:base_url+"creport/elektromedik_hasilpemeriksaanranap",
        data:{idrencana:idrencana},
        dataType:"JSON",
        success:function(value){
            stopLoading();
            var printData = '<div style="float: none;"><img style="opacity:0.6;filter:alpha(opacity=60);width:9cm" src="'+base_url+'assets/images/'+value.header+'" />\n';
            var keteranganradiologi = value.rencana.keteranganradiologi+((value.rencana.saranradiologi=='')?'':'<b>Saran:</b>')+value.rencana.saranradiologi;
            var diagnosa = '';
            var elektromedik = ''; 
            for (var x in value.diagnosa) { 
                diagnosa += '<span style="display:block;">'+value.diagnosa[x].icd+' '+value.diagnosa[x].namaicd;+'</span>';
            }
            for (var y in value.elektromedik) { elektromedik += '<span style="display:block;">'+value.elektromedik[y].icd+' '+value.elektromedik[y].namaicd;+'</span>';}
            var identitas = value.identitas;
            var garis= '<div style="border-top:1px solid #3a3a3a;"></div>';
            var borderlr= 'border-left:1px solid #3a3a3a;border-right:1px solid #3a3a3a;';
            var borderbt= 'border-bottom:1px solid #3a3a3a;border-top:1px solid #3a3a3a;';
            var style1='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            var style2='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            // printData +=garis;
            printData += '<table '+style1+'>';
            printData += '<tr><td>N.Periksa/N.RM </td><td>:</td> <td >'+identitas.idpemeriksaan+'/'+identitas.norm+'</td><td>D.Pengirim</td> <td>:</td><td style="height:auto;width:35%;">'+value.identitasranap.namadokter+'</td><tr>';
            printData += '<tr><td>Nama</td>             <td>:</td> <td style="height:auto;width:35%;">'+ identitas.identitas +' '+identitas.namalengkap+'</td><td>Asal</td>   <td>:</td><td style="height:auto;width:35%;">'+value.identitasranap.namabangsal+' No. '+value.identitasranap.nobed+'</td><tr>';
            printData += '<tr><td>J. Kelamin</td>        <td>:</td> <td>'+identitas.jeniskelamin+'</td><td>T. Periksa</td><td>:</td><td>'+value.identitasranap.waktu+'</td><tr>';
            printData += '<tr><td>Usia</td>             <td>:</td> <td>'+identitas.usia+'</td><td>Alamat</td> <td>:</td><td style="height:auto;width:35%;">'+identitas.alamat+'</td><tr>';
            printData += '</table>';
            printData += garis;
            printData += '<table '+style1+'><tr ><th>Diagnosis Klinis</th><th>Pemeriksaan Radiologis</th></tr>';
            printData += '<tr><td style="height:auto;width:50%;">'+ diagnosa  +'</td><td style="height:auto;width:50%;">'+elektromedik+'</td></tr></table>';
            printData += garis;
            printData += '<table '+style2+'><tr><td style="padding:5px;text-align:justify;">'+keteranganradiologi+'</td></tr></table>';
            printData += '<table style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br><br><br><br><br></td></tr><tr><td>'+value.dokterpenilai+'<br>'+value.sipdokterpenilai+'</td></tr></table>';
            var style = '<style>@page {size:21cm 29.7cm;}</style>';
            fungsi_cetaktopdf(printData,style);
        },
        error:function(result){
            stopLoading(); fungsiPesanGagal(); return false;
        }
    });
}