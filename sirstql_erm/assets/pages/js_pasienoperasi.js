'use strict';
var tblpasienoperasi;
$(function(){
    
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    tblpasienoperasi = $('#listpo').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],"ajax": {"data":{tanggal:function(){return $('#tanggalawal').val()},filtertanggal:function(){return $('input[name="filtertanggal"]:checked').val();}},"url": base_url+'cpelayanan/listpo',"type": "POST"},"columnDefs": [{ "targets": [ 2 ],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
});
$(document).on('click','#tampil',function(){ tblpasienoperasi.ajax.reload(); });
$(document).on('click','#refresh',function(){$('input[type="search"]').val('').keyup();tblpasienoperasi.state.clear();$('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now'); tblpasienoperasi.ajax.reload(); });
$(document).on('change','#filtertanggal',function(){ tblpasienoperasi.ajax.reload(); });
$(document).on('click','#terlaksana',function(){var ur = $(this).attr('ur');var data = $(this).attr('ido');confirm(ur,data,'terlaksana');});
$(document).on('click','#detail',function(){
    window.location.href=base_url + 'cpelayanan/detailoperasi';
});
$(document).on('click','#batal',function(){
    var ur = $(this).attr('ur');
    var data = $(this).attr('ido');
    confirm(ur,data,'batal');
});
function confirm(ur,data,mode)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Operasi '+((mode=='batal') ? 'batal' : 'telah' ) +' dilaksanakan?',
        buttons: {
            ok: function () {                
                ajax('cadmission/'+ur,{id:data});
            },
            batal: function () {               
            }            
        }
    });
}
function ajax(url,datapost)
{
    $.ajax({ 
        url: base_url+url,
        type : "post",      
        dataType : "json",
        data : datapost,
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                tblpasienoperasi.ajax.reload();
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                    
            notif(result.status, result.message);
            return false;
        }

    }); 
}