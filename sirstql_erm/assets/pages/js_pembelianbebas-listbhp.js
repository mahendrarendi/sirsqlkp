"use strict";
$(function(){
    localStorage.removeItem('idbarangpembelibebas');
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
    listobat();
});

var list='';
function listobat()
{
  list = $('#listpembelibebas').DataTable({
    "dom": '<"toolbar">frtip',"stateSave" : true,"processing": true,"serverSide": true,"searching" : true,"paging": true,"ordering" : true,"info": true,"lengthChange": true,
    "ajax": {
      "data":{tanggal:function(){return $('input[name="tanggal"]').val();}, ispegawai:function(){return $('input[name="ispegawai"]:checked').val(); } },
      "url": base_url+"cpelayanan/dt_pembelianbebas",
      "type": "POST"
    },
    "columnDefs":[{"targets": [4,7], "orderable": false},],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { /*This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.*/
      $(nRow).attr('style', qlstatuswarna(aData[6]));
    },
    "drawCallback": function (settings, json) {/*-- Function that is called every time DataTables performs a draw.*/
      $('[data-toggle="tooltip"]').tooltip();
    },
  });
}

$(document).on('click','#refresh',function(){

  $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate','now');
  $('input[type="search"]').val('').keyup();
  list.state.clear();
  list.ajax.reload();
});
$(document).on('click','#tampil',function(){list.ajax.reload();});
$(document).on('click','#ispegawai', function(){list.ajax.reload();});
$(document).on('click','#ubah', function(){localStorage.setItem('idbarangpembelibebas',$(this).attr('i')); window.location.href=base_url+"cpelayanan/pembelianbebas_bhp";})
$(document).on('click','#selesai', function(){
  var id = $(this).attr('i');
  $.confirm({
        icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi',content: 'Selesaikan.?',
        buttons: {
          simpan: function(){pob_selesaibatal(id,'selesai');},
          batal: function(){}
        }
    });
})
$(document).on('click','#batal', function(){
  var id = $(this).attr('i');
  $.confirm({
        icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi',content: 'Batalkan.?',
        buttons: {
          simpan: function(){pob_selesaibatal(id,'batal');},
          batal: function(){}            
        }
    });
})
/*selesai atau batal obat bebas*/
function pob_selesaibatal(id,status)
{
  $.ajax({
    type: "POST",url: base_url + 'cpelayanan/selesaipob',data:{i:id, status:status},dataType: "JSON",
    success: function(result){list.ajax.reload();},
    error: function(result){fungsiPesanGagal();return false;}
  });
}

function simpanpembayaran(idbp,idtagihan)
{
    var tunai = unconvertToRupiah($('#txttunai').val());
    var transfer = unconvertToRupiah($('#txttransfer').val());
    var potonggaji = unconvertToRupiah($('#txtpotonggaji').val());
    var idbank = $('#selbanktujuan').val();
    var penanggung = $('#penanggung').val();
    //simpan bayar
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/bayarpob',
        data: {i:idbp,tunai:tunai,transfer:transfer, potonggaji:potonggaji,idt:idtagihan,idbank:idbank,penanggung:penanggung},
        dataType: "JSON",
        success: function(result) {
            cetaknota(idbp);
            return list.ajax.reload();
        },
        error: function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}