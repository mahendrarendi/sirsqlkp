//mahmud, clear
var table;
$(function () 
{
  switch($('#jsmode').val()) {
    case 'aturanpakai':
        $('.select2').select2();
        setKeterangan();
      break;
    
    case 'mastertarif':
        $('.select2').select2();
        select2serachmulti($("#icd"),"cmasterdata/mastertarif_listicd"); //pencarian data icd
        select2serachmulti($("#kodetipeakun"),"cmasterdata/fillTipeakun"); //pencarian data tipe akun
        select2serachmulti($("#kodeakun"),"cmasterdata/fillAkun"); //pencarian data  akun
      break;
      
    case 'salin':
        $('.select2').select2();
        select2serachmulti($("#icd"),"cmasterdata/mastertarif_listicd"); //pencarian data icd
        select2serachmulti($("#kodetipeakun"),"cmasterdata/fillTipeakun"); //pencarian data tipe akun
        select2serachmulti($("#kodeakun"),"cmasterdata/fillAkun"); //pencarian data  akun
      break;
      
    case 'formloket':
        select2serachmulti($("#icd"),"cmasterdata/fillIcd"); //pencarian data  ICD
        $('.dropdownloket').select2();
      break;
    case 'formunit' :
      $('.select2').select2();
        break;
    default:
//      
  }
});

//hapus akun coa pendapatan
function hapusakunpendapatan()
{
    $.confirm({
        icon: 'fa fa-question',
//        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus Akun',
        content: 'Hapus COA Pendapatan.',
        buttons: {
            hapus: function () { 
                
                $('#kodeakun').html('<option value="" >- Pilih -</option>');
                select2serachmulti($("#kodeakun"),"cmasterdata/fillAkun"); //menampilkan data  akun           
            },
            batal: function () {               
            }            
        }
    });
}


// mahmud, clear
function simpan_bangsal()
{
    if($('input[name="namabangsal"]').val()=='')
    {
        $('input[name="namabangsal"]').focus();
        alert_empty("nama bangsal");
    }
    else if($('input[name="sewakamar"]').val()=='')
    {
        $('input[name="sewakamar"]').focus();
        alert_empty("sewa kamar");
    }
    else if($('input[name="keperawatan"]').val()=='')
    {
        $('input[name="keperawatan"]').focus();
        alert_empty("keperawatan");
    }
    else if($('input[name="jeniskelaminbangsal"]').val()=='')
    {
        $('input[name="jeniskelaminbangsal"]').focus();
        alert_empty("jeniskelaminbangsal");
    }
    else
    {
        $('#Formbangsal').submit();
    }
}
// mahmud, clear -> set total tarif
function sum_tarifbangsal(value)
{ 
    var total = 0, sewakamar=$('input[name="sewakamar"]').val(),keperawatan=$('input[name="keperawatan"]').val();
    total += parseFloat(isnan(sewakamar)) + parseFloat(isnan(keperawatan));
    return $('#total').val(convertToRupiah(total));
}
// mahmud, clear -> simpan bed
function simpan_bed()
{
    if($('input[name="nobed"]').val()==='')
    {
        $('input[name="nobed"]').focus();
        alert_empty("nomor bed");
    }
    else
    {
        $('#Formbed').submit();
    }
}
function setnobed_tonumber(value)
{ 
    var no='0';
    no = parseFloat(isnan(value));
    return $('#nobed').val(no);
}
/////////// JS LAMA


function simpan_propinsi()
{
  if($('#namapropinsi').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid propinsi.!'});
  }
  else
  {
    $('#FormPropinsi').submit();
  }
}

function simpan_kabupaten()
{
  if($('#namakabupaten').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kabupaten.!'});
  }
  else
  {
    $('#Formkabupaten').submit();
  }
}

function simpan_kelurahan()
{
  if($('#namakelurahan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid peran.!'});
  }
  else
  {
    $('#Formkelurahan').submit();
  }
}

function simpan_kecamatan()
{
  if($('#namakecamatan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kecamatan.!'});
  }
  else
  {
    $('#Formkecamatan').submit();
  }
}

function simpan_pendidikan()
{
  if($('#namapendidikan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenjangpendidikan.!'});
  }
  else
  {
    $('#Formpendidikan').submit();
  }
}

//menampilkan data enum pasien = add penanggungjawab
function tampil_data_enum_pegawai()
{
    $.ajax
          ({
                url:'tampil_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:''},
                success: function(result){
                    //menampilkan data statuskeaktifan pegawai
                    edit_data_enum($('#liststatuskeaktifan'),result.statuskeaktifan,'');
                    //menampilkan data kelurahan
                    edit_dropdown_gruppegawai($('#listgruppegawai'),result.gruppegawai,'');
                    //menampilkan data pendidikan
                    edit_dropdown_person($('#listperson'),result.person,'');
                },
                error: function(result){
                    console.log(result);
                }
            });
}

//form tambah grup pegawai
function tambah_gruppegawai()
{
    var modalSize = 'small';
    var modalTitle = 'Add Data Grup Pegawai';
    var modalContent = '<form action="" id="Formgruppegawai">' +
                            '<div class="form-group">' +
                                '<label>Grup pegawai</label>' +
                                '<input type="text" name="gruppegawai" placeholder="gruppegawai" id="gruppegawai" class="form-control"/>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
              
                    //jika data grup pegawai kosong
                	if( ! $('#gruppegawai').val() )
                	{
                            alert_empty('grup pegawai');
                            return false;
                	}
                    //selain itu
                	else
                	{
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'save_gruppegawai', //alamat controller yang dituju (di js base url otomatis)
                                data: $("#Formgruppegawai").serialize(), //
                                dataType: "JSON", //tipe data yang dikirim
                                //jika  berhasil
                                success: function(result) {
                                    notif(result.status, result.message);
                                    return refreshGruppegawai();

                                },
                                //jika error
                                error: function(result) {
                                    console.log(result.responseText);
                                }
                            });
                	}
                }
            },
            //menu back
            formReset:{
            	text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        //ketika form di tampilkan
        //inisialisasi dropdown select2 
        $('.select2').select2();
        //ambil data provinsi
        get_data_propinsi();

        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//fungsi ambil data grup pegawai
function get_data_pegawai()
{
    var select='';
    $.ajax({
        url:'cari_datagruppegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#Listgruppegawai').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idgruppegawai +'" >' + result[i].namagruppegawai +'</option>';
            }
            $('#Listgruppegawai').html('<select onchange="get_data_gruppegawai(this.value)" class="select2 form-control" style="width:100%" id="gruppegawai" name="gruppegawai">'+
                            '<option value="0">Pilih</option>' +
                            select +
                            '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    });	
}

// menampilkan data pegawai
function refreshpegawai()
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_pegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#pegawai').empty();
            for(i in result)
            {
                    select = select + '<option value="'+ result[i].idpegawai +'" >' + result[i].idperson +'</option>';
            }
            $('#pegawai').html(select);
            $('.select2').select2();

        },
        error: function(result){
            console.log(result);
        }
    });	
}

// menampilkan data pegawai
function refreshGruppegawai()
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_gruppegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#namagruppegawai').empty();
            for(i in result)
            {
                    select = select + '<option value="'+ result[i].idgruppegawai +'" >' + result[i].namagruppegawai +'</option>';
            }
            $('#namagruppegawai').html(select);
            $('.select2').select2();

        },
        error: function(result){
            console.log(result);
        }
    });	
}

// entri pendaftaran
function entri_pegawai()
{
    if($('textarea[name="titeldepan"]').val()==='')
    {	
        $('textarea[name="titeldepan"]').focus();
        alert_empty('titeldepan');
    }
    
    else if($('input[name="namapegawai"]').val()==='')
    {	
        $('input[name="namapegawai"]').focus();
        alert_empty('nama pegawai');
    }
	else if($('textarea[name="titelbelakang"]').val()==='')
    {	
        $('textarea[name="titelbelakang"]').focus();
        alert_empty('titelbelakang');
    }
	
    else if($('select[name="namagruppegawai"]').val()=='0')
    {	
        alert_empty('namagruppegawai');
    }
    else if($('input[name="statuskeaktifan"]').val()==='')
    {	
        $('input[name="statuskeaktifan"]').focus();
        alert_empty('statuskeaktifan');
    }
    else if($('textarea[name="nip"]').val()==='')
    {	
        $('textarea[name="nip"]').focus();
        alert_empty('nip');
    }
    else
    {
        $.ajax({
        url:'save_pegawai',
        type:'POST',
        dataType:'JSON',
        data:$('#Formpegawai').serialize(),
        success: function(result){
            console.log(result);
            // jika simpan berhasil
            if(result.norm)
            {
                $('input[name="pilihpegawai"]').val(result.norm);
            }
            notif(result.pesan.status,result.pesan.message);
            
        },
        error: function(result){
            console.log(result);
        }
    });
        
        ;
    }
}

//fungsi kosongkan inputan
function kosongkanformpegawai()
{
    $('textarea[name="titeldepan"]').val('');
	$('textarea[name="titelbelakang"]').val('');
	$('input[name="statuskeaktifan"]').val('');
    $('textarea[name="alamat"]').val('');
    $.ajax
      ({
            url:'tampil_pegawai',
            type:'POST',
            dataType:'JSON',
            data:{id:''},
            success: function(result){
				
            //menampilkan data kelurahan
            edit_dropdown_person($('#kelurahan'),result.kelurahan,'');
            //menampilkan data pendidikan
            edit_dropdown_gruppegawai($('#pendidikan'),result.pendidikan,'');
            },
        error: function(result){
            console.log(result);
        }
    }); 
    
}
// tampilkan data pasien
$("#pilihpegawai").on("keydown", function (e) {
    if (e.keyCode == 14) {
        var value = $('#pilihpegawai').val();
        var select;
        var selected;
        var i;
        //jika tidak ada data inputan
        if(!value)
        {
            kosongkanformpegawai();
        }
        //selain itu
        else
        {
            $.ajax({
                url:'tampil_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                    // jika data pegawai kosong
                    if(result.pasien==null)
                    {
                        kosongkanformpegawai();
                    }
                    // selain itu
                    else
                    {                
                        $('input[name="titeldepan"]').val(result.pegawai['titeldepan']);
                        $('input[name="idperson"]').val(result.pegawai['idperson']);
					    $('input[name="titelbelakang"]').val(result.pegawai['titelbelakang']);
                        $('input[name="nip"]').val(result.pegawai['nip']);
                        //menampilkan statuskeaktifan
                        edit_data_enum($('#statuskeaktifan'),result.statuskeaktifan,result.pegawai['statuskeaktifan']);
                        //menampilkan data jeniskelamin pasien
                        edit_dropdown_gruppegawai($('#gruppegawai'),result.gruppegawai,result.pasien['idgruppegawai']);
                        //menampilkan data pendidikan
                        edit_dropdown_person($('#namalengkap'),result.namalengkap,result.pasien['idperson']);
                    }
                    
                    
                },
                error: function(result){
                    console.log(result);
                }
            });

        }
        
          
    }
});
// cari data pegawai
$("#pilihpegawai").on("keyup", function (e) {
    if (e.keyCode != 14) {
        $('#Listpegawai').empty();
        var value = $('#pilihpegawai').val();
        var datalist='';
        var i=0;
        // console.log('tes cari', value);
        if(value)
        {
            $.ajax({
                url:'cari_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                // console.log(result);
                    for(i in result)
                    {

                        datalist = datalist + '<option value="'+ result[i].namalengkap+'">';
                    }
                    $('#Listpegawai').html(datalist);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
        
    }
});
// fungsi edit data enum (idhtml,dataenum,datayangterpilih)
function edit_data_enum(column,data,selected_data)
{
    var select='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i]==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i] +'" '+ selected +' >' + data[i] +'</option>';
    }
    column.html(select);
}



function edit_dropdown_person(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idperson==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idperson +'" '+ selected +' >' + data[i].namalengkap +'</option>';
    }
    column.html(select);
}

function edit_dropdown_gruppegawai(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idgruppegawai==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idgruppegawai +'" '+ selected +' >' + data[i].namagruppegawai +'</option>';
    }
    column.html(select);
}


//reset data pendaftaran
function reset_pegawai()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset Form?',
        buttons: {
            confirm: function () { 
                window.location.reload(true)
            },
            cancel: function () {               
            }            
        }
    });
	
}

//kembali ke halaman list pegawai
function back_pegawai()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list pegawai?',
        buttons: {
            confirm: function () { 
                window.location.href='pegawai';
            },
            cancel: function () {               
            }            
        }
    });
    
}

function simpan_gruppegawai()
{
  if($('#namagruppegawai').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid grup pegawai.!'});
  }
  else
  {
    $('#Formgruppegawai').submit();
  }
}

function simpan_instalasi()
{
  if($('#namainstalasi').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid instalasi.!'});
  }
  else
  {
    $('#Forminstalasi').submit();
  }
}

function simpan_unit()
{
  if($('#namaunit').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Nama Unit Harus Diisi.'});
  }
  else if($('#akronimunit').val()==='')
  {
      $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Kode Unit Harus Diisi.'});
  }
  else
  {
    $('#Formunit').submit();
  }
}

function back_barang() //kembali ke halaman list pegawai poli
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list barang?',
        buttons: {
            confirm: function () { 
                window.location.href='barang';
            },
            cancel: function () {               
            }            
        }
    });   
}


function simpan_barang()
{
  if($('#namabarang').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid barang.!'});
  }
  else
  {
    $('#Formbarang').submit();
  }
}

function simpan_pbf()
{
    if($('#namapbf').val()==='')
    {
        $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please Provide A Valid PBF.!'});
    }
    else
    {
        $('#Formpedagangbesarfarmasi').submit();
    }
}
function simpan_jenisicd()
{
  if($('#jenisicd').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenisicd.!'});
  }
  else
  {
    $('#Formjenisicd').submit();
  }
}

function simpan_distributor()
{
if($('#distributor').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid distributor.!'});
  }
  else if($('#alamat').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid alamat.!'});
  }
  else if($('#telepon').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid telepon.!'});
  }
  else
  {
    $('#Formdistributor').submit();
  }
}

function simpan_barangpabrik()
{
if($('#namapabrik').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid input.!'});
  }
  else
  {
    $('#Formbarangpabrik').submit();
  }
}

function simpan_jenispenyakit()
{
  if($('#jenispenyakit').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenispenyakit.!'});
  }
  else
  {
    $('#Formjenispenyakit').submit();
  }
}

simpan_jenispenyakit
function simpan_sediaan()
{
  if($('#namasediaan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid sediaan.!'});
  }
  else
  {
    $('#Formsediaan').submit();
  }
}
function simpan_satuan()
{
  if($('#namasatuan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid satuan.!'});
  }
  else
  {
    $('#Formsatuan').submit();
  }
}

function simpan_jenistarif()
{
  if($('#jenistarif').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenistarif.!'});
  }
  else
  {
    $('#Formjenistarif').submit();
  }
}

function simpan_kelas()
{
  if($('#kelas').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kelas.!'});
  }
  else
  {
    $('#Formkelas').submit();
  }
}

function simpan_mastertarif()
{
  if($('#mastertarif').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid mastertarif.!'});
  }
  else
  {
    $('#Formmastertarif').submit();
  }
}

// mahmud, clear 12 juli 2019 --> save master tarif paket pemeriksaan
function simpan_mastertarifpaketpemeriksaan()
{
  if($('#idkelas').val()==='0')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid Kelas Tarif.!'});
  }
  else if($('#idpaketpemeriksaan').val()==='0')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid Paket Tarif.!'});
  }
  else if($('#idjenistarif').val()==='0')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid Jenis Tarif.!'});
  }
  else
  {
    $('#Formmastertarifpaketpemeriksaan').submit();
  }
}

function simpan_stasiun()
{
  if($('#namastasiun').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid stasiun.!'});
  }
  else
  {
    $('#Formstasiun').submit();
  }
}

$(document).on('click','#loket_hapusicd',function(){
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus ICD',
        content: '',
        buttons: {
            hapus: function () { 
                $('#icd').html('<option value="">-Pilih-</option>')         
            },
            batal: function () {               
            }            
        }
    });
});

function simpan_loket()
{
  var idunit    = $('#idunit').val();
  var kodeloket = $('#kodeloket').val();
  if($('#namaloket').val()==='')
  {
    alert('Nama Loket Harap Diisi.');
  }
  else if(idunit == "" && kodeloket == "")
  {      
    alert('Jika Unit Tidak Dipilih, Harap masukan kode loket, untuk keperluan kodebooking pasien.');
  }
  else if(idunit !== "" && kodeloket !== "")
  {      
    alert('Jika Unit Dipiplih, Harap kosongkan kode loket, untuk keperluan kodebooking pasien.');
  }
  else
  {
    $('#Formloket').submit();
  }
}

//fungsi untuk cek kode loket apakah sudah digunakan
function cek_kodeloket(kode)
{
    startLoading();
    $.ajax({
        url: base_url +'cmasterdata/cek_kodeloket',
        type:'POST',
        dataType:'JSON',
        data:{kodeloket:kode,idloket:$('input[name="idloket"]').val()},
        success: function(result){
            stopLoading();
            if(result.status == 'warning')
            {
                $.alert(result.message);
                $('#kodeloket').val('');
            }
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
    
}

//cek kode unit
function cek_kodeunit(kode)
{
    startLoading();
    $.ajax({
        url: base_url +'cmasterdata/cek_kodeunit',
        type:'POST',
        dataType:'JSON',
        data:{akronimunit:kode,idunit:$('input[name="idunit"]').val()},
        success: function(result){
            stopLoading();
            if(result.status == 'warning')
            {
                $.alert(result.message);
                $('#akronimunit').val('');
            }
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

// -- sum tarif rumah sakit
function sum_tarifrs(value)
{ 
    var total = 0, jasaoperator=$('input[name="jasaoperator"]').val(),nakes=$('input[name="nakes"]').val(),jasars=$('input[name="jasars"]').val(),bhp=$('input[name="bhp"]').val(),akomodasi=$('input[name="akomodasi"]').val(),margin=$('input[name="margin"]').val(),sewa=$('input[name="sewa"]').val();

    total += parseFloat(isnan(jasaoperator)) + parseFloat(isnan(nakes)) + parseFloat(isnan(jasars)) + parseFloat(isnan(bhp))+ parseFloat(isnan(akomodasi))+ parseFloat(isnan(margin))+ parseFloat(isnan(sewa));
    return $('#totaltarif').val(convertToRupiah(total));
}
// -- sum tarif paket pemeriksaan rumah sakit
// mahmud, clear 12 juli 2019
function sum_pakettarifrs(value)
{ 
    var total = 0, jasaoperator=$('input[name="jasaoperator"]').val(),nakes=$('input[name="nakes"]').val(),jasars=$('input[name="jasars"]').val(),bhp=$('input[name="bhp"]').val(),akomodasi=$('input[name="akomodasi"]').val(),margin=$('input[name="margin"]').val(),sewa=$('input[name="sewa"]').val();

    total += parseFloat(isnan(jasaoperator)) + parseFloat(isnan(nakes)) + parseFloat(isnan(jasars)) + parseFloat(isnan(bhp))+ parseFloat(isnan(akomodasi))+ parseFloat(isnan(margin))+ parseFloat(isnan(sewa));
    return $('#totaltarif').val(convertToRupiah(total));
}

function simpan_golongan()
{
  if($('#golongan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid golongan.!'});
  }
  else
  {
    $('#FormGolongan').submit();
  }
}
// save aturan pakai barang
function simpan_aturanpakai()
{
  if($('#kodeaturanpakai').val()==='')
  {
    $('#kodeaturanpakai').focus();
    $.alert('kode aturan pakai tidak boleh kosong');
  }
  else if($('#pemakaian').val()==='')
  {
    $('#pemakaian').focus();
    $.alert('pemakaian tidak boleh kosong');
  }
  else if($('#waktupakai').val()==='')
  {
    $('#waktupakai').focus()
    $.alert('waktupakai tidak boleh kosong');
  }
  else if($('#kalisehari').val()==='')
  {
    $('#kalisehari').focus();
    $.alert('kalisehari tidak boleh kosong');
  }
  else
  {
    $('#FormAturanpakai').submit();
  }
}
// setKeteranganAturanPakai
function setKeterangan()
{   var pemakaian   = $('select[name="pemakaian"]').val();
    var waktupakai  = $('select[name="waktupakai"]').val();
    var kalisehari  = $('input[name="kalisehari"]').val();
    var jumlah      = $('input[name="jumlah"]').val();
    // console.log(kalisehari);
    $('#keterangan').val(kalisehari+' x sehari '+ ((waktupakai==='-')?'':waktupakai+' makan ') + jumlah+ ' '+pemakaian);
}
// -- cari golongan by jenis
function barang_carigolbyjenis(value)
{
    if(value!='0')
    {
        var dthtml='';
        $.ajax({
            url: base_url +'cmasterdata/barang_carigolbyjenis',
            type:'POST',
            dataType:'JSON',
            data:{jenis:value},
            success: function(result){
                $('#golongan').empty();
                for (i in result )
                {
                    dthtml += '<option value="'+result[i].idgolongan+'">'+result[i].golongan+'</option>';
                }
                $('#golongan').html(dthtml);
                $('.select2').select2();
            },
            error: function(result){
                // console.log(result);
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
// setInputAngka('hargabeli'); // --lihat file sirstql line 175
setInputAngka('persenmargin'); // --lihat file sirstql line 175
// -- set harga jual
function barang_sethargajual()
{
    var total = 0, hargabeli=$('input[name="hargabeli"]').val(), persenmargin = $('input[name="persenmargin"]').val();

    total += (parseInt(isnan(hargabeli)) * (parseInt(isnan(persenmargin))/100) ) + parseInt(isnan(hargabeli)) ;
    // console.log(total);
    return $('input[name="hargajual"]').val(total);
}