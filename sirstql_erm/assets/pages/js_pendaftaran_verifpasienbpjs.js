"use strict";
var listdata='';
$(function(){
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    //loaddtpendaftaran();// tampilkan data pendaftaran sekarang/hari ini
});



function loaddtpendaftaran() //fungsi panggil data pendaftaran hari ini
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtverifpasienbpjs",
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}

function listPasien(){
    //$('#tglawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((localStorage.getItem('tglawal')==undefined)? 'now' : localStorage.getItem('tglawal') ) );
    //$('#tglakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((localStorage.getItem('tglakhir')==undefined)? 'now' : localStorage.getItem('tglakhir') ) );
    listdata = $('#listdata').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],"ajax": {"data":{tglawal:function(){return $('#tglawal').val();}, tglakhir:function(){return $('#tglakhir').val();} },"url": base_url+'cadmission/dt_verifikasiklaim',"type": "POST"},"columnDefs": [{ "targets": [ ],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', qlstatuswarna(aData[10]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}
function pendaftaranlistdt(result) // -- menampilkan data pendaftaran ke html
{   
    var menuVerif = '' ;
    var bgVerif='';
    var beriantrian='',no=0;
    var tampildt = '<table style="font-size: 12px;" id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'+
              '<thead>'+
              '<tr >'+
                '<th>No</th>'+
                '<th>NO.RM</th>'+
                '<th>PASIEN</th>'+
                // '<th>NO.SEP</th>'+
                // '<th>NO.RUJUKAN</th>'+
                '<th>W.DAFTAR</th>'+
                '<th>W.PERIKSA</th>'+
                '<th>C.DAFTAR</th>'+
                '<th>P.KLINIK</th>'+
                '<th>PEMERIKSAAN</th>'+
                '<th>C.BAYAR</th>'+
                '<th></th>'+
                '<th width="100px;"></th>'+
              '</tr>'+
              '</thead>'+
              '<tbody >';
    for(i in result)
    {   
        ++no ;
        // jika cara bayar jknnonpbi/jnkpbi
        menuVerif='<a onclick="formInacbg('+result[i].idpendaftaran+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Formulir INA-CBG"><i class="fa fa-print"></i></a> ';
        menuVerif+='<a onclick="resumepasien('+result[i].idpendaftaran+')" class="btn  '+((result[i].resumeprint=='1')?'bg-black':'btn-primary')+' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN"><i class="fa fa-print"></i></a> ';
        
        if(result[i].carabayar!=='asuransi lain' && result[i].isverif!='1'){
            menuVerif +='<a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',1,'+no+','+result[i].resumeprint+')" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Verif BPJS"><i class="fa fa-check"></i></a>';
            menuVerif += ' <a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',3,'+no+','+result[i].resumeprint+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
        }else if(result[i].carabayar=='asuransi lain' && result[i].isverif!='1'){//selain itu atau jika pasien mempunyai jaminan selain jkn
        
            menuVerif+='<a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',1,'+no+','+result[i].resumeprint+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Verif Asuransi Lain"><i class="fa fa-check"></i></a>';
            menuVerif += ' <a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',3,'+no+','+result[i].resumeprint+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
        }else{
            menuVerif += ' <a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',2,'+no+','+result[i].resumeprint+')" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Batal Verif"><i class="fa fa-warning"></i></a>';
            menuVerif += ' <a onclick="setVerifPendaftaran('+result[i].idpendaftaran+',3,'+no+','+result[i].resumeprint+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
        }
        // jika pasien sudah terverifikasi
        if(result[i].isverif=='1') bgVerif='background-color:#e0f0d8;';
        else if(result[i].isverif=='2') bgVerif='background-color:#f3df96;'; //selain itu atau gagal verif
        else if(result[i].isverif=='3') bgVerif='background-color:#f0D8D8';
        else bgVerif='';

        tampildt += '<tr id="row'+no+'" style="'+bgVerif+'">'+ //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
           '<td><input type="hidden" id="mode'+no+'" value="'+result[i].carabayar+'"/>'+no+'</td>'+
           '<td>'+result[i].norm+'</td>'+
           '<td>'+result[i].namalengkap+'</td>'+
           // '<td>'+result[i].nosep+'</td>'+
           // '<td>'+result[i].norujukan+'</td>'+
           '<td>'+result[i].waktu+'</td>'+
           '<td>'+result[i].waktuperiksa+'</td>'+
           '<td>'+result[i].caradaftar+'</td>'+
           '<td>'+result[i].namaunit+'</td>'+
           '<td>'+result[i].jenispemeriksaan+'</td>'+
           '<td>'+result[i].carabayar+'</td>'+
           '<td>'+((result[i].isdibayar != "1" && result[i].isselesaisemua == "0")?'BLM SLS':'DIBAYAR')+'</td>'+
           '<td id="menu'+no+'">'+menuVerif+'</td></tr>';
    }
    $('#tampildt').empty();
    $('#tampildt').html(tampildt+'</tfoot></table>');//
    $('[data-toggle="tooltip"]').tooltip();//inisialisasi tooltips
    $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,});
    menubar_table();//panggil fungsi tambahkan menu di atas tabel
}

function cari_dataverifbytanggal() /// -- cari berdasarkan range tanggal
{   
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggallahir').val();
    if(tglawal=='')
    {
        $('#tanggallahir').val('')
        alert_empty('tanggal awal');
    }
    else
    {
        $.ajax({ 
            url: base_url+'cadmission/loaddtverifpasienbpjs',
            type : "post",      
            dataType : "json",
            data : { tgl1:ambiltanggal(tglawal), tgl2:ambiltanggal(tglakhir) },
            success: function(result) {
                stopLoading();
                pendaftaranlistdt(result);
                $('#tanggalawal').val(tglawal);
                $('#tanggallahir').val(tglakhir);
            },
            error: function(result){
                stopLoading();                 
                fungsiPesanGagal();
                return false;
            }
        });
    }  
}

$(document).on("click","#verifDaftarPasienBpjs", function(){
    var idpendaftaran = $(this).attr("alt");
    var nobaris = $(this).attr("nobaris");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Yakin berkas sudah ditemukan.?',
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/pendaftaran_updateverifpasienbpjs",
                    type:"POST",
                    dataType:"JSON",
                    data:{id:idpendaftaran},
                    success:function(result){
                        notif(result.status, result.message);//panggil fungsi popover message
                        setTimeout(function(){window.location.reload(true);},2000);

                    },
                    error:function(result)
                    {
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    });
});

/////////FUNGSI VERIFIKASI PENDAFTARAN//////
// $(document).on("click","#verifpasienbpjs", function(){
function setVerifPendaftaran(id,status,no,stsprintverif){
    var menuVerif='', mode=$('#mode'+no).val();
    menuVerif='<a onclick="formInacbg('+id+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Formulir INA-CBG"><i class="fa fa-print"></i></a> ';
    menuVerif+='<a onclick="resumepasien('+id+')" class="btn '+((stsprintverif=='1')?'bg-black':'btn-primary')+' btn-xs" data-toggle="tooltip" data-original-title="RESUME PASIEN"><i class="fa fa-print"></i></a> ';
    if(mode!=='asuransi lain' && status!='1')
    {
        menuVerif+='<a onclick="setVerifPendaftaran('+id+',1,'+no+','+stsprintverif+')" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Verif BPJS"><i class="fa fa-check"></i></a>';
        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
    }
    else if(mode=='asuransi lain' && status!='1')//selain itu atau jika pasien mempunyai jaminan selain jkn
    {
        menuVerif+='<a onclick="setVerifPendaftaran('+id+',1,'+no+','+stsprintverif+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Verif Asuransi Lain"><i class="fa fa-check"></i></a>';
        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
    }
    else
    {
        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',2,'+no+','+stsprintverif+')" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Batal Verif"><i class="fa fa-warning"></i></a>';
        menuVerif += ' <a onclick="setVerifPendaftaran('+id+',3,'+no+','+stsprintverif+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Gagal Verif"><i class="fa fa-ban"></i></a>';
    }
    if(status=='2' || status=='3')
    {
        $.ajax({
            type: "POST",
            url: base_url+'cadmission/updateverifpasienbpjs',
            data:{id:id, status:status},
            dataType: "JSON",
            success: function(result) {
                // deleteRow('row'+nobaris);
                if(status=='2'){bg='#f3df96';}//selain itu atau gagal verif
                else if(status=='3'){bg='#f0D8D8';}
                $('#row'+no).css('background-color',bg);
                $('#menu'+no).empty();
                $('#menu'+no).html(menuVerif);
                notif(result.status, result.message);
            },
            error: function(result) {
                fungsiPesanGagal();
                return false;
            }
        });
    }
    else
    {
    // FORM VERIFIKASI
    var modalTitle = 'Verifikasi Pendaftaran Pasien';
    var modalContent = '<form action="" id="Formverifpasienbpjs">' +
                            '<input type="hidden" name="carabayar" class="form-control"/>' +
                            '<input type="hidden" name="caradaftar" class="form-control"/>' +
                            '<input type="hidden" name="idperson" class="form-control"/>' +
                            '<input type="hidden" name="idpendaftaran" class="form-control"/>' +
                            '<input type="hidden" name="nopesan" class="form-control"/>' +
                            '<input type="hidden" name="unit" id="unitterpilih"/>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">Nama Pasien</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nama" class="form-control" readonly/>' +
                                '</div>'+
                                '<div>&nbsp;</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">No.Identitas(KTP)</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nik" placeholder="No Kartu Tanda Penduduk" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">TanggalPeriksa</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" id="tanggalverif" onchange="cari_poliklinik(this.value)" name="tanggal" class="datepicker form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                            '<div class="form-group">' +
                                '<label class="label-control col-sm-3">PoliTujuan</label>' +
                                '<div class="col-sm-8">' +
                                    '<select id="poliklinik" name="politujuan" class="form-control select2" style="width:100%;"><option value="0">Pilih</option></select>'+
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.JKN</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nojkn" placeholder="No Jaminan Kesehatan Nasional" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.SEP</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nosep" placeholder="No Surat Eligibilitas Peserta" class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 jkn" style="margin:4px 0px;"></div>'+
                            '<div class="form-group jkn">' +
                                '<label class="label-control col-sm-3">No.Rujukan</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="norujukan" placeholder="No Rujukan " class="form-control"/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
                            '<div class="form-group skdp">' +
                                '<label class="label-control col-sm-3">No.SKDP</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="noskdp" placeholder="No SKDP " class="form-control" readonly/>' +                                
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12 skdp" style="margin:4px 0px;"></div>'+
                            '<div class="form-group skdp">' +
                                '<label class="label-control col-sm-3">No.Kontrol</label>' +
                                '<div class="col-sm-8">' +
                                '<input type="text" name="nokontrol" placeholder="No Kontrol " class="form-control" readonly/>' +                                
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',
        buttons: {
            formSubmit: {
                text: 'Verif',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="nik"]').val()==='') //jika no namapenanggung kosong
                    {
                        alert_empty('nik');
                        return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST",
                            url: base_url+'cadmission/saveverifpasienbpjs',
                            data: $("#Formverifpasienbpjs").serialize(),
                            dataType: "JSON",
                            success: function(result) {
                                // deleteRow('row'+nobaris);
                                $('#row'+no).css('background-color','#e0f0d8');
                                $('#menu'+no).empty();
                                $('#menu'+no).html(menuVerif);
                                notif(result.status, result.message);
                                if($('input[name="caradaftar"]').val()=='online' || $('input[name="noskdp"]').val()!==''){setTimeout(function(){cetakAntrianAnjungan(result.antrian, result.dokter,result.pasien);},500);}
                                // pendaftaranListRow(id,nobaris);//tampil per baris
                            },
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { 
        //saat form dijalankan
        // tambahkan kode yang diinginkan
        getverifpasienbpjs(id);//ambil data verifikasi
        $('[data-toggle="tooltip"]').tooltip();
        $('.datepicker').datepicker({'autoclose':true,'format': "dd/mm/yyyy",'startDate':new Date(),'orientation':"bottom"});
        $('.select2').select2();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
    }
}
// fungsi ambil data pasien yang akan diverifikasi
function getverifpasienbpjs(idpendaftaran)
{
    $.ajax({
        type:"POST",
        dataType:"JSON",
        url:base_url+"cadmission/getverifpasienbpjs",
        data:{idpendaftaran:idpendaftaran},
        success:function(result){
            // jika jenis pembayaran mandiri
            $('input[name="nik"]').val(result.nik);
            $('input[name="idperson"]').val(result.idperson);
            $('input[name="unit"]').val(result.idjadwal);
            $('input[name="idpendaftaran"]').val(result.idpendaftaran);
            $('input[name="nopesan"]').val(result.nopesan);
            $('input[name="nama"]').val(result.namalengkap);
            $('input[name="carabayar"]').val(result.carabayar);
            $('input[name="caradaftar"]').val(result.caradaftar);
            $('input[name="noskdp"]').val(result.noskdpql);
            $('#tanggalverif').val(result.tanggal);
            cari_poliklinik($('#tanggalverif').val());
            // inputan JKN
            $('input[name="nojkn"]').val(result.nojkn);
            $('input[name="nosep"]').val();
            $('input[name="norujukan"]').val();
            // jika noskdpl tidak kosong atau pasien kunjungan kedua
            if(result.noskdpql=='0'){$('.skdp').hide();}
            else{$('.skdp').show();$('input[name="nokontrol"]').val(result.nokontrol);}
        },
        error:function(){

        }
    });
}

// cetak antrian pasien anjungan
function cetakAntrianAnjungan(antrian,dokter,pasien)
{
    var cetak = '<div style="width:6cm;float: none;padding-left:4px;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                  <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                      '<tr><td><span style="padding-left:25px;font-size:80px;margin:0; text-align:center;"">'+ antrian['no'] +'</span></td></tr>'+
                      '<tr><td><span>'+ dokter['namadokter'] +'</span></td></tr>'+
                      '<tr><td><span>'+ antrian['namaloket'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['norm'] +'</span></td></tr>'+
                      '<tr><td><span>'+ pasien['namalengkap'] +'</span></td></tr>'+
                      '<tr><td><div>'+ pasien['alamat'] +'</td></tr>'+
                    '<tr><td></div>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
}
// cari jadwal poli
function cari_poliklinik(value) //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    $.ajax({
        type: "POST",
        url: 'cari_jadwalpoli',
        data:{date: ambiltanggal(value)},
        dataType: "JSON",
        success: function(result) {
            // console.log(result);
             edit_dropdown_poliklinik($('#poliklinik'),result,$('#unitterpilih').val());//menampilkan poliklinik
             $('#unitterpilih').val('');
        },
        error: function(result) {
            console.log(result.responseText);
        }
    });
}

function edit_dropdown_poliklinik(column,data,selected_data) //fungsi edit poliklinik
{
    // console.log(name);
    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(i in data)
        {
            select = select + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" '+ if_select(data[i].idjadwal,selected_data) +' >' + data[i].namaunit +' '+ data[i].namadokter +' ' +if_empty(data[i].tanggal)+' </option>';
        }
        column.html('<option value="0">Pilih</option>'+ select );
        $('.select2').select2();
    }   
}

// mahmud, clear :: form ina-cbg
function formInacbg(idp)
{
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/verif_getidentitaspasien',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result) {
    var icdd='', icdt='',nicdd='', nicdt='';
    for(x in result.hp)
    {
       if(result.hp[x].idjenisicd==2){icdd += result.hp[x].icd+'<br>'; nicdd += result.hp[x].namaicd+'<br>';}else{icdd += '';nicdd += '';}
       if(result.hp[x].idjenisicd!=2){icdt += result.hp[x].icd+'<br>'; nicdt += result.hp[x].namaicd+'<br>';}else{icdt += '';nicdt += '';}
    }
    var nominal = result.nominal;
    var result = result.identitas;
    var bordertd='border:0.7px solid; padding:0.7px 5px;';
    var cetak = '<div style="height:2.5cm;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;""><table border="0"><tr><td><img style="margin-bottom:1px; width:6cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td><td style="padding-left:10px;font-size:15px;"> <b>FORMULIR <br>VERIFIKASI INA-CBG</b></td><td style="font-size:12px;padding-left:10px;"><span style="border:1px solid #000;padding:3px 4px;font-weight:bold;">RAWAT JALAN</span></td></tr></table></div>';
        cetak +='<div style="height:6.5cm;float: none; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
        cetak +='<table border="0" style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
        cetak +='<tr><td>1.</td><td>Nama RS</td><td ><span style="'+bordertd+'">RSU QUEEN LATIFA</span></td>     <td>2.</td><td>Kelas RS</td><td ><span style="'+bordertd+'">D</span></td></tr>';
        cetak +='<tr><td>2.</td><td>Nomor Kode RS</td><td > <span style="'+bordertd+'">3404045</span></td>       <td>4.</td><td>Cara Bayar</td><td ><span style="'+bordertd+'">'+result['carabayar']+'</span></td></tr>';
        cetak +='<tr><td>5.</td><td>Nomor Rekam Medis</td><td><span style="'+bordertd+'">'+result['norm']+'</span></td></tr>';
        cetak +='<tr><td>6.</td><td>Nama Pasien</td><td colspan="5"><span style="'+bordertd+'">'+result['identitas']+' '+result['namalengkap']+'</span></td></tr>';
        cetak +='<tr><td>7.</td><td>Jenis Perawatan</td><td><span style="'+bordertd+'">'+result['jenispemeriksaan']+'</span></td>       <td>8.</td><td>Poliklinik</td><td ><span style="'+bordertd+'">'+result['namaunit']+'</span></td></tr>';
        cetak +='<tr><td>9.</td><td>Total Biaya</td><td><span style="'+bordertd+'"> '+nominal+' </span></td>           <td>10.</td><td>Berat Lahir</td><td ><span style="'+bordertd+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Kg/Gram</td></tr>';
        cetak +='<tr><td>11.</td><td>Tanggal Masuk</td><td><span style="'+bordertd+'">'+result['tglperiksa']+'</span></td></tr>';
        cetak +='<tr><td>12.</td><td>Jumlah Hari Perawatan</td><td><span style="'+bordertd+'">1 Hari</span></td> <td>13.</td><td>Tanggal Lahir</td><td ><span style="'+bordertd+'">'+result['tanggallahir']+'</span></td></tr>';
        cetak +='<tr><td>14.</td><td>Usia</td><td><span style="'+bordertd+'">'+result['usia']+'</span></td></tr>';
        cetak +='<tr><td>15.</td><td>Jenis Kelamin</td><td><span style="'+bordertd+'">'+result['jeniskelamin']+'</span></td></tr>';
        cetak +='<tr><td>16.</td><td>Cara Pulang</td><td><span style="'+bordertd+'">'+result['kondisikeluar']+'</span></td></tr>';
        cetak +='</table></div>';

        cetak +='<div style="float: none; width:14cm; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
        cetak +='<table border="1" cellspacing="0" cellspadding="0"  style="text-align:center;width:100%;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
        cetak +='<tr align="left"><td colspan="6">Kode Diagnosa</td></tr>';
        cetak +='<tr ><td rowspan="2">NO</td> <td rowspan="2">KODE ICD-10</td><td rowspan="2" style="width:25%;">DIAGNOSIS</td><td colspan="2">KASUS</td><td rowspan="2">TTD DOKTER</td> </tr>';
        cetak +='<tr><td>L</td><td>B</td> </tr>';
        cetak +='<tr style="height:130px;"><td></td> <td>'+icdd+'</td> <td style="font-size:9px;text-align:left;">'+nicdd+'</td> <td></td> <td></td> <td  rowspan="6"> '+result['dokter']+'</td> </tr>';

        cetak +='<tr align="left"><td colspan="5">Kode Tindakan</td></tr>';
        cetak +='<tr><td>NO</td> <td>KODE ICD-9</td><td colspan="3">TINDAKAN/OPERASI</td></tr>';
        cetak +='<tr style="height:130px;"> <td></td> <td>'+icdt+'</td><td colspan="3" style="font-size:9px;text-align:left;">'+nicdt+'</td> </tr>';
    fungsi_cetaktopdf(cetak,'<style>@page {size:15cm 21cm; margin:0.2cm;}</style>');
    },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}

