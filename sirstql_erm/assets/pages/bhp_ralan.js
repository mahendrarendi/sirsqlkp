function pemeriksaanklinikRefreshBhp(idpendaftaran)//refreshDataBHP
{
    var modelist   = $('input[name="modesave"]').val();
    var jenisrawat = localStorage.getItem('jenisrawat');
    $.ajax({
        type: "POST",
        url: base_url+'cpelayanan/obatbhpralan_refresh',
        data: {
            i:idpendaftaran, 
            jenisrawat:jenisrawat,
            modelist:modelist
        },
        dataType: "JSON",
        success: function(result) {
            var penggunaan = result.penggunaan;
            var result = result.bhpperiksa;
            var lisBhp = '', i=0, no=0, subtotal=0, total=0, totaldiresepkan=0,totaldiberikan=0,jmlresepdiberikan=0;
            for (i in result)
            {
                total = result[i].total.toString();
                total = total.slice(0, -3);
                total = parseInt(total);
                totaldiresepkan = (result[i].kekuatanperiksa/result[i].kekuatan) * result[i].jumlahdiresepkan;
                totaldiberikan = (result[i].kekuatanperiksa/result[i].kekuatan) * result[i].jumlahpemakaian;
                jmlresepdiberikan = (result[i].jumlahRacikan*totaldiresepkan) / result[i].jumlahpemakaian  ;
                subtotal += parseInt(result[i].total);
                ++no;
                if(modelist=='verifikasi')
                {
                    if(grup!==result[i].grup && result[i].grup!=0)
                    {
                        lisBhp+='<tr class="bg bg-gray">\n\
                                    <td colspan="2"><b>Racikan '+result[i].grup+'</b> </td> \n\
                                    <td colspan="2">Jumlah Racik Diresepkan<sup>2)</sup> :<input id="jumlahRacikan_ralan'+result[i].grup+'" onchange="editBhpRacikanRalan('+result[i].grup+')" size="1" placeholder="Jumlah" type="text" class="form-pelayanan" value="'+result[i].jumlahRacikan+'" />\n\
                                    <td></td>\n\
                                    <td colspan="2">Kemasan Racik : <select class="form-pelayanan" id="P_kemasan_ralan'+result[i].grup+'" onchange="editBhpKemasanRalan('+result[i].grup+')"><option value="0">Kemasan</option></select></td>\n\
                                    <td></td>\n\
                                    <td colspan="2">Jumlah Racik Diberikan<sup>5)</sup> : <input id="jumlahRacikDiberikan_ralan'+result[i].grup+'" onchange="editBhpRacikDiberikanRalan('+result[i].grup+')" " size="1" placeholder="Jumlah" type="text" class="form-pelayanan" value="'+ result[i].jumlahRacikDiberikan +'" /></td>\n\
                                    <td>&nbsp;'+carapenggunaan(no,result[i].penggunaan)+'</td>\n\
                                    <td><select  style="display:inline;" class="sel2 form-control" onchange="editBhpAturanpakaiRalan('+no+')" id="P_aturanpakai_ralan'+no+'" name="P_aturanpakai_ralan'+no+'"><option value="0">Pilih</option></select></td>\n\
                                    <td> '+ ((jenisrawat=='ranap') ? '' : menuUbahAsuransiHasilPemeriksaan(result[i].grup,result[i].jaminanasuransi,'grupobat'))+' <a onclick="cetakBhpAturanpakaiRajal('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a></td>\n\
                                </tr>';
                        isicombokemasan('P_kemasan_ralan'+result[i].grup ,result[i].idkemasan);
                    }
                    lisBhp += '<tr id="listBhp'+ no +'" '+((result[i].grup==0) ? '' : 'class="bg bg-info"' )+' ><td>'+ no +'</td>'+
                              '<td>'+result[i].namabarang+ ((result[i].kadaluarsa==0) ? '' : '<br/>[Exp: '+result[i].kadaluarsa+' ]' ) +' </td>'+
                              '<td>'+result[i].jumlahdiresepkan+'</td>'+
                              '<td>'+result[i].kekuatan+'</td>'+
                              '<td>'+result[i].kekuatanperiksa+'</td>'+
                              '<td>'+result[i].grup+'</td>'+
                              '<td class="bg bg-danger">'+totaldiresepkan+'</td>'+
                              '<td>'+convertToRupiah(total)+'</td>'+
                              '<td>'+totaldiberikan+'</td>'+
                              '<td class="bg bg-info"><b>'+result[i].jumlahpemakaian+'</b></td>'+
                              '<td>'+ ((result[i].grup==0) ? '<select id="penggunaan_ralan'+no+'" class="form-pelayanan" onchange="editBhppenggunaanRalan('+no+')" name="penggunaan"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select>' : '') +'</td>'+
                              '<td>'+ ((result[i].grup==0) ? '<select  style="display:inline;width:100%;" class="sel2 form-control" onchange="editBhpAturanpakaiRalan('+no+')" id="P_aturanpakai_ralan'+no+'" name="P_aturanpakai_ralan'+no+'"><option value="0">Pilih</option></select>' : '') +
                              '<td><a onclick="cetakBhpAturanpakaiRajal('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a> </td></tr>';

                    var grup=result[i].grup;
                }
                else
                {
                    if(grup!==result[i].grup && result[i].grup!=0)
                    {
                        lisBhp+='<tr class="bg bg-gray">\n\
                                    <td colspan="2"><b>Racikan '+result[i].grup+'</b> </td> \n\
                                    <td colspan="2">Jumlah Racik Diresepkan<sup>2)</sup> :<input id="jumlahRacikan_ralan'+result[i].grup+'" onchange="editBhpRacikanRalan('+result[i].grup+')" size="1" placeholder="Jumlah" type="text" class="form-pelayanan" value="'+result[i].jumlahRacikan+'" />\n\
                                    <td></td>\n\
                                    <td colspan="2">Kemasan Racik : <select class="form-pelayanan" id="P_kemasan_ralan'+result[i].grup+'" onchange="editBhpKemasanRalan('+result[i].grup+')"><option value="0">Kemasan</option></select></td>\n\
                                    <td></td>\n\
                                    <td colspan="2">Jumlah Racik Diberikan<sup>5)</sup> : <input id="jumlahRacikDiberikan_ralan'+result[i].grup+'" onchange="editBhpRacikDiberikanRalan('+result[i].grup+')" " size="1" placeholder="Jumlah" type="text" class="form-pelayanan" value="'+ result[i].jumlahRacikDiberikan +'" /></td>\n\
                                    <td>&nbsp;'+carapenggunaan(no,result[i].penggunaan)+'</td>\n\
                                    <td><select  style="display:inline;" class="sel2 form-control" onchange="editBhpAturanpakaiRalan('+no+')" id="P_aturanpakai_ralan'+no+'" name="P_aturanpakai_ralan'+no+'"><option value="0">Pilih</option></select></td>\n\
                                    <td> '+ ((jenisrawat=='ranap') ? '' : menuUbahAsuransiHasilPemeriksaan(result[i].grup,result[i].jaminanasuransi,'grupobat'))+' <a onclick="cetakBhpAturanpakaiRajal('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a></td>\n\
                                </tr>';
                        isicombokemasan('P_kemasan_ralan'+result[i].grup ,result[i].idkemasan);
                    }
                    var jumlahdiresepkan = ((result[i].jumlahdiresepkan=='0.00') ? '' : result[i].jumlahdiresepkan );
                    
                    var menubarang = '';                    
                    if( result[i].grup==0 && jenisrawat !='ranap')
                    {
                        menubarang += menuUbahAsuransiHasilPemeriksaan(result[i].idbarangpemeriksaan,result[i].jaminanasuransi,'obat');
                    }
                    
                    if( result[i].grup==0 )
                    {
                        menubarang += ' <a onclick="cetakBhpAturanpakaiRajal('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a>';
                    }
                    
                    if( result[i].grup==0 && result[i].jenisinput != 'sisaranap')
                    {
                        menubarang += ' <a onclick="hapusBhpRalan('+no+')" data-toggle="tooltip" data-original-title="Hapus" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>';
                    }
                    //menu untuk input obat diberikan (unit ugd,farmasi)
                    menubarang += ((sessionStorage.idunitterpilih=='0') ? '' : ' <a id="detailpemberian" idbarang="'+result[i].idbarang+'" jumlah="'+totaldiresepkan+'" idp="'+result[i].idbarangpemeriksaan+'" namaobat="'+result[i].namabarang +'" data-toggle="tooltip" data-original-title="Detail Pemberian" class="btn btn-primary btn-xs"><i class="fa fa-arrows-alt"></i></a>');
                    
                    lisBhp += '<tr id="listBhp'+ no +'" '+((result[i].grup==0) ? '' : 'class="bg bg-info"' )+' >'+
                              '<td>'+ no +'</td>'+
                              '<td><input type="hidden" id="idbarangpemeriksaan_ralan'+no+'" value="'+result[i].idbarangpemeriksaan+'">'+result[i].namabarang +' </td>'+
                              '<td><input type="text" onchange="editBhpJumlahRalan('+no+')" id="jumlah_ralan'+no+'" size="4" class="form-pelayanan"  name="jumlah[]" value="'+jumlahdiresepkan+'" '+ ((result[i].jenisinput=='sisaranap') ? 'disabled style="cursor:no-drop"' : '' )+'></td>'+
                              '<td>'+result[i].kekuatan+'</td>'+
                              '<td><input type="text" onchange="editBhpKekuatanRalan('+no+')" id="kekuatan_ralan'+no+'" size="1" class="form-pelayanan" name="kekuatan[]" value="'+result[i].kekuatanperiksa+'" '+ ((result[i].jenisinput=='sisaranap') ? 'disabled style="cursor:no-drop"' : '' )+'></td>'+
                              '<td><input type="text" onchange="editBhpGrupRalan('+no+')" id="grup_ralan'+no+'" size="1" class="form-pelayanan"  name="grup[]" value="'+result[i].grup+'" '+ ((result[i].jenisinput=='sisaranap') ? 'disabled style="cursor:no-drop"' : '' )+'></td>'+
                              '<td class="bg bg-danger">'+totaldiresepkan+'</td>'+
                              '<td>'+convertToRupiah(total)+'</td>'+
                              '<td><input type="text" onchange="editBhpJumlahdiberikanRalan('+no+','+result[i].kekuatan+')" id="jumlahdiberikan_ralan'+no+'" size="1" '+((result[i].grup!='0' || result[i].jenisinput=='sisaranap')?'disabled style="cursor:no-drop"':'')+'  class="form-pelayanan" value="'+totaldiberikan+'"/></td>'+
                              '<td class="bg bg-info"><b>'+result[i].jumlahpemakaian+'</b></td>'+
                              '<td>'+ ((result[i].grup==0) ? '<select id="penggunaan_ralan'+no+'" class="form-pelayanan" onchange="editBhppenggunaanRalan('+no+')" name="penggunaan"><option value=""'+((result[i].penggunaan=='')?'selected':'')+'>Pilih</option> <option '+((result[i].penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> <option '+((result[i].penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> <option '+((result[i].penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option></select>' : '') +'</td>'+
                              '<td>'+ ((result[i].grup==0) ? '<select  style="display:inline;width:100%;" class="sel2 form-control" onchange="editBhpAturanpakaiRalan('+no+')" id="P_aturanpakai_ralan'+no+'" name="P_aturanpakai_ralan'+no+'"><option value="0">Pilih</option></select>' : '') +
                              '<td style="width:112px;">'+ menubarang +' <input type="hidden" id="idbarang_ralan'+no+'" value="'+result[i].idbarang+'">'+
                              '</td></tr>';
                      
                    var grup=result[i].grup;
                }
                isicomboaturanpakai('P_aturanpakai_ralan'+no ,result[i].idbarangaturanpakai);
                
            }
            $('#viewBhpRalan').empty();
            lisBhp +='<tr style="background-color:#d2d6de;"><td colspan="6">Subtotal</td><td colspan="1">'+convertToRupiah(subtotal)+'</td><td colspan="3"></td></tr>';
            //lisBhp +='<tr><td colspan="9"> &nbsp;</td></tr><tr class="bg bg-info"><td colspan="6">Rekap Total</td><td colspan="1" id="totalPemeriksaan"></td><td colspan="3"></td></tr>';
            $('#viewBhpRalan').html(lisBhp);
            $('.sel2').select2();
            $('.select2').css('width','100%');
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

$(document).on('click','#detailpemberian',function(){
    var idb      = $(this).attr('idp');
    var namaobat = $(this).attr('namaobat');
    var jumlah   = $(this).attr('jumlah');
    var idbarang = $(this).attr('idbarang');
    var modalSize   = 'lg';
    var modalTitle  = 'Detail Pemberian Obat';
    var modalContent= '<div class="col-xs-12">\n\
                        <table class="">\n\
                            <tr><th>Obat/BHP</th><td> : ' + namaobat + '</td></tr>\n\
                            <tr><th>Jumlah Diresepkan</th><td> : ' + jumlah + '</td></tr>\n\
                            <tr><th>Jumlah Diberikan</th><td> : <span id="detailpemberian_jumlahdiberikan"></span></td></tr>\n\
                        </table>\n\
                        <div id="detailpemberian_barangdiberikan"></div>\n\
                       </div>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'lg',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {    
            //menampilkan detail pemberian obat
            $.ajax({ 
                url: base_url+'cpelayanan/get_barangpemeriksaan_detail',
                type : "POST",      
                dataType : "JSON",
                data:{idb:idb,idbarang:idbarang},
                success: function(result){
//                    $('.select2').select2();
                    var dthtml = '<tr class="bg bg-yellow-gradient"><th>Batch.No</th><th>Kadaluarsa</th><th>Stok Unit</th><th>Diberikan</th></tr>';
                    for(var x in result)
                    {
                        dthtml += '<tr>\n\
                                    <input type="hidden" id="idbp_'+x+'" value="'+ idb +'"/>\n\
                                    <input type="hidden" id="idp_'+x+'" value="'+ result[x].idbarangpembelian +'"/>\n\
                                    <input type="hidden" name="index[]" value="'+x+'"/>\n\
                                    <td>'+result[x].batchno+'</td>\n\
                                    <td>'+result[x].kadaluarsa+'</td>\n\
                                    <td><input type="hidden" id="detailpemberian_stok'+x+'" value="'+result[x].stok+'" />'+result[x].stok+'</td>\n\
                                    <td><input onchange="cekjumlahstok(this.value,'+x+','+jumlah+')" id="detailpemberian_jumlah'+x+'" type="text" value="'+result[x].diberikan+'"/></td>\n\
                                   </tr>';
                    }
                    
                    //hitung diberikan
                    setTimeout(function(){ 
                        var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
                        var jumlah = 0;
                        for(var z in arrIndex)
                        {
                            jumlah += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
                        }
                        $('#detailpemberian_jumlahdiberikan').text(jumlah);
                    },1000);
                    
                    
                    $('#detailpemberian_barangdiberikan').html('<table class="table table-hover table-bordered table-striped" id="detailpemberian_barangdiberikan">'+dthtml+'</table>');
                },
                error: function(result){                  
                    notif(result.status, result.message);
                    return false;
                }
            });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});


function cekjumlahstok(jumlah,index,resep)
{
    var stok   = parseFloat($('#detailpemberian_stok'+index).val());
    var jumlah = parseFloat($('#detailpemberian_jumlah'+index).val());
    var diberikan  = jumlah;
    //totaldiberikan
    var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
    var totdiberikan = 0;
    
    for(var z in arrIndex)
    {
        totdiberikan += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
    }
    
    //set diberikan
    if( totdiberikan > resep){
        notif('danger', 'Jumlah diberikan tidak boleh melebihi resep dokter.!');
        $('#detailpemberian_jumlah'+index).val( 0 );
        return;
    }else{               
        diberikan  = jumlah;
        if(jumlah > stok)
        {
            notif('danger', 'Jumlah diberikan tidak boleh melebihi stok.!');
            diberikan  = ((stok > resep) ? resep : stok );
        }        
        
        var idbp = $('#idbp_'+index).val();
        var idp  = $('#idp_'+index).val();        
        startLoading();
        $.ajax({ 
            url: base_url+'cpelayanan/detailpemberianobatralan',
            type : "post",      
            dataType : "json",
            data : {idbp:idbp,idp:idp,jumlah:diberikan},
            success: function(result) {                                                                   
                stopLoading();                
                $('#detailpemberian_jumlah'+index).val( diberikan );
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }

        }); 
    }
}

function carapenggunaan(no,penggunaan)
{
    var result = '<select id="penggunaan_ralan'+no+'" class="form-pelayanan" onchange="editBhppenggunaanRalan('+no+')" name="penggunaan">\n\
        <option value=""'+((penggunaan=='')?'selected':'')+'>Pilih</option> \n\
        <option '+((penggunaan=='Sebelum')?'selected':'')+' value="Sebelum">Sebelum</option> \n\
        <option '+((penggunaan=='Bersama')?'selected':'')+' value="Bersama">Bersama</option> \n\
        <option '+((penggunaan=='Sesudah')?'selected':'')+' value="Sesudah">Sesudah</option>\n\
        </select>';
    return result;
}

function settingBhpRalan(d,field,x,y,no,grup='')
{
    var jenisrawat = localStorage.getItem('jenisrawat');
    var modesave = $('input[name="modesave"]').val();
    startLoading();
    $.ajax({ 
        url: base_url+'cpelayanan/pemeriksaan_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:field, x:x,y:y,g:grup,i:idpendaftaran,jenisrawat:jenisrawat,idbarang:$('#idbarang_ralan'+no).val(),idinap:$('input[name="idinap"]').val(),modesave:modesave},
        success: function(result) {                                                                   
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                if(result.status=='success'){
                    // notif(result.status, result.message);
                    pemeriksaanklinikRefreshBhp(idpendaftaran,jenisrawat);
                    setTimeout(function(){
                        var noples = no+1;
                        if(field=='jumlah'){$('#jumlah_ralan'+noples).focus();}
                        else if(field=='kekuatan'){$('#kekuatan_ralan'+noples).focus();}
                        else if(field=='grup'){$('#grup_ralan'+noples).focus();}
                        else if(field=='jumlahdiberikan'){$('#jumlahdiberikan_ralan'+noples).focus();}
                        else if(field=='penggunaan'){$('#penggunaan_ralan'+noples).focus();}
                        else if(field=='aturanpakai'){$('#P_aturanpakai_ralan'+noples).focus();}
//                        else{$('#jumlah_ralan'+(no+1)).focus();}
                    },300);

                }else{
                    stopLoading();
                    notif(result.status, result.message);
                    return false;
                }    
            }
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

// mahmud, clear -> pilih aturan pakai obat
function isicomboaturanpakai(id, selected)
{
    if (is_empty(localStorage.getItem('localStorageAP')))
    {
        $.ajax({ 
            url: base_url+'cmasterdata/get_barangaturanpakai',
            type : "POST",      
            dataType : "JSON",
            success: function(result) {                                                                   
                localStorage.setItem('localStorageAP', JSON.stringify(result));
                fillaturanpakai(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillaturanpakai(id, selected);
            },
            error: function(result){
                fillaturanpakai(id, selected);
            }
        });
    }
}
//basit, clear
function fillaturanpakai(id, selected)
{
    var aturanpakai = JSON.parse(localStorage.getItem('localStorageAP'));
    var i=0;
    for (i in aturanpakai)
    {
      $("#"+id).append('<option ' + ((aturanpakai[i].idbarangaturanpakai===selected)?'selected':'') + ' value="'+aturanpakai[i].idbarangaturanpakai+'">'+aturanpakai[i].signa+'</option>');
    }
}

function cetakBhpAturanpakaiRajal(value='')//cetak aturan pakai
{
    if($('#P_aturanpakai_ralan'+value).val()==0){
        $.alert('Aturan pakai belum diisi.!');
    }else{
    startLoading();
    var idb = $('#idbarang_ralan'+value).val();
    $.ajax({
        dataType:"JSON",
        url:base_url+"cpelayanan/pemeriksaan_cetak_aturanpakai",
        data:{idp:idpendaftaran,idb:idb,jenisrawat:localStorage.getItem('jenisrawat')},
        type:"POST",
        success:function(result){
            stopLoading();
            var cetak = '<div style="width:5.8cm;height:4cm;border-top:1px solid #000;border-right:1px solid #000;border-left:1px solid #000;float: center;text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;<img style="width:3cm; margin-top:2px;" src="'+base_url+'assets/images/'+result.header+'" />\n\
                          <table border="0" style="width:100%;font-size:small;text-align:center;float:center;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak += "<tr style='font-size:9px;'><td colspan='4' align='left'><b>&nbsp;&nbsp;&nbsp;&nbsp;Apoteker : "+result.apoteker+"</b></td></tr>";
            cetak += "<tr style='font-size:6px;'><td colspan='4'>________________________________________________________________________</td></tr>";
            cetak += "<tr style='font-size:9.5px;solid #000;'><td colspan='2' align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+norm+"</td><td align='right'>Tgl :<span style='font-size:8.5px;'>"+result.aturan['tglperiksa']+"&nbsp;&nbsp;&nbsp;&nbsp;</span></td><td></td></tr>";
            cetak += "<tr style='font-size:9.5px;border-bottom:1px solid #000;'><td colspan='2' align='left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+toCamelCase(result.aturan['namalengkap'])+"</td><td align='right'>Ttl :<span style='font-size:8.5px;'>"+result.aturan['tempatlahir']+", "+result.aturan['tanggallahir']+"&nbsp;&nbsp;&nbsp;&nbsp;</span></td></tr>";
            cetak += "<tr style='font-size:6px;'><td colspan='4'>________________________________________________________________________</td></tr>";
            if(value!==''){
            cetak += "<tr style='font-size:10px;'><td colspan='4'>"+ ((result.aturan['grup']=='0')? result.aturan['namaobat'] : '') +"</td></tr>";
            cetak += "<tr style='font-size:10px;'><td colspan='4'><b>"+result.aturan['aturanpakai']+"</b></td></tr>";
            cetak += "<tr style='font-size:10px;'><td colspan='4'>"+((result.aturan['penggunaan']!=='')?result.aturan['penggunaan']+' Makan':'')+"</td></tr>";
            }else{
                cetak += "<tr style='font-size:10px;'><td colspan='4'>"+ $('textarea[name="aturanpakai"]').val() +"</td></tr>";
            }
            cetak +='</table></div><div style="width:5.8cm;height:0.73cm;border-right:1px solid #000;border-left:1px solid #000;border-bottom:1px solid #000; float: center;text-align:center;"><table style="width:100%;font-size:small;text-align:center;float:center;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak += "<tr style='font-size:8.5px;'><td>Pagi / Siang / Sore / Malam</td></tr>";
            cetak += "<tr style='font-size:8.5px;'><td>Semoga Lekas Sembuh</td></tr>";
            cetak += '</table></div>';
            fungsi_cetaktopdf(cetak,'<style>@page {size:5.8cm 5cm;margin:0.0cm;}</style>');
        },
        error:function(result)
        {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
    }
}


function editBhpAturanpakaiRalan(value)//seting atau ubah grup bhp
{
    var d = $('select[name="P_aturanpakai_ralan'+value+'"]').val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#idbarang_ralan'+value).val();
    var grup = $('#grup_ralan'+value).val();
    settingBhpRalan(d,'aturanpakai',x,y,value,grup);
}
function editBhpKemasanRalan(value)//seting atau ubah grup bhp
{
    var d = $('#P_kemasan_ralan'+value+'').val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#idbarang_ralan'+value).val();
    var grup = value;
    settingBhpRalan(d,'kemasan',x,y,value,grup);
}


function editBhpGrupRalan(value)//seting atau ubah grup bhp
{
    var d = $('#grup_ralan'+value).val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#idbarang_ralan'+value).val();
    settingBhpRalan(d,'grup',x,y,value);
}
function editBhpJumlahRalan(value)//seting atau ubah grup bhp
{
    var grup = $('#grup_ralan'+value).val();
    var d = ((grup!='0')? $('#jumlah_ralan'+value).val() * $('#jumlahRacikan_ralan'+grup).val() : $('#jumlah_ralan'+value).val()) ;
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#harga_ralan'+value).val();
    settingBhpRalan(d,'jumlah',x,y,value);
}
function editBhpJumlahdiberikanRalan(no,kekuatan)
{
     var grup = $('#grup_ralan'+no).val();
    var jml = (kekuatan / $('#kekuatan_ralan'+no).val() ) * $('#jumlahdiberikan_ralan'+no).val();
    var d = ((grup!='0')?jml * $('#jumlahRacikan_ralan'+grup).val() : jml);
    var x = $('#idbarangpemeriksaan_ralan'+no).val();
    var y = $('#harga_ralan'+no).val();
    // console.log($('#kekuatan2'+no).val());
    settingBhpRalan(d,'jumlahdiberikan',x,y,no);
}
function editBhpKekuatanRalan(value)//seting atau ubah grup bhp
{
    var d = $('#kekuatan_ralan'+value).val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#harga_ralan'+value).val();
    settingBhpRalan(d,'kekuatan',x,y,value);
}
function editBhppenggunaanRalan(value)
{
    var d = $('#penggunaan_ralan'+value).val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#harga_ralan'+value).val();
    var grup = $('#grup_ralan'+value).val();
    settingBhpRalan(d,'penggunaan',x,y,value,grup);
}
 
function editBhpRacikanRalan(value)
{
    var d = $('#jumlahRacikan_ralan'+value).val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#idbarang_ralan'+value).val();
    var grup = value;
    settingBhpRalan(d,'jumlahRacikan',x,y,value,grup);
}
function editBhpRacikDiberikanRalan(value)
{
    var d = $('#jumlahRacikDiberikan_ralan'+value).val();
    var x = $('#idbarangpemeriksaan_ralan'+value).val();
    var y = $('#idbarang_ralan'+value).val();
    var grup = value;
    settingBhpRalan(d,'jumlahRacikanDiberikan',x,y,value,grup);
}


// mahmud, clear
function cetakBhpResep()
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        dataType:"JSON",
        url:base_url+"cpelayanan/pemeriksaan_cetak_resepobatbhp",
        data:{daftar:idpendaftaran,jenisrawat:localStorage.getItem('jenisrawat')},
        type:"POST",
        success:function(result){
            stopLoading();
            var cetakresep='';
            var style ='style="width:9.5cm;font-size:11px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #0e3044;"';
            var style2 ='style="width:9.5cm;font-size:10.5px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #0e3044;"';
            var no=0,no2=1;
                for(x in result.resep)
                {
                    var resep = result.resep[x];
                    totaldiresepkan = (resep.kekuatanperiksa/resep.kekuatan) * resep.jumlahdiresepkan;
                    ((resep.grup==0 || grup!==resep.grup)? no=1 : no +=1  );
                    cetakresep +=  "<tr style='font-size:11.5px;'><td colspan='2'>"+((resep.grup==0 || grup!==resep.grup)?'<b>R/</b>':'&nbsp;&nbsp;&nbsp;')+"  "+resep.namabarang+ ' ' +((resep.jumlahRacikan!=null)? (totaldiresepkan/resep.jumlahRacikan).toFixed(3) : totaldiresepkan ) + ' '+ ((resep.grup==='0')?(( resep.pemakaian===null)?'': resep.pemakaian):'') +" </td></tr>";
                    if (resep.grup==0)
                    {
                        cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+((resep.signa===null)?'':resep.signa)+"</td></tr>";
                        cetakresep += ((resep.signa===null)?'':"<tr  style='font-size:6px;' ><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>") ;
                    }
                    for(i in result.grup)
                    {
                        if(resep.grup!=0 && result.grup[i].jumlahgrup==no && resep.grup==result.grup[i].grup )
                        {
                            cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<i>"+resep.kemasan+" dtd no "+resep.jumlahRacikan+"</i></td></tr>";
                            cetakresep +="<tr style='font-size:11.5px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+resep.signa+"</td></tr>";
                            cetakresep +="<tr style='font-size:6px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
                        }
                    }
                    var grup = resep.grup;
                }

            // resep fix
            var cetak = '<img style="margin-bottom:1px; width:5.5cm" src="'+base_url+'assets/images/'+result.header+'" />\n <div style="width:9.5cm;height:15cm;float: none; border-top:1px solid #000;">';
                cetak +='<table border="0" '+style+'>';
                cetak +='<tr style="color: #0e3044;"><td>Ruang/klinik: '+result.pasien['namaunit']+'</td><td >Tanggal : '+result.pasien['tglperiksa']+'</td></tr>';
                cetak +='<tr  style="color: #0e3044;"><td>Carabayar: '+result.pasien['carabayar']+'</td><td>Riwayat alergi obat</td></tr>';
                cetak +='<tr  style="color: #0e3044;"><td></td><td style="padding-left:8px;color: #0e3044;">'+toCamelCase(result.pasien['alergi'])+'</td></tr>';
                cetak += cetakresep;
                cetak += '</table></div>';

                cetak +='<div style="width:9.5cm;height:2.3cm;float: none;margin-bottom:15px;"><table border="0" '+style2+'>';
                cetak +='<tr style="color: #0e3044;"><td style="width:13%;">Nama</td><td>: '+result.pasien['namalengkap']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td>Alamat</td><td>: '+result.pasien['alamat']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td>No.RM</td><td>: '+result.pasien['norm']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td>Ttl/Usia</td><td>: '+result.pasien['tanggallahir']+'/'+result.pasien['usia']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td>BB</td><td>: </td></tr>';
                cetak +='<tr style="color: #0e3044;"><td>Dokter</td><td>: '+result.pasien['dokter']+'</td></tr>';
                cetak += '</table></div>';
                cetak += '<div style="width:9.5cm;height:0.2cm;border-top:1px solid #000;font-size:11px; text-align:center;margin:0px;padding:0px;color: #0e3044;">'+result.noteresep+'</div>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:9.5cm 20.5cm;margin:0.1cm;}</style>');
        },
        error:function(result)
        {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
//mahmud, clear -> Copy Resep
function cetakBhpCopyResep()
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        dataType:"JSON",
        url:base_url+"cpelayanan/pemeriksaan_cetak_resepobatbhp",
        data:{daftar:idpendaftaran,jenisrawat:localStorage.getItem('jenisrawat')},
        type:"POST",
        success:function(result){
            stopLoading();
            var cetakresep='', diberi='';
            var style ='style="width:9.5cm;font-size:10px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #0e3044;"';
            var style2 ='style="width:9.5cm;font-size:10px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #0e3044;"';
            var no=0,no2=1;
            
                for(x in result.resep)
                {
                    var resep = result.resep[x];
                    totaldiresepkan = (resep.kekuatanperiksa/resep.kekuatan) * resep.jumlahdiresepkan;
                    totaldiberikan = (resep.kekuatanperiksa/resep.kekuatan) * resep.jumlahpemakaian;
                    ((resep.grup==0 || grup!==resep.grup)? no=1 : no +=1  );
                    cetakresep +=  "<tr style='font-size:11px;'><td colspan='2'>"+((resep.grup==0 || grup!==resep.grup)?'<b>R/</b>':'&nbsp;&nbsp;&nbsp;')+"  "+resep.namabarang+ ' ' + ((resep.jumlahRacikan!=null)? (totaldiresepkan/resep.jumlahRacikan).toFixed(3) : totaldiresepkan)  + ' '+ ((resep.grup==='0')?resep.pemakaian:'') +" </td></tr>";
                    if (resep.grup==0)
                    {
                        cetakresep +="<tr style='font-size:11px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+resep.signa+" &nbsp;&nbsp;&nbsp;&nbsp;("+((totaldiberikan > 0)? ((totaldiberikan === totaldiresepkan)? 'det' :'det '+totaldiberikan) :'nedet')+")</td></tr>";
                        cetakresep += ((resep.signa===null)?'':"<tr  style='font-size:6px;' ><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>") ;
                    }
                    for(i in result.grup)
                    {
                        if(resep.grup!=0 && result.grup[i].jumlahgrup==no && resep.grup==result.grup[i].grup )
                        {
                            cetakresep +="<tr style='font-size:11px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;<i>"+resep.kemasan+" dtd no "+resep.jumlahRacikan+"</i></td></tr>";
                            cetakresep +="<tr style='font-size:11px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;"+resep.signa+" &nbsp;&nbsp;&nbsp;&nbsp;("+((resep.jumlahRacikDiberikan > 0)? ((resep.jumlahRacikan == resep.jumlahRacikDiberikan)? 'det' :'det '+resep.jumlahRacikDiberikan) :'nedet')+")</td></tr>";
                            cetakresep +="<tr style='font-size:6px;'><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>";
                        }
                    }
                    var grup = resep.grup;
                }
            // copy resep fix
            var cetak = '<img style="margin-bottom:1px; width:5cm" src="'+base_url+'assets/images/'+result.header+'" />\n <div style="width:10cm;height:18cm;float: none; border-top:1px solid #000;">';
                cetak +='<table border="0" '+style+'>';
                cetak +='<tr style="color: #0e3044;"><td >Tanggal : '+result.pasien['tglperiksa']+'</td> <td >No.'+result.pasien['idpemeriksaan']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td colspan="2">Dokter &nbsp;&nbsp;: '+result.pasien['dokter']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;"><td colspan="2">Pasien &nbsp;&nbsp;: '+result.pasien['namalengkap']+'</td></tr>';
                cetak +='<tr style="color: #0e3044;" align="center"><td colspan="2"><b>COPY RESEP</b></td></tr>';
                cetak +='<tr style="color: #0e3044;"><td colspan="2">'+ cetakresep +'</td></tr>';
                cetak += '</table></div>';
                cetak +='<div style="width:9.5cm;height:0.4cm;float: none; padding-right:10px;"><table border="0" '+style2+'>';
                cetak +="<tr style='border-bottom: 1px solid #f3f3f3;color: #0e3044;' align='right'><td>"+result.apoteker+"</td></tr>";
                cetak +="<tr style='color: #0e3044;' align='right'><td>"+result.sipa+"</td></tr>";
                cetak += '</table></div>';
                fungsi_cetaktopdf(cetak,'<style>@page {size:9.5cm 20cm ;margin:0.1cm;}</style>');
        },
        error:function(result)
        {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusBhpRalan(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus Obat/Bhp.?',
        buttons: {
            Hapus: function () {                
                var x = $('#idbarangpemeriksaan_ralan'+value).val();
                var d='', y='';
                settingBhpRalan(d,'del',x,y,value);
            },
            Batal: function () {               
            }            
        }
    });
}

