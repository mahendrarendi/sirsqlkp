//mahmud, clear
'use strict';
var dtbarang='';
localStorage.setItem('customspage',0);
$(function(){
    switch($('#jsmode').val())
    {
        case 'form':
            setInputAngka('hargabeli'); // --lihat file sirstql line 175
            setInputAngka('persenmargin'); // --lihat file sirstql line 175
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green'})
            setPengaturanMargin($('#margin_manual').val());
            break;
        case 'datastokbarang':
            listDataStokBarang();
            break;   
        default:
            list_barang();//tampil data barang
            totalkekayaan();
    }
});


//set pengaturan margin
function setPengaturanMargin(value)
{
    $('[data-setmargin]').hide();
    $('[data-setmargin*='+value+']').show();
}

$(document).on('change','#idunitfarmasi',function(){
    dtbarang.ajax.reload(null,false);
});

//list data stok barang
function listDataStokBarang()
{
    dtbarang = $('.table').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],"ajax": {
            "data":{idunit:function(){ return $('#idunitfarmasi').val(); }},
            "url": base_url+'cfarmasi/dt_datastokbarang',
            "type": "POST"},
        "columnDefs": [{ "targets": [],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}

// Total totalkekayaan Obat dan BHP
function totalkekayaan()
{
    var total=0; 
    $.ajax({ 
        url:base_url+'cfarmasi/totalkekayanobatdanbhp',type:"POST",dataType:"JSON",
        success: function(r){
            if(r!=null)
            {
                total += parseFloat(r.totalkekayaan);
                $('#totalkekayaan').text('Rp '+ ((r.totalkekayaan==null) ? 0 : convertToRupiah(total) ) );
            }
        },
        error: function(result){
            fungsiPesanGagal();return false;
        }
    });
}
// 

function list_barang()
{
    dtbarang = $('.table').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],"ajax": {
            "data":{},
            "url": base_url+'cfarmasi/dt_barang',
            "type": "POST"},
        "columnDefs": [{ "targets": [0,3,4,14],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}

$(document).on('click','#unduh',function(){ window.location.href=base_url+"creport/excel_inventaris";});

function simpan_barang()
{
  if($('#namabarang').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid barang.!'});
  }
  else
  {
    $('#Formbarang').submit();
  }
}
function barang_sethargajual(){
    var total = 0, hargabeli=$('input[name="hargabeli"]').val(), persenmargin = $('input[name="persenmargin"]').val();
    total += (parseInt(isnan(hargabeli)) * (parseInt(isnan(persenmargin))/100) ) + parseInt(isnan(hargabeli)) ;
    return $('input[name="hargajual"]').val(total);
}

function barang_sethargaumum(){
    var total = 0, hargabeli=$('input[name="hargabeli"]').val(), persenmargin = $('input[name="persenmargin_hargaumum"]').val();
    total += (parseInt(isnan(hargabeli)) * (parseInt(isnan(persenmargin))/100) ) + parseInt(isnan(hargabeli)) ;
    return $('input[name="hargaumum"]').val(total);
}

//Riwayat Obat
$(document).on("click","#btnRiwayatBarang", function(){
   var id = $(this).attr('dt');
   var nama = $(this).attr('namabrg');
   var kode = $(this).attr('kodebrg');
   riwayatbarang(id,nama,kode);
});

var riwayatobat='';
function riwayatbarang(idbarang,nama,kode)
{
    var modalContent ='<table id="riwayatObat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
    +'<thead><tr><th>No</th><th>PBF</th><th>Distributor</th><th>Waktu</th><th>Batchno</th><th>Kadaluarsa</th><th>HargaBeli</th><th>Diskon</th><th>Jumlah</th><th>Jenis Distribusi</th><th>Unit Asal</th><th>Unit Tujuan</th></tr></thead><tbody></tfoot></table>';
    $.confirm({
        title: 'Riwayat Obat/BHP '+'['+kode+'] '+nama,content: modalContent,columnClass: 'lg',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {  
            formReset:{ /*menu back*/
                text: 'tutup',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            riwayatobat = $('#riwayatObat').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "order": [],
                "ajax": {
                    "data":{
                        idb:function(){ return idbarang}
                    },
                    "url": base_url+'cfarmasi/dt_riwayatobat',
                    "type": "POST"
                },"columnDefs": [{ "targets": [ 8, 9],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
        // bind to events
        }
    });
}



// -- cari golongan by jenis
function barang_carigolbyjenis(value)
{
    if(value!='0')
    {
        var dthtml='';
        $.ajax({
            url: base_url +'cfarmasi/barang_carigolbyjenis',
            type:'POST',
            dataType:'JSON',
            data:{jenis:value},
            success: function(result){
                $('#golongan').empty();
                for (var i in result )
                {
                    dthtml += '<option value="'+result[i].idgolongan+'">'+result[i].golongan+'</option>';
                }
                $('#golongan').html(dthtml);
                $('.select2').select2();
            },
            error: function(result){
                // console.log(result);
                fungsiPesanGagal();
                return false;
            }
        });
    }
}

//hidden data barang
$(document).on('click','#btnHiddenBarang',function(){
    var namabarang = $(this).attr('namabrg');
    var idbarang   = $(this).attr('idbarang');
    var status     = $(this).attr('status');
    var pesan      = ((status == '1') ? '<i>Open Hidden</i>' : 'Hidden' );
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Apakah anda yakin akan '+pesan+' <br>'+namabarang,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url + 'cfarmasi/hiddenbarang',
                    type : "post",      
                    dataType : "json",
                    data : { idbarang:idbarang,status:status},
                    success: function(result) {                  
                        notif(result.status, result.message);
                        dtbarang.ajax.reload(null,false);
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});

//hapus data barang
$(document).on("click","#hapusbarang", function(){ // DELETE USER
    var table = $(this).attr("alt");
    var id = $(this).attr("alt2");
    var halaman = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Hapus Obat/BHP?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url + 'cfarmasi/hapusbarang',
                    type : "post",      
                    dataType : "json",
                    data : { t:table, i:id, hal:halaman },
                    success: function(result) {                  
                        notif(result.status, result.message);
                        dtbarang.ajax.reload(null,false);
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});