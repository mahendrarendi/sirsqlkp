'use strict';
$(function(){
    tampildata();
    tampilsewaalat();
    tampildiagnosa();
    tampiltindakan();
    tampildiagnosapost();
    tampildtbarangpemeriksaan();
    select2serachmulti($('#dokteranestesi'),'cmasterdata/searchmultidokter');
    select2serachmulti($('#dokteroperator'),'cmasterdata/searchmultidokter');
    select2serachmulti($('#dokteranak'),'cmasterdata/searchmultidokter');
    select2serachmulti($('#penata'),'cmasterdata/searchmultipegawai');
    select2serachmulti($('#asisten'),'cmasterdata/searchmultipegawai');
    select2serachmulti($('#instrumen'),'cmasterdata/searchmultipegawai');
    select2serachmulti($('#sirkuler'),'cmasterdata/searchmultipegawai');
    select2serachmulti($('#penerimabayi'),'cmasterdata/searchmultipegawai');
    select2serachmulti($('#caritindakan'),'cmasterdata/fillicdtindakan');
    select2serachmulti($('#caridiagnosa'),'cmasterdata/fillicddiagnosa');
    select2serachmulti($('#caridiagnosasekunder'),'cmasterdata/fillicddiagnosa');    
    select2serachmulti($('#caridiagnosapost'),'cmasterdata/fillicddiagnosa');
    select2serachmulti($('#caridiagnosasekunderpost'),'cmasterdata/fillicddiagnosa');    
    select2serachmulti($('#caribhpobat'),'cmasterdata/fillobatbhp');
});

function tampildata()
{
    startLoading();
    $.ajax({
        url: base_url+'cpelayanan/formready_pemeriksaanoperasi',
        type : "post",
        dataType : "json", 
        data : {
            idop:localStorage.getItem('idoperasi'),
            idjop:localStorage.getItem('idjadwaloperasi')
        },
        success: function(result) {
            jenisoperasi(result.jenisoperasi,result.operasi);
            jenisanestesi(result.jenisanestesi,result.operasi);
            editdataoption($('#idmacamoperasi'),result.macamoperasi, ((result.operasi==null) ? '' : result.operasi['idmacamoperasi'] ),'<option value="0">Input..</option>' );
            editdataoption($('#tarifoperasi'),result.tarifoperasi, ((result.biayaoperasi==null) ? '' : result.biayaoperasi['idjenistarifoperasi'] ),'<option value="0">Input..</option>' );
            editdataoption($('#jaminan'),result.jaminan, ((result.jaminan==null) ? '' : result.operasi['jaminan'] ),'<option value="0">Input..</option>' );
            editdataoption($('#tariftindakan'),result.tariftindakan, ((result.biayaoperasi==null) ? '' : result.biayaoperasi['jenistindakan'] ),'<option value="0">Input..</option>' );
            setvalinput('idoperasi',result.operasi['idoperasi']);
            setvalinput('idpendaftaran',result.operasi['idpendaftaran']);
            setvalinput('idjadwaloperasi',result.operasi['idjadwaloperasi']);
            setvalformid($('#uraianpembedahan'),result.operasi['uraianpembedahan']);
            setvalformid($('#obatanestesi'),result.operasi['obatanestesi']);
            setvalformid($('#diagnosapraoperasi'),result.operasi['diagnosapraoperasi']);
            setvalformid($('#diagnosapascaoperasi'),result.operasi['diagnosapascaoperasi']);
            setvalformid($('#tindakanoperasi'),result.operasi['tindakanoperasi']);
            setvalformid($('#jenistindakan'),result.operasi['jenistindakan']);
            setvalformid($('#jumlahperdarahan'),result.operasi['jumlahperdarahan']);
            setvalformid($('#sitologi'),result.operasi['sitologi']);
            setvalformid($('#patologianatomi'),result.operasi['patologianatomi']);
            setvalformid($('#asaljaringan'),result.operasi['asaljaringan']);
            setvalformid($('#pemasanganimplant'),result.operasi['pemasanganimplant']);
            setselectvalue($('#dokteranestesi'),result.operasi['idpegawaidokteranestesi'],result.operasi['dokteranestesi']);
            setselectvalue($('#dokteroperator'),result.operasi['iddokteroperator'],result.operasi['dokteroperator']);
            setselectvalue($('#dokteranak'),result.operasi['idpegawaidokteranak'],result.operasi['dokteranak']);
            setselectvalue($('#penata'),result.operasi['idpegawaipenata'],result.operasi['penata']);
            setselectvalue($('#instrumen'),result.operasi['idpegawaiintrumen'],result.operasi['instrumen']);
            setselectvalue($('#asisten'),result.operasi['idpegawaiasisten'],result.operasi['asisten']);
            setselectvalue($('#sirkuler'),result.operasi['idpegawaisirkuler'],result.operasi['sirkuler']);
            setselectvalue($('#penerimabayi'),result.operasi['idpegawaipenerimabayi'],result.operasi['penerimabayi']);     
            
            tampilpenggunaanbarang();
            
            $('#jammulai').val(result.operasi['jammulai']);
            $('#jamselesai').val(result.operasi['jamselesai']);
            $('#jammulai').timepicker({ showInputs: true,autoclose: true, showMeridian:false});//Timepicker
            $('#jamselesai').timepicker({ showInputs: true,autoclose: true, showMeridian:false});//Timepicker
            $('#tanggalmulai').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((result.operasi['tanggalmulai'] != null) ? result.operasi['tanggalmulai'] : 'now' ) ); //Initialize Date picker
            $('#tanggalselesai').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate', ((result.operasi['tanggalselesai'] != null) ? result.operasi['tanggalselesai'] : 'now' ) ); //Initialize Date picker
            
            $('.textarea').wysihtml5();
            //resiko tinggi
            if(result.operasi!=null)
            {
                var dtresiko = '<label><input type="radio" name="resikotinggi" value="0" class="flat-red"'+ ((result.operasi.resikotinggi=='0') ? 'checked' : '' ) +'> Tidak </label>&nbsp;&nbsp;&nbsp;\n\
                    <label><input type="radio" name="resikotinggi" value="1" class="flat-red" '+ ((result.operasi.resikotinggi=='1') ? 'checked' : '' ) +'> Ya </label>';
                $('#resikotinggi').html(dtresiko);
                
                var dtisoperasisc = '<label><input type="radio" name="isoperasisc" value="0" class="flat-red"'+ ((result.operasi.isoperasisc=='0') ? 'checked' : '' ) +'> Tidak </label>&nbsp;&nbsp;&nbsp;\n\
                    <label><input type="radio" name="isoperasisc" value="1" class="flat-red" '+ ((result.operasi.isoperasisc=='1') ? 'checked' : '' ) +'> Ya </label>';
                $('#isoperasisc').html(dtisoperasisc);
            }
            
            //identitas pasien
            profilpasien(result.operasi);
            
        stopLoading();
        },
        error: function(result){
            stopLoading();
            notif(result.status, result.message);
            return false;
        }

    }); 
}
//tampil data sewa alat
var dtsewaalat='';
function tampilsewaalat()
{
    dtsewaalat = $('#dtsewaalat').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
        "ajax": { 
            data : { idoperasi : function(){return localStorage.getItem('idoperasi');} },
            "url": base_url+'cpelayanan/dt_operasisewaalat',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//list diagnosa
var dtdiagnosa = '';
function tampildiagnosa()
{
    dtdiagnosa = $('#dtdiagnosa').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "searching" :false,
        "order": [],
        "ajax": { 
            data : { idoperasi : function(){return localStorage.getItem('idoperasi');} },
            "url": base_url+'cpelayanan/dt_operasidiagnosa',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//list diagnosa post
var dtdiagnosapost = '';
function tampildiagnosapost()
{
    dtdiagnosapost = $('#dtdiagnosapost').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "searching" :false,
        "order": [],
        "ajax": { 
            data : { idoperasi : function(){return localStorage.getItem('idoperasi');} },
            "url": base_url+'cpelayanan/dt_postoperasidiagnosa',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//list tindakan
var dttindakan = '';
function tampiltindakan()
{
    dttindakan = $('#dttindakan').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "searching" :false,
        "order": [],
        "ajax": { 
            data : { idoperasi : function(){return localStorage.getItem('idoperasi');} },
            "url": base_url+'cpelayanan/dt_operasitindakan',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//list obatbhp
var dtbarangpemeriksaan = '';
function tampildtbarangpemeriksaan()
{
    dtbarangpemeriksaan = $('#dtbarangpemeriksaan').DataTable({
        "dom": '<"toolbar">frtip',
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "searching" :false,
        "order": [],
        "ajax": { 
            data : { idoperasi : function(){return localStorage.getItem('idoperasi');} },
            "url": base_url+'cpelayanan/dt_operasibarangpemeriksaan',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { }, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//tambah obat
$(document).on('change','#caribhpobat',function(){
   save_barangpemeriksaan(this.value,'tambah',1,0); 
});


//hapus barangpemeriksaan
$(document).on('click','#hapus_obat',function(){
    var idbarangoperasi = $(this).attr('alt');
    var barang = $(this).attr('barang');
    
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus Obat / BHP',
        content: '<b>Obat / BHP </b> = '+ barang,
        buttons: {
            formSubmit: {
                text: 'hapus',
                btnClass: 'btn-blue',
                action: function () {                    
                    save_barangpemeriksaan(0,'hapus',0,idbarangoperasi); 
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//ubah jumlah barangpemeriksaan
$(document).on('change','#setjumlah_obat',function(){
    var jumlah = this.value;
    var idbarangoperasi = $(this).attr('alt');
    save_barangpemeriksaan(0,'ubah',jumlah,idbarangoperasi); 
});

/**
 * Simpan Barangpemeriksaan
 * @param {type} idbarang Idbarang
 * @param {type} mode Mode simpan
 * @param {type} jumlah Jumlah barang
 * @returns {undefined} 
 */
function save_barangpemeriksaan(idbarang,mode,jumlah,idbarangoperasi)
{
    var jaminan = $('#jaminan').val();
    if(jaminan == '')
    {
        alert('Jaminan Harap Dipilih Terlebih Dahulu.');
        return false;
    }
    
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_save_barangpemeriksaan', //alamat controller yang dituju (di js base url otomatis)
        data: {
            idbarang:idbarang,
            idbarangoperasi:idbarangoperasi,
            jaminan:jaminan,
            mode:mode,
            jumlah:jumlah,
            idoperasi:localStorage.getItem('idoperasi'),
            idpendaftaran:localStorage.getItem('idpendaftaranop')
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                dtbarangpemeriksaan.ajax.reload(null,false);
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

//ubah jumlah sewa alat
$(document).on('change','#setjumlah_sewaalat',function(){
    var jumlah = this.value;
    var idsewa = $(this).attr('alt');
    save_sewaalat(0,idsewa,jumlah,0,'ubahjumlah');
});

//ubah harga sewa alat
$(document).on('change','#setharga_sewaalat',function(){
    var harga  = unconvertToRupiah(this.value);
    var idsewa = $(this).attr('alt');
    var idalat = $(this).attr('alt2');
    save_sewaalat(idalat,idsewa,0,harga,'ubahharga');
});

//tambah sewa alat
$(document).on('click','#tambah_sewaalat',function(){
    var harga  = $(this).attr('alt2');
    var idalat = $(this).attr('alt');
    save_sewaalat(idalat,0,0,harga,'tambah');
});

//batal sewa alat
$(document).on('click','#batal_sewaalat',function(){
    var idsewa = $(this).attr('alt');
    save_sewaalat(0,idsewa,0,0,'batal');
});

//tambah alat
$(document).on('click','#tambah_alat',function(){
    tambah_alat();
});

function tambah_alat()
{
    var formAlat = '<form action="" id="formSewaalat" autocomplete="off">' +
                        '<div class="form-group">' +
                            '<div class="col-md-12">' +
                                '<label>Nama Alat</label>' +
                                '<input type="text" name="namaalat" class="form-control"/>'+
                            '</div>'+
                        '</div>'+

                        '<div class="form-group">' +
                            '<div class="col-md-12">' +
                                '<label>Alias</label>' +
                                '<input type="text" name="alias" class="form-control"/>'+
                            '</div>'+
                        '</div>'+

                        '<div class="form-group" >' +
                            '<div class="col-md-12">' +
                                '<label>Harga Sewa</label>' +
                                '<input type="text" name="harga" class="form-control"/>'+
                            '</div>'+
                        '</div>'+                            
                    '</form>';
    $.confirm({
        icon: 'fa fa-plus-square',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Tambah Alat',
        content: formAlat,
        buttons: {
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="namaalat"]').val()==''){
                        alert('Nama Alat Harap Diisi.');
                        return false;
                    }else if($('input[name="harga"]').val()==''){
                        alert('Harga Sewa Harap Diisi.');
                        return false;
                    }else{
                        startLoading();
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cpelayanan/operasi_add_sewaalat', //alamat controller yang dituju (di js base url otomatis)
                            data:$('#formSewaalat').serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status,result.message);
                                if(result.status=='success')
                                {
                                    dtsewaalat.ajax.reload(null,false);
                                }
                                stopLoading();
                            },
                            error: function(result) { //jika error
                                stopLoading();
                                fungsiPesanGagal();
                            }
                        });
                    }
                    
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    });
}


/**
 * Simpan Sewa Alat
 * @param {type} idalat Idalat
 * @param {type} idsewa Idsewaalat
 * @param {type} jumlah Jumlah
 * @param {type} harga  Harga
 * @param {type} type   Type
 * @returns {undefined}
 */
function save_sewaalat(idalat,idsewa,jumlah,harga,type)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_save_sewaalat', //alamat controller yang dituju (di js base url otomatis)
        data: {
            idalat:idalat,
            idsewa:idsewa,
            jumlah:jumlah,
            harga:harga,
            type:type,
            idoperasi:localStorage.getItem('idoperasi'),
            idpendaftaran:localStorage.getItem('idpendaftaranop')
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                dtsewaalat.ajax.reload(null,false);
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}


//input tindakan
$(document).on('change','#caritindakan',function(){
   save_tindakan(this.value);
});

//simpan input diagnosa
function save_tindakan(icd)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_save_tindakan', //alamat controller yang dituju (di js base url otomatis)
        data: {icd:icd,idoperasi:localStorage.getItem('idoperasi'),idpendaftaran:localStorage.getItem('idpendaftaranop')}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                dttindakan.ajax.reload(null,false);
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

//set jumlah tindakan
$(document).on('change','#setjumlah_tindakan',function(){
   var idtindakan = $(this).attr('alt');
   var jumlah    = this.value;
   update_jumlah_tindakan(jumlah,idtindakan);
});

function update_jumlah_tindakan(jumlah,idtindakan)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_update_jumlahtindakan', //alamat controller yang dituju (di js base url otomatis)
        data: {jumlah:jumlah,id:idtindakan}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                dttindakan.ajax.reload(null,false);
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

//konfirmasi hapus tindakan
$(document).on('click','#hapus_tindakan',function(){
    var icd = $(this).attr('icd');
    var idtindakan = $(this).attr('alt');
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus Tindakan',
        content: '<b>Tindakan </b> '+ icd,
        buttons: {
            formSubmit: {
                text: 'hapus',
                btnClass: 'btn-blue',
                action: function () {
                    hapus_tindakan(icd,idtindakan);
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//hapus tindakan
function hapus_tindakan(icd,idtindakan)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_delete_tindakan', //alamat controller yang dituju (di js base url otomatis)
        data: {icd:icd,id:idtindakan}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                dttindakan.ajax.reload(null,false);
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

//input diagnosa post primer
$(document).on('change','#caridiagnosapost',function(){
   save_diagnosa(this.value,'primer','post');
});
//input diagnosa post sekunder
$(document).on('change','#caridiagnosasekunderpost',function(){
   save_diagnosa(this.value,'sekunder','post');
});

//input diagnosa primer
$(document).on('change','#caridiagnosa',function(){
   save_diagnosa(this.value,'primer','pra');
});
//input diagnosa sekunder
$(document).on('change','#caridiagnosasekunder',function(){
   save_diagnosa(this.value,'sekunder','pra');
});

//simpan input diagnosa
function save_diagnosa(icd,level,jenis)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_save_diagnosa', //alamat controller yang dituju (di js base url otomatis)
        data: {
            icd:icd,
            level:level,
            jenis:jenis,
            idoperasi:localStorage.getItem('idoperasi'),
            idpendaftaran:localStorage.getItem('idpendaftaranop')
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                if(jenis=='pra'){
                    dtdiagnosa.ajax.reload(null,false);
                }else{
                    dtdiagnosapost.ajax.reload(null,false);
                }
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}

//konfirmasi hapus diagnosa
$(document).on('click','#hapusdiagnosa',function(){
    var icd = $(this).attr('icd');
    var iddiagnosa = $(this).attr('alt');
    var jenis = $(this).attr('icd');
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus Diagnosa',
        content: '<b>ICD</b> '+ icd,
        buttons: {
            formSubmit: {
                text: 'hapus',
                btnClass: 'btn-blue',
                action: function () {
                    hapus_diagnosa(icd,iddiagnosa,jenis);
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//hapus diagnosa
function hapus_diagnosa(icd,iddiagnosaoperasi,jenis)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/operasi_delete_diagnosa', //alamat controller yang dituju (di js base url otomatis)
        data: {icd:icd,id:iddiagnosaoperasi}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            if(result.status=='success')
            {
                if(jenis=='pra'){
                    dtdiagnosa.ajax.reload(null,false);
                }else{
                    dtdiagnosapost.ajax.reload(null,false);
                }                
            }
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}




//profil pasien
function profilpasien(profil)
{
    var profilePasien = '<table class="table-hover table-bordered  col-xs-12 col-md-6">'
            +'<tr class="bg-yellow"><th colspan="2"><h4>&nbsp;Jadwal Operasi</h4></th></tr>'
            +'<tr><td class="bg-info">&nbsp;Tgl/Jam Rencana Operasi </td><td>: '+profil['waktuoperasi']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Kode Booking</td><td>: '+profil['kodebooking']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;dr. Operator</td><td>: '+profil['namadokter']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Nama Poli</td><td>: '+profil['namaunit']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Jenis Tindakan</td><td>: '+profil['jenistindakan']+'</td></tr>'
            +'</table>\n\
            <table class="table-hover table-bordered col-xs-12 col-md-6">'
            +'<tr class="bg-yellow"><th colspan="2"><h4>&nbsp;Identitas Pasien</h4></th></tr>'
            +'<tr><td class="bg-info">&nbsp;No.RM</td><td>: '+profil['norm']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Nama Pasien </td><td>: '+profil['namalengkap']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Agama/Usia/Tgl.Lahir</td><td>: '+profil['agama']+' / '+profil['usia']+' / '+profil['tanggallahir']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Jenis Kelamin</td><td>: '+profil['jeniskelamin']+'</td></tr>'
            +'<tr><td class="bg-info">&nbsp;Alamat</td><td>: '+profil['alamat']+'</td></tr>'
            +'</table>';
    $('#profilepasien').empty();
    $('#profilepasien').html(profilePasien);
}

//menamppilkan jenis operasi
function jenisoperasi(jenisoperasi,selected)
{
    var jenis = '';
    var no=0;
    for(var x in jenisoperasi)
    {
        no++;
        var checked =  ((selected.jenisoperasi != null &&  selected.jenisoperasi.includes(jenisoperasi[x].id) ) ? 'checked' : '' )  ;
        jenis += '<label style="min-width:75px;"><input type="checkbox" name="jenisoperasi[]" '+ checked +' value="'+jenisoperasi[x].id+'" /> '+jenisoperasi[x].txt +'</label>&nbsp;' ;
        if(no > 4 )
        {
            jenis += '<br/>';
            no=0;
        }
    }
    $('#jenisoperasi').html(jenis);
}

//menampilkan jenis anestesi
function jenisanestesi(jenisanestesi,selected)
{
    var jenis = '';
    var no=0;
    for(var x in jenisanestesi)
    {
        no++;
        var checked = ((selected.jenisanestesi != null &&  selected.jenisanestesi.includes(jenisanestesi[x].id) ) ? 'checked' : '' )  ;
        jenis += '<label style="min-width:75px;"><input type="checkbox" name="jenisanestesi[]" '+checked+' value="'+jenisanestesi[x].id+'" /> '+jenisanestesi[x].txt +'</label>&nbsp;'+((no==4)? '':'') ;
        if(no > 3 )
        {
            no=0;
        }
    }
    $('#jenisanestesi').html(jenis);
}

//memberikan nilai form inputan berdasarkan id
function setvalformid(id,value){id.val(value);}

//save operasi
$(document).on('click','#saveoperasi',function(){
   $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Simpan Operasi',
        content: 'Simpan Tindakan dan Diagnosa Operasi.?',
        buttons: {
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    $('#FormOperasi').submit();
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//kembali 
$(document).on('click','#kembali',function(){
   $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Kembali ke jadwal operasi',
        content: 'Yakin perubahan tidak ingin disimpan?',
        buttons: {
            formSubmit: {
                text: 'konfirmasi',
                btnClass: 'btn-blue',
                action: function () {
                   window.location.href = base_url+"cpelayanan/jadwaloperasi";
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//pemberian obat barangpemeriksaan
$(document).on('click','#pemberian_obat',function(){
    var idbp     = $(this).attr('alt');
    var namaobat = $(this).attr('namaobat');
    var idbarang = $(this).attr('idbarang');
    var modalSize   = 'lg';
    var modalTitle  = 'Detail Pemberian Obat';
    var modalContent= '<div class="col-xs-12">\n\
                        <table class="">\n\
                            <tr><th>Obat/BHP</th><td> : ' + namaobat + '</td></tr>\n\
                            <tr><th>Jumlah Pemakaian</th><td> : <span id="detailpemberian_jumlahdiberikan"></span></td></tr>\n\
                        </table>\n\
                        <div id="detailpemberian_barangdiberikan"></div>\n\
                       </div>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'lg',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {    
            //menampilkan detail pemberian obat
            $.ajax({ 
                url: base_url+'cpelayanan/get_operasi_barangpemeriksaan_detail',
                type : "POST",      
                dataType : "JSON",
                data:{idbp:idbp,idbarang:idbarang},
                success: function(result){
//                    $('.select2').select2();
                    var dthtml = '<tr class="bg bg-yellow-gradient"><th>Batch.No</th><th>Kadaluarsa</th><th>Stok Unit</th><th>Diberikan</th></tr>';
                    for(var x in result)
                    {
                        dthtml += '<tr>\n\
                                    <input type="hidden" id="idbp_'+x+'" value="'+ idbp +'"/>\n\
                                    <input type="hidden" id="idp_'+x+'" value="'+ result[x].idbarangpembelian +'"/>\n\
                                    <input type="hidden" name="index[]" value="'+x+'"/>\n\
                                    <td>'+result[x].batchno+'</td>\n\
                                    <td>'+result[x].kadaluarsa+'</td>\n\
                                    <td><input type="hidden" id="detailpemberian_stok'+x+'" value="'+result[x].stok+'" />'+result[x].stok+'</td>\n\
                                    <td><input onchange="cekjumlahstok(this.value,'+x+')" id="detailpemberian_jumlah'+x+'" type="text" value="'+result[x].diberikan+'"/></td>\n\
                                   </tr>';
                    }
                    
                    //hitung diberikan
                    setTimeout(function(){ 
                        var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
                        var jumlah = 0;
                        for(var z in arrIndex)
                        {
                            jumlah += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
                        }
                        $('#detailpemberian_jumlahdiberikan').text(jumlah);
                    },1000);
                    
                    
                    $('#detailpemberian_barangdiberikan').html('<table class="table table-hover table-bordered table-striped" id="detailpemberian_barangdiberikan">'+dthtml+'</table>');
                },
                error: function(result){                  
                    notif(result.status, result.message);
                    return false;
                }
            });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});

function cekjumlahstok(jumlah,index)
{
    var stok   = parseFloat($('#detailpemberian_stok'+index).val());
    var jumlah = parseFloat($('#detailpemberian_jumlah'+index).val());
    var diberikan  = jumlah;
    //totaldiberikan
    var arrIndex = $("input[name='index[]']").map(function(){return $(this).val();}).get();
    var totdiberikan = 0;
    
    for(var z in arrIndex)
    {
        totdiberikan += parseFloat($('#detailpemberian_jumlah'+arrIndex[z]).val());
    }
    
    //set diberikan           
    diberikan  = jumlah;
    if(jumlah > stok)
    {
        notif('danger', 'Jumlah diberikan tidak boleh melebihi stok.!');
        diberikan  = stok;
    }        

    var idbp = $('#idbp_'+index).val();
    var idp  = $('#idp_'+index).val();        
    startLoading();
    $.ajax({ 
        url: base_url+'cpelayanan/detailpemberianobatoperasi',
        type : "post",      
        dataType : "json",
        data : {idbp:idbp,idp:idp,jumlah:diberikan},
        success: function(result) {                                                                   
            stopLoading();                
            $('#detailpemberian_jumlah'+index).val( diberikan );
            dtbarangpemeriksaan.ajax.reload();
        },
        error: function(result){  
            stopLoading();
            notif(result.status, result.message);
            return false;
        }
    });
}

$(document).on('click','#selesaipemberian_obat',function(){
    var idbarangoperasi = $(this).attr('alt');  
    var barang          = $(this).attr('alt2');  
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Selesai Pemberian Obat / BHP',
        content: '<b>Obat / BHP </b> = '+ barang,
        buttons: {
            formSubmit: {
                text: 'selesai',
                btnClass: 'btn-blue',
                action: function () {                    
                    startLoading();
                    $.ajax({ 
                        url: base_url+'cpelayanan/setdistribusibarangobatoperasi',
                        type : "post",      
                        dataType : "json",
                        data : {i:idbarangoperasi,s:'keluar'},
                        success: function(result) {                                                                   
                            stopLoading();                
                            dtbarangpemeriksaan.ajax.reload();
                        },
                        error: function(result){              
                            stopLoading();
                            notif(result.status, result.message);
                            return false;
                        }
                    });
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

$(document).on('click','#inputulangpemberian_obat',function()
{
    var idbarangoperasi = $(this).attr('alt');  
    var barang          = $(this).attr('alt2');  
    $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Input Ulang Pemberian Obat / BHP',
        content: '<b>Obat / BHP </b> = '+ barang,
        buttons: {
            formSubmit: {
                text: 'Input',
                btnClass: 'btn-blue',
                action: function () {                    
                    startLoading();
                    $.ajax({ 
                        url: base_url+'cpelayanan/setdistribusibarangobatoperasi',
                        type : "post",      
                        dataType : "json",
                        data : {i:idbarangoperasi,s:'pembetulankeluar'},
                        success: function(result) {                                                                   
                            stopLoading();                
                            dtbarangpemeriksaan.ajax.reload();
                        },
                        error: function(result){              
                            stopLoading();
                            notif(result.status, result.message);
                            return false;
                        }
                    });
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

//hapus petugas / batal petugas yang sudah dipilih
$(document).on('click','#hapuspetugas',function(){
   var idpetugas = $(this).attr('idpetugas');
   $('#'+idpetugas).empty();
   $('#'+idpetugas).html('<option value="0">Pilih</option>');
});

//tampil penggunaan barang
function tampilpenggunaanbarang()
{
    var idoperasi     = $('input[name="idoperasi"]').val();
    startLoading();
    $.ajax({ 
        url:  base_url+'cpelayanan/getdata_barangoperasi',
        type:"POST",
        dataType:"JSON",
        data:{idoperasi:idoperasi},
        success: function(result){
            stopLoading();
            var row = '';
            var sediaan = '';
            var no=0;
            for(var x in result)
            {
                if(sediaan != result[x].namasediaan)
                {
                    row += '<tr class="bg bg-gray text-bold"><td>&nbsp;</td><td colspan="3" > &nbsp;'+result[x].namasediaan+'</td></tr>';
                    no = 0;
                }
                row += '<tr id="'+result[x].idbarang+'">\n\
                    <td class="text-center">'+ ++no +'</td>\n\
                    <td>'+result[x].namabarang+'</td>\n\
                    <td id="'+result[x].idbarang+'"><input type="text" class="form-control" id="jumlahobat" idbarang="'+result[x].idbarang+'" idoperasi="'+idoperasi+'" idbarangoperasi="'+result[x].idbarangoperasi+'" idjenistarif="'+result[x].idjenistarif+'" harga="'+result[x].hargajual+'" value="'+ result[x].jumlahpemakaian +'"/></td>\n\
                    <td>'+result[x].namasatuan+'</td>\n\
                </tr>';
                sediaan = result[x].namasediaan;
            }
            $('#bodybarangpenggunaan').html(row);
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//ubah barang operasi
$(document).on('change','#jumlahobat',function(){
   var idoperasi       = $(this).attr('idoperasi');
   var idbarangoperasi = $(this).attr('idbarangoperasi');
   var idbarang        = $(this).attr('idbarang');
   var idjenistarif    = $(this).attr('idjenistarif');
   var harga           = $(this).attr('harga');   
   var idpendaftaran   = $('input[name="idpendaftaran"]').val();
   var jumlah          = this.value;
   
   startLoading();
    $.ajax({ 
        url:  base_url+'cpelayanan/updatedata_barangoperasi',
        type:"POST",
        dataType:"JSON",
        data:{
            idoperasi:idoperasi,
            idbarangoperasi:idbarangoperasi,
            idbarang:idbarang,
            idjenistarif:idjenistarif,
            idpendaftaran:idpendaftaran,
            harga:harga,
            jumlah:jumlah
        },
        success: function(result){
            stopLoading();            
            notif(result.status, result.message);
            if(idbarangoperasi == 0 && result.status == 'success')
            {
                tampilpenggunaanbarang();
            }
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
});