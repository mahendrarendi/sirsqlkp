//untuk menampilkan date picker di textbbox
function isiantrian() {
    $.ajax({ 
    url: 'loadantrian',
    type : "post",      
    dataType : "json",
    data : {s:$('select[name="stasiun"]').val(),L:$('#pilihloket').val(),tgl:ambiltanggal($('#tanggal').val()),all:(($('#semuaantrian:checked').val())?'1':'0')},
    success: function(result) {
        isiTabel(result);
        $('.input-sm').focus();
    },
    error: function(result){
        fungsiPesanGagal();
        return false;
    }
});
}

function isiTabel(result)
{
    var html = '<table id="tbl_listantrian" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">'+
    '<thead><tr><th>LOKET - NO.ANTRIAN - NAMA PASIEN</th><th></th></tr></thead><tbody>';
    for(var i in result)
    {
        var no = parseInt(i)+1;
        html += '<tr id="row'+no+'" align="right"><td><i class="'+((result[i].fontawesome == '')?'':result[i].fontawesome)+'"></i> '+result[i].namaloket+' - '+result[i].no+' - '+result[i].namalengkap+'</td>'
        if (result[i].status == "ok")
        {
            html += '<td>OK</td>';
        }
        else
        {
            var sedangproses = '';
            if (result[i].status != "sedang proses")
            {
                sedangproses = '<a id="sedangproses" idjenisloket="'+result[i].idjenisloket+'" idpendaftaran="'+result[i].idpendaftaran+'" idpemeriksaan="'+result[i].idpemeriksaan+'" alt="'+result[i].idantrian+'" alt2=" ('+result[i].namaloket+' no '+result[i].no+')" class=" btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Sedang Proses"><i class="fa fa-bolt"></i></a> ';
            }
            else
            {
                sedangproses = '-: Sedang Proses :-';
            }
            
            html += '<td id="tombol'+result[i].idantrian+'" align="left"> ' 
                    + '<a id="panggilulang" alt="'+result[i].idantrian+'" alt2=" ('+result[i].namaloket+' no '+result[i].no+')" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Panggil Ulang"><i class="fa fa-bullhorn"></i></a> ' 
                    + sedangproses
                    + '<a id="ok" idjenisloket="'+result[i].idjenisloket+'" idpendaftaran="'+result[i].idpendaftaran+'" idpemeriksaan="'+result[i].idpemeriksaan+'" alt="'+result[i].idantrian+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="OK"><i class="fa fa-check"></i></a> '
                    + '<a id="batal" idjenisloket="'+result[i].idjenisloket+'" idpendaftaran="'+result[i].idpendaftaran+'" idpemeriksaan="'+result[i].idpemeriksaan+'" alt="'+result[i].idantrian+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>'
                    + '</td>';
        }
        html += '</tr>';
    }
    html +='</tfoot></table>';
    $('#listantrian').empty();
    $('#listantrian').html(html);
    if(sessionStorage.panggilSemuaAntrian==='1'){$('#tbl_listantrian').DataTable({"stateSave": true,"paging":false});}
    $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
    // $('.table').DataTable(); //Initialize Data Tables
}

function buattombolloket()
{
  $.ajax({ 
    url: 'loadloket',
    type : "post",      
    dataType : "json",
    data : {s:$('select[name="stasiun"]').val(),tgl:ambiltanggal($('#tanggal').val())},
    success: function(result) {
        var html = '';
        var sta = [];
        var ant = [];
        var amb = [];
        for(i in result['statistik'])
        {
            ant[result['statistik'][i].idloket] = result['statistik'][i].antri;
            amb[result['statistik'][i].idloket] = result['statistik'][i].antriandiambil;
            sta[result['statistik'][i].idloket] = 'antri ' + result['statistik'][i].antri + ' &nbsp;total ' + result['statistik'][i].antriandiambil;
        }
        for(i in result['loket'])
        {
            if (amb[result['loket'][i].idloket] > 0)
            {
            html += '<a id="next" alt="'+result['loket'][i].idloket+'" alt2="'+result['loket'][i].idstasiun+'" style="cursor: pointer; color: #fff;" onclick="" style="color:#fff;"><div class="col-lg-3 col-xs-6">'
                    + '<div style="padding:4px " class="small-box '+ (ant[result['loket'][i].idloket]>0?'bg-green':'bg-red') +'" >'
            + '<div class="inner">'
              + '<h4>'+ result['loket'][i].namaloket +'</h4>'
              + '<p>'+ sta[result['loket'][i].idloket] +'</p>'
            + '</div>'
            + '<div class="icon">'
              + '<i class="'+((result['loket'][i].fontawesome == '')?'fa fa-bullhorn':result['loket'][i].fontawesome)+'"></i>'
            + '</div></div></div></a>';
            }
        }
        $('#tombolberikutnya').empty();
        $('#tombolberikutnya').html(html);
        isiantrian();

    },
    error: function(result){                  
        fungsiPesanGagal();
        return false;
    }
});
}

//dipanggil dari a id next - di dalam fungsi buattombolloket()
$(document).on("click","#next", function(){
    var idl = $(this).attr("alt");
    var ids = $(this).attr("alt2");
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: 'next', //alamat controller yang dituju (di js base url otomatis)
        data: {l:idl, s:ids}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
//            isiTabel(result);
            buattombolloket();
            if(result.status == 'notif')
            {
                alert(result.message+'<br> Status Tagian :'+result.statustagihan);
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
});

//dipanggil dari a id panggilulang - di dalam fungsi isiTabel()
$(document).on("click","#panggilulang", function(){
    var ida = $(this).attr("alt");
    var idl = $(this).attr("alt2");
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: 'panggilulang', //alamat controller yang dituju (di js base url otomatis)
        data: {a:ida}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status, result.message + idl);
            if(result.status == 'notif')
            {
                alert(result.message+'<br> Status Tagian <llabel class="label label-warning"> '+result.statustagihan+'</label>');
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
});

//dipanggil dari a id batal - di dalam fungsi isiTabel()
$(document).on("click","#batal", function(){
    var ida          = $(this).attr("alt");
    var idjenisloket = $(this).attr("idjenisloket");
    var idpendaftaran = $(this).attr("idpendaftaran");
    var idpemeriksaan = $(this).attr("idpemeriksaan");
        var formKonfirmasi = '<form action="" autocomplete="off">' +
                        '<div class="form-group">' +
                            '<label>Keterangan Pembatalan</label>' +
                            '<textarea id="alasanpembatalan" class="form-control" rows="5" placeholder="ketik keterangan pembatalan."></textarea>'+
                        '</div>'+                           
                    '</form>';
            
        $.confirm({
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Batal Antrian',
            content: formKonfirmasi,
            buttons: {
                formSubmit: {
                    text: 'konfirmasi',
                    btnClass: 'btn-blue',
                    action: function () {
                        var alasan = $('#alasanpembatalan').val();
                        if(alasan == '')
                        {
                            alert('Silakan Masukan Keterangan Pembatalan.');
                            return false;
                        }
                        else
                        {
                            startLoading();
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'batal', //alamat controller yang dituju (di js base url otomatis)
                                data: {
                                    a:ida,
                                    idjenisloket:idjenisloket,
                                    idpendaftaran:idpendaftaran,
                                    idpemeriksaan:idpemeriksaan,
                                    keterangan:alasan
                                }, //
                                dataType: "JSON", //tipe data yang dikirim
                                success: function(result) { //jika  berhasil
                                    stopLoading();
                                    notif(result.status, result.message);
                                    $('#tombol'+ida).empty();
                                    $('#tombol'+ida).html('Dibatalkan');
                                },
                                error: function(result) { //jika error
                                    stopLoading();
                                    console.log(result.responseText);
                                }
                            });
                        }
                        
                    }
                },
                //menu reset
                formReset:{
                    text: 'batal',
                    btnClass: 'btn-danger'
                }         
            },
            onContentReady: function () {
//                select2serachmulti($('#antrianok_pilihpasien'),'cmasterdata/fillpasienperiksa');
            }
            
        });
    
    
});

//dipanggil dari a id ok - di dalam fungsi isiTabel()
$(document).on("click","#ok", function(){
    var ida          = $(this).attr("alt");
    var idjenisloket = $(this).attr("idjenisloket");
    var idpendaftaran = $(this).attr("idpendaftaran");
    var idpemeriksaan = $(this).attr("idpemeriksaan");
    
    if(idjenisloket == 1)
    {
        var formAlat = '<form action="" id="formSewaalat" autocomplete="off">' +
                        '<div class="form-group bg bg-info">' +
                            '<p>Ketik NORM pasien yang dilayani, agar waktu pelayanan admisi terekam di sistem.</p>'+
                        '</div>'+ 
                        '<div class="form-group">' +
                            '<label>NORM PASIEN</label>' +
                            '<input type="text" class="form-control" id="antrianok_pilihpasien" placeholder="ketik norm"/>'+
                        '</div>'+                           
                    '</form>';
            
        $.confirm({
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Selesai Pelayanan Admisi',
            content: formAlat,
            buttons: {
                formSubmit: {
                    text: 'selesai',
                    btnClass: 'btn-blue',
                    action: function () {
                        idpendaftaran = $('#antrianok_pilihpasien').val();
                        if(idpendaftaran == null)
                        {
                            alert('Silakan Memilih Pasien Dahulu.');
                            return false;
                        }
                        else
                        {
                            antrian_proses_OK(ida,idjenisloket,idpendaftaran,idpemeriksaan);
                        }
                        
                    }
                },
                //menu reset
                formReset:{
                    text: 'batal',
                    btnClass: 'btn-danger'
                }         
            },
            onContentReady: function () {
            }
            
        });
    }
    else
    {
        antrian_proses_OK(ida,idjenisloket,idpendaftaran,idpemeriksaan);
    }
    
    
});

/**
 * Simpan Proses OK Antrian
 * @param {type} ida
 * @param {type} idjenisloket
 * @param {type} idpendaftaran
 * @param {type} idpemeriksaan
 * @returns {undefined}
 */
function antrian_proses_OK(ida,idjenisloket,idpendaftaran,idpemeriksaan)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: 'ok', //alamat controller yang dituju (di js base url otomatis)
        data: {
            a:ida,
            idjenisloket:idjenisloket,
            idpendaftaran:idpendaftaran,
            idpemeriksaan:idpemeriksaan
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            notif(result.status, result.message);
            $('#tombol'+ida).empty();
            $('#tombol'+ida).html('OK');
        },
        error: function(result) { //jika error
            stopLoading();
            console.log(result.responseText);
        }
    });
}

//dipanggil dari a id ok - di dalam fungsi isiTabel()
$(document).on("click","#sedangproses", function(){
    var ida = $(this).attr("alt");
    var idjenisloket  = $(this).attr('idjenisloket');
    var idpendaftaran = $(this).attr('idpendaftaran');
    var idpemeriksaan = $(this).attr('idpemeriksaan');
    var idb = $(this).attr("alt2");
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: 'sedangproses', //alamat controller yang dituju (di js base url otomatis)
        data: {
            a:ida,
            idjenisloket:idjenisloket,
            idpendaftaran:idpendaftaran,
            idpemeriksaan:idpemeriksaan
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            notif(result.status, result.message);
            $('#tombol'+ida).empty();
            $('#tombol'+ida).html('<a id="panggilulang" alt="'+ida+'" alt2="'+idb+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Panggil Ulang"><i class="fa fa-bullhorn"></i></a> -: Sedang Proses :- <a id="ok" alt="'+ida+'" idjenisloket="'+idjenisloket+'" idpendaftaran="'+idpendaftaran+'" idpemeriksaan="'+idpemeriksaan+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="OK"><i class="fa fa-check"></i></a>');
        },
        error: function(result) { //jika error
            stopLoading();
            console.log(result.responseText);
        }
    });
});


$(function () {
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now'); //Initialize Date picker
    buattombolloket();
    setInterval('buattombolloket()', 5000);//panggil ulang setiap 5 detik
    if(sessionStorage.panggilSemuaAntrian==='1')
    {
        tampilpilihloket($('#pilihloket'),$('#stasiun').val());
    }

});
// mahmud, clear -> tampil loket
function pilihstasiun(){tampilpilihloket($('#pilihloket'),$('#stasiun').val());}
