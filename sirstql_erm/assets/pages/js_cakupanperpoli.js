// PELAPORAN CAKUPAN POLI
$(document).ready(function () {
    $('input[type="radio"].flat-red').iCheck({radioClass:'iradio_flat-green'});
    $('input[name="hari"]').datepicker({autoclose: true, /* endDate: new Date(), */ format: "yyyy-mm-dd", /*viewMode: "months", minViewMode: "months",*/orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    $('input[name="bulan"]').datepicker({autoclose: true, format: "M-yyyy", viewMode: "months", minViewMode: "months",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
  generatedata();
});

// load data laporan report_stprs
function generatedata()
{
  startLoading();
  var order = $("input[name='order']:checked"). val();
  var date = ((order=='hari')?$('input[name="hari"]').val():$('input[name="bulan"]').val());
  $.ajax({
    url:base_url+"creport/cakupanperpoli_listdata",
    data:{t:date,o:order},
    type:"POST",
    dataType:"JSON",
    success: function(result){
      stopLoading();
      var data='';
      data +='<table  style="margin-top:5px;" class="table table-hover table-bordered table-striped" >';
      data +='<thead><tr class="bg bg-gray"><td>'+order.toUpperCase()+'</td><td>Jumlah Pasien</td></tr></thead>';
      data +='<tr><td>'+date+'</td><td>'+convertToRupiah(result[0].jumlahpasien)+'</td></tr>';
      data +='<thead><tr class="bg bg-gray"><td>Pasien Lama</td><td>Pasien Baru</td></tr></thead>';
      data +='<tr><td>'+convertToRupiah(result[0].pasienlama)+'</td><td>'+convertToRupiah(result[0].pasienbaru)+'</td></tr>';
      data +='</table>';
      data +='<table  style="margin-top:5px;" class="table table-hover table-bordered table-striped" >';
      
      
    var total=0, no=0, row=0;
     for(x in result){
         no++;row--;
         total+=parseInt(result[x].pasienperiksa);
         if(carabayar!=result[x].carabayar)
         {
             total=parseInt(result[x].pasienperiksa);
             data+='<tr class="bg-yellow text-center"><th colspan="5">'+result[x].carabayar+'</th></tr>';
             data+='<tr><th>No</th><th>Kabupaten</th><th>Kecamatan</th><th>Jumlah Pasien</th><th>Total Pasien</th></tr>';
         }
            
         data +='<tr><td>'+no+'</td><td>'+result[x].kabupaten+'</td><td>'+result[x].namakecamatan+'</td><td>'+result[x].pasienperiksa+'</td><td>'+total+'</td></tr>';
//         if(row-no!==0){data +='<tr><td colspan="3"><td>'+total+'</td></tr>';}
          var carabayar=result[x].carabayar;
     }
     data +='</table>';

        $('#tampildata').empty();
        $('#tampildata').html(data);
//      var dt = '', dtwaktu='', dtjenis='',dtjumlahbayar='',dtcarabayar='',dtpenyakit='',dttindakan='';
//      dtwaktu = ((result.jumlahpasien.length==0)?'<td colspan="3">Data Tidak ada</td>':'<td></td><td>'+result.jumlahpasien[0].waktu+'</td><td>'+result.jumlahpasien[0].jumlahpasien+'</td>');
//      dtjenis = ((result.jumlahpasien.length==0)?'<td colspan="3">Data Tidak ada</td>':'<td></td><td>'+result.jumlahpasien[0].pasienlama+'</td><td>'+result.jumlahpasien[0].pasienbaru+'</td>');
//      for(x in result.jumlahpasien)
//      {
//        dtcarabayar+='<td>'+result.jumlahpasien[x].carabayar+'</td>';
//        dtjumlahbayar+='<td>'+result.jumlahpasien[x].jumlahbayar+'</td>';
//      }
//      if(result.jumlahpasien.length==0){
//        dtcarabayar='<td colspan="3">Data Tidak ada</td>';
//        dtjumlahbayar='<td colspan="3">Data Tidak ada</td>';
//      }
//      var no =0;
//      for(i in result.penyakit){dtpenyakit+='<tr><td>'+ ++no +'</td><td>'+result.penyakit[i].icd+' '+result.penyakit[i].namaicd+'</td><td>'+result.penyakit[i].total+'</td><td>'+angkadesimal(result.penyakit[i].percentase,2)+' &#37;</td></tr>';}
//      var no =0;
//      for(i in result.tindakan){dttindakan+='<tr><td>'+ ++no +'</td><td>'+result.tindakan[i].icd+' '+result.tindakan[i].namaicd+'</td><td>'+result.tindakan[i].total+'</td><td>'+angkadesimal(result.tindakan[i].percentase,2)+' &#37;</td></tr>';}
//      $('#waktu').empty();
//      $('#waktu').html(dtwaktu);
//      $('#jenispasien').empty();
//      $('#jenispasien').html(dtjenis);
//      $('#carabayar').empty();
//      $('#carabayar').html(dtcarabayar);
//      $('#jumlahbayar').empty();
//      $('#jumlahbayar').html(dtjumlahbayar);
//      $('#penyakit').empty();
//      $('#penyakit').html(dtpenyakit);
//      $('#tindakan').empty();
//      $('#tindakan').html(dttindakan);
    },
    error:function()
    {
      stopLoading();
      fungsiPesanGagal();
      return false;
    }
  });
}