var table, jumlahicdtindakan = 0, val_rencanabarangobatbhp = [];
$(function(){      
    get_databangsal('#tampilbangsal',localStorage.getItem('idbangsal'));
    tampilDataRawatinap($('select[name="id_inap"]'), localStorage.getItem('idinap'));
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",(if_undefined(localStorage.getItem('tanggalawal'), 'now')));
    $('#tanggalakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",(if_undefined(localStorage.getItem('tanggalakhir'), 'now')));
    listpasien();
});

$(document).on('click','#reload',function(){
    localStorage.setItem('idbangsal','');
    localStorage.setItem('idinap','')
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    $('#tanggalakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    get_databangsal('#tampilbangsal','');
    tampilDataRawatinap($('select[name="id_inap"]'), '');
    setlocalstorage_daterange();
    listpasien();
});

$(document).on('change','#tampilbangsal',function(){
    localStorage.setItem('idbangsal',this.value);
    setlocalstorage_daterange();
    listpasien();
});

$(document).on('change','#statusperawatan',function(){
    setlocalstorage_daterange();
    listpasien();
});

$(document).on('click','#tampilchecklist',function(){
    setlocalstorage_daterange();
    listpasien();
});

$(document).on('change','#id_inap',function(){
    
    localStorage.setItem('idinap',this.value);
    setlocalstorage_daterange();
    listpasien();
});

function setlocalstorage_daterange()
{
    localStorage.setItem("tanggalawal",$('#tanggalawal').val());
    localStorage.setItem("tanggalakhir",$('#tanggalakhir').val());
}
$(document).on('click','#tampilsekarang',function(){
    listpasien('sekarang');
});
/// -- list data pasien
function listpasien(sekarang='')
{
    startLoading();
    var backgroundinfo ='', idrencanamedispemeriksaanterakhir, idrencanamedishasilpemeriksaanterakhir;
    var status      = $('#statusperawatan').val();
    var idbangsal   = localStorage.getItem('idbangsal');
    var idinap      = localStorage.getItem('idinap');
    var sekarang    = sekarang;
    var tglawal     = (localStorage.getItem("tanggalawal")==null) ? $('#tanggalawal').val() : localStorage.getItem("tanggalawal");
    var tglakhir    = (localStorage.getItem("tanggalakhir")==null) ? $('#tanggalakhir').val() : localStorage.getItem("tanggalakhir");
    var paket=null,paketparent=null, ordermode, x=0, no=0,notabel=0; 
    var html = '';
    $.ajax({
      url:base_url + 'cpelayananranap/caripasienchecklist',
      type:'POST',
      dataType:'JSON',
      data:{
          idinap:idinap,
          idbangsal:idbangsal,
          tglawal:tglawal,
          tglakhir:tglakhir,
          status:status,
          sekarang:sekarang
      },
      success : function(result){
          stopLoading();
          if(result==null)
          {
              html += '<tr class="text-center"><td colspan="6">No data available in table</td></tr>';
          }
          else
          {
              for (x in result)
              {
                  var paketparentlain=result[x].idpaketpemeriksaanparent;
                  if(result[x].status=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                  else if(result[x].status=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                  else if(result[x].status=='tunda'){backgroundinfo='background-color:#F7E59E;';}
                  else {backgroundinfo='';}
                  // list jadwal perawatan
                  if (result[x].idrencanamedispemeriksaan != idrencanamedispemeriksaanterakhir)
                  {
                      html +='<tr class="bg-gray" id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                             '<td>'+ ++notabel +'</td>'+
                             '<td>'+result[x].namabangsal+'</td>'+
                             '<td>'+result[x].pasien+'</td>'+
                             '<td colspan="3">'+result[x].waktuterbilang+' <div id="status_'+no+'"><div style="float:left;width:60px">'+result[x].status+'</div><div style="float:left"> ';
                              if(result[x].status=='terlaksana' || result[x].status=='batal'){
                                  html +='<a id="pemeriksaanranapBatalterlaksana_" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa fa-reply-all"></i></a> ';
                              }else{
                                  html +='<a id="pemeriksaanranapTerlaksana_" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check"></i></a> '+
                                         '<a id="pemeriksaanranapBatal_" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa  fa-minus"></i></a> ';
                              }
                              html+='</div></div></td></tr>';
                  }
                  
                  if(result[x].statuspelaksanaan=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                  else if(result[x].statuspelaksanaan=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                  else if(result[x].statuspelaksanaan=='tunda'){backgroundinfo='background-color:#F7E59E;';}
                  else {backgroundinfo='';}
                  // jika jenis bukan obat
                  if(result[x].jenistindakanobat!='obat')
                  {
                      // list paket
                      if(paket!=result[x].idpaketpemeriksaan & result[x].idpaketpemeriksaan!=null)
                      {
                          html += '<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                             '<td colspan="4"></td>'+ // '+result[x].idrencanamedishasilpemeriksaan+'
                             '<td>'+((result[x].idrencanamedishasilpemeriksaan != idrencanamedishasilpemeriksaanterakhir)?'':'|--')+'<b>'+result[x].namapaketpemeriksaan+'</b></td>'+
                             '<td id="status'+no+'"><div><div style="float:left;width:60px">'+result[x].statuspelaksanaan+'</div><div style="float:left;width:60px"></div><div style="float:left"> ';

                          // MENU TINDAKAN
                          if( paketparentlain=='null' ||  paketparentlain==null)
                          {
                              if(result[x].statuspelaksanaan=='terlaksana' || result[x].statuspelaksanaan=='batal'){
                               html +='<a onclick="ubahStatusPakettindakan('+result[x].idpaketpemeriksaan+',\'rencana\','+result[x].idrencanamedishasilpemeriksaan+','+result[x].idrencanamedispemeriksaan+')" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa fa-reply"></i></a> ';
                              }else{
                               html +='<a onclick="ubahStatusPakettindakan('+result[x].idpaketpemeriksaan+',\'terlaksana\','+result[x].idrencanamedishasilpemeriksaan+','+result[x].idrencanamedispemeriksaan+')" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check-square"></i></a> '+
                                      '<a onclick="ubahStatusPakettindakan('+result[x].idpaketpemeriksaan+',\'batal\','+result[x].idrencanamedishasilpemeriksaan+','+result[x].idrencanamedispemeriksaan+')" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-square"></i></a>';
                              }
                          }
                          html +='</div></div></td></tr>';
                      }

                  }
                  
                  // list tindakan || tampilkan tindakan jika icd tidak null
                  if(result[x].icd!=null)
                  {
                      html +='<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                             '<td colspan="4"></td>'+ //'+result[x].idrencanamedishasilpemeriksaan+'
                             // jika ada paket dan paket baru  atau jika tidak ada paket tampilkan menu
                             '<td>'+((result[x].idrencanamedishasilpemeriksaan != idrencanamedishasilpemeriksaanterakhir)?'':'|--')+result[x].tindakanobat+'</td>'+
                             '<td id="status'+no+'"><div><div style="float:left;width:60px">'+result[x].statuspelaksanaan+'</div><div style="float:left"> ';
                      // MENU 
                      if(result[x].idpaketpemeriksaan==null || result[x].idpaketpemeriksaan=='null')
                      {
                          if(result[x].statuspelaksanaan=='terlaksana' || result[x].statuspelaksanaan=='batal'){
                           html +='<a id="'+(result[x].jenistindakanobat=='tindakan'?'pemeriksaanranapBatalterlaksana':'obatranapBatalterlaksana')+'" nobaris="'+no+'" alt2="'+(result[x].jenistindakanobat=='tindakan'?result[x].idrencanamedispemeriksaan:result[x].idrencanamedisbarangpemeriksaan)+'" idn="'+result[x].idinap+'" icd="'+result[x].icd+'" jns="'+result[x].jenistindakanobat+'" idh="'+result[x].idrencanamedishasilpemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa fa-reply"></i></a> ';
                          }else{
                           html +='<a id="'+(result[x].jenistindakanobat=='tindakan'?'pemeriksaanranapTerlaksana':'obatranapTerlaksana')+'" nobaris="'+no+'" alt2="'+(result[x].jenistindakanobat=='tindakan'?result[x].idrencanamedispemeriksaan:result[x].idrencanamedisbarangpemeriksaan)+'" idn="'+result[x].idinap+'" icd="'+result[x].icd+'" jns="'+result[x].jenistindakanobat+'" idh="'+result[x].idrencanamedishasilpemeriksaan+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check-square"></i></a> '+
                                  '<a id="'+(result[x].jenistindakanobat=='tindakan'?'pemeriksaanranapBatal':'obatranapBatal')+'" nobaris="'+no+'" alt2="'+(result[x].jenistindakanobat=='tindakan'?result[x].idrencanamedispemeriksaan:result[x].idrencanamedisbarangpemeriksaan)+'" idn="'+result[x].idinap+'" icd="'+result[x].icd+'" jns="'+result[x].jenistindakanobat+'" idh="'+result[x].idrencanamedishasilpemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-square"></i></a>';
                          }
                      }
                      html +='</div></div></td></tr>';
                  }
                  idrencanamedispemeriksaanterakhir =  result[x].idrencanamedispemeriksaan;
                  idrencanamedishasilpemeriksaanterakhir =  result[x].idrencanamedishasilpemeriksaan;    
                  var paket=result[x].idpaketpemeriksaan;
                  var paketparent=result[x].idpaketpemeriksaanparent;
              }
          }
          $('#tampildata').html(html);
          loadbelumdiplot();
          $('[data-toggle="tooltip"]').tooltip();
      },
      error : function(result){
          fungsiPesanGagal();
          return false;
      }
    });
    
}

//basit, clear
function loadbelumdiplot()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/belumdiplot',
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            if (!is_null(result))
            {
                var belumdiplot = '',no=0;
                for (i in result)
                {
                    belumdiplot += '<tr><td>'+ ++no +'</td><td colspan="3">'+result[i].belumdiplot+'</td></tr>';
                }
                $('#pasienbelumdiplot').html('<table class="table table-striped"><thead><tr><th style="width: 10px">#</th><th colspan="3">Pasien Belum Ada Rencana Keperawatan</th></tr></thead><tbody>'+belumdiplot+'</tbody></table>');
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}


//---------- basit, clear
$(document).on("click","#pemeriksaanranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var idn =  $(this).attr("idn");
    var jns =  $(this).attr("jns");
    var icd =  $(this).attr("icd");
    var idh =  $(this).attr("idh");
    fungsi_pemeriksaanranapUbahStatus('terlaksana',id,nobaris, idn,jns,icd,idh);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var idn =  $(this).attr("idn");
    var jns =  $(this).attr("jns");
    var icd =  $(this).attr("icd");
    var idh =  $(this).attr("idh");
    fungsi_pemeriksaanranapUbahStatus('batal',id,nobaris, idn,jns,icd,idh);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var idn =  $(this).attr("idn");
    var jns =  $(this).attr("jns");
    var icd =  $(this).attr("icd");
    var idh =  $(this).attr("idh");
    fungsi_pemeriksaanranapUbahStatus('rencana',id,nobaris, idn,jns,icd,idh);
});

//---------- basit, clear
$(document).on("click","#obatranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_obatranapUbahStatus('terlaksana',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#obatranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_obatranapUbahStatus('batal',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#obatranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_obatranapUbahStatus('rencana',id,nobaris);
});

//---------- basit, clear
$(document).on("click","#pemeriksaanranapTerlaksana_", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus_('terlaksana',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatal_", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus_('batal',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatalterlaksana_", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus_('rencana',id,nobaris);
});

//---------- basit, clear
function fungsi_pemeriksaanranapUbahStatus(status,id,nobaris,idn=null,jns=null,icd=null,idh=null) //fungsi ubah status
{
    startLoading();
    var backgroundinfo='';
    if(status=='selesai'){backgroundinfo='#e0f0d8;';}
    else if(status=='batal'){backgroundinfo='#f0D8D8;';}
    else if(status=='tunda'){backgroundinfo='#F7E59E;';}
    $.ajax({ 
        url: 'pemeriksaanranap_checklist_ubahstatus',
        type : "post",      
        dataType : "json",
        data : {i:id, status:status,idn:idn,jns:jns,icd:icd,idh:idh},
        success: function(result) {
            stopLoading();
           // notif(result.status, result.message);
//            listpasien('', '');
            if(result.status=='success')
            {
                if(status=='terlaksana' || status=='batal')
                {
                    
                    $('#status'+nobaris).empty();
                    $('#status'+nobaris).html('<div style="'+backgroundinfo+'"><div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="pemeriksaanranapBatalterlaksana" nobaris="'+nobaris+'" alt2="'+id+'" idn="'+idn+'" icd="'+icd+'" jns="'+jns+'" idh="'+idh+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa fa-reply"></i></a></div></div> ');
                }
                else 
                {
                    $('#status'+nobaris).empty();
                    $('#status'+nobaris).html('<div style="'+backgroundinfo+'"><div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="pemeriksaanranapTerlaksana" nobaris="'+nobaris+'" alt2="'+id+'" idn="'+idn+'" icd="'+icd+'" jns="'+jns+'" idh="'+idh+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check-square"></i></a> '+
                                                '<a id="pemeriksaanranapBatal" nobaris="'+nobaris+'" alt2="'+id+'" idn="'+idn+'" icd="'+icd+'" jns="'+jns+'" idh="'+idh+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-square"></i></a></div></div> ');
                }
                $('#row'+nobaris).css("background-color",backgroundinfo);
            }
        },
        error: function(result){  
            stopLoading();
//            fungsiPesanGagal();
            return false;
        }
    }); 
}

//---------- basit, clear
function fungsi_obatranapUbahStatus(status,id,nobaris) //fungsi ubah status
{
    startLoading();
    $.ajax({ 
        url: 'obatranap_checklist_ubahstatus',
        type : "post",      
        dataType : "json",
        data : {i:id, status:status},
        success: function(result) {
            stopLoading();
//            notif(result.status, result.message);
//            listpasien('', '');
            var backgroundinfo='';
            if(status=='selesai'){backgroundinfo='#e0f0d8;';}
            else if(status=='batal'){backgroundinfo='#f0D8D8;';}
            else if(status=='tunda'){backgroundinfo='#F7E59E;';}
    
            if(result.status=='success')
            {
                if(status=='terlaksana' || status=='batal')
                {
                    $('#status'+nobaris).empty();
                    $('#status'+nobaris).html('<div><div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="obatranapBatalterlaksana" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa fa-reply"></i></a></div></div> ');
                }
                else 
                {
                    $('#status'+nobaris).empty();
                    $('#status'+nobaris).html('<div><div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="obatranapTerlaksana" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check-square"></i></a> '+
                                                '<a id="obatranapBatal" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-square"></i></a></div></div> ');
                }
            }            
            $('#row'+nobaris).css("background-color",backgroundinfo);
        },
        error: function(result){ 
            stopLoading();
//            fungsiPesanGagal();
            return false;
        }
    }); 
}

//---------- basit, clear
function fungsi_pemeriksaanranapUbahStatus_(status,id,nobaris) //fungsi ubah status
{
    $.ajax({ 
        url: 'pemeriksaanranap_checklist_ubahstatus_',
        type : "post",      
        dataType : "json",
        data : {i:id, status:status},
        success: function(result) {
           
            if(status=='terlaksana')
            {
                listpasien();
            }
            else
            {
                if(result.status=='success')
                {
                    if(status=='terlaksana' || status=='batal')
                    {
                          listpasien();
//                        $('#status_'+nobaris).empty();
//                        $('#status_'+nobaris).html('<div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="pemeriksaanranapBatalterlaksana_" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa  fa-reply-all"></i></a></div> ');
                    }
                    else 
                    {
                        $('#status_'+nobaris).empty();
                        $('#status_'+nobaris).html('<div style="float:left;width:60px">'+status+'</div><div style="float:left"> ' + '<a id="pemeriksaanranapTerlaksana_" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check"></i></a> '+
                                                    '<a id="pemeriksaanranapBatal_" nobaris="'+nobaris+'" alt2="'+id+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus"></i></a></div> ');
                    }
                }    
            }
            
        },
        error: function(result){                  
//            fungsiPesanGagal();
            return false;
        }
    }); 
}

function detailrencanabelumterlaksana(rencana)
{
    var list_rencana='';
    for(var x in rencana)
    {
        list_rencana += '<tr><td>'+rencana[x].icd+'</td><td>'+rencana[x].statuspelaksanaan+'</td></tr>';
    }
    
    var modalTitle = 'Rencana Perawatan Belum Terlaksana';
    var modalContent = '<form action="">' +
                          '<table class="table table-striped">\n\
                            <tr class="bg bg-yellow-gradient"><td>Rencana Perawatan</td><td>Status Perawatan</td></tr>\n\
                            '+list_rencana+'\n\
                            </table> \n\
                            <small class="text text-red">#ubah status tindakan dan obat yang direncanakan menjadi terlaksana atau batal untuk dapat menyelesaikan perencanaan.</text>'+
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        }
    });
}

// mahmud, clear
function ubahStatusPakettindakan(id,status,irhp,irp)
{
    // id     : idpaketpemeriksaan
    // status : statuspelaksanaan
    // irhp   : id rencana medis hasil pemeriksaan
    // irp    : id rencana medis pemeriksaan
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpaket",
        data:{x:'paket pemeriksaan', y:id, z:status,a:irp,b:irhp},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success'){
                listpasien('pasien', localStorage.getItem('poliselected'),localStorage.getItem('idinap'));
            }//tampil hasil laboratorium
        },
        error:function(result){
            fungsiPesanGagal();return false;
        }
    });
}