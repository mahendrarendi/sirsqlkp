// ************ LIST LOCAL STORAGE
// idinap
// idpoli
// 
var table, jumlahicdtindakan = 0, val_rencanabarangobatbhp = [];
$(function(){ 

    get_databangsal('#tampilbangsal'); 
    var idinap = localStorage.getItem('idinap');           //ambil idinap yg dibuat dari administrasi ranap
    if(idinap!==null){return search_by_datapasien(idinap);}//jika id inap tidak kosong maka tampilkan data pemeriksaan pasien dengan id inap yg dipilih 
    var idpoli = localStorage.getItem('poliselected');
    listpasien('bangsal', idpoli);
});

//---------- basit, clear
/// --  cari berdasarkan bangsal
function search_by_databangsal(x){listpasien('bangsal', x);}
function search_by_datapasien(x){listpasien('pasien',localStorage.getItem('poliselected'),x);}
//---------- basit, clear
/// --  cari berdasarkan poli
function search_by_daterange(){listpasien('bangsal',localStorage.getItem('poliselected'));}
function search_by_now(){listpasien('now',localStorage.getItem('poliselected'));}
//---------- basit, clear
function setpoli_afterselect(x)
{
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("poliselected",x);

    } else {
         pesanUndefinedLocalStorage();
    }
}

//---------- basit, clear
function menubar_table()
{
    $("div.toolbar").html('<select id="tampilbangsal" name="bangsal" onchange="search_by_databangsal(this.value)" class="form-control" style="margin-right:4px;"></select><input id="tanggalawal" size="4" class="datepicker form-control" placeholder="Tanggal awal" > <input id="tanggalakhir" size="4" class="datepicker form-control" placeholder="Tanggal akhir"> <a class="btn btn-info btn-sm" onclick="search_by_daterange()"><i class="fa fa-desktop"></i> Tampil</a> <a style="margin-right: 6px;" onclick="refresh_pagebangsaljs()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a style="margin-right: 6px;" onclick="search_by_now()" class="btn btn-warning btn-sm"><i class="fa fa-chevron-circle-down"></i> Tampil Sekarang</a> <select class="select2 form-control" style="width:258px;" name="id_inap" onchange="search_by_datapasien(this.value)"><option value="0">Pilih</option></select>');
    $("div.dataTables_filter").after('<table class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%"><tbody><tr><a style="margin-right: 6px;" class="btn btn-success btn-sm" onclick="tambah_rencana_perawatan()"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan</a><a style="margin-right: 6px;" class="btn btn-primary btn-sm" onclick="tambah_rencana_perawatan_paket()"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan [Paket]</a> <a style="margin-right: 6px;" class="btn btn-warning btn-sm" onclick="tambah_rencana_bhp()"><i class="fa fa-plus-circle"></i> Tambah/Edit BHP / Obat</a><td></td></tr></tbody></table>');
    $("div.dataTables_filter").after('<div id="pasienbelumdiplot"></div>');
    tampilDataRawatinap($('select[name="id_inap"]')); //ambil data inap
}
//--- basit, clear
function tambah_rencana_perawatan()
{
    jumlahicdtindakan = 0;
    jumlahrencanaperawatan = 0 ;
    var modalTitle = 'Tambah Rencana Perawatan';
    var modalContent = '<form action="" id="FormRencanaPerawatan">' +
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Pasien Rawat Inap</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select>'+
                                '</div>' +
                                '<div class="col-sm-4">' +
                                '<label>Dokter (Opsional)</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idpegawaidokter">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>' +
                            '<div class="form-group">' +
                                '<div class="col-sm-12">' +
                                '<label>Waktu Mulai Perawatan (tahun-bulan-tanggal jam)</label>' +
                                '<input type="text" name="tgl" class="datetimepicker form-control"/>'+
                                '</div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>' +
                            '<div class="form-group col-sm-12">'+
                                '<a id="tambahrencanaperawatan" class=" btn btn-warning btn-sm"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan</a>'+
                                '<table id="rencanaperawatan" width="100%" class="table table-hover table-bordered table-striped">'+
                                '<tr style="border-top:3px solid #f39c10;"><td style="padding: 14px 0px 10px 0px;">'+
                                '<div class="form-group">'+
                                
                                '<div class="col-sm-5">'+
                                '<label>ICD Rencana Perawatan</label>'+
                                '<select class="select2 form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'"  name="icd[]">'+
                                '<option value="0">Pilih ICD Perawatan</option>'+
                                '</select>'+
                                '</div>'+
                                
                                '<div class="col-sm-2">'+
                                '<label>Jumlah Tindakan</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value="">'+
                                '</div>'+
                                
                                '<div class="col-sm-3">'+
                                '<label>Jumlah Per Hari</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value="">'+
                                '</div> '+
                                
                                '<div class="col-sm-2">'+
                                '<label>Selama (Hari)</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="selama[]" value="">'+
                                '</div>'+
                                
                                '</div>'+
                                '<div class="col-sm-12">&nbsp;</div>' +
                                '<div class="form-group">'+
                                '<div id="rencanabarangbarang'+jumlahrencanaperawatan+'" class="col-sm-2"><a id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Tambah Rencana BHP/Obat"><i class="fa fa-plus-circle"></i> Tambah Rencana BHP/Obat</a>'+
                                '</div> '+
                                '<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label>'+
                                '</div> '+
                                '<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pesan</label>'+
                                '</div> '+
                                '<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label>'+
                                '</div>'+
                                '<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pakai (Per Tindakan)</label>'+
                                '</div>'+
                                '</div>'+
                                '</td></tr></table>'+
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'lg',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buat rencana keperawatan',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tgl"]').val() == '')
                    {
                        alert_empty('tgl');
                        return false;
                    }
                    else if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else if ($('select[name="icd"]').val() == 0)
                    {
                        alert_empty('icd');
                        return false;
                    }
                    else if ($('input[name="jumlahperhari"]').val() == '')
                    {
                        alert_empty('jumlahperhari');
                        return false;
                    }
                    else if ($('input[name="selama"]').val() == '')
                    {
                        alert_empty('selama');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("idinapselected",$('select[name="idinap"]').val());
                        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
                        localStorage.setItem("jumlahtindakan",$('input[name="jumlahtindakan"]').val());
                        localStorage.setItem("selama",$('input[name="selama"]').val());
                        simpanrencana();
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinapselected')); //ambil data inap
        tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5"); //ambil data icd
        tampilDataDokter($('select[name="idpegawaidokter"]')); //ambil data icd
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function simpanrencana()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencana_trial',
        //data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icd"]').val(), ipd:$('select[name="idpegawaidokter"]').val(), td:$('input[name="jumlahtindakan"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        data: $("#FormRencanaPerawatan").serialize(),
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
             notif(result.status,result.message);
//            listpasien('','');
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//--- basit, on progress
function tambah_rencana_perawatan_paket()
{
    var modalTitle = 'Tambah Rencana Perawatan [Paket]';
    var modalContent = '<form action="" id="FormRencanaPerawatan">' +
                            '<div class="form-group">' +
                                '<label>Waktu Mulai Perawatan (tahun-bulan-tanggal jam)</label>' +
                                '<input type="text" name="tgl" class="datetimepicker form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Pasien Rawat Inap</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>ICD Paket</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="icdpaket">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Jumlah Per Hari</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahperhari" value="'+localStorage.getItem('jumlahperhari')+'">'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Selama (Hari)</label>' +
                                '<input type="text" class="form-control" style="width:100%;" name="selama" value="'+localStorage.getItem('selama')+'">'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buat rencana keperawatan [paket]',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tgl"]').val() == '')
                    {
                        alert_empty('tgl');
                        return false;
                    }
                    else if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else if ($('select[name="icdpaket"]').val() == 0)
                    {
                        alert_empty('icdpaket');
                        return false;
                    }
                    else if ($('input[name="jumlahperhari"]').val() == '')
                    {
                        alert_empty('jumlahperhari');
                        return false;
                    }
                    else if ($('input[name="selama"]').val() == '')
                    {
                        alert_empty('selama');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("idinapselected",$('select[name="idinap"]').val());
                        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
                        localStorage.setItem("selama",$('input[name="selama"]').val());
                        simpanrencanapaket();
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinapselected')); //ambil data inap
        tampilDataIcdpaket($('select[name="icdpaket"]'), "1,3,4,5"); //ambil data icd
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}


//basit, clear
function simpanrencanapaket()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencanapaket',
        data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icdpaket"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            listpasien('', '');
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//--- basit, clear
function tambah_rencana_bhp()
{
    var modalTitle = 'Rencana BHP / Obat';
    var modalContent = '<form action="" id="FormRencanaBHP">' +
                            '<div class="form-group">' +
                                '<label>Pasien Rawat Inap</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buka menu rencana bhp/obat',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else
                    {
                        if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
                            localStorage.setItem("idinapselected",$('select[name="idinap"]').val());
                            localStorage.removeItem("idrencanamedispemeriksaan");
                            localStorage.setItem("idinap", $('select[name="idinap"]').val());
                            localStorage.removeItem("norm");
                            localStorage.removeItem("idpendaftaranranap");
                            window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp';
                        }
                    }
                }
            },
            //menu back
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinapselected')); //ambil data inap
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

/// -- list data pasien, on progress
function listpasien(jns='', idpoli='',x='')
{
    startLoading();
    setpoli_afterselect(idpoli); 
    var backgroundinfo ='';
    var data        = (jns=='bangsal') ? ((idpoli=='') ? $('select[name="bangsal"]').val() : idpoli ) : (jns=='pasien') ? ((x=='') ? $('select[name="id_inap"]').val() : x ) : '';
    var tglawal     = (is_undefined($('#tanggalawal').val()) && !is_undefined(localStorage.getItem("tanggalawal")))?localStorage.getItem("tanggalawal"):$('#tanggalawal').val();
    var tglakhir    = (is_undefined($('#tanggalakhir').val()) && !is_undefined(localStorage.getItem("tanggalakhir")))?localStorage.getItem("tanggalakhir"):$('#tanggalakhir').val();
    var x=0, no=0, html = '<table id="table" class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">'+
                  '<thead>'+
                  '<tr>'+
                    '<th>No</th>'+
                    '<th>NO.RM</th>'+
                    '<th>NAMA PASIEN</th>'+
                    '<th>NO.SEP</th>'+
                    '<th>WAKTU PEMERIKSAAN</th>'+
                    '<th>DOKTER DPJP</th>'+
                    '<th>BANGSAL</th>'+
                    '<th>STATUS</th>'+
                    '<th style="width:90px;">AKSI</th>'+
                  '</tr>'+
                  '</thead>'+
                  '<tbody>';

                  /////////cari dan tampilkan data
                  $.ajax({
                    url:base_url + 'cpelayananranap/getpasienranap',
                    type:'POST',
                    dataType:'JSON',
                    data:{data:data,tglawal:tglawal, tglakhir:tglakhir,jenis:jns},
                    success : function(result){
                        stopLoading();
                        for (x in result)
                        {
                        if(result[x].status=='selesai'){backgroundinfo='background-color:#e0f0d8;';}
                        else if(result[x].status=='batal'){backgroundinfo='background-color:#f0D8D8;';}
                        else if(result[x].status=='tunda'){backgroundinfo='background-color:#F7E59E;';}
                        else {backgroundinfo='';}
                            html += '<tr id="row'+ ++no +'" style="'+ backgroundinfo +'">'+
                                        '<td>'+ no +'</td>'+
                                       '<td>'+result[x].norm+'</td>'+
                                       '<td>'+result[x].namalengkap+'</td>'+
                                       '<td>'+result[x].nosep+'</td>'+
                                       '<td>'+result[x].waktu+'</td>'+
                                       '<td>'+result[x].namadokter+'</td>'+
                                       '<td>'+result[x].namabangsal+'</td>'+
                                       '<td id="status'+no+'">'+result[x].status+'</td>';
                                       
                                       if(result[x].status=='terlaksana' || result[x].status=='batal')
                                       {
                                        html +='<td>'+
                                            '<a id="pemeriksaanranapView" nomori="'+result[x].idinap+'" alt="'+result[x].idrencanamedispemeriksaan+'" norm="'+result[x].norm+'" alt2="'+result[x].idpendaftaran+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Hasil Periksa"><i class="fa fa-eye"></i></a> '+
                                            '<a id="pemeriksaanranapBHP" alt="'+result[x].idinap+'" norm="'+result[x].norm+'" alt2="'+result[x].idpendaftaran+'" alt3="'+result[x].idkelas+'" alt5="'+result[x].idkelasjaminan+'" alt4="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="BHP/Farmasi"><i class="fa fa-toggle-off"></i></a> '+
                                        //                                            '<a id="pemeriksaanranapViewBHP" alt="'+result[x].idinap+'" norm="'+result[x].norm+'" alt2="'+result[x].idpendaftaran+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Lihat BHP/Farmasi"><i class="fa fa-binoculars"></i></a> '+
                                            '<a id="pemeriksaanranapBatalterlaksana" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa  fa-times"></i></a> '+
                                       '</td>'+
                                       '</tr>';
                                       }
                                       else 
                                       {
                                        html +='<td>'+
                                            '<a id="pemeriksaanranapPeriksa" nomori="'+result[x].idinap+'" alt="'+result[x].idrencanamedispemeriksaan+'" norm="'+result[x].norm+'" alt2="'+result[x].idpendaftaran+'" alt3="'+result[x].idkelas+'" alt5="'+result[x].idkelasjaminan+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Pemeriksaan"><i class="fa fa-stethoscope"></i></a> '+
                                            '<a id="pemeriksaanranapBHP" alt="'+result[x].idinap+'" norm="'+result[x].norm+'" alt2="'+result[x].idpendaftaran+'" alt3="'+result[x].idkelas+'" alt5="'+result[x].idkelasjaminan+'" alt4="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="BHP/Farmasi"><i class="fa fa-toggle-off"></i></a> '+
//                                            '<a id="pemeriksaanranapRujuk" alt="'+result[x].idrencanamedispemeriksaan+'" alt2="'+ambiltanggal2(result[x].tanggal)+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Tambah Layanan" ><i class="fa fa-plus"></i></a> '+
                                             '<a id="pemeriksaanranapTerlaksana" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check"></i></a> '+
                                            '<a id="pemeriksaanranapBatal" nobaris="'+no+'" alt2="'+result[x].idrencanamedispemeriksaan+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-circle"></i></a> '+
                                       '</td>'+
                                       '</tr>';
                                       }
                                       
                        }
                        $('#listpasien').empty();
                        $('#listpasien').html(html+'</tfoot></table>');
                        table = $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,}); //Initialize Data Tables
                        $('[data-toggle="tooltip"]').tooltip();
                        menubar_table();
                        get_databangsal('#tampilbangsal',idpoli);
                        tampilDataRawatinap($('select[name="id_inap"]'), data);
                        loadbelumdiplot();
                        $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglawal, 'now')));
                        $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",(if_undefined(tglakhir, 'now')));
                        if (!is_undefined(tglawal)) localStorage.setItem("tanggalawal",tglawal);
                        if (!is_undefined(tglakhir)) localStorage.setItem("tanggalakhir",tglakhir);
                    },
                    error : function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                  });
    
}

//basit, clear
function loadbelumdiplot()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/belumdiplot',
        dataType: "JSON", //tipe data yang dikirim
        data:{idbangsal:localStorage.getItem('poliselected')},
        success: function(result) { //jika  berhasil
            if (!is_null(result))
            {
                var belumdiplot = '',no=0;
                for (i in result)
                {
                    belumdiplot += '<tr><td>'+ ++no +'</td><td colspan="3">'+result[i].belumdiplot+'</td></tr>';
                }
                $('#pasienbelumdiplot').html('<table class="table table-striped"><thead><tr><th style="width: 10px">#</th><th colspan="3">Pasien Belum Ada Rencana Keperawatan</th></tr></thead><tbody>'+belumdiplot+'</tbody></table>');
            }
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

//---------- basit, clear
function refresh_pagebangsaljs()
{  
    table.state.clear();
    get_databangsal('#tampilbangsal','');
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
    listpasien('bangsal', localStorage.getItem('poliselected'));
}
/////////////////END CARI PASIEN////////////////

//---------- basit, clear
$(document).on("click","#pemeriksaanranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('terlaksana','Pemeriksaan Terlaksana?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('batal','Batal Pemeriksaan?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanranapUbahStatus('rencana','Batal Terlaksana?',id,nobaris);
});

//--
$(document).on("click","#tambahrencanabarang", function(){
    var jumlahrencana = $(this).attr("jumlahrencana");
    jumlahicdtindakan++;
    $('#rencanabarangobatbhp'+jumlahrencana).append('<select class="select2 form-control" style="width:100%;" id="rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangobatbhp'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select>');
    $('#rencanabarangjumlahpesan'+jumlahrencana).append('<input type="text" class="form-control" style="width:100%;" id="rencanabarangjumlahpesan'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpesan'+jumlahrencana+'['+jumlahicdtindakan+']" />');
    $('#rencanabarangsatuanpakai'+jumlahrencana).append('<select class="select2 form-control" style="width:100%;" id="rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangsatuanpakai'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select>');
    $('#rencanabarangjumlahpakai'+jumlahrencana).append('<input type="text" class="form-control" style="width:100%;" id="rencanabarangjumlahpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpakai'+jumlahrencana+'['+jumlahicdtindakan+']" />');
    isicombobhp('rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan);
    isicombosatuan('rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'', '');
});

$(document).on("click","#tambahrencanaperawatan", function(){
   jumlahrencanaperawatan++;
   var rencana='<tr style="border-top:3px solid #f39c10;"><td style="padding: 14px 0px 10px 0px;">'+
                                '<div class="form-group">'+
                                '<div class="col-sm-5">'+
                                '<label>ICD Rencana Perawatan</label>'+
                                '<select class="select2 form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'" name="icd[]">'+
                                '<option value="0">Pilih ICD Perawatan</option>'+
                                '</select>'+
                                '</div>'+
                                
                                '<div class="col-sm-2">'+
                                '<label>Jumlah Tindakan</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value="">'+
                                '</div>'+
                                
                                '<div class="col-sm-3">'+
                                '<label>Jumlah Per Hari</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value="">'+
                                '</div> '+
                                
                                '<div class="col-sm-2">'+
                                '<label>Selama (Hari)</label>'+
                                '<input type="text" class="form-control" style="width:100%;" name="selama[]" value="">'+
                                '</div>'+
                                
                                '</div>'+
                                '<div class="col-sm-12">&nbsp;</div>' +
                                '<div class="form-group">'+
                                '<div id="rencanabarangbarang'+jumlahrencanaperawatan+'" class="col-sm-2"><a id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Tambah Rencana BHP/Obat"><i class="fa fa-plus-circle"></i> Tambah Rencana BHP/Obat</a>'+
                                '</div> '+
                                '<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label>'+
                                '</div> '+
                                '<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pesan</label>'+
                                '</div> '+
                                '<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label>'+
                                '</div>'+
                                '<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pakai (Per Tindakan)</label>'+
                                '</div>'+
                                '</div>'+
                                '</td></tr>';
   $('#rencanaperawatan').append(rencana);
  tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5"); //ambil data icd
});

function isicombobhp(id)//cari bhp
{
    $('select[id="'+id+'"]').select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
        stopLoading();
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idbarang, text: item.namabarang +' _ Rp '+ convertToRupiah(item.hargajual) +' _ '+ convertToRupiah(item.stok) }}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}

//basit, clear
function isicombosatuan(id, selected)
{
    if (is_empty(localStorage.getItem('dataSatuan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_satuan',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataSatuan', JSON.stringify(result));
                fillcombosatuan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombosatuan(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombosatuan(id, selected);
            },
            error: function(result){
                fillcombosatuan(id, selected);
            }
        });
    }
}

//basit, clear
function fillcombosatuan(id, selected)
{
    var datasatuan = JSON.parse(localStorage.getItem('dataSatuan'));
    var i=0;
    for (i in datasatuan)
    {
      $("#"+id).append('<option ' + ((datasatuan[i].idsatuan===selected)?'selected':'') + ' value="'+datasatuan[i].idsatuan+'">'+datasatuan[i].namasatuan+'</option>');
    }
    $("#"+id).select2();
}

//---------- basit, clear
function fungsi_pemeriksaanranapUbahStatus(status,content,id,nobaris) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: content,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'pemeriksaanranap_ubahstatus',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                        notif(result.status, result.message);
                        listpasien('', '');
                        if(result.status=='success')
                        {
                            $('#status'+nobaris).empty();
                            $('#status'+nobaris).text(status);
                        }
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
$(document).on("click","#pemeriksaanranapRujuk", function(){ 
    var id = $(this).attr("alt");
    var tanggalJadwal = $(this).attr("alt2");
    var modalTitle = 'Rujuk Pemeriksaan Pasien';
    var modalContent = '<form action="" id="FormRujuk">' +
                            '<input type="hidden" value="'+id+'" name="idperiksa">'+
                            '<div class="form-group">' +
                                '<label>Poliklinik</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idjadwal">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Status</label>' +
                                '<select class="form-control" style="width:100%;" name="status">'+
                                '<option value="0">Pilih</option>'+
                                '<option value="rujuk internal kembali">Rujuk Internal Kembali</option>'+
                                '<option value="rujuk internal tidak kembali">Rujuk Internal Tidak Kembali</option>'+
                                '<option value="rujuk eksternal">Rujuk Internal Eksternal</option>'+
                                '</select>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Rujuk',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="poliklinik"]').val()==='0')
                    {
                            alert_empty('poliklinik');
                            return false;
                    }
                    else if($('select[name="status"]').val()==='0')
                    {
                            alert_empty('status');
                            return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: 'pemeriksaanranap_saverujuk', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormRujuk").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                qlReloadPage(600); //reload page
                            },
                            error: function(result) { //jika error
                                fungsiPesanGagal(); // console.log(result.responseText);
                                return false;
                            }
                        });
                    }
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        tampilDataPoliklinik($('select[name="idjadwal"]'),tanggalJadwal); //ambil data poli
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapPeriksa", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        localStorage.setItem("idkelas", $(this).attr("alt3"));
        localStorage.setItem("idkelasjaminan", $(this).attr("alt5"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_periksa';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBHP", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt4"));
        localStorage.setItem("idinap", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        localStorage.setItem("idkelas", $(this).attr("alt3"));
        localStorage.setItem("idkelasjaminan", $(this).attr("alt5"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapView", function(){ //view pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idinap");
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_view';
    } else {
         pesanUndefinedLocalStorage();
    }
});
