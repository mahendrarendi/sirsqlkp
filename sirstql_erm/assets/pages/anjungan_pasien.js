$(function () {
    // Initialize select2
    $('.select2').select2();
    //Initialize Date picker
    $('.datepicker').datepicker({
      autoclose: true,
      format: "dd/mm/yyyy",
      orientation: "top",
    }).datepicker("setDate",'now'); 
    // carabayar();
    cari_poliklinik($('#date').val());
    cari_kelas();
})



function startLoading(){ $("body").addClass("loading"); }
function stopLoading(){ $("body").removeClass("loading"); }


function cari_kelas() // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{   
    var dt='';
    $.ajax({
        url:base_url+"canjunganpasien/get_kelaslayanan",
        dataType:"JSON",
        type:"POST",
        success:function(result){
            for(x in result)
            {
                dt+='<option value="'+result[x].idkelas+'">'+result[x].kelas+'</option>';
            }
            $('#kelas').empty();
            $('#kelas').html(dt);
        },
        error:function()
        {
            $.alert('Error while get or update data, Please contact developer..!');
            return false;
        }
    });
}
function carabayar() // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{   
    var dt='';
    $.ajax({
        url:base_url+"canjunganpasien/get_carabayar",
        dataType:"JSON",
        type:"POST",
        success:function(result){
            for(x in result)
            {
                dt+='<option value="'+result[x]+'">'+result[x]+'</option>';
            }
            $('#carabayar').empty();
            $('#carabayar').html(dt);
        },
        error:function()
        {
            $.alert('Error while get or update data, Please contact developer..!');
            return false;
        }
    });
}

///---print to pdf
function fungsi_cetaktopdf(content,style=''){
    w = window.open();
    w.document.write(style);
    w.document.write(content);
    w.document.write('<script type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</script>');
    w.document.close();
    w.focus(); 
}
function register_cekrm()
{
    //Save data to sessionStorage
    sessionStorage.setItem('key', '');
    // Get saved data from sessionStorage
    var sess = sessionStorage.getItem('key');
    // Remove saved data from sessionStorage
    // sessionStorage.removeItem('key')
    var norm=$('#norm').val();
    if(norm=='')
    {
        $('#norm').focus();
        $.alert('No.RM harus  di isi..!');
    }
    else if(sess =='')
    {
        $.alert('Silakan login dulu..!');
    }
    else
    {
        $.ajax({
            url:base_url+"canjunganpasien/register_ceknorm",
            data:{id:norm},
            dataType:"JSON",
            type:"POST",
            success:function(result){
                if(result.status=='danger'){$('#norm').focus();$.alert(result.isi);}
                if(result.isi==null){$('#norm').focus();$.alert('No.RM yang anda masukan salah...!');}
                else{$('#pp').val(result.isi['idperson']);$('#nama').val(result.isi['namalengkap']);$('#kelamin').val(result.isi['jeniskelamin']);$('#lahir').val(result.isi['tanggallahir']);$('#alamat').val(result.isi['alamat']);}
            },
            error:function()
            {
                $.alert('Error while get or update data, Please contact developer..!');
                return false;
            }
        });
    }
}

function register_cekrmanjungan()
{
    startLoading();
    var norm=$('#norm').val();
    if(norm=='')
    {
        stopLoading();
        $('#norm').focus();
        $.alert('No.RM harus  di isi..!');
    }
    else
    {
        $.ajax({
            url:base_url+"canjunganpasien/register_ceknorm",
            data:{id:norm},
            dataType:"JSON",
            type:"POST",
            success:function(result){
                stopLoading();
                if(result.status=='danger'){$('#norm').focus();$.alert(result.isi);}
                if(result.isi==null){$('#norm').focus();$.alert('No.RM yang anda masukan salah...!');}
                else{$('#pp').val(result.isi['idperson']);$('#nama').val(result.isi['namalengkap']);$('#kelamin').val(result.isi['jeniskelamin']);$('#lahir').val(result.isi['tanggallahir']);$('#alamat').val(result.isi['alamat']);}
            },
            error:function()
            {
                stopLoading();
                $.alert('Error while get or update data, Please contact developer..!');
                return false;
            }
        });
    }
}
function if_empty(nilai){var x = '';if(nilai > 0 || nilai !=null){return x = nilai;}else{return x = '';}}
function ambiltanggal(value)//fungsi tampil tanggal, format value: dd/mm/yyyy
{   
    v = value.split("/");
    tanggal = v[2]+'-'+v[1]+'-'+v[0];
    return tanggal;
}
function cari_poliklinik(value) //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    var dt='';
    $.ajax({
        type: "POST",
        url: base_url+'canjunganpasien/cari_jadwalpoli',
        data:{date: ambiltanggal(value)},
        dataType: "JSON",
        success: function(data) {
             for(i in data)
        {
            dt = dt + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" >' + data[i].namaunit +' '+ if_empty(data[i].titeldepan) +' '+ if_empty(data[i].namalengkap) +' '+ if_empty(data[i].titelbelakang) +' ' +if_empty(data[i].tanggal)+' </option>';
        }
        $('#poli').empty();
        $('#poli').html('<option value="0">Pilih</option>'+ dt );
        },
        error: function(result) {
            $.alert('Error while get or update data, Please contact developer..!');
            return false;
        }
    });
}
function batal_registrasi()
{
    $.confirm({
        title: 'Info.!',
        content: 'Batalkan pendaftaran periksa..?',
        buttons: {
            Batalkan: function () {
                location.reload();
            },
            Kembali: function () {
            }
        }
    });
}
function lanjut_registrasi()
{
    if($('#norm').val()=='' || $('#nama').val()=='')
    {
        $.alert('Mohon masukan No.RM pasien dengan benar..!');
    }
    else if($('#poli').val()=='0')
    {
        $.alert('Mohon pilih poli pemeriksaan..!');
    }
    else if($('#carabayar').val()=='0')
    {
        $.alert('Cara bayar belum dipilih..!');
    }
    else
    {
        createAntrian(39,5);
    }
}

function createAntrian(x,y)
{
  startLoading();
  $.ajax({ 
    url: base_url +'canjunganpasien/registrasi_savedata',
    type : "post",      
    dataType : "json",
    data : {caradaftar:$('#caradaftar').val(),pp:$('#pp').val(),jadwal:$('#poli').val(), rm:$('#norm').val(), tanggal:ambiltanggal($('#date').val()), kelas:$('#kelas').val(), carabayar:$('#carabayar').val()},
    success: function(result) {
        stopLoading();
        if(result.status=='danger')
        {
            $.alert(result.message);
        }
        else
        {
            var cetak = '<div style="width:6cm;float: none;"><img style="opacity:0.5;filter:alpha(opacity=70);width:6cm" src="'+base_url+'/assets/images/background.svg" />\n\
                      <table border="0" style="width:6cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">'+
                          '<tr><td><b>Antrian  Periksa</b></td></tr>'+
                          '<tr><td><span style="padding-left:25px;font-size:75px;margin:0; text-align:center;"">'+ result.noantrian +'</span></td></tr>'+
                          '<tr><td><span>'+ result.dokter['namadokter'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.namaloket +'</span></td></tr>'+
                          '<tr><td><span>'+ result.datapasien['norm'] +'</span></td></tr>'+
                          '<tr><td><span>'+ result.datapasien['namalengkap'] +'</span></td></tr>'+
                          '<tr><td><div>'+ result.datapasien['alamat'] +'</td></tr>';
            // jika loket daftar tidak kosong maka tambahkan cetak antrian loket daftar 
            if(result.antridaftar!=='pasienmandiri')
            {
                cetak+= '<tr><td></div>'+
                        '<tr><td>==============================</td></tr>'+
                        '<tr><td><b>'+result.loketdaftar+'</b></td></tr>'+
                        '<tr><td style="font-size:20px">Antrian: <b>'+result.antridaftar+'</b></td></tr>'+
                        '<tr><td>Waktu Pendaftaran : '+ $('#waktudaftar').val()+'</td></tr>';
            }
                    fungsi_cetaktopdf(cetak,'<style>@page {size:7.6cm 100%;margin:0.2cm;}</style>');
        } 
    },
    error: function(result){   
        stopLoading();
        $.alert('Error while get or update data, Please contact developer..!');
        return false;
    }
});
}
