$(function (){
    getpenanggung();
});

/**
 * Cetak Nota Penjualan Bebas
 * idbp = idbarangpembelibebas
 */
function cetaknota(idbp)
{
    startLoading();
    var font = '', fontx = '', paper = '', margin = '', lebar = '', lebargambar='',total=0;
    var listkasir='',tipesebelum='',subtotal=0,total=0,no=0, selisih = 0, jenispemeriksaan='rajal';
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_notapenjualanbebas',
        data: {i:idbp},
        dataType: "JSON",
        success: function(result) {
            lebar = (jenispemeriksaan!='rajal') ? 'width:19cm;' : 'width:6cm;';
            lebargambar = (jenispemeriksaan!='rajal') ? '' : lebar;
            font = lebar + ((jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:small;');
            fontx = (jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:x-small;';
            paper = (jenispemeriksaan!='rajal') ? '20' : '7.6';
            margin = (jenispemeriksaan!='rajal') ? '2' : '0.2';
            listkasir += '<div style="' + lebar + 'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' + lebargambar + '" src="'+base_url+'assets/images/headerkuitansi' + ((jenispemeriksaan!='rajal') ? '' : 'small') + '.jpg" />\n\
                          <table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            // INFO TAGIHAN
            listkasir += '<tr><td>Nama</td><td>: '+ result['infotagihan'][0].namacetak +'</td></tr><tr><td>Keterangan</td><td>: '+ result['infotagihan'][0].keterangan +'</td></tr></table>';

            // list item
            listkasir += '<table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            listkasir += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>';
            for(x in result['detailtagihan']){ total += parseInt(result['detailtagihan'][x].jumlah) * result['detailtagihan'][x].harga; listkasir+='<tr><td>'+result['detailtagihan'][x].nama+'</td><td align="right">'+parseInt(result['detailtagihan'][x].jumlah)+'</td><td align="right">'+convertToRupiah(result['detailtagihan'][x].harga)+'</td></tr>';}

            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiah(total)+'</td></tr>';
            listkasir += '</table><table border="0" style="' + font + 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            
            // DETAIL TAGIHAN
            for(var u in result['infotagihan'])
            {
                listkasir += '';
                selisih   += (parseInt( result['infotagihan'][u].nominal , 10) + parseInt( result['infotagihan'][u].pembulatan , 10)) - ( parseInt(result['infotagihan'][u].dibayar, 10) + parseInt(result['infotagihan'][u].potongan,10));
                kembali   = (selisih < 0) ? selisih * -1 : 0 ;
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][u].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][u].waktu+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][u].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].nominal)+'</td></tr>';
                listkasir += (result['infotagihan'][u].potongan>0) ? '<tr><td>Potongan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].potongan)+'</td></tr>' : '';
                listkasir += (result['infotagihan'][u].pembulatan > 0) ? '<tr><td>Pembulatan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].pembulatan)+'</td></tr>' : '';
                listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+ convertToRupiah(result['infotagihan'][u].totaltagihan)+'</td></tr>';
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][u].dibayar)+'</td></tr>';
                listkasir += (result['infotagihan'][u].kekurangan>0) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][u].kekurangan)+'</td></tr>' : '';
                listkasir += (kembali>0) ? '<tr><td>Kembali</td><td align="right">'+convertToRupiah(kembali)+'</td></tr>' : '';
                listkasir += '<tr><td>Jenistagihan</td><td align="right">'+result['infotagihan'][u].jenistagihan+'</td></tr>';
                listkasir += '<tr><td>Carabayar</td><td align="right">'+result['infotagihan'][u].carabayar+'</td></tr>';
            }
            
            listkasir += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            listkasir += '</table></div>';
            stopLoading();
            fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

function hitungkembalian(event)
{
    var txttagihan = unconvertToRupiah($('input[name="txttagihan"]').val());
    var txttunai = unconvertToRupiah($('input[name="txttunai"]').val());
    var txttransfer = unconvertToRupiah($('input[name="txttransfer"]').val());
    var txtpotonggaji = unconvertToRupiah($('input[name="txtpotonggaji"]').val());
    
    var txttotaldibayar = parseInt(txttunai) + parseInt(txttransfer) + parseInt(txtpotonggaji);
    var txtkembali = parseInt(txttotaldibayar) - parseInt(txttagihan);
//    var txtsisa = parseInt(txttagihan) - parseInt(txttotaldibayar);    
    var txtkekurangan = parseInt(txttagihan) - parseInt(txttotaldibayar);
    
    $('input[name="txttunai"]').val( txttunai );
    $('input[name="txttransfer"]').val( txttransfer );
    $('input[name="txtpotonggaji"]').val( txtpotonggaji );
    $('input[name="txtkembali"]').val( txtkembali );
//    $('input[name="txtsisatagihan"]').val( convertToRupiah(txtsisa) );
    $('input[name="txtkekurangan"]').val( ((txtkekurangan < 0) ? 0 : txtkekurangan ) );
    
    
    if ($('input[name="txtkembali"]').val() < 0) $('input[name="txtkembali"]').val(0);
//    if ($('input[name="txtsisatagihan"]').val() < 0) $('input[name="txtsisatagihan"]').val(0);
    
    return;
}

//form pembayaran 
function bayar(idbp,subtotal,idtagihan='')
{
    var modalSize = 'lg';
    var modalTitle = 'Pelunasan Biaya Pembelian Bebas';
    var modalContent = '<form action="" id="formBayar">' +
                            '<div class="form-group col-md-12 bg bg-info" style="padding:10px;border-radius:4px;">' +
                                '<div class="pull-left" style="font-size:16px;padding-top:0px;margin-top:0px;"><label>TOTAL TAGIHAN : </label></div>'+
                                '<div class="pull-right"><label id="labeltotaltagihan" class="" style="font-size:30px;margin-left:8px;">Rp '+convertToRupiah(subtotal)+'</label></div>'+
                                '<input type="hidden" name="altt" value="'+idbp+'" />' +
                                
                            '</div>' +
                            '<div class="form-group">' +
                                '<div class="col-md-6">'+
                                    '<label>TAGIHAN</label>' +
                                    '<input style="margin-bottom:8.5px;" type="text" name="txttagihan" value="'+convertToRupiah(subtotal)+'" id="txttagihan" class="form-control" onkeyup="hitungkembalian(event)" disabled />' +
                                    
                                    '<label>TUNAI</label>' +
                                    '<input style="margin-bottom:8.5px;" type="text" name="txttunai" value="0" placeholder="nominal tunai" autocomplete="off" id="txttunai" class="form-control" onkeyup="hitungkembalian(event)" />' +
                                    
                                '</div>'+
                                '<div class="col-md-6">' +
                                    '<label>TRANSFER</label>'+
                                    '<input type="text" name="txttransfer" placeholder="nominal transfer" id="txttransfer" autocomplete="off" class="form-control" value="" onkeyup="hitungkembalian(event)" />' +
                                    '<div">'+
                                    '<select id="selbanktujuan" onchange="hitungkembalian(event)" name="selbanktujuan" class="select2" style="width:100%;"><option>Bank Tujuan</option></select></div>'+
                                '</div>'+
                                
                            '</div>'+
                            
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group" style="margin-top:9px;">' +
                                '<div class="col-md-6">' +
                                    '<label>POTONG GAJI</label>' +
                                    '<input type="text" name="txtpotonggaji" placeholder="potonggaji" class="form-control" id="txtpotonggaji" autocomplete="off" onkeyup="hitungkembalian(event)"/>' +
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<label>Penanggung</label>\n\
                                    <input list="listpenanggung" id="penanggung" name="penanggung" type="text" class="form-control" placeholder="penanggung"/>\n\
                                    <datalist id="listpenanggung">'+
                                    '</datalist>'+
                                '</div>'+
                            '</div>' +
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group" style="margin-top:9px;">' +
                                '<div class="col-md-6">' +
                                    '<label>KEKURANGAN</label>' +
                                    '<input type="text" name="txtkekurangan" placeholder="kekurangan" class="form-control" disabled/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group" style="margin-top:9px;">' +
                                '<div class="col-md-6">' +
                                    '<label>KEMBALI</label>' +
                                    '<input type="text" name="txtkembali"  placeholder="kembali" class="form-control" disabled/>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#txttunai').val()=='' && $('#txttransfer').val()=='')
                    {
                        $('#txttunai').focus();
                        alert_empty('inputan tunai atau transfer');
                        return false;
                    }
                    else if( $('#selbanktujuan').val()!= 0  && $('#txttransfer').val()=='0')
                    {
                        alert_empty('inputan transfer');
                        return false;
                    }
                    else if($('#selbanktujuan').val()== 0  && $('#txttransfer').val()!='0')
                    {
                        alert_empty('bank');
                        return false;
                    }
                    else
                    {
                        simpanpembayaran(idbp,idtagihan);  
                    }
                    
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        dropdown_getdata($('#selbanktujuan'),'cmasterdata/fillkeuakunbank','','<option value="0">Bank Tujuan</option>');
        listpenanggung();
        $('.select2').select2();
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function getpenanggung()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/fillpenanggungkasir',type:"POST",dataType:"JSON",
        success: function(result){
            dtpenanggung = result
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    }); 
}
function listpenanggung()
{
    var data = dtpenanggung;
    var list = '';
    for(var x in data)
    {
        list += '<option value="'+data[x].text+'">';
    }
   $('#listpenanggung').empty();
   $('#listpenanggung').html(list);
}
