$(function () 
{ 
    // jika halaman add user maka inisialisasi select 2 dll...
    switch($('#jsmode').val()) {
    case 'masteruser':
      panggilMasteruser();
      break;
    case 'masteraksesuser':
      panggilMasteraksesuser();
      break;
    case 'masterakseshalaman':
      panggilMasterakseshalaman();
      break;
    case 'akseshalaman':
      panggilMasterakseshalaman();
      break;
    default:
      // code block
      // loadFungsiDashboard();
  }
})
// master user
function panggilMasteruser()
{
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass   : 'iradio_flat-green'
        });
}
// master akses user
function panggilMasteraksesuser()
{
  //Initialize Select2 Elements
  $('.select2').select2();
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  });
}
// master akses halaman
function panggilMasterakseshalaman()
{
  //Initialize Select2 Elements
  // $('.select2').select2();
  //Flat red color scheme for iCheck
  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
  });
}

function simpan_user()
{
  if($('#username').val()==='')
  {
    alert_empty('username');
  }
  else if($('#password').val()==='')
  {
    alert_empty('password');
  }
  else
  {
    $('#FormUser').submit();
  }
}

function simpan_akses_user()
{
  if($('#username').val()==='')
  {
    alert_empty('username');
  }
  else if($('#password').val()==='')
  {
    alert_empty('password');
  }
  else
  {
    $('#FormUser').submit();
  }
}

function simpan_peran()
{
  if($('#namaperan').val()==='')
  {
    alert_empty('peran');
  }
  else
  {
    $('#FormPeran').submit();
  }
}

function simpan_halaman()
{
  if($('#namahalaman').val()==='')
  {
    alert_empty('halaman');
  }
  else
  {
    $('#FormHalaman').submit();
  }
}

function simpan_hakakseshalaman()
{
  if($('#idhalaman').val()==='')
  {
    alert_empty('halaman');
  }
  else
  {
    $('#FormHakakseshalaman').submit();
  }
}

function simpan_hubungan()
{
  if($('#hubungan').val()==='')
  {
    alert_empty('hubungan');
  }
  else
  {
    $('#FormHubungan').submit();
  }
}
