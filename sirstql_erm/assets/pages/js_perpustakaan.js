var table;
var pagemode=$('#jsmode').val();
  switch(pagemode) 
  {
    case 'buku':
      startLoading();
      getbuku(null);
      break;
    case 'kategori':
      startLoading();
      getkategori();
      break;
    case 'penulis':
      startLoading();
      getpenulis();
      break;
    case 'penerbit':
      startLoading();
      getpenerbit();
      break;
    default:
        //run code
  }
//  FORM BUKU
function formbuku(title,editdata)
{
    startLoading();
    var mSize='lg'
    var mTitle= 'FORM '+title+' BUKU';
    var mContent='<form action="" id="formbuku">'
    +'<input type="hidden" name="idbuku" value="" />'
    +'<div class="form-group">'
            +'<label class="col-sm-2 control-label text-right"> Judul Buku <span class="asterisk">*</span></label>'
            +'<div class="col-sm-9"><input type="text" name="judulbuku" placeholder="judul buku" value="" class="form-control">'
    +'</div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right"> Ringkasan <span class="asterisk">*</span></label>'
            +'<div class="col-sm-9"><textarea placeholder="ringkasan buku" name="ringkasan" rows="4" class="form-control"></textarea>'
    +'</div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right"> ISBN <span class="asterisk">*</span></label>'
            +'<div class="col-sm-4"><input placeholder="isbn" type="text" name="isbn" value="" class="form-control">'
    +'</div></div>'
    +'<div class="form-group">'
            +'<label class="col-sm-2 control-label text-right" style="padding:0px;padding-top:2px;"> Jumlah Hlm</label>'
            +'<div class="col-sm-3"><input type="text" name="jumlahhalaman" placeholder="jumlah halaman buku" value="" class="form-control">'
    +'</div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right">Penerbit <span class="asterisk">*</span></label>'
            +'<div class="col-sm-4"><select name="penerbit" class="form-control select2"></select>'
    +'</div><div class="col-sm-1"><a onclick="formpenerbit(\'TAMBAH\',null)" '+tooltip('tambah penerbit')+' class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a></div></div>'
    +'<div class="form-group">'
            +'<label class="col-sm-1 control-label text-right" style="padding:0px;padding-top:2px;"> Tahun Terbit <span class="asterisk">*</span></label>'
            +'<div class="col-sm-2"><input type="text" name="tahunterbit" value="" placeholder="tahun terbit" class="form-control">'
    +'</div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right">Penulis <span class="asterisk">*</span></label>'
            +'<div class="col-sm-4"><select name="penulis" class="form-control select2"><option value="0">Pilih</option></select> '
    +'</div><div class="col-sm-1"><a onclick="formpenulis(\'TAMBAH\',null)" '+tooltip('tambah penulis')+' class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a></div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right">Kategori <span class="asterisk">*</span></label>'
            +'<div class="col-sm-4"><select name="kategori" class="form-control select2"><option value="0">Pilih</option></select>'
    +'</div><div class="col-sm-1"><a onclick="formkategori(\'TAMBAH\',null)"'+tooltip('tambah kategori')+' class="btn btn-primary btn-xs"><i class="fa fa-plus"></i></a></div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-2 control-label text-right">Jumlah Buku <span class="asterisk">*</span></label>'
            +'<div class="col-sm-2"><input name="jumlahbuku" type="text" placeholder="jumlah buku" class="form-control"/>'
    +'</div></div>'
    +'</form>';
$.confirm({
    title: mTitle,
    content: mContent,
    columnClass: mSize,
    autoRefresh:true,
    buttons: {
        formSubmit:{
                text: 'SAVE',
                btnClass: 'btn-primary',
                action: function (){
                    if($('input[name="judulbuku"]').val()==''){
                        alert_empty('judul buku');
                        return false;
                    }else if($('textarea[name="ringkasan"]').val()==''){
                        alert_empty('ringkasan buku');
                        return false;
                    }else if($('input[name="isbn"]').val()==''){
                        alert_empty('isbn');
                        return false;
                    }else if($('input[name="jumlahhalaman"]').val()==''){
                        alert_empty('jumlah halaman buku');
                        return false;
                    }else if($('select[name="penerbit"]').val()==0){
                        alert_empty('penerbit');
                        return false;
                    }else if($('input[name="tahunterbit"]').val()==''){
                        alert_empty('tahun terbit buku');
                        return false;
                    }else if($('select[name="penulis"]').val()==0){
                        alert_empty('penulis');
                        return false;
                    }else if($('select[name="kategori"]').val()==0){
                        alert_empty('kategori');
                        return false;
                    }else if($('input[name="jumlahbuku"]').val()==''){
                        alert_empty('jumlah buku');
                        return false;
                    }else{
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cmasterdata/savebuku',
                            data: $("#formbuku").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            //jika  berhasil
                            success: function(result) {
                                stopLoading();
                                notif(result.status, result.message);
                                getbuku(null);
                            },
                            //jika error
                            error: function(result) {
                               stopLoading();
                               fungsiPesanGagal();
                            }
                        });
                    }
                }
        },
        formReset:{
            text: 'BATAL',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    stopLoading();
    if(editdata!=null){
        getbuku(editdata);
    }else{
        getpenulis(null,'list');
        getkategori(null,'list');
        getpenerbit(null,'list');
    }
    $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
    $('.select2').select2();
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it 
    });
    }
});
}

function hapus(id,url){
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/'+url,
                    type : "post",      
                    dataType : "json",
                    data : { id:id },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            if(url=='hapusbuku'){
                                getbuku(null);
                            }else if(url=='hapuskategori'){
                                getkategori(null);
                            }else if(url=='hapuspenulis'){
                                getpenulis(null);
                            }else if(url=='hapuspenerbit'){
                                getpenerbit(null);
                            }
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){                    
                        notif(result.status, result.message);
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
//form penerbit
function formpenerbit(title,editdata)
{
    startLoading();
    var mTitle= 'FORM '+title+' PENERBIT';
    var mContent='<form action="" id="formpenerbit">'
    +'<input type="hidden" value="" name="idpenerbit" />'
    +'<div class="form-group">'
            +'<label class="col-sm-12 control-label"> Penerbit</label>'
            +'<div class="col-sm-12"><input type="text" name="penerbit" placeholder="nama penerbit" value="" class="form-control">'
    +'</div></div>'
    +'<div class="form-group">'
            +'<label class="col-sm-12 control-label"> Kota</label>'
            +'<div class="col-sm-12"><input type="text" name="kota" value="" placeholder="kota penerbit" class="form-control">'
    +'</div></div>'
    +'<div class="col-xs-12"></div><div class="form-group">'
            +'<label class="col-sm-12 control-label"> Alamat</label>'
            +'<div class="col-sm-12"><textarea name="alamat" class="form-control" placeholder="alamat penerbit"></textarea>'
    +'</div></div>'
    +'</form>';
    $.confirm({
        title: mTitle,
        content: mContent,
        columnClass: 'small',
        autoRefresh:true,
        buttons: {
            formSubmit:{
                text: 'SAVE',
                btnClass: 'btn-primary',
                action: function (){
                    if($('input[name="penerbit"]').val()==''){
                        alert_empty('penerbit');
                        return false;
                    }else if($('input[name="kota"]').val()==''){
                        alert_empty('kota penerbit');
                        return false;
                    }else if($('textarea[name="alamat"]').val()==''){
                        alert_empty('alamat penerbit');
                        return false;
                    }else{
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cmasterdata/savepenerbit',
                            data: $("#formpenerbit").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            //jika  berhasil
                            success: function(result) {
                                stopLoading();
                                notif(result.status, result.message);
                                getpenerbit(null);
                            },
                            //jika error
                            error: function(result) {
                               stopLoading();
                               fungsiPesanGagal();
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'BATAL',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        stopLoading();
        // bind to events
        if(editdata!=null){getpenerbit(editdata,'edit');}
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it 
        });
        }
    });
}
//FORM KATEGORI
function formkategori(title,editdata)
{
    startLoading();
    var mTitle= 'FORM '+title+' KATEGORI';
    var mContent='<form action="" id="formkategori">'
    +'<input type="hidden" value="" name="idkategori" />'
    +'<div class="form-group">'
            +'<label class="col-sm-3 control-label text-right"> Kategori</label>'
            +'<div class="col-sm-9"><input type="text" name="kategori" placeholder="kategori buku" value="" class="form-control">'
    +'</div></div>'
    +'</form>';
    $.confirm({
        title: mTitle,
        content: mContent,
        columnClass: 'small',
        autoRefresh:true,
        buttons: {
            formSubmit:{
                text: 'SAVE',
                btnClass: 'btn-primary',
                action: function (){
                    if($('input[name="kategori"]').val()==''){
                        alert_empty('kategori');
                        return false;
                    }else{
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cmasterdata/savekategori',
                            data: $("#formkategori").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            //jika  berhasil
                            success: function(result) {
                                stopLoading();
                                notif(result.status, result.message);
                                getkategori(null);
                            },
                            //jika error
                            error: function(result) {
                               stopLoading();
                               fungsiPesanGagal();
                            }
                        });
                    }   
                }
            },
            formReset:{
                text: 'BATAL',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        stopLoading();
        if(editdata!=null){getkategori(editdata,'edit');}
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it 
        });
        }
    });
}
//FORM PENULIS
function formpenulis(title,editdata)
{
    startLoading();
    var mTitle= 'FORM '+title+' PENULIS';
    var mContent='<form action="" id="formpenulis">'
    +'<input type="hidden" value="" name="idpenulis" />'
    +'<div class="form-group">'
            +'<label class="col-sm-12 control-label"> Penulis</label>'
            +'<div class="col-sm-12"><input type="text" name="penulis" placeholder="penulis" value="" class="form-control">'
    +'</div></div>'
    +'</form>';
$.confirm({
    title: mTitle,
    content: mContent,
    columnClass: 'small',
    autoRefresh:true,
    buttons: {
        formSubmit:{
                text: 'SAVE',
                btnClass: 'btn-primary',
                action: function (){
                    if($('input[name="penulis"]').val()==''){
                        alert_empty('penulis');
                        return false;
                    }else{
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cmasterdata/savepenulis',
                            data: $("#formpenulis").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            //jika  berhasil
                            success: function(result) {
                                stopLoading();
                                notif(result.status, result.message);
                                getpenulis(null);
                            },
                            //jika error
                            error: function(result) {
                               stopLoading();
                               fungsiPesanGagal();
                            }
                        });
                    }
                }
            },
        formReset:{
            text: 'BATAL',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    stopLoading();
    if(editdata!=null){getpenulis(editdata,'edit');}
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it 
    });
    }
});
}
//list buku
function buku(data)
{
    var header = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
              +'<thead>'
              +'<tr>'
                +'<th style="width:60px;">No</th>'
                +'<th>Judul</th>'
                +'<th>Kode</th>'
                +'<th>Jumlah</th>'
                +'<th>ISBN</th>'
                +'<th>Penulis</th>'
                +'<th>Penerbit</th>'
                +'<th>Kategori</th>'
                +'<th></th>'
              +'</tr>'
              +'</thead>'
              +'<tbody>';
      
      var isitabel='', no=0;
          for(i in data)
          {
              isitabel+='<tr><td>'+ ++no +'</td><td>'+data[i].judulbuku+'</td><td>'+data[i].kodebuku+'</td><td>'+data[i].jumlahbuku+'</td><td>'+data[i].isbn+'</td><td>'+data[i].penulis+'</td><td>'+data[i].penerbit+'</td><td>'+data[i].kategori+'</td><td><a onclick="formbuku(\'UBAH\','+data[i].idbuku+')" class="btn btn-xs btn-warning" '+tooltip('ubah')+'><i class="fa fa-edit"></i></a> <a onclick="hapus('+data[i].idbuku+',\'hapusbuku\')" class="btn btn-xs btn-danger" '+tooltip('hapus')+'><i class="fa fa-trash"></i></a></td></tr>';
          }
          
          isitabel+='</tfoot></table>';
    $('#listbuku').empty();
    $('#listbuku').html(header+isitabel);
    table = $('.table').DataTable({"dom": '<"toolbar">frtip',"processing": true,"stateSave": true,});
    $("div.toolbar").html(' <a class="btn btn-info btn-sm" onclick="getbuku(null)"><i class="fa fa-desktop"></i> Tampil</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a onclick="formbuku(\'TAMBAH\',null)" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Buku</a> <a onclick="unduhbuku()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>');
    $('[data-toggle="tooltip"]').tooltip();
}
//ambil data buku
function getbuku(id)
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cmasterdata/getbuku',
        data: {id:id}, //
        dataType: "JSON", //tipe data yang dikirim
        //jika  berhasil
        success: function(result) {
            stopLoading();
//            notif(result.status, result.message);
            if(id==null){
                buku(result);
            }else{
                $('input[name="idbuku"]').val(result[0].idbuku);
                $('input[name="judulbuku"]').val(result[0].judulbuku);
                $('input[name="jumlahbuku"]').val(result[0].jumlahbuku);
                $('input[name="isbn"]').val(result[0].isbn);
                $('input[name="tahunterbit"]').val(result[0].tahunterbit);
                $('input[name="jumlahhalaman"]').val(result[0].jumlahhalaman);
                $('textarea[name="ringkasan"]').val(result[0].ringkasan);
                getpenerbit(result[0].idpenerbit,'list');
                getpenulis(result[0].idpenulis,'list');
                getkategori(result[0].idkategori,'list');
                
            }
            
        },
        //jika error
        error: function(result) {
           stopLoading();
           fungsiPesanGagal();
        }
    });
}
function unduhbuku()
{
    window.location.href=base_url+"creport/downloadexcel_buku";
}
//list KATEGORI
function kategori(data)
{  
 var header = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
              +'<thead>'
              +'<tr>'
                +'<th style="width:60px;">No</th>'
                +'<th>Kode</th>'
                +'<th>Kategori</th>'
                +'<th></th>'
              +'</tr>'
              +'</thead>'
              +'<tbody>';
    
    var isitabel='',no=0;
    for(x in data)
    {
        isitabel+='<tr><td>'+ ++no +'</td><td>'+data[x].kodekategori+'</td><td>'+data[x].kategori+'</td><td><a onclick="formkategori(\'UBAH\','+data[x].idkategori+')" class="btn btn-xs btn-warning" '+tooltip('ubah')+'><i class="fa fa-edit"></i></a> <a onclick="hapus('+data[x].idkategori+',\'hapuskategori\')" class="btn btn-xs btn-danger" '+tooltip('hapus')+'><i class="fa fa-trash"></i></a></td></tr>';
    }
    isitabel+='</tfoot></table>';
    $('#listkategori').empty();
    $('#listkategori').html(header+isitabel);
    table = $('.table').DataTable({"dom": '<"toolbar">frtip',"processing": true,"stateSave": true,});
    $("div.toolbar").html(' <a class="btn btn-info btn-sm" onclick="getkategori(null)"><i class="fa fa-desktop"></i> Tampil</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a onclick="formkategori(\'TAMBAH\',null)" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Kategori</a>');
}
//list penulis
function penulis(data)
{
    var header = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
              +'<thead>'
              +'<tr>'
                +'<th style="width:60px;">No</th>'
                +'<th>Penulis</th>'
                +'<th></th>'
              +'</tr>'
              +'</thead>'
              +'<tbody>';
      
    var isitabel='',no=0;
    for(x in data)
    {
        isitabel+='<tr><td>'+ ++no +'</td><td>'+data[x].penulis+'</td><td><a onclick="formpenulis(\'UBAH\','+data[x].idpenulis+')" class="btn btn-xs btn-warning" '+tooltip('ubah')+'><i class="fa fa-edit"></i></a> <a onclick="hapus('+data[x].idpenulis+',\'hapuspenulis\')" class="btn btn-xs btn-danger" '+tooltip('hapus')+'><i class="fa fa-trash"></i></a></td></tr>';
    }
          isitabel+='</tfoot></table>';
    $('#listpenulis').empty();
    $('#listpenulis').html(header+isitabel);
    table = $('.table').DataTable({"dom": '<"toolbar">frtip',"processing": true,"stateSave": true,});
    $("div.toolbar").html(' <a class="btn btn-info btn-sm" onclick="getpenulis(null)"><i class="fa fa-desktop"></i> Tampil</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a onclick="formpenulis(\'TAMBAH\',null)" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Penulis</a>');
}
//list penerbit
function penerbit(data)
{
    var header = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%"><thead><tr><th style="width:60px;">No</th><th>Penerbit</th><th></th></tr></thead><tbody>';
    var isitabel='',no=0;
    for(x in data)
    {
        isitabel+='<tr><td>'+ ++no +'</td><td>'+data[x].penerbit+', '+data[x].kotapenerbit+', '+data[x].alamatpenerbit+'</td><td><a onclick="formpenerbit(\'UBAH\','+data[x].idpenerbit+')" class="btn btn-xs btn-warning" '+tooltip('ubah')+'><i class="fa fa-edit"></i></a> <a onclick="hapus('+data[x].idpenerbit+',\'hapuspenerbit\')" class="btn btn-xs btn-danger" '+tooltip('hapus')+'><i class="fa fa-trash"></i></a></td></tr>';
    }
    $('#listpenerbit').empty();
    $('#listpenerbit').html(header+isitabel);
    table = $('.table').DataTable({"dom": '<"toolbar">frtip',"processing": true,"stateSave": true,});
    $("div.toolbar").html(' <a class="btn btn-info btn-sm" onclick="getpenerbit(null)"><i class="fa fa-desktop"></i> Tampil</a> <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a onclick="formpenerbit(\'TAMBAH\',null)" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Penerbit</a>');
}

function refresh_page()
{
     table.state.clear();
    window.location.reload(true);
}
  
//ambildata penulis
function getpenulis(id,mode=null)
{
    $.ajax({
        url: base_url+"cmasterdata/getpenulis",
        dataType:"JSON",
        data:{id:((mode=='list')?'':id)},
        type:"POST",
        success:function(result){
          stopLoading(); 
          if(mode==null){
            penulis(result);
          }else if(mode=='edit'){
             $('input[name="idpenulis"]').val(result[0].idpenulis);
             $('input[name="penulis"]').val(result[0].penulis);
          }else{
            var html='<option value="0">Pilih</option>';
            for (x in result)
            {
                html+='<option '+((id!=null && result[x].idpenulis==id)?' selected ':'')+' value="'+result[x].idpenulis+'">'+result[x].penulis+'</option>';
            }
            $('select[name="penulis"]').empty();
            $('select[name="penulis"]').html(html);
            $('.select2').select2();
          }
          
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
        }
    });
}

//ambildata penerbit
function getpenerbit(id,mode=null)
{
    $.ajax({
        url: base_url+"cmasterdata/getpenerbit",
        dataType:"JSON",
        data:{id: ((mode=='list')?'':id)},
        type:"POST",
        success:function(result){
          stopLoading(); 
          if(mode==null){
            penerbit(result);
          }else if(mode=='edit'){
             $('input[name="idpenerbit"]').val(result[0].idpenerbit);
             $('input[name="penerbit"]').val(result[0].penerbit);
             $('input[name="kota"]').val(result[0].kotapenerbit);
             $('textarea[name="alamat"]').val(result[0].alamatpenerbit);
          }else{
            var html='<option value="0">Pilih</option>';
            for (x in result)
            {
                html+='<option '+((id!=null && result[x].idpenerbit==id)?' selected ':'')+' value="'+result[x].idpenerbit+'">'+result[x].penerbit+' - '+result[x].kotapenerbit+'</option>';
            }
            $('select[name="penerbit"]').empty();
            $('select[name="penerbit"]').html(html);
            $('.select2').select2();
          }
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
        }
    });
}

//ambildata kategori
function getkategori(id,mode=null)
{
    startLoading();
    $.ajax({
        url: base_url+"cmasterdata/getkategori",
        dataType:"JSON",
        data:{id:((mode=='list')?'':id)},
        type:"POST",
        success:function(result){
          stopLoading(); 
          if(mode==null){
            kategori(result);
          }else if(mode=='edit'){
             $('input[name="idkategori"]').val(result[0].idkategori);
             $('input[name="kategori"]').val(result[0].kategori);
          }else{
            var html='<option value="0">Pilih</option>';
            for (x in result)
            {
                html+='<option '+((id!=null && result[x].idkategori==id)?' selected ':'')+' value="'+result[x].idkategori+'">'+result[x].kategori+'</option>';
            }
            $('select[name="kategori"]').empty();
            $('select[name="kategori"]').html(html);
            $('.select2').select2();
          }
          
        },
        error:function(result){
          stopLoading();
          fungsiPesanGagal();
        }
    });
}

