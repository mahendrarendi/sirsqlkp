$(document).on('click','#loadMoreRiwayat',function(){
    loadMoreRiwayat();
});

function loadMoreRiwayat()
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cadmission/riwayat_allpemeriksaan_pasien', //alamat controller yang dituju (di js base url otomatis)
        data: {
            norm: $('input[name="norm"]').val(),
            tanggalperiksa:$('#riwayattanggalpemeriksaan').val()
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            if(result.status == 'info')
            {
                $.alert(result.message);
            }
            else
            {
                tampilkan_riwayat_operasi(result);
                tampil_riwayat_ranap(result);
                tampil_riwayat_ralan(result);
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    }); 
}

function tampilkan_riwayat_operasi(result)
{
    if(result.operasi != 'datakosong')
    {
        var row = '<li class="time-label">'
                +'<span class="bg-red">'
                  +'Laporan Operasi, <a class="fa fa-calendar" style="color:#fff;"></a> '+ result.operasi.tanggalmulai
                +'</span>'
          +'</li>';
  
        row += '<li>'
          +'<i class="fa fa-user-md bg-blue"></i>'
          +'<div class="timeline-item">';
        row +='<h3 class="timeline-header"><a href="#">'+result.operasi.jenistindakan+' </a>  <span style="font-size:14px;"><a class="fa fa-clock-o"></a> '+result.operasi.jammulai +' - '+ result.operasi.jamselesai +' </span></h3>';
        row += '';
        row += '<div class="timeline-body">'
        row += detail_operasi(result.operasi.dokteroperator,'Dokter Operator')
             + detail_operasi(result.operasi.asistenbedah,'Asisten Bedah')
             + detail_operasi(result.operasi.instrumen,'Perawat Instrument')
             + detail_operasi(result.operasi.resikotinggi,'Resiko Tinggi')
             + detail_operasi(result.operasi.diagnosapraoperasi,'Diagnosis Pra Operasi')
             + detail_operasi(result.operasi.diagnosapascaoperasi,'Diagnosis Post Operasi')
             + detail_operasi(result.operasi.tindakanoperasi,'Tindakan Operasi')
             + detail_operasi(result.operasi_jenisoperasi.jenisoperasi,'Jenis Operasi')
             + detail_operasi(result.operasi.macamoperasi,'Macam Operasi')
             + detail_operasi(result.operasi_jenisanestesi.jenisanestesi,'Jenis Anestesi')
             + detail_operasi(result.operasi.jumlahperdarahan,'Jumlah Perdarahan')
             + detail_operasi(result.operasi.patologianatomi,'Patologi Anatomi')
             + detail_operasi(result.operasi.asaljaringan,'Asal Jaringan')
             + detail_operasi(result.operasi.sitologi,'Sitologi')
             + detail_operasi(result.operasi.pemasanganimplant,'Pemasangan Implant')
             + detail_operasi(result.operasi.uraianpembedahan,'Uraian Pembedahan (Prosedure operasi yang dilakukan dan rincian temuan, ada dan tidak adanya komplikasi)')
            +'</div>';

        row+='</div></li>';
        $('#riwayatAkhir').before(row);
    }
}

function detail_operasi(result,judul)
{
    var row = '<div class="form-group">'
        +'<label>'+judul+'</label>'
        +'<div class="riwayatparagraf">'+result+'</div>'
      +'</div>';
      return row;
}

function riwayat_detail(result,judul)
{
    var row = '';
    for(var x in result)
    {
        row += result[x].hasil+'<br>';
    }
    var group ='<div class="form-group">'
        +'<label>'+judul+'</label>'
        +'<div class="riwayatparagraf">'+row+'</div>'
    +'</div>';
    return group;
}


function riwayat_catatan(data,judul)
{
    var row = '';
    for(var x in data)
    {
        row+= '<span>'+data[x].petugas+'<br> '+data[x].sip+'<br><small>'+data[x].profesi+'</small></span><span>'+data[x].soa_soa+'</span><span>'+data[x].soa_p+'</span>';
    }
    
    var group ='<div class="form-group">'
        +'<label>'+judul+'</label>'
        +'<div class="riwayatparagraf">'+row+'</div>'
    +'</div>';
    return group;
}

//tampil riwayat ranap
function tampil_riwayat_ranap(result)
{
    if(result.ranap_pemeriksaan != 'datakosong')
    {
        var row = '<li class="time-label">'
                +'<span class="bg-yellow">'
                  +'Rawat Inap, <a class="fa fa-calendar" style="color:#fff;"></a> '+ result.ranap_pemeriksaan.waktumasuk +' - '+ ((result.ranap_pemeriksaan.waktukeluar == '0000-00-00 00:00:00') ? 'Sekarang' : result.ranap_pemeriksaan.waktukeluar)
                +'</span>'
          +'</li>';
  
        row += '<li>'
          +'<i class="fa fa-user-md bg-blue"></i>'
          +'<div class="timeline-item">';
  
        var no=0;
        for(var x in result.ranap_rencana)
        {
            no++;
            var ranap_radiografi = '';
            var row_radiografi = '';
            for(var z in result.ranap_radiografi[x])
            {
                var dtradiografi = result.ranap_radiografi[x][z];
                ranap_radiografi += '<a style="margin:6px;" href="'+base_url+'cpelayananranap/pemeriksaanklinik_radiologiefilm/'+dtradiografi.idhasilpemeriksaan_efilm+'/'+dtradiografi.idpendaftaran+'/'+dtradiografi.idrencanamedispemeriksaan+'" target="_blank"><img class="img-responsive" style="max-height:195px;min-width:195px;display: inline;" src="'+result.url_nas_simrs+''+dtradiografi.efilm+'" alt="Hasil Radiografi"></a>';
            }
            if(ranap_radiografi !='')
            {
                row_radiografi = '<div class="form-group">'
                    +'<label>Hasil Radiografi</label>'
                    +'<div class="riwayatparagraf">'+ranap_radiografi+'</div>'
                +'</div>';
            }
            
            var data = result.ranap_rencana[x];
            row +='<h3 class="timeline-header" '+((no>1) ? ' style="border-top:1px solid #ddd;" ' : '' )+'><a href="#">Perawatan Pasien </a>  <span style="font-size:14px;"><a class="fa fa-clock-o"></a> '+data.waktu+' </span>- <span class="label label-default">'+data.status+'</span></h3>';
            row += '<div class="timeline-body">'
                + riwayat_detail(result.ranap_anamnesa[x],'Anamnesis')
                + riwayat_detail(result.ranap_lanjutan[x],'Pemeriksaan')
                + riwayat_detail(result.ranap_tindakan[x],'Diagnosa & Terapi')          
                + riwayat_detail(result.ranap_bhp[x],'OBAT/BHP') 
                + row_radiografi
                + riwayat_catatan(result.ranap_catatanterintegrasi[x],'Catatan Terintegrasi')  
                +'</div>';
        }
        row+='</div></li>';
        
        

        $('#riwayatAkhir').before(row);
    }
    
}


function tampil_riwayat_ralan(result)
{
    
    $('#riwayattanggalpemeriksaan').val(result.pendaftaran.waktuperiksa);
    
    var row = '<li class="time-label">'
          +'<span class="bg-aqua">'
            +'Rawat Jalan, <a class="fa fa-calendar" style="color:#fff;"></a> '+ result.pendaftaran.tanggalperiksa
          +'</span>'
    +'</li>';
    
    for(var x in result.ralan_pemeriksaan)
    {
        var data = result.ralan_pemeriksaan[x];
        row += '<li>'
          +'<i class="fa fa-user-md bg-blue"></i>'

          +'<div class="timeline-item">'
            +'<h3 class="timeline-header"><a href="#">Poliklinik '+data.namaunit+'</a>  - <span class="label label-default">'+data.status+'</span></h3>'

              +'<div class="timeline-body">'
                  +'<div class="form-group">'
                      +'<label>Dokter DPJP</label>'
                      +'<div class="riwayatparagraf">'+data.dokter_dpjp+'</div>'
                  +'</div>'

                  +'<div class="form-group">'
                      +'<label>Dokter Pemeriksa</label>'
                      +'<div class="riwayatparagraf">'+data.dokter_pemeriksa+'</div>'
                  +'</div>';
          
                    
                  var anamnesis = '';       
                  
                  anamnesis += data.anamnesa;
                  for(var z in result.ralan_anamnesis[x])
                  {
                      anamnesis += result.ralan_anamnesis[x][z].hasil_anamnesis+'<br>';
                  } 
                  row += '<div class="form-group">'
                      +'<label>Anamnesis</label>'
                      +'<div class="riwayatparagraf">'+anamnesis+'</div>'
                  +'</div>';
          
                  var pemeriksaan='';   
                  pemeriksaan += data.keterangan;
                  pemeriksaan += if_null(result.pendaftaran.keteranganradiologi)+((result.pendaftaran.saranradiologi=='')?'':'<b>Saran:</b>')+result.pendaftaran.saranradiologi+'<br>'+if_null(result.pendaftaran.keteranganlaboratorium)+'<br>';
                  for(var z in result.ralan_planjutan[x])
                  {
                      pemeriksaan += result.ralan_planjutan[x][z].hasil_diagnosis+'<br>';
                  }
                  
                  pemeriksaan += '<br>'+result.pendaftaran.keteranganekokardiografi;
                  row+='<div class="form-group">'
                      +'<label>Pemeriksaan</label>'
                      +'<div class="riwayatparagraf">'+pemeriksaan+'</div>'
                  +'</div>';
                  
                  var ralan_radiografi = '';
                  for(var z in result.ralan_radiografi[x])
                  {
                      var dtradiografi = result.ralan_radiografi[x][z];
                      ralan_radiografi += '<a style="margin:6px;" href="'+base_url+'cpelayanan/pemeriksaanklinik_radiologiefilm/'+dtradiografi.idhasilpemeriksaan_efilm+'/'+dtradiografi.idpendaftaran+'" target="_blank"><img class="img-responsive" style="max-height:195px;min-width:195px;display: inline;" src="'+result.url_nas_simrs+''+dtradiografi.efilm+'" alt="Hasil Radiografi"></a>';
                  }
                  if(ralan_radiografi !='')
                  {
                      row +='<div class="form-group">'
                            +'<label>Hasil Radiografi</label>'
                            +'<div class="riwayatparagraf">'+ralan_radiografi+'</div>'
                        +'</div>';
                  }
          

                  row +='<div class="form-group">'
                      +'<label>Catatan</label>'
                      +'<div class="riwayatparagraf">'+data.rekomendasi+'</div>'
                  +'</div>';

                  var diagnosis = '';               
                  for(var z in result.ralan_diagnosis[x])
                  {
                      diagnosis += result.ralan_diagnosis[x][z].hasil_diagnosis+'<br>';
                  }
                   row+='<div class="form-group">'
                      +'<label>Diagnosa & Terapi</label>'
                      +'<div class="riwayatparagraf">'+diagnosis+'</div>'
                  +'</div>';

                  +'<div class="form-group">'
                      +'<label>BHP/OBAT</label>'
                      +'<div class="riwayatparagraf">'+listresep(result.ralan_resep[x],result.ralan_grup[x]); +'</div>'
                  +'</div>'

              +'</div>'
          +'</div>'
        +'</li>';
    }
    
    $('#riwayatAkhir').before(row);
}


$(document).on('click','#btnRiwayatPasien',function(){
   $('.fastmenu').addClass('hidden');
   var tanggal = $('#riwayattanggalpemeriksaan').val();
   console.log(tanggal);
   if(tanggal == '')
   {
       loadMoreRiwayat();
   }
});

$(document).on('click','#btnPemeriksaan',function(){
   $('.fastmenu').removeClass('hidden');
});