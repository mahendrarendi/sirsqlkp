var norm = localStorage.getItem('norm');//siapkan norm -> ambil dari localstorage norm
var idperiksa = localStorage.getItem('idperiksa');//siapkan idperiksa -> ambil dari localstorage idperiksa
var idpendaftaran = localStorage.getItem('idp');
localStorage.setItem('jenisrawat','rajal');

$(function(){
    $('.select2').select2();
    pemeriksaanklinikDetailPasien();//panggil fungsi (cari data pasien)
    $('input[name="idperiksa"]').val(idperiksa);
    if (!(peranperan.includes("12") && peranperan.length == 1))
    {
        pemeriksaanklinikRefreshBhp();//list bhp
        cariBhpFarmasi($('select[name="caribhp"]'));
    }
});

function cariBhpFarmasi(selectNameTag)//cari bhp
{
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanklinik_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idbarang, text: item.namabarang}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
// pemeriksaan_settbhp
function inputBhpFarmasi(value)//input bhp
{
    if($('textarea[name="keteranganobat"]').val()=='')
    {
        $('textarea[name="keteranganobat"]').focus();
        alert_empty('keterangan resep');
    }
    else
    {
        $.ajax({
            type:"POST",url:"pemeriksaan_inputbhp",data:{x:value, i:idperiksa},dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanklinikRefreshBhp(); //refresh diagnosa
                }
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
function pemeriksaanklinikRefreshBhp()//refreshDataBHP
{
    $.ajax({
        type: "POST",url: 'pemeriksaanklinik_refreshbhp',data: {i:idperiksa},dataType: "JSON",
        success: function(result) {
            var penggunaan = result.penggunaan;
            var result = result.bhpperiksa;
            var lisBhp = '', i=0, no=0, subtotal=0, total=0;jumlahambil=0;
            for (i in result)
            {
                total = result[i].total.toString();
                total = total.slice(0, -3);
                total = parseInt(total);
                jumlahambil = (result[i].kekuatanperiksa/result[i].kekuatan) * result[i].jumlahpemakaian;

                subtotal += parseInt(result[i].total);
                lisBhp += '<tr id="listBhp'+ ++no +'"><td>'+ no +'</td>'+
                          '<td>'+result[i].namabarang+'</td>'+
                          '<td>'+result[i].jumlahpemakaian+'</td>'+
                          '<td>'+result[i].kekuatan+'</td>'+
                          '<td>'+result[i].kekuatanperiksa+'</td>'+
                          '<td>'+jumlahambil+'</td>'+
                          '<td>'+convertToRupiah(total)+'</td>'+
                          '<td>'+result[i].penggunaan+'</td>'+
                          '<td>'+result[i].signa+'</td>'+
                          '<td><input type="hidden" id="idbarang_ralan'+no+'" value="'+result[i].idbarang+'"><a onclick="cetakBhpAturanpakaiRajal('+no+')" data-toggle="tooltip" data-original-title="Etiket" class="btn btn-default btn-xs"><i class="fa fa-print"></i></a></td>'
                          '</tr>';
            }
            $('#viewDataBhp').empty();
            lisBhp +='<tr class="bg bg-info"><td colspan="6">Subtotal</td><td colspan="1">'+convertToRupiah(subtotal)+'</td><td colspan="2"></td></tr>';
            lisBhp +='<tr><td colspan="8"> &nbsp;</td></tr><tr class="bg bg-info"><td colspan="6">Rekap Total</td><td colspan="1" id="totalPemeriksaan"></td><td colspan="2"></td></tr>';
            $('#viewDataBhp').html(lisBhp);
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function editBhpGrup(value)//seting atau ubah grup bhp
{
    var d = $('#grup'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'grup',x,y);
}
function editBhpJumlah(value)//seting atau ubah grup bhp
{
    var d = $('#jumlah'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#harga'+value).val();
    settingBhp(d,'jumlah',x,y);
}
function editBhpSetHarga(value)//seting atau ubah grup bhp
{
    var d = $('#harga'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'harga',x,y);
}
function settingBhp(d,type,x,y)
{
    $.ajax({ 
        url: 'pemeriksaan_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:type, x:x,y:y },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                pemeriksaanklinikRefreshBhp();
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}
function hapusBhp(value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                var x = $('#idbarangpemeriksaan'+value).val();
                var d='', y='';
                settingBhp(d,'del',x,y);
            },
            cancel: function () {               
            }            
        }
    });
}
//////////////////////////Vital Sign////////////////
function pilihvitalsign(value,ispaket)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahvitalsign",
        data:{x:value, y:idpendaftaran,ispaket:ispaket},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshVitalsign(idpendaftaran); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshVitalsign(value)//refreshDataDiagnosa
{
    var x=0, html='', paket='';
    $('#listVitalsign').empty();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listvitalsign',
        data: {x:value, y:idperiksa},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                paket = ((namapaket!=result[x].namapaketpemeriksaan)? '<tr height="30px"><td class="bg-warning" colspan="5">'+result[x].namapaketpemeriksaan+' </td><td class="bg-warning"></td><tr>' : ''); 
                html += paket + '<tr><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                '<td> <input type="hidden" name="icdvitalsign[]" value="'+result[x].icd+'"/><input type="hidden" name="istextvitalsign[]" value="'+result[x].istext+'"/>'+
                '<input readonly type="text" class="form-control" value="'+ ((result[x].nilai==null)? '' : result[x].nilai) +'" placeholder="Input nilai" name="nilaivitalsign[]" size="1" /></td>'+
                '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                '<td>'+ result[x].satuan +'</td>'+
                '</tr>';
                var namapaket = result[x].namapaketpemeriksaan;
            }
            $('#listVitalsign').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function hapusvitalsign(x)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapusvitalsign',
        data: {x:x},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshVitalsign(idpendaftaran);
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////DIAGNOSA////////////////
function pilihdiagnosa(value)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahdiagnosa",
        data:{x:value, y:idpendaftaran},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshDiagnosa(idpendaftaran); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshDiagnosa(value,idicd)//refreshDataDiagnosa
{
    var x=0, html='';
    $('#listDiagnosa').empty();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listdiagnosa',
        data: {x:value,idicd:idicd},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                html += '<tr><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ result[x].aliasicd +'</td><td>'+ result[x].icdlevel +'</td></tr>';
            }
            $('#listDiagnosa'+idicd).html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function hapusdiagnosa(x)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapusdiagnosa',
        data: {x:x},
        dataType: "JSON",
        success: function(result) {
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshDiagnosa(idpendaftaran); //refresh diagnosa
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////TINDAKAN////////////////
function pilihtindakan(value)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahtindakan",
        data:{x:value, y:idpendaftaran},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(idpendaftaran); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

function pemeriksaanRefreshTindakan(value)//refreshDataDiagnosa
{
    var x=0, html='', subtotal=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listtindakan',
        data: {x:value},
        dataType: "JSON",
        success: function(result) {
            for(x in result.tindakan)
            {
                subtotal += parseInt(result.tindakan[x].total);
                html += '<tr>\n\
                <td>'+ result.tindakan[x].icd +'</td>\n\
                <td>'+ result.tindakan[x].namaicd +'</td>\n\
                <td>'+ result.tindakan[x].dokterpenerimajm +'</td>\n\
                <td>'+ result.tindakan[x].jumlah +'</td>\n\
                <td>'+ result.tindakan[x].total +'</td></tr>';
            }
            $('#listTindakan').empty();
            html += '<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listTindakan').html(html);
            $('#totalPemeriksaan').empty();
            $('#totalPemeriksaan').html(convertToRupiah(bulatkanRatusan(parseInt(result.total[0].totalseluruh1) + parseInt(result.total[0].totalseluruh2))));
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function hapustindakan(x)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapustindakan',
        data: {x:x},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(idpendaftaran);
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////RADIOLOGI////////////////
function pilihradiologi(value)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahradiologi",
        data:{x:value, y:idpendaftaran},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshRadiologi(idpendaftaran); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshRadiologi(value)//refreshDataDiagnosa
{
    var x=0, html='', subtotal=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listradiologi',
        data: {x:value},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                subtotal += parseInt(result[x].total);
                html += '<tr><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ result[x].total +'</td></tr>';
            }
            $('#listRadiologi').empty();
            html += '<tr class="bg bg-info"><td colspan="2">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listRadiologi').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function hapusradiologi(x)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapusradiologi',
        data: {x:x},
        dataType: "JSON",
        success: function(result) {
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshRadiologi(idpendaftaran);
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////Laboratorium////////////////
function pilihlaboratorium(value,ispaket)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahlaboratorium",
        data:{x:value, y:idpendaftaran,ispaket:ispaket},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshLaboratorium(idpendaftaran); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshLaboratorium(value)//refreshDataDiagnosa
{
    var x=0, html='', paket='', parent='', subtotal=0, keteranganhasil='';
    var modelist = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listlaboratorium',
        data: {x:value, y:idperiksa},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {   
                if(result[x].idjenisicd2=='4')
                {
                    //tampilkan keterangan hasil laborat
                    if(result[x].idgrup != idgrup && idgrup!=null)
                    {
                         html += '<tr ><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
                         keteranganhasil='';
                    }
                    paket = ((namapaket==result[x].namapaketpemeriksaan && gruppaket==result[x].gruppaketperiksa) ? '' : ((result[x].namapaketpemeriksaan==null)?'':'<tr height="30px" '+((result[x].idgrup==result[x].idpaketpemeriksaan)? 'class="bg-info"' : '' )+'><td colspan="6" class="text-bold">'+result[x].namapaketpemeriksaan+'</td><td>'+((result[x].isubahwaktuhasil == 1) ? '<a class="btn btn-warning btn-xs"  id="ubahwaktuhasil"><i class="fa fa-clock-o"></i> Ubah Waktu Hasil</a>' : '' )+'</td></tr>' ));
                    subtotal += parseInt(result[x].total) - parseInt(result[x].potongantagihan);
                    html += paket + ((result[x].icd==null)?'': '<tr '+((result[x].idgrup!==null)?'':'class="bg-warning"')+'><td> &nbsp;&nbsp;&nbsp;'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                    ///////set untuk simpan data
                    '<td><input type="hidden" name="icd[]" value="'+result[x].icd+'"/><input type="hidden" id="istypehasil'+result[x].idhasilpemeriksaan+'" value="'+result[x].istext+'"/>'+ result[x].nilai +'</td>'+
                    '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                    '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                    '<td>'+ result[x].satuan +'</td>'+
                    '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].total) : '' )+'</td>'+
                    '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].potongantagihan) : '' )+'</td>'+
                    '<td></td>'
                    +'</tr>');
                    
                    
                    var idpaketpemeriksaan = result[x].idpaketpemeriksaan;
                    if(result[x].keteranganhasil !=null && result[x].keteranganhasil != keteranganhasil)
                    {
                        keteranganhasil = result[x].keteranganhasil;
                    }
                    var idgrup = result[x].idgrup;
                    var namapaket= result[x].namapaketpemeriksaan;
                    var gruppaket=result[x].gruppaketperiksa;    
                    
                }
            }
            //tampilkan keterangan hasil laborat
            html += '<tr ><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
            
            $('#listLaboratorium').empty();
            $('#listLaboratorium').html(html+'<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
    
$(document).on('click','#ubahwaktuhasil',function(){
    var modalTitle = 'Ubah Waktu Hasil Laboratorium';
    var modalContent = '<form action="" id="formUbah">' +
                            '<div class="form-group">' +
                                '<label>Waktu Proses</label>' +
                                '<input type="text" id="txtwaktuproses" name="txtwaktuproses" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Waktu Selesai</label>' +
                                '<input type="text"id="txtwaktuselesai" name="txtwaktuselesai"  class="form-control"/>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: base_url + 'cpelayanan/set_waktuhasillaborat',
                        data: { idpemeriksaan : idperiksa, ok:$('#txtwaktuselesai').val(), proses:$('#txtwaktuproses').val()},
                        dataType: "JSON",
                        success: function(result) {
                            notif(result.status,result.message);
                        },
                        error: function(result) { //jika error
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        $.ajax({
            type: "POST",
            url: base_url + 'cpelayanan/get_waktuhasillaborat',
            data: { idpemeriksaan : idperiksa },
            dataType: "JSON",
            success: function(result) {
                $('#txtwaktuproses').val(result.proses);
                $('#txtwaktuselesai').val(result.selesai);
            },
            error: function(result) { //jika error
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
    
function hapuslaboratorium(x)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuslaboratorium',
        data: {x:x},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshLaboratorium(idpendaftaran);
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function hapuspaketparent(x,z)
{
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuspaketpemeriksaan',
        data: {x:x, y:idpendaftaran},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                if(z=='laboratorium')
                {
                    pemeriksaanRefreshLaboratorium(idpendaftaran);
                }
                else
                {
                    pemeriksaanRefreshVitalsign(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////diagnosa_or_tindakan//////////////////
function diagnosa_or_tindakan(x,y)
{
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanklinik_diagnosa",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                jenisicd: y,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
///////////////////////////////////////////////
function pilihJenisIcd(value)//pilih jenis diagnosa
{
    pilihPaketDiagnosa($('select[name="paket"]'),value);
}
function singleDiagnosa(value)//tambah single diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addsinglediagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function paketDiagnosa(value)//tambah paket diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addpaketdiagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pilihSingleDiagnosa(selectNameTag,icd) //pilih diagnosa 
{  
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanklinik_cariicd",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                icd:icd,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.namaicd + ((item.aliasicd!='') ? ' >>> ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
function pilihPaketDiagnosa(htmlTagName,icd) //pilih diagnosa 
{  
    $.ajax({
        url:'diagnosa_pilihpaket',
        type:'POST',
        dataType:'JSON',
        data:{x:icd},
        success: function(result){
            var select='<option value="0" >Pilih</option>';
            htmlTagName.empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idpaketpemeriksaan +'" >' + result[i].namapaketpemeriksaan +'</option>';
            }
            htmlTagName.html(select);
            $('.select2').select2();
            pilihSingleDiagnosa($('select[name="single"]'),icd);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
function pemeriksaanklinikDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanklinik_caridetailpasien',
        data: {norm:norm, i:idperiksa},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            pemeriksaan_inputAnamnesa(result.grupjadwal);
            $('input[name="dokter"]').val(result.periksa.namalengkap);
            $('textarea[name="alergiobat"]').val(result.pasien.alergi);
            $('textarea[name="diagnosa"]').val(result.periksa.diagnosa);
            $('textarea[name="keterangan"]').val(result.periksa.keterangan);
            $('textarea[name="rekomendasi"]').val(result.periksa.rekomendasi);
            $('textarea[name="keteranganradiologi"]').val(result.pasien.keteranganradiologi);
            $('textarea[name="saranradiologi"]').val(result.pasien.saranradiologi);
            if(result.keterangan_ekokardiografi.length > 0)
            {
                if(result.pasien.keteranganekokardiografi == null){
                    $('textarea[name="keteranganekokardiografi"]').val(result.keterangan_ekokardiografi[0].keterangan);
                }else{
                    $('textarea[name="keteranganekokardiografi"]').val(result.pasien.keteranganekokardiografi);
                }
            }
            $('textarea[name="keteranganobat"]').val(result.pasien.keteranganobat);
            $('textarea[name="keteranganlaboratorium"]').val(result.pasien.keteranganlaboratorium);
            $('#kondisikeluar').val(result.pasien.kondisikeluar);
            $('#statuspasienpulang').val(result.pasien.statuspulang);
            viewIdentitasPasien(result.pasien,result.penanggung);//panggil view identitas
            // fungsiViewHasilPemeriksaanPasien(result.viewhasilpemeriksaan, result.hasilpemeriksaan);
            $('input[name="idpendaftaran"]').val(result.periksa.idpendaftaran);
            $('input[name="norm"]').val(result.pasien.norm);
            //refresh diagnosa
            pemeriksaanRefreshDiagnosa(result.periksa.idpendaftaran,10);
            pemeriksaanRefreshDiagnosa(result.periksa.idpendaftaran,9);
//            pemeriksaanRefreshDiskon(result.periksa.idpendaftaran);
            pemeriksaanRefreshTindakan(result.periksa.idpendaftaran);
            pemeriksaanRefreshRadiologi(result.periksa.idpendaftaran);
            tampilHasilRadiografi(result.periksa.idpendaftaran,'view');
            pemeriksaanRefreshLaboratorium(result.periksa.idpendaftaran);
            pemeriksaanRefreshVitalsign(result.periksa.idpendaftaran);                   
            pemeriksaanRefreshEkokardiografi(result.periksa.idpendaftaran);
            $('input[name="idunit"]').val(result.periksa.idunit);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
// mahmud, clear :: menambahkan inputan anamnesa
function pemeriksaan_inputAnamnesa(value)
{
    var dt='';
    for(x in value)
    {
        var namaunit = ((value.length == 1)?'':value[x].namaunit);
        dt+='<input id="idanamnesa'+x+'" type="hidden" name="grupanamnesa[]" value="'+value[x].idpemeriksaan+'">';
        dt+='<div class="form-group">'
                +'<label for="" class="col-sm-2 control-label">Data Subyektif '+namaunit+'</label>'
                +'<div class="col-sm-9"><textarea id="namaanamnesa'+x+'" disabled class="form-control textarea" name="anamnesa'+value[x].idpemeriksaan+'" placeholder="Anamnesa '+namaunit+'" rows="4">'+value[x].anamnesa+'</textarea>'
                +'</div>'
              +'</div>';
    }
    $('#pemeriksaanInputAnamnesa').empty();
    $('#pemeriksaanInputAnamnesa').html(dt);
    $('.textarea').wysihtml5({toolbar:false});
}
function viewIdentitasPasien(value,valpenanggung)//tampilidentitas
{
    var profile='';
    var dtp = '';
    for(var x in valpenanggung){dtp+='<tr><td>'+valpenanggung[x].namalengkap+'</td><td>'+valpenanggung[x].hubungan+'</td><td>'+valpenanggung[x].notelpon+'</td></tr>';}
    var penanggung = '<table class="table table-bordered"><tr><th>Nama Penanggung</th><th>Hubungan </th><th> No.Telp</th></tr>'+dtp+'</table>';
    
    profile = '<div class="bg-info">'
                +'<div class="box-body box-profile">'
                 +'<div class="login-logo">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-muted text-center">'+value.ispasienlama +'</p>'
                  +'<h5 class="text-muted text-center">Usia '+ambilusia(value.tanggallahir, value.tglsekarang)+' </h5>'
                +'</div>'
              +'</div>';
    var detail_profile='';
    detail_profile = '<div class="bg-info"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>NIK </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nik+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-calendar margin-r-5"></i>Tanggal Lahir</strong></td>'
                  +'<td class="text-muted">: &nbsp;'+value.tanggallahir+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.jeniskelamin+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor SEP </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nosep+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' //'+value.golongandarah+' '+value.rh+'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RUJUKAN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norujukan+'</td>'

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.pendidikan+'</td>'

                  +((value.tanggalrujukan=='0000-00-00' || value.tanggalrujukan=='')? '<td colspan="2"></td>':'<td class="bg bg-red"><strong><i class="fa fa-circle-o margin-r-5"></i>Rujukan BPJS Selanjutnya</strong></td><td class="bg bg-red text-muted">: &nbsp; '+getNextDate(value.tanggalrujukan,90)+'</td>')

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i> Agama</strong></td>'
                  +'<td class="text-muted" >: &nbsp;'+value.agama+'</td>'

                    +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Penjamin </strong></td>'
                +'<td class="text-muted">: &nbsp; '+value.carabayar+'</td>'

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Alamat </strong></td>'
                  +'<td class="text-muted" width="250px">: &nbsp; '+value.alamat+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ayah </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.ayah+'</td>'
                +'</tr>'
                +'<tr><td colspan="2"><strong><i class="fa fa-circle-o margin-r-5"></i>Penanggung </strong></td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ibu </strong></td><td class="text-muted">: &nbsp; '+value.ibu+'</td></tr>'
                +'<tr><td colspan="2">'+penanggung+'</td></tr>'        
                +'</tbody>'
             +'</table></div></div>';
    $('#pemeriksaanklinik_profile').html(profile);
    $('#pemeriksaanklinik_detailprofile').html(detail_profile);
    // $('#view_identitasPasien').html('<b>No.RM:</b> '+value.norm+' ,<b> No.JKN:</b>  '+value.nojkn+' ,<b> NIK:</b>  '+value.nik+', <b>Nama:</b>  '+value.namalengkap+', <b> TanggalLahir:</b> '+value.tanggallahir +', <b> Alamat:</b> '+value.alamat+' [<b style="color:#000;">'+ ((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama ) +'</b>]');
}
function updateBiayahasilperiksa(y,z)//seting hahasilpemeriksaanpaket laboratorium
{
    var idpaketygdiubah = y;
    var modalTitle = 'Ubah Biaya';
    var modalContent = '<form action="" id="Formubahhargapaket">' +
                            '<div class="form-group">' +
                            '<input type="hidden" name="id" value="'+idpaketygdiubah+'" class="form-control"/>'+
                            //jasa operator
                                '<div class="col-md-3">'+
                                    '<label>Jasa Operator</label>' +
                                    '<input type="text" name="jasaoperator" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //nakes
                                '<div class="col-md-3">'+
                                    '<label>Nakes</label>' +
                                    '<input type="text" name="nakes" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //jasars
                                '<div class="col-md-3">'+
                                    '<label>Jasars</label>' +
                                    '<input type="text" name="jasars" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //bhp
                                '<div class="col-md-3">'+
                                    '<label>Bhp</label>' +
                                    '<input type="text" name="bhp" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //akomodasi
                                '<div class="col-md-3">'+
                                    '<label>Akomodasi</label>' +
                                    '<input type="text" name="akomodasi" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //margin
                                '<div class="col-md-3">'+
                                    '<label>Margin</label>' +
                                    '<input type="text" name="margin" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //sewa
                                '<div class="col-md-3">'+
                                    '<label>Sewa</label>' +
                                    '<input type="text" name="sewa" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Update',
                btnClass: 'btn-blue',
                action: function () {
                    
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: 'savepaketygdiubah', //alamat controller yang dituju
                        data: $("#Formubahhargapaket").serialize(), //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            notif(result.status, result.message);
                            if(result.status=='success')
                            {
                                if(z=='Tindakan')
                                {
                                    pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val());
                                }
                                else if(z=='Radiologi')
                                {
                                    pemeriksaanRefreshRadiologi($('input[name="idpendaftaran"]').val());
                                }
                                else
                                {
                                    pemeriksaanRefreshLaboratorium($('input[name="idpendaftaran"]').val());
                                }
                            }
                        },
                        error: function(result) { //jika error
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        //tampilkan harga yang akan diubah
        tampilhargayangdiubah(idpaketygdiubah);
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function tampilhargayangdiubah(x)
{
    $.ajax({
        type:"POST",
        url:"tampilhargayangdiubah",
        data:{x:x},
        dataType:"JSON",
        success: function(result){
            $('input[name="jasaoperator"]').val(result.jasaoperator);
            $('input[name="nakes"]').val(result.nakes);
            $('input[name="jasars"]').val(result.jasars);
            $('input[name="bhp"]').val(result.bhp);
            $('input[name="akomodasi"]').val(result.akomodasi);
            $('input[name="margin"]').val(result.margin);
            $('input[name="sewa"]').val(result.sewa);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusPaketPeriksa(value)//hapus paket pemeriksaan
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = value;
    $.ajax({
        type:"POST",
        url:"delete_paket_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function deleteHasilPeriksa(value)//hapus hasil periksa per icd
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = $('#deleteHasilPeriksa'+value).val();
    $.ajax({
        type:"POST",
        url:"delete_hasil_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaan_tampilpasien() //tampil data pasien
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tampilpasien",
        data:{x:norm, y:idperiksa},
        dataType:"JSON",
        success: function(result){
            $('#labNormPasien').html(': '+result.p.norm);
            $('#labNamaPasien').html(': '+result.p.namalengkap);
            $('#labPengirim').html(': '+result.per.titeldepan+' '+result.per.namalengkap+' '+result.per.titelbelakang);
            $('#labKelaminPasien').html(': '+result.p.jeniskelamin);
            $('#labTgllahirPasien').html(': '+result.p.tanggallahir);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

//////////////MENAMPILKAN DATA DETAIL PASIEN//////////////////
function pemeriksaandetailTampilPasien()
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_detail_tampil_pasien",
        data:{x:idperiksa},
        dataType:"JSON",
        success:function(result){
            // console.log(result);
            pemeriksaandetailTampilPasienListData(result);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaandetailTampilPasienListData(data)
{
    var modalTitle = '<div class="col-sm-12 center">Detail Pasien</div>';
    var modalContent = '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Nama</th><th style="font-weight:normal;">: '+data.namalengkap+'</th></tr>'+
                                    '<tr><th>No.RM</th><th style="font-weight:normal;">: '+data.norm+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamat+'</th></tr>'+
                                    '<tr><th>Telpon</th><th style="font-weight:normal;">: '+data.telponpasien+'</th></tr>'+
                                    '<tr><th>Kelamin</th><th style="font-weight:normal;">: '+data.jeniskelamin+'</th></tr>'+
                                    '<tr><th>TanggalLahir</th><th style="font-weight:normal;">: '+data.tanggallahir+'</th></tr>'+
                                    '<tr><th>Agama</th><th style="font-weight:normal;">: '+data.agama+'</th></tr>'+
                                    '<tr><th>Status</th><th style="font-weight:normal;">: '+data.statusmenikah+'</th></tr>'+
                                    '<tr><th>Golongan Darah</th><th style="font-weight:normal;">: '+data.golongandarah+'</th></tr>'+
                                    '<tr><th>Rhesus</th><th style="font-weight:normal;">: '+data.rh+'</th></tr>'+
                                    '<tr><th>Alergi</th><th style="font-weight:normal;">: '+data.alergi+'</th></tr>'+
                                '</table>'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Penanggung</th><th style="font-weight:normal;">: '+data.namapenjawab+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamatpenjawab+'</th></tr>'+
                                    '<tr><th>No.Telpon</th><th style="font-weight:normal;">: '+data.notelpon+'</th></tr>'+
                                    '<tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr>'+
                                    '<tr><th>Klinik</th><th style="font-weight:normal;">: '+data.namaunit+'</th></tr>'+
                                    '<tr><th>No.Antri</th><th style="font-weight:normal;">: '+data.noantrian+'</th></tr>'+
                                    '<tr><th>Dokter</th><th style="font-weight:normal;">: '+data.titeldepan+' '+data.namadokter+' '+data.titelbelakang+'</th></tr>'+
                                    '<tr><th>Carabayar</th><th style="font-weight:normal;">: '+data.carabayar+'</th></tr>'+
                                    '<tr><th>No.SEP</th><th style="font-weight:normal;">: '+data.nosep+'</th></tr>'+
                                '</table>'+
                            '</div>';
    $.alert({
        title: modalTitle,
        content: modalContent,
        columnClass: 'xl',
    });
}

function pemeriksaanRefreshEkokardiografi(value)
{
//    
    var x=0, html='', paket='', parent='', subtotal=0, keteranganhasil='';
    var modelist = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listekokardiografi',
        data: {x:value, y:idperiksa,modelist:modelist},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {  
                    paket = (
                                (
                                    namapaket==result[x].namapaketpemeriksaan && gruppaket==result[x].gruppaketperiksa) ? '' : 
                                    ((result[x].namapaketpemeriksaan==null)?'':'<tr height="30px" '+((result[x].idgrup==result[x].idpaketpemeriksaan)? 'class="bg-info"': '' ) +'><td colspan="3" class="text-bold">'+result[x].namapaketpemeriksaan+'</td><td class="">'+ 
                                    ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].total : '' ) +'</td></tr>' 
                                )
                            );
                                        
                    subtotal += parseInt(result[x].total) - parseInt(result[x].potongantagihan);
                    html += paket + ((result[x].icd==null)?'': '<tr '+((result[x].idgrup!==null)?'':'class=""')+'><td> &nbsp;&nbsp;&nbsp;'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                    '<td>'+result[x].nilai+'</td>'+
                    '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) + ' ' +result[x].satuan +'</td></tr>');


                var idpaketpemeriksaan = result[x].idpaketpemeriksaan;
                var idgrup = result[x].idgrup;
                var namapaket= result[x].namapaketpemeriksaan;
                var gruppaket=result[x].gruppaketperiksa;    
                    
            }
            
            
            $('#listEkokardiografi').empty();
            $('#listEkokardiografi').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}