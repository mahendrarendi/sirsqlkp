var dtkasir='';
var dtpenanggung = [];
$(function (){
    settanggal();
    select2_serachmultidata();
    get_datapoli('#idunit', ( (localStorage.getItem('idunit')) ? localStorage.getItem('idunit') : '' ) );
    $('#idunit').select2();
    $('#idbangsal').select2();
    listdatakasir();
    getpenanggung();
});

function getpenanggung()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/fillpenanggungkasir',type:"POST",dataType:"JSON",
        success: function(result){
            dtpenanggung = result
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    }); 
}

function reloadPemeriksaanByUnit()
{ 
    localStorage.setItem('idunit',$('#idunit').val());
    return dtkasir.ajax.reload(null,false);
}

$(document).on('click','#tampilkasir',function(){ dtkasir.ajax.reload();});
$(document).on('click','#refreshkasir',function(){ 
    settanggal(); 
    $('#caripasien').val(''); 
    $('input[type="search"]').val('').keyup(); 
    dtkasir.state.clear(); 
    dtkasir.ajax.reload();});
$(document).on('change','#caripasien',function(){ dtkasir.ajax.reload(); });
function reloadKasir(){dtkasir.ajax.reload();}

function settanggal()
{
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
}
function listdatakasir()
{
    dtkasir = $('#dtkasir').DataTable({"dom": '<"toolbar">frtip',"processing": true,"serverSide": true,"stateSave": true,"order": [],
     "ajax": {
         "data":{
             isbelumdibayar:function(){return $('input[name="isbelumdibayar"]:checked').val();},
             tgl:function(){ return $('input[name="tanggal"]').val()}, 
             tgl2:function(){ return $('input[name="tanggal2"]').val()}, 
             norm:function(){ return $('#caripasien').val()},
             idunit:function(){return localStorage.getItem('idunit')},
         },
         "url": base_url+'cpelayanan/dt_listkasir',
         "type": "POST"
     },
     "columnDefs": [{ "targets": [ 9,10,11,12],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
       //$(nRow).attr('id', 'row' + iDataIndex);
        $(nRow).attr('style', qlstatuswarnapp(aData[13]));
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}
function select2_serachmultidata()
{
  var hal=0;
  $("#caripasien").select2({
      allowClear: true,
      ajax: { url: base_url+"cpelayanan/kasir_getdtpasien", type: "post", dataType: 'json', delay: 250,
       data: function (params) {
        ((params.term=='')?hal +=1:hal=0);
        return { searchTerm: params.term, page: hal, };
       },
       processResults: function (response) {
         return {
            results: response,
         };

       },
       cache: true
      }
     });
}

$(document).on('click','#detailtagihan', function(){
    var idp = $(this).attr('alt');
    startLoading();
    $.ajax({ 
        url:  base_url+'cpelayanan/keu_detailtagihan',type:"POST",dataType:"JSON",data:{p:idp},
        success: function(result){
            stopLoading();
            var header='<table class="table table-striped table-bordered">';
            header+='<tr><td class="bg-info">No.RM</td><td colspan="5">: '+result[0].norm+'</td></tr>';
            header+='<tr><td class="bg-info">Nama Pasien</td><td colspan="5">: '+result[0].namapasien+'</td></tr>';
            header+='<tr><td class="bg-info">Tgl.Periksa</td><td colspan="5">: '+result[0].tanggalperiksa+'</td></tr>';
            header+='<tr><td class="bg-info">Carabayar</td><td colspan="5">: '+result[0].carabayar+'</td></tr>';
            header+='<tr><td class="bg-info">StatusTagihan</td><td colspan="5">: '+result[0].statustagihan+'</td></tr>';
            header+='<tr class="bg-yellow"><th>Waktu Tagih</th><th>Tagihan</th><th>Kekurangan</th><th>Dibayar</th><th>Jenis Tagihan</th><th></th></tr>';
            var body = '';
            for(var x in result)
            {
                body+='<tr><td>'+result[x].waktutagih+'</td><td>'+convertToRupiah(result[x].tagihan)+'</td><td>'+convertToRupiah(parseInt(result[x].tagihan)-parseInt(result[x].dibayar))+'</td><td>'+convertToRupiah(result[x].dibayar)+'</td><td>'+result[x].jenistagihan+'</td><td><a id="ubahjenistagihan" alt="'+result[x].idtagihan+'" ispesanranap="'+result[x].statusinap+'" alt2="'+convertToRupiah (result[x].tagihan)+'" alt3="'+result[x].idpendaftaran+'" alt4="'+result[x].jenispemeriksaan+'" alt5="'+convertToRupiah(result[x].dibayar)+'" alt6="'+result[x].jenistagihan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Bayar" href="#"><i class="fa fa-edit"></i></a> <a id="batallunas" waktu="'+result[x].waktutagih+'" nominal="'+convertToRupiah(result[x].dibayar)+'" jenistagihan="'+result[x].jenistagihan+'" idt="'+result[x].idtagihan+'" class="btn btn-xs btn-danger" '+  tooltip('Batal Lunas') +' ><i class="fa fa-money"></i></a> </td></tr>';
            }
            var html = header+body+'</table>';
            
            $.confirm({ //aksi ketika di klik menu
                title: 'Ubah Jenis Tagihan',
                content: html,
                closeIcon: true,
                columnClass: 'medium',
                buttons: {
                    
                    //menu back
                    formReset:{
                        text: 'kembali',
                        btnClass: 'btn-danger'
                    }
                },
            });
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    }); 
});

$(document).on("click","#ubahjenistagihan", function(){
    var idt = $(this).attr("alt");
    var nom = $(this).attr("alt2");
    var idp = $(this).attr("alt3");
    var jp  = $(this).attr("alt4");
    var dibayar = $(this).attr("alt5");
    var modeprint = (($(this).attr("ispesanranap")=='pesan') ? 3 : '' );
    var jenistagihan = $(this).attr("alt6");
    var isranap = (($(this).attr("ispesanranap")=='pesan') ? '<div class="bg bg-danger" style="padding-left:none;">*Pasien Sudah Booking RANAP. <br> Setelah menyelesaikan pembayaran, status pasien dimutasi ke RANAP.</div>' : '' );
    formBayarNonJaminan(idt,nom,idp,jp,modeprint,isranap,dibayar,jenistagihan);
});

//dipanggil dari a id bayar - di dalam fungsi tampilkankasir()
$(document).on("click","#bayar", function(){
    var idt = $(this).attr("alt");
    var nom = $(this).attr("alt2");
    var idp = $(this).attr("alt3");
    var jp  = $(this).attr("alt4");
    var modeprint = (($(this).attr("ispesanranap")=='pesan') ? 3 : '' );
    var isranap = (($(this).attr("ispesanranap")=='pesan') ? '<label class="col-xs-12" style="padding-left:none;"><small class="text text-red">*Pasien Sudah Booking RANAP. <br> Setelah menyelesaikan pembayaran, status pasien dimutasi ke RANAP.</small></label>' : '' );
    formBayarNonJaminan(idt,nom,idp,jp,modeprint,isranap);
});

function formBayarNonJaminan(idt,nom,idp,jp,modeprint,isranap,dibayar=0,jenistagihan='')
{
    var bayar_transfer = ((jenistagihan!='' && jenistagihan=='transfer') ?  dibayar : 0 );
    var bayar_tagihan = ((jenistagihan!='' && jenistagihan=='tagihan') ?  dibayar : 0 );
    var modalSize = 'lg';
    var modalTitle = 'Pelunasan Biaya';
    var modalContent = '<form action="" id="formBayar">' +
                            '<div class="form-group col-md-12 bg bg-info" style="padding:10px;border-radius:4px;">' +
                                '<div class="pull-left" style="font-size:16px;padding-top:0px;margin-top:0px;"><label>TOTAL TAGIHAN : </label></div>'+
                                '<div class="pull-right"><label id="labeltotaltagihan" class="" style="font-size:30px;margin-left:8px;">Rp '+nom+'</label></div>'+
                                '<input type="hidden" name="altt" value="'+idt+'" />' +
                                '<input type="hidden" name="altp" value="'+idp+'" />' +
                                '<input type="hidden" name="modebayar" value="'+ ((dibayar > 0) ? 'ubahtagihan' :'tagihan' ) +'" />' +
                                
                            '</div>' +
                            '<div class="form-group">' +
                                '<div class="col-md-6">'+
                                    '<label>TAGIHAN</label>' +
                                    '<input style="margin-bottom:8.5px;" type="text" name="txttagihan" value="'+nom+'" id="txttagihan" class="form-control" onkeyup="hitungkembalian(event)" disabled />' +
                                    
                                    '<label>NOMINAL BAYAR</label>' +
                                    '<input style="margin-bottom:8.5px;" type="text" name="txtnominalbayar" value="'+bayar_tagihan+'" placeholder="nominal tunai" autocomplete="off" id="txtnominalbayar" class="form-control" onkeyup="hitungkembalian(event)" />' +
                                    
                                '</div>'+
                                '<div class="col-md-6">' +
                                    '<label>CARA BAYAR</label>'+
                                    '<div">\n\
                                        <select id="carabayar" onchange="optioncarabayar(this.value)" class="form-control" style="margin-bottom:6px;"></select>'+
                                        '<span id="optioncarabayar"></span>'+
                                        
                                '</div>'+
                                
                            '</div>'+
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group" style="margin-top:9px;">' +
                                '<div class="col-md-6">' +
                                    '<label>KEKURANGAN</label>' +
                                    '<input type="text" name="txtkekurangan" placeholder="kekurangan" class="form-control" disabled/>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-md-12"></div>'+
                            '<div class="form-group" style="margin-top:9px;">' +
                                '<div class="col-md-6">' +
                                    '<label>KEMBALI</label>' +
                                    '<input type="text" name="txtkembali"  placeholder="kembali" class="form-control" disabled/>' +
                                '</div>'+
                            '</div>' +
                            isranap+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#txtnominalbayar').val()=='' || $('#txtnominalbayar').val()=='0' ||  $('#txtnominalbayar').val()== 0)
                    {
                        $('#txtnominalbayar').focus();
                        alert_empty('Nominal Pembayaran.');
                        return false;
                    }
                    else if( $('#carabayar').val() == 0  || $('#carabayar').val()=='0')
                    {
                        alert_empty('Cara Pembayaran.');
                        return false;
                    }
                    else if($('#selbanktujuan').val()== 0  && $('#carabayar').val() == 'transfer')
                    {
                        alert_empty('bank tujuan');
                        return false;
                    }
                    else
                    {
                        simpanpembayaran();
                        cetakvpasien($('input[name="altp"]').val(), jp,modeprint);    
                    }
                    
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        dropdown_getdata($('#carabayar'),'cmasterdata/get_enumjenispembayaran','','<option value="0">Pilih</option>');
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}


//tampil bank atau penanggung
function optioncarabayar(value){
    var option = '';
    if(value == 'potonggaji'){
        option = '<label>Penanggung</label>\n\
                  <input list="listpenanggung" id="penanggung" name="penanggung" type="text" class="form-control" placeholder="penanggung"/>\n\
                  <datalist id="listpenanggung"></datalist>';
        $('#optioncarabayar').html(option);
        listpenanggung();
    }else if(value == 'transfer'){
        option = '<label>Bank Tujuan</label>\n\
                  <select id="selbanktujuan" onchange="hitungkembalian(event)" name="selbanktujuan" class="select2" style="width:100%;"><option value="">Pilih</option></select></div>';
        $('#optioncarabayar').html(option);        
        dropdown_getdata($('#selbanktujuan'),'cmasterdata/fillkeuakunbank','','<option value="0">Bank Tujuan</option>');
        $('.select2').select2();
    }else{
        $('#optioncarabayar').html(option);        
    }
    
}
function listpenanggung(){
    var data = dtpenanggung;
    var list = '';
    for(var x in data)
    {
        list += '<option value="'+data[x].text+'">';
    }
   $('#listpenanggung').empty();
   $('#listpenanggung').html(list);
}

//dipanggil dari a id jknnonpbi - di dalam fungsi tampilkankasir()
$(document).on("click","#jknnonpbi", function(){
    var idt = $(this).attr("alt");
    var idp = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var jp  = $(this).attr("alt4");
    var modeprint = (($(this).attr("ispesanranap")=='pesan') ? 3 : '' );
    var nominal = $(this).attr("nominal");
    
    $.confirm({
        title: 'Konfirmasi Pembayaran',
        content: 'Bayar dengan JKN NONPBI.?',
        closeIcon: true,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Bayar',
                btnClass: 'btn-warning',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cpelayanan/simpan_pembayaran_jkn', //alamat controller yang dituju (di js base url otomatis)
                        data: {t:idt, p:idp, j:'jknnonpbi',dibayar:nominal}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            if(result['status'] == undefined)
                            {
                                cetakvpasien(idp, jp,modeprint);
                                dtkasir.ajax.reload(null,false);
                                
                            }
                            else
                            {
                                return formBayarNonJaminan(result['tagihan']['idtagihan'],result['tagihan']['nominal'],idp,jp,'mandiri','');
                            }
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
});

$(document).on("click","#jknpbi", function(){
    var idt = $(this).attr("alt");
    var idp = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var jp  = $(this).attr("alt4");
    var modeprint = (($(this).attr("ispesanranap")=='pesan') ? 3 : '' );
    var nominal = $(this).attr("nominal");
    $.confirm({
        title: 'Konfirmasi Pembayaran',
        content: 'Bayar dengan JKNPBI.?',
        closeIcon: true,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Bayar',
                btnClass: 'btn-warning',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cpelayanan/simpan_pembayaran_jkn', //alamat controller yang dituju (di js base url otomatis)
                        data: {t:idt, p:idp, j:'jknpbi',dibayar:nominal}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            if(result['status'] == undefined)
                            {
                                cetakvpasien(idp, jp,modeprint);
                                dtkasir.ajax.reload(null,false);
                            }
                            else
                            {
                                return formBayarNonJaminan(result['tagihan']['idtagihan'],result['tagihan']['nominal'],idp,jp,'mandiri','');
                            }
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
    
});
//dipanggil dari a id retur - di dalam fungsi tampilkankasir()
$(document).on("click","#retur", function(){
    var idt = $(this).attr("alt");
    var nominal = $(this).attr("alt2");
    var idp = $(this).attr("alt3");
    var jp  = $(this).attr("alt4");
    var modeprint = '3';
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayanan/simpan_returpembayaran', //alamat controller yang dituju (di js base url otomatis)
        data: {p:idp, d:nominal}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            cetakvpasien(idp, jp, modeprint);
            dtkasir.ajax.reload(null,false);
        },
        error: function(result) { //jika error
            stopLoading();
            console.log(result.responseText);
        }
    });
});

$(document).on("click","#batalperiksa", function(){
    var idp = $(this).attr("alt");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Batal Pemeriksaan.?',
        buttons: {
            ok: function () {                
                startLoading();
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url + 'cadmission/hapus_data_pendaftaran', //alamat controller yang dituju (di js base url otomatis)
                    data: {i:idp}, //
                    dataType: "JSON", //tipe data yang dikirim
                    success: function(result) { //jika  berhasil
                        stopLoading();
                        dtkasir.ajax.reload(null,false);
                    },
                    error: function(result) { //jika error
                        stopLoading();
                        console.log(result.responseText);
                    }
                });
            },
            batal: function () {               
            }            
        }
    });
    
});

$(document).on("click","#bukapemeriksaan", function(){
    var idp = $(this).attr("alt");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Buka Pemeriksaan.?',
        buttons: {
            ok: function () {                
                startLoading();
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url + 'cadmission/buka_pelayanan', //alamat controller yang dituju (di js base url otomatis)
                    data: {i:idp}, //
                    dataType: "JSON", //tipe data yang dikirim
                    success: function(result) { //jika  berhasil
                        stopLoading();
                        dtkasir.ajax.reload(null,false);
                    },
                    error: function(result) { //jika error
                        stopLoading();
                        console.log(result.responseText);
                    }
                });
            },
            batal: function () {               
            }            
        }
    });
    
});


//menghitung kembalian atau menyimpan
function hitungkembalian(event)
{
    if (event.keyCode == 13)
    {
        simpanpembayaran();   
    }
    else
    {
        var banktujuan = $('#selbanktujuan').val();
        var txttagihan = unconvertToRupiah($('input[name="txttagihan"]').val());
        var txtnominalbayar = unconvertToRupiah($('input[name="txtnominalbayar"]').val());
        var dibayar = parseInt(txtnominalbayar); 
        $('input[name="txtnominalbayar"]').val( ((txtnominalbayar < 1) ? 0 : convertToRupiah(txtnominalbayar) ) );
        $('input[name="txtkembali"]').val(convertToRupiah( (dibayar - txttagihan) ) );
        var txtkekurangan = (txttagihan - dibayar);
        if (txtkekurangan < 0) { txtkekurangan = 0;}
        $('input[name="txtkekurangan"]').val( convertToRupiah(txtkekurangan));
        var total = convertToRupiahBulat( parseInt(txttagihan));
        $('#labeltotaltagihan').text('Rp '+total);
    }
}

function simpanpembayaran()
{
    if($('#txtnominalbayar').val()=='')
    {
        $('#txtnominalbayar').focus();
        alert_empty('Nominal Pembayaran');
        return false;
    }
    else
    {
        var txtkekurangan = unconvertToRupiah($('input[name="txtkekurangan"]').val());
        var txtnominalbayar = unconvertToRupiah($('input[name="txtnominalbayar"]').val());
        var txtkembali = unconvertToRupiah($('input[name="txtkembali"]').val());
        var banktujuan = $('select[name="selbanktujuan"]').val();
        var carabayar  = $('#carabayar').val();
        var penanggung = $('#penanggung').val();
        startLoading();
        $.ajax({
            type: "POST", //tipe pengiriman data
            url: base_url + 'cpelayanan/simpan_pembayaran', //alamat controller yang dituju (di js base url otomatis)
            data: {
                k:txtkembali, 
                s:txtkekurangan, 
                b:txtnominalbayar,
                j:'tagihan', 
                t:$('input[name="altt"]').val(), 
                p:$('input[name="altp"]').val(), 
                modebayar:$('input[name="modebayar"]').val(),
                carabayar:carabayar,
                bt:banktujuan,
                penanggung:penanggung
            }, //
            dataType: "JSON", //tipe data yang dikirim
            success: function(result) { //jika  berhasil
                stopLoading();
                dtkasir.ajax.reload(null,false);
            },
            error: function(result) { //jika error
                stopLoading();
                console.log(result.responseText);
            }
        });
    }
}
$(document).on("click","#cetakvpasienralan", function(){ //cetak kasir pasien rajal
    cetakvpasien($(this).attr("alt"), 'rajal', 3);
});
$(document).on("click","#cetakvjaminan", function(){
    cetakvpasien($(this).attr("alt"), 'rajal', 3,'jaminan');
});
$(document).on("click","#cetakvmandiri", function(){
    cetakvpasien($(this).attr("alt"), 'rajal', 3,'mandiri');
});
$(document).on("click","#cetakvpasienranap", function(){ //cetak kasir pasien rajal
    cetakvpasien($(this).attr("alt"), 'ranap', 4);
});
$(document).on("click","#cetakvpasien", function()//cetak kasir
{
    cetakvpasien($(this).attr("alt"), $(this).attr("alt4"), 1);
});

$(document).on("click","#cetakvpasiengabung", function()//cetak kasir
{
    cetakvpasien($(this).attr("alt"), $(this).attr("alt4"), 2);
});
$(document).on("click","#cetakvklaim", function()//cetak versi klaim
{
    cetakvpasien($(this).attr("alt"), 'rajal',1,'klaim');
});
function cetakvpasien(ip, jenispemeriksaan, mode,modelist='')
{
    startLoading();
    var idp = ip;
    var jenispemeriksaan = jenispemeriksaan;
    var jenisnota = (mode==3) ? 'rajal' : (mode==4) ? 'ranap' : '' ;
    var font = '', fontx = '', paper = '', margin = '', lebar = '', lebargambar;
    var listkasir='',tipesebelum='',subtotal=0,total=0,no=0, selisih = 0;
    $.ajax({
        type: "POST",
        url: base_url + ((jenispemeriksaan == 'rajalnap') ? 'cpelayanan/cetak_versiranap' : 'cpelayanan/cetak_langsung' ),
        data: {p:idp, j:jenisnota,i:modelist, jns:0},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            
            if(jenispemeriksaan == 'rajalnap')
            {
                print_vpasienranap(result);
                
            return true;
            }
            
            lebar = (jenispemeriksaan!='rajal') ? 'width:19cm;' : 'width:6cm;';
            lebargambar = (jenispemeriksaan!='rajal') ? '' : lebar;
            font = lebar + ((jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:small;');
            fontx = (jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:x-small;';
            paper = (jenispemeriksaan!='rajal') ? '20' : '7.6';
            margin = (jenispemeriksaan!='rajal') ? '2' : '0.2';
            listkasir += '<div style="' + lebar + 'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' + lebargambar + '" src="'+base_url+'assets/images/headerkuitansi' + ((jenispemeriksaan!='rajal') ? '' : 'small') + '.jpg" />\n\
                          <table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                if (jenispemeriksaan!='rajal')
                {
                    listkasir += '<tr><td>Nama</td><td colspan="3">'+result['infotagihan'][0].namalengkap+'</td></tr>';
                    listkasir += '<tr><td>Umur</td><td>'+result['infotagihan'][0].usia+'</td><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                    listkasir += '<tr><td>Alamat</td><td colspan="3">'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Ruang/Kelas</td><td colspan="3">'+result['infotagihan'][0].ruangkelas+'</td></tr>';
                    listkasir += '<tr><td>Tanggal Masuk</td><td>'+result['infotagihan'][0].waktumasuk+'</td><td>Tanggal Keluar</td><td>'+result['infotagihan'][0].waktukeluar+'</td></tr>';
                    listkasir += '<tr><td>Dirawat Selama</td><td colspan="3">'+result['infotagihan'][0].dirawatselama+'</td></tr>';
                    listkasir += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">'+result['infotagihan'][0].dokterdbjp+'</td></tr>';
                    listkasir += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">'+result['infotagihan'][0].kelas+'</td></tr>';
                }
                else
                {
                    listkasir += '<tr><td>Nama</td><td>'+result['infotagihan'][0].namalengkap+'</td></tr>';
                    listkasir += '<tr><td>Alamat</td><td>'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                    listkasir += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                }
            }
            listkasir += '</table><table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            listkasir += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>';
            var akomodasi=0;
            for(x in result['detailtagihan'])
            {
                y = x-1;
                if (tipesebelum != result['detailtagihan'][x].jenistarif)
                {
                    // if (tipesebelum != '') listkasir += '<tr><td>'+ ++no +' Akomodasi ' +result['detailtagihan'][y].jenistarif  + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '"></td><td align="right">'+ convertToRupiah(parseInt(akomodasi,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                    if (tipesebelum != '') listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
                    listkasir += '<tr><td colspan="3">'+result['detailtagihan'][x].jenistarif+'</td></tr>';
                    // akomodasi = 0;
                    subtotal  = 0;
                }
                subtotal += (parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                total    += (result['detailtagihan'][x].jenistarif=='Diskon') ? 0 :  (parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)) ;
                if(result['detailtagihan'][x].iskenapajak==1)
                {//jika iskenapajak maka tarif ditampilkan terpisah antara jasamedis dengan akomodasi
                    listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].jasaoperator,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                    listkasir += '<tr><td style="font-size:11px;">'+ ++no +' Akomodasi '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].akomodasi,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                }else{
                    listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama + ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].sewa,10)+parseInt(result['detailtagihan'][x].nonsewa,10)) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                }
                tipesebelum = result['detailtagihan'][x].jenistarif;
            }
            listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiahBulat((total))+'</td></tr>';
            listkasir += '</table><table border="0" style="' + font + 'width="100%";font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            
            //jika mode 1,3 atau biaya admin > 0 tampilkan riwayat transaksi
            if(modelist=='jaminan' || modelist=='mandiri' || modelist=='klaim' )
            {       
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][0].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][0].waktutagih+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][0].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat( total )+'</td></tr>';
                listkasir += '<tr><td>Pembulatan</td><td align="right">'+convertToRupiah( bulatkanLimaRatusan(total) - total )+'</td></tr>';
                listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+convertToRupiah( bulatkanLimaRatusan(total) )+'</td></tr>';                
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(bulatkanLimaRatusan(total))+'</td></tr>' ;
                listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+ ((modelist=='mandiri')? 'tagihan' : result['infotagihan'][0].jenistagihan )+'</td></tr>';
            }
            else
            {
                if (mode == 1 || mode == 3)
                {
                    for(x in  result['infotagihan'])
                    {
                        selisih   += parseInt( ((mode==3) ? total : result['infotagihan'][x].nominal ) , 10) - ( parseInt(result['infotagihan'][x].dibayar, 10) + parseInt(result['infotagihan'][x].potongan,10));
                        listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                        listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                        listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                        listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiah( result['infotagihan'][x].nominal )+'</td></tr>';//convertToRupiah(result['infotagihan'][x].nominal)
                        listkasir += '<tr><td>Potongan</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].potongan)+'</td></tr>';  
                        listkasir += '<tr><td>Pembulatan</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].pembulatan)+'</td></tr>';  
                        listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+ convertToRupiahBulat( parseInt(result['infotagihan'][x].nominal) + parseInt(result['infotagihan'][x].pembulatan) - parseInt(result['infotagihan'][x].potongan ) ) +'</td></tr>';
                            if(result['infotagihan'][x].jenistagihan!='jknnonpbi' ||  result['infotagihan'][x].jenistagihan!='jknpbi' )
                            {
                                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][x].dibayar)+'</td></tr>' ;
                            }
                            listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                            listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );                    
                        listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
                    }
                }
                else
                {
                    var nonota = '', waktutagih='', waktubayar='', nominal = 0, dibayar = 0, potongan = 0,pembulatan=0, jenistagihan, kekurangan=0;
                    for(x in result['infotagihan'])
                    {
                        nonota          += result['infotagihan'][x].idtagihan + ' | ';
                        waktutagih      += result['infotagihan'][x].waktutagih + ' | ';
                        waktubayar      += result['infotagihan'][x].waktubayar + ' | ';
                        nominal         += parseInt(result['infotagihan'][x].nominal, 10);
                        dibayar         += parseInt(result['infotagihan'][x].dibayar, 10);
                        potongan        += parseInt(result['infotagihan'][x].potongan, 10);
                        pembulatan      += parseInt(result['infotagihan'][x].pembulatan, 10);
                        kekurangan      += parseInt(result['infotagihan'][x].kekurangan, 10);
                        jenistagihan    = result['infotagihan'][x].jenistagihan;
                    }
                    nominal = nominal - kekurangan;
                    listkasir += '<tr><td>No Nota</td><td align="right">'+nonota+'</td></tr>';
                    listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+waktutagih+'</td></tr>';
                    listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+waktubayar+'</td></tr>';
                    listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat(nominal)+'</td></tr>';//convertToRupiah(result['infotagihan'][x].nominal)
                    if (potongan > 0)
                    {
                        listkasir += '<tr><td>Potongan</td><td align="right">'+convertToRupiahBulat(potongan)+'</td></tr>';
                        listkasir += '<tr><td>Pembulatan</td><td align="right">'+convertToRupiahBulat(pembulatan)+'</td></tr>';                          
                        listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+convertToRupiahBulat(nominal + pembulatan - potongan)+'</td></tr>';
                    }
                    else
                    {
                        listkasir += '<tr><td>Pembulatan</td><td align="right">'+convertToRupiahBulat(pembulatan)+'</td></tr>'; 
                        listkasir += '<tr><td>Tot.Tagihan</td><td align="right">'+convertToRupiahBulat(nominal+pembulatan)+'</td></tr>';
                    }
                    
                    if (jenispemeriksaan!='rajal')
                    {
                        listkasir += ((result['infotagihan'][x].jenistagihan=='jknnonpbi' ||  result['infotagihan'][x].jenistagihan=='jknpbi' ) ? '' : '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(dibayar)+'</td></tr>') ;

                    }
                    else
                    {
                        listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(dibayar)+'</td></tr>';
                        listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                        listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                    }
                    listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+jenistagihan+'</td></tr>';
                }
            }
            listkasir += ((result['infotagihan'][0].jenispembayaran !=='jknpbi' && result['infotagihan'][0].jenispembayaran != 'jknnonpbi' && result['infotagihan'][0].jenispembayaran !='asuransi') ? '<tr><td>Cara Bayar</td><td align="right">'+result['infotagihan'][0].jenispembayaran+'</td></tr>' : '' ) ;
            listkasir += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            // listkasir += '<tr><td colspan="2" align="center">--Harga obat sudah termasuk ppn--</td></tr>';
            listkasir += '</table></div>';
            fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}


function print_vpasienranap(result)
{
    var listkasir = '';
    listkasir += '<div style="float: none;"><img style="opacity:0.9;margin-bottom:-2px;width:67%;"  src="'+base_url+'assets/images/kwitansibig.svg" />';
    listkasir += '<div style="margin-top:-10px;">__________________________________________________________________________________________________________________________________</div>';
    listkasir += '<div style="margin-top:-14px;margin-bottom:6px;">__________________________________________________________________________________________________________________________________</div>';
    
    listkasir += '<div style="text-align:center;font-size:24px;margin-bottom:5px;">KUITANSI RAWAT INAP</div>';
    listkasir += '<table border="0" style="width:100%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
    listkasir += '<tr><td>Nama Pasien</td><td colspan="3">: '+result['infotagihan'][0].namalengkap+'</td><td>&nbsp;&nbsp; RM</td><td>: '+result['infotagihan'][0].norm+'</td></tr>';
    listkasir += '<tr><td>Umur</td><td colspan="3">: '+result['infotagihan'][0].usia+'</td></tr>';
    listkasir += '<tr><td>Alamat</td><td colspan="4" style="font-size:13px;">: '+result['infotagihan'][0].alamat+'</td></tr>';
    listkasir += '<tr><td>Ruang/Kelas</td><td colspan="3">: '+result['infotagihan'][0].ruangkelas+'</td></tr>';
    listkasir += '<tr><td>Tanggal Masuk</td><td colspan="3">: '+result['infotagihan'][0].waktumasuk+'</td><td>&nbsp;&nbsp; Tanggal Keluar</td><td>: '+result['infotagihan'][0].waktukeluar+'</td></tr>';
    listkasir += '<tr><td>Dirawat Selama</td><td colspan="3">: '+result['infotagihan'][0].dirawatselama+' hari</td></tr>';
    listkasir += '<tr><td>Dokter Penanggungjawab</td><td colspan="3">: '+result['infotagihan'][0].dokterdbjp+'</td></tr>';
    listkasir += '<tr><td>Kelas Perawatan</td><td colspan="3">: '+result['infotagihan'][0].kelas+'</td></tr>';
    listkasir += '</table>';
    listkasir += '<div>__________________________________________________________________________________________________________________________________</div>';
    
    listkasir += '<table border="0" style="width:100%;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
    var no = 0;
    var total = 0;
    var jenistarif = '';
    var subtotal = 0;
    
    for(var x in result.listtagihan)
    {
        if(jenistarif != '' && jenistarif != result.listtagihan[x].jenistarif)
        {
            listkasir += '<tr><td>'+ ++no +'</td><td>'+jenistarif+'</td><td style="text-align:right">Rp </td><td style="text-align:left">'+convertToRupiah(subtotal)+'</td></tr>';
            subtotal = 0;
        }
        jenistarif = result.listtagihan[x].jenistarif;
        subtotal += parseInt(result.listtagihan[x].nominal);        
        total += parseInt(result.listtagihan[x].nominal);
    }    
    listkasir += '<tr><td>'+ ++no +'</td><td>'+jenistarif+'</td><td style="text-align:right">Rp </td><td style="text-align:left">'+convertToRupiah(subtotal)+'</td></tr>';
    
    listkasir += '<tr><td colspan="4">__________________________________________________________________________________________________________________________________</td></tr>';
    listkasir += '<tr><td></td><td style="text-align:right">Total Biaya</td><td style="text-align:right">Rp </td><td style="text-align:left">'+convertToRupiah(total)+'</td></tr>';
    
    listkasir += '<tr><td colspan="4">&nbsp;</td></tr>';
    listkasir += '<tr><td></td><td colspan="2"><i>Terbilang : '+Terbilang(total)+' Rupiah</i></td><td></td></tr>';
    
    listkasir += '<tr><td colspan="4">&nbsp;</td></tr>';
    listkasir += '<tr><td colspan="4">&nbsp;</td></tr>';
    
    listkasir += '<tr><td colspan="2"></td><td style="text-align:right;">'+result.ttd+'</td><td></td></tr>';
    listkasir += '<tr><td colspan="2"></td><td style="text-align:right; padding-right:20px;">Bagian Keuangan</td><td></td></tr>';
    
    listkasir += '</table>'; 
    
    
    listkasir += '</div>';
    fungsi_cetaktopdf(listkasir,'<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>');
}

function print_vpasienralan()
{
    fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
}

$(document).on("click","#cetakvjenistarif", function()//cetak kasir
{
    var idp = $(this).attr("alt");
    var jenispemeriksaan = $(this).attr("alt4");
    var font = '', fontx = '', paper = '', margin = '', lebar = '', lebargambar;
//    var idt = $(this).attr("alt2");
    var listkasir='',tipesebelum='',subtotal=0,total=0,totalsewa=0,no=0;
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_langsung',
        data: {p:idp},
        dataType: "JSON",
        success: function(result) {
//            console.log(result);
            lebar = (jenispemeriksaan!='rajal') ? 'width:19cm;' : 'width:6cm;';
            lebargambar = (jenispemeriksaan!='rajal') ? '' : lebar;
            font = lebar + ((jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:small;');
            fontx = (jenispemeriksaan!='rajal') ? 'font-size:normal;' : 'font-size:x-small;';
            paper = (jenispemeriksaan!='rajal') ? '20' : '7.6';
            margin = (jenispemeriksaan!='rajal') ? '2' : '0.2';
            listkasir += '<div style="' + lebar + 'float: none;"><img style="opacity:0.5;filter:alpha(opacity=50);' + lebargambar + '" src="'+base_url+'assets/images/headerkuitansi' + ((jenispemeriksaan!='rajal') ? '' : 'small') + '.jpg" />\n\
                          <table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                if (jenispemeriksaan!='rajal')
                {
                    listkasir += '<tr><td>Nama</td><td colspan="3">'+result['infotagihan'][0].namalengkap+'</td></tr>';
                    listkasir += '<tr><td>Umur</td><td>'+result['infotagihan'][0].usia+'</td><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                    listkasir += '<tr><td>Alamat</td><td colspan="3">'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Ruang/Kelas</td><td colspan="3">'+result['infotagihan'][0].ruangkelas+'</td></tr>';
                    listkasir += '<tr><td>Tanggal Masuk</td><td>'+result['infotagihan'][0].waktumasuk+'</td><td>Tanggal Keluar</td><td>'+result['infotagihan'][0].waktukeluar+'</td></tr>';
                    listkasir += '<tr><td>Dirawat Selama</td><td colspan="3">'+result['infotagihan'][0].dirawatselama+'</td></tr>';
                    listkasir += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">'+result['infotagihan'][0].dokterdbjp+'</td></tr>';
                    listkasir += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">'+result['infotagihan'][0].kelas+'</td></tr>';
                }
                else
                {
                    listkasir += '<tr><td>Nama</td><td>'+result['infotagihan'][0].namalengkap+'<br/>'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                    listkasir += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                }
            }
            listkasir += '</table><table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            listkasir += '<tr><td>No/Item</td><td align="right" >Jml</td><td align="right">Nominal</td></tr>';
            for(x in result['detailtagihan'])
            {
                if (tipesebelum != result['detailtagihan'][x].jenistarif)
                {
                    if (tipesebelum != '') listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
                    listkasir += '<tr><td colspan="3">'+result['detailtagihan'][x].jenistarif+'</td></tr>';
                    subtotal  = 0;
                }
                totalsewa+= parseInt(result['detailtagihan'][x].sewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                subtotal += parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                total    += (result['detailtagihan'][x].jenistarif=='Diskon') ? 0 : parseInt(result['detailtagihan'][x].nonsewa,10) * parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10));
                listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama+ ((parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)>1)?' ['+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10)+']'):'')+'</td><td align="right" style="font-size:' + fontx + '">'+ hapus3digitkoma(result['detailtagihan'][x].jumlah) +'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].nonsewa,10))* parseFloat(result['detailtagihan'][x].jumlah) * (parseInt(result['detailtagihan'][x].kekuatan,10)/parseInt(result['detailtagihan'][x].kekuatanbarang,10)))+'</td></tr>'; 
                tipesebelum = result['detailtagihan'][x].jenistarif;
            }
            listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(subtotal)+'</td></tr>';
            if (totalsewa > 0)
            {
                listkasir += '<tr><td colspan="3">Sewa Alat</td></tr>';
                listkasir += '<tr><td align="right" colspan="2">:: Subtotal ::</td><td align="left">'+convertToRupiah(totalsewa)+'</td></tr>';
                total     += totalsewa;
                
            }
            listkasir += '<tr><td align="right" colspan="2">== Total ==</td><td align="right">'+convertToRupiahBulat(bulatkanRatusan(total))+'</td></tr>';
            listkasir += '</table><table border="0" style="' + font + 'font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            for(x in result['infotagihan'])
            {
                listkasir += '<tr><td>No Nota</td><td align="right">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td align="right">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td align="right">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                listkasir += '<tr><td>Nominal</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].nominal)+'</td></tr>';
                listkasir += '<tr><td>Potongan</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].potongan)+'</td></tr>';
                if (jenispemeriksaan!='rajal')
                {
                    
                    listkasir += '<tr><td>Biaya</td><td align="right">'+convertToRupiahBulat(result['infotagihan'][x].nominal - result['infotagihan'][x].potongan)+'</td></tr>';
                }
                listkasir += '<tr><td>Dibayar</td><td align="right">'+convertToRupiah(result['infotagihan'][x].dibayar)+'</td></tr>';
                listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                listkasir += '<tr><td>Jenis Tagihan</td><td align="right">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
            }
            listkasir += '<tr><td colspan="2" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            // listkasir += '<tr><td colspan="2" align="center">--Harga obat sudah termasuk ppn--</td></tr>';
            listkasir += '</table></div>';
            fungsi_cetaktopdf(listkasir,'<style>@page {size:' + paper + 'cm 100%;margin:' + margin + 'cm;}</style>');
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
});

$(document).on("click","#cetakvsemua", function()//cetak kasir
{
    var jenispemeriksaan = $(this).attr("alt4");
    var idp = $(this).attr("alt");
//    var idt = $(this).attr("alt2");
    var listkasir='',icdsebelum='', tipesebelum='',total=0,totalpotongan=0,no=0,selisih=0;
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_langsung_lengkap',
        data: {p:idp },
        dataType: "JSON",
        success: function(result) {
            listkasir += '<div style="width:29.7cm;float: none;"><table border="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                if (jenispemeriksaan!='rajal')
                {
                    listkasir += '<tr><td>Nama</td><td colspan="3">'+result['infotagihan'][0].namalengkap+'</td></tr>';
                    listkasir += '<tr><td>Umur</td><td>'+result['infotagihan'][0].usia+'</td><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                    listkasir += '<tr><td>Alamat</td><td colspan="3">'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Ruang/Kelas</td><td colspan="3">'+result['infotagihan'][0].ruangkelas+'</td></tr>';
                    listkasir += '<tr><td>Tanggal Masuk</td><td>'+result['infotagihan'][0].waktumasuk+'</td><td>Tanggal Keluar</td><td>'+result['infotagihan'][0].waktukeluar+'</td></tr>';
                    listkasir += '<tr><td>Dirawat Selama</td><td colspan="3">'+result['infotagihan'][0].dirawatselama+'</td></tr>';
                    listkasir += '<tr><td>Dokter Penanggung Jawab</td><td colspan="3">'+result['infotagihan'][0].dokterdbjp+'</td></tr>';
                    listkasir += '<tr><td>Kelas Perawatan/Kelas Jaminan</td><td colspan="3">'+result['infotagihan'][0].kelas+'</td></tr>';
                }
                else
                {
                    listkasir += '<tr><td>Nama</td><td>'+result['infotagihan'][0].namalengkap+'<br/>'+result['infotagihan'][0].alamat+'</td></tr>';
                    listkasir += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                    listkasir += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
                }
            }
            for(x in result['detailtagihan'])
            {
                if (icdsebelum != result['detailtagihan'][x].jenisicd)
                {
//                    if (tipesebelum != '') listkasir += '<tr><td align="right">:: Subtotal ::</td><td align="left">'+subtotal+'</td></tr>';
                    listkasir += '<tr><td colspan="13" style="font-size:small;"><strong>'+result['detailtagihan'][x].jenisicd+'</strong></td></tr>';
//                    subtotal  = 0;
                }
                if (tipesebelum != result['detailtagihan'][x].jenistarif)
                {
//                    if (tipesebelum != '') listkasir += '<tr><td align="right">:: Subtotal ::</td><td align="left">'+subtotal+'</td></tr>';
                    listkasir += '<tr><td colspan="13" style="font-size:small;"><u>'+result['detailtagihan'][x].jenistarif+'</u></td></tr>';
                    listkasir += '<tr><td>Nama</td><td>Nilai</td><td align="right">Jasa Op.</td><td align="right">Nakes</td><td align="right">Jasa RS</td><td align="right">BHP</td><td align="right">Akomodasi</td><td align="right">Margin</td><td align="right">Sewa</td><td align="right">Kekuatan</td><td align="right">Resep</td><td align="right">Jumlah</td><td align="right">Harga</td><td align="right">Pot.</td><td align="right">Total</td></tr>';
//                    subtotal  = 0;
                    no = 0;
                }
//                totalsewa+= parseInt(result['detailtagihan'][x].total,10);
//                subtotal += parseInt(result['detailtagihan'][x].nonsewa,10);
//                total    += parseInt(result['detailtagihan'][x].nonsewa,10);
                if (result['detailtagihan'][x].idgroup != 0 && result['detailtagihan'][x].idpaketpemeriksaan != 0 && result['detailtagihan'][x].icd == null && result['detailtagihan'][x].idpaketpemeriksaan != result['detailtagihan'][x].idgrup)
                    listkasir += '<tr><td colspan="13">'+result['detailtagihan'][x].nama+'</td></tr>';
                else
                    listkasir += '<tr><td>'+ ++no +' '+result['detailtagihan'][x].nama+'</td><td>'+result['detailtagihan'][x].nilai+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].jasaoperator,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].nakes,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].jasars,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].bhp,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].akomodasi,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].margin,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].sewa,10)))+'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].kekuatan,10))+'/'+convertToRupiah(parseInt(result['detailtagihan'][x].kekuatanbarang,10))+'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].jumlahdiresepkan,10))+'</td><td align="right">'+ convertToRupiah(parseInt(result['detailtagihan'][x].jumlahpemakaian,10))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].harga,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].potongan,10)))+'</td><td align="right">'+ convertToRupiah((parseInt(result['detailtagihan'][x].total-result['detailtagihan'][x].potongan,10)))+'</td></tr>';
                tipesebelum     = result['detailtagihan'][x].jenistarif;
                icdsebelum      = result['detailtagihan'][x].jenisicd;
                total           += (parseInt(result['detailtagihan'][x].total,10));
                totalpotongan   += (parseInt(result['detailtagihan'][x].potongan,10));
            }
            listkasir += '<tr><td align="right" colspan="11">== Total ==</td><td align="right">'+convertToRupiahBulat(bulatkanRatusan(total-totalpotongan))+'</td></tr>';
            for(x in result['infotagihan'])
            {
                selisih   += parseInt(result['infotagihan'][x].nominal, 10) - parseInt(result['infotagihan'][x].potongan, 10) - parseInt(result['infotagihan'][x].dibayar, 10);
                listkasir += '<tr><td>No Nota</td><td colspan="2">'+result['infotagihan'][x].idtagihan+'</td></tr>';
                listkasir += '<tr><td>Waktu Tagih</td><td colspan="2">'+result['infotagihan'][x].waktutagih+'</td></tr>';
                listkasir += '<tr><td>Waktu Bayar</td><td colspan="2">'+result['infotagihan'][x].waktubayar+'</td></tr>';
                
                if (jenispemeriksaan!='rajal')
                {
                    listkasir += '<tr><td>Nominal</td><td align="right" colspan="2">'+convertToRupiah((result['infotagihan'][x].nominal))+'</td></tr>';
                    listkasir += '<tr><td>Potongan</td><td align="right" colspan="2">'+convertToRupiah(result['infotagihan'][x].potongan)+'</td></tr>';
                    listkasir += '<tr><td>Biaya</td><td align="right" colspan="2">'+convertToRupiahBulat(result['infotagihan'][x].nominal - result['infotagihan'][x].potongan)+'</td></tr>';
                }
                else
                {
                    listkasir += '<tr><td>Nominal</td><td align="right" colspan="2">'+convertToRupiahBulat((result['infotagihan'][x].nominal))+'</td></tr>';
                }
                listkasir += '<tr><td>Dibayar</td><td align="right" colspan="2">'+convertToRupiah(result['infotagihan'][x].dibayar)+'</td></tr>';
                listkasir += ((result['infotagihan'][x].kekurangan >0 ) ? '<tr><td>Kekurangan</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kekurangan)+'</td></tr>' : '' );
                listkasir += ((result['infotagihan'][x].kembalian >0 ) ? '<tr><td>Kembalian</td><td align="right">'+convertToRupiah(result['infotagihan'][x].kembalian)+'</td></tr>' : '' );
                listkasir += '<tr><td>Jenis Tagihan</td><td align="right" colspan="2">'+result['infotagihan'][x].jenistagihan+'</td></tr>';
            }
            listkasir += '<tr><td colspan="12" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            // listkasir += '<tr><td colspan="2" align="center">--Harga obat sudah termasuk ppn--</td></tr>';
            listkasir += '</table></div>';
            fungsi_cetaktopdf(listkasir,'<style>@page {size:29.7cm 21cm;margin:0.2cm;}</style>');
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
});

$(document).on("click","#cetakrekapinap", function()//cetak kasir
{
    var idin = $(this).attr("alt5");
    var idp  = $(this).attr("alt");
    var html = '', grupsebelum = '';
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_rekap_inap',
        data: {in:idin, idp:idp},
        dataType: "JSON",
        success: function(result) {
            html += '<div style="width:29.7cm;float: none;">';
            html += '<table cellpadding="0" cellspacing="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            if (result['infotagihan'] != null)
            {
                html += '<tr><td width="200px">Nama</td><td width="500px">'+result['infotagihan'][0].namalengkap+'<br/>'+result['infotagihan'][0].alamat+'</td></tr>';
                html += '<tr><td>Penanggung</td><td>'+result['infotagihan'][0].penanggung+'</td></tr>';
                html += '<tr><td>No RM</td><td>'+result['infotagihan'][0].norm+'</td></tr>';
            }
            html += '</table>';
            html += '<table border="1" cellpadding="0" cellspacing="0" style="width:28cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            html += '<tr><th></th>';
            for(k in result['detailranap']['header'])
            {
                html += '<th>'+ result['detailranap']['header'][k] +'</th>';
            }
            html += '</tr>';
            
            for (g in result['detailranap']['grup'])
            {
                html += '<tr><td>' + g.toUpperCase() + '</td></tr><tr>';
                for(b in result['detailranap']['grup'][g])
                {
                    html += '<tr><td>' + result['detailranap']['id'][b] + '</td>';
                    for(k in result['detailranap']['header'])
                    {
                        html += '<td>' + result['detailranap']['data'][g][b][k] + '</td>';
                    }
                    html += '</tr>';
                }
            }
            
            html += '<tr><td colspan="12" align="center">dicetak oleh '+ sessionStorage.user +', pada ' + (new Date().toLocaleString("id")) + '</td></tr>';
            // listkasir += '<tr><td colspan="2" align="center">--Harga obat sudah termasuk ppn--</td></tr>';
            html += '</table></div>';
            fungsi_cetaktopdf(html,'<style>@page {size:29.7cm 21cm;margin:0.2cm;}</style>');
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
});

$(document).on("click","#pemeriksaanklinikSelesai", function(){ //pemeriksaan selesai
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    var norm    = $(this).attr("norm");
    var nama    = $(this).attr('nama');
    var content = '<p><label class="label label-primary"><b>Apakah Pemeriksaan Sudah Selesai.?</b></label><br> Nama Pasien : '+nama+'<br>'+'Norm : '+norm+' <br> <small class="text text-red">*Pemeriksaan yang sudah diselesikan tidak dapat dibuka kembali.</small></p>';
    fungsi_pemeriksaanklinikUbahStatus('selesai',content,id,nobaris);
});
$(document).on("click","#pemeriksaanklinikBatalselesai", function(){ // pemeriksaan batal selesai
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanklinikUbahStatus('sedang ditangani','Batal Selesai?',id,nobaris);
});

function fungsi_pemeriksaanklinikUbahStatus(status,content,id,nobaris) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: content,
        buttons: {
            ok: function () {                
                $.ajax({ 
                    url: base_url +'cpelayanan/pemeriksaanklinik_batalselesai',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                        dtkasir.ajax.reload(null,false);
                        notif(result.status, result.message);
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            batal: function () {               
            }            
        }
    });
}
// GENERATE TO PDF
function generatetopdf_versipasien(value){window.open(base_url+'creport/download_pdfkasir/'+value);}
// GENERATE TO EXCEL
$(document).on("click","#generatetoexcek_versipasien", function(){ //pemeriksaan selesai
    var value = $(this).attr("judul");
    console.log(value);
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' kasir_versipasien';
})


//Batal Lunas
$(document).on('click','#batallunas',function(){
    var idt = $(this).attr('idt');
    var nominal = $(this).attr('nominal');
    var waktu = $(this).attr('waktu');
    var jenistagihan = $(this).attr('jenistagihan');
    $.confirm({
        title: 'Konfirmasi Batal Pelunasan',
        content: '<b>Detail Tagihan</b>\n\
        <br> Waktu Tagih : '+waktu
        +' <br> Nominal  : '+nominal
        +' <br> JenisTagihan : '+jenistagihan,
        closeIcon: true,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Konfirmasi',
                btnClass: 'btn-warning',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cpelayanan/batal_pelunasan', //alamat controller yang dituju (di js base url otomatis)
                        data: {idt:idt}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            dtkasir.ajax.reload(null,false);
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
});

$(document).on('click','#pemanggilankasir',function(){
    var idp = $(this).attr('idp');
    var idper = $(this).attr('idper');
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cantrian/buatantriankasir', //alamat controller yang dituju (di js base url otomatis)
        data: {idp:idp,idper:idper}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status, result.message);
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    }); 
});

$(document).on("click","#batalselesai", function(){
    var idp = $(this).attr("idp");
    var idt = $(this).attr("idt");
    var rm  = $(this).attr("norm");
    var nama  = $(this).attr("nama");
    $.confirm({
        title: 'Batal Selesai',
        content: 'Batal Penyelesaian Pasien BPJS.? <br> Nama: '+nama+' <br> NORM: '+rm,
        closeIcon: true,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'konfirmasi',
                btnClass: 'btn-primary',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cpelayanan/batalselesai', //alamat controller yang dituju (di js base url otomatis)
                        data: {idpendaftaran:idp,idtagihan:idt}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                                dtkasir.ajax.reload(null,false);
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
    
});

$(document).on('click','#resetpembayaran',function(){
    var idpendaftaran = $(this).attr('idp');
    var modalSize = 'md';
    var modalTitle = 'Reset Pembayaran.';
    var modalContent = '<form action="" id="formReset">' +
                            '<input type="hidden" name="idpendaftaran" value="'+idpendaftaran+'" />'+
                            '<div class="form-group">' +
                                '<label>Alasan </label>' +
                                '<textarea name="reset_alasan" id="reset_alasan" class="form-control" rows="3" word-limit="true" min-words="5"></textarea>' +
                            '</div>'+
                            '<div class="form-group">' +
                                '<label>Petugas </label>' +
                                '<p>'+sessionStorage.user+'</p>' +
                            '</div>'+
                            '<div class="form-group bg bg-red" style="padding:4px;">' +
                                '<label>Peringatan..! </label>' +
                                '<p class="bg bg-red">\n\
                                <ol>'+
                                    '<li>Aktivitas <i>reset</i> pembayaran akan disimpan oleh sistem.</li>'+
                                    '<li>Kasir harus menyelesaikan ulang pelunasan biaya.</li>'+
                                '</ol>'+
                                '</p>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    
                    //--hitung jumlah kata
                    // Create array of words, skipping the blanks
                    var removedBlanks = [];
                    removedBlanks = $('#reset_alasan').val().split(/\s+/).filter(Boolean);
                    // Get word count
                    var wordCount = removedBlanks.length;
                    //--end
                    
                    if($("#reset_alasan").val() == '')
                    {
                        alert_empty('Alasan.');
                        return false;
                    }
                    else if(wordCount < 3)
                    {
                        $.alert({icon: 'fa  fa-warning',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Warning!',content:'Alasan minimal 3 kata.' });
                        return false;
                    }
                    else
                    {
                        startLoading();
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url + 'cpelayanan/resetpembayaran_ralan', //alamat controller yang dituju (di js base url otomatis)
                            data:$('#formReset').serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                stopLoading();
                                notif(result.status,result.message);
                                dtkasir.ajax.reload(null,false);
                            },
                            error: function(result) { //jika error
                                stopLoading();
                                console.log(result.responseText);
                            }
                        });
                    }
                    
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});



$(document).on("click","#cetakvdetail", function(){
    var idp = $(this).attr('alt');
    $.ajax({
        type: "POST",
        url: base_url + 'cpelayanan/cetak_versidetail',
        data: {
            p:idp, 
            j:'detail',
            i:'kasir', 
            jns:0
        },
        dataType: "JSON",
        success: function(result) {
            stopLoading();

            var listkasir = '';
            listkasir += '<div style="float: none;">';
            listkasir += '<table border="0" style="font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            listkasir += '<tr><td>NORM</td><td colspan="3">: '+result['infotagihan'][0].norm+'</td></tr>';
            listkasir += '<tr><td>Nama</td><td colspan="3">: '+result['infotagihan'][0].namalengkap+'</td></tr>';
            listkasir += '</table>';
            listkasir += '<table border="0" cellspacing="0" cellpadding="0" style="width:100%;font-family: padding:1px;"Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            
            var jenistarif = '';
            var no = 0;
            var bt = 'border-bottom:1px solid #000;';
            var bb = 'border-bottom:1px solid #000;';
            var jenistanggal = '';
            var total = 0;
            for(var x in result.detail)
            {
                if(jenistarif != result.detail[x].jenistarif || jenistarif == '')
                {
                    
                    listkasir += '<tr><td colspan="5">&nbsp;</td>';
                    listkasir += '<tr><td colspan="5" style="text-align:left;font-weight:bold;background-color:#eee;border-bottom:1px solid #000;"> Rekap '+result.detail[x].jenistarif +'</td></tr>'                    
                    
                    
                }
                var jnstgl = result.detail[x].jenistarif+''+result.detail[x].tanggal;
                if(jenistanggal != jnstgl || jenistanggal == '')
                {
                    if(jenistarif == result.detail[x].jenistarif)
                    {
                        listkasir += '<tr><td colspan="5">&nbsp;</td>';
                    }
                    
                    listkasir += '<tr>\n\
                    <td style="'+bt+'" width="100px;"> <i>'+result.detail[x].tanggal+'</i></td>\n\
                    <td style="'+bt+'"> Nama</td>\n\
                    <td style="'+bt+'"> jumlah</td>\n\
                    <td style="'+bt+'"> @harga</td>\n\
                    <td style="'+bt+'"> Subtotal</td></tr>';                  
                    no=0;
                }
                
                
                
                listkasir += '<tr>\n\
                <td> </td>\n\
                <td> '+ ++no +'. '+result.detail[x].nama +'</td>\n\
                <td> '+result.detail[x].jumlah +'</td>\n\
                <td> '+convertToRupiah(result.detail[x].subtotal) +'</td>\n\
                <td> '+convertToRupiah(parseInt(result.detail[x].subtotal) * parseFloat(result.detail[x].jumlah),'.') +'</td>\n\
                </tr>';
                total += parseInt(result.detail[x].subtotal) * parseFloat(result.detail[x].jumlah);
                jenistanggal = result.detail[x].jenistarif+''+result.detail[x].tanggal;
                jenistarif = result.detail  [x].jenistarif;
            }
            
            listkasir += '<tr><td colspan="5">&nbsp;</td>';
            listkasir += '<tr><td colspan="5">&nbsp;</td>';
            
            listkasir += '<tr>\n\
                    <td colspan="4" style="'+bt+'"> Total Biaya</td>\n\
                    <td style="'+bt+'"> '+convertToRupiah(total,'.')+'</td></tr>';
            listkasir += '</table>';

            listkasir += '</div>';
            fungsi_cetaktopdf(listkasir,'<style>@page {size: 20 cm 100%;margin:0.1 cm;}</style>');
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
});



function Terbilang(angka)
{
    var x = parseInt(angka);
    var abil = ["", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"];
    if (x < parseInt(12))
    {
        return " " + abil[x];
    }
    else if (x < parseInt(20))
    {
        return Terbilang(x - parseInt(10)) + " Belas";
    }
    else if (x < parseInt(100))
    {
        return Terbilang(x / parseInt(10)) + " Puluh" + Terbilang(x % parseInt(10));
    }
    else if (x < parseInt(200))
    {
        return " Seratus" + Terbilang(x - parseInt(100));
    }   
    else if (x < parseInt(1000))
    {
        return Terbilang(x / parseInt(100)) + " Ratus" + Terbilang(x % parseInt(100));
    }
    else if (x < parseInt(2000))
    {
        return " Seribu" + Terbilang(x - parseInt(1000));
    }
    else if (x < parseInt(1000000))
    {
        return Terbilang(x / parseInt(1000)) + " Ribu" + Terbilang(x % parseInt(1000));
    }
    else if (x < parseInt(1000000000))
    {
        return Terbilang(x / parseInt(1000000)) + " Juta" + Terbilang(x % parseInt(1000000));
    }
}