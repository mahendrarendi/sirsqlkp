$(function () {
    // $('#example1').DataTable()
    // $('#example2').DataTable({
    //   'paging'      : true,
    //   'lengthChange': false,
    //   'searching'   : false,
    //   'ordering'    : true,
    //   'info'        : true,
    //   'autoWidth'   : false
    // });
    select2();
  })

function select2()
{
  var hal=1;
  $("#testSelect").select2({
      ajax: { 
       url: base_url+"cmasterdata/mastertarif_listicd",
       type: "post",
       dataType: 'json',
       delay: 250,
       data: function (params) {
        ((params.term=='')?hal+=1:hal=1);
        return {
          searchTerm: params.term, // search term
          page: hal,
        };
       },
       processResults: function (response) {
         return {
            results: response,
         };

       },
       cache: true
      }
     });
}

function simpan(){
  // if($('#namaBarang').val()==''){
  //   $.alert("Nama barang harap diisi..!");
  // }else{
    $('#sampleForm').submit();
  // }
}

// ///////// set u inventory item list
function sample_funtion(value){

 
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'delete_page', 
                    type : "post",      
                    dataType : "json",                                               
                    // data:{id:id},
                    error: function(result){                    
                        // console.log(result.responseText);
                        $.alert('Server Error: delete failed');
                        return false;
                    },
                    success: function(result) {     
                      console.log(result);                                                                    
                        if(result.sts=='1'){
                              window.location = 'page'; 
                        }else{
                            $.alert(result.msg);
                        }                       
                       
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}