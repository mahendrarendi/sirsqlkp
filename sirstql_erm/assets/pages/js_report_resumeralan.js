function cetak_resumralan(idp)
{   
    // var norm = (($(this).attr('norm'))==undefined? localStorage.getItem('norm') : $(this).attr('norm') );
    $.ajax({
        url:base_url+"cadmission/resumerawatjalanjson",
        dataType:"JSON",
        type:"POST",
        data:{idp:idp},
        success:function(result){
        var periksa='';
        for(var x in result.dtperiksa)
        {
            var diagnosa='', penunjang='', terapi='', tindakan='';
            diagnosa += result.dtperiksa[x].anamnesa+'<br>';
            for( var a in result.dtdiagnosis[x]){ diagnosa += result.dtdiagnosis[x][a].hasil_diagnosis+'<br>';}
            for( var b in result.tindakan[x]){tindakan += result.tindakan[x][b].hasil_diagnosis+'<br>';}
            for( var c in result.obat[x]){tindakan += result.obat[x][c].obat+'<br>';}
            for( var y in result.vitalsign[x]){ penunjang += result.vitalsign[x][y].hasil_diagnosis+'<br>';}
            for( var d in result.penunjang[x]){penunjang += result.penunjang[x][d].hasil_diagnosis+'<br>';}
            terapi += tindakan;
            periksa+='<tr><td><p class="main">Tanggal Periksa</p></td><td><p class="main">'+result.dtperiksa[x].tanggal+'</p></td></tr>'
                   +'<tr><td><p class="main">Diagnosa</p></td><td><p class="main">'+diagnosa+'</p></td></tr>'
                   +'<tr><td><p class="main">Pemeriksaan Penunjang</p></td><td><p class="main">'+penunjang+'</p></td></tr>'
                   +'<tr><td><p class="main">Terapi</p></td><td><p class="main">'+terapi+'</p></td></tr>'
                   +'<tr><td><p class="main">Dokter</p></td><td><p class="main">'+result.dtperiksa[x].namadokter+'<br><br><br></p></td></tr>';
        }
        var style = '<style rel="stylesheet" media="print">@media print{.main{padding:4px;} }</style>';
        var print ='<div style="padding:8px; font-weight:100px;"><table border="1" cellspacing="0" cellpadding="0"><tr><td colspan="2"><img style="width:300px;" src="'+base_url+'/assets/images/headerlembarrm.svg" ></td>'
                +'</tr><tr><td colspan="2"><table><tr><td>No. RM</td><td>: '+result.dtpasien['norm']+' </td></tr><tr><td>Nama</td><td>: '+result.dtpasien['namalengkap']+' </td></tr><tr><td>Tanggal lahir</td><td>: '+result.dtpasien['tanggallahir']+' </td></tr></table></td></tr>'
                +'<tr><td colspan="2"><span class="main" style="font-size:2ppx; opacity:2;">RIWAYAT KUNJUNGAN RAWAT JALAN</span></td></tr>'
                + periksa +'</table></div>';
            fungsi_cetaktopdf(print,style);
        },
        error:function(){
        }
      });
}