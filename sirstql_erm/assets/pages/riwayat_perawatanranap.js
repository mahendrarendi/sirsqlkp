function getdata_riwayatperawatanranap(idinap,idpendaftaran,norm)
{
  startLoading();
  $.ajax({
      url:base_url+"cpelayananranap/getdata_riwayatperawatanranap",
      dataType:"JSON",
      type:"POST",
      data:{
          idinap:idinap,
          idpendaftaran:idpendaftaran,
          norm:norm,
          mode:''
      },
      success:function(result){
          stopLoading();
          lembarriwayatperawatanpasien(result,idinap,idpendaftaran,norm);
      },
      error:function(){
          stopLoading()
          
      }
  });
}

$(document).on('click','#riwayatperawatan',function(){
    var idinap = $(this).attr('idinap');
    var idpendaftaran = $(this).attr('idpendaftaran');
    var norm = $(this).attr('norm');
    getdata_riwayatperawatanranap(idinap,idpendaftaran,norm);
});

// mahmud, clear
function lembarriwayatperawatanpasien(result,idinap,idpendaftaran,norm)
{
  var penanggung = result.penanggung;
//  var periksa = result.dtperiksa;
//  var planjutan = result.planjutan;
//  var anamnesis = result.dtanamnesis;
//  var diagnosis = result.dtdiagnosis;
//  var obat = result.dtobat;
//  var detailperiksa = result.detailperiksa;
  var dtranap = result.dtranap;
//  var rencanamedisranap = result.rencanamedisranap;
//  var rencana_anamnesa  = result.rencana_anamnesa;
//  var rencana_lanjutan  = result.rencana_lanjutan;
//  var rencana_tindakan  = result.rencana_tindakan;
//  var rencanabp         = result.rencanabp;
	var result = result.dtpasien;
	var split='&nbsp;&nbsp;&nbsp;';
	var style = '<style rel="stylesheet" media="print"> @media print {.borderleft{border-left:1px solid #2D2D2D;border-top:1px solid #2D2D2D;} .border{border-right:1px solid #2D2D2D; border-left:1px solid #2D2D2D; border-top:1px solid #2D2D2D; padding:4px; margin:0;} .jarak{padding-top:8px;}  } </style>';
	
        var printContents = '<div style="padding:8px;">'+
	'<img src="'+base_url+'/assets/images/headerlembarrm.svg" >'+
	'<hr><span style="font-size:30px;">Lembar Rekam Medik &nbsp;&nbsp;&nbsp;&nbsp;  No. RM.: '+norm+'</span><hr>'+
	'<table>'+
		'<tr><td>NAMA LENGKAP '+ split +'</td><td>'+ result.namalengkap +'</td> <td style="padding-right:100px;">&nbsp;&nbsp;</td><td class="bg bg-info">&nbsp; Waktu Masuk Ranap'+ split +'</td><td>: '+ dtranap.waktumasuk +'</td></tr>'+
		'<tr><td>JENIS KELAMIN '+ split +'</td><td>'+ result.jeniskelamin  +'</td> <td style="padding-right:100px;">&nbsp;&nbsp;</td><td class="bg bg-info">&nbsp; Kelas Perawatan '+ split +'</td><td>: '+ dtranap.kelasperawatan +'</td></tr>'+
		'<tr><td>TGL LAHIR '+ split +'</td><td>'+ result.tanggallahir  +'</td> <td style="padding-right:100px;">&nbsp;&nbsp;</td><td class="bg bg-info">&nbsp; Kelas Jaminan '+ split +'</td><td>: '+ dtranap.kelasjaminan +'</td></tr>'+
		'<tr><td>AGAMA '+ split +'</td><td>'+ result.agama  +'</td> <td style="padding-right:100px;">&nbsp;&nbsp;</td><td class="bg bg-info">&nbsp; Kamar '+ split +'</td><td>: '+ dtranap.kamar +'</td></tr>'+
		'<tr><td>PEKERJAAN '+ split +'</td><td>'+ result.namapekerjaan+'</td> <td style="padding-right:100px;">&nbsp;&nbsp;</td><td class="bg bg-info">&nbsp; Dokter DPJP '+ split +'</td><td>: '+ dtranap.namadokter +'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamat +'</td></tr>'+
		'<tr><td>KELURAHAN '+ split +'</td><td>'+  result.namadesakelurahan +'</td></tr>'+
		'<tr><td>NO. TELP '+ split +'</td><td>'+ result.notelpon +'</td></tr>'+
		'<tr><td>GOL DARAH '+ split +'</td><td>'+ result.golongandarah +'</td></tr>'+
		'<tr><td>PENDIDIKAN '+ split +'</td><td>'+ result.namapendidikan +'</td></tr>'+
		'<tr><td>PEKERJAAN '+ split +'</td><td>'+ result.namapekerjaan +'</td></tr>'+
		'<tr><td>STATUS KAWIN '+ split +'</td><td>'+ result.statusmenikah +'</td></tr>'+
		'<tr><td colspan="2">&nbsp;</td></tr>'+
		'<tr><td>NAMA AYAH '+ split +'</td><td>'+ result.ayah +'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamatayah +'</td></tr>'+
		'<tr><td>NAMA IBU '+ split +'</td><td>'+ result.ibu +'</td></tr>'+
		'<tr><td>ALAMAT '+ split +'</td><td>'+ result.alamatibu +'</td></tr>'+
		'<tr><td colspan="2">&nbsp;</td></tr>'+
        '<tr><td colspan="2">Data Penanggung</td></tr>'+
        '</table>'+
        '<table class="unselectable table" border="1" cellspacing="0">\n\
            <thead>\n\
            <tr class="bg-info">\n\
                <td>&nbsp;Nama</td>\n\
                <td>&nbsp;Hubungan</td>\n\
                <td>Telpon</td>\n\
                <td>Alamat</td>\n\
            </tr></thead>';
		for(var p in penanggung)
                { 
                    printContents+= '<tr>\n\
                    <td>&nbsp;'+penanggung[p].namalengkap+'</td>\n\
                    <td>'+penanggung[p].namahubungan+'</td>\n\
                    <td>'+penanggung[p].notelpon+'</td>\n\
                    <td>'+penanggung[p].alamat+'</td></tr>'; 
                }
                printContents += '</table>';   
                printContents += 
                    '<div class="col-md-12 row">'
                      +'<div class="nav-tabs-custom">'
                        +'<ul class="nav nav-tabs">'
                          +'<li class="active"><a id="riwayatranap" idinap="'+idinap+'" idpendaftaran="'+idpendaftaran+'" norm="'+norm+'" data-toggle="tab" aria-expanded="true">Riwayat Perawatan Rawat Inap</a></li>'
                          +'<li class=""><a id="riwayatralan" idinap="'+idinap+'" idpendaftaran="'+idpendaftaran+'" norm="'+norm+'"  data-toggle="tab" aria-expanded="false">Riwayat Pemeriksaan Rawat Jalan</a></li>'
                          +'<li class=""><a id="catatanintegrasi" idinap="'+idinap+'" idpendaftaran="'+idpendaftaran+'" norm="'+norm+'" data-toggle="tab" aria-expanded="false">Catatan Terintegrasi</a></li>'
                        +'</ul>'
                        +'<div class="tab-content">'
                          +'<div class="tab-pane active" id="tab_1">'
                          +'</div>'
                          +'<div class="tab-pane" id="tab_2">'
                          +'</div>'
                          +'<div class="tab-pane" id="tab_3">'
                          +'</div>'
                        +'</div>'
                      +'</div>'
                    +'</div>'
                  +'</div>';        
        
	$('#displayRiwayatPasien').empty();
	$('#displayRiwayatPasien').html(printContents);
	$.alert({
            title: '',
            content: printContents,
            columnClass: 'xlarge',
            buttons: {
                formReset:{
                    text: "Kembali",
                    btnClass: 'btn-danger btn-xs'
                },
            },
            onContentReady: function () {
                getdatariwayatpemeriksaan(idinap,idpendaftaran,norm,'rawatinap')
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // if the user submits the form by pressing enter in the field.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // reference the button and click it
                });
            }
	});
}

//riwayat catatan terintegrasi
$(document).on('click','#catatanintegrasi',function(){
    var idinap = $(this).attr('idinap');
    var idpendaftaran= $(this).attr('idpendaftaran');
    var norm = $(this).attr('norm');
    getdatariwayatpemeriksaan(idinap,idpendaftaran,norm,'catatanterintegrasi');
    $('#tab_1').removeClass('active');
    $('#tab_2').removeClass('active');
    $('#tab_3').addClass('active');
});

//riwayat rawat jalan
$(document).on('click','#riwayatralan',function(){
    var idinap = $(this).attr('idinap');
    var idpendaftaran= $(this).attr('idpendaftaran');
    var norm = $(this).attr('norm');
    getdatariwayatpemeriksaan(idinap,idpendaftaran,norm,'rawatjalan');
    $('#tab_1').removeClass('active');
    $('#tab_2').addClass('active');
    $('#tab_3').removeClass('active');
});



//riwayat rawat inap
$(document).on('click','#riwayatranap',function(){
    var idinap = $(this).attr('idinap');
    var idpendaftaran= $(this).attr('idpendaftaran');
    var norm = $(this).attr('norm');
    getdatariwayatpemeriksaan(idinap,idpendaftaran,norm,'rawatinap');
    $('#tab_1').addClass('active');
    $('#tab_2').removeClass('active');
    $('#tab_3').removeClass('active');
});

//tampil riwayat ranap
function riwayat_rawatinap(result)
{
    var rencanamedisranap = result.rencanamedisranap;
    var rencana_anamnesa  = result.rencana_anamnesa;
    var rencana_lanjutan  = result.rencana_lanjutan;
    var rencana_tindakan  = result.rencana_tindakan;
    var rencanabp         = result.rencanabp;
    var printContents = '<table class="unselectable row" width="100%" style="margin-top:2px;" border="1" cellspacing="0" cellpadding="0"><thead>'+
    '<tr class="bg bg-yellow-gradient"><td colspan="7"><h4 class="text text-bold"> &nbsp;&nbsp;Riwayat Perawatan Rawat Inap</h4><label class="label label-danger">#Riwayat yang ditampilkan semua perawatan dengan status terlaksana.</label></td></tr>'+
    '<tr class="bg-info">\n\
        <td>&nbsp;Waktu </td>\n\
        <td>&nbsp;Anamnesis</td>\n\
        <td> &nbsp;Pemeriksaan </td>\n\
        <td>&nbsp;Diagnosa & Terapi </td>\n\
        <td>&nbsp;BHP/OBAT </td>\n\
    </tr>';

    for( var a in rencanamedisranap)
    {
        var list = rencanamedisranap;
        printContents +='<tr class="bg-warning"><td>&nbsp;'+list[a].waktu+'</td>'+'<td style="padding:2px;">';
        for( var b in rencana_anamnesa[a])
        {
            var listrencana_anamnesa = rencana_anamnesa[a];
            printContents +=listrencana_anamnesa[b].hasil+'<br>';
        }
        printContents += '</td><td style="padding:2px;">';

        for( var c in rencana_lanjutan[a])
        {
          var listrencana_lanjutan = rencana_lanjutan[a];
          printContents +=if_null(listrencana_lanjutan[c].hasil)+'<br>';
        }
        printContents +='</td><td style="padding:2px;">';
        
        for( var d in rencana_tindakan[a])
        {
          var listrencana_tindakan = rencana_tindakan[a];
          printContents +=listrencana_tindakan[d].hasil+'<br>';
        }
        printContents +='</td><td style="padding:2px;">';
        
        for( var e in rencanabp[a])
        {
          printContents += rencanabp[a][e].hasil+'<br>';
        }
        printContents+=' </td></tr>';
    }

    printContents += '<table>';
    $('#tab_1').empty();
    $('#tab_1').html(printContents);
}

//tampil riwayat ralan
function riwayat_ralan(result)
{
   var periksa = result.dtperiksa;
  var planjutan = result.planjutan;
  var anamnesis = result.dtanamnesis;
  var diagnosis = result.dtdiagnosis;
  var obat = result.dtobat;
  var detailperiksa = result.detailperiksa;
    var printContents =
        '<table class="unselectable" width="100%" style="margin-top:2px;" border="1" cellspacing="0" cellpadding="0"><thead>'+
        '<tr class="bg bg-yellow-gradient"><td colspan="7"><h4 class="text text-bold"> &nbsp;&nbsp;Riwayat Pemeriksaan Rawat Jalan</h4></td></tr>'+
        '<tr class="bg-info">\n\
            <td>&nbsp;Waktu </td>\n\
            <td>&nbsp;Anamnesis</td>\n\
            <td> &nbsp;Pemeriksaan </td>\n\
            <td> &nbsp;Catatan </td>\n\
            <td>&nbsp;Diagnosa & Terapi </td>\n\
            <td>&nbsp;BHP/OBAT </td>\n\
            <td>&nbsp;DOKTER </td>\n\
        </tr>';
        for( var i in periksa)
        {
            var list = periksa;
            printContents +='<tr class="bg-warning"><td>&nbsp;'+list[i].tanggal+'<br> &nbsp;'+list[i].waktu+'</td>'+'<td style="padding:2px;">';
             // list anamnesis
            printContents+=if_null(detailperiksa[i][0].anamnesa)+'<br>';
            for( x in anamnesis[i])
            {
                var listanamnesis = anamnesis[i];
                printContents +=listanamnesis[x].hasil_anamnesis+'<br>';
            }
            printContents += '</td><td style="padding:2px;">';
            // list pemeriksaan
            printContents += if_null(detailperiksa[i][0].keterangan)+'<br> <b>Hasil Expertise:</b>'+if_null(detailperiksa[i][0].keteranganradiologi)+((detailperiksa[i][0].saranradiologi=='')?'':'<b>Saran:</b>')+detailperiksa[i][0].saranradiologi+'<br>'+if_null(detailperiksa[i][0].keteranganlaboratorium)+'<br>';

            for( z in planjutan[i])
            {
              var listplanjutan = planjutan[i];
              printContents +=if_null(listplanjutan[z].hasil_diagnosis)+'<br>';
            }
            printContents += '</td><td style="padding:2px;">'+if_null(detailperiksa[i][0].rekomendasi)+'<br>';

            printContents +='</td><td style="padding:2px;">';
            printContents+=if_null(detailperiksa[i][0].diagnosa)+'<br>';
            // list diagnosis
            for( a in diagnosis[i])
            {
              var listdiagnosis = diagnosis[i];
              printContents +=listdiagnosis[a].hasil_diagnosis+'<br>';
            }
            printContents +='</td><td style="padding:2px;">';
            // list obat
            printContents+='<b>Resep Dokter :</b><br>'+if_null(detailperiksa[i][0].keteranganobat)+'<br> <b>Obat Diberikan Farmasi:</b><br/>';
            for( y in obat[i])
            {
              var listobat = obat[i];
              printContents +=listobat[y].obat+'<br>';
            }
            printContents+=' </td><td style="padding:2px;">'+list[i].namadokter+' <br> <i style="font-size:10px;"> Poli '+list[i].namaunit+'</i></td></tr>';
        }
        printContents += '<table>'; 
        $('#tab_2').empty();
        $('#tab_2').html(printContents);
}

//tampil riwayat catatan terintegrasi
function riwayat_catatanintegrasi(result)
{
    var printContents =
        '<table class="unselectable table table-hover table-striped" width="100%" style="margin-top:2px;" border="1" cellspacing="0" cellpadding="0"><thead>'+
        '<tr class="bg bg-yellow-gradient"><td colspan="5"><h4 class="text text-bold"> &nbsp;&nbsp;Catatan Terintegrasi</h4></td></tr>'+
        '<tr class="bg-info">\n\
            <td>&nbsp;Tgl/Jam </td>\n\
            <td>&nbsp;Profesi <br><small>Pemberi Asuhan</small></td>\n\
            <td> &nbsp; Hasil Asesmen Penatalaksanaan Pasien </td>\n\
            <td> &nbsp; Intruksi Profesional Pemberi Asuhan </td>\n\
            <td>&nbsp; Petugas </td>\n\
        </tr>';
    var catatan = result.catatan;
    for( var i in catatan ){ printContents += '<tr><td>'+catatan[i].tanggal+'/'+catatan[i].jam+'</td><td>'+catatan[i].profesi+'</td><td>'+catatan[i].soa_soa+'</td><td>'+catatan[i].soa_p+'</td><td>'+catatan[i].petugas+'<br>sip:'+catatan[i].sip+'</td></tr>';}
        printContents += '<table>'; 
        $('#tab_3').empty();
        $('#tab_3').html(printContents);
}




function getdatariwayatpemeriksaan(idinap,idpendaftaran,norm,mode)
{
    $.ajax({
      url:base_url+"cpelayananranap/getdata_riwayatperawatanranap",
      dataType:"JSON",
      type:"POST",
      data:{
          idinap:idinap,
          idpendaftaran:idpendaftaran,
          norm:norm,
          mode:mode
      },
      success:function(result){
          stopLoading();
          if(mode == 'rawatinap')
          {
              riwayat_rawatinap(result);
          }
          else if(mode == 'rawatjalan')
          {
              riwayat_ralan(result);
          }
          else if (mode == 'catatanterintegrasi')
          {
              riwayat_catatanintegrasi(result);
          }
      },
      error:function(){
          stopLoading()
          
      }
  });
}