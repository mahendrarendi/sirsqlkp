  'use strict';
  var jsmode=$('#jsmode').val();
  $('#jsmode').remove();
  switch(jsmode) {
    case 'tampilpesanan':
      tampilbelanjapesan('jsontampilpesananbarang');
      break;
    case 'belanja':
      tampilDetailBelanja();
      caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbrwlname');
      pilihdistributor();
      break;
    case 'retur':
      if(localStorage.getItem('modeubah')=='pembetulanpesan'){
        $('input[name="modeubah"]').val(localStorage.getItem('modeubah'));
        perbaikanpesanan_listpesan(localStorage.getItem('idpesan'),jsmode);
      }else{
        tampilbarang(getlocalstorage('lsdistribusibarang'));
        
      }
      caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbrwlname');
      break;  
    case 'pesan':
      if(localStorage.getItem('modeubah')=='pembetulanpesan'){
        $('input[name="modeubah"]').val(localStorage.getItem('modeubah'));
        perbaikanpesanan_listpesan(localStorage.getItem('idpesan'),jsmode);
      }else{
        tampilbarang(getlocalstorage('lsdistribusibarang')); 
      }
      caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbrwlname');
      break;
    case 'kirim':
      $('input[name="modeubah"]').val(localStorage.getItem('modeubah'));
      barangpesananunit(localStorage.getItem('idpesan'));
      break;
    case 'addtransformasibarang':
      tampiltransformasibarang(getlocalstorage('lstransformasiasal'));
      caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbpwlname');
      break;
    case 'stokbarang':
        $('.table').DataTable({/*"dom": '<"toolbar">frtip',*/"processing": true,"stateSave": true,"paging":true,"ordering": true});
      break;
    default:
      // code block
  }
  $(document).on('change','#ppnbelanja',function(){ 
    var ppn = parseFloat(unconvertToRupiah($('input[name="ppn"]').val()));
    $('input[name="ppn"]').val(convertToRupiah(ppn));
    var tagihan = parseFloat(unconvertToRupiah($('input[name="tagihan"]').val()));
    var potongan = parseFloat(unconvertToRupiah($('input[name="potongan"]').val()));
    var total = (tagihan - potongan) + ppn;
    $('input[name="totaltagihan"]').val(convertToRupiah(total));
  });
//menampilkan detail belanja
function tampilDetailBelanja()
{
    if (!is_empty(localStorage.getItem('idfakturbarangdetailbelanja')))
    {
      $.ajax({ 
          url:  base_url+'cmasterdata/jsonvpembelian',
          type : "POST",      
          dataType : "JSON",
          data:{idbarangfaktur:localStorage.getItem('idfakturbarangdetailbelanja')},
          success: function(hasil) {
            var result=hasil.pembelian;
            for (var x in result){addbarangpembelian(result[x].idbarang +','+ result[x].kode+','+result[x].namabarang+','+result[x].namasatuan+','+result[x].hargabeli+','+result[x].jumlah+','+result[x].kadaluarsa+','+result[x].batchno+','+result[x].idbarangdistribusi+','+result[x].idbarangpembelian+','+result[x].persendiskon+','+result[x].nominaldiskon+','+result[x].hargaasli,'ubah');}
            removelocalstorage('idfakturbarangdetailbelanja');
            tampilfakturbelanja(hasil);//menampilkan data faktur
            tampilbarang(getlocalstorage('lsdistribusibarang'));
          },
          error: function(result){  
            stopLoading();
              fungsiPesanGagal();
              return false;
          }
        });
    }
    else
    {
      tampilfakturbelanja();
      tampilbarang(getlocalstorage('lsdistribusibarang'));
      
    }
}
//******* cari barang untuk ditambahklan ke localstorage [lsdistribusibarang]
function caribarang(attributname,urlbarang)
{
    attributname.select2({
    minimumInputLength: 4,
    allowClear: true,
    ajax: {
        url: base_url+urlbarang, dataType: 'json', delay: 150, cache: false, type:"POST",
        data: function (params) {
            return {
                namaobat: params.term,
                unittujuan: $('select[name="idunitasal"]').val(),
                mode:$('input[name="mode"]').val(),
                moderetur:jsmode,

            };
        },
      processResults: function(data) { return { results : data,}; },
    }
    });
}
//** menambahkan barang ke localstorage [lsdistribusibarang]
function addbarangpembelian(value, md='tambah')
{
  var arrdata = split_to_array(value);
  var hargappn =  arrdata[4];  
  var data = getlocalstorage('lsdistribusibarang');
  var dataobat = JSON.stringify({idbarangdistribusi:(arrdata[8]!=undefined)?arrdata[8]:'',idbarangpembelian:(arrdata[9]!=undefined)?arrdata[9]:'',idbarang:arrdata[0],kode:arrdata[1],batchno:(arrdata[7]!=undefined)?arrdata[7]:'',kadaluarsa:(arrdata[6]!=undefined)?arrdata[6]:'',namabarang:arrdata[2],jumlah:arrdata[5],jumlahold:arrdata[5],harga:hargappn,hargaasli:(arrdata[6]!=undefined)?arrdata[12]:arrdata[4],namasatuan:arrdata[3],keterangan:'',dpersen:if_undefined(arrdata[10],0),dnominal:if_undefined(arrdata[11],0)});
  addlocalstorage('lsdistribusibarang',dataobat);  
  if (md == 'tambah')
  {
    $('select[name="idbarang"]').empty();
    $('select[name="idbarang"]').html('<option value="0">Cari barang/obat</option>');
    // notif('success','Tambah barang berhasil..!');
    tampilbarang(getlocalstorage('lsdistribusibarang'));
  }
}
function addbarangbarangdatang(value,md='tambah')
{
    var arrdata = split_to_array(value);
    var hargappn =  Math.round((parseInt(arrdata[4]) + ((parseInt(arrdata[4]) / 100) * 10 ) ))  ;  
    var data = getlocalstorage('lsdistribusibarang');
    var dataobat = JSON.stringify({idbarangdistribusi:(arrdata[8]!=undefined)?arrdata[8]:'',idbarangpembelian:(arrdata[9]!=undefined)?arrdata[9]:'',idbarang:arrdata[0],kode:arrdata[1],batchno:(arrdata[7]!=undefined)?arrdata[7]:'',kadaluarsa:(arrdata[6]!=undefined)?arrdata[6]:'',namabarang:arrdata[2],jumlah:arrdata[5],jumlahold:arrdata[5],harga:hargappn,hargaasli:(arrdata[6]!=undefined)?arrdata[12]:arrdata[4],namasatuan:arrdata[3],keterangan:'',dpersen:if_undefined(arrdata[10],0),dnominal:if_undefined(arrdata[11],0)});
    addlocalstorage('lsdistribusibarang',dataobat);  
    if (md == 'tambah')
    {
      $('select[name="idbarang"]').empty();
      $('select[name="idbarang"]').html('<option value="0">Cari barang/obat</option>');
      // notif('success','Tambah barang berhasil..!');
      tampilbarang(getlocalstorage('lsdistribusibarang'));
    }
}
function copybarangpembelian(name,index) // salin data 
{
   copylocalstorage(name,index);
   notif('success','Salin barang berhasil..!');
   return tampilbarang(getlocalstorage(name));
}
function updatebarangpembelian(index)//hapus localstorage by index
{
    var dataobat = getlocalstorage('lsdistribusibarang');
    dataobat[index] = JSON.stringify({idbarangdistribusi:$('#idbarangdistribusi'+index).val(),idbarangpembelian :$('#idbarangpembelian'+index).val(),idbarang:$('#idbarang'+index).val(),kode:$('#kode'+index).val(),batchno: $('#batchno'+index).val(),kadaluarsa:$('#kadaluarsa'+index).val(),namabarang: $('#namabarang'+index).val(),jumlah:unconvertToRupiah($('#jumlah'+index).val()),jumlahold:unconvertToRupiah($('#jumlahold'+index).val()),harga:unconvertToRupiah($('#hargappn'+index).val()),hargaasli:unconvertToRupiah($('#harga'+index).val()),namasatuan:$('#namasatuan'+index).val(),keterangan:$('#keterangan'+index).val(),dpersen:parseFloat($('#dpersen'+index).val()),dnominal:unconvertToRupiah($('#dnominal'+index).val()),status:'',});
    updatelocalstorage('lsdistribusibarang',dataobat);
    tampilbarang(getlocalstorage('lsdistribusibarang'));
    var jumlah = unconvertToRupiah($('#jumlah'+index).val());
    if(jumlah>0){ settinghargabelimasterbarang(index); }
}

function settinghargabelimasterbarang(index)
{
  var idbarang = $('#idbarang'+index).val();
  var harga = unconvertToRupiah($('#hargappn'+index).val());
  $.ajax({ 
    url:  base_url+'cmasterdata/cekhargabelimasterbarang',
    type : "POST",      
    dataType : "JSON",
    data:{idbarang:idbarang,hargabeli:harga},
    success: function(result){
      if(result.status==='ok')
      {
        var html= 'Data Barang <b>'+result.data.kode +' '+ result.data.namabarang+' </b>, Harga Sekarang: @HargaBeli+PPN '+convertToRupiah(result.data.hargabeli);

        $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'UBAH HARGA BELI',
            content: html +' <br> Ubah Ke: @HargaBeli+PPN '+ convertToRupiah(harga) + '<br><br><span class="label label-danger">*harga di master barang akan otomatis berubah jika klik ubah.</span>',
            buttons: {
                ubah: function () { /*Ubah harga Beli (termasuk ppn) */
                  $.ajax({ 
                    url:  base_url+'cmasterdata/sethargabelimasterbarang',
                    type : "POST",      
                    dataType : "JSON",
                    data:{idbarang:idbarang,hargabeli:harga},
                    success: function(result){},
                    error: function(result){ fungsiPesanGagal(); return false;}
                  });
                },
                tidak: function () {}
            }
        });
      }

    },
    error: function(result){  
        fungsiPesanGagal();
        return false;
    }
  });
}

function deletebarangpembelian(index)//hapus localstorage by index
{
  
    var a= $('#idbarangdistribusi'+index).val();
    var b= $('#idbarangpembelian'+index).val();
    var c= $('#idbarang'+index).val();
    var d= $('input[name="idbarangfaktur"]').val();
    var h= $('input[name="statusfaktur"]').val();
  if(b>0){
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi',
        content: 'Hapus BHP/Obat.?',
        buttons: {
            ubah: function () { /*Ubah harga Beli (termasuk ppn) */
                startLoading();
                $.ajax({url:base_url+'cmasterdata/setdelete_barangpembelian',type:"POST",dataType:"JSON",data:{idbd:a,idbp:b,idb:c,status:h},
                  success: function(result){
                    loadafter_deletebarangpembelian(index);
                    setTimeout(function(){
                      var e= unconvertToRupiah($('input[name="tagihan"]').val());
                      var f= unconvertToRupiah($('input[name="potongan"]').val());
                      var g= unconvertToRupiah($('input[name="ppn"]').val());
                      $.ajax({url:base_url+'cmasterdata/updatefaktur_ad_barangpembelian',type:"POST",dataType:"JSON",data:{idbf:d,tagihan:e,potongan:f,ppn:g,mode:'ubah'},
                        success: function(result){},
                        error: function(result){ stopLoading();fungsiPesanGagal(); return false;}
                      }); 
                    },1000); 
                  },
                  error: function(result){ stopLoading();fungsiPesanGagal(); return false;}
                });          
            },
            tidak: function () {}
        }
    });
  }else{
    loadafter_deletebarangpembelian(index);
  }
}
function loadafter_deletebarangpembelian(index)
{
  
    stopLoading();
    var dataobat = getlocalstorage('lsdistribusibarang');
        dataobat[index] = JSON.stringify({idbarangdistribusi:'',idbarangpembelian :'',idbarang:'',kode:'',batchno:'',kadaluarsa:'',namabarang:'',jumlah:'',harga:'',namasatuan:'',keterangan:'',dpersen:0,dnominal:0,status:'batal',});
    updatelocalstorage('lsdistribusibarang',dataobat);
    tampilbarang(getlocalstorage('lsdistribusibarang'));
    hitungTagihanBelanja();
}
//menampilkan data barang
function tampilbarang(data)
{
 var isi ='', no=0,total=0;
 for(var i in data)
 {
  var obj = JSON.parse(data[i]);
    if(obj.status!=='batal')
    {
      if(jsmode=='pesan')
      {
        isi += '<tr>'    
        +'<input type="hidden" name="idbarang[]" id="idbarang'+i+'" value="'+obj.idbarang+'"/>'
        +'<input type="hidden" name="statusbarang[]" id="statusbarang'+i+'" value="'+obj.statusbarang+'"/>'
        +'<td>'+ obj.kode +'<input type="hidden" id="kode'+i+'" name="kode[]" value="'+ obj.kode +'"/></td>'
        +'<td>'+ obj.namabarang +'</td>'
        +'<td>'+ convertToRupiah(obj.harga) +'</td>'
        +'<td><input type="hidden"  id="jumlahsebelum'+i+'" name="jumlahsebelum[]" value="'+ obj.jumlah +'"/><input type="text"  id="jumlah'+i+'" class="form-control" size="1" name="jumlah[]" value="'+ obj.jumlah +'"/></td>'
        +'<td>'+ obj.namasatuan +'</td>'
        +'<td><textarea class="form-control" rows="1" name="keterangan[]" id="keterangan'+i+'">'+ obj.keterangan +'</textarea></td>'
        +'<td>'+((localStorage.getItem('modeubah')!==null)?'':'<a onclick="deletebarangpembelian('+i+')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Batalkan"><i class="fa fa-minus-circle"></i></a>')+'</td>'
        +'</tr>';
      }else if(jsmode=='retur'){
        isi += '<tr>'    
        +'<input type="hidden" name="idbarang[]" id="idbarang'+i+'" value="'+obj.idbarang+'"/>'
        +'<input type="hidden" name="idbarangpembelian[]" id="idbarangpembelian'+i+'" value="'+obj.idbarangpembelian+'"/>'
        +'<input type="hidden" name="statusbarang[]" id="statusbarang'+i+'" value="'+obj.statusbarang+'"/>'
        +'<td>'+ obj.kode +'<input type="hidden" id="kode'+i+'" name="kode[]" value="'+ obj.kode +'"/></td>'
        +'<td>'+ obj.namabarang +'</td>'
        +'<td>'+ convertToRupiah(obj.harga) +'</td>'
        +'<td>'+ obj.batchno +'</td>'
        +'<td>'+ obj.kadaluarsa +'</td>'
        +'<td><input type="hidden"  id="jumlahsebelum'+i+'" name="jumlahsebelum[]" value="'+ obj.jumlah +'"/><input type="text"  id="jumlah'+i+'" class="form-control" size="1" name="jumlah[]" value="'+ obj.jumlah +'"/></td>'
        +'<td>'+ obj.namasatuan +'</td>'
        +'<td><textarea class="form-control" rows="1" name="keterangan[]" id="keterangan'+i+'">'+ obj.keterangan +'</textarea></td>'
        +'<td>'+((localStorage.getItem('modeubah')!==null)?'':'<a onclick="deletebarangpembelian('+i+')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Batalkan"><i class="fa fa-minus-circle"></i></a>')+'</td>'
        +'</tr>';
      }else{
        isi += '<tr>'    
        +'<input id="jumlahold'+i+'" type="hidden" name="jumlahold[]" value="'+ convertToRupiah(obj.jumlahold) +'"/>'
        +'<input id="idbarangdistribusi'+i+'" type="hidden" name="idbarangdistribusi[]" value="'+obj.idbarangdistribusi+'"/>'
        +'<input id="idbarangpembelian'+i+'" type="hidden" name="idbarangpembelian[]" value="'+obj.idbarangpembelian+'"/>'
        +'<input id="idbarang'+i+'" type="hidden" name="idbarang[]" id="idbarang'+i+'" value="'+obj.idbarang+'"/>'
        +'<input type="hidden" name="statusbarang[]" id="statusbarang'+i+'" value="'+obj.statusbarang+'"/>'
        +'<td>'+ obj.kode +'<input type="hidden" id="kode'+i+'" name="kode[]" value="'+ obj.kode +'"/></td>'
        +'<td>'+ obj.namabarang +'<input type="hidden" id="namabarang'+i+'" name="namabarang[]" value="'+ obj.namabarang +'"/></td>'
        +'<td><input type="text" id="batchno'+i+'" size="6" onchange="updatebarangpembelian('+i+')" name="batchno[]" placeholder="BatchNo" autocomplete="off" value="'+ obj.batchno +'"/></td>'
        +'<td><input type="text" id="kadaluarsa'+i+'" class="datepicker" onchange="updatebarangpembelian('+i+')" size="6" autocomplete="off" name="kadaluarsa[]" placeholder="yyyy-mm-dd" value="'+ obj.kadaluarsa +'"/></td>'
        +'<td><input type="text"  id="jumlah'+i+'" size="6" autocomplete="off" onchange="updatebarangpembelian('+i+')" name="jumlah[]" value="'+ convertToRupiah(obj.jumlah) +'"  onkeyup="hitungTagihanBelanja()" /></td>'
        +'<td><input type="text"  id="hargappn'+i+'" size="6" autocomplete="off" onchange="updatebarangpembelian('+i+')" name="hargappn[]" value="'+ convertToRupiah(obj.harga) +'"/></td>'
        +'<td><input type="text"  id="harga'+i+'" size="6" autocomplete="off" onchange="updatehargappn('+i+')" name="harga[]" value="'+ convertToRupiah(obj.hargaasli) +'"/></td>'
        +'<td><input type="text"  id="dpersen'+i+'" size="6" autocomplete="off" onchange="hitungdiskon('+i+',\'persen\')" name="persendiskon[]" value="'+ obj.dpersen+'"/></td>'
        +'<td><input type="text"  id="dnominal'+i+'" size="6" autocomplete="off" onchange="hitungdiskon('+i+',\'nominal\')" name="nominaldiskon[]" value="'+ convertToRupiah(obj.dnominal)+'"/></td>'
        +'<td>'+ obj.namasatuan +'<input type="hidden" id="namasatuan'+i+'" name="namasatuan[]" value="'+ obj.namasatuan +'"/></td>'
        +'<td>'+ convertToRupiah(parseFloat(obj.jumlah) * parseFloat(obj.harga) )+'<input type="hidden" id="namasatuan'+i+'" name="namasatuan[]" value="'+ obj.namasatuan +'"/></td>'
        +'<td> <a class="btn btn-info btn-sm" onClick="hitungdiskon('+i+',\'persen\')" data-toggle="tooltip" data-original-title="Hitung Ulang Diskon"><i class="fa fa-calculator"></i></a> <a onclick="copybarangpembelian(\'lsdistribusibarang\','+i+')" class="btn btn-warning btn-sm" data-toggle="tooltip" data-original-title="Salin"><i class="fa fa-copy"></i></a> <a onclick="deletebarangpembelian('+i+')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Batalkan"><i class="fa fa-minus-circle"></i></a></td>'
        +'</tr>';
      }
      
    }
  }
  $('#tbodydistribusi').empty();
  $('#tbodydistribusi').html(isi);
  (jsmode!=='pesan')? hitungTagihanBelanja():'';
  $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd", orientation: "bottom"}); //Initialize Date picker
  $('[data-toggle="tooltip"]').tooltip();
}

function updatehargappn(index)
{
  var hargaasli  = unconvertToRupiah($('#harga'+index).val());
  var potongan = parseFloat($('#dpersen'+index).val());
  var harga = hargaasli - (hargaasli * potongan/100);
  var hargappn = convertToRupiah(Math.round( harga * 1.1));
  $('#hargappn'+index).val(hargappn);
  settinghargabelimasterbarang(index);
  hitungTagihanBelanja();
}
//simpan  barangpembelian
function simpanbarangpembelian()
{
  if($('#tanggalfaktur').val()=='')
  {
    notif('warning','Tanggal faktur Belum diisi.!');
  }
  else if($('#idbarangdistributor').val()==0)
  {
   notif('warning','Distributor Belum diisi..!.!'); 
  }
  else if($('input[name="tagihan"]').val()==0)
  {
   notif('warning','Item masih kosong.');  
  }
  else
  {
      if($('input[name="setterimabarang"]:checked').val())
      {
          $.confirm({
            icon: 'fa fa-exclamation', theme: 'modern', closeIcon: true, animation: 'scale', type: 'orange', title: 'Perhatian!',
            content: 'Data yang telah disimpan tidak dapat diubah lagi, apakah data inputan anda sudah tidak ada perubahan.?',
            buttons: {
                simpan: function () { $('#Formbarangpembelian').submit(); },
                batal: function () { }
            }
        });
      }
      else
      {
        $('#Formbarangpembelian').submit();   
      }
  }
}

function hitungdiskon(index,mode)
{   
    var diskon=0, nominal= parseInt(unconvertToRupiah($('#harga'+index).val())) * parseInt(unconvertToRupiah($('#jumlah'+index).val()));
    if(mode=='persen'){
        //cari nominal diskon
        diskon = nominal * (parseFloat($('#dpersen'+index).val()) / 100 );
        $('#dnominal'+index).val(diskon.toFixed(0));
    }else{
        //cari persen diskon
        diskon = (parseInt($('#dnominal'+index).val()) / nominal) * 100;
        $('#dpersen'+index).val(angkadesimal(diskon,2));
        $('#dnominal'+index).val(parseFloat($('#dnominal'+index).val()).toFixed(0));
    }
    updatehargappn(index);
    hitungTagihanBelanja();
    updatebarangpembelian(index);//update rows
}

// mahmud clear 
// ketika menu detail di klik
$(document).on("click","#detaildistribusi", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("idbf", $(this).attr("alt"));//create storage id barang faktur
        window.location.href=base_url + 'cmasterdata/detail_distribusibarang';
    } else {
         pesanUndefinedLocalStorage();
    }
});

function distribusibelanjabarang(idfaktur, modeview=false)
{
    startLoading();
    localStorage.setItem('idfaktur',idfaktur); // buat storage idpesan
    localStorage.setItem('modeview', modeview);
    window.location.href=base_url + 'cmasterdata/distribusibelanjabarang';
}
//menampilkan data retur barang
function tabelReturBarang(data)
{
  var isi = '',no=0;
  for(var x in data)
  {
      if(idbarangfaktur!=data[x].idbarangfaktur){no++;}
      if(no==2){no=0;}
      var idbarangfaktur = data[x].idbarangfaktur;
      var style =  ((no==1)?'style="background-color:#fff;"':'style="background-color:#eeeeee85;"');
      //hideid >> untuk membedakan data by idbarangfsktur
      var hideid='<span style="color:#ffffff00;font-size:0px;">'+data[x].nofaktur+'</span>';
      isi +='<tr '+style+'><td>' + hideid + data[x].nofaktur+'</td><td>' + hideid+ data[x].waktu +'</td><td>'+ hideid+data[x].unitasal+'/'+data[x].unittujuan+ ((data[x].statuspengembalian=='menunggu')? ((sessionStorage.unitterpilih!=data[x].unittujuan)? ' <a onclick="kirimpengembalian('+data[x].idbarangpengembalian+',false)" '+tooltip('Pengembalian Diterima')+' class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>' :' <a  onclick="kirimpengembalian('+data[x].idbarangpengembalian+',\'pembetulanpesan\')" ' +tooltip('pembetulan')+' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> ') : ((sessionStorage.unitterpilih!=data[x].unittujuan)? ' <a onclick="kirimpengembalian('+data[x].idbarangpengembalian+',\'pembetulankirim\')" ' +tooltip('pembetulan')+' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>' : '' )) + ((sessionStorage.unitterpilih=='GUDANG')? '' : ' <a  onclick="cetakbarangpesanan('+data[x].idbarangpengembalian+')" ' +tooltip('cetak')+' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>') +'</td><td style="'+((data[x].statuspengembalian=='selesai')?'background-color:#8bc34a;':'')+'">'+hideid+data[x].statuspengembalian+'</td><td>'+hideid+data[x].kode+'/'+data[x].namabarang+'</td><td>'+'<span class="hidden">'+x+'</span>'+ convertToRupiah(data[x].jumlah)+'/'+data[x].namasatuan+'</td><td>'+ data[x].keterangan +'</td><td></td></tr>';  //'+ hideid+ data[x].namalengkap +'
   }
  var table = '<table border="1" class="table table-striped table-hover  dt-responsive" cellspacing="0" width="100%"><thead>'
              +'<tr style="background-color:#ffc10759;"><td rowspan="2">Faktur</td><td rowspan="2">Tanggal</td><td rowspan="2">Unit Asal/Tujuan</td><td rowspan="2">Status</td><td colspan="3">Detail Barang</td><td rowspan="2">Pemesan</td></tr>'
              +'<tr style="background-color:#ffc10759;"><td>Kode/Nama</td><td>jumlah/satuan</td><td>keterangan</td></tr>'
              +'</thead><tbody>' + isi +'</tfoot></table>';
  $('#tampildistribusi').empty();
  $('#tampildistribusi').html(table);
  $('.table').DataTable({"dom": '<"toolbar">frtip',"processing": true,"stateSave": true,"paging":false,"ordering": false});
  var menu='<input name="bulan" class="form-control datepicker" size="4"/>';
    menu+=' <a id="pengembalianbarang" class="btn btn-info btn-sm" ><i class="fa fa-desktop"></i> Tampil</a>';
    menu+=' <a onclick="refresh_halaman()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
    menu+=((sessionStorage.unitterpilih!='GUDANG')?' <a href="'+base_url+'tambahpengembalian" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Pengembalian</a>':'');  
    $("div.toolbar").html(menu);
    $('[data-toggle="tooltip"]').tooltip();
  var caribulan = localStorage.getItem('caribulan');
  $('.datepicker').datepicker({autoclose: true,format: "M yyyy", viewMode: "months", minViewMode: "months",orientation: "bottom"}).datepicker("setDate", ((caribulan==null || caribulan==undefined)?'now':caribulan) ); //Initialize Date picker
  dynamicallyMergeCells(); //--panggil dari sirstql.min.js
}

///************tambah colspan dan rowspan otomatis*****************
//function dynamicallyMergeCells()
//{
////    dynamically adds colspans and / or rowspans to a table cell, when it contains certain content that is the same as the one in the next cell
////    https://stackoverflow.com/questions/29162783/dynamically-add-colspans-and-or-rowspans-to-a-table-cell
//    var i = 0;
//    var $trs = $('.table tr');
//    $trs.each(function() {
//        var $tds = $(this).find('td');
//        var width = $tds.length;
//        var num = 2;
//        for(i = width - 2; i >= 0; i--) {
//            if($($tds[i]).html() == $($tds[i + 1]).html() && $($tds[i]).html() != ""){
//                $($tds[i]).attr('colspan', num);
//                num++;
//                $($tds[i + 1]).remove();
//            } else {
//                num = 2;
//            }
//        }
//        $tds = $(this).find('td');
//        width = $tds.length;
//        $($tds[0]).attr('seq', 0);
//        for(i = 1; i < width; i++) {
//            $($tds[i]).attr('seq', parseInt($($tds[i - 1]).attr('seq')) + $($tds[i - 1]).prop('colspan'));
//        }
//    });
//
//    var height = $trs.length;
//    var j = 0;
//
//    for(i = height - 2; i >= 0; i--){
//        $($trs[i]).find('td').each(function() {
//            var seq = parseInt($(this).attr('seq'));
//            var $tdUnder = $($trs[i + 1]).find('td[seq="' + seq + '"]');
//            if($tdUnder.length && ($tdUnder.html() != "") && ($tdUnder.html() == $(this).html()) && ($tdUnder.prop('colspan') == $(this).prop('colspan'))) {
//                $(this).prop('rowspan', $tdUnder.prop('rowspan') + 1);
//                $tdUnder.remove();
//            }
//        });
//    }
//}



//START BARANG PESANAN


//form persetujuan pengembalian
function kirimpengembalian(idpesan, modeubah)
{
    startLoading();
    localStorage.setItem('idpesan',idpesan); // buat storage idpesan
    localStorage.setItem('modeubah', modeubah);
    if(modeubah=='pembetulanpesan')
    {
        window.location.href=base_url + 'tambahpengembalian';
    }
    else
    {
        window.location.href=base_url + 'cmasterdata/saveterimabarangunit';
    }
    
}

function barangpesananunit()
{
      var data = getlocalstorage('barangpesanan');
      if(data==null)
      {
         $.ajax({url:base_url+'cmasterdata/jsontampilpesananbarang',type:"POST",dataType:"JSON",
            data:{id:localStorage.getItem('idpesan'),mode:localStorage.getItem('modeubah')},
            success: function(result) {
               stopLoading();
               removelocalstorage('barangpesanan');
               addlocalstorage('barangpesanan',result);
               tampilbarangpesanan(getlocalstorage('barangpesanan'));
            },
            error: function(result){stopLoading(); fungsiPesanGagal(); return false;}
          });
      }
      else
      {
          tampilbarangpesanan(getlocalstorage('barangpesanan'));
      }
}
function perbaikanpesanan_listpesan(ip,mode)
{
    $('input[name="ip"]').val(localStorage.getItem('idpesan'));
    var data = getlocalstorage('lsdistribusibarang');
    if(data==null)
    {
       $.ajax({ 
          url:base_url+ ((mode=='pesan') ? 'cmasterdata/jsontampilpesananbarang' : 'cmasterdata/jsontampilpengembalianbarang') ,
          type:"POST",dataType:"JSON",
          data:{id:localStorage.getItem('idpesan')},
          success: function(result){
          setidunitasal(result[0].idunit);
            stopLoading();
            var barang='', dblocal='lsdistribusibarang';
             removelocalstorage(getlocalstorage(dblocal));
            for (var i in result)
            {
                barang  = JSON.stringify(
                {
                    idbarangpembelian:result[i].idbarangpembelian,
                    idbarang:result[i].idbarang,
                    kode:result[i].kode,
                    kadaluarsa:result[i].kadaluarsa,
                    batchno:result[i].batchno,
                    namabarang:result[i].namabarang,
                    jumlah:result[i].jumlah,
                    harga:result[i].hargajual,
                    namasatuan:result[i].namasatuan,
                    keterangan:result[i].keterangan,
                    status:'',
                });
                addlocalstorage(dblocal,barang);  
            }
            tampilbarang(getlocalstorage(dblocal));
          },
          error:function(result){stopLoading(); fungsiPesanGagal();return false;}
        });
    }
    else
    {
        tampilbarang(getlocalstorage('lsdistribusibarang'));
    }
}
function tampilbarangpesanan(data)
{
    $('input[name="idbarangpemesanan"]').val(data[0][0].idbarangpemesanan);
    $('input[name="nofaktur"]').val(data[0][0].nofaktur);
    $('input[name="waktu"]').val(data[0][0].waktu);
    // $('input[name="unitasal"]').val(data[0][0].unitasal);
    $('select[name="idunittujuan"]').empty();
    $('select[name="idunittujuan"]').html('<option value="'+data[0][0].idunittujuan+'">'+data[0][0].unittujuan+'</option>');
    var isi ='', no=0,total=0;
    for(var i in data[0])
    {          
        isi += '<tr class="bg bg-yellow"><th>No</th><th>Kode</th><th width="40"></th><th>Barang/Obat</th><th>JumlahPesan</th><th>BelumPlot</th><th>Satuan</th><th>Keterangan</th></tr>'
        +'<tr id="row'+i+'"><input id="idbarangparent'+i+'" type="hidden" value="'+data[0][i].idbarang+'" />'   
        +'<td>'+ ++no +'</td>'
        +'<td>'+ data[0][i].kode +'</td>'
        +'<td><input id="jumlahdetail'+data[0][i].idbarang+'" type="hidden" value="0" /></td>'
        +'<td>'+ data[0][i].namabarang +'</td>'
        +'<td><div id="jumlahpesan'+data[0][i].idbarang+'">'+ convertToRupiah(parseInt(data[0][i].jumlah)) +'</div></td>'
        +'<td><div id="belumdiplot'+data[0][i].idbarang+'">'+ convertToRupiah(parseInt(data[0][i].plot)) +'</div></td>'
        +'<td>'+ data[0][i].namasatuan +'</td>'
        +'<td>'+ data[0][i].keterangan +'</td>'
        +'</tr>';
     }
     $('#tbodybarangpesanan').empty();
     $('#tbodybarangpesanan').html(isi);
     setidunitasal();
     for(var i in data[0]){tambahbarangkirim(data[0][i].idbarang,i,data[0][0].idbarangpemesanan,'tampilpesan');}
     $('[data-toggle="tooltip"]').tooltip();
}

function setidunitasal(idunit='')
{
  // ambil id asal dari user yang login
  var idunitasal = (idunit=='') ? sessionStorage.idunitterpilih  : idunit ;
  var arr = [], option='';
  $('select[name="idunitasal"]').find('option').each(function(index) {arr.push ([$(this).val(),$(this).text()]);});
  for(var no in arr ){ option += '<option '+ ((idunitasal == arr[no][0])? 'selected':'' ) +'  value="'+arr[no][0]+'">'+arr[no][1]+'</option>';}
  $('select[name="idunitasal"]').empty();
  $('select[name="idunitasal"]').html(option);
}

function setbelumdiplot(idb, jumlah)
{
    $('#belumdiplot'+idb).html(ifnull(parseInt(unconvertToRupiah($('#belumdiplot'+idb).html())) + parseInt(jumlah), 0));
}

function belumdiplotawal(idb, jumlahsudah)
{
    var bp = unconvertToRupiah($('#jumlahpesan'+idb).html()) - jumlahsudah;
    setbelumdiplot(idb, bp);
}

function settombolsemua(idb, no)
{
    $('#tombolsemua'+idb+'_'+no).html(((parseInt(ifnull($('#stok'+idb+'_'+no).val(), 0))>=parseInt(ifnull($('#belumdiplot'+idb).html(), 0)) && parseInt(ifnull($('#belumdiplot'+idb).html(), 0))>0)?'<a onclick="$(\'#jumlah'+idb+'_'+no+'\').val('+parseInt(ifnull($('#belumdiplot'+idb).html(), 0))+');keyupjumlah('+idb+','+no+')"  '+tooltip('Simpan Pilihan')+' class="btn btn-xs btn-primary"><i class="fa fa-save"></i> Pilih Ini</a>':''));
}

function simpanbarangpesanandetail(idb, jumlah)
{
    alert("Simpan idbarang " + idb + "dengan jumlah" + jumlah);
}

function tambahbarangkirim(idb,no,idp,mode='')
{

  startLoading();
    $.ajax({ 
        url:base_url+'cmasterdata/jsontampilpesananbarangstok',
        type:"POST",dataType:"JSON",
        data:{id:idb, idunit:$('select[name="idunitasal"]').val(), idpm:idp, ht:($('#belumdiplot'+idb).html() == $('#jumlahpesan'+idb).html() )?0:1 },
        success: function(hasil) {
          stopLoading();
          if(hasil.length==0 && mode!='tampilpesan'){return notif_confirminfo('Stok barang kosong.'); }
            var tdstok='';
            var idbarangparent = $('#idbarangparent'+no).val();
            for(var x in hasil)
            {  
              $('#jumlahdetail'+idb).val(parseInt($('#jumlahdetail'+idb).val(), 10) + 1);
              if (hasil[x].idbarangpembelian > 0)
                tdstok+='<tr id="'+no+'rowstok'+idbarangparent+'_'+x+'"><input type="hidden" value="xxx" name="length[]"/><input  type="hidden" name="idbarang[]" value="'+idbarangparent+'" /><input type="hidden" name="idbarangpembelian[]" value="'+hasil[x].idbarangpembelian+'" /><td>'+hasil[x].kode+'</td><td>'+hasil[x].namabarang+'</td><td>'+hasil[x].batchno+'</td><td>'+hasil[x].kadaluarsa+'</td><td><input type="hidden" id="stok'+idbarangparent+'_'+x+'" value="'+hasil[x].stok+'" /><div id="divstok'+idbarangparent+'_'+x+'">'+hasil[x].stok+'</div></td><td><input id="pembetulan'+idbarangparent+'_'+x+'" type="hidden" name="jumlahpembetulan[]" value="'+parseInt(hasil[x].jumlah)+'" /><input type="hidden" value="'+parseInt(hasil[x].jumlah)+'" id="jumlahsebelum'+idbarangparent+'_'+x+'" /><input onkeyup="keyupjumlah('+idb+', '+x+')" id="jumlah'+idbarangparent+'_'+x+'" name="jumlah[]" value="'+parseInt(hasil[x].jumlah)+'" type="text" class="qlform-control" size="1" /><div id="tombolsemua'+idbarangparent+'_'+x+'"></div></td><td>'+hasil[x].namasatuan+'</td><td></td></tr>'; 
              else
                tdstok+='<tr><td>'+hasil[x].kode+'</td><td>'+hasil[x].namabarang+'</td><td>'+hasil[x].batchno+'</td><td>'+hasil[x].kadaluarsa+'</td><td>-</td><td>'+hasil[x].jumlah+'</td><td>'+hasil[x].namasatuan+'</td><td></td></tr>'; 
            }       
            var tabel='<table  class="table table-striped table-bordered table-hover" style="width:100%; margin:0px; padding:2px;"><theader>'+((hasil.length==0 && mode=='tampilpesan')?'<tr><td class="rop-habis text-center">Stok Barang Kosong</td></tr>':'<tr style="background-color:#ecf0f5"><th>Kode</th><th>Barang/Obat</th><th>BatchNo</th><th width="10%">Kadaluarsa</th><th width="8%">Stok</th><th width="15%">Jumlah Diberikan</th><th>Satuan</th><th></th></tr>')+'</theader><tbody id="lstblbrgpesanan">'+tdstok+'</tbody></table>';
            var header='<table class="table table-striped table-bordered table-hover"><thead><tr class="bg bg-yellow"><th>No</th><th>Kode</th><th width="40"></th><th>Barang/Obat</th><th>JumlahPesan</th><th>Satuan</th><th>Keterangan</th></tr></thead>';
            $('#row'+no).after('<tr><td colspan="2"></td><td colspan="5">'+tabel+'</td></tr>');
            for (var x in hasil) { settombolsemua(idb, x); }
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
      });
}
function sumStokPesanan(idb,no)
{
  var sebelum = $('#pembetulan'+idb+'_'+no).val();
  var stok = $('#stok'+idb+'_'+no).val();
  var diberikan=$('#jumlah'+idb+'_'+no).val();
  var hitung = (parseInt(sebelum) + parseInt(stok)) - parseInt(ifnull(diberikan,0));
  $('#divstok'+idb+'_'+no).html(hitung);
}
function keyupjumlah(idb, no)
{
    var updatejumlah=0,jumlah=parseInt(ifnull($('#jumlah'+idb+'_'+no).val(), 0)), jumlahsebelum=parseInt(ifnull($('#jumlahsebelum'+idb+'_'+no).val(), 0));
    updatejumlah = jumlah - jumlahsebelum;
    if(parseInt($('#stok'+idb+'_'+no).val()) < updatejumlah)
    {
        $.confirm({icon: 'fa fa-exclamation',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Peringatan!',
        content: 'Jumlah melebihi stok yang ada.',
        buttons: {
            confirm: function () {
                $('#jumlah'+idb+'_'+no).val(parseInt($('#stok'+idb+'_'+no).val()));
                updatejumlah = parseInt($('#jumlah'+idb+'_'+no).val()) - jumlahsebelum;
                setbelumdiplot(idb, -updatejumlah);
                $('#jumlahsebelum'+idb+'_'+no).val($('#jumlah'+idb+'_'+no).val());
                settombolsemua(idb, no);
            }
          }
        });
    }
    else if (parseInt($('#belumdiplot'+idb).html()) < updatejumlah)
    {
        $.confirm({icon: 'fa fa-exclamation',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Peringatan!',
        content: 'Jumlah melebihi belum diplot.',
        buttons: {
            confirm: function () {
                $('#jumlah'+idb+'_'+no).val(parseInt($('#belumdiplot'+idb).html()) + jumlahsebelum);
                updatejumlah = parseInt($('#jumlah'+idb+'_'+no).val()) - jumlahsebelum;
                setbelumdiplot(idb, -updatejumlah);
                $('#jumlahsebelum'+idb+'_'+no).val($('#jumlah'+idb+'_'+no).val());
                settombolsemua(idb, no);
            }
        }
        });
    }
    else
    {
        setbelumdiplot(idb, -updatejumlah);
        $('#jumlahsebelum'+idb+'_'+no).val($('#jumlah'+idb+'_'+no).val());
        settombolsemua(idb, no);        
        sumStokPesanan(idb,no);
    }
}
function addls_barangkirim() //add local storage barang kirim
{
    var barang = JSON.stringify({idbarang:arrdata[0],kode:arrdata[1],kadaluarsa:'',namabarang:arrdata[2],jumlah:0,harga:arrdata[4],namasatuan:arrdata[3],keterangan:'',status:'',});
  addlocalstorage('lsdistribusibarang',dataobat);  
  $('select[name="idbarang"]').empty();
  $('select[name="idbarang"]').html('<option value="0">Cari barang/obat</option>');
  notif('success','Tambah barang berhasil..!');
  tampilbarang(getlocalstorage('barangpesanan'));
}
//end barang pesanan


function savekirimbarangpesanan()
{    
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Pastikan data sudah sesuai.!!',
        buttons: {
            confirm: function () {
            $('#Formbarangpesanan').submit();
            },
            cancel: function () {

            },
        }
    });
    
}
//TRANSFORMASI BARANG
//** menambahkan barang ke localstorage [lstransformasiasal]
function addtransformasibarang(value,parent)
{
    var dblocal = 'lstransformasiasal';
    var arrdata = split_to_array(value);
    var barang  = JSON.stringify(
    {
        idbarangpembelian:arrdata[0],
        idbarang:arrdata[1],
        kode:arrdata[2],
        namabarang:arrdata[3],
        namasatuan:arrdata[4],
        hargabeli:arrdata[5],
        stok:arrdata[6],
        kadaluarsa:arrdata[7],
        batchno:arrdata[8],
        status:'',
    });
    addlocalstorage(dblocal,barang);  
  $('select[name="idbarang"]').empty();
  $('select[name="idbarang"]').html('<option value="0">Cari barang/obat</option>');
  notif('success','Tambah transformasi barang berhasil..!');
  tampiltransformasibarang(getlocalstorage(dblocal));
}



function loaddataviewtransformasibarang()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/jsonvtrans',
        type : "POST",      
        dataType : "JSON",
        data:{w:localStorage.getItem("waktu"), gw:localStorage.getItem("grupwaktu") },
        success: function(result) {
            var dblocal = 'lstransformasiasal';
            var barang;
            removelocalstorage(dblocal);
            for (var i in result)
            {
                barang  = JSON.stringify(
                {
                    id:result[i].id,
                    jenisdistribusi:result[i].jenisdistribusi,
                    idbarangpembelian:result[i].idbarangpembelian,
                    idbarang:result[i].idbarang,
                    kode:result[i].kode,
                    namabarang:result[i].namabarang,
                    namasatuan:result[i].namasatuan,
                    hargabeli:result[i].hargabeli,
                    jumlah:result[i].jumlah,
                    stok:'',
                    kadaluarsa:result[i].kadaluarsa,
                    batchno:result[i].batchno,
                    status:'',
                });
                addlocalstorage(dblocal,barang);  
            }
            tampiltransformasibarang(getlocalstorage(dblocal), true);
        },
        error: function(result){  
            fungsiPesanGagal();
            return false;
        }
      });
}

//menampilkan data transformasi barang
function tampiltransformasibarang(data, isignoreview=false)
{
 var isview='';
 var isi ='', no=0,total=0, isundo=localStorage.getItem('undotransformasi'); isview=!(is_empty(localStorage.getItem('waktu')) || is_empty(localStorage.getItem('grupwaktu')));
 var dblocal = 'lstransformasiasal';
 if (isview && !isignoreview)
 {
     if(isundo==0)
     {
        $('#btnsimpantransformasibarang').remove();
     }
     $('#selectbarang').remove();
     loaddataviewtransformasibarang();
 }
 else
 {
    parent = 0;
    for(var i in data)
    {  
       var obj = JSON.parse(data[i]);
       if(obj.status!=='batal')
       {
        if(((isview && obj.jenisdistribusi == 'transformasiasal') || !isview))
        {       
                isi += '<tr class="bg bg-yellow"><th>No</th><th>Kode</th><th>Barang/Obat</th><th>Hargabeli</th><th>Stok</th><th>Distribusi</th><th style="width:70px;"></th><th>Satuan</th><th>Kadaluarsa</th><th>BatchNo</th></tr>';
                isi += '<tr><td>';
                if (!isview || isundo==1)
                {
                    isi += '<input type="hidden" id="idbarang'+i+'" value="'+obj.idbarang+'">\n\
                    <input type="hidden" name="idbarangpembelian[]" value="'+obj.idbarangpembelian+'">\n\
                    <input type="hidden" name="kadaluarsa[]" value="'+obj.kadaluarsa+'">\n\
                    <input type="hidden" name="batchno[]" value="'+obj.batchno+'">\n\
                    <input type="hidden" name="hargabeli[]" value="'+obj.hargabeli+'">\n\
                    <input type="hidden" name="barangkeluar[]" value="k">\n\
                    <input type="hidden" name="index[]" value="'+i+'">\n\
                    <input type="hidden" name="parentidbarangpembelian[]" value="'+obj.idbarang+'">'+ ++no +'</td>';
                }
                isi += '<td>'+ obj.kode +'</td>'
                    +'<td>'+ obj.namabarang +'</td>'
                    +'<td>'+ obj.hargabeli +'</td>'
                    +'<td>'+((!isview)?'<input type="hidden" id="stok'+i+'" value="'+obj.stok+'" />'+ obj.stok:'') +'</td>'
                    +'<td id="jumlahkeluar'+i+'" width="143px;">'+((!isview || isundo==1)?'<input id="jumlahambil'+i+'" name="jumlahkeluar[]" placeholder="jumlah distribusi" onkeyup="onkeyupJumlahKeluar(this.value,'+i+')" '+((isundo==1)?'value="'+obj.jumlah+'" readonly ':'')+' type="text"  class="form-control" rows="6" >':obj.jumlah) +' </td>'
                    +'<td> '+((!isview || isundo==1)?'<a onclick="addtransformasitujuan('+i+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Transformasi Tujuan"><i class="fa fa-random"></i></a> '+((!isview)? '<a onclick="batalkantransformasibarang('+i+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>':'' ):'')+'</td>'
                    +'<td>'+ obj.namasatuan +'</td>'
                    +'<td>'+ obj.kadaluarsa +'</td>'
                    +'<td>'+ obj.batchno +'</td>'
                    +'</tr>';
                isi+='<tr class="'+((isview || isundo==1)?'':'hidden')+'" id="parent'+i+'"  style="background-color:#ecf0f5"><td></td><th>Kode</th><th>Barang/Obat</th><th width="15%">Transformasi</th><th></th><th>Satuan</th></tr>';
                parent = i;
        }
        else
        {
            isi +='<tr id="rowbarangtujuan'+parent+'"><td></td><td><input type="hidden" name="barangmasuk'+parent+'[]" value="m">\n\<input type="hidden" name="idbarang'+parent+'[]" value="'+obj.idbarang+'">'+obj.kode+'</td><td>'+obj.namabarang+' exp:'+obj.kadaluarsa+'</td><td style="width:200px;">'+((isview && isundo==0)?obj.jumlah:'<input type="hidden" value="'+obj.idbarangpembelian+'" name="idbarangpembelianasal'+parent+'[]" /><input type="hidden" value="'+obj.jumlah+'" name="jumlahpembetulan'+parent+'[]" /><input name="jumlahdistribusi'+parent+'[]" placeholder="jumlah diberikan" name="diberikan[]" value="'+obj.jumlah+'"  rows="3">')+'</td><td></td><td>'+obj.namasatuan+'</td></tr>';  
        }
        $('#tbodybarangtransformasi').empty();
        ((isview)? $('#tbodybarangtransformasi').html(isi):$('#tbodybarangtransformasi').html(isi)); 
       }
     }
     $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd", orientation: "bottom"}).datepicker("setDate", 'now' ); //Initialize Date picker
     $('[data-toggle="tooltip"]').tooltip();
 }
}

function addtransformasitujuan(index)
{
    var modalSize = 'medium';
    var modalTitle = 'Cari Barang...';
    var modalContent = '<div class="form-group"><select onchange="onchange_addbarangtujuan(this.value,'+index+')" name="idbarang" class="form-control select2"></select></div>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: modalSize,
        buttons: {
            formReset:{
            	text: 'Tutup',
                btnClass: 'btn-danger',
                 action: function () {
                  caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbpwlname'); 
                }
            }
        },
        onContentReady: function () {
            caribarang($('select[name="idbarang"]'),'cmasterdata/jsonbrwlname');
        }
    });
}
function onkeyupJumlahKeluar(jumlah,index)
{
    if(parseInt(jumlah) > parseInt($('#stok'+index).val()))
    {
        notifExlamation('Jumlah melebihi stok yang ada.',$('#jumlahambil'+index).val($('#stok'+index).val()));
    }
}
function onchange_addbarangtujuan(data,index)
{ 
  var arrdata = split_to_array(data);
  var html='<tr id="rowbarangtujuan'+index+'"><td></td><td><input type="hidden" name="barangmasuk'+index+'[]" value="m"><input type="hidden" name="idbarang'+index+'[]" value="'+arrdata[0]+'">'+arrdata[1]+'</td><td>'+arrdata[2]+' exp:'+arrdata[6]+'</td><td style="width:200px;"><input type="hidden" value="'+arrdata[5]+'" name="jumlahpembetulan[]" /><input name="jumlahdistribusi'+index+'[]" placeholder="jumlah diberikan" name="diberikan[]" value="'+arrdata[5]+'"  rows="3"></td><td><a onclick="hapusbarangtujuan('+index+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a></td><td>'+arrdata[3]+'</td></tr>';
  $('#parent'+index).removeClass("hidden");
  $('#parent'+index).after(html);
}
function hapusbarangtujuan(i,isundo)
{ 
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi!',content: 'Yakin akan dihapus.?',
        buttons: {
            confirm: function () {
                $('#rowbarangtujuan'+i).remove();
            }
        }
    });
}
//batal atau hapus transformasi barang
function batalkantransformasibarang(index)
{
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi!',content: 'Yakin akan dihapus.?',
        buttons: {
            confirm: function () {
            var barang = getlocalstorage('lstransformasiasal');
            barang[index] = JSON.stringify({transformasiasal:'transformasiasal',status:'batal'});
            updatelocalstorage('lstransformasiasal',barang);
            tampiltransformasibarang(getlocalstorage('lstransformasiasal'));
            refresh_halaman();
            }
        }
    });
    
}
//mahmud, clear
//savetransformasibarang
function savetransformasibarang()
{
    var arrIndex=$("input[name='index[]']").map(function(){return $(this).val();}).get();
    for(var x in arrIndex)
    {
        if($('#jumlahambil'+arrIndex[x]).val()=='' || $('#jumlahambil'+arrIndex[x]).val()==0)
        {
            return  notifExlamation('Jumlah distribusi harap diisi.',$('#jumlahambil'+arrIndex[x]).focus());
        }
        else
        {
         var arrIndexChild=$("input[name='jumlahdistribusi"+arrIndex[x]+"[]']").map(function(){return $(this).val();}).get();
            for(var i in arrIndexChild)
            {
                   if(arrIndexChild[i]=='' || arrIndexChild[i]==0)
                   {
                       return  notifExlamation('Jumlah diberikan tidak boleh kosong..','');
                   }   
            }
        }
    }
    $('#Formtransformasi').submit();
}
//mahmud, clear
//HITUNG hutang belanja
function hitungTagihanBelanja()
{
    var jumlah = $('input[name="jumlah[]"]').map(function(){return $(this).val();}).get();
    var harga = $('input[name="harga[]"]').map(function(){return $(this).val();}).get();
    var hargappn = $('input[name="hargappn[]"]').map(function(){return $(this).val();}).get();
    var diskon = $('input[name="nominaldiskon[]"]').map(function(){return $(this).val();}).get();
    var tagihan=0, potongan=0, tagihanppn=0;
    for(var x in jumlah)
    {
      tagihan += parseFloat((jumlah[x]=='')? 0 :parseInt(unconvertToRupiah(jumlah[x]))) * parseInt(unconvertToRupiah(harga[x])); 
      tagihanppn += parseFloat((jumlah[x]=='')? 0 :parseInt(unconvertToRupiah(jumlah[x]))) * parseInt(unconvertToRupiah(hargappn[x])); 
      potongan += parseFloat(unconvertToRupiah(diskon[x]));
    } 
    var dpp =((parseInt(100) / parseInt(110)) * parseInt(tagihanppn));
    var ppn =(parseInt(10) / parseInt(100)) * parseInt(dpp);
    // var ppn = parseInt(tagihan) * 1.1;
    var totaltagihan = parseInt(tagihan) + parseInt(ppn) - parseInt(potongan);
    $('input[name="tagihan"]').val(convertToRupiah(tagihan.toFixed(0)));
    $('input[name="ppn"]').val(convertToRupiah(ppn.toFixed(0)));
    $('input[name="potongan"]').val(convertToRupiah(potongan.toFixed(0)));
    $('input[name="totaltagihan"]').val(convertToRupiah(totaltagihan.toFixed(0)));
}
//add view faktur belanja
function tampilfakturbelanja(result='')
{
    var data = (result=='')?'':result.faktur;
    if(data!=''){addlocalstorage('fakturbelanjabarang',JSON.stringify({idbarangfaktur:result.idfaktur,nofaktur:data.nofaktur,tanggalfaktur:data.tanggalfaktur,tanggaljatuhtempo:data.tanggaljatuhtempo,statusfaktur:data.statusfaktur,idbarangdistributor:data.idbarangdistributor,distributor:data.distributor,tagihan:data.tagihan,dibayar:data.dibayar,kekurangan:data.kekurangan,nopesan:data.nopesan}));}
    var faktur = getlocalstorage('fakturbelanjabarang');
    var obj = (faktur==undefined)?'':JSON.parse(faktur[0]);
    if(obj.statusfaktur=='diterima'){ $('input[name="setterimabarang"]').prop('checked', true); $('input[name="setterimabarang"]').attr('readonly'); }
    $('input[name="statusfaktur"]').val(obj.statusfaktur);
    $('input[name="idbarangfaktur"]').val(obj.idbarangfaktur);
    $('#menubatal').after((obj!='' && obj.statusfaktur !='diterima') ? ' <a class="btn btn-danger btn-lg" id="hapusfaktur"> <i class="fa fa-minus-circle"></i> BATALKAN FAKTUR</a>' : '');
    $('input[name="nofaktur"]').val(obj.nofaktur);
    $('input[name="nopesan"]').val(obj.nopesan);
    $('input[name="dibayar"]').val((faktur==undefined)?0:obj.dibayar);
    $('select[name="idbarangdistributor"]').html(((faktur==undefined)?'<option value="0">Pilih</option>':'<option value="'+obj.idbarangdistributor+'">'+obj.distributor+'</option>'))
    $('#tanggalfaktur').datepicker({autoclose: true,format: "yyyy-mm-dd", orientation: "bottom"}).datepicker("setDate", (faktur==undefined) ? 'now' : obj.tanggalfaktur  ); //Initialize Date picker
    $('#tanggaljatuhtempo').datepicker({autoclose: true,format: "yyyy-mm-dd", orientation: "bottom"}).datepicker("setDate", (faktur==undefined) ? 'now' : obj.tanggaljatuhtempo  ); //Initialize Date picker
}
//mahmud, clear
function pembayaranBelanja(tagihan,dibayar)
{
    var index = setPembayaranBelanja(tagihan,dibayar);
    var status = ['Lunas','Belum Lunas','Kelebihan Bayar'];
    return status[index];
}
//mahmud, clear
function statusPembayaranBelanja(tagihan,dibayar)
{
    var index = setPembayaranBelanja(tagihan,dibayar);
    var status = ['lunas','belumlunas','kelebihanbayar'];
    return status[index];
}
$(document).on('click','#hapusfaktur',function(){
    var d = $('input[name="idbarangfaktur"]').val();
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi',content: 'Faktur Akan Dihapus.?',
        buttons: { hapus: function () { 
          $.ajax({url:base_url+'cmasterdata/updatefaktur_ad_barangpembelian',type:"POST",dataType:"JSON",data:{idbf:d,mode:'hapus'},
            success: function(result){
              if(result){
                window.location.href=base_url+"cmasterdata/belanjabarang";
              }
            },
            error: function(result){ stopLoading();fungsiPesanGagal(); return false;}
          }); 
        }, batal: function () { } }
    });
});
//mahmud, clear
function setPembayaranBelanja(tagihan,dibayar)
{
    var tagihan = parseFloat(tagihan);
    var dibayar = parseFloat(dibayar);
    var index = (tagihan < dibayar)? 2 : (tagihan > dibayar) ? 1 : 0 ;
    return index;
}
//mahmud, clear
function pilihdistributor() //cari distributor
{
    $('select[name="idbarangdistributor"]').select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax:{url: base_url+"cmasterdata/json_pilihdistributor",dataType: 'json',delay: 150,cache: false,data: function (params) {return {q: params.term,page: params.page || 1,};},processResults: function(data, params) {var page = params.page || 1;return {results: $.map(data, function (item) { return {id: item.idbarangdistributor, text: item.distributor}}),pagination: {more: true}};},}
    });
}
//mahmud, clear
