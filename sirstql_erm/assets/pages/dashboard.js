var table;
$(function () {
  switch($('#view_page').val()) {
    case 'dashboard':
      cakupanPoli();
      break;
    default:
      get_datapoli('#tampilpoli','');
      $('.select2').select2();
      $('#tahun').datepicker({autoclose: true,format: "yyyy",orientation: "bottom", viewMode: "years",minViewMode: "years"}).datepicker('setDate','now');// bind to events
      dashboard_bypoli();
      $('#tahunbulan').datepicker({autoclose: true, endDate: new Date(), format: "M yyyy",viewMode: "months",minViewMode: "months",orientation: "top"}).datepicker("setDate",'now'); //Initialize Date picker;
  }
});

$(document).on('click','#tampiltindakanall',function(){
    startLoading();
    $('#judultindakanall').text('TINDAKAN RAWAT JALAN '+$('#tahunbulan').val());
    $.ajax({
        url:base_url+"cdashboard/getdatatindakanall",
        type:'POST',
        dataType:'JSON',
        data:{tahunbulan:$('#tahunbulan').val()},
        success: function(data){
            stopLoading();
            var rows = '';
            var harga = 0;
            var jumlah = '';
            var namaicd = '';
            var namaicd_new = '';
            var kdicd   = '';
            var no = 0;
            var subtotal = 0;
            var total = 0;
            
            for(var x in data)
            {

                if( ( ((data[x].icd == null) ? data[x].namapaketpemeriksaan : data[x].namaicd ) != namaicd && namaicd != '') && (harga !=data[x].total && harga!='') )
                {
                    subtotal += jumlah * harga; 
                    total += jumlah * harga; ;
                    rows += '<tr><td>'+ ++no +'</td><td>'+ kdicd +'</td><td>'+ namaicd +'</td><td>'+ convertToRupiah(jumlah) +'</td><td>'+ convertToRupiah(harga) +'</td><td>'+convertToRupiah(subtotal)+'</td><td>'+convertToRupiah(total)+'</td></tr>';
                    jumlah = 0;
                    subtotal = 0;
                }
                
                
                namaicd = ((data[x].icd == null) ? data[x].namapaketpemeriksaan : data[x].namaicd );
                kdicd   = ((data[x].icd == null) ? data[x].idpaketpemeriksaan : data[x].icd );
                
                jumlah++;
                harga   = data[x].total;
            }
            var table =  '<table class="table table-bordered table-hover"><thead><tr style="background:#edd38c;"><th>No</th><th>ICD</th><th>Nama Icd</th><th>Jumlah</th><th>@Harga</th><th>Subtotal</th><th>Total</th></tr></thead><tbody>'+rows+'</tfooter></table>';
            $('#tindakanall').html(table);
            $('.table').dataTable();

        },
        error: function(data){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    }); 
});

$(document).on('click','#unduhtindakanall',function(){
    var url = base_url+"cdashboard/unduhdatatindakanall";
    var form = $('<form action="' + url + '" method="post">' +
      '<input type="hidden" name="tahunbulan" value="'+$('#tahunbulan').val()+'" />' +
      '</form>');
    $('body').append(form);
    form.submit();
});

$(document).on('click','#unduhcakupanpoli',function(){
    window.location.href = base_url+"cdashboard/unduh_cakupanpoli/"+$('#tahun').val()+"/"+$('#tampilpoli').val();
})


function dashboard_bypoli(idunit)
{
  startLoading();
  $.ajax({
      url:base_url+"cdashboard/getdashboarddata_bypoli",
      type:'POST',
      dataType:'JSON',
      data:{unit:idunit,tahun:$('#tahun').val()},
      success: function(data){
          stopLoading();
          proporsiLamaBaru(data);
          cakupanPoli(data);
          proporsiJaminanPasien(data);
          // diagnosatindakan10besar(idunit);
          //diagnosa10besar(data);
          
      },
      error: function(data){
          stopLoading();
          fungsiPesanGagal();
          return false;
      }
  }); 

}

$(document).on('click','#tampilcakupanpoli',function(){
    var idunit = $('#tampilpoli').val();
    dashboard_bypoli(idunit);
});

function tampildiahnosa10besar()
{
    var idunit = $('#tampilpoli').val();
    diagnosatindakan10besar(idunit);
}
function diagnosatindakan10besar(idunit)
{
    startLoading();
    $.ajax({
      url:base_url+"cdashboard/diagnosatindakan10besar",
      type:'POST',
      dataType:'JSON',
      data:{unit:idunit,tahun:$('#tahun').val()},
      success: function(data){
        stopLoading();
        diagnosa10besar(data);
          
      },
      error: function(data){
          stopLoading();
          fungsiPesanGagal();
          return false;
      }
    }); 
  
}

function cakupanPoli(hasil)
{  
  $('#cakupan').empty();
  $('#cakupan').html('<div class="box-header with-border"><h3 class="box-title" style="font-size:13px;">CAKUPAN '+((hasil.unit)==null?'':hasil.unit['namaunit'])+' '+hasil.tahun2+'</h3></div><div class="box-body"><div class="chart"><canvas id="cakupanPoli" style="height:250px"></canvas></div></div>');
  var ctx = $('#cakupanPoli');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: hasil.bulan,
        datasets: 
        [
          
          {
              label: 'TARGET '+hasil.tahun2,
              data: hasil.targetcakupan,
              backgroundColor: [
                  'rgba(255, 255, 255, 0)'
              ],
              borderColor: [
                  'rgba(120, 119, 119, 1)'
              ],
              // Changes this dataset to become a line
              type: 'line'
          },
          {
              label: hasil.tahun1,
              data: hasil.cakupan1,
              backgroundColor: hasil.bgcakupan1,
          },
          {
              label: hasil.tahun2,
              data: hasil.cakupan2,
              backgroundColor: hasil.bgcakupan2,
          }
          
        ],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
}

function proporsiJaminanPasien(hasil)
{
  $('#jaminan').empty();
  $('#jaminan').html('<div class="box-header with-border"><h3 class="box-title" style="font-size:13px;">PROPORSI JAMINAN PASIEN '+((hasil.unit)==null?'':hasil.unit['namaunit'])+ ' '+hasil.tahun2+'</h3></div><div class="box-body"><div class="chart"><canvas id="proporsiJaminan" style="height:250px"></canvas></div></div>');
  var ctx = $('#proporsiJaminan');
  var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: hasil.pjcarabayar,
        datasets: 
        [
          
          {
            data: hasil.pjdata,
            backgroundColor:hasil.pjwarna
            
          }
          
        ],
    },
});
}

function proporsiLamaBaru(hasil)
{
  $('#pasien').empty();
  $('#pasien').html('<div class="box-header with-border"><h3 class="box-title" style="font-size:13px;">PROPORSI PASIEN LAMA DAN BARU '+((hasil.unit)==null?'':hasil.unit['namaunit'])+ ' '+hasil.tahun2+'</h3></div><div class="box-body"><div class="chart"><canvas id="proporsiLamaBaru" style="height:250px"></canvas></div></div>');
  var ctx = $('#proporsiLamaBaru');
  var myChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
//        labels:['Januari','Februari'],
//        datasets:[{label:'Pasien Lama',data:[20,80],backgroundColor:['rgba(74, 148, 193, 1)','rgba(74, 148, 193, 1)']},{label:'Pasien Baru',data:[80,20],backgroundColor:['rgba(243, 156, 17, 1)','rgba(243, 156, 17, 1)']}]
        labels: hasil.bulan,
        datasets:[{label: 'Pasien Lama',data: hasil.plama,backgroundColor: hasil.blama,},{label: 'Pasien Baru',data: hasil.pbaru,backgroundColor: hasil.bbaru,}],
    },
    options: {
        scales: {
           xAxes: [{stacked:true}],
           yAxes:[{stacked:true}],
        },
    tooltips: {
      enabled: true,
      callbacks: {
        label: function(tooltipItem, data) {
          var label = data.datasets[tooltipItem.datasetIndex].label;
          var val = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return label + ' : ' + val + ' %';
        }
      }
    },
    // showTooltips: false,
    // onAnimationComplete: function (f) {
    //   conso

    //     var ctx = this.chart.myChart;
    //     ctx.font = this.scale.font;
    //     ctx.fillStyle = this.scale.textColor
    //     ctx.textAlign = "center";
    //     ctx.textBaseline = "bottom";

    //     this.datasets.forEach(function (dataset) {
    //         dataset.points.forEach(function (points) {
    //             ctx.fillText(points.value, points.x, points.y - 10);
    //         });
    //     })
    // }
  }
});
} 

// diagnosa 10 besar
function diagnosa10besar(hasil)
{
 var unit = ( $('#tampilpoli option:selected').text() == 'Farmasi') ? '' : $('#tampilpoli option:selected').text() ;
 $('#judulicd10').html('<b>10 DIAGNOSIS TERBANYAK '+unit +' ' +$('#tahun').val()+' </b>');
 $('#judulicd9').html('<b>10 TINDAKAN TERBANYAK '+unit + ' '+$('#tahun').val()+' </b>');
 var isitable='',header='',judul='',no=0,isitable2='',judul2='', modulus=0;
 for( a in hasil.bln10besar)
 {
   modulus = a % 2;
   var modbg=((modulus > 0) ? 'class="bg bg-info"' : '');
   if(hasil.diagnosa10bsr[a].length!=0)
   {
     header+='<td colspan="4" '+ modbg +'>'+hasil.bln10besar[a]+'</td>';
     judul +='<td '+ modbg +'>NO</td><td '+modbg+' >DIAGNOSA</td><td '+modbg+' >JUMLAH</td><td '+modbg+' >PERSENTASE</td>';
     judul2 +='<td '+ modbg +'>NO</td><td '+modbg+' >TINDAKAN</td><td '+modbg+' >JUMLAH</td><td '+modbg+' >PERSENTASE</td>';
   }
 }
 for(x=0; x<10; x++ ){
   isitable+='<tr>';
   isitable2+='<tr>';
   ++no;
   for(i=0; i<12;i++)
     {
      modulus = i % 2;
      modbg=((modulus > 0) ? 'class="bg bg-info"' : '');
       if(hasil.diagnosa10bsr[i].length!=0){
         isitable+='<td '+modbg+'>'+ no +'</td><td '+modbg+'>'+((hasil.diagnosa10bsr[i][x]==undefined)?'':hasil.diagnosa10bsr[i][x].icd)+' '+((hasil.diagnosa10bsr[i][x]==undefined)?'':hasil.diagnosa10bsr[i][x].namaicd)+'</td><td '+modbg+'>'+((hasil.diagnosa10bsr[i][x]==undefined)?'':hasil.diagnosa10bsr[i][x].total)+'</td><td '+modbg+'>'+((hasil.diagnosa10bsr[i][x]==undefined)?'':angkadesimal(hasil.diagnosa10bsr[i][x].percentase,2)+'%')+'</td>';
         isitable2+='<td '+modbg+'>'+ no +'</td><td '+modbg+'>'+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].icd)+' '+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].namaicd)+'</td><td '+modbg+'>'+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].total)+'</td><td '+modbg+'>'+((hasil.tindakan10bsr[i][x]==undefined)?'':angkadesimal(hasil.tindakan10bsr[i][x].percentase,2)+'%')+'</td>';
       }
     }
   isitable+='</tr>';
   isitable2+='</tr>';
 }
 var table   = '<table border="1" class="table table-striped table-bordered" style="width:100%"><tr>'+header+'</tr><tr>'+judul+'</tr>'+isitable+'<table>';
 var table2   = '<table border="1" class="table table-striped table-bordered" style="width:100%"><tr>'+header+'</tr><tr>'+judul2+'</tr>'+isitable2+'<table>';
 $('#diagnosa10besar').empty();
 $('#diagnosa10besar').html(table);
 $('#tindakan10besar').empty();
 $('#tindakan10besar').html(table2);
}
