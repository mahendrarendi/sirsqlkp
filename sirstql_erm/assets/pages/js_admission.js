$(function () {
  //Initialize Select2 Elements
    $('.select2').select2()
  //Initialize Date picker
    $('#datepicker').datepicker
    ({
        autoclose: true,
        format: "yyyy-mm-dd",
        orientation: "bottom"
    })
})

///tambah penanggung jawab
function tambah_penanggung()
{
    var modalTitle = 'Add Data Penanggung';
    var modalContent = '<form action="" id="FormPenanggungJawab">' +
                            '<input type="hidden" name="normPenanggung" id="normPenanggung" />'+
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>No.Identitas*</label>' +
                                '<input type="text" name="noidentitas" id="noidentitas" placeholder="No.Identitas" class="form-control"/>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Agama</label>' +
                                '<select name="agama" id="listAgama" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+

                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Nama Penanggung*</label>' +
                                '<input type="text" name="namapenanggung" id="namapenanggung" placeholder="Nama penanggung" class="form-control"/>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>GolDarah</label>' +
                                '<select name="golongandarah" id="listGolDarah" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+

                            '<div class="form-group">' +
                                '<div class="col-sm-3" >' +
                                '<label>JnsKelamin</label>' +
                                '<select name="jeniskelamin" id="listJeniskelamin" class="form-control select2">' +
                                '</select>' +
                                '</div>'+

                                '<div class="col-sm-5">' +
                                '<label>TanggalLahir*</label>' +
                                '<input type="text" name="tanggallahir" placeholder="Tanggal lahir" id="tanggallahir" class="form-control"/>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Rhesus</label>' +
                                '<select name="rhesus" id="listRhesus" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+

                            '<div class="form-group">' +
                                
                                '<div class="col-sm-4">' +
                                '<label>Tempat Lahir</label>' +
                                '<input type="text" name="tempatlahir" placeholder="Tempat Lahir" class="form-control"/>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Pekerjaan</label>' +
                                '<select name="pekerjaan" id="listPekerjaan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Pendidikan</label>' +
                                '<select name="pendidikan" id="listPendidikan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+


                            '<div class="form-group">' +
                                '<div class="col-sm-4">' +
                                '<label>No.Telpon</label>' +
                                '<input type="text" name="notelpon" placeholder="No.Telpon" class="form-control"/>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Hubungan Keluarga</label>' +
                                '<select name="hubungan" id="listHubungan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Status</label>' +
                                '<select name="status" id="listStatus" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+

                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Alamat <a href="#" onclick="copy_alamat_pasien()">&nbsp;&nbsp;&nbsp;copy alamat pasien</a></label>' +
                                '<textarea class="form-control" rows="1" placeholder="Alamat sesuai identitas" name="alamatpenanggung"></textarea>' +
                                '</div>'+

                                '<div class="col-sm-4" >' +
                                '<label>Kelurahan</label>' +
                                '<select name="kelurahan" id="listKelurahan" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +
                            '<div class="col-sm-12" style="margin:4px 0px;"></div>'+
                        '</form>';

    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    //jika no identitas kosong
                    if($('#noidentitas').val()==='')
                    {
                            alert_empty('noidentitas');
                            return false;
                    }
                    //jika no namapenanggung kosong
                    else if($('#namapenanggung').val()==='')
                    {
                            alert_empty('nama penanggung');
                            return false;
                    }
                    //jika no tanggallahir kosong
                    else if($('#tanggallahir').val()==='')
                    {
                            alert_empty('tanggal lahir');
                            return false;
                    }
                    //selain itu
                    else
                    {
                        $.ajax
                        ({
                            type: "POST",
                            url: 'save_penanggungjawab',
                            data: $("#FormPenanggungJawab").serialize(),
                            dataType: "JSON",
                            success: function(result) {
                                notif(result.status, result.message);
                                return refreshPenanggung();

                            },
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        //tampilkan data enum = add penanggung jawab 
        $('#normPenanggung').val($('#norm').val());
        tampil_data_enum_pasien();
        $('.select2').select2();
        $('#tanggallahir').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            orientation: "bottom"
        })
        // get_data_propinsi();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

// menampilkan data penanggung
function refreshPenanggung()
{
    var norm = $('#norm').val();
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_penanggung',
        type:'POST',
        dataType:'JSON',
        data:{norm:norm},
        success: function(result){
            $('#penanggung').empty();
            for(i in result)
            {
                    select = select + '<option value="'+ result[i].idpasienpenanggungjawab +'" >' + result[i].namalengkap +' (' + result[i].namahubungan +')';
            }
            $('#penanggung').html(select);
            $('.select2').select2();

        },
        error: function(result){
            console.log(result);
        }
    }); 
}

//kopy alamat pasien
function copy_alamat_pasien()
{
    var alamat = $('textarea[name="alamat"]').val();
    $('textarea[name="alamatpenanggung"]').val(alamat);
}
//menampilkan data enum pasien = add penanggungjawab
function tampil_data_enum_pasien()
{
    $.ajax
          ({
                url:'tampil_pasien',
                type:'POST',
                dataType:'JSON',
                data:{id:''},
                success: function(result){
                    //menampilkan data agama pasien
                    edit_data_enum($('#listAgama'),result.agama,'');
                    //menampilkan data jeniskelamin pasien
                    edit_data_enum($('#listJeniskelamin'),result.jeniskelamin,'');
                    //menampilkan data golongandarah pasien
                    edit_data_enum($('#listGolDarah'),result.golongandarah,'');
                    //menampilkan data rhesus pasien
                    edit_data_enum($('#listRhesus'),result.rhesus,'');
                    //menampilkan data perkawinan pasien
                    edit_data_enum($('#listStatus'),result.perkawinan,'');
                    //menampilkan data kelurahan
                    edit_dropdown_kelurahan($('#listKelurahan'),result.kelurahan,'');
                    //menampilkan data pendidikan
                    edit_dropdown_pendidikan($('#listPendidikan'),result.pendidikan,'');
                    //menampilkan data pekerjaan
                    edit_dropdown_pekerjaan($('#listPekerjaan'),result.pekerjaan,'');
                    //menampilkan data hubungan
                    edit_dropdown_hubungan($('#listHubungan'),result.hubungan,'');  
                    
                },
                error: function(result){
                    console.log(result);
                }
            });
}

//form tambah geografi kelurahan
function tambah_kelurahan()
{
    var modalSize = 'small';
    var modalTitle = 'Add Data Kelurahan';
    var modalContent = '<form action="" id="FormKelurahan">' +
                            '<div class="form-group">' +
                                '<label>Provinsi</label>' +
                                '<div id="ListProvinsi" style="display:relative;">' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" id="propinsi" name="propinsi">'+
                        		'<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>'+
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>Kabupaten</label>' +
                                '<div id="ListKabupaten">' +
                                '<select name="kabupaten" class="kabupaten form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>Kecamatan</label>' +
                                '<div id="ListKecamatan">' +
                                '<select name="kecamatan" class="kecamatan form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                                '</div>'+
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>Kelurahan</label>' +
                                '<input type="text" name="kelurahan" placeholder="Kelurahan" id="Kelurahan" class="form-control"/>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    //jika data provinsi kosong
                	if($('#propinsi').val()==='' || $('#propinsi').val()==='0')
                	{
                            alert_empty('Propinsi');
                            return false;
                	}
                    //jika data kabupaten kosong
                	else if($('#kabupaten').val()==='' || $('#kabupaten').val()==='0')
                	{
                            alert_empty('Kabupaten Kota');
                            return false;
                	}
                    //jika data kecamatan kosong
                	else if($('#kecamatan').val()==='' || $('#kecamatan').val()==='0')
                	{
                            alert_empty('Kecamatan');
                            return false;
                	}
                    //jika data kelurahan kosong
                	else if( ! $('#Kelurahan').val() )
                	{
                            alert_empty('Kelurahan');
                            return false;
                	}
                    //selain itu
                	else
                	{
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'save_geografikelurahan', //alamat controller yang dituju (di js base url otomatis)
                                data: $("#FormKelurahan").serialize(), //
                                dataType: "JSON", //tipe data yang dikirim
                                //jika  berhasil
                                success: function(result) {
                                    notif(result.status, result.message);
                                    return refreshKelurahan();

                                },
                                //jika error
                                error: function(result) {
                                    console.log(result.responseText);
                                }
                            });
                	}
                }
            },
            //menu back
            formReset:{
            	text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        //ketika form di tampilkan
        //inisialisasi dropdown select2 
        $('.select2').select2();
        //ambil data provinsi
        get_data_propinsi();

        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//fungsi ambil data geografi
function get_data_propinsi()
{
    var select='';
    $.ajax({
        url:'cari_propinsi',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#ListProvinsi').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idpropinsi +'" >' + result[i].namapropinsi +'</option>';
            }
            $('#ListProvinsi').html('<select onchange="get_data_kabupaten(this.value)" class="select2 form-control" style="width:100%" id="propinsi" name="propinsi">'+
                            '<option value="0">Pilih</option>' +
                            select +
                            '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    });	
}

//fungsi ambil data geografi
function get_data_kabupaten(value)
{
    var select='';
    $.ajax({
        url:'cari_kabupaten',
        type:'POST',
        dataType:'JSON',
        data:{x:value},
        success: function(result){
            $('#ListKabupaten').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idkabupatenkota +'" >' + result[i].namakabupatenkota +'</option>';
            }
            $('#ListKabupaten').html('<select onchange="get_data_kecamatan(this.value)" class="select2 form-control" style="width:100%" id="kabupaten" name="kabupaten">'+
                             '<option value="0">Pilih</option>' +
                             select +
                            '</select>');

            $('#ListKecamatan').empty();
            $('#ListKecamatan').html('<select onchange="get_data_kabupaten(this.value)" class="select2 form-control" style="width:100%" id="propinsi" name="propinsi">'+
                                     '<option value="0">Pilih</option>' +
                                     '</select>');
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    });	
}

//fungsi ambil data geografi
function get_data_kecamatan(value)
{
	var select='';
	$.ajax({
            url:'cari_kecamatan',
            type:'POST',
            dataType:'JSON',
            data:{x:value},
            success: function(result){
                $('#ListKecamatan').empty();
                for(i in result)
                {
                select = select + '<option value="'+ result[i].idkecamatan +'" >' + result[i].namakecamatan +' '+ result[i].kodepos +'</option>';
                }
                $('#ListKecamatan').html('<select class="select2 form-control" style="width:100%" id="kecamatan" name="kecamatan">'+
                                 '<option value="0">Pilih</option>' +
                                 select +
                                '</select>');
                $('.select2').select2();

            },
            error: function(result){
                console.log(result);
            }
	});	
}

// menampilkan data kelurahan
function refreshKelurahan()
{
    var select='<option value="0">Pilih</option>';
    $.ajax({
        url:'cari_kelurahan',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#kelurahan').empty();
            for(i in result)
            {
                    select = select + '<option value="'+ result[i].iddesakelurahan +'" >' + result[i].namadesakelurahan +' '+ result[i].namakecamatan +' '+ result[i].namakabupatenkota +' '+ result[i].namapropinsi +' '+ result[i].kodepos+'</option>';
            }
            $('#kelurahan').html(select);
            $('.select2').select2();

        },
        error: function(result){
            console.log(result);
        }
    });	
}

// entri pendaftaran
function entri_pendaftaran()
{
    if($('input[name="noidentitas"]').val()==='')
    {
        $('input[name="noidentitas"]').focus();
        alert_empty('no identitas');
    }
    else if($('input[name="namapasien"]').val()==='')
    {	
        $('input[name="namapasien"]').focus();
        alert_empty('nama pasien');
    }
    else if($('select[name="jeniskelamin"]').val()=='0')
    {	
        alert_empty('no jenis kelamin');
    }
    else if($('input[name="tanggallahir"]').val()==='')
    {	
        $('input[name="tanggallahir"]').focus();
        alert_empty('no tanggal lahir');
    }
    else if($('textarea[name="alamat"]').val()==='')
    {	
        $('textarea[name="alamat"]').focus();
        alert_empty('alamat');
    }
    else
    {
        $.ajax({
        url:'save_pendaftaran_poli',
        type:'POST',
        dataType:'JSON',
        data:$('#FormPendaftaranPoli').serialize(),
        success: function(result){
            console.log(result);
            // jika simpan berhasil
            if(result.norm)
            {
                $('input[name="pilihpasein"]').val(result.norm);
                $('input[name="norm"]').val(result.norm);
                $('input[name="idperson"]').val(result.person);
                //menampilkan poliklinik
                edit_dropdown_poliklinik($('#listPoliklinik'),result.poliklinik,'');
                //menampilkan penanggung
                edit_dropdown_penanggung($('#listPenanggung'),result.penanggung,'');
            }

            if(result.noantrian)
            {
                //menampilkan data antrian
                tampil_data_antrian($('#listAntrian'),result.noantrian);
            }
            // console.log(result.norm,' ',result.noantrian);
            notif(result.pesan.status,result.pesan.message);
            
        },
        error: function(result){
            console.log(result);
        }
    });
        
        ;
    }
}

//fungsi kosongkan inputan
function kosongkanFormPendaftaran()
{
    $('input[name="idperson"]').val('');
    $('input[name="norm"]').val('');
    $('input[name="noidentitas"]').val('');
    $('input[name="namapasien"]').val('');
    $('input[name="tempatlahir"]').val('');
    $('input[name="tanggallahir"]').val('');
    $('input[name="notelepon"]').val('');
    $('input[name="tanggallahir"]').val('');
    $('textarea[name="alamat"]').val('');

    $.ajax
      ({
            url:'tampil_pasien',
            type:'POST',
            dataType:'JSON',
            data:{id:''},
            success: function(result){

            //menampilkan data agama pasien
            edit_data_enum($('#agama'),result.agama,'');
            //menampilkan data jeniskelamin pasien
            edit_data_enum($('#jeniskelamin'),result.jeniskelamin,'');
            //menampilkan data golongandarah pasien
            edit_data_enum($('#golongandarah'),result.golongandarah,'');
            //menampilkan data rhesus pasien
            edit_data_enum($('#rhesus'),result.rhesus,'');
            //menampilkan data perkawinan pasien
            edit_data_enum($('#perkawinan'),result.perkawinan,'');
            //menampilkan data kelurahan
            edit_dropdown_kelurahan($('#kelurahan'),result.kelurahan,'');
            //menampilkan data pendidikan
            edit_dropdown_pendidikan($('#pendidikan'),result.pendidikan,'');
            //menampilkan data pekerjaan
            edit_dropdown_pekerjaan($('#pekerjaan'),result.pekerjaan,'');
            },
        error: function(result){
            console.log(result);
        }
    }); 
    //menampilkan data bpjs
    $('input[name="nojkn"]').val('');
    $('input[name="jenispeserta"]').val('');
    $('input[name="kelas"]').val('');
    $('input[name="tanggalcetak"]').val('');
    $('textarea[name="informasi"]').val('');  
    //menampilkan poliklinik
    edit_dropdown_poliklinik($('#listPoliklinik'),'','');
    //menampilkan penanggung
    edit_dropdown_penanggung($('#listPenanggung'),'','');
    //menampilkan data antrian
    tampil_data_antrian($('#listAntrian'));
    $('.select2').select2();
}
// tampilkan data pasien
$("#pilihpasein").on("keydown", function (e) {
    if (e.keyCode == 13) {
        var value = $('#pilihpasein').val();
        var select;
        var selected;
        var i;
        //jika tidak ada data inputan
        if(!value)
        {
            kosongkanFormPendaftaran();
        }
        //selain itu
        else
        {
            $.ajax({
                url:'tampil_pasien',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                    // jika pasien data pasien kosong
                    if(result.pasien==null)
                    {
                        kosongkanFormPendaftaran();
                    }
                    // selain itu
                    else
                    {
                        $('input[name="idperson"]').val(result.pasien['idperson']);
                        $('input[name="norm"]').val(result.bpjs['norm']);
                        $('input[name="noidentitas"]').val(result.pasien['nik']);
                        $('input[name="namapasien"]').val(result.pasien['namalengkap']);
                        $('input[name="tempatlahir"]').val(result.pasien['tempatlahir']);
                        $('input[name="tanggallahir"]').val(result.pasien['tanggallahir']);
                        $('input[name="notelepon"]').val(result.pasien['notelpon']);
                        $('input[name="tanggallahir"]').val(result.pasien['tanggallahir']);
                        $('textarea[name="alamat"]').val(result.pasien['alamat']);
                        //menampilkan data agama pasien
                        edit_data_enum($('#agama'),result.agama,result.pasien['agama']);
                        //menampilkan data jeniskelamin pasien
                        edit_data_enum($('#jeniskelamin'),result.jeniskelamin,result.pasien['jeniskelamin']);
                        //menampilkan data golongandarah pasien
                        edit_data_enum($('#golongandarah'),result.golongandarah,result.pasien['golongandarah']);
                        //menampilkan data rhesus pasien
                        edit_data_enum($('#rhesus'),result.rhesus,result.pasien['rh']);
                        //menampilkan data perkawinan pasien
                        edit_data_enum($('#perkawinan'),result.perkawinan,result.pasien['statusmenikah']);
                        //menampilkan data kelurahan
                        edit_dropdown_kelurahan($('#kelurahan'),result.kelurahan,result.pasien['iddesakelurahan']);
                        //menampilkan data pendidikan
                        edit_dropdown_pendidikan($('#pendidikan'),result.pendidikan,result.pasien['idpendidikan']);
                        //menampilkan data pekerjaan
                        edit_dropdown_pekerjaan($('#pekerjaan'),result.pekerjaan,result.pasien['idpekerjaan']);  
                        //menampilkan data bpjs
                        $('input[name="nojkn"]').val(result.bpjs['idbpjs']);
                        $('input[name="jenispeserta"]').val(result.bpjs['idjenispeserta']);
                        $('input[name="kelas"]').val(result.bpjs['idkelas']);
                        $('input[name="tanggalcetak"]').val(result.bpjs['tglcetakkartu']);
                        $('textarea[name="informasi"]').val(result.bpjs['informasi']);  
                        //menampilkan poliklinik
                        edit_dropdown_poliklinik($('#listPoliklinik'),result.poliklinik,'');
                        //menampilkan penanggung
                        edit_dropdown_penanggung($('#listPenanggung'),result.penanggung,result.pasien['idpasienpenanggungjawab']);
                        //menampilkan data antrian
                        tampil_data_antrian($('#listAntrian'));
                    }
                    
                    
                },
                error: function(result){
                    console.log(result);
                }
            });

        }
        
          
    }
});
// cari data pasien
$("#pilihpasein").on("keyup", function (e) {
    if (e.keyCode != 13) {
        $('#ListPasien').empty();
        var value = $('#pilihpasein').val();
        var datalist='';
        var i=0;
        // console.log('tes cari', value);
        if(value)
        {
            $.ajax({
                url:'cari_pasien',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                // console.log(result);
                    for(i in result)
                    {

                        datalist = datalist + '<option value="'+ result[i].norm+'-'+ result[i].nojkn+'">';
                    }
                    $('#ListPasien').html(datalist);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
        
    }
});
// fungsi edit data enum (idhtml,dataenum,datayangterpilih)
function edit_data_enum(column,data,selected_data)
{
    var select='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i]==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i] +'" '+ selected +' >' + data[i] +'</option>';
    }
    column.html(select);
}
//fungsi menampilkan data antrian
function tampil_data_antrian(column,no='')
{
    column.empty();
    column.html('No.Antrian:' + no);
}
//fungsi edit penanggung
function edit_dropdown_penanggung(column,data,selected_data)
{
    // console.log(name);
    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(i in data)
        {
            if(data[i].idpasienpenanggungjawab==selected_data){ selected='selected'; }
            else{selected='';}
            select = select + '<option value="'+ data[i].idpasienpenanggungjawab +'" '+ selected +' >' + data[i].namalengkap +' (' + data[i].namahubungan +')</option>';
        }
        column.html('<label for="" class="col-sm-1 control-label">Penanggung</label>'+
                    '<div class="col-sm-3">'+
                    '<select class="select2 form-control" style="width:100%" id="penanggung" name="penanggung">'+
                    '<option value="0">Pilih</option>'+ select +
                    '</select>'+
                    '</div>'+
                    '<a onclick="tambah_penanggung()" data-toggle="tooltip" data-original-title="Tambah Penanggung Jawab" class="btn btn-default btn-sm"><i class="fa fa-plus-square"></i></a>'+
                    '<label style="padding-left:30px;" class="text text-blue" id="listAntrian"></label>');

        $('.select2').select2();
        $('[data-toggle="tooltip"]').tooltip()
    }
    
}

//fungsi edit poliklinik
function edit_dropdown_poliklinik(column,data,selected_data)
{
    // console.log(name);
    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(i in data)
        {
            if(data[i].idunit==selected_data){ selected='selected'; }
            else{selected='';}
            select = select + '<option value="'+ data[i].idunit +'" '+ selected +' >' + data[i].namaunit +'</option>';
        }
        column.html('<label for="" class="col-sm-1 control-label">Poliklinik</label>'+
                    '<div class="col-sm-3">'+
                    '<select class="select2 form-control" style="width:100%" id="poliklinik" name="poliklinik">'+
                    '<option value="0">Pilih</option>'+ select +
                    '</select>'+
                    '</div>'+
                    '<div id="listPenanggung"></div>');
        $('.select2').select2();
    }
    
}

//fungsi edit dropdown
function edit_dropdown_hubungan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idhubungan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idhubungan +'" '+ selected +' >' + data[i].namahubungan +'</option>';
    }
    column.html(select);
}

function edit_dropdown_pendidikan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idpendidikan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpendidikan +'" '+ selected +' >' + data[i].namapendidikan +'</option>';
    }
    column.html(select);
}

function edit_dropdown_pekerjaan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idpekerjaan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idpekerjaan +'" '+ selected +' >' + data[i].namapekerjaan +'</option>';
    }
    column.html(select);
}

function edit_dropdown_kelurahan(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].iddesakelurahan==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].iddesakelurahan +'" '+ selected +' >' + data[i].namadesakelurahan +' '+ data[i].namakecamatan +' '+ data[i].kodepos+'</option>';
    }
    column.html(select);
}


//reset data pendaftaran
function reset_pendaftaran()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset Form?',
        buttons: {
            confirm: function () { 
                window.location.reload(true)
            },
            cancel: function () {               
            }            
        }
    });
	
}

//kembali ke halaman list pendaftaran poli
function back_pendaftaran()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list pendaftaran?',
        buttons: {
            confirm: function () { 
                window.location.href='pendaftaran_poli';
            },
            cancel: function () {               
            }            
        }
    });
    
}
