/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){
    listaplicare();
});
function listaplicare()
{
    var x = 0, y = 0, z = 0, idxl = '', list_ = [], no = 0, html = '';
    startLoading();
    html = '<table id="datalist" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'+
                 '<thead>'+
                    '<tr class="header-table-ql">'+
                      '<th>Nama Ruang</th>'+
                      '<th>Kode Ruang</th>'+
                      '<th>Jenis</th>'+
                      '<th>Kapasitas</th>'+
                      '<th>Tersedia</th>'+
                      '<th>Campuran</th>'+
                      '<th>Pria</th>'+
                      '<th>Wanita</th>'+
                      '<th>Kode Kelas</th>'+
                      '<th>Aksi</th>'+
                    '</tr>'+
                 '</thead>'+
                 '<tbody>';

                  /////////cari dan tampilkan data
                  $.ajax({
                    url:base_url + 'cmasterdata/aplicare_getdata',
                    type:'POST',
                    dataType:'JSON',
                    data:{},
                    success : function(result){
                        stopLoading();
                        if (result['data_list'] == null)
                        {
                            notif('warning', 'Server Aplicare mungkin tidak aktif. Kontak Admin.');
                        }
                        else
                        {
                            for (x in result['data_list'])
                            {
                                idxl = result['data_list'][x]['namaruang'];
                                list_[idxl] = [];
                                list_[idxl]['bpjs']    = result['data_list'][x];
                                list_[idxl]['both']    = false;
                                list_[idxl]['sama']    = true;
                            }
                        }
                        for (y in result['data_db'])
                        {
                            idxl = result['data_db'][y]['namabangsal'];
                            if (!(idxl in list_))
                                list_[idxl] = [];
                            list_[idxl]['rs']    = result['data_db'][y];
                            list_[idxl]['both']  = false;
                            list_[idxl]['sama']  = true;
                            if ("bpjs" in list_[idxl])  //jika nama bangsal rs ada di bpjs
                            {
                                list_[idxl]['both']  = true;
                                list_[idxl]['sama']  = (list_[idxl]['bpjs']['kapasitas']==result['data_db'][y]['kapasitas']) 
                                        && (list_[idxl]['bpjs']['tersediapriawanita']==result['data_db'][y]['tersediacampur']) 
                                        && (list_[idxl]['bpjs']['tersediapria']==result['data_db'][y]['tersedialakilaki']) 
                                        && (list_[idxl]['bpjs']['tersediawanita']==result['data_db'][y]['tersediaperempuan']);
                            }
                        }
                        
                        for (z in list_)
                        {
                            no = 0;
                            sudah = false;
                            no++;
                            //lastupdate, tersediapriawanita, kapasitas, namaruang, koderuang, tersediawanita, tersediapria, kodekelas
                            if ("bpjs" in list_[z])
                            {
                                sudah = true;
                                html += '<tr ' + ((!list_[z]['sama'])?' style="background-color:#f0d8d8"':'') + '>';
                                html += '<td>'+list_[z]['bpjs']['namaruang']+'</td><td>'+list_[z]['bpjs']['koderuang']+'</td><td>BPJS</td><td>'+list_[z]['bpjs']['kapasitas']+'</td><td>'+list_[z]['bpjs']['tersedia']+'</td><td>'+list_[z]['bpjs']['tersediapriawanita']+'</td><td>'+list_[z]['bpjs']['tersediapria']+'</td><td>'+list_[z]['bpjs']['tersediawanita']+'</td><td>'+list_[z]['bpjs']['kodekelas']+'</td><td>' +
                                               '<a id="hapusdariaplicare" krj="'+list_[z]['bpjs']['koderuang']+'" kkj="'+list_[z]['bpjs']['kodekelas']+'" nr="'+list_[z]['bpjs']['namaruang']+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Buat di Aplicare"><i class="fa fa-minus"></i></a> ' + 
                                        '</td>';
                                html += '</tr>';
                            }
                            if ("rs" in list_[z])
                            {
                                html += '<tr '+((!list_[z]['sama'])?' style="background-color:#f0d8d8"':'')+'>';
                                html += '<td>'+list_[z]['rs']['namabangsal']+'</td><td>'+list_[z]['rs']['koderuangjkn']+'</td><td>RS</td><td>'+list_[z]['rs']['kapasitas']+'</td><td>'+(parseInt(list_[z]['rs']['tersediacampur'])+parseInt(list_[z]['rs']['tersedialakilaki'])+parseInt(list_[z]['rs']['tersediaperempuan']))+'</td><td>'+list_[z]['rs']['tersediacampur']+'</td><td>'+list_[z]['rs']['tersedialakilaki']+'</td><td>'+list_[z]['rs']['tersediaperempuan']+'</td><td>'+list_[z]['rs']['kodekelasjkn']+'</td>' +
                                        '<td>'+((!list_[z]['both'])
                                                ?'<a id="buatdiaplicare" nr="'+list_[z]['rs']['namabangsal']+'" krj="'+list_[z]['rs']['koderuangjkn']+'" k="'+list_[z]['rs']['kapasitas']+'" tc="'+list_[z]['rs']['tersediacampur'] +'" tl="'+list_[z]['rs']['tersedialakilaki']+'" tp="'+list_[z]['rs']['tersediaperempuan']+'" kkj="'+list_[z]['rs']['kodekelasjkn']+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buat di Aplicare"><i class="fa fa-plus"></i></a> '
                                                :'<a id="perbaruiaplicare" nr="'+list_[z]['rs']['namabangsal']+'" krj="'+list_[z]['rs']['koderuangjkn']+'" k="'+list_[z]['rs']['kapasitas']+'" tc="'+list_[z]['rs']['tersediacampur'] +'" tl="'+list_[z]['rs']['tersedialakilaki']+'" tp="'+list_[z]['rs']['tersediaperempuan']+'" kkj="'+list_[z]['rs']['kodekelasjkn']+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Perbarui Aplicare"><i class="fa fa-pencil-square-o"></i></a> ')+
                                        '</td>';
                                html += '</tr>';
                            }
                        }
                        
                        $('#listaplicare').empty();
                        $('#listaplicare').html(html+'</tbody></table>');
                        $('#datalist').dataTable({"pageLength":1000});
                    },
                    error : function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                  });
    
}

$(document).on("click","#perbaruiaplicare", function(){
    startLoading();
    $.ajax({
        type:"POST",
        url: base_url+"cmasterdata/aplicare_updatebed",
        data:{
            nr:$(this).attr("nr"), 
            krj:$(this).attr("krj"), 
            k:$(this).attr("k"), 
            tc:$(this).attr("tc"), 
            tl:$(this).attr("tl"), 
            tp:$(this).attr("tp"), 
            kkj:$(this).attr("kkj")
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            listaplicare();
        },
        error:function(result){
            stopLoading();
            return false;
        }
    });
});

$(document).on("click","#buatdiaplicare", function(){
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"cmasterdata/aplicare_createbed",
        data:{
            nr:$(this).attr("nr"),
            krj:$(this).attr("krj"),
            k:$(this).attr("k"),
            tc:$(this).attr("tc"),
            tl:$(this).attr("tl"), 
            tp:$(this).attr("tp"), 
            kkj:$(this).attr("kkj")
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            listaplicare();
        },
        error:function(result){
            stopLoading();
            return false;
        }
    });
});

$(document).on("click","#hapusdariaplicare", function(){
    var krj_ = $(this).attr("krj"), kkj_ = $(this).attr("kkj");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data "'+$(this).attr("nr")+' '+$(this).attr("kkj")+'" yang ada di Aplicare?',
        buttons: {
            confirm: function () 
            { 
                startLoading();
                $.ajax({
                type:"POST",
                url: base_url+"cmasterdata/aplicare_deletebed",
                data:{krj: krj_, kkj: kkj_},
                dataType:"JSON",
                success:function(result){
                    stopLoading();
                    listaplicare();
                },
                error:function(result){
                    stopLoading();
                    return false;
                }
                });
            },
            cancel: function () {               
            }            
        }
    });
    
});