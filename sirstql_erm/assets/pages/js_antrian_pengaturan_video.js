$(function () 
{ 
    
});

//form upload video
$(document).on('click','#addvideo',function(){
    var form = '<div class="progress" id="progressDivId">\n\
            <div class="progress-bar" id="progress-bar"></div>\n\
            <div class="percent" id="percent">0%</div>\n\
            <div id="status"></div>\n\
        </div>\n\
        <form action="" id="formUpload">' +
            '<div class="form-group">' +
                '<label>Pilih Video</label>' +
                '<input type="file" class="form-control" id="upload_video" name="upload_video"/>'+
            '</div>'+ 
            '<div class="form-group">' +
                '<label>Priview Video</label>' +
                '<div id="priviewupload" class="col-md-12 row"></div>'+
            '</div>'+
        '</form>';
            
    $.confirm({
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Upload Video',
        columnClass:'m',
        content: form,
        buttons: {
            formSubmit: {
                text: 'upload',
                btnClass: 'btn-blue',
                action: function () {
                    if($('#upload_video').val() == '')
                    {
                        alert('Video Belum Dipilih');
                        return false;
                    }
                    upload_video();
                    return false;
                    
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        },
        onContentReady: function () {
        }

    });
});

function upload_video()
{
    var formdata = new FormData();
    var file     = document.getElementById('upload_video').files[0];
    var filename = 'upload_video';
    formdata.append(filename, file);
    var ajax = new XMLHttpRequest();
    ajax.upload.addEventListener("progress",progressUpload, false);
    ajax.onreadystatechange = function ()
    {
        if(ajax.readyState==XMLHttpRequest.DONE)
        {
            if(ajax.responseText)
            {
                var result = ajax.responseText;
                notif(result.status, result.message);
                setTimeout(function(){window.location.reload();},1000);
            }	    	
        }
    }
    ajax.open("POST", base_url+'cantrian/upload_video', true);
    ajax.send(formdata);
}

//fungsi tampil proses 
function progressUpload(event){
    var percent = (event.loaded / event.total) * 100;
    document.getElementById("progress-bar").style.width = Math.round(percent)+'%';    
    document.getElementById("percent").innerHTML = Math.round(percent)+"% loading...";
}

$(document).on('change','#upload_video',function(){
   var size = this.files[0].size;
   if(parseInt(size) > parseInt(204857600))
   {
       alert('Ukuran Video Maksimal 200 MB');
       $('#upload_video').val('');
       return false;
   }
   var file_url = URL.createObjectURL(this.files[0]); 
   $('#priviewupload').html('<embed src="'+file_url+'" style="width:100%;"></embed>');
});

$(document).on("click","#hapusvideo", function(){ // hapus video
    var idvideo   = $(this).attr('idvideo');
    var namavideo = $(this).attr('namavideo');
    var dokumen   = $(this).attr('dokumen');
    
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Hapus Video',
        content: 'Apakah anda yakin akan menghapus video? ---- <br> <p>'+namavideo+'</p>',
        buttons: {
            hapus: function () {    
                startLoading();
                $.ajax({ 
                    url: base_url + 'cantrian/hapus_video',
                    type : "post",      
                    dataType : "json",
                    data : {
                        idvideo:idvideo,
                        dokumen:dokumen
                    },
                    success: function(result) {  
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success')
                        {
                            $('#display_video'+idvideo).remove();
                        }
                    },
                    error: function(result){  
                        stopLoading();
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            batal: function () {               
            }            
        }
    });
});

$(document).on('change','#halaman',function(){
   window.location.href=base_url+'cantrian/pengaturan_video?halaman='+this.value;
});