var dtlaboratoriumranap='';
localStorage.setItem('idbangsal',(localStorage.getItem('idbangsal')==undefined) ? '' : localStorage.getItem('idbangsal'));

$(function(){ 
    get_databangsal('#idbangsal',localStorage.getItem('idbangsal'));
    $('.sel2').select2();
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    list_laboratoriumranap();
    autolod_laboratdetail();
});


//refresh data laboratorium
$(document).on('click','#refresh',function(){ // refresh data
   $('input[type="search"]').val('').keyup();
   dtlaboratoriumranap.state.clear();
   dtlaboratoriumranap.ajax.reload(); 
});

//tampil data laboratorium
$(document).on('click','#tampil',function(){
   dtlaboratoriumranap.ajax.reload(null,false); 
});

function cariByBangsal(){
    localStorage.setItem('idbangsal',$('#idbangsal').val());
    dtlaboratoriumranap.ajax.reload();
}

function list_laboratoriumranap() {
    dtlaboratoriumranap = $('#dtlaboratranap').DataTable( {
        "processing": true,
        "serverSide": true,
         "ajax": {
            "data":{idbangsal:function(){return $('#idbangsal').val();}, tanggal:function(){return $('#tanggal').val();}},
            "url": base_url+'cpelayananranap/dt_laboratoriumranap',
            "type": "POST"
        },
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            { "data": "norm" },
            { "data": "namalengkap" },
            { "data": "tanggallahir" },
            { "data": "nosep" },
            { "data": "waktu" },
            { "data": "namadokter" },
            { "data": "namabangsal" },
            { "data": "menu" }
        ],
        "columnDefs": [{ "targets": [-1],"orderable": false,},],
        "order": [[1, 'asc']],
        "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', labstatus(aData[6]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
    } );
 
    // Array to track the ids of the details displayed rows
    var detailRows = [];
 
    $('#dtlaboratranap tbody').on( 'click', 'tr td.details-control', function () {
        //hapus detail sebelumnya  yang activ saat halaman dimuat ulang
        $('#localstorageActiveRow').remove();
        
        var tr = $(this).closest('tr');
        var row = dtlaboratoriumranap.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
        
        if ( row.child.isShown() )
        {
            tr.removeClass('details');
            row.child.hide(); 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else
        {
            tr.addClass( 'details' );
            row.child( tabledetail( row.data() ) ).show();
            detailRows.push( tr.attr('id') );
            dt_laboratoriumdetail( tr.attr('id') );
        }
    });
 
    // On each draw, loop over the `detailRows` array and show any child rows
    dtlaboratoriumranap.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
}


//----------------- data table details

function tabledetail ( d ) {
    var child = '<div class="" style="border:3px solid #d2d6de;margin:0px -4px 2px -4px;padding:5px;">\n\
                <table id="tabledetail" style="margin:0px;margin-top:2px;" class="table table-hover table-striped table-bordered">\n\
                    <thead><tr style="background-color:rgb(193 193 193 / 77%);">\n\
                        <th>Idpemeriksaan</th><th>Waktu Tindakan</th><th>Tindakan</th><th>Status</th><th width="100"></th>\n\
                    </tr></thead>\n\
                    <tbody id="bodytabledetail'+d.DT_RowId+'"></tbody>\n\
                </table></div>';
    return child;
    
    
}

//set active pasien yang sebelumnya diperiksa saat halaman dimuat ulang
function autolod_laboratdetail()
{
    var idp = localStorage.getItem('labdetailID');
    if(idp != undefined || idp !='' || idp != 'undefined' || idp != null)
    {
        setTimeout(function(){
            $('#'+idp).after('<tr id="localstorageActiveRow"><td colspan="9"><div class="" style="border:3px solid #d2d6de;margin:0px -4px 2px -4px;padding:5px;">\n\
                <table id="tabledetail" style="margin:0px;margin-top:2px;" class="table table-hover table-striped table-bordered">\n\
                    <thead><tr style="background-color:rgb(193 193 193 / 77%);">\n\
                        <th>Idpemeriksaan</th><th>Waktu Tindakan</th><th>Tindakan</th><th>Status</th><th width="100"></th>\n\
                    </tr></thead>\n\
                    <tbody id="bodytabledetail'+idp+'"></tbody>\n\
                </table></div></td></tr>');
            dt_laboratoriumdetail(idp,'autoload');
            $('#'+idp).addClass('details');
        },800);
    }
}

//tampilkan data laboratorium detail
function dt_laboratoriumdetail(idp,mode='')
{
    localStorage.setItem('labdetailID',idp);
    var tanggal = $('#tanggal').val();
    var rows    = '';
    
    if(mode==''){startLoading();}
    $.ajax({
        type: "POST",
        url: base_url+'cpelayananranap/dt_laboratoriumranapdetail',
        dataType: "JSON",
        data:{tanggal:tanggal,idp:idp},
        success: function(result) { //jika  berhasil
            if(mode==''){stopLoading();}
            
            var bgstatus='';
            if(result.length > 0)
            {
                for(var x in result)
                {
                    bgstatus = ((result[x].status =='batal') ? 'style="background-color:#f0D8D8;"' : 'style="background-color:#e0f0d8;"' );
                    var menu = '';
                    if(result[x].status == 'rencana')
                    {
                        bgstatus = '';
                        menu += ' <a id="pemeriksaanranapPeriksa" mode="periksa" nomori="'+result[x].idinap+'" alt="'+result[x].idrencanamedispemeriksaan+'" idpendaftaran="'+result[x].idpendaftaran+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Pemeriksaan"><i class="fa fa-stethoscope"></i></a>';
                        menu += ' <a id="pemeriksaanranapTerlaksana" alt="'+result[x].idrencanamedispemeriksaan+'" idpendaftaran="'+'+result[x].idpendaftaran+'+'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>';
                        menu += ' <a id="pemeriksaanranapBatal" alt="'+result[x].idrencanamedispemeriksaan+'" idpendaftaran="'+result[x].idpendaftaran+'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-circle"></i></a>';
                    }
                    
                    if(result[x].status =='terlaksana' || result[x].status =='batal')
                    {
                        menu += ' <a id="pemeriksaanranapBatalterlaksana" alt="'+result[x].idrencanamedispemeriksaan+'" idpendaftaran="'+result[x].idpendaftaran+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Rencana"><i class="fa  fa-folder-open"></i></a>';
                        menu += ' <a id="pemeriksaanranapPeriksa" mode="view" nomori="'+result[x].idinap+'" alt="'+result[x].idrencanamedispemeriksaan+'" idpendaftaran="'+result[x].idpendaftaran+'" class=" btn btn-info btn-xs" data-toggle="tooltip" data-original-title="View"><i class="fa  fa-eye"></i></a>';
                    }
                    rows += '<tr '+bgstatus+'><td>'+result[x].idrencanamedispemeriksaan+'</td><td>'+result[x].waktu+'</td><td>'+result[x].tindakan+'</td><td>'+result[x].status+'</td><td>'+menu+'</td></tr>';
                }
            }
            else
            {
                rows += '<tr><td class="text-center" colspan="5">Data Tidak Ditemukan.</td></tr>';
            }
            $('#bodytabledetail'+idp).html(rows);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function(result) { //jika error
            if(mode==''){stopLoading();}
            fungsiPesanGagal(); return false;
        }
    });
}


$(document).on("click","#pemeriksaanranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("idpendaftaran");
    fungsi_pemeriksaanranapUbahStatus('terlaksana','Pemeriksaan Terlaksana?',id,idpendaftaran);
});
$(document).on("click","#pemeriksaanranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("idpendaftaran");
    fungsi_pemeriksaanranapUbahStatus('batal','Batal Pemeriksaan?',id,idpendaftaran);
});
$(document).on("click","#pemeriksaanranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt");
    var nobaris = $(this).attr("nobaris");    
    var idpendaftaran = $(this).attr("idpendaftaran");
 fungsi_pemeriksaanranapUbahStatus('rencana','Batal Terlaksana?',id,idpendaftaran);
});
$(document).on("click","#pemeriksaanranapPeriksa", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { 
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("modepemeriksaanranap", 'laboratoriumranap');
        var mode = $(this).attr('mode');
        var url = ((mode=='view') ? 'pemeriksaanranap_view' : 'pemeriksaanranap_periksa' );
        window.location.href=base_url + 'cpelayananranap/'+url;
    } else {
        //jika browser tidak support webstorage
         pesanUndefinedLocalStorage();
    }
});

function fungsi_pemeriksaanranapUbahStatus(status,content,id,idpendaftaran) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: content,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'pemeriksaanranap_ubahstatus',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                        //jika status menunggu
                        if(result.status=='menunggu')
                        {
                            detailrencanabelumterlaksana(result.rencana);                            
                            $.alert('Ubah Rencana Menjadi Terlaksana atau Batal');
                            return true;
                        }
                        notif(result.status, result.message);
                        dt_laboratoriumdetail('row_'+idpendaftaran);
                        $('#row_'+idpendaftaran).addClass('details');
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

function detailrencanabelumterlaksana(rencana)
{
    var list_rencana='';
    for(var x in rencana)
    {
        list_rencana += '<tr><td>'+rencana[x].icd+'</td><td>'+rencana[x].statuspelaksanaan+'</td></tr>';
    }
    
    var modalTitle = 'Rencana Perawatan Belum Terlaksana';
    var modalContent = '<form action="">' +
                          '<table class="table table-striped">\n\
                            <tr class="bg bg-yellow-gradient"><td>Rencana Perawatan</td><td>Status Perawatan</td></tr>\n\
                            '+list_rencana+'\n\
                            </table> \n\
                            <small class="text text-red">#ubah status rencana perawatan menjadi terlaksana atau batal untuk dapat menyelesaikan perencanaan.</text>'+
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        }
    });
}