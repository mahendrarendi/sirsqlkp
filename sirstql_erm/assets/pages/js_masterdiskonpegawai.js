var diskon='';
$(function () {
    listdata();
})
//menampilkan data aset
function listdata()
{ 
  diskon = $('#diskonpegawai').DataTable({
      "processing": true,
      "serverSide": true,
      "lengthChange": false,
      "searching" : true,
      "stateSave": true,
      "order": [],
      "ajax": {
          "data":{},
          "url": base_url+'cmasterdata/dt_masterdiskonpegawai',
          "type": "POST"
      },
      "columnDefs": [{ "targets": [5],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
 });
}

$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  diskon.state.clear();
  diskon.ajax.reload();
});
function reloaddata(){diskon.ajax.reload(null,false);}
//tambah aset
$(document).on("click",'#add',function(){formtambahedit('','Tambah Diskon');});
//ubah aset
$(document).on("click",'#edit',function(){formtambahedit($(this).attr('alt'),'Ubah Diskon');});

function formtambahedit(iddiskon,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formdiskon" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<input type="hidden" name="iddiskonpegawai" value="'+iddiskon+'"/>'+
                                '<div class="col-md-12">' +
                                    '<label>Nama Diskon</label>' +
                                    '<input class="form-control" name="namadiskon" id="namadiskon"/>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Kelas</label>' +
                                    '<select class="form-control" name="idkelas" id="idkelas" ><option value="">-Pilih-</option></select>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Jenis Tarif</label>' +
                                    '<select class="form-control" id="idjenistarif" name="idjenistarif"><option value="">-Pilih-</option></select>'+
                                '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Potongan</label>' +
                                    '<input type="text" id="potongan" class="form-control" name="potongan" value="" />' +  
                                '</div>'+
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="namadiskon"]').val()==''){
                        alert('Nama Diskon  Harus Diisi.');
                        return false;
                    }else if($('select[name="idkelas"]').val()==''){
                        alert('Kelas Harus Diisi.');
                        return false;
                    }else if($('select[name="jenistarif"]').val()==''){
                        alert('Jenis Tarif Harus Diisi.');
                        return false;
                    }else if($('input[name="potongan"]').val()=='' && $('select[name="jenisdiskon"]').val()!='harganet'){
                        alert('Potongan Harus Diisi.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            formready(iddiskon);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function inputedit(id,value)
{
    id.empty();
    id.val(value);
}
function formready(iddiskon)
{
    
    startLoading();
    $.ajax({ 
        url:  base_url+'cmasterdata/formreadymasterdiskonpegawai',type:"POST",dataType:"JSON",data:{id:iddiskon},
        success: function(result){
            stopLoading();
            var diskon = result.diskon;
            editdataoption($('#idjenistarif'),result.jenistarif, ((diskon!=null)? diskon.idjenistarif:'') );
            editdataoption($('#idkelas'),result.kelas,((diskon!=null)? diskon.idkelas:''));
            if(diskon!=null){
                inputedit($('#potongan'),diskon.potongan);
                inputedit($('#namadiskon'),diskon.namadiskon);
            }
        },
        error: function(result){stopLoading();fungsiPesanGagal();return false;}
    });
}

function simpan()
{
    $.ajax({ 
        url:  base_url+'cmasterdata/save_masterdiskonpegawai',type:"POST",dataType:"JSON",data:$('#formdiskon').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}


$(document).on("click","#hapus", function(){
    var id = $(this).attr('alt');
    var diskon = $(this).attr('diskon');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: diskon+'<br>Hapus Diskon ?',
        buttons: {
            hapus: function () {                
                $.ajax({ 
                    url: base_url+'cmasterdata/hapus_diskonpegawai',
                    type : "post",      
                    dataType : "json",
                    data : {id:id },
                    success: function(result) {      
                        notif(result.status, result.message);
                        reloaddata();
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            batal: function () {               
            }            
        }
    });
});