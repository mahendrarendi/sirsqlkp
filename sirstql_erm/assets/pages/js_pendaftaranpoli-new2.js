// 'use strict';
var tablependaftaran;
includejs('/assets/pages/pendaftaran_pendaftaran.min.js');
$(function(){
    // localStorage.clear();
    localStorage.removeItem("idpendaftaran"); //hapus localstorage idpendaftaran
    localStorage.removeItem("modependaftaran"); //hapus localstorage modependaftaran
    // listPendaftaranpoli();// tampilkan data pendaftaran sekarang/hari ini
    loaddtpendaftaran();
});

function onchange_checkboxisrawatgabung()
{
    $('#israwatgabung').click(function(){
        if($(this).is(":checked")){//saat ceklis is rawat gabung dipilih
            $('#isirawatgabung').val('1');
            $('#listBed').empty();
        }
        else if($(this).is(":not(:checked)")){//saat ceklis is rawat gabung tidak dipilih
            $('#isirawatgabung').val('0');
            $('#listBed').empty();
        }
    });
}


function loaddtpendaftaran() //fungsi panggil data pendaftaran hari ini
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+"cadmission/loaddtpendaftaran",
        dataType: "JSON",
        data:{tglawal:localStorage.getItem('tglawaldaftar'),tglakhir:localStorage.getItem('tglakhirdaftar')},
        success: function(result) {
            //console.log(localStorage.getItem('tglawaldaftar'));
            stopLoading();
            pendaftaranlistdt(result); //panggil fungsi tampil ke html
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
  });
}
function setLocalStorageTgl1(date){return localStorage.setItem('tglawaldaftar',date);}
function setLocalStorageTgl2(date){return localStorage.setItem('tglakhirdaftar',date);}
function search_by_date_range() /// -- cari berdasarkan range tanggal
{   
    startLoading();
    var tglawal = $('#tanggalawal').val(), tglakhir = $('#tanggalakhir').val();
    if(tglawal=='')
    {
        $('#tanggalakhir').val('')
        alert_empty('tanggal awal');
    }
    else
    {
        $.ajax({ 
            url: base_url+'cadmission/search_by_date_range',
            type : "post",      
            dataType : "json",
            data : { tglawal:tglawal, tglakhir:tglakhir },
            success: function(result) {
                stopLoading();
                pendaftaranlistdt(result);
                $('#tanggalawal').val(tglawal);
                $('#tanggalakhir').val(tglakhir);
            },
            error: function(result){
                stopLoading();                 
                fungsiPesanGagal();
                return false;
            }
        });
    }  
}
function pendaftaranlistdt(result) // -- menampilkan data pendaftaran ke html
{
    var menuOrder='';
    var beriantrian='',no=0,buka='',batal='';
    var tampildt = '<table style="font-size: 10px;" id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'+
              '<thead>'+
              '<tr >'+
                '<th>NO</th>'+
                '<th>NO.RM</th>'+
                '<th>NIK</th>'+
                '<th>NAMA PASIEN</th>'+
                '<th class="none">NO.SEP</th>'+
                '<th class="none">NO.RUJUKAN</th>'+
                '<th class="none">Tgl Lahir</th>'+
                '<th class="none">No.Telpon</th>'+
                '<th class="none">Alamat</th>'+
                '<th>WAKTU DAFTAR</th>'+
                '<th>WAKTU PERIKSA</th>'+
                '<th>CARA DAFTAR</th>'+
                '<th>KLINIK TUJUAN</th>'+
                '<th>PEMERIKSAAN</th>'+
                '<th>CARA BAYAR</th>'+
                '<th>CETAK</th>'+
                '<th style="width:120px"> </th>'+
              '</tr>'+
              '</thead>'+
              '<tbody >';
    for(i in result)
    {   
        ++no ;
        // bokingjadwaloperasi
        bookingjadwaloperasi = (( result[i].isjadwaloperasi==0 && result[i].idstatuskeluar==1 )?' <a nobar="'+no+'" id="bookingjadwaloperasi"  class="btn btn-success btn-xs nobar'+no+'" '+tooltip('Booking Jadwal Operasi')+' idp="'+result[i].idpendaftaran+'"><i class="fa fa-calendar"></i></a>':'');
        // menu pindah ranap
        ((result[i].jenispemeriksaan=='rajalnap' || result[i].jenispemeriksaan=='ranap')? menuranap='': menuranap='<a onclick="pindahRanap('+result[i].norm+','+result[i].idpendaftaran+','+no+')" class="btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Pindah RANAP"><i class="fa fa-bed"></i></a>');
        // ///// MENU BERI ANTRIAN, TERANTRIKAN dan VERIFIKASI
        if(result[i].isantri==null) // menu beri antrian
        {
            beriantrian='<a id="pemeriksaanklinikBeriantrian" class=" btn btn-success btn-xs" data-original-title="Beri Antrian"';    
        }
        else// menu terantrikan
        {
            beriantrian='<a onclick="cetakAntrian('+result[i].idpemeriksaan+','+result[i].idperson+')" class=" btn bg-purple btn-xs" data-original-title="Cetak Antrian"';
        }

        // menu order
        if(result[i].isberkassudahada=='0'){menuOrder='<a onclick="changeToProses('+no+','+result[i].idpendaftaran+','+2+')" nobaris="'+no+'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Order RM"><i class="fa fa-search"></i></a>';}

        // menu buka layanan
        buka ='';
        if(result[i].idstatuskeluar=='3')
        {
            buka=' <a onclick="buka_pelayanan('+result[i].idpendaftaran+','+no+')" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Buka Pelayanan"><i class="fa fa-stethoscope"></i></a> ';
            beriantrian=' <a class=" btn bg-black btn-xs" style="cursor:not-allowed;" data-original-title="Terantrikan"';
        }
        //menu batal layanan
        batal='';
        if(result[i].idstatuskeluar!='3')//status masih dalam pemeriksaan
        {
            batal=' <a onclick="hapus_data_pendaftaran('+result[i].idpendaftaran+','+no+')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a> ';
        }

        if(result[i].jenispemeriksaan!=='rajal'){beriantrian='<a class=" btn bg-black btn-xs" style="cursor:not-allowed;" data-original-title="Terantrikan"';}
        tampildt += '<tr style="'+ qlstatuswarnapp(result[i].idstatuskeluar) +'" id="row'+ no +'">'+ //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
           '<td>'+ no +'</td>'+
           '<td>'+result[i].norm+'</td>'+
           '<td>'+result[i].nik+'</td>'+
           '<td>'+result[i].namalengkap+'</td>'+
           '<td>'+result[i].nosep+'</td>'+
           '<td>'+result[i].norujukan+'</td>'+
           '<td>'+result[i].tanggallahir+'</td>'+
           '<td>'+result[i].notelpon+'</td>'+
           '<td>'+result[i].alamat+'</td>'+
           '<td>'+result[i].waktu+'</td>'+
           '<td>'+result[i].waktuperiksa+'</td>'+
           '<td>'+result[i].caradaftar+'</td>'+
           '<td>'+result[i].namaunit+'</td>'+
           '<td>'+result[i].jenispemeriksaan+'</td>'+
           '<td>'+result[i].carabayar+'</td>'+
           '<td>'
            +' <a onclick="buatQrPasien('+result[i].norm+')" data-toggle="tooltip" data-original-title="Cetak kartu" class="btn btn-primary btn-xs" ><i class="fa fa-credit-card"> '+ifnullstring(result[i].cetakkartu)+' </i></a>'
            +' <a onclick="cetak_lembarrm('+result[i].norm+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Lembar RM"><i class="fa fa-print"></i></a>'
            +' <a onclick="cetak_resumralan('+result[i].idpendaftaran+')" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="RM03.4"><i class="fa fa-print"></i></a>'
            +' <a onclick="cetak_gelang('+result[i].norm+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Cetak Gelang"><i class="fa fa-circle-o"></i></a>'
            +' <a onclick="cetak_label('+result[i].norm+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Cetak Label"><i class="fa fa-list-alt"></i></a>'
           +'</td>'
           +'<td>'+menuranap + bookingjadwaloperasi
           +' <a id="editPendaftaranPoli" alt="'+result[i].idpendaftaran+'" class="btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Ubah"><i class="fa fa-pencil"></i></a>'
           +batal
           +buka
           +menuOrder+' '
           +beriantrian+' nobaris="'+no+'" alt="'+result[i].idpemeriksaan+'" alt2="'+result[i].idunit+'" alt3="'+result[i].idperson+'" alt4="'+result[i].idpendaftaran+'"  data-toggle="tooltip"><i class="fa fa-check"></i></a>';
           tampildt+='</td></tr>';
    }
    $('#tampildt').empty();
    $('#tampildt').html(tampildt+'</tfoot></table>');//
    $('[data-toggle="tooltip"]').tooltip();//inisialisasi tooltips
    $('#table').DataTable({"dom": '<"toolbar">frtip',"stateSave": true,"columnDefs": [{"targets": [ 10 ], "orderable": false, },],});
    menubar_table();//panggil fungsi tambahkan menu di atas tabel
}
function ifnullstring(value)
{
    if(value==null)
    {
        return 0;
    }
    else
    {
        return value;
    }
}
function menubar_table()//tambah menu
{
    $("div.toolbar").html('<input onchange="setLocalStorageTgl1(this.value)" id="tanggalawal" style="margin-right:6px;" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal" ><input onchange="setLocalStorageTgl2(this.value)" id="tanggalakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggal" > <a class="btn btn-info btn-sm" onclick="search_by_date_range()"><i class="fa fa-desktop"></i> Tampil</a> <a href="'+base_url+'cadmission/add_pendaftaran_poli" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Entri Pendaftaran</a> <a style="margin-right: 6px;" onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> <a class="btn btn-info btn-sm" onclick="gantiDokterPraktek()"><i class="fa fa-stethoscope"></i> Dokter Pengganti</a>');
    $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",((localStorage.getItem('tglawaldaftar'))?localStorage.getItem('tglawaldaftar'):'now')); //Initialize Date picker;
    $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",((localStorage.getItem('tglakhirdaftar'))?localStorage.getItem('tglakhirdaftar'):'now')); //Initialize Date picker;
//    button_pane();//panggil fungsi button pane
}
function refresh_page()
{
    // table.state.clear();
    $('#tanggalawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", 'now' );
    $('#tanggalakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now' );
    window.location.reload(true);
}



// function listPendaftaranpoli()
// {
//     $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
//     tablependaftaran = $('#tablependaftaran').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],"ajax": {"data":{tglawal:function(){return $('#tglawal').val();}, tglakhir:function(){return $('#tglakhir').val();}},"url": base_url+'cadmission/list_pendaftaranpoli',"type": "POST"},"columnDefs": [{ "targets": [ ],"orderable": false,},],
//    "fnCreatedRow": function (nRow, aData, iDataIndex) {$(nRow).attr('style', qlstatuswarnapp(aData[12]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
//    "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
//    });
// }
// function refreshPendaftaran()
// {
//     $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
//     $('input[type="search"]').val('').keyup();
//     tablependaftaran.ajax.reload();
// }
// function reloadPendaftaran()
// {
//     tablependaftaran.ajax.reload();
// }
function changeToProses(no,idpendaftaran,status)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Ubah status berkas rekam medis.?',
        buttons: {
            confirm: function () { 
                $.ajax({
                    url:base_url+"cadmission/ubahstatus_arusrekamedis",
                    type:"POST",
                    dataType:"JSON",
                    data:{status:status,id:idpendaftaran},
                    success:function(result){
                    notif(result.status, result.message);//panggil fungsi popover message
                    },
                    error:function(result){
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            cancel: function () {               
            }            
        }
    });
}

/// -- cetak gelang
function cetak_gelang(id)
{
  var style = '<style rel="stylesheet" media="print"> @media print { div{ background-color:#000; } } </style>';
  $.ajax({
        type: "POST",
        url: "cetak_gelang",
        data:{id},
        dataType: "JSON",
        success: function(result) {
            var printContents = '<div><label>Nama Pasien</label> : '+result[0].namalengkap+'<br>'+
                       '<label>No.RM</label> : '+result[0].norm+'<br>'+
                       '</div>';
            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}
// Cetak Label Pasien Ranap
function cetak_label(id)
{
  var style = '<style>@page{size:15.5cm 19cm; margin:0px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;}</style>';
  $.ajax({
        type: "POST",
        url: "cetak_gelang",
        data:{id},
        dataType: "JSON",
        success: function(result) {
            var label='<div style="font-size:12px;text-align:center;padding-top:5.5px;border-radius:4px;display:inline-block;height:1.68cm; width:5.2cm;border:0.01px solid #f0f0f0;margin:1px 4px 1px 0px;">'+result[0].rmttl+'<br>'+result[0].namalengkap+'<br>'+result[0].nik+'</div>';
            var printContents = '', down=0, right=0;
            for(down=0; down<10;down++)
            {
                for(right=0; right<3; right++)
                {
                    printContents += label;
                }
                printContents+='<br/>';
            }

            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}
/// -- cetak lembar rm
function cetak_lembarrm(id)
{
  var split='&nbsp;&nbsp;&nbsp;';
  var style = '<style rel="stylesheet" media="print"> @media print { .borderleft{border-left:1px solid #2D2D2D;border-top:1px solid #2D2D2D;} .border{border-right:1px solid #2D2D2D; border-left:1px solid #2D2D2D; border-top:1px solid #2D2D2D; padding:4px; margin:0;} .jarak{padding-top:8px;}  } </style>';
  $.ajax({
        type: "POST",
        url: "cetak_lembarrm",
        data:{id:id},
        dataType: "JSON",
        success: function(result) {
        	var pasien = result.pasien;
        	var ortu = result.ortu;
            var printContents = '<div style="padding:8px;">'+
            '<img src="'+base_url+'/assets/images/headerlembarrm.svg" >'+
            '<hr><span style="font-size:30px;">Lembar Rekam Medik &nbsp;&nbsp;&nbsp;&nbsp;  No. RM.:'+id+'</span><hr>'+
            '<table>'+
            '<tr><td>NAMA LENGKAP '+ split +'</td><td>'+ pasien.namalengkap +'</td></tr>'+
            '<tr><td>JENIS KELAMIN '+ split +'</td><td>'+ pasien.jeniskelamin  +'</td></tr>'+
            '<tr><td>TGL LAHIR '+ split +'</td><td>'+ pasien.tanggallahir  +'</td></tr>'+
            '<tr><td>AGAMA '+ split +'</td><td>'+ pasien.agama  +'</td></tr>'+
            '<tr><td>PEKERJAAN '+ split +'</td><td>'+ pasien.namapekerjaan+'</td></tr>'+
            '<tr><td>ALAMAT '+ split +'</td><td>'+ pasien.alamat +'</td></tr>'+
            '<tr><td>KELURAHAN '+ split +'</td><td>'+  pasien.namadesakelurahan +'</td></tr>'+
            '<tr><td>NO. TELP '+ split +'</td><td>'+ pasien.notelpon +'</td></tr>'+
            '<tr><td>GOL DARAH '+ split +'</td><td>'+ pasien.golongandarah +'</td></tr>'+
            '<tr><td>PENDIDIKAN '+ split +'</td><td>'+ pasien.namapendidikan +'</td></tr>'+
            '<tr><td>PEKERJAAN '+ split +'</td><td>'+ pasien.namapekerjaan +'</td></tr>'+
            '<tr><td>STATUS KAWIN '+ split +'</td><td>'+ pasien.statusmenikah +'</td></tr>'+
            '<tr><td colspan="2">&nbsp;</td></tr>'+
            '<tr><td colspan="2">Data Penanggung</td></tr>'+
            '</table><table cellspacing="0"><tr><td class="borderleft">Nama</td><td class="borderleft">Hubungan</td><td class="borderleft">Telpon</td><td class="border">Alamat</td></tr>';
            for(var p in result.penanggung){ printContents+= '<tr><td class="borderleft">'+result.penanggung[p].namalengkap+'</td><td class="borderleft">'+result.penanggung[p].namahubungan+'</td><td class="borderleft">'+result.penanggung[p].notelpon+'</td><td class="border">'+result.penanggung[p].alamat+'</td></tr>'; }
            printContents +=
            '<table width="100%" style="margin-top:5px;">'+
            '<hr>'+
            '<tr style="text-align:center;">'+
            '<td>Tanggal & Jam</td><td>Anamnesis & Pemeriksaan</td><td>Diagnosis & Terapi</td><td>ICD</td><td>DOKTER</td>'+
            '</tr>'+
            '<tr style="height:420px;">'+
            '<td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="borderleft">&nbsp;</td><td class="border">&nbsp;</td>'+
            '</tr>'+
            '<table>'+
            '</div>';
            fungsi_cetaktopdf(printContents,style);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
  });
}

function buka_pelayanan(x,y) // buka pemeriksaan
{
    var id = x;var nobaris = y;
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Buka Pelayanan?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'buka_pelayanan',
                    type : "post",      
                    dataType : "json",
                    data : { i:id },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            // tablependaftaran.ajax.reload();
                            pendaftaranListRow(id,nobaris);//get data auto load beri antrian
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
function hapus_data_pendaftaran(x,y){ //hapus atau batal pelayanan/pemerirksan
    var id = x;var nobaris = y;
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'hapus_data_pendaftaran',
                    type : "post",      
                    dataType : "json",
                    data : { i:id },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            // tablependaftaran.ajax.reload();
                             pendaftaranListRow(id,nobaris);//get data auto load beri antrian
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){               
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

// -- pindah rawat inap
function info_sep(){ $.alert("Input No SEP untuk bisa dipindahkan ke rawat Inap, untuk pasien umum/mandiri No SEP tidak di input..!");}
// -- list bed
function ranap_listbangsal(norm,idpendaftaran,nobaris)
{ 
    $.ajax({ 
        url: base_url+'cadmission/ranap_listbangsal',
        type : "post",      
        dataType : "json",
        data: {a:norm},
        success: function(result) {
        console.log(result.pasien['carabayar']);
            var dthtml='', dthtmlbangsal='', dtkelasrawat='<option value="0">Kelas Perawatan</option>',dtkelasjamin='<option value="jaminan">Kelas Jaminan</option>', dtdokter='<option value="0">Dokter DPJP</option>';
            $('#ranap_carabayar').val(result.pasien['carabayar']);// list cara bayar
            $('#pindahranap_identitasPasien').html('<b>No.RM:</b> '+result.pasien['norm']+' ,<b> No.JKN:</b>  '+result.pasien['nojkn']+',<b> Penjamin:</b>  '+result.pasien['carabayar']+' ,<b> NIK:</b>  '+result.pasien['nik']+', <b>Nama:</b>  '+result.pasien['namalengkap']+', <b> TanggalLahir:</b> '+result.pasien['tanggallahir']+', <b> Alamat:</b> '+result.pasien['alamat']+' [<b style="color:#000;">'+ ((result.pasien['norm']<'10083900') ? 'Pasien Lama' : result.pasien['ispasienlama'] ) +'</b>]');// list identitas pasien
            // dokter
            for(var b in result.dokter){dtdokter+='<option value='+result.dokter[b].idpegawai+'>'+result.dokter[b].namalengkap+'</option>'}
            $('#ranap_dokterdpjp').empty();
            $('#ranap_dokterdpjp').html(dtdokter);
            // kelas perawatan
            for ( var a in result.kelas){dtkelasrawat+='<option value='+result.kelas[a].idkelas+'>'+result.kelas[a].kelas+'</option>';}
            $('#ranap_kelasperawatan').empty();
            $('#ranap_kelasperawatan').html(dtkelasrawat);
            // kelas jaminan
            if (result.pasien['carabayar'] == 'mandiri')
            {
                dtkelasjamin+='<option value="0">Mandiri</option>';
            }
            else
            {
                for ( var a in result.kelas)
                {
                    ((result.kelas[a].kelas=='Kelas VIP' || result.kelas[a].kelas=='Mandiri')?dtkelasjamin+='':dtkelasjamin+='<option value='+result.kelas[a].idkelas+'>'+result.kelas[a].kelas+'</option>');
                }
            }
            $('#ranap_kelasjaminan').empty();
            $('#ranap_kelasjaminan').html(dtkelasjamin);
            // bed
            for( var i in result.statusbed){dthtml += '<td style="padding:10px;"><img src="'+base_url+'assets/images/'+ result.statusbed[i].file +'" width="45"/><br> '+ result.statusbed[i].statusbed +'</td> &nbsp;';}
            $('#listStatusBangsal').empty();
            $('#listStatusBangsal').html(dthtml);
            // tampil bangsal
            for( var x in result.bangsal){dthtmlbangsal += '<a class="btn btn-primary btn-sm" id="ranaplistbed" nobaris="'+nobaris+'" idbangsal="'+result.bangsal[x].idbangsal+'" daftar="'+idpendaftaran+'" bangsal="'+result.bangsal[x].namabangsal+' '+result.bangsal[x].kelas+'" cursor:pointer;">'+result.bangsal[x].namabangsal+'</a>&nbsp;';}
            $('#listBangsal').empty();
            $('#listBangsal').html(dthtmlbangsal);
            $('.select2').select2();
            return;
        },
        error: function(result){         
            fungsiPesanGagal();
            return false;
        }
    });

}
// list bed
$(document).on("click","#ranaplistbed", function(){
    startLoading();
    var value = $(this).attr("idbangsal");
    var idpendaftaran = $(this).attr("daftar");
    var bangsal = $(this).attr("bangsal");
    var cekrawatgabung = $('#isirawatgabung').val();
    localStorage.removeItem('bangsal');
    localStorage.setItem('bangsal',bangsal);
    var nobaris = $(this).attr("nobaris");
    $('#judul_listbed').empty();
    $('#judul_listbed').text('List Bed '+bangsal);
    $.ajax({ 
        url: base_url+'cadmission/ranap_listbed',
        type : "post",      
        dataType : "json",
        data:{id:value},
        success: function(result) {
            stopLoading();
            var dtinfo = '';
            for( var x in result.info){dtinfo += ' nomor rekam medis '+ result.info[x].norm+' - a/n pasien '+result.info[x].namalengkap+result.info[x].bed+'<br>';}
            $('#infobangsal').empty();
            $('#infobangsal').html(dtinfo);
            if(dtinfo!=''){$('#infobangsal').css("padding","8 px");}
            var dthtml='<tr>', no=0;
            for(var i in result.bangsal)
            {
               var allowmenu = '';
               var dt_tr='';  ++no; if(no>7){no=0;dt_tr='</tr><tr>';}
               // cekrawatgabung
               if(result.bangsal[i].statusbed=='Bed Kosong' && cekrawatgabung!=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else if(result.bangsal[i].statusbed=='Bed Wanita' && cekrawatgabung=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else if(result.bangsal[i].statusbed=='Bed Laki-laki' && cekrawatgabung=='1')
               {
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+nobaris+','+result.bangsal[i].nobed+')" ';
               }
               else
               {
                allowmenu = 'style="cursor:not-allowed;" ';
               }
               dthtml += '<td style="text-align:center; padding:4px;"><a '+ allowmenu + '><img style="border-radius:0px; width:120px;height:120px;" src="'+base_url+'assets/images/'+result.bangsal[i].file+'" alt="'+result.bangsal[i].statusbed+'"><span class="users-list-name"  style="font-size:30px;">'+result.bangsal[i].nobed+'</span></a></td>'+dt_tr;
            }
            $('#listBed').empty();
            $('#listBed').html(dthtml+'</tr>');
        },
        error: function(result){          
            fungsiPesanGagal();
            return false;
        }
    });
    
});
//  -- rawat inap pilih bed
function ranap_pilihbed(idbangsal,idbed,idpendaftaran,nobaris,nobed)
{
    
    if($('#nosep').val()=='' && $('#ranap_carabayar').val()!=='mandiri')
    {
        alert_empty('nosep');
    }
    else if($('#ranap_dokterdpjp').val()=='0')
    {
        alert_empty('dokter dpjp');
    }
    else if($('#ranap_kelasperawatan').val()=='0')
    {
        alert_empty('kelas perawatan');
    }
    else if($('#ranap_kelasjaminan').val()=='jaminan')
    {
        alert_empty('kelas jaminan');
    }
    else
    {
        $.confirm({
            icon: 'fa fa-question',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Konfirmasi!',
            content: 'Pastikan bed yang dipilih sudah sesuai...!<br> <b>Bed '+localStorage.getItem('bangsal')+' nomor '+nobed+'</b>',
            buttons: {
                confirm: function () { 
                startLoading();
                $.ajax({ 
                    url: 'pendaftaranranap_pilihbed',
                    type : "post",      
                    dataType : "json",
                    data : {israwatgabung:$('#isirawatgabung').val(), bangsal:idbangsal, bed:idbed, nosep:$('#nosep').val(), daftar:idpendaftaran,kelasperawatan:$('#ranap_kelasperawatan').val(),kelasjaminan:$('#ranap_kelasjaminan').val(),dokter:$('#ranap_dokterdpjp').val() },
                    success: function(result) {
                        stopLoading();
                        $('.jconfirm').hide();
                        notif(result.status, result.message);
                        // tablependaftaran.ajax.reload();
                         pendaftaranListRow(idpendaftaran,nobaris);//get data auto load beri antrian

                    },
                    error: function(result){          
                        notif(result.status, result.message);
                        return false;
                    }
                    
                });
            },
                cancel: function () {               
                }            
            }
        });
    }
}
// -- pindah rawat inap
function pindahRanap(value,idpendaftaran,nobaris)
{
    startLoading();
    var mSize='lg'
    var mTitle=' Pindah Rawat Inap';
    var mContent='';
    var menusave='simpan';
    var menubatal='batal';
    var url= base_url+'cadmission/pendaftaran_pindahranap';
    var data = $("#formPindahRanap").serialize();
    
    mContent +='<form action="" id="formPindahRanap">'+
    '<input type="hidden" name="norm" value="'+ value +'" />'+
    '<input type="hidden" name="idpendaftaran" value="'+ idpendaftaran +'" />'+

'<div class="pad margin ">'+
  '<div class="callout callout-warning" style="margin-bottom: 0!important;">'+
    '<h4><i class="fa fa-default"></i> Identitas Pasien:</h4>'+
    '<div id="pindahranap_identitasPasien"></div>'+
  '</div>'+
'</div>'+

    '<div class="col-sm-6" style="margin:none;">'+
        '<div class="box no-border" style="margin:none;">'+
                '<div class="box-body">'+
                '<div class="chart">'+
                    '<table border="1"><tbody style="display:block;overflow:auto;"><tr id="listStatusBangsal"></tr></tfoot></table>'+
                '</div>'+
            '</div>'+
        '</div>'+    
    '</div>'+

    '<div class="col-sm-4">'+
      '<div class="input-group">'+
        '<input type="text" id="nosep" class="form-control" placeholder="No.SEP">'+
        '<input type="hidden" id="ranap_carabayar"/>'+
        '<div class="input-group-addon" style="padding:4px;">'+
          '<a class="btn btn-xs btn-info" onclick="info_sep()"><i class="fa fa-question-circle"></i></a>'+
        '</div>'+
      '</div>'+
      
      '<div class="form-group" style="margin-top:4px;">'+
        '<select id="ranap_dokterdpjp" class="form-control select2"><option>Dokter DPJP</option></select>'+
        '<select id="ranap_kelasperawatan" class="form-control" style="margin-top:4px;"><option>Kelas Perawatan</option></select>'+
        '<select id="ranap_kelasjaminan" class="form-control" style="margin-top:4px;"><option>Kelas Jaminan</option></select>'+
      '</div>'+
    '</div>'+

    '<div class="col-sm-2">'+
        '<input type="checkbox" id="israwatgabung" class="flat-red"> Pasien Rawat Gabung'+
        '<input type="hidden" id="isirawatgabung"/>'+
    '</div>'+


    '<div class="col-sm-12" style="margin:none;">'+
    '<h4>Pilih Bangsal :</h4>'+
        '<table><tbody style="display:block;overflow:auto;"><tr id="listBangsal"></tr></tfoot></table>'+
        '</div>'+  

    '<div class="col-md-12">'+
        '<style type="text/css">.fixed_header tbody{display:block;overflow:auto;height:200px;width:100%.fixed_header thead tr{display:block}</style>'+
        '<div class="box no-border">'+
            '<div class="with-border">'+
                '<h3 class="box-title" id="judul_listbed"> List Bed</h3>'+
                '</div>'+
                '<div class="box-body">'+
                '<div class="lablel label-warning" id="infobangsal"></div>'+
                '<div class="chart">'+
                '<table" id="listBed"></table>'+
                '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+    
    '</div>'+
'</div>'+
'<div class="col-sm-12" style="margin:4px 0px;"></div>';
$.confirm({
    title: mTitle,
    content: mContent,
    columnClass: mSize,
    autoRefresh:true,
    buttons: {
        formReset:{
            text: menubatal,
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    onchange_checkboxisrawatgabung();
    stopLoading();
    ranap_listbangsal(value,idpendaftaran,nobaris);
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
});
}
//// --- generate no antrian
// include file js antrian_antrian
includejs('/assets/pages/antrian_antrian.min.js');
//dipanggil dari a id pemeriksaanklinikBeriantrian - di dalam fungsi tampilkanantrian()
$(document).on("click","#pemeriksaanklinikBeriantrian", function(){
    var idp = $(this).attr("alt");
    var nobaris = $(this).attr("nobaris");
    var idu = $(this).attr("alt2");
    var idpr = $(this).attr("alt3");
    var id = $(this).attr("alt4");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Beri Antrian?',
        buttons: {
            confirm: function () { 
            // setTimeout(function(){
            beriAntrian(idp,idu,idpr,id,nobaris);
        // update isantri di pendaftaran},500);
            
            // window.location.reload(true);
            
        },
            cancel: function () {               
            }            
        }
    });
});
function gantiDokterPraktek()
{
    startLoading();
    var mTitle=' Ganti Dokter Praktek';
    var mContent='';
    var url= base_url+'cadmission/gantiDokterPraktek';
    var data = $("#form").serialize();
    
    mContent +='<form action="" id="form">'+
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
        '<label>Tanggal</label>'+
        '<input id="tanggalgantidokter" class="form-control" onchange="cari_poliklinik(this.value)" >'+
      '</div>'+
    '</div>'+
    
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
        '<label>Jadwal Dokter</label>'+
        '<select id="poliklinik" name="jadwaldokter" class="form-control select2"><option>Pilih Jadwal</option></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-sm-12">'+
      '<div class="form-group">'+
      '<label>Dokter Pengganti</label><br>'+
      '<input type="radio" name="statusdokter" value="dokterdalam" checked /> '+
      '<span>Dokter Dalam</span> '+
      ' <input type="radio" name="statusdokter" value="dokterluar" /> '+
      '<span>Dokter Luar</span>'+
      '<select id="pilihdokterpengganti" name="dokterpengganti" class="form-control select2"><option>Pilih Dokter</option></select>'+
      '</div>'+
    '</div></form>';
$.confirm({
    title: mTitle,
    content: mContent,
    columnClass: 'small',
    autoRefresh:true,
    buttons: {
        formSubmit:{
            text:'Simpan',
            btnClass: 'btn-primary',
            action: function (){
                    
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url +'cadmission/gantiDokterPraktek', //alamat controller yang dituju (di js base url otomatis)
                    data: $("#form").serialize(), //
                    dataType: "JSON", //tipe data yang dikirim
                    //jika  berhasil
                    success: function(result) {
                        notif(result.status, result.message);
                    },
                    //jika error
                    error: function(result) {
                        console.log(result.responseText);
                    }
                });
            }
            
        },
        formReset:{
            text:'Batal',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    stopLoading();
    //panggil dokter
    $('#tanggalgantidokter').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
    tampildokter();
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
   });
}

function tampildokter()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url +'cadmission/tampildokter', //alamat controller yang dituju (di js base url otomatis)
        data: $("#form").serialize(), //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            var html='<option value="0">Pilih</option>';
            for(var x in result){html+='<option value="'+result[x].idpegawai+'">'+result[x].namadokter+'</option>';}
            $('#pilihdokterpengganti').empty();
            $('#pilihdokterpengganti').html(html);
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}

function pendaftaranListRow(id,nobaris)
{
    search_by_date_range();
}

$(document).on('click','#bookingjadwaloperasi',function(){
    var baris = $(this).attr('nobar');
    var mContent='<form action="" id="formbookingjadwalop">'+

    '<div class="col-xs-12">'+
      '<div class="bg-info"><p class="small">Perhatikan Waktu Rencana Operasi,<br> Bed tidak bisa dipilih jika pada waktu yang direncanakan sudah dipesan.</p></div>'+
    '</div>'+
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Waktu Rencana Operasi (Y-M-D H:m)</label>'+
        '<input type="hidden" name="idp" value="'+$(this).attr('idp')+'" />'+
        '<input id="setwaktuoperasi" class="form-control datetime" type="text" name="waktuoperasi"/>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Bangsal</label>'+
        '<select id="idbangsalop" onchange="caribedoperasi(this.value)" name="idbangsal" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Bed/Ruang</label>'+
        '<select id="idbedop" name="idbed" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '<div class="col-xs-12">'+
      '<div class="form-group">'+
        '<label>Jenis Tindakan</label>'+
        '<select id="idjenistindakanop" name="idjenistindakan" class="form-control select2"></select>'+
      '</div>'+
    '</div>'+
    
    '</form>';
$.confirm({
    title: 'Booking Jadwal Operasi',
    content: mContent,
    closeIcon: true,
    closeIconClass: 'fa fa-close',
    columnClass: 'col-xs-offset-2 col-xs-8',
    autoRefresh:true,
    buttons: {
        formSubmit:{
            text:'Booking',
            btnClass: 'btn-primary',
            action: function (){
                if($('select[name="idbangsal"]').val()=='0'){
                    alert_empty('Bangsal belum dipilih.!');
                    return false;
                }else if($('select[name="idbed"]').val()=='0'){
                    alert_empty('Bed belum dipilih.!');
                    return false;
                }else if($('select[name="idjenistindakan"]').val()=='0'){
                    alert_empty('Jenis tindakan belum dipilih.!');
                    return false;
                }else{
                    startLoading();
                    $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url +'cadmission/bookingjadwaloperasi', //alamat controller yang dituju (di js base url otomatis)
                    data: $("#formbookingjadwalop").serialize(), //
                    dataType: "JSON", //tipe data yang dikirim
                    //jika  berhasil
                    success: function(result){
                        stopLoading();
                        (result.status=='success') ? $('.nobar'+baris).addClass('hide') : '';
                        notif(result.status, result.message);
                    },
                    //jika error
                    error: function(result) {
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
                }
            }
            
        },
        formReset:{
            text:'Batal',
            btnClass: 'btn-danger'
        }
    },
    onContentReady: function () {
    $('input[name="waktuoperasi"]').datetimepicker({format:"YYYY-MM-DD HH:mm", sideBySide: true,defaultDate:new Date()});
    get_databangsal($('#idbangsalop'),'','bangsaloperasi');
    $.ajax({
        type: "POST",
        url: base_url +'cadmission/carijenistindakanoperasi',
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) {
            var opt='<option value="0">Pilih</option>';
            for(x in result){opt+='<option value="'+result[x].idjenistindakan+'">'+result[x].jenistindakan+'</option>';}
            $('#idjenistindakanop').empty();
            $('#idjenistindakanop').html(opt);
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
    
    // bind to events
    var jc = this;
    this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
    });
    }
   });
});

function caribedoperasi(idbangsal)
{
    if(idbangsal==0){
        alert_empty('Bangsal Harap Dipilih');
    }else{
        $.ajax({
            type: "POST",
            url: base_url +'cadmission/caribedoperasi',
            data:{idbangsal:idbangsal,waktu:$('#setwaktuoperasi').val()},
            dataType: "JSON", //tipe data yang dikirim
            success: function(result) {
                var opt='<option value="0">Pilih</option>';
                for(x in result){opt+='<option '+((result[x].kodebooking==null) ? '' : 'disabled') +' value="'+result[x].idbed+'">'+result[x].bed+ ((result[x].kodebooking==null) ? '' : ' - bed sudah dipesan, mohon ubah waktu rencana operasi untuk bisa membuat jadwal.!') +'</option>';}
                $('#idbedop').empty();
                $('#idbedop').html(opt);
            },
            error: function(result) {
                fungsiPesanGagal();
                return false;
            }
        });
    }
}