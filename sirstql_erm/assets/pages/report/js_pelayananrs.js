$(function () 
{ 
    
})

function generate_rl51()
{
	var modalSize = 'xsmall';
    var modalTitle = 'Generate Jadwal';
    var modalContent = '<form action="" id="FormJadwal">' +
                            '<div class="form-group">' +
                                '<label>Input Tahun</label>' +
                                '<input type="text" name="tahun" placeholder="yyyy" class="datepicker form-control"/>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'unduh',
                btnClass: 'btn-blue',
                action: function () {
                    
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            $('.datepicker').datepicker({
            autoclose: true,
            minViewMode:2,
            format: "yyyy",
            orientation: "bottom",
            }).datepicker("setDate",'now'); //Initialize Date picker
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}