var table;
$(function () {

  // $( 'ul.nav nav-tabs' ).on( 'click', function() {
  //       $( this ).parent().find( 'li.active' ).removeClass( 'active' );
  //       $( this ).addClass( 'active' );
  // });
  $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
  switch($('#jsmode').val()) {
    case 'chart_demografikunjunganpasien':
      getdataDemografiPasien();
      break;
    default:
      // get_datapoli('#tampilpoli','');
      // $('.select2').select2();
      // dashboard_bypoli();
      // tampil_chartdemografikunjunganpasien();
  }
});
function printChart()
{
   chart.exportChart({format: "jpg"});
}

function tampil_tabledemografikunjunganpasien(tampil,jenislaporan='')
{
  var labels='';
  for(a in tampil.labels){ labels+='<tr><td>'+tampil.labels[a]+'</td><td></td><td></td></tr>';}
    console.log(tampil.datasets);
  var dtlist='<div class="col-xs-12 col-md-2"></div><div class="col-xs-12 col-md-8"><table class="table table-bordered"><body><tr><td>'+jenislaporan+'</td><td>Jumlah</td><td>Persentase</td></tr>'+labels+'</body></table></div>';
  $('.chart').empty();
  $('.chart').html(dtlist);
}
// menampilkan data menjadi chart
function tampil_chartdemografikunjunganpasien(tampil,jenislaporan='')
{
  // $('#navactivegrafikdemografi').empty();
  // $('#navactivegrafikdemografi').html();
 
  $('#demografipasien').empty();
  $('#demografipasien').html('<div class="box-body"><p class="text-center text-bold">Demografi Pasien Berdasarkan '+jenislaporan+' <br> Tanggal Pendaftaran Pasien Periode '+tampil.tanggal1+' - '+tampil.tanggal2+'<br> Total Kunjungan : '+tampil.total+' </p><div class="chart text-center"><canvas id="chart_demografikunjunganpasien" style="height:300px"></canvas></div></div>');
  var ctx = $('#chart_demografikunjunganpasien');
  if(localStorage.getItem('lsJenisTampilan')!=='wilayah')
  {
    var chart = new Chart(ctx, {
      type: ((jenislaporan=='Pekerjaan' || jenislaporan=='Pendidikan')?'horizontalBar':'pie'),
      data: {
        labels: tampil.labels,
        datasets: [{  data: tampil.dtchart,backgroundColor: tampil.backgroundColor,}]
      },
      options: {
      
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var allData = data.datasets[tooltipItem.datasetIndex].data;
              var tooltipLabel = data.labels[tooltipItem.index];
              var tooltipData = allData[tooltipItem.index];
              var total = 0;
              for (var i in allData) {total += parseInt(allData[i]);}
              var tooltipPercentage = angkadesimal((tooltipData / total) * 100,2);
              return tooltipLabel + ': ' + tooltipData + ' pasien (' + tooltipPercentage + '%)';
            }
          }

          
        },
        legend: {
          display: ((jenislaporan=='Pekerjaan' || jenislaporan=='Pendidikan')?false:true)
        },
        scales: ((jenislaporan=='Pekerjaan'|| jenislaporan=='Pendidikan')? { 
          xAxes: [{
              categorySpacing:0,
              ticks:{
              beginAtZero:true
            }
          }]
        }:'')
      }

    });
  }
  else
  {
    var chart = new Chart(ctx, {
      type:'bar',
      data: tampil.dtchart,
        // labels: ["Chocolate", "Vanilla", "Strawberry"],
        // datasets : tampil.dtchart,
    // datasets: [
    //     {
    //         label: "Blue",
    //         backgroundColor: "blue",
    //         data: [3,7,4]
    //     },
    //     {
    //         label: "Red",
    //         backgroundColor: "red",
    //         data: [4,3,5]
    //     },
    //     {
    //         label: "Green",
    //         backgroundColor: "green",
    //         data: [7,2,6]
    //     }
    // ]

      
      options: {
        // tooltips: {
        //   callbacks: {
        //     label: function(tooltipItem, data) {
              // var allData = data.datasets[tooltipItem.datasetIndex].data;
              // var tooltipLabel = data.datasets[tooltipItem.datasetIndex].label;
              // console.log(tooltipLabel);
              // var tooltipData = allData[tooltipItem.index];
              // var total = 0;
              // for (var i in allData) {total += parseInt(allData[i]);}
              // var tooltipPercentage = angkadesimal((tooltipData / total) * 100,2);
              // return tooltipLabel + ': ' + tooltipData + ' pasien (' + tooltipPercentage + '%)';
        //     }
        //   }
        // },


        tooltips: {
         // untuk mengatur tampilan tooltip
         mode: 'label',
         callbacks: {
            label: function(t, d) {
              var dstLabel = d.datasets[t.datasetIndex].label;
              var yLabel = t.yLabel;
              var allData = d.datasets[t.datasetIndex].data;
              var tooltipData = allData[t.index];
              var total = 0;
              // menghitunga total data dan parsing data ke integer
              for (var i in allData) {total += parseInt(allData[i]);}
              // perhitungan percentase
              var tooltipPercentage = ((tooltipData==0)? 0:angkadesimal((tooltipData / total) * 100,2));
              return dstLabel + ': ' + yLabel + ' pasien ('+ tooltipPercentage + '%)';
            }
         }
      },

        scales: {
          // untuk mengatur yAxes mulai dari 0
          yAxes:[{
            ticks:{
              beginAtZero:true
            }
          }]

        },
        // legend: {
        //   display: ((jenislaporan=='Pekerjaan' || jenislaporan=='Pendidikan')?false:true)
        // },
        // scales: ((jenislaporan=='Pekerjaan'|| jenislaporan=='Pendidikan')? { 
        //   xAxes: [{
        //       categorySpacing:0
        //   }]
        // }:'')
      }

    });
  }

}

// reset data demografi pasien pendaftaran
function resetDemografiPasien()
{
  $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
  $('select[name="jenislaporan"]').val('Agama');
  getdataDemografiPasien();

}
// ketika jenis laporan dipilih aksi yang dilakukan adalah baris dibawah ini ->  buat data local storage jenis tampilan
function jenistampilan(value){ localStorage.setItem('lsJenisTampilan',value); getdataDemografiPasien();}
// 
function getdataDemografiPasien(mode='')
{
  //siapkan data yang akan dikirim dalam bentuk json
  var jsondtsend = {
    jenistampilan:localStorage.getItem('lsJenisTampilan'),
    jenislaporan:$('select[name="jenislaporan"]').val(),
    tanggal1:ambiltanggal($('#tgl1').val()),
    tanggal2:ambiltanggal($('#tgl2').val()),
    mode:mode
    };

  startLoading();
  $.ajax({
      url:base_url+"creport/getdata_demografipasien",//url tujuan
      type:'POST',//tipe pengiriman data
      dataType:'JSON',//tipe data yg dikirim
      data:jsondtsend,//data yang dikirim
      success: function(data){//jika berhasil 
          stopLoading();
          ((mode=='tabel')? tampil_tabledemografikunjunganpasien(data,$('select[name="jenislaporan"]').val()):tampil_chartdemografikunjunganpasien(data,$('select[name="jenislaporan"]').val()));
      },
      error: function(data){ //jika eror
          stopLoading();
          fungsiPesanGagal();
          return false;
      }
  });
}

