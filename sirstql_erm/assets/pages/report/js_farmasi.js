var belanjaobat,pemakaianobat,defectaobat,obatkadaluarsa, resepobat, obatgenerikpaten,barangslowmoving,dtpenggunaanbaranginclude;

// var table;
$(document).ready(function () {
  caridokter($('#dokterresepobat'));
  select_belanjaobat();
  select_defectaobat();
  select_obatkadaluarsa();
  $('.tahunbulan').datepicker({autoclose: true, endDate: new Date(), format: "M yyyy",viewMode: "months",minViewMode: "months",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
  select_resepobat();
  select_obatgenerikpaten();
  select_pemakaianobat();
  select_pemakaianobatbebas();
  select_slowmovingbarang();
  tampil_penggunaanbarangincludetindakan();
  
  
  select2serachmulti($('#pilihbarang_includetindakan'),'cmasterdata/fillbarang');
});


$(document).on('click','#tampil_penggunaanbarangincludetindakan',function(){
    dtpenggunaanbaranginclude.ajax.reload(null,false);
});

$(document).on('click','#excel_penggunaanbarangincludetindakan',function(){
    var url = base_url+'creport/unduhexcel/penggunaanbarangincludetindakan';
    var form = $('<form action="' + url + '" method="post">' +
      '<input type="hidden" name="min" value="'+$('#min_includetindakan').val()+'" />' +
      '<input type="hidden" name="max" value="'+$('#max_includetindakan').val()+'" />' +
      '<input type="hidden" name="idunit" value="'+$('#pilihunitbarang_includetindakan').val()+'" />' +
      '<input type="hidden" name="idbarang" value="'+$('#pilihbarang_includetindakan').val()+'" />' +
      '<input type="hidden" name="namaunit" value="'+$('#pilihunitbarang_includetindakan option:selected').text()+'" />' +
      '</form>');
    $('body').append(form);
    form.submit();
});

$(document).on('click','#reload_penggunaanbarangincludetindakan',function(){
    $('input[type="search"]').val('').keyup();
    dtpenggunaanbaranginclude.state.clear();
    $('#pilihbarang_includetindakan').empty();
    $('#pilihbarang_includetindakan').html('<option value="">Pilih Barang</option>');
    select2serachmulti($('#pilihbarang_includetindakan'),'cmasterdata/fillbarang');
    $('#max_includetindakan').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    $('#min_includetindakan').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    dtpenggunaanbaranginclude.ajax.reload();
});

//laporan penggunaan barang bhp/alkes include tindakan
function tampil_penggunaanbarangincludetindakan()
{
  $('#pilihunitbarang_includetindakan').select2();
  dtpenggunaanbaranginclude = $('#dtpenggunaanbarangincludetindkaan').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "data":{
            idbarang:function(){return $('#pilihbarang_includetindakan').val()}, 
            idunit:function(){return $('#pilihunitbarang_includetindakan').val()}, 
            min:function(){return $('#min_includetindakan').val()}, 
            max:function(){return $('#max_includetindakan').val()}
        },
        "url": base_url+'creport/dt_penggunaanbarangincludetindakan',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [ ],"orderable": false,},],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
//       $(nRow).attr('id', 'row' + iDataIndex);
    },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
       $('[data-toggle="tooltip"]').tooltip();
    },
    });
  $(".datepicker").datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
}


//-- unduh excel laporan
//mahmud,clear
function excel_laporanpenggunaanresepbulanan()
{
    var url = base_url+'creport/unduhexcel/laporanpenggunaanresepbulanan';
    var form = $('<form action="' + url + '" method="post">' +
      '<input type="hidden" name="tahunbulan" value="'+$('#tahunbulan_laporan1').val()+'" />' +
      '</form>');
    $('body').append(form);
    form.submit();
}
//-- view laporan
//mahmud,clear
function tampil_laporanpenggunaanresepbulanan()
{
    startLoading();
    $.ajax({
        url: base_url+ "creport/view_laporanpenggunaanresepbulanan",
        dataType:"JSON",
        type:"POST",
        data:{
            tahunbulan:$('#tahunbulan_lp_resepbulanan').val()
        },
        success:function(result){
            stopLoading();
            var rows   = '';
            var jumlahpasien = 0;
            var idpendaftaran = 0;
            var tanggal  = '';
            var idbarang = 0;
            var jumlahresep = 0;
            var jumlahracikan = 0;
            var idbarangpemeriksaangrup = 0;
            for(var x in result)
            {
                
                
                //tampil data
                if(result[x].tanggal != tanggal && tanggal !='')
                {
                    rows += '<tr><td>'+tanggal+'</td><td>'+jumlahpasien+'</td><td>'+jumlahracikan+'</td><td>'+jumlahresep+'</td></tr>';
                    jumlahpasien = 0;
                    jumlahresep  = 0;
                    jumlahracikan = 0;
                }
                
                
                //hitung jumlah pasien
                if(result[x].idpendaftaran != idpendaftaran)
                {
                    jumlahpasien += 1;
                }
                
                //hitung jumlah resep Obat (R)
                if(result[x].idbarang != idbarang && result[x].grup == 0)
                {
                    jumlahresep += 1;
                }
                
                //hitung jumlah racikan (R)
                if(result[x].grup != 0 && result[x].idbarangpemeriksaangrup != idbarangpemeriksaangrup )
                {
                    jumlahracikan += 1;
                }
                
                
               
                idpendaftaran = result[x].idpendaftaran;
                tanggal       = result[x].tanggal;
                idbarang      = result[x].idbarang;
                idbarangpemeriksaangrup          = result[x].idbarangpemeriksaangrup;
                
            }
            var table = '<table id="tbl_laporanpenggunaanresepbulanan" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                    +'<thead>'
                    +'<tr class="bg bg-yellow-gradient">'
                      +'<th>Tanggal</th>'
                      +'<th>Jumlah Pasien</th>'
                      +'<th>Jumlah Racikan atau Puyeran (R)</th>'
                      +'<th>Jumlah Obat (R)</th>'
                    +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    + ((rows == '') ? '<tr><td colspan="4">No data available in table</td></tr>' : rows )
                    +'</tfoot>'
                  +'</table>';
            
            $('#view_laporanpenggunaanresepbulanan').empty();
            $('#view_laporanpenggunaanresepbulanan').html(table);
            $('#tbl_laporanpenggunaanresepbulanan').dataTable();
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//-- unduh excel laporan
//mahmud,clear
function excel_laporanpenggunaanobatperbulan()
{
    var url = base_url+'creport/unduhexcel/laporanpenngunaanobatbulanan';
    var form = $('<form action="' + url + '" method="post">' +
      '<input type="hidden" name="tahunbulan" value="'+$('#tahunbulan_laporan1').val()+'" />' +
      '</form>');
    $('body').append(form);
    form.submit();
}
//-- view laporan
//mahmud,clear
function tampil_laporanpenggunaanobatperbulan()
{
    startLoading();
    $.ajax({
        url: base_url+ "creport/view_laporanpenggunaanobatperbulan",
        dataType:"JSON",
        type:"POST",
        data:{
            tahunbulan:$('#tahunbulan_laporan1').val()
        },
        success:function(result){
            stopLoading();
            var rows = '';
            var jumlah  = 0;
            var subbpjs = 0;
            var subbumum= 0;
            for(var x in result)
            {
                if(result[x].resep == 0 && result[x].bebas == 0)
                {
                    //
                }
                else
                {
                    jumlah   = parseFloat(result[x].resep) + parseFloat(result[x].bebas);
                    subbpjs  = parseFloat(result[x].hargabpjs) + parseFloat(jumlah);
                    subbumum = parseFloat(result[x].hargaumum) + parseFloat(jumlah);
                    rows+= '<tr><td>'+result[x].namabarang+'</td><td>'+convertToRupiahDecimal(result[x].resep)+'</td><td>'+convertToRupiahDecimal(result[x].bebas)+'</td><td>'+convertToRupiahDecimal(jumlah)+'</td><td>'+convertToRupiah(result[x].hargabpjs)+'</td><td>'+convertToRupiahDecimal(subbpjs)+'</td><td>'+convertToRupiah(result[x].hargaumum)+'</td><td>'+convertToRupiahDecimal(subbumum)+'</td></tr>';
                }
            }
            var table = '<table id="tbl_laporan1" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
                    +'<thead>'
                    +'<tr class="bg bg-yellow-gradient">'
                      +'<th>Nama Barang</th>'
                      +'<th>Resep</th>'
                      +'<th>Bebas</th>'
                      +'<th>Jumlah</th>'
                      +'<th>Harga BPJS</th>'
                      +'<th>Subtotal</th>'
                      +'<th>Harga Umum</th>'
                      +'<th>Subtotal</th>'
                    +'</tr>'
                    +'</thead>'
                    +'<tbody>'
                    + ((rows == '') ? '<tr><td colspan="8">No data available in table</td></tr>' : rows )
                    +'</tfoot>'
                  +'</table>';
            
            $('#view_laporan1').empty();
            $('#view_laporan1').html(table);
            $('#tbl_laporan1').dataTable();
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//-- laporan distribusi gudang

//-- unduh excel laporan
//mahmud,clear
function excel_laporandistribusigudang()
{
    var url = base_url+'creport/unduhexcel/laporandistribusigudang';
    var form = $('<form action="' + url + '" method="post">' +
      '<input type="hidden" name="tahunbulan" value="'+$('#tahunbulan_distribusigudang').val()+'" />' +
      '<input type="hidden" name="mode" value="distribusigudang" />' +
      '</form>');
    $('body').append(form);
    form.submit();
}
//-- view laporan
//mahmud,clear
function tampil_laporandistribusigudang()
{
    startLoading();
    $.ajax({
        url: base_url+ "creport/view_laporandistribusigudang",
        dataType:"JSON",
        type:"POST",
        data:{
            tahunbulan:$('#tahunbulan_distribusigudang').val(),
            mode:'distribusigudang'
        },
        success:function(result){
            list_laporan_distribusigudang(result,'tbl_laporandistribusigudang','view_laporandistribusigudang');
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}


//-- view laporan
//mahmud,clear
function tampil_laporandetaildistribusigudang()
{
    startLoading();
    $.ajax({
        url: base_url+ "creport/view_laporandistribusigudang",
        dataType:"JSON",
        type:"POST",
        data:{
            tanggal1:$('#lddg_tanggal1').val(),
            tanggal2:$('#lddg_tanggal2').val(),
            mode:'detaildistribusigudang'
        },
        success:function(result){
            list_laporan_distribusigudang(result,'tbl_laporandetaildistribusigudang','view_laporandetaildistribusigudang');
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

// -- list data laporan distribusi gudang
//mahmud, clear
function list_laporan_distribusigudang(result,idtabel,idframekonten)
{
    stopLoading();
    var rows   = '';
    var masuk  = 0;
    var keluar = 0;
    var namabarang = '';
    for(var x in result)
    {
        if(result[x].jenisdistribusi == 'masuk' || result[x].jenisdistribusi == 'pembetulanmasuk')
        {
            if(result[x].jenisdistribusi == 'masuk'){
                masuk += parseFloat(result[x].jumlah);
            }else{
                masuk -= parseFloat(result[x].jumlah);
            }
        }

        if(result[x].jenisdistribusi == 'keluar' || result[x].jenisdistribusi == 'pembetulankeluar')
        {
            if(result[x].jenisdistribusi == 'keluar'){
                keluar += parseFloat(result[x].jumlah);
            }else{
                keluar -= parseFloat(result[x].jumlah);
            }

        }

        if(namabarang != result[x].namabarang && namabarang != '')
        {
            rows += '<tr><td>'+result[x].namabarang+'</td><td>'+convertToRupiahDecimal(masuk)+'</td><td>'+convertToRupiahDecimal(keluar)+'</td><td>'+result[x].namasatuan+'</td></tr>';
            masuk = 0;
            keluar = 0;
        }
        namabarang = result[x].namabarang;

    }
    var table = '<table id="'+idtabel+'" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
            +'<thead>'
            +'<tr class="bg bg-yellow-gradient">'
              +'<th>Nama Barang</th>'
              +'<th>Masuk</th>'
              +'<th>Keluar</th>'
              +'<th>Satuan</th>'
            +'</tr>'
            +'</thead>'
            +'<tbody>'
            + ((rows == '') ? '<tr><td colspan="4">No data available in table</td></tr>' : rows )
            +'</tfoot>'
          +'</table>';

    $('#'+idframekonten).empty();
    $('#'+idframekonten).html(table);
    
    if(rows != '')
    {
        $('#'+idtabel).dataTable();
    }
    
}



//-- convert to rupiah decimal
//mahmud,clear
function convertToRupiahDecimal(num)
{
    var num = parseFloat(num);
    var result = 0;
    var mod = num % 1;
    // num % 1 != 0
    // 23 % 1 = 0
    // 23.5 % 1 = 0.5
    if(num !=0)
    {
        if(mod == 0)
        {
            result = convertToRupiah(num);
        }
        else 
        {
            var num = String(num);
            var dot = num.indexOf(".");
            var dec= parseInt(dot)+1;
            var num_int = num.slice(0,dot);
            var num_dec = num.slice(dec);
            var nominal = convertToRupiah(num_int);
            result = nominal +','+num_dec;
        }
        return result;
    }
    else
    {
        return num;
    }
    
}

function select_slowmovingbarang()
{
    barangslowmoving = $('#barangslowmoving').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
        "ajax": {"url": base_url+'creport/list_slowmovingobat',"type": "POST"},
        "columnDefs": [{ "targets": [ ],"orderable": false,},],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
       $(nRow).attr('id', 'row' + iDataIndex);
    },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
       $('[data-toggle="tooltip"]').tooltip();
    },
    });
}

function select_belanjaobat()
{
  belanjaobat = $('#belanjaobat').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "url": base_url+'creport/list_belanjaobat',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [ ],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}
/** menampilkan data defecta obat */
function select_defectaobat()
{
  defectaobat = $('#defectaobat').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "url": base_url+'creport/list_defectaobat',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [ ],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
     });
}
/** menampilkan data  obat kadaluarsa*/
function select_obatkadaluarsa()
{
    obatkadaluarsa = $('#obatkadaluarsa').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
        "ajax": {
            "url": base_url+'creport/list_obatkadaluarsa',
            "type": "POST"
        },
        "columnDefs": [{ "targets": [ ],"orderable": false,},],
       "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
          $(nRow).attr('id', 'row' + iDataIndex);
       },
       "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
       },
    });
}


/** menampilkan data pemakaian obat bebas*/
function setpencarianobatbebas()
{
  $('#pilihobatbebas').select2({
    placeholder: "Cari Obat/Bhp",
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cpelayanan/pemeriksaanklinik_caribhp",dataType: 'json',delay: 100,cache: false,
        data: function (params) {return {q: params.term,page: params.page || 1,};},
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idbarangpembelian + ',' +item.idbarang, text: item.namabarang +'  | stok '+ convertToRupiah(item.stok) +' '+ item.namasatuan +' | harga '+ convertToRupiah(item.hargajual)  + ((item.kadaluarsa==0) ? '' : ' | Exp:'+ item.kadaluarsa )  }}),
                pagination: {
                    more: true
                }
            };
        },              
    }
    });
}

/** menampilkan data pemakaian obat */
function setpencarianobat()
{
  $('#pilihobat').select2({
    placeholder: "Cari Obat/Bhp",
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cpelayanan/pemeriksaanklinik_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {return {q: params.term,page: params.page || 1,};},
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idbarangpembelian + ',' +item.idbarang, text: item.namabarang +'  | stok '+ convertToRupiah(item.stok) +' '+ item.namasatuan +' | harga '+ convertToRupiah(item.hargajual)  + ((item.kadaluarsa==0) ? '' : ' | Exp:'+ item.kadaluarsa )  }}),
                pagination: {
                    more: true
                }
            };
        },              
    }
    });
}
function setpencarianpasien()
{
  $('#pilihpasien').select2({
    placeholder: "Cari Pasien",
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cmasterdata/json_serachpasien",
        dataType: 'json',
        type:'post',
        delay: 100,
        cache: false,
        data: function (params) {
            return {
                cari: params.term,
            };
        },
        processResults: function(data, params) { 
            return { results:data};
        },              
    }
    });
}

function reloadpembelianobatbebas()
{
  $('#pilihobatbebas').empty();
  pembelianobatbebas.ajax.reload();
}

$(document).on('change','#pilihobatbebas',function(){
    pembelianobatbebas.ajax.reload();
});

function select_pemakaianobatbebas()
{
    setpencarianobatbebas();
    pembelianobatbebas = $('#pembelianobatbebas').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],
    "ajax":
    {
        "data":
        {
            obat:function(){return $('#pilihobatbebas').val()}, 
            min:function(){return $('#tgl1obatbebas').val()}, 
            max:function(){return $('#tgl2obatbebas').val()},
            custompage:function(){ return localStorage.getItem('customspage')}
        },
        "url": base_url+'creport/list_penjualanobatbebas',
        "type": "POST"
    },
    "columnDefs": [{ 
            "targets": [ ],
            "orderable": false,
        },],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
       $(nRow).attr('id', 'row' + iDataIndex);
    },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
       $('[data-toggle="tooltip"]').tooltip();
    },
    });
  $(".datepicker").datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
}

function excel_pemakaianobatbebas()
{
  var tgl1 = $('#tgl1obatbebas').val();
  var tgl2 = $('#tgl2obatbebas').val();
  window.location.href=base_url+"creport/downloadexcel_page/"+tgl1+'|'+tgl2+' laporandataobatbebas';
}

function reloadpemakaianobat()
{
  $('#pilihpasien').empty();
  $('#pilihobat').empty();
  pemakaianobat.ajax.reload();
}
function select_pemakaianobat()
{
  setpencarianobat();
  setpencarianpasien();
  pemakaianobat = $('#pemakaianobat').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "data":{
            pasien:function(){return $('#pilihpasien').val()}, 
            obat:function(){return $('#pilihobat').val()}, 
            min:function(){return $('#min').val()}, 
            max:function(){return $('#max').val()},
            custompage:function(){ return localStorage.getItem('customspage')}
        },
        "url": base_url+'creport/list_pemakaianobat',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [ ],"orderable": false,},],
    "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
       $(nRow).attr('id', 'row' + iDataIndex);
    },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
       $('[data-toggle="tooltip"]').tooltip();
    },
    });
  $(".datepicker").datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
}
/** unduh format excel data pemakaian obat */
function excel_pemakaianobat()
{
  var tgl1 = $('#min').val();
  var tgl2 = $('#max').val();
  window.location.href=base_url+"creport/downloadexcel_page/"+tgl1+'|'+tgl2+' laporandataobat';
}
/** ambil data dokter */
function caridokter(selectId)
{
   $.ajax({
        url: base_url+ "cadmission/masterjadwal_caridokter",
        dataType:"JSON",
        type:"POST",
        success:function(result){
            var option ='<option value="">Pilih Dokter</option>';
            for(var x in result){option +='<option value="'+result[x].idpegawai+'"> ' + result[x].titeldepan + ' '+result[x].namalengkap+' '+ result[x].titelbelakang + '</option>';}
            selectId.empty();
            selectId.html(option);
            selectId.select2();
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
/** ambil data resep obat */
function select_resepobat()
{
    $.ajax({
        url: base_url+ "creport/list_resepobat",
        dataType:"JSON",
        type:"POST",
        data:{
            tb:$('#tbresepobat').val(),
            dok:$('#dokterresepobat').val()
        },
        success:function(result){
            var dataResep = '', no=0, unit='';
              for (var x in result) 
              {
                if(unit != result[x].namaunit)
                {
                    no=0;
                    if(unit=='' || unit != result[x].namaunit){dataResep +='<tr><td>'+result[x].dokter+'</td><td></td><td></td><td></td></tr>';}
                    dataResep +='<tr><td>'+result[x].namaunit+'</td><td></td><td></td><td></td></tr>';
                }
                dataResep +='<tr><td>'+ ++no +'</td><td>R/ &nbsp;'+ result[x].namabarang +'</td><td>'+ convertToRupiah(result[x].hargajual) +'</td><td></td></tr>';
                unit=result[x].namaunit;
              }
            $('#listresepobat').empty();
            $('#listresepobat').html('<table id="listDataResep"  class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%"><thead><tr><td>No</td><td>Resep</td><td>@harga</td><td></td></tr></thead><tbody>'+dataResep+'</tbody></table>');
            
            $('#listDataResep').dataTable({"aaSorting": [],"columnDefs": [{"targets": [ 0,1,2 ], "orderable": false}]});
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//--unduh format excel data resep
function excel_resepobat()
{
  var tb = $('#tbresepobat').val();
  var dokter = $('#dokterresepobat').val();
  if(!dokter)
  {
    $.alert('Pilih dokter dahulu.!');
    return;
  }
  window.location.href=base_url+"creport/pelaporanresepexcel/"+tb+'/'+dokter;
}

//--ambil data obat generik paten
function select_obatgenerikpaten()
{
  obatgenerikpaten = $('#obatgenerikpaten').DataTable({
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],"ajax": {
        "data":{
            tahunbulan:function(){return $('#generikpatenmonth').val();}
        },
        "url": base_url+'creport/list_obatgenerikpaten',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [ ],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) {$(nRow).attr('id', 'row' + iDataIndex);}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
  $('#generikpatenmonth').datepicker({autoclose: true, endDate: new Date(), format: "M yyyy",viewMode: "months",minViewMode: "months",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
}

/** unduh format excel data obat generik paten */
function excel_obatgenerikpaten()
{
  var tb = $('#generikpatenmonth').val();
  window.location.href=base_url+"creport/pelaporanogpexcel/"+tb;
}
