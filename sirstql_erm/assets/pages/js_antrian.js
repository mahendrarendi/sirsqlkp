/**
 * Mahmud clear
 * jika file ada perubahan pada antrian.js, pada file antrian/v_tampilantrian.php harap versi (js?ver=) Java Script diperbarui. ex: namafile.min.js?ver=2, 
 */

//set local storage cek sudah panggil antrian
localStorage.setItem('sudahpanggil','');
$( document ).ready(function() {
    panggil();
    setInterval('displayServerTime()', 5000);
    var myvid = document.getElementById('play_video');
    var myvids = [
        base_url+"assets/display2/2 Apa itu Penyakit STROKE_.mp4",
        base_url+"assets/display2/3 Apa itu Kolesterol_.mp4",
        base_url+"assets/display2/4 Apa itu Tuberculosis (TBC)_ - Feat. Palang Merah Indonesia.mp4",
        base_url+"assets/display2/TELEMEDICINE.mp4",
        base_url+"assets/display2/5 Apa Manfaat Kopi_.mp4",
        base_url+"assets/display2/6 Apakah Mata Minus Bisa Sembuh_.mp4",
//        base_url+"assets/display2/6 Buah dan Sayur Pengusir Asam Urat.mp4",
//        base_url+"assets/display2/6 Cara Menjaga Kesehatan Mata agar Tetap Sehat.mp4",
        base_url+"assets/display2/7 Cara Membersihkan Telinga!.mp4",
//        base_url+"assets/display2/7 Makanan untuk menurunkan darah tinggi.mp4",
        base_url+"assets/display2/8 Cara Menghilangkan Bau Mulut!.mp4",
//        base_url+"assets/display2/9 Ciri-ciri, tanda & gejala kolesterol tinggi.mp4",
        base_url+"assets/display2/9 Demam Berdarah-Haruskah Minum Jus Jambu__.mp4",
        base_url+"assets/display2/9 Makanan penurun kolesterol tinggi dengan cepat.mp4",
        base_url+"assets/display2/10 Faktor Risiko Hipertensi dan Cara Mencegahnya.mp4",
        base_url+"assets/display2/10 Pesan Umum Gizi Seimbang.mp4",
        base_url+"assets/display2/11 Geng Sehat Sudah Mulai Stres_ Begini Cara Mengelola Stres yang Benar!_1.mp4",
        base_url+"assets/display2/12 Kenapa Kita Bisa Alergi_.mp4",
        base_url+"assets/display2/13 Kenapa sih Rokok itu Berbahaya_.mp4",
        base_url+"assets/display2/TELEMEDICINE.mp4",
        base_url+"assets/display2/14 Makanan Murah Pereda Stres.mp4",
        base_url+"assets/display2/15 Tahap Perkembangan Anak 0-6 Bulan.mp4",
        base_url+"assets/display2/16 Waspada Serangan Jantung! Pahami Risikonya Berikut Ini.mp4",
//        base_url+"assets/display2/17.mp4",
        base_url+"assets/display2/18 PENTING NYA PEMERIKSAAN PAP SMEAR.mp4",
//        base_url+"assets/display2/19 PERJUANGAN TENAGA MEDIS.mp4",
        base_url+"assets/display2/20 EVENT RUMAH SAKIT BERSAMA ARTIS.mp4",
        base_url+"assets/display2/21 PERSALINAN AMAN SAAT PANDEMI DI RSU QUEEN LATIFA.mp4",
        base_url+"assets/display2/22 PERSALINAN AMAN.mp4",
//        base_url+"assets/display2/23 Rumah Sakit Bebas Covid-19.mp4",
        base_url+"assets/display2/25 Vaksin.mp4",
        base_url+"assets/display2/26 Diabetes.mp4",
        base_url+"assets/display2/27.mp4",
        base_url+"assets/display2/28 Aktivitas Janin 24 Jam Sebelum Dilahirkan.mp4",
        base_url+"assets/display2/29 Cara Alami Percepat Pembukaan saat Bersalin.mp4",
        base_url+"assets/display2/30 Fakta Imunisas.mp4",
        base_url+"assets/display2/31 Momok Bernama Campak dan Rubella.mp4",
        base_url+"assets/display2/32 Proses dan Tahap Persalinan Normal.mp4",
        base_url+"assets/display2/A CARA PEMAKAIAKN APAR.mp4",
//        base_url+"assets/display2/Anemia_ Penyabab Anemia dan Pengobatan Penyakit Anemia_youtube_original.mp4",
        base_url+"assets/display2/Animasi - Ayo Cuci Tangan Pakai Sabun.mp4",
        base_url+"assets/display2/Animasi - Ayo Makan Sayur dan Buah.mp4",
        base_url+"assets/display2/Animasi HIV Aids.mp4",
        base_url+"assets/display2/Animasi Mengenai Diabetes Melitus (2).mp4",
        base_url+"assets/display2/Animasi Mengenai Hipertensi.mp4",
        base_url+"assets/display2/TELEMEDICINE.mp4",
        base_url+"assets/display2/Apa itu Demam berdarah - Copy.mp4",
        base_url+"assets/display2/Apa itu Thalasemia (Video Animasi Pengenalan  Thalassaemia ) - Copy.mp4",
        base_url+"assets/display2/Beginilah Cara Kerja Vaksin - Copy.mp4",
        base_url+"assets/display2/bpjs.mp4",
        base_url+"assets/display2/Cara Cuci Tangan Menurut WHO - Kesehatan - Copy.mp4",
        base_url+"assets/display2/Cara Pemberian ASI Eksklusif - Copy.mp4",
        base_url+"assets/display2/Edukasi Diabetes Melitus (DM) - Copy.mp4",
        base_url+"assets/display2/Gerakan Janin dalam USG 4D.mp4",
        base_url+"assets/display2/Iklan PSA Kemenkes RI - Imunisasi Lanjutan 'Boneka' 30s (2018) - Copy.mp4",
        base_url+"assets/display2/Iklan PSA Kemenkes RI X Germas - Ayo Makan Ikan 30s (2017) - Copy.mp4",
        base_url+"assets/display2/Iklan PSA Kemenkes RI X Germas - Cegah Anemia Dengan Obat TTD 30s (2017) - Copy.mp4",
        base_url+"assets/display2/Iklan PSA Kemenkes RI x Novell - Ingat Kolesterol Ingat CALL 30s (2018) - Copy.mp4",
        base_url+"assets/display2/ILM Kementerian Kesehatan Republik Indonesia (2017) - Copy.mp4",
//        base_url+"assets/display2/Infografis Iklan Layanan Masyarakat - Sayur dan Buah Kementerian Kesehatan RI_HD.mp4",
        base_url+"assets/display2/Inst-video-1.mp4",
        base_url+"assets/display2/Inst-video-2.mp4",
        base_url+"assets/display2/Inst-video-4.mp4",
//        base_url+"assets/display2/Inst-video-5.mp4",
        base_url+"assets/display2/Inst-video-6.mp4",
        base_url+"assets/display2/TELEMEDICINE.mp4",
        base_url+"assets/display2/Inst-video-7.mp4",
//        base_url+"assets/display2/Inst-video-8.mp4",
        base_url+"assets/display2/KALCare Hipertensi Video.mp4",
        base_url+"assets/display2/kartupasien.mp4",
        base_url+"assets/display2/Kenali 5 Tanda Bayi Anda Sehat Dalam Kandungan.mp4",
//        base_url+"assets/display2/KLINIK MINGGU.mp4",
//        base_url+"assets/display2/Langkah-langkah mencuci tangan yang benar menurut WHO.mp4",
        base_url+"assets/display2/Penjelasan Kanker Paru-paru, Penyebab dan Cara Pengobatannya - Herbal TV.mp4",
//        base_url+"assets/display2/Penjelasan Penyakit Tuberkulosis (TBC) dan Hal-hal Unik yang Berhubungan dengan TBC.mp4",
        base_url+"assets/display2/Penjelasan Sistem Pernapasan pada Manusia dan Organ-organnya - Herbal TV.mp4",
        base_url+"assets/display2/Penjelasan Tentang Bronkitis dan Hal-hal Unik yang Berkaitan dengan Bronkitis - Herbal TV_youtube_original.mp4",
        base_url+"assets/display2/Pertumbuhan Sel Kanker Pada Tubuh.mp4",
        base_url+"assets/display2/TELEMEDICINE.mp4",
        base_url+"assets/display2/Tanda - tanda Atau Gejala  Mata Akan Mengalami Katarak.mp4",
        base_url+"assets/display2/Tes Buta Warna,Tes Kesehatan Masuk Polisi-Polri-Instansi Pemerintah.mp4",
        base_url+"assets/display2/Tuberkulosis Resisten Obat dalam Bahasa (aksen dari Indonesia).mp4",
        base_url+"assets/display2/Videografis TOSS TBC FB - IG - Twitter - Path.mp4",
        base_url+"assets/display2/Video Slank Youtube-FB-IG-Twitter-Path.mp4",
      ];
//      console.log(myvids);
    var activeVideo = 1;
    myvid.addEventListener('ended', function(e) {
      // update the active video index
      activeVideo = (++activeVideo) % myvids.length;
      // update the video source and play
      myvid.src = myvids[activeVideo];
      myvid.play();
    });
});
function textToSpeech(pesan, nada, nilai, volume){responsiveVoice.speak( pesan,"Indonesian Female",{ pitch: nada, rate: nilai, volume: volume })}
function displayServerTime(){
        //buat object date berdasarkan waktu di client
        var clientTime = new Date();
        //buat object date dengan menghitung selisih waktu client dan server
        var time = new Date(clientTime.getTime());
        //ambil nilai jam
        var sh = time.getHours().toString();
        //ambil nilai menit
        var sm = time.getMinutes().toString();
        //ambil nilai detik
        var ss = time.getSeconds().toString();
        //tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
        var clock = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
        panggil();

    }


function panggil()
{
    //jika pemanggilan kosong maka panggil ambil data antrian berikutnya
    if(localStorage.getItem('sudahpanggil') == '')
    {
        var speech = '';
        var ids = window.location.pathname.split('/')[4];
        var listAntrian='';
        var vid = document.getElementById("play_video");
        $.ajax({ 
        url: base_url + 'cantrian/panggilantrian',
        type : 'post',      
        data: {s:ids},
        dataType : 'json',
        success: function(result) {

                if (result.panggil!='') listAntrian+='<audio id="music" loop src="'+base_url+'assets/bells.mp3" autoplay></audio><div class="container" style=" padding-left: 1.5%;"><p><h3>Daftar Panggilan</h3></p></div>';

                for(i in result.panggil)
                {
                    speech += result.panggil[i].pemanggilan;

                    listAntrian+='<div class="col-sm-11"  style="text-align: center; padding-top: 4px;">'
                        +'<div style="padding:4px " class="small-box bg-'+qlwarna(result.panggil[i].idloket)+'" >'
                        +'<div class="inner" >'
                          +'<h2><strong>'+result.panggil[i].namaloket+'</strong></h2>'
                          +'<h1 style="font-size:90px;margin:0; text-align:center;""><b>'+result.panggil[i].no+'</b></h1>'
                        +'</div>'
                        +'<div class="icon"><i class="'+((result.panggil[i].fontawesome == '')?'fa fa-bullhorn':result.panggil[i].fontawesome)+' fa-lg"></i></div>'
                      +'</div>'
                      +'</div>';
                updateAntrian(result.panggil[i].idantrian);
                }
                $('#listpanggilantrian').empty();
                $('#listpanggilantrian').html(listAntrian);
                //set pemanggilan ada
                localStorage.setItem('sudahpanggil',speech);
                if(speech!=''){ vid.volume = 0.0;}
                else{setTimeout(function(){vid.volume = 0.2;},4500);}
                
                //ubah text to voice
                setTimeout(function(){
                    textToSpeech(speech,1,1,12); 
                },1000);
                
                //set pemanggilan
                setTimeout(function(){
                    localStorage.setItem('sudahpanggil','');
                },1000);

                lastDisplayAntrian(result.antrian);//panggil fungsi display no antrian
                //set pemanggilan kosong
                
            },
            error: function(result){ 
                // textToSpeech('gagal maning. gagal maning',1,1,1);
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
///////////TAMPIL NO ANTRIAN TERAKHIR//////
function lastDisplayAntrian(x)
{   

    var listAntrian='';
    for(i in x)
        {
            listAntrian+='<div class="col-sm-2" style="text-align: center;">'
                        +'<div class="small-box bg-'+ qlwarna(x[i].idloket) +'" ><div class="inner" >'
                          +'<strong style="font-size: 30px;">'+ x[i].namaloket +'</strong>'
                          +'<h1 style="font-size:65px;margin:0; text-align:center;""><b>'+ x[i].no +'</b></h1>'
                        +'</div>'
                        +'<div class="icon"><i class="'+((x[i].fontawesome == '')?'fa fa-bullhorn':x[i].fontawesome)+'"></i>'
                        +'</div>'
                        +'</div>'
                        +'</div>';
        }
        $('#listakhirantrian').empty();
        $('#listakhirantrian').html(listAntrian);
}

///////////UPDATE ANTRIAN/////
function updateAntrian(value)
{
    $.ajax({url: base_url + 'cantrian/updateantrian',type : 'post',data: {id:value},dataType : 'json',
    success: function(result) {},
    error: function(result){
        console.log('update antrian gagal...');
        // fungsiPesanGagal();
        return false;
    }
});
}