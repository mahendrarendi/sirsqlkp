'use strict';
var tablepemeriksaan;

function setLocalStorageTgl1(date)
{
    return localStorage.setItem('tglawal',date);
}

function setLocalStorageTgl2(date)
{
    return localStorage.setItem('tglakhir',date);
}

$(function(){ 
    if (typeof(Storage) === "undefined") { pesanUndefinedLocalStorage();}
    localStorage.removeItem("idpendaftaran"); //hapus localstorage idpendaftaran
    localStorage.removeItem("modependaftaran"); //hapus localstorage modependaftaran
    localStorage.removeItem("localStorageAP"); //hapus localstorage localStorageAturanPakai
    localStorage.removeItem("localStoragePegawaiDokter");
    $('#idunit').select2();
    $('#idpegawaidokter').select2();
    get_datapoli('#idunit', ( (localStorage.getItem('idunit')) ? localStorage.getItem('idunit') : '' ) );
    dropdown_getdata($('#idpegawaidokter'),'cadmission/dropdown_listdokter', ( (localStorage.getItem('idpegawaidokter')) ? localStorage.getItem('idpegawaidokter') : '' ),'<option value="0">Pilih Dokter</option>');
    setTimeout(function(){ listPemeriksaan(); }, 100);
});
function select2_serachmultidata()
{
  var hal=0;
  $("#norm").select2({
      allowClear: true,
      ajax: { 
       url: base_url+"cpelayanan/pemeriksaanklinik_getdtpasien",
       type: "post",
       dataType: 'json',
       delay: 250,
       data: function (params) {
        ((params.term=='')?hal +=1:hal=0);
        return {
          searchTerm: params.term,
          page: hal,
        };
       },
       processResults: function (response) {
         return {
            results: response,
         };

       },
       cache: true
      }
     });
}

function listPemeriksaan(){
    $('#tglawal').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((localStorage.getItem('tglawal')==undefined)? 'now' : localStorage.getItem('tglawal') ) );
    $('#tglakhir').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate", ((localStorage.getItem('tglakhir')==undefined)? 'now' : localStorage.getItem('tglakhir') ) );
    tablepemeriksaan = $('#tablepemeriksaan').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],
        "ajax": {
            "data":{
                isfarmasi:function(){return $('input[name="isfarmasi"]:checked').val();},
                isjkn:function(){return $('input[name="isjkn"]:checked').val();},
                unit:function(){return $('#idunit').val();},
                iddokter:function(){return $('#idpegawaidokter').val();},
                tglawal:function(){return $('#tglawal').val();},
                tglakhir:function(){return $('#tglakhir').val();},
                norm:function(){return $('#norm').val();}
            },
            "url": base_url+'cpelayanan/list_pemeriksaan',
            "type": "POST"
        },"columnDefs": [{ "targets": [ -1 ],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
       $(nRow).attr('style', qlstatuswarna(aData[15]));
   }, 
   "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
       $('[data-toggle="tooltip"]').tooltip();
   },
   });
}
function refreshPemeriksaan()
{
    $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
    $('input[type="search"]').val('').keyup();
    tablepemeriksaan.state.clear();
    setLocalStorageTgl1($('#tglawal').val()); setLocalStorageTgl2($('#tglakhir').val());
    tablepemeriksaan.ajax.reload();
}
function reloadPemeriksaan()
{ 
    setLocalStorageTgl1($('#tglawal').val()); setLocalStorageTgl2($('#tglakhir').val()); tablepemeriksaan.ajax.reload(null,false);
}
function reloadPemeriksaanByUnit()
{ 
    localStorage.setItem('idunit',$('#idunit').val());return reloadPemeriksaan();
}
function reloadPemeriksaanByDokter()
{ 
    localStorage.setItem('idpegawaidokter',$('#idpegawaidokter').val());return reloadPemeriksaan();
}

function reloadPemeriksaanByNorm()
{ 
    tablepemeriksaan.ajax.reload();  $('#norm').empty();
}

// download pelayanan excel
function get_exceldata()
{
    var value = ambiltanggal($('#tglawal').val())+'|'+ambiltanggal($('#tglakhir').val());
    window.location.href=base_url+"creport/downloadexcel_page/"+value+' pelayananperiksa '+ $('#idunit').val();
}
function if_undefined(value,alt)
{
    if(value==='undefined')
    {
        return alt;
    }
    else
    {
        return value;
    }
}
/////////////////END CARI PASIEN////////////////
// mahmud, clear :: panggil pasien
function panggilpasien(id)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cantrian_1/panggilulang', //alamat controller yang dituju (di js base url otomatis)
        data: {a:id}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status, result.message + id);
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}
function pemanggilanhasil(id)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cantrian_1/pemanggilanhasil', //alamat controller yang dituju (di js base url otomatis)
        data: {i:id}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status, result.message + id);
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}
//cek apakah pasien mendapatkan diagnosa dengan golongan KNS, jika iya maka pasien tidak dapat dibuatkan skdp dari dokter hanya dapat dibuatkan surat rujuk balik
function isGolonganicdKNS(idpemeriksaan,idpendaftaran)
{
    $.ajax({
        type: "POST",
        url: base_url+'cpelayanan/cekgolonganicdkns', //alamat controller yang dituju (di js base url otomatis)
        data: {i:idpemeriksaan}, 
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            if(result!=false){
                notif('danger','Terdapat diagnosa KNS<br> Pasien tidak dapat dibuatkan SKDP dan hanya dapat dibuatkan Surat Rujuk Balik dari Dokter.!');$('.jconfirm').hide(); return false;
            }else{
                var idp = idpendaftaran;
                var nobaris = $(this).attr("nobaris");
                var modalTitle = 'SKDP <br><h4>Surat Keterangan Dalam Perawatan</h4>';
                var modalContent = '<form action="" id="FormRujuk">' +
                                        '<input type="hidden" value="'+idp+'" name="idp">'+
                                        '<div class="form-group">' +
                                            '<label>Tanggal Kontrol <i class="fa fa-question"></i></label>' +
                                            '<input type="text" id="tglskdp" class="datepicker form-control" name="tanggal" onchange="skdp_getjadwal(this.value)"/> '+
                                        '</div>'+
                                        '<div class="form-group">' +
                                            '<label>Poliklinik</label>' +
                                            '<select class="select2 form-control" style="width:100%;" id="skdp_poliklinik" name="idjadwal">'+
                                            '<option value="0">Pilih</option>'+
                                            '</select>' +
                                        '</div>' +
                                        '<div class="form-group">' +
                                            '<label>Petugas</label>' +
                                            '<select class="select2 form-control" style="width:100%;" id="skdp_petugas" name="idpetugas">'+
                                            // '<option value="0">Pilih</option>'+
                                            '</select>' +
                                        '</div>' +
                                        '<div class="form-group">' +
                                            '<label>Pertimbangan Dokter</label>' +
                                            '<textarea name="pertimbangan" class="form-control" rows="3"></textarea>' +
                                        '</div>' +
                                        '<div class="form-group">' +
                                            '<label><input type="checkbox" name="daftar" /> Include Pendaftaran</label>' +
                                        '</div>' +
                                    '</form>';
                $.confirm({ //aksi ketika di klik menu
                    title: modalTitle,
                    content: modalContent,
                    columnClass: 'medium',
                    buttons: {
                        formSubmit: {
                            text: 'Cetak',
                            btnClass: 'btn-blue',
                            action: function () {
                                if($('input[name="tanggal"]').val()=='')
                                {
                                        alert_empty('tanggal');
                                        return false;
                                }
                                else if($('select[name="idjadwal"]').val()==='0')
                                {
                                        alert_empty('poliklinik');
                                        return false;
                                }
                                else //selain itu
                                {
                                    $.ajax({
                                        type: "POST", //tipe pengiriman data
                                        url: 'pemeriksaanklinik_buatskdp', //alamat controller yang dituju (di js base url otomatis)
                                        data: $("#FormRujuk").serialize(), //
                                        dataType: "JSON", //tipe data yang dikirim
                                        success: function(result) { //jika  berhasil
                                            // console.log(result);
                                            if(result.status=='danger')
                                            {
                                                $.alert(result.message);
                                            }
                                            else
                                            {
                                                // panggil fungsi cetak skdp
                                                notif(result.status, result.message);
                                                cetak_skdp(result.diagnosa,result.skdp,result.pasien);
                                                reloadPemeriksaan(); //reload page   
                                            }
                                        },
                                        error: function(result) { //jika error
                                            fungsiPesanGagal(); // console.log(result.responseText);
                                            return false;
                                        }
                                    });
                                }
                            }
                        },
                        formReset:{ //menu back
                            text: 'batal',
                            btnClass: 'btn-danger'
                        }
                    },
                    onContentReady: function () {
                    stopLoading();
                    // setTimeout(function(){},500); //ketika form di tampilkan
                    // var setdate = new Date();
                    //     setdate.setDate(90);
                    $('#tglskdp').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate","now");// bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                    }
                });
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal();
        }
    });
}
// cari petugas
function cariskdp_petugas()
{   
    $("select[name='idpetugas']").select2({
    placeholder: "NIK | Nama",
    allowClear: true,
      ajax: {
        url: base_url+"cmasterdata/caridatapegawai",type: "post",dataType: 'json',delay: 200,
        data: function (params) {return {searchTerm: params.term};},
        processResults: function (response) {return {results: response};},
        cache: true
      }
     });
}
// buat skdp
$(document).on("click","#pemeriksaanklinikBuatSKDP", function(){ //buat skdp
    startLoading();
    isGolonganicdKNS($(this).attr("alt3"),$(this).attr("alt2"));
});
// fungsi cetak skdp
function cetak_skdp(diagnosa,skdp,pasien)
{
    var dtdiagnosa='',dttindakan='';
    for(var x in diagnosa)
    {
       dtdiagnosa += ((diagnosa[x].idjenisicd==2)? diagnosa[x].icd+' '+diagnosa[x].namaicd+'<br>' : '') ;
       dttindakan += ((diagnosa[x].idjenisicd!=2)? diagnosa[x].icd+' '+diagnosa[x].namaicd+'<br>' : '') ;
    }
    var dtcetak = '<img src="'+base_url+'/assets/images/headerlembarrm.svg"><img src="'+base_url+'/assets/images/garishitam.svg">';
    dtcetak += '<table style="margin-top:8px;margin-bottom:8px;"><tr ><td width="35%"></td><td><h4 style="margin:0;">SURAT KETERANGAN DALAM KEPERAWATAN</h4></td></tr></table>'+
    '<table>'+
    '<tr><td><b>SKDP </b></td><td>:</td><td> <b>'+skdp.noskdpql+'</b> </td></tr>'+
    '<tr><td><b>No. KONTROL </b></td><td>:</td><td> <b>'+skdp.nokontrol+'</b></td></tr>'+
    '<tr><td>Nama Pasien</td><td>:</td><td> '+pasien.namalengkap+' </td></tr>'+
    '<tr><td>Jenis Kelamin</td><td>:</td><td> '+pasien.jeniskelamin+' </td></tr>'+
    '<tr><td>No RM</td><td>:</td><td> '+if_empty(pasien.norm)+' </td></tr>'+
    '<tr><td>Nomor Kartu BPJS</td><td>:</td><td> '+if_empty(pasien.nojkn)+' </td></tr>'+
    '<tr><td>Alamat</td><td>:</td><td> '+if_empty(pasien.alamat)+' </td></tr>'+
    '<tr style="margin-top:2px; margin-bottom:3px;"><td>Diagnosa</td><td>:</td><td><div>'+dtdiagnosa+'</div></td></tr>'+
    '<tr><td>Terapi</td><td>:</td><td><div>'+dttindakan+'</div></td></tr>'+
    '<tr><td>Kontrol Kembali Tanggal</td><td>:</td><td>'+skdp.waktuperiksa+'</td></tr>'+
    '<tr><td>Tanggal Surat Rujukan</td><td>:</td><td></td></tr>'+
    '</table><table>'+
    '<tr><td>Pasien tersebut diatas masih dalam perawatan <b>Dokter Spesialis</b> di RSU Queen Latifa dengan pertimbangan<br> sebagai berikut : <b>'+skdp.pertimbangan+'</b></td></tr>'+
    '<tr><td>Surat keterangan ini digunakan untuk 1 (satu) kali kunjungan pasien<br> dengan diagnosa sebagai mana diatas pada tanggal <b>'+skdp.waktuperiksa+'</b><td></tr>'+
    '</table><table style="text-align:center;width:100%;" >'+
    '<tr><td style="padding-left:50%;">Yogyakarta, '+pasien.waktuperiksa+'</td></tr>'+
    '<tr><td style="padding-left:50%;">Dokter<br><br><br><br></td></tr>'+
    '<tr><td style="padding-left:50%;"><b>'+pasien.namadokter+'</b></td></tr>'+
    '</table>';
    fungsi_cetaktopdf(dtcetak,'<style>@page {size:A5; margin:0.2cm;}</style>'); // -- panggil fungsi cetak
}
// aksi cetak skdp
function pemeriksaanklinik_cetakskdp(id)
{
    $.ajax({ 
        url: 'pemeriksaanklinik_cetakskdp',
        type : "post",      
        dataType : "json",
        data : {id:id},
        success: function(result) {
            // panggil fungsi cetak skdp
            cetak_skdp(result.diagnosa,result.skdp,result.pasien);
            reloadPemeriksaan();//reload page
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    });
}

//--ambil jadwal skdp
function skdp_getjadwal(value){tampilDataPoliklinik($('select[name="idjadwal"]'),tglubah_format1(value));/*panggil tampil poli di sirstql.js line 192*/ setTimeout(function(){cariskdp_petugas();},500);}
$(document).on("click","#pemeriksaanklinikSelesai", function(){ //ubah status selesai
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanklinikUbahStatus('selesai','Selesai Pemeriksaan?',id,nobaris);
});
$(document).on("click","#pemeriksaanklinikBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanklinikUbahStatus('batal','Batal Pemeriksaan?',id,nobaris);
});
$(document).on("click","#pemeriksaanklinikTunda", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanklinikUbahStatus('tunda','Tunda Pemeriksaan?',id,nobaris);
});
$(document).on("click","#pemeriksaanklinikBatalselesai", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanklinikUbahStatus('sedang ditangani','Batal Selesa?',id,nobaris);
});

$(document).on("click","#pemeriksaanklinikUpdatePeriksa",function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        localStorage.removeItem("idp");
        
        localStorage.setItem("idperiksa",$(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idp", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_update_periksa';
    } else {
         pesanUndefinedLocalStorage();
    }
});

$(document).on("click","#pemeriksaanklinikUpdateEkokardiografi",function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        localStorage.removeItem("idp");
        
        localStorage.setItem("idperiksa",$(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idp", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_update_ekokardiografi';
    } else {
         pesanUndefinedLocalStorage();
    }
});

function fungsi_pemeriksaanklinikUbahStatus(status,content,id,nobaris) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: content,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'pemeriksaanklinik_ubahstatus',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                        notif(result.status, result.message);
                        reloadPemeriksaan();
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

function rujuktampilpoli(date)
{
    tampilDataPoliklinik($('select[name="idjadwal"]'),ambiltanggal(date));
}

$(document).on("click","#pemeriksaanklinikRujuk", function(){ 
    var id = $(this).attr("alt");
    var tanggalJadwal = $(this).attr("alt2");
    var idperson = $(this).attr("idp");
    var idpendaftaran = $(this).attr("idpendaftaran");
    var norm = $(this).attr("norm");
    var modalTitle = 'Rujuk Pemeriksaan Pasien';
    var modalContent = '<form action="" id="FormRujuk">' +
                            '<input type="hidden" value="'+id+'" name="idperiksa">'+
                            '<input type="hidden" value="'+idperson+'" name="idperson">'+
                            '<input type="hidden" value="'+idpendaftaran+'" name="idpendaftaran">'+
                            '<input type="hidden" value="'+norm+'" name="norm">'+
                            '<div class="form-group col-md-5">' +
                                '<label>Tanggal Periksa</label>' +
                                '<input type="text" class="datepicker form-control" onchange="rujuktampilpoli(this.value)" name="tanggalperiksa" />'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group col-md-7">' +
                                '<label>Poliklinik</label>' +
                                '<select class="form-control select_jadwalralan" style="width:100%;" name="idjadwal">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group col-md-5">' +
                                '<label>Status</label>' +
                                '<select class="form-control" style="width:100%;" name="status">'+
                                '<option value="0">Pilih</option>'+
                                '<option value="rujuk internal kembali">Rujuk Internal Kembali</option>'+
                                '<option value="rujuk internal tidak kembali">Rujuk Internal Tidak Kembali</option>'+
                                '<option value="rujuk eksternal">Rujuk Internal Eksternal</option>'+
                                '</select>' +
                            '</div>'+
                            
                            '<div class="form-group col-md-7">'+
                                '<label>Pemeriksaan Terjadwal</label>' +
                                '<div class="form-control" style="border:0px;">'+
                                '<label><input type="radio" name="jenis" value="0" checked/> Tidak &nbsp;&nbsp;&nbsp;</label>' +
                                '<label><input type="radio" name="jenis" value="1"/> Ya</label>' +
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group col-md-5">' +
                                '<label>Laboratorium</label>' +
                                '<select class="form-control" style="width:100%;" id="laboratorium">'+
                                    '<option value="">Pilih Laboratorium</option>'+
                                '</select>' +                                
                            '</div>'+
                            
                            '<div class="form-group col-md-7">' +
                                '<label>Paket Laboratorium</label>' +
                                '<select class="form-control" style="width:100%;" id="paketlaboratorium">'+
                                    '<option value="">Pilih Paket Laboratorium</option>'+
                                '</select>' +
                            '</div>'+
                            
                            '<div class="forom-group col-md-12" style="margin-top:-12px;">'+
                                '<table class="table table-striped non-margin" style="margin-bottom:10px;"><tbody id="list_laboratorium"></tbody></table>'+
                            '</div>'+
                            
                            '<div class="form-group col-md-12">' +
                                '<label>Elektromedik</label>' +
                                '<select class="form-control" style="width:100%;" id="elektromedik">'+
                                    '<option value="">Pilih Elektromedik</option>'+
                                '</select>' +
                            '</div>'+
                            
                            '<div class="forom-group col-md-12" style="margin-top:-12px;">'+
                                '<table class="table table-striped non-margin" style="margin-bottom:8px;"><tbody id="list_elektromedik"></tbody></table>'+
                            '</div>'+
                            
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'l',
        buttons: {
            formSubmit: {
                text: 'Rujuk',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="poliklinik"]').val()==='0')
                    {
                            alert_empty('poliklinik');
                            return false;
                    }
                    else if($('select[name="status"]').val()==='0')
                    {
                            alert_empty('status');
                            return false;
                    }
                    else //selain itu
                    {
                        startLoading();
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: 'pemeriksaanklinik_saverujuk', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormRujuk").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                stopLoading();
                                if(result.status =='success')
                                {
                                    notif(result.status, result.message);
                                    qlReloadPage(600); //reload page
                                }
                                else
                                {
                                    $.alert(result.message);
                                }
                                
                            },
                            error: function(result) { //jika error
                                stopLoading();
                                fungsiPesanGagal(); // console.log(result.responseText);
                                return false;
                            }
                        });
                    }
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now');
        tampilDataPoliklinik($('select[name="idjadwal"]'),tanggalJadwal);
        diagnosa_or_tindakan($('#laboratorium'),4);
        diagnosa_or_tindakan($('#elektromedik'),5);
        select2serachmulti($('#paketlaboratorium'),'cmasterdata/fill_paket_laboratorium_ralan')
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});

$(document).on('change','#laboratorium',function(){   
   var id = new Date().getUTCMilliseconds();
   var icdlaborat = this.value;
   var dt = '<tr id="'+id+'"><td><input type="hidden" name="icdlaborat[]" value="'+icdlaborat+'" >&nbsp;'+$('#laboratorium option:selected').text()+'</td><td><a id="hapuslaborat" valid="'+id+'" '+tooltip('hapus')+' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
   $('#list_laboratorium').append(dt);
   $('#laboratorium').html('<option value="">Pilih Laboratorium</option>');
   $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('change','#paketlaboratorium',function(){
   var id = new Date().getUTCMilliseconds();
   var icdlaborat = this.value;
   var dt = '<tr id="'+id+'"><td><input type="hidden" name="paketlaborat[]" value="'+icdlaborat+'" >&nbsp;'+$('#paketlaboratorium option:selected').text()+'</td><td><a id="hapuslaborat" valid="'+id+'" '+tooltip('hapus')+' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
   $('#list_laboratorium').append(dt);
   $('#paketlaboratorium').html('<option value="">Pilih Paket Laboratorium</option>');
   $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','#hapuslaborat',function(){
   var valid = $(this).attr('valid');
   $('#'+valid).remove();
});

$(document).on('change','#elektromedik',function(){   
   var id = new Date().getUTCMilliseconds();
   var elektromedik = this.value;
   var dt = '<tr id="'+id+'"><td><input type="hidden" name="icdelektromedik[]" value="'+elektromedik+'" >&nbsp;'+$('#elektromedik option:selected').text()+'</td><td><a id="hapuselektromedik" valid="'+id+'" '+tooltip('hapus')+' class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></td></tr>';
   $('#list_elektromedik').append(dt);
   $('#elektromedik').html('<option value="">Pilih Elektromedik</option>');
   $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','#hapuselektromedik',function(){
   var valid = $(this).attr('valid');
   $('#'+valid).remove();
});

function diagnosa_or_tindakan(x,y,z='')
{
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cpelayanan/pemeriksaanklinik_diagnosa",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                jenisicd: y,
                idicd:z,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
            };
        },              
    }
    });
}

$(document).on("click","#pemeriksaanklinikPeriksa", function(){ //pemeriksaan klinik
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        localStorage.removeItem("idp");
        
        localStorage.setItem("idperiksa",$(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idp", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_periksa';
    } else {
         pesanUndefinedLocalStorage();
    }
});
$(document).on("click","#pemeriksaanklinikVerifikasi", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        localStorage.removeItem("idp");
        
        localStorage.setItem("idperiksa",$(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idp", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_verifikasi';
    } else {
         pesanUndefinedLocalStorage();
    }
});
$(document).on("click","#pemeriksaanklinikPeriksaDokter", function(){ //pemeriksaan klinik
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        
        localStorage.removeItem("idp");
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        var id = $(this).attr("alt");
        var norm = $(this).attr("norm");
        var idpendaftaran = $(this).attr("alt2");
        localStorage.setItem("idperiksa",id);
        localStorage.setItem("norm", norm);
        localStorage.setItem("idp", idpendaftaran);
        $.ajax({
            type: "POST", //tipe pengiriman data
            url: base_url+'cpelayanan/pemeriksaan_uwaktuperiksadokter', //alamat controller yang dituju (di js base url otomatis)
            data: {id:id}, //
            dataType: "JSON", //tipe data yang dikirim
            success: function(result) { //jika  berhasil
                window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_periksa';
            },
            error: function(result) { //jika error
                 fungsiPesanGagal();
                 return false;
            }
        });
        
    } else {
         pesanUndefinedLocalStorage();
    }
});


$(document).on("click","#pemeriksaanklinikView", function(){ //view pemeriksaan
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idperiksa");
        localStorage.removeItem("norm");
        var id = $(this).attr("alt");
        var norm = $(this).attr("norm");
        var idpendaftaran = $(this).attr("alt2");
        localStorage.setItem("idperiksa", id);
        localStorage.setItem("norm", norm);
        localStorage.setItem("idp", idpendaftaran);
        window.location.href=base_url + 'cpelayanan/pemeriksaanklinik_view';
    } else {
         pesanUndefinedLocalStorage();
    }
});