var norm = localStorage.getItem('norm');
//siapkan norm -> ambil dari localstorage norm
var idpendaftaran = localStorage.getItem('idpendaftaranranap');
var modepemeriksaanranap   = localStorage.getItem('modepemeriksaanranap');

$(function(){
    
    localStorage.removeItem('dataDokter');
    $('input[name="idinap"]').val(localStorage.getItem('idinap'));
    $('#riwayatdtpasien').attr('norm',norm);
    $('.select2').select2();
    pemeriksaanranapDetailPasien();//panggil fungsi (cari data pasien)
    $('input[name="idrencanamedispemeriksaan"]').val(localStorage.getItem('idrencanamedispemeriksaan'));
    pemeriksaanRefreshVitalsign();//list vital sign
    pemeriksaanRefreshRadiologi(); //list radiologi
    tampilHasilRadiografi(idpendaftaran,localStorage.getItem('idrencanamedispemeriksaan'));
    diagnosa_or_tindakan($('select[name="carivitalsign"]'),1);
    diagnosa_or_tindakan($('select[name="caridiagnosa"]'),2);
    diagnosa_or_tindakan($('select[name="caritindakan"]'),3);
    diagnosa_or_tindakan($('select[name="carilaboratorium"]'),4);
    diagnosa_or_tindakan($('select[name="cariradiologi"]'),5);
    pemeriksaanranapRefreshBhp();//list bhp
    pemeriksaanRefreshTindakan(); //list tindakan
    pemeriksaanRefreshDiagnosa(); //list tindakan
    pemeriksaanRefreshLaboratorium();//list laboratorium
    load_soap();
    $('#modepemeriksaan').val(modepemeriksaanranap);   
    select2serachmulti($('select[name="caribhp"]'),'cmasterdata/fillbarang');
});

//cetak hasil laboratorium per paket
function cetak_hasillab(idpaketpemeriksaan=null)
{
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var mode='cetakpaket';
    print_laboratorium(idrencanamedispemeriksaan,mode,idpaketpemeriksaan);
}
//cetak hasil laboratorium non paket
$(document).on('click','#cetakhasillaboratnonpaket',function(){
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var mode='cetaknonpaket';
    print_laboratorium(idrencanamedispemeriksaan,mode,null);
});

function waktupengerjaanlabranap()
{
    $.alert();
}

// mahmud, clear
// pemeriksaan_settrencanainputbhp
function rencanaInputBhp(value)
{
    if(value!='0')
    {
        $.ajax({
            type:"POST",
            url:"pemeriksaanranap_ubahstatusrencanabhp",
            data:{
                x:value, 
                i:localStorage.getItem('idrencanamedispemeriksaan'),
                idp:localStorage.getItem('idpendaftaranranap'),
            },
            dataType:"JSON",
            success:function(result){
                $('select[name="caribhp"]').html('<option value="">Pilih</option>');
                notif(result.status, result.message);
                if(result.status=='success'){pemeriksaanranapRefreshBhp();}
            },
            error:function(result){fungsiPesanGagal();return false;}
        });
    }
}
// mahmud, clear
function pemeriksaanranapRefreshBhp()//refreshDataBHP
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listrencanabhp',
        data: {i:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            var html = '', i=0, no=0, subtotal=0, total=0;jumlahambil=0;
            for (i in result)
            {
                var idrencana = result[i].idrencanamedisbarangpemeriksaan;
                html += '<tr id="listrencanabhp'+ ++no +'">\n\
                <td>'+ no +'</td>\n\
                <td>'+result[i].namabarang+'</td>\n\
                <td><input '+ ((result[i].statuspelaksanaan == 'terlaksana') ? 'disabled' : '' ) +' onchange="update_jumlahrencanabhp('+no+','+idrencana+',this.value)" type="text" id="rencanabhp_jumlah'+no+'" class="text-center" value="'+ result[i].jumlah+'" size="5" /></td>\n\
                <td><select style="width:90px;" class="carisatuan_ob" idr="'+result[i].idrencanamedisbarangpemeriksaan+'"><option value="'+result[i].idsatuanpemakaian+'">'+result[i].namasatuan+'</option></select></td>\n\
                <td>'+result[i].harga+'</td>\n\
                <td>'+result[i].total+'</td>\n\
                <td>'+result[i].statuspelaksanaan+'</td>';
                // add menu aksi
                html +='<td>';

                if (result[i].statuspelaksanaan == 'terlaksana' || result[i].statuspelaksanaan == 'batal')
                {
                    html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'rencana\', \'bhp\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                }
                else
                {
                    html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'terlaksana\', \'bhp\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>';
                    html +=' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'batal\', \'bhp\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                }
                html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusRencanaBhp('+idrencana+', \'bhp\')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                html += '<input type="hidden" id="idrencanabhp'+no+'" value="'+result[i].idrencanamedisbarang+'"/>';

                ((result[i].jumlahrencana)>0 ? html += ' <a id="tombolbatalrencanabhp'+no+'" onclick="batalRencana('+no+',\'rencanabhp\',\'Rencana Bhp\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Pemakaian (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )
                html+='</td></tr>';
            }
            $('#viewDataBhp').empty();
            $('#viewDataBhp').html(html);
            
            select2serachmulti($('.carisatuan_ob'),'cmasterdata/fillsatuan');
            $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
        },
        error: function(result) {fungsiPesanGagal();return false;}
    });
}


// mahmud, clear
function batalPlotBhp(no)
{
    $('#tombolbatal'+no+'').remove();
    $('.tooltip').hide();
}
// mahmud, clear
function update_jumlahrencanabhp(x,y,jumlah)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatejumlahrencanabhp",
        data:{y:y, z:jumlah},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function ubahStatusRencanaBhp(barang, status, asal)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_ubahstatusrencanabhp",
        data:{b:barang, s:status},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if (asal=='bhp')
                pemeriksaanranapRefreshBhp();
            else
                pemeriksaanRefreshTindakan();
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusRencanaBhp(value, asal)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus rencana Bhp/Obat?',
        buttons: {
            confirm: function () {//konfirm  
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusrencanabhp',
                    data: {x:value},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if (asal=='bhp')
                            pemeriksaanranapRefreshBhp();
                        else
                            pemeriksaanRefreshTindakan();
                    },
                    error: function(result){
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
            },
            cancel: function () {//cancel            
            }            
        }
    });
}
function editBhpGrup(value)//seting atau ubah grup bhp
{
    var d = $('#grup'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'grup',x,y,value);
}
function editBhpJumlah(value)//seting atau ubah grup bhp
{
    var d = $('#jumlah'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#harga'+value).val();
    settingBhp(d,'jumlah',x,y,value);
}
function editBhpKekuatan(value)//seting atau ubah grup bhp
{
    var d = $('#kekuatan'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#harga'+value).val();
    settingBhp(d,'kekuatan',x,y,value);
}
function editBhpSetHarga(value)//seting atau ubah grup bhp
{
    var d = $('#harga'+value).val();
    var x = $('#idbarangpemeriksaan'+value).val();
    var y = $('#idbarang'+value).val();
    settingBhp(d,'harga',x,y,value);
}
function settingBhp(d,field,x,y,no)
{
    $.ajax({ 
        url: 'pemeriksaan_settbhp',
        type : "post",      
        dataType : "json",
        data : { d:d, type:field, x:x,y:y },
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                pemeriksaanranapRefreshBhp();
                setTimeout(function(){
                    // if(field=='grup'){$('#jumlah'+no).focus();}
                    // if(field=='jumlah'){$('#kekuatan'+no).focus();}
                    if(field=='jumlah'){$('#kekuatan'+no).focus();}
                    else{$('#jumlah'+(no+1)).focus();}
                },300);
                
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

// mahmud, clear
//////////////////////////Vital Sign////////////////
function pilihvitalsign(value,ispaket='')
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilvitalsign",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran,ispaket:ispaket, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshVitalsign();} //refresh vitalsign
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function pemeriksaanRefreshVitalsign()//refreshDataVitalsign
{
    var x=0, html='', paket='', no=0, readonly='';
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listvitalsign',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                no++;
                // start menu paket
                menu='';
                if (result[x].statuspelaksanaan == 'terlaksana' || result[x].statuspelaksanaan == 'batal')
                {
                    menu += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Vital Sign\','+result[x].idpaketpemeriksaan+', \'rencana\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                }
                else
                {
                    menu += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Vital Sign\','+result[x].idpaketpemeriksaan+', \'terlaksana\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                            ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Vital Sign\','+result[x].idpaketpemeriksaan+', \'batal\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                }
                menu += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusPakettindakan(\'Vital Sign\','+result[x].idpaketpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                // end menu paket
                html += ((namapaket!=result[x].namapaketpemeriksaan && result[x].idpaketpemeriksaan!=null)? '<tr height="30px"><td class="bg-warning" colspan="5">'+result[x].namapaketpemeriksaan+' </td><td class="bg-warning">'+result[x].statuspelaksanaan+'</td><td class="bg-warning">'+menu+'<input type="hidden" id="idrencanapaketvitalsign'+no+'" value="'+result[x].idpaketpemeriksaan+'"/>'+((result[x].jumlahrencana)>0 ? ' <a id="tombolbatalrencanapaketvitalsign'+no+'" onclick="batalRencana('+no+',\'rencanapaketvitalsign\',\'Rencana Paket Laboratorium\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Vital Sign (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : '' ) +'</td><tr>' : ''); 
                html +='<tr id="listrencanavitalsign'+no+'"><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                '<input type="hidden" id="idjenisnilaivitalsign'+no+'" value="'+result[x].istext+'"/>'+
                '<td><input class="qlform-control" onchange="simpanHasil_icdPeriksa(\'Vital Sign\','+no+',\'nilaivitalsign\','+result[x].idrencanamedishasilpemeriksaan+')" type="text" value="'+((result[x].istext=='text')? result[x].nilaitext : result[x].nilai )+'"  placeholder=" Input '+result[x].istext+'" id="nilaivitalsign'+no+'" '+ql_readonlytxt(result[x].statuspelaksanaan,'terlaksana')+' size="25" /></td>'+ 
                '<td>'+ result[x].nilaidefault +'</td>'+
                '<td>'+ result[x].nilairujukan +'</td>'+
                '<td>'+ result[x].satuan +'</td>'+
                '<td>'+ result[x].statuspelaksanaan+'</td>';

                // menu per vital sign
                if(result[x].namapaketpemeriksaan==null)
                {
                    html +='<td>';
                    if (result[x].statuspelaksanaan == 'terlaksana' || result[x].statuspelaksanaan == 'batal')
                    {
                        html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusVitalsign('+result[x].idrencanamedishasilpemeriksaan+', \'rencana\', \''+result[x].icd+'\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                    }
                    else
                    {
                        html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusVitalsign('+result[x].idrencanamedishasilpemeriksaan+', \'terlaksana\', \''+result[x].icd+'\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                                ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusVitalsign('+result[x].idrencanamedishasilpemeriksaan+', \'batal\', \''+result[x].icd+'\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                    }
                    html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusVitalsign('+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" id="idrencanavitalsign'+no+'" value="'+result[x].icd+'"/>';
                    ((result[x].jumlahrencana)>0 ? html += ' <a id="tombolbatalrencanavitalsign'+no+'" onclick="batalRencana('+no+',\'rencanavitalsign\',\'Rencana Vital Sign\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Vital Sign (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )
                    html+='</td>';
                }
                var namapaket = result[x].namapaketpemeriksaan;
                html +='</tr>';
            }
            $('#listVitalsign').empty();
            $('#listVitalsign').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); return false;
        }
    });
}
// mahmud, clear
function ubahStatusVitalsign(v1, v2, v3)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilvitalsign",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshVitalsign();} //refresh vitalsign
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusVitalsign(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan vitalSign?',
        buttons: {
            confirm: function () {//konfirm  
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapusvitalsign',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshVitalsign();}
                    },
                    error: function(result){
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
            },
            cancel: function () {//cancel            
            }            
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////TINDAKAN////////////////
//basit, clear
function ubahStatusTindakan(v1, v2, v3)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpemeriksaan",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(); //refresh diagnosa
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//basit, clear
function editDokter(value)
{
    startLoading();
    $.ajax({ 
        url: 'pemeriksaanranap_ubahdokterhasilpemeriksaan',
        type : "post",      
        dataType : "json",
        data : { i:$('#idrencanamedishasilpemeriksaan'+value).val(), id:$('#iddokter'+value).val() },
        success: function(result) {
            stopLoading();
            if(result.status=='success'){
                notif(result.status, result.message);                
            }else{
                notif(result.status, result.message);
                return false;
            }              
        },
        error: function(result){ 
            stopLoading();
            notif(result.status, result.message);
            return false;
        }
        
    }); 
}

//---mahmud, clear
function pemeriksaanRefreshDiagnosa()//refreshDataDiagnosa
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listdiagnosa',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            var html='';
            for(var x in result)
            {
                html += '<tr>\n\
                        <td>'+result[x].icd+'</td>\n\
                        <td>'+result[x].namaicd+'</td>\n\
                        <td>'+result[x].aliasicd+'</td>\n\
                        <td><a id="delete_diagnosa" icd="'+result[x].icd+'" idrencanamedishasilpemeriksaan="'+result[x].idrencanamedishasilpemeriksaan+'" class="btn btn-xs btn-danger" '+tooltip('Hapus Diagnosa')+' ><i class="fa fa-trash"></i></a></td>\n\
                        </tr>';
            }
            $('#listDiagnosa').empty();
            $('#listDiagnosa').html(html);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}
//mahmud, clear
$(document).on('click','#delete_diagnosa',function(){
    var icd = $(this).attr('icd');
    var id  = $(this).attr('idrencanamedishasilpemeriksaan');
    inputdelete_diagnosa(icd,'hapus',id);
});

//mahmud, clear
function inputdelete_diagnosa(icd,mode,id='')
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"inputdiagnosa_rencana_medis_hasilpemeriksaan",
        data:{
            a:icd,
            b:idpendaftaran,
            c:localStorage.getItem('idrencanamedispemeriksaan'),
            d:mode,
            e:id
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshDiagnosa();
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//basit, clear
function pilihtindakan(value)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpemeriksaan",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshTindakan(); //refresh diagnosa
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}



//---basit, clear
function pemeriksaanRefreshTindakan()//refreshDataDiagnosa
{
    var x=0,no=0, nobhp=0, html='', subtotal=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listtindakan',
        data: {
            x:localStorage.getItem('idrencanamedispemeriksaan')
        },
        dataType: "JSON",
        success: function(result) {
            var idrencanamedishasilpemeriksaansebelum = 0;
            for(x in result.tindakan)
            {
                //isi tindakan
                if (result.tindakan[x].idrencanamedishasilpemeriksaan != idrencanamedishasilpemeriksaansebelum)
                {
                    ++no;
                    subtotal += parseInt(result.tindakan[x].total);
                    html += '<tr id="listrencanatindakan'+no+'">'+
                            '<td><input type="hidden" id="idrencanamedishasilpemeriksaan'+no+'" size="5" class="form-control"  value="'+result.tindakan[x].idrencanamedishasilpemeriksaan+'">'+ result.tindakan[x].icd +'</td>'+
                            '<td>'+ result.tindakan[x].namaicd +' <a class="btn btn-primary btn-xs" data-toggle="tooltip" onclick="tambahBarangKeTindakan('+result.tindakan[x].idrencanamedispemeriksaan+','+result.tindakan[x].idrencanamedishasilpemeriksaan+')" data-original-title="Tambah Rencana BHP/Obat"><i class="fa fa-medkit"></i></a></td>'+
                            '<td><select class="caridoktertinap form-control" style="width:100%" onchange="editDokter('+no+')" id="iddokter'+no+'" name="iddokter'+no+'"><option value="'+result.tindakan[x].idpegawaidokter+'">'+result.tindakan[x].namadokter+'</option></select></td>'+
                            '<td class="text-center"><input id="idtindakan'+ no +'" type="hidden" value="'+result.tindakan[x].idrencanamedishasilpemeriksaan+'"><input type="text" size="1" class="text-center" id="setjmltindakan'+no+'" value="'+ result.tindakan[x].jumlah +'" onchange="set_jmltindakan('+no+')"/> </td>'+
                            '<td>'+ convertToRupiah(result.tindakan[x].total) +'</td>'+
                            '<td>'+ result.tindakan[x].statuspelaksanaan +'</td>'+
                    '<td>';
                    if (result.tindakan[x].statuspelaksanaan == 'terlaksana' || result.tindakan[x].statuspelaksanaan == 'batal')
                    {
                        html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusTindakan('+result.tindakan[x].idrencanamedishasilpemeriksaan+', \'rencana\', \''+result.tindakan[x].icd+'\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                    }
                    else
                    {
                        html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusTindakan('+result.tindakan[x].idrencanamedishasilpemeriksaan+', \'terlaksana\', \''+result.tindakan[x].icd+'\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                                ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusTindakan('+result.tindakan[x].idrencanamedishasilpemeriksaan+', \'batal\', \''+result.tindakan[x].icd+'\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                    }
                    html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapustindakan('+result.tindakan[x].idrencanamedishasilpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" id="idrencanatindakan'+no+'" value="'+result.tindakan[x].icd+'"/>';
                    ((result.tindakan[x].jumlahrencana)>0 ? html += ' <a id="tombolbatalrencanatindakan'+no+'" onclick="batalRencana('+no+',\'rencanatindakan\',\'Rencana Tindakan\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Tindakan (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )
                    html+='</td></tr>';
//                    isicombodokter('iddokter'+no, result.tindakan[x].idpegawaidokter);
                }
                
                //isi obat untuk tindakan (jika ada)
                if (!is_empty(result.tindakan[x].idrencanamedisbarangpemeriksaan))
                {
                    var idrencana = result.tindakan[x].idrencanamedisbarangpemeriksaan;
                    html += '<tr id="listrencanabhp'+ ++nobhp +'" class="bg bg-warning">\n\
                            <td align="right">|-----------&nbsp;</td>\n\
                            <td>'+result.tindakan[x].namabarang+'</td>\n\
                            <td>&nbsp;</td>\n\
                            <td><input onchange="update_jumlahrencanabhp('+nobhp+','+idrencana+',this.value)" type="text" id="rencanabhp_jumlah'+nobhp+'" class="text-center" value="'+ result.tindakan[x].jumlahbhp+'" size="2" /> &nbsp;<select style="width:90px;" class="carisatuan_ob" idr="'+result.tindakan[x].idrencanamedisbarangpemeriksaan+'"><option value="'+result.tindakan[x].idsatuanpemakaian+'">'+result.tindakan[x].namasatuan+'</option></select></td>\n\
                            <td>'+convertToRupiah(result.tindakan[x].totalbhp,'.')+'</td>\n\
                            <td>'+result.tindakan[x].statuspelaksanaanbhp+'</td>';
                    // add menu aksi
                    html +='<td>';

                    if (result.tindakan[x].statuspelaksanaanbhp == 'terlaksana' || result.tindakan[x].statuspelaksanaanbhp == 'batal')
                    {
                        html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'rencana\', \'tindakan\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                    }
                    else
                    {
                        
                        html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'terlaksana\', \'tindakan\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>';
                        html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusRencanaBhp('+idrencana+', \'batal\', \'tindakan\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                    }
                    html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusRencanaBhp('+idrencana+', \'tindakan\')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                    html += '<input type="hidden" id="idrencanabhptindakan'+nobhp+'" value="'+result.tindakan[x].idrencanamedisbarang+'"/>';
                    
                    ((result.tindakan[x].jumlahrencanabhp)>0 ? html += ' <a id="tombolbatalrencanabhp'+nobhp+'" onclick="batalRencana('+nobhp+',\'rencanabhptindakan\',\'Rencana Bhp\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Pemakaian (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )
                    html+='</td></tr>';
                    subtotal += parseInt(result.tindakan[x].totalbhp);
                }
                idrencanamedishasilpemeriksaansebelum = result.tindakan[x].idrencanamedishasilpemeriksaan;
            }
            html+='<tr class="bg bg-info"><td colspan="4">Subtotal</td><td colspan="3">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listTindakan').empty();
            $('#listTindakan').html(html);
            $('[data-toggle="tooltip"]').tooltip();            
            select2serachmulti($('.carisatuan_ob'),'cmasterdata/fillsatuan');            
            select2serachmulti($('.caridoktertinap'),'cmasterdata/fillcaridokter');
            
            $('.sel2').select2();
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
    
}

$(document).on('change','.carisatuan_ob',function(){
    var idr = $(this).attr('idr');
    var ids = this.value;
    $.ajax({ 
        url: base_url+'cpelayananranap/rs_inap_rencana_medis_barangpemeriksaan_updatesatuan',
        type : "post",      
        data:{ids:ids,idr:idr},
        dataType : "json",
        success: function(result) {
            if(result.status == 'danger')
            {
                $.alert(result.message);
            }
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
    });
});



//basit, progress
function isicombodokter(id, selected)
{
    if (is_empty(localStorage.getItem('dataDokter')))
    {
        $.ajax({ 
            url: '../cadmission/masterjadwal_caridokter',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataDokter', JSON.stringify(result));
                fillcombodokter(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombodokter(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombodokter(id, selected);
            },
            error: function(result){
                fillcombodokter(id, selected);
            }
        });
    }
}

//basit, progress
function fillcombodokter(id, selected)
{
    var datadokter = JSON.parse(localStorage.getItem('dataDokter'));
    var i=0;
    for (i in datadokter)
    {
      $("#"+id).append('<option ' + ((datadokter[i].idpegawai===selected)?'selected':'') + ' value="'+datadokter[i].idpegawai+'">'+datadokter[i].namalengkap+'</option>');
    }
}

//basit, clear
/// -- seting ubah jumlah tindakan
function set_jmltindakan(no)
{
    var idtindakan = $('#idtindakan'+no).val();
    var jumlah = $('#setjmltindakan'+no).val();
    if(jumlah=='')
    {
        $('#setjmltindakan').val(1);
        $('#setjmltindakan').focus();
        alert_empty('jumlah');//jika kosong tampilkan notif

    }
    else
    {
        startLoading();
        $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_ubahjumlahtindakan',
        data: {id:idtindakan,jumlah:jumlah},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            notif(result.status, result.message);
            pemeriksaanRefreshTindakan();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
    }
}

//-- basit, clear
function hapustindakan(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan?',
        buttons: {
            confirm: function () {//konfirm
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapustindakan',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success')
                        {
                            pemeriksaanRefreshTindakan();
                        }
                    },
                    error: function(result) { //jika error
                        stopLoading();
                        fungsiPesanGagal(); // console.log(result.responseText);
                        return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
//////////////////////////END TINDAKAN/////////////
// mahmud, clear
//////////////////////////START RADIOLOGI//////////
function pilihradiologi(value)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilradiologi",
        //x tidak perlu, agar bernilai null
        data:{y:'rencana',z:value,a:localStorage.getItem('idrencanamedispemeriksaan'),b:idpendaftaran, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshRadiologi(); //refresh radiologi
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
// mahmud, clear
function pemeriksaanRefreshRadiologi()//refreshDataRadiologi
{
    var x=0, html='', subtotal=0, no=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_listradiologi',
        data: {x:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                no++;
                subtotal += parseInt(result[x].total);
                html += '<tr id="listrencanaradiologi'+no+'"><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ convertToRupiah(result[x].total) +'</td><td>'+result[x].statuspelaksanaan+'</td>';
                // add menu aksi
                html +='<td>';
                if (result[x].statuspelaksanaan == 'terlaksana' || result[x].statuspelaksanaan == 'batal')
                {
                    if (result[x].statuspelaksanaan == 'terlaksana')
                    {
                        html += '<a class="btn btn-primary btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idrencanamedishasilpemeriksaan+',\'Radiologi\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>';
                    }
                    html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusRadiologi('+result[x].idrencanamedishasilpemeriksaan+', \'rencana\', \''+result[x].icd+'\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                }
                else
                {
                    html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusRadiologi('+result[x].idrencanamedishasilpemeriksaan+', \'terlaksana\', \''+result[x].icd+'\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                            ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusRadiologi('+result[x].idrencanamedishasilpemeriksaan+', \'batal\', \''+result[x].icd+'\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                }
                html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusRadiologi('+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                html += '<input type="hidden" id="idrencanaradiologi'+no+'" value="'+result[x].icd+'"/>';
                ((result[x].jumlahrencana)>0 ? html += ' <a id="tombolbatalrencanaradiologi'+no+'" onclick="batalRencana('+no+',\'rencanaradiologi\',\'Rencana Radiologi\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Radiologi (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )
                html+='</td></tr>';
            }
            $('#listRadiologi').empty();
            $('#listRadiologi').html(html+'<tr class="bg bg-info"><td colspan="2">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function hapusRadiologi(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan elektromedik?',
        buttons: {
            confirm: function () {//konfirm
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusradiologi',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshRadiologi();}/*tampil hasil radiologi*/
                    },
                    error: function(result) { 
                        fungsiPesanGagal();return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
// mahmud, clear
function ubahStatusRadiologi(v1, v2, v3)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilradiologi",
        data:{x:v1, y:v2, z:v3, k:localStorage.getItem("idkelas"), kj:localStorage.getItem("idkelasjaminan")},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshRadiologi();}//tampil hasil radiologi
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
//////////////////////////END RADIOLOGI///////////


// mahmud, clear
//////////////////////////START LABORATORIUM//////
function pilihlaboratorium(value,ispaket='')
{
    if(value!=0 || value !='')
    {
        ((ispaket=='') ? $('select[name="paketlaboratorium"]').val('') : $('select[name="carilaboratorium"]').val('') );
        startLoading();
        $.ajax({
            type:"POST",
            url:"pemeriksaanranap_updatehasillaboratorium",
            data:{
                y:'rencana',
                z:value,
                a:localStorage.getItem('idrencanamedispemeriksaan'),
                b:idpendaftaran,
                ispaket:ispaket, 
                k:localStorage.getItem("idkelas"), 
                kj:localStorage.getItem("idkelasjaminan")
            },
            dataType:"JSON",
            success:function(result){
                stopLoading();
                notif(result.status, result.message);
                if(result.status=='success'){
                    pemeriksaanRefreshLaboratorium();
                }
            },
            error:function(result){
                stopLoading();
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
// mahmud, clear
function ubahStatusLaboratorium(v1, v2, v3)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasillaboratorium",
        data:{
            x:v1, 
            y:v2, 
            z:v3, 
            k:localStorage.getItem("idkelas"), 
            kj:localStorage.getItem("idkelasjaminan")
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success'){pemeriksaanRefreshLaboratorium();}
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
// mahmud, clear
function ubahStatusPakettindakan(mode,id,status,idrencanamedishasilpemeriksaan)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_updatehasilpaket",
        data:{
            x:mode, 
            y:id, 
            z:status,
            a:localStorage.getItem('idrencanamedispemeriksaan'),
            b:idrencanamedishasilpemeriksaan, 
            k:localStorage.getItem("idkelas")
        },
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success'){
                if(mode=='Laboratorium'){pemeriksaanRefreshLaboratorium();}
                else{pemeriksaanRefreshVitalsign();}
            }//tampil hasil laboratorium
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear
function pemeriksaanRefreshLaboratorium()//refreshDataLaboratorium
{
    var x=0, html='', paket='', paketparent='', subtotal=0,menu='', no=0, idpaket=0, keteranganhasil='';
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listlaboratorium',
        data: {
            x:localStorage.getItem('idrencanamedispemeriksaan'),
            m:'view'
        },
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {   
                no++;
                menu='';
                
                //tampilkan keterangan hasil laborat
                if(result[x].idpaketpemeriksaanparent != idpaketpemeriksaanparent && idpaketpemeriksaanparent!=null)
                {
                     html += '<tr class="bg bg-info"><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
                     keteranganhasil='';
                }
                    
                idpaket = ((result[x].idpaketpemeriksaanparent==null)? result[x].idpaketpemeriksaan : result[x].idpaketpemeriksaanparent );
                menu += ' <a id="inputeditkesimpulanhasil" namapaket="'+result[x].namapaketpemeriksaanparent+'" idpaketpemeriksaan="'+result[x].idpaketpemeriksaanparent+'" class="btn btn-primary btn-xs" '+tooltip('Input/Edit Kesimpulan Hasil')+'><i class="fa fa-file-text"></i></a> ';
                menu += ' <a id="waktupengerjaanlabranap" mode="paket" idpaket="'+idpaket+'" namapaket="'+result[x].namapaketpemeriksaan+'" class="btn btn-info btn-xs" '+tooltip('Waktu Pengerjaan')+'><i class="fa fa-clock-o"></i></a> ';
                menu += ' <a onclick="cetak_hasillab('+idpaket+')" class="btn btn-info btn-xs" '+tooltip('Cetak Hasil')+'><i class="fa fa-print"></i></a> ';

                if (result[x].statuspelaksanaan == 'terlaksana' || result[x].statuspelaksanaan == 'batal')
                {
                    if (result[x].statuspelaksanaan == 'terlaksana' )
                    {
                        menu += '<a class="btn btn-primary btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idrencanamedishasilpemeriksaan+',\'Laboratorium\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>';
                    }
                    menu += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Laboratorium\','+idpaket+', \'rencana\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                }
                else
                {
                    menu += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Laboratorium\','+idpaket+', \'terlaksana\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                            ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusPakettindakan(\'Laboratorium\','+idpaket+', \'batal\','+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                }
                menu += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusPakettindakan(\'Laboratorium\','+idpaket+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                
                // end menu paket
                subtotal += parseInt(result[x].total);
                var paketlain = result[x].namapaketpemeriksaanparent;
                html += ((namapaket!=result[x].namapaketpemeriksaan & result[x].idpaketpemeriksaan!=null ) ? '<tr class="bg bg-gray" height="30px"><td colspan="5">'+result[x].namapaketpemeriksaan+'</td>'+  result[x].statuspelaksanaan +'</td>'+ ((result[x].namapaketpemeriksaanparent==null)? '<td>'+  convertToRupiah(result[x].total) +'</td><td><td>'+menu + '<input type="hidden" id="idrencanapaketlaboratorium'+no+'" value="'+result[x].idpaketpemeriksaan+'"/>'+((result[x].jumlahrencana)>0 ?  ' <a id="tombolbatalrencanapaketlaboratorium'+no+'" onclick="batalRencana('+no+',\'rencanapaketlaboratorium\',\'Rencana Paket Laboratorium\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Laboratorium (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : '' ) +'</td>' :'<td ></td><td >'+  result[x].statuspelaksanaan +'</td>') +'</tr>' : '');

               if(result[x].icd!=null)
               {
                    html += '<tr id="listrencanalaboratorium'+no+'"><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                    '<input type="hidden" id="idjenisnilaiicdlab'+no+'" value="'+result[x].istext+'"/>'+
                    '<td>'+ formInputHasilLaborat(no,result[x]) +'</td>'+ 
                    '<td>'+ result[x].nilaidefault +'</td>'+
                    '<td>'+ result[x].nilairujukan +'</td>'+
                    '<td>'+ result[x].satuan +'</td>'+
                    '<td>'+ ((result[x].namapaketpemeriksaan!=null) ? '' : convertToRupiah(result[x].total))+'</td>'+
                    '<td>'+ result[x].statuspelaksanaan +'</td>';

                    // menampilkan menu per icd
                    if(result[x].namapaketpemeriksaan==null )
                    {
                        html +='<td>';

                        if (result[x].statuspelaksanaan == 'terlaksana' || result[x].statuspelaksanaan == 'batal')
                        {
                            if (result[x].statuspelaksanaan == 'terlaksana' )
                            {
                                html += '<a class="btn btn-primary btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idrencanamedishasilpemeriksaan+',\'Laboratorium\')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>';
                            }
                            html += ' <a class="btn btn-warning btn-xs" data-toggle="tooltip" onclick="ubahStatusLaboratorium('+result[x].idrencanamedishasilpemeriksaan+', \'rencana\', \''+result[x].icd+'\')" data-original-title="Rencana"><i class="fa fa-times"></i></a>';
                        }
                        else
                        {
                            html += ' <a class="btn btn-success btn-xs" data-toggle="tooltip" onclick="ubahStatusLaboratorium('+result[x].idrencanamedishasilpemeriksaan+', \'terlaksana\', \''+result[x].icd+'\')" data-original-title="Terlaksana"><i class="fa fa-check"></i></a>' +
                                    ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="ubahStatusLaboratorium('+result[x].idrencanamedishasilpemeriksaan+', \'batal\', \''+result[x].icd+'\')" data-original-title="Batal"><i class="fa fa-minus-circle"></i></a>';
                        }
                        html += ' <a class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="hapusLaboratorium('+result[x].idrencanamedishasilpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                        html += '<input type="hidden" id="idrencanalaboratorium'+no+'" value="'+result[x].icd+'"/>';
                        ((result[x].jumlahrencana)>0 ? html += ' <a id="tombolbatalrencanalaboratorium'+no+'" onclick="batalRencana('+no+',\'rencanalaboratorium\',\'Rencana Tindakan\')" data-toggle="tooltip" data-original-title="Batalkan Rencana Laboratorium (Tidak Termasuk Yang Terlaksana)" class="btn btn-danger btn-xs"><i class="fa fa-arrows-alt"></i></a>' : html+='' )            
                        html += '</td>';
                    }
                    // end menu per icd
                    html +='</tr>';
                }
                var namapaket=result[x].namapaketpemeriksaan;
                var namaparent=result[x].namapaketpemeriksaanparent;
                if(result[x].keteranganhasil !=null && result[x].keteranganhasil != keteranganhasil)
                {
                    keteranganhasil = result[x].keteranganhasil;
                }
                var idpaketpemeriksaanparent = result[x].idpaketpemeriksaanparent;
            }
            //tampilkan keterangan hasil laborat
            html += '<tr class="bg bg-info"><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
            
            html+='<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listLaboratorium').empty();
            $('#listLaboratorium').html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal();return false;
        }
    });
    
}

//set waktu pengerjaan hasil laboratorium
$(document).on('click','#waktupengerjaanlabranap',function(){
    var modeset  = $(this).attr('mode');
    var namapaket = ((modeset=='paket') ? $(this).attr('namapaket') : 'Semua Tindakan Laborat Non Paket');
    var idpaket   = $(this).attr('idpaket');
    
    var modalTitle = 'Waktu Pengerjaan';
    var modalContent = '<form action="" id="waktupengerjaan">' +
                            '<div class="form-group">' +
                                '<label>Tindakan Laboratorium</label>' +
                                '<input type="text" class="form-control" value="'+namapaket+'" readonly/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Waktu Mulai Pengerjaan ( YYYY-MM-DD H:i:s )</label>' +
                                '<input type="text" name="waktumulai" placeholder="waktu mulai" class="form-control datetimepicker"/>' +
                                '<small class="text text-red">Mohon isi sesuai waktu pelayanan.!</small>'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Waktu Selesai ( YYYY-MM-DD H:i:s )</label>' +
                                '<input type="text" name="waktuselesai" placeholder="waktu selesai" class="form-control datetimepicker"/>' +
                                '<small class="text text-red">Mohon isi sesuai waktu pelayanan.!</small>'
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'm',
        buttons: {
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    startLoading();
                    $.ajax({
                        type:"POST",
                        url:"simpan_waktupelayananranaplaborat",
                        data:{
                            x:localStorage.getItem('idrencanamedispemeriksaan'),
                            y:idpaket,waktumulai:$('input[name="waktumulai"]').val(),
                            waktuselesai:$('input[name="waktuselesai"]').val(),
                            mode:modeset
                        },
                        dataType:"JSON",
                        success:function(result){
                            stopLoading();
                            notif(result.status, result.message);
                        },
                        error:function(result){
                            stopLoading();
                            fungsiPesanGagal();return false;
                        }
                    });
                }
            },
            formReset:{
            	text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH:mm:ss", sideBySide: true});
        getwaktuperiksalaborat(idpaket,modeset);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});

//tampilkan waktu pemeriksaan lab jika sudah ada
function getwaktuperiksalaborat(idpaket,modeset)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:base_url+"cpelayananranap/getwaktuperiksalaborat",
        data:{x:localStorage.getItem('idrencanamedispemeriksaan'),y:idpaket,mode:modeset},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(result==null){
                $.alert('Belum Ada Tindakan Laboratorium.');
            }else{
                $('input[name="waktumulai"]').val( result.waktumulai );
                $('input[name="waktuselesai"]').val( result.waktuselesai );
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}


//Insert Update Kesimpulan Hasil Laboratorium
$(document).on('click','#inputeditkesimpulanhasil',function(){
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var namapaket = $(this).attr('namapaket');
    var idpaketpemeriksaan  = $(this).attr('idpaketpemeriksaan');
    
    var konten = '<b>Paket Laborat : '+namapaket+' </b> </br>'
                +'<b>Kesimpulan</b> : <input type="hidden" id="mode_kesimpulanhasil"/> <textarea id="kesimpulanlaborat" class="form-control textkesimpulan" rows="12"></textarea>'
    $.confirm({
        icon: 'fa fa-check-square-o',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        columnClass:'l',
        title: 'Kesimpulan Hasil',
        content: konten,
        buttons: {
            simpan: function () { 
                $.ajax({
                    url:base_url+"cpelayananranap/insert_update_get_kesimpulanhasillaborat",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        idrencanamedispemeriksaan:idrencanamedispemeriksaan,
                        idpaketpemeriksaan:idpaketpemeriksaan,
                        kesimpulan:$('#kesimpulanlaborat').val(),
                        mode:$('#mode_kesimpulanhasil').val()
                    },
                    success:function(result){
                        notif(result.status, result.message);
                        var idpendaftaran = $('input[name="idpendaftaran"]').val();
                        pemeriksaanRefreshLaboratorium(idpendaftaran);
                    },
                    error:function(result){ 
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            batal: function () {               
            }            
        },
        onContentReady: function () { // bind to events 
           startLoading();
           $.ajax({
                url:base_url+"cpelayananranap/insert_update_get_kesimpulanhasillaborat",
                type:"POST",
                dataType:"JSON",
                data:{
                    idrencanamedispemeriksaan:idrencanamedispemeriksaan,
                    idpaketpemeriksaan:idpaketpemeriksaan,
                    mode:'getdata'
                },
                success:function(result){
                    stopLoading();
                    $('#mode_kesimpulanhasil').val( ((result!=null) ? 'updatedata' : 'insertdata' ) );
                    $('#kesimpulanlaborat').val( ((result!=null) ? result.keteranganhasil : '') );
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.textkesimpulan').wysihtml5();
                },
                error:function(result){ 
                    stopLoading();
                    fungsiPesanGagal();
                    return false;
                }
            }); 
        }
    });
});


function formInputHasilLaborat(no,result)
{
    var form='';
    if(result.istext=='text' || result.istext=='textnumerik')
    {
        form = '<textarea '+ ((result.isbolehinput>0)? '' : 'readonly' ) +' onchange="simpanHasil_icdPeriksa(\'Laboratorium\','+no+',\'nilaiicdlab\','+result.idrencanamedishasilpemeriksaan+')" id="nilaiicdlab'+no+'" class="form-control" '+ql_readonlytxt(result.statuspelaksanaan,'terlaksana')+' placeholder="Input '+result.istext+'" rows="2">'+ result.nilaitext +'</textarea>';
    }
    else if(result.istext=='numerik')
    {
        form = '<input class="qlform-control" onchange="simpanHasil_icdPeriksa(\'Laboratorium\','+no+',\'nilaiicdlab\','+result.idrencanamedishasilpemeriksaan+')" type="text" value="'+result.nilai+'" placeholder=" Input '+result.istext+'" id="nilaiicdlab'+no+'" '+ql_readonlytxt(result.statuspelaksanaan,'terlaksana')+' size="25" />';
    }
    else
    {
        form ='';
    }
    
    return form;
}
// mahmud, clear
function simpanHasil_icdPeriksa(mode,no,idnilai,idhasil)
{
    var nilai = $('#'+idnilai+no).val();
    var jenisnilai = $('#idjenis'+idnilai+no).val();
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaanranap_simpanHasil_icdPeriksa",
        data:{w:jenisnilai,x:mode,y:idhasil,z:nilai},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();return false;
        }
    });
}
// mahmud, clear 
function hapusPakettindakan(mode,idpaket)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus paket tindakan '+mode+'?',
        buttons: {
            confirm: function () {//konfirm
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaanranap_hapusPakettindakan',
                    data: {x:idpaket,y:mode,z:localStorage.getItem('idrencanamedispemeriksaan')},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success'){
                            if(mode=='Laboratorium'){pemeriksaanRefreshLaboratorium();}/*tampil hasil Laboratorium*/
                            else{pemeriksaanRefreshVitalsign();}/*tampil hasil Vitalsign*/
                        }
                    },
                    error: function(result) { 
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
// mahmud, clear
function hapusLaboratorium(x)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: 'Hapus tindakan laboratorium?',
        buttons: {
            confirm: function () {//konfirm
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapuslaboratorium',
                    data: {x:x},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success'){pemeriksaanRefreshLaboratorium();}/*tampil hasil Laboratorium*/
                    },
                    error: function(result) { 
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
        },
            cancel: function () {//cancel            
            }            
        }
    });
}
function hapuspaketparent(x,z)
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuspaketpemeriksaan',
        data: {x:x, y:idpendaftaran},
        dataType: "JSON",
        success: function(result) {
            console.log(result);
            notif(result.status, result.message);
            if(result.status=='success')
            {
                if(z=='laboratorium')
                {
                    pemeriksaanRefreshLaboratorium(idpendaftaran);
                }
                else
                {
                    pemeriksaanRefreshVitalsign(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////diagnosa_or_tindakan//////////////////
function diagnosa_or_tindakan(x,y)
{
    startLoading();
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_diagnosa",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            stopLoading();
            return {
                q: params.term,
                jenisicd: y,
                idinap:$('input[name="idinap"]').val(),
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
///////////////////////////////////////////////
function pilihJenisIcd(value)//pilih jenis diagnosa
{
    pilihPaketDiagnosa($('select[name="paket"]'),value);
}
function singleDiagnosa(value)//tambah single diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addsinglediagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function paketDiagnosa(value)//tambah paket diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addpaketdiagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pilihSingleDiagnosa(selectNameTag,icd) //pilih diagnosa 
{  
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_cariicd",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                icd:icd,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.namaicd + ((item.aliasicd!='') ? ' >>> ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}
function pilihPaketDiagnosa(htmlTagName,icd) //pilih diagnosa 
{  
    $.ajax({
        url:'diagnosa_pilihpaket',
        type:'POST',
        dataType:'JSON',
        data:{x:icd},
        success: function(result){
            var select='<option value="0" >Pilih</option>';
            htmlTagName.empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idpaketpemeriksaan +'" >' + result[i].namapaketpemeriksaan +'</option>';
            }
            htmlTagName.html(select);
            $('.select2').select2();
            pilihSingleDiagnosa($('select[name="single"]'),icd);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
// mahmud, clear
function updateBiayahasilperiksa(y,z)//seting hahasilpemeriksaanpaket laboratorium
{
    var idpaketygdiubah = y;
    var modalTitle = 'Ubah Biaya';
    var modalContent = '<form action="" id="Formubahhargapaket">' +
                            '<div class="form-group">' +
                            '<input type="hidden" name="id" value="'+idpaketygdiubah+'" class="form-control"/>'+
                            //jasa operator
                                '<div class="col-md-3">'+
                                    '<label>Jasa Operator</label>' +
                                    '<input type="text" name="jasaoperator" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //nakes
                                '<div class="col-md-3">'+
                                    '<label>Nakes</label>' +
                                    '<input type="text" name="nakes" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //jasars
                                '<div class="col-md-3">'+
                                    '<label>Jasars</label>' +
                                    '<input type="text" name="jasars" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //bhp
                                '<div class="col-md-3">'+
                                    '<label>Bhp</label>' +
                                    '<input type="text" name="bhp" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //akomodasi
                                '<div class="col-md-3">'+
                                    '<label>Akomodasi</label>' +
                                    '<input type="text" name="akomodasi" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //margin
                                '<div class="col-md-3">'+
                                    '<label>Margin</label>' +
                                    '<input type="text" name="margin" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //sewa
                                '<div class="col-md-3">'+
                                    '<label>Sewa</label>' +
                                    '<input type="text" name="sewa" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Update',
                btnClass: 'btn-blue',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: 'savepaketygdiubah', //alamat controller yang dituju
                        data: $("#Formubahhargapaket").serialize(), //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            notif(result.status, result.message);
                            if(result.status=='success')
                            {
                                if(z=='Tindakan')
                                {
                                    pemeriksaanRefreshTindakan();
                                }
                                else if(z=='Radiologi')
                                {
                                    pemeriksaanRefreshRadiologi();
                                }
                                else
                                {
                                    pemeriksaanRefreshLaboratorium();
                                }
                            }
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        //tampilkan harga yang akan diubah
        tampilhargayangdiubah(idpaketygdiubah);
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function tampilhargayangdiubah(x)
{
    $.ajax({
        type:"POST",
        url:"tampilhargayangdiubah",
        data:{x:x},
        dataType:"JSON",
        success: function(result){
            $('input[name="jasaoperator"]').val(result.jasaoperator);
            $('input[name="nakes"]').val(result.nakes);
            $('input[name="jasars"]').val(result.jasars);
            $('input[name="bhp"]').val(result.bhp);
            $('input[name="akomodasi"]').val(result.akomodasi);
            $('input[name="margin"]').val(result.margin);
            $('input[name="sewa"]').val(result.sewa);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusPaketPeriksa(value)//hapus paket pemeriksaan
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = value;
    $.ajax({
        type:"POST",
        url:"delete_paket_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function deleteHasilPeriksa(value)//hapus hasil periksa per icd
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = $('#deleteHasilPeriksa'+value).val();
    $.ajax({
        type:"POST",
        url:"delete_hasil_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanranapRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaan_tampilpasien() //tampil data pasien
{
	$.ajax({
		type:"POST",
		url:"pemeriksaan_tampilpasien",
		data:{x:norm, y:localStorage.getItem('idrencanamedispemeriksaan')},
		dataType:"JSON",
		success: function(result){
			$('#labNormPasien').html(': '+result.p.norm);
			$('#labNamaPasien').html(': '+result.p.namalengkap);
			$('#labPengirim').html(': '+result.per.titeldepan+' '+result.per.namalengkap+' '+result.per.titelbelakang);
			$('#labKelaminPasien').html(': '+result.p.jeniskelamin);
			$('#labTgllahirPasien').html(': '+result.p.tanggallahir);
		},
		error:function(result){
			fungsiPesanGagal();
			return false;
		}
	});
}

//////////////MENAMPILKAN DATA DETAIL PASIEN//////////////////
function pemeriksaandetailTampilPasien()
{
	$.ajax({
		type:"POST",
		url:"pemeriksaan_detail_tampil_pasien",
		data:{x:localStorage.getItem('idrencanamedispemeriksaan')},
		dataType:"JSON",
		success:function(result){
			// console.log(result);
			pemeriksaandetailTampilPasienListData(result);
		},
		error:function(result){
			fungsiPesanGagal();
			return false;
		}
	});
}
function pemeriksaandetailTampilPasienListData(data)
{
	var modalTitle = '<div class="col-sm-12 center">Detail Pasien</div>';
    var modalContent = '<div class="col-sm-6">'+
                            	'<table class="table table-striped" >'+
                            		'<tr><th>Nama</th><th style="font-weight:normal;">: '+if_empty(data.namalengkap)+'</th></tr>'+
                            		'<tr><th>No.RM</th><th style="font-weight:normal;">: '+if_empty(data.norm)+'</th></tr>'+
                            		'<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamat+'</th></tr>'+
                            		'<tr><th>Telpon</th><th style="font-weight:normal;">: '+data.telponpasien+'</th></tr>'+
                            		'<tr><th>Kelamin</th><th style="font-weight:normal;">: '+data.jeniskelamin+'</th></tr>'+
                            		'<tr><th>TanggalLahir</th><th style="font-weight:normal;">: '+data.tanggallahir+'</th></tr>'+
                            		'<tr><th>Agama</th><th style="font-weight:normal;">: '+data.agama+'</th></tr>'+
                            		'<tr><th>Status</th><th style="font-weight:normal;">: '+data.statusmenikah+'</th></tr>'+
                            		'<tr><th>Golongan Darah</th><th style="font-weight:normal;">: '+data.golongandarah+'</th></tr>'+
                            		'<tr><th>Rhesus</th><th style="font-weight:normal;">: '+data.rh+'</th></tr>'+
                            		'<tr><th>Alergi</th><th style="font-weight:normal;">: '+data.alergi+'</th></tr>'+
                            	'</table>'+
                        	'</div>'+
                        	'<div class="col-sm-6">'+
                            	'<table class="table table-striped" >'+
                            		'<tr><th>Penanggung</th><th style="font-weight:normal;">: '+data.namapenjawab+'</th></tr>'+
                            		'<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamatpenjawab+'</th></tr>'+
                            		'<tr><th>No.Telpon</th><th style="font-weight:normal;">: '+data.notelpon+'</th></tr>'+
                            		'<tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr>'+
                            		'<tr><th>Klinik</th><th style="font-weight:normal;">: '+data.namaunit+'</th></tr>'+
                            		'<tr><th>No.Antri</th><th style="font-weight:normal;">: '+data.noantrian+'</th></tr>'+
                            		'<tr><th>Dokter</th><th style="font-weight:normal;">: '+data.titeldepan+' '+data.namadokter+' '+data.titelbelakang+'</th></tr>'+
                            		'<tr><th>Carabayar</th><th style="font-weight:normal;">: '+data.carabayar+'</th></tr>'+
                            		'<tr><th>No.SEP</th><th style="font-weight:normal;">: '+data.nosep+'</th></tr>'+
                            	'</table>'+
                        	'</div>';
    $.alert({
        title: modalTitle,
        content: modalContent,
        columnClass: 'xl',
    });
}
//basit, clear
function pemeriksaanranapDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanranap_caridetailpasien',
        data: {
            i:localStorage.getItem('idinap'),
            x:localStorage.getItem('idrencanamedispemeriksaan')
        },
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if (is_null(norm) || is_null(idpendaftaran))
            {
                norm            = result.pasien.norm;
                localStorage.setItem('norm', norm);
                idpendaftaran   = result.pasien.idpendaftaran;
                localStorage.setItem('idpendaftaranranap', idpendaftaran);
            }
            
            $('#waktupemeriksaan').html(result.rencana.waktu);
            $('textarea[name="catatan"]').val(result.pasien.catatan);
            $('#keteranganradiologi').val(result.rencana.keteranganradiologi);
            $('#saranradiologi').val(result.rencana.saranradiologi);
            $('textarea[name="keteranganlaboratorium"]').val(result.pasien.keteranganlaboratorium);
            $('textarea[name="keteranganobat"]').val(result.pasien.keteranganobat);
            
            viewIdentitasPasien(result.pasien);//panggil view identitas
            $('input[name="idpendaftaran"]').val(result.pasien.idpendaftaran);
//            $('input[name="idunit"]').val(result.rajal.idunit);
            $('input[name="norm"]').val(result.pasien.norm);
            
            $('textarea[name="dokter"]').val(if_empty(result.rajal.dokter));
            $('textarea[name="anamnesa"]').val(result.rajal.anamnesa);
            $('textarea[name="keterangan"]').val(result.rajal.keterangan);
            
            $('.textarea').wysihtml5({toolbar:false});
            
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}



function viewIdentitasPasien(value)//tampilidentitas
{
    
    var profile = '<div class="bg bg-gold">'
                +'<div class="box-body box-profile" >'
                 +'<div class="login-logo" style="margin-bottom:0px;">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-center">NIK : '+value.nik+'</p>'
                  +'<h5 class=" text-center"> <i class="fa fa-calendar margin-r-5"></i> '+value.tanggallahir+' /  <i>'+value.usia+'</i> </h5>'
                  +'<p class="text-center"><i>Ayah : '+value.ayah+' , Ibu : '+value.ibu+'</i></p>'
                +'</div>'
              +'</div>';
    var detail_profile = '<div class="bg bg-gold"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td>: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                 +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td>: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Tempat Lahir</strong></td>'
                  +'<td>: &nbsp;'+value.tempatlahir+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td>: &nbsp; '+value.jeniskelamin+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td>: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' 
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td>: &nbsp; '+value.namapendidikan+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i> Agama</strong></td>'
                  +'<td>: &nbsp;'+value.agama+'</td>'
                  
                +'</tr>'
        
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Alamat </strong></td>'
                  +'<td width="250px">: &nbsp; '+value.alamat+'</td>'

                  
                +'</tr>'
                +'</tbody>'
             +'</table></div></div>';
    $('#ranap_profile').html(profile);
    $('#ranap_detailprofile').html(detail_profile);
    
//    $('#ranap_detailperiksa').html(detail_periksa);
//    $('#view_identitasPasien').html('<b>No.RM:</b> '+value.norm+' ,<b> No.JKN:</b>  '+value.nojkn+',<b> Penjamin:</b>  '+value.carabayar+' ,<b> NIK:</b>  '+value.nik+', <b>Nama:</b>  '+value.namalengkap+', <b> TanggalLahir:</b> '+value.tanggallahir+', <b> Alamat:</b> '+value.alamat+' [<b style="color:#000;">'+ ((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama )+' | '+value.kelas +'</b>]');
}
// mahmud, clear
function batalRencana(no,mode,pesan)
{
    var ispaket='', value = $('#id'+mode+no).val();
    if(mode=='rencanapaketlaboratorium' || mode=='rencanapaketvitalsign'){ispaket='paket';}
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Batalkan Rencana '+pesan+'? (Tidak Termasuk Yang Terlaksana)',
        buttons: {
            confirm: function () {    
                startLoading();
                $.ajax({
                    type: "POST", //tipe pengiriman data
                    url: base_url + 'cpelayananranap/batalrencana_periksaranap',
                    data: {i:value,mode:mode,pesan:pesan,ispaket:ispaket, idinap:localStorage.getItem('idinap')},//, k:localStorage.getItem('idrencanamedispemeriksaan')
                    dataType: "JSON", //tipe data yang dikirim
                    success: function(result) { //jika  berhasil
                        stopLoading();
                        if(result.status=='success')
                        {
                            notif(result.status, result.message);
                            $('#tombolbatal'+mode+no).remove();
                            if (mode=='rencanabhp' || mode=='rencanabhptindakan')
                            {
                                pemeriksaanRefreshTindakan();
                                pemeriksaanranapRefreshBhp();
                            }
                            else if (mode == 'rencanalaboratorium' || mode == 'rencanapaketlaboratorium')
                            {
                                pemeriksaanRefreshLaboratorium();
                            }
                            else if (mode == 'rencanaradiologi')
                            {
                                pemeriksaanRefreshRadiologi();
                            }
                            else if (mode == 'rencanatindakan')
                            {
                                pemeriksaanRefreshTindakan();
                            }
                            else if (mode == 'rencanavitalsign' || mode == 'rencanapaketvitalsign')
                            {
                                pemeriksaanRefreshVitalsign()();
                            }
                        }
                        else
                        {
                            notif(result.status, result.message);
                            return false;
                        }
                    },
                    error: function(result) { //jika error
                        stopLoading();
                        fungsiPesanGagal(); // console.log(result.responseText);
                        return false;
                    }
                });
            },
            cancel: function () {               
            }            
        }
    });
}

//--- mahmud, clear
//tambah obat atau bhp ke tindakan
function tambahBarangKeTindakan(idrencanamedispemeriksaan, idrencanamedishasilpemeriksaan)
{
    jumlahicdtindakan = 0;
    var modalTitle = 'Tambah Obat/BHP ke Tindakan';
    var modalContent = '<form action="" id="FormTambahBarangKeTindakan">' +
                        '<input type="hidden" value="'+idrencanamedispemeriksaan+'" name="idrencanamedispemeriksaan">'+
                        '<input type="hidden" value="'+idrencanamedishasilpemeriksaan+'" name="idrencanamedishasilpemeriksaan">'+
                        '<input type="hidden" value="'+idpendaftaran+'" name="idpendaftaran">'+
                            '<div class="form-group">' +
                                '<div class="form-group">' +
                                '<div class="col-sm-12">' +
                                '<label>Obat/BHP</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="tambahbhpditindakan">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>' +
                                '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'tambah',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else
                    {
                        simpanBarangKeTindakan();
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            select2serachmulti($('select[name="tambahbhpditindakan"]'),'cmasterdata/fillbarang');
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function simpanBarangKeTindakan()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencanabhp_single',
        //data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icd"]').val(), ipd:$('select[name="idpegawaidokter"]').val(), td:$('input[name="jumlahtindakan"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        data: $("#FormTambahBarangKeTindakan").serialize(),
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            pemeriksaanRefreshTindakan();
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}

$(document).on('click','#cetakexpertise',function(){
   var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
   elektromedik_cetakexpertiseranap(idrencanamedispemeriksaan);
});


$(document).on('click','#simpanHasilExpertiseRanap',function(){
    
    var keterangan = $('#keteranganradiologi').val();
    var saran      = $('#saranradiologi').val();
    simpanHasilExpertiseRanap(keterangan,saran);
});

function simpanHasilExpertiseRanap(keterangan,saran)
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpanHasilExpertiseRanap',
        data: {
            keterangan:keterangan,
            saran:saran,
            idrencanamedispemeriksaan:$('input[name="idrencanamedispemeriksaan"]').val()
        },
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
        },
        error: function(result) { //jika error
            console.log(result.responseText);
        }
    });
}
$(document).on('click','#riwayatperawatan',function(){
    var idinap = $('input[name="idinap"]').val();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    var norm = $('input[name="norm"]').val();
    getdata_riwayatperawatanranap(idinap,idpendaftaran,norm);
});

//input soap ranap
$(document).on('click','#inputsoap',function(){
    form_soap('');
});

//edit soap ranap
$(document).on('click','#editsoap',function(){
    form_soap($(this).attr('waktu'));
});

function form_soap(waktu)
{
    var idrencanamedispemeriksaan = localStorage.getItem('idrencanamedispemeriksaan');
    var modalTitle = 'INPUT SOAP';
    var modalContent = '<form action="" id="FormSOAP" class="col-xs-12">' +
                            '<input type="hidden" value="'+idrencanamedispemeriksaan+'" name="idrp" />'+
                            '<input type="hidden" value="" name="idpetugas" id="idpetugas" />'+
                            '<input type="hidden" id="idprofesi" name="idprofesi" value="" />'+
                            '<input type="hidden" value="" name="waktu" id="waktu" />'+
                            '<div class="form-group">' +
                                '<label>Nama Petugas</label>' +
                                '<select class="form-control" id="idpegawai" name="idpegawai"><option>Pilih</option></select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>SIP</label>' +
                                '<input class="form-control" id="sip" name="sip" placeholder="[otomatis sesuai petugas yang dipilih]" readonly />'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Profesi</label>' +
                                '<input class="form-control" id="profesi" name="profesi" placeholder="[otomatis sesuai petugas yang dipilih]" readonly />'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>SOA</label>' +
                                '<textarea class="form-control textarea" id="soa_soa" name="soa_soa" placeholder="Input.. " rows="8"></textarea>'+
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>P</label>' +
                                '<textarea class="form-control textarea" id="soa_p" name="soa_p" placeholder="Input.." rows="8"></textarea>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'l',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'Simpan',
                btnClass: 'btn-blue',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cpelayananranap/insertupdate_rs_inap_soap',
                        data: $("#FormSOAP").serialize(),
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            load_soap();
                            notif(result.status,result.message);
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            //menu back
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            edit_soap(waktu);
            select2serachmulti($('#idpegawai'),'cmasterdata/fillpetugasranap');
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function edit_soap(waktu)
{
    if(waktu != '')
    {
        var idrp = localStorage.getItem('idrencanamedispemeriksaan');
        $.ajax({
            type: "POST", //tipe pengiriman data
            url: base_url + 'cpelayananranap/edit_rs_inap_soap',
            data: {idrp:idrp,waktu:waktu},
            dataType: "JSON", //tipe data yang dikirim
            success: function(result) { //jika  berhasil
                $('#idpegawai').html('<option value="'+result.idpegawai+'">'+result.pegawai+'</option>');
                $('#idpetugas').val(result.idpegawai);
                $('#idprofesi').val(result.idprofesi);
                $('#waktu').val(result.waktu);
                $('#sip').val(result.sip);
                $('#profesi').val(result.profesi);
                $('#soa_soa').val(result.soa);
                $('#soa_p').val(result.p);
                $('#idpegawai').attr('disabled',true);
            },
            error: function(result) { //jika error
                stopLoading();
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
    }
}

$(document).on('change','#idpegawai',function(){    
    var isi = this.value;
    var dt = this.value.split(",");
    $('#idpetugas').val(dt[0]);
    $('#sip').val(dt[1]);
    $('#idprofesi').val(dt[3]);
    $('#profesi').val(dt[2]);
});

function load_soap()
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/dt_soapranap',
        data: {idrp:localStorage.getItem('idrencanamedispemeriksaan')},
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            var rows = '';
            for( var x in result)
            {
                rows += '<tr><td>'+result[x].waktu+'</td><td>'+result[x].pegawai+'</td><td>'+result[x].sip+'</td><td>'+result[x].profesi+'</td><td>'+result[x].soa+'</td><td>'+result[x].p+'</td><td><a id="editsoap" waktu="'+result[x].waktu+'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> ubah</a></td></tr>';
            }
            $('#listsoap').html(rows);
            stopLoading();
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}