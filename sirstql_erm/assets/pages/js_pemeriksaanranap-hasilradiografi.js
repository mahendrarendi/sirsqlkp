//upload file efilm radiologi
$(document).on('click','#btnRadiologiEfilm',function(){
    var norm          = $('input[name="norm"]').val();
    var idperencanaan = $('input[name="idrencanamedispemeriksaan"]').val();
    var modalContent = '<form id="formUploadHasilradiografi">' +
        '<div class="form-group">' +
            '<input type="hidden" name="norm" value="'+norm+'" />'+
            '<input type="hidden" name="idpendaftaran" value="'+idpendaftaran+'" />'+
            '<input type="hidden" name="idperencanaan" value="'+idperencanaan+'" />'+
            '<label>Pilih File</label>' +
            '<input class="form-control" accept=".png,.jpg,.jpeg,.gif" type="file" id="fileEfilm" name="fileEfilm">' +
        '</div>' +
    '</form>';
        $.confirm({
        title  : 'Unggah Hasil Radiografi',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
            text: 'simpan',
            btnClass: 'btn-blue',
                action: function () {
                    if($('#fileEfilm').val() == '')
                    {
                        alert('File Harap Dipilih.');
                        return false;
                    }
                    else 
                    {
                        startLoading();
                        var formData = new FormData($('#formUploadHasilradiografi')[0]);
                        $.ajax({
                            type: "POST",
                            url: base_url + "cpelayananranap/upload_file_radiografi",
                            data: formData,
                            async: false,
                            cache: false,
                            contentType: false,
                            enctype: 'multipart/form-data',
                            processData: false,
                            dataType: "JSON",
                            success: function(result) {            
                                stopLoading();
                                notif(result.status, result.message);
                                if(result.status == 'success')
                                {
                                    tampilHasilRadiografi(idpendaftaran,idperencanaan);
                                }
                            },
                            error: function(result) {
                                stopLoading();
                                notifError();
                                $('#formUploadHasilradiografi')[0].reset();
                            }
                        });
                    }
                }
            },
           
            formReset:{ //menu batal
                text: 'Batal',
                btnClass: 'btn-danger',
                action: function(){
                }
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
});

//tampil hasil radiografi
function tampilHasilRadiografi(idpendaftaran,idperencanaan,mode='')
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayananranap/view_file_radiografi', //alamat controller yang dituju (di js base url otomatis)
        data: {
            idpendaftaran:idpendaftaran,
            idperencanaan:idperencanaan,
            viewmode:mode
        }, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) {
            $('#viewHasilRadiografi').empty();
            $('#viewHasilRadiografi').html(result);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error            
            fungsiPesanGagal();
            return false;
        }
    });
}

//hapus file radiografi
$(document).on('click','#delete_file_radiografi',function(){
    var idpendaftaran = $(this).attr('idpendaftaran');
    var idperencanaan = $(this).attr('idperencanaan');
    var idefilm  = $(this).attr('idefilm');
    var filename = $(this).attr('filename');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: 'Hapus hasil radiografi.?',
        buttons: {
            Hapus: function () {                
                startLoading();
                $.ajax({
                    type: "POST",
                    url: base_url+'cpelayananranap/delete_file_radiografi',
                    data: {
                        idpendaftaran:idpendaftaran,
                        idefilm:idefilm,
                        idperencanaan:idperencanaan,
                        filename:filename
                    },
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success')
                        {
                            tampilHasilRadiografi(idpendaftaran,idperencanaan);
                        }
                    },
                    error: function(result) {
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
            },
            Batal: function () {               
            }            
        }
    });  
});