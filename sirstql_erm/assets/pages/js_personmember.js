var personmember='';
$(function () {
    listdata();
})

function listdata()
{ 
  personmember = $('#personmember').DataTable({"processing": true,"serverSide": true,"lengthChange": false,"searching" : true,"stateSave": true,"order": [],
 "ajax": {
     "data":{},
     "url": base_url+'cadmission/dt_personmember',
     "type": "POST"},
     "columnDefs": [{ "targets": [0,4,5],"orderable": false,},],
 "fnCreatedRow": function (nRow, aData, iDataIndex) { /*$(nRow).attr('style', qlstatuswarna(aData[10]));*/}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
 "drawCallback": function (settings, json) {
    $('[data-toggle="tooltip"]').tooltip(); 
  },
// -- Function that is called every time DataTables performs a draw.
 });
}
//muat ulang data pasien member 
$(document).on('click','#reload',function(){
  $('input[type="search"]').val('').keyup();
  personmember.state.clear();
  personmember.ajax.reload();
});
//load data pasien member
function reloaddata(){personmember.ajax.reload(null,false);}
//tambah member
$(document).on("click",'#add',function(){formtambahedit('','Tambah Member Pasien');});
//ubah member
$(document).on("click",'#edit',function(){
    var norm = $(this).attr('idmember')+','+$(this).attr('norm');
    formtambahedit(norm,'Ubah Member Pasien');
});

//hapus member
$(document).on('click','#delete',function(){
    var norm = $(this).attr('norm');
    var idmember = $(this).attr('idmember');
    $.confirm({
        title: 'Hapus Member',
        content: '.',
        closeIcon: true,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Hapus',
                btnClass: 'btn-warning',
                action: function () {
                    startLoading();
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: base_url + 'cadmission/hapusmember', //alamat controller yang dituju (di js base url otomatis)
                        data: {norm:norm, idmember:idmember}, //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            stopLoading();
                            location.reload();
                            return false;
                        },
                        error: function(result) { //jika error
                            stopLoading();
                            console.log(result.responseText);
                        }
                    });
                }
            },
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
    });
    
});
//detail member
$(document).on("click",'#detailmember',function(){
    var norm = $(this).attr('norm');
    var modalContent = '<div class="col-xs-12"><b>NORM : </b>'+ $(this).attr('norm') +'<br> <b>NAMA PASIEN : </b>'+ $(this).attr('namalengkap') +'<br> <b>TGL Lahir :</b> '+ $(this).attr('tgllahir') +'<br> <b>Alamat : </b>'+ $(this).attr('alamat') +'</div>';
        modalContent+= '<div class="col-xs-12">'
                +'<table class="table table-bordered table-striped">'
                +'<thead><tr class="bg bg-yellow-gradient"><td>Member</td><td>Keterangan</td><td>Tanggal Daftar</td><td>Daftar Ulang</td><td>Status</td><td width="80px;"></td></tr></thead>'
                +'<tbody id="list_member"></tfoot>'
                +'</table></div>';
        
    $.dialog({
        title: 'Detail Member',
        content: modalContent,
        columnClass: 'middle',closeIcon: true,animation: 'scale',type: 'orange',
        onContentReady: function () {
            startLoading();
            var rows_member='';
            $.ajax({ 
                url:base_url+'cadmission/detailmember',type:"POST",dataType:"JSON",data:{norm:norm},
                success: function(result){
                    stopLoading();           
                    for(var x in result)
                    {
                        rows_member += '<tr><td>'+result[x].member+'</td><td>'+result[x].keterangan+'</td><td>'+result[x].tanggaldaftar+'</td><td>'+result[x].daftarulang+'</td><td>'+result[x].status+'</td><td><a id="edit" norm="'+norm+'" idmember="'+result[x].idmember+'" class="btn btn-warning btn-xs" '+tooltip('Ubah')+'><i class="fa fa-edit"></i></a> <a id="delete" norm="'+norm+'" idmember="'+result[x].idmember+'" class="btn btn-danger btn-xs" '+tooltip('Hapus')+'><i class="fa fa-trash"></i></a></td></tr>';
                    }
                    $('#list_member').html(rows_member);
                    $('[data-toggle="tooltip"]').tooltip();
                },
                error: function(result){stopLoading();fungsiPesanGagal();return false;}
            });
        }
    });
});

function formtambahedit(norm,judul)
{
    var modalTitle = judul;
    var modalContent = '<form action="" id="formmember" autocomplete="off">' +
                            '<div class="form-group">' +
                                '<input type="hidden" name="mode" value="Tambah Member" />'+
                                '<div class="col-md-12">' +
                                    '<label>Pilih Pasien</label>' +
                                    '<select id="norm" name="norm" class="form-control select2"><option value="">-Pilih-</option></select>' +
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Pilih Member</label>' +
                                    '<select  id="idmember" name="idmember"  class="form-control select2"><option value="">-Pilih-</option></select>' +
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group" >' +
                                '<div class="col-md-12">' +
                                    '<label>Tanggal Mendaftar</label>' +
                                    '<input type="text" name="tanggaldaftar" class="form-control datepicker" readonly />'+
                                '</div>'+
                            '</div>'+
                            
                            '<div class="form-group">' +
                                '<div class="col-md-12">' +
                                    '<label>Hari Berlaku</label>' +
                                    '<input type="text" id="masamember" name="hariberlaku" class="form-control" readonly/>'+
                                '</div>'+
                            '</div>'+
                            
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 's',closeIcon: true,animation: 'scale',type: 'orange',
        buttons: {        
            formSubmit: {/*menu save atau simpan*/
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="norm"]').val()==''){
                        alert('Pilih Pasien.');
                        return false;
                    }else if($('select[name="idmember"]').val()==''){
                        alert('Pilih Member.');
                        return false;
                    }else if($('niput[name="tanggaldaftar"]').val()==''){
                        alert('Input Tanggal Daftar.');
                        return false;
                    }
                    else{
                        simpan();    
                    }
                }
            },
            formReset:{ /*menu back*/
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        formready(norm);
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
//ketika form diakses
function formready(norm)
{
    if(norm !='')
    {
        startLoading();
        var split = norm.split(',');
        $.ajax({ 
            url:  base_url+'cadmission/formreadypersonmember',type:"POST",dataType:"JSON",data:{id:split[0],norm:split[1]},
            success: function(result){
                stopLoading();
                $('#norm').html('<option value="'+result.norm+'">'+result.pasien+'</option>');
                $('#idmember').html('<option value="'+result.idmember+'">'+result.member+'</option>');
                $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",result.tanggaldaftar);
                $('#masamember').val(result.hariberlaku +' Hari');
                $('input[name="mode"]').val('Ubah Member');
            },
            error: function(result){stopLoading();fungsiPesanGagal();return false;}
        });
    }
    ((norm=='') ? $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate","now") : '' );
    select2serachmulti($('#norm'),'cmasterdata/caripasien');//cari pasien
    select2serachmulti($('#idmember'),'cmasterdata/fillcarimember');//cari member
    
    
}

//aksi simpan member
function simpan()
{
    $.ajax({ 
        url:  base_url+'cadmission/save_personmember',type:"POST",dataType:"JSON",data:$('#formmember').serialize(),
        success: function(r){
            notif(r.status,r.message);
            reloaddata();
        },
        error: function(result){fungsiPesanGagal();return false;}
    });
}


//set hari berlaku saat member dipilih
$(document).on('change','#idmember',function(){
    var hari = $(this).val().split(',')[1]; // split value -- ambil hari berlaku
    $('#masamember').val( hari+' Hari ');
});