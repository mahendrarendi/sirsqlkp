function simpan_icd() //fungsi simpan icd
{
  if($('input[name="kodeicd"]').val()==='') //jika kode icd kosong
  {
    $('input[name="kodeicd"]').focus();
    alert_empty('kode icd');
    return false;
  }
  else if(panjangkodeicd($('#inputkodeicd').val()))
  {
      return false;
  }
  else if(inputkodeicd($('#inputkodeicd').val()))
  {
      return false;
  }
  else if($('input[name="namaicd"]').val()==='') //jika nama icd kosong
  {
    $('input[name="namaicd"]').focus();
    alert_empty('nama icd');
    return false;
  }
  else
  {
    $('#FormICD').submit(); //submit form icd
  }
}
function cari_berdasarkanjenisicd(value)
{
  window.location.href= base_url + 'cmasterdata/icd_search_byjenisicd/'+value;
}

$(document).on('keyup','#inputkodeicd',function(){
   var dataicd=$('#inputkodeicd').val();
   panjangkodeicd(dataicd);
   inputkodeicd(dataicd);
});
function panjangkodeicd(dataicd)
{
    if(dataicd.length > 6)
    {
        $('#maxicd').text('Panjang Kode ICD Tidak Boleh Melebihi 6 karakter.');
    }
    else
    {
        $('#maxicd').text('');
    }
}
function inputkodeicd(dataicd)
{
    $.ajax({ 
        url: base_url+'cmasterdata/inputkodeicd',
        type : "post",      
        dataType : "json",
        data : { icd:dataicd},
        success: function(result) {
            if(result!=null)
            {
                $('#uniqicd').text('ICD '+result['icd']+' Sudah Ada. Mohon gunakan kode icd lain');
            }
            else
            {
                $('#uniqicd').text('');
            }
        },
        error: function(result){                    
            notif(result.status, result.message);
            return false;
        }

    }); 
}

var table;
$(function(){
  //datatables
  $('.select2').select2();
});