// ************ LIST LOCAL STORAGE
// idinap
// idbangsal
// pemeriksaanranap_tgl1
// pemeriksaanranap_tgl2
// 
var table, jumlahicdtindakan = 0, val_rencanabarangobatbhp = [], dtranap='';
$(function(){ 
    //jika browser tidak support webstorage
    if(typeof(Storage)==undefined){pesanUndefinedLocalStorage();}
    getdatevalue();
    dt_pemeriksaanranap();
    loadbelumdiplot();
});

$(document).on('click','#tampilpemeriksaanranap',function(){
   
   setdatevalue($('#tanggalawal').val(),$('#tanggalakhir').val());
   loadbelumdiplot();
   dtranap.ajax.reload(); 
});

$(document).on('click','#reload',function(){
   $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
   $('input[type="search"]').val('').keyup();
   dtranap.state.clear();
   localStorage.setItem("idbangsal",'');
   localStorage.setItem("idinap",'');
   setdatevalue($('#tanggalawal').val(),$('#tanggalakhir').val());
   get_databangsal('#tampilbangsal','');
   tampilDataRawatinap($('select[name="id_inap"]'), '');
   dtranap.ajax.reload(); 
});

$(document).on('click','#tampilbangsal',function(){
    localStorage.setItem("idbangsal",this.value);
    setdatevalue($('#tanggalawal').val(),$('#tanggalakhir').val());
    loadbelumdiplot();
    dtranap.ajax.reload();
});

$(document).on('click','#statusperawatan',function(){
    loadbelumdiplot();
    dtranap.ajax.reload();
});

function setdatevalue(tgl1,tgl2)
{
    localStorage.setItem("pemeriksaanranap_tgl1",tgl1);
    localStorage.setItem("pemeriksaanranap_tgl2",tgl2);
    
}

function getdatevalue()
{
    var tgl1 = localStorage.getItem("pemeriksaanranap_tgl1");
    var tgl2 = localStorage.getItem("pemeriksaanranap_tgl2");
    $('#tanggalawal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", ((tgl1==null) ? 'now' : tgl1 ) );
    $('#tanggalakhir').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",((tgl2==null) ? 'now' : tgl2 ) );
}

function caribypasien(value)
{
     localStorage.setItem("idinap",value);
     loadbelumdiplot();
     dtranap.ajax.reload();
}

function dt_pemeriksaanranap()
{
    var idbangsal = localStorage.getItem('idbangsal'); 
    var idinap = localStorage.getItem('idinap'); 
    
    dtranap = $('#dtranap').DataTable({
        "processing": true,
        "serverSide": true,
        "stateSave": true,
        "order": [],
        "ajax": {
            "data":{
                idbangsal:function(){ return localStorage.getItem('idbangsal')},
                idinap : function(){ return localStorage.getItem('idinap')},
                tanggalawal:function(){ return $('#tanggalawal').val()},
                tanggalakhir:function(){ return $('#tanggalakhir').val()},
                status:function(){ return $('#statusperawatan').val()}
            },
            "url": base_url+'cpelayananranap/dt_pemeriksaanranap',
            "type": "POST"
        },
        "columnDefs": [{
                "targets": [1,2,3,5,8,9],
                "orderable": false,
            },],
        "fnCreatedRow": function (nRow, aData, iDataIndex) { 
           // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
//          $(nRow).attr('id', 'row' + iDataIndex);
           $(nRow).attr('style', statuspemeriksaanranap(aData[7]));
        },
        "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
            $('[data-toggle="tooltip"]').tooltip();
        },
    });
    
    get_databangsal('#tampilbangsal',idbangsal);
    tampilDataRawatinap($('select[name="id_inap"]'), idinap);
}

function statuspemeriksaanranap(status)
{
    var warna = {batal:'background-color:#f0D8D8;',terlaksana:'background-color:#e0f0d8;'} ; 
    return  warna[status];
}

//--- basit, clear
function tambah_rencana_perawatan()
{
    jumlahicdtindakan = 0;
    jumlahrencanaperawatan = 0 ;
    var modalTitle = 'Tambah Rencana Perawatan';
    var modalContent = '<form action="" id="FormRencanaPerawatan">' +
                            '<div class="form-group">' +
                                '<div class="col-sm-8">' +
                                '<label>Pasien Rawat Inap</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select>'+
                                '</div>' +
                                '<div class="col-sm-4">' +
                                '<label>Dokter DPJP (Opsional)</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idpegawaidokter">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>' +
                            '<div class="form-group">' +
                                '<div class="col-sm-12">' +
                                '<label>Waktu Perawatan  (Tahun-Bulan-Hari Jam)</label>' +
                                '<input type="text" name="tgl" class="datetimepicker form-control"/>'+
                                '</div>' +
                            '</div>' +
                            '<div class="col-sm-12">&nbsp;</div>' +
                            '<div class="form-group col-sm-12">'+
                                '<a id="tambahrencanaperawatan" class=" btn btn-warning btn-sm"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan</a>'+
                                '<table id="rencanaperawatan" width="100%" class="table table-hover table-bordered table-striped">'+
                                '<tr style="border-top:3px solid #f39c10;"><td style="padding: 14px 0px 10px 0px;">'+
                                '<div class="form-group">'+
                                
                                '<div class="col-sm-5"><label>ICD Rencana Perawatan</label><select class="selectDataICD form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'"  name="icd[]"><option value="0">Pilih ICD Perawatan</option></select></div>'+
                                '<div class="col-sm-2"><label>Jumlah Tindakan</label><input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value=""><small class="text text-red">#jumlah setiap kali perawatan</small></div>'+
                                '<div class="col-sm-3"><label>Jumlah Per Hari</label><input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value=""><small class="text text-red">#jumlah tindakan / hari. Ex: 1 hari 3 x, maka diisi 3</small></div> '+
                                '<div class="col-sm-2"><label>Selama (Hari)</label><input type="text" class="form-control" style="width:100%;" name="selama[]" value=""><small class="text text-red">#berapa hari tindakan dilakukan</small></div>'+
                                
                                '</div>'+
                                '<div class="col-sm-12">&nbsp;</div>' +
                                '<div class="form-group">'+
                                '<div id="rencanabarangbarang'+jumlahrencanaperawatan+'" class="col-sm-2"><a id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Tambah Rencana BHP/Obat"><i class="fa fa-plus-circle"></i> Tambah Rencana BHP/Obat</a></div> '+
                                '<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label></div> '+
                                '<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pesan</label></div> '+
                                '<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label></div>'+
                                '<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pakai (Per Tindakan)</label></div>'+
                                '</div>'+
                                '</td></tr></table>'+
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        closeIcon: true,
        content: modalContent,
        columnClass: 'lg',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'buat rencana keperawatan',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('input[name="tgl"]').val() == '')
                    {
                        alert_empty('tgl');
                        return false;
                    }
                    else if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else if ($('select[name="icd"]').val() == 0)
                    {
                        alert_empty('icd');
                        return false;
                    }
                    else if ($('input[name="jumlahperhari"]').val() == '')
                    {
                        alert_empty('jumlahperhari');
                        return false;
                    }
                    else if ($('input[name="selama"]').val() == '')
                    {
                        alert_empty('selama');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("idinapselected",$('select[name="idinap"]').val());
                        localStorage.setItem("jumlahperhari",$('input[name="jumlahperhari"]').val());
                        localStorage.setItem("jumlahtindakan",$('input[name="jumlahtindakan"]').val());
                        localStorage.setItem("selama",$('input[name="selama"]').val());
                        simpanrencana();
                    }
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        var dateNow = new Date();
        $('.datetimepicker').datetimepicker({format:"YYYY-MM-DD HH", sideBySide: true,defaultDate:dateNow}); //Initialize Date Time picker //bisa ditambah option inline: true
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinap')); //ambil data inap
        tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5"); //ambil data icd
        tampilDataDokter($('select[name="idpegawaidokter"]')); //ambil data icd
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//basit, clear
function simpanrencana()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/simpan_rencana',
        //data: {t:$('input[name="tgl"]').val(), in:$('select[name="idinap"]').val(), ic:$('select[name="icd"]').val(), ipd:$('select[name="idpegawaidokter"]').val(), td:$('input[name="jumlahtindakan"]').val(), j:$('input[name="jumlahperhari"]').val(), s:$('input[name="selama"]').val()}, //
        data: $("#FormRencanaPerawatan").serialize(),
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            notif(result.status,result.message);
            setTimeout(function(){ window.location.reload(true);}, 1000);
           
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}


//--- mahmud, clear
function pemberian_bhp()
{
    var modalTitle = 'Pemberian Obat/BHP';
    var modalContent = '<form action="" id="FormRencanaBHP">' +
                            '<div class="form-group">' +
                                '<label>Pasien Rawat Inap</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idinap">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'detail pemberian bhp/obat',
                btnClass: 'btn-blue',
                action: function () {
                    if ($('select[name="idinap"]').val() == 0)
                    {
                        alert_empty('idinap');
                        return false;
                    }
                    else
                    {
                        localStorage.setItem("idinap",$('select[name="idinap"]').val());
                        localStorage.removeItem("idrencanamedispemeriksaan");
                        localStorage.setItem("idinap", $('select[name="idinap"]').val());
                        localStorage.removeItem("norm");
                        localStorage.removeItem("idpendaftaranranap");
                        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp';   
                    }
                }
            },
            //menu back
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        tampilDataRawatinap($('select[name="idinap"]'), localStorage.getItem('idinap')); //ambil data inap
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}


//basit, clear
function loadbelumdiplot()
{
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url + 'cpelayananranap/belumdiplot',
        dataType: "JSON", //tipe data yang dikirim
        data:{idbangsal:localStorage.getItem('idbangsal')},
        success: function(result) { //jika  berhasil
            if (!is_null(result))
            {
                var belumdiplot = '',no=0;
                for (i in result)
                {
                    belumdiplot += '<tr><td>'+ ++no +'</td><td colspan="3">'+result[i].belumdiplot+'</td></tr>';
                }
                $('#pasienbelumdiplot').html('<table class="table table-striped"><thead><tr><th style="width: 10px">#</th><th colspan="3">Pasien Belum Ada Rencana Keperawatan</th></tr></thead><tbody>'+belumdiplot+'</tbody></table>');
            }
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}
/////////////////END CARI PASIEN////////////////

//---------- basit, clear
$(document).on("click","#pemeriksaanranapTerlaksana", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('terlaksana','Pemeriksaan Terlaksana?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatal", function(){ //ubah status batal
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
    fungsi_pemeriksaanranapUbahStatus('batal','Batal Pemeriksaan?',id,nobaris);
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBatalterlaksana", function(){ //ubah status tunda
    var id = $(this).attr("alt2");
    var nobaris = $(this).attr("nobaris");
 fungsi_pemeriksaanranapUbahStatus('rencana','Batal Terlaksana?',id,nobaris);
});

//--
$(document).on("click","#tambahrencanabarang", function(){
    var jumlahrencana = $(this).attr("jumlahrencana");
    jumlahicdtindakan++;
    $('#rencanabarangobatbhp'+jumlahrencana).append('<select class="select2 form-control" style="width:100%;" id="rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangobatbhp'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select>');
    $('#rencanabarangjumlahpesan'+jumlahrencana).append('<input type="text" class="form-control" style="width:100%;" id="rencanabarangjumlahpesan'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpesan'+jumlahrencana+'['+jumlahicdtindakan+']" />');
    $('#rencanabarangsatuanpakai'+jumlahrencana).append('<select class="select2 form-control" style="width:100%;" id="rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangsatuanpakai'+jumlahrencana+'['+jumlahicdtindakan+']"><option value="0">Pilih</option></select>');
    $('#rencanabarangjumlahpakai'+jumlahrencana).append('<input type="text" class="form-control" style="width:100%;" id="rencanabarangjumlahpakai'+jumlahrencana+jumlahicdtindakan+'" name="rencanabarangjumlahpakai'+jumlahrencana+'['+jumlahicdtindakan+']" />');
    isicombobhp('rencanabarangobatbhp'+jumlahrencana+jumlahicdtindakan);
    isicombosatuan('rencanabarangsatuanpakai'+jumlahrencana+jumlahicdtindakan+'', '');
});

$(document).on("click","#tambahrencanaperawatan", function(){
   jumlahrencanaperawatan++;
   var rencana='<tr style="border-top:3px solid #f39c10;"><td style="padding: 14px 0px 10px 0px;">'+
               '<div class="form-group">'+
               '<div class="col-sm-5"><label>ICD Rencana Perawatan</label><select class="selectDataICD form-control" style="width:100%;" id="icd'+jumlahrencanaperawatan+'" name="icd[]"><option value="0">Pilih ICD Perawatan</option></select></div>'+
               '<div class="col-sm-2"><label>Jumlah Tindakan</label><input type="text" class="form-control" style="width:100%;" name="jumlahtindakan[]" value=""><small class="text text-red">#jumlah setiap kali perawatan</small></div>'+
               '<div class="col-sm-3"><label>Jumlah Per Hari</label><input type="text" class="form-control" style="width:100%;" name="jumlahperhari[]" value=""><small class="text text-red">#jumlah tindakan / hari. Ex: 1 hari 3 x, maka diisi 3</small></div>'+
               '<div class="col-sm-2"><label>Selama (Hari)</label><input type="text" class="form-control" style="width:100%;" name="selama[]" value=""><small class="text text-red">#berapa hari tindakan dilakukan</small></div>'+
               '</div>'+
               '<div class="col-sm-12">&nbsp;</div>' +
               '<div class="form-group">'+
               '<div id="rencanabarangbarang'+jumlahrencanaperawatan+'" class="col-sm-2"><a id="tambahrencanabarang" jumlahrencana="'+jumlahrencanaperawatan+'" class=" btn btn-warning btn-xs" data-toggle="tooltip" data-original-title="Tambah Rencana BHP/Obat"><i class="fa fa-plus-circle"></i> Tambah Rencana BHP/Obat</a></div> '+
               '<div id="rencanabarangobatbhp'+jumlahrencanaperawatan+'" class="col-sm-4"><label>Obat/BHP</label></div> '+
               '<div id="rencanabarangjumlahpesan'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pesan</label></div> '+
               '<div id="rencanabarangsatuanpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Satuan Pakai</label></div>'+
               '<div id="rencanabarangjumlahpakai'+jumlahrencanaperawatan+'" class="col-sm-2"><label>Jmlh Pakai (Per Tindakan)</label></div>'+
               '</div></td></tr>';
   $('#rencanaperawatan').append(rencana);
  tampilDataIcd($('#icd'+jumlahrencanaperawatan), "1,3,4,5"); //ambil data icd
});

function isicombobhp(id)//cari bhp
{
    $('select[id="'+id+'"]').select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanranap_caribhp",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
        stopLoading();
            return {
                q: params.term,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            // var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return { results: data }
            
        },              
    }
    });
}

//basit, clear
function isicombosatuan(id, selected)
{
    if (is_empty(localStorage.getItem('dataSatuan')))
    {
        $.ajax({ 
            url: '../cmasterdata/get_satuan',
            type : "post",      
            dataType : "json",
            success: function(result) {                                                                   
                localStorage.setItem('dataSatuan', JSON.stringify(result));
                fillcombosatuan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        //ini memang begini karena ada bugs, dulunya hanya:
        //fillcombosatuan(id, selected);
        //tapi entah kenapa tidak ngisi select
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillcombosatuan(id, selected);
            },
            error: function(result){
                fillcombosatuan(id, selected);
            }
        });
    }
}

//basit, clear
function fillcombosatuan(id, selected)
{
    var datasatuan = JSON.parse(localStorage.getItem('dataSatuan'));
    var i=0;
    for (i in datasatuan)
    {
      $("#"+id).append('<option ' + ((datasatuan[i].idsatuan===selected)?'selected':'') + ' value="'+datasatuan[i].idsatuan+'">'+datasatuan[i].namasatuan+'</option>');
    }
    $("#"+id).select2();
}

//---------- basit, clear
function fungsi_pemeriksaanranapUbahStatus(status,content,id,nobaris) //fungsi ubah status
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: content,
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'pemeriksaanranap_ubahstatus',
                    type : "post",      
                    dataType : "json",
                    data : {i:id, status:status},
                    success: function(result) {
                       
                        
                        //jika status menunggu
                        if(result.status=='menunggu')
                        {
                             notif('danger','Ubah Rencana Menjadi Terlaksana atau Batal');
                            detailrencanabelumterlaksana(result.rencana);
                            return true;
                        }
                        
                         notif(result.status, result.message);
                        
                        dtranap.ajax.reload(null,false);
                        //jika status success
                        if(result.status=='success')
                        {
                            $('#status'+nobaris).empty();
                            $('#status'+nobaris).text(status);
                        }
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}

function detailrencanabelumterlaksana(rencana)
{
    var list_rencana='';
    for(var x in rencana)
    {
        list_rencana += '<tr><td>'+rencana[x].icd+'</td><td>'+rencana[x].statuspelaksanaan+'</td></tr>';
    }
    
    var modalTitle = 'Rencana Perawatan Belum Terlaksana';
    var modalContent = '<form action="">' +
                          '<table class="table table-striped">\n\
                            <tr class="bg bg-yellow-gradient"><td>Rencana Perawatan</td><td>Status Perawatan</td></tr>\n\
                            '+list_rencana+'\n\
                            </table> \n\
                            <small class="text text-red">#ubah status rencana perawatan menjadi terlaksana atau batal untuk dapat menyelesaikan perencanaan.</text>'+
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        }
    });
}

$(document).on("click","#pemeriksaanranapRujuk", function(){ 
    var id = $(this).attr("alt");
    var tanggalJadwal = $(this).attr("alt2");
    var modalTitle = 'Rujuk Pemeriksaan Pasien';
    var modalContent = '<form action="" id="FormRujuk">' +
                            '<input type="hidden" value="'+id+'" name="idperiksa">'+
                            '<div class="form-group">' +
                                '<label>Poliklinik</label>' +
                                '<select class="select2 form-control" style="width:100%;" name="idjadwal">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Status</label>' +
                                '<select class="form-control" style="width:100%;" name="status">'+
                                '<option value="0">Pilih</option>'+
                                '<option value="rujuk internal kembali">Rujuk Internal Kembali</option>'+
                                '<option value="rujuk internal tidak kembali">Rujuk Internal Tidak Kembali</option>'+
                                '<option value="rujuk eksternal">Rujuk Internal Eksternal</option>'+
                                '</select>' +
                            '</div>'+
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Rujuk',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="poliklinik"]').val()==='0')
                    {
                            alert_empty('poliklinik');
                            return false;
                    }
                    else if($('select[name="status"]').val()==='0')
                    {
                            alert_empty('status');
                            return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: 'pemeriksaanranap_saverujuk', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormRujuk").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                qlReloadPage(600); //reload page
                            },
                            error: function(result) { //jika error
                                fungsiPesanGagal(); // console.log(result.responseText);
                                return false;
                            }
                        });
                    }
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        tampilDataPoliklinik($('select[name="idjadwal"]'),tanggalJadwal); //ambil data poli
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapPeriksa", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("modepemeriksaanranap", 'pemeriksaanranap');
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        localStorage.setItem("idkelas", $(this).attr("alt3"));
        localStorage.setItem("idkelasjaminan", $(this).attr("alt5"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_periksa';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapBHP", function(){ //pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt4"));
        localStorage.setItem("idinap", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        localStorage.setItem("idkelas", $(this).attr("alt3"));
        localStorage.setItem("idkelasjaminan", $(this).attr("alt5"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- basit, clear
$(document).on("click","#pemeriksaanranapView", function(){ //view pemeriksaan ranap
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.removeItem("idinap");
        localStorage.setItem('idinap',$(this).attr("nomori"));
        localStorage.setItem("idrencanamedispemeriksaan", $(this).attr("alt"));
        localStorage.setItem("norm", $(this).attr("norm"));
        localStorage.setItem("idpendaftaranranap", $(this).attr("alt2"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_view';
    } else {
         pesanUndefinedLocalStorage();
    }
});
