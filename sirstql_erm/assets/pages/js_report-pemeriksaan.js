function cetak_hasillab(idpemeriksaan='',idpendaftaran='',idunit='')//download hasil laboratorium
{
    var modalTitle = 'Cetak Hasil Laboratorium';
    var modalContent = '<form action="" id="FormPrintLab"><div class="form-group border-red"><label class="form-label">Paket Laboratorium</label><select class="form-control" name="paketlab"></select></div></form>';
    $.confirm({ /*aksi ketika di klik menu*/
        title: modalTitle,closeIcon: true,closeIconClass: 'fa fa-close',content: modalContent,columnClass: 'large',
        buttons: {
            formSubmit: {
                text: 'Cetak',
                btnClass: 'btn-blue',
                action: function () {
                        print_laboratorium(idpemeriksaan,idpendaftaran,idunit,$('select[name="paketlab"]').val());
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-warning',
                action: function(){
                    // location.reload();
                }
            }
        },
        onContentReady: function () { //ketika form di tampilkan
            $.ajax({
                type: "POST",url: base_url+'cmasterdata/getpaketlab',dataType: "JSON",
                success: function(result) { //jika  berhasil
                    var dt='<option value="0">Paket Laboratorium</option><option value="00">Bukan Paket</option>';
                    for(var x in result){dt+='<option value="'+result[x].idpaketpemeriksaan+'">'+result[x].namapaketpemeriksaan+'</option>';}
                    $('select[name="paketlab"]').html(dt);
                },
                error: function(result) { //jika error
                    fungsiPesanGagal(); return false;
                }
            });
        // tambah menu unduh
        $('.jconfirm-buttons').after('<a onclick="unduhfilepdfhasillaboratorium('+idpemeriksaan+','+idpendaftaran+','+idunit+')" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Unduh</a> <a onclick="cetakhasilkepdf('+idpemeriksaan+','+idpendaftaran+','+idunit+')" class="btn btn-primary"><i class="fa fa-print"></i> Cetak.v2 </a>');

        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
    
}
// GENERATE TO PDF
function cetakhasilkepdf(idpemeriksaan,idpendaftaran,idunit)
{
    print_laboratorium(idpemeriksaan,idpendaftaran,idunit,$('select[name="paketlab"]').val(),'pdf');
}


function unduhfilepdfhasillaboratorium(idpemeriksaan,idpendaftaran,idunit)
{
    var dt = idpemeriksaan+'-'+idpendaftaran+'-'+idunit+'-'+$('select[name="paketlab"]').val();
    window.open(base_url+'creport/download_pdflaborat/'+dt);

}


function print_laboratorium(idpemeriksaan,idpendaftaran,idunit,paketlab,modecatek='')
{
    var header='', style='', i=0, printData='';
    var idpendaftaran = ((idpendaftaran=='')?$('input[name="idpendaftaran"]').val():idpendaftaran) , idpemeriksaan = ((idpemeriksaan=='')?$('input[name="idperiksa"]').val():idpemeriksaan) , idunit = ((idunit=='')? $('input[name="idunit"]').val() : idunit); 
    $.ajax({
        type:"POST",
        url:base_url+"creport/laboratorium_hasilpemeriksaan",
        data:{x:idpendaftaran,y:idunit,z:idpemeriksaan,p:paketlab},
        dataType:"JSON",
        success:function(value){
            var namars = value.namars;
            var identitas = value.identitas; var gruppaket='';
            var waktu = value.waktu;
            var pesan = value.pesan;
//            var jeniscetak = value.jeniscetak;
            var formatcetak = value.formatcetak;
            var keteranganhasil = '';
            var cetakbarcode = ((formatcetak.iscetakbarcode != undefined && formatcetak.iscetakbarcode==1 ) ? '1' : '0' );
            var icdisibarcode = '';
//            var ttdokter = value.ttdokter;
            var value = value.hasillab;
            var pagebreak = ((modecatek=='') ?  'page-break-before: always;margin-top:4.5cm";' : '' );
            header += ((modecatek=='') ? '' : '<img style="width:20cm;" src="'+base_url+'assets/images/headerlaboratorium.jpg" />' );
            header += '<table style="'+pagebreak+'width:100%; text-align:left; font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;">';
            header += '<tr><td>No.Periksa/No.RM </td><td>:</td> <td >'+identitas.idpemeriksaan+'/'+identitas.norm+'</td><td>Dokter</td> <td>:</td><td style="height:auto;width:30%;">'+identitas.dokter+'</td><tr>';
            header += '<tr><td>Nama</td>             <td>:</td> <td style="height:auto;width:40%;">'+identitas.namalengkap+'</td><td>Asal</td>   <td>:</td><td style="height:auto;width:30%;">'+identitas.namaunit+'</td><tr>';
            header += '<tr><td>J.Kelamin</td>          <td>:</td> <td>'+identitas.jeniskelamin+'</td><td>Periksa</td><td>:</td><td>'+((waktu==null) ? '' : waktu.periksa )+'</td><tr>';
            header += '<tr><td>Tgl.Lahir</td>             <td>:</td> <td>'+identitas.tanggallahir+ ((identitas.usia == '') ? '' : ' ( '+identitas.usia+' )') +'</td><td>Selesai</td> <td>:</td><td>'+((waktu==null) ? '' : waktu.selesai )+'</td><tr>';
            header += '<tr><td>Alamat</td>           <td>:</td><td style="height:auto;font-size:12px;" colspan="4">'+identitas.alamat+'</td><tr>';
            header += '</table>';

            header += '<table cellpadding="5" cellspacing="0" style=" width:18cm;font-size:14px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"">';
            // jenis cetak kosong default, selain itu lab hiv
            header += formatcetak.judul;//((jeniscetak==null || jeniscetak=='') ? '' : '<tr><td colspan="5">IMUNOSEROLOGI</td></tr>');
            header += formatcetak.header;//((jeniscetak==null || jeniscetak=='') ? '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">PEMERIKSAAN</td><td style="border:1px solid #000;border-left:0px;">HASIL</td><td style="border:1px solid #000;border-left:0px;min-width:80px;">N DEFAULT</td><td style="border:1px solid #000;border-left:0px;min-width:90px;">N RUJUKAN</td><td style="border:1px solid #000;border-left:0px;min-width:60px;" >SATUAN</td></tr>' : '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">Pemeriksaan</td><td style="border:1px solid #000;border-left:0px;">Hasil</td><td style="border:1px solid #000;border-left:0px;">Nilai Rujukan</td><td style="border:1px solid #000;border-left:0px;">Metode</td><td style="border:1px solid #000;border-left:0px;">Hasil</td></tr>');
            printData += header;
            var printPaket='';
            var  i=0, nilai='', jenisicd='', paketpemeriksaanparent='', paketpemeriksaan='', nilairujukan='', nilaiantigen='';
            for(i in value)
            {   
                nilaiantigen = value[i].nilai ;
                nilairujukan = value[i].nilaiacuanrendah+tandaPenghubung(value[i].nilaiacuantinggi,'-')+value[i].nilaiacuantinggi;
                if(value[i].idjenisicd2=='4')
                {
                    if(paketlab!=0 && paketlab!=00 ){
                        //CETAK HASIL LABORATORIUM SESUAI PAKET YANG DIPILIH
                        printData='';
                        
                        //tampilkan keterangan hasil laborat jika satu grup dan satu paket
                        if(gruppaket!==value[i].gruppaketperiksa && idgrup!= undefined)
                        {
                             printPaket += '<tr ><td colspan="5" style="padding-left:7px;">'+keteranganhasil+'</td></tr>'; 
                             keteranganhasil = '';
                        }
                        
                        if( paketlab==value[i].idpaketpemeriksaan || paketlab == value[i].idpaketpemeriksaanparent)
                        { 
                            if(gruppaket!==value[i].gruppaketperiksa){printPaket+=header;}
                            paketpemeriksaan = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum && gruppaket==value[i].gruppaketperiksa)?'': '<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>');
                            printPaket       = printPaket +paketpemeriksaan;// + ((jeniscetak==null || jeniscetak=='') ?  paketpemeriksaan : '');//jika jenis cetak null  nama paket dicetak
                            if(icd != value[i].icd && value[i].icd!=null)//jika icd sama lewati
                            {
                                
                                if(formatcetak.kode=='AHIV')
                                {
                                    printPaket = printPaket  +'<tr>'+
                                      '<td>'+ value[i].namaicd+'</td>'+
                                      '<td>'+ value[i].nilai +'</td>'+
                                      '<td>'+ nilairujukan +'</td>'+
                                      '<td>'+ value[i].metode +'</td>'+
                                      '<td>'+ value[i].kesimpulanhasil +'</td>'+
                                      '</tr>';
                                }
                                else if(formatcetak.kode=='PCRS')
                                {
                                    printPaket = printPaket  +'<tr>'+
                                      '<td>'+ value[i].namaicd+'</td>'+
                                      '<td>'+ value[i].nilai +'</td>'+
                                      '<td>'+ nilairujukan +'</td>'+                                      
                                      '<td>'+ value[i].satuan +'</td>'+
                                      '<td>'+ value[i].metode +'</td>'+
                                      '</tr>';                                    
                                }
                                else if(formatcetak.kode=='RTPCR')
                                {
                                    printPaket = printPaket  +'<tr>'+
                                      '<td>'+ value[i].namaicd+'</td>'+                                                                        
                                      '<td>'+ value[i].satuan +'</td>'+
                                      '<td>'+ value[i].nilai +'</td>'+                                  
                                      '<td>'+ formatcetak.metode +'</td>'+
                                      '<td>'+ formatcetak.status +'</td>'+
                                      '</tr>';                                    
                                }
                                else
                                {
                                    printPaket = printPaket  +'<tr>'+
                                      '<td>'+value[i].namaicd+'</td>'+
                                      '<td>'+ value[i].nilai +'</td>'+
                                      '<td>'+ ((value[i].nilaidefault==null) ? '' : value[i].nilaidefault  ) +'</td>'+
                                      '<td>'+ nilairujukan +'</td>'+
                                      '<td>'+ value[i].satuan +'</td>'+
                                      '</tr>';
                                }
                                icdisibarcode += value[i].namaicd+' '+value[i].nilai +', ';
                                    
                            }
                                    var icd = value[i].icd;
                                    // var jenisicdsebelum=value[i].jenisicd;
                                    var namapaketpemeriksaansebelum = value[i].namapaketpemeriksaan;
                                    var gruppaket=value[i].gruppaketperiksa;
                        }
                        if(value[i].keteranganhasil !=null && value[i].keteranganhasil != keteranganhasil)
                        {
                            var keteranganhasil = value[i].keteranganhasil;
                        }
                        var idgrup = value[i].idgrup;
                    }
                    else
                    {
                        if(paketlab==='00')
                        {
                            //CETAK HASIL LABORATORIUM - KETIKA JENIS PAKET YANG DIPILIH BUKAN PAKET
                            if(value[i].idpaketpemeriksaan < 1 && value[i].idpaketpemeriksaanparent < 1)
                            {
                                printData = printData  +'<tr>'+
                                            '<td>'+value[i].namaicd+'</td>'+
                                            '<td>'+ value[i].nilai +'</td>'+
                                            '<td>'+((value[i].nilaidefault==null) ? '' : value[i].nilaidefault  )+'<input type="hidden" name="icd[]" value="'+value[i].icd+'"><input type="hidden" name="istext[]" value="'+value[i].istext+'"></td>'+
                                            '<td>'+value[i].nilaiacuanrendah+tandaPenghubung(value[i].nilaiacuantinggi,'-')+value[i].nilaiacuantinggi+'</td>'+
                                            '<td>'+value[i].satuan+'</td>'+
                                            '</tr>';
                            }
                        }
                        else
                        {
                           //CETAK HASIL LABORAT TANPA MEMILIH JENIS PAKET
                           // paketpemeriksaanparent = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum)?'': '<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>');
                           paketpemeriksaan = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum && gruppaket==value[i].gruppaketperiksa)?'': ((value[i].namapaketpemeriksaan==null)?'':'<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>'));
                            printData = printData  + paketpemeriksaanparent + paketpemeriksaan;
                            if(icd != value[i].icd && value[i].icd!=null)//jika icd sama lewati
                            {
                                printData = printData  +'<tr>'+
                                '<td>'+value[i].namaicd+'</td>'+
                                '<td>'+ value[i].nilai +'</td>'+
                                '<td>'+((value[i].nilaidefault==null) ? '' : value[i].nilaidefault  )+'<input type="hidden" name="icd[]" value="'+value[i].icd+'"><input type="hidden" name="istext[]" value="'+value[i].istext+'"></td>'+
                                '<td>'+value[i].nilaiacuanrendah+tandaPenghubung(value[i].nilaiacuantinggi,'-')+value[i].nilaiacuantinggi+'</td>'+
                                '<td>'+value[i].satuan+'</td>'+
                                '</tr>';
                            }
                            var icd = value[i].icd;
                            var namapaketpemeriksaansebelum = value[i].namapaketpemeriksaan;
                            var gruppaket=value[i].gruppaketperiksa;
                        }
                    }
                }
            }
            
            printData += printPaket;
            
            var style = '<style>@page {size:21cm 29.7cm;padding-bottom:0px;margin-bottom:0px;}</style>';
            
            //tampilkan keterangan hasil laborat terakhir
            var isibarcode = namars+'/'+waktu.selesai+'/'+identitas.idpemeriksaan+'/'+identitas.norm+'/'+identitas.namalengkap+'/'+icdisibarcode;
            //generate qrcode
            if(cetakbarcode=='1')
            {
                var code = new QRCode(document.getElementById("qrcode"),{width : 165,height : 165});
                code.makeCode(isibarcode);   
                var qrpasien = '';
                setTimeout(function(){
                    qrpasien += document.getElementById('qrcode').innerHTML;
                    printData += '<tr><td colspan="2" style="padding-left:7px;padding-top:15px;">'+((keteranganhasil==undefined)?'':keteranganhasil)+'</td><td colspan="3" align="center" style="padding-top:25px;">'+qrpasien+'</td></tr> </table> ';
                    fungsi_cetaktopdf(printData,style);
                },100);
                setTimeout(function(){$('#qrcode').empty();},110);
            }
            else
            {
                printData += '<tr><td colspan="5" style="padding-left:7px;">'+((keteranganhasil==undefined)?'':keteranganhasil)+'</td></tr> </table> ';
                fungsi_cetaktopdf(printData,style);
            }
            
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}



// ELEKTRO MEDIK
function cetak_hasilelektromedik(idpendaftaran='',idunit='')//download hasil laboratorium
{
    elektromedik_cetakexpertise(idpendaftaran,idunit);    
}
function elektromedik_cetakexpertise(idpendaftaran,idunit)
{
    var idpendaftaran = ((idpendaftaran=='')?$('input[name="idpendaftaran"]').val():idpendaftaran) , idunit = ((idunit=='')? $('input[name="idunit"]').val() : idunit); 
    $.ajax({type:"POST",url:base_url+"creport/elektromedik_hasilpemeriksaan",data:{x:idpendaftaran,y:idunit},dataType:"JSON",
        success:function(value){
            stopLoading();
            var printData = '<div style="float: none;"><img style="opacity:0.6;filter:alpha(opacity=60);width:9cm" src="'+base_url+'assets/images/'+value.header+'" />\n', style='', i=0;
            var keteranganradiologi=value.identitas.keteranganradiologi+((value.identitas.saranradiologi=='')?'':'<b>Saran:</b>')+value.identitas.saranradiologi;
            var diagnosa='';
            var elektromedik = '';
            for (var x in value.diagnosa) { diagnosa += '<span style="display:block;">'+value.diagnosa[x].icd+' '+value.diagnosa[x].namaicd;+'</span>';}
            for (var y in value.elektromedik) { elektromedik += '<span style="display:block;">'+value.elektromedik[y].icd+' '+value.elektromedik[y].namaicd;+'</span>';}
            var identitas = value.identitas;
            var garis= '<div style="border-top:1px solid #3a3a3a;"></div>';
            var borderlr= 'border-left:1px solid #3a3a3a;border-right:1px solid #3a3a3a;';
            var borderbt= 'border-bottom:1px solid #3a3a3a;border-top:1px solid #3a3a3a;';
            var style1='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", "Palatino";color: #333333;"';
            var style2='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", "Palatino";color: #333333;"';
            // printData +=garis;
            printData += '<table '+style1+'>';
            printData += '<tr><td>N.Periksa/N.RM </td><td>:</td> <td >'+identitas.idpemeriksaan+'/'+identitas.norm+'</td><td>D. Pengirim</td> <td>:</td><td style="height:auto;width:35%;">'+identitas.dokter+'</td><tr>';
            printData += '<tr><td>Nama</td>             <td>:</td> <td style="height:auto;width:35%;">'+ identitas.identitas +' '+identitas.namalengkap+'</td><td>Asal</td>   <td>:</td><td style="height:auto;width:35%;">'+identitas.namaunit+'</td><tr>';
            printData += '<tr><td>J. Kelamin</td>        <td>:</td> <td>'+identitas.jeniskelamin+'</td><td>T. Periksa</td><td>:</td><td>'+identitas.waktu+'</td><tr>';
            printData += '<tr><td>Usia</td>             <td>:</td> <td>'+identitas.usia+'</td><td>Alamat</td> <td>:</td><td style="height:auto;width:35%;">'+identitas.alamat+'</td><tr>';
            printData += '</table>';
            printData +=garis;
            printData += '<table '+style1+'><tr ><th>Diagnosis Klinis</th><th>Pemeriksaan Radiologis</th></tr>';
            printData += '<tr><td style="height:auto;width:50%;">'+ diagnosa  +'</td><td style="height:auto;width:50%;">'+elektromedik+'</td></tr></table>';
            printData +=garis;
            printData += '<table '+style2+'><tr><td style="padding:5px;text-align:justify;">'+keteranganradiologi+'</td></tr></table>';
            printData += '<table style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br><br><br><br><br></td></tr><tr><td>'+value.dokterpenilai+'<br>'+value.sipdokterpenilai+'</td></tr></table>';
            var style = '<style>@page {size:21cm 29.7cm;}</style>';
            fungsi_cetaktopdf(printData,style);
        },
        error:function(result){
            stopLoading(); fungsiPesanGagal(); return false;
        }
    });
}

function cetak_hasilechocardiography(idpendaftaran,idunit)
{
    var x=0, html='', paket='', parent='', subtotal=0, keteranganhasil='';
    $.ajax({
        type:"POST",
        url:base_url+"creport/ekokardiografi_hasilpemeriksaan",
        data:{x:idpendaftaran,y:idunit},
        dataType:"JSON",
        success:function(value){
            stopLoading();
            
            var fullborder = 'border:1px solid #3a3a3a;';
            var result = value.ekokardiografi;
            for(x in result)
            {  
                                        
                    html += '<tr><td style="'+fullborder+'">'+result[x].namaicd +'</td>'+
                    '<td style="'+fullborder+'">'+result[x].nilai +'</td>'+
                    '<td style="'+fullborder+'">'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) + ' ' +result[x].satuan +'</td></tr>'; 
                    
            }
            
            var printData = '<div style="float: none;"><img style="opacity:0.6;filter:alpha(opacity=60);width:9cm" src="'+base_url+'assets/images/'+value.header+'" />\n', style='', i=0;
            var diagnosa  = '';
            var ekokardiografi = '';
            
            for (var x in value.diagnosa)
            { 
                diagnosa += '<span style="display:block;">'+value.diagnosa[x].icd+' '+value.diagnosa[x].namaicd;+' </span>';
            }
//            for (var y in value.ekokardiografi) { ekokardiografi += '<span style="display:block;">'+value.ekokardiografi[y].icd+' '+value.ekokardiografi[y].namaicd;+'</span>';}
            var identitas = value.identitas;
            var borderlr= 'border-left:1px solid #3a3a3a;border-right:1px solid #3a3a3a;';
            var borderbt= 'border-bottom:1px solid #3a3a3a;border-top:1px solid #3a3a3a;';
            var style1='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            var style2='style="width:100%; text-align:left; font-size:medium;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"';
            // printData +=garis;
            printData += '<table style="width="100%">';
            printData += '<tr><td>Name</td>           <td>:</td> <td>'+ identitas.identitas +' '+identitas.namalengkap+'</td><td style="padding-left:70px;">Clinic/Ward</td><td>:</td> <td>'+identitas.namaunit+'</td><tr>';
            printData += '<tr><td>Birthdate/Age </td> <td>:</td> <td>'+identitas.tanggallahir+'/'+identitas.usia+'</td><td style="padding-left:70px;">Date</td><td>:</td> <td>'+identitas.tglperiksa+'</td><tr>';
            printData += '<tr><td>Medical record</td> <td>:</td> <td>'+identitas.norm+'</td><tr>';
            printData += '<tr><td>Diagnosis</td>    <td>:</td> <td>'+diagnosa+'</td><tr>';
            printData += '</table>';
            printData += '<table cellpadding="4" cellspacing="0" '+style1+'><tr><th  style="'+fullborder+'">Measurement</th><th  style="'+fullborder+'">Result</th><th  style="'+fullborder+'">Normal (Adult)</th></tr>'+html+'</table>';
//            printData +=garis;
            printData += '<table '+style2+'><tr><td style="padding:5px;text-align:justify;">'+identitas.keteranganekokardiografi+'</td></tr></table>';
            printData += '<table style="margin-top:10px;margin-left:65%; text-align:center;"><tr><td>Dokter Penilai<br><br><br><br><br></td></tr><tr><td>'+identitas.dokter+'</td></tr></table>';
            var style = '<style>@page {size:21cm 29.7cm;margin:none;}</style>';
            fungsi_cetaktopdf(printData,style);
        },
        error:function(result){
            stopLoading(); fungsiPesanGagal(); return false;
        }
    });
}