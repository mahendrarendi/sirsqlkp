var table;
$(function () {
    setcaritanggal();
    $('.select2').select2(); //Initialize Select2 Elements
    $('.timepicker').timepicker({ showInputs: false});//Timepicker
    table = $('#tableMasterJadwal').DataTable({
      "dom": '<"toolbar">frtip',
      "processing": true, 
      "serverSide": true,
      "stateSave": true, 
      "order": [], 
      "ajax": {
          "data":{cariawal:function(){return $('#cariawal').val();},cariakhir:function(){return $('#cariakhir').val();}},
          "url": "load_datainputjadwal", //-- url
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [3,4,5,6,7,8,9], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },
  });
});

function setcaritanggal(){ $('.caritanggal').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom",startDate:'now',}).datepicker('setDate','now'); }
$(document).on('click','#reload',function(){ setcaritanggal(); table.state.clear(); table.ajax.reload();});
$(document).on('click','#tampil',function(){ table.ajax.reload();});

function hapusJadwal(no, value)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {   
                startLoading();
                $.ajax({
                    type: "POST",
                    url: 'inputjadwal_hapus',
                    data: {i:value},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        $("#hapus"+no).html("--dihapus--");
                    },
                    error: function(result) { //jika error
                        stopLoading();
                        fungsiPesanGagal(); // console.log(result.responseText);
                        return false;
                    }
                });
            },
            cancel: function () {               
            }            
        }
    });
}

function editJadwal(no,id)
{
    startLoading();
    var no = parseInt(no)-1;
    var id = id;
    $.ajax({
        type: "post", //tipe pengiriman data
        url: 'inputjadwal_editdata', //alamat controller yang dituju (di js base url otomatis)
        data:{id:id}, //
        dataType: "json", //tipe data yang dikirim
        success: function(result) {
        stopLoading();
        var modalTitle = 'Ubah Jadwal Dokter';
        var modalContent = '<form action="" id="FormJadwal">' +
                           '<input type="hidden" name="idjadwal" id="idjadwal" value="'+id+'" />'+
                           '<input type="hidden" name="no" id="no" value="'+no+'" />'+
                           '<div class="form-group col-sm-12">'+
                            '<label class="label-control">Pilih Dokter</label>'+
                            '<select style="width:100%;" class="select2 form-control" id="dokter" name="dokter"><option value="0">Pilih</option><select>' +
                           '</div>'+
                           '<div class="form-group col-sm-6">' +
                                '<label>Loket</label>' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" name="loket" id="loket">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            
                            '<div class="form-group col-sm-6">' +
                                '<label>Grup Jadwal</label>' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" name="jadwalgrup" id="jadwalgrup">'+
                                '<option value="0">Tidak Ada Grup</option>'+
                                '</select>' +
                            '</div>' +
                           '<div class="form-group col-sm-12">'+
                            '<label class="label-control">Waktu</label>'+
                            '<input style="margin-bottom:3px;" type="text" value="'+ambiltanggal2(result.tanggal)+'" id="tanggal"  class="datepicker form-control col-sm-6"/> <input type="text" value="'+ambilwaktu(result.tanggal)+'" id="mulai" '+styleInput+'  class="timepicker col-sm-3" size="3"/>'+
                            ' - <input type="text" '+styleInput+' value="'+result.jamakhir+'" id="akhir"  class="timepicker col-sm-3" size="3"/>'+
                           '</div>'+
                           '<div class="form-group col-sm-6">'+
                            '<label class="label-control">Quota</label>'+
                            '<input type="text" value="'+result.quota+'" id="quota" class="form-control" />'+
                           '</div>'+
                           '<div class="form-group col-sm-6">'+
                            '<label class="label-control">Status</label>'+
                            '<select style="width:100%;" class="select2 form-control" id="status" name="status">'+
                                '<option value="0">Pilih</option>'+
                            '<select>'+
                           '</div>'+
                           '<div class="form-group col-sm-12">'+
                            '<label class="label-control">Catatan</label>'+
                            '<textarea id="catatan" class="form-control" rows="1" cols="6" >'+result.catatan+'</textarea>'+
                           '</div>'+
                           '</form>';
        //aksi ketika di klik menu
        $.confirm({
            title: modalTitle,
            content: modalContent,
            columnClass: 'large',
            buttons: {
                //menu save atau simpan
                formSubmit: {
                    text: 'update',
                    btnClass: 'btn-blue',
                    action: function () {
                        var id = $('#idjadwal').val();
                        var no = $('#no').val();
                        var dokter = $('#dokter').val();
                        var loket = $('#loket').val();
                        var jadwalgrup = $('#jadwalgrup').val();
                        var tanggal = $('#tanggal').val();
                        var mulai = $('#mulai').val();
                        var akhir = $('#akhir').val();
                        var status = $('#status').val();
                        var quota = $('#quota').val();
                        var catatan = $('#catatan').val();
                        if(dokter==='0')
                        {
                                alert_empty('dokter');
                                return false;
                        }
                        else if(!loket)
                        {
                                alert_empty('loket');
                                return false;
                        }
                        else if(!tanggal)
                        {
                                alert_empty('tanggal');
                                return false;
                        }
                        else if(!mulai)
                        {
                                alert_empty('jam mulai');
                                return false;
                        }
                        else if(!akhir)
                        {
                                alert_empty('jam akhir');
                                return false;
                        }
                        else if(status==='0')
                        {
                                alert_empty('status');
                                return false;
                        }
                        else if(!quota)
                        {
                                alert_empty('quota');
                                return false;
                        }
                        else
                        {
                            startLoading();
                            $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: 'inputjadwal_update', //alamat controller yang dituju (di js base url otomatis)
                            data: {id:id, dokter:dokter, loket:loket, jadwalgrup:jadwalgrup, tanggal:tanggal, mulai:mulai, akhir:akhir, status:status, quota:quota, catatan:catatan},
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) {
                                stopLoading();
                                console.log(result);
                                notif(result.status, result.message);
                                table.ajax.reload();
                            },
                            //jika error
                            error: function(result) {
                                stopLoading();
                                console.log(result.responseText);
                            }
                        });
                        }
                    }
                },
                //menu back
                formReset:{
                    text: 'Batal',
                    btnClass: 'btn-danger'
                }
            },
            onContentReady: function () {
                fungsi_tampildokter(result.idpegawaidokter);
                fungsi_tampilloket(result.idloket,result.idunit);//panggil fungsi tampil loket
                fungsi_tampiljadwalgrup(result.idjadwalgrup);//panggil fungsi tampil jadwal grup
                tampil_status(result.status);
                $('.select2').select2();
                $('.timepicker').timepicker({ showInputs: true,autoclose: true, showMeridian:false});//Timepicker
                $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}); //Initialize Date picker
                $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
                $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}); //Initialize Date picker
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
            }
        });

    },
        //jika error
        error: function(result) {
            console.log(result.responseText);
        }
    });
}

function generate_jadwal()
{
    var modalSize = 'small';
    var modalTitle = 'Generate Jadwal';
    var modalContent = '<form action="" id="FormGenerateJadwal">' +
                            '<div class="form-group">' +
                                '<label>Tanggal Awal</label>' +
                                '<input type="text" onchange="settglakhir(this.value)" name="tglawal" placeholder="yyyy-mm-dd" autocomplete="off" class="datepicker form-control"/>' +
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>Tanggal Akhir</label>' +
                                '<input type="text" id="tglakhir" readonly name="tglakhir" autocomplete="off" placeholder="yyyy-mm-dd" class="datepicker form-control"/>' +
                            '</div>' +
                            
                            '<div class="form-group">' +
                                '<label>Poli/Unit</label>' +
                                '<select id="idunit" name="idunit" class="form-control"></select>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'generate',
                btnClass: 'btn-blue',
                action: function () {
                    if(!$('input[name="tglawal"]').val())
                    {
                            alert_empty('tanggal awal');
                            return false;
                    }
                    else if(!$('input[name="tglakhir"]').val())
                    {
                            alert_empty('Tanggal Akhir');
                            return false;
                    }
                    else
                    {
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'inputjadwal_generate', //alamat controller yang dituju (di js base url otomatis)
                                data: $("#FormGenerateJadwal").serialize(), //
                                dataType: "JSON", //tipe data yang dikirim
                                //jika  berhasil
                                success: function(result) {
                                    notif(result.status, result.message);
                                    setTimeout(function(){
                                        table.ajax.reload();
                                    },550);
                                },
                                //jika error
                                error: function(result) {
                                    console.log(result.responseText);
                                }
                            });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'close',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        $('input[name="tglawal"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom",startDate:'now',}).datepicker('setDate','now'); //Initialize Date picker
        
        var tampildt='<option value="">Semua Poli/Unit</option>';
        $.ajax({ 
            url: base_url+'cmasterdata/get_datapoli',type : "post", dataType : "json",
            success: function(result) {
                $('#idunit').empty();
                for (var i in result)
                {
                    tampildt +='<option value="'+result[i].idunit+'">'+result[i].namaunit+'</option>';
                }
                $('#idunit').html(tampildt);
            },
            error: function(result){                  
                fungsiPesanGagal();
                return false;
            }
        });
        
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function settglakhir(datenow)
{
    var end = nextdate(31,datenow);
    $('#tglakhir').val(end);
}

function fungsi_tampilpoli(selected) //panggil fungsi tampil poli
{
    var select='';
    $.ajax({
        url:'masterjadwal_caripoli',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="poli"]').empty();
            for(i in result)
            {
                
            select = select + '<option '+if_select(result[i].idunit,selected)+' value="'+ result[i].idunit +'" >' + result[i].namaunit +'</option>';
            }
            $('select[name="poli"]').html('<option value="0">Pilih</option>' + select + '</select>');
        },
        error: function(result){
            console.log(result);
        }

    }); 
}

function fungsi_tampildokter(selected) //panggil fungsi tampil dokter
{
    var select='';
    $.ajax({
        url:'masterjadwal_caridokter',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="dokter"]').empty();
            for(i in result)
            {
            select = select + '<option '+if_select(result[i].idpegawai,selected)+' value="'+ result[i].idpegawai +'" >' + result[i].namalengkap  +'</option>';
            }
            $('select[name="dokter"]').html('<option value="0">Pilih</option>' + select + '</select>');
        },
        error: function(result){
            console.log(result);
        }

    }); 
}
function tampil_status(selected) // fungsi edit data enum (idhtml,dataenum,datayangterpilih)
{
    $('select[name="status"]').empty();
    var select='<option value="0">Pilih</option>';
    var selectoption='';
    $.ajax({
        url:'inputjadwal_tampilstatus',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            for(i in result)
            {
                // console.log(selected);
                select = select + '<option '+if_select(result[i],selected)+' value="'+ result[i] +'" >' + result[i] +'</option>';
            }
            $('select[name="status"]').html(select);
        },
        error: function(result){
            console.log(result);
        }
    }); 
}

//hapus jadwal
$(document).on('click','#hapus',function(){
    var modalSize = 'small';
    var modalTitle = 'Hapus Jadwal';
    var modalContent = '<form action="" id="FormHapusJadwal">' +
                            '<div class="form-group">\n\
                                <p class="bg bg-red" style="padding:3px;">Harap Isi Tanggal dan Unit dengan Benar.</p>\n\
                            </div>' +
                            '<div class="form-group">' +
                                '<label>Tanggal Awal</label>' +
                                '<div class="input-group"> \n\
                                    <input type="text" name="tglawal" placeholder="yyyy-mm-dd" autocomplete="off" class="datepicker form-control tglhapus" readonly/>\n\
                                    <div class="input-group-addon">\n\
                                      <i class="fa fa-calendar"></i>\n\
                                    </div>' +
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>Tanggal Akhir</label>' +
                                '<div class="input-group">\n\
                                    <input type="text" id="tglakhir" readonly name="tglakhir" autocomplete="off" placeholder="yyyy-mm-dd" class="datepicker form-control tglhapus"/>\n\
                                    <div class="input-group-addon">\n\
                                      <i class="fa fa-calendar"></i>\n\
                                    </div>\n\
                                </div>' +
                            '</div>' +
                            
                            '<div class="form-group">' +
                                '<label>Poli/Unit</label>' +
                                '<select id="idunit" name="idunit" class="form-control"></select>' +
                            '</div>' +
                            
                            '<div class="form-group">' +
                                '<label>Dokter</label>' +
                                '<select id="iddokter" name="iddokter" class="form-control"><option value="">Pilih dokter</option></select>' +
                            '</div>' +
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'Hapus',
                btnClass: 'btn-blue',
                action: function () {
                    if(!$('input[name="tglawal"]').val())
                    {
                            alert('Tanggal Awal Harap Diisi');
                            return false;
                    }
                    else if(!$('input[name="tglakhir"]').val())
                    {
                            alert('Tanggal Akharap Diisi.');
                            return false;
                    }
                    else
                    {
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cadmission/hapusjadwal', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormHapusJadwal").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            //jika  berhasil
                            success: function(result) {
                                notif(result.status, result.message);
                                if(result.status == 'success')
                                {
                                    table.ajax.reload();
                                }
                            },
                            //jika error
                            error: function(result) {
                                console.log(result.responseText);
                            }
                        });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'close',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        $('.tglhapus').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom",startDate:'+4d',}).datepicker('setDate','+4d'); //Initialize Date picker
        
        var tampildt='<option value="">Semua Poli/Unit</option>';
        $.ajax({ 
            url: base_url+'cmasterdata/get_datapoli',type : "post", dataType : "json",
            success: function(result) {
                $('#idunit').empty();
                for (var i in result)
                {
                    tampildt +='<option value="'+result[i].idunit+'">'+result[i].namaunit+'</option>';
                }
                $('#idunit').html(tampildt);
            },
            error: function(result){                  
                fungsiPesanGagal();
                return false;
            }
        });
        
        
        select2serachmulti($('#iddokter'),'cmasterdata/fillcaridokter');
        
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});