"use strict";
var panelskdp='';
$(function () 
{ 
    setdate();
    listdata();
})

function listdata(){
    panelskdp = $('#panelskdp').DataTable({"processing": true,"serverSide": true,"lengthChange":false,"searching" : false,"stateSave": true,"order": [],"ajax": {"data":{tanggal:function(){return $('input[name="tanggal"]').val();},cari:function(){return $('input[name="cari"]').val();} },"url": base_url+'cpelayanan/dtskdp',"type": "POST"},"columnDefs": [{ "targets": [ 7,8,9],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) {}, /*$(nRow).attr('style', qlstatuswarna(aData[10]));, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.*/
   "drawCallback": function (settings, json) {},/*Function that is called every time DataTables performs a draw.*/
   });
}
function refresh(){
	setdate(); $('input[type="search"]').val('').keyup();
    panelskdp.state.clear();
    panelskdp.ajax.reload();
}
function cari(){panelskdp.ajax.reload();}
function setdate(){$('input[name="tanggal"]').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate", 'now');}

$(document).on('click','#ubahskdp',function(){
	var nokontr = $(this).attr('no');
	$.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',
            title: 'Ubah No.SKDP',
            content: '<input type="input" class="form-control" name="noskdp" placeholder="input No.SKDP" />',
            buttons: {
            	simpan: function(){$.ajax({ url:  base_url+'cpelayanan/setnoskdp', type:"POST",dataType:"JSON", data:{nokontrol:nokontr,noskdp:$('input[name="noskdp"]').val()},success: function(result){ notif(result.status, result.message); panelskdp.ajax.reload();},error: function(result){ fungsiPesanGagal(); return false;}});},
            	batal: function () {}
            }
        });
});