$(function (){ 
    var mode = $('#modepage_antrol').val();
    
    antrol_referensi_getdata();
});


//get data ref poli
function antrol_referensi_getdata()
{
    var mode = $('#modepage_antrol').val(); //'referensi_poli'; //
    startLoading();
    $.ajax({ 
        url: base_url + 'cbpjs/antrol_getdata',
        type : "post",      
        dataType : "json",
        data : {
            mode:mode                
        },
        success: function(result) {   
            stopLoading();
            if(result.metaData.code == 1)
            {
                if(mode == 'referensi_poli'){
                    list_referensi_poli(result.response);
                }
                else if(mode == 'referensi_dokter'){
                    list_referensi_dokter(result.response);
                }
            }
            else
            {
                //if(mode == 'referensi_poli'){
                //    $('#bodyReferensi').html('<tr><td colspan="8" class="text-center">Data Tidak Ada.</td></tr>');
                //}
                // else if(mode == 'referensi_dokter'){
                //     $('#bodyReferensi').html('<tr><td colspan="8" class="text-center">Data Tidak Ada.</td></tr>');
                // }
                $.alert(result.metaData.code+' '+result.metaData.message);
                $('#bodyReferensi').html('<tr><td colspan="8" class="text-center">Data Tidak Ada.</td></tr>');
            }

        },
        error: function(result){
           stopLoading();
           notif(result.status, result.message);
           return false;
        }

    });
}

//list referensi poli
function list_referensi_poli(result)
{
    var row    = '';
    var no     = 0;
    // data for table >>>
    for(var x in result)
    {
        ++no;
        row += '<tr>\n\
                    <td>'+no+'</td>\n\
                    <td>'+result[x].nmpoli+'</td>\n\
                    <td>'+result[x].nmsubspesialis+'</td>\n\
                    <td>'+result[x].kdsubspesialis+'</td>\n\
                    <td>'+result[x].kdpoli+'</td>\n\
                </tr>';
    }
    $('#bodyReferensi').html(row);
    $('#tblReferensi').dataTable();
    // <<< data for table

    // data for select button >>>
    // $(kodePoliBPJS).empty();
    // var menuSelectdt='';
    // var x ='';
    // for(var i in result)
    // {
    //     menuSelectdt +='<option value="'+result[i].kdpoli+'">'+result[i].nmpoli+'</option>';
    // }
    // $('#kodePoliBPJS').html(menuSelectdt);
    // <<< data for select button
}

//list referensi dokter
function list_referensi_dokter(result)
{
    var row = '';
    var no=0;
    for(var x in result)
    {
        ++no;
        row += '<tr>\n\
                    <td>'+no+'</td>\n\
                    <td>'+result[x].namadokter+'</td>\n\
                    <td>'+result[x].kodedokter+'</td>\n\
                </tr>';
    }
    $('#bodyReferensi').html(row);
    $('#tblReferensi').dataTable();
}
