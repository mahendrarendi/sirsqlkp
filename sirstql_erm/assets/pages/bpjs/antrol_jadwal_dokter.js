$(function (){ 
    //Initialize Date picker
    $('#tanggalJadwalDokter').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom",startDate:"now"}).datepicker("setDate",'now');
    antrol_jadwal_dokter_getdata();
});

//cari jadwal dokter  
$(document).on('click','#cari_jadwal_dokter',function(){
    antrol_jadwal_dokter_getdata();
});


//get data ref poli
function antrol_jadwal_dokter_getdata()
{  
    var kodepoli = $('#kodePoliBPJS').val();
    var tanggal = $('#tanggalJadwalDokter').val();
    startLoading();
    $.ajax({ 
        url: base_url + 'cbpjs/antrol_getdata',
        type : "post",      
        dataType : "json",
        data : {
            mode:"referensi_jadwal_dokter",
            kodepoli:kodepoli,
            tanggal:tanggal                 
        },
        success: function(result) {
            stopLoading();
            if(result.metaData.code == 200)
            {
                list_referensi_jadwal_dokter(result.response);
            }
            else
            {
                $('#bodyReferensiJadwalDokter').html('<tr><td colspan="8" class="text-center">Data Tidak Ada.</td></tr>');
                $.alert(result.metaData.code+' '+result.metaData.message);
            }

        },
        error: function(result){
           stopLoading();
           notif(result.status, result.message);
           return false;
        }

    });
}


//list referensi dokter
function list_referensi_jadwal_dokter(result)
{
    var row = '';
    var no=0;
    // data for table >>>
    for(var x in result) 
    {
        ++no;
        row += '<tr>\n\
                    <td>'+no+'</td>\n\
                    <td>'+result[x].kodepoli+' - '+result[x].namapoli+'</td>\n\
                    <td>'+result[x].kodesubspesialis+' - '+ result[x].namasubspesialis +'</td>\n\
                    <td>'+result[x].kodedokter+' - '+result[x].namadokter+'</td>\n\
                    <td><a id="btnjadwal" kodepoli="'+result[x].kodepoli+'" kodesubspesialis="'+result[x].kodesubspesialis+'" kodedokter="'+result[x].kodedokter+'" hari="'+result[x].hari+'" jadwal="'+result[x].jadwal+'" dokter="'+result[x].namadokter+'" poli="'+result[x].namapoli+'" subspesialis="'+result[x].namasubspesialis+'" data-toggle="modal" data-target="#modal-jadwal" class="btn btn-sm btn-warning" '+tooltip('Ubah Jadwal')+'>'+harinasional(result[x].hari)+', '+result[x].jadwal+' WIB <i class="fa fa-edit"></i></a></td>\n\
                    <td>'+result[x].kapasitaspasien+'</td>\n\
                    <td>'+((result[x].libur == 0) ? '-' : 'Libur' )+'</td>\n\
                </tr>';
    }
    $('#bodyReferensiJadwalDokter').html(row);
    $('#tblReferensiJadwalDokter').dataTable();
    $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
    // <<< data for table
}


//form update jadwal -- saat menu ubah jadwal di klik
$(document).on('click','#btnjadwal',function(){
    var kodepoli     = $(this).attr('kodepoli');
    var kodesubpoli = $(this).attr('kodesubspesialis');
    var kodedokter   = $(this).attr('kodedokter');
    var hari  = $(this).attr('hari');
    var jadwal= $(this).attr('jadwal');

    $('#kodedokter').val(kodedokter);
    $('#kodepoli').val(kodepoli);
    $('#kodesubpoli').val(kodesubpoli);
    $('#hari').val(hari);
    $('#jadwal').val(jadwal);
    var waktuJadwal = jadwal.split('-');
    $('#mulai').val(waktuJadwal[0]);
    $('#selesai').val(waktuJadwal[1]);

    var poli = $(this).attr('poli');
    var subspesialis = $(this).attr('subspesialis');
    var dokter = $(this).attr('dokter');
    $('#txtPoli').val(poli);
    $('#txtSubspesialis').val(subspesialis);
    $('#txtDokter').val(dokter);

    $('.timepicker').timepicker({ showInputs: true,autoclose: true, showMeridian:false});//Timepicker
});

//  update jadwal -- saat menu simpan di klik
$(document).on('click','#btnSimpanJadwal',function(){
    var data = $('#formJadwal').serialize();
    startLoading();
    $.ajax({ 
        url: base_url + 'cbpjs/antrol_jadwal_dokter_update',
        type : "post",      
        dataType : "json",
        data : data,
        success: function(result) {
            stopLoading();
            if(result.metaData.code == 200)
            {
                $('#modal-jadwal').modal('hide');
                notif('success', result.metaData.message);
            }
            else
            {
                $.alert(result.metaData.code+' '+result.metaData.message);
            }

        },
        error: function(result){
           stopLoading();
           notif(result.status, result.message);
           return false;
        }

    });
})