"use strict";
var listdata = "";
function listdatatable() {
    listdata = $("#dttable").DataTable({
        processing: !0,
        serverSide: !0,
        lengthChange: !1,
        searching: !0,
        stateSave: !0,
        order: [],
        ajax: { data: {}, url: base_url + "cmasterdata/dt_paketpemeriksaan", type: "POST" },
        columnDefs: [{ targets: [5], orderable: !1 }],
        fnCreatedRow: function (a, e, t) {},
        drawCallback: function (a, e) {
            $('[data-toggle="tooltip"]').tooltip();
        },
    });
}
function menuSimpanPaket() {
    "" === $('input[name="namapaketpemeriksaan"]').val()
        ? ($('input[name="namapaketpemeriksaan"]').focus(), alert_empty("nama paketpemeriksaan"))
        : "" === $('select[name="idjenisicd"]').val()
        ? alert_empty("Jenis Icd")
        : "" === $('select[name="idkelas"]').val()
        ? alert_empty("Kelas Tarif")
        : "" === $('select[name="idjenistarif"]').val()
        ? alert_empty("Jenis Tarif")
        : "" === $('input[name="jasaoperator"]').val()
        ? ($('input[name="jasaoperator"]').focus(), alert_empty("Jasa Operator"))
        : "" === $('input[name="nakes"]').val()
        ? ($('input[name="nakes"]').focus(), alert_empty("Nakes"))
        : "" === $('input[name="jasars"]').val()
        ? ($('input[name="jasars"]').focus(), alert_empty("Jasa RS"))
        : "" === $('input[name="bhp"]').val()
        ? ($('input[name="bhp"]').focus(), alert_empty("Bhp"))
        : "" === $('input[name="akomodasi"]').val()
        ? ($('input[name="akomodasi"]').focus(), alert_empty("Akomodasi"))
        : "" === $('input[name="margin"]').val()
        ? ($('input[name="margin"]').focus(), alert_empty("Margin"))
        : "" === $('input[name="sewa"]').val()
        ? ($('input[name="sewa"]').focus(), alert_empty("Sewa"))
        : $.confirm({
              icon: "fa fa-info",
              theme: "modern",
              closeIcon: !0,
              animation: "scale",
              type: "orange",
              title: "Informasi",
              content: "Yakin akan menyimpan paket pemeriksaan? ",
              buttons: {
                  simpan: function () {
                      submitFormPaketPemeriksaan();
                  },
                  batal: function () {},
              },
          });
}
function submitFormPaketPemeriksaan() {
    startLoading(),
        $.ajax({
            type: "post",
            url: base_url + "cmasterdata/save_paketpemeriksaan",
            dataType: "json",
            data: $("#FormPaketPemeriksaan").serialize(),
            success: function (a) {
                stopLoading(),
                    notif(a.status, a.message),
                    "success" == a.status &&
                        setTimeout(function () {
                            window.location.href = base_url + "cmasterdata/paketpemeriksaan";
                        }, 1300);
            },
            error: function (a) {
                stopLoading(), fungsiPesanGagal();
            },
        });
}
function formready() {
    startLoading(),
        $.ajax({
            type: "post",
            url: base_url + "cmasterdata/formready_paketpemeriksaan",
            dataType: "json",
            data: { id: $('input[name="idpaketpemeriksaan"]').val() },
            success: function (a) {
                stopLoading();
                var e = "",
                    t = a.datapaket;
                if (
                    (editdataoption($("#idjenisicd"), a.jenisicd, "" == t ? "" : t.idjenisicd, '<option value="">Pilih</option>'),
                    editdataoption($("#idjeniscetak"), a.jeniscetak, "" == t ? "" : t.idjeniscetak, '<option value="">Pilih</option>'),
                    editdataoption($("#idpaketpemeriksaanrule"), a.paketrule, "" == t ? "" : t.idpaketpemeriksaanrule, '<option value="">Pilih</option>'),
                    null != a.paketpaket)
                ) {
                    for (var i in a.paketpaket)
                        e +=
                            "<label><input " +
                            ("" != t && -1 != $.inArray(a.paketpaket[i].idpaketpemeriksaan, t.paket_paket.split(",")) ? "checked" : "") +
                            ' class="flat-red" type="checkbox" name="paketpaket[]" value="' +
                            a.paketpaket[i].idpaketpemeriksaan +
                            '"> ' +
                            a.paketpaket[i].namapaketpemeriksaan +
                            "</label><br>";
                    $("#checkbox_pilihpaket").empty(), $("#checkbox_pilihpaket").html(e);
                }
                $('input[type="checkbox"].flat-red').iCheck({ checkboxClass: "icheckbox_flat-green" }),
                    "" != t &&
                        (setInputValue("namapaketpemeriksaan", t.namapaketpemeriksaan),
                        setInputValue("namahasilcetak", t.namapaketpemeriksaancetak),
                        $('select[name="ispunyachild"]').empty(),
                        $('select[name="ispunyachild"]').html("<option " + ("0" == t.ispunyachild ? "selected" : "") + ' value="0">Tidak</option><option ' + ("1" == t.ispunyachild ? "selected" : "") + ' value="1">Punya</option>'),
                        $("#iscetakqrcode").val(t.iscetakbarcode),
                        $("#isubahwaktuhasil").val(t.isubahwaktuhasil)),
                    view_rs_paket_pemeriksaan_detail(a.detailpaket);
            },
            error: function (a) {
                stopLoading(), fungsiPesanGagal();
            },
        });
}
function view_detailpaketpemeriksaan() {
    startLoading(),
        $.ajax({
            type: "post",
            url: base_url + "cmasterdata/formready_detailpaketpemeriksaan",
            dataType: "json",
            data: { id: $('input[name="idpaketpemeriksaan"]').val() },
            success: function (a) {
                stopLoading();
                var e = a.datapaket;
                "" != e && setInputValue("namapaketpemeriksaan", e.namapaketpemeriksaan), view_rs_paket_pemeriksaan_detail(a.detailpaket);
            },
            error: function (a) {
                stopLoading(), fungsiPesanGagal();
            },
        });
}
function view_rs_paket_pemeriksaan_detail(a) {
    if ("" !== a) {
        var e = "",
            t = 0,
            i = ((e = ""), "");
        for (var t in a) {
            e +=
                (i == a[t].namapaketpemeriksaan && n == a[t].gruppaketperiksa
                    ? ""
                    : null == a[t].namapaketpemeriksaan
                    ? ""
                    : '<tr height="30px" class="bg bg-gray"><td colspan="4" class="text-bold">' +
                      a[t].namapaketpemeriksaan +
                      '</td><td class=""> ' +
                      (a[t].idpaketpemeriksaan == a[t].idpaketpemeriksaanparent
                          ? ""
                          : '<a idpaketpemeriksaan="' +
                            a[t].idpaketpemeriksaan +
                            '" idpaketdetail="' +
                            a[t].idpaketpemeriksaandetail +
                            '" id="ubahpaket" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="Ubah Paket"><i class="fa fa-pencil"></i></a> <a namapaket="' +
                            a[t].namapaketpemeriksaan +
                            '" idpaketpemeriksaan="' +
                            a[t].idpaketpemeriksaan +
                            '" idpaketdetail="' +
                            a[t].idpaketpemeriksaandetail +
                            '" id="hapuspaketdetail" class="btn btn-default btn-xs" data-toggle="tooltip"  data-original-title="Hapus Paket"><i class="fa fa-trash"></i></a>') +
                      "</td></tr>") +
                (null == a[t].icd
                    ? ""
                    : "<tr " +
                      (null !== a[t].idgrup ? "" : 'class="bg-warning"') +
                      "><td> &nbsp;&nbsp;&nbsp;" +
                      a[t].icd +
                      " - " +
                      a[t].namaicd +
                      "</td><td>" +
                      (null == a[t].nilaidefault ? "" : a[t].nilaidefault) +
                      "</td><td>" +
                      a[t].nilairujukan +
                      "</td><td>" +
                      a[t].satuan +
                      "</td><td>" +
                      (null == a[t].idgrup
                          ? '<a idpaketpemeriksaan="' +
                            a[t].idpaketpemeriksaan +
                            '" idpaketdetail="' +
                            a[t].idpaketpemeriksaandetail +
                            '" id="ubahicd" class="btn btn-default btn-xs" data-toggle="tooltip" data-original-title="Ubah ICD"><i class="fa fa-pencil"></i></a> <a icd="' +
                            a[t].icd +
                            " - " +
                            a[t].namaicd +
                            '" idpaketdetail="' +
                            a[t].idpaketpemeriksaandetail +
                            '" class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusicd" data-original-title="Hapus ICD"><i class="fa fa-trash"></i></a>'
                          : "") +
                      "</td></tr>");
            a[t].idpaketpemeriksaan, a[t].idgrup, (i = a[t].namapaketpemeriksaan);
            var n = a[t].gruppaketperiksa;
        }
        $("#listdetail").empty(), $("#listdetail").html(e), $('[data-toggle="tooltip"]').tooltip();
    }
}
function edit_rs_paket_pemeriksaan_detail(a, e, t) {
    var i = $('input[name="idpaketpemeriksaan"]').val(),
        n = "" == a ? "Tambah Paket Detail" : "Ubah Paket Detail",
        s =
            '<form action="" id="formDetailPaket" autocomplete="off"><input type="hidden" name="detail_idpaketpemeriksaandetail" value="' +
            a +
            '" /><input type="hidden" name="detail_formmode" value="' +
            t +
            '" />' +
            ("tambahpaket" == t || "ubahpaket" == t
                ? '<input type="hidden" name="detail_parent_parent" value="' +
                  i +
                  '" /><input type="hidden" name="detail_idpaketpemeriksaan_selected" value="' +
                  e +
                  '" /><div class="form-group"><label>Paket Parent</label><select name="detail_idpaketpemeriksaanparent" id="detail_idpaketpemeriksaanparent" class="select2 form-control"></select></div><div class="form-group"><label>Paket Pemeriksaan</label><input type="text" name="detail_idpaketpemeriksaan" id="detail_idpaketpemeriksaan" class="form-control"/></div><div class="form-group"><label>Jenis Icd</label><select name="detail_idjenisicd" id="detail_idjenisicd" class="form-control"></select></div><div class="form-group"><label>Child</label><select name="detail_ispunyachild" id="detail_ispunyachild" class="form-control"><option value="0">Tidak</option><option value="1">Punya</option></select><small class="text text-red">#jika paket memiliki sub paket, isi dengan punya jika tidak isi dengan tidak.</small></div>'
                : '<input type="hidden" name="detail_idpaketpemeriksaanparent" value="' +
                  i +
                  '" /><div class="form-group"><label>Pilih Paket Pemeriksaan</label><select name="detail_idpaketpemeriksaan" id="detail_paketpemeriksaan" class="select2 form-control"></select></div><div class="form-group"><label>Input ICD</label><select name="detail_icd" id="detail_icd" class="form-control select2"><option value="">Pilih</option></select></div>') +
            "</form>";
    function p() {
        startLoading(),
            $.ajax({
                type: "post",
                url: base_url + "cmasterdata/insert_or_update_rs_paket_pemeriksaan_detail",
                dataType: "json",
                data: $("#formDetailPaket").serialize(),
                success: function (a) {
                    stopLoading(), notif(a.status, a.message), view_detailpaketpemeriksaan();
                },
                error: function (a) {
                    stopLoading(), fungsiPesanGagal();
                },
            });
    }
    $.confirm({
        title: n,
        content: s,
        columnClass: "s",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        buttons: {
            formSubmit: {
                text: "simpan",
                btnClass: "btn-blue",
                action: function () {
                    if ("tambahicd" == t || "ubahicd" == t) {
                        if ("" == $('select[name="detail_icd"]').val()) return alert("ICD Harap Dipilih."), !1;
                        p();
                    } else if ("tambahpaket" == t || "ubahpaket" == t) {
                        if ("" == $("#detail_idpaketpemeriksaan").val()) return alert("Paket Pemeriksaan Harap Diisi."), !1;
                        p();
                    }
                },
            },
            formReset: { text: "keluar", btnClass: "btn-danger" },
        },
        onContentReady: function () {
            !(function (a, e, t, i) {
                $.ajax({
                    url: base_url + "cmasterdata/get_rs_paket_pemeriksaan_detail",
                    type: "post",
                    dataType: "json",
                    data: { id: a, mode: e, idpaketdetail: i, idpaketpemeriksaan_selected: t },
                    success: function (a) {
                        var i = "",
                            n = "ubahpaket" == e ? a.detailpaket.idpaketpemeriksaanparent : t;
                        for (var s in a.paketparent)
                            i += "<option " + (n == a.paketparent[s].idpaketpemeriksaan ? "selected" : "") + ' value="' + a.paketparent[s].idpaketpemeriksaan + '">' + a.paketparent[s].namapaketpemeriksaan + "</option>";
                        "tambahicd" == e || "ubahicd" == e
                            ? ($("#detail_paketpemeriksaan").html(i),
                              "" != a.icd && $("#detail_icd").html('<option value="' + a.icd.icd + '">' + a.icd.icd + " - " + a.icd.namaicd + "</option>"),
                              select2serachmulti($("#detail_icd"), "cmasterdata/fillIcd"))
                            : ("tambahpaket" != e && "ubahpaket" != e) ||
                              ($("#detail_idjenisicd").html('<option value="' + a.jenisicd.idjenisicd + '">' + a.jenisicd.namajenisicd + "</option>"),
                              $("#detail_idpaketpemeriksaanparent").html(i),
                              $("#detail_idpaketpemeriksaanparent").before("" == i ? '<br><small class="text text-red">Paket Tidak Punya Child, <br> Tidak Bisa Menambahkan Paket Dibawahnya.</small>' : ""),
                              "ubahpaket" == e &&
                                  ($("#detail_idpaketpemeriksaanparent").prop("disabled", !0),
                                  $("#detail_idjenisicd").prop("disabled", !0),
                                  $("#detail_idpaketpemeriksaan").val(a.detailpaket.namapaketpemeriksaan),
                                  $("#detail_ispunyachild").html(
                                      "<option " + (0 == a.detailpaket.ispunyachild ? "selected" : "") + ' value="0">Tidak</option><option ' + (1 == a.detailpaket.ispunyachild ? "selected" : "") + ' value="1">Punya</option>'
                                  ),
                                  $("#detail_ispunyachild").prop("disabled", !0)));
                    },
                    error: function (a) {
                        return notif(a.status, a.message), !1;
                    },
                });
            })(i, t, e, a),
                $(".select2").select2();
            var n = this;
            this.$content.find("form").on("submit", function (a) {
                a.preventDefault(), n.$$formSubmit.trigger("click");
            });
        },
    });
}
function delete_rs_paket_pemeriksaan_detail(a, e, t, i) {
    $.confirm({
        icon: "fa fa-info",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Informasi",
        content: "Hapus Dari Paket Pemeriksaan,<br>" + ("icd" == t ? " ICD " : " Paket ") + " <b>" + i + "</b>" + ("icd" == t ? "" : ' <br><small class="text text-red"> ICD Yang Memiliki Paket Parent Tersebut Akan Terhapus.! </small>'),
        buttons: {
            hapus: function () {
                startLoading(),
                    $.ajax({
                        url: base_url + "cmasterdata/delete_rs_paket_pemeriksaan_detail",
                        type: "post",
                        dataType: "json",
                        data: { idpaketdetail: a, modedelete: t, idpaketpemeriksaan: e },
                        success: function (a) {
                            stopLoading(), notif(a.status, a.message), view_detailpaketpemeriksaan();
                        },
                        error: function (a) {
                            return stopLoading(), notif(a.status, a.message), !1;
                        },
                    });
            },
            batal: function () {},
        },
    });
}
function setInputValue(a, e) {
    return $('input[name="' + a + '"]').val(e);
}
function menuKosongkan() {
    $.confirm({
        icon: "fa fa-info",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Informasi",
        content: "Yakin form paket pemeriksaan dikosongkan? ",
        buttons: {
            kosongkan: function () {
                window.location.reload();
            },
            batal: function () {},
        },
    });
}
function menuKembali() {
    $.confirm({
        icon: "fa fa-info",
        theme: "modern",
        closeIcon: !0,
        animation: "scale",
        type: "orange",
        title: "Informasi",
        content: "Yakin kembali ke list paket pemeriksaan? ",
        buttons: {
            kembali: function () {
                window.location.href = base_url + "cmasterdata/paketpemeriksaan";
            },
            batal: function () {},
        },
    });
}
$(function () {
    switch ($("#jsmode").val()) {
        case "input":
            formready();
            break;
        case "detail":
            view_detailpaketpemeriksaan();
            break;
        case "view":
            listdatatable();
    }
}),
    $(document).on("click", "#reload", function () {
        $('input[type="search"]').val("").keyup(), listdata.state.clear(), listdata.ajax.reload();
    }),
    $(document).on("click", "#hapuspaket", function () {
        var a = $(this).attr("idpaket"),
            e = $(this).attr("namapaket");
        $.confirm({
            icon: "fa fa-info",
            theme: "modern",
            closeIcon: !0,
            animation: "scale",
            type: "orange",
            title: "Informasi",
            content: "Hapus Paket  <br>" + e,
            buttons: {
                hapus: function () {
                    startLoading(),
                        $.ajax({
                            type: "post",
                            url: base_url + "cmasterdata/hapus_paketpemeriksaan",
                            dataType: "json",
                            data: { id: a },
                            success: function (a) {
                                notif(a.status, a.message), stopLoading(), listdata.ajax.reload(null, !1);
                            },
                            error: function (a) {
                                stopLoading(), fungsiPesanGagal();
                            },
                        });
                },
                batal: function () {},
            },
        });
    }),
    $(document).on("click", "#ubahpaket", function () {
        edit_rs_paket_pemeriksaan_detail($(this).attr("idpaketdetail"), $(this).attr("idpaketpemeriksaan"), "ubahpaket");
    }),
    $(document).on("click", "#hapuspaketdetail", function () {
        delete_rs_paket_pemeriksaan_detail($(this).attr("idpaketdetail"), $(this).attr("idpaketpemeriksaan"), "paketpemeriksaan", $(this).attr("namapaket"));
    }),
    $(document).on("click", "#ubahicd", function () {
        edit_rs_paket_pemeriksaan_detail($(this).attr("idpaketdetail"), $(this).attr("idpaketpemeriksaan"), "ubahicd");
    }),
    $(document).on("click", "#unduh", function () {
        window.location.href = base_url + "creport/excel_paketpemeriksaan";
    }),
    $(document).on("click", "#hapusicd", function () {
        delete_rs_paket_pemeriksaan_detail($(this).attr("idpaketdetail"), 0, "icd", $(this).attr("icd"));
    });
