var listpasienranap='';
localStorage.setItem('idbangsal',(localStorage.getItem('idbangsal')==undefined) ? '' : localStorage.getItem('idbangsal'));
$(function(){ 
    get_databangsal('#idbangsal',localStorage.getItem('idbangsal'));
    $('.sel2').select2();
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    setTimeout(function(){ listPasienRanap(); }, 100);
});
function listPasienRanap()
{  listpasienranap = $('#listpasienranap').DataTable({"processing": true,"serverSide": true,"stateSave": true,"order": [],"ajax": {
   "data":{status:function(){return $('#status').val();},idbangsal:function(){return $('#idbangsal').val();}, tanggal:function(){return $('#tanggal').val();}},
   "url": base_url+'cpelayananranap/dt_administrasiranap',"type": "POST"},"columnDefs": [{ "targets": [7,8,10],"orderable": false,},],
   "fnCreatedRow": function (nRow, aData, iDataIndex) { $(nRow).attr('style', qlstatuswarna(aData[9]));}, // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
   "drawCallback": function (settings, json) {$('[data-toggle="tooltip"]').tooltip();},// -- Function that is called every time DataTables performs a draw.
   });
}

//ubahdpjpranap
$(document).on('click','#ubahdpjpranap',function(){
    var norm       = $(this).attr('norm');
    var idinap     = $(this).attr('nomori');
    var namapasien = $(this).attr('namalengkap');
    var dokter     = $(this).attr('dokter');
    var bangsal    = $(this).attr('bangsal');
    
    var konten = '<b> IDENTITAS PASIEN RANAP </b> </br>'
                +'<p class="bg bg-info" style="padding:4px 8px;border-radius:3px;"><b>Dokter DPJP</b> : '+dokter +'</br>'
                +'<b>Bangsal</b> : '+bangsal +'</br>'
                +'<b>NORM</b> : '+norm +'</br>'
                +'<b>Nama Pasien</b> : '+namapasien +'</p>'
                +'<b>Dokter Pengganti</b> : <select id="filldpjp" class="form-control select2"><option value="">-Pilih-</option></select></br>'
    $.confirm({
        icon: 'fa fa-question',
//        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Ubah Dokter DPJP',
        content: konten,
        buttons: {
            ubah: function () { 
                $.ajax({
                    url:base_url+"cpelayananranap/ubahdpjprawatinap",
                    type:"POST",
                    dataType:"JSON",
                    data:{iri:idinap, idd:$('#filldpjp').val()},
                    success:function(result){
                        notif(result.status, result.message);
                        listpasienranap.ajax.reload(null,false); 
                    },
                    error:function(result){ 
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            batal: function () {               
            }            
        },
        onContentReady: function () { // bind to events
            select2serachmulti($('#filldpjp'),'cmasterdata/fillcaridokter');
        }
    });
});
//refresh data inap
$(document).on('click','#refresh',function(){ // refresh data
   $('input[type="search"]').val('').keyup();
   listpasienranap.state.clear();
   listpasienranap.ajax.reload(); 
});

//tampil data inap
$(document).on('click','#tampil',function(){
   listpasienranap.ajax.reload(null,false); 
});

function cariByStatus(){
    if($('#status').val() =='pesan' || $('#status').val()=='ranap' )
    {
        $('#tanggal').remove();
        localStorage.setItem('idbangsal',$('#status').val());
        listpasienranap.ajax.reload(null,false);
    }else{
        $('#tanggal').remove();
        $('#tampil').before('<input type="text" id="tanggal" name="tanggal" class="datepicker" size="7" /> ');
        $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker("setDate",'now');
    }
}
function cariByBangsal(){localStorage.setItem('idbangsal',$('#idbangsal').val());listpasienranap.ajax.reload();}

$(document).on("click","#administrasiranapSelesai", function(){ 
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("alt2");
    var idbed = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    var norm = $(this).attr("norm");
    var namapasien = $(this).attr("namapasien");
    var pesan = 'Selesai rawat inap..? ';
        pesan += '<div class="text text-left">\n\
                  <br> Norm : '+norm+' \n\
                  <br> Nama Pasien : '+namapasien +
                 '<br> <small class="text text-red">#Rencana Perawatan setelah tanggal pasien selesai ranap akan dihapus oleh sistem.</small>\n\
                 <br> <small class="text text-info">#Untuk dapat menyelesaikan ranap, rencana perawatan harus diubah menjadi terlaksana atau batal.</text></div>';
    administrasiUbahStatus('selesai',pesan,id,idpendaftaran,nobaris,idbed);
});
//---------- mahmud, clear
$(document).on("click","#administrasiranapBatalSelesai", function(){ 
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("alt2");
    var idbed = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    administrasiUbahStatus('ranap','Selesaikan rawat inap..?',id,idpendaftaran,nobaris,idbed);
});
//---------- mahmud, clear
$(document).on("click","#administrasiranapMenunggu", function(){ 
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("alt2");
    var idbed = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    administrasiUbahStatus('menunggu','Ubah status rawat inap ke Menunggu..?',id,idpendaftaran,nobaris,idbed);
});
//---------- mahmud, clear
$(document).on("click","#administrasiranapBatal", function(){ 
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("alt2");
    var idbed = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    administrasiUbahStatus('batal','Batalkan rawat inap..?',id,idpendaftaran,nobaris,idbed);
});
//---------- mahmud, clear
$(document).on("click","#administrasiranapPeriksa", function(){
    if (typeof(Storage) !== "undefined") {
        localStorage.removeItem("idinap");
        localStorage.setItem('idinap',$(this).attr("nomori"));
        window.location.href=base_url + 'cpelayananranap/pemeriksaanranap';
    } else {
         pesanUndefinedLocalStorage();
    }
});
//---------- mahmud, clear
$(document).on("click","#administrasiranapRanap", function(){ 
    var id = $(this).attr("alt");
    var idpendaftaran = $(this).attr("alt2");
    var idbed = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    administrasiUbahStatus('ranap','Buka layanan rawat inap..?',id,idpendaftaran,nobaris,idbed);
});

$(document).on("click","#administrasiranapKeluarkamar", function(){ 
    var idinap = $(this).attr("alt");
    var idbed = $(this).attr("alt3");
    var idbangsal = $(this).attr("alt4");
    $.ajax({
        url: base_url+"cpelayananranap/ubahstatusbed",
        data:{status:1, i:idbed, ib:idbangsal, ii:idinap, statusinap:'pulang'},
        dataType:"JSON",
        type:"POST",
        success:function(result){
            notif(result.status, result.message);
            listpasienranap.ajax.reload();
        },
        error:function(result){
            fungsiPesanGagal();
        }
    });
});

// mahmud, clear
// ubah status rawat inap
function administrasiUbahStatus(status,pesan,idinap,idpendaftaran,nobaris,idbed)
{
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: pesan,
        buttons: {
            konfirmasi: function () {
                startLoading();
                $.ajax({
                    url: base_url+"cpelayananranap/administrasi_ranap_ubahstatus",
                    data:{status:status, idinap:idinap, idpendaftaran:idpendaftaran, idbed:idbed},
                    dataType:"JSON",
                    type:"POST",
                    success:function(result){
                        stopLoading();
                        if(result.status=='menunggu'){
                            detailrencana(result.rencana);
                        }else{
                            notif(result.status, result.message);
                            listpasienranap.ajax.reload();
                        }
                    },
                    error:function(result){
                        stopLoading();
                        fungsiPesanGagal();
                    }
                });
            },
            batal: function () {               
            }            
        }
    });
}

function detailrencana(rencana)
{
    var list_rencana='';
    for(var x in rencana)
    {
        list_rencana += '<tr><td>'+rencana[x].waktuperencanaan+'</td><td>'+rencana[x].statusrencana+'</td><td>'+rencana[x].icd+'</td><td>'+rencana[x].statusperawatan+'</td></tr>';
    }
    
    var modalTitle = 'Rencana Perawatan Belum Terlaksana';
    var modalContent = '<form action="" id="FormJadwal">' +
                          '<table class="table table-striped">\n\
                            <tr class="bg bg-yellow-gradient"><td>Waktu Rencana</td><td>Status Rencana</td><td>Rencana Perawatan</td><td>Status Perawatan</td></tr>\n\
                            '+list_rencana+'\n\
                            </table> <small class="text text-red">#Kunjungi halaman Pemeriksaan Rawat Inap => <a class="btn btn-xs btn-success" href="'+base_url+'cpelayananranap/pemeriksaanranap"><i class="fa fa-stethoscope"></i></a>, untuk mengubah status perencanaan.</text>'+
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu back
            formReset:{
                text: 'kembali',
                btnClass: 'btn-danger'
            }
        }
    });
}

// mahmud, clear ==== UBAH STATUS BED ===
$(document).on("click","#ubahstatusbed", function(){
    var idbangsal = $(this).attr("ibang");
    var idbed = $(this).attr("ibed");
    var idstatusbed = $(this).attr("istatus");
    var stat1 = $(this).attr("stat1");
    var stat2 = $(this).attr("stat2");
    var inap=$(this).attr("nomori");;
    ubahstatusbed(idbangsal, idbed,idstatusbed,stat1,stat2,inap);
});
function ubahstatusbed(idbangsal, idbed,idstatusbed,stat1,stat2,inap)
{
    $.confirm({icon: 'fa fa-question',theme: 'modern',closeIcon: true,animation: 'scale',type: 'orange',title: 'Konfirmasi!',
        content: 'Ubah status dari '+stat1+' ke '+stat2+'..? ',
        buttons: {
            confirm: function () {
                startLoading();
                $.ajax({ 
                    url: base_url +'cpelayananranap/ubahstatusbed',
                    type : "post",      
                    dataType : "json",
                    data : {ib: idbangsal, i:idbed, status:idstatusbed,ii:inap},
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        listpasienranap.ajax.reload();
                    },
                    error: function(result){        
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
}
// ******* PINDAH KAMAR ********
$(document).on("click","#ranap_pindahkamar", function(){
    localStorage.removeItem("bedasal");
    localStorage.setItem('bedasal',$(this).attr("bangsal"));
    ranap_pindahkamar($(this).attr("norm"),$(this).attr("idp"),$(this).attr("inap"));
});
function ranap_pindahkamar(mr,idp,inap)
{
    $('.jconfirm').show();
    startLoading();   
    // menampilkan status bangsal
var mContent='<div class="col-sm-5" style="margin:none;"><h4>Status Bed: </h4><div class="box no-border" style="margin:none;"><div class="box-body"><div class="chart"><table border="1"><tbody style="display:block;overflow:auto;"><tr id="stsbangsal_list"></tr></tfoot></table></div></div></div></div>';
    // menampilkan list bangsal
    mContent+='<div class="col-sm-7" style="margin:none;"><h4>Bangsal: </h4><table><tbody style="display:block;overflow:auto;"><tr id="bangsal_list"></tr></tfoot></table><select class="form-control" name="kelasperawatan" style="width:50%;margin-top:5px;" id="kelasperawatan"></select> <input type="checkbox" id="israwatgabung" class="flat-red"> <label>Rawat Gabung</label></div>';
    // menampilkan bed
    mContent+=
    '<div class="col-md-12">'+
        '<style type="text/css">.fixed_header tbody{display:block;overflow:auto;height:200px;width:100%.fixed_header thead tr{display:block}</style>'+
        '<div class="box no-border">'+
            '<div class="with-border">'+
                '<h4 class="box-title" id="judul_listbed">List Bed</h4>'+
                '</div>'+
                '<div class="box-body">'+
                '<div class="lablel label-warning" id="infobangsal"></div>'+
                '<div class="chart">'+
                '<table" id="listBed"></table>'+
                '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+    
    '</div>';
    $.confirm({
        title: 'PINDAH KAMAR RANAP',content: mContent,columnClass: 'lg',autoRefresh:true,
        buttons: {
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger',
                action: function(){
                    $('.jconfirm').empty();
                    $('.jconfirm').hide();
                }   
            }
        },
        onContentReady: function () { // bind to events
            stopLoading();
            ranap_listbangsal(mr,idp,inap);
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
// menampilkan data bangsal
function ranap_listbangsal(norm,idpendaftaran,inap)
{
    $.ajax({ 
        url      : base_url+'cpelayananranap/pindahkamar_listbangsal',
        type     : "post",      
        dataType : "json",
        data     : {mr:norm},
        success: function(result) {
            var bangsal_list='';
            for(x in result.bangsal){bangsal_list += '<a class="btn btn-primary btn-sm" id="ranaplistbed" inap="'+inap+'" idbangsal="'+result.bangsal[x].idbangsal+'" daftar="'+idpendaftaran+'" bangsal="'+result.bangsal[x].namabangsal+' '+result.bangsal[x].kelas+'" cursor:pointer;">'+result.bangsal[x].namabangsal+'</a>&nbsp;';}
            $('#bangsal_list').empty();
            $('#bangsal_list').html(bangsal_list);

            var stsbangsal_list='';
            for( i in result.statusbed){stsbangsal_list += '<td style="padding:10px;"><img src="'+base_url+'assets/images/'+ result.statusbed[i].file +'" width="45"/><br> '+ result.statusbed[i].statusbed +'</td> &nbsp;';}
            $('#stsbangsal_list').empty();
            $('#stsbangsal_list').html(stsbangsal_list);

            var kelasperawatan='<option value="0">Kelas Perawatan</option>';
            for(a in result.kelas){ kelasperawatan+='<option value="'+result.kelas[a].idkelas+'">'+result.kelas[a].kelas+'</option>';}
            $('#kelasperawatan').empty();
            $('#kelasperawatan').html(kelasperawatan);
        },
        error: function(result){         
            fungsiPesanGagal();
            return false;
        }
    });

}

$(document).on('click','#israwatgabung',function(){
    $('#listBed').empty();
    
})
// MENAMPILKAN BED SETELAH MEMILIH BANGSAL
// list bed
$(document).on("click","#ranaplistbed", function(){
    startLoading();
    var value = $(this).attr("idbangsal");
    var inap = $(this).attr("inap");
    var idpendaftaran = $(this).attr("daftar");
    var bangsal = $(this).attr("bangsal");

    var cekrawatgabung = ($('#israwatgabung').is(":checked")) ? 1 : 0 ;
    localStorage.removeItem('bangsal');
    localStorage.setItem('bangsal',bangsal);
    $('#judul_listbed').empty();
    $('#judul_listbed').text('List Bed '+bangsal);
    $.ajax({ 
        url: base_url+'cadmission/ranap_listbed',
        type : "post",      
        dataType : "json",
        data:{id:value},
        success: function(result) {
            stopLoading();
            var dtinfo = '';
            for(x in result.info){dtinfo += ' nomor rekam medis '+ result.info[x].norm+' - a/n pasien '+result.info[x].namalengkap+result.info[x].bed+'<br>';}
            $('#infobangsal').empty();
            $('#infobangsal').html(dtinfo);
            if(dtinfo!=''){$('#infobangsal').css("padding","8 px");}
            var dthtml='<tr>', no=0;
            for( i in result.bangsal)
            { 
               var allowmenu = '';
               var dt_tr='';  ++no; if(no>7){no=0;dt_tr='</tr><tr>';}
               // cekrawatgabung
               if(result.bangsal[i].statusbed=='Bed Kosong' && cekrawatgabung!='1'){
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+inap+','+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+result.bangsal[i].nobed+')" ';
               }else if(result.bangsal[i].statusbed=='Bed Wanita' && cekrawatgabung=='1'){
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+inap+','+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+result.bangsal[i].nobed+')" ';
               }else if(result.bangsal[i].statusbed=='Bed Laki-laki' && cekrawatgabung=='1'){
                 allowmenu = 'style="cursor:pointer;" onclick="ranap_pilihbed('+inap+','+result.bangsal[i].idbangsal+','+result.bangsal[i].idbed+','+idpendaftaran+','+result.bangsal[i].nobed+')" ';
               }else{
                allowmenu = 'style="cursor:not-allowed;" ';
               }
               dthtml += '<td style="text-align:center; padding:4px;"><a '+ allowmenu + '><img style="border-radius:0px; width:120px;height:120px;" src="'+base_url+'assets/images/'+result.bangsal[i].file+'" alt="'+result.bangsal[i].statusbed+'"><span class="users-list-name"  style="font-size:30px;">'+result.bangsal[i].nobed+'</span></a></td>'+dt_tr;
            }
            $('#listBed').empty();
            $('#listBed').html(dthtml+'</tr>');
        },
        error: function(result){          
            fungsiPesanGagal();
            return false;
        }
    });
    
});
// aksi bed dipilih
//  -- rawat inap pilih bed
function ranap_pilihbed(inap,idbangsal,idbed,idpendaftaran,nobed)
{
    var idkelas = $('#kelasperawatan').val();
    if(idkelas==0)
    {
        $.alert({
            icon: 'fa fa-exclamation',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Peringatan',
            content: 'Kelas Perawatan Belum dipilih.!'
        });
    }
    else
    {
        $.confirm({
            icon: 'fa fa-exclamation',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            title: 'Konfirmasi Pindah Kamar.',
            content: '<p style="text-align:left;">Bed Asal : '+localStorage.getItem('bedasal')+'<br> Bed Tujuan: '+ localStorage.getItem('bangsal') +' ('+nobed+') <br>Kelas Perawatan: '+$('#kelasperawatan option:selected').text()+'</p>',
            buttons: {
                confirm: function () { 
                startLoading();
                    $.ajax({ 
                        url: base_url+'cpelayananranap/pindahkamar_simpan',
                        type : "post",      
                        dataType : "json",
                        data : { bangsal:idbangsal, bed:idbed,inap:inap,idkelas:idkelas},
                        success: function(result) {
                            stopLoading();
                            $('.jconfirm').empty();
                            $('.jconfirm').hide();
                            listpasienranap.ajax.reload(); 
                            notif(result.status, result.message);
                        },
                        error: function(result){
                            stopLoading();
                            fungsiPesanGagal();
                            return false;
                        }
                        
                    });
                },
                cancel: function () {               
                }            
            }
        });
    }
}

$(document).on('click','#bhpfarmasiranap',function(){
    localStorage.setItem("idinap",$(this).attr('nomori'));
    localStorage.removeItem("idrencanamedispemeriksaan");
    localStorage.removeItem("norm");
    localStorage.removeItem("idpendaftaranranap");
    window.location.href=base_url + 'cpelayananranap/pemeriksaanranap_bhp'; 
});
