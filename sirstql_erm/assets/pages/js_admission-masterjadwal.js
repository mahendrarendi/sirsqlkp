var datamasterjadwal = '';
$(function () {
    dt_masterjadwal();
})

function dt_masterjadwal(){
    datamasterjadwal =  $('#dtmasterjadwal').DataTable({
    "dom": '<"toolbar">frtip',
    "processing": true,
    "serverSide": true,
    "stateSave": true,
    "order": [],
    "ajax": {
        "url": base_url+'cadmission/dt_masterjadwal',
        "type": "POST"
    },
    "columnDefs": [{ "targets": [],"orderable": false,},],
     "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
     },
     "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
     },
    });
}

$(document).on("click","#masterjadwalEdit", function(){ // edit jadwal
    var id = $(this).attr("alt");
    getdatajadwal(id,'ubah','Edit Jadwal')
});
$(document).on("click","#masterjadwalCopy", function(){ // edit jadwal
    var id = $(this).attr("alt");
    getdatajadwal(id,'salin','Salin Jadwal')
});
function getdatajadwal(idjadwal,mode,formTitle)
{
	$.ajax({ 
        url: 'masterjadwal_edit',
        type : "post",      
        dataType : "json",
        data : { x:idjadwal },
        success: function(result) {  
            masterjadwalFormInput(formTitle,result,mode); 
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
    });
}

function masterjadwalAdd()
{
    masterjadwalFormInput('Add Jadwal','','tambah');
}
function masterjadwalFormInput(title,dataEdit,mode)
{
    var modalTitle = title;
    var modalContent = '<form action="" id="FormJadwal">' +
                            '<input type="hidden" name="i" value=""/>' +
                            '<input type="hidden" name="mode" value="'+mode+'"/>' +
                            '<div class="form-group col-sm-6">' +
                                '<label>Poliklinik</label>' +
                                '<select class="select2 form-control" onchange="masterjadwalcariloket(this.value)" style="width:100%; display:relative;" name="poli">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +

                            '<div class="form-group col-sm-6">' +
                                '<label>Dokter</label>' +
                                '<select name="dokter" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                            '</div>' +

                            '<div class="form-group col-sm-6">' +
                                '<label>Loket</label>' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" name="loket">'+
                                '<option value="0">Pilih</option>'+
                                '</select>' +
                            '</div>' +
                            
                            '<div class="form-group col-sm-6">' +
                                '<label>Grup Jadwal</label>' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" name="jadwalgrup">'+
                                '<option value="0">Tidak Ada Grup</option>'+
                                '</select>' +
                            '</div>' +

                            '<div class="form-group col-sm-12">' +
                                '<label>Hari</label>' +
                                '<select name="hari" class="form-control select2">' +
                                  '<option value="0">Pilih</option>' +
                                '</select>' +
                            '</div>' +

                            '<div class="form-group col-sm-6">' +
                                '<label>Jam Mulai</label>' +
                                '<input type="text" name="jammulai" placeholder="Jam Mulai" class="timepicker form-control"/>' +
                            '</div>' +

                            '<div class="form-group col-sm-6">' +
                                '<label>Jam Akhir</label>' +
                                '<input type="text" name="jamakhir" placeholder="Jam Akhir" class="timepicker form-control"/>' +
                            '</div>' +

                            '<div class="form-group col-sm-4">' +
                                '<label>Quota</label>' +
                                '<input type="text" name="quota" placeholder="Quota" class="form-control" readonly/>' +
                            '</div>' +
                            
                            '<div class="form-group col-sm-4">' +
                                '<label>Quota JKN</label>' +
                                '<input type="text" onkeyup="hitung_quota()" name="quotajkn" placeholder="Quota Pasien JKN" class="form-control"/>' +
                            '</div>' +
                            
                            '<div class="form-group col-sm-4">' +
                                '<label>Quota Non JKN</label>' +
                                '<input type="text" onkeyup="hitung_quota()" name="quotanonjkn" placeholder="Quota Pasien Non JKN" class="form-control"/>' +
                            '</div>' +
                            
                            '<div class="form-group col-sm-12">' +
                                '<small class="text-red">Harap isi Quota JKN & Quota Non JKN dengan Benar, digunakan untuk bridging sistem dengan BPJS.</small>' +
                            '</div>' +
                            
                        '</form>';
    //aksi ketika di klik menu
    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'large',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="poli"]').val()==='0')
                    {
                            alert('Poli Harap Dipilih.');
                            return false;
                    }
                    else if($('select[name="dokter"]').val()==='0')
                    {
                            alert('Dokter Harap Dipilih.');
                            return false;
                    }
                    else if($('select[name="loket"]').val()==='0')
                    {
                            alert('Loket Harap Dipilih.');
                            return false;
                    }
                    else if($('select[name="hari"]').val()==='0')
                    {
                            alert('Hari Harap Dipilih.');
                            return false;
                    }
                    else if(!$('input[name="jammulai"]').val() )
                    {
                            alert('Jam Mulai Harap Diisi.');
                            return false;
                    }
                    else if(!$('input[name="jamakhir"]').val() )
                    {
                            alert('Jam Akhir Harap Diisi.');
                            return false;
                    }
                    else if(!$('input[name="quota"]').val() )
                    {
                            alert('Quota Non JKN dan Quota JKN Harap Diisi.');
                            return false;
                    }
                    else
                    {
                            $.ajax({
                                type: "POST", //tipe pengiriman data
                                url: 'masterjadwal_save', //alamat controller yang dituju (di js base url otomatis)
                                data: $("#FormJadwal").serialize(), //
                                dataType: "JSON", //tipe data yang dikirim
                                success: function(result) {
                                    notif(result.status, result.message);
                                    qlReloadPage(600);
                                },
                                //jika error
                                error: function(result) {
                                    console.log(result.responseText);
                                }
                            });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
            if(dataEdit!==null)
            {
                $('input[name="i"]').val(dataEdit.idmasterjadwal);
                $('input[name="jammulai"]').val(dataEdit.jammulai);
                $('input[name="jamakhir"]').val(dataEdit.jamakhir);
                $('input[name="quota"]').val(dataEdit.quota);                
                $('input[name="quotajkn"]').val(dataEdit.quota_jkn);                
                $('input[name="quotanonjkn"]').val(dataEdit.quota_nonjkn);
            }
        
            fungsi_tampilpoli(dataEdit.idunit);//panggil fungsi tampil poli
            fungsi_tampildokter(dataEdit.idpegawaidokter);//panggil fungsi tampil dokter
            fungsi_tampilloket(dataEdit.idloket,dataEdit.idunit);//panggil fungsi tampil loket
            fungsi_tampiljadwalgrup(dataEdit.idjadwalgrup);//panggil fungsi tampil jadwal grup
            fungsi_tampilhari(dataEdit.hari);//panggil fungsi tampil hari
        
        $('.select2').select2();
        $('.timepicker').timepicker({ showInputs: true, showMeridian:false});//Timepicker
        
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

function masterjadwalcariloket(idunit)
{
    fungsi_tampilloket(0,idunit);
}

//fungsi hitung total quota 
function hitung_quota()
{
    var jkn    = parseInt($('input[name="quotajkn"]').val());
    var nonjkn = parseInt($('input[name="quotanonjkn"]').val());
    
    var quota  = jkn + nonjkn;
    
    $('input[name="quota"]').val(quota);
}

function fungsi_tampilpoli(selected) //panggil fungsi tampil poli
{
    var select='';
    $.ajax({
        url:'masterjadwal_caripoli',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="poli"]').empty();
            for(i in result)
            {
                select = select + '<option '+if_select(result[i].idunit,selected)+' value="'+ result[i].idunit +'" >' + result[i].namaunit +'</option>';
            }
            $('select[name="poli"]').html('<option value="0">Pilih</option>' + select + '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    }); 
}
function fungsi_tampildokter(selected) //panggil fungsi tampil dokter
{
    var select='';
    $.ajax({
        url:'masterjadwal_caridokter',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="dokter"]').empty();
            for(var i in result)
            {
            select = select + '<option '+if_select(result[i].idpegawai,selected)+' value="'+ result[i].idpegawai +'" >'+ result[i].namalengkap +'</option>';
            }
            $('select[name="dokter"]').html('<option value="0">Pilih</option>' + select + '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    }); 
}

function fungsi_tampilhari(selected) //panggil fungsi tampil hari
{
    var select='';
    $.ajax({
        url:'masterjadwal_tampilhari',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('select[name="hari"]').empty();
            for(var i in result)
            {
            select = select + '<option '+if_select(i,selected)+' value="'+ i +'" >' + result[i] +'</option>';
            }
            $('select[name="hari"]').html('<option value="0">Pilih</option>' + select + '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    }); 
}
$(document).on("click","#masterjadwalDelete", function(){ // DELETE USER
    var id = $(this).attr("alt");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete Jadwal?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'masterdata_deletejadwal',
                    type : "post",      
                    dataType : "json",
                    data : { x:id },
                    success: function(result) {                                                          
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            qlReloadPage(600);
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){                  
                        fungsiPesanGagal();
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});