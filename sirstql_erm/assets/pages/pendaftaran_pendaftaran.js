'use strict';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function cari_poliklinik(value,grup='') //fungsi cari poli di rs jadwal berdasarkan tanggal input
{ 
    $.ajax({type: "POST",url: 'cari_jadwalpoli',data:{date: ambiltanggal(value),grup:grup},
        dataType: "JSON",
        success: function(result) {
             edit_dropdown_poliklinik($('#poliklinik'),result,'');//menampilkan poliklinik
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}

function edit_dropdown_poliklinik(column,data,selected_data) //fungsi edit poliklinik
{
    // data jadwal yang dipilih split menjadi array
    var selectarray = split_to_array(selected_data);

    if(data === '')
    {
        column.empty();
        column.html('');
    }
    else
    {
        var select ='';
        var selected;
        column.empty();
        for(var i in data)
        {
            
            select = select + '<option value="'+ data[i].idunit +','+ data[i].idjadwal +'" '+ ((selectarray.includes(data[i].idjadwal))?'selected':'') +' >' + data[i].namaunit +' '+ data[i].namadokter +' ' +if_empty(data[i].tanggal,'')+' </option>';
        }
        column.html('<option value="0">Pilih</option>'+ select );
        $('.select2').select2();
    }   
}
