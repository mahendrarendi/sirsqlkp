$(function () {
    $('.select2').select2(); //Initialize Select2 Elements 
    cariicdpaket($('select[name="icd"]'));   
    if($('#hideidicd').val()!='')
    {
      var data ='<option value='+$('#hideidicd').val()+'>'+$('#hideidicd').val()+' | '+ $('#hideidnameicd').val()+' </option>'
      $('select[name="icd"]').html(data);
    }
})

function simpan_paketpemeriksaandetail() //fungsi simpan paketpemeriksaandetail
{
  if($('select[name="idpaket"]').val()==='0') //jika nama paketpemeriksaandetail kosong
  {
    $('select[name="idpaket"]').focus();
    alert_empty('paketpemeriksaan');
    return false;
  }
  else if($('select[name="icd"]').val()==='0') //jika nama paketpemeriksaandetail kosong
  {
    $('select[name="icd"]').focus();
    alert_empty('icd');
    return false;
  }
  else
  {
    $('#FormPaketPemeriksaandetail').submit(); //submit form paketpemeriksaandetail
  }
}

// pencarian icd
function cariicdpaket(x)
{
    // startLoading();
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cmasterdata/paketpemeriksaandetail_cariicd",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            // stopLoading();
            return {
                i: params.term,
                // page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
                }
            };
        },              
    }
    });
}