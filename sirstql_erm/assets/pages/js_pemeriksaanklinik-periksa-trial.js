var norm = localStorage.getItem('norm');//siapkan norm -> ambil dari localstorage norm
var idperiksa = localStorage.getItem('idperiksa');//siapkan idperiksa -> ambil dari localstorage idperiksa
var idpendaftaran = localStorage.getItem('idp');
localStorage.setItem('jenisrawat','rajal');
var modehalamanjs = $('input[name="modehalaman"]').val();
$(function(){
    $('input[name="idperiksa"]').val(idperiksa);
    $('#riwayatdtpasien').attr('norm',norm);
    $('.select2').select2();
    $('.datepicker').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}); //Initialize Date picker
    pemeriksaanklinikDetailPasien();
    $('input[type="checkbox"].flat-red').iCheck({checkboxClass: 'icheckbox_flat-green',radioClass   : 'iradio_flat-green'})
    cariBhpFarmasi($('select[name="caribhp"]'));
    //load dropdown diagnosa/tindakan/radiologi/laboratorium 
    diagnosa_or_tindakan($('select[name="carivitalsign"]'),1);
    diagnosa_or_tindakan($('select[name="caridiagnosa"]'),2,10);
    diagnosa_or_tindakan($('select[name="caridiagnosa9"]'),2,9);
    diagnosa_or_tindakan($('select[name="caritindakan"]'),3);
    diagnosa_or_tindakan($('select[name="carilaboratorium"]'),4);
    diagnosa_or_tindakan($('select[name="cariradiologi"]'),5);
    diagnosa_or_tindakan($('select[name="caridiskon"]'),8);
    pemeriksaanklinikRefreshBhp(idpendaftaran,'rajal');
    $('input[name="norm"]').val(localStorage.getItem('norm'));
    //pemeriksaanklinikRefreshAlergi(norm);
    $('[data-toggle="tooltip"]').tooltip();
    
    startTimeLayanan();
    var mulai = $('#waktusekarang').val();
    $('#waktumulai').val(mulai);
});

function cariBhpFarmasi(selectNameTag)//cari bhp
{
    selectNameTag.select2({
    placeholder: "Pilih Obat/Bhp",
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: base_url+"cpelayanan/pemeriksaanklinik_cariobatbhprawatjalan",
        dataType: 'json',
        delay: 100,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                carabayar:$('input[name="carabayar"]').val(),
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.idbarangpembelian + ',' +item.idbarang, text: item.namabarang +'  | stok '+ convertToRupiah(item.stok) +' '+ item.namasatuan +' | harga '+ convertToRupiah(item.hargajual)  + ((item.kadaluarsa==0) ? '' : ' | Exp:'+ item.kadaluarsa )  }}),
                pagination: {
                    more: true
                }
            };
        },              
    }
    });
}
// pemeriksaan_settbhp
function inputBhpFarmasi(value)//input bhp
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_inputbhp",
        data:{x:value, i:idperiksa,modesave:$('input[name="modesave"]').val()},
        dataType:"JSON",
        success:function(result){
            if(isselesaiperiksa(result.status))
            {
                if(result.status=='success')
                {
                    pemeriksaanklinikRefreshBhp(idpendaftaran);
                    $('select[name="caribhp"]').empty();
                }
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
//---Vital Sign
function pilihvitalsign(value,ispaket)
{   
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    var idperiksa = $('input[name="idperiksa"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahvitalsign",
        data:{x:value, y:idpendaftaran,ispaket:ispaket, z:idperiksa,modesave:$('input[name="modesave"]').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshVitalsign(idpendaftaran); //refresh vital sign
                }
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshVitalsign(value)//refresh Data Vital Sign
{
    var x=0, html='', paket='';
    $('#listVitalsign').empty();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listvitalsign',
        data: {x:value, y:idperiksa,modelist:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                paket = ((namapaket!=result[x].namapaketpemeriksaan)? '<tr height="30px"><td class="text-bold" colspan="5">'+result[x].namapaketpemeriksaan+' </td><td class=""><a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="hapuspaketparent('+result[x].idgrup+',\'vitalsign\''+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td><tr>' : ''); 
                html += paket + '<tr><td>'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                '<td> <input type="hidden" name="icdvitalsign[]" value="'+result[x].icd+'"/><input type="hidden" id="istypehasil'+result[x].idhasilpemeriksaan+'" value="'+result[x].istext+'"/>'+
                '<input id="'+result[x].idhasilpemeriksaan+'" type="text" class="form-pelayanan" value="'+ result[x].nilai +'" onchange="savenilaipemeriksaan_vitalsign('+result[x].idhasilpemeriksaan+')" placeholder="Input '+result[x].istext+'" name="nilaivitalsign[]" size="1" /></td>'+
                '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                '<td>'+ result[x].satuan +'</td>'+
                '<td><a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusvitalsign" icd="'+result[x].icd+' - '+result[x].namaicd+'" vid="'+result[x].idhasilpemeriksaan+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>';
                var namapaket = result[x].namapaketpemeriksaan;
            }
            $('#listVitalsign').html(html);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error: function(result) {
            fungsiPesanGagal();
            return false;
        }
    });
}
//hapus vital sign
$(document).on('click','#hapusvitalsign',function()
{
    var ICD = $(this).attr('icd');
    var x = $(this).attr('vid');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+ICD+' </b> <br>Hapus Dari Vital Sign.?',
        buttons: {
            Hapus: function () {                
                startLoading();
                var idpendaftaran = $('input[name="idpendaftaran"]').val();
                $.ajax({
                    type: "POST",
                    url: 'pemeriksaan_hapusvitalsign',
                    data: {x:x,modesave:$('input[name="modesave"]').val()},
                    dataType: "JSON",
                    success: function(result) {
                        stopLoading();
                        notif(result.status, result.message);
                        if(result.status=='success')
                        {
                            pemeriksaanRefreshVitalsign(idpendaftaran);
                        }
                    },
                    error: function(result) {
                        stopLoading();
                        fungsiPesanGagal();
                        return false;
                    }
                });
            },
            Batal: function () {               
            }            
        }
    });  
})
//---DIAGNOSA icd 10
function pilihdiagnosa(value,idicd, level)
{
    var idp = $('input[name="idpendaftaran"]').val();
    var idpr = $('input[name="idperiksa"]').val();
    if(level=='primer')
    {
        $.ajax({
            type: "POST",
            url: 'pemeriksaan_cekdiagnosaprimer',
            data: {x:idpr,modelist:$('input[name="modesave"]').val(),idicd:idicd},
            dataType: "JSON",
            success: function(result){
                if(result.status > 0){notif('warning', 'ICD '+idicd+' dengan level primer sudah ada!');}
                else{simpandiagnosa(value,level,idicd,idp,idpr);}
            },
            error: function(result){ //jika error
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
    }else{
        simpandiagnosa(value,level,idicd,idp,idpr);
    }
}
function simpandiagnosa(value,level,idicd,idp,idpr)
{
    startLoading();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahdiagnosa",
        data:{x:value, y:idp,z:idpr,idicd:idicd, l:level,modesave:$('input[name="modesave"]').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshDiagnosa(idp,idicd); //refresh diagnosa
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshDiagnosa(value,idicd)//refreshDataDiagnosa
{
    var x=0, html='';
    $('#listDiagnosa').empty();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listdiagnosa',
        data: {x:value,idicd:idicd,modelist:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result){
            for(x in result){html += '<tr><td>'+ result[x].icd +'</td><td>'+ result[x].namaicd +'</td><td>'+ result[x].icdlevel +'</td><td>'+ result[x].aliasicd +'</td><td><a id="hapusdiagnosa" class="btn btn-default btn-xs" data-toggle="tooltip" idicd="'+idicd+'" namaicd="'+result[x].namaicd+'"  alt2="'+result[x].icd+'" alt="'+result[x].idhasilpemeriksaan+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>';}
            
            $('#listDiagnosa'+idicd).html(html);
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
//hapus diagnosa
$(document).on('click','#hapusdiagnosa',function(){
    var x       = $(this).attr('alt');
    var icd     = $(this).attr('alt2');
    var namaicd = $(this).attr('namaicd');
    var idicd   = $(this).attr('idicd');
   
   $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+icd+' - '+namaicd+' </b> <br>Hapus Dari ICD '+idicd+ ((idicd==10)?' (Diagnosa) ':' (Tindakan) ') + '.?',
        buttons: {
            Hapus: function () {                
                hapusdiagnosa(x,icd,idicd);
            },
            Batal: function () {               
            }            
        }
    });  
});
function hapusdiagnosa(x,icd,idicd)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapusdiagnosa',
        data: {x:x,modesave:$('input[name="modesave"]').val(),icd:icd,idp:$('input[name="idpendaftaran"]').val()},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanRefreshDiagnosa(idpendaftaran,idicd); //refresh diagnosa
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////TINDAKAN////////////////
function pilihtindakan(value)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    var idperiksa = $('input[name="idperiksa"]').val();
    var idunit = $('input[name="idunit"]').val();
    var icd = value;
    $('select[name="caritindakan"]').val('');
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahtindakan",
        data:{x:icd, y:idpendaftaran,z:idperiksa,modesave:$('input[name="modesave"]').val(),u:idunit},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshTindakan(idpendaftaran); //refresh diagnosa
                }
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshTindakan(value)//refreshDataDiagnosa
{
    var modelist = $('input[name="modesave"]').val();
    var x=0,no=0, html='', subtotal=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listtindakan',
        data: {x:value,modelist:modelist},
        dataType: "JSON",
        success: function(result) {
            for(x in result.tindakan)
            {
                ++no;
                subtotal += parseInt(result.tindakan[x].total) - parseInt(result.tindakan[x].potongantagihan);
                
                if(modelist=='verifikasi')
                {
                    html += '<tr><td>'+ result.tindakan[x].icd +'</td>'
                    +'<td>'+ result.tindakan[x].namaicd +'</td>'
                    +'<td>'+((result.tindakan[x].total==0) ? '' : '<select onchange="updatePegawaiDokter('+ no +')" style="display:inline; width:100%" class="form-pelayanan dokselect2" id="comboPegawaiDokter'+no+'"><option>Pilih</option></select>') +'</td>'
                    +'<td class="text-center">'+ result.tindakan[x].jumlah +'</td>'
                    +'<td>'+((result.tindakan[x].total==0) ? '' : convertToRupiah(result.tindakan[x].total) )+'</td>'
                    +'<td>'+((result.tindakan[x].potongantagihan==0) ? '' : convertToRupiah(result.tindakan[x].potongantagihan) )+'</td>'
                    +'<td></td></tr>';
                }
                else
                {
                    html += '<tr><td>'+ result.tindakan[x].icd +'</td>'
                    +'<td>'+ result.tindakan[x].namaicd +'</td>'
                    +'<td>'+((result.tindakan[x].total==0) ? '' : '<select onchange="updatePegawaiDokter('+ no +')" style="display:inline; width:100%" class="form-pelayanan dokselect2" id="comboPegawaiDokter'+no+'"><option>Pilih</option></select>') +'</td>'
                    +'<td class="text-center">'+((result.tindakan[x].total==0) ? '' : '<input id="idTindakan'+ no +'" type="hidden" value="'+result.tindakan[x].idhasilpemeriksaan+'"><input type="text" class="form-pelayanan text-center" id="setjmlTindakan'+no+'" value="'+ result.tindakan[x].jumlah +'" onchange="set_jmltindakan('+no+',\'Tindakan\''+')" />')+'</td>'
                    +'<td>'+((result.tindakan[x].total==0) ? '' : convertToRupiah(result.tindakan[x].total) )+'</td>'
                    +'<td>'+((result.tindakan[x].potongantagihan==0) ? '' : convertToRupiah(result.tindakan[x].potongantagihan) )+'</td>'
                    +'<td> '+
                          menuUbahAsuransiHasilPemeriksaan(result.tindakan[x].idhasilpemeriksaan,result.tindakan[x].jaminanasuransi,'tindakan')
                          +' <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result.tindakan[x].idhasilpemeriksaan+',\'Tindakan\''+')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>'
                          +' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapustindakan" tid="'+result.tindakan[x].idhasilpemeriksaan+'" icd="'+result.tindakan[x].icd+'" namaicd="'+result.tindakan[x].namaicd+'"  data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                          +'</td></tr>';
                }
                
            isicombopegawaidokter('comboPegawaiDokter'+no,result.tindakan[x].iddokterjasamedis);
            }
            html+='<tr class="bg bg-info"><td colspan="4">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listTindakan').empty();
            $('#listTindakan').html(html);
            $('#totalPemeriksaan').empty();
            $('#totalPemeriksaan').html(convertToRupiah(bulatkanRatusan(parseInt(result.total[0].totalseluruh1) + parseInt(result.total[0].totalseluruh2))));
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
            $('.dokselect2').select2();
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}


function pemeriksaanRefreshDiskon(value)//refreshDataDiskon
{
    var x=0,no=0, html='', subtotal=0;
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listdiskon',
        data: {x:value,modelist:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {
                ++no;
                subtotal += parseInt(result[x].total);
                html += '<tr>'
                +'<td>'+ result[x].icd +'</td>'
                +'<td>'+ result[x].namaicd +'</td>'
                +'<td>'+ convertToRupiah(result[x].nominal)+ ((result[x].jenisdiskon=='nominal') ? '' : '%' ) +'</td>'
                +'<td>'+ convertToRupiah(result[x].total)+'</td>'
                +'<td> <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="hapusdiskon('+result[x].idhasilpemeriksaan+')" data-original-title="Hapus"><i class="fa fa-trash"></i></a>'
                +'</td>'
                +'</tr>';                
            }
            html+='<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>';
            $('#listDiskon').empty();
            $('#listDiskon').html(html);
//            $('#totalPemeriksaan').empty();
//            $('#totalPemeriksaan').html(convertToRupiah(bulatkanRatusan(parseInt(result.total[0].totalseluruh1) + parseInt(result.total[0].totalseluruh2))));
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
//            $('.dokselect2').select2();
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function pilihdiskon(value)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    var idperiksa = $('input[name="idperiksa"]').val();
    var idunit = $('input[name="idunit"]').val();
    var icd = value;
    $('select[name="caridiskon"]').val('');
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahdiskon",
        data:{x:icd, y:idpendaftaran,z:idperiksa,modesave:$('input[name="modesave"]').val(),u:idunit},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshDiskon(idpendaftaran); //refresh diagnosa
                }
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusdiskon(idhp)
{
    hapustindakan(idhp);
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    pemeriksaanRefreshDiskon(idpendaftaran);
}

// update pegawai dokter
function updatePegawaiDokter(no)
{
    $.ajax({ 
        url: 'hasilpemeriksaan_updatePegawaiDokter',
        type : "post",      
        dataType : "json",
        data : {idhp:$('#idTindakan'+no).val(),idpd:$('#comboPegawaiDokter'+no).val()},
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);
                 pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val());             
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            notif(result.status, result.message);
            return false;
        }
        
    });
}
// pilih pegawaidokter
function isicombopegawaidokter(id, selected)
{
    if (is_empty(localStorage.getItem('localStoragePegawaiDokter')))
    {
        $.ajax({ 
            url: base_url+'cadmission/masterjadwal_caridokter',
            type : "POST",      
            dataType : "JSON",
            success: function(result) {                                                                   
                localStorage.setItem('localStoragePegawaiDokter', JSON.stringify(result));
                fillpegawaidokter(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillpegawaidokter(id, selected);
            },
            error: function(result){
                fillpegawaidokter(id, selected);
            }
        });
    }
}
function fillpegawaidokter(id, selected)
{
    var pegawaidokter = JSON.parse(localStorage.getItem('localStoragePegawaiDokter'));
    var i=0;
    for (i in pegawaidokter)
    {
      $("#"+id).append('<option ' + ((pegawaidokter[i].idpegawai===selected)?'selected':'') + ' value="'+pegawaidokter[i].idpegawai+'">'+pegawaidokter[i].namalengkap+'</option>');
    }
}
/// -- seting ubah jumlah tindakan
function set_jmltindakan(no,type)
{
    var idtindakan = $('#id'+type+no).val();
    var jumlah = $('#setjml'+type+no).val();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    if(jumlah=='')
    {
        $('#setjmltindakan').val(1);
        $('#setjmltindakan').focus();
        alert_empty('jumlah');//jika kosong tampilkan notif

    }
    else
    {
        startLoading();
        $.ajax({
        type: "POST",
        url: 'pemeriksaan_ubahjumlahtindakan',
        data: {id:idtindakan,jumlah:jumlah},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(type!=='Radiologi')
                {
                    pemeriksaanRefreshTindakan(idpendaftaran);
                }
                else
                {
                    pemeriksaanRefreshRadiologi(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
    }
}

//hapus tindakan di klik
$(document).on('click','#hapustindakan',function(){
    var x = $(this).attr('tid');
    var icd = $(this).attr('icd');
    var namaicd = $(this).attr('namaicd');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+icd+' - '+namaicd+' </b> <br>Hapus Dari Tindakan.?',
        buttons: {
            Hapus: function () {                
                hapustindakan(x);
            },
            Batal: function () {               
            }            
        }
    });  
})

function hapustindakan(x)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapustindakan',
        data: {x:x,modesave:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshTindakan(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function isselesaiperiksa(status)
{
    if(status=='selesaiperiksa')
    {
        $.alert('<b>Pemeriksaan Sudah Selesai</b><div class="text text-red">Perubahan Tidak Tersimpan.</div><br> Mohon Melakukan Tambah/Ubah/Hapus Tindakan Sebelum Pasien Menyelesaikan Pembayaran.');
        return false;
    }
    return true;
    
}
/////////////////////////////////////////////////
//////////////////////////RADIOLOGI////////////////
function pilihradiologi(value)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    var idperiksa = $('input[name="idperiksa"]').val();
    var icd = value;
    $('select[name="cariradiologi"]').val('');
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahradiologi",
        data:{x:icd, y:idpendaftaran, z:idperiksa,modesave:$('input[name="modesave"]').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshRadiologi(idpendaftaran); //refresh diagnosa
                }
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshRadiologi(value)//refreshDataDiagnosa
{
    var x=0, html='', subtotal=0;
    $('#listRadiologi').empty();
    var modelist = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listradiologi',
        data: {x:value,modelist:modelist},
        dataType: "JSON",
        success: function(result) {
            var no=0;
            for(x in result)
            { 
                no++;
                subtotal += parseInt(result[x].total) - parseInt(result[x].potongantagihan);
                if(modelist=='verifikasi')
                {
                    html += '<tr><td>'+ result[x].icd +'</td>'
                    +'<td>'+ result[x].namaicd +'</td>'
                    +'<td>'+ result[x].jumlah +'</td>'
                    +'<td>'+ convertToRupiah(result[x].total) +'</td>'
                    +'<td>'+ convertToRupiah(result[x].potongantagihan) +'</td>'
                    +'<td> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusradiologi" rid="'+result[x].idhasilpemeriksaan+'" icd="'+result[x].icd+' - '+result[x].namaicd+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a> </td></tr>';
                }
                else
                {
                    html += '<tr><td>'+ result[x].icd +'</td>'
                    +'<td>'+ result[x].namaicd +'</td>'
                    +'<td><input id="idRadiologi'+ ++no +'" type="hidden" value="'+result[x].idhasilpemeriksaan+'"><input type="text" class="form-pelayanan text-center" id="setjmlRadiologi'+no+'" value="'+ result[x].jumlah +'" onchange="set_jmltindakan('+no+',\'Radiologi\''+')"/></td>'
                    +'<td>'+ convertToRupiah(result[x].total) +'</td>'
                    +'<td>'+ convertToRupiah(result[x].potongantagihan) +'</td>'
                    +'<td> '+menuUbahAsuransiHasilPemeriksaan(result[x].idhasilpemeriksaan,result[x].jaminanasuransi,'radiologi')+' <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idhasilpemeriksaan+',\'Radiologi\''+')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>'
                    +' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapusradiologi" rid="'+result[x].idhasilpemeriksaan+'" icd="'+result[x].icd+' - '+result[x].namaicd+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a></td></tr>';
                }
            }
            $('#listRadiologi').html(html+'<tr class="bg bg-info"><td colspan="3">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function simpanhasilradiologi(id)
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_simpanhasilradiologi',
        data: {i:id, h: $('#'+id).val()},
        dataType: "JSON",
        success: function(result) {
            notif(result.status, result.message);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
//hapus radiologi
$(document).on('click','#hapusradiologi',function(){
    var x = $(this).attr('rid');
    var icd = $(this).attr('icd');
   $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+icd+' </b> <br>Hapus Dari Radiologi.?',
        buttons: {
            Hapus: function () {                
                hapusradiologi(x);
            },
            Batal: function () {               
            }            
        }
    });  
});
function hapusradiologi(x)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapusradiologi',
        data: {x:x,modesave:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshRadiologi(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
//////////////////////////Laboratorium////////////////
function pilihlaboratorium(value,ispaket)
{
    startLoading();
    $('select[name="carilaboratorium"]').val('');
    $('select[name="paketlaboratorium"]').val('');
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tambahlaboratorium",
        data:{x:value, y:idpendaftaran,ispaket:ispaket,z:idperiksa,modesave:$('input[name="modesave"]').val()},
        dataType:"JSON",
        success:function(result){
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshLaboratorium(idpendaftaran); //refresh diagnosa
                }
            }
        },
        error:function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaanRefreshLaboratorium(value)//refreshLaborat
{
    var x=0, html='', paket='', parent='', subtotal=0, keteranganhasil='';
    var modelist = $('input[name="modesave"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listlaboratorium',
        data: {x:value, y:idperiksa,modelist:modelist},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {   
                if(result[x].idjenisicd2=='4')
                {
                    //tampilkan keterangan hasil laborat
                    if(result[x].idgrup != idgrup && idgrup!=null)
                    {
                         html += '<tr ><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
                         keteranganhasil='';
                    }
                    
                    if(modelist=='verifikasi')
                    {
                        paket = ((namapaket==result[x].namapaketpemeriksaan && gruppaket==result[x].gruppaketperiksa) ? '' : ((result[x].namapaketpemeriksaan==null)?'':'<tr height="30px" '+((result[x].idgrup==result[x].idpaketpemeriksaan)? 'class="bg-info"' : '' )+'><td colspan="5" class="text-bold">'+result[x].namapaketpemeriksaan+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].total : '' )+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].potongantagihan : '' )+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? menuKesimpulanHasil(result[x]) +' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuspaketlaborat" plidgrup="'+result[x].idgrup+'" plgruppaket="'+result[x].gruppaketperiksa+'" icd="'+result[x].namapaketpemeriksaan+'"  data-original-title="Hapus"><i class="fa fa-trash"></i></a>' : '' )+'</td></tr>' ));
                        subtotal += parseInt(result[x].total) - parseInt(result[x].potongantagihan);
                        html += paket + ((result[x].icd==null)?'': '<tr '+((result[x].idgrup!==null)?'':'class="bg-warning"')+'><td> &nbsp;&nbsp;&nbsp;'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                        ///////set untuk simpan data
                        '<td>'+
                        formInputHasilLaborat(result[x]) +'</td>'+
                        '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                        '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                        '<td>'+ result[x].satuan +'</td>'+
                        '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].total) : '' )+'</td>'+
                        '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].potongantagihan) : '' )+'</td>'+
                        '<td>' + ((result[x].idgrup ==null) ? ' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuslaborat" lid="'+result[x].idhasilpemeriksaan+'" icd="'+result[x].icd+' - '+result[x].namaicd+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a>' : '' ) +'</td></tr>');
                    }
                    else
                    {
                        paket = ((namapaket==result[x].namapaketpemeriksaan && gruppaket==result[x].gruppaketperiksa) ? '' : ((result[x].namapaketpemeriksaan==null)?'':'<tr height="30px" '+((result[x].idgrup==result[x].idpaketpemeriksaan)? 'class="bg-info"' : '' )+'><td colspan="5" class="text-bold">'+result[x].namapaketpemeriksaan+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].total : '' )+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].potongantagihan : '' )+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? menuKesimpulanHasil(result[x]) +' '+ menuUbahAsuransiHasilPemeriksaan(result[x].idhasilpemeriksaan,result[x].jaminanasuransi,'laboratorium') +' <a class="btn btn-xs btn-default" onclick="print_laboratorium('+$('input[name="idperiksa"]').val()+','+$('input[name="idpendaftaran"]').val()+','+$('input[name="idunit"]').val()+','+result[x].idgrup+')" '+tooltip('Cetak')+' ><i class="fa fa-print"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idhasilpemeriksaan+',\'Laboratorium\''+')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a> <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuspaketlaborat" plidgrup="'+result[x].idgrup+'" plgruppaket="'+result[x].gruppaketperiksa+'" icd="'+result[x].namapaketpemeriksaan+'"  data-original-title="Hapus"><i class="fa fa-trash"></i></a>' : '' )+'</td></tr>' ));
                        subtotal += parseInt(result[x].total) - parseInt(result[x].potongantagihan);
                        html += paket + ((result[x].icd==null)?'': '<tr '+((result[x].idgrup!==null)?'':'class="bg-warning"')+'><td> &nbsp;&nbsp;&nbsp;'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                        ///////set untuk simpan data
                        '<td><input type="hidden" name="icd[]" value="'+result[x].icd+'"/><input type="hidden" id="istypehasil'+result[x].idhasilpemeriksaan+'" value="'+result[x].istext+'"/>'+
                        formInputHasilLaborat(result[x]) +'</td>'+
                        '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                        '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                        '<td>'+ result[x].satuan +'</td>'+
                        '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].total) : '' )+'</td>'+
                        '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].potongantagihan) : '' )+'</td>'+
                        '<td> ' + ((result[x].idgrup ==null) ? menuUbahAsuransiHasilPemeriksaan(result[x].idhasilpemeriksaan,result[x].jaminanasuransi,'laboratorium') +'<a class="btn btn-default btn-xs" data-toggle="tooltip" onclick="updateBiayahasilperiksa('+result[x].idhasilpemeriksaan+',\'Laboratorium\''+')" data-original-title="Ubah Tarif"><i class="fa fa-pencil"></i></a>'+
                        ' <a class="btn btn-default btn-xs" data-toggle="tooltip" id="hapuslaborat" lid="'+result[x].idhasilpemeriksaan+'" icd="'+result[x].icd+' - '+result[x].namaicd+'" data-original-title="Hapus"><i class="fa fa-trash"></i></a>' : '' ) +'</td>'
                        +'</tr>');
                    }
                    
                    var idpaketpemeriksaan = result[x].idpaketpemeriksaan;
                    if(result[x].keteranganhasil !=null && result[x].keteranganhasil != keteranganhasil)
                    {
                        keteranganhasil = result[x].keteranganhasil;
                    }
                    var idgrup = result[x].idgrup;
                    var namapaket= result[x].namapaketpemeriksaan;
                    var gruppaket=result[x].gruppaketperiksa;    
                    
                }
            }
            //tampilkan keterangan hasil laborat
            html += '<tr ><td colspan="7" style="padding-left:15px;">'+keteranganhasil+'</td></tr>'; 
            
            $('#listLaboratorium').empty();
            $('#listLaboratorium').html(html+'<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function formInputHasilLaborat(result)
{
    var form='';
    if(result.istext=='text' || result.istext=='textnumerik')
    {
        form = '<textarea '+ ((result.isbolehinput>0)? '' : 'readonly' ) +' onchange="savenilaipemeriksaan_laborat('+result.idhasilpemeriksaan+',\'laborat\')" id="'+result.idhasilpemeriksaan+'" class="form-control" placeholder="Input '+result.istext+'" rows="2">'+ result.nilai +'</textarea>';
    }
    else if(result.istext=='numerik')
    {
        form = '<input id="'+result.idhasilpemeriksaan+'" type="text" class="form-pelayanan" value="'+ result.nilai +'" onchange="savenilaipemeriksaan_laborat('+result.idhasilpemeriksaan+',\'laborat\')"  placeholder="Input '+result.istext+'" name="nilai[]" size="1" '+ ((result.isbolehinput>0)? '' : 'readonly' ) +' />';
    }
    else
    {
        form ='';
    }
    
    return form;
}
function menuKesimpulanHasil(hasil)
{
    return '<a id="inputeditkesimpulanhasil" nama="'+hasil.namapaketpemeriksaan+'" idgrup="'+hasil.idgrup+'" gruppaket="'+hasil.gruppaketperiksa+'" class="btn btn-xs btn-default" '+tooltip('Input/Edit Kesimpulan Hasil')+'><i class="fa fa-file-text"></i></a>';
}

//Insert Update Kesimpulan Hasil Laboratorium
$(document).on('click','#inputeditkesimpulanhasil',function(){
    var idpemeriksaan = $('input[name="idperiksa"]').val();
    var nama = $(this).attr('nama');
    var idgruppaket  = $(this).attr('idgrup');
    var gruppaket  = $(this).attr('gruppaket');
    
    var konten = '<b>Paket Laborat : '+nama+' </b> </br>'
                +'<b>Kesimpulan</b> : <span id="menugeneratekesimpulan"></span><input type="hidden" id="mode_kesimpulanhasil"/> <textarea id="kesimpulanlaborat" class="form-control textkesimpulan" rows="12"></textarea>'
    $.confirm({
        icon: 'fa fa-check-square-o',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        columnClass:'l',
        title: 'Kesimpulan Hasil',
        content: konten,
        buttons: {
            simpan: function () { 
                $.ajax({
                    url:base_url+"cpelayanan/insert_update_get_kesimpulanhasil",
                    type:"POST",
                    dataType:"JSON",
                    data:{
                        idpemeriksaan:idpemeriksaan,
                        idpaketpemeriksaan:idgruppaket,
                        gruppaketperiksa:gruppaket,
                        kesimpulan:$('#kesimpulanlaborat').val(),
                        mode:$('#mode_kesimpulanhasil').val()
                    },
                    success:function(result){
                        notif(result.status, result.message);
                        var idpendaftaran = $('input[name="idpendaftaran"]').val();
                        pemeriksaanRefreshLaboratorium(idpendaftaran);
                    },
                    error:function(result){ 
                        fungsiPesanGagal();
                        return false;
                    }
                });            
            },
            batal: function () {               
            }            
        },
        onContentReady: function () { // bind to events 
           startLoading();
           $.ajax({
                url:base_url+"cpelayanan/insert_update_get_kesimpulanhasil",
                type:"POST",
                dataType:"JSON",
                data:{
                    idpemeriksaan:idpemeriksaan,
                    idpaketpemeriksaan:idgruppaket,
                    gruppaketperiksa:gruppaket,
                    mode:'getdata'
                },
                success:function(result){
                    stopLoading();
                    $('#mode_kesimpulanhasil').val( ((result!=null) ? 'updatedata' : 'insertdata' ) );
                    $('#kesimpulanlaborat').val( ((result!=null) ? result.keteranganhasil : '') );
                    $('[data-toggle="tooltip"]').tooltip();
                    $('.textkesimpulan').wysihtml5();
                },
                error:function(result){ 
                    stopLoading();
                    fungsiPesanGagal();
                    return false;
                }
            }); 
        }
    });
});


$(document).on('click','#hapuslaborat',function(){
    var x = $(this).attr('lid');
    var icd = $(this).attr('icd');
   
   $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+icd+' </b> <br>Hapus Dari Laboratorium.?',
        buttons: {
            Hapus: function () {                
                hapuslaboratorium(x);
            },
            Batal: function () {               
            }            
        }
    });  
});

function hapuslaboratorium(x)
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuslaboratorium',
        data: {x:x,modesave:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanRefreshLaboratorium(idpendaftaran);
                }
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

//hapus paket parent laborat
//onclick="hapuspaketparent('+result[x].idgrup+',\'laboratorium\','+result[x].gruppaketperiksa+')"
$(document).on('click','#hapuspaketlaborat',function(){
    var gruppaketperiksa = $(this).attr('plgruppaket');
    var x = $(this).attr('plidgrup');
    var z = 'laboratorium';
    var icd = $(this).attr('icd');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<b> '+icd+' </b> <br>Hapus Dari Laboratorium.?',
        buttons: {
            Hapus: function () {                    
            hapuspaketparent(x,z,gruppaketperiksa);
            },
            Batal: function () {               
            }            
        }
    }); 
});

function hapuspaketparent(x,z,gruppaketperiksa='')
{
    startLoading();
    var idpendaftaran = $('input[name="idpendaftaran"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_hapuspaketpemeriksaan',
        data: {x:x, y:idpendaftaran,gpp:gruppaketperiksa,modesave:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            if(isselesaiperiksa(result.status))
            {
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    if(z=='laboratorium')
                    {
                        pemeriksaanRefreshLaboratorium(idpendaftaran);
                    }
                    else
                    {
                        pemeriksaanRefreshVitalsign(idpendaftaran);
                    }
                }
            }
        },
        error: function(result) { //jika error
            stopLoading();
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
/////////////////////////////////////////////////
// mahmud, clear --> save 
function pemeriksaan_savenilaipemeriksaan(value,jenisperiksa,user='')
{
    var istext = $('#istypehasil'+value).val();
    var nilai = $('#'+value).val();
    $.ajax({
            type:"POST",
            url:"pemeriksaan_savenilaipemeriksaan",
            data:{a:value,b:nilai,c:istext,d:idperiksa,u:user},
            dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    var idp = $('input[name="idpendaftaran"]').val();
//                    if(jenisperiksa=='laboratorium'){
//                        pemeriksaanRefreshLaboratorium(idp);
//                    }
                    
//                    else if(jenisperiksa=='vitalsign'){
//                        pemeriksaanRefreshVitalsign(idp);
//                    }
                }
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });


}

//simpan hasil periksa laboratorium
function savenilaipemeriksaan_laborat(value,user='')
{
    pemeriksaan_savenilaipemeriksaan(value,'laboratorium',user);
}
//simpan hasil periksa vitalsign
function savenilaipemeriksaan_vitalsign(value,user='')
{
    pemeriksaan_savenilaipemeriksaan(value,'vitalsign',user);
}
//////////////////////diagnosa_or_tindakan//////////////////
function diagnosa_or_tindakan(x,y,z='')
{
    startLoading();
    x.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanklinik_diagnosa",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            stopLoading();
            return {
                q: params.term,
                jenisicd: y,
                idicd:z,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.icd + ' | ' + item.namaicd + ((item.aliasicd!='') ? ' / ' : '' )+ item.aliasicd}}),
            };
        },              
    }
    });
}
///////////////////////////////////////////////
function pilihJenisIcd(value)//pilih jenis diagnosa
{
    pilihPaketDiagnosa($('select[name="paket"]'),value);
}
function singleDiagnosa(value)//tambah single diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addsinglediagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function paketDiagnosa(value)//tambah paket diagnosa
{
    $.ajax({
        type:"POST",
        url:"diagnosa_addpaketdiagnosa",
        data:{x:value, y:$('input[name="idpendaftaran"]').val()},
        dataType:"JSON",
        success:function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa(); //refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pilihSingleDiagnosa(selectNameTag,icd) //pilih diagnosa 
{  
    selectNameTag.select2({
    minimumInputLength: 3,
    allowClear: true,
    ajax: {
        url: "pemeriksaanklinik_cariicd",
        dataType: 'json',
        delay: 150,
        cache: false,
        data: function (params) {
            return {
                q: params.term,
                icd:icd,
                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
            var page = params.page || 1; 
            return {
                results: $.map(data, function (item) { return {id: item.icd, text: item.namaicd + ((item.aliasicd!='') ? ' >>> ' : '' )+ item.aliasicd}}),
                pagination: {
                    more: true
                }
            };
        },              
    }
    });
}
function pilihPaketDiagnosa(htmlTagName,icd) //pilih diagnosa 
{  
    $.ajax({
        url:'diagnosa_pilihpaket',
        type:'POST',
        dataType:'JSON',
        data:{x:icd},
        success: function(result){
            var select='<option value="0" >Pilih</option>';
            htmlTagName.empty();
            for(i in result)
            {
                select = select + '<option value="'+ result[i].idpaketpemeriksaan +'" >' + result[i].namapaketpemeriksaan +'</option>';
            }
            htmlTagName.html(select);
            $('.select2').select2();
            pilihSingleDiagnosa($('select[name="single"]'),icd);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    }); 
}
function pemeriksaanklinikDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanklinik_caridetailpasien',
        data: {norm:norm, i:idperiksa,modesave:$('input[name="modesave"]').val()},
        dataType: "JSON",
        success: function(result) {
            //tampil menu ubah dokter
            if(result.pasien.aksesubahdokter=='1'){
                $('#viewMenuUbahDokter').html('<a id="pemeriksaanklinik_ubahdokter" class="btn btn-warning btn-xs"><i class="fa fa-user-md"></i> Ubah Dokter</a>');
            }
            
            if(result.pasien.ubahloketsaatperiksa=='1'){
                $('#viewMenuUbahLoket').html('<a id="pemeriksaanklinik_ubahloket" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Ubah Loket</a>');
            }
//            $('#validasidataobyektif').val(result.grupjadwal[0].validasidataobyektif);
//            $('#validasidatasubyektif').val(result.grupjadwal[0].validasidatasubyektif);
//            $('#validasiicd10').val(result.grupjadwal[0].validasiicd10);
            pemeriksaan_inputAnamnesa(result.grupjadwal);
            // $('#adaracikan').attr('checked',true);
            // $('checked[name="adaracikan"]').attr('checked');
            ((result.pasien.adaracikan==1)? $('#adaracikan').attr('checked',true):$('#adaracikan').attr('checked',false));
            $('input[name="dokter"]').val(if_empty(result.periksa.namalengkap));
            $('textarea[name="alergiobat"]').val(result.pasien.alergi);
            //kategori skrining
            edit_skrining($('#skrining'),result.skrining,result.pasien.kategoriskrining);
            
//            $('textarea[name="anamnesa"]').val((result.periksa.anamnesa));
            $('textarea[name="diagnosa"]').val(result.periksa.diagnosa);
            $('textarea[name="keterangan"]').val(result.periksa.keterangan);
            $('textarea[name="rekomendasi"]').val(result.periksa.rekomendasi);
            $('textarea[name="keteranganobat"]').val(result.pasien.keteranganobat);
            $('textarea[name="keteranganlaboratorium"]').val(result.pasien.keteranganlaboratorium);
            $('textarea[name="keteranganradiologi"]').val(result.pasien.keteranganradiologi);
            $('textarea[name="saranradiologi"]').val(result.pasien.saranradiologi);
            if(result.pasien.indikasipw!=0){
                $('#cindikasipasien').attr('checked',true);
                setFormIndikasiPasienWabah(true);
            }
            
            viewIdentitasPasien(result.pasien,result.blacklist, result.penanggung);//panggil view identitas
            // fungsiViewHasilPemeriksaanPasien(result.viewhasilpemeriksaan, result.hasilpemeriksaan);
            $('input[name="idpendaftaran"]').val(result.periksa.idpendaftaran);
            $('input[name="idperson"]').val(result.pasien.idperson);
            $('input[name="status"]').val(result.pasien.idstatuskeluar);
            $('input[name="carabayar"]').val(result.pasien.carabayar);
            //refresh diagnosa
            pemeriksaanRefreshDiagnosa(result.periksa.idpendaftaran,9); //refresh diagnosa 9
            pemeriksaanRefreshDiagnosa(result.periksa.idpendaftaran,10); //refresh diagnosa 10
            pemeriksaanRefreshDiskon(result.periksa.idpendaftaran);
            pemeriksaanRefreshTindakan(result.periksa.idpendaftaran);
            pemeriksaanRefreshRadiologi(result.periksa.idpendaftaran);
            pemeriksaanRefreshLaboratorium(result.periksa.idpendaftaran);
            pemeriksaanRefreshVitalsign(result.periksa.idpendaftaran);
            $('input[name="idunit"]').val(result.periksa.idunit);
            $('input[name="idloket"]').val(result.pasien.idloket);
            $('input[name="namaloket"]').val(result.pasien.namaloket);
            if( result.pasien.carabayar !='jknpbi' && result.pasien.carabayar !='jknnonpbi' && result.pasien.carabayar !='bpjstenagakerja')
            {
                $('.pasien-prolanis').addClass('hide');
            }
            ((result.pasien.pasienprolanis==1)? $('#prolanis').attr('checked',true):$('#prolanis').attr('checked',false));
            $('#kunjunganberikutnya').datepicker({autoclose: true,format: "yyyy-mm-dd",orientation: "bottom"}).datepicker('setDate',result.pasien.tanggal_kunjunganberikutnya); //Initialize Date picker            
            
            list_enum('kondisikeluar',result.kondisikeluar,result.pasien.kondisikeluar);
            list_enum('statuspasienpulang',result.statuspasienpulang,result.pasien.statuspulang);
            stopLoading();
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
// mahmud, clear :: menambahkan inputan anamnesa
function pemeriksaan_inputAnamnesa(value)
{
    var v_inputdatasubyektifralan = sessionStorage.v_inputdatasubyektifralan;
    var dt='';
    for(x in value)
    {
        var namaunit = ((value.length == 1)?'':value[x].namaunit);
        dt+='<input id="idanamnesa'+x+'" type="hidden" name="grupanamnesa[]" value="'+value[x].idpemeriksaan+'">';
        dt+='<div class="form-group">'
                +'<label for="" class="col-sm-2 control-label">Data Subyektif '+namaunit+'</label>'
                +'<div class="col-sm-9"> <a class="btn btn-warning" onclick="pemeriksaanklinik_savedtsubyektif('+x+')">Simpan</a>'
                  +'<textarea id="namaanamnesa'+x+'" class="form-control textarea" name="anamnesa'+value[x].idpemeriksaan+'" placeholder="Anamnesa '+namaunit+'" rows="6" '+((v_inputdatasubyektifralan=='')? 'disabled' : '' )+'>'+value[x].anamnesa+'</textarea>'
                +'</div>'
              +'</div>';
    }
    $('#pemeriksaanInputAnamnesa').empty();
    $('#pemeriksaanInputAnamnesa').html(dt);
    $('.textarea').wysihtml5({toolbar:false});
}
function viewIdentitasPasien(value,blacklist,valpenanggung)
{
    localStorage.setItem('namapasien',value.namalengkap);
    localStorage.removeItem('namapasien');
    localStorage.setItem('namapasien',value.namalengkap);
    var profile='';
    var detail_profile='';
    var dtp = '';
    for(var x in valpenanggung){dtp+='<tr><td>'+valpenanggung[x].namalengkap+'</td><td>'+valpenanggung[x].hubungan+'</td><td>'+valpenanggung[x].notelpon+'</td></tr>';}
    var penanggung = '<table class="table table-bordered"><tr><th>Nama Penanggung</th><th>Hubungan </th><th> No.Telp</th></tr>'+dtp+'</table>';
    if(modehalamanjs!=='pemeriksaanklinik2'){
    profile = '<div class="bg-info">'
                +'<div class="box-body box-profile" >'
                 +'<div class="login-logo" style="margin-bottom:0px;">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-muted text-center">'+((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama )+'</p>'
                  +'<h5 class=" text-center">Usia '+ambilusia(value.tanggallahir, value.tglsekarang)+' </h5>'
                +'</div>'
              +'</div>';
    detail_profile = '<div class="bg-info"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>NIK </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nik+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-calendar margin-r-5"></i>Tanggal Lahir</strong></td>'
                  +'<td class="text-muted">: &nbsp;'+value.tanggallahir+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Tempat Lahir</strong></td>'
                  +'<td class="text-muted">: &nbsp;'+value.tempatlahir+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.jeniskelamin+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor SEP </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nosep+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' //'+value.golongandarah+' '+value.rh+'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RUJUKAN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norujukan+'</td>'

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.pendidikan+'</td>'

                  +((value.tanggalrujukan=='0000-00-00' || value.tanggalrujukan=='')? '<td colspan="2"></td>':'<td class="bg bg-red"><strong><i class="fa fa-circle-o margin-r-5"></i>Rujukan BPJS Selanjutnya</strong></td><td class="bg bg-red text-muted">: &nbsp; '+getNextDate(value.tanggalrujukan,90)+'</td>')

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i> Agama</strong></td>'
                  +'<td class="text-muted" >: &nbsp;'+value.agama+'</td>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Penjamin </strong></td>'
                +'<td class="text-muted">: &nbsp; '+value.carabayar+'</td>'
                +'</tr>'
        
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i>Alamat </strong></td>'
                  +'<td class="text-muted" width="250px">: &nbsp; '+value.alamat+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ayah </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.ayah+'</td>'
                +'</tr>'
                +'<tr><td colspan="2"><strong><i class="fa fa-circle-o margin-r-5"></i>Penanggung </strong></td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ibu </strong></td><td class="text-muted">: &nbsp; '+value.ibu+'</td></tr>'
                +'<tr><td colspan="2">'+penanggung+'</td></tr>'
                +'</tbody>'
             +'</table></div></div>';
         }else{
            profile = '<div class="login-logo" style="margin-bottom:0px;"><b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-md"></i></b></div>'
                  +'<p class="text-muted text-center">'+((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama )+'</p>'
                  +'<p class="text-center">'+value.norm+'</p>';
            detail_profile = '<h4 >Identitas Pasien </h4>   '

                  +'<table class="tbl">'
                      +'<tr>'
                        +'<td>Nama</td><td>: '+value.namalengkap+'</td>'
                        +'<td class="padLeft">J.Kelamin</td><td>: '+value.jeniskelamin+'</td>'
                        +'<td class="padLeft">Ayah</td><td>: '+value.ayah+'</td>'
                        +'<td class="padLeft">No.RM</td><td>: '+value.norm+'</td>'
                        +'<td class="padLeft">No.SEP</td><td>: '+value.nosep+'</td></tr>'
                      +'<tr>'
                        +'<td>Tgl.Lahir (Usia)</td><td>: '+value.tanggallahir+' ('+value.usia+')</td>'
                        +'<td class="padLeft">Gol.Darah</td><td>: '+value.golongandarah+' '+value.rh+'</td>'
                        +'<td class="padLeft">Ibu</td><td>: '+value.ibu+'</td>'
                        +'<td class="padLeft">No.JKN</td><td>: '+value.nojkn+'</td>'
                        +'<td class="padLeft">Penjamin</td><td>: '+value.carabayar+'</td></tr>'
                      +'<tr>'
                        +'<td>Alamat</td><td>: '+value.alamat+'</td>'
                        +'<td class="padLeft">Agama</td><td>: '+value.agama+'</td>'
                        +'<td class="padLeft">Penanggung</td><td>: '+value.penanggung+'</td>'
                        +'<td class="padLeft">No.Rujukan</td><td>: '+value.norujukan+'</td></tr>'
                  +'</table>';
         }
         
         //data blaclklist
         
        var dtblacklist = '';
        var no = 0;
        for(var i in blacklist)
        {
            dtblacklist += '<tr><td>'+ ++no +'</td><td>'+ blacklist[i].namaunit +'</td></tr>';
        }
        
        if(dtblacklist !='')
        {
            profile +='<div class="box box-solid box-info" style="margin-top:10px;">\n\
                   <div class="box-header ">\n\
                           <h3 class="box-title ">Pasien ini diblacklist di Poli</h3>\n\
                   </div>\n\
                   <div class="box-body no-padding">\n\
                     <table class="table table-striped">\n\
                       <tbody><tr><th style="width: 10px">#</th><th>Unit/Poli</th></tr>'+dtblacklist+'</tbody>\n\
                     </table>\n\
                   </div>\n\
                 </div>';
        }
    $('#pemeriksaanklinik_profile').html(profile);
    $('#pemeriksaanklinik_detailprofile').html(detail_profile);
}
function updateBiayahasilperiksa(y,z)//seting hahasilpemeriksaanpaket laboratorium
{
    var idpaketygdiubah = y;
    var modalTitle = 'Ubah Biaya';
    var modalContent = '<form action="" id="Formubahhargapaket">' +
                            '<div class="form-group">' +
                            '<input type="hidden" name="idp" value="'+$('input[name="idpendaftaran"]').val()+'" class="form-control"/>'+
                            '<input type="hidden" name="id" value="'+idpaketygdiubah+'" class="form-control"/>'+
                            //jasa operator
                                '<div class="col-md-3">'+
                                    '<label>Jasa Operator</label>' +
                                    '<input type="text" name="jasaoperator" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //nakes
                                '<div class="col-md-3">'+
                                    '<label>Nakes</label>' +
                                    '<input type="text" name="nakes" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //jasars
                                '<div class="col-md-3">'+
                                    '<label>Jasars</label>' +
                                    '<input type="text" name="jasars" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //bhp
                                '<div class="col-md-3">'+
                                    '<label>Bhp</label>' +
                                    '<input type="text" name="bhp" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //akomodasi
                                '<div class="col-md-3">'+
                                    '<label>Akomodasi</label>' +
                                    '<input type="text" name="akomodasi" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //margin
                                '<div class="col-md-3">'+
                                    '<label>Margin</label>' +
                                    '<input type="text" name="margin" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //sewa
                                '<div class="col-md-3">'+
                                    '<label>Sewa</label>' +
                                    '<input type="text" name="sewa" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            //potongan tagihan
                                '<div class="col-md-3">'+
                                    '<label>Potongan</label>' +
                                    '<input type="text" name="potongan" class="form-control"/>'+
                                    '</select>' +
                                '</div>'+
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            formSubmit: {
                text: 'Update',
                btnClass: 'btn-blue',
                action: function () {
                    
                    $.ajax({
                        type: "POST", //tipe pengiriman data
                        url: 'savepaketygdiubah', //alamat controller yang dituju
                        data: $("#Formubahhargapaket").serialize(), //
                        dataType: "JSON", //tipe data yang dikirim
                        success: function(result) { //jika  berhasil
                            if(isselesaiperiksa(result.status))
                            {
                                notif(result.status, result.message);
                                if(result.status=='success')
                                {
                                    if(z=='Tindakan')
                                    {
                                        pemeriksaanRefreshTindakan($('input[name="idpendaftaran"]').val());
                                    }
                                    else if(z=='Radiologi')
                                    {
                                        pemeriksaanRefreshRadiologi($('input[name="idpendaftaran"]').val());
                                    }
                                    else
                                    {
                                        pemeriksaanRefreshLaboratorium($('input[name="idpendaftaran"]').val());
                                    }
                                }
                            }
                        },
                        error: function(result) { //jika error
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            formReset:{ //menu back
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        //tampilkan harga yang akan diubah
        tampilhargayangdiubah(idpaketygdiubah);
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function tampilhargayangdiubah(x)
{
    $.ajax({
        type:"POST",
        url:"tampilhargayangdiubah",
        data:{x:x},
        dataType:"JSON",
        success: function(result){
            $('input[name="jasaoperator"]').val(result.jasaoperator);
            $('input[name="nakes"]').val(result.nakes);
            $('input[name="jasars"]').val(result.jasars);
            $('input[name="bhp"]').val(result.bhp);
            $('input[name="akomodasi"]').val(result.akomodasi);
            $('input[name="margin"]').val(result.margin);
            $('input[name="sewa"]').val(result.sewa);
            $('input[name="potongan"]').val(result.potongantagihan);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function hapusPaketPeriksa(value)//hapus paket pemeriksaan
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = value;
    $.ajax({
        type:"POST",
        url:"delete_paket_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function deleteHasilPeriksa(value)//hapus hasil periksa per icd
{
    var x = $('input[name="idpendaftaran"]').val();
    var y = $('#deleteHasilPeriksa'+value).val();
    $.ajax({
        type:"POST",
        url:"delete_hasil_periksa",
        data:{x:x, y:y},
        dataType:"JSON",
        success: function(result){
            notif(result.status, result.message);
            if(result.status=='success')
            {
                pemeriksaanklinikRefreshDiagnosa();//refresh diagnosa
            }
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaan_tampilpasien() //tampil data pasien
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tampilpasien",
        data:{x:norm, y:idperiksa},
        dataType:"JSON",
        success: function(result){
            $('#labNormPasien').html(': '+result.p.norm);
            $('#labNamaPasien').html(': '+result.p.namalengkap);
            $('#labPengirim').html(': '+result.per.titeldepan+' '+result.per.namalengkap+' '+result.per.titelbelakang);
            $('#labKelaminPasien').html(': '+result.p.jeniskelamin);
            $('#labTgllahirPasien').html(': '+result.p.tanggallahir);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}

//////////////MENAMPILKAN DATA DETAIL PASIEN//////////////////
function pemeriksaandetailTampilPasien()
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_detail_tampil_pasien",
        data:{x:idperiksa},
        dataType:"JSON",
        success:function(result){
            // console.log(result);
            pemeriksaandetailTampilPasienListData(result);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaandetailTampilPasienListData(data)
{
    var modalTitle = '<div class="col-sm-12 center">Detail Pasien</div>';
    var modalContent = '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Nama</th><th style="font-weight:normal;">: '+if_empty(data.namalengkap)+'</th></tr>'+
                                    '<tr><th>No.RM</th><th style="font-weight:normal;">: '+if_empty(data.norm)+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamat+'</th></tr>'+
                                    '<tr><th>Telpon</th><th style="font-weight:normal;">: '+data.telponpasien+'</th></tr>'+
                                    '<tr><th>Kelamin</th><th style="font-weight:normal;">: '+data.jeniskelamin+'</th></tr>'+
                                    '<tr><th>TanggalLahir</th><th style="font-weight:normal;">: '+data.tanggallahir+'</th></tr>'+
                                    '<tr><th>Agama</th><th style="font-weight:normal;">: '+data.agama+'</th></tr>'+
                                    '<tr><th>Status</th><th style="font-weight:normal;">: '+data.statusmenikah+'</th></tr>'+
                                    '<tr><th>Golongan Darah</th><th style="font-weight:normal;">: '+data.golongandarah+'</th></tr>'+
                                    '<tr><th>Rhesus</th><th style="font-weight:normal;">: '+data.rh+'</th></tr>'+
                                    '<tr><th>Alergi</th><th style="font-weight:normal;">: '+data.alergi+'</th></tr>'+
                                '</table>'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Penanggung</th><th style="font-weight:normal;">: '+data.namapenjawab+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamatpenjawab+'</th></tr>'+
                                    '<tr><th>No.Telpon</th><th style="font-weight:normal;">: '+data.notelpon+'</th></tr>'+
                                    '<tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr>'+
                                    '<tr><th>Klinik</th><th style="font-weight:normal;">: '+data.namaunit+'</th></tr>'+
                                    '<tr><th>No.Antri</th><th style="font-weight:normal;">: '+data.noantrian+'</th></tr>'+
                                    '<tr><th>Dokter</th><th style="font-weight:normal;">: '+data.titeldepan+' '+data.namadokter+' '+data.titelbelakang+'</th></tr>'+
                                    '<tr><th>Carabayar</th><th style="font-weight:normal;">: '+data.carabayar+'</th></tr>'+
                                    '<tr><th>No.SEP</th><th style="font-weight:normal;">: '+data.nosep+'</th></tr>'+
                                '</table>'+
                            '</div>';
    $.alert({
        title: modalTitle,
        content: modalContent,
        columnClass: 'xl',
    });
}
// form add diagnosa
function pemeriksaanklinik_adddiagnosa()
{
    var modalSize = 'small';
    var modalTitle = 'Form Tambah ICD X';
    var modalContent = '<form action="" id="FormAddIcd">' +
                            '<div class="form-group">' +
                                '<label>Kode ICD</label>' +
                                '<input type="text" name="kodeicd" placeholder="kode icd" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Nama ICD</label>' +
                                '<input type="text" name="namaicd" placeholder="nama icd" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Alias ICD</label>' +
                                '<input type="text" name="aliasicd" placeholder="alias icd" class="form-control"/>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: { //menu save atau simpan
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('input[name="kodeicd"]').val()==='')
                    {
                            alert_empty('kode icd');
                            return false;
                    }
                    else if($('input[name="namaicd"]').val()==='')
                    {
                            alert_empty('nama icd');
                            return false;
                    }
                    else if($('input[name="aliasicd"]').val()==='')
                    {
                            alert_empty('alias icd');
                            return false;
                    }
                    else //selain itu
                    {
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cmasterdata/pemeriksaan_saveicd', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormAddIcd").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                pilihdiagnosa($('input[name="kodeicd"]').val());
                            },
                            error: function(result) { //jika error
                                fungsiPesanGagal();
                                return false;
                            }
                        });
                    }
                }
            },
            formReset:{ //menu batal
                text: 'Tutup',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
// mahmud, clear
function tutupRiwayatPeriksa()
{
    var riwayat = '<div class="box-body text-center" style="overflow-y: scroll;overflow-x: scroll; height: 120px;" >'
                +'<div class="error-content">'
                  +'<h4><i class="fa fa-warning text-aqua"></i> Riwayat belum ditampilkan.</h4>'
                  +'<a class="btn btn-primary" onclick="tampilRiwayatSaatPeriksa()">Tampil Riwayat</a>'
                +'</div>'
              +'</div>';
    $('#listRiwayatSaatperiksa').empty();
    $('#listRiwayatSaatperiksa').html(riwayat);
    $('#listTglRiwayatPeriksa').empty();
}
//mahmud, clear
function tampilRiwayatSaatPeriksa() // tampilkan riwayat saat periksa
{
    startLoading();
    $.ajax({
        type: "POST", //tipe pengiriman data
        url: base_url+'cpelayanan/list_riwayat_sattperiksa', //alamat controller yang dituju (di js base url otomatis)
        data: {idperiksa:$('input[name="idperiksa"]').val()}, //
        dataType: "JSON", //tipe data yang dikirim
        success: function(result) { //jika  berhasil
            stopLoading();
            if(result.status=='empty'){return notif('info','Riwayat tidak ditemukan...!');}
            else
            {
            var riwayat ='<div class="box-body" style="overflow-y: scroll;overflow-x: scroll; height: 350px;">';
        //       // <!-- Riwayat DIAGNOSA ===================== -->
              riwayat += ''
              +'<div class="pull-right"><a class="btn btn-danger" onclick="tutupRiwayatPeriksa()">Tutup Riwayat</a></div>'
              +'<br>'
              +'<div class="form-group">'
                +'<div class="col-sm-12">'
                  +'<span class="label label-default">Diagnosa diluar icd 10</span> <a onclick="salinRiwayatDiagnosa()" class="btn btn-primary btn-xs"  data-toggle="tooltip" data-original-title="Salin Diagnosa diluar ICD 10"><i class="fa fa-copy"></i>Salin</a>'
                  +'<textarea class="form-control" name="riwayatdiagnosa" rows="1" disabled>'+result.periksa['diagnosa']+'</textarea>'
                +'</div>'
              +'</div>'
              // <!-- LIST HASIL DIAGNOSA -->
                +'<div class="form-group">'
                  +'<div class="col-sm-12">'
                    +'<label><h3>Diagnosa</h3></label> '
                   +'<table class="table">'
                      +'<thead>'
                        +'<tr class="bg-info">'
                          +'<th>ICD</th>'
                          +'<th>Nama ICD</th>'
                          +'<th>Alias</th>'
                          +'<th></th>'
                        +'</tr>'
                      +'</thead>'
                      +'<tbody>';
                      for(x in result.diagnosa)
                      {
                        riwayat+='<tr><td>'+result.diagnosa[x].icd+'</td><td>'+result.diagnosa[x].namaicd+'</td><td>'+result.diagnosa[x].aliasicd+'</td><td><a id="salinRiwayatIcd" isiSalinRiwayatIcd="'+result.diagnosa[x].icd+'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin '+result.diagnosa[x].icd+'"><i class="fa fa-copy"></i> Salin</a></td></tr>';
                      }
                      riwayat+='</tbody>'
                    +'</table>'
                  +'</div>'
                +'</div>'

              // <!-- Riwayat BHP -->
              +'<div class="form-group">'
                +'<div class="col-sm-12">'
                   +'<span class="label label-default">Keterangan BHP/Obat</span> <a onclick="salinRiwayatKetObat()" class="btn btn-xs btn-primary" data-toggle="tooltip" data-original-title="Salin Keterangan Obat/BHP"><i class="fa fa-copy"></i> Salin </a>'
                  +'<textarea disabled class="form-control" name="riwayatketeranganobat" rows="1">'+result.periksa['keteranganobat']+'</textarea>'
                +'</div>'
              +'</div>'
              +'<div class="form-group">'
                  +'<div class="col-sm-12">'
                    +'<label><h3>Obat/BHP</h3></label>'
                   +'<table class="table">'
                      +'<thead>'
                        +'<tr class="bg-info">'
                          +'<th>NamaObat/BHP</th>'                          
                          +'<th>Kekuatan</th>'
                          +'<th>JumlahAmbil</th>'
                          +'<th>Tatacara</th>'
                          +'<th></th>'
                        +'</tr>'
                      +'</thead>'
                      +'<tbody>';
                      for(i in result.obat)
                      {
                        riwayat+='<tr><td>'+result.obat[i].namabarang+'</td><td>'+result.obat[i].kekuatan+'</td><td>'+result.obat[i].jumlahpemakaian+'</td><td>'+result.obat[i].tatacara+'</td><td><a onclick="salinRiwayatObat('+result.obat[i].idbarang+')" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin '+result.obat[i].namabarang+'"><i class="fa fa-copy"></i> Salin</a></td></tr>';
                      }
                      riwayat+='</tbody>'
                    +'</table>'
                  +'</div>'
                +'</div>';
    riwayat +='</div>';
    $('#listRiwayatSaatperiksa').empty();
    $('#listRiwayatSaatperiksa').html(riwayat);
    $('#listTglRiwayatPeriksa').empty();
    $('#listTglRiwayatPeriksa').text(result.periksa.waktu);
    $('[data-toggle="tooltip"]').tooltip() //initialize tooltip
    }
        },
        error: function(result) { //jika error
            fungsiPesanGagal();
            return false;
        }
    });
}
// mahmud, clear
function salinRiwayatDiagnosa()
{
    $('textarea[name="diagnosa"]').val($('textarea[name="riwayatdiagnosa"]').val());
    return notif('success','Menyalin Diagnosa Berhasil...!');
}
// mahmud, clear
function salinRiwayatKetObat()
{
    $('textarea[name="keteranganobat"]').val($('textarea[name="riwayatketeranganobat"]').val());
    return notif('success','Menyalin Keterangan Obat/BHP Berhasil...!');
}
function salinRiwayatObat(value)
{
    if($('textarea[name="keteranganobat"]').val()=='')
    {
        $('textarea[name="keteranganobat"]').focus();
        alert_empty('keterangan resep');
    }
    else
    {
        $.ajax({
            type:"POST",
            url:"pemeriksaan_salinbhp",
            data:{x:value, i:idperiksa,p:$('input[name="idpendaftaran"]').val()},
            dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanklinikRefreshBhp(); //refresh diagnosa
                }
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
    }
}
// mahmud, clear
$(document).on("click","#salinRiwayatIcd", function(){
    pilihdiagnosa($(this).attr("isiSalinRiwayatIcd"));
});
// mahmud, clear
$(document).on("click","#periksa_salinketeranganobat", function(){
    $('#keteranganobat').val('');
    $('#keteranganobat').val($(this).attr("ket"));
    notif('success', 'Salin ketetangan obat berhasil..!');
});
// mahmud, clear -> pilih aturan pakai obat
function isicomboaturanpakai(id, selected)
{
    if (is_empty(localStorage.getItem('localStorageAP')))
    {
        $.ajax({ 
            url: base_url+'cmasterdata/get_barangaturanpakai',
            type : "POST",      
            dataType : "JSON",
            success: function(result) {                                                                   
                localStorage.setItem('localStorageAP', JSON.stringify(result));
                fillaturanpakai(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillaturanpakai(id, selected);
            },
            error: function(result){
                fillaturanpakai(id, selected);
            }
        });
    }
}

//basit, clear
function fillaturanpakai(id, selected)
{
    var aturanpakai = JSON.parse(localStorage.getItem('localStorageAP'));
    var i=0;
    for (i in aturanpakai)
    {
      $("#"+id).append('<option ' + ((aturanpakai[i].idbarangaturanpakai===selected)?'selected':'') + ' value="'+aturanpakai[i].idbarangaturanpakai+'">'+aturanpakai[i].signa+'</option>');
    }
}

// mahmud, clear -> pilih aturan pakai obat
function isicombokemasan(id, selected)
{
    if (is_empty(localStorage.getItem('localStorageKEMASAN')))
    {
        $.ajax({ 
            url: base_url+'cmasterdata/get_kemasan',
            type : "POST",      
            dataType : "JSON",
            success: function(result) {                                                                   
                localStorage.setItem('localStorageKEMASAN', JSON.stringify(result));
                fillkemasan(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillkemasan(id, selected);
            },
            error: function(result){
                fillkemasan(id, selected);
            }
        });
    }
}

function fillkemasan(id, selected)
{
    var kemasan = JSON.parse(localStorage.getItem('localStorageKEMASAN'));
    var i=0;
    for (i in kemasan)
    {
      $("#"+id).append('<option ' + ((kemasan[i].idkemasan===selected)?'selected':'') + ' value="'+kemasan[i].idkemasan+'">'+kemasan[i].kemasan+'</option>');
    }
}
function setfocusVitalsign(){$('#focusVitalsign').focus();}//fokus focusVitalsign
function setfocusElektromedik(){$('#focusElektromedik').focus();} // fokus radiologi
function setfocuslaboratorium(){$('#focusLaboratorium').focus();}// fokus laboratorium
function setfocusdiagnosa(){$('#focusDiagnosa').focus();} // fokus diagnosa
function setfocustindakan(){$('#focusTindakan').focus();} // fokus tindakan
function setfocusobatbhp(){$('#focusObat').focus();}  // fokus obat/bhp
// fungsi dijalankan setelah menu simpan di klik
function FormPemeriksaanKlinikSubmit()
{
    startLoading();
    if($('#namaanamnesa0').val()=='' && $('#validasidatasubyektif').val() !=0 )
    {
        stopLoading();
        alert('Harap Melengkapi Data Subyektif.!');
        return false;
    }
    else if($('#dataobyektif').val()=='' && $('#validasidataobyektif').val() !=0)
    {
        stopLoading();
        alert('Harap Melengkapi Data Obyektif.!');
        return false;
    }
    else
    {
//        startLoading();
//        $.ajax({
//            url:base_url+"cpelayanan/validation_icd10",
//            data:{idp:idpendaftaran},
//            type:"POST",
//            dataType:"JSON",
//            success:function(result){
//                stopLoading();
//                if(result==0)
//                {
//                    alert(' Mohon Lengkapi ICD 10 .!');
//                }
//                else
//                {
                    $('#FormPemeriksaanKlinik').submit();
//                }
//                
//            },
//            error:function(hasil){
//                stopLoading();
//                fungsiPesanGagal();
//                return false;
//            }
//        })
    }
    
}
// menampilkan tanggal riwayat periksa
function pemeriksaanklinik_gettglriwayat(value='')
{
    startLoading();
    ((value=='')?tgl1='':tgl1=$('input[name="riwayat_tgl1"]').val());
    $.ajax({
        url:base_url+"cpelayanan/get_riwayat_tglperiksa",
        data:{tgl1:tgl1,tgl2:value,norm:norm},
        type:"POST",
        dataType:"JSON",
        success:function(hasil){
            stopLoading();
            if(hasil.length==0)
            {
                $('#r_hasilPeriksa').empty();
                $('#r_hasilPeriksa').html('<div class="error-content text-center"><h3><i class="fa fa-warning text-yellow"></i> Riwayat pemeriksaan belum ada..!.</h3></div>');
                return true;
            }
            var data='';
            for(x in hasil){data += '<button onclick="pemeriksaanklinik_getriwayatperiksa('+hasil[x].idpendaftaran+')" type="button" class="btn btn-default">'+hasil[x].waktuperiksa+'</button>';}
            $('#r_tanggalPeriksa').empty();
            $('#r_tanggalPeriksa').html(data);
            pemeriksaanklinik_getriwayatperiksa(hasil[0].idpendaftaran);
        },
        error:function(hasil){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    })
}
// menampilkan data riwayat sesuai tanggal yg dipilih
function pemeriksaanklinik_getriwayatperiksa(value)
{
    $.ajax({
        url:base_url+"cpelayanan/get_riwayatperiksa",
        data:{id:value},
        type:"POST",
        dataType:"JSON",
        success:function(hasil){
            var identitas = hasil.identitas, html='', vitalsign='', radiologi='',laboratorium='', diagnosa='',tindakan='',obat='';
            for ( x in hasil.vitalsign) { vitalsign += hasil.vitalsign[x].hasil_diagnosis;}
            for ( x in hasil.radiologi) { radiologi += hasil.radiologi[x].hasil_diagnosis;}
            for ( x in hasil.laboratorium) { laboratorium += hasil.laboratorium[x].hasil_diagnosis;}
            for ( x in hasil.diagnosa) { diagnosa += hasil.diagnosa[x].icd+' '+hasil.diagnosa[x].hasil_diagnosis+' <a id="salinRiwayatIcd" isisalinriwayaticd="'+hasil.diagnosa[x].icd+'" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Salin"><i class="fa fa-copy"></i> Salin</a><br>';}
            for ( x in hasil.tindakan) { tindakan += hasil.tindakan[x].icd+' '+hasil.tindakan[x].hasil_diagnosis;}
            
            for (i in identitas)
            {
                html+='<table style="margin-left: 16px;" class="col-sm-12 unselectable">'
                    +((i!=0)?'<tr class="bg bg-gray"><th>&nbsp;</th><td></td></tr>':'')
                    +'<tr><th style="width: 10%;">DOKTER</th><td>: '+identitas[i].dokter+'</td></tr>'
                    +'<tr><th>WAKTU</th><td>: '+identitas[i].tglperiksa+'</td></tr>'
                    +'<tr><th>KLINIK</th><td>: '+identitas[i].namaunit+'</td></tr>'
                    +'</table>'
                    +'<table style="margin: 16px;margin-top: 10px;" class="col-sm-11">'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">ANAMNESIS </th><th></tr>'
                    +'<tr><td colspan="2" class="unselectable">'+identitas[i].anamnesa+'<br>'+vitalsign+'</td></tr>'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">Elektromedik </th><th></tr>'
                    +'<tr><td class="unselectable">'+radiologi+'</td><td class="unselectable">'+identitas[i].keteranganradiologi+'</td></tr>'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">LABORATORIUM </th><th></tr>'
                    +'<tr><td colspan="2" class="unselectable">'+laboratorium+'</td></tr>'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">DIAGNOSA</th><th></tr>'
                    +'<tr><td colspan="2" class="unselectable">'+diagnosa+'</td></tr>'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">TINDAKAN</th><th></tr>'
                    +'<tr><td colspan="2" class="unselectable">'+tindakan+'</td></tr>'
                    +'<tr style="border-bottom:1px solid #000;"><th colspan="2">OBAT/BHP</th><th></tr>'
                    +'<tr><td colspan="2" class="unselectable">'+identitas[i].keteranganobat+' <a class="btn btn-primary btn-xs" id="periksa_salinketeranganobat" ket="'+identitas[i].keteranganobat+'"><i class="fa fa-copy"></i> Salin Keterang Obat</a><br><br>'+periksa_cetakresep(hasil.grup,hasil.obat,'1',hasil.obat)+'</td></tr>'
                  +'</table>';    
            }
            
            $('#r_hasilPeriksa').empty();
            $('#r_hasilPeriksa').html(html);
            $('[data-toggle="tooltip"]').tooltip();
        },
        error:function(hasil){
            fungsiPesanGagal();
            return false;
        }
    })
}

//salin riwayat obat
$(document).on('click','#salinriwayatresep',function(){
   var grup  = $(this).attr('grup');
   var idbp  = $(this).attr('idbp');
   var resep = $(this).attr('resep') + '<br>' + ((grup != '0' ) ? $(this).attr('kemasangrup') + "<br>" +$(this).attr('signagrup') : $(this).attr('signa') ) 
   
   $.confirm({
        icon: 'fa fa-question',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Salin Resep',
        content: resep,
        buttons: {
            formSubmit: {
                text: 'salin',
                btnClass: 'btn-blue',
                action: function () {                    
                    startLoading();
                    $.ajax({ 
                         url: base_url+'cpelayanan/obat_salinriwayatobat',
                         type : "post",      
                         dataType : "json",
                         data : {grup:grup,idbp:idbp,idp:localStorage.getItem('idperiksa'),idpend:localStorage.getItem('idp')},
                         success: function(result) {                                                                   
                             stopLoading();
                             notif(result.status,result.message);
                             if(result.status=='success')
                             {
                                 pemeriksaanklinikRefreshBhp(idpendaftaran,'rajal');
                             }
                         },
                         error: function(result){
                             stopLoading();
                             fungsiPesanGagal();
                             return false;
                         }

                     });
                }
            },
            //menu reset
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }         
        }
    }); 
});

// SIMPAN KETERANGA OBAT SETELAH SELESAI MENULIS RESEP
function pemeriksaanklinik_saveketobat(value)
{
   startLoading();
   $.ajax({ 
        url: 'pemeriksaanklinik_saveketobat',
        type : "post",      
        dataType : "json",
        data : {id:$('input[name="idpendaftaran"]').val(),data:value},
        success: function(result) {                                                                   
            stopLoading();
            if(result.status=='success'){
                notif(result.status, result.message);           
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
        
    });
}
// SIMPAN KETERANGA OBAT SETELAH SELESAI MENULIS disgnosa atau data subyektif
function pemeriksaanklinik_savedtsubyektif(no)
{
    $.ajax({ 
        url: 'pemeriksaanklinik_savedatasubyektif',
        type : "post",      
        dataType : "json",
        data : {dt:$('#namaanamnesa'+no).val(),id:$('#idanamnesa'+no).val()},
        success: function(result) {                                                                   
            if(result.status=='success'){
                notif(result.status, result.message);           
            }else{
                notif(result.status, result.message);
                return false;
            }                        
        },
        error: function(result){                  
            fungsiPesanGagal();
            return false;
        }
        
    });
}

//$('.icheckbox_flat-green').on('click', function() {
//    $('#formIndikasiPasienWabah').removeClass('hide').fadeIn('slow');
//});
//--Indikasi Pasien Wabah
function setFormIndikasiPasienWabah(value)
{
    var value = $("input:checkbox[name='cindikasipasien']:checked").map(function(){return $(this).val();}).get();
    if(value!='')
    {
        $.ajax({ 
            url: base_url+'cpelayanan/pemeriksaanklinik_getpemeriksaanindikasipasienwabah',
            type : "post",      
            dataType : "json",
            data : {idp:idperiksa},
            success: function(result) {  
                var x = result;
                $('#formIndikasiPasienWabah').removeClass('hide').fadeIn('slow');
                dropdownlist_pemeriksaanpasienwabah('jeniswabah',x.wabah,((x.pw==null)?'':x.pw.idjeniswabah));// tagid,data,selected
                dropdownlist_pemeriksaanpasienwabah('statusrawatwabah',x.status,((x.pw==null)?'':x.pw.idstatusrawatwabah));// tagid,data,selected
                dropdownlist_pemeriksaanpasienwabah('sebabpenularan',x.penularan,((x.pw==null)?'':x.pw.idsebabpenularan));// tagid,data,selected
                change_statusrawatIPW(((x.pw==null)?'':x.pw.idstatusrawatwabah),((x.pw==null)?'':x.pw.idpenangananwabah));
                
            },
            error: function(result){  
                fungsiPesanGagal();
                return false;
            }

        });
        
    }
    else
    {
        $('#formIndikasiPasienWabah').addClass('hide').fadeIn('slow');
    }
}

//mahmud, clear -> list 
function dropdownlist_pemeriksaanpasienwabah(tagid,data,selected)
{
    var option= '<option value="">Pilih</option>', id='',name='';
    $('#'+tagid).empty();
    for(i in data){
        id=((tagid=='jeniswabah')? data[i].idjeniswabah : ((tagid=='statusrawatwabah')? data[i].idstatusrawatwabah :((tagid=='sebabpenularan')? data[i].idsebabpenularan :((tagid=='penanganan')? data[i].idpenangananwabah :''))));
        name=((tagid=='jeniswabah')? data[i].jeniswabah : ((tagid=='statusrawatwabah')? data[i].statusrawat :((tagid=='sebabpenularan')? data[i].sebabpenularan :((tagid=='penanganan')? data[i].penanganan :''))));
        option = option + '<option value="'+ id +'" '+ ((id==selected)?'selected':'') +' >' + name +'</option>';
    }
    $('#'+tagid).html(option);
}
//saat dropdown status rawat indikasi pasien wabah dipilih
function change_statusrawatIPW(value,select)
{
    $.ajax({ 
        url: base_url+'cpelayanan/pemeriksaanklinik_getpenangananIPW',
        type : "post",      
        dataType : "json",
        data :{status:value},
        success: function(result) { 
            dropdownlist_pemeriksaanpasienwabah('penanganan',result,select);// tagid,data,selected
        },
        error: function(result){ 
            fungsiPesanGagal();
            return false;
        }

    });
}
function formAlergi(idp='',id='')
{
    var modalContent = '<form action="" id="FormAddAlergi"> <input type="hidden" name="alergi_idperson" /><input type="hidden" name="mode" />' +
                            '<div class="form-group">' +
                                '<label>Jenis Alergi</label>' +
                                '<select onchange="onchange_jenisalergi(this.value)" name="jenisalergi" class="form-control" ><option value="obat">Obat</option><option>Makanan</option><option>Minuman</option><option>Lainya</option></select>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label class="obatalergi">Pilih Obat</label>' +
                                '<select class="obatalergi form-control" name="alergi_idbarang"><option value="">Pilih Obat</option></select>' +
                                '<label class="keteranganalergi hide">Keterangan</label>' +
                                '<textarea class="keteranganalergi hide form-control" name="keterangan" rows="3"></textarea>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Reaksi</label>' +
                                '<textarea name="reaksi" rows="3" class="form-control"></textarea>' +
                            '</div>' +
                        '</form>';
    $.confirm({
        title: 'Tambah Data Alergi',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'Simpan',
                btnClass: 'btn-blue',
                action: function () {
                    if($('select[name="jenisalergi"]').val()=='obat' && $('select[name="alergi_idbarang"]').val()=='')
                    {
                        alert_empty(' (obat belum ada)');
                            return false;
                    }
                    else if($('input[name="keterangan"]').val()==='')
                    {
                            alert_empty('keterangan');
                            return false;
                    }
                    else //selain itu
                    {
                        $('.select2').removeClass('hide');
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cpelayanan/pemeriksaan_savealergi', //alamat controller yang dituju (di js base url otomatis)
                            data: $("#FormAddAlergi").serialize(), //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                pemeriksaanklinikRefreshAlergi(norm);
                            },
                            error: function(result) { //jika error
                                fungsiPesanGagal();
                                return false;
                            }
                        });
                    }
                }
            },
            formReset:{ //menu batal
                text: 'Batal',
                btnClass: 'btn-danger',
                action: function(){
                    $('.select2').removeClass('hide');
                }
                
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
        $('input[name="alergi_idperson"]').val($('input[name="idperson"]').val());
        if(idp!=''){tampilUbahAlergi(idp,id);}
        $.ajax({ type:"POST", url: base_url +"cmasterdata/getdataenumjson", dataType:"JSON",data:{t:'ppas_a'}, success:function(hasil){ var opt=''; for(x in hasil){opt+='<option value="'+hasil[x]+'">'+hasil[x]+'</option>';} $('select[name="jenisalergi"]').empty();$('select[name="jenisalergi"]').html(opt); onchange_jenisalergi();} });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
function tampilUbahAlergi(idp,id)
{   
    $.ajax({
       url:base_url+"cpelayanan/get_dataalergi",
       data:{idp:idp,id:id,mode:'e'},
       dataType:"JSON",
       type:"POST",
       success:function(s){
           console.log(s['keterangan']);
           $('textarea[name="keteranganalergi"]').val(s['keterangan']);
       },
       error:function(e){
           return false;
       }
    });
}
//saat memilih jenis alergi 
function onchange_jenisalergi()
{
    var e = $('select[name="jenisalergi"]').val();
    if(e=='obat'){ 
        $('.obatalergi').removeClass('hide');
        $('.select2').removeClass('hide');
        $('.keteranganalergi').addClass('hide');
        cariBhpFarmasi($('select[name="alergi_idbarang"]'));
    }else{
        $('.select2').addClass('hide');
        $('.obatalergi').addClass('hide');$('.keteranganalergi').removeClass('hide');
    }
}

function pemeriksaanklinikRefreshAlergi(norm)
{
    $.ajax({
       url:base_url+"cpelayanan/get_dataalergi",
       data:{n:norm,mode:'a'},
       dataType:"JSON",
       type:"POST",
       success:function(s){
           var alergi = '';
           for(var x in s){alergi+='<tr><td><a onclick="formAlergi('+s[x].idperson+','+s[x].id+')" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> <a onclick="hapusAlergi('+s[x].idperson+','+s[x].id+')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a></td><td>'+s[x].jenisalergi+'</td><td>'+s[x].keterangan+'</td><td>'+s[x].reaksi+'</td></tr>';}
           $('#listAlergi').empty();
           $('#listAlergi').html('<table id="tblalergi" class="table table-striped"><thead> <tr class="bg bg-yellow"><th><a i="" id="addalergi" class="btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Tambah Alergi"><i class="fa fa-plus-circle"></i></a></th><th>Jenis</th><th>Obat/Keterangan</th><th>Reaksi</th></tr></thead><tbody>'+alergi+'</tfoot></table>');
           $('#tblalergi').DataTable({"columnDefs": [{ "orderable": false, "targets": 0 }]});
           $('#addalergi').on('click',function(){ formAlergi()});
       },
       error:function(e){
           return false;
       }
    });
}

/* Ubah Jaminan Asuransi Periksa per Tindakan */
$(document).on('click','#ubahjaminanasuransi',function(){
    // 1 asuransi, 0 mandiri
    var idpemeriksaan = $(this).attr('alt');
    var jaminan = (($(this).attr('jaminan')==0) ? 1 : 0 );
    var modeUbah = $(this).attr('mode');
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi!',
        content: '<b>Ubah Jaminan Tindakan</b> <br/> '+((jaminan==1) ? 'Dari <u>Mandiri</u> ke Asuransi' : 'Dari Asuransi ke <u>Mandiri</u>' ),
        buttons: {
            ubah: function () {       
                startLoading();
                $.ajax({ 
                    url: base_url+'cpelayanan/ubahjaminanasuransi_ralan',
                    type : "post",      
                    dataType : "json",
                    data : {idp:idpemeriksaan,jaminan:jaminan,mode:modeUbah,idpend:idpendaftaran},
                    success: function(result) {     
                        stopLoading();
                        if(result.status=='success'){
                            switch(modeUbah)
                            {
                                case 'tindakan':
                                    pemeriksaanRefreshTindakan(idpendaftaran);
                                    break;
                                case 'radiologi':
                                    pemeriksaanRefreshRadiologi(idpendaftaran);
                                    break;
                                case 'laboratorium':
                                    pemeriksaanRefreshLaboratorium(idpendaftaran);
                                    break;
                                case 'obat':
                                    pemeriksaanklinikRefreshBhp(idpendaftaran);
                                    break;
                                case 'grupobat':
                                    pemeriksaanklinikRefreshBhp(idpendaftaran);
                                    break;
                                default:
                            }
                            notif(result.status, result.message);
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){
                        stopLoading();
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            batal: function () {               
            }            
        }
    });
});

// Menu Ubah Jaminan Asuransi Hasil pemeriksaan
function menuUbahAsuransiHasilPemeriksaan(idhasilpemeriksaan,jaminan,mode)
{
    var titel = ((jaminan=='0') ? 'Mandiri' : 'Asuransi' );
    return '<a class="btn '+((jaminan=='0') ? 'btn-default' : 'btn-info' )+' btn-xs" data-toggle="tooltip" id="ubahjaminanasuransi" mode="'+mode+'" alt="'+idhasilpemeriksaan+'" jaminan="'+jaminan+'" data-original-title="jaminan '+titel+'"><i class="fa fa-flag"></i></a>';
}

$(document).on('click','#pemeriksaanklinik_ubahdokter',function(){
    var modalContent = '<form action="" id="FormUbahDokter">' +
        '<div class="form-group">' +
            '<input type="hidden" value="'+idperiksa+'" />'+
            '<label>Dokter Pengganti</label>' +
            '<select id="iddokterpengganti" name="iddokterpengganti" class="form-control select2"><option value="0">Pilih Dokter</option></select>' +
        '</div>' +
    '</form>';
        $.confirm({
        title  : 'Ubah DPJP',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
            text: 'Simpan',
            btnClass: 'btn-blue',
                action: function () {
                    if($('#iddokterpengganti').val()==0){
                        alert('Dokter Belum Dipilih.');
                        return false;
                    }else{
                        startLoading();
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cpelayanan/pemeriksaanklinik_ubahdokter', //alamat controller yang dituju (di js base url otomatis)
                            data: {ip:idperiksa,idok:$('#iddokterpengganti').val()}, //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                notif(result.status, result.message);
                                //replace dokter
                                if(result.status=='success'){
                                    var dokter = $('#iddokterpengganti option:selected').text();
                                    $('input[name="dokter"]').val(dokter);
                                }
                                stopLoading();
                            },
                            error: function(result) { //jika error
                                stopLoading();
                                fungsiPesanGagal();
                                return false;
                            }
                        });
                    }
                }
            },
           
            formReset:{ //menu batal
                text: 'Batal',
                btnClass: 'btn-danger',
                action: function(){
                }
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
            select2serachmulti($("#iddokterpengganti"),"cmasterdata/fillcaridokter"); //pencarian data icd
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
});

//ubah loket pemeriksaan


$(document).on('click','#pemeriksaanklinik_ubahloket',function(){
    var modalContent = '<form action="" id="FormUbahLoket">' +
        '<div class="form-group">' +
            '<input type="hidden" value="'+idperiksa+'" />'+
            '<label>Loket Pengganti</label>' +
            '<select id="idloketpengganti" name="idloketpengganti" class="form-control select2"><option value="0">Pilih Loket</option></select>' +
        '</div>' +
    '</form>';
        $.confirm({
        title  : 'Ubah Loket',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
            text: 'Simpan',
            btnClass: 'btn-blue',
                action: function () {
                    if($('#idloketpengganti').val()==0){
                        alert('Loket Belum Dipilih.');
                        return false;
                    }else{
                        startLoading();
                        $.ajax({
                            type: "POST", //tipe pengiriman data
                            url: base_url+'cpelayanan/pemeriksaanklinik_ubahloket', //alamat controller yang dituju (di js base url otomatis)
                            data: {
                                ip:idperiksa,
                                idl:$('#idloket').val(),
                                idlp:$('#idloketpengganti').val(),
                                idp:$('input[name="idpendaftaran"]').val()
                            }, //
                            dataType: "JSON", //tipe data yang dikirim
                            success: function(result) { //jika  berhasil
                                
                                stopLoading();
                                notif(result.status, result.message);
                                //replace dokter
                                if(result.status=='success'){
                                    var loket = $('#idloketpengganti option:selected').text();
                                    $('input[name="namaloket"]').val(loket);
                                }
                            },
                            error: function(result) { //jika error
                                stopLoading();
                                fungsiPesanGagal();
                                return false;
                            }
                        });
                    }
                }
            },
           
            formReset:{ //menu batal
                text: 'Batal',
                btnClass: 'btn-danger',
                action: function(){
                }
            }
        },
        onContentReady: function () { //ketika form di tampilkan
        // bind to events
            select2serachmulti($("#idloketpengganti"),"cmasterdata/fillcariloket"); //pencarian data icd
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
});