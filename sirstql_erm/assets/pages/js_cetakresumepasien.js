// mahmud, clear :: resumepasien
function resumepasien(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/verif_resumepasien',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result) {
        stopLoading();
        var pasien = result.identitas;
        var vital = result.vitalsign, tind=result.tindakan, lab=result.laboratorium, rad=result.radiologi, obat = result.obat, diag=result.diagnosa;
        var dtv='', dtt='', dtl='', dtr='', dto='', dtd='';
        for(x in vital){dtv+=vital[x].namaicd+': '+vital[x].nilai+' '+vital[x].satuan+', ';}
        for(x in tind){dtt+= tind[x].icd+ ' ' +tind[x].namaicd+'<br>';}
        for(x in lab){dtl+=((lab[x].namaicd==null)?'':lab[x].namaicd+': '+lab[x].nilai+' , ');}
        for(x in rad){dtr+=((rad[x].namaicd==null)?'':rad[x].namaicd+'<br>');}
        for(x in diag){dtd+=((diag[x].namaicd==null)?'':diag[x].icd+' '+ diag[x].namaicd+'<br>');}
        var keteranganradiologi= result.ketradiologi['keteranganradiologi']+((result.ketradiologi['saranradiologi']=='')?'':'<b>Saran:</b>')+result.ketradiologi['saranradiologi'];
    var header = '<div style="height:1.5cm;float: none;font-size:small;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;"><table border="0">';
    var body ='<div style="border-top:0.3px solid #333;margin-bottom:5px;font-size:10px;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;">';
    var bodytop = '<div style="float: none;font-size:small;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;"><table style="font-size:10px;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;"" border="0">';
        header +='<tr><td><img style="margin-bottom:1px; width:10cm" src="'+base_url+'assets/images/headerresume.svg" /></td></tr></table></div>';
    var cetak = header;
        cetak += '<div style="height:1.7cm;background-color:#f0f0f0; border-top:0.5px solid #333; font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;"><table style="font-size:10.3px;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;"" border="0"><tr><td>Tgl. Periksa</td><td>: '+pasien['tglperiksa']+'</td></tr> <tr><td>Klinik</td><td>: '+pasien['namaunit']+'</td></tr><tr><td>No.RM/Nama</td><td>: '+pasien['norm']+'/'+pasien['identitas']+' '+pasien['namalengkap']+'</td></tr><tr><td>Tgl. Lahir</td><td>: '+pasien['tanggallahir']+'</td></tr></table></div>';
        cetak += '<div style="font-weight:bold; padding-left:1.4cm; font-size:small;font-family: "Arial", "Book Antiqua", Palatino, serif;color: #333333;">RESUME PELAYANAN RAWAT JALAN</div>';
        cetak += bodytop+'<tr><td>DATA SUBYEKTIF</td><td>:</td></tr></table>';
        cetak += body+result.ketradiologi['anamnesa']+'</div>';

        cetak += bodytop+'<tr><td>DATA OBYEKTIF</td><td>:</td></tr></table>';
        cetak += body+dtv+'<br>'+result.ketradiologi['keterangan']+'</div>';

        cetak += bodytop+'<tr><td>DIAGNOSA</td><td>:</td></tr></table>';
        cetak += body+dtd+' '+result.ketradiologi['diagnosa']+'</div>';

        cetak += bodytop+'<tr><td>TINDAKAN</td><td>:</td></tr></table>';
        cetak += body+dtt+'</div>';
        cetak += bodytop+'<tr><td>LABORATORIUM</td><td>:</td></tr></table>';
        cetak += body+dtl+'</div></table>';
        cetak += bodytop+'<tr><td>RADIOLOGI</td><td>:</td></tr></table>';
        cetak += body+bodytop+'<tr><td>'+dtr+'</td><td>'+keteranganradiologi+'</td></tr></table></div>';
        cetak += bodytop+'<tr><td>RESEP</td><td>:</td></tr></table>';
        cetak += body+bodytop+periksa_cetakresep(result.grup,obat)+'</table></div>';
        cetak += '<div style="text-align:center;padding-left:60%;"><br>DOKTER <br><br><br><br><br><br>'+pasien['dokter']+'</div>';
    fungsi_cetaktopdf(cetak,'<style>@page {size:A5 lanscape; margin:0.2cm 0.5cm;}</style>');
    
    },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

// mahmud, clear :: form ina-cbg
function formInacbg(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/verif_getidentitaspasien',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result) {
        stopLoading();
        var icdd='', icdt='',nicdd='', nicdt='';
        for(var x in result.hp)
        {
           if(result.hp[x].idjenisicd==2){icdd += result.hp[x].icd+'<br>'; nicdd += result.hp[x].namaicd+'<br>';}else{icdd += '';nicdd += '';}
           if(result.hp[x].idjenisicd!=2){icdt += result.hp[x].icd+'<br>'; nicdt += result.hp[x].namaicd+'<br>';}else{icdt += '';nicdt += '';}
        }
        var nominal = result.nominal;
        var result = result.identitas;
        var bordertd='border:0.7px solid; padding:0.7px 5px;';
        var cetak = '<div style="height:2.5cm;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;""><table border="0"><tr><td><img style="margin-bottom:1px; width:6cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td><td style="padding-left:10px;font-size:15px;"> <b>FORMULIR <br>VERIFIKASI INA-CBG</b></td><td style="font-size:12px;padding-left:10px;"><span style="border:1px solid #000;padding:3px 4px;font-weight:bold;">RAWAT JALAN</span></td></tr></table></div>';
            cetak +='<div style="height:6.5cm;float: none; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<table border="0" style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<tr><td>1.</td><td>Nama RS</td><td ><span style="'+bordertd+'">RSU QUEEN LATIFA</span></td>     <td>2.</td><td>Kelas RS</td><td ><span style="'+bordertd+'">D</span></td></tr>';
            cetak +='<tr><td>3.</td><td>Nomor Kode RS</td><td > <span style="'+bordertd+'">3404045</span></td>       <td>4.</td><td>Cara Bayar</td><td ><span style="'+bordertd+'">'+result['carabayar']+'</span></td></tr>';
            cetak +='<tr><td>5.</td><td>Nomor Rekam Medis</td><td><span style="'+bordertd+'">'+result['norm']+'</span></td></tr>';
            cetak +='<tr><td>6.</td><td>Nama Pasien</td><td colspan="5"><span style="'+bordertd+'">'+result['identitas']+' '+result['namalengkap']+'</span></td></tr>';
            cetak +='<tr><td>7.</td><td>Jenis Perawatan</td><td><span style="'+bordertd+'">'+result['jenispemeriksaan']+'</span></td>       <td>8.</td><td>Poliklinik</td><td ><span style="'+bordertd+'">'+result['namaunit']+'</span></td></tr>';
            cetak +='<tr><td>9.</td><td>Total Biaya</td><td><span style="'+bordertd+'"> '+nominal+' </span></td>           <td>10.</td><td>Berat Lahir</td><td ><span style="'+bordertd+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Kg/Gram</td></tr>';
            cetak +='<tr><td>11.</td><td>Tanggal Masuk</td><td><span style="'+bordertd+'">'+result['tglperiksa']+'</span></td></tr>';
            cetak +='<tr><td>12.</td><td>Jumlah Hari Perawatan</td><td><span style="'+bordertd+'">1 Hari</span></td> <td>13.</td><td>Tanggal Lahir</td><td ><span style="'+bordertd+'">'+result['tanggallahir']+'</span></td></tr>';
            cetak +='<tr><td>14.</td><td>Usia</td><td><span style="'+bordertd+'">'+result['usia']+'</span></td></tr>';
            cetak +='<tr><td>15.</td><td>Jenis Kelamin</td><td><span style="'+bordertd+'">'+result['jeniskelamin']+'</span></td></tr>';
            cetak +='<tr><td>16.</td><td>Cara Pulang</td><td><span style="'+bordertd+'">'+result['kondisikeluar']+'</span></td></tr>';
            cetak +='</table></div>';

            cetak +='<div style="float: none; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<table border="1" cellspacing="0" cellspadding="0"  style="text-align:center;width:100%;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<tr align="left"><td colspan="6">Kode Diagnosa</td></tr>';
            cetak +='<tr ><td rowspan="2">NO</td> <td rowspan="2">KODE ICD-10</td><td rowspan="2" style="width:25%;">DIAGNOSIS</td><td colspan="2">KASUS</td><td rowspan="2">TTD DOKTER</td> </tr>';
            cetak +='<tr><td>L</td><td>B</td> </tr>';
            cetak +='<tr style="height:130px;"><td></td> <td>'+icdd+'</td> <td style="font-size:9px;text-align:left;">'+nicdd+'</td> <td></td> <td></td> <td  rowspan="6"> '+result['dokter']+'</td> </tr>';

            cetak +='<tr align="left"><td colspan="5">Kode Tindakan</td></tr>';
            cetak +='<tr><td>NO</td> <td>KODE ICD-9</td><td colspan="3">TINDAKAN/OPERASI</td></tr>';
            cetak +='<tr style="height:130px;"> <td></td> <td>'+icdt+'</td><td colspan="3" style="font-size:9px;text-align:left;">'+nicdt+'</td> </tr>';
        fungsi_cetaktopdf(cetak,'<style>@page {size:15cm 21cm; margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}


// mahmud, clear :: form asesmen medis rawat jalan
function asasmenmedisrawatjalan(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_asesmenmedisralan',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result) {
        stopLoading();
        
        if(result.asesmen==null){
           info('Data Tidak Ada.');
            return true;
        }
        
        var icdd='', icdt='',nicdd='', nicdt='';
        for(var x in result.hp)
        {
           if(result.hp[x].idjenisicd==2){icdd += result.hp[x].icd+'<br>'; nicdd += result.hp[x].namaicd+'<br>';}else{icdd += '';nicdd += '';}
           if(result.hp[x].idjenisicd!=2){icdt += result.hp[x].icd+'<br>'; nicdt += result.hp[x].namaicd+'<br>';}else{icdt += '';nicdt += '';}
        }
        var asesmen = result.asesmen;
        var result  = result.identitas;
        var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
        var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
        var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
        var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
        var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
        
        //header     
        var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                    <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%"><tr>\n\
                        <td '+bordertd+'><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td>\n\
                        <td '+bordernotleft+'> \n\
                            <table '+styleFont+'>\n\
                            <tr><td>No.RM</td><td>: '+result['norm']+'</td></tr>\n\
                            <tr><td>NIK</td><td>: '+result['nik']+'</td></tr>\n\
                            <tr><td>Nama</td><td>: '+result['identitas']+' '+result['namalengkap']+'</td></tr>\n\
                            <tr><td>Tanggal Lahir</td><td>: '+result['tanggallahir']+'</td></tr>\n\
                            </table>\n\
                        </td>\n\
                        <td '+bordernotleft+' width="100px;">Resiko Tinggi : Tidak </td></tr>\n\
                    <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>ASESMEN MEDIS RAWAT JALAN</b></td></tr>\n\
                    <tr><td style="text-align:center;'+bordernottop+'" colspan="3">Diisi oleh dokter</td></tr>\n\
                    </table>\n\
                    <table border="0" '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                    <tr><td style="width:180px;'+bordernottop+'">Keluhan Utama</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.keluhanutama+' </td></tr>\n\
                    <tr><td style="'+bordernottop+'">Riwayat Penyakit</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.riwayatpenyakit+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Riwayat Alergi</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.riwayatalergi+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Keadaan Umum</td> <td style="'+borderbottomright+'" colspan="2"> Tensi : '+asesmen.tensi+' mmHg &nbsp;&nbsp; Nadi: '+asesmen.nadi+' x/menit &nbsp;&nbsp; Respirasi: '+asesmen.respirasi+' x/menit &nbsp;&nbsp; Suhu: '+asesmen.suhu+' &#8451;</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Pemeriksaan Fisik</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pemeriksaanfisik+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Hasil pemeriksaan penunjang</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pemeriksaanpenunjang+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Diagnosa</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.diagnosa+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Pengobatan</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pengobatan+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Pemeriksaan lanjutan</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pemeriksaanlanjutan+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Diet</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.diet+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Rencana Asuhan</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.rencanaasuhan+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Keluar dari poliklinik : <br> '+asesmen.kondisikeluar+'</td> <td style="'+borderbottomright+'" colspan="2"> Kulon Progo, '+asesmen.tglperiksa+' jam '+asesmen.jamperiksa+' <br> Dokter Yang Memeriksa <br><br><br><br>'+asesmen.dokterpemeriksa+'</td></tr>\n\
                    </table></div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

// mahmud, clear :: form triase dan asesmen medis ugd
function triaseasesmenmedisugd(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_triaseasesmenmedisugd',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result){
        stopLoading();        
        if(result.asesmen==null){
           info('Data Tidak Ada.');
            return true;
        }
        
        //start skala input
        var row = '<tr><th>Pengkajian</th><th>0</th><th>1</th><th>2</th><th>Nilai</th></tr>';    
        var perulangan=0;
        var tr ='';
        var td ='';
        var grup=0;      
        var nilaiscala=0;

        for(var x in result.scalaflacc)
        {
            perulangan++;
            var checked = (( result.scalaflaccugd!=null && result.scalaflaccugd.length > 4 && (result.scalaflaccugd[grup].pengkajian==result.scalaflacc[x].pengkajian && result.scalaflaccugd[grup].nilai==result.scalaflacc[x].nilai) ) ? 'checked' : '' ) ;        
            td += '<td><label class="noweight"><input '+checked+' grup="'+grup+'" id="setnilai_grupflacc" nilai="'+result.scalaflacc[x].nilai+'" type="radio" name="rad_'+result.scalaflacc[x].pengkajian+'" value="'+result.scalaflacc[x].nilai+'" />&nbsp;'+result.scalaflacc[x].keterangan+'</label></td>';        
            if(perulangan==3)
            {
                tr  = '<tr><th>'+result.scalaflacc[x].pengkajian+'<input type="hidden" name="nameradioflacc[]" value="'+result.scalaflacc[x].pengkajian+'" /></th>';
                row += tr + td +'<td class="nilaiflacc" id="grup_'+grup+'">'+ ((result.scalaflaccugd!=null && result.scalaflaccugd.length > 4) ? result.scalaflaccugd[grup].nilai : 0 ) +'</td></tr>';            
                tr='';
                td='';
                perulangan=0;                
                nilaiscala += parseInt(result.scalaflaccugd[grup].nilai);
                grup++;
            }
            
        }
            
        row += '<tr><td colspan="4" style="border:0.7px solid;">&nbsp;&nbsp;Total Nilai</td><td style="border-top:0.7px solid;border-right:0.7px solid;border-bottom:0.7px solid;" id="totalflacc">&nbsp;'+nilaiscala+'</td></tr>';
        row += '<tr><td colspan="5"> <b>#Keterangan</b> &nbsp;&nbsp; 0 : nyaman , 1-3 : kurang nyaman , 4-5 : nyeri sedang , 7-10 : nyeri berat</td></tr>';
        var skalaflacc = '<table class="table table-bordered table-striped table-hover">'+row+'</table>';
        //skala input
               
        
        var asesmen = result.asesmen;
        var kstart  = result.kstart;
        var result  = result.identitas;
        var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
        var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
        var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
        var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
        var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
        var borderleftright='border-right:0.7px solid; border-left:0.7px solid;';
        
        //header     
        var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                    <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%"><tr>\n\
                        <td '+bordertd+' width="100"><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td>\n\
                        <td '+bordernotleft+' width="300"> \n\
                            <table '+styleFont+'>\n\
                            <tr><td>No.RM</td><td>: '+result['norm']+'</td></tr>\n\
                            <tr><td>NIK</td><td>: '+result['nik']+'</td></tr>\n\
                            <tr><td>Nama</td><td>: '+result['identitas']+' '+result['namalengkap']+'</td></tr>\n\
                            <tr><td>Tanggal Lahir</td><td>: '+result['tanggallahir']+'</td></tr>\n\
                            </table>\n\
                        </td>\n\
                        <td '+bordernotleft+' width="100px;">Resiko Tinggi : Tidak </td></tr>\n\
                    <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>TRIASE & ASESMEN MEDIS GAWAT DARURAT</b></td></tr>\n\
                    <tr><td style="text-align:center;'+bordernottop+'" colspan="3">Diisi oleh dokter</td></tr>\n\
                    </table>\n\
                    <table border="0" '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                    <tr><td colspan="3" style="'+bordernottop+'">Umur: '+result.usia+' &nbsp;&nbsp; Jk: '+result.jeniskelamin+' &nbsp;&nbsp;&nbsp; Agama: '+result.agama+' &nbsp;&nbsp;&nbsp; Tanggal Masuk: '+asesmen.tanggalmasuk+'&nbsp;&nbsp;&nbsp; Jam:'+asesmen.jammasuk+' <br> Alamat: '+result.alamat+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Informasi didapat dari</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.sumberinformasi+' </td></tr>\n\
                    <tr><td style="'+bordernottop+'">Cara masuk</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.caramasuk+' </td></tr>\n\
                    <tr><td style="'+bordernottop+'">Cara datang</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.caradatang+' </td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+' text-align:center;"><b>Kriteria START <i>(Simple Triage and Rapid Transportation)</i></b></td></tr>\n\
                    <tr><td style="'+bordernottop+'"><b>Warna</b></td> <td style="'+borderbottomright+'"><b>Keterangan</b></td> <td style="'+borderbottomright+'"><b> Respon Time </b></td></tr>\n\
                    <tr><td style="'+bordernottop+'">'+kstart.warna+'</td> <td style="'+borderbottomright+'">'+kstart.keterangan+'</td> <td style="'+borderbottomright+'"> '+kstart.respontime+' </td></tr>\n\
                    <tr><td colspan="3" style="'+borderleftright+'">&nbsp;</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'">KU : '+asesmen.keadaanumum+' <br> Kesadaran : '+asesmen.kesadaran+'</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'">TD : '+asesmen.tekanandarah+' mmHg &nbsp;&nbsp; N : '+asesmen.nadi+' x/menit &nbsp;&nbsp; RR : '+asesmen.respirasi+' x/menit &nbsp;&nbsp; S : '+asesmen.suhu+' &#8451; Spo2 : '+asesmen.spo2+' % &nbsp;&nbsp; BB : '+asesmen.beratbadan+' Kg &nbsp;&nbsp; TB : '+asesmen.tinggibadan+' cm  </td></tr>\n\
                    <tr><td style="'+bordernottop+'">Keluhan Utama</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.keluhanutama+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Riwayat Penyakit</td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.riwayatpenyakit+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Riwayat Alergi Obat dan Makanan</td> <td style="'+borderbottomright+'" colspan="2">'+result.alergi+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Pemeriksaan Fisik</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pemeriksaanfisik+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Pemeriksaan Lokalis</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.pemeriksaanlokalis+'</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'"><b>Skrining & Asesmen Nyeri</b></td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'">Nyeri untuk anak ≥ 6 tahun dan dewasa</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'"><img class="col-md-10" src="'+base_url+'assets/images/skalanyeri.jpg"> <br/> Hasil Intensitas Nyeri : '+asesmen.nilaiskalanyeri+'</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'">FLACC untuk anak < 6 tahun</td></tr>\n\
                    <tr><td colspan="3" style="'+bordernottop+'">'+skalaflacc+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Hasil Observasi Pasien</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.hasilobservasi+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Hasil pemeriksaan penunjang</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.hasilpemeriksaanpenunjang+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Hasil Pemeriksaan EKG/Radiologi</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.hasilpemeriksaanekg+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Diagnosis</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.diagnosis+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Diteruskan kepada / DPJP </td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.dpjp+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Nama & Tanda Tangan Dokter </td> <td style="'+borderbottomright+'" colspan="2"> <br> <br> <br><br> '+asesmen.dokterugd+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Rencana Asuhan</td> <td style="'+borderbottomright+'" colspan="2">'+asesmen.rencanaasuhan+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'" colspan="3">Tanggal Keluar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '+asesmen.tanggalkeluar+' <br> Jam keluar &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : '+asesmen.jamkeluar+' </td> </tr>\n\
                    <tr><td style="'+bordernottop+'">Kondisi Saat Keluar Dari IGD </td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.kondisikeluar+'</td></tr>\n\
                    <tr><td style="'+bordernottop+'">Keluar IGD </td> <td style="'+borderbottomright+'" colspan="2"> '+asesmen.carakeluar+'</td></tr>\n\
                    </table></div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//mahmud clear
function cetak_laporan_operasi(idp)
{
    var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
    var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
    var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
    var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
    var bordernotleft2='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var left = "border-left:0.7px solid; padding:0.7px 5px;";
    var right= "border-right:0.7px solid; padding:0.7px 5px;";
    var leftright  = "border-left:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;";
    var leftbottom = "border-left:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var rightbottom= "border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var leftbottomright= "border-left:0.7px solid;border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";


    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_laporanoperasi',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result){
        stopLoading();
        if(result.operasi == 'null')
        {
            alert('Data Operasi Tidak Ada.');
            return;
        }
        var jenisanestesi  =  result.jenisanestesi.jenisanestesi;
        var jenisoperasi  =  result.jenisoperasi.jenisoperasi;
        var result = result.operasi;
            var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td '+bordertd+' width="100"><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td>\n\
                    <td '+bordernotleft+' width="300"> \n\
                        <table '+styleFont+'>\n\
                        <tr><td>No.RM</td><td>: '+result.norm+'</td></tr>\n\
                        <tr><td>NIK</td><td>: '+result.nik+'</td></tr>\n\
                        <tr><td>Nama</td><td>: '+result.namalengkap+'</td></tr>\n\
                        <tr><td>Tanggal Lahir</td><td>: '+result.tanggallahir+'</td></tr>\n\
                        </table>\n\
                    </td>\n\
                    <td '+bordernotleft+' width="100px;">Resiko Tinggi : '+result.resikotinggi+' </td>\n\
                </tr>\n\
                <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>LAPORAN OPERASI</b></td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Nama Dokter Operator <br>'+result.dokteroperator+'</td>\n\
                    <td style="'+bordernotleft2+'">Nama Asisten Bedah <br>'+result.asistenbedah+'</td>\n\
                    <td style="'+borderbottomright+'">Nama Perawat Instrument <br>'+result.instrumen+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Diagnosis Pra Operasi <br> '+result.diagnosis_pra+'</td>\n\
                    <td style="'+bordernotleft2+'">Diagnosis Post Operasi <br> '+result.diagnosis_post+'</td>\n\
                    <td style="'+borderbottomright+'">Tindakan Operasi <br>'+result.tindakan_operasi+'</td>\n\
                </tr>\n\
                <tr><td colspan="3" style="'+leftright+'">Jenis Operasi &nbsp;&nbsp;&nbsp;&nbsp;: '+jenisoperasi+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftright+'">Macam Operasi : '+result.macamoperasi+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftbottomright+'">Jenis Anestesi &nbsp;&nbsp;&nbsp;: '+jenisanestesi+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftright+'">Jumlah Perdarahan : '+result.jumlahperdarahan+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftright+'">Patologi Anatomi &nbsp;&nbsp;: '+result.patologianatomi+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftright+'">Sitologi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: '+result.sitologi+' </td></tr>\n\
                <tr><td colspan="3" style="'+leftbottomright+'">Pemasangan Implant : '+result.pemasanganimplant+' </td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Tanggal Operasi <br>'+result.tanggaloperasi+'</td>\n\
                    <td style="'+bordernotleft2+'">Jam Mulai Operasi <br>'+result.jammulai+'</td>\n\
                    <td style="'+bordernotleft2+'">Jam Selesai Operasi <br>'+result.jamselesai+'</td>\n\
                    <td style="'+borderbottomright+'">Lama Operasi <br>'+result.lamaoperasi+' menit</td>\n\
                </tr>\n\
                <tr><td colspan="4" style="'+bordernottop+'"> Uraian Pembedahan (Prosedure operasi yang dilakukan dan rincian temuan, ada dan tidak adanya komplikasi)<br>'+result.uraianpembedahan+'</td></tr>\n\
                <tr>\n\
                    <td colspan="2" style="'+leftbottom+'">&nbsp:</td>\n\
                    <td colspan="2" style="'+rightbottom+' text-align:center;">Yogyakarta,  '+result.tanggalselesai+', Jam:'+result.jamselesai+' WIB <br>Dokter Operator <br><br><br><br>'+result.dokteroperator+'</td>\n\
                </tr>\n\
                </table>\n\
                </div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

//cetak laporan operasi
function cetak_catatan_terintegrasi(idp)
{
    var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
    var bordertd='style="border:0.7px solid; padding:0.7px 5px;"';
    var bordernottop='border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;';
    var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
    var bordernotleft2='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var borderbottomright='border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;';
    var left = "border-left:0.7px solid; padding:0.7px 5px;";
    var right= "border-right:0.7px solid; padding:0.7px 5px;";
    var buttom= "border-bottom:0.7px solid; padding:0.7px 5px;";
    var leftright  = "border-left:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;";
    var leftbottom = "border-left:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var rightbottom= "border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";
    var leftbottomright= "border-left:0.7px solid;border-right:0.7px solid; border-bottom:0.7px solid; padding:0.7px 5px;";

    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_catatanintegrasiranap',
        data:{idp:idp},
        dataType: "JSON",
        success: function(result){
        stopLoading();
        if(result.ranap == 'null')
        {
            alert('Data Tidak Ada.');
            return;
        }
        var rows = '';
        
        for(var x in result.catatan)
        {
            rows += '<tr><td style="'+bordernottop+'">'+result.catatan[x].tanggal+' / '+result.catatan[x].jam+'</td><td style="'+rightbottom+'">'+result.catatan[x].profesi+'</td><td style="'+rightbottom+'">'+result.catatan[x].soa_soa+'</td><td style="'+rightbottom+'">'+result.catatan[x].soa_p+'</td><td style="'+rightbottom+'">'+result.catatan[x].petugas+'<br>'+result.catatan[x].sip+'</td></tr>';
        }
        
            var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr><td colspan="2" style="text-align:right;">RM 05.3</td></tr>\n\
                <tr>\n\
                    <td '+bordertd+' width="100"><img style="margin-bottom:1px; width:7cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td>\n\
                    <td '+bordernotleft+' width="200"> \n\
                        <table '+styleFont+'>\n\
                        <tr><td>No.RM</td><td>: '+result.pasien.norm+' </td></tr>\n\
                        <tr><td>NIK</td><td>: '+result.pasien.nik+'</td></tr>\n\
                        <tr><td>Nama</td><td>: '+result.pasien.namalengkap+'</td></tr>\n\
                        <tr><td>Tanggal Lahir</td><td>: '+result.pasien.tanggallahir+'</td></tr>\n\
                        </table>\n\
                    </td>\n\
                </tr>\n\
                <tr><td style="text-align:center;'+bordernottop+'" colspan="3"><b>CATATAN TERINTEGRASI</b></td></tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td style="'+left+'" width="15%">Tanggal Masuk </td><td style="padding:0.7px 5px;">: '+result.ranap.tanggalmasuk+'</td>\n\
                    <td style="padding:0.7px 5px;" width="5%">Jam</td><td style="padding:0.7px 5px;">: '+result.ranap.jammasuk+'</td>\n\
                    <td style="padding:0.7px 5px;" width="22%">DPJP Utama</td><td style="'+right+'">: '+result.ranap.dokterutama+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td style="'+leftbottom+'" width="15%">Ruang</td><td style="'+buttom+'">: '+result.ranap.namabangsal+' '+result.ranap.nobed+'</td>\n\
                    <td style="'+buttom+'">Kelas</td><td style="'+buttom+'">: '+result.ranap.kelas+'</td>\n\
                    <td style="'+buttom+'">Dokter Rawat Bersama</td><td style="'+rightbottom+'">: </td>\n\
                </tr>\n\
                </table>\n\
                <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                <tr>\n\
                    <td style="'+bordernottop+'">Tanggal/Jam</td>\n\
                    <td style="'+bordernotleft2+'">Profesi</td>\n\
                    <td style="'+bordernotleft2+'">Hasil Asesmen Penatalaksanaan Pasien</td>\n\
                    <td style="'+borderbottomright+'">Intruksi Profesional Pemberi Asuhan</td>\n\
                    <td style="'+borderbottomright+'">Petugas</td>';
                cetak +=rows;                    
                cetak +='</tr>\n\
                </table>\n\
                </div>';
        fungsi_cetaktopdf(cetak,'<style>@page { margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}


function print_ringkasanpasienpulang(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_ringkasanpasienpulang',
        data:{idpendaftaran:idp},
        dataType: "JSON",
        success: function(result) {
        stopLoading();
        var diagnosaprimer = '', diagnosasekunder='', tindakan='';
        for(var x in result)
        {
           if(result[x]['jenis']=='icd10')
           {
               
               if(result[x]['level'] == 'primer')
               {
                   diagnosaprimer += result[x]['icd']+' '+result[x]['namaicd']+'<br>';
               }
               
               if(result[x]['level'] == 'sekunder')
               {
                   diagnosasekunder += result[x]['icd']+' '+result[x]['namaicd']+'<br>';
               }
               
           }
           
           if(result[x]['jenis'] == 'icd9')
           {
               tindakan += result[x]['icd']+' '+result[x]['namaicd']+'<br>';
           }
           
        }
        var styleFont = 'style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"';
        var bordernotleft='style="border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
        var borderleftbottom='style="border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;"';
        var borderleftbottomright='style="border-left:0.7px solid;border-bottom:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;"';
        var bordernottop='style="border-right:0.7px solid; border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;"';
        var borderleft='style="border-left:0.7px solid;"';
        var borderright='style="border-right:0.7px solid;"';
        var borderleftright='style="border-left:0.7px solid;border-right:0.7px solid;"';
        var borderbottomleft='style="border-bottom:0.7px solid; border-left:0.7px solid; padding:0.7px 5px;"';
        var borderbottomright='style="border-bottom:0.7px solid; border-right:0.7px solid; padding:0.7px 5px;"';
        var border='style="border-left:0.7px solid;border-right:0.7px solid; border-bottom:0.7px solid; border-top:0.7px solid; padding:0.7px 5px;"';
        var cetak = '<div style="float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;"">\n\
                    <table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%"><tr>\n\
                       <tr><td colspan="4" style="text-align:right;">RM 05.1</td></tr>\n\
                        <td '+border+' width="200px;"><img style="margin-bottom:1px; width:5cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td>\n\
                        <td '+bordernotleft+' colspan="2" wisth="60%" > \n\
                            <table '+styleFont+'>\n\
                            <tr><td>No.RM</td><td>: '+result[0]['norm']+'</td></tr>\n\
                            <tr><td>Nama</td><td>: '+result[0]['namapasien']+'</td></tr>\n\
                            <tr><td>Tanggal Lahir</td><td>: '+result[0]['tanggallahir']+'</td></tr>\n\
                            </table>\n\
                        </td>\n\
                        <td '+bordernotleft+' width="100px;">Resiko Tinggi : <br> '+result[0]['resikotinggi']+' </td></tr>\n\
                    <tr><td align="center" '+bordernottop+' colspan="4"><b>RINGKASAN PASIEN PULANG (<i>DISCHARGE SUMMARY</i>)</b></td></tr>\n\
                    </table><table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                    <tr><td '+borderleftbottom+' rowspan="2" colspan="2" width="50%">Alamat :<br>'+result[0]['alamat']+'</td><td '+borderleftbottom+'>Jenis Kelamin : <br>'+result[0]['jeniskelamin']+'</td><td '+borderleftbottomright+'>UMUR : <br>'+result[0]['usia']+'</td></tr>\n\
                    <tr><td '+borderleftbottom+'>Tanggal Masuk</td><td '+borderleftbottomright+'>Tanggal Keluar/Meninggal</td></tr>\n\
                    <tr><td '+borderleftbottom+'>Ruang : '+result[0]['namabangsal']+'</td><td '+borderleftbottom+'>Kelas : '+result[0]['kelas']+'</td><td '+borderleftbottom+'>'+result[0]['waktumasuk']+'</td><td '+borderleftbottomright+'>'+result[0]['waktukeluar']+'</td></tr>\n\
                    </table><table border="0"  '+styleFont+' cellspacing="0" cellspadding="0" width="100%">\n\
                    <tr><td colspan="2" '+bordernottop+'>INDIKASI RAWAT INAP :<br>'+result[0]['indikasirawatinap']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>DIAGNOSIS :<br>'+result[0]['diagnosis']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>KOMORDIBITAS LAIN :<br>'+result[0]['komordibitaslain']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>RINGKASAN RIWAYAT & PEMERIKSAAN FISIK (yang penting / berhubungan) :<br>'+result[0]['ringkasanriwayat_pemeriksaanfisik']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>HASIL LABORATORIUM / PA, RONTGEN, USG, dll (yang penting berhubungan) :<br>'+result[0]['hasilpemeriksaanpenunjang']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>TERAPI / PENGOBATAN (selama rawat inap) :<br>'+result[0]['terapiselamaranap']+'</td></tr>\n\
                    <tr><td '+borderleft+'> DIAGNOSA UTAMA :</td><td '+borderleftright+'> DIAGNOSA SEKUNDER :</td></tr>\n\
\n\
                    <tr><td '+borderbottomleft+'>'+diagnosaprimer+'</td><td '+borderleftbottomright+'>'+diagnosasekunder+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>TINDAKAN / PROSEDUR / OPERASI : <br> '+tindakan+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>TERAPI PULANG :<br>'+result[0]['terapipulang']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>KONDISI SAAT PULANG :<br>'+result[0]['kondisisaatpulang']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>CARA KELUAR RS : <br>'+result[0]['carapulang']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>ANJURAN / INTRUKSI DAN EDUKASI (TINDAK LANJUT) :<br>'+result[0]['anjuran']+'</td></tr>\n\
                    <tr><td colspan="2" '+bordernottop+'>TINDAK LANJUT :<br>'+result[0]['tindaklanjut']+'</td></tr>\n\
                    <tr><td colspan="2" '+borderleftright+'>&nbsp;Kulon Progo, </td></tr>\n\
                    <tr><td '+borderleft+' align="center">Dokter yang merawat</td><td '+borderright+' align="center">Pasien / keluarga pasien</td></tr>\n\
                    <tr><td '+borderbottomleft+' align="center"><br><br><br><br>'+result[0]['dokterdpjp']+'<br><hr style="width:70%;">Nama terang & tanda tangan</td><td '+borderbottomright+' align="center"><br><br><br><br><br><hr style="width:70%;">Nama terang & tanda tangan</td></tr>\n\
                    </table></div>';
        fungsi_cetaktopdf(cetak,'<style>@page {margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}

function print_forminacbgranap(idp)
{
    startLoading();
    $.ajax({
        type: "POST",
        url: base_url+'cadmission/cetak_forminacbgranap',
        data:{idpendaftaran:idp},
        dataType: "JSON",
        success: function(result) {
        stopLoading();
        var icdd='', icdt='',nicdd='', nicdt='', no=0;
        for(var x in result)
        {
           if(result[x]['jenis']=='icd10')
           {
               icdd += result[x]['icd']+'<br>'; 
               if(result[x]['level'] == 'primer')
               {
                   nicdd += 'Primer :' +result[x]['namaicd']+'<br> Sekunder : <br>';
               }
               else
               {
                   nicdd += ++no +'. '+result[x]['namaicd']+'<br>';
               }
               
           }
           
           if(result[x]['jenis'] == 'icd9')
           {
               icdt += result[x]['icd']+'<br>'; 
               nicdt += result[x]['namaicd']+'<br>';
           }
           
        }
        var blankspace = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        var bordertd='border:0.7px solid; padding:0.7px 5px;';
        var cetak = '<div style="height:2.5cm;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;""><table style="width:100%;" border="0"><tr><td><img style="margin-bottom:1px; width:6cm" src="'+base_url+'assets/images/headerkuitansismall.jpg" />\n</td><td style="padding-left:10px;font-size:15px;"> <b>FORMULIR <br>VERIFIKASI INA-CBG</b></td><td style="font-size:12px;padding-left:10px;"><span style="border:1px solid #000;padding:3px 4px;font-weight:bold;">RAWAT INAP</span></td></tr></table></div>';
            cetak +='<div style="height:6.5cm;float: none; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<table border="0" style=" width:100%;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            
            cetak +='<tr>\n\
            <td>1.</td><td>Nama RS</td><td ><span style="'+bordertd+'">RSU QUEEN LATIFA</span></td>\n\
            <td>2.</td><td>Kelas RS</td><td ><span style="'+bordertd+'">D</span></td>\n\
            </tr>';
            
            cetak +='<tr>\n\
            <td>3.</td><td>Nomor Kode RS</td><td > <span style="'+bordertd+'">3404045</span></td>\n\
            <td>4.</td><td>Cara Bayar</td><td ><span style="'+bordertd+'">'+blankspace+'</span></td>\n\
            </tr>';
            
            cetak +='<tr><td>5.</td><td>Nomor Rekam Medis</td><td><span style="'+bordertd+'">'+result[0]['norm']+'</span></td></tr>';
            cetak +='<tr><td>6.</td><td>Nama Pasien</td><td colspan="5"><span style="'+bordertd+'">'+result[0]['namapasien']+'</span></td></tr>';
            
            cetak +='<tr>\n\
            <td>7.</td><td>Jenis Perawatan</td><td><span style="'+bordertd+'">Rawat Inap</span></td>\n\
            <td>8.</td><td>Ruang/Bangsal</td><td ><span style="'+bordertd+'">'+result[0]['namabangsal']+'</span></td>\n\
            </tr>';
            
            cetak +='<tr>\n\
            <td>9.</td><td>Total Biaya</td><td><span style="'+bordertd+'"> '+blankspace+' </span></td>\n\
            <td>10.</td><td>Berat Lahir</td><td ><span style="'+bordertd+'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;Kg/Gram</td>\n\
            </tr>';
            
            cetak +='<tr>\n\
            <td>11.</td><td>Tanggal Masuk</td><td><span style="'+bordertd+'">'+result[0]['waktumasuk']+'</span></td>\n\
            <td>12.</td><td>Tanggal Keluar</td><td ><span style="'+bordertd+'">'+result[0]['waktukeluar']+'</span></td>\n\
            </tr>';
            
            cetak +='<tr>\n\
            <td>13.</td><td>Jumlah Hari Perawatan</td><td><span style="'+bordertd+'">'+blankspace+'</span></td>\n\
            <td>14.</td><td>Tanggal Lahir</td><td ><span style="'+bordertd+'">'+result[0]['tanggallahir']+'</span></td>\n\
            </tr>';
            
            cetak +='<tr><td>15.</td><td>Usia</td><td><span style="'+bordertd+'">'+result[0]['usia']+'</span></td></tr>';
            cetak +='<tr><td>16.</td><td>Jenis Kelamin</td><td><span style="'+bordertd+'">'+result[0]['jeniskelamin']+'</span></td></tr>';
            cetak +='<tr><td>17.</td><td>Cara Pulang</td><td><span style="'+bordertd+'">'+result[0]['carapulang']+'</span></td></tr>';
            cetak +='</table></div>';

            cetak +='<div style="float: none; font-size:extra-small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<table border="1" cellspacing="0" cellspadding="0"  style="text-align:center;width:100%;float: none; font-size:small;font-family: "Times New Rowman", "Book Antiqua", Palatino, serif;color: #333333;">';
            cetak +='<tr align="left"><td colspan="4">Kode Diagnosa</td></tr>';
            cetak +='<tr ><td>NO</td> <td>KODE ICD-10</td><td style="width:50%;">DIAGNOSIS</td><td>TTD DOKTER</td> </tr>';
            cetak +='<tr style="height:130px;"><td></td> <td>'+icdd+'</td> <td style="font-size:10.5px;text-align:left;">'+nicdd+'</td> <td  rowspan="4"> '+result[0]['dokterdpjp']+'</td> </tr>';

            cetak +='<tr align="left"><td colspan="3">Kode Tindakan</td></tr>';
            cetak +='<tr><td>NO</td> <td>KODE ICD-9</td><td>TINDAKAN/OPERASI</td></tr>';
            cetak +='<tr style="height:130px;"> <td></td> <td>'+icdt+'</td><td style="font-size:10.5px;text-align:left;">'+nicdt+'</td> </tr>';
        fungsi_cetaktopdf(cetak,'<style>@page {margin:0.2cm;}</style>');
        },
        error: function(result) {
            stopLoading();
            fungsiPesanGagal();
            return false;
        }
    });
}