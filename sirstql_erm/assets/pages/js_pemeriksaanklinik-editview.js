var norm = localStorage.getItem('norm');//siapkan norm -> ambil dari localstorage norm
var idperiksa = localStorage.getItem('idperiksa');//siapkan idperiksa -> ambil dari localstorage idperiksa
var idpendaftaran = localStorage.getItem('idp');
var modehalamanjs = $('input[name="modehalaman"]').val();
$(function(){
    $('input[name="idperiksa"]').val(idperiksa);
    $('#riwayatdtpasien').attr('norm',norm);
    pemeriksaanklinikDetailPasien();
});

function pemeriksaanRefreshRadiologi(value)//refreshDataDiagnosa
{
    var x=0, html='', subtotal=0;
    $('#listRadiologi').empty();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listradiologi',
        data: {x:value},
        dataType: "JSON",
        success: function(result) {
            var no=0;
            for(x in result)
            { no++;
                subtotal += parseInt(result[x].total);
                html += '<tr><td>'+ result[x].icd +'</td>'
                +'<td>'+ result[x].namaicd +'</td>'
                +'<td>'+result[x].jumlah+'</td>'
                +'<td>'+ convertToRupiah(result[x].total) +'</td></tr>';
            }
            $('#listRadiologi').html(html+'<tr class="bg bg-info"><td colspan="2">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function simpanhasilradiologi(id)
{
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_simpanhasilradiologi',
        data: {i:id, h: $('#'+id).val()},
        dataType: "JSON",
        success: function(result) {
            notif(result.status, result.message);
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function pemeriksaanRefreshLaboratorium(value)//refreshDataDiagnosa
{
    var x=0, html='', paket='', parent='', subtotal=0, editlab = $('input[name="edithasillab"]').val();
    $.ajax({
        type: "POST",
        url: 'pemeriksaan_listlaboratorium',
        data: {x:value, y:idperiksa},
        dataType: "JSON",
        success: function(result) {
            for(x in result)
            {   
                if(result[x].idjenisicd2=='4')
                {
                paket = ((namapaket==result[x].namapaketpemeriksaan && gruppaket==result[x].gruppaketperiksa) ? '' : ((result[x].namapaketpemeriksaan==null)?'':'<tr height="30px" '+((result[x].idgrup==result[x].idpaketpemeriksaan)? 'class="bg-info"' : '' )+'><td colspan="5" class="">'+result[x].namapaketpemeriksaan+'</td><td class="">'+ ((result[x].idgrup==result[x].idpaketpemeriksaan)? result[x].total : '' )+'</td><td>'+((result[x].isubahwaktuhasil == 1) ? '<a class="btn btn-warning btn-xs"  id="ubahwaktuhasil"><i class="fa fa-clock-o"></i> Ubah Waktu Hasil</a>' : '' )+'</td></tr>' ));
                subtotal += parseInt(result[x].total);
                html += paket + ((result[x].icd==null)?'': '<tr '+((result[x].idgrup!==null)?'':'class="bg-warning"')+'><td> &nbsp;&nbsp;&nbsp;'+ result[x].icd +' - '+result[x].namaicd +'</td>'+
                ///////set untuk simpan data
                '<td><input type="hidden" name="icd[]" value="'+result[x].icd+'"/><input type="hidden" id="istypehasil'+result[x].idhasilpemeriksaan+'" value="'+result[x].istext+'"/>'+
                '<input id="'+result[x].idhasilpemeriksaan+'" type="text" class="form-pelayanan" value="'+ result[x].nilai +'" onchange="pemeriksaan_savenilaipemeriksaan('+result[x].idhasilpemeriksaan+',\'laborat\')"  placeholder="Input '+result[x].istext+'" name="nilai[]" size="1" '+ ((editlab>0)? '' : 'readonly' ) +' /></td>'+
                '<td>'+ ((result[x].nilaidefault==null)? '' : result[x].nilaidefault)  +'</td>'+
                '<td>'+ ((result[x].nilaiacuanrendah==null)? '' : result[x].nilaiacuanrendah) + ((result[x].nilaiacuantinggi=='')? '' : ' - '+result[x].nilaiacuantinggi) +'</td>'+
                '<td>'+ result[x].satuan +'</td>'+
                '<td>'+ ((result[x].idgrup ==null) ? convertToRupiah(result[x].total) : '' )+'</td></tr>');
                var namapaket= result[x].namapaketpemeriksaan;
                var gruppaket=result[x].gruppaketperiksa;
                }
            }
            $('#listLaboratorium').empty();
            $('#listLaboratorium').html(html+'<tr class="bg bg-info"><td colspan="5">Subtotal</td><td colspan="2">'+convertToRupiah(subtotal)+'</td></tr>');
            $('[data-toggle="tooltip"]').tooltip(); //initialize tooltip
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}

 
$(document).on('click','#ubahwaktuhasil',function(){
    var modalTitle = 'Ubah Waktu Hasil Laboratorium';
    var modalContent = '<form action="" id="formUbah">' +
                            '<div class="form-group">' +
                                '<label>Waktu Proses</label>' +
                                '<input type="text" id="txtwaktuproses" name="txtwaktuproses" class="form-control"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label>Waktu Selesai</label>' +
                                '<input type="text"id="txtwaktuselesai" name="txtwaktuselesai"  class="form-control"/>' +
                            '</div>' +
                        '</form>';
    $.confirm({ //aksi ketika di klik menu
        title: modalTitle,
        content: modalContent,
        closeIcon: true,
        type:'orange',
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'simpan',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: base_url + 'cpelayanan/set_waktuhasillaborat',
                        data: { idpemeriksaan : idperiksa, ok:$('#txtwaktuselesai').val(), proses:$('#txtwaktuproses').val()},
                        dataType: "JSON",
                        success: function(result) {
                            notif(result.status,result.message);
                        },
                        error: function(result) { //jika error
                            fungsiPesanGagal(); // console.log(result.responseText);
                            return false;
                        }
                    });
                }
            },
            //menu back
            formReset:{
                text: 'batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        $.ajax({
            type: "POST",
            url: base_url + 'cpelayanan/get_waktuhasillaborat',
            data: { idpemeriksaan : idperiksa },
            dataType: "JSON",
            success: function(result) {
                $('#txtwaktuproses').val(result.proses);
                $('#txtwaktuselesai').val(result.selesai);
            },
            error: function(result) { //jika error
                fungsiPesanGagal(); // console.log(result.responseText);
                return false;
            }
        });
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});
    
/////////////////////////////////////////////////
// mahmud, clear --> save 
function pemeriksaan_savenilaipemeriksaan(value,user='')
{
    var istext = $('#istypehasil'+value).val();
    var nilai = $('#'+value).val();
    $.ajax({
            type:"POST",
            url:"pemeriksaan_savenilaipemeriksaan",
            data:{a:value,b:nilai,c:istext,d:idperiksa,u:user},
            dataType:"JSON",
            success:function(result){
                notif(result.status, result.message);
                if(result.status=='success')
                {
                    pemeriksaanklinikRefreshBhp(); //refresh diagnosa
                }
            },
            error:function(result){
                fungsiPesanGagal();
                return false;
            }
        });
}
function pemeriksaanklinikDetailPasien()//cari data pasien
{
    startLoading();
    $.ajax({
        type: "POST",
        url: 'pemeriksaanklinikpenunjang_caridetailpasien',
        data: {norm:norm, i:idperiksa},
        dataType: "JSON",
        success: function(result) {
            stopLoading();
            $('input[name="dokter"]').val(if_empty(result.periksa.namalengkap));
            $('textarea[name="keteranganlaboratorium"]').val(result.pasien.keteranganlaboratorium);
            $('textarea[name="keteranganradiologi"]').val(result.pasien.keteranganradiologi);
            $('textarea[name="saranradiologi"]').val(result.pasien.saranradiologi);
            viewIdentitasPasien(result.pasien, result.penanggung);//panggil view identitas
            $('input[name="idpendaftaran"]').val(result.periksa.idpendaftaran);
            $('input[name="norm"]').val(norm);
            $('input[name="idperson"]').val(result.pasien.idperson);
            $('input[name="status"]').val(result.pasien.idstatuskeluar);
            pemeriksaanRefreshRadiologi(result.periksa.idpendaftaran);
            tampilHasilRadiografi(result.periksa.idpendaftaran);
            pemeriksaanRefreshLaboratorium(result.periksa.idpendaftaran);
            $('input[name="idunit"]').val(result.periksa.idunit);
            $('.textarea').wysihtml5({toolbar:false});
        },
        error: function(result) { //jika error
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
}
function viewIdentitasPasien(value,valpenanggung)
{
    localStorage.setItem('namapasien',value.namalengkap);
    localStorage.removeItem('namapasien');
    localStorage.setItem('namapasien',value.namalengkap);
    var profile='';
    var detail_profile='';
    var dtp = '';
    for(var x in valpenanggung){dtp+='<tr><td>'+valpenanggung[x].namalengkap+'</td><td>'+valpenanggung[x].hubungan+'</td><td>'+valpenanggung[x].notelpon+'</td></tr>';}
    var penanggung = '<table class="table table-bordered"><tr><th>Nama Penanggung</th><th>Hubungan </th><th> No.Telp</th></tr>'+dtp+'</table>';
    if(modehalamanjs!=='pemeriksaanklinik2'){
    profile = '<div class="bg-info">'
                +'<div class="box-body box-profile" >'
                 +'<div class="login-logo" style="margin-bottom:0px;">'
                  +'<b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-2x"></i></b>'
                +'</div>'
                  +'<h3 class="profile-username text-center">'+value.namalengkap+'</h3>'
                  +'<p class="text-muted text-center">'+((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama )+'</p>'
                  +'<h5 class=" text-center">Usia '+ambilusia(value.tanggallahir, value.tglsekarang)+' </h5>'
                +'</div>'
              +'</div>';
    detail_profile = '<div class="bg-info"><div class="box-body box-profile" ><table class="table table-striped" >'
              +'<tbody>'
              +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>NIK </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nik+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RM </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norm+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-calendar margin-r-5"></i>Tanggal Lahir</strong></td>'
                  +'<td class="text-muted">: &nbsp;'+value.tanggallahir+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor JKN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nojkn+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-genderless margin-r-5"></i>Jenis Kelamin</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.jeniskelamin+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor SEP </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.nosep+'</td>'
                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-tint margin-r-5"></i>Gol Darah</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.golongandarah+' '+value.rh+' </td>' //'+value.golongandarah+' '+value.rh+'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nomor RUJUKAN </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.norujukan+'</td>'

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-book margin-r-5"></i>Pendidikan</strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.pendidikan+'</td>'

                  +((value.tanggalrujukan=='0000-00-00' || value.tanggalrujukan=='')? '<td colspan="2"></td>':'<td class="bg bg-red"><strong><i class="fa fa-circle-o margin-r-5"></i>Rujukan BPJS Selanjutnya</strong></td><td class="bg bg-red text-muted">: &nbsp; '+getNextDate(value.tanggalrujukan,90)+'</td>')

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-map-marker margin-r-5"></i> Agama</strong></td>'
                  +'<td class="text-muted" >: &nbsp;'+value.agama+'</td>'

                    +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Penjamin </strong></td>'
                +'<td class="text-muted">: &nbsp; '+value.carabayar+'</td>'

                +'</tr>'
                +'<tr>'
                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Alamat </strong></td>'
                  +'<td class="text-muted" width="250px">: &nbsp; '+value.alamat+'</td>'

                  +'<td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ayah </strong></td>'
                  +'<td class="text-muted">: &nbsp; '+value.ayah+'</td>'
                +'</tr>'
                +'<tr><td colspan="2"><strong><i class="fa fa-circle-o margin-r-5"></i>Penanggung </strong></td><td><strong><i class="fa fa-circle-o margin-r-5"></i>Nama Ibu </strong></td><td class="text-muted">: &nbsp; '+value.ibu+'</td></tr>'
                +'<tr><td colspan="2">'+penanggung+'</td></tr>'
                +'</tbody>'
             +'</table></div></div>';
         }else{
            profile = '<div class="login-logo" style="margin-bottom:0px;"><b class="text-yellow"><i class="fa '+((value.jeniskelamin=='laki-laki')?'fa-male':'fa-female')+' fa-md"></i></b></div>'
                  +'<p class="text-muted text-center">'+((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama )+'</p>'
                  +'<p class="text-center">'+value.norm+'</p>';
            detail_profile = '<h4 >Identitas Pasien </h4>   '

                  +'<table class="tbl">'
                      +'<tr>'
                        +'<td>Nama</td><td>: '+value.namalengkap+'</td>'
                        +'<td class="padLeft">J.Kelamin</td><td>: '+value.jeniskelamin+'</td>'
                        +'<td class="padLeft">Ayah</td><td>: '+value.ayah+'</td>'
                        +'<td class="padLeft">No.RM</td><td>: '+value.norm+'</td>'
                        +'<td class="padLeft">No.SEP</td><td>: '+value.nosep+'</td></tr>'
                      +'<tr>'
                        +'<td>Tgl.Lahir (Usia)</td><td>: '+value.tanggallahir+' ('+value.usia+')</td>'
                        +'<td class="padLeft">Gol.Darah</td><td>: '+value.golongandarah+' '+value.rh+'</td>'
                        +'<td class="padLeft">Ibu</td><td>: '+value.ibu+'</td>'
                        +'<td class="padLeft">No.JKN</td><td>: '+value.nojkn+'</td>'
                        +'<td class="padLeft">Penjamin</td><td>: '+value.carabayar+'</td></tr>'
                      +'<tr>'
                        +'<td>Alamat</td><td>: '+value.alamat+'</td>'
                        +'<td class="padLeft">Agama</td><td>: '+value.agama+'</td>'
                        +'<td class="padLeft">Penanggung</td><td>: '+value.penanggung+'</td>'
                        +'<td class="padLeft">No.Rujukan</td><td>: '+value.norujukan+'</td></tr>'
                  +'</table>';
         }
    $('#pemeriksaanklinik_profile').html(profile);
    $('#pemeriksaanklinik_detailprofile').html(detail_profile);
    // $('#view_identitasPasien').html('<b>No.RM:</b> '+value.norm+' ,<b> No.JKN:</b>  '+value.nojkn+',<b> Penjamin:</b>  '+value.carabayar+' ,<b> NIK:</b>  '+value.nik+', <b>Nama:</b>  '+value.namalengkap+', <b> TanggalLahir:</b> '+value.tanggallahir+', <b> Alamat:</b> '+value.alamat+' [<b style="color:#000;">'+ ((value.norm<'10083900') ? 'Pasien Lama' : value.ispasienlama ) +'</b>]');
}
function pemeriksaan_tampilpasien() //tampil data pasien
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_tampilpasien",
        data:{x:norm, y:idperiksa},
        dataType:"JSON",
        success: function(result){
            $('#labNormPasien').html(': '+result.p.norm);
            $('#labNamaPasien').html(': '+result.p.namalengkap);
            $('#labPengirim').html(': '+result.per.titeldepan+' '+result.per.namalengkap+' '+result.per.titelbelakang);
            $('#labKelaminPasien').html(': '+result.p.jeniskelamin);
            $('#labTgllahirPasien').html(': '+result.p.tanggallahir);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
//////////////MENAMPILKAN DATA DETAIL PASIEN//////////////////
function pemeriksaandetailTampilPasien()
{
    $.ajax({
        type:"POST",
        url:"pemeriksaan_detail_tampil_pasien",
        data:{x:idperiksa},
        dataType:"JSON",
        success:function(result){
            // console.log(result);
            pemeriksaandetailTampilPasienListData(result);
        },
        error:function(result){
            fungsiPesanGagal();
            return false;
        }
    });
}
function pemeriksaandetailTampilPasienListData(data)
{
    var modalTitle = '<div class="col-sm-12 center">Detail Pasien</div>';
    var modalContent = '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Nama</th><th style="font-weight:normal;">: '+if_empty(data.namalengkap)+'</th></tr>'+
                                    '<tr><th>No.RM</th><th style="font-weight:normal;">: '+if_empty(data.norm)+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamat+'</th></tr>'+
                                    '<tr><th>Telpon</th><th style="font-weight:normal;">: '+data.telponpasien+'</th></tr>'+
                                    '<tr><th>Kelamin</th><th style="font-weight:normal;">: '+data.jeniskelamin+'</th></tr>'+
                                    '<tr><th>TanggalLahir</th><th style="font-weight:normal;">: '+data.tanggallahir+'</th></tr>'+
                                    '<tr><th>Agama</th><th style="font-weight:normal;">: '+data.agama+'</th></tr>'+
                                    '<tr><th>Status</th><th style="font-weight:normal;">: '+data.statusmenikah+'</th></tr>'+
                                    '<tr><th>Golongan Darah</th><th style="font-weight:normal;">: '+data.golongandarah+'</th></tr>'+
                                    '<tr><th>Rhesus</th><th style="font-weight:normal;">: '+data.rh+'</th></tr>'+
                                    '<tr><th>Alergi</th><th style="font-weight:normal;">: '+data.alergi+'</th></tr>'+
                                '</table>'+
                            '</div>'+
                            '<div class="col-sm-6">'+
                                '<table class="table table-striped" >'+
                                    '<tr><th>Penanggung</th><th style="font-weight:normal;">: '+data.namapenjawab+'</th></tr>'+
                                    '<tr><th>Alamat</th><th style="font-weight:normal;">: '+data.alamatpenjawab+'</th></tr>'+
                                    '<tr><th>No.Telpon</th><th style="font-weight:normal;">: '+data.notelpon+'</th></tr>'+
                                    '<tr><th colspan="2"><h4>Layanan Pendaftaran</h4></th></tr>'+
                                    '<tr><th>Klinik</th><th style="font-weight:normal;">: '+data.namaunit+'</th></tr>'+
                                    '<tr><th>No.Antri</th><th style="font-weight:normal;">: '+data.noantrian+'</th></tr>'+
                                    '<tr><th>Dokter</th><th style="font-weight:normal;">: '+data.titeldepan+' '+data.namadokter+' '+data.titelbelakang+'</th></tr>'+
                                    '<tr><th>Carabayar</th><th style="font-weight:normal;">: '+data.carabayar+'</th></tr>'+
                                    '<tr><th>No.SEP</th><th style="font-weight:normal;">: '+data.nosep+'</th></tr>'+
                                '</table>'+
                            '</div>';
    $.alert({
        title: modalTitle,
        content: modalContent,
        columnClass: 'xl',
    });
}
function FormPemeriksaanKlinikSubmit(){$('#FormPemeriksaanKlinik').submit();} // fungsi dijalankan setelah menu simpan di klik
