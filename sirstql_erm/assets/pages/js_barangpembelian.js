/***** selectDistributor => menyimpan id distributor yang erpilih */
/***** storageBeliobat   => menyimpan data pembelian obat*/
/***** masterobat        => menyimpan master obat*/
/***** storageNofaktur   => menyimpan nofaktur*/
/***** iddetail          => menyimpan idbarangtransaksidetail*/
/***** caribulan              => menyimpan data pencarian bulan */

var table;
var barangpembelian;
$(document).ready(function () {
  switch($('#jsmode').val()) {
    case 'barangpembelian':
      pesanBrowserLocalStorage();
      removelocalstorage('lsbarangpembelian');
      barangpembelian();
      break;
    case 'addbarangpembelian':
      tampilbarangpembelian(getlocalstorage('lsbarangpembelian'));
      caribarang($('select[name="idbarang"]'),'cmasterdata/getrs_baranglikename');
      $('select[name="idbarangdistributor"]').select2();
      $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"});
      break;
    case 'editpembelianobat':
      editpembelianobat();
      break;
    case 'viewpembelianobat':
      viewpembelianobat();
      break;
    default:
      // code block
  }
});

//******* cari barang untuk ditambahklan ke localstorage [lsbarangpembelian]
function caribarang(attributname)
{
//    startLoading();
    attributname.select2({
    minimumInputLength: 4,
    allowClear: true,
    ajax: {
        url: base_url+"cmasterdata/getrs_baranglikename",
        dataType: 'json',
        delay: 150,
        cache: false,
        type:"POST",
        data: function (params) {
//            stopLoading();
            return {
                namaobat: params.term,
//                page: params.page || 1,
            };
        },
        processResults: function(data, params) { 
//            var page = params.page || 1; //  NO NEED TO PARSE DATA `processResults` automatically parse it //var c = JSON.parse(data);
            return {
                results: $.map(data, function (list) { return {id: list.idbarang +','+ list.kode+','+list.namabarang+','+list.namasatuan+','+list.hargabeli , text: list.kode +' '+ list.namabarang }}),
//                pagination: {
//                    more: (page * 10) <= data[0].total_count // THE `10` SHOULD BE SAME AS `$resultCount FROM PHP, it is the number of records to fetch from table` 
//                }
            };
        },              
    }
    });
}
//** menambahkan barang ke localstorage [lsbarangpembelian]
function addbarangpembelian(value)
{
    var arrdata = split_to_array(value);
    var dataobat = JSON.stringify({
        idbarang  :arrdata[0],
        kode      :arrdata[1],
        kadaluarsa:'',
        namabarang:arrdata[2],
        jumlah    :0,
        harga     :arrdata[4],
        namasatuan:arrdata[3],
        status:'',
      });
  addlocalstorage('lsbarangpembelian',dataobat);  
  $('select[name="idbarang"]').empty();
  $('select[name="idbarang"]').html('<option value="0">Cari barang/obat</option>');
  notif('success','Tambah barang berhasil..!');
  tampilbarangpembelian(getlocalstorage('lsbarangpembelian'));
}
function copybarangpembelian(name,index)
{
    copylocalstorage(name,index);
    notif('success','Saling barang berhasil..!');
   return tampilbarangpembelian(getlocalstorage(name));
}
function deletebarangpembelian(index)
{
    var dataobat = getlocalstorage('lsbarangpembelian');
        dataobat[index] = JSON.stringify({
        idbarang  :'',
        kode      :'',
        kadaluarsa:'',
        namabarang:'',
        jumlah    :'',
        harga     :'',
        namasatuan:'',
        status:'batal',
      });
    updatelocalstorage('lsbarangpembelian',dataobat);
    tampilbarangpembelian(getlocalstorage('lsbarangpembelian'));
}
//menampilkan data barang
function tampilbarangpembelian(data)
{
 var isi ='', no=0,total=0;
 for(i in data)
 {
  var obj = JSON.parse(data[i]);
    if(obj.status!=='batal')
    {
      // ((data[i].statusbarang==undefined)?JSON.parse(storageBeliobat[i]):storageBeliobat[i]);
      isi += '<tr>'    
      +'<input type="hidden" name="idbarang[]" id="idbarang'+i+'" value="'+obj.idbarang+'"/>'
      +'<input type="hidden" name="statusbarang[]" id="statusbarang'+i+'" value="'+obj.statusbarang+'"/>'
      +'<td>'+ ++no +'</td>'
      +'<td>'+ obj.kode +'<input type="hidden" id="kode'+i+'" name="kode[]" value="'+ obj.kode +'"/></td>'
      +'<td>'+ obj.namabarang +'<input type="hidden" id="namabarang'+i+'" name="namabarang[]" value="'+ obj.namabarang +'"/></td>'
      +'<td><input type="text" id="kadaluarsa'+i+'" class="form-control datepicker" size="1" name="kadaluarsa[]" value="'+ obj.kadaluarsa +'"/></td>'
      +'<td><input type="text"  id="harga'+i+'" class="form-control" size="1" name="harga[]" value="'+ obj.harga +'"/></td>'
      +'<td><input type="text"  id="jumlah'+i+'" class="form-control" size="1" name="jumlah[]" value="'+ obj.jumlah +'"/></td>'
      +'<td>'+ obj.namasatuan +'<input type="hidden" id="namasatuan'+i+'" name="namasatuan[]" value="'+ obj.namasatuan +'"/></td>'
//      +'<td><textarea id="keterangan'+i+'" rows="1" class="form-control" name="keterangan[]">'+ obj.keterangan +'</textarea></td>'
      +'<td><a onclick="copybarangpembelian(\'lsbarangpembelian\','+i+')" class="btn btn-warning btn-sm" data-toggle="tooltip" data-original-title="Salin"><i class="fa fa-copy"></i></a> <a onclick="deletebarangpembelian('+i+')" class="btn btn-danger btn-sm" data-toggle="tooltip" data-original-title="Batalkan"><i class="fa fa-minus-circle"></i></a></td>'
      +'</tr>';
    }
  }
  $('#bodybarangpembelian').empty();
  $('#bodybarangpembelian').html(isi);
  $('.datepicker').datepicker({autoclose: true,format: "yyyy-dd-mm", orientation: "bottom"}).datepicker("setDate", 'now' ); //Initialize Date picker
  $('[data-toggle="tooltip"]').tooltip();
}
//simpan  barangpembelian
function simpanbarangpembelian()
{
  if($('#nofaktur').val()=='')
  {
    notif('warning','NoFaktur Belum diisi.!');
  }
  else if($('#tanggalfaktur').val()=='')
  {
    notif('warning','Tanggal faktur Belum diisi.!');
  }
  else if($('#idbarangdistributor').val()==0)
  {
   notif('warning','Distributor Belum diisi..!.!'); 
  }
  else
  {
    // buat storage form inputan
//    localStorage.setItem('selectDistributor',$('select[name="iddistributor"]').val());
//    if(mode==1)
//    { 
      $('#Formbarangpembelian').submit();
//    }
//    else
//    {
//      $('#Formbarangtransaksi').removeAttr('action');
//      $("#Formbarangtransaksi").attr('action', base_url+'cmasterdata/save_pembelianbarangselesai');
//      $('#Formbarangtransaksi').submit();
//    }
  }
}
















//OLD SCRIPT 













function barangpembeliantambahobat()
{
  // isi variabel storageBeliobat dengan data pembelian obat
  var storageBeliobat = localStorage.getItem('storageBeliobat');//retrive store data
  storageBeliobat = JSON.parse(storageBeliobat);//converts string to object
  if(storageBeliobat == null){storageBeliobat = [];} //jika data null

  if($('select[name="iddisributor"]').val()=='')
  {
    notif('danger','Distributor tidak boleh kosong..!');
  }
  else if($('input[name="nofaktur"]').val()=='')
  {
   notif('danger','NoFaktur tidak boleh kosong..!'); 
  }
  else
  {
  var obat = JSON.stringify({
    idbarang  :'',
    batchno   :'',
    expdate   :'',
    namabarang:'',
    jumlah    :0,
    harga     :0,
    namasatuan    :'',
    total     :0,
    statusbarang:'',
    isedit    :'',
  });//menambahkan form input obat baru
  storageBeliobat.push(obat);
  localStorage.setItem('storageBeliobat', JSON.stringify(storageBeliobat));
  lisobat();
  return true;
  }
}


//**END BARANG PEMBELIAN
///************ BARANG TRANSAKSI
// mahmud clear 
// ketika menu edit di klik
$(document).on("click","#editpembelian", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("iddetail", $(this).attr("alt"));
        window.location.href=base_url + 'cmasterdata/editpembelianobat';
    } else {
         pesanUndefinedLocalStorage();
    }
});
// ketika menu view di klik
$(document).on("click","#viewpembelian", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("iddetail", $(this).attr("alt"));
        window.location.href=base_url + 'cmasterdata/viewpembelianobat';
    } else {
         pesanUndefinedLocalStorage();
    }
});
// ketika menu batal di klik
$(document).on("click","#batalpembelian", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
        localStorage.setItem("iddetail", $(this).attr("alt"));
        ubahstatus_btrxdetail('batal',localStorage.getItem("iddetail"));
    } else {
         pesanUndefinedLocalStorage();
    }
});
// ketika menu selesai di klik
$(document).on("click","#selesaipembelian", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
      localStorage.setItem("iddetail", $(this).attr("alt"));
      ubahstatus_btrxdetail('selesai',localStorage.getItem("iddetail"));
    } else {
         pesanUndefinedLocalStorage();
    }
});
// ketika menu terima di klik
$(document).on("click","#terimapembelian", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
      localStorage.setItem("iddetail", $(this).attr("alt"));
      ubahstatus_btrxdetail('diterima',localStorage.getItem("iddetail"));
    } else {
         pesanUndefinedLocalStorage();
    }
});
// ketika menu terima di klik
$(document).on("click","#batalselesai", function(){
    if (typeof(Storage) !== "undefined") { //jika browser tidak support webstorage
      localStorage.setItem("iddetail", $(this).attr("alt"));
      ubahstatus_btrxdetail('rencana',localStorage.getItem("iddetail"));
    } else {
         pesanUndefinedLocalStorage();
    }
});

// update status barang transaksi detail
function ubahstatus_btrxdetail(status,id)
{
  $.ajax({
    url:base_url+"cmasterdata/ubahstatus_btrxdetail",
    data:{status:status,id:id},
    type:"POST",
    dataType:"JSON",
    success:function(hasil){
      console.log(hasil);
      notif(hasil.status,hasil.message)
      if(hasil.status!='danger'){pembelianobat();}
    },
    error:function(hasil){
      fungsiPesanGagal();
      return false;
    }
  });
}
// merefresh halaman
function refresh_halaman()
{
    // table.state.clear();
    $('.datepicker').datepicker({autoclose: true,format: "M yyyy", viewMode: "months", minViewMode: "months",orientation: "bottom"}).datepicker("setDate", 'now' );
    window.location.reload(true);
}

// hapus data local storage
function removestoragepembelianobat()
{
  localStorage.removeItem('storageBeliobat');
  localStorage.removeItem('masterobat');
  localStorage.removeItem('selectDistributor');
}
// menampilkan data barangpembelian
function barangpembelian()
{
  localStorage.setItem('caribulan',$('input[name="bulan"]').val());
  startLoading();
  $.ajax({ 
    url: base_url+'cmasterdata/jsondistribusibarang',
    type : "POST",      
    dataType : "JSON",
    data:{bulan:$('input[name="bulan"]').val()},
    success: function(result) {
      stopLoading();
      isitable(result);
    },
    error: function(result){  
      stopLoading();
        fungsiPesanGagal();
        return false;
    }
  });
}
function isitable(data)
{
  // siapkan isi tabel
  var isi = '',no=0;
  for( x in data)
  { 
    isi +='<tr><td>'+ ++no +'</td><td>'+ data[x].nofaktur +'</td><td>'+ data[x].tanggalfaktur +'</td><td>'+ data[x].distributor +'</td><td>'+ data[x].total +'</td><td>'+ data[x].namalengkap +'</td><td><a id="detaildistribusi" alt="'+data[x].idbarangfaktur+'" class="btn btn-xs btn-info" '+tooltip('detail')+'><i class="fa fa-window-maximize"></i></a></td></tr>';
//    isi +='<tr style="'+qlstatuswarna(data[x].statustransaksi)+'"><td>'+no+'</td><td>'+data[x].notransaksi+'</td><td>'+data[x].nofaktur+'</td><td>'+data[x].distributor+'</td><td>'+data[x].tanggalkirim+'</td><td>'+data[x].tanggaltagih+'</td><td>'+data[x].jangkawaktu+' Hari</td><td>'+data[x].nominal+'</td><td>'+data[x].potongan+'</td><td>'+data[x].dibayar+'</td><td>'+data[x].kekurangan+'</td><td style="'+status('viewmode',data[x].kekurangan,data[x].nominal)+'">'+data[x].status+'</td><td style="'+keteranganbayar('viewmode',data[x].dibayar,data[x].jangkawaktu,data[x].nominal)+'">'+data[x].keterangan+'</td><td> '+((data[x].statustransaksi!='selesai')? '<a id="selesaipembelian" alt="'+data[x].idbarangtransaksidetail+'" class="btn btn-xs btn-success" '+tooltip('Selesai')+'><i class="fa fa-check"></i></a>' :'')+ ((data[x].statustransaksi=='diterima' || data[x].statustransaksi=='selesai')?'':' <a id="terimapembelian" alt="'+data[x].idbarangtransaksidetail+'" class="btn btn-xs btn-primary" '+tooltip('Terima')+'><i class="fa fa-check"></i></a> ')+((data[x].statustransaksi!=='selesai')?' <a class="btn btn-xs btn-warning" id="editpembelian" alt="'+data[x].idbarangtransaksidetail+'" '+tooltip('Ubah')+'><i class="fa fa-pencil"></i></a>':' <a class="btn btn-xs btn-warning" id="batalselesai" alt="'+data[x].idbarangtransaksidetail+'" '+tooltip('Batal Selesai')+'><i class="fa fa-times-circle"></i></a>')+' <a class="btn btn-xs btn-info" id="viewpembelian" alt="'+data[x].idbarangtransaksidetail+'" '+tooltip('Detail')+'><i class="fa fa-eye"></i></a> '+((data[x].statustransaksi=='batal' || data[x].statustransaksi=='selesai')?'':' <a id="batalpembelian" alt="'+data[x].idbarangtransaksidetail+'" class="btn btn-xs btn-danger" '+tooltip('Batal')+'><i class="fa fa-minus-circle"></i></a>')+'</td></tr>';
  }
  // siapkan tabel
  var table = '<table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">'
              +'<thead>'
                +'<tr><th>No</th><th>NoFaktur</th><th>Tgl.Faktur</th><th>Distributor</th><th>Total</th><th>User</th><th></th></tr>'
              +'</thead><tbody>' + isi +'</tfoot></table>';

  $('#tampilbarangpembelian').empty();
  $('#tampilbarangpembelian').html(table);
  $('.table').DataTable({
    "dom": '<"toolbar">frtip',
    "processing": true, 
    "stateSave": true,
    "columnDefs": [{"targets": [ 5], "orderable": false}],
    // "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
    //   $(nRow).attr('id', 'row' + iDataIndex);
    // },
    "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
        $('[data-toggle="tooltip"]').tooltip();
    },
  });
  var menu='<input name="bulan" class="form-control datepicker" size="4"/>';
      menu+=' <a class="btn btn-info btn-sm" onclick="barangpembelian()"><i class="fa fa-desktop"></i> Tampil</a>';
      menu+=' <a onclick="refresh_halaman()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
      menu+=' <a href="'+base_url+'cmasterdata/addbarangpembelian" class="btn btn-primary btn-sm"><i class="fa fa-shopping-cart"></i> '+((sessionStorage.belanjabarang==1)?"Belanja":"Pesan")+' Barang</a>';
//      menu+=' <a href="'+base_url+'cadmission/" class="btn btn-primary btn-sm"><i class="fa fa-mail-forward"></i> Retur Pembelian</a>';
//      menu+=' <a href="'+base_url+'cmasterdata/" class="btn btn-primary btn-sm"><i class="fa fa-tag"></i> Penjualan</a>';
//      menu+=' <a href="'+base_url+'cadmission/" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i> Retur Penjualan</a>';
  $("div.toolbar").html(menu);
  var caribulan = localStorage.getItem('caribulan');
  $('.datepicker').datepicker({autoclose: true,format: "M yyyy", viewMode: "months", minViewMode: "months",orientation: "bottom"}).datepicker("setDate", ((caribulan==null || caribulan==undefined)?'now':caribulan) ); //Initialize Date picker
}
///*****************************

///************ PEMBELIAM OBAT
// mahmud clear 
// function noFaktur()
// {
//   var akronim = explode_replace($('select[name="iddisributor"]').val(),',','').substring(1,9);
//   var d     = new Date(ambiltanggal($('input[name="tanggalkirim"]').val()));
//   var bulan = d.getMonth() + 1;
//   var tahun = d.getFullYear().toString().substring(2,5);
//   $('input[name="nofaktur"]').val(((akronim=='')?'': tahun+''+bulan+'/'+akronim ));
// }
// tambah pembelian barang
function jangkaWaktu()
{
  var jumlahHari= datediff($('input[name="tanggalkirim"]').val(),$('input[name="tanggaltagih"]').val());
  $('input[name="jangkawaktu"]').val(jumlahHari);
}
// ADD pembelian obat (local storage)
function tambahobat()
{
  // isi variabel storageBeliobat dengan data pembelian obat
  var storageBeliobat = localStorage.getItem('storageBeliobat');//retrive store data
  storageBeliobat = JSON.parse(storageBeliobat);//converts string to object
  if(storageBeliobat == null){storageBeliobat = [];} //jika data null

  if($('select[name="iddisributor"]').val()=='')
  {
    notif('danger','Distributor tidak boleh kosong..!');
  }
  else if($('input[name="nofaktur"]').val()=='')
  {
   notif('danger','NoFaktur tidak boleh kosong..!'); 
  }
  else
  {
  var obat = JSON.stringify({
    idbarang  :'',
    batchno   :'',
    expdate   :'',
    namabarang:'',
    jumlah    :0,
    harga     :0,
    namasatuan    :'',
    total     :0,
    statusbarang:'',
    isedit    :'',
  });//menambahkan form input obat baru
  storageBeliobat.push(obat);
  localStorage.setItem('storageBeliobat', JSON.stringify(storageBeliobat));
  lisobat();
  return true;
  }
}
// ****************
// UPDATE pembelian obat (local storage)
function ubahobat(index,stat)
{
  // isi variabel storageBeliobat dengan data pembelian obat
  var storageBeliobat = localStorage.getItem('storageBeliobat');//retrive store data
  storageBeliobat = JSON.parse(storageBeliobat);//converts string to object
  if(storageBeliobat == null){storageBeliobat = [];} //jika data null

  var total=parseInt($('#jumlah'+index).val()) * parseInt($('#harga'+index).val());
  storageBeliobat[index] = JSON.stringify({
    idbarang  : explode_getdt($('#idbarang'+index).val(),',',0),
    batchno   : $('#batchno'+index).val(),
    expdate   : $('#expdate'+index).val(),
    jumlah    : $('#jumlah'+index).val(),
    harga     : $('#harga'+index).val(),
    namasatuan     : $('#satuan'+index).val(),
    total     : total,
    statusbarang: stat,
    isedit    : $('#isedit'+index).val()
  });//ubah data yang terpilih sesuai dengan index
  localStorage.setItem('storageBeliobat', JSON.stringify(storageBeliobat));

  // notif(((stat=='')?'primary':((stat=='dibayar')?'success':'danger')),((stat=='batal' || stat=='hapus')?'Ubah berhasil.':'Ubah berhasil.') );
  lisobat();
}
function lisobat()
{
  // isi variabel storageBeliobat dengan data pembelian obat
  var storageBeliobat = localStorage.getItem('storageBeliobat');//retrive store data
  storageBeliobat = JSON.parse(storageBeliobat);//converts string to object
  // if(storageBeliobat == null){storageBeliobat = [];} //jika data null
 var isi ='', no=0,total=0;
 for(i in storageBeliobat)
 { 
  var obi = storageBeliobat[i];
  var obi = ((storageBeliobat[i].statusbarang==undefined)?JSON.parse(storageBeliobat[i]):storageBeliobat[i]);
    // if(obi.statusbarang!='hapus')
    // {
      
      ((obi.statusbarang=='hapus')? '' : ++no );
      var disabled=((obi.statusbarang=='batal')?'readonly':'');
      total += ((obi.statusbarang=='hapus' || obi.statusbarang=='batal')? 0 : parseInt(obi.total));
      isi += '<tr style="'+((obi.statusbarang=='batal')? qlstatuswarnapp(3) : ((obi.statusbarang=='diterima')? qlstatuswarnapp(2) : ((obi.statusbarang=='rencana')? qlstatuswarnapp(4):((obi.statusbarang=='hapus')? 'display:none;':''))))+'" class="text-black">'
                +'<td>'+ no +' <input type="hidden" name="statusbarang[]" value="'+obi.statusbarang+'"/></td>'
                +'<td><select id="idbarang'+i+'" onchange="cekhargaobat(this.value,'+i+')" name="idbarang[]" class="form-control qlselect2" style="width: 100%;" '+disabled+'><option value="">Pilih</option></select></td>'
                +'<td><input class="form-control" size="1" type="text" name="batchno[]" id="batchno'+i+'" value="'+obi.batchno+'" '+disabled+'></td>'
                +'<td><input class="form-control expdate" size="1" type="text" name="expdate[]" id="expdate'+i+'" value="'+obi.expdate+'" '+disabled+'></td>'
                +'<td><input class="form-control" size="1" type="text" name="jumlah[]" id="jumlah'+i+'" value="'+obi.jumlah+'" '+disabled+'></td>'
                +'<td><input type="text" class="form-control" size="1" id="satuan'+i+'" value="'+obi.namasatuan+'" disabled></td>'
                  +'<td><input type="text" class="form-control" size="1" name="harga[]" id="harga'+i+'" value="'+obi.harga+'" '+disabled+'></td>'
                  +'<td id="total'+i+'"><input type="hidden" value="'+obi.total+'" name="total[]" />'+obi.total+'</td>'
                  +'<td> '+(($('#jsmode').val()!=='viewpembelianobat')?' <a onclick="ubahobat('+i+',\'diterima\')" class="btn btn-xs btn-success" '+tooltip('diterima')+'><i class="fa fa-check"></i></a> <a onclick="ubahobat('+i+',\'rencana\')" class="btn btn-xs btn-primary" '+tooltip('simpan')+'><i class="fa fa-save"></i></a> '+((obi.statusbarang=='batal')?'':'<a onclick="ubahobat('+i+',\'batal\')" class="btn btn-xs btn-warning" '+tooltip('batal')+'><i class="fa fa-minus-circle"></i></a> ')+'<a onclick="ubahobat('+i+',\'hapus\')" class="btn btn-xs btn-danger" '+tooltip('hapus')+'><i class="fa fa-trash"></i></a>':'')
                  + '<input id="isedit'+i+'" type="hidden" name="isedit[]" value="'+((storageBeliobat[i].statusbarang==undefined)?obi.isedit:'edit')+'"/>'
                  +'</td>'
                +'</tr>';
                isicomboobat('idbarang'+i,obi.idbarang);
    // }
  }
  $('#bodylistobat').empty();
  $('#bodylistobat').html(isi);
  $('.qlselect2').select2();
  
  $('.expdate').datepicker({autoclose: true,format:"mm-dd-yyyy",orientation: "top"});

  $('[data-toggle="tooltip"]').tooltip();
  nominal(total);
  kekurangan();
}
function nominal(nominal)//hitung nominal
{
  $('input[name="nominal"]').val(nominal);
  status();
}
function kekurangan() //hitung kekurangan
{
  var nominal = $('input[name="nominal"]').val();
  var dibayar = $('input[name="dibayar"]').val();
  var potongan = $('input[name="potongan"]').val();
  var kekurangan = parseInt(((nominal=='')? 0 : nominal)) - parseInt(((dibayar=='')?0:dibayar)) - parseInt(((potongan=='')? 0 :potongan));
  $('input[name="kekurangan"]').val(kekurangan);
  status();
}
function status(mode=null,kekurangan=null,nominal=null)//buat status pembelian obat
{
  if(mode==null)
  { 
    var kekurangan = $('input[name="kekurangan"]').val();
    var nominal    = $('input[name="nominal"]').val();
    keteranganbayar();
  }
  var status='';
  if(kekurangan > 0)
  {
    if(mode=='viewmode')
    { 
        return qlstatuswarna2(1);
    }
      status='BELUM LUNAS';
      $('input[name="statusdetail"]').css("background-color","#ffc107");
  }
  else if(kekurangan < 0)
  {
    if(mode=='viewmode'){return qlstatuswarna2(2);}
      status='KELEBIHAN BAYAR';
      $('input[name="statusdetail"]').css("background-color","#ffeb3b");
  }
  else if(nominal== 0)
  {
   status='';
  }
  else 
  {
    if(mode=='viewmode'){return qlstatuswarna2(3);}
     status='LUNAS';
     $('input[name="statusdetail"]').css("background-color","#cddc39");
  }

  ((mode==null)? $('input[name="statusdetail"]').val(status):'');
}
// buat keterangan pembayaran
function keteranganbayar(mode=null,dibayar=null,jangkawaktu=null,nominal=null)
{
  if(mode==null)
  { 
    var dibayar =  $('input[name="dibayar"]').val();
    var jangkawaktu = $('input[name="jangkawaktu"]').val();
    var nominal = $('input[name="nominal"]').val();
  }
  var keterangan='';
  if(dibayar>0)
  {
    if(mode=='viewmode'){return qlstatuswarna2(3);}
      keterangan='DIBAYAR';
      $('input[name="keterangan"]').css("background-color","#cddc39");
  }
  else if(dibayar=='' && jangkawaktu < 0 )
  { 
    if(mode=='viewmode'){return qlstatuswarna2(2);} 
      keterangan='TERLAMBAT BAYAR';
      $('input[name="keterangan"]').css("background-color","#ff5722");
  }
  else if(nominal == 0)
  {
   keterangan=''; 
  }
  else if(dibayar=='' || dibayar==0)
  {
    if(mode=='viewmode'){return qlstatuswarna2(2);}
      keterangan='BELUM DIBAYAR';
      $('input[name="keterangan"]').css("background-color","#ffc107");
  }
  ((mode==null)? $('input[name="keterangan"]').val(keterangan):'');
}
function pembayaran() //hitung pembayaran
{
  kekurangan();
}
function isicomboobat(id, selected)
{
    if (is_empty(localStorage.getItem('masterobat')))
    {
        $.ajax({ 
            url: base_url+'cmasterdata/getjsonmasterobat',
            type : "POST",      
            dataType : "JSON",
            success: function(result) {                                                                   
                localStorage.setItem('masterobat', JSON.stringify(result));
                return fillobat(id, selected);
            },
            error: function(result){                  
                notif(result.status, result.message);
                return false;
            }
        });
    }
    else
    {
        $.ajax({
            url: '../cmasterdata/kosongan',
            type : "post",      
            dataType : "json",
            succcess: function(result){
                fillobat(id, selected);
            },
            error: function(result){
                fillobat(id, selected);
            }
        });
    }


}
function fillobat(id, selected)
{
    var obat = JSON.parse(localStorage.getItem('masterobat'));
    var i=0;
    for (i in obat)
    {
      $("#"+id).append('<option ' + ((obat[i].idbarang===selected)?'selected':'') + ' value="'+obat[i].idbarang+','+obat[i].hargabeli+','+obat[i].namasatuan+'"> '+obat[i].kode+ ' ' +obat[i].namabarang+'</option>');
    }
}

function simpanbarang()
{
  if($('select[name="iddistributor"]').val()=='')
  {
    notif('warning','Distributor tidak boleh kosongan.!');
  }
  else if($('input[name="nofaktur"]').val()=='')
  {
    notif('warning','Nofaktur tidak boleh kosongan.!');
  }
  else if($('select[name="idbarang"]').val()=='')
  {
   notif('warning','Barang belum dipilih..!.!'); 
  }
  else
  {
    // buat storage form inputan
    localStorage.setItem('selectDistributor',$('select[name="iddistributor"]').val());
    if(mode==1)
    { 
      $('#Formbarangtransaksi').submit();
    }
    else
    {
      $('#Formbarangtransaksi').removeAttr('action');
      $("#Formbarangtransaksi").attr('action', base_url+'cmasterdata/save_pembelianbarangselesai');
      $('#Formbarangtransaksi').submit();
    }
  }
}
// UBAH PEMBELIAN OBAT
function ubahpembelianobat(idpo)
{
  startLoading();
  $.ajax({ 
    url: base_url+'cmasterdata/getpembelianobat',
    type : "POST",      
    dataType : "JSON",
    success: function(result) {
      stopLoading();
      isitable(result)
    },
    error: function(result){  
      stopLoading();
        fungsiPesanGagal();
        return false;
    }
  });
}
// ambil data distributor
function getjsondistributor()
{
    localStorage.removeItem('selectDistributor')
  $.ajax({ 
    url: base_url+'cmasterdata/getjsondistributor',
    type : "POST",      
    dataType : "JSON",
    success: function(result) {
        isicombodistributor(result,localStorage.getItem('selectDistributor'));

    },
    error: function(result){
        fungsiPesanGagal();
        return false;
    }
  });
}
// isi combo box distributor
function isicombodistributor(data,selected='')
{
    alert(selected);
  var isicombo='';
  for(x in data)
  {
    isicombo += '<option value="'+data[x].iddistributor+'" '+((data[x].iddistributor==selected)?'selected':'')+'>'+data[x].distributor+'</option>';
  }
  var html='<select  class="form-control distributor" name="iddistributor"><option value="">Pilih</option>'+isicombo+'</select>';
  $('#filldistributor').empty();
  $('#filldistributor').html(html);
  $('.distributor').select2();
}

// MODE VIEW PEMBELIAN BARANg
function viewpembelianobat()
{
  geteditpembelianobat();
  $('.datepicker').datepicker({autoclose: true,format:"yyyy-mm-dd",orientation: "bottom"});
  
  setTimeout(function(){
    // $('.btn').hide();
    $('form *').prop('disabled', true);
    // $('#btnkembali').show();
  },1);
}
// MODE UBAH PEMBELIAN BARANg
function editpembelianobat()
{
  geteditpembelianobat();
  $('.datepicker').datepicker({autoclose: true,format:"yyyy-mm-dd",orientation: "bottom"});
}
// ambil data ubah pembelian obat
function geteditpembelianobat()
{
  $.ajax({ 
    url: base_url+'cmasterdata/geteditpembelianobat',
    type : "POST",      
    dataType : "JSON",
    data:{id:localStorage.getItem('iddetail')},
    success: function(result) {
        isiformdetail(result.trxdetail);
        localStorage.setItem('storageBeliobat',JSON.stringify(result.trx));
        lisobat();
    },
    error: function(result){
        fungsiPesanGagal();
        return false;
    }
  });
}
// tampilkan di form detail
function isiformdetail(data)
{
  localStorage.setItem('selectDistributor',data['iddistributor']);
  getjsondistributor();
  $('input[name="iddetail"]').val(data['idbarangtransaksidetail']);
  $('input[name="nofaktur"]').val(data['nofaktur']);
  $('input[name="jangkawaktu"]').val(data['jangkawaktu']);
  $('input[name="tanggalkirim"]').val(data['tanggalkirim']);
  $('input[name="tanggaltagih"]').val(data['tanggaltagih']);
  $('input[name="dibayar"]').val(data['dibayar']);
  $('input[name="potongan"]').val(data['potongan']);
  $('input[name="nominal"]').val(data['nominal']);
  $('input[name="kekurangan"]').val(data['kekurangan']);
  $('input[name="status"]').val(data['status']);
  $('input[name="keterangan"]').val(data['keterangan']);

}
// jika barang dipilih cek harga obat
function cekhargaobat(value,no)
{
    console.log(value);
  $('#harga'+no).val(explode_getdt(value,',',1));
  $('#satuan'+no).val(explode_getdt(value,',',2));
}