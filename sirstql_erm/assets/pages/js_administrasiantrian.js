//untuk menampilkan date picker di textbbox
$(function () {
     $('.datepicker').datepicker({autoclose: true,format: "dd/mm/yyyy",orientation: "bottom"}).datepicker("setDate",'now'); //Initialize Date picker
});

//tombol tampilkan diklik
function tampilkanantrian()
{  
    startLoading();
    $.ajax({
        url:'loadantrian',
        type:'POST',
        dataType:'JSON',
        data:{i:$('select[name="unit"]').val(), t:ambiltanggal($('input[name="tanggal"]').val())},
        //panggil Cpelayanan/loadantrian(), hasil json-nya disimpan di dalam result
        success: function(result){
            stopLoading();
            isiTabel(result);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });  
}

//dipanggil dari a id pemeriksaanklinikBeriantrian - di dalam fungsi tampilkanantrian()
var jsantrian = document.createElement('script');
jsantrian.src = base_url+'/assets/pages/antrian_antrian.js';
document.head.appendChild(jsantrian);

$(document).on("click","#pemeriksaanklinikBeriantrian", function(){
    var idp = $(this).attr("alt");
    var idu = $(this).attr("alt2");
    var idpr = $(this).attr("alt3");
    var id = $(this).attr("alt4");
    // setTimeout(function(){
    beriAntrian(idp,idu,idpr,id);
// update isantri di pendaftaran},500);
});

//jika txtqrcode ditekan enter
$("#textqrcodeantrian").
on("keydown", function (e) {
    if (e.keyCode == 13) {
    $.ajax({
        url:'loadantrianbypasien',
        type:'POST',
        dataType:'JSON',
        data:{q:$('input[name="textqrcodeantrian"]').val()},
        //panggil Cpelayanan/loadantrian(), hasil json-nya disimpan di dalam result
        success: function(result){
            isiTabel(result);
        },
        error: function(result){
            fungsiPesanGagal(); // console.log(result.responseText);
            return false;
        }
    });
    }
});

function isiTabel(result)
{
    var html = '<table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">'+
              '<thead>'+
              '<tr>'+
                '<th>NO</th>'+
                '<th>POLI/UNIT</th>'+
                '<th>JADWAL</th>'+
                '<th>NO.RM</th>'+
                '<th>NAMA PASIEN</th>'+
                '<th>NO.SEP</th>'+
                '<th>NO.PESAN</th>'+
                '<th>NO.ANTRIAN</th>'+
                '<th></th>'+
              '</tr>'+
              '</thead>'+
              '<tbody>';
    var i=0;
    var id=0;
    for(i in result)
    {
        no = parseInt(i)+1;
        html = html + '<tr id="row'+no+'"><td>'+no+'</td><td>'+result[i].namaunit+'</td><td>'+result[i].tanggal+'</td><td>'+result[i].norm+'</td><td>'+result[i].namalengkap+'</td><td>'+result[i].nosep+'</td><td>'+result[i].nopesan+'</td><td id="antrian'+result[i].idpemeriksaan+'">'+result[i].noantrian+'</td>';
        if (result[i].noantrian == 0)
            html = html + '<td id="tombol'+result[i].idpemeriksaan+'"><a id="pemeriksaanklinikBeriantrian" nobaris="'+no+'" alt="'+result[i].idpemeriksaan+'" alt2="'+result[i].idunit+'" alt3="'+result[i].idperson+'" alt4="'+result[i].idpendaftaran+'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Beri Antrian"  href="#"><i class="fa fa-check"></i></a></td>';
        else
            html = html + '<td></td>';
        html = html + '</tr>';
    }
    html += '</tfoot></table>';
    //bersihkan isi tabel di v_administrasiantrian dengan id listantrian, kemudian isi
    $('#listantrian').empty();
    $('#listantrian').html(html);
    $('.table').DataTable({"stateSave": true,});
}