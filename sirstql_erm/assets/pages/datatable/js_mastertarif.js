var table;
$(function(){
  table = $('#example').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_dt_mastertarif",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [3,4,5,6,7,8,9,10], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  $("div.toolbar").html('<a href="'+base_url+'cmasterdata/add_mastertarif" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data</a> <a style="margin-right: 6px;" onclick="window.location.reload(true);" class="btn btn-default btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>');
});
