var table;
$(function(){
  //datatables
  table = $('#example').DataTable({
      "dom": '<"toolbar">frtip', //-- 
      "processing": true, 
      "serverSide": true,
      "stateSave": true, 
      "order": [], 
       
      "ajax": {
          "url": "loaddataicd",
          "type": "POST"
      },
      language : {
        sLoadingRecords : '<span style="width:100%;"><img src="'+base_url+'assets/ajax-load.gif"></span>'
      },
      "columnDefs": [
      { 
          "targets": [ 0 ], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu='<a href="'+base_url+'cmasterdata/add_icd" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Entri ICD</a>';
      menu += ' <a onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>';
      $("div.toolbar").html(menu);
});