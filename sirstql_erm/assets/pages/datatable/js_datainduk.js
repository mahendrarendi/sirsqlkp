var table = '';

$(document).ready(function () {
    listdata();
});


function listdata()
{
    table = $('#datainduk').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "paging":false,
      "false":false,
      "info":false,
      "order": [], 
       
      "ajax": {
          "url": base_url +"cadmission/dt_datainduk",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [ 5,6,7 ], 
          "orderable": false, 
      },
      ],
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
}


//mahmud, clear
$(document).on('click','#rmganda',function(){
  var modalContent = 
  '<form action="" id="FormPindahRM">' +
    '<div class="form-group">' +
        '<label class="control-label">RM.Asal</label>' +
        '<div class="col-sm-12"><select class="form-control select2" name="rmasal" id="rmasal"><option value="">Pilih</option></select></div>' +
    '</div>' +
    '<div class="col-sm-12">&nbsp;</div>'+
    '<div class="form-group">' +
        '<label class="control-label">RM.Tujuan</label>' +
        '<div class="col-sm-12"><select class="form-control select2" name="rmtujuan" id="rmtujuan"><option value="">Pilih</option></select></div>' +
    '</div>' +
    
'</form>';
    $.confirm({ //aksi ketika di klik menu
        title: 'Pindah Data RM',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'Pindah',
                btnClass: 'btn-primary',
                action: function () {
                    if ($('select[name="rmasal"]').val() == '')
                    {
                        alert_empty('NO.RM');
                        return false;
                    }
                    else if ($('select[name="rmtujuan"]').val() == '')
                    {
                        alert_empty('NO.RM');
                        return false;
                    }
                    else
                    {
                        $.ajax({
                          type:"POST",
                          url:base_url+"cadmission/perbaikandatapasien_rmganda",
                          data:{1:$('select[name="rmasal"]').val(),2:$('select[name="rmtujuan"]').val()},
                          dataType:"JSON",
                          success:function(result){
                            notif(result.status, result.message);
                          },
                          error:function(result){
                            fungsiPesanGagal();
                            return false;
                          }

                        });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
          select2serachmulti($('#rmasal'),'cmasterdata/fillcaripasien');  
          select2serachmulti($('#rmtujuan'),'cmasterdata/fillcaripasien');  
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});


//mahmud, clear
$(document).on('click','#pindahriwayat',function(){
  var modalContent = 
  '<form action="" id="FormPindahRiwayat">' +
    '<div class="form-group">' +
        '<label class="col-sm-4">RM.Asal</label>' +
        '<div class="col-sm-8"><input type="text" placeholder="No.RM Asal" name="rmasal" class="form-control" id="idrmasal" /></div>' +
    '</div>' +
    '<div class="col-sm-12"></div>'+
    '<div class="form-group">' +
        '<label class="col-sm-4">Tanggal Periksa</label>' +
        '<div class="col-sm-8"><select name="tanggalperiksa" id="tanggalperiksa" class="form-control"><option value="">Pilih Tanggal Periksa</option></select></div>' +
    '</div>' +
    '<div class="col-sm-12"></div>'+
    '<div class="form-group">' +
        '<label class="col-sm-4">RM.Tujuan</label>' +
        '<div class="col-sm-8"><input type="text" placeholder="No.RM Tujuan" name="rmtujuan" class="form-control"/></div>' +
    '</div>' +
    
'</form>';
    $.confirm({ //aksi ketika di klik menu
        title: 'Pindah Riwayat Pemeriksaan',
        content: modalContent,
        columnClass: 'medium',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: 'Pindah',
                btnClass: 'btn-primary',
                action: function () {
                    if ($('input[name="rmasal"]').val() == '')
                    {
                        alert_empty('NO.RM Asal');
                        return false;
                    }
                    else if ($('input[name="tanggalperiksa"]').val() == '')
                    {
                        alert_empty('Tanggal Periksa');
                        return false;
                    }
                    else if ($('input[name="rmtujuan"]').val() == '')
                    {
                        alert_empty('NO.RM Tujuan');
                        return false;
                    }
                    else
                    {
                        $.ajax({
                          type:"POST",
                          url:base_url+"cadmission/update_riwayatperiksapasienbynorm",
                          data:{idp:$('#tanggalperiksa').val(),norm:$('input[name="rmtujuan"]').val()},
                          dataType:"JSON",
                          success:function(result){
                            notif(result.status, result.message);
                          },
                          error:function(result){
                            fungsiPesanGagal();
                            return false;
                          }

                        });
                    }
                }
            },
            //menu back
            formReset:{
                text: 'Batal',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
});

$(document).on('keyup','#idrmasal',function(){
   var norm = this.value;
   $.ajax({
    type:"POST",
    url:base_url+"cadmission/get_riwayatperiksapasienbynorm",
    data:{norm:norm},
    dataType:"JSON",
    success:function(result){
        edit_dropdown($('#tanggalperiksa'),result,'');
    },
    error:function(result){
      fungsiPesanGagal();
      return false;
    }

  });
});

// unduh file data induk
function get_dataindukexcel()
{
  var modalContent = 
  '<form action="" id="FormUnduhPasien">' +
    '<div class="form-group">' +
        '<label class="col-sm-3">RM.Awal</label>' +
        '<div class="col-sm-8"><input type="text" onkeyup="datainduk_setunduhnorm(this.value)" placeholder="input no rekam medis" name="normawal" class="form-control"/></div>' +
    '</div>' +
    '<div class="col-sm-12"></div>'+
    '<div class="form-group">' +
        '<label class="col-sm-3">RM.Akhir</label>' +
        '<div class="col-sm-8"><input type="text" readonly placeholder="otomatis" name="normakhir"  class="form-control"/></div>' +
    '</div>' +
    
'</form>';
    $.confirm({ //aksi ketika di klik menu
        title: 'Unduh Data Induk Pasien',
        content: modalContent,
        columnClass: 'small',
        buttons: {
            //menu save atau simpan
            formSubmit: {
                text: '<i class="fa fa-file-excel-o"> unduh</i>',
                btnClass: 'btn-success',
                action: function () {
                    if ($('input[name="normawal"]').val() == '')
                    {
                        alert_empty('nomor rekam medis');
                        return false;
                    }
                    else
                    {
                        window.location.href=base_url+"creport/downloadexcel_page/"+ $('input[name="normawal"]').val() +'|'+ $('input[name="normakhir"]').val() +' laporandataindukpasien';
                    }
                }
            },
            //menu back
            formReset:{
                text: '<i class="fa fa-minus-circle"> batal</i>',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}
// set range norm
function datainduk_setunduhnorm(value)
{
  var total = 0;
    total += parseFloat(isnan(value)) + parseFloat(isnan(8000));
    return $('input[name="normakhir"]').val(total);
}
function get_dataindukbytgllahir(value)
{
  $.ajax({
    type:"POST",
    url:base_url+"cadmission/get_dataindukbytgllahir",
    data:{tanggallahir:ambiltanggal(value)},
    dataType:"JSON",
    success:function(result){
      var dthtml='';
      if(result=='')
      {
        dthtml+='<tr class="text-center"><td colspan="8">Tidak ada pasien dengan tanggal Lahir '+value+'</td></tr>';
      }
      for(x in result)
      {
        dthtml+='<tr><td>'+result[x].nik+'</td><td>'+result[x].norm+'</td><td>'+result[x].namalengkap+'</td><td>'+result[x].tanggallahir+'</td><td>'+result[x].jeniskelamin+'</td><td>'+result[x].notelpon+'</td><td>'+result[x].alamat+'</td><td><a id="riwayatdtpasien" norm="'+result[x].norm+'" data-toggle="tooltip" title="" data-original-title="Riwayat Periksa" class="btn btn-info btn-xs" ><i class="fa fa-eye"></i></a> <a onclick="buatQrPasien('+result[x].norm+')" data-toggle="tooltip" data-original-title="Cetak kartu" class="btn btn-primary btn-xs" ><i class="fa fa-credit-card"> '+result[x].cetakkartu+' </i></a> </td></tr>';
      }
      $('#displayBytgllahir').empty();
      $('#displayBytgllahir').html(dthtml);
    },
    error:function(result){
      fungsiPesanGagal();
      return false;
    }

  });
}
function refresh_page()
{
  $('input[type="search"]').val('').keyup();
  table.state.clear();//menghapus stateSave di datatable 
  table.ajax.reload();//reload ajax di datatable
  
}


$(document).on("click","#resetpassword", function(){ //reset password
    var norm    = $(this).attr("norm");
    var tanggal = $(this).attr("tanggal");
    var pasien  = $(this).attr("pasien");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Konfirmasi',
        content: '<p class="text-left">Apakah anda yakin akan mengatur ulang password.? <br>Pasien : '+pasien+' <br> Norm : '+norm+' <br><label class="label label-danger"> Password otomatis akan diganti menjadi tanggal lahir</label><br><label class="label label-danger"> tanpa tanda penghubung.</label><br> contoh : tanggal lahir pasien : 2020-03-02 maka password nya : 20200302 </p>',
        buttons: {
            reset: function () {                
                $.ajax({ 
                    url: base_url+'cadmission/reset_password_pasien',
                    type : "post",      
                    dataType : "json",
                    data : { norm:norm, tanggal:tanggal },
                    success: function(result) {
                    // console.log(result);        
                        notif(result.status, result.message);
                        table.ajax.reload(null,false);
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                }); 
            },
            batal: function () {               
            }            
        }
    });
});