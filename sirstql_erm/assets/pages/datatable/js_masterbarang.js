'use strict';
$(function(){
    startLoading();
    $.ajax({ 
        url:  base_url+'cmasterdata/jsonvstok_barang',
        type : "POST",      
        dataType : "JSON",
//        data:{idbarangfaktur:localStorage.getItem('idfakturbarangdetailbelanja')},
        success: function(result) {
            stopLoading();
            var no=0, listbarang='';
            for (var x in result){
                 listbarang+='<tr><td>'+ ++no +'</td><td>'+result[x].namabarang+'</td><td>'+result[x].kode+'</td><td>'+result[x].stok+'</td><td class="rop-'+setStatusROP(result[x].stok,result[x].stokaman,result[x].stokhabis,result[x].stokminimal)+'"></td><td>'+result[x].jenistarif+'</td><td>'+result[x].namasatuan+'</td><td>'+result[x].namasediaan+'</td><td>'+result[x].jenis+'</td><td>'+result[x].tipeobat+'</td><td>'+result[x].hargabeli+'</td><td>'+result[x].hargajual+'</td><td>'+result[x].kekuatan+'</td><td><a data-toggle="tooltip" title="" data-original-title="Edit Barang" class="btn btn-warning btn-xs" href="'+base_url+'cmasterdata/edit_barang/'+result[x].idbarang+'"><i class="fa fa-pencil"></i></a></td></tr>';
            }
            $('#listbarang').empty();
            $('#listbarang').html(listbarang);
            $('#listdatatable').DataTable({"dom": '<"toolbar">frtip',"stateSave": true, "drawCallback": function (settings, json) {},});
            $('[data-toggle="tooltip"]').tooltip();
            $("div.toolbar").html('<a href="'+base_url+'cmasterdata/add_barang" class="btn btn-default btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a> <a onclick="window.location.reload(true);" class="btn btn-default btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> '+
          '<a href="'+base_url+'cmasterdata/sediaan" class="btn btn-default btn-sm"><i class="fa  fa-file-o"></i> Sediaan</a> '+
          '<a href="'+base_url+'cmasterdata/satuan" class="btn btn-default btn-sm"><i class="fa  fa-file-o"></i> Satuan</a> '+
          '<a href="'+base_url+'cmasterdata/golongan" class="btn btn-default btn-sm"><i class="fa  fa-file-o"></i> Golongan</a> ');
        },
        error: function(result){  
          stopLoading();
            fungsiPesanGagal();
            return false;
        }
      });

});
function setStatusROP(stok,aman,habis,minimal)
{
    var index = (parseFloat(stok) <= parseFloat(habis)) ? 1 :(parseFloat(stok) <= parseFloat(minimal))? 2 : (parseFloat(stok) > parseFloat(aman))?3:0;
    var rop = ['aman','habis','minimal','normal'];
    return rop[index];
}