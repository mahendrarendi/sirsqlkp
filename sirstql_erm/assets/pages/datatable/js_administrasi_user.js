var table;
$(function(){
  table = $('#login_user').DataTable({
      "dom": '<"toolbar">frtip',
      "stateSave": true,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
       
      "ajax": {
          "url": "load_datauser",
          "type": "POST"
      },
      "columnDefs": [
      { 
          "targets": [0], 
          "orderable": false, 
      },
      ],
      "fnCreatedRow": function (nRow, aData, iDataIndex) { // --  This function is called on every 'draw' event, and allows you to dynamically modify any aspect you want about the created DOM.
        $(nRow).attr('id', 'row' + iDataIndex);
      },
      "drawCallback": function (settings, json) { // -- Function that is called every time DataTables performs a draw.
          $('[data-toggle="tooltip"]').tooltip();
      },

  });
  var menu = '<a href="'+base_url+'cadministrasi/add_user" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>'; 
      menu +=' <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>';
  $("div.toolbar").html(menu);
});
