== "System External" mengunduh data ==
Misalnya "System External" hendak melakukan proses Lab dari idpemeriksaan 1995

Sehingga, "System External" melakukan query: http://192.168.100.254:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/1995 atau http://141113126034.ip-dynamic.com:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/1995 dengan metode GET
Didapat hasil (13 pemeriksaan):
[
    {
        "idhasilpemeriksaan": "6989",
        "norm": "10000001",
        "namalengkap": "Tn A",
        "icd": "UF1",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6990",
        "norm": "10000002",
        "namalengkap": "Tn B",
        "icd": "UF2",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6991",
        "norm": "10000003",
        "namalengkap": "Tn C",
        "icd": "UF3",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6992",
        "norm": "10000004",
        "namalengkap": "Tn D",
        "icd": "l10",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6993",
        "norm": "10000005",
        "namalengkap": "Tn E",
        "icd": "l35",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6994",
        "norm": "10000006",
        "namalengkap": "Tn F",
        "icd": "l11",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6995",
        "norm": "10000007",
        "namalengkap": "Tn G",
        "icd": "l12",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6996",
        "norm": "10000008",
        "namalengkap": "Tn H",
        "icd": "l13",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6997",
        "norm": "10000009",
        "namalengkap": "Tn I",
        "icd": "l114",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6998",
        "norm": "10000010",
        "namalengkap": "Tn J",
        "icd": "l15",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "6999",
        "norm": "10000011",
        "namalengkap": "Tn K",
        "icd": "l16",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "7000",
        "norm": "10000012",
        "namalengkap": "Tn L",
        "icd": "l17",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    },
    {
        "idhasilpemeriksaan": "7001",
        "norm": "10000013",
        "namalengkap": "Tn M",
        "icd": "l18",
        "nilai": null,
        "nilaitext": null,
        "remark": ""
    }
]

== "System External" mengunggah data ==
Pemeriksaan 1:
{
	"idhasilpemeriksaan": "6989",
        "norm": "10000014",
        "namalengkap": "Tn N",
	"icd": "UF1",
	"nilai": null,
	"nilaitext": null,
	"remark": ""
}
misalnya: nilai berisi 90
maka "System External" melakukan query: http://192.168.100.254:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/6989/n/90 atau http://141113126034.ip-dynamic.com:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/6989/n/90 dengan metode PUT

Pemeriksaan 2:
{
	"idhasilpemeriksaan": "6990",
        "norm": "10000015",
        "namalengkap": "Tn O",
	"icd": "UF2",
	"nilai": null,
	"nilaitext": null,
	"remark": ""
}
misalnya: nilai berisi 300, nilaitext berisi jernih, remark berisi rm
maka "System External" melakukan query: http://192.168.100.254:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/6990/n/300/txt/jernih/rmk/rm atau http://141113126034.ip-dynamic.com:8080/sirstql/index.php/api/webservice/pemeriksaanlab/id/6990/n/300/txt/jernih/rmk/rm dengan metode PUT

dan seterusnya