// set table 
$(function () 
{ 
    //Initialize Data Tables
    $('#table').DataTable()
})

//DELETE ROW
function deleteRow(rowid)  
{   
    var row = document.getElementById(rowid);
    var table = row.parentNode;
    while ( table && table.tagName != 'TABLE' )
        table = table.parentNode;
    if ( !table )
        return;
    table.deleteRow(row.rowIndex);
}
// DELETE USER
$(document).on("click","#delete_data", function(){
    var table = $(this).attr("alt");
    var id = $(this).attr("alt2");
    var halaman = $(this).attr("alt3");
    var nobaris = $(this).attr("nobaris");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Delete data?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'single_delete',
                    type : "post",      
                    dataType : "json",
                    data : { t:table, i:id, hal:halaman },
                    success: function(result) {                                                                   
                        if(result.status=='success'){
                            notif(result.status, result.message);
                            deleteRow('row'+nobaris);
                        }else{
                            notif(result.status, result.message);
                            return false;
                        }                        
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                    
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
//reset password
$(document).on("click","#reset_password", function(){
    var username = $(this).attr("alt");
    var id = $(this).attr("alt2");
    $.confirm({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset password?',
        buttons: {
            confirm: function () {                
                $.ajax({ 
                    url: 'reset_password',
                    type : "post",      
                    dataType : "json",
                    data : { i:id, u:username },
                    success: function(result) {
                    // console.log(result);        
                        notif(result.status, result.message);
                    },
                    error: function(result){                    
//                        console.log(result.responseText);
                        notif(result.status, result.message);
                        return false;
                    }
                }); 
            },
            cancel: function () {               
            }            
        }
    });
});
