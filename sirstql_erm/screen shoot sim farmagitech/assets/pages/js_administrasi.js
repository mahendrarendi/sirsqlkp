$(function () 
{ 
    // jika halaman add user maka inisialisasi select 2 dll...
    if($('#page_view').val()==='Add Akses User' || $('#page_view').val()==='Edit Akses User' || $('#page_view').val()==='Add Akses Halaman' || $('#page_view').val()==='Edit Akses Halaman' )
    {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass   : 'iradio_flat-green'
        })
    }
    
    
})




function simpan_user()
{
  if($('#username').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid username.!'});
  }
  else if($('#password').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid password.!'});
  }
  else
  {
    $('#FormUser').submit();
  }
}




function simpan_akses_user()
{
  if($('#username').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid username.!'});
  }
  else if($('#password').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid password.!'});
  }
  else
  {
    $('#FormUser').submit();
  }
}

function simpan_peran()
{
  if($('#namaperan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid peran.!'});
  }
  else
  {
    $('#FormPeran').submit();
  }
}

function simpan_halaman()
{
  if($('#namahalaman').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid halaman.!'});
  }
  else
  {
    $('#FormHalaman').submit();
  }
}

function simpan_hakakseshalaman()
{
  if($('#idhalaman').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid Halaman.!'});
  }
  else
  {
    $('#FormHakakseshalaman').submit();
  }
}

function simpan_propinsi()
{
  if($('#namapropinsi').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid propinsi.!'});
  }
  else
  {
    $('#FormPropinsi').submit();
  }
}

function simpan_kabupaten()
{
  if($('#namakabupaten').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kabupaten.!'});
  }
  else
  {
    $('#FormKabupaten').submit();
  }
}

function simpan_kelurahan()
{
  if($('#namakelurahan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid peran.!'});
  }
  else
  {
    $('#FormKelurahan').submit();
  }
}

function simpan_kecamatan()
{
  if($('#namakecamatan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kecamatan.!'});
  }
  else
  {
    $('#FormKecamatan').submit();
  }
}

function simpan_pendidikan()
{
  if($('#namapendidikan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenjangpendidikan.!'});
  }
  else
  {
    $('#Formpendidikan').submit();
  }
}


