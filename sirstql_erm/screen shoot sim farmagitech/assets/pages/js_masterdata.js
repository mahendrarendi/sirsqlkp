$(function () 
{ 
    // jika halaman add user maka inisialisasi select 2 dll...
     //Initialize Select2 Elements
        $('.select2').select2()
        //Flat red color scheme for iCheck
       
    
    
})


function simpan_propinsi()
{
  if($('#namapropinsi').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid propinsi.!'});
  }
  else
  {
    $('#FormPropinsi').submit();
  }
}

function simpan_kabupaten()
{
  if($('#namakabupaten').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kabupaten.!'});
  }
  else
  {
    $('#Formkabupaten').submit();
  }
}

function simpan_kelurahan()
{
  if($('#namakelurahan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid peran.!'});
  }
  else
  {
    $('#Formkelurahan').submit();
  }
}

function simpan_kecamatan()
{
  if($('#namakecamatan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid kecamatan.!'});
  }
  else
  {
    $('#Formkecamatan').submit();
  }
}

function simpan_pendidikan()
{
  if($('#namapendidikan').val()==='')
  {
    $.alert({icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid jenjangpendidikan.!'});
  }
  else
  {
    $('#Formpendidikan').submit();
  }
}

//menampilkan data enum pasien = add penanggungjawab
function tampil_data_enum_pegawai()
{
    $.ajax
          ({
                url:'tampil_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:''},
                success: function(result){
                    //menampilkan data Titel Depan
                    edit_data_enum($('#listtiteldepan'),result.titeldepan,'');
                    //menampilkan data nama pegawai
                    edit_data_enum($('#listnamalegkap'),result.namalengkap,'');
                    //menampilkan data Titel Belakang
                    edit_data_enum($('#listtitelbelakang'),result.titelbelakang,'');
                    //menampilkan data Grup Pegawai
                    edit_data_enum($('#listgruppegawai'),result.gruppegawai,'');
                    //menampilkan data Status Keaktifan
                    edit_data_enum($('#liststatuskeaktifan'),result.statuskeaktifan,'');
                    //menampilkan data nip
                    edit_dropdown_kelurahan($('#listnip'),result.nip,'');                    
                },
                error: function(result){
                    console.log(result);
                }
            });
}
//tambah grup pegawai
function tambah_gruppegawai()
{
    var modalSize = 'small';
    var modalTitle = 'Add Data Pegawai';
    var modalContent = '<form action="" id="Formgruppegawai">' +
                            '<div class="form-group">' +
                                '<label>Data Grup Pegawai</label>' +
                                '<div id="Listgruppegawai" style="display:relative;">' +
                                '<select class="select2 form-control" style="width:100%; display:relative;" id="gruppegawai" name="gruppegawai">'+
                        		'<option value="0">Pilih</option>'+
                                '</select>' +
                                '</div>'+
                            '</div>' +

                            '<div class="form-group">' +
                                '<label>gruppegawai</label>' +
                                '<input type="text" name="gruppegawai" placeholder="gruppegawai" id="gruppegawai" class="form-control"/>' +
                            '</div>' +
                        '</form>';

    $.confirm({
        title: modalTitle,
        content: modalContent,
        columnClass: 'small',
        buttons: {
            formSubmit: {
                text: 'save',
                btnClass: 'btn-blue',
                action: function () {
                	// aksi save data
                	if($('#gruppegawai').val()==='' || $('#gruppegawai').val()==='0')
                	{
                            alert_empty('gruppegawai');
                            return false;
                	}
                	else if( ! $('#gruppegawai').val() )
                	{
                            alert_empty('gruppegawai');
                            return false;
                	}
                	else
                	{
                            $.ajax
                            ({
                                type: "POST",
                                url: 'save_gruppegawai',
                                data: $("#Formgruppegawai").serialize(),
                                dataType: "JSON",
                                success: function(result) {
                                    notif(result.status, result.message);
                                    return refreshpegawai();

                                },
                                error: function(result) {
                                    console.log(result.responseText);
                                }
                            });
                	}
                }
            },
            formReset:{
            	text: 'back',
                btnClass: 'btn-danger'
            }
        },
        onContentReady: function () {
        $('.select2').select2();
        get_data_gruppegawai();
        // bind to events
        var jc = this;
        this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it
        });
        }
    });
}

//fungsi ambil data geografi
function get_data_gruppegawai()
{
    var select='';
    $.ajax
    ({
        url:'cari_gruppegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#Listgruppegawai').empty();
            for(i in result)
            {
            select = select + '<option value="'+ result[i].idgruppegawai +'" >' + result[i].namagruppegawai +'</option>';
            }
            $('#Listgruppegawai').html('<select onchange="get_data_pegawai(this.value)" class="select2 form-control" style="width:100%" id="gruppegawai" name="gruppegawai">'+
                            '<option value="0">Pilih</option>' +
                            select +
                            '</select>');
            
            $('.select2').select2();
        },
        error: function(result){
            console.log(result);
        }

    });	
}



// menampilkan data kelurahan
function refreshgruppegawai()
{
    var select='<option value="0">Pilih</option>';
    $.ajax
    ({
        url:'cari_gruppegawai',
        type:'POST',
        dataType:'JSON',
        success: function(result){
            $('#gruppegawai').empty();
            for(i in result)
            {
                    select = select + '<option value="'+ result[i].idgrupegawai +'" >' + result[i].namagruppegawai +' </option>';
            }
            $('#gruppegawai').html(select);
            $('.select2').select2();

        },
        error: function(result){
            console.log(result);
        }
    });	
}

// entri pendaftaran
function entri_pegawai()
{
    if($('textarea[name="titeldepan"]').val()==='')
    {	
        $('textarea[name="titeldepan"]').focus();
        alert_empty('titeldepan');
    }
    else if($('input[name="namapasien"]').val()==='')
    {	
        $('input[name="namapasien"]').focus();
        alert_empty('nama pasien');
    }
	else if($('textarea[name="titelbelakang"]').val()==='')
    {	
        $('textarea[name="titelbelakang"]').focus();
        alert_empty('titelbelakang');
    }
     else if($('input[name="nip"]').val()==='')
    {	
        $('input[name="nip"]').focus();
        alert_empty('nip');
    }
    else
    {
        $('#Formpegawai').submit();
    }
}

// tampilkan data pegawai
$("#pilihpegawai").on("keydown", function (e) {
    if (e.keyCode == 16) {
        var value = $('#pilihpegawai').val();
    // console.log('tes tampil',value);
        var select;
        var selected;
        var i;
        if(value!=='0')
        {
          $.ajax
          ({
                url:'tampil_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                   
                    $('input[name="titeldepan"]').val(result.pegawai['titeldepan']);
					$('input[name="idperson"]').val(result.pegawai['idperson']);
                    $('input[name="titelbelakang"]').val(result.pegawai['titelbelakang']);
                    $('input[name="idgruppegawai"]').val(result.pegawai['idgruppegawai']);
                    $('input[name="statuskeaktifan"]').val(result.pegawai['statuskeaktifan']);
                    $('input[name="nip"]').val(result.pegawai['nip']);
                    //menampilkan data gruppegawai
                    edit_data_enum($('#namagruppegawai'),result.gruppegawai,result.pegawai['namagruppegawai']);
                    //menampilkan data statuskeaktifan
                    edit_data_enum($('#statuskeaktifan'),result.statuskeaktifan,result.pegawai['statuskeaktifan']);
                    //menampilkan data person
                    edit_data_enum($('#idperson'),result.idperson,result.pegawai['idperson']);
                   
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
    }
});
// cari data pegawai
$("#pilihpegawai").on("keydown", function (e) {
    if (e.keyCode == 13) {
        $('#Listpegawai').empty();
        var value = $('#pilihpegawai').val();
        var datalist='';
        var i=0;
        // console.log('tes cari', value);
        $.ajax
          ({
                url:'cari_pegawai',
                type:'POST',
                dataType:'JSON',
                data:{id:value},
                success: function(result){
                // console.log(result);
                    for(i in result)
                    {

                        datalist = datalist + '<option value="'+ result[i].titelbelakang + result[i].namalengkap+'-'+ result[i].titeldepan+'">';
                    }
                    $('#Listpegawai').html(datalist);
                },
                error: function(result){
                    console.log(result);
                }
            });
    }
});
// fungsi edit data enum (idhtml,dataenum,datayangterpilih)
function edit_data_enum(column,data,selected_data)
{
    var select='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i]==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i] +'" '+ selected +' >' + data[i] +'</option>';
    }
    column.html(select);
}




function edit_dropdown_person(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idperson==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idperson +'" '+ selected +' >' + data[i].namaperson +'</option>';
    }
    column.html(select);
}

function edit_dropdown_gruppegawai(column,data,selected_data)
{
    // console.log(name);
    var select ='<option value="0">Pilih</option>';
    var selected;
    column.empty();
    for(i in data)
    {
        if(data[i].idgruppegawai==selected_data){ selected='selected'; }
        else{selected='';}
        select = select + '<option value="'+ data[i].idgruppegawai +'" '+ selected +' >' + data[i].namagruppegawai +'</option>';
    }
    column.html(select);
}

function reset_pegawai()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Reset Form?',
        buttons: {
            confirm: function () { 
                window.location.reload(true)
            },
            cancel: function () {               
            }            
        }
    });
	
}

//kembali ke halaman pendaftaran poli
function back_pegawai()
{
    $.confirm
    ({
        icon: 'fa fa-question',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Confirmation!',
        content: 'Back to list pendaftaran?',
        buttons: {
            confirm: function () { 
                window.location.href='pegawai';
            },
            cancel: function () {               
            }            
        }
    });
    
}
