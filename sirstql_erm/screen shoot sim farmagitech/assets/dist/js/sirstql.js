$(function () 
{ 
    $('[data-toggle="tooltip"]').tooltip()
    var set_status = $('#set_status').val();
    var set_message = $('#set_message').val();
    $('#generate_barcode').val('');
    // jika status tidak kosong panggil fungsi notif
    if(set_status !=='')
    {
        notif(set_status, set_message);
    }
})

// notifikasi aksi save, edit, delete dll.
function notif(status, message)
{
    // console.log(status);
    $.notify({
        icon: 'fa fa-bell fa-lg',
        message: '<strong>&nbsp;'+message+'</strong>'
    },{
        type: status,
        timer: 1000
    });
    return true;
}

//fungsi notif inputan kosong
function alert_empty(value)
{
    $.alert
    ({
        icon: 'fa  fa-warning',
        theme: 'modern',
        closeIcon: true,
        animation: 'scale',
        type: 'orange',
        title: 'Warning!',
        content: 'Please provide a valid '+value+'.!'
    });
}

///////////////////QRCODE////////////////////////////
var qrcode = new QRCode(document.getElementById("qrcode"),{
    width : 100,
    height : 100
});
function makeCode () 
{      
    var elText = document.getElementById("textqrcode");
    if (!elText.value) 
    {
        alert_empty('code to generate');
    }
    else
    {
        qrcode.makeCode(elText.value);
         setTimeout(function(){
            printQrcode('qrcode');
        },2); 
    }
}

$("#textqrcode").
on("keydown", function (e) {
    if (e.keyCode == 13) {
        makeCode();

    }
});

function printQrcode(divName){

   var printContents = document.getElementById(divName).innerHTML;
    w = window.open();
    w.document.write(printContents);
    w.document.write('<style>body{  margin: auto;}</style><script type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</script>');
//    /w.onbeforeprint='<style>body{  margin: auto;}</style><script type="text/javascript">' + 'window.onload = function() { window.print(); window.close(); };' + '</script>';
    w.document.close();
    w.focus(); 

}
/////////////////////////////////////