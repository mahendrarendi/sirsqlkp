<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['translate_uri_dashes'] = FALSE;
$route['welcome']            = 'sample/userguide';//halaman welcome CI
$route['default_controller'] = 'clogin';//halaman login pelayanan
$route['dashboard']          = 'cdashboard';//halaman dashboard
$route['404_override']       = 'cdashboard/error_page';//halaman error/404
$route['daftarperiksa']      = 'canjunganpasien/register_pasien';//halaman registrasi periksa pasien
$route['lihatantrian']       = 'canjunganpasien';//halaman list antrian
$route['jadwaldokter']       = 'canjunganpasien/jadwaldokter';//halaman registrasi periksa pasien
$route['loginperiksa']       = 'canjunganpasien/loginperiksapasien';//halaman login registrasi pasien
$route['sampleapilap']       = 'assets/dokumen/sampel_pemeriksaanlab.txt';

// Inventaris
$route['barangdatang']       = 'cmasterdata/belanjabarang';
$route['tambahbarangdatang'] = 'cmasterdata/distribusibelanjabarang';
$route['permintaanbarang']   = 'cmasterdata/pesanbarang';
$route['tambahpermintaan']   = 'cmasterdata/distribusipesanbarang';
$route['pengembalianbarang'] = 'cmasterdata/returbarang';
$route['tambahpengembalian'] = 'cmasterdata/distribusireturbarang';

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
$route['api/antrean/token'] = 'api/wsfkrtl/token';
