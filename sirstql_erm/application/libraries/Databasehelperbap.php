<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Databasehelperbap.php
 * <br />Database Helper Class
 * <br />
 * <br />This is a DB helper for consistency and simplicity
 * 
 * @author Basit Adhi Prabowo, S.T. <basit@unisayogya.ac.id>
 * @access public
 * @link https://github.com/basit-adhi/MyCodeIgniterLibs/blob/master/libraries/Databasehelperbap.php
 */
class Databasehelperbap
{
    /**
     *
     * @var array session value that stored in session
     */
    private $session_ofpartitionfield;
    /**
     *
     * @var array database's table
     */
    private $tables;
    /**
     *
     * @var type array of all tables used in current query
     */
    private $current_query_tables;
    /**
     *
     * @var type array of all tables that already used
     */
    private $used_query_tables;
    /**
     *
     * @var CI super-object
     */
    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    function __construct()
    {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        //--
        $this->CI->load->helper('variablebap');
        $this->loadSession();
        $this->tables = new TableStructure();
    }

    // --------------------------------------------------------------------
    
    /**
     * Initial table structure
     * Customize this function, change all variable inside this function to fit your needs
     */
    private function loadSession()
    {
//Example:
//        $this->session_ofpartitionfield = array("tahunanggaran" => ifnull($this->CI->session->userdata("idtahunanggaran"), 0));
//        $this->session_ofpartitionfield = array("isclosing" => ifnull($this->CI->session->userdata("isclosing"), 0), "tahunbulan" => ifnull($this->CI->session->userdata("tahunbulan"), 0));
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Register table(s) definition
     * Example 1:
     * $this->databasehelperbap->registerTable("table1");           - will register a table
     * Example 2:
     * $this->databasehelperbap->registerTable("table1, table2");   - will register more than one table
     * Customize this function (conditional switch), change to fit your tables definition
     * @param type $tablename   table(s) to select
     */
    function registerTable($tablename)
    {
        /* if $tablename consist more than 1 table (use comma), then register it 1 by 1 */
        if (strpos($tablename, ",") !== false)
        {
           foreach (explode(",", $tablename) as $singletablename)
           {
               $this->registerTable(trim($singletablename));
           }
        }
        /* register 1 table */
        elseif (!array_key_exists($tablename, $this->tables->name))
        {
            //Customize this conditional switch, change to fit your tables definition
            //     * Add an table structure
            //     * @param string $tablename     table's name
            //     * @param string $tablealias    alias of the table
            //     * @param string $key           primary key of the table (without alias)
            //     * @param array $onjoin         other table that join in the current table (without alias), 
            //                                    - format 1: array("other table name" => "field in the current table that join to other table", ...), 
            //                                    - format 2: array("other table name" => array("field in the current table that join to other table", "field in the other table that join to current table if that field is not a primary key"), ...)
            //     * @param array $partitionkey   partition key (without alias), 
            //                                    - format: array("partition key in the table" => "session index", ...)
            //     addTableStructure($tablename, $tablealias, $key, $onjoin = array(), $partitionkey = array())
//Example:
//            switch ($tablename)
//            {
//                //sample no join to other table, no partition
//                case "ueu_tbl_tahunanggaran"    : $this->tables->addTableStructure($tablename, "ta", "idtahunanggaran", array(), array()); break;
//                //sample no join to other table, partition
//                case "ueu_tbl_unit"             : $this->tables->addTableStructure($tablename, "tu", "id_unit", array(), array("tahunanggaran" => "tahunanggaran")); break;
//                //sample join to other table on other table's primary key, partition
//                case "ueu_tbl_user"             : $this->tables->addTableStructure($tablename, "tus", "idlog", array("ueu_tbl_unit" => "id_unit"), array("tahunanggaran" => "tahunanggaran")); break;
//                //sample join to other table on selected field in other table, no partition
//                case "tbl_misi_unit"            : $this->tables->addTableStructure($tablename, "mu", "id", array("ueu_tbl_unit" => array("kodeunit", "kodeunit")), array()); break;
//                //not found
//                default: break;
//            }
            switch ($tablename)
            {
                case "antrian_antrian"                  : $this->tables->addTableStructure($tablename, "aa", "idantrian", array("antrian_loket" => "idloket", "person_person" => "idperson"), array()); break;
                case "antrian_loket"                    : $this->tables->addTableStructure($tablename, "al", "idloket", array("antrian_stasiun" => "idstasiun", "rs_icd" => "icdtarifpemeriksaan"), array()); break;
                case "antrian_stasiun"                  : $this->tables->addTableStructure($tablename, "as", "idstasiun", array(), array()); break;
                case "bpjs_bpjs"                        : $this->tables->addTableStructure($tablename, "pbjs", "idbpjs", array("person_pasien" => "norm", "bpjs_statuspeserta" => "idstatuspeserta","bpjs_jenispeserta" => "idjenispeserta","bpjs_hakkelas"=>"idkelas"), array()); break;
                case "bpjs_hakkelas"                    : $this->tables->addTableStructure($tablename, "pbjh", "idkelas", array(), array()); break;
                case "bpjs_klinikperujuk"               : $this->tables->addTableStructure($tablename, "pbjk", "idklinik", array(), array()); break;
                case "bpjs_jenispeserta"                : $this->tables->addTableStructure($tablename, "pbjp", "idjenispeserta", array(), array()); break;
                case "bpjs_statuspeserta"               : $this->tables->addTableStructure($tablename, "psp", "idstatuspeserta", array(), array()); break;
                case "bpjs_bridging_vclaim"             : $this->tables->addTableStructure($tablename, "bpvc", "idpendaftaran", array(), array()); break;
                case "demografi_pekerjaan"              : $this->tables->addTableStructure($tablename, "pkj", "idpekerjaan", array(), array()); break;
                case "demografi_pendidikan"             : $this->tables->addTableStructure($tablename, "pdd", "idpendidikan", array(), array()); break;
                case "geografi_desakelurahan"           : $this->tables->addTableStructure($tablename, "dk", "iddesakelurahan", array("geografi_kecamatan" => "idkecamatan"), array()); break;
                case "geografi_kabupatenkota"           : $this->tables->addTableStructure($tablename, "kk", "idkabupatenkota", array("geografi_propinsi" => "idpropinsi"), array()); break;
                case "geografi_kecamatan"               : $this->tables->addTableStructure($tablename, "kc", "idkecamatan", array("geografi_kabupatenkota" => "idkabupatenkota"), array()); break;
                case "geografi_propinsi"                : $this->tables->addTableStructure($tablename, "pr", "idpropinsi", array(), array()); break;
                case "helper_autonumber"                : $this->tables->addTableStructure($tablename, "ha", "", array(), array()); break;
                case "inventaris_barang"                : $this->tables->addTableStructure($tablename, "ib", "idbaranginventaris", array("inventaris_barangkategori" => "idbarangkategori"), array()); break;
                case "inventaris_barangdetail"          : $this->tables->addTableStructure($tablename, "ibd","idbarangdetail", array("inventaris_barang" => "idbaranginventaris","inventaris_model"=>"idmodelbarang","rs_satuan"=>"idsatuan"), array()); break;
                case "inventaris_inventaris"            : $this->tables->addTableStructure($tablename, "ii", "idinventaris", array("inventaris_barangdetail" => "idbarangdetail","inventaris_ruang"=>"idruanginventaris","inventaris_ruang"=>"idruanginventarissebelum"), array()); break;
                case "inventaris_model"                 : $this->tables->addTableStructure($tablename, "im", "idmodelbarang", array(), array()); break;
                case "inventaris_ruang"                 : $this->tables->addTableStructure($tablename, "ir", "idruanginventaris", array("inventaris_blok"=>"idblokinventaris"), array()); break;
                case "inventaris_blok"                  : $this->tables->addTableStructure($tablename, "ib", "idblokinventaris", array(), array()); break;
                case "keu_tagihan"                      : $this->tables->addTableStructure($tablename, "kt", "idtagihan", array("rs_pendaftaran" => "idpendaftaran"), array()); break;
                case "konfigurasi"                      : $this->tables->addTableStructure($tablename, "konfig", "nama", array(), array()); break;
                case "login_user"                       : $this->tables->addTableStructure($tablename, "lu", "iduser", array("person_person"=>"idperson"), array()); break;
                case "login_halaman"                    : $this->tables->addTableStructure($tablename, "lh", "idhalaman", array(), array()); break;
                case "login_peran"                      : $this->tables->addTableStructure($tablename, "lp", "idperan", array(), array()); break;
                case "login_hakaksesuser"               : $this->tables->addTableStructure($tablename, "lhau", "idhakaksesuser", array("login_user" => "iduser"), array()); break;
                case "login_hakakseshalaman"            : $this->tables->addTableStructure($tablename, "lhah", "idhakakseshalaman", array("login_halaman" => "idhalaman"), array()); break;
                case "login_log"                        : $this->tables->addTableStructure($tablename, "ll", "idlog", array(), array()); break;
                case "person_grup_pegawai"              : $this->tables->addTableStructure($tablename, "pgp", "idgruppegawai", array(), array()); break;
                case "person_hubungan"                  : $this->tables->addTableStructure($tablename, "ph", "idhubungan", array(), array()); break;
                case "person_pasien"                    : $this->tables->addTableStructure($tablename, "ppas", "norm", array("person_person" => "idperson"), array()); break;
                case "person_pasien_penanggungjawab"    : $this->tables->addTableStructure($tablename, "pppj", "idpasienpenanggungjawab", array("person_pasien" => "norm", "person_person" => "idperson", "person_hubungan" => "idhubungan"), array()); break;
                case "person_pegawai"                   : $this->tables->addTableStructure($tablename, "ppg", "idpegawai", array("person_grup_pegawai" => "idgruppegawai", "person_person" => "idperson"), array()); break;
                case "person_pendaftaran"               : $this->tables->addTableStructure($tablename, "ppd", "idpendaftaran", array("person_pasien" => "norm", "person_pasien_penanggungjawab" => "idpasienpenanggungjawab", "rs_unit" => "idunit", "rs_statuskeluar" => "idstatuskeluar", "rs_kelas" => "idkelas", "rs_skdp" => "noskdpql"), array()); break;
                case "person_person"                    : $this->tables->addTableStructure($tablename, "pper", "idperson", array("geografi_desakelurahan" => "iddesakelurahan", "demografi_pekerjaan" => "idpekerjaan", "demografi_pendidikan" => "idpendidikan"), array()); break;
                case "rs_bangsal"                       : $this->tables->addTableStructure($tablename, "rbl", "idbangsal", array("person_pegawai" => "idpegawaika", "rs_kelas" => "idkelas"), array()); break;
                case "rs_barang"                        : $this->tables->addTableStructure($tablename, "rbr", "idbarang", array("rs_satuan" => "idsatuan", "rs_sediaan" => "idsediaan", "rs_jenistarif" => "idjenistarif", "rs_barang_golongan" => "idgolongan"), array()); break;
                case "rs_barang_aturanpakai"            : $this->tables->addTableStructure($tablename, "rbrap", "idbarangaturanpakai", array(), array()); break;
                case "rs_barangpemeriksaan"             : $this->tables->addTableStructure($tablename, "rbp", "idbarangpemeriksaan", array("rs_pendaftaran" => "idpendaftaran", "rs_barang" => "idbarang", "rs_jenistarif" => "idjenistarif", "rs_pemeriksaan" => "idpemeriksaan"), array()); break;    
                case "rs_barang_golongan"               : $this->tables->addTableStructure($tablename, "rbg", "idgolongan", array(), array()); break;
                case "rs_barang_distributor"            : $this->tables->addTableStructure($tablename, "rbard", "idbarangdistributor", array(), array()); break;
                case "rs_barangpabrik"                  : $this->tables->addTableStructure($tablename, "rbapbr", "idbarangpabrik", array(), array()); break;
                case "rs_barang_distribusi"             : $this->tables->addTableStructure($tablename, "rbad", "idbarangdistribusi", array(), array()); break;
                case "rs_barang_distributor"            : $this->tables->addTableStructure($tablename, "rbd", "idbarangdistributor", array(), array()); break;      
                case "rs_barang_pbf"                    : $this->tables->addTableStructure($tablename, "rpbf", "idbarangpbf", array(), array()); break;
                case "rs_barang_pembelian"              : $this->tables->addTableStructure($tablename, "rbap", "idbarangpembelian", array(), array()); break;
                case "rs_barang_pembelianbebas"         : $this->tables->addTableStructure($tablename, "rbpb", "idbarangpembelianbebas", array(), array()); break;
                case "rs_barang_pembelibebas"           : $this->tables->addTableStructure($tablename, "rbpb", "idbarangpembelibebas", array(), array()); break;
                case "rs_bed"                           : $this->tables->addTableStructure($tablename, "rbd", "idbed", array("rs_bangsal" => "idbangsal", "rs_bed_status" => "idstatusbed"), array()); break;
                case "rs_bed_status"                    : $this->tables->addTableStructure($tablename, "rbs", "idstatusbed", array(), array()); break;
                case "rs_fakturnonfarmasi"              : $this->tables->addTableStructure($tablename, "rf", "idfaktur", array(), array()); break;
                case "rs_hasilpemeriksaan"              : $this->tables->addTableStructure($tablename, "rhp", "idhasilpemeriksaan", array("person_pendaftaran" => "idpendaftaran", "rs_icd" => "icd", "rs_jenistarif" => "idjenistarif", "rs_pemeriksaan" => "idpemeriksaan"), array()); break;    
                case "rs_icd"                           : $this->tables->addTableStructure($tablename, "ricd", "icd", array("rs_jenisicd" => "idjenisicd"), array()); break;
                case "rs_icd_golongansebabpenyakit"     : $this->tables->addTableStructure($tablename, "ricdgsp", "idgolongansebabpenyakit", array(), array()); break;
                case "rs_inap"                          : $this->tables->addTableStructure($tablename, "rin", "idinap", array("person_pendaftaran" => "idpendaftaran", "person_pegawai" => "idpegawaidokter", "rs_bed" => "idbed"), array()); break;
                case "rs_inap_rencana_medis_hasilpemeriksaan"   : $this->tables->addTableStructure($tablename, "rirmh", "idrencanamedishasilpemeriksaan", array("rs_inap_rencana_medis_pemeriksaan" => "idrencanamedispemeriksaan", "rs_icd" => "icd", "rs_jenistarif" => "idjenistarif", "rs_inap_rencana_medis_pemeriksaan" => "idrencanamedispemeriksaan", "person_pegawai" => "idpegawaidokter"), array()); break;
                case "rs_inap_rencana_medis_barangpemeriksaan"             : $this->tables->addTableStructure($tablename, "rirmbp", "idrencanamedisbarangpemeriksaan", array("rs_inap_rencana_medis_barang" => "idrencanamedisbarang", "rs_inap_rencana_medis_pemeriksaan" => "idrencanamedispemeriksaan"), array()); break;    
                case "rs_inap_rencana_medis_barang"             : $this->tables->addTableStructure($tablename, "rirmb", "idrencanamedisbarang", array("rs_inap" => "idinap", "rs_barang" => "idbarang", "rs_jenistarif" => "idjenistarif", "rs_pemeriksaan" => "idpemeriksaan"), array()); break;    
                case "rs_inap_rencana_medis_pemeriksaan"        : $this->tables->addTableStructure($tablename, "rirmp", "idrencanamedispemeriksaan", array("rs_inap" => "idinap"), array()); break;    
                case "rs_instalasi"                     : $this->tables->addTableStructure($tablename, "ri", "idinstalasi", array("person_pegawai" => "idpegawaika"), array()); break;
                case "rs_jadwal"                        : $this->tables->addTableStructure($tablename, "rj", "idjadwal", array("rs_unit" => "idunit", "person_pegawai" => "idpegawaidokter", "antrian_loket" => "idloket"), array()); break;
                case "rs_jenisicd"                      : $this->tables->addTableStructure($tablename, "rjic", "idjenisicd", array(), array()); break;      
                case "rs_jenispenyakit"                      : $this->tables->addTableStructure($tablename, "rjp", "idjenispenyakit", array(), array()); break;
                case "rs_jenistarif"                    : $this->tables->addTableStructure($tablename, "rjt", "idjenistarif", array(), array()); break;
                case "rs_kelas"                         : $this->tables->addTableStructure($tablename, "rkl", "idkelas", array(), array()); break;
                case "rs_masterjadwal"                  : $this->tables->addTableStructure($tablename, "rmj", "idmasterjadwal", array("rs_unit" => "idunit", "person_pegawai" => "idpegawaidokter", "antrian_loket" => "idloket"), array()); break;
                case "rs_mastertarif"                   : $this->tables->addTableStructure($tablename, "rmtr", "idmastertarif", array("rs_kelas" => "idkelas", "rs_icd" => "icd", "rs_jenistarif" => "idjenistarif"), array()); break;
                case "rs_masterdiskon"                  : $this->tables->addTableStructure($tablename, "rmds", "idmasterdiskon", array("rs_kelas" => "idkelas", "rs_icd" => "icd", "rs_jenistarif" => "idjenistarif"), array()); break;
                case "rs_masterdiskonpegawai"           : $this->tables->addTableStructure($tablename, "rmdsp", "iddiskonpegawai", array("rs_kelas" => "idkelas", "rs_jenistarif" => "idjenistarif"), array()); break;
                case "rs_mastermember"                  : $this->tables->addTableStructure($tablename, "rmm", "idmember", array(), array()); break;
                case "rs_mastertarif_paket_pemeriksaan" : $this->tables->addTableStructure($tablename, "rmtrpm", "idmastertarifpaketpemeriksaan", array("rs_kelas" => "idkelas", "rs_paket_pemeriksaan" => "idpaketpemeriksaan", "rs_jenistarif" => "idjenistarif"), array()); break;
                case "rs_operasi"                       : $this->tables->addTableStructure($tablename, "ro", "idoperasi", array(), array()); break;
                case "rs_paket_pemeriksaan"             : $this->tables->addTableStructure($tablename, "rpp", "idpaketpemeriksaan", array("rs_icd" => "icdpaket", "rs_jenisicd" => "idjenisicd"), array()); break;
                case "rs_paket_pemeriksaan_detail"      : $this->tables->addTableStructure($tablename, "rppd", "idpaketpemeriksaandetail", array("rs_paket_pemeriksaan" => "idpaketpemeriksaan", "rs_icd" => "icd"), array()); break;
                case "rs_paket_pemeriksaan_rule"        : $this->tables->addTableStructure($tablename, "rppr", "idpaketpemeriksaanrule", array(), array()); break;
                case "rs_paket_pemeriksaan_rule_pesan"  : $this->tables->addTableStructure($tablename, "rpprp", "idpaketpemeriksaanrulepesan", array(), array()); break;
                case "rs_pemeriksaan"                   : $this->tables->addTableStructure($tablename, "rprk", "idpemeriksaan", array("person_pasien" => "norm", "rs_jadwal" => "idjadwal", "person_pendaftaran" => "idpendaftaran"), array()); break;
                case "rs_pemeriksaan_wabah"             : $this->tables->addTableStructure($tablename, "rprkw", "idpemeriksaanwabah", array("rs_pemeriksaan" => "idpemeriksaan"), array()); break;
                case "rs_satuan"                        : $this->tables->addTableStructure($tablename, "rst", "idsatuan", array(), array()); break;
                case "rs_sediaan"                       : $this->tables->addTableStructure($tablename, "rsd", "idsediaan", array(), array()); break;
                case "rs_skdp"                        : $this->tables->addTableStructure($tablename, "rsk", "noskdpql", array("rs_jadwal" => "idjadwal"), array()); break;
                case "rs_unit"                          : $this->tables->addTableStructure($tablename, "ru", "idunit", array("person_pegawai" => "idpegawaika", "rs_instalasi" => "idinstalasi", "rs_paket_pemeriksaan" => "idpaketpemeriksaan"), array()); break;
                
            default: break;
            }
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Generate Partition Filter
     * @param type $table   table to select
     */
    private function generatePartitionFilter($table)
    {
        /* generate partition filter */
        if (!empty($this->tables->partitionkey[$table])) 
        {
            foreach ($this->tables->partitionkey[$table] as $fieldPartitionInTable => $indexPartitionInSession)
            {
                $this->CI->db->where($this->tables->tablealias[$table].".".$fieldPartitionInTable, $this->session_ofpartitionfield[$indexPartitionInSession]);
            }
        }
    }
    
    /**
     * Generate Join
     * @param type $table   table to select
     */
    private function generateJoin($table, $alias)
    {
//        $this->used_query_tables[] = $table;
        if (!empty($this->tables->onjoin[$table])) 
        {
            foreach ($this->tables->onjoin[$table] as $tablejoin => $field)
            {
                //only join registered tables, not all
                $this->join($table, $tablejoin, $field, $alias);
            }
        }
    }
    
    private function join($table, $tablejoin, $field, $alias)
    {
        if (in_array($tablejoin, $this->current_query_tables)) 
        {
            if (!array_search($table, $this->used_query_tables))
            {
                $this->CI->db->from($tablejoin." ".$this->tables->tablealias[$table].((is_array($this->tables->tablealias[$tablejoin]))?$this->tables->tablealias[$tablejoin][0]:$this->tables->tablealias[$tablejoin]));
                $tablejoinalias = $alias.$this->tables->tablealias[$table].((is_array($this->tables->tablealias[$tablejoin]))?$this->tables->tablealias[$tablejoin][0].".".$this->tables->tablealias[$tablejoin][1]:$this->tables->tablealias[$tablejoin].".".$this->tables->key[$tablejoin]);
                $this->CI->db->where($this->tables->tablealias[$table].".".$field."=".$tablejoinalias);
//                $this->generateJoin($this->tables->tablealias[$table], $tablejoinalias);
            }
            else
            {
                $tablejoinalias = $alias.((is_array($this->tables->tablealias[$tablejoin]))?$this->tables->tablealias[$tablejoin][0].".".$this->tables->tablealias[$tablejoin][1]:$this->tables->tablealias[$tablejoin].".".$this->tables->key[$tablejoin]);
                $this->CI->db->where($this->tables->tablealias[$table].".".$field."=".$tablejoinalias);
            }
            $this->used_query_tables[$table]        = $table;
            $this->used_query_tables[$tablejoin]    = $tablejoin;
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Adds a SELECT clause to a query, automatically add join, automatically add partition filter
     * @param type $select      The SELECT portion of a query
     * @param type $fromtable   table(s) to select, example1 : "table1", example2: "table1, table2" 
     */
    private function selectfrom($select, $fromtable)
    {
        $this->loadSession();
        $this->used_query_tables    = array();
        /* convert select to array */
        $this->current_query_tables = explode_ns(",", $fromtable);
        /* generate select and from */
        $this->CI->db->select($select);
        $this->CI->db->from(implode_2a(",", $this->current_query_tables, select_array_from_values($this->tables->tablealias, $this->current_query_tables)));
        if (is_array($this->current_query_tables))
        {
            $this->used_query_tables[$this->current_query_tables[0]]    = $this->current_query_tables[0];
        }
        /* try generate join and partition filter */
        foreach ($this->current_query_tables as $table)
        {
            $this->generateJoin($table, "");
            $this->generatePartitionFilter($table);
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Adds a SELECT clause to a query, automatically add join, automatically add partition filter, then execute $this->CI->db->get(). You can combine with CI Query Builder before use this function.
     * Same as if you use CI:
     * $this->db->select($select);
     * $this->db->from($fromtable);
     * $this->db->where($join1, $join2);        - implicit, see registerTable()
     * $this->db->where($partitionkey, $value); - implicit, see registerTable()
     * $this->db->get();
     * Example 1:
     * $this->databasehelperbap->get_selectfrom("*", "ueu_tbl_unit");
     * Example 2:
     * $this->db->order_by($order);
     * $this->db->where($customwhere);
     * $this->databasehelperbap->get_selectfrom("*", "ueu_tbl_unit");
     * @param type $select      The SELECT portion of a query
     * @param type $fromtable   table(s) to select, example1 : "table1", example2: "table1, table2" 
     * @param type $where       Filter / where portion of a query (array)
     * @param type $limit       The LIMIT clause
     * @param type $offset      The OFFSET clause
     * @return type CI_DB_result instance (same as $this->CI->db->get())
     */
    function get_selectfrom($select, $fromtable, $where = array(), $limit = null, $offset = null)
    {
        if ($limit != NULL)
        {
            $this->CI->db->limit($limit, ifnull($offset, 0));
        }
        $this->selectfrom($select, $fromtable);
        if (is_array($where) && count($where) > 0)
        {
            $this->CI->db->where($where);
        }
        return $this->get();
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Compiles and executes an INSERT statement
     * @param type $table   table to insert
     * @param type $data    An associative array of field/value pairs
     * @return type result of execution
     */
    public function insert($table, $data)
    {
        $this->CI->db->insert($table, $data);
    }
    
    /**
     * Get last insert id
     * @return type   last insert id
     */
    public function insert_id()
    {
        return $this->CI->db->insert_id();
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Compiles and executes batch INSERT statement
     * @param type $table   table to insert
     * @param type $data    Multidimensional associative array of field/value pairs
     * @return type result of execution
     */
    public function insert_batch($table, $data)
    {
        $this->CI->db->insert_batch($table, $data);
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Executes an DELETE statement if there is an existing KEY, then compiles and executes an INSERT statement
     * @param type $table   table to insert
     * @param type $data    An associative array of field/value pairs
     * @return type result of execution
     */
    public function insert_replaceonduplicate($table, $data)
    {
        return $this->CI->db->replace($table, $data);
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Executes an DELETE statement if there is an existing KEY, then compiles and executes an INSERT statement
     * @param type $table   table to insert
     * @param type $data    An associative array of field/value pairs
     * @return type result of execution
     */
    public function insert_ignoreduplicate($table, $data, $onduplicate='')
    {
        if (array_key_exists($table, $this->tables->key) && count($data) > 0)
        {
            return $this->CI->db->query("INSERT INTO $table (".implode(",", array_keys($data)).") VALUES ('".implode("','", $data)."') ON DUPLICATE KEY UPDATE ".((empty($onduplicate))?$this->tables->key[$table]."=".$this->tables->key[$table]:$onduplicate));
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Compiles and executes an UPDATE statement
     * @param type $table   table to update
     * @param type $data    An associative array of field/value pairs
     * @param type $id      Key value to UPDATE
     * @return type result of execution
     */
    function update($table, $data, $id)
    {
        if (array_key_exists($table, $this->tables->key))
        {
            $this->generatePartitionFilter($table);
            $this->CI->db->where($this->tables->key[$table], $id);
            return $this->CI->db->update($table." ".$this->tables->tablealias[$table], $data);
        }
    }
    
    /**
     * Compiles and executes an UPDATE statement
     * @param type $table   table to update
     * @param type $data    An associative array of field/value pairs
     * @param type $where   where clause
     * @return type result of execution
     */
    function update_where($table, $data, $where)
    {
        
        $this->generatePartitionFilter($table);
        return $this->CI->db->update($table." ".$this->tables->tablealias[$table], $data, $where);
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Compiles and executes batch UPDATE statement
     * @param type $table   table to update
     * @param type $data    Multidimensional associative array of field/value pairs
     * @param type $id      Key value to UPDATE
     * @return type result of execution
     */
    function update_batch($table, $data, $id)
    {
        if (array_key_exists($table, $this->tables->key))
        {
            $this->generatePartitionFilter($table);
            $this->CI->db->where($this->tables->key[$table], $id);
            return $this->CI->db->update_batch($table." ".$this->tables->tablealias[$table], $data);
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Adds a DELETE clause to a query
     * @param type $table   table to delete
     * @param type $id      Key value to DELETE
     * @return type result of execution
     */
    function delete($table, $id)
    {
        if (array_key_exists($table, $this->tables->key))
        {
            $this->generatePartitionFilter($table);
            $this->CI->db->where($this->tables->key[$table], $id);
            return $this->CI->db->delete($table);
        }
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Show last query has been executed
     */
    function debug()
    {
        echo $this->CI->db->last_query();;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * return CI DB
     * @return type CI DB
     */
    function db()
    {
        return $this->CI->db;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Generates a platform-specific query string that counts all records returned by an Query Builder query
     * @return type CI_DB_result instance (same as $this->CI->db->get())
     */
    function get()
    {
        return $this->CI->db->get();
    }
}

class TableStructure
{
    /**
     *
     * @var array   table's name
     */
    var $name               = array();
    /**
     *
     * @var array   primary key of the table
     */
    var $key                = array();
    /**
     *
     * @var array   alias of the table
     */
    var $tablealias         = array();
    /**
     *
     * @var array   partition key 
     */
    var $partitionkey       = array();
    /**
     *
     * @var array   other table that join in the current table
     */
    var $onjoin             = array();
    
    /**
     * Add an table structure
     * @param string $tablename     table's name
     * @param string $tablealias    alias of the table
     * @param string $key           primary key of the table (without alias)
     * @param array $onjoin         other table that join in the current table (without alias), format 1: array("other table name" => "field in the current table that join to other table", ...), format 2: array("other table name" => array("field in the current table that join to other table", "field in the other table that join to current table if that field is not a primary key"), ...)
     * @param array $partitionkey   partition key (without alias), format: array("partition key in the table" => "session index", ...)
     */
    function addTableStructure($tablename, $tablealias, $key, $onjoin = array(), $partitionkey = array())
    {
        if (!in_array_r($tablename, $this->name))
        {
            if (!in_array_r($tablealias, $this->tablealias))
            {
                $this->tablealias[$tablename]   = $tablealias;
            }
            else
            {
                echo "ERROR: Duplicate Table Alias!!";
            }
            $this->name[]                       = $tablename;
            $this->key[$tablename]              = $key;
            $this->onjoin[$tablename]           = $onjoin;
            $this->partitionkey[$tablename]     = $partitionkey;
        }
    }
        
}

//class linkedarray
//{
//    public $node;
//
//    public function __construct($item)
//    {
//        $node = null;
//    }
//    
//    private function isEmpty()
//    {
//        return $node === null;
//    }
//    
//    public function insertNode($rootid, $branchid, $item)
//    {
//        if ($this->isEmpty())
//        {
//            $node[$rootid][$branchid]   = $item;
//        }
//        elseif
//    }
//}
/**
EXAMPLE
 * ------------------------------
Model application/models/Mexample.php
 * ------------------------------
<?php
class Mexample extends CI_Model {

    var $tabledef;

    function __construct()
    {
	parent::__construct();
        $this->load->library('Databasehelperbap');
        $this->databasehelperbap->registerTable("ueu_tbl_unit,ueu_tbl_user");
    }

    //generate: select * from ueu_tbl_unit tu, ueu_tbl_user tus where tus.id_unit=tu.id_unit and tu.tahunanggaran=$this->CI->session->userdata("idtahunanggaran") and tus.tahunanggaran=$this->CI->session->userdata("idtahunanggaran")
    public function getDataSample1($name)
    {
        return $this->databasehelperbap->get_selectfrom("*", "ueu_tbl_unit,ueu_tbl_user");
    }

    //generate: select * from ueu_tbl_unit tu, ueu_tbl_user tus where tus.id_unit=tu.id_unit and tu.tahunanggaran=$this->CI->session->userdata("idtahunanggaran") and tus.tahunanggaran=$this->CI->session->userdata("idtahunanggaran") and filter1=$filter1
    public function getDataSample2($name, $filter1)
    {
        $this->databasehelperbap->db()->where("fiter1", $filter1);
        return $this->databasehelperbap->get_selectfrom("*", "ueu_tbl_unit,ueu_tbl_user");
    }
}
 */