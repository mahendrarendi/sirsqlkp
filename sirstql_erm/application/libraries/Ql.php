<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Ql {
    
    protected $CI;
    
    public function __construct() {
        $this->CI = & get_instance();
    }
//--------- PENDAFTARAN && PEMERIKSAAN PASIEN
    /**
     * Simpan atau ubah  person pendaftaran, $data dalam bentuk array
     * @param $idpendaftaran Id Pendaftaran
     * @param $data Data yang disimpan (array)
     */
    public function person_pendaftaran_insert_or_update($idpendaftaran,$data)
    {
        
        $this->CI->mgenerikbap->setTable('person_pendaftaran');
        $simpan = ((empty($idpendaftaran))? $this->CI->mgenerikbap->update_or_insert_ignoreduplicate($data,$idpendaftaran) : $this->CI->db->update('person_pendaftaran',$data,['idpendaftaran'=>$idpendaftaran]));
        return $simpan;
    }
//--simpan atau ubah rs_pemeriksaan, $data dalam bentuk array
    public function rs_pemeriksaan_insert_or_update($idpemeriksaan,$data)
    {
        $this->CI->mgenerikbap->setTable('rs_pemeriksaan');
        $simpan = ((empty($idpemeriksaan))? $this->CI->mgenerikbap->update_or_insert_ignoreduplicate($data,$idpemeriksaan) : $this->CI->db->update('rs_pemeriksaan',$data,['idpemeriksaan'=>$idpemeriksaan]));
        return $simpan;
    }
//--insert atau update rs_pemeriksaan_wabah, $data= array
    public  function rs_pemeriksaanwabah_insertupdate($idp,$data)
    {
        $q=$this->CI->db->get_where('rs_pemeriksaan_wabah',['idpemeriksaan'=>$idp]);
        $id = (($q->num_rows()>0)? $q->row_array()['idpemeriksaanwabah']:'');
        $this->CI->mgenerikbap->setTable('rs_pemeriksaan_wabah');
//        $simpan = ((empty($id))? $this->CI->mgenerikbap->update_or_insert_ignoreduplicate($data,$id) : $this->CI->db->update('rs_pemeriksaan_wabah',$data,['idpemeriksaanwabah'=>$id]));
        $simpan=$this->CI->mgenerikbap->update_or_insert_ignoreduplicate($data,$id);
        return $simpan;
    }
//-- menyimpan data pendaftaran pertama kali
//-- update pendaftaran itu akan mengisi status pasien apakah pasien baru atau lama
//-- status digunakan untuk tagihan pendaftaran otomatis
//-- sehingga pemberian nopesan harus di atas, sebelum tambah pemeriksaan
    /**
     * pendaftaran save pemeriksaan
     * @param type $idpendaftaran
     * @param type $idjadwal
     * @param type $nopesan
     * @param type $norm
     * @param type $callid
     * @param type $tanggalperiksa
     */
    public function pendaftaranpoli_savedaftarpemeriksaan($idpendaftaran,$idjadwal,$nopesan,$norm,$callid,$tanggalperiksa)
    {   
        $this->person_pendaftaran_insert_or_update($idpendaftaran,['nopesan'=>$nopesan]);
        $data = [
            'idpendaftaran'=>$idpendaftaran,
            'idjadwal'  =>$idjadwal,
            'nopesan'   =>$nopesan,
            'tahunbulan'=> date('Yn'),
            'waktu'     =>(empty(format_date_to_sql($tanggalperiksa))) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(format_date_to_sql($tanggalperiksa))),
            'norm'      =>$norm,
            'status'    =>'pesan',
            'callid'    =>$callid,
            'noantrian' =>'0'
        ];
        $simpan = $this->rs_pemeriksaan_insert_or_update('',$data);
        $idpemeriksaan = $this->CI->db->insert_id();

        $simpan = $this->addpaketvitalsign($idpemeriksaan,$idjadwal,$idpendaftaran);
        $this->CI->load->model('mtriggermanual');
        $this->CI->mtriggermanual->airs_pemeriksaan($idjadwal);
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:'.$this->CI->session->userdata('username').';status:'.$data['status'].';idpendaftaran:'.$idpendaftaran.';norm:'.$norm.';idjadwal:'.$idjadwal,
            'expired' =>date('Y-m-d', strtotime("+2 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->CI->mgenerikbap->setTable('login_log');
        $this->CI->mgenerikbap->update_or_insert_ignoreduplicate($datal);
    }
    
//------ ANTRIAN PASIEN
    /**
     * ANTRIAN PASIEN : Buat Nomor Pesan
     * @param type $date Tanggal
     * @param type $callid Callid
     * @param type $idunit Id Unit
     */
    public function antrianset_nopesan($date,$callid,$idunit)
    {
    	$this->CI->mgenerikbap->setTable('rs_unit');
        $akronimunit = $this->CI->mgenerikbap->select("idloket",['idunit'=>$idunit])->row_array();
    	$expired = date('Y-m-d', strtotime($date));
    	$id 	 = date('Ymd',strtotime($date)).$akronimunit['idloket'];
        $this->antrian_autonumber($id,$callid,$expired);
    }
    
    /**
     * Buat Nomorantrian Periksa
     * @param type $callid  Callid
     * @param type $idunit  Id Unit
     * @param type $idpemeriksaan Id Pemeriksaan
     * @param type $idperson Id Person
     */
    public function antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson)
    {
    	$akronimunit = $this->CI->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket, idpendaftaran, rj.tanggal","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();
        $this->CI->db->update('rs_pemeriksaan',['waktu'=>$akronimunit['tanggal']],['idpemeriksaan'=>$idpemeriksaan]);
    	if (empty($akronimunit))
        {
            $unit = $this->CI->mgenerikbap->select_multitable("namaunit","rs_unit",['idunit'=>$idunit])->row_array();
            echo json_encode(['err'=>'ERR_LOKET', 'namaunit' => $unit['namaunit']]);
        }
        
    	$date = $this->CI->mgenerikbap->select_multitable('waktu','rs_pemeriksaan',['idpemeriksaan'=>$idpemeriksaan])->row_array()['waktu'];
    	$expired = date('Y-m-d', strtotime($date));
    	$id      = 'Antrian'.date('Ymd',strtotime($date)).'-'.$akronimunit['idstasiun'].'-'.$akronimunit['idloket'];
        $this->antrian_autonumber($id,$callid,$expired);
        $noantrian = $this->CI->db->select('number')->get_where('helper_autonumber',['callid'=>$callid,'expired'=>$expired,'id'=>$id])->row_array()['number'];
        $dataa = ['idloket'=>$akronimunit['idloket'], 'tanggal'=>$expired,'kodebooking'=>$callid, 'no'=>$noantrian, 'status'=>'antri', 'idperson'=>$idperson,'idpemeriksaan'=>$idpemeriksaan];
        $this->CI->db->insert('antrian_antrian',$dataa);
    }
    /**
     * Buat nomor otomatis
     * @param type $id
     * @param type $callid
     * @param type $expired
     */
    public function antrian_autonumber($id,$callid,$expired)
    {
    	$this->CI->mgenerikbap->setTable('helper_autonumber');
        $data = ['id'=>$id,'number'=>null,'callid'=>$callid,'expired'=>$expired];
        $this->CI->mgenerikbap->insert($data);
    }
    /**
     * Ambil nomor antrian
     * @param type $callid 
     * @return nomor antrian
     */
    public function antrian_get_number($callid)
    {
    	return $q=$this->CI->mgenerikbap->select_multitable('number','helper_autonumber',['callid'=>$callid])->row_array()['number'];
    }
    
//--mahmud, clear 
    public function buat_antriansep($idpemeriksaan)
    {
    	$dtdaftar  = $this->CI->db->query("select pp.carabayar, rp.waktu, ppas.idperson, pp.caradaftar from rs_pemeriksaan rp, person_pendaftaran pp, person_pasien ppas where rp.idpemeriksaan='$idpemeriksaan' and pp.idpendaftaran = rp.idpendaftaran and ppas.norm=pp.norm")->row_array();
        if( $dtdaftar['carabayar']=='mandiri' OR $dtdaftar['caradaftar']=='online'){
            return $antrisep =['noantrian' =>'','loket'=>''];
        }else{
            $tgl = date('Y-m-d',strtotime($dtdaftar['waktu']));
            $idloket   = 39;
            $idstasiun = 1;
            $callid    = generaterandom(8);

            $expired = date('Y-m-d', strtotime($tgl));
            $id      = 'Antrisep'.date('Ymd', strtotime($tgl)).'-'.$idstasiun.'-'.$idloket;
            $this->antrian_autonumber($id,$callid,$expired);
            $noantrisep = $this->antrian_get_number($callid);

            $data   = ['idloket'=>$idloket, 'tanggal'=>date('Y-m-d', strtotime($tgl)), 'no'=>$noantrisep, 'status'=>'antri','idperson'=>$dtdaftar['idperson']];
            $arrMsg = $this->CI->db->insert('antrian_antrian',$data);
            $loket  = $this->CI->db->query("select * from antrian_loket where idloket='$idloket'")->row_array();
            return $antrisep = ['noantrian' =>$noantrisep,'loket'=>$loket['namaloket']];
        }
    }
    
//---------------PAKET VITAL SIGN
//--menambahkan paket vita sign saat pendaftaran pasien
    public function addpaketvitalsign($idpemeriksaan,$idjadwal,$idpendaftaran,$idjadwalgrup='')
    {
        $this->CI->load->model('mviewql');
        $listpaket = $this->CI->mviewql->viewpaketvitalsign($idjadwal,$idjadwalgrup);//liast vitalsign paket
        if(!empty($listpaket))
        {
            foreach ($listpaket as $obj)
            {
                // jika icd sudah ada maka tidak disimpan
                if($this->paketicdperiksa($idpendaftaran,$obj->icd)->num_rows() < 1)
                {
                    $data = ['idpemeriksaan'=>$idpemeriksaan,'idpendaftaran'=>$idpendaftaran,'icd'=>$obj->icd,'idpaketpemeriksaan'=>$obj->idpaketpemeriksaan];
                    $arrMsg = $this->CI->db->insert("rs_hasilpemeriksaan",$data);
                    if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Save Paket Vitalsign Failed..!']);}
                }
            }
        }
    }
    
    public  function paketicdperiksa($idp,$icd)
    {
        return $this->CI->db->query("select * from rs_hasilpemeriksaan where idpendaftaran='".$idp."' and icd='".$icd."'");
    }
    /*
     * Group Data Objek -> data pendaftaran pasien
     * @param type $dataObj    Data Pendaftaran Pasien Dalam Bentuk Object
     */    
    public function group_datapendaftaran($dataObj)
    {
        array_multisort(array_map(function($element){return $element->idpendaftaran;}, $dataObj), SORT_ASC, $dataObj);
        $arrData=[];
        $idpendaftaran='';
        foreach($dataObj as $val)
        {
            if($idpendaftaran=='' || $idpendaftaran != $val->idpendaftaran)
            {
                $arrData[]=$val;
            }
            $idpendaftaran = $val->idpendaftaran;
        }
        array_multisort(array_map(function($element){return $element->waktu;}, $arrData), SORT_DESC, $arrData);
        return $arrData;
    }
    /**
     * Insert Task Antrean
     * @param type $taskid  referensi di tabel bpjs_bridging_antrean_task
     * @param type $idpendaftaran Id Pendaftaran
     * @return type
     */
    public function insert_antrean_task_log($taskid,$idpendaftaran)
    {
        $save = $this->CI->db->replace('bpjs_bridging_antrean_task_log',['taskid'=>$taskid,'idpendaftaran'=>$idpendaftaran]);
        return $save;
    }

    /**
     * Simpan atau ubah  Bridging BPJS VClaim
     * @param $idpendaftaran --> Id Pendaftaran
     * @param $data --> Data yang disimpan (array)
     */
    public function bpjs_bridging_vclaim_insert_or_update($idpendaftaran,$data)
    {          

        $idpendaftaranStatus = []; 
        $idpendaftaranStatus = $this->CI->db->query("SELECT * FROM bpjs_bridging_vclaim where idpendaftaran='".$idpendaftaran."'")->row_array();
        // var_dump("idpendaftaranStatus = ".$idpendaftaranStatus); die;
            
        $data['idpendaftaran'] = $idpendaftaran;
        $this->CI->mgenerikbap->setTable('bpjs_bridging_vclaim');
        $simpan = ((is_null($idpendaftaranStatus))? $this->CI->mgenerikbap->insert_ignoreduplicate($data) : $this->CI->db->update('bpjs_bridging_vclaim',$data,['idpendaftaran'=>$idpendaftaran]));
        return $simpan;
}
}
/* End of file Ql.php */
/* Location: ./application/libraries/Ql.php */