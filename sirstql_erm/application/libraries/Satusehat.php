<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require_once 'LZCompressor/autoload.php'; // untuk decompress response dari vclaim v2

class Satusehat
{
    /**DEV */
    // private $auth_url='https://api-satusehat-dev.dto.kemkes.go.id/oauth2/v1';
    // private $base_url='https://api-satusehat-dev.dto.kemkes.go.id/fhir-r4/v1';
    /**PROD */
    private $auth_url='https://api-satusehat.kemkes.go.id/oauth2/v1';
    private $base_url='https://api-satusehat.kemkes.go.id/fhir-r4/v1';
    private $url;
    private $client_id;
    private $client_secret;
    private $OrganizationID;
    private $token  = "";
    
    private function upass()
    {
        if(file_exists("./QLKP.php"))
        {
            // setting for QLKP - RSU QUEEN LATIFA KULON PROGO
            /** DEV */
            // $this->client_id='7POnk4REnAtM3ohUVXV8WWWYJolDbu2LKPW5UipJ4oNIK31e';
            // $this->client_secret='IYcprRKiXH8a23cD0nGDuXLiwYCpcQkbgwUpQcbxhuLyUDffhxAQxF0nNMTh2eUG'; 
            // $this->OrganizationID='10084049';
            /** PROD */
            $this->client_id='DhnxvzP04z8nutsv4jRONom82C0vi0MnbJZQXS1Dnzgdzy0z';
            $this->client_secret='oGCf0nG9orl7KDVlrcykMdYGS8qW8t8VkYLhC8FKrdB07VILXenuWp0szCqQmzuG'; 
            $this->OrganizationID='100027527';
        }
        else 
        {   // setting for QLKP - RSU QUEEN LATIFA YOGYAKARTA       
            $this->client_id = 'Dp6BGoLAtM7gLXtLL7AI58rAaLubUpfL5e0lHiUo08bpA2zG';
            $this->client_secret='JJcut0EqceF0bBQrL6sQgkiHa55wZVxZ15R2XCBGorfqb1MikKUutzjD9H1FRYv0';
            $this->OrganizationID='10084055';
        }
    }
    
    function createSignature() //$requestParameter=array())
    {
        $this->upass();
        $this->url = $this->auth_url.'/accesstoken?grant_type=client_credentials';
        
        // Init
        $ch = curl_init();

        // Setopt array
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'client_id='.$this->client_id.'&client_secret='.$this->client_secret,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/x-www-form-urlencoded'
            ),
          ));
          
          // Execute
          $data = curl_exec($ch);
          // Close
          curl_close($ch);
                    
          if (empty($data) or $data == "null")
          {
              $data = curl_error($ch);
              return json_decode($data);  
          }else {
            // return $data;  
            $result =  json_decode($data);
            $result_token['Authorization'] = 'Bearer '.$result->access_token;     
            return $result_token;
          }          
    }

    function requestSatuSehat($addurl, $method, $token) //$requestParameter=array())
    {
        $this->upass();
        $this->url = $this->base_url.$addurl;
        // Init
        $ch = curl_init();

        // Setopt array
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
              'Content-Type: application/json',
            //   $token
              'Authorization: '.$token
            ),
          ));
          
          // Execute
          $data = curl_exec($ch);
          // Close
          curl_close($ch);
                    
          if (empty($data) or $data == "null")
          {
              $data = curl_error($ch);
          }          
          
          return json_decode($data);  
    }
    
    function createSatuSehat($addurl, $data, $token, $method) //$requestParameter=array())
    {
        $this->upass();
        $this->url = $this->base_url.$addurl;
        // Init
        $ch = curl_init();
        $contentType = ($method === 'PATCH') ? 'application/json-patch+json' : 'application/json';

        // Setopt array
        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
              'Content-Type: '.$contentType,
              'Authorization: '.$token,
              'Content-Length: ' . strlen($data)
            ),
            CURLOPT_POSTFIELDS => $data, // Kirim data dalam body
          ));
          
          // Execute
          $response = curl_exec($ch);
          $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          // Close
          curl_close($ch);
                    
          return array('response' => $response, 'http_code' => $httpCode);
    }
     
    function getOrganizationID(){
      if(file_exists("./QLKP.php"))
        {
            // setting for QLKP - RSU QUEEN LATIFA KULON PROGO
            /** DEV */
            // $this->client_id='7POnk4REnAtM3ohUVXV8WWWYJolDbu2LKPW5UipJ4oNIK31e';
            // $this->client_secret='IYcprRKiXH8a23cD0nGDuXLiwYCpcQkbgwUpQcbxhuLyUDffhxAQxF0nNMTh2eUG'; 
            // $this->OrganizationID='10084049';
            /** PROD */
            $this->client_id='DhnxvzP04z8nutsv4jRONom82C0vi0MnbJZQXS1Dnzgdzy0z';
            $this->client_secret='oGCf0nG9orl7KDVlrcykMdYGS8qW8t8VkYLhC8FKrdB07VILXenuWp0szCqQmzuG'; 
            $this->OrganizationID='100027527';
        }
      else 
        {   // setting for QLKP - RSU QUEEN LATIFA YOGYAKARTA       
          /**DEV */
            $this->client_id = 'JVIKL2nsXG2VCJ7b9wy6QeOUmum9XhZah1AFwjGmq5XfWP95';
            $this->client_secret='mpxr9n0Yn8BzryNxlbRGuqe3dTf0XKUMH1lmNdut8wIsmvRFOt7f4X4rXqPnB50H';
            $this->OrganizationID='897d5669-2692-41ed-be93-6a7a097da53f';
          /** PROD */
            // $this->client_id = 'CZ9PtSAVlGkHZigY5OSqQ6iq4Rkd977bfwnGnJsOsjo15iOS';
            // $this->client_secret='20xeADbPDwxg74lT4YqkAcrY4IGLnICPsAGTG1HdVl56ujFboPMMAgWA0AXMUFAY';
            // $this->OrganizationID='100027524';
        }
      return $this->OrganizationID;
    }     
}