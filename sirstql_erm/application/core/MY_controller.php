<?php

class MY_controller extends  CI_controller
{
    function __construct()
    {
	   parent::__construct();
    }
    
    // ubah rs_pemeriksaan, $data= data yg diubah dalam bentuk array,, $where = array
    public function rs_pemeriksaan_update_where($data,$where)
    {
        $simpan = $this->db->update('rs_pemeriksaan',$data,$where);
        return $simpan;
    }
    
    /**
     * Set Antrian Periksa
     * @param type $idunit  Id Unit
     * @param type $idpemeriksaan ID Pemeriksaan
     * @param type $idperson Id Person
     */

     public function start_antrianset_antrianperiksa($idunit,$idpemeriksaan,$idperson,$modeapp='local')
    {
        $callid         = generaterandom(8);
        //buat no antrian periksa
        $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson);
        //get data antrian
        $get_antrian = $this->db->query("SELECT rp.idpendaftaran, aa.kodebooking, aa.no,  rj.quota_nonjkn, rj.quota_jkn,
        ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
        ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn,
        ifnull((select pp.norujukan from person_pendaftaran pp WHERE pp.idpendaftaran = rp.idpendaftaran),'') as nomorreferensi
        FROM antrian_antrian aa
        join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
        join rs_jadwal rj on rj.idjadwal = rp.idjadwal
        join rs_unit ru on ru.idunit = rj.idunit
        WHERE aa.idpemeriksaan ='".$idpemeriksaan."'")->row_array();

        //update rs_pemeriksaan
        $this->db->update('rs_pemeriksaan',['noantrian'=>$get_antrian['no'],'kodebooking'=>$get_antrian['kodebooking'],'status'=>'antrian'], ['idpemeriksaan'=>$idpemeriksaan]);

        //insert bridging vclaim
        $idpendaftaranBridging = $get_antrian['idpendaftaran'];

        $databridging['idpendaftaran']     = $get_antrian['idpendaftaran'];
        $databridging['quota_jkn']     = $get_antrian['quota_jkn'];
        $databridging['quota_nonjkn']  = $get_antrian['quota_nonjkn'];
        $databridging['quota_jkn_sisa']  = intval($get_antrian['quota_jkn']) - intval($get_antrian['antrianjkn']) ;
        $databridging['quota_nonjkn_sisa']  = intval($get_antrian['quota_nonjkn']) - ( intval($get_antrian['totalantrean']) - intval($get_antrian['antrianjkn']) ) ;
        $databridging['nomorreferensi']     = $get_antrian['nomorreferensi'];
        $databridging['insert_by']     = $this->session->userdata('username');
        
        if($idpendaftaranBridging)
        {
            $cek_idpendaftaranBridging = $this->db->query("SELECT * FROM `bpjs_bridging_vclaim` WHERE idpendaftaran='".$idpendaftaranBridging."' limit 1;")->row_array();
            $new_idpendaftaranBridging =null;
            
            if($cek_idpendaftaranBridging){
                $new_idpendaftaranBridging = $cek_idpendaftaranBridging['idpendaftaran'];
            } else{
                $new_idpendaftaranBridging = null;
            }
            
        $this->mgenerikbap->setTable('bpjs_bridging_vclaim'); //set table yang akan dipakai
        $this->mgenerikbap->update_or_insert_ignoreduplicate($databridging, $new_idpendaftaranBridging);       
        }// $this->db->replace('bpjs_bridging_vclaim',$databridging);
        
        $akronimunit = $this->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();

        $datapasien = $this->mgenerikbap->select_multitable("norm, concat(ifnull(identitas,''),' ',ifnull(namalengkap,'')) as namalengkap, usia(tanggallahir) as usia, date_format(tanggallahir,'%d %b %Y') as tanggallahir, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();
        // ambil nama dokter
        $dtdokter = $this->db->query("SELECT DATE_FORMAT(rp.waktu, '%d/%m/%Y') as waktu,  CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
        // antrian sep
        //$antrisep = $this->ql->buat_antriansep($idpemeriksaan);
        
        $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';buat antrian-poli;idunit:'.$idunit.';idpemeriksaan:'.$idpemeriksaan.';'.';norm:'.$datapasien['norm'].';namaloket:'.$akronimunit['namaloket'],
                'expired' =>date('Y-m-d', strtotime("+2 weeks", strtotime(date('Y-m-d'))))
            ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        
    }

     public function admisi_antrianset_antrianperiksa($idunit,$idpemeriksaan,$idperson,$modeapp='local')
    {
        // $callid         = generaterandom(8);
        //buat no antrian periksa
        // $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson);
        //get data antrian
        $get_antrian = $this->db->query("SELECT rp.idpendaftaran, aa.kodebooking, aa.no,  rj.quota_nonjkn, rj.quota_jkn,
        ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
        ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn,
        ifnull((select pp.norujukan from person_pendaftaran pp WHERE pp.idpendaftaran = rp.idpendaftaran),'') as nomorreferensi
        FROM antrian_antrian aa
        join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
        join rs_jadwal rj on rj.idjadwal = rp.idjadwal
        join rs_unit ru on ru.idunit = rj.idunit
        WHERE aa.idpemeriksaan ='".$idpemeriksaan."'")->row_array();

        // print_r($get_antrian);
        // die();

        //update rs_pemeriksaan
        $this->db->update('rs_pemeriksaan',['noantrian'=>$get_antrian['no'],'kodebooking'=>$get_antrian['kodebooking'],'status'=>'antrian'], ['idpemeriksaan'=>$idpemeriksaan]);

        //insert bridging vclaim
        $idpendaftaranBridging = $get_antrian['idpendaftaran'];

        $databridging['idpendaftaran']     = $get_antrian['idpendaftaran'];
        $databridging['quota_jkn']     = $get_antrian['quota_jkn'];
        $databridging['quota_nonjkn']  = $get_antrian['quota_nonjkn'];
        $databridging['quota_jkn_sisa']  = intval($get_antrian['quota_jkn']) - intval($get_antrian['antrianjkn']) ;
        $databridging['quota_nonjkn_sisa']  = intval($get_antrian['quota_nonjkn']) - ( intval($get_antrian['totalantrean']) - intval($get_antrian['antrianjkn']) ) ;
        $databridging['nomorreferensi']     = $get_antrian['nomorreferensi'];
        $databridging['insert_by']     = $this->session->userdata('username');
        
        if($idpendaftaranBridging)
        {
            $cek_idpendaftaranBridging = $this->db->query("SELECT * FROM `bpjs_bridging_vclaim` WHERE idpendaftaran='".$idpendaftaranBridging."' limit 1;")->row_array();
            $new_idpendaftaranBridging =null;
            
            if($cek_idpendaftaranBridging){
                $new_idpendaftaranBridging = $cek_idpendaftaranBridging['idpendaftaran'];
            } else{
                $new_idpendaftaranBridging = null;
            }
            
        $this->mgenerikbap->setTable('bpjs_bridging_vclaim'); //set table yang akan dipakai
        $this->mgenerikbap->update_or_insert_ignoreduplicate($databridging, $new_idpendaftaranBridging);       
        }// $this->db->replace('bpjs_bridging_vclaim',$databridging);
        
        $akronimunit = $this->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();

        $datapasien = $this->mgenerikbap->select_multitable("norm, concat(ifnull(identitas,''),' ',ifnull(namalengkap,'')) as namalengkap, usia(tanggallahir) as usia, date_format(tanggallahir,'%d %b %Y') as tanggallahir, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();
        // ambil nama dokter
        $dtdokter = $this->db->query("SELECT DATE_FORMAT(rp.waktu, '%d/%m/%Y') as waktu,  CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
        // antrian sep
        //$antrisep = $this->ql->buat_antriansep($idpemeriksaan);
        
        $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';buat antrian-poli;idunit:'.$idunit.';idpemeriksaan:'.$idpemeriksaan.';'.';norm:'.$datapasien['norm'].';namaloket:'.$akronimunit['namaloket'],
                'expired' =>date('Y-m-d', strtotime("+2 weeks", strtotime(date('Y-m-d'))))
            ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        
    }
    public function antrianset_antrianperiksa($idunit,$idpemeriksaan,$idperson,$modeapp='local')
    {
        $callid         = generaterandom(8);
        //buat no antrian periksa
        $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson);
        //get data antrian
        $get_antrian = $this->db->query("SELECT rp.idpendaftaran, aa.kodebooking, aa.no,  rj.quota_nonjkn, rj.quota_jkn,
        ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
        ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn,
        ifnull((select pp.norujukan from person_pendaftaran pp WHERE pp.idpendaftaran = rp.idpendaftaran),'') as nomorreferensi
        FROM antrian_antrian aa
        join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
        join rs_jadwal rj on rj.idjadwal = rp.idjadwal
        join rs_unit ru on ru.idunit = rj.idunit
        WHERE aa.idpemeriksaan ='".$idpemeriksaan."'")->row_array();

        //update rs_pemeriksaan
        $this->db->update('rs_pemeriksaan',['noantrian'=>$get_antrian['no'],'kodebooking'=>$get_antrian['kodebooking'],'status'=>'antrian'], ['idpemeriksaan'=>$idpemeriksaan]);

        //insert bridging vclaim
        $idpendaftaranBridging = $get_antrian['idpendaftaran'];

        $databridging['idpendaftaran']     = $get_antrian['idpendaftaran'];
        $databridging['quota_jkn']     = $get_antrian['quota_jkn'];
        $databridging['quota_nonjkn']  = $get_antrian['quota_nonjkn'];
        $databridging['quota_jkn_sisa']  = intval($get_antrian['quota_jkn']) - intval($get_antrian['antrianjkn']) ;
        $databridging['quota_nonjkn_sisa']  = intval($get_antrian['quota_nonjkn']) - ( intval($get_antrian['totalantrean']) - intval($get_antrian['antrianjkn']) ) ;
        $databridging['nomorreferensi']     = $get_antrian['nomorreferensi'];
        $databridging['insert_by']     = $this->session->userdata('username');
        
        if($idpendaftaranBridging)
        {
            $cek_idpendaftaranBridging = $this->db->query("SELECT * FROM `bpjs_bridging_vclaim` WHERE idpendaftaran='".$idpendaftaranBridging."' limit 1;")->row_array();
            $new_idpendaftaranBridging =null;
            
            if($cek_idpendaftaranBridging){
                $new_idpendaftaranBridging = $cek_idpendaftaranBridging['idpendaftaran'];
            } else{
                $new_idpendaftaranBridging = null;
            }
            
        $this->mgenerikbap->setTable('bpjs_bridging_vclaim'); //set table yang akan dipakai
        $this->mgenerikbap->update_or_insert_ignoreduplicate($databridging, $new_idpendaftaranBridging);       
        }// $this->db->replace('bpjs_bridging_vclaim',$databridging);
        
        $akronimunit = $this->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();

        $datapasien = $this->mgenerikbap->select_multitable("norm, concat(ifnull(identitas,''),' ',ifnull(namalengkap,'')) as namalengkap, usia(tanggallahir) as usia, date_format(tanggallahir,'%d %b %Y') as tanggallahir, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();
        // ambil nama dokter
        $dtdokter = $this->db->query("SELECT DATE_FORMAT(rp.waktu, '%d/%m/%Y') as waktu,  CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
        // antrian sep
        //$antrisep = $this->ql->buat_antriansep($idpemeriksaan);
        
        $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';buat antrian-poli;idunit:'.$idunit.';idpemeriksaan:'.$idpemeriksaan.';'.';norm:'.$datapasien['norm'].';namaloket:'.$akronimunit['namaloket'],
                'expired' =>date('Y-m-d', strtotime("+2 weeks", strtotime(date('Y-m-d'))))
            ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            
        $result = array(
            'noantrian'=>$get_antrian['no'],
            'kodebooking'=>$get_antrian['kodebooking'],
            'datapasien'=>$datapasien,
            'namaloket'=>$akronimunit['namaloket'],
            'dokter'=>$dtdokter
            );
        
        if($modeapp=='local')
        {
            echo json_encode($result);
        }
        else
        {
            return $result;      
        }
        
    }
    
    // RUJUK PEMERIKSAAN KLINIK
    public function rujuk_addpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK))
        {
            $idpemeriksaan = $this->input->post('idperiksa');
            $idjadwal      = $this->input->post('idjadwal');
            $idp           = $this->input->post('idperson');
            $status        = $this->input->post('status');
            $arrMsg = $this->rujuk_updatepemeriksaan($idpemeriksaan,$status,$idjadwal);
            $idrp = $this->rujuk_savepemeriksaan($idpemeriksaan,$idjadwal,$status,$idp);
            $arrMsg = $this->rujuk_generatenoantrian($idrp,$idjadwal,$idp);
            
            $status  = (($arrMsg) ? 'success' :'danger' );
            $message = (($arrMsg) ? 'Rujuk Success.!' :'Rujuk Failed.!' );
            $response = [
              'idpemeriksaan'=>$idrp,
              'status'=>$status,
              'message'=>$message
            ];
            return $response;
        }
    }
    public function pendaftaran_politerpadu()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK))
        {
            $idpemeriksaan = $this->input->post('idperiksa');
            $idjadwal      = $this->input->post('idjadwal');
            $idp           = $this->input->post('idperson');
            $status        = '';
            $idrp = $this->rujuk_savepemeriksaan($idpemeriksaan,$idjadwal,$status,$idp);
            $arrMsg = $this->rujuk_generatenoantrian($idrp,$idjadwal,$idp);
            // pesan_success_danger($arrMsg, 'Rujuk Success.!', 'Rujuk Failed.!', 'js');   
        }
    }
    private function rujuk_updatepemeriksaan($id,$status,$idjadwal)
    {
        $cekdata = $this->db->query("SELECT if(ru.polikhusus = 1,0,1) as polikhusus FROM rs_jadwal rj join rs_unit ru on ru.idunit = rj.idunit WHERE idjadwal ='".$idjadwal."'");
        $polikhususakhir = 0;
        if($cekdata->num_rows() > 0)
        {
            $data = $cekdata->row_array();
            $polikhususakhir = $data['polikhusus'];
        }
        
        $arrMsg = $this->db->update('rs_pemeriksaan',['status'=>$status,'polikhususakhir'=>$polikhususakhir],$id);
        if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Update Pemeriksaan Failed..!']);}
    }
    /**
     * Rujuk Internal Ralan Buat Pemeriksaan Baru 
     * @param type $id
     * @param type $jadwal
     * @param type $status
     * @param type $idp
     * @return type
     */
    private function rujuk_savepemeriksaan($id,$jadwal,$status,$idp)
    {

        $pemeriksaan_sebelum = $this->db->query("select * from rs_pemeriksaan where idpemeriksaan='$id'")->row_array();
        $data = [
            'idpendaftaran'=>$pemeriksaan_sebelum['idpendaftaran'],
            'idjadwal'  =>$jadwal,
            'nopesan'  =>$pemeriksaan_sebelum['nopesan'],
            'tahunbulan' =>$pemeriksaan_sebelum['tahunbulan'],
            'norm'=>$pemeriksaan_sebelum['norm'],
            'status'=>$status,
            'waktu'=>format_date_to_sql($this->input->post('tanggalperiksa')),
            'noantrian'=> '',
            'idpemeriksaansebelum'=>$pemeriksaan_sebelum['idpemeriksaan']
        ];
        $arrMsg = $this->rujuk_savepaketvitalsign($jadwal,$pemeriksaan_sebelum['idpendaftaran']);
        $this->mgenerikbap->setTable("rs_pemeriksaan");
        $arrMsg = $this->mgenerikbap->insert($data);
        return $this->db->insert_id();
        if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Save Rujuk Pemeriksaan Failed..!']);}
    }
    
    private function rujuk_generatenoantrian($idpemeriksaan,$idjadwal,$idp)
    {
        $callid = generaterandom(8);
        $idunit = $this->db->query("select idunit from rs_jadwal where idjadwal='".$idjadwal."'")->row_array()['idunit'];
        $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idp);
        $this->db->update('rs_pemeriksaan',['noantrian'=>$this->ql->antrian_get_number($callid)],['idpemeriksaan'=>$idpemeriksaan]);
        return true;
    }
    private function rujuk_savepaketvitalsign($idjadwal,$idpendaftaran)
    {
        $this->load->model('mviewql');
        $listpaket = $this->mviewql->viewpaketvitalsign($idjadwal);//liast vitalsign paket
        if(!empty($listpaket))
        {
            foreach ($listpaket as $obj) {
                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$obj->icd);
                if(empty($cekData->num_rows()))
                {
                    $data = ['idpendaftaran'=>$idpendaftaran,'icd'=>$obj->icd];
                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan",$data);
                    if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Save Paket Vitalsign Failed..!']);}
                }
            }
        }
    }
    // RUJUK


    // data dokter
    public function sql_ambildatadokter()
    {
        $sql = 'SELECT idpegawai, trim(concat(ifnull(titeldepan, ""), " ", namalengkap, " ", ifnull(titelbelakang, ""))) as namalengkap FROM person_pegawai, person_person where idgruppegawai=5 and `person_pegawai`.idperson=person_person.idperson order by namalengkap';
       return $this->db->query($sql);
    }
    
    // save pemeriksaan grup_jadwal
    protected function pendaftaranpoli_savedaftarpemeriksaan_grupjadwal($idpendaftaran,$jadwal,$nopesan,$norm,$callid,$idjadwalgrup)
    {
        $this->ql->person_pendaftaran_insert_or_update($idpendaftaran,['nopesan'=>$nopesan]);
        foreach ($jadwal as $value) {
            $idjadwal = explode(',',$value)[1];
            $data = [
                'idpendaftaran'=>$idpendaftaran,
                'idjadwal'  =>$idjadwal,
                'nopesan'  =>$nopesan,
                'tahunbulan' =>date('Yn'),
                'waktu'     =>(empty(format_date_to_sql($this->input->post('pilihtanggalantrian')))) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(format_date_to_sql($this->input->post('pilihtanggalantrian')))),
                'norm'=>$norm,
                'status'=>'pesan',
                'callid'=>$callid,
                'noantrian'=>'0'
            ];
            $simpan = $this->ql->rs_pemeriksaan_insert_or_update('',$data);
            $idpemeriksaan = $this->db->insert_id();
            $simpan = $this->ql->addpaketvitalsign($idpemeriksaan,$idjadwal,$idpendaftaran,$idjadwalgrup);
            $this->load->model('mtriggermanual');
            $this->mtriggermanual->airs_pemeriksaan($idjadwal);
            $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';status:'.$data['status'].';idpendaftaran:'.$idpendaftaran.';norm:'.$norm,
                'expired' =>date('Y-m-d', strtotime("+1 weeks", strtotime(date('Y-m-d'))))
            ];
            $this->mgenerikbap->setTable('login_log');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        }
        $this->mkombin->settarifgrupperiksa($idpemeriksaan,$idjadwalgrup);// set tarif otomatis grup pemeriksaan
        
    }
    // mahmud, clear
    protected function pendaftaran_savebpjs($norm,$jenispeserta,$kelas,$informasi,$tanggalcetak,$statuspeserta)
    {
        $data = [
            'norm'=>$norm,
            'idjenispeserta'=>$jenispeserta,
            'idkelas'=>$kelas,
            'informasi'=>$informasi,
            'tglcetakkartu'=>$tanggalcetak,
            'idstatuspeserta'=>$statuspeserta
        ];
        $this->mgenerikbap->setTable('bpjs_bpjs');
       return $this->mgenerikbap->update_or_insert_ignoreduplicate($data);
    }
    // mahmud, clear
    protected function pendaftaran_updatebpjs($norm,$jenispeserta,$kelas,$informasi,$tanggalcetak,$statuspeserta)
    {
        $data = [
            'idjenispeserta'=>$jenispeserta,
            'idkelas'=>$kelas,
            'informasi'=>$informasi,
            'tglcetakkartu'=>$tanggalcetak,
            'idstatuspeserta'=>$statuspeserta
        ];
       return $this->db->update('bpjs_bpjs',$data,['norm'=>$norm]);
    }
    /**
    * Permintaan Barang
    * @param
    * type $idinap Id Inap
    * type $idbarang Id Barang
    * type $jenistransaksi Jenis Transaksi
    * type $jumlah Jumlah Permintaan
    */
    // mahmud, clear
    protected function barangpemesanan_inputbhp($idinap,$idbarang,$jenistransaksi,$jumlah,$isupdate='update')
    {   
        if(empty(!$this->session->userdata('idunitterpilih')))
        {
            $cekpesan = $this->db->query("select * from rs_barang_pemesanan where idinap='".$idinap."' and statusbarangpemesanan='menunggu' and jenistransaksi='".$jenistransaksi."'");
            if($cekpesan->num_rows() > 0){
                $cek     = $cekpesan->row_array();
                $idpesan = $cek['idbarangpemesanan'];
            }else{
                $callid = generaterandom(8);
                $fakturid = (($jenistransaksi=='pesanobatpulang' or $jenistransaksi=='pesanobatranap') ? fakturpermintaanbarang() : fakturpengembalianbarang() );
                $this->ql->antrian_autonumber( $fakturid,$callid,date('Y-m-d',strtotime("+2 weeks")));
                $nofaktur = $fakturid.$this->db->get_where('helper_autonumber',['callid'=>$callid])->row_array()['number'];
                
                if($jenistransaksi=='pesanobatpulang' or $jenistransaksi=='pesanobatranap') 
                {
                    $idunittujuan = $this->session->userdata('idunitterpilih');
                    $idunit= $this->db->query("select idunitparent from rs_unit_farmasi where idunit ='".$idunittujuan."'")->row_array()['idunitparent'];
                }
                else
                {
                    $idunit = $this->session->userdata('idunitterpilih');
                    $idunittujuan = $this->db->query("select idunitparent from rs_unit_farmasi where idunit ='".$idunit."'")->row_array()['idunitparent'];
                }        
                $idpersonpetugas = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('pper')));
                
                $this->db->insert('rs_barang_pemesanan',['idinap'=>$idinap,'idunit'=>$idunit,'idunittujuan'=>$idunittujuan,'jenistransaksi'=>$jenistransaksi,'nofaktur'=>$nofaktur,'idpersonpetugas'=>$idpersonpetugas]);
                $idpesan = $this->db->insert_id();
            }
            $data = ['idbarangpemesanan'=>$idpesan,'idbarang'=>$idbarang,'jumlah'=> ((empty($jumlah)) ? 1 : $jumlah ) ];

            $cekdetail = $this->db->query("select * from rs_barang_pemesanan_detail where idbarangpemesanan='".$idpesan."' and idbarang='".$idbarang."'");
            if($cekdetail->num_rows() > 0){
                $cek = $cekdetail->row_array();
                $jumlahupdate = (($isupdate=='update') ?  $cek['jumlah'] + ((empty($jumlah)) ? 1 : $jumlah ) : $jumlah);
                $arrmsg = $this->db->update('rs_barang_pemesanan_detail',['jumlah'=>$jumlahupdate],['idbarang'=>$idbarang,'idbarangpemesanan'=>$idpesan]);
            }else{
                $arrmsg = $this->db->insert('rs_barang_pemesanan_detail',$data);
            }
            return $arrmsg;
        }
        return true;
    }
    
    /*
     * Mutasi rajal ke ranap
     * @param type $idp Idpendaftaran
     */
    public function mutasirajalkeranap($idp)
    {
        $inap = $this->db->select("ri.idinap, ri.idkelas, ri.callid, (select idbangsal from rs_bed b where b.idbed=ri.idbed) idbangsal")->get_where('rs_inap ri',['idpendaftaran'=>$idp,'status'=>'pesan']);
        if($inap->num_rows() > 0)
        {
            $datainap = $inap->row_array();
            $save = $this->db->update('person_pendaftaran',['jenispemeriksaan'=>'rajalnap','idstatuskeluar'=>1],['idpendaftaran'=>$idp]);
            $save = $this->db->update('rs_inap',['status'=>'ranap','administrasimasuk'=>0],['idinap'=>$datainap['idinap']]);
            return $save;
        }
        return true;
    }
    /*Insert Log Hasil Pemeriksaan*/
    //mahmud, clear
    public function log_hasilpemeriksaan($idpendaftaran,$status,$icd,$idb='')
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $deskripsi = 'STATUS:'.$status.' IP:'.$_SERVER['REMOTE_ADDR'].' Client-Info:'. exec('getmac');
        $expired = date('Y-m-d', strtotime("+8 weeks", strtotime(date('Y-m-d'))));
        $save = $this->db->insert('log_hasilpemeriksaan',['idpendaftaran'=>$idpendaftaran,'iduser'=>$iduser,'icd'=>$icd,'idbarang'=>$idb,'url'=>get_url(),'deskripsi'=>$deskripsi,'expired'=>$expired]);
        return $save;
    }
    
    /**
     * Jika Pemeriksaan selesai
     * @param type $idp Id Pendaftaran
     * @param type $mode Mode List or Mode Save
     */
    public function isselesaipemeriksaan($idp,$mode)
    {
        if($mode!='verifikasi'){
            $query = $this->db->select('idstatuskeluar')->get_where('person_pendaftaran',['idpendaftaran'=>$idp])->row_array();
            if($query['idstatuskeluar']=='2' || $query['idstatuskeluar']==2)
            {
                return false;
            }
            return true;
        }
        return true;
        
    }
    /**
     * Ambil iduser yang sedang login
     * @return type
     */
    public function get_iduserlogin()
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('iduser')));
        return $iduser;
    }
    
    public function getdata_penanggungpasien($norm)
    {
        return  $this->db->query("select pp.namalengkap, pp.notelpon, pp.alamat, ph.namahubungan from person_pasien_penanggungjawab ppp,person_person pp, person_hubungan ph where ppp.norm='".$norm."' and ppp.statuspenanggung='penanggung' and pp.idperson=ppp.idperson and ph.idhubungan=ppp.idhubungan")->result_array();
    }
    
    /**
     * Insert waktu layanan
     * @param type $norm Norm
     * @param type $idpendaftaran Id Pendaftaran
     * @param type $kegiatan  Kegiatan
     * @param type $waktumulai    Waktu Mulai
     * @param type $waktuselesai  Waktu Selesai
     */
    public function insert_waktulayanan($norm,$idpendaftaran,$kegiatan,$tanggalperiksa,$unit,$waktumulai='',$waktuselesai='')
    {
        $iduser = $this->get_iduserlogin();
        $data = [
            'norm'=>$norm,
            'idpendaftaran'=>$idpendaftaran,
            'kegiatan'=>$kegiatan,
            'unitlayanan'=>$unit,
            'waktumulai'=> ((empty($waktumulai)) ? date('H:i:s') : $waktumulai ),
            'waktuselesai'=> ((empty($waktuselesai)) ? date('H:i:s') : $waktuselesai ),
            'tanggalperiksa'=>$tanggalperiksa,
            'tanggallayanan'=>date('Y-m-d'),
            'iduser'=>$iduser
        ];
        return $this->db->insert('rs_waktu_layanan',$data);
    }
        
    /**
     * update waktu layanan
     * @param type $idpendaftaran
     * @param type $unit
     * @return type
     */
    public function update_waktulayanan($idpendaftaran,$unit)
    {
        $iduser = $this->get_iduserlogin();
        $data = [
            'waktuselesai'=> date('H:i:s'),
            'iduser' => $iduser
        ];
        
        $where = [
            'idpendaftaran'=>$idpendaftaran,
            'unitlayanan'=>$unit
        ];
        return $this->db->update('rs_waktu_layanan',$data,$where);
    }
    
    /**
     * get row id after insert
     * @param type $tableName   nama tabel
     * @param type $callid      callid
     */
    public function getinsert_id($tableName,$callid,$id)
    {
        $result = $this->db->select($id)->get_where($tableName,['callid'=>$callid])->row_array(); 
        $this->db->update($tableName,['callid'=>''],[$id=>$result[$id]]);
        return $result[$id];
    }
    
    //distribusi obat atau barang ke pasien ralan
    public function distribusibarangkepasienrralan($idpendaftaran,$jenisdistribusi)
    {
        $status = (($jenisdistribusi=='keluar') ? 'terlaksana' : 'rencana' );
        $wherestatus = (($jenisdistribusi=='keluar') ? 'rencana' : 'terlaksana' );
        $dtobat = $this->db->get_where('rs_barangpemeriksaan_detail',['status'=>$wherestatus,'idpendaftaran'=>$idpendaftaran,'jumlah !='=>'0.00']);
        if($dtobat->num_rows() > 0)
        {
            $obat = $dtobat->result_array();
            foreach ($obat as $arr)
            {
                $callid = generaterandom(4);
                $data = [
                    'jumlah'=>$arr['jumlah'],
                    'idunit'=>$arr['idunit'],
                    'idbarangpembelian'=>$arr['idbarangpembelian'],
                    'jenisdistribusi'  =>$jenisdistribusi,
                    'statusdistribusi' =>'tidakdiubah',
                    'grupwaktu' => $callid
                ];
                $distribusi = $this->db->insert('rs_barang_distribusi',$data);                
                $idbd = $this->db->select('idbarangdistribusi')->get_where('rs_barang_distribusi',['grupwaktu'=>$callid])->row_array()['idbarangdistribusi'];
                $distribusi = $this->db->update('rs_barang_distribusi',['grupwaktu'=>NULL],['idbarangdistribusi'=>$idbd]);
                $distribusi = $this->db->update('rs_barangpemeriksaan_detail',['status'=>$status,'idbarangdistribusi'=>$idbd],['idbarangpembelian'=>$arr['idbarangpembelian'],'idunit'=>$arr['idunit'],'idbarangpemeriksaan'=>$arr['idbarangpemeriksaan']]);
            }
            return $distribusi;
        }
        return;
    }
    
    //buat qrcode via php
    public function generateciqrcode($data)
    {
        $now = time();
        $this->load->library('ciqrcode');
        $config['imagedir']     = 'assets/images/';
        $config['quality']      = true;
        $config['size']         = '1024';
        $config['black']        = array(224,255,255);
        $config['white']        = array(70,130,180);
        $this->ciqrcode->initialize($config);
        $imgname                = $now.'.png';
        $params['data']         = $data;
        $params['level']        = 'H'; //H=High
        $params['size']         = 10;
        $params['savename']     = FCPATH.$config['imagedir'].$imgname;
        $this->ciqrcode->generate($params);
        $filename = $config['imagedir'].$now.'.png';
        return $filename;
//         unlink('assets/images/tester.png');
    }
    
    public function distribusibarangobatbebas($idbarangpembelibebas,$jenisdistribusi)
    {
        if($this->session->userdata('idunitterpilih'))
        {
            $status = (($jenisdistribusi=='keluar') ? 'terlaksana' : 'rencana' );
            $wherestatus = (($jenisdistribusi=='keluar') ? 'rencana' : 'terlaksana' );
            $dtobat = $this->db->get_where('rs_barang_pembelianbebas_detail',['status'=>$wherestatus,'idbarangpembelibebas'=>$idbarangpembelibebas,'jumlah !='=>'0.00']);
            if($dtobat->num_rows() > 0)
            {
                $obat = $dtobat->result_array();
                foreach ($obat as $arr)
                {
                    $callid = generaterandom(4);
                    $data = [
                        'jumlah'=>$arr['jumlah'],
                        'idunit'=>$arr['idunit'],
                        'idbarangpembelian'=>$arr['idbarangpembelian'],
                        'jenisdistribusi'  =>$jenisdistribusi,
                        'statusdistribusi' =>'tidakdiubah',
                        'grupwaktu' => $callid
                    ];
                    $distribusi = $this->db->insert('rs_barang_distribusi',$data);                
                    $idbd = $this->db->select('idbarangdistribusi')->get_where('rs_barang_distribusi',['grupwaktu'=>$callid])->row_array()['idbarangdistribusi'];
                    $distribusi = $this->db->update('rs_barang_distribusi',['grupwaktu'=>NULL],['idbarangdistribusi'=>$idbd]);
                    $distribusi = $this->db->update('rs_barang_pembelianbebas_detail',['status'=>$status,'idbarangdistribusi'=>$idbd],['idbarangpembelian'=>$arr['idbarangpembelian'],'idunit'=>$arr['idunit'],'idbarangpembelibebas'=>$arr['idbarangpembelibebas']]);
                }
                return $distribusi;
            }
            return;
        }
        return;
    }
    
    public function distribusibarangobatoperasi($idbarangoperasi,$jenisdistribusi)
    {
        $statusbo    = (($jenisdistribusi=='keluar') ? 1 : 0 );
        $status      = (($jenisdistribusi=='keluar') ? 'terlaksana' : 'rencana' );
        $wherestatus = (($jenisdistribusi=='keluar') ? 'rencana' : 'terlaksana' );
        $dtobat = $this->db->get_where('rs_operasi_barangpemeriksaan_detail',['status'=>$wherestatus,'idbarangoperasi'=>$idbarangoperasi,'jumlah !='=>'0.00']);
        if($dtobat->num_rows() > 0)
        {
            $obat = $dtobat->result_array();
            foreach ($obat as $arr)
            {
                $callid = generaterandom(4);
                $data = [
                    'jumlah'=>$arr['jumlah'],
                    'idunit'=>$arr['idunit'],
                    'idbarangpembelian'=>$arr['idbarangpembelian'],
                    'jenisdistribusi'  =>$jenisdistribusi,
                    'statusdistribusi' =>'tidakdiubah',
                    'grupwaktu' => $callid
                ];
                $distribusi = $this->db->insert('rs_barang_distribusi',$data);               
                $idbd = $this->db->select('idbarangdistribusi')->get_where('rs_barang_distribusi',['grupwaktu'=>$callid])->row_array()['idbarangdistribusi'];
                $distribusi = $this->db->update('rs_barang_distribusi',['grupwaktu'=>NULL],['idbarangdistribusi'=>$idbd]);
                $distribusi = $this->db->update('rs_operasi_barangpemeriksaan_detail',['status'=>$status,'idbarangdistribusi'=>$idbd],['idbarangpembelian'=>$arr['idbarangpembelian'],'idunit'=>$arr['idunit'],'idbarangoperasi'=>$idbarangoperasi]);                
                $distribusi = $this->db->update('rs_operasi_barangpemeriksaan',['selesai'=>$statusbo],['idbarangoperasi'=>$idbarangoperasi]);
            }
            return $distribusi;
        }
        return;
    }
    
    public function distribusibarangobatranap($idrencanamedisbarangpemeriksaan,$status)
    {
        $idunit = $this->session->userdata('idunitterpilih');
        if($status =='terlaksana' or $status =='rencana')
        {
            $jenisdistribusi = (($status == 'terlaksana') ? 'keluar' : 'pembetulankeluar' );
            $status      = (($jenisdistribusi=='keluar') ? 'terlaksana' : 'rencana' );
            $wherestatus = (($jenisdistribusi=='keluar') ? 'rencana' : 'terlaksana' );
            $dtobat = $this->db->get_where('rs_inap_rencana_medis_barangpemeriksaan_detail',['status'=>$wherestatus,'idrencanamedisbarangpemeriksaan'=>$idrencanamedisbarangpemeriksaan,'jumlah !='=>'0.00']);
            if($dtobat->num_rows() > 0)
            {
                $obat = $dtobat->result_array();
                foreach ($obat as $arr)
                {
                    $callid = generaterandom(4);
                    $data = [
                        'jumlah'=>$arr['jumlah'],
                        'idunit'=>$arr['idunit'],
                        'idbarangpembelian'=>$arr['idbarangpembelian'],
                        'jenisdistribusi'  =>$jenisdistribusi,
                        'statusdistribusi' =>'tidakdiubah',
                        'grupwaktu' => $callid
                    ];
                    $distribusi = $this->db->insert('rs_barang_distribusi',$data);                
                    $idbd = $this->db->select('idbarangdistribusi')->get_where('rs_barang_distribusi',['grupwaktu'=>$callid])->row_array()['idbarangdistribusi'];
                    $distribusi = $this->db->update('rs_barang_distribusi',['grupwaktu'=>NULL],['idbarangdistribusi'=>$idbd]);
                    $distribusi = $this->db->update('rs_inap_rencana_medis_barangpemeriksaan_detail',['status'=>$status,'idbarangdistribusi'=>$idbd],['idbarangpembelian'=>$arr['idbarangpembelian'],'idunit'=>$arr['idunit'],'idrencanamedisbarangpemeriksaan'=>$idrencanamedisbarangpemeriksaan]);
                }
                return $distribusi;
            }
            return;
        }
    }
    
    //get idbarangpesan depo ke farmasi
    public function getidpesandeporanap($post,$mode)
    {
        $idbpesan = 0;
        if(isset($post['idinap']) && empty(!$post['idinap']))
        {
            $query = $this->db->select('idbarangpemesanan')
                    ->get_where('rs_barang_pemesanan',['idinap'=>$post['idinap'],'statusbarangpemesanan'=>'menunggu','tanggalpemesanan'=>$this->input->post('tanggalfaktur'),'jenistransaksi'=>$mode]);
            if($query->num_rows() > 0)
            {
                $dt = $query->row_array();
                $idbpesan = $dt['idbarangpemesanan']; 
            }
            else
            {
                $idbpesan = 0; 
            }
            return $idbpesan;
        }
        return $idbpesan;
    }
    
    /**
     * Konfigurasi FTP
     * @return boolean
     * mahmud, clear
     */
    public function ftp_upload()
    {
        $this->load->library('ftp');
        $config['hostname'] = HOSTNAMENAS;
        if(file_exists("./QLKP.php")){
            // NAS Storage QLKP
            $config['username'] = 'admin';
            $config['password'] = 'admin';
        } else {
            // NAS Storage QL Jogja
            $config['username'] = 'sitiql';
            $config['password'] = 'adminsirstql2018';
        }
        $config['debug']    = TRUE;
        return $config;
    }
    
    /**
     * Save Log Task Antrean
     * @param type $idjenisloket Id Jenisloket
     * @param type $idantrian Id Antrian
     * @param type $status   Status Antrian
     * @param type $idpemeriksaan Id Pemeriksaan
     * @param type $idpendaftaran Id Pendaftaran
     * mahmud, clear
     */
    public function bridging_save_log_antrean($idjenisloket,$idantrian,$status,$idpemeriksaan,$idpendaftaran)
    {
        //referensi jenis loket di tabel --> antrian_loket_jenis
        //referensi taskid di tabel --> bpjs_bridging_antrean_task
        $save = 0;
        //simpan log admisi
        // Task ID 1- 3
        if($idjenisloket == 1 && $status == 'OK' && empty(!$idpendaftaran))
        {
            
            $dtpendaftaran = $this->db->query("SELECT idpendaftaran FROM `person_pendaftaran` WHERE idpendaftaran = '".$idpendaftaran."' and (idstatuskeluar = 1 or idstatuskeluar = 0 )")->result_array();
            if(empty(!$dtpendaftaran[0]['idpendaftaran']))
            {
                
                $dtantri = $this->db->select('waktuinput,waktusedangproses,waktuok')->get_where('antrian_antrian',['idantrian'=>$idantrian])->row_array();
                $input   = $dtantri['waktuinput'];
                $proses  = $dtantri['waktusedangproses'];
                $selesai = $dtantri['waktuok'];
                // $thistime=date("Y-m-d H:i:s"); // get current time
                $digits = 1;
                    $num= str_pad(rand(15, pow(26, $digits)-1), $digits, '0', STR_PAD_LEFT);
                    $numrand = "-".$num."min";
                    $thistime = date('Y-m-d H:i:s', strtotime($numrand));

                
                //[Andri] disable taskId 1 dan taskId 2
                //$save = $this->db->replace('bpjs_bridging_antrean_task_log',['waktu'=>$input,'idpendaftaran'=>$dtpendaftaran[0]['idpendaftaran'],'taskid'=>1]);
                //$save = $this->db->replace('bpjs_bridging_antrean_task_log',['waktu'=>$proses,'idpendaftaran'=>$dtpendaftaran[0]['idpendaftaran'],'taskid'=>2]);
                //$save = $this->db->replace('bpjs_bridging_antrean_task_log',['waktu'=>$selesai,'idpendaftaran'=>$dtpendaftaran[0]['idpendaftaran'],'taskid'=>3]);
                
                $save = $this->db->replace('bpjs_bridging_antrean_task_log',['waktu'=>$thistime,'idpendaftaran'=>$dtpendaftaran[0]['idpendaftaran'],'taskid'=>3]);
            
                //kirim task id ke server jkn
                $this->bridging_antrol_updatewaktu($idpendaftaran,3);
            }
            else
            {
                $save = 1;
            }
            
        }
        //simpan log poli
        // Task ID 4-5
        if($idjenisloket == 4)
        {
            $taskid = (($status == 'OK') ? 5 : 4 );
            $taskid = (($status == 'BATAL') ? 99 : $taskid );
            if(empty($idpendaftaran) && empty(!$idpemeriksaan))
            {
                $dtperiksa = $this->db->select('rp.idpendaftaran, pp.carabayar')->join('person_pendaftaran pp','pp.idpendaftaran = rp.idpendaftaran')->get_where('rs_pemeriksaan rp',['rp.idpemeriksaan'=>$idpemeriksaan])->row_array();                
                $idpendaftaran = $dtperiksa['idpendaftaran'];
                $save = $this->db->replace('bpjs_bridging_antrean_task_log',['idpendaftaran'=>$idpendaftaran,'taskid'=>$taskid]);

                //kirim antrian onlien ke jkn khusus pasien umum
                // if($taskid == 4 && ($dtperiksa['carabayar'] != 'jknnonpbi' && $dtperiksa['carabayar'] != 'jknpbi' ))
                // {
                //     //kirim antrian ke server jkn
                //     $this->bridging_antrol_add($idpendaftaran);
                //     //kirim task id 1-3 ke server jkn
                //     $this->bridging_antrol_updatewaktu($idpendaftaran,3);
                // }
                //kirim task id 4/5 ke server jkn
                $this->bridging_antrol_updatewaktu($idpendaftaran,$taskid);
                
            }            
        }
        
        //simpan log farmasi
        //Task ID 6-7 / 99
        if($idjenisloket == 5)
        {
            $taskid = (($status == 'OK') ? 7 : 6 );            
            $taskid = (($status == 'BATAL') ? 99 : $taskid );
            $save = $this->db->replace('bpjs_bridging_antrean_task_log',['idpendaftaran'=>$idpendaftaran,'taskid'=>$taskid]);
            
            //kirim task id ke server jkn
            $this->bridging_antrol_updatewaktu($idpendaftaran,$taskid);
        }
        return $save;
    }
    
    /**
     * Bridging Antrian Online
     * kirim antrean oleh user
     * @param type $idpendaftaran
     * mahmud, clear
     */

     //new-save log antrol [nurhi]
    public function bridging_save_log_antrean_taskid($idpemeriksaan,$idpendaftaran,$taskid)
    {
                $dtperiksa = $this->db->select('rp.idpendaftaran, pp.carabayar')->join('person_pendaftaran pp','pp.idpendaftaran = rp.idpendaftaran')->get_where('rs_pemeriksaan rp',['rp.idpemeriksaan'=>$idpemeriksaan])->row_array();                
                $idpendaftaran = $dtperiksa['idpendaftaran'];

                $save = $this->db->replace('bpjs_bridging_antrean_task_log',['idpendaftaran'=>$idpendaftaran,'taskid'=>$taskid]);
                $this->bridging_antrol_updatewaktu($idpendaftaran,$taskid);
                
    }
    public function admisi_bridging_antrol_add($idpendaftaran)
    {
        // change rp.kodebooking to aa.kodebooking
        $sql = "select rp.idpendaftaran, pp.norm, aa.kodebooking, pp.carabayar, rp.noantrian,  pp.idjeniskunjungan, 
        date(pp.waktuperiksa) as tanggalperiksa, pp.ispasienlama, ppas.nojkn, pper.nik, pper.notelpon, 
        ifnull(bbv.nomorreferensi,'') as nomorreferensi, bbv.quota_jkn, bbv.quota_jkn_sisa, bbv.quota_nonjkn, bbv.quota_nonjkn_sisa, 
        fdatetime_to_milisecond(aa.estimasidilayani) as estimasidilayani, 
        ppag.bpjs_kodedokter, namadokter(ppag.idpegawai) as namadokter, ru.kodebpjs as kodeunit,  ru.namaunit,
        rj.jammulai, rj.jamakhir
            from rs_pemeriksaan rp 
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran and (pp.idjeniskunjungan != 5 or pp.idjeniskunjungan is not null)
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
            join person_pegawai ppag on ppag.idpegawai = rp.idpegawaidokter
            join rs_unit ru on ru.idunit = rp.idunit
            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
            join bpjs_bridging_vclaim bbv on bbv.idpendaftaran = pp.idpendaftaran
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            WHERE rp.idpendaftaran = '".$idpendaftaran."' 
                and rp.idpemeriksaansebelum is null 
                and al.bridging_jkn = 1" ; // sql
                // and pp.sudahsingkronjkn = 0"; // sql
        $data = $this->db->query($sql)->result_array();
        if($data)
        {
            // var_dump("data = ".json_encode($data)); die;
            $this->kirim_antrianonline($data);
        }        
        // return $data;
    }
    public function bridging_antrol_add($idpendaftaran)
    {
        // change rp.kodebooking to aa.kodebooking
        $sql = "select rp.idpendaftaran, pp.norm, aa.kodebooking, pp.carabayar, rp.noantrian,  pp.idjeniskunjungan, 
        date(pp.waktuperiksa) as tanggalperiksa, pp.ispasienlama, ppas.nojkn, pper.nik, pper.notelpon, 
        ifnull(bbv.nomorreferensi,'') as nomorreferensi, bbv.quota_jkn, bbv.quota_jkn_sisa, bbv.quota_nonjkn, bbv.quota_nonjkn_sisa, 
        fdatetime_to_milisecond(aa.estimasidilayani) as estimasidilayani, 
        ppag.bpjs_kodedokter, namadokter(ppag.idpegawai) as namadokter, ru.kodebpjs as kodeunit,  ru.namaunit,
        rj.jammulai, rj.jamakhir
            from rs_pemeriksaan rp 
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran and (pp.idjeniskunjungan != 5 or pp.idjeniskunjungan is not null)
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
            join person_pegawai ppag on ppag.idpegawai = rp.idpegawaidokter
            join rs_unit ru on ru.idunit = rp.idunit
            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
            join bpjs_bridging_vclaim bbv on bbv.idpendaftaran = pp.idpendaftaran
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            WHERE rp.idpendaftaran = '".$idpendaftaran."' 
                and rp.idpemeriksaansebelum is null 
                and al.bridging_jkn = 1" ; // sql
                // and pp.sudahsingkronjkn = 0"; // sql
        $data = $this->db->query($sql)->result_array();
        if($data)
        {
            // var_dump("data = ".json_encode($data)); die;
            $this->kirim_antrianonline($data);
        }        
        return $data;
    }
    public function bridging_antrol_add_apm($idpendaftaran)
    {
        // change rp.kodebooking to aa.kodebooking
        $sql = "select rp.idpendaftaran, pp.norm, aa.kodebooking, pp.carabayar, rp.noantrian,  pp.idjeniskunjungan, 
        date(pp.waktuperiksa) as tanggalperiksa, pp.ispasienlama, ppas.nojkn, pper.nik, pper.notelpon, 
        ifnull(bbv.nomorreferensi,'') as nomorreferensi, bbv.quota_jkn, bbv.quota_jkn_sisa, bbv.quota_nonjkn, bbv.quota_nonjkn_sisa, 
        fdatetime_to_milisecond(aa.estimasidilayani) as estimasidilayani, 
        ppag.bpjs_kodedokter, namadokter(ppag.idpegawai) as namadokter, ru.kodebpjs as kodeunit,  ru.namaunit,
        rj.jammulai, rj.jamakhir
            from rs_pemeriksaan rp 
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran 
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
            join person_pegawai ppag on ppag.idpegawai = rp.idpegawaidokter
            join rs_unit ru on ru.idunit = rp.idunit
            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
            join bpjs_bridging_vclaim bbv on bbv.idpendaftaran = pp.idpendaftaran
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            WHERE rp.idpendaftaran = '".$idpendaftaran."' 
                and rp.idpemeriksaansebelum is null 
                and al.bridging_jkn = 1" ; // sql
        $data = $this->db->query($sql)->result_array();
        if($data)
        {
            // var_dump("data = ".json_encode($data)); die;
            $this->kirim_antrianonline($data);
        }        
        return $data;
    }

    /**
     * Bridging Antrian Online 
     * Kirim Antrian Onlien oleh server
     * @return void
     * mahmud, clear
     */
    public function bridging_antrol_kirim_antrian_otomatis()
    {
        // change rp.kodebooking to aa.kodebooking
        $sql = "select rp.idpendaftaran, pp.norm, aa.kodebooking, pp.carabayar, rp.noantrian,  pp.idjeniskunjungan, 
        date(pp.waktuperiksa) as tanggalperiksa, pp.ispasienlama, ppas.nojkn, pper.nik, pper.notelpon, 
        ifnull(bbv.nomorreferensi,'') as nomorreferensi, bbv.quota_jkn, bbv.quota_jkn_sisa, bbv.quota_nonjkn, bbv.quota_nonjkn_sisa, 
        fdatetime_to_milisecond(aa.estimasidilayani) as estimasidilayani, 
        ppag.bpjs_kodedokter, namadokter(ppag.idpegawai) as namadokter, ru.kodebpjs as kodeunit,  ru.namaunit,
        rj.jammulai, rj.jamakhir
            from rs_pemeriksaan rp 
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran and (pp.idjeniskunjungan != 5 or pp.idjeniskunjungan is not null)
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
            join person_pegawai ppag on ppag.idpegawai = rp.idpegawaidokter
            join rs_unit ru on ru.idunit = rp.idunit
            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
            join bpjs_bridging_vclaim bbv on bbv.idpendaftaran = pp.idpendaftaran
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            WHERE rp.idpemeriksaansebelum is null and al.bridging_jkn = 1 and date(rp.waktu) = date(now()) AND pp.sudahsingkronjkn=0"; // sql
        $data = $this->db->query($sql)->result_array();
        $this->kirim_antrianonline($data);
    }

    /**
     * Kirim Antrian Ke Server JKN
     *
     * @param [type] $data
     * @return void
     */
    private function kirim_antrianonline($data)
    {
        foreach( $data as $arr)
        {
            $request = array(
                "kodebooking"=> $arr['kodebooking'],
                "jenispasien"=> (($arr['carabayar'] == "jknpbi" OR $arr['carabayar'] == "jknnonpbi") ? "JKN" : "NON JKN" ),
                "nomorkartu"=> $arr['nojkn'],
                "nik"=> $arr['nik'],
                "nohp"=> $arr['notelpon'],
                "kodepoli"=> $arr['kodeunit'],
                "namapoli"=> $arr['namaunit'],
                "pasienbaru"=> (($arr['ispasienlama'] == 0) ? 1 : 0),
                "norm"=> $arr['norm'],
                "tanggalperiksa"=> $arr['tanggalperiksa'],
                "kodedokter"=> $arr['bpjs_kodedokter'] ,
                "namadokter"=> $arr['namadokter'] ,
                "jampraktek"=> date('H:i',strtotime($arr['jammulai'])).'-'.date('H:i',strtotime($arr['jamakhir'])),
                "jeniskunjungan"=> $arr['idjeniskunjungan'], // --> 1 (Rujukan FKTP), 2 (Rujukan Internal), 3 (Kontrol), 4 (Rujukan Antar RS)
                "nomorreferensi"=> $arr['nomorreferensi'],
                "nomorantrean"=> $arr['noantrian'],
                "angkaantrean"=> $arr['noantrian'],
                "estimasidilayani"=> $arr['estimasidilayani'],
                "sisakuotajkn"=> $arr['quota_jkn_sisa'],
                "kuotajkn"=> $arr['quota_jkn'],
                "sisakuotanonjkn"=> $arr['quota_nonjkn_sisa'],
                "kuotanonjkn"=> $arr['quota_nonjkn'],
                "keterangan"=> "Peserta harap 30 menit lebih awal guna pencatatan administrasi."
            );
            $url        = '/antrean/add';
            $uploadData = json_encode($request);
            $method     = 'POST';
            $response   = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method); 
            //jika berhasil tambah antrean
            if($response['metaData']->code == 200)
            {
                $this->db->update('person_pendaftaran',['sudahsingkronjkn'=>1],['idpendaftaran'=>$arr['idpendaftaran'] ]);
            } 

            //simpan log
            $data_log['idpendaftaran'] = $arr['idpendaftaran'];
            $data_log['code']    = $response['metaData']->code;
            $data_log['message'] = $response['metaData']->message;
            $this->db->insert('bpjs_error_kirimserver_antrian',$data_log);
        }   
    }
    
    /**
     * Bridging Antrian Online -- Update Waktu Antrian
     * @param type $kodebooking
     * @param type $taskid
     * @param type $waktu
     * @param type $idpendaftaran
     */
    public function bridging_antrol_updatewaktu($idpendaftaran,$taskid)
    {
        //ambil dari tabel --> bpjs_bridging_antrean_task_log
        if($taskid == 3)
        {
            // change rp.kodebooking to aa.kodebooking
            $sql = "SELECT aa.kodebooking, fdatetime_to_milisecond(a.waktu) as waktu, a.taskid
            FROM bpjs_bridging_antrean_task_log a
            join rs_pemeriksaan rp on rp.idpendaftaran = a.idpendaftaran and rp.idpemeriksaansebelum is null
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran 
            -- and pp.sudahsingkronjkn = 1 
            and (pp.idjeniskunjungan is not null or pp.idjeniskunjungan !=5)
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            join antrian_antrian aa ON aa.idpemeriksaan=rp.idpemeriksaan
            WHERE a.idpendaftaran = '".$idpendaftaran."' and a.sudahsingkronjkn = 0 and a.taskid in (3,2,1) ORDER by taskid";
        }
        else
        {
            // change rp.kodebooking to aa.kodebooking
            $sql = "SELECT aa.kodebooking, fdatetime_to_milisecond(a.waktu) as waktu, a.taskid
            FROM bpjs_bridging_antrean_task_log a
            join rs_pemeriksaan rp on rp.idpendaftaran = a.idpendaftaran and rp.idpemeriksaansebelum is null            
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran 
            -- and pp.sudahsingkronjkn = 1 
            and (pp.idjeniskunjungan is not null or pp.idjeniskunjungan !=5)
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            join antrian_antrian aa ON aa.idpemeriksaan=rp.idpemeriksaan
            WHERE a.idpendaftaran = '".$idpendaftaran."' and a.sudahsingkronjkn = 0 and a.taskid = '".$taskid."'";
        }

        $dtantrian = $this->db->query($sql)->result_array();
        foreach($dtantrian as $arr)
        {
            
            $request = array(
                "kodebooking" => $arr['kodebooking'],
                "taskid"=>$arr['taskid'],
                "waktu"=>$arr['waktu']
            );
            $url        = '/antrean/updatewaktu';
            $uploadData = json_encode($request);
            $method     = 'POST';
            $response   = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method);

            //jika berhasil update status singkron
            if($response['metaData']->code == 200)
            {
                $this->update_log_kirim_taskid($idpendaftaran,$arr['taskid']);
                // var_dump("</br> meta data 1 = ".$response['metaData']->code);
            }

            //simpan log
            $data_log['idpendaftaran'] = $idpendaftaran;
            $data_log['taskid']  = $arr['taskid'];
            $data_log['code']    = $response['metaData']->code;
            $data_log['message'] = $response['metaData']->message;
            $this->db->insert('bpjs_error_kirimserver_taskid',$data_log);
            // var_dump("</br> meta data 2 = ".$response['metaData']->code);
        }
    }
    


    /**
     * Bridging Antrian Online --> Batal Antrian
     * @param type $kodebooking
     * @param type $keterangan
     * @param type $idpendaftaran
     */
    public function bridging_antrol_batal($keterangan,$idpendaftaran)
    {
        $taskid = 99;
        $dtantrian = $this->db->query("SELECT rp.kodebooking, fdatetime_to_milisecond(a.waktu) as waktu, a.taskid
        FROM bpjs_bridging_antrean_task_log a
        join rs_pemeriksaan rp on rp.idpendaftaran = a.idpendaftaran and rp.idpemeriksaansebelum is null
        WHERE a.idpendaftaran = '".$idpendaftaran."' and a.sudahsingkronjkn = 0 and a.taskid = '".$taskid."'")->result_array();
        
        foreach($dtantrian as $arr)
        {
            $request = array(
                "kodebooking"=> $arr['kodebooking'],
                "keterangan"=> $keterangan
            );
            $url        = '/antrean/batal';
            $uploadData = json_encode($request);
            $method     = 'POST';
            $response   = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method);

            //jika berhasil update status singkron
            if($response['metaData']->code == 200)
            {
                $this->update_log_kirim_taskid($idpendaftaran,$taskid);
            }

            // var_dump($response);
            $data['idpendaftaran'] = $idpendaftaran;
            $data['taskid']  = $taskid;
            $data['code']    = $response['metaData']->code;
            $data['message'] = $response['metaData']->message;
            $this->db->insert('bpjs_error_kirimserver_taskid',$data);
        }
    }

    

    /**
     * Bridging Antrian Onlien Kirim Task ID Ototmatis
     * mahmud, clear
     */
     public function bridging_antrol_kirim_taskid_otomatis()
     {
        $tanggal = date('Y-m-d');
        // change rp.kodebooking to aa.kodebooking
         $sql = "SELECT aa.kodebooking, fdatetime_to_milisecond(a.waktu) as waktu, a.taskid, a.idpendaftaran
         FROM bpjs_bridging_antrean_task_log a
            join rs_pemeriksaan rp on rp.idpendaftaran = a.idpendaftaran             
            join person_pendaftaran pp on pp.idpendaftaran = a.idpendaftaran
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan  
         WHERE  a.sudahsingkronjkn = 0 and date(a.waktu) = '".$tanggal."' and pp.sudahsingkronjkn = 1
         order by a.idpendaftaran DESC, aa.kodebooking ASC, a.taskid ASC limit 200";
         $query = $this->db->query($sql)->result_array();

        //  --  join rs_pemeriksaan rp on rp.idpendaftaran = a.idpendaftaran and rp.idpemeriksaansebelum is null            
        //  --  join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran and (pp.idjeniskunjungan is not null or pp.idjeniskunjungan !=5)
        //  --  join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
        //  --  join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
        foreach($query as $arr)
        {  
            $request = array(
                "kodebooking" => $arr['kodebooking'],
                "taskid"=>$arr['taskid'],
                "waktu"=>$arr['waktu']
            );
            $url        = '/antrean/updatewaktu';
            $uploadData = json_encode($request);
            $method     = 'POST';
            $response   = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method);

            //jika berhasil update status singkron
            if($response['metaData']->code == 200)
            {
                $this->update_log_kirim_taskid($arr['idpendaftaran'],$arr['taskid']);
            }

            //simpan log
            $data_log['idpendaftaran'] = $arr['idpendaftaran'];
            $data_log['taskid']  = $arr['taskid'];
            $data_log['code']    = $response['metaData']->code;
            $data_log['message'] = $response['metaData']->message;
            $this->db->insert('bpjs_error_kirimserver_taskid',$data_log);
        }
         
    }

    /**
     * Update Status Kirim Task ID
     */
    public function update_log_kirim_taskid($idpendaftaran,$taskid)
    {
        $where = [];
        $update= [];
        $where['idpendaftaran'] = $idpendaftaran;
        $where['taskid']        = $taskid;
        $update['sudahsingkronjkn'] = 1;
        $tabel = 'bpjs_bridging_antrean_task_log';
        $this->db->update($tabel,$update,$where);
    }


}

/* End of file ${TM_FILENAME:${1/(.+)/MY_controller.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/MY_controller/:application/core/${1/(.+)/MY_controller.php/}} */