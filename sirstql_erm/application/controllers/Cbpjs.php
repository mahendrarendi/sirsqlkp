<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Cbpjs extends MY_controller
{
    function __construct()
    {
       parent::__construct();
       // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
       $this->load->model('mdatatable');
       $this->load->model('mbpjs');
    }
    
    
    public function bpjs_cari_peserta()
    {
        $nomor = $this->input->get('nomor'); // nomor [nik/nokartu]
        $jenis = $this->input->get('jenis'); // jenis [nik/nokartu]
        $peserta = $this->bpjsbap->requestVclaimPeserta($nomor,$jenis);
        var_dump($peserta);
//        echo json_encode($peserta);
    }
    public function bpjs_referensi()
    {
        $jenis = $this->input->get('jenis'); // jenis referensi
        $peserta = $this->bpjsbap->requestVclaimReferensi('propinsi');
        var_dump($peserta);
//        echo json_encode($peserta);
    }
    
    /**
     * Peserta BPJS
     */
    public function peserta()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJSPESERTA)) //lihat define di atas
        { 
           $data = [
               'content_view'      => 'bpjs/v_peserta',
               'active_menu'       => 'bpjs',
               'active_sub_menu'   => 'bpjs_peserta',
               'active_menu_level' => '',
               'plugins'           => [PLUG_DATE]
              ];

           $data['title_page']     = 'Cari Peserta';
           $data['mode']           = 'view';
           $data['script_js']      = ['bpjs/cari_peserta'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //tampil data peserta
    public function peserta_getdata()
    {
        $nomor = $this->input->post('nomor'); // nomor [nik/nokartu]
        $jenis = $this->input->post('jenis'); // jenis [nik/nokartu]
        
        $url        = "/Peserta/".$jenis."/".$nomor."/tglSEP/".date('Y-m-d');
        $uploadData = '';
        $method     = 'GET';
        $peserta = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        echo json_encode($peserta);
    }
    
    /**
     * SEP 
     * Mahmud, clear
     */
    public function sep()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'pembuatan_sep' );
            $data = [
                'content_view'      => 'bpjs/v_sep',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'bpjs_sep',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
               ];
            
            $data['title_page']     = 'Surat Eligibilitas Peserta';
            $data['mode']           = 'view';
            $data['script_js']      = ['bpjs/vclaim_sep'];
            
            //jika mode pembuatan sep
            if($tabActive == 'pembuatan_sep' or $tabActive == 'pembuatan_sep_manual')
            {
                $data['lakalantas']  = $this->db->get('bpjs_lakalantas_status')->result_array();
                $data['drodownunit'] = $this->db->query("select kodebpjs, idunit, namaunit from rs_unit where kodebpjs !='' ")->result_array();
                $data['plugins'] = [PLUG_DATE, PLUG_CHECKBOX, PLUG_DROPDOWN, PLUG_DATATABLE];
            }
            else
            {
                $data['plugins'] = [PLUG_DATE,  PLUG_DROPDOWN, PLUG_DATATABLE, PLUG_VALIDATION];
            }
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //ambil data sep
    //mahmud, clear
    public function sep_getdata()
    {
        $nosep = $this->input->post('nosep'); // nomorsep
        $nik = $this->input->post('nik'); // NIK
        $notelepon = $this->input->post('notelpon'); // nomor telepon

        $url        = "/SEP/".$nosep;
        $uploadData = '';
        $method     = 'GET';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    
    // cari SIP Dokter
    public function cariSIPDokter()
    {
        $kodeDokterBPJS = $this->input->post('kodeDokterBPJS'); // kode Dokter BPJS
        $SIPDokter = $this->db->query("SELECT * FROM person_pegawai  
                                            WHERE bpjs_kodedokter='".$kodeDokterBPJS."' 
                                            LIMIT 1")->result_array();
        echo json_encode($SIPDokter);        
    }
    
    // simpan Rencana Kontrol/Inap ke Database
    public function simpanRencanaKontrolDatabase()
    {
        $data['tglRencanaKontrol'] = $this->input->post("tglRencanaKontrol");
        $data['namaDokter'] = $this->input->post("namaDokter");
        $data['noKartuBPJS'] = $this->input->post("noKartuBPJS");
        $data['namaPasien'] = $this->input->post("namaPasien");
        $data['kelamin'] = $this->input->post("kelamin");
        $data['tglLahir'] = $this->input->post("tglLahir");
        $data['namaDiagnosa'] = $this->input->post("namaDiagnosa");
        $data['noSEP'] = $this->input->post("noSEP");
        $data['tglSEP'] = $this->input->post("tglSEP");
        $data['noRujukan'] = $this->input->post("noRujukan");
        $data['tglRujukan'] = $this->input->post("tglRujukan");
        $data['spesialis'] = $this->input->post("spesialis");
        $data['tglTerbit'] = $this->input->post("tglTerbit");
        $data['tanggal_cetak'] = $this->input->post("tanggal_cetak");
        $data['diagnosaDPJPManual'] = $this->input->post("diagnosaDPJPManual");
        $data['noSuratKontrol'] = $this->input->post("noSuratKontrol");
        
        $noSuratKontrol = $this->input->post("noSuratKontrol");
        $where['noSuratKontrol'] = $noSuratKontrol;

        // cek dulu noSuratKontrol sudah ada belum di tabel 'bpjs_bridging_rencana_kontrol_inap'
        $cek_noSuratKontrol = $this->db->query("SELECT * FROM bpjs_bridging_rencana_kontrol_inap  
                                                WHERE noSuratKontrol='".$noSuratKontrol."' 
                                                LIMIT 1")->result_array();
                
        if($cek_noSuratKontrol)
        {
            // update table 'bpjs_bridging_rencana_kontrol_inap'
            $save = $this->db->update('bpjs_bridging_rencana_kontrol_inap',$data,$where);
        } else {
            // insert ke table 'bpjs_bridging_rencana_kontrol_inap'
            $save = $this->db->insert('bpjs_bridging_rencana_kontrol_inap',$data);
        }
        pesan_success_danger($save,'Simpan Rencana Kontrol/Inap Berhasil.', 'Simpan Rencana Kontrol/Inap Gagal.', 'js');
    }

    // simpan Tanggal Cetak ke Database
    public function simpanTanggalCetak()
    {
        $data['tanggal_cetak'] = $this->input->post("tanggalCetak");
        $noSuratKontrol = $this->input->post("noSuratKontrol");
        $where['noSuratKontrol'] = $this->input->post("noSuratKontrol");

        // cek dulu noSuratKontrol sudah ada belum di tabel 'bpjs_bridging_rencana_kontrol_inap'
        $cek_noSuratKontrol = $this->db->query("SELECT * FROM bpjs_bridging_rencana_kontrol_inap  
                                                WHERE noSuratKontrol='".$noSuratKontrol."' 
                                                LIMIT 1")->result_array();
                
        if($cek_noSuratKontrol)
        {
            // update table 'bpjs_bridging_rencana_kontrol_inap'
            $save = $this->db->update('bpjs_bridging_rencana_kontrol_inap',$data,$where);
        } else {
            // no action
            $save = "";
        }
        pesan_success_danger($save,'Update tanggal cetak Berhasil.', 'Update tanggal cetak Gagal.', 'js');
    }

    // hapus Rencana Kontrol/Inap dari Database
    public function deleteRencanaKontrolDatabase()
    {
        $noSuratKontrol = $this->input->post("no_surat_kontrol");
        
        // cek dulu noSuratKontrol sudah ada belum di tabel 'bpjs_bridging_rencana_kontrol_inap'
        $cek_noSuratKontrol = $this->db->query("SELECT * FROM bpjs_bridging_rencana_kontrol_inap  
                                                WHERE noSuratKontrol='".$noSuratKontrol."' 
                                                LIMIT 1")->result_array();
        $delete='';    
        if($cek_noSuratKontrol)
        {
            // delete $noSuratKontrol dari table 'bpjs_bridging_rencana_kontrol_inap'
            $delete = $this->db->delete('bpjs_bridging_rencana_kontrol_inap',['noSuratKontrol'=>$noSuratKontrol]);
        }
        pesan_success_danger($save,'Hapus Rencana Kontrol/Inap Berhasil.', 'Hapus Rencana Kontrol/Inap Gagal.', 'js');
    }

    public function searchRekonFromLocal(){
        $noSEP = $this->input->post("noSEP");
        $data = $this->db->query("SELECT * FROM bpjs_bridging_rencana_kontrol_inap  
                                                WHERE noSEP='".$noSEP."' 
                                                LIMIT 1")->result_array();
        // echo json_encode($data);
        if($data){
            echo json_encode($data);
        } else {
            return false;
        }
    }

    
    // simpan SEP ke Database
    public function simpanSEPDatabase()
    {
        
        $data['noKartu'] = $this->input->post("noKartu");
        $data['tglSep'] = $this->input->post("tglSep");
        $data['ppkPelayanan'] = $this->input->post("ppkPelayanan");
        $data['jnsPelayanan'] = $this->input->post("jnsPelayanan");
        $data['klsRawatHak'] = $this->input->post("klsRawatHak");
        $data['klsRawatNaik'] = $this->input->post("klsRawatNaik");
        $data['pembiayaan'] = $this->input->post("pembiayaan");
        $data['penanggungJawab'] = $this->input->post("penanggungJawab");
        $data['noMR'] = $this->input->post("noMR");
        $data['asalRujukan'] = $this->input->post("asalRujukan");
        $data['tglRujukan'] = $this->input->post("tglRujukan");
        $data['noRujukan'] = $this->input->post("noRujukan");
        $data['ppkRujukan'] = $this->input->post("ppkRujukan");
        $data['catatan'] = $this->input->post("catatan");
        $data['diagAwal'] = $this->input->post("diagAwal");
        $data['poliTujuan'] = $this->input->post("tujuan");
        $data['poliEksekutif'] = $this->input->post("eksekutif");
        $data['cob'] = $this->input->post("cob");
        $data['katarak'] = $this->input->post("katarak");
        $data['lakaLantas'] = $this->input->post("lakaLantas");
        $data['noLP'] = $this->input->post("noLP");
        $data['tglKejadian'] = $this->input->post("tglKejadian");
        $data['keterangan'] = $this->input->post("keterangan");
        $data['suplesi'] = $this->input->post("suplesi");
        $data['noSepSuplesi'] = $this->input->post("noSepSuplesi");
        $data['kdPropinsi'] = $this->input->post("kdPropinsi");
        $data['kdKabupaten'] = $this->input->post("kdKabupaten");
        $data['kdKecamatan'] = $this->input->post("kdKecamatan");
        $data['tujuanKunj'] = $this->input->post("tujuanKunj");
        $data['flagProcedure'] = $this->input->post("flagProcedure");
        $data['kdPenunjang'] = $this->input->post("kdPenunjang");
        $data['assesmentPel'] = $this->input->post("assesmentPel");
        $data['noSuratSKDP'] = $this->input->post("noSurat");
        $data['kodeDPJP'] = $this->input->post("kodeDPJP");
        $data['dpjpLayan'] = $this->input->post("dpjpLayan");
        $data['noTelp'] = $this->input->post("noTelp");
        $data['user'] = $this->input->post("user");
        $data['cetakKe'] = $this->input->post("cetakKe");

        $data['noSEP'] = $this->input->post("noSEP");

        $noSEP = $this->input->post("noSEP");
        $where['noSEP'] = $noSEP;

        // cek dulu noSEP sudah ada belum di tabel 'bpjs_bridging_sep'
        $cek_noSEP = $this->db->query("SELECT * FROM bpjs_bridging_sep  
                                                WHERE noSEP='".$noSEP."' 
                                                LIMIT 1")->result_array();
                
        if($cek_noSEP)
        {
            // update table 'bpjs_bridging_sep'
            $save = $this->db->update('bpjs_bridging_sep',$data,$where);
        } else {
            // insert ke table 'bpjs_bridging_sep'
            $save = $this->db->insert('bpjs_bridging_sep',$data);
        }
        pesan_success_danger($save,'Simpan SEP Berhasil.', 'Simpan SEP Gagal.', 'js');
    }

    // hapus SEP dari Database
    public function deleteSEPDatabase()
    {
        $noSEP = $this->input->post("noSEP");
        
        // cek dulu noSEP sudah ada belum di tabel 'bpjs_bridging_sep'
        $cek_noSEP= $this->db->query("SELECT * FROM bpjs_bridging_sep  
                                                 WHERE noSEP='".$noSEP."' 
                                                 LIMIT 1")->result_array();
         $delete='';    
        if($cek_noSEP)
        {
            // delete $noSEP dari table 'bpjs_bridging_sep'
            $delete = $this->db->delete('bpjs_bridging_sep',['noSEP'=>$noSEP]);
        }
        pesan_success_danger($save,'Hapus SEP Berhasil.', 'Hapus SEP Gagal.', 'js');
    }

    //ambil data sep update tgl pulang
    //mahmud, clear
    public  function sep_updatetglpulang_list()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $bulan  = date('n', strtotime($tahunbulan));
        $tahun  = date('Y', strtotime($tahunbulan));
        $filter = $this->input->post('filter');
        
        $url        = "/Sep/updtglplg/list/bulan/".$bulan."/tahun/".$tahun."/".$filter;
        $uploadData = '';
        $method     = 'GET';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    //update tanggal pulang
    //mahmud, clear
    public function sep_updateTglPulang()
    {
        $post = $this->input->post();
        $url        = "/SEP/2.0/updtglplg";
        
        $request = array(
            "request"=> array(
                "t_sep"=> array(
                   "noSep"           => $post['nosep'],
                   "statusPulang"    => $post['status_pulang'],
                   "noSuratMeninggal"=> (($post['status_pulang'] != 4) ? '' : $post['no_surat'] ),
                   "tglMeninggal"    => (($post['status_pulang'] != 4) ? '' : $post['tgl_meninggal']),
                   "tglPulang"       => $post['tgl_pulang'],
                   "noLPManual"      => $post['nolp'],
                   "user"            => "Coba Ws"
                )
            )
        );
        
        $uploadData = json_encode($request);
        $method     = 'PUT';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }

    //hapus sep
    //mahmud, clear
    public function sep_delete()
    {
        $nosep = $this->input->post('nosep'); // nomorsep
        $url        = "/SEP/2.0/delete";
        
        $request = array(
            "request"=> array(
                "t_sep"=> array(
                   "noSep" => $nosep,
                   "user" => "Coba Ws"
                )
            )
        );
        
        $uploadData = json_encode($request);
        $method     = 'DELETE';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }

    //save sep
    //mahmud, clear
    public function sep_create()
    {
        
        $postdata = $this->input->post();
        $jenispelayanan = ((isset($postdata['jenispelayanan']))? $postdata['jenispelayanan'] :2);
        
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));

        $sql        = "SELECT namalengkap FROM person_person a LEFT JOIN login_user b ON b.idperson = a.idperson WHERE b.iduser='".$iduser."'";
        $getrow_user= $this->db->query($sql)->row();

        if( !empty($getrow_user) ){
            $namauser = $getrow_user->namalengkap;
        }else{
            $namauser = 'RSU Queen Latifa Yogyakarta';
        }
 
        
        //request rawat jalan - sep awal
        $request = array(
            "request" => array(
                "t_sep" => array(
                    "noKartu"=> $postdata['nokartu'], //{nokartu BPJS}
                    "tglSep" => $postdata['txtTanggalSep'], //{tanggal penerbitan sep format yyyy-mm-dd}
                    "ppkPelayanan" => BPJS_KODEPPK, //{kode faskes pemberi pelayanan}
                    "jnsPelayanan" => (string)$jenispelayanan, //{jenis pelayanan = 1. r.inap 2. r.jalan}
                    "klsRawat" => array(
                       "klsRawatHak"  => ((isset($postdata['idkelasrawat'])) ? $postdata['idkelasrawat'] : $postdata['idhakkelas'] ), //{sesuai kelas rawat peserta, 1. Kelas 1, 2. Kelas 2, 3. Kelas 3}
                       "klsRawatNaik" => ((isset($postdata['naikkelas'])) ? $postdata['klsRawatNaik'] : "" ), //{diisi jika naik kelas rawat, 1. VVIP, 2. VIP, 3. Kelas 1, 4. Kelas 2, 5. Kelas 3, 6. ICCU, 7. ICU}",
                       "pembiayaan"   => ((isset($postdata['naikkelas'])) ? $postdata['pembiayaan'] : "" ), //{1. Pribadi, 2. Pemberi Kerja, 3. Asuransi Kesehatan Tambahan. diisi jika naik kelas rawat}
                       "penanggungJawab"=> ((isset($postdata['naikkelas'])) ? $postdata['penanggungJawab'] : "" ) //{Contoh: jika pembiayaan 1 maka penanggungJawab=Pribadi. diisi jika naik kelas rawat}
                    ),
                    "noMR"=>$postdata['txtNoMR'],
                    "rujukan"=>array(
                        "asalRujukan"=>$postdata['asalrujukan'], //{asal rujukan ->1.Faskes 1, 2. Faskes 2(RS)}
                        "tglRujukan" =>$postdata['txtTanggalRujukan'], //{tanggal rujukan format: yyyy-mm-dd}
                        "noRujukan"  =>$postdata['txtNorujukan'], //{nomor rujukan}
                        "ppkRujukan" =>$postdata['txtKodeFaskes'] //{kode faskes rujukan -> baca di referensi faskes}
                    ),
                    "catatan" =>$postdata['catatan'], //{catatan peserta}
                    "diagAwal"=>((isset($postdata['diagnosa'])) ? $postdata['diagnosa'] : '' ), //{diagnosa awal ICD10 -> baca di referensi diagnosa}
                    "poli"    =>array(
                        "tujuan"   =>((isset($postdata['kode_poli'])) ? $postdata['kode_poli'] : '' ),//{kode poli -> baca di referensi poli}
                        "eksekutif"=> ((isset($postdata['eksekutif'])) ? $postdata['eksekutif'] : '0') //{poli eksekutif -> 0. Tidak 1.Ya}
                    ),
                    "cob"=>array(
                            "cob"=> ((isset($postdata['txtcob'])) ? $postdata['txtcob'] : '0') ////{cob -> 0.Tidak 1. Ya}
                        ),
                    "katarak"=>array( 
                        "katarak"=> ((isset($postdata['katarak'])) ? $postdata['katarak'] : '0') //{katarak --> 0.Tidak 1.Ya}
                        ),
                    "jaminan"=>array(
                        "lakaLantas"=>$postdata['lakalantas'],    //0 : Bukan Kecelakaan lalu lintas [BKLL], 1 : KLL dan bukan kecelakaan Kerja [BKK], 2 : KLL dan KK, 3 : KK
                        "noLP"      =>$postdata['nolp'],//{No. LP}
                        "penjamin"  =>array(
                            "tglKejadian"=>$postdata['tanggal_kejadian'], //{tanggal kejadian KLL format: yyyy-mm-dd}
                            "keterangan" =>$postdata['keterangan_kll'], //{Keterangan Kejadian KLL}
                            "suplesi"    =>array(
                                "suplesi"       => ((isset($postdata['suplesi'])) ? $postdata['suplesi'] : '0' ), //{Suplesi --> 0.Tidak 1. Ya}
                                "noSepSuplesi"  =>$postdata['nosepsuplesi'], //{No.SEP yang Jika Terdapat Suplesi}
                                "lokasiLaka"    =>array(
                                    "kdPropinsi" =>$postdata['kdPropinsi'],
                                    "kdKabupaten"=>$postdata['kdKabupaten'],
                                    "kdKecamatan"=>$postdata['kdKecamatan']
                                )
                            )
                        )
                    ),
                    "tujuanKunj"    => ((isset($postdata['tujuan_kunjungan'])) ? $postdata['tujuan_kunjungan'] : '0') , //{"0": Normal, "1": Prosedur, "2": Konsul Dokter}
                    "flagProcedure" => ((isset($postdata['flag_procedure'])) ? $postdata['flag_procedure'] : ""), //{"0": Prosedur Tidak Berkelanjutan, "1": Prosedur dan Terapi Berkelanjutan} ==> diisi "" jika tujuanKunj = "0"
                    "kdPenunjang"   => ((isset($postdata['kdpenunjang'])) ? $postdata['kdpenunjang'] :""), // {"1": Radioterapi, "2": Kemoterapi, "3": Rehabilitasi Medik, "4": Rehabilitasi Psikososial,  "5": Transfusi Darah, "6": Pelayanan Gigi, "7": Laboratorium, "8": USG, "9": Farmasi, "10": Lain-Lain, "11": MRI, "12": HEMODIALISA} ==> diisi "" jika tujuanKunj = "0"
                    "assesmentPel"  => ((isset($postdata['assesment_pel'])) ? $postdata['assesment_pel'] :""), //{"1": Poli spesialis tidak tersedia pada hari sebelumnya, "2": Jam Poli telah berakhir pada hari sebelumnya,  "3": Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya,  "4": Atas Instruksi RS} ==> diisi jika tujuanKunj = "2" atau "0" (politujuan beda dengan poli rujukan dan hari beda), "5": Tujuan Kontrol,
                    // "skdp"=>array(
                    //     "noSurat"   => ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" ), //{Nomor Surat Kontrol}
                    //     "kodeDPJP"  => ((isset($postdata['dpjp_kontrol'])) ? $postdata['dpjp_kontrol'] : "" ) //{kode dokter DPJP --> baca di referensi dokter DPJP}
                    // ),
                    "skdp"=>array(
                        "noSurat"   => (!empty($postdata['no_surat_kontrol']) ? $postdata['no_surat_kontrol'] : "" ), //{Nomor Surat Kontrol}
                        "kodeDPJP"  => (!empty($postdata['dpjp_kontrol']) ? $postdata['dpjp_kontrol'] : "" ) //{kode dokter DPJP --> baca di referensi dokter DPJP}
                    ),
                    "dpjpLayan" => ((isset($postdata['dpjp'])) ? $postdata['dpjp'] : "" ), //(diisi "" jika jnsPelayanan = "1" (RANAP),
                    "noTelp"    => $postdata['notelepon'],
                    "user"      => $namauser
                )
            )
        );

        $finger_cek = $this->get_finger_vclaim( $postdata['nokartu'],$postdata['txtTanggalSep'] );
        
        // if( $finger_cek->kode == $this->not_finger() ){
        //     $return = ['metaData'=>['code'=>202,'message'=>$finger_cek->status]];
        //     echo json_encode( $return );
        //     die();
        // }

        $url        = "/SEP/2.0/insert";
        $uploadData = json_encode($request);
        $method     = 'POST';
        $contenType = 'Application/x-www-form-urlencoded';
        // $contenType = "";
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        

        /**
         * jika sep berhasil dibuat dan sep rawat jalan 
         * --> simpan no rujukan/norencana kontrol sebagai noreferensi di tabel bpjs_bridging_vclaim
         * --> simpan norujukan,nosep,norencanakontrol di tabel person_pendaftaran
         * mahmud, clear
         */        
        if($response['metaData']->code == 200 && $jenispelayanan==2)
        {
            $where  = [];
            $where['idpendaftaran'] = $this->input->post('idpendaftaran');

            $dt_bbv = [];
            $dt_bbv['nomorreferensi'] = ((isset($postdata['no_surat_kontrol']) && empty(!$postdata['no_surat_kontrol']) ) ? $postdata['no_surat_kontrol'] : $postdata['txtNorujukan']);
            $this->db->update('bpjs_bridging_vclaim',$dt_bbv,$where);
            //----
            $dt_pp  = [];
            $dt_pp['nosep']     = $response['response']->sep->noSep;
            $dt_pp['norujukan'] = $postdata['txtNorujukan'];
            $dt_pp['nokontrol'] = ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" );
            $this->db->update('person_pendaftaran',$dt_pp,$where);

            //kirim antrian online
            // $this->bridging_antrol_add($this->input->post('idpendaftaran'));
        }


        // [+] Mr.ganteng
        $data_log = [
            'datecreated'=>indonesia_date(),
            'jenispengiriman'=> ( !empty($postdata['no_surat_kontrol']) ? "Kontrol" : "Rujukan" ),
            'usercreate'=> $this->session->userdata('username'),
            'response'=>serialize($response),
            'responsefinger' => serialize($finger_cek),
        ];
        $this->mbpjs->insert_log_bpjs_integration($data_log);
        
        echo json_encode($response);        
    }

    // [Andri]
    // kirim kode booking ke server Antrian Online BPJS
    public function kirim_Antrian_Online()
    {
        $postdata = $this->input->post();
        $norm = $this->input->post('txtNoMR');
        $txtTanggalSep = $this->input->post('txtTanggalSep');

        $idpendaftaran = [];
        $idpendaftaran = $this->db->query("SELECT  rp.idpendaftaran, 
                                                    aa.kodebooking, 
                                                    aa.no,  
                                                    rj.quota_nonjkn, 
                                                    rj.quota_jkn,
                                                    ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
                                                    ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn
                                            FROM antrian_antrian aa
                                            join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
                                            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
                                            join rs_unit ru on ru.idunit = rj.idunit
                                            WHERE rp.norm='".$norm."'
                                                    AND date(waktu)=date('".$txtTanggalSep."')
                                            LIMIT 1")->row_array();

        if($idpendaftaran)
        {
            $myIdPendaftaran = $idpendaftaran['idpendaftaran'];  
            //update person_pendaftaran
            $dt_pp  = [];
            $dt_pp['nosep']     = $postdata['nosep'];
            $dt_pp['norujukan'] = $postdata['txtNorujukan'];
            $dt_pp['nokontrol'] = ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" );
            
            // disable code update
            //$this->db->update('person_pendaftaran',$dt_pp,$idpendaftaran['idpendaftaran']);
            // $this->db->query("UPDATE person_pendaftaran 
            //                     set nosep = '".$postdata['nosep'].", norujukan ='".$postdata['txtNorujukan']."'
            //                     where idpendaftaran = '".$myIdPendaftaran."'");
                                                    
            
            // update / insert table bpjs_bridging_vclaim 
            $vclaim_data = [
                        // 'norujukan_keluar' => $postdata('norujukan_keluar'),
                        'norencanakontrol' => ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" ),
                        // 'kddiagnosa_awal' => $postdata('kddiagnosa_awal'),  // from vclaim
                        // 'diagnosa_awal' => $postdata('diagnosa_awal'),  // from vclaim
                        'nomorreferensi' => ((isset($postdata['no_surat_kontrol']) && empty(!$postdata['no_surat_kontrol']) ) ? $postdata['no_surat_kontrol'] : $postdata['txtNorujukan']),
                        'quota_jkn' => $idpendaftaran['quota_jkn'],
                        'quota_nonjkn' => $idpendaftaran['quota_nonjkn'],
                        'quota_jkn_sisa' => intval($idpendaftaran['quota_jkn']) - intval($idpendaftaran['antrianjkn']),
                        'quota_nonjkn_sisa' => intval($idpendaftaran['quota_nonjkn']) - ( intval($idpendaftaran['totalantrean']) - intval($idpendaftaran['antrianjkn']) )
                    ];  
            $save_SEP  = $this->ql->bpjs_bridging_vclaim_insert_or_update($myIdPendaftaran, $vclaim_data);
            if($save_SEP) // if success
            { 
                //kirim antrian online
                $sendToServer = $this->bridging_antrol_add($myIdPendaftaran);
                // var_dump("sendToServer == ".json_encode($sendToServer));
                // var_dump("|| myIdPendaftaran = ".$myIdPendaftaran);
                // die;
                pesan_success_danger($sendToServer,"SUKSES dikirim ke Server Antrian Online..!","GAGAL dikirim ke Server Antrian Online..!","js");
            }     
        }
        else
        {
            pesan_success_danger(0," ","<h4><strong><center>GAGAL dikirim ke
                                        </br>Server Antrian Online..!!</center></strong></h4>
                                        </br>
                                        <strong><i>Tips : </i></strong>
                                        <ul> 
                                            <li>pastikan No RM terdiri dari 8 digit</li>
                                            <li>cek nomor HP di Admisi >  Data Induk</li>
                                            <li>cek NIK di Admisi > Data Induk</li>
                                            <li>cek No Kartu BPJS di Admisi > Data Induk</li>
                                        </ul>",
                                        "js");
        }

        
    }
    
    //update sep
    //mahmud, clear
    public function sep_update()
    {
        $postdata = $this->input->post();
        
        //request rawat jalan
        $request = array(
            "request" => array(
                "t_sep" => array(
                    "noSep"=> $postdata['nosep'],
                    "klsRawat" => array(
                       "klsRawatHak"  => ((isset($postdata['idkelasrawat'])) ? $postdata['idkelasrawat'] : $postdata['idhakkelas'] ), //{sesuai kelas rawat peserta, 1. Kelas 1, 2. Kelas 2, 3. Kelas 3}
                       "klsRawatNaik" => ((isset($postdata['naikkelas'])) ? $postdata['klsRawatNaik'] : "" ), //{diisi jika naik kelas rawat, 1. VVIP, 2. VIP, 3. Kelas 1, 4. Kelas 2, 5. Kelas 3, 6. ICCU, 7. ICU}",
                       "pembiayaan"   => ((isset($postdata['naikkelas'])) ? $postdata['pembiayaan'] : "" ), //{1. Pribadi, 2. Pemberi Kerja, 3. Asuransi Kesehatan Tambahan. diisi jika naik kelas rawat}
                       "penanggungJawab"=> ((isset($postdata['naikkelas'])) ? $postdata['penanggungJawab'] : "" ) //{Contoh: jika pembiayaan 1 maka penanggungJawab=Pribadi. diisi jika naik kelas rawat}
                    ),
                    "noMR" => $postdata['txtNoMR'],
                    "catatan"=>$postdata['catatan'],
                    "diagAwal"=> ((isset($postdata['diagnosa'])) ? $postdata['diagnosa'] : '' ),
                    "poli"=> array(
                            "tujuan"=> ((isset($postdata['kode_poli'])) ? $postdata['kode_poli'] : '' ),
                            "eksekutif"=> ((isset($postdata['eksekutif'])) ? $postdata['eksekutif'] : 0 )
                    ),
                    "cob"=> array("cob"=> ((isset($postdata['txtcob'])) ? $postdata['txtcob'] : 0 ) ),
                    "katarak"=> array(
                            "katarak"=> ((isset($postdata['katarak'])) ? $postdata['katarak'] : 0 )
                    ),
                    "jaminan" => array(
                            "lakaLantas"=> $postdata['lakalantas'],
                            "penjamin" => array(
                                    "tglKejadian"=> $postdata['tanggal_kejadian'],
                                    "keterangan"=> $postdata['keterangan_kll'],
                                    "suplesi"=> array(
                                            "suplesi"=> ((isset($postdata['suplesi'])) ? $postdata['suplesi'] : 0 ),
                                            "noSepSuplesi"=> $postdata['nosepsuplesi'],
                                            "lokasiLaka"=> array(
                                                    "kdPropinsi"=> $postdata['kdPropinsi'],
                                                    "kdKabupaten"=> $postdata['kdKabupaten'],
                                                    "kdKecamatan"=> $postdata['kdKecamatan']
                                            )
                                    )
                            )
                    ),
                    "dpjpLayan"=>$postdata['dpjp'],
                    "noTelp"=> $postdata['notelepon'],
                    "user"=>"Coba Ws"
                )
            )
        ); 
        
        $url        = "/SEP/2.0/update";
        $uploadData = json_encode($request);
        $method     = 'PUT';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    //pengajuan sep (Sep BackDate atau Finger Print)
    //mahmud, clear
    public function sep_pengajuanSEP()
    {
        $post = $this->input->post();
        $request = array(
           "request" => array(
              "t_sep" => array(
                 "noKartu"=> $post['nokartu'],
                 "tglSep"=> $post['tanggal_sep'],
                 "jnsPelayanan"=> $post['jenis_pelayanan'],
                 "jnsPengajuan"=> $post['jenis_pengajuan'],
                 "keterangan"=> $post['keterangan'],
                 "user"=> "Coba Ws"
               )
            )
        );
        
        $url        = "/Sep/pengajuanSEP";
        $uploadData = json_encode($request);
        $method     = 'POST';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        if($post['jenis_pengajuan'] == 1 OR $response['metaData']->code != 200)
        {
            echo json_encode($response);
        }
        else
        {
            $this->sep_aprovalSEP();
        }
    }
    
    //approvel pengajuan sep finger print
    //mahmud, clear
    public function sep_aprovalSEP()
    {
        $post = $this->input->post();
        $request = array(
           "request" => array(
              "t_sep" => array(
                 "noKartu"=> $post['nokartu'],
                 "tglSep"=> $post['tanggal_sep'],
                 "jnsPelayanan"=> $post['jenis_pelayanan'],
                 "jnsPengajuan"=> $post['jenis_pengajuan'],
                 "keterangan"=> $post['keterangan'],
                 "user"=> "Coba Ws"
               )
            )
        );
        
        $url        = "/Sep/aprovalSEP";
        $uploadData = json_encode($request);
        $method     = 'POST';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    //menampilkan data finger print
    //mahmud, clear
    public function sep_finger_getdata()
    {
        $tglPelayanan  = $this->input->post('tanggal_pelayanan');
        $noKartu       = $this->input->post('nokartu');
        $url = "";
        
        if(empty($noKartu))
        {
            //Get List Finger Print
            $url = "/SEP/FingerPrint/List/Peserta/TglPelayanan/".$tglPelayanan;
        }
        else
        {
            //Get Finger Print
            $url = "/SEP/FingerPrint/Peserta/".$noKartu."/TglPelayanan/".$tglPelayanan;
        }
        $uploadData = '';
        $method     = 'GET';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);        
        echo json_encode($response);
    }
    
    //--Pencarian data SEP Induk Kecelakaan Lalu Lintas
    //mahmud, on dev
    public function sep_datainduk_kll()
    {
        $nokartu    = "0001109221558";//$this->input->post('nokartu');
        $url        = "/sep/KllInduk/List/".$nokartu;
        
        $uploadData = '';
        $contenType = '';        
        $method     = 'GET';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    
    //--Pencarian data potensi SEP Sebagai Suplesi Jasa Raharja
    //mahmud, clear
    public function suplesi_jasa_raharja()
    {
        $noKartu      = $this->input->post('nokartu');
        $tglPelayanan = $this->input->post('tglPelayanan');
        
        $url = "/sep/JasaRaharja/Suplesi/".$noKartu."/tglPelayanan/".$tglPelayanan;
        
        $uploadData = '';
        $contenType = '';        
        $method     = 'GET';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    /**
     * RUJUKAN KELUAR RS BPJS
     */
    public function rujukan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'form_rujukan' );
            $data = [
                'content_view'      => 'bpjs/v_rujukan',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'bpjs_rujukan',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'plugins'           => [PLUG_DATE, PLUG_DATATABLE, PLUG_DROPDOWN]
               ];

            $data['title_page']     = 'Rujukan';
            $data['mode']           = 'view';
            $data['script_js']      = ['bpjs/vclaim_rujukan'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //tampil data rujukan 
    /**
     * Mr. Ganteng
     */
    public function rujukan_getdata()
    {   
        $tglperiksa = $this->input->post('tglperiksa');
        $nomor = $this->input->post('nomor');
        $jenis = $this->input->post('jenis');
        // $asalrujukan = (($this->input->post('asalrujukan') == 1) ? '' : 'RS/' );
        // $url        =  (($jenis == 'Kartu') ? "/Rujukan/Peserta/".$asalrujukan.$nomor : "/Rujukan/".$asalrujukan.$nomor );
        
        if( $this->input->post('asalrujukan') == 1 ){
            $asalrujukan = '';
        }else{
            $asalrujukan = 'RS/';
        }

        if( $jenis == 'Kartu' ){
            // $cek_finger_pasien = $this->get_finger_vclaim( $nomor,$tglperiksa );
            $url = "/Rujukan/Peserta/".$asalrujukan.$nomor;
        }else{
            $url = "/Rujukan/".$asalrujukan.$nomor;
        }


        $uploadData = '';
        $method     = 'GET';
        $peserta    = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        echo json_encode($peserta);
    }

    /**
     * Mr. Ganteng
     */
    public function get_finger_vclaim( $nojkn,$tglpelayanan ){

        $url        = '/SEP/FingerPrint/Peserta/'.$nojkn.'/TglPelayanan/'.$tglpelayanan;

        $uploadData = '';
        $method     = 'GET';
        $finger     = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        
        return $finger['response']; //object
    }

    public function is_finger(){
        return 1;
    }

    public function not_finger(){
        return 0;
    }
    
    //list data rujukan keluar rs
    public function rujukankeluar_list()
    {
        $tglawal  = $this->input->post('tanggal_awal');
        $tglakhir = $this->input->post('tanggal_akhir');
        
        $url        = "/Rujukan/Keluar/List/tglMulai/".$tglawal."/tglAkhir/".$tglakhir;
        $contenType = "Application/x-www-form-urlencoded";
        $method     = "GET";
        $uploadData = "";
        
        $response   = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);

        $rs_keluar_bytanggal = $response['response']->list;

        # tampung object api kedalam array
        $response_new = [];
        foreach( $rs_keluar_bytanggal as $row ){
            $response_new[] = (array) $row; 
        }
            
        # loop tampungan Array
        $rs_keluar_byrujukan = [];
        foreach( $response_new as $rownew ){
            $no_rujukan = $rownew['noRujukan'];

            # panggil api rujukan keluar by norujukan
            $response_byrujukan = $this->bpjsbap->requestVclaim("/Rujukan/Keluar/".$no_rujukan,$uploadData,$method,$contenType);
                
            $rs_keluar_byrujukan['list'][] = $response_byrujukan['response']->rujukan;
        }
        
        $res_api = [
            'metaData' => $response['metaData'],
            'response' => $rs_keluar_byrujukan,
        ]; 
        
        echo json_encode( $res_api );

        // echo json_encode($response);
    }
    
    //insert update rujukan keluar
    public function rujukankeluar_insert()
    {
        $postdata = $this->input->post();
        $request = array(
            "request" => array(
                "t_rujukan" => array(
                    "noSep"=> $postdata['nosep'],
                    "tglRujukan"=> $postdata['tglrujukan'],
                    "tglRencanaKunjungan"=>$postdata['tglrencanakunjungan'],
                    "ppkDirujuk"=> (($postdata['tiperujukan'] == 2) ? $postdata['kodeppkasal'] : $postdata['kodefaskes'] ) , //kode faskes, 8 digit}"
                    "jnsPelayanan"=> $postdata['jenislayanan'], //1-> rawat inap, 2-> rawat jalan
                    "catatan"=> $postdata['catatan'],
                    "diagRujukan"=> ((isset($postdata['kode_diagnosa'])) ? $postdata['kode_diagnosa'] : '' ),//kode diagnosa
                    "tipeRujukan"=> $postdata['tiperujukan'], // 0->Penuh, 1->Partial, 2->balik PRB
                    "poliRujukan"=> (($postdata['tiperujukan'] == 2) ? "" : $postdata['polirujukan'] ), //kosong untuk tipe rujukan 2, harus diisi jika 0 atau 1
                    "user"=> "Coba Ws"
                )
            )
        ); 
        
        $url        = "/Rujukan/2.0/insert";
        $method     = 'POST';
        $uploadData = json_encode($request);
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    //update rujukan keluar
    public function rujukankeluar_update()
    {
        $postdata = $this->input->post();
        $request = array(
            "request" => array(
                "t_rujukan" => array(
                    "noRujukan"=> $postdata['norujukan'],
                    "tglRujukan"=> $postdata['tglrujukan'],
                    "tglRencanaKunjungan"=>$postdata['tglrencanakunjungan'],
                    "ppkDirujuk"=> (($postdata['tiperujukan'] == 2) ? $postdata['kodeppkasal'] : $postdata['kodefaskes'] ), //kode faskes, 8 digit}"
                    "jnsPelayanan"=> $postdata['jenislayanan'], //1-> rawat inap, 2-> rawat jalan
                    "catatan"=> $postdata['catatan'],
                    "diagRujukan"=> ((isset($postdata['kode_diagnosa'])) ? $postdata['kode_diagnosa'] : '' ),//kode diagnosa
                    "tipeRujukan"=> $postdata['tiperujukan'], // 0->Penuh, 1->Partial, 2->balik PRB
                    "poliRujukan"=> (($postdata['tiperujukan'] == 2) ? "" : $postdata['polirujukan'] ), //kosong untuk tipe rujukan 2, harus diisi jika 0 atau 1
                    "user"=> "Coba Ws"
                )
            )
        ); 
        
        $url        = "/Rujukan/2.0/Update" ;
        $method     = 'PUT';
        $uploadData = json_encode($request);
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    
    /**
     * monitoring
     * mahmud, clear
     */
    public function monitoring()
    {
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
           $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'data_kunjungan' );
           $data = [
               'content_view'      => 'bpjs/v_monitoring',
               'active_menu'       => 'bpjs',
               'active_sub_menu'   => 'bpjs_monitoring',
               'active_menu_level' => '',
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE],
               'tabActive'         => $tabActive,
              ];

           $data['title_page']     = 'Monitoring';
           $data['mode']           = 'view';
           $data['script_js']      = ['bpjs/vclaim_monitoring'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //tampil data monitoring
    //mahmud, clear
    public function monitoring_getdata()
    {
        $param1 = $this->input->post('param1');
        $param2 = $this->input->post('param2');
        $param3 = $this->input->post('param3');
        
        $mode = $this->input->post('mode');
        $peserta = array();
        
        $url = '';
        if($mode == 'data_kunjungan')
        {
            $url = "/Monitoring/Kunjungan/Tanggal/".$param1."/JnsPelayanan/".$param2;
        }
        else if($mode == 'data_klaim')
        {
            $url = "/Monitoring/Klaim/Tanggal/".$param1."/JnsPelayanan/".$param2."/Status/".$param3;
        }
        else if($mode == 'data_histori')
        {
            $url = "/monitoring/HistoriPelayanan/NoKartu/".$param1."/tglMulai/".$param2."/tglAkhir/".$param3;
        }
        else
        {
            $url = "/monitoring/JasaRaharja/JnsPelayanan/".$param1."/tglMulai/".$param2."/tglAkhir/".$param3;
        }
        
        $uploadData = '';
        $method     = 'GET';
        $peserta = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        echo json_encode($peserta);
    }
    
    //tampil data klaim
    //mahmud, clear
    public function klaim_getdata()
    {
        $nosep = $this->input->post('nosep'); // nomorsep
        $jenis = $this->input->post('jenis'); // jenis pencarian
        $jnsCari = (($jenis == 1) ? 'Internal/' : '' );
        
        
        $url        = "/SEP/".$jnsCari.$nosep;
        $uploadData = '';
        $method     = 'GET';
        $contenType = 'Application/x-www-form-urlencoded';
        $peserta = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($peserta);
    }
    
    //pembuatan rujuk balik prb
    //on dev
    public function rujukbalik()
    {
        $get = $this->input->get();
        
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'form_prb' );
            $data = [
               'content_view'      => 'bpjs/v_prb',
               'active_menu'       => 'bpjs',
               'active_sub_menu'   => 'bpjs_prb',
               'active_menu_level' => '',
               'tabActive'         => $tabActive,
               'plugins'           => [PLUG_DATE, PLUG_DATATABLE]
              ];

           $data['title_page']     = 'Rujuk Balik (PRB)';
           $data['mode']           = 'view';
           $data['script_js']      = ['bpjs/vclaim_prb'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function rujukbalik_getdata()
    {
        $jenis = $this->input->post('jenis'); // jenis metode pencarian [tanggal PRB/noSRB]        
        if($jenis == 'tanggalsrb')
        {
            $url = '/prb/tglMulai/'.$this->input->post('tanggal_mulai').'/tglAkhir/'.$this->input->post('tanggal_akhir');
        }
        else //jenis noSRB
        {
            $url = '/prb/'.$this->input->post('nomorsrb').'/nosep/'.$this->input->post('nosep');
        }
        
        $uploadData = '';
        $method     = 'GET';
        $peserta = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        echo json_encode($peserta);
    }
    
    //buat rencana kontrol
    public function rujukbalik_insert()
    {
        $postdata = $this->input->post(); 
        
        // Get Array from data
        $obat = array();
        foreach($postdata['kodeObat'] as $x => $val) {
            //ambil data tiap row
            $row = array();
            $row['idUniqueObat'] = $postdata['idUniqueObat'][$x];
            $row['kdObat'] = $postdata['kodeObat'][$x];
            $row['namaObat'] = $postdata['namaObat'][$x];
            $row['signa1'] = $postdata['txtsigna1'][$x];
            $row['signa2'] = $postdata['txtsigna2'][$x];
            $row['jmlObat'] = $postdata['jumlahObat'][$x];
            $obat[] = $row;
        }
        

        $url  = "/PRB/insert";
        $request = array(
            "request" => array(
                "t_prb"=> array(
                    "noSep" => $postdata['txtnosep'],
                    "noKartu" => $postdata['nokartu'],
                    "alamat" => $postdata['txtalamat'],
                    "email" => $postdata['txtemail'], 
                    "programPRB" => $postdata['idprogramprb'],//"03",//
                    "kodeDPJP" => $postdata['kddpjp'], //"34371",//
                    "keterangan" => $postdata['txtdeskripsi'], //"Kecapekan kerja", // 
                    "saran" => $postdata['txtsaran'], //"Pasien harus olahraga bersama setiap minggu dan cuti, edukasi agar jangan disuruh kerja terus, lama lama stress.", // 
                    "user" => "1234567",//$postdata['txtuser'], 
                    "obat"=> $obat
                    ) 
                )
        );            
    
        $uploadData = json_encode($request);
        $method     = 'POST';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
        // untuk cek respon array
        //$response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        //echo json_encode($request);
    }
    //update rujukbalik
    public function rujukbalik_update()
    {
        $postdata = $this->input->post(); 
        $obat = array();
        foreach($postdata['kodeObat'] as $x => $val) {
            //ambil data tiap row
            $row = array();
//            $row['idUniqueObat'] = $postdata['idUniqueObat'][$x];
            $row['kdObat'] = $postdata['kodeObat'][$x];
            $row['nmObat'] = $postdata['namaObat'][$x];
            $row['signa1'] = $postdata['txtsigna1'][$x];
            $row['signa2'] = $postdata['txtsigna2'][$x];
            $row['jmlObat'] = $postdata['jumlahObat'][$x];
            $obat[] = $row;
        }
        $postdata = $this->input->post();
        $request = array(
            "request" => array(
                "t_prb"=> array(
                    "noSrb" => $postdata['txtnosrb'],
                    "noSep" => $postdata['txtnosep'],
                    "alamat" => $postdata['txtalamat'],
                    "email" => $postdata['txtemail'], 
                    "kodeDPJP" => $postdata['kddpjp'], //"34371",//
                    "keterangan" => $postdata['txtdeskripsi'], //"Kecapekan kerja", // 
                    "saran" => $postdata['txtsaran'], //"Pasien harus olahraga bersama setiap minggu dan cuti, edukasi agar jangan disuruh kerja terus, lama lama stress.", // 
                    "user" => "1234567",//$postdata['txtuser'], 
                    "obat"=> $obat
                )
            )
        ); 
        
        $url        = "/PRB/Update" ;
        $method     = 'PUT';
        $uploadData = json_encode($request);
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }

    //hapus rujukbalik
    public function rujukbalik_delete()
    {
        $request = array(
            "request"=> array(
                "t_prb"=> array(
                    "noSrb"=> $this->input->post('txtnosrb'),
                    "noSep" => $this->input->post('txtnosep'),
                    "user" => "1234567" //$postdata['txtuser']
                )
            )
        );
        
        $url        = '/PRB/Delete';
        $uploadData = json_encode($request);
        $method     = 'DELETE';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    
    
    //referensi
    public function referensi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
           $data = [
               'content_view'      => 'bpjs/v_referensi',
               'active_menu'       => 'bpjs',
               'active_sub_menu'   => 'bpjs_ref',
               'active_menu_level' => '',
               'plugins'           => [PLUG_DATATABLE,PLUG_DROPDOWN, PLUG_DATE]
              ];

           $data['title_page']     = 'Referensi';
           $data['mode']           = 'view';
           $data['ref']            = $this->db->get('bpjs_vclaim_referensi')->result_array();
           $data['script_js']      = ['bpjs/vclaim_referensi'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //menampilkan referensi
    //mahmud, clear
    public function referensi_getdata()
    {
        $kdref = $this->input->post('kode');
        $param1= $this->input->post('parameter1');
        $param2= $this->input->post('parameter2');
        $param3= $this->input->post('parameter3');
        
        $get_ref  = $this->db->get_where('bpjs_vclaim_referensi',['kode'=>$kdref])->row_array();        
        $url      = $get_ref['url'];
        
        if($kdref == 'diagnosa' or $kdref == 'poli' or $kdref == 'procedure' or $kdref == 'dokter' or $kdref == 'kabupaten')
        {
            $url = $get_ref['url'].$param1;
        }
        else if($kdref == 'faskes')
        {
            $url = $get_ref['url'].$param1.'/'.$param2;
        }
        else if($kdref == 'dpjp')
        {
            $param2 = ((empty($param2)) ? date('Y-m-d') : $param2 );
            $param1 = ((empty($param1)) ? 1 : $param1 );
            $url = $get_ref['url'].$param1.'/tglPelayanan/'.$param2.'/Spesialis/'.$param3;
        }        
        
        $method   = 'GET';
        $uploadData = '';
        $referensi  = $this->bpjsbap->requestVclaim($url,$uploadData,$method);
        
        if($referensi['metaData']->code == 200)
        {
            $response = [];
            $row  = '';           
            $data = '';
            if($kdref == 'diagnosa' or $kdref == 'poli' or $kdref == 'faskes' or $kdref == 'procedure' or $kdref == 'dokter')
            {
                $data = $referensi['response']->$kdref;
            }
            else
            {
                $data = $referensi['response']->list;
            }
            
            
            $list = '';
            foreach ($data as $obj)
            {
                $list .= '<tr><td>'.$obj->kode.'</td><td>'.$obj->nama.'</td></tr>';
            }
            $row .= '<thead><tr class="header-table-ql"><th>Kode</th><th>Nama</th></tr></thead><tbody>'.$list.'</tfoot>';
            
            $response['metaData'] = $referensi['metaData'];
            $response['table'] = '<table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%" id="dttable">'.$row.'</table>';
            
            echo json_encode($response);
        }
        else
        {            
            echo json_encode($referensi);
        }
    }
    
    //cari referensi data saat bridging berjalan
    //mahmud, clear
    public function referensi_getdata_()
    {
        $method     = 'GET';
        $uploadData = '';
        $url        = '';
        $contenType = '';
        
        $moderef = $this->input->post('moderef');
        switch ($moderef) 
        {
            case 'propinsi':
                    $url = '/referensi/propinsi';
                break;
            case 'kabupaten':
                    $url = '/referensi/kabupaten/propinsi/'.$this->input->post('kd_propinsi');
                break;
            case 'kecamatan':
                    $url = '/referensi/kecamatan/kabupaten/'.$this->input->post('kd_kabupaten');
                break;
            case 'dpjp':
                    $post = $this->input->post();
                    $pelayanan = ((isset($post['jenispelayanan'])) ? $post['jenispelayanan'] : 2 );
                    $url = '/referensi/dokter/pelayanan/'.$pelayanan.'/tglPelayanan/'.$this->input->post('tanggalsep').'/Spesialis/'.$this->input->post('kodepoli');
                break;
            
            case 'faskes':
                    $param1 = $this->input->post('param1');
                    $param2 = $this->input->post('param2');
                    $url = '/referensi/faskes/'.$param1.'/'.$param2;
                break;
            case 'diagnosa':
                    $url = '/referensi/diagnosa/'.$this->input->post('param');
                break;
            case 'rencanakontrol_carisep':
                    $url = '/RencanaKontrol/nosep/'.$this->input->post('param');
                break;
            case 'rencanakontrol_caripoli':
                    $param1 = $this->input->post('param1');
                    $param2 = $this->input->post('param2');
                    $param3 = $this->input->post('param3');

                    $url    = 'RencanaKontrol/ListSpesialistik/JnsKontrol/'.$param1.'/nomor/'.$param2.'/TglRencanaKontrol/'.$param3;
                break;
            case 'rencanakontrol_caridokter':
                    /*Content-Type: Application/x-www-form-urlencoded
                    Parameter 1: Jenis kontrol --> 1: SPRI, 2: Rencana Kontrol
                    Parameter 2: Kode poli
                    Parameter 3: Tanggal rencana kontrol --> format yyyy-MM-dd*/


                    $param1 = $this->input->post('param1');
                    $param2 = $this->input->post('param2');
                    $param3 = $this->input->post('param3');
                    $contenType = 'Application/x-www-form-urlencoded';
                    $url = '/RencanaKontrol/JadwalPraktekDokter/JnsKontrol/'.$param1.'/KdPoli/'.$param2.'/TglRencanaKontrol/'.$param3;
                break;
            case 'cari_norencanakontrol':
                    /*Content-Type: Application/x-www-form-urlencoded
                     * Parameter: Nomor Surat Kontrol Peserta*/            

                    $param1 = $this->input->post('param');
                    $contenType = 'Application/x-www-form-urlencoded';
                    $url = '/RencanaKontrol/noSuratKontrol/'.$param1;
                break;
            case 'cari_poli':
                    $param1 = $this->input->post('param1');
                    $url = '/referensi/poli/'.$param1;
                break;
            case 'cari_peserta_bykartu':                
                    $param1 = $this->input->post('param1');
                    $param2 = $this->input->post('param2');
                    $url = '/Peserta/nokartu/'.$param1.'/tglSEP/'.$param2;
                break;
            case 'cari_programprb': //Pencarian data diagnosa program PRB
                    $url = '/referensi/diagnosaprb';
                break;
            case 'cari_obatprb': // Search by "Obat PRB"
                    $param1 = $this->input->post('param1');
                    $url = '/referensi/obatprb/'.$param1;
                break;
            case 'cari_dataspesialistik': // Cari Data Spesialistik
                    $ppkrujuk   = $this->input->post('ppkrujuk');
                    $tglrujukan = $this->input->post('tglrujukan');
                    $contenType = 'Application/x-www-form-urlencoded';
                    $url        = '/Rujukan/ListSpesialistik/PPKRujukan/'.$ppkrujuk.'/TglRujukan/'.$tglrujukan; 
                break;
            
            case 'cari_datasarana' : //Cari Data Sarana PPK
                    $ppkrujuk   = $this->input->post('ppkrujuk');
                    $contenType = 'Application/x-www-form-urlencoded';
                    $url        = '/Rujukan/ListSarana/PPKRujukan/'.$ppkrujuk; 
                break;
            case 'cari_rujukankeluar' : //Get Data Detail Rujukan Keluar RS Berdasarkan Nomor Rujukan
                    $norujukan  = $this->input->post('norujukan');
                    $contenType = 'Application/x-www-form-urlencoded';
                    $url        = '/Rujukan/Keluar/'.$norujukan; 
                break;                
            case 'cari_SRB_SEP' : //Get Data Detail by SRB_SEP
                    $noSRB = $this->input->post('noSRB');
                    $noSEP = $this->input->post('noSEP');
                    $url        = '/prb/'.$noSRB.'/nosep/'.$noSEP; 
                break;                  
            default:
                break;
        }
        
        $response  = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    
    
    /**
     * Rencana Kontrol
     */
    public function rencanakontrol()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'rencana_kontrol' );
            $data = [
                'content_view'      => 'bpjs/v_rencanakontrol',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'bpjs_rencanakontrol',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'plugins'           => [PLUG_DATE, PLUG_CHECKBOX, PLUG_DROPDOWN, PLUG_DATATABLE]
               ];
            
            $data['title_page']     = 'Rencana Kontrol/Rencana Inap';
            $data['mode']           = 'view';
            $data['script_js']      = ['bpjs/vclaim_rencanakontrol'];
            
            //jika mode pembuatan sep
            if($tabActive == 'pembuatan_sep')
            {
                $data['lakalantas']  = $this->db->get('bpjs_lakalantas_status')->result_array();
                $data['drodownunit'] = $this->db->query("select kodebpjs, idunit, namaunit from rs_unit where kodebpjs !='' ")->result_array();
            }
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //list rencana kontrol
    public function rencanakontrol_list()
    {
        $param1 = $this->input->post('tanggal_awal');        
        $param2 = $this->input->post('tanggal_akhir');
        $param3 = $this->input->post('filter_tanggal');
        $url        = "/RencanaKontrol/ListRencanaKontrol/tglAwal/".$param1."/tglAkhir/".$param2."/filter/".$param3;
        $uploadData = "";
        $method     = 'GET';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    
    //buat rencana kontrol
    public function rencanakontrol_insert()
    {
        $postdata = $this->input->post();
        $url = "";
        $request = array();
        
        //insert spri
        if($postdata['pelayanan'] == 1)
        {
            $url  = "/RencanaKontrol/InsertSPRI";
            $request = array(
                "request"=> array(
                    "noKartu"=>$postdata['nokartu'],
                    "kodeDokter"=>$postdata['dpjp'],
                    "poliKontrol"=>str_replace(' ', '', $postdata['kode_poli']),
                    "tglRencanaKontrol"=>$postdata['tanggal_rencana_kontrol'],
                    "user"=>"coba ws"
                )
            );
            
        }
        else // insert kontrol
        {
            $url  = "/RencanaKontrol/insert";
            $request = array(
                "request" =>  array(
                    "noSEP"=>$postdata['nosep'],//"{nomor SEP}",
                    "kodeDokter"=>$postdata['dpjp'],//"{kode dokter}",
                    "poliKontrol"=> str_replace(' ', '', $postdata['kode_poli']),//"{kode poli}",
                    "tglRencanaKontrol"=>$postdata['tanggal_rencana_kontrol'],//"{Rawat Jalan: diisi tanggal rencana kontrol, format: yyyy-MM-dd. Rawat Inap: diisi tanggal SPRI, format: yyyy-MM-dd}",
                    "user"=>'coba ws',//"{user pembuat rencana kontrol}"
                )
            );
        }
        
        $uploadData = json_encode($request);
        $method     = 'POST';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        echo json_encode($response);
    }
    
    //update rencana kontrol
    public function rencanakontrol_update()
    {
        $postdata = $this->input->post();
        $request = array(
            "request" =>  array(
                "noSuratKontrol" => $postdata['no_surat_kontrol'],
                "noSEP"=>$postdata['nosep'],//"{nomor SEP}",
                "kodeDokter"=>$postdata['dpjp'],//"{kode dokter}",
                "poliKontrol"=> str_replace(' ', '', $postdata['kode_poli']),//"{kode poli}",
                "tglRencanaKontrol"=>$postdata['tanggal_rencana_kontrol'],//"{Rawat Jalan: diisi tanggal rencana kontrol, format: yyyy-MM-dd. Rawat Inap: diisi tanggal SPRI, format: yyyy-MM-dd}",
                "user"=>'coba ws',//"{user pembuat rencana kontrol}"
            )
        );
        
        $url        = "/RencanaKontrol/Update";
        $uploadData = json_encode($request);
        $method     = 'PUT';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    //hapus rencana kontrol
    public function rencanakontrol_delete()
    {
        $request = array(
            "request"=> array(
                "t_suratkontrol"=> array(
                "noSuratKontrol"=> $this->input->post('no_surat_kontrol'),
                "user"=> "coba ws"
                )
            )
        );
        
        $url        = '/RencanaKontrol/Delete';
        $uploadData = json_encode($request);
        $method     = 'DELETE';
        $contenType = 'Application/x-www-form-urlencoded';
        $response = $this->bpjsbap->requestVclaim($url,$uploadData,$method,$contenType);
        
        echo json_encode($response);
    }
    
    /**
     * Bridging Applicare
     * mahmud, clear
     */
    public function settingaplicare()
    {
        return [    'content_view'      => 'masterdata/v_aplicare',
                    'active_menu'       => 'bpjs',
                    'active_sub_menu'   => 'bpjs',
                    'active_menu_level' => 'aplicare'
               ];
    }
    
    public function aplicare()
    {
	if ($this->pageaccessrightbap->checkAccessRight(V_APLICARE)) //lihat define di atas
        {
            $data                = $this->settingaplicare();
            $data['title_page']  = 'Aplicare';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Aplicare';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['script_js']   = ['js_aplicare'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    } 
    
    // add SEP
    public function add_SEP()
    {
        $idpendaftaran = $this->input->post('idpendaftaran');
        $vclaim_data = [
                    //'idpendaftaran' => $this->input->post('idpendaftaran'),
                    'norujukan_keluar' => $this->input->post('norujukan_keluar'),
                    'norencanakontrol' => $this->input->post('norencanakontrol'),
                    'kddiagnosa_awal' => $this->input->post('kddiagnosa_awal'),  // from vclaim
                    'diagnosa_awal' => $this->input->post('diagnosa_awal'),  // from vclaim
                    'nomorreferensi' => $this->input->post('nomorreferensi') // source ??
                    ];        
        $save_SEP  = $this->ql->bpjs_bridging_vclaim_insert_or_update($idpendaftaran, $vclaim_data);
        pesan_success_danger($save_SEP,"Berkas berhasil di update..!","Berkas gagal di update..!","js");
       
    }
        
    // get Data From Database
    public function getDataFromDatabase()
    {
        $idpendaftaran = $this->input->post('nomorRujukan');
        $query = $this->db->query("select * 
                                    FROM bpjs_bridging_rencana_kontrol_inap 
                                    WHERE noSuratKontrol='$idpendaftaran'")->result();
        if($query){
            $query['metaData']['code']='200';
        } else {
            $query['metaData']['code']='201';
            $query['metaData']['message']='No Surat Kontrol/Inap '.$idpendaftaran.' tidak ditemukan di database sitiQL';
        }
        echo json_encode($query);  
    }
        
    /**
     * ***********************************
     * Bridging Antrian Online Mobile JKN
     */

    // Setting for antrian online (Antrol)
    public function antrol()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'referensi_poli' );
            $data = [
                'active_menu'       => 'bpjs',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'mode'              => 'view',
               ];
            
            $data['title_page']         = 'Referensi';
            $data['active_sub_menu']    = 'antrol_referensi';
            $data['content_view']       = 'bpjs/v_antrol_referensi';
            $data['script_js']          = ['bpjs/antrol_referensi'];

            $data['plugins']            = [PLUG_DATE,  PLUG_DROPDOWN, PLUG_DATATABLE, PLUG_VALIDATION];
           
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }


    public function antrol_dashboard()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        { 
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'dashboardPerBulan' );
            $data = [
                'content_view'      => 'bpjs/v_antrol_dashboard',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'antrol_dashboard',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'title_page'        => 'Dashboard Antrian Online',
                'mode'              => 'view'
               ];
            
            
            $data['script_js']      = ['bpjs/antrol_dashboard'];
            $data['plugins'] = [PLUG_CHART, PLUG_DATE,  PLUG_DROPDOWN, PLUG_DATATABLE, PLUG_VALIDATION];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    /**
     * Melihat referensi jadwal dokter yang ada pada Aplikasi HFIS
     */
    public function antrol_jadwal_dokter()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        {   
            $data = [
                'content_view'      => 'bpjs/v_antrol_jadwal_dokter',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'antrol_jadwaldokter',
                'active_menu_level' => '',
                'title_page'        => 'Jadwal Dokter',
                'mode'              => 'view',
                'poli'              => $this->db->select('kodebpjs, namaunit')->get_where('rs_unit',"kodebpjs != ''")->result_array()
               ];

            $data['script_js']  = ['bpjs/antrol_jadwal_dokter'];
            $data['plugins']    = [PLUG_DATE, PLUG_DATATABLE, PLUG_TIME];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     *  Update jadwal dokter yang ada pada Aplikasi HFIS
     */
    public function antrol_jadwal_dokter_update()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        {   
            
            $kdpoli    = $this->input->post('kodepoli');
            $kdsubpoli = $this->input->post('kodesubpoli');
            $kddokter= $this->input->post('kodedokter');
            $hari    = $this->input->post('hari');

            $mulai   = date('H:i', strtotime($this->input->post('mulai')));
            $selesai = date('H:i', strtotime($this->input->post('selesai')));

            $request = array(
                "kodepoli"=> $kdpoli,
                "kodesubspesialis"=> $kdsubpoli,
                "kodedokter"=> $kddokter,
                    "jadwal"=> array(
                        array(
                            "hari"  => $hari,
                            "buka"  => $mulai,
                            "tutup" => $selesai
                        )
                    )
            );
            $uploadData = json_encode($request);
            $url        = "/jadwaldokter/updatejadwaldokter";
            $method     = "POST";
            $response = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method);
            echo json_encode($response);
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     * Referensi 
     */
    public function antrol_referensi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        {
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'referensi_poli' );
            $data = [
                'content_view'      => 'bpjs/v_antrol_referensi',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'antrol_referensi',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'title_page'        => 'Referensi',
                'mode'              => 'view',
               ];
               
            $data['script_js']  = ['bpjs/antrol_referensi'];
            $data['plugins']    = [PLUG_DATE,  PLUG_DROPDOWN, PLUG_DATATABLE, PLUG_VALIDATION];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     * Log 
     */
    public function antrol_log()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BPJS)) //lihat define di atas
        {
            $get = $this->input->get();
            
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'log_taskId' );
            $data = [
                'content_view'      => 'bpjs/v_antrol_log',
                'active_menu'       => 'bpjs',
                'active_sub_menu'   => 'antrol_log',
                'active_menu_level' => '',
                'tabActive'         => $tabActive,
                'title_page'        => 'Log Antrian Online',
                'mode'              => 'view',
               ];
               
            $data['script_js']  = ['bpjs/antrol_log'];
            $data['plugins']    = [PLUG_DATE,  PLUG_DROPDOWN, PLUG_DATATABLE, PLUG_VALIDATION];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * Bridging Antrian Online -- Get Data
     */
    public function antrol_getdata()
    {
        $mode = $this->input->post('mode');
        $response= array();
        $request = '';        
        $method  = 'GET';
        $url     = '';
        
        if($mode == 'dashboardPerBulan')
        {
            //Dashboard waktu per bulan
            $waktuAntrol = $this->input->post('waktuAntrol');
            $bulanAntrol  = date('m', strtotime($waktuAntrol)); //'04';//
            $tahunAntrol = date('Y', strtotime($waktuAntrol)); //'2022';//
            $jenisWaktu = $this->input->post('jenisWaktu');
            
            $url = '/dashboard/waktutunggu/bulan/'.$bulanAntrol.'/tahun/'.$tahunAntrol.'/waktu/'.$jenisWaktu;
            $uploadData = $request;
            $response = $this->bpjsbap->requestBpjsAntrolDashboard($url,$uploadData,$method);
            echo json_encode($response);
            // echo $response;
            return;
            // $response = array(
            //     "metadata" => array(
            //        "code"    => 200,
            //        "message" => "OK"
            //     ),
            //     "response" => array(
            //        "list" => array (
            //             array(
            //                 "kdppk"=> "1311R002",
            //                 "waktu_task1"=> 0,
            //                 "avg_waktu_task4"=> 0,
            //                 "jumlah_antrean"=> 1,
            //                 "avg_waktu_task3"=> 0,
            //                 "namapoli"=> "BEDAH",
            //                 "avg_waktu_task6"=> 0,
            //                 "avg_waktu_task5"=> 0,
            //                 "nmppk"=> "RSU AISYIYAH",
            //                 "avg_waktu_task2"=> 0,
            //                 "avg_waktu_task1"=> 0,
            //                 "kodepoli"=> "BED",
            //                 "waktu_task5"=> 0,
            //                 "waktu_task4"=> 0,
            //                 "waktu_task3"=> 0,
            //                 "insertdate"=> 1627873951000,
            //                 "tanggal"=> "2021-04-16",
            //                 "waktu_task2"=> 0,
            //                 "waktu_task6"=> 0
            //             )
            //        )
            //     )
            // );
            // echo json_encode($response);
            // return;
        }
        else if($mode == 'dashboardPerTanggal')
        {
            //Dashboard waktu per tanggal
            $waktuAntrol = $this->input->post('waktuAntrol');
            $jenisWaktu = $this->input->post('jenisWaktu');
            
            $url = '/dashboard/waktutunggu/tanggal/'.$waktuAntrol.'/waktu/'.$jenisWaktu;
            $uploadData = $request;
            $response = $this->bpjsbap->requestBpjsAntrolDashboard($url,$uploadData,$method);
            echo json_encode($response);
            return;
            // $response = array(
            //     "metadata" => array(
            //        "code"    => 200,
            //        "message" => "OK"
            //     ),
            //     "response" => array(
            //        "list" => array (
            //             array(
            //                 "kdppk"=> "1311R002",
            //                 "waktu_task1"=> 0,
            //                 "avg_waktu_task4"=> 0,
            //                 "jumlah_antrean"=> 1,
            //                 "avg_waktu_task3"=> 0,
            //                 "namapoli"=> "BEDAH",
            //                 "avg_waktu_task6"=> 0,
            //                 "avg_waktu_task5"=> 0,
            //                 "nmppk"=> "RSU AISYIYAH",
            //                 "avg_waktu_task2"=> 0,
            //                 "avg_waktu_task1"=> 0,
            //                 "kodepoli"=> "BED",
            //                 "waktu_task5"=> 0,
            //                 "waktu_task4"=> 0,
            //                 "waktu_task3"=> 0,
            //                 "insertdate"=> 1627873951000,
            //                 "tanggal"=> "2021-04-16",
            //                 "waktu_task2"=> 0,
            //                 "waktu_task6"=> 0
            //             )
            //        )
            //     )
            // );
            // echo json_encode($response);
            // return;
        }
        else if($mode == 'antreanPerTanggal')
        {
            $waktuAntrol = $this->input->post('waktuAntrol');
            $url = '/antrean/pendaftaran/tanggal/'.$waktuAntrol;
            $uploadData = $request;
            // $response = $this->bpjsbap->requestBpjsAntrolDashboard($url,$uploadData,$method);
            $response = $this->bpjsbap->requestBpjsAntreanPerTanggal($url,$uploadData,$method);
            echo json_encode($response);
            return;
            // $response = array(
            //     "metadata" => array(
            //        "code"    => 200,
            //        "message" => "OK"
            //     ),
            //     "response" => array(
            //        "list" => array (
            //             array(
            //                 "kdppk"=> "1311R002",
            //                 "waktu_task1"=> 0,
            //                 "avg_waktu_task4"=> 0,
            //                 "jumlah_antrean"=> 1,
            //                 "avg_waktu_task3"=> 0,
            //                 "namapoli"=> "BEDAH",
            //                 "avg_waktu_task6"=> 0,
            //                 "avg_waktu_task5"=> 0,
            //                 "nmppk"=> "RSU AISYIYAH",
            //                 "avg_waktu_task2"=> 0,
            //                 "avg_waktu_task1"=> 0,
            //                 "kodepoli"=> "BED",
            //                 "waktu_task5"=> 0,
            //                 "waktu_task4"=> 0,
            //                 "waktu_task3"=> 0,
            //                 "insertdate"=> 1627873951000,
            //                 "tanggal"=> "2021-04-16",
            //                 "waktu_task2"=> 0,
            //                 "waktu_task6"=> 0
            //             )
            //        )
            //     )
            // );
            // echo json_encode($response);
            // return;
        }
        else if($mode == 'listWaktuTaskID')
        {
            // $response = array(
            //                 "response" => array(
            //                         "list" => array(
            //                             array(
            //                                 "wakturs" => "16-03-2021 11:32:49 WIB",
            //                                 "waktu" => "24-03-2021 12:55:23 WIB",
            //                                 "taskname" => "mulai waktu tunggu admisi",
            //                                 "taskid" => 1,
            //                                 "kodebooking" => "Y03-20#1617068533"
            //                             )
            //                         )
            //                 ),
            //                 "metadata" => array(
            //                     "code" => 200,
            //                     "message" => "OK"
            //                 )
            //             );
            // echo json_encode($response);
            // return;
            // Melihat waktu task id yang telah dikirim ke BPJS
            $kodebooking = $this->input->post('kodebooking');
            $method  = 'POST';
            $url     = '/antrean/getlisttask';
            $request = json_encode(["kodebooking"=> $kodebooking]); //'Y03-20#1617068533']);
        }
        else if($mode == 'referensi_jadwal_dokter')
        {
            //Melihat referensi jadwal dokter yang ada pada Aplikasi HFIS
            $kodepoli = $this->input->post('kodepoli');
            $tanggal  = $this->input->post('tanggal');
            $url      = '/jadwaldokter/kodepoli/'.$kodepoli.'/tanggal/'.$tanggal;
            $request  = '';
        }
        else if($mode == 'referensi_dokter')
        {
            //Melihat referensi dokter yang ada pada Aplikasi HFIS
            $url      = '/ref/dokter';
            $request  = '';
        }
        else if($mode == 'referensi_poli')
        {
            //Melihat referensi poli yang ada pada Aplikasi HFIS
            $url      = '/ref/poli';
            $request  = '';
        }
        else if($mode == 'antreanBelumDilayani')
        {
            $url = '/antrean/pendaftaran/aktif';
            $uploadData = $request;
            $method  = 'GET';
            $response = $this->bpjsbap->requestBpjsAntreanBelumDilayani($url,$uploadData,$method);
            echo json_encode($response);
            return;
        }
        
        $uploadData = $request;
        $response = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method);
        echo json_encode($response);
    }

    /**
     * Bridging Antrian Online Log -- Get Data
     */
    public function antrol_log_getdata()
    {
        $mode = $this->input->post('mode');                                   
        $list   = $this->mdatatable->dt_dataAntrolLog_(true); 
        $data = [];
        $no = $_POST['start'];

        if($mode == 'log_taskId')
        {
            if($list){
                foreach($list->result() as $obj){            
                    $row = [];
                    $row[] = ++$no;
                    $row[] = "<b>KodeBooking : </b>".$obj->kodebooking."</br>
                            <b>No.RM : </b>".$obj->norm."</br>
                            <b>No.JKN : </b>".$obj->nojkn."</br>
                            <b>No.NIK : </b>".$obj->nik."</br>
                            <b>No.SEP : </b>".$obj->nosep."</br>
                            <b>IdPendaftaran : </b>".$obj->idpendaftaran;
                    $row[] = $obj->namalengkap;
                    $row[] = $obj->tanggal."</br>".$obj->jam;
                    $row[] = $obj->taskid;
                    $row[] = $obj->message;
                    $data[] = $row;
                }
            }
        }
        else if($mode == 'log_kode_booking')
        {  
            if($list){
                foreach($list->result() as $obj){            
                    $row = [];
                    $row[] = ++$no;
                    $row[] = "<b>KodeBooking : </b>".$obj->kodebooking."</br>
                            <b>No.RM : </b>".$obj->norm."</br>
                            <b>No.JKN : </b>".$obj->nojkn."</br>
                            <b>No.NIK : </b>".$obj->nik."</br>
                            <b>No.SEP : </b>".$obj->nosep."</br>
                            <b>IdPendaftaran : </b>".$obj->idpendaftaran;
                    $row[] = $obj->namalengkap;
                    $row[] = $obj->tanggal."</br>".$obj->jam;
                    $row[] = $obj->message;
                    $row[] = $obj->namajeniskunjungan;
                    $row[] = $obj->norujukan;
                    $row[] = "<b>CaraBayar : </b>".$obj->carabayar."</br><b>CaraDaftar : </b>".$obj->caradaftar."</br><b>Status : </b>".statuskeluar($obj->idstatuskeluar);
                    $row[] = $obj->user;
                    $row[] = $obj->sudahsingkronjkn;
                    $data[] = $row;
                }
            }
        }
        else if($mode == 'log_antrian')
        {  
            if($list){
                foreach($list->result() as $obj){            
                    $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->taskid;
                    $row[] = $obj->waktu;
                    $row[] = $obj->sudahsingkronjkn;
                    $row[] = $obj->kodebooking."  ||  ".$obj->tanggal." ".$obj->jam;
                    $row[] = $obj->idpendaftaran;
                    $data[] = $row;
                }
            }
        }
        
        echo json_encode([
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_dataAntrolLog(),
            "recordsFiltered" => $this->mdatatable->filter_dt_dataAntrolLog(),
            "data" => $data
        ]);
    }

    /**
     * Tambah Antrean
     */
    public function antrol_tambah_antrian($idpend)
    {
        $this->bridging_antrol_add($idpend);
    }

    /**
     * Update Antrean
     */
    public function antrol_ubah_antrian($idpend,$taskid)
    {
        $this->bridging_antrol_updatewaktu($idpend,$taskid);
    }

    /**
     * Batal Antrian
     */
    public function antrol_batal_antrian($ket,$idpend)
    {
        $this->bridging_antrol_batal($ket,$idpend);
    }

    //cetak excel
    public function cetak_excel_langsung()
    {
        $id = $this->input->post("p");
        $listData = $this->input->post("listkasir");
        $title = $this->input->post("title");

        $data['fileName']     = $title.' - '.$id;
        $data['listkasir']     = $listData;
        
        $this->load->view('pelayanan/excel_cetak_langsung',$data);  // sending data excel to file
    }

    // Andri
    // Sending WA
     // [Andri]
    // kirim kode booking ke server Antrian Online BPJS
    // public function kirim_WA()
    // {
    //     $postdata['nohp'] = '085643639163';
    //     $postdata['pesan'] = 'test kirim pesan';
        
    //     $norm = $this->input->post('txtNoMR');
    //     $txtTanggalSep = $this->input->post('txtTanggalSep');

    //     $idpendaftaran = [];
    //     $idpendaftaran = $this->db->query("SELECT  rp.idpendaftaran, 
    //                                                 aa.kodebooking, 
    //                                                 aa.no,  
    //                                                 rj.quota_nonjkn, 
    //                                                 rj.quota_jkn,
    //                                                 ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
    //                                                 ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn
    //                                         FROM antrian_antrian aa
    //                                         join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
    //                                         join rs_jadwal rj on rj.idjadwal = rp.idjadwal
    //                                         join rs_unit ru on ru.idunit = rj.idunit
    //                                         WHERE rp.norm='".$norm."'
    //                                                 AND date(waktu)=date('".$txtTanggalSep."')
    //                                         LIMIT 1")->row_array();

    //     if($idpendaftaran)
    //     {
    //         $myIdPendaftaran = $idpendaftaran['idpendaftaran'];  
    //         //update person_pendaftaran
    //         $dt_pp  = [];
    //         $dt_pp['nosep']     = $postdata['nosep'];
    //         $dt_pp['norujukan'] = $postdata['txtNorujukan'];
    //         $dt_pp['nokontrol'] = ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" );
            
    //         // disable code update
    //         //$this->db->update('person_pendaftaran',$dt_pp,$idpendaftaran['idpendaftaran']);
    //         // $this->db->query("UPDATE person_pendaftaran 
    //         //                     set nosep = '".$postdata['nosep'].", norujukan ='".$postdata['txtNorujukan']."'
    //         //                     where idpendaftaran = '".$myIdPendaftaran."'");
                                                    
            
    //         // update / insert table bpjs_bridging_vclaim 
    //         $vclaim_data = [
    //                     // 'norujukan_keluar' => $postdata('norujukan_keluar'),
    //                     'norencanakontrol' => ((isset($postdata['no_surat_kontrol'])) ? $postdata['no_surat_kontrol'] : "" ),
    //                     // 'kddiagnosa_awal' => $postdata('kddiagnosa_awal'),  // from vclaim
    //                     // 'diagnosa_awal' => $postdata('diagnosa_awal'),  // from vclaim
    //                     'nomorreferensi' => ((isset($postdata['no_surat_kontrol']) && empty(!$postdata['no_surat_kontrol']) ) ? $postdata['no_surat_kontrol'] : $postdata['txtNorujukan']),
    //                     'quota_jkn' => $idpendaftaran['quota_jkn'],
    //                     'quota_nonjkn' => $idpendaftaran['quota_nonjkn'],
    //                     'quota_jkn_sisa' => intval($idpendaftaran['quota_jkn']) - intval($idpendaftaran['antrianjkn']),
    //                     'quota_nonjkn_sisa' => intval($idpendaftaran['quota_nonjkn']) - ( intval($idpendaftaran['totalantrean']) - intval($idpendaftaran['antrianjkn']) )
    //                 ];  
    //         $save_SEP  = $this->ql->bpjs_bridging_vclaim_insert_or_update($myIdPendaftaran, $vclaim_data);
    //         if($save_SEP) // if success
    //         { 
    //             //kirim antrian online
    //             $sendToServer = $this->bridging_antrol_add($myIdPendaftaran);
    //             pesan_success_danger($sendToServer,"SUKSES dikirim ke Server Antrian Online..!","GAGAL dikirim ke Server Antrian Online..!","js");
    //         }     
    //     }
    //     else
    //     {
    //         pesan_success_danger(0," ","<h4><strong><center>GAGAL dikirim ke
    //                                     </br>Server Antrian Online..!!</center></strong></h4>
    //                                     </br>
    //                                     <strong><i>Tips : </i></strong>
    //                                     <ul> 
    //                                         <li>pastikan No RM terdiri dari 8 digit</li>
    //                                         <li>cek nomor HP di Admisi >  Data Induk</li>
    //                                         <li>cek NIK di Admisi > Data Induk</li>
    //                                         <li>cek No Kartu BPJS di Admisi > Data Induk</li>
    //                                     </ul>",
    //                                     "js");
    //     }

        
    // }

}
/* End of file ${TM_FILENAME:${1/(.+)/Cbpjs.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cbpjs/:application/controllers/${1/(.+)/Cbpjs.php/}} */






