<?php

use MY_controller;

defined('BASEPATH') or exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Cmasterdata extends MY_controller
{
    function __construct()
    {
        parent::__construct();
        // jika user belum login maka akses ditolak
        if ($this->session->userdata('sitiql_session') != 'aksesloginberhasil') {
            pesan_belumlogin();
        }
        $this->load->model('mmasterdata');
        $this->load->model('mdatatable');
        $this->load->model('mviewql');
        $this->load->model('msqlbasic');
        $this->load->library(array('form_validation', 'session'));
    }

    // cari pegawai
    public function caridatapegawai()
    {
        $cari = $this->input->post('searchTerm');
        $fetchData = (empty($cari)) ? '' : $this->db->query("select ppeg.*, pp.namalengkap, pp.nik from person_pegawai ppeg, person_person pp where ppeg.statuskeaktifan='aktif' and pp.idperson=ppeg.idperson and pp.namalengkap like '%" . $cari . "%' limit 10")->result();
        $data = array();
        if (!empty($fetchData)) {
            foreach ($fetchData as $obj) {
                $data[] = array("id" => $obj->idpegawai, "text" => $obj->nik . ' | ' . $obj->titeldepan . ' ' . $obj->namalengkap . ' ' . $obj->titelbelakang);
            }
        }
        echo json_encode($data);
    }
    ///////////////////////MASTERDATA PROPINSI ///////////////////
    //-------------------------- ^^ Standar
    public function settingpropinsi()
    {
        return [
            'content_view'      => 'masterdata/v_propinsi',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'wilayah',
            'active_menu_level' => 'propinsi'
        ];
    }
    public function propinsi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PROPINSI)) //lihat define di atas
        {
            $data                = $this->settingpropinsi();
            $data['title_page']  = 'Propinsi';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Propinsi';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['jsmode']      = 'propinsi';
            $data['script_js']   = ['js_datatable'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    // load display data propinsi
    public function load_dtpropinsi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PROPINSI)) //lihat define di atas
        {
            $list = $this->mdatatable->dt_geografi_propinsi_(true);
            $data = array();
            $no = $_POST['start'];
            foreach ($list->result() as $field) {

                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $id           = $this->encryptbap->encrypt_urlsafe($field->idpropinsi, "json");
                $tabel        = $this->encryptbap->encrypt_urlsafe('geografi_propinsi', "json");
                $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_PROPINSI, "json");
                $row = array();
                $row[] = ++$no;
                $row[] = $field->namapropinsi;
                $row[] = '<a data-toggle="tooltip" title="" data-original-title="Edit Propinsi" class="btn btn-warning btn-xs" href="' . base_url('cmasterdata/edit_propinsi/' . $id) . '" ><i class="fa fa-pencil"></i> Edit</a>
                              <a data-toggle="tooltip" title="" data-original-title="Delete Propinsi" id="delete_data" nobaris="' . ($no - 1) . '" class="btn btn-danger btn-xs" href="#" alt="' . $tabel . '" alt2="' . $id . '" alt3="' . $idhalaman . '">
                             <i class="fa fa-trash"></i> Delete</a>';
                $data[] = $row;
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_geografi_propinsi(),
                "recordsFiltered" => $this->mdatatable->filter_dt_geografi_propinsi(),
                "data" => $data,
            );
            //output dalam format JSON
            echo json_encode($output);
        } else {
            aksesditolak();
        }
    }
    public function add_propinsi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PROPINSI)) //lihat define di atas
        {

            $this->load->helper('form');
            $data               = $this->settingpropinsi();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Propinsi';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_propinsi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PROPINSI)) //lihat define di atas
        {
            $this->load->helper('form');
            $data                 = $this->settingpropinsi();
            $this->mgenerikbap->setTable('geografi_propinsi');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']  = $this->mgenerikbap->select('*', ['idpropinsi' => $id])->row_array();
            $data['title_page'] = 'Edit Propinsi';
            $data['mode']       = 'edit';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_propinsi() // SIMPAN PROPINSI
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PROPINSI)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namapropinsi', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idpropinsi   =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idpropinsi')));
                $data['namapropinsi'] = $this->input->post('namapropinsi');
                $this->mgenerikbap->setTable('geografi_propinsi');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idpropinsi);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idpropinsi) ? 'Save Success.!' : 'Update Success.!', empty($idpropinsi) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/propinsi'));
        }
    }
    public function single_delete() // DELETE DATA
    {
        if ($this->pageaccessrightbap->checkAccessRight($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('hal')))) //lihat define di atas
        {
            $tableName = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('t'));
            $id = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('i'));

            if ($tableName == 'rs_icd') {
                $datal = [
                    'url'     => get_url(),
                    'log'     => 'user:' . $this->session->userdata('username') . ';status:hapus icd;icd:' . $id . ';',
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];
                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            }

            if ($tableName == 'rs_mastertarif') {
                $icd = $this->db->select('icd')->get_where($tableName, ['idmastertarif' => $id])->row_array()['icd'];
                $datal = [
                    'url'     => get_url(),
                    'log'     => 'user:' . $this->session->userdata('username') . ';status:hapus tarif;icd:' . $icd . ';',
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];
                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            }

            if ($tableName == "rs_bangsal") {
                applicare_deletedata($id);
            } elseif ($tableName == "rs_bed") {
                $query = $this->db->query("SELECT idbangsal FROM rs_bed where idbed=" . $id)->row_array();
                if (array_key_exists("idbangsal", $query)) {
                    aplicare_updatebed($query["idbangsal"]);
                }
            } else if ($tableName == 'rs_paket_pemeriksaan') {
                $this->db->delete('rs_paket_pemeriksaan_detail', ['idpaketpemeriksaanparent' => $id]);
                $this->db->delete('rs_paket_pemeriksaan', ['parent_parent' => $id]);
                $this->db->delete('rs_mastertarif_paket_pemeriksaan', ['idpaketpemeriksaan' => $id]);
            }

            if ($tableName == 'rs_barang') {
                $arrMsg = $this->db->delete($tableName, ['idbarang' => $id]);
            } else {
                $this->mgenerikbap->setTable($tableName);
                $arrMsg = $this->mgenerikbap->delete($id);
            }

            pesan_success_danger($arrMsg, 'Delete Success.!', 'Delete Failed.!', 'js'); //jika hapus berhasil status success selain itu status error
        }
    }
    ///////////////////////MASTERDATA KABUPATEN///////////////////
    public function settingkabupaten()
    {
        return [
            'content_view'      => 'masterdata/v_kabupaten',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'wilayah',
            'active_menu_level' => 'kabupaten'
        ];
    }
    public function kabupaten()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KABUPATEN)) //lihat define di atas
        {
            $this->load->model('mmasterdata');
            $data                = $this->settingkabupaten();
            $data['data_list']   = $this->mmasterdata->get_data_kabupaten();
            $data['title_page']  = 'Kabupaten';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Kabupaten';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_kabupaten()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KABUPATEN)) //lihat define di atas
        {
            $this->load->helper('form');
            $data               = $this->settingkabupaten();
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $data['data_akses'] = $this->mgenerikbap->select('*')->result_array();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add kabupaten';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_kabupaten()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KABUPATEN)) //lihat define di atas
        {
            $data                = $this->settingkabupaten();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kabupatenkota');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idkabupatenkota' => $id])->row_array();
            $data['title_page'] = 'Edit Kabupaten';
            $data['mode'] = 'edit';
            $data['plugins'] = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_kabupaten() // SIMPAN KABUPATEN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KABUPATEN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('idpropinsi', '', 'required');
            $this->form_validation->set_rules('namakabupatenkota', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idkabupatenkota = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idkabupatenkota')));
                $idpropinsi = $this->input->post('idpropinsi');
                $namakabupatenkota = $this->input->post('namakabupatenkota');
                $data = ['idpropinsi' => $idpropinsi, 'namakabupatenkota' => $namakabupatenkota];
                //Simpan
                $this->mgenerikbap->setTable('geografi_kabupatenkota');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idkabupatenkota);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idkabupatenkota) ? 'Save Success.!' : 'Update Success.!', empty($idkabupatenkota) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/kabupaten'));
        }
    }
    ///////////////////////MASTERDATA KECAMATAN///////////////////
    public function settingkecamatan()
    {
        return [
            'content_view'      => 'masterdata/v_kecamatan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'wilayah',
            'active_menu_level' => 'kecamatan'
        ];
    }
    public function kecamatan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KECAMATAN)) //lihat define di atas
        {
            $data                = $this->settingkecamatan();
            $this->load->model('mmasterdata');
            $data['data_list']   = $this->mmasterdata->get_data_kecamatan();
            $data['title_page']  = 'Kecamatan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Kecamatan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_kecamatan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KECAMATAN)) //lihat define di atas
        {

            $data               = $this->settingkecamatan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list1']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kabupatenkota');
            $data['data_list2']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kecamatan');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $data['data_akses'] = $this->mgenerikbap->select('*')->result_array();
            $data['data_edit'] = '';
            $data['title_page'] = 'Add kecamatan';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_kecamatan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KECAMATAN)) //lihat define di atas
        {
            $data                = $this->settingkecamatan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list1']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kabupatenkota');
            $data['data_list2']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kecamatan');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kecamatan');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idkecamatan' => $id])->row_array();
            $data['title_page'] = 'Edit Kecamatan';
            $data['mode'] = 'edit';
            $data['plugins'] = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_kecamatan() // SIMPAN KECAMATAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KECAMATAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('idpropinsi', '', 'required');
            $this->form_validation->set_rules('idkabupatenkota', '', 'required');
            $this->form_validation->set_rules('namakecamatan', '', '');
            $this->form_validation->set_rules('kodepos', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idkecamatan = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idkecamatan')));
                $idkabupatenkota = $this->input->post('idkabupatenkota');
                $namakecamatan = $this->input->post('namakecamatan');
                $kodepos = $this->input->post('kodepos');
                $data = ['idkabupatenkota' => $idkabupatenkota, 'namakecamatan' => $namakecamatan, 'kodepos' => $kodepos];
                //Simpan
                $this->mgenerikbap->setTable('geografi_kecamatan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idkecamatan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idkecamatan) ? 'Save Success.!' : 'Update Success.!', empty($idkecamatan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/kecamatan'));
        }
    }
    ///////////////////////MASTERDATA KELURAHAN///////////////////
    public function settingkelurahan()
    {
        return [
            'content_view'      => 'masterdata/v_kelurahan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'wilayah',
            'active_menu_level' => 'kelurahan'
        ];
    }
    public function kelurahan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELURAHAN)) //lihat define di atas
        {
            $data                = $this->settingkelurahan();
            $this->load->model('mmasterdata');
            $data['data_list']   = $this->mmasterdata->get_data_kelurahan();
            $data['title_page']  = 'Kelurahan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Kelurahan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_kelurahan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELURAHAN)) //lihat define di atas
        {

            $data               = $this->settingkelurahan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list1']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kabupatenkota');
            $data['data_list2']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kecamatan');
            $data['data_list3']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_desakelurahan');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $data['data_akses'] = $this->mgenerikbap->select('*')->result_array();
            $data['data_edit'] = '';
            $data['title_page'] = 'Add kelurahan';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_kelurahan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELURAHAN)) //lihat define di atas
        {
            $data                = $this->settingkelurahan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('geografi_propinsi');
            $data['data_list1']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kabupatenkota');
            $data['data_list2']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_kecamatan');
            $data['data_list3']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_desakelurahan');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();
            $this->mgenerikbap->setTable('geografi_desakelurahan');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['iddesakelurahan' => $id])->row_array();
            $data['title_page'] = 'Edit Kelurahan';
            $data['mode'] = 'edit';
            $data['plugins'] = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_kelurahan() // SIMPAN KECAMATAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELURAHAN)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('idpropinsi', '', 'required');
            $this->form_validation->set_rules('idkabupatenkota', '', 'required');
            $this->form_validation->set_rules('idkecamatan', '', 'required');
            $this->form_validation->set_rules('kodepos', '', '');
            $this->form_validation->set_rules('namadesakelurahan', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $iddesakelurahan = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('iddesakelurahan')));
                $idkecamatan = $this->input->post('idkecamatan');
                $namadesakelurahan = $this->input->post('namadesakelurahan');
                $data = ['idkecamatan' => $idkecamatan, 'namadesakelurahan' => $namadesakelurahan];
                //Simpan
                $this->mgenerikbap->setTable('geografi_desakelurahan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $iddesakelurahan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($iddesakelurahan) ? 'Save Success.!' : 'Update Success.!', empty($iddesakelurahan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/kelurahan'));
        }
    }
    ///////////////////////MASTERDATA JENJANG PENDIDIKAN ///////////////////
    //-------------------------- ^^ Standar
    public function settingpendidikan()
    {
        return [
            'content_view'      => 'masterdata/v_pendidikan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'demografi',
            'active_menu_level' => 'pendidikan'
        ];
    }
    public function pendidikan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDIDIKAN)) //lihat define di atas
        {
            $data                = $this->settingpendidikan();
            $data['data_list']   = $this->db->get('demografi_pendidikan')->result();
            $data['title_page']  = 'Jenjang Pendidikan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Jenjang Pendidikan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_pendidikan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDIDIKAN)) //lihat define di atas
        {
            $this->load->helper('form');

            $data               = $this->settingpendidikan();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Pendidikan';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_pendidikan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDIDIKAN)) //lihat define di atas
        {
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $this->load->helper('form');
            $data               = $this->settingpendidikan();
            $data['data_edit']  = $this->db->get_where('demografi_pendidikan', ['idpendidikan' => $id])->row_array();
            $data['title_page'] = 'Edit Pendidikan';
            $data['mode']       = 'edit';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    // SIMPAN PENDIDIKAN
    public function save_pendidikan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENDIDIKAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namapendidikan', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idpendidikan   =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idpendidikan')));
                $data = [
                    'namapendidikan' => $this->input->post('namapendidikan'),
                    'levelkkni' => $this->input->post('levelkkni')
                ];

                //Simpan
                $id = $this->input->post('idpendidikan');
                $this->mgenerikbap->setTable('demografi_pendidikan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idpendidikan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idpendidikan) ? 'Save Success.!' : 'Update Success.!', empty($idpendidikan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/pendidikan'));
        }
    }
    ///////////////////////MASTERDATA ICD ///////////////////
    //-------------------------- ^^ Standar
    public function settingicd()
    {
        return [
            'content_view'      => 'masterdata/v_icd',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'icd',
            'active_menu_level' => 'mastericd'
        ];
    }
    public function icd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ICD)) //lihat define di atas
        {
            $data                = $this->settingicd();
            $data['title_page']  = 'ICD';
            $data['mode']        = 'view1';
            $data['table_title'] = 'Data ICD';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['jsmode']      = 'icd';
            $data['script_js']   = ['js_datatable'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_icd()
    {
        $getData = $this->mdatatable->dt_icd_(true);
        $data = [];
        $no = $_POST['start'];
        $nobaris = 0;
        if ($getData) {
            foreach ($getData->result() as $field) {
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $id           = $this->encryptbap->encrypt_urlsafe($field->icd, "json");
                $tabel        = $this->encryptbap->encrypt_urlsafe('rs_icd', "json");
                $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_ICD, "json");
                $klasifikasi  = "";
                $idicd_        = $field->idicd;
                if ($idicd_ == 10) {
                    $klasifikasi  = "ICD 10";
                } else if ($idicd_ == 9) {
                    $klasifikasi  = "ICD 9";
                } else if ($idicd_ == 0) {
                    $klasifikasi  = "Lainnya";
                } else {
                    $klasifikasi  = "-";
                }
                $row = [];
                $row[] = $field->icd;
                $row[] = $field->namaicd;
                $row[] = $field->aliasicd;
                $row[] = $field->nilaiacuanrendah . ((!empty($field->nilaiacuantinggi)) ? ' - ' : '') . $field->nilaiacuantinggi . ' ' . $field->satuan;
                $row[] = $field->statuskronis;
                $row[] = $field->golonganicd;
                $row[] = $field->jenispenyakit;
                $row[] = $field->nodtd . ' - ' . $field->golongansebabpenyakit;
                $row[] = $klasifikasi;
                $row[] = '<a data-toggle="tooltip" title="" data-original-title="Ubah" class="btn btn-warning btn-xs" href="' . base_url('cmasterdata/edit_icd/' . $id) . '" ><i class="fa fa-pencil"></i></a>';
                //<a data-toggle="tooltip" title="" data-original-title="Hapus" id="delete_data" nobaris="'.$nobaris.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'"><i class="fa fa-trash"></i></a>
                $data[] = $row;
                $no++;

                if ($no > 9) {
                    $nobaris = 0;
                } else {
                    $nobaris++;
                }
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_icd(),
            "recordsFiltered" => $this->mdatatable->filter_dt_icd(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function testingicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ICD)) //lihat define di atas
        {
            $data                = $this->settingicd();
            $data['data_list']   = $this->db->query("select * from rs_icd, rs_jenisicd limit 0 , 50")->result();
            $data['title_page']  = 'ICD';
            $this->mgenerikbap->setTable('rs_jenisicd');
            $data['searchjenisicd'] = $this->mgenerikbap->select('*')->result();
            $data['selectsearchidjenisicd']  = '';
            $data['mode']        = 'testing';
            $data['table_title'] = 'Data ICD';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = ['js_masterdata-icd'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public  function inputkodeicd()
    {
        $icd = $this->input->post('icd');
        $result = $this->db->select('icd')->get_where('rs_icd', ['icd' => $icd])->row_array();
        echo json_encode($result);
    }
    public function add_icd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ICD)) //lihat define di atas
        {
            $data                  = $this->settingicd();
            $this->load->helper('form');
            $data['data_edit']     = '';
            $data['title_page']    = 'Add ICD';
            $data['mode']          = 'add';
            $this->mgenerikbap->setTable('rs_jenisicd');
            $data['jenisicd']      = $this->mgenerikbap->select("*")->result();
            $data['sebabpenyakit'] = $this->db->get('rs_icd_golongansebabpenyakit')->result();
            $data['jenispenyakit'] = $this->db->get('rs_jenispenyakit')->result();
            $data['istext']        = $this->mmasterdata->get_data_enum('rs_icd', 'istext');
            $data['golonganicd']   = $this->mmasterdata->get_data_enum('rs_icd', 'golonganicd');
            $data['statuskronis']  = $this->mmasterdata->get_data_enum('rs_icd', 'statuskronis');
            $data['klasifikasi']   = ['10' => 'ICD 10', '9' => 'ICD 9', '0' => 'Lainnya'];
            $data['plugins']       = [PLUG_DROPDOWN];
            $data['script_js']     = ['js_masterdata-icd'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_icd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ICD)) //lihat define di atas
        {
            $data                  = $this->settingicd();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_icd');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']     = $this->mgenerikbap->select('*', ['icd' => $id])->row_array();
            $data['title_page']    = 'Edit ICD';
            $this->mgenerikbap->setTable('rs_jenisicd');
            $data['jenisicd']      = $this->mgenerikbap->select("*")->result();
            $data['sebabpenyakit'] = $this->db->get('rs_icd_golongansebabpenyakit')->result();
            $data['jenispenyakit'] = $this->db->get('rs_jenispenyakit')->result();
            $data['istext']        = $this->mmasterdata->get_data_enum('rs_icd', 'istext');
            $data['golonganicd']   = $this->mmasterdata->get_data_enum('rs_icd', 'golonganicd');
            $data['statuskronis']  = $this->mmasterdata->get_data_enum('rs_icd', 'statuskronis');
            $data['klasifikasi']   = ['10' => 'ICD 10', '9' => 'ICD 9', '0' => 'Lainnya'];
            $data['mode']          = 'edit';
            $data['plugins']       = [PLUG_DROPDOWN];
            $data['script_js']     = ['js_masterdata-icd'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_icd() // SIMPAN ICD
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ICD)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('kodeicd', '', 'required');
            $this->form_validation->set_rules('namaicd', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $id =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id')));
                $data = [ //set data yang akan disimpan
                    'idicd'           => $this->input->post('klasifikasiicd'),
                    'icd'             => $this->input->post('kodeicd'),
                    'namaicd'         => $this->input->post('namaicd'),
                    'aliasicd'        => $this->input->post('aliasicd'),
                    'satuan'          => $this->input->post('satuan'),
                    'nilaiacuanrendah' => $this->input->post('nilaiacuanrendah'),
                    'nilaiacuantinggi' => $this->input->post('nilaiacuantinggi'),
                    'idjenisicd'      => $this->input->post('jenisicd'),
                    'nilaidefault'    => $this->input->post('nilaidefault'),
                    'istext'          => $this->input->post('istext'),
                    'statuskronis'    => $this->input->post('statuskronis'),
                    'golonganicd'     => $this->input->post('golonganicd'),
                    'idgolongansebabpenyakit' => $this->input->post('idgolongansebabpenyakit'),
                    'idjenispenyakit' => $this->input->post('idjenispenyakit'),
                    'isjmbukanpenunjang	' => $this->input->post('isjmbukanpenunjang')
                ];
                $this->mgenerikbap->setTable('rs_icd'); //set table
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id); //Simpan
                //log
                $datal = [
                    'url'     => get_url(),
                    'log'     => 'user:' . $this->session->userdata('username') . ';status: ' . ((empty($id)) ? ' add icd' : ' ubah icd;') . ' icd=' . $this->input->post('kodeicd') . 'idjenisicd=' . $this->input->post('jenisicd') . 'namaicd=' . $this->input->post('namaicd'),
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];
                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
                pesan_success_danger($simpan, empty($id) ? 'Save Success.!' : 'Update Success.!', empty($id) ? 'Save  Failed.!' : 'Update Failed.!'); //jika simpan status success selain itu status error
            }
            redirect(base_url('cmasterdata/icd')); //kembali ke halaman icd
        }
    }
    // SIMPAN ICD -> dari Pemeriksaan yang di input dokter atau perawat
    public function pemeriksaan_saveicd()
    {
        $data = [ //set data yang akan disimpan
            'icd'             => $this->input->post('kodeicd'),
            'namaicd'         => $this->input->post('namaicd'),
            'aliasicd'        => $this->input->post('aliasicd'),
            'idjenisicd'      => '2'
        ];
        $this->mgenerikbap->setTable('rs_icd'); //set table
        $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data);
        pesan_success_danger($simpan, 'Add Icd Success.!', 'Add Icd Failed.!', "js");
    }
    //-- mahmud, clear
    //-- paket pemeriksaan rule
    //start paket pemeriksaan rule
    public function paketpemeriksaanrule()
    {
        $data['content_view'] = 'masterdata/v_paketpemeriksaanrule';
        $data['active_menu'] = 'masterdata';
        $data['active_sub_menu']   = 'paketpemeriksaan';
        $data['active_menu_level'] = 'paketpemeriksaanrule';
        $data['title_page']  = 'Paket Pemeriksaan Rule';
        $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN];
        $data['script_js']   = ['js_masterdata-paketpemeriksaanrule'];
        $this->load->view('v_index', $data);
    }

    public function dt_paketpemeriksaanrule()
    {
        $getData = $this->mdatatable->dt_paketpemeriksaanrule_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->namarule;
                $row[] = '<a id="edit" alt="' . $obj->idpaketpemeriksaanrule . '" class="btn btn-warning btn-xs" ' . ql_tooltip('ubah rule') . '><i class="fa fa-edit"></i></a> '
                    . '<a id="hapusrule" alt="' . $obj->idpaketpemeriksaanrule . '" alt2="' . $obj->namarule . '" class="btn btn-danger btn-xs" ' . ql_tooltip('hapus rule') . '><i class="fa fa-trash"></i></a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_paketpemeriksaanrule(),
            "recordsFiltered" => $this->mdatatable->filter_dt_paketpemeriksaanrule(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function save_paketpemeriksaanrule()
    {
        $post = $this->input->post();
        $data = ['namarule' => $post['namarule']];
        $id   = $post['id'];
        //save rule
        $this->mgenerikbap->setTable('rs_paket_pemeriksaan_rule');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);

        $idrule = empty($id) ? $this->db->insert_id() : $id;
        //save detail rule
        for ($i = 0; $i < count($post['jumlahdetail']); $i++) {
            $batasatas = ($post['batasatas'][$i] == 'null' || $post['batasatas'][$i] == '') ? NULL : $post['batasatas'][$i];
            $batasbawah = ($post['batasbawah'][$i] == 'null' || $post['batasbawah'][$i] == '') ? NULL : $post['batasbawah'][$i];
            $dataDetail = [
                'idpaketpemeriksaanrule' => $idrule,
                'icd' => $post['icd'][$i],
                'batasatas' => $batasatas,
                'batasbawah' => $batasbawah,
                'teks' => $post['teks'][$i],
                'skor' => $post['skor'][$i]
            ];

            $arrMsg = $this->db->replace('rs_paket_pemeriksaan_rule_detail', $dataDetail);
        }

        $mode = empty($id) ? 'Tambah' : 'Ubah';
        pesan_success_danger($arrMsg, $mode . ' Paket pemeriksaan Rule Berhasil. ', $mode . ' Paket pemeriksaan Rule Gagal.', 'js');
    }

    public function onreadyformpaketpemeriksaanrule()
    {
        $id = $this->input->post('i');
        $data['ruledetail'] = $this->db->select('rpprd.*, (select namaicd from rs_icd where icd=rpprd.icd) as namaicd')->get_where('rs_paket_pemeriksaan_rule_detail rpprd', ['idpaketpemeriksaanrule' => $id])->result_array();
        $data['rule'] = $this->db->get_where('rs_paket_pemeriksaan_rule', ['idpaketpemeriksaanrule' => $id])->row_array();
        echo json_encode($data);
    }

    public function hapus_rs_paket_pemeriksaan_rule_detail()
    {
        $arrMsg = $this->db->delete('rs_paket_pemeriksaan_rule_detail', ['icd' => $this->input->post('icd'), 'idpaketpemeriksaanrule' => $this->input->post('idpaketpemeriksaanrule')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    public function hapus_rs_paket_pemeriksaan_rule()
    {
        $arrMsg = $this->db->delete('rs_paket_pemeriksaan_rule', ['idpaketpemeriksaanrule' => $this->input->post('idpaketpemeriksaanrule')]);
        $arrMsg = $this->db->delete('rs_paket_pemeriksaan_rule_detail', ['idpaketpemeriksaanrule' => $this->input->post('idpaketpemeriksaanrule')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    //end paket pemeriksaan rule
    //-------------------

    //-- mahmud, clear
    //-- paket pemeriksaan rule pesan
    //start paket pemeriksaan rule pesan
    public function paketpemeriksaanrulepesan()
    {
        $data['content_view'] = 'masterdata/v_paketpemeriksaanrulepesan';
        $data['active_menu'] = 'masterdata';
        $data['active_sub_menu']   = 'paketpemeriksaan';
        $data['active_menu_level'] = 'paketpemeriksaanrulepesan';
        $data['title_page']  = 'Paket Pemeriksaan Rule Pesan';
        $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA];
        $data['script_js']   = ['js_masterdata-paketpemeriksaanrulepesan'];
        $this->load->view('v_index', $data);
    }

    public function dt_paketpemeriksaanrulepesan()
    {
        $getData = $this->mdatatable->dt_paketpemeriksaanrulepesan_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->namarule;
                $row[] = $obj->skor;
                $row[] = $obj->pesan;
                $row[] =  '<a id="edit" alt="' . $obj->idpaketpemeriksaanrulepesan . '" class="btn btn-warning btn-xs" ' . ql_tooltip('ubah pesan') . '><i class="fa fa-edit"></i></a> '
                    . '<a id="hapuspesan" alt="' . $obj->idpaketpemeriksaanrulepesan . '" alt2="' . $obj->namarule . '" class="btn btn-danger btn-xs" ' . ql_tooltip('hapus pesan') . '><i class="fa fa-trash"></i></a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_paketpemeriksaanrulepesan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_paketpemeriksaanrulepesan(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function save_paketpemeriksaanrulepesan()
    {
        $post = $this->input->post();
        $data = [
            'idpaketpemeriksaanrule' => $post['idpaketpemeriksaanrule'],
            'skor' => $post['skor'],
            'pesan' => $post['pesan']
        ];
        $id = $post['idpaketpemeriksaanrulepesan'];

        //save rule pesan
        $this->mgenerikbap->setTable('rs_paket_pemeriksaan_rule_pesan');
        $arrMsg = $this->mgenerikbap->update_or_insert_replaceonduplicate($data, $id);
        $mode = empty($id) ? 'Tambah' : 'Ubah';
        pesan_success_danger($arrMsg, $mode . ' Paket pemeriksaan Rule Pesan Berhasil. ', $mode . ' Paket pemeriksaan Rule Pesan Gagal.', 'js');
    }

    public function onreadyformpaketpemeriksaanrulepesan()
    {
        $data = $this->db->select('rpprp.*, (select namarule from rs_paket_pemeriksaan_rule rppr where rppr.idpaketpemeriksaanrule=rpprp.idpaketpemeriksaanrule) as namarule')->get_where('rs_paket_pemeriksaan_rule_pesan rpprp', ['idpaketpemeriksaanrulepesan' => $this->input->post('i')])->row_array();
        echo json_encode($data);
    }

    public function hapus_rs_paket_pemeriksaan_rule_pesan()
    {
        $arrMsg = $this->db->delete('rs_paket_pemeriksaan_rule_pesan', ['idpaketpemeriksaanrulepesan' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    //end paket pemeriksaan rule pesan
    //-------------------


    //-- mahmud, clear
    //-- jenis cetak hasil paket pemeriksaan
    public function jeniscetakpaketpemeriksaan()
    {
        $data['content_view'] = 'masterdata/v_jeniscetakpaketpemeriksaan';
        $data['active_menu'] = 'masterdata';
        $data['active_sub_menu']   = 'paketpemeriksaan';
        $data['active_menu_level'] = 'jeniscetakpaketpemeriksaan';
        $data['title_page']  = 'Jenis Cetak Paket Pemeriksaan';
        $data['plugins']     = [PLUG_DATATABLE];
        $data['script_js']   = ['js_masterdata-jeniscetakpaketpemeriksaan'];
        $this->load->view('v_index', $data);
    }

    public function dt_jeniscetakpaketpemeriksaan()
    {
        $getData = $this->mdatatable->dt_jeniscetakpaketpemeriksaan_(true);
        $data = [];
        $no   = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->kode;
                $row[] = $obj->jenis;
                $row[] = $obj->judul;
                $row[] = $obj->header;
                $row[] =  '<a id="edit" alt="' . $obj->idjeniscetak . '" class="btn btn-warning btn-xs" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> '
                    . '<a id="hapus" alt="' . $obj->idjeniscetak . '" alt2="' . $obj->jenis . '" class="btn btn-danger btn-xs" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_jeniscetakpaketpemeriksaan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_jeniscetakpaketpemeriksaan(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function formreadyjeniscetak()
    {
        $id = $this->input->post('id');
        $data = $this->db->get_where('rs_jeniscetaklaboratorium', ['idjeniscetak' => $id])->row_array();
        echo json_encode($data);
    }

    public function save_jeniscetaklaboratorium()
    {
        $data['kode']  = $this->input->post('kode');
        $data['jenis'] = $this->input->post('jenis');
        $data['judul'] = $this->input->post('judul');
        $data['header'] = $this->input->post('header');

        $idjeniscetak = $this->input->post('idjeniscetak');
        if (empty($idjeniscetak)) {
            $arrMsg = $this->db->insert('rs_jeniscetaklaboratorium', $data);
        } else {
            $arrMsg = $this->db->update('rs_jeniscetaklaboratorium', $data, ['idjeniscetak' => $idjeniscetak]);
        }
        pesan_success_danger($arrMsg, 'Simpan Berhasil.', 'Simpan Gagal.', 'js');
    }

    public function hapus_jeniscetaklaboratorium()
    {
        $id = $this->input->post('id');
        $arrMsg = $this->db->delete('rs_jeniscetaklaboratorium', ['idjeniscetak' => $id]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    //-- /jenis cetak hasil paket pemeriksaan

    ///////////////////////MASTERDATA MASTER TARIF PAKET PEMERIKSAAN ///////////////////
    //-------------------------- ^^ Standar
    public function settingmastertarifpaketpemeriksaan()
    {
        return [
            'content_view'      => 'masterdata/v_mastertarifpaketpemeriksaan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'tarif',
            'active_menu_level' => 'mastertarifpaketpemeriksaan'
        ];
    }

    public function mastertarifpaketpemeriksaan()
    {
        //    if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        //        {
        $data                = $this->settingmastertarifpaketpemeriksaan();
        $data['title_page']  = 'Tarif Paket Pemeriksaan';
        $data['mode']        = 'view';
        $data['table_title'] = 'Tarif Paket Pemeriksaan';
        $data['plugins']     = [PLUG_DATATABLE];
        $data['jsmode']      = 'view';
        $data['script_js']   = ['js_masterdata-mastertarifpaketpemeriksaan'];
        $this->load->view('v_index', $data);
        //        }
        //        else
        //        {
        //            aksesditolak();
        //        }
    }

    public function dt_mastertarifpaketpemeriksaan()
    {
        $getData = $this->mdatatable->dt_mastertarifpaketpemeriksaan_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $this->encryptbap->generatekey_once("TARIFPAKET");
                $idmtp = $this->encryptbap->encrypt_urlsafe($obj->idmastertarifpaketpemeriksaan, "json");
                $row = [];
                $row[] = $obj->namapaketpemeriksaan;
                $row[] = $obj->jenisicd;
                $row[] = $obj->kelas;
                $row[] = $obj->jenistarif;
                $row[] = $obj->kode . ' ' . $obj->nama;
                $row[] = convertToRupiah($obj->jasaoperator);
                $row[] = convertToRupiah($obj->nakes);
                $row[] = convertToRupiah($obj->jasars);
                $row[] = convertToRupiah($obj->bhp);
                $row[] = convertToRupiah($obj->akomodasi);
                $row[] = convertToRupiah($obj->margin);
                $row[] = convertToRupiah($obj->sewa);
                $row[] = convertToRupiah($obj->total);
                $row[] = convertToRupiah($obj->total_asuransi);
                $row[] =  '<a href="' . base_url() . 'cmasterdata/edit_mastertarifpaketpemeriksaan/' . $idmtp . '" class="btn btn-warning btn-xs" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> '
                    . '<a id="hapusmastertarifpaket" namapaket="' . $obj->namapaketpemeriksaan . '" alt="' . $idmtp . '" class="btn btn-danger btn-xs" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_mastertarifpaketpemeriksaan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_mastertarifpaketpemeriksaan(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function add_mastertarifpaketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data               = $this->settingmastertarifpaketpemeriksaan();
            $data['idpaketpemeriksaan'] = '';
            $data['idmastertarifpaketpemeriksaan'] = '';
            $data['title_page'] = 'Add Tarif Paket Pemeriksaan';
            $data['mode']       = 'add';
            $data['jsmode']     = 'input';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata-mastertarifpaketpemeriksaan'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_mastertarifpaketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data              = $this->settingmastertarifpaketpemeriksaan();
            $data['idmastertarifpaketpemeriksaan']   = $this->uri->segment('3');
            $data['title_page'] = 'Edit Tarif Paket Pemeriksaan';
            $data['mode']      = 'edit';
            $data['jsmode']    = 'input';
            $data['plugins']   = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata-mastertarifpaketpemeriksaan'];
            $this->load->view('v_index', $data);
        }
    }

    public function formready_mastertarifpaketpemeriksaan()
    {
        $id = ((empty($this->input->post('idmastertarifpaketpemeriksaan'))) ? 0 : json_decode($this->encryptbap->decrypt_urlsafe("TARIFPAKET", $this->input->post('idmastertarifpaketpemeriksaan'))));

        $data['datatarif']      = empty($id) ? '' : $this->db->get_where('rs_mastertarif_paket_pemeriksaan', ['idmastertarifpaketpemeriksaan' => $id])->row_array();
        $data['datapaket']      = $this->db->select('idpaketpemeriksaan as id, namapaketpemeriksaan as txt')->get_where('rs_paket_pemeriksaan')->result();
        $data['kelas']          = $this->db->select('idkelas as id, kelas as txt')->get("rs_kelas")->result();
        $data['jenisicd']       = $this->db->select('idjenisicd as id, jenisicd as txt')->get("rs_jenisicd")->result();
        $data['jenistarif']     = $this->db->select('idjenistarif as id, jenistarif as txt')->get("rs_jenistarif")->result();
        $data['coapendapatan']  = empty($id) ? [] : $this->db->select('kode as id, concat("[ ",kode,"] ",nama) as txt')->get_where('keu_akun', ['kode' => $data['datatarif']['coapendapatan']])->result_array();
        echo json_encode($data);
    }
    //simpan tarif paket pemeriksaan
    public function save_mastertarifpaketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            //Persiapkan data
            $post = $this->input->post();
            $id = ((empty($this->input->post('idmastertarifpaketpemeriksaan'))) ? 0 : json_decode($this->encryptbap->decrypt_urlsafe("TARIFPAKET", $this->input->post('idmastertarifpaketpemeriksaan'))));

            $datatarif = [
                'idkelas' => $post['idkelas'],
                'idpaketpemeriksaan' => $post['idpaketpemeriksaan'],
                'isbolehedit' => $post['isbolehedit'],
                'idjenistarif' => $post['idjenistarif'],
                'coapendapatan' => isset($post['coapendapatan']) ? $post['coapendapatan'] : '',
                'jasaoperator' => $post['jasaoperator'],
                'nakes' => $post['nakes'],
                'jasars' => $post['jasars'],
                'bhp' => $post['bhp'],
                'akomodasi' => $post['akomodasi'],
                'margin' => $post['margin'],
                'sewa' => $post['sewa']
            ];

            $this->mgenerikbap->setTable('rs_mastertarif_paket_pemeriksaan');
            $simpan = $this->mgenerikbap->update_or_insert_replaceonduplicate($datatarif, $id);
            pesan_success_danger($simpan, empty($id) ? 'Simpan Tarif Paket Berhasil.' : 'Ubah Tarif Paket Berhasil', empty($id) ? 'Simpan Tarif Paket Gagal.!' : 'Ubah Tarif Paket Gagal.', 'js');
        }
    }

    public function hapus_mastertarifpaketpemeriksaan()
    {
        $id = json_decode($this->encryptbap->decrypt_urlsafe("TARIFPAKET", $this->input->post('id')));
        $arrMsg = $this->db->delete('rs_mastertarif_paket_pemeriksaan', ['idmastertarifpaketpemeriksaan' => $id]);
        pesan_success_danger($arrMsg, 'Hapus Tarif Paket Pemeriksaan Berhasil.', 'Hapus Tarif Paket Pemeriksaan Gagal.', 'js');
    }

    ///////////////////////MASTERDATA PAKET PEMERIKSAAN ///////////////////
    //-------------------------- ^^ Standar
    public function settingpaketpemeriksaan()
    {
        return [
            'content_view'      => 'masterdata/v_paketpemeriksaan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'paketpemeriksaan',
            'active_menu_level' => 'paketpemeriksaan'
        ];
    }
    public function paketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data                = $this->settingpaketpemeriksaan();
            $data['title_page']  = 'Paket Pemeriksaan';
            $data['mode']        = 'view';
            $data['jsmode']      = 'view';
            $data['table_title'] = 'Paket Pemeriksaan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = ['js_masterdata-paketpemeriksaan'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function dt_paketpemeriksaan()
    {
        $getData = $this->mdatatable->dt_paketpemeriksaan_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $this->encryptbap->generatekey_once("PAKETPEMERIKSAAN");
                $id = $this->encryptbap->encrypt_urlsafe($obj->idpaketpemeriksaan, "json");
                $row = [];
                $row[] = $obj->namapaketpemeriksaan;
                $row[] = $obj->namapaketpemeriksaancetak;
                $row[] = $obj->jenisicd;
                $row[] = $obj->namarule;
                $row[] = $obj->jenis;
                $row[] =  '<a href="' . base_url() . 'cmasterdata/detail_paketpemeriksaan/' . $id . '" class="btn btn-info btn-xs" ' . ql_tooltip('Detail Paket') . '><i class="fa fa-list"></i></a> '
                    . '<a href="' . base_url() . 'cmasterdata/edit_paketpemeriksaan/' . $id . '" class="btn btn-warning btn-xs" ' . ql_tooltip('Ubah Paket') . '><i class="fa fa-edit"></i></a> '
                    . '<a id="hapuspaket" namapaket="' . $obj->namapaketpemeriksaan . '" idpaket="' . $id . '" class="btn btn-danger btn-xs" ' . ql_tooltip('Hapus Paket') . '><i class="fa fa-trash"></i></a> ';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_paketpemeriksaan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_paketpemeriksaan(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function add_paketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data               = $this->settingpaketpemeriksaan();
            $data['idpaket']    = '';
            $data['title_page'] = 'Add Paket Pemeriksaan';
            $data['mode']       = 'add';
            $data['jsmode']     = 'input';
            $data['plugins']    = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']  = ['js_masterdata-paketpemeriksaan'];
            $this->load->view('v_index', $data);
        }
    }
    public function formready_paketpemeriksaan()
    {
        $id = 0;
        if ($this->input->post('id') != '') {
            $id = json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $this->input->post('id')));
        }
        $wherenot_idpaket = (empty($id)) ? '' : " and idpaketpemeriksaan!='" . $id . "'";
        $data['datapaket'] = empty($id) ? '' : $this->db->get_where('rs_paket_pemeriksaan', ['idpaketpemeriksaan' => $id])->row_array();
        $data['jenisicd']  = $this->db->select('idjenisicd as id, jenisicd as txt')->get("rs_jenisicd")->result();
        $data['jeniscetak'] = $this->db->select('idjeniscetak as id, jenis as txt')->get("rs_jeniscetaklaboratorium")->result();
        $data['paketrule'] = $this->db->select('idpaketpemeriksaanrule as id, namarule as txt')->get("rs_paket_pemeriksaan_rule")->result();
        $data['paketpaket'] = $this->db->query("select * from rs_paket_pemeriksaan where paket_paket ='' and idpaketpemeriksaanparent ='0'" . $wherenot_idpaket)->result();
        echo json_encode($data);
    }

    public function formready_detailpaketpemeriksaan()
    {
        $id = json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $this->input->post('id')));
        $wherenot_idpaket = (empty($id)) ? '' : " and idpaketpemeriksaan!='" . $id . "'";
        $data['datapaket'] = empty($id) ? '' : $this->db->get_where('rs_paket_pemeriksaan', ['idpaketpemeriksaan' => $id])->row_array();
        $data['detailpaket'] = (empty($id)) ? '' : $this->db->query('select c.namapaketpemeriksaan, a.*,b.namaicd, b.nilaidefault, b.satuan, concat(ifnull(nilaiacuanrendah,""),if(nilaiacuantinggi="",""," - "),ifnull(nilaiacuantinggi,"")) nilairujukan from rs_paket_pemeriksaan_detail a join rs_icd b on b.icd=a.icd join rs_paket_pemeriksaan c on c.idpaketpemeriksaan=a.idpaketpemeriksaan where  a.idpaketpemeriksaanparent="' . $id . '" order by idpaketpemeriksaan, b.namaicd asc')->result();
        echo json_encode($data);
    }

    public function edit_paketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data              = $this->settingpaketpemeriksaan();
            $data['idpaket']   = $this->uri->segment('3');
            $data['title_page'] = 'Edit Paket Pemeriksaan';
            $data['mode']      = 'edit';
            $data['jsmode']    = 'input';
            $data['plugins']   = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js'] = ['js_masterdata-paketpemeriksaan'];
            $this->load->view('v_index', $data);
        }
    }
    public function detail_paketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            $data              = $this->settingpaketpemeriksaan();
            $data['idpaket']   = $this->uri->segment('3');
            $data['title_page'] = 'Detail Paket Pemeriksaan';
            $data['mode']      = 'detail';
            $data['jsmode']    = 'detail';
            $data['plugins']   = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js'] = ['js_masterdata-paketpemeriksaan'];
            $this->load->view('v_index', $data);
        }
    }
    public function get_rs_paket_pemeriksaan_detail()
    {
        $id = json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $this->input->post('id')));
        $post = $this->input->post();
        $response = [];
        switch ($this->input->post('mode')) {
            case 'tambahicd':
                $response['icd'] = '';
                $response['paketparent'] = $this->db->query("select idpaketpemeriksaan, namapaketpemeriksaan from rs_paket_pemeriksaan where idpaketpemeriksaan='" . $id . "' or parent_parent='" . $id . "' order by idpaketpemeriksaan asc")->result();
                break;
            case 'ubahicd':
                $response['icd']         = $this->db->query("select icd, (select namaicd from rs_icd where icd=rppd.icd) as namaicd from rs_paket_pemeriksaan_detail rppd where rppd.idpaketpemeriksaandetail='" . $post['idpaketdetail'] . "'")->row_array();
                $response['paketparent'] = $this->db->query("select idpaketpemeriksaan, namapaketpemeriksaan from rs_paket_pemeriksaan where idpaketpemeriksaan='" . $id . "' or parent_parent='" . $id . "' order by idpaketpemeriksaan asc")->result();
                break;
            case 'tambahpaket':
                $response['paketparent'] = $this->db->query("select idpaketpemeriksaan, namapaketpemeriksaan from rs_paket_pemeriksaan where (idpaketpemeriksaan='" . $id . "' or parent_parent='" . $id . "') and ispunyachild='1' order by idpaketpemeriksaan asc")->result_array();
                $response['jenisicd']    = $this->db->query("select idjenisicd, (select jenisicd from rs_jenisicd where idjenisicd = rpp.idjenisicd) as namajenisicd from rs_paket_pemeriksaan rpp where idpaketpemeriksaan='" . $id . "'")->row_array();
                break;
            case 'ubahpaket':
                $response['paketparent'] = $this->db->query("select idpaketpemeriksaan, namapaketpemeriksaan from rs_paket_pemeriksaan where (idpaketpemeriksaan='" . $id . "' or parent_parent='" . $id . "') and ispunyachild='1' order by idpaketpemeriksaan asc")->result_array();
                $response['jenisicd']    = $this->db->query("select idjenisicd, (select jenisicd from rs_jenisicd where idjenisicd = rpp.idjenisicd) as namajenisicd from rs_paket_pemeriksaan rpp where idpaketpemeriksaan='" . $id . "'")->row_array();
                $response['detailpaket'] = $this->db->query("select idpaketpemeriksaanparent, namapaketpemeriksaan, ispunyachild from rs_paket_pemeriksaan rpp where idpaketpemeriksaan='" . $this->input->post('idpaketpemeriksaan_selected') . "'")->row_array();
                break;
        }

        echo json_encode($response);
    }


    //simpan atau ubah paket pemeriksaan detail
    public function insert_or_update_rs_paket_pemeriksaan_detail()
    {
        $post = $this->input->post();
        $idpaketpemeriksaanparent = (($post['detail_formmode'] == 'tambahicd' || $post['detail_formmode'] == 'ubahicd') ? json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $post['detail_idpaketpemeriksaanparent'])) : json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $post['detail_parent_parent'])));
        $id = $post['detail_idpaketpemeriksaandetail'];

        if ($post['detail_formmode'] == 'tambahicd' or $post['detail_formmode'] == 'ubahicd') {
            $data = [
                'idpaketpemeriksaan' => $post['detail_idpaketpemeriksaan'],
                'idpaketpemeriksaanparent' => $idpaketpemeriksaanparent,
                'icd' => $post['detail_icd']
            ];
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan_detail');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);
        } else if ($post['detail_formmode'] == 'tambahpaket') {
            $data = [
                'namapaketpemeriksaan' => $post['detail_idpaketpemeriksaan'],
                'idpaketpemeriksaanparent' => $post['detail_idpaketpemeriksaanparent'],
                'parent_parent' => $idpaketpemeriksaanparent,
                'idjenisicd' => $post['detail_idjenisicd'],
                'ispunyachild' => $post['detail_ispunyachild']
            ];
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            $id = '';
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);
        } else if ($post['detail_formmode'] == 'ubahpaket') {
            $data = [
                'namapaketpemeriksaan' => $post['detail_idpaketpemeriksaan'],
            ];
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            $id = $this->input->post('detail_idpaketpemeriksaan_selected');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);
        }
        $mode = empty($id) ? ' Tambah ' : ' Ubah ';
        pesan_success_danger($arrMsg, $mode . ' DetailPaket Pemeriksaan', $mode . 'Detail Paket Pemeriksaan', 'js');
    }

    public function delete_rs_paket_pemeriksaan_detail()
    {
        $post = $this->input->post('modedelete');
        if ($post == 'icd') {
            $arrMsg = $this->db->delete('rs_paket_pemeriksaan_detail', ['idpaketpemeriksaandetail' => $this->input->post('idpaketdetail')]);
        } else if ($post == 'paketpemeriksaan') {
            $arrMsg = $this->db->delete('rs_paket_pemeriksaan', ['idpaketpemeriksaan' => $this->input->post('idpaketpemeriksaan')]);
            $arrMsg = $this->db->delete('rs_paket_pemeriksaan_detail', ['idpaketpemeriksaan' => $this->input->post('idpaketpemeriksaan')]);
        }
        pesan_success_danger($arrMsg, 'Hapus Paket Pemeriksaan', 'Hapus Paket Pemeriksaan', 'js');
    }


    public function save_paketpemeriksaan() // SIMPAN paketpemeriksaan
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            //Persiapkan data
            $post = $this->input->post();
            $id = ((empty($this->input->post('idpaketpemeriksaan'))) ? '' : json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $this->input->post('idpaketpemeriksaan'))));
            //paket pemeriksaan
            $paketpaket = ((empty($post['paketpaket'])) ? '' : implode(',', $post['paketpaket']));

            $dtpaketperiksa = [
                'namapaketpemeriksaan' => $post['namapaketpemeriksaan'],
                'namapaketpemeriksaancetak' => $post['namahasilcetak'],
                'idjenisicd' => $post['idjenisicd'],
                'paket_paket' => $paketpaket,
                'idpaketpemeriksaanrule' => $post['idpaketpemeriksaanrule'],
                'idjeniscetak' => $post['idjeniscetak'],
                'ispunyachild' => $post['ispunyachild'],
                'iscetakbarcode' => $post['iscetakqrcode'],
                'isubahwaktuhasil' => $post['isubahwaktuhasil']
            ];
            //set table
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            //simpan
            $mode = empty($id) ? 'Simpan' : 'Ubah';
            $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($dtpaketperiksa, $id);
            pesan_success_danger($simpan, $mode . ' Paket Pemeriksaan Berhasil.', $mode . ' Paket Pemeriksaan Gagal.', 'js');
        }
    }
    public function hapus_paketpemeriksaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)) //lihat define di atas
        {
            //Persiapkan data
            $id = json_decode($this->encryptbap->decrypt_urlsafe("PAKETPEMERIKSAAN", $this->input->post('id')));
            $arrMsg = $this->db->delete('rs_paket_pemeriksaan', ['idpaketpemeriksaan' => $id]);
            $arrMsg = $this->db->delete('rs_paket_pemeriksaan_detail', ['idpaketpemeriksaan' => $id]);
            pesan_success_danger($arrMsg, 'Hapus Paket Pemeriksaan Berhasil.', 'Hapus Paket Pemeriksaan Gagal.', 'js');
        }
    }
    //    public function insert_paketchildpemeriksaan()
    //    {
    //        $post=$this->input->post();
    //        $dtpaket = ['namapaketpemeriksaan' =>$post['namapaket'],'idjenisicd'=>$post['idjenisicd'],'ispunyachild'=>$post['ispunyachild'],'idpaketpemeriksaanparent' =>$post['idparent'],'parent_parent'=>$post['parent_parent']];//data yang disimpan
    //        $simpan = $this->db->insert('rs_paket_pemeriksaan',$dtpaket);//simpan
    //        
    //        $arrMsg=['idpaket'=>$this->db->insert_id(),'idparent'=>$post['idparent'],'namapaket'=>$post['namapaket'],'ischild'=>$post['ispunyachild'],'status'=>(($simpan)?'success':'danger'),'message'=>(($simpan)? 'Tambah Paket Child Berhasil.' : 'Tambah Paket Child Gagal.')];
    //        echo json_encode($arrMsg);
    //    }
    ///////////////////////MASTERDATA PAKET PEMERIKSAANDETAIL ///////////////////
    //-------------------------- ^^ Standar
    public function settingpaketpemeriksaandetail()
    {
        return [
            'content_view'      => 'masterdata/v_paketpemeriksaandetail',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'inventaris',
            'active_menu_level' => 'paketpemeriksaandetail'
        ];
    }
    public function paketpemeriksaandetail()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL)) //lihat define di atas
        {
            $data                = $this->settingpaketpemeriksaandetail();
            $data['data_list']   = $this->db->query("select distinct ri.*, rpd.idpaketpemeriksaandetail, rj.jenisicd, rpp.namapaketpemeriksaan from rs_paket_pemeriksaan_detail rpd join rs_icd ri on ri.icd = rpd.icd join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rpd.idpaketpemeriksaan join rs_jenisicd rj on rj.idjenisicd = ri.idjenisicd where ri.icd = rpd.icd and rpp.idpaketpemeriksaan = rpd.idpaketpemeriksaan")->result();
            $data['title_page']  = 'Paket Pemeriksaan Detail';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Paket Pemeriksaan Detail';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function paketpemeriksaandetail_cariicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL)) //lihat define di atas
        {
            $id = $this->input->get('i');
            echo json_encode($this->db->query("select * from rs_icd ri where ri.icd like '%$id%' or ri.namaicd like '%$id%'")->result());
        } else {
            aksesditolak();
        }
    }
    public function add_paketpemeriksaandetail()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL)) //lihat define di atas
        {
            $data               = $this->settingpaketpemeriksaandetail();
            $this->load->helper('form');
            $data['data_edit']  = '';
            $this->db->where("ispunyachild!=", 1);
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            $data['data_paket'] = $this->mgenerikbap->select('*')->result();
            // $this->mgenerikbap->setTable('rs_icd');
            // $data['data_icd'] = $this->mgenerikbap->select('*')->result();
            $data['title_page'] = 'Add Paket Pemeriksaan Detail';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata-paketpemeriksaandetail'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_paketpemeriksaandetail()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL)) //lihat define di atas
        {
            $data                 = $this->settingpaketpemeriksaandetail();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan_detail');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']    = $this->db->query("select DISTINCT d.*, i.namaicd  FROM rs_paket_pemeriksaan_detail d, rs_icd i WHERE d.idpaketpemeriksaandetail='$id' and i.icd = d.icd")->row_array();
            $this->db->where("ispunyachild!=", 1);
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            $data['data_paket'] = $this->mgenerikbap->select('*')->result();
            // $this->mgenerikbap->setTable('rs_icd');
            // $data['data_icd'] = $this->mgenerikbap->select('*')->result();
            $data['title_page']   = 'Edit Paket Pemeriksaan Detail';
            $data['mode']         = 'edit';
            $data['plugins']      = [PLUG_DROPDOWN];
            $data['script_js']    = ['js_masterdata-paketpemeriksaandetail'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_paketpemeriksaandetail() // SIMPAN paketpemeriksaandetail
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('idpaket', '', 'required');
            $this->form_validation->set_rules('icd', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $id =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id')));
                $data = ['idpaketpemeriksaan'   => $this->input->post('idpaket'), 'icd' => $this->input->post('icd')]; //set data yang akan disimpan
                $this->mgenerikbap->setTable('rs_paket_pemeriksaan_detail'); //set table
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id); //Simpan
                pesan_success_danger($simpan, empty($id) ? 'Save Success.!' : 'Update Success.!', empty($id) ? 'Save  Failed.!' : 'Update Failed.!'); //jika simpan status success selain itu status error
            }
            redirect(base_url('cmasterdata/paketpemeriksaandetail')); //kembali ke halaman paketpemeriksaandetail
        }
    }
    public function cariicdpaket_pemeriksaan()
    {
        $icd = $this->input->get('icd');
        $this->db->select("icd, namaicd, aliasicd, nilaidefault, satuan, concat(ifnull(nilaiacuanrendah,''),if(nilaiacuantinggi='','',' - '),ifnull(nilaiacuantinggi,'')) nilairujukan, ");
        $this->db->like("namaicd", "$icd");
        $this->db->or_like("icd", "$icd");
        $this->db->or_like("aliasicd", "$icd");
        echo json_encode($this->db->get_where('rs_icd', ['idjenisicd!=2'])->result());
    }
    ///////////////////////MASTERDATA GRUPPEGAWAI ///////////////////
    //-------------------------- ^^ Standar

    public function settinggruppegawai()
    {
        return [
            'content_view'      => 'masterdata/v_gruppegawai',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'demografi',
            'active_menu_level' => 'gruppegawai'
        ];
    }

    public function gruppegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GRUPPEGAWAI)) //lihat define di atas
        {
            $data                = $this->settinggruppegawai();
            $data['data_list']   = $this->db->get('person_grup_pegawai')->result();
            $data['title_page']  = 'Grup Pegawai';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Grup Pegawai';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function add_gruppegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GRUPPEGAWAI)) //lihat define di atas
        {
            $data               = $this->settinggruppegawai();
            $this->load->helper('form');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Grup Pegawai';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_gruppegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GRUPPEGAWAI)) //lihat define di atas
        {
            $data                 = $this->settinggruppegawai();
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->db->get_where('person_grup_pegawai', ['idgruppegawai' => $id])->row_array();

            $data['title_page'] = 'Edit Grup Pegawai';
            $data['mode']       = 'edit';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    // SIMPAN GRUPPEGAWAI
    public function save_gruppegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GRUPPEGAWAI)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('namagruppegawai', '', 'required');
            //Jika Valid maka lanjutkan proses simpan
            if (validation_input()) {
                //Persiapkan data
                $idgruppegawai =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idgruppegawai')));
                $data = ['namagruppegawai' => $this->input->post('namagruppegawai')];
                //Simpan
                $this->mgenerikbap->setTable('person_grup_pegawai');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idgruppegawai);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idgruppegawai) ? 'Save Success.!' : 'Update Success.!', empty($idgruppegawai) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/gruppegawai'));
        }
    }

    ///////////////////////MASTERDATA INSTALASI///////////////////
    public function settinginstalasi()
    {
        return [
            'content_view'      => 'masterdata/v_instalasi',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'instalasi'
        ];
    }
    public function instalasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INSTALASI)) //lihat define di atas
        {
            $data                   = $this->settinginstalasi(); //letakkan di baris pertama
            $data['data_list']      = $this->mgenerikbap->select_multitable("ri.idinstalasi, ri.namainstalasi, ri.kodeinstalasi, pper.namalengkap", "rs_instalasi, person_pegawai, person_person")->result();
            $data['title_page']     = 'Instalasi';
            $data['mode']           = 'view';
            $data['table_title']    = 'List Data Instalasi ';
            $data['plugins']        = [PLUG_DATATABLE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }


    public function add_instalasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INSTALASI)) //lihat define di atas
        {
            $data               = $this->settinginstalasi();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('person_pegawai');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['data_pegawai']   = $this->mgenerikbap->select_multitable("ppg.idpegawai,pper.namalengkap", "person_pegawai, person_person")->result();
            $data['data_akses']     = $this->mgenerikbap->select('*')->result_array();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Instalasi';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_instalasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INSTALASI)) //lihat define di atas
        {
            $data                = $this->settinginstalasi();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('person_pegawai');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['data_pegawai']   = $this->mgenerikbap->select_multitable("ppg.idpegawai,pper.namalengkap", "person_pegawai, person_person")->result();
            $this->mgenerikbap->setTable('rs_instalasi');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']      = $this->mgenerikbap->select('*', ['idinstalasi' => $id])->row_array();
            $data['title_page']     = 'Edit Instalasi';
            $data['mode']           = 'edit';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    // SIMPAN INSTALASI
    public function save_instalasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INSTALASI)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('idpegawai', '', 'required');
            $this->form_validation->set_rules('namainstalasi', '', '');
            $this->form_validation->set_rules('kodeinstalasi', '', '');
            //Jika tidak Valid
            if (validation_input()) {
                //Persiapkan data
                $idinstalasi   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idinstalasi')));
                $namainstalasi = $this->input->post('namainstalasi');
                $kodeinstalasi = $this->input->post('kodeinstalasi');
                $idpegawai     = $this->input->post('idpegawai');
                $data = ['namainstalasi' => $namainstalasi, 'kodeinstalasi' => $kodeinstalasi, 'idpegawaika' => $idpegawai];
                //Simpan
                $this->mgenerikbap->setTable('rs_instalasi');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idinstalasi);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idinstalasi) ? 'Save Success.!' : 'Update Success.!', empty($idinstalasi) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/instalasi'));
        }
    }

    ///////////////////////MASTERDATA UNIT///////////////////
    public function settingunit()
    {
        return [
            'content_view'      => 'masterdata/v_unit',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'unit'
        ];
    }
    public function unit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_UNIT)) //lihat define di atas
        {
            $data                   = $this->settingunit(); //letakkan di baris pertama
            $data['data_list']      = $this->mmasterdata->masterdata_getdataunit();
            $data['title_page']     = 'Unit';
            $data['mode']           = 'view';
            $data['table_title']    = 'List Data Unit ';
            $data['plugins']        = [PLUG_DATATABLE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function add_unit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_UNIT)) //lihat define di atas
        {

            $data               = $this->settingunit();
            $this->load->helper('form');
            $data['data_pegawai']           = $this->db->join('person_person pper', 'pper.idperson = ppai.idperson')->get('person_pegawai ppai')->result();
            $data['data_instalasi']         = $this->db->get('rs_instalasi')->result();
            $data['data_paketpemeriksaan']  = $this->db->get('rs_paket_pemeriksaan')->result();
            $data['data_jenisicd']          = $this->db->get('rs_jenisicd')->result_array();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Unit';
            $data['mode']       = 'add';
            $data['jsmode']     = 'formunit';
            $data['plugins']    = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_unit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_UNIT)) //lihat define di atas
        {
            $data                = $this->settingunit();
            $this->load->helper('form');
            $data['data_pegawai']           = $this->db->join('person_person pper', 'pper.idperson = ppai.idperson')->get('person_pegawai ppai')->result();
            $data['data_instalasi']         = $this->db->get('rs_instalasi')->result();
            $data['data_paketpemeriksaan']  = $this->db->get('rs_paket_pemeriksaan')->result();
            $data['data_jenisicd']          = $this->db->get('rs_jenisicd')->result_array();

            $idunit = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']  = $this->db->get_where('rs_unit', ['idunit' => $idunit])->row_array();
            $data['title_page'] = 'Edit Unit';
            $data['mode']       = 'edit';
            $data['jsmode']     = 'formunit';
            $data['plugins']    = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    //cek apakah kode unit sudah ada
    public function cek_kodeunit()
    {
        $akronimunit = $this->input->post('akronimunit');
        $idunit   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idunit')));

        //cek kode sudah digunakan diloket
        $cek = $this->db->select('idunit')->get_where('rs_unit', ['akronimunit' => $akronimunit]);
        $result = array();
        $result['status']   = 'success';
        $result['idunit']  = $idunit;

        if ($cek->num_rows() > 0) {
            $unit = $cek->row_array();

            if ($unit['idunit'] == $idunit) {
                //cek kode sudah digunakan di loket
                $cek = $this->db->get_where('antrian_loket', ['kodeloket' => $akronimunit]);
                if ($cek->num_rows() > 0) {
                    $result['status']  = 'warning';
                    $result['message'] = 'Kode Unit Sudah Digunakan di Kode Loket, Harap Masukan Kode Yang Lain.';
                }

                echo json_encode($result);
                return;
            }

            //kode sudah digunakan di loket
            $result['status']  = 'warning';
            $result['message'] = 'Kode Unit Sudah Digunakan, Harap Masukan Kode Yang Lain.';
        }

        //cek kode sudah digunakan di loket
        $cek = $this->db->get_where('antrian_loket', ['kodeloket' => $akronimunit]);
        if ($cek->num_rows() > 0) {
            $result['status']  = 'warning';
            $result['message'] = 'Kode Unit Sudah Digunakan di Kode Loket, Harap Masukan Kode Yang Lain.';
        }

        echo json_encode($result);
    }

    // SIMPAN UNIT
    public function save_unit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_UNIT)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('namaunit', '', 'required');
            $this->form_validation->set_rules('akronimunit', '', 'required');
            $this->form_validation->set_rules('idpegawaika', '', '');
            $this->form_validation->set_rules('idinstalasi', '', '');
            //Jika tidak Valid
            if (validation_input()) {
                //Persiapkan data
                $idunit      = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idunit')));
                $namaunit    = $this->input->post('namaunit');
                $akronimunit = strtoupper($this->input->post('akronimunit'));
                $kodebpjs    = $this->input->post('kodebpjs');
                $idpegawai   = $this->input->post('idpegawai');
                $idinstalasi = $this->input->post('idinstalasi');
                $idpaketpemeriksaan = $this->input->post('idpaketpemeriksaan');

                $hakinputidjenisicd = $this->input->post('hakinputidjenisicd');
                empty($hakinputidjenisicd) ? $hakinput = '' : $hakinput = implode(',', $hakinputidjenisicd);
                $data = [
                    'namaunit' => $namaunit,
                    'akronimunit' => $akronimunit,
                    'idpegawaika' => $idpegawai,
                    'idinstalasi' => $idinstalasi,
                    'idpaketpemeriksaan' => $idpaketpemeriksaan,
                    'hakinputidjenisicd' => $hakinput,
                    'kodebpjs' => $kodebpjs
                ];
                //Simpan
                $this->mgenerikbap->setTable('rs_unit');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idunit);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idunit) ? 'Save Success.!' : 'Update Success.!', empty($idunit) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/unit'));
        }
    }

    ///////////////////////MASTERDATA BARANG ///////////////////
    //-------------------------- ^^ Standar
    public function settingbarang()
    {
        return [
            'content_view'      => 'masterdata/v_barang',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'inventaris',
            'active_menu_level' => 'barang'
        ];
    }
    public function barang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                = $this->settingbarang();
            $data['title_page']  = 'Barang';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Barang';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['jsmode']      = 'list';
            $data['script_js']   = ['js_inventarisbarang'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    // -- tampilkan data master barang
    public  function dt_barang() //menampilkan stok barang sementara
    {
        $list   = $this->mdatatable->dt_barang_(true);

        $no = $_POST['start'];
        foreach ($list->result() as $obj) {
            $this->encryptbap->generatekey_once("HIDDENTABEL");
            $id           = $this->encryptbap->encrypt_urlsafe($obj->idbarang, "json");
            $tabel        = $this->encryptbap->encrypt_urlsafe('rs_barang', "json");
            $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_BARANG, "json");

            $menu  = '<a data-toggle="tooltip" id="btnRiwayatBarang" kodebrg="' . $obj->kode . '" namabrg="' . $obj->namabarang . '" dt="' . $id . '" data-original-title="History" class="btn btn-info btn-xs"><i class="fa fa-history"></i></a>';
            $menu .= ' <a data-toggle="tooltip" title="" data-original-title="Edit" class="btn btn-warning btn-xs" href="' . base_url('cfarmasi/edit_barang/' . $id) . '" ><i class="fa fa-pencil"></i></a>';

            if ($this->pageaccessrightbap->checkAccessRight(V_MENU_HAPUS_DATABARANG)) {
                $menu .= ' <a data-toggle="tooltip" title="" data-original-title="Delete" id="delete_data" nobaris="' . ($no - 1) . '" class="btn btn-danger btn-xs" href="#" alt="' . $tabel . '" alt2="' . $id . '" alt3="' . $idhalaman . '"> <i class="fa fa-trash"></i></a>';
            }
            $row = [];
            $row[] = ++$no;
            $row[] = $obj->namabarang;
            $row[] = $obj->kode;
            $row[] = convertToRupiah($obj->stok);
            $row[] = '<span class="rop-' . status_ROP($obj->stok, $obj->stokaman, $obj->stokminimal) . '">' . $obj->rop . '</span>';
            $row[] = $obj->jenistarif;
            $row[] = $obj->namasatuan;
            $row[] = $obj->namasediaan;
            $row[] = $obj->jenis;
            $row[] = $obj->tipeobat;
            $row[] = convertToRupiah($obj->hargabeli);
            $row[] = convertToRupiah($obj->hargajual);
            $row[] = convertToRupiah($obj->het);
            $row[] = $obj->kekuatan;
            $row[] = ($obj->statusbarang) ? '<span class="label label-success">Digunakan</span>' : '<span class="label label-danger">Tidak Digunakan</span>';
            $row[] = $menu;
            $data[] = $row;
        }
        echo json_encode([
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_barang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_barang(),
            "data" => $data
        ]);
    }

    public function dt_riwayatobat()
    {
        $getData = $this->mdatatable->dt_riwayatobat_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->namapbf;
                $row[] = $obj->distributor;
                $row[] = $obj->waktu;
                $row[] = $obj->batchno;
                $row[] = $obj->kadaluarsa;
                $row[] = convertToRupiah($obj->hargabeli);
                $row[] = convertToRupiah($obj->nominaldiskon);
                $row[] = $obj->jumlah;
                $row[] = $obj->jenisdistribusi;
                $row[] = $obj->unitasal;
                $row[] = $obj->unittujuan;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_riwayatobat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_riwayatobat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    // -- add data barang
    public function add_barang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                  = $this->settingbarang(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page']    = 'Add Barang';
            $data['mode']          = 'add';
            $data['jsmode']        = 'form';
            $data['dt_jenis']      = $this->mmasterdata->get_data_enum('rs_barang', 'jenis');
            $data['dt_satuan']     = $this->db->get('rs_satuan')->result();
            $data['dt_jenistarif'] = $this->db->get('rs_jenistarif')->result();
            $data['dt_pabrik']     = $this->db->get('rs_barangpabrik')->result();
            $data['data_sediaan']  = $this->db->get('rs_sediaan')->result();
            $data['dt_tipeobat']   = $this->mmasterdata->get_data_enum('rs_barang', 'tipeobat');
            $data['data_edit']     = '';
            $data['plugins']       = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']     = ['js_inventarisbarang'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    // --  
    public function barang_carigolbyjenis()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('rs_barang_golongan');
            $jenis = $this->input->post('jenis');
            echo json_encode($this->mgenerikbap->select('*', ['jenis' => $jenis])->result());
        } else {
            aksesditolak();
        }
    }
    public function edit_barang() //edit data pegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                 = $this->settingbarang(); //letakkan di baris pertama
            $this->load->helper('form'); //load form helper
            $data['title_page']   = 'Edit Barang'; //judul halaman
            $data['mode']         = 'edit';
            $data['jsmode']       = 'form';
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['dt_jenis']      = $this->mmasterdata->get_data_enum('rs_barang', 'jenis');
            $data['dt_satuan']     = $this->db->get('rs_satuan')->result();
            $data['dt_jenistarif'] = $this->db->get('rs_jenistarif')->result();
            $data['dt_pabrik']     = $this->db->get('rs_barangpabrik')->result();
            $data['data_sediaan']  = $this->db->get('rs_sediaan')->result();
            $data['dt_tipeobat']   = $this->mmasterdata->get_data_enum('rs_barang', 'tipeobat');
            $data['data_edit']     = $this->db->query("select * from rs_barang a  where a.idbarang='" . $id . "'")->row_array();
            $data['data_golongan'] = $this->db->get_where('rs_barang_golongan', ['jenis' => $data['data_edit']['jenis']])->result();
            $data['plugins']       = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']     = ['js_inventarisbarang'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function save_barang() // SIMPAN KECAMATAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('idsatuan', '', 'required');
            $this->form_validation->set_rules('idsediaan', '', 'required');
            $this->form_validation->set_rules('idjenistarif', '', 'required');
            $this->form_validation->set_rules('namabarang', '', 'required');
            $this->form_validation->set_rules('keluhan', '', '');
            $this->form_validation->set_rules('jenis', '', 'required');
            $this->form_validation->set_rules('golongan', '', 'required');
            $this->form_validation->set_rules('hargabeli', '', 'required');
            $this->form_validation->set_rules('persenmargin', '', '');
            // $this->form_validation->set_rules('tatacara','','');
            $this->form_validation->set_rules('kekuatan', '', 'required');

            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $post = $this->input->post();
                $idbarang = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['idbarang']));
                $data['namabarang'] = $post['namabarang'];
                $data['stokminimal'] = $post['stokminimal'];
                $data['stokhabis']  = $post['stokhabis'];
                $data['stokaman']   = $post['stokaman'];
                $data['rop']        = $post['rop'];
                $data['idjenistarif'] = $post['idjenistarif'];
                $data['idsatuan']   = $post['idsatuan'];
                $data['idsediaan']  = $post['idsediaan'];
                $data['jenis']      = $post['jenis'];
                $data['idgolongan'] = $post['golongan'];
                $data['tipeobat']   = $post['tipeobat'];
                $data['hargabeli']  = $post['hargabeli'];
                $data['persenmargin'] = $post['persenmargin'];
                $data['het']        = $post['het'];
                $data['kekuatan']   = $post['kekuatan'];
                $data['keluhan']    = $post['keluhan'];
                $data['kandungan']  = $post['kandungan'];
                $data['indikasi']   = $post['indikasi'];
                $data['kontraindikasi'] = $post['kontraindikasi'];
                $data['efeksamping']    = $post['efeksamping'];
                $data['idbarangpabrik'] = $post['idbarangpabrik'];
                $data['isfornas']   = (($post['isfornas']) ? 1 : 0);
                $this->mgenerikbap->setTable('rs_barang');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idbarang);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idbarang) ? 'Save Success.!' : 'Update Success.!', empty($idbarang) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cfarmasi/barang'));
        }
    }

    //total kekayaan obat dan bhp berdasarkan unit
    public function totalkekayanobatdanbhp()
    {
        if (empty(!$this->session->userdata('idunitterpilih'))) {
            $data = $this->db->query("select sum(a.stok * b.hargabeli) as totalkekayaan from rs_barang_stok a join rs_barang_pembelian b on b.idbarangpembelian=a.idbarangpembelian WHERE idunit = '" . $this->session->userdata('idunitterpilih') . "' and stok > 0");
            if ($data->num_rows() > 0) {
                $data = $data->row_array();
            } else {
                $data = null;
            }
        } else {
            $data = null;
        }

        echo json_encode($data);
    }



    //*******pencarian barang like nama barang
    // jsonbarangwherelikename
    public function jsonbrwlname()
    {
        // cari barang : retur, pesan, transformasi tujuan
        $namaobat = $this->input->post('namaobat');
        $mode   = $this->input->post('mode');
        $idunit = (($mode == 'retur') ? $this->session->userdata('idunitterpilih') : $this->input->post('unittujuan'));
        $query = (($mode) == 'retur' ? "SELECT concat(rb.idbarang,',',rb.kode,',',rb.namabarang,',',namasatuan,',',rb.hargabeli,',',0,',',rbp.kadaluarsa,',',rbp.batchno,',',0,',',bs.idbarangpembelian) as id, concat(rb.kode,' ',rb.namabarang,' ED:',rbp.kadaluarsa,' BATCH.No:',rbp.batchno,' Stok:',bs.stok,' ',namasatuan) as text  from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian  and bs.idunit='" . $idunit . "' where rs.idsatuan=rb.idsatuan and bs.stok > 0 and namabarang like '%" . $namaobat . "%' limit 20" : "SELECT concat(rb.idbarang,',',kode,',',namabarang,',',namasatuan,',', rb.hargabeli,',',0) as id, concat(kode,' ',namabarang,' ( stok ', ifnull(sum(bs.stok),0) ,' ',namasatuan ,' )') as text from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian and bs.idunit='" . $idunit . "' where rs.idsatuan=rb.idsatuan  and namabarang like '%" . $namaobat . "%' GROUP by rb.idbarang limit 20");
        echo json_encode($this->db->query($query)->result());
    }
    public function json_pilihdistributor()
    {
        $offset = ($this->input->get('page') * 20) - 20;
        echo json_encode($this->db->query("SELECT idbarangdistributor, distributor from rs_barang_distributor where distributor like '%" . $this->input->get('q') . "%' limit 20 offset " . $offset)->result());
    }
    // ubah hapus pembelian
    public function setdelete_barangpembelian()
    {
        $post = $this->input->post();
        $arrmsg = $this->db->delete('rs_barang_pembelian', ['idbarangpembelian' => $post['idbp']]);
        if ($post['status'] != 'diterima') {
            $arrmsg = $this->db->delete('rs_barang_distribusi', ['idbarangpembelian' => $post['idbp']]);
        } else {
            $arrmsg = $this->db->query("insert into rs_barang_distribusi (`idbarangpembelian`, `idunit`, `jumlah`, `jenisdistribusi`, `idunittujuan`, `waktu`, `grupwaktu`, `grupasal`, `idbarangpemesanan`, `idpersonpetugas`, `statusdistribusi`) select idbarangpembelian, idunit, jumlah, 'keluar', idunittujuan, waktu, grupwaktu, grupasal, idbarangpemesanan, idpersonpetugas, statusdistribusi from rs_barang_distribusi where idbarangpembelian='" . $post['idbp'] . "'");
        }
        echo json_encode($arrmsg);
    }

    public function updatefaktur_ad_barangpembelian()
    {
        $post = $this->input->post();
        if ($post['mode'] == 'ubah') {
            $arrmsg = $this->db->update('rs_barang_faktur', ['potongan' => $post['potongan'], 'ppn' => $post['ppn'], 'tagihan' => $post['tagihan']], ['idbarangfaktur' => $post['idbf']]);
        } else {
            $arrmsg = $this->db->update('rs_barang_faktur', ['statusfaktur' => 'batal'], ['idbarangfaktur' => $post['idbf']]);
        }
        echo json_encode($arrmsg);
    }

    public function jsonvpembelian()
    {
        $idbarangfaktur = $this->input->post('idbarangfaktur');
        //        $this->encryptbap->generatekey_once("FAKTURBELANJA");
        //        $idfaktur = $this->encryptbap->encrypt_urlsafe(json_encode($idbarangfaktur));
        $idfaktur = $idbarangfaktur;
        $dt_faktur    = $this->db->select('rbf.*, (select distributor from rs_barang_distributor where idbarangdistributor=rbf.idbarangdistributor) distributor')->get_where('rs_barang_faktur rbf', ['rbf.idbarangfaktur' => $idbarangfaktur])->row_array();
        $dt_pembelian = $this->db->query("SELECT rbd.idbarangdistribusi, rbd.idbarangpembelian, rb.idbarang,kode, namabarang, rbp.hargabeli,rbp.hargaasli, namasatuan, CAST(jumlah AS UNSIGNED) as jumlah,rbp.batchno, rbp.kadaluarsa, rbp.persendiskon, rbp.nominaldiskon from rs_barang_faktur rbf, rs_barang_pembelian rbp, rs_barang_distribusi rbd, rs_barang rb, rs_satuan rs where rbp.idbarangfaktur=rbf.idbarangfaktur and (rbd.idbarangpembelian=rbp.idbarangpembelian and jenisdistribusi='masuk') and rb.idbarang=rbp.idbarang and rs.idsatuan=rb.idsatuan and rbf.idbarangfaktur = '" . $idbarangfaktur . "'")->result();
        echo json_encode(['pembelian' => $dt_pembelian, 'faktur' => $dt_faktur, 'idfaktur' => $idfaktur]);
    }

    //** cari barang pembelian where like
    // jsonbarangpembelian_wherelike
    public function jsonbpwlname()
    {
        $namaobat = $this->input->post('namaobat');
        echo json_encode($this->db->query("select concat(bp.idbarangpembelian,',',b.idbarang,',',b.kode,',',b.namabarang,',',s.namasatuan,',',bp.hargabeli,',',bs.stok,',',DATE_FORMAT(bp.kadaluarsa,'%d/%m/%Y'),',',bp.batchno) as id, concat(b.kode,' ',b.namabarang,' (',bs.stok,' ',s.namasatuan,') ','ED: ',bp.kadaluarsa) as text from rs_barang_pembelian bp, rs_barang b, rs_barang_stok bs, rs_satuan s where b.idbarang=bp.idbarang and bs.idbarangpembelian=bp.idbarangpembelian and s.idsatuan=b.idsatuan and b.namabarang like '%" . $namaobat . "%' and bs.stok!='0' and bs.idunit='" . $this->session->userdata('idunitterpilih') . "' order by b.namabarang, bp.kadaluarsa asc limit 50")->result_array());
    }

    //json view tansformasibarang
    public function jsonvtrans()
    {
        $waktu      = $this->input->post("w");
        $grupwaktu  = $this->input->post("gw");
        echo json_encode($this->db->query("select concat(waktu, grupwaktu) as id, bp.idbarangpembelian, jenisdistribusi, bp.idbarang, DATE_FORMAT(bp.kadaluarsa,'%d/%m/%Y') kadaluarsa, bp.batchno,bp.hargabeli, b.namabarang, b.kode, 0 as stok, jumlah, s.namasatuan from rs_barang_distribusi bd, rs_barang_pembelian bp, rs_barang b, rs_satuan s where bp.idbarangpembelian=bd.idbarangpembelian and b.idbarang=bp.idbarang and b.idsatuan=s.idsatuan and (jenisdistribusi='transformasiasal' or jenisdistribusi='transformasihasil') and waktu='" . $waktu . "' and grupwaktu='" . $grupwaktu . "' order by waktu, grupasal, if(jenisdistribusi='transformasiasal',0,1)")->result());
    }
    ///////////////////////MASTERDATA JENIS ICD ///////////////////
    //-------------------------- ^^ Standar
    public function settingjenisicd()
    {
        return [
            'content_view'      => 'masterdata/v_jenisicd',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'icd',
            'active_menu_level' => 'jenisicd'
        ];
    }
    public function jenisicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISICD)) //lihat define di atas
        {
            $data                = $this->settingjenisicd();
            $this->mgenerikbap->setTable('rs_jenisicd');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Jenisicd';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Jenis ICD';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_jenisicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISICD)) //lihat define di atas
        {
            $data               = $this->settingjenisicd();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Jenis ICD';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_jenisicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISICD)) //lihat define di atas
        {
            $data                 = $this->settingjenisicd();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_jenisicd');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idjenisicd' => $id])->row_array();
            $data['title_page'] = 'Edit Jenis ICD';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_jenisicd() // SIMPAN JENIS ICD
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISICD)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('jenisicd', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idjenisicd =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idjenisicd')));
                $jenisicd = $this->input->post('jenisicd');
                $data = ['jenisicd' => $jenisicd];
                //Simpan
                $this->mgenerikbap->setTable('rs_jenisicd');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idjenisicd);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idjenisicd) ? 'Save Success.!' : 'Update Success.!', empty($idjenisicd) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/jenisicd'));
        }
    }
    ///////////////////////MASTERDATA JENIS PENYAKIT ///////////////////
    //-------------------------- ^^ Standar
    public function settingjenispenyakit()
    {
        return [
            'content_view'      => 'masterdata/v_jenispenyakit',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'icd',
            'active_menu_level' => 'jenispenyakit'
        ];
    }
    public function jenispenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data                = $this->settingjenispenyakit();
            $this->mgenerikbap->setTable('rs_jenispenyakit');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Jenis Penyakit STPRS';
            $data['mode']        = 'view';
            //            $data['table_title'] = 'Data Jenis Penyakit';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_jenispenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data               = $this->settingjenispenyakit();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Jenis Penyakit';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_jenispenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data                 = $this->settingjenispenyakit();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_jenispenyakit');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idjenispenyakit' => $id])->row_array();
            $data['title_page'] = 'Edit Jenis Penyakit';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_jenispenyakit() // SIMPAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('jenispenyakit', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $post = $this->input->post();
                $idjenispenyakit =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['idjenispenyakit']));
                $data = [
                    'jenispenyakit' => $post['jenispenyakit']
                ];
                //Simpan
                $this->mgenerikbap->setTable('rs_jenispenyakit');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idjenispenyakit);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idjenispenyakit) ? 'Save Success.!' : 'Update Success.!', empty($idjenispenyakit) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/jenispenyakit'));
        }
    }

    ///////////////////////MASTERDATA JENIS PENYAKIT ///////////////////
    //-------------------------- ^^ Standar
    public function settinggolongansebabpenyakit()
    {
        return [
            'content_view'      => 'masterdata/v_golongansebabpenyakit',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'icd',
            'active_menu_level' => 'golongansebabpenyakit'
        ];
    }
    public function golongansebabpenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data                = $this->settinggolongansebabpenyakit();
            $this->mgenerikbap->setTable('rs_icd_golongansebabpenyakit');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Golongan Sebab Penyakit';
            $data['mode']        = 'view';
            //            $data['table_title'] = 'Data Jenis Penyakit';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_golongansebabpenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data               = $this->settinggolongansebabpenyakit();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Golongan Sebab Penyakit';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata-golongansebabpenyakit'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_golongansebabpenyakit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            $data                 = $this->settinggolongansebabpenyakit();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_icd_golongansebabpenyakit');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idgolongansebabpenyakit' => $id])->row_array();
            $data['title_page'] = 'Edit Golongan Sebab Penyakit';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata-golongansebabpenyakit'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_golongansebabpenyakit() // SIMPAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('golongansebabpenyakit', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $post = $this->input->post();
                $idjenispenyakit =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['idgolongansebabpenyakit']));
                $data = [
                    'golongansebabpenyakit' => $post['golongansebabpenyakit'],
                    'nodtd' => $post['nodtd']
                ];
                //Simpan
                $this->mgenerikbap->setTable('rs_icd_golongansebabpenyakit');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idjenispenyakit);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idjenispenyakit) ? 'Save Success.!' : 'Update Success.!', empty($idjenispenyakit) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/golongansebabpenyakit'));
        }
    }
    ///////////////////////MASTERDATA SEDIAAN ///////////////////
    //-------------------------- ^^ Standar
    public function settingsediaan()
    {
        return [
            'content_view'      => 'masterdata/v_sediaan',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'sediaan',
            'active_menu_level' => ''
        ];
    }
    public function sediaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SEDIAAN)) //lihat define di atas
        {
            $data                = $this->settingsediaan();
            $this->mgenerikbap->setTable('rs_sediaan');
            $data['data_list']   = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Sediaan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Sediaan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_sediaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SEDIAAN)) //lihat define di atas
        {
            $data               = $this->settingsediaan();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Sediaan';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $data['jnssediaan'] = $this->mmasterdata->get_data_enum('rs_sediaan', 'jenissediaan');
            $this->load->view('v_index', $data);
        }
    }
    public function edit_sediaan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SEDIAAN)) //lihat define di atas
        {
            $data                 = $this->settingsediaan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_sediaan');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idsediaan' => $id])->row_array();
            $data['title_page'] = 'Edit Sediaan';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $data['jnssediaan'] = $this->mmasterdata->get_data_enum('rs_sediaan', 'jenissediaan');
            $this->load->view('v_index', $data);
        }
    }
    public function save_sediaan() // SIMPAN SEDIAAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SEDIAAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namasediaan', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idsediaan =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idsediaan')));
                $namasediaan = $this->input->post('namasediaan');
                $jenissediaan = $this->input->post('jenissediaan');
                $data = ['namasediaan' => $namasediaan, 'jenissediaan' => $jenissediaan];
                //Simpan
                $this->mgenerikbap->setTable('rs_sediaan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idsediaan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idsediaan) ? 'Save Success.!' : 'Update Success.!', empty($idsediaan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/sediaan'));
        }
    }
    ///////////////////////MASTERDATA SATUAN ///////////////////
    //-------------------------- ^^ Standar
    public function settingsatuan()
    {
        return [
            'content_view'      => 'masterdata/v_satuan',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'satuan',
            'active_menu_level' => ''
        ];
    }
    public function satuan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SATUAN)) //lihat define di atas
        {
            $data                = $this->settingsatuan();
            $this->mgenerikbap->setTable('rs_satuan');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Satuan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Satuan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_satuan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SATUAN)) //lihat define di atas
        {
            $data               = $this->settingsatuan();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Satuan';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_satuan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SATUAN)) //lihat define di atas
        {
            $data                 = $this->settingsatuan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_satuan');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idsatuan' => $id])->row_array();
            $data['title_page'] = 'Edit Satuan';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_satuan() // SIMPAN SATUAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SATUAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namasatuan', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idsatuan =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idsatuan')));
                $namasatuan = $this->input->post('namasatuan');
                $data = ['namasatuan' => $namasatuan];
                //Simpan
                $this->mgenerikbap->setTable('rs_satuan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idsatuan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idsatuan) ? 'Save Success.!' : 'Update Success.!', empty($idsatuan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/satuan'));
        }
    }
    public function get_satuan()
    {
        //        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $this->mgenerikbap->setTable('rs_satuan');
            echo json_encode($this->mgenerikbap->select('*')->result());
        }
    }

    public function kosongan()
    {
        return json_encode('');
    }
    public function get_dokter()
    {
        //        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $sql = 'SELECT idpegawai, trim(concat(ifnull(titeldepan, ""), " ", namalengkap, " ", ifnull(titelbelakang, ""))) as namalengkap FROM person_pegawai, person_person where idgruppegawai=5 and `person_pegawai`.idperson=person_person.idperson order by namalengkap';
            echo json_encode($this->db->query($sql)->result());
        }
    }
    public function searchmultipegawai()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->like("namalengkap", "$cari");
        $sql = $this->db->limit(20);
        $sql = $this->db->offset($halaman * 30);
        $sql = $this->db->order_by('namalengkap', 'asc');
        $sql = $this->db->select("idpegawai as id, trim(concat(ifnull(titeldepan, ''), ' ', namalengkap, ' ', ifnull(titelbelakang, ''))) as text");
        $sql = $this->db->join('person_person pper', 'pper.idperson= ppai.idperson');
        $sql = $this->db->get("person_pegawai ppai");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }
    public function searchmultidokter()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->where("ppai.idprofesi", 1);
        $sql = $this->db->like("namalengkap", "$cari");
        $sql = $this->db->limit(20);
        $sql = $this->db->offset($halaman * 30);
        $sql = $this->db->order_by('namalengkap', 'asc');
        $sql = $this->db->select("idpegawai as id, trim(concat(ifnull(titeldepan, ''), ' ', namalengkap, ' ', ifnull(titelbelakang, ''))) as text");
        $sql = $this->db->join('person_person pper', 'pper.idperson= ppai.idperson');
        $sql = $this->db->get("person_pegawai ppai");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }

    ///////////////////////MASTERDATA INSTALASI///////////////////
    public function settingjenistarif()
    {
        return [
            'content_view'      => 'masterdata/v_jenistarif',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'tarif',
            'active_menu_level' => 'jenistarif'
        ];
    }
    public function jenistarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISTARIF)) //lihat define di atas
        {
            $data                   = $this->settingjenistarif(); //letakkan di baris pertama
            $data['data_list']      = $this->mgenerikbap->select_multitable("rjt.idjenistarif, rjt.jenistarif, rjt.kodebpjsjenistarif", "rs_jenistarif")->result();
            $data['title_page']     = 'Jenis Tarif';
            $data['mode']           = 'view';
            $data['table_title']    = 'List Data Jenis tarif ';
            $data['plugins']        = [PLUG_DATATABLE];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }


    public function add_jenistarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISTARIF)) //lihat define di atas
        {
            $this->load->helper('form');
            $data = $this->settingjenistarif();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Jenis Tarif';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_jenistarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISTARIF)) //lihat define di atas
        {
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data              = $this->settingjenistarif();
            $data['data_edit'] = $this->db->get_where('rs_jenistarif', ['idjenistarif' => $id])->row_array();
            $data['title_page'] = 'Edit Jenis Tarif';
            $data['mode']      = 'edit';
            $data['plugins']   = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    // SIMPAN JENIS TARIF
    public function save_jenistarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JENISTARIF)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('jenistarif', '', 'required');
            $this->form_validation->set_rules('kodebpjsjenistarif', '', '');
            //Jika tidak Valid
            if (validation_input()) {
                //Persiapkan data
                $idjenistarif = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idjenistarif')));
                $jenistarif   = $this->input->post('jenistarif');
                $kodebpjsjenistarif = $this->input->post('kodebpjsjenistarif');
                $data = ['jenistarif' => $jenistarif, 'kodebpjsjenistarif' => $kodebpjsjenistarif];
                //Simpan
                $this->mgenerikbap->setTable('rs_jenistarif');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idjenistarif);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idjenistarif) ? 'Save Success.!' : 'Update Success.!', empty($idjenistarif) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/jenistarif'));
        }
    }
    ///////////////////////MASTERDATA KELAS ///////////////////
    //-------------------------- ^^ Standar
    public function settingkelas()
    {
        return [
            'content_view'      => 'masterdata/v_kelas',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'kelas'
        ];
    }
    public function kelas()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELAS)) //lihat define di atas
        {
            $data                = $this->settingkelas();
            $data['data_list']   = $this->db->get('rs_kelas')->result();
            $data['title_page']  = 'Kelas';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Kelas';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_kelas()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELAS)) //lihat define di atas
        {
            $data               = $this->settingkelas();
            $this->load->helper('form');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Kelas';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_kelas()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELAS)) //lihat define di atas
        {
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data               = $this->settingkelas();
            $data['data_edit']  = $this->db->get_where('rs_kelas', ['idkelas' => $id])->row_array();
            $data['title_page'] = 'Edit Kelas';
            $data['mode']       = 'edit';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_kelas() // SIMPAN KELAS
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KELAS)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('kelas', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idkelas =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idkelas')));
                $data    = [
                    'kodekelasjkn'      =>  $this->input->post('kodekelasjkn'),
                    'kodekelassiranap'  =>  $this->input->post('kodekelassiranap'),
                    'kelas' =>  $this->input->post('kelas')
                ];
                //Simpan
                $this->mgenerikbap->setTable('rs_kelas');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idkelas);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idkelas) ? 'Save Success.!' : 'Update Success.!', empty($idkelas) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/kelas'));
        }
    }

    ///////////////////////MASTERDATA MASTER TARIF///////////////////
    public function settingmastertarif()
    {
        return [
            'content_view'      => 'masterdata/v_mastertarif',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'tarif',

        ];
    }
    public function mastertarifralan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {
            $data                = $this->settingmastertarif(); //letakkan di baris pertama
            $data['title_page']  = 'Master Tarif Ralan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Master Tarif Ralan';
            $data['active_menu_level'] = 'mastertarifralan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['jsmode']      = 'mastertarifralan';
            $data['script_js']   = ['js_mastertarif'];
            $data['modeTarif']   = 'ralan';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function mastertarifranap()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {
            $data                = $this->settingmastertarif(); //letakkan di baris pertama
            $data['title_page']  = 'Master Tarif Ranap';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Master Tarif Ranap';
            $data['active_menu_level'] = 'mastertarifranap';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['jsmode']      = 'mastertarifranap';
            $data['script_js']   = ['js_mastertarif'];
            $data['modeTarif']   = 'ranap';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function getData_mastertarif() // get data master tarif
    {
        $data = $this->mdatatable->dt_mastertarif()->result();
        echo json_encode($data);
    }


    // -- tampilkan data master tarif
    public function dt_mastertarif() // data tabel master tarif
    {
        $getData = $this->mdatatable->dt_mastertarif_(true);
        $modetarif = (($this->input->post('mode') == 'ralan') ? 'mastertarifralan' : 'mastertarifranap');
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $field) {
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $id         = $this->encryptbap->encrypt_urlsafe($field->idmastertarif, "json");
                $tabel      = $this->encryptbap->encrypt_urlsafe('rs_mastertarif', "json");
                $idhalaman  = $this->encryptbap->encrypt_urlsafe(V_MASTERTARIF, "json");
                $no++;
                $row = array();
                $row[] = $field->icd;
                $row[] = $field->namaicd;
                $row[] = $field->kelas;
                $row[] = $field->jenistarif;
                $row[] = '<b>' . $field->kode . '</b> ' . $field->nama;
                $row[] = '<b>' . $field->kodeakun . '</b> ' . $field->namaakun;
                $row[] = convertToRupiah($field->jasaoperator);
                $row[] = convertToRupiah($field->nakes);
                $row[] = convertToRupiah($field->jasars);
                $row[] = convertToRupiah($field->bhp);
                $row[] = convertToRupiah($field->akomodasi);
                $row[] = convertToRupiah($field->margin);
                $row[] = convertToRupiah($field->sewa);
                $row[] = convertToRupiah($field->total);
                $row[] =  ' <a data-toggle="tooltip" title="" data-original-title="Salin Data" class="btn btn-warning btn-xs" href="' . base_url('cmasterdata/salin_mastertarif/' . $modetarif . '/' . $id) . '" ><i class="fa fa-window-restore"></i></a>'
                    . ' <a data-toggle="tooltip" title="" data-original-title="Edit Master Tarif" class="btn btn-warning btn-xs" href="' . base_url('cmasterdata/edit_mastertarif/' . $modetarif . '/' . $id) . '" ><i class="fa fa-pencil"></i></a>
                            <a data-toggle="tooltip" title="" data-original-title="Delete Master Tarif" id="delete_data" nobaris="' . ($no - 1) . '" class="btn btn-danger btn-xs" href="#" alt="' . $tabel . '" alt2="' . $id . '" alt3="' . $idhalaman . '"><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_mastertarif(),
            "recordsFiltered" => $this->mdatatable->filter_dt_mastertarif(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function getDataExcelPaketPemeriksaan() // get data
    {
        $thisMode  = $this->input->post("mode");
        $this->load->model('mdatatable');

        $data = $this->mdatatable->dt_mastertarifpaketpemeriksaan()->result();

        echo json_encode($data);
    }

    //cetak excel
    public function cetak_excel_langsung()
    {
        $idpendaftaran  = $this->input->post("p");
        $listData = $this->input->post("listData");
        $title = $this->input->post("title");
        $data['fileName']     = $title . ' - ' . $idpendaftaran;
        $data['listData']     = $listData;
        $this->load->view('masterdata/excel_cetak_langsung', $data);
    }

    public function add_mastertarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {
            $this->load->library('user_agent');
            $this->load->helper('form');
            $data                     = $this->settingmastertarif();
            $data['isbolehedit_list'] = [0 => 'Tarif TIDAK Boleh Diubah di Pemeriksaan', '1' => 'Tarif Boleh Diubah di Pemeriksaan'];
            $data['data_list1']       = $this->db->get('rs_kelas')->result();
            $data['data_list3']       = $this->db->get('rs_jenistarif')->result();
            $data['data_edit']        = '';
            $data['title_page']       = 'Add Master Tarif';
            $data['mode']             = 'add';
            $data['jsmode']           = 'mastertarif';
            $data['plugins']          = [PLUG_DROPDOWN];
            $data['script_js']        = ['js_masterdata'];
            $data['active_menu_level'] = $this->uri->segment(3);
            $data['modetarif'] = $this->agent->referrer();
            $this->load->view('v_index', $data);
        }
    }
    public function mastertarif_listicd()
    {
        $icd = $this->input->post('searchTerm');
        $this->mgenerikbap->setTable('rs_icd');
        $this->db->where("idjenisicd !='1'")->where("idjenisicd !='2'");
        $this->db->like("namaicd", "$icd");
        $this->db->or_like("icd", "$icd");
        $this->db->or_like("aliasicd", "$icd");
        $this->db->limit(30);
        $this->db->offset($this->input->post('page') * 30);
        $this->db->order_by('icd', 'asc');
        $fetchData = $this->mgenerikbap->select("icd, namaicd, aliasicd")->result();
        $data = array();
        foreach ($fetchData as $obj) {
            $data[] = array("id" => $obj->icd, "text" => $obj->icd . ' | ' . $obj->namaicd);
        }
        echo json_encode($data);
    }
    public function edit_mastertarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {

            $this->load->library('user_agent');
            $this->load->helper('form');
            $data   = $this->settingmastertarif();
            $data['isbolehedit_list'] = [0 => 'Tarif TIDAK Boleh Diubah di Pemeriksaan', '1' => 'Tarif Boleh Diubah di Pemeriksaan'];
            $data['data_list1'] = $this->db->get('rs_kelas')->result();
            $data['data_list3'] = $this->db->get('rs_jenistarif')->result();
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('4')));
            $data['data_edit'] = $this->db->get_where('rs_mastertarif', ['idmastertarif' => $id])->row_array();
            $data['icd']       = $this->db->get_where('rs_icd', ['icd' => $data['data_edit']['icd']])->result();
            $data['tipeakun']  = $this->db->get_where('keu_tipeakun', ['kode' => $data['data_edit']['tipecoapendapatan']])->result();
            $data['akun']      = $this->db->get_where('keu_akun', ['kode' => $data['data_edit']['coapendapatan']])->result();
            $data['title_page'] = 'Edit Master Tarif';
            $data['mode']      = 'edit';
            $data['jsmode']    = 'mastertarif';
            $data['plugins']   = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $data['active_menu_level'] = $this->uri->segment(3);
            $data['modetarif'] = $this->agent->referrer();
            $this->load->view('v_index', $data);
        }
    }
    public function salin_mastertarif()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {

            $this->load->library('user_agent');
            $this->load->helper('form');
            $data          = $this->settingmastertarif();
            $data['isbolehedit_list']    = [0 => 'Tarif TIDAK Boleh Diubah di Pemeriksaan', '1' => 'Tarif Boleh Diubah di Pemeriksaan'];
            $data['data_list1'] = $this->db->get('rs_kelas')->result();
            $data['data_list3'] = $this->db->get('rs_jenistarif')->result();

            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('4')));
            $data['data_edit'] = $this->db->get_where('rs_mastertarif', ['idmastertarif' => $id])->row_array();
            $data['icd']       = $this->db->get_where('rs_icd', ['icd' => $data['data_edit']['icd']])->result();
            $data['tipeakun']  = $this->db->get_where('keu_tipeakun', ['kode' => $data['data_edit']['tipecoapendapatan']])->result();
            $data['akun']      = $this->db->get_where('keu_akun', ['kode' => $data['data_edit']['coapendapatan']])->result();

            $data['title_page'] = 'Salin Master Tarif';
            $data['active_menu_level'] = $this->uri->segment(3);
            $data['mode']      = 'salin';
            $data['jsmode']    = 'mastertarif';
            $data['plugins']   = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $data['modetarif'] = $this->agent->referrer();
            $this->load->view('v_index', $data);
        }
    }
    public function save_mastertarif() // SIMPAN MASTER TARIF
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('idkelas', '', 'required');
            $this->form_validation->set_rules('icd', '', 'required');
            $this->form_validation->set_rules('idjenistarif', '', 'required|greater_than[0]');
            $this->form_validation->set_rules('isbolehedit', '', 'required');
            $this->form_validation->set_rules('jasoperator', '', '');
            $this->form_validation->set_rules('jasars', '', '');
            $this->form_validation->set_rules('bhp', '', '');
            $this->form_validation->set_rules('akomodasi', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idmastertarif = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idmastertarif')));
                $idkelas       = $this->input->post('idkelas');
                $icd           = $this->input->post('icd');
                $isbolehedit   = $this->input->post('isbolehedit');
                $idjenistarif  = $this->input->post('idjenistarif');
                $kodetipeakun  = $this->input->post('kodetipeakun');
                $kodeakun      = $this->input->post('kodeakun');
                $jasaoperator  = $this->input->post('jasaoperator');
                $nakes         = $this->input->post('nakes');
                $jasars        = $this->input->post('jasars');
                $bhp           = $this->input->post('bhp');
                $akomodasi     = $this->input->post('akomodasi');
                $margin        = $this->input->post('margin');
                $sewa          = $this->input->post('sewa');
                $total         = $this->input->post('total');

                // idkelas for Ranap
                $idkelasKelas1       = '2';
                $idkelasKelas2       = '3';
                $idkelasKelas3       = '4';
                $idkelasKelasVIP     = '5';
                $idkelasKelasHCU     = '6';
                $idkelasKelasKBY     = '7';

                // margin for Ranap
                $marginKelas1        = '10000';
                $marginKelas2        = $margin;
                $marginKelas3        = $margin;
                $marginKelasVIP      = '20000';
                $marginKelasHCU      = '20000';
                $marginKelasKBY      = $margin;

                // total for Ranap
                $totalKelas1        = ($total + $marginKelas1);
                $totalKelas2        = $total;
                $totalKelas3        = $total;
                $totalKelasVIP      = ($total + $marginKelasVIP);
                $totalKelasHCU      = ($total + $marginKelasHCU);
                $totalKelasKBY      = $total;

                $data = [
                    'idkelas'   => $idkelas,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $margin,
                    'sewadasar' => $sewa,
                    'total'     => $total
                ];

                $data_kelas1 = [
                    'idkelas'   => $idkelasKelas1,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelas1,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelas1
                ];

                $data_kelas2 = [
                    'idkelas'   => $idkelasKelas2,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelas2,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelas2
                ];

                $data_kelas3 = [
                    'idkelas'   => $idkelasKelas3,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelas3,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelas3
                ];

                $data_kelasVIP = [
                    'idkelas'   => $idkelasKelasVIP,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelasVIP,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelasVIP
                ];

                $data_kelasHCU = [
                    'idkelas'   => $idkelasKelasHCU,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelasHCU,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelasHCU
                ];

                $data_kelasKBY = [
                    'idkelas'   => $idkelasKelasKBY,
                    'icd'       => $icd,
                    'isbolehedit'   => $isbolehedit,
                    'idjenistarif'  => $idjenistarif,
                    'tipecoapendapatan' => $kodetipeakun,
                    'coapendapatan' => $kodeakun,
                    'jasaoperatordasar' => $jasaoperator,
                    'nakesdasar'    => $nakes,
                    'jasarsdasar'   => $jasars,
                    'bhpdasar'      => $bhp,
                    'akomodasidasar' => $akomodasi,
                    'margindasar'   => $marginKelasKBY,
                    'sewadasar' => $sewa,
                    'total'     => $totalKelasKBY
                ];

                //Simpan
                $this->mgenerikbap->setTable('rs_mastertarif');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idmastertarif);

                //Simpan for all Class 
                if ($idkelas == '0') // only if kelas = Rawat Jalan
                {
                    $simpanKelas1 = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelas1, $icd);
                    $simpanKelas2 = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelas2, $icd);
                    $simpanKelas3 = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelas3, $icd);
                    $simpanKelasVIP = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelasVIP, $icd);
                    $simpanKelasHCU = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelasHCU, $icd);
                    $simpanKelasKBY = $this->mgenerikbap->update_or_insert_ignoreduplicate($data_kelasKBY, $icd);
                }

                //log
                $datal = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelas .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $margin .
                        '; sewadasar=' . $sewa .
                        '; total=' . $total,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelas1 = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelas1 .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelas1 .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelas1,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelas2 = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelas2 .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelas2 .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelas2,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelas3 = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelas3 .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelas3 .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelas3,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelasVIP = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelasVIP .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelasVIP .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelasVIP,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelasHCU = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelasHCU .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelasHCU .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelasHCU,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $datalKelasKBY = [
                    'url'     => get_url(),
                    'log'     =>    'user:' . $this->session->userdata('username') .
                        '; status: ' . ((empty($idmastertarif)) ? ' add tarif' : ' ubah tarif') .
                        '; idkelas=' . $idkelasKelasKBY .
                        '<br>' .
                        'icd=' . $icd .
                        '; isbolehedit=' . $isbolehedit .
                        '; idjenistarif=' . $idjenistarif .
                        '; tipecoapendapatan=' . $kodetipeakun .
                        '<br>' .
                        'coapendapatan=' . $kodeakun .
                        '; jasaoperatordasar=' . $jasaoperator .
                        '; nakesdasar=' . $nakes .
                        '; jasarsdasar=' . $jasars .
                        '<br>' .
                        'bhpdasar=' . $bhp .
                        '; akomodasidasar=' . $akomodasi .
                        '; margindasar=' . $marginKelasKBY .
                        '; sewadasar=' . $sewa .
                        '; total=' . $totalKelasKBY,
                    'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
                ];

                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);

                //upload log for Ranap
                if ($idkelas == '0') // only if kelas = Rawat Jalan
                {
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelas1);
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelas2);
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelas3);
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelasVIP);
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelasHCU);
                    $this->mgenerikbap->update_or_insert_ignoreduplicate($datalKelasKBY);
                }

                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idmastertarif) ? 'Save Success.!' : 'Update Success.!', empty($idmastertarif) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect($this->input->post('modetarif'));
        }
    }

    ///////////////////////MASTERDATA STASIUN ///////////////////
    //-------------------------- ^^ Standar
    public function settingstasiun()
    {
        return [
            'content_view'      => 'masterdata/v_stasiun',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'stasiun'
        ];
    }
    public function stasiun()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_STASIUN)) //lihat define di atas
        {
            $data                = $this->settingstasiun();

            $this->mgenerikbap->setTable('antrian_stasiun');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Stasiun';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Stasiun';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_stasiun()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_STASIUN)) //lihat define di atas
        {
            $this->load->helper('form');
            $data               = $this->settingstasiun();
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Stasiun';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_stasiun()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_STASIUN)) //lihat define di atas
        {
            $this->load->helper('form');
            $data                 = $this->settingstasiun();
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']  = $this->db->get_where('antrian_stasiun', ['idstasiun' => $id])->row_array();
            $data['title_page'] = 'Edit Stasiun';
            $data['mode']       = 'edit';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_stasiun() // SIMPAN STASIUN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_STASIUN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namastasiun', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idstasiun   =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idstasiun')));
                $namastasiun = $this->input->post('namastasiun');
                $data = ['namastasiun' => $namastasiun];
                //Simpan
                $this->mgenerikbap->setTable('antrian_stasiun');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idstasiun);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idstasiun) ? 'Save Success.!' : 'Update Success.!', empty($idstasiun) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/stasiun'));
        }
    }

    ///////////////////////MASTERDATA LOKET///////////////////
    public function settingloket()
    {
        return [
            'content_view'      => 'masterdata/v_loket',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'loket'
        ];
    }
    public function loket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKET)) //lihat define di atas
        {
            $this->load->model('mmasterdata');
            $data                = $this->settingloket();
            $data['data_list']   = $this->mmasterdata->get_data_loket();
            $data['title_page']  = 'Loket';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Loket';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_loket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKET)) //lihat define di atas
        {
            $this->load->helper('form');
            $data               = $this->settingloket();
            $data['dtstasiun']  = $this->db->select('idstasiun, namastasiun')->get('antrian_stasiun')->result();
            $data['dtunit']     = $this->db->select('idunit, namaunit')->get('rs_unit')->result();
            $data['dtjenisloket'] = $this->db->select("kode, jenis, ifnull(antrean_taskid,'') as antrean_taskid")->get('antrian_loket_jenis')->result();
            $data['data_edit']  = '';
            $data['title_page'] = 'Tambah Loket';
            $data['mode']       = 'add';
            $data['jsmode']     = 'formloket';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_loket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKET)) //lihat define di atas
        {
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));

            $data = $this->settingloket();
            $data['dtstasiun']  = $this->db->select('idstasiun, namastasiun')->get('antrian_stasiun')->result();
            $data['dtunit']     = $this->db->select('idunit, namaunit')->get('rs_unit')->result();
            $data['dtjenisloket'] = $this->db->select("kode, jenis, ifnull(antrean_taskid,'') as antrean_taskid")->get('antrian_loket_jenis')->result();
            $data['data_edit']  = $this->db->select('a.*,ri.namaicd')->join('rs_icd ri', 'ri.icd=a.icdtarifpemeriksaan', 'left')->get_where('antrian_loket a', ['a.idloket ' => $id])->row_array();
            $data['title_page'] = 'Ubah Loket';
            $data['mode']       = 'edit';
            $data['jsmode']     = 'formloket';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    //cek apakah kode loket sudah ada
    public function cek_kodeloket()
    {
        $kodeloket = $this->input->post('kodeloket');
        $idloket   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idloket')));

        //cek kode sudah digunakan diloket
        $cek = $this->db->select('idloket')->get_where('antrian_loket', ['kodeloket' => $kodeloket]);
        $result = array();
        $result['status']   = 'success';
        $result['idloket']  = $idloket;

        if ($cek->num_rows() > 0) {
            $loket = $cek->row_array();

            if ($loket['idloket'] == $idloket) {
                //cek kode sudah digunakan di unit
                $cek = $this->db->get_where('rs_unit', ['akronimunit' => $kodeloket]);
                if ($cek->num_rows() > 0) {
                    $result['status']  = 'warning';
                    $result['message'] = 'Kode Loket Sudah Digunakan di Kode Unit, Harap Masukan Kode Yang Lain.';
                }

                echo json_encode($result);
                return;
            }

            //kode sudah digunakan di loket
            $result['status']  = 'warning';
            $result['message'] = 'Kode Loket Sudah Digunakan, Harap Masukan Kode Yang Lain.';
        }

        //cek kode sudah digunakan di unit
        $cek = $this->db->select('akronimunit')->get_where('rs_unit', ['akronimunit' => $kodeloket]);
        if ($cek->num_rows() > 0) {
            $result['status']  = 'warning';
            $result['message'] = 'Kode Loket Sudah Digunakan di Kode Unit, Harap Masukan Kode Yang Lain.';
        }

        echo json_encode($result);
    }

    //fungsi untuk menyimpan data loket
    public function save_loket() // SIMPAN KABUPATEN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKET)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('idstasiun', '', 'required');
            $this->form_validation->set_rules('namaloket', '', 'required');
            $this->form_validation->set_rules('isambil', '', 'required');
            $this->form_validation->set_rules('icdtarifpemeriksaan', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idloket   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idloket')));
                $idstasiun = $this->input->post('idstasiun');
                $idunit    = $this->input->post('idunit');
                $namaloket = $this->input->post('namaloket');
                $kodeloket = strtoupper($this->input->post('kodeloket'));
                $isambil   = $this->input->post('isambil');
                $bridging_jkn = $this->input->post('bridging_jkn');
                $estimasi  = $this->input->post('estimasi');
                $icdtarifpemeriksaan = $this->input->post('icd');
                $idjenisloket = $this->input->post('idjenisloket');
                $data = [
                    'idstasiun' => $idstasiun,
                    'idunit' => $idunit,
                    'kodeloket' => $kodeloket,
                    'namaloket' => $namaloket,
                    'isambil' => $isambil,
                    'bridging_jkn' => $bridging_jkn,
                    'icdtarifpemeriksaan' => $icdtarifpemeriksaan,
                    'idjenisloket' => $idjenisloket,
                    'lamapelayanan' => $estimasi
                ];

                //Simpan
                $this->mgenerikbap->setTable('antrian_loket');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idloket);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idloket) ? 'Save Success.!' : 'Update Success.!', empty($idloket) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/loket'));
        }
    }
    // mahmud, clear -> menampilkan loket by stasiun
    public function tampil_loketbystasiun()
    {
        echo json_encode($this->db->get_where('antrian_loket', ['idstasiun' => $this->input->post('stasiun')])->result());
    }

    /// ---  panggil data poli js
    public function get_datapoli()
    {
        // $pg = $this->input->post('pgmenu');

        $query_unit = $this->db->get_where('rs_unit', ['levelgudang' => 'unit'])->result();

        // if( isset( $pg ) ){
        //     $return = array('query'=>$query_unit);
        // }else{
        // $return = array('query'=>$query_unit);
        // }

        echo json_encode($query_unit);
    }

    /// ---  panggil data bangsal js
    public function get_databangsal()
    {
        echo json_encode($this->db->get('rs_bangsal')->result());
    }


    ///////////////////////MASTERDATA GOLONGAN BARANG ///////////////////
    //-------------------------- ^^ Standar
    public function settinggolongan()
    {
        return [
            'content_view'      => 'masterdata/v_golongan',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'golongan',
            'active_menu_level' => ''
        ];
    }
    public function golongan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GOLONGAN)) //lihat define di atas
        {
            $data                = $this->settinggolongan();
            $this->mgenerikbap->setTable('rs_barang_golongan');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Golongan';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Golongan';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_golongan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GOLONGAN)) //lihat define di atas
        {
            $data               = $this->settinggolongan();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Add Golongan';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_golongan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GOLONGAN)) //lihat define di atas
        {
            $data                 = $this->settinggolongan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_barang_golongan');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idgolongan' => $id])->row_array();
            $data['title_page'] = 'Edit Golongan';
            $data['mode'] = 'edit';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_golongan() // SIMPAN GOLONGAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_GOLONGAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('jenis', '', 'required');
            $this->form_validation->set_rules('golongan', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idgolongan =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idgolongan')));
                $jenis = $this->input->post('jenis');
                $golongan = $this->input->post('golongan');
                $data = ['jenis' => $jenis, 'golongan' => $golongan];
                //Simpan
                $this->mgenerikbap->setTable('rs_barang_golongan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idgolongan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idgolongan) ? 'Save Success.!' : 'Update Success.!', empty($idgolongan) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/golongan'));
        }
    }

    ///////////////////////MASTERDATA BANGSAL///////////////////
    public function settingbangsal()
    {
        return [
            'content_view'      => 'masterdata/v_bangsal',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'bangsal'
        ];
    }
    public function bangsal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BANGSAL)) //lihat define di atas
        {
            $data                = $this->settingbangsal();
            $data['data_list']   = $this->mgenerikbap->select_multitable('*', 'rs_bangsal, person_pegawai, person_person, rs_kelas')->result();
            $data['title_page']  = 'Bangsal';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Bangsal';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_bangsal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BANGSAL)) //lihat define di atas
        {
            $this->load->helper('form');
            $data                  = $this->settingbangsal();
            $this->mgenerikbap->setTable('person_pegawai');
            $data['data_list']     = $this->mgenerikbap->select('*')->result();
            $data['data_pegawai']  = $this->mgenerikbap->select_multitable("ppg.idpegawai,pper.namalengkap", "person_pegawai, person_person")->result();

            $this->mgenerikbap->setTable('rs_kelas');
            $data['data_kelas']    = $this->mgenerikbap->select('*')->result();

            $data['dasarkelastarif']     = $this->mmasterdata->get_data_enum('rs_bangsal', 'dasarkelastarif');
            $data['jeniskelaminbangsal'] = $this->mmasterdata->get_data_enum('rs_bangsal', 'jeniskelaminbangsal');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Unit';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_bangsal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BANGSAL)) //lihat define di atas
        {
            $this->load->helper('form');
            $data               = $this->settingbangsal();
            $this->mgenerikbap->setTable('person_pegawai');
            $data['data_list']  = $this->mgenerikbap->select('*')->result();

            $data['data_pegawai'] = $this->mgenerikbap->select_multitable("ppg.idpegawai,pper.namalengkap", "person_pegawai, person_person")->result();

            $this->mgenerikbap->setTable('rs_kelas');
            $data['data_kelas']  = $this->mgenerikbap->select('*')->result();

            $data['dasarkelastarif']     = $this->mmasterdata->get_data_enum('rs_bangsal', 'dasarkelastarif');
            $data['jeniskelaminbangsal'] = $this->mmasterdata->get_data_enum('rs_bangsal', 'jeniskelaminbangsal');

            $this->mgenerikbap->setTable('rs_bangsal');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']  = $this->mgenerikbap->select('*', ['idbangsal' => $id])->row_array();

            $data['title_page'] = 'Edit Bangsal';
            $data['mode']       = 'edit';
            $data['plugins']    = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function save_bangsal() // SIMPAN BANGSAL
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BANGSAL)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namabangsal', '', 'required');
            $this->form_validation->set_rules('dasarkelastarif', '', 'required');
            $this->form_validation->set_rules('idpegawaika', '', 'required');
            $this->form_validation->set_rules('idkelas', '', 'required');
            $this->form_validation->set_rules('jeniskelaminbangsal', '', 'required');
            $this->form_validation->set_rules('total', '', '');
            $this->form_validation->set_rules('fasilitas', '', '');
            $this->form_validation->set_rules('koderuangjkn', '', '');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idbangsal = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idbangsal')));
                $datab['namabangsal']        = $this->input->post('namabangsal');
                $datab['dasarkelastarif']    = $this->input->post('dasarkelastarif');
                $datab['idpegawaika']          = $this->input->post('idpegawaika');
                $datab['idkelas']            = $this->input->post('idkelas');
                $datab['jeniskelaminbangsal'] = $this->input->post('jeniskelaminbangsal');
                $datab['fasilitas']          = $this->input->post('fasilitas');
                $datab['koderuangjkn']       = $this->input->post('koderuangjkn');

                //simpan bangsal
                $this->mgenerikbap->setTable('rs_bangsal');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($datab, $idbangsal);

                if (empty($idbangsal)) {
                    $this->mgenerikbap->setTable('rs_kelas');
                    $kodekelasjkn = $this->mgenerikbap->select('kodekelasjkn', ['idkelas' => $datab['idkelas']])->row_array();
                    applicare_adddata(["koderuangjkn" => $datab['koderuangjkn'], "namabangsal" => $datab['namabangsal'], "kapasitas" => 0, "tersedialakilaki" => 0, "tersediaperempuan" => 0, "tersediacampur" => 0, "kodekelasjkn" => $kodekelasjkn["kodekelasjkn"]]);
                } else {
                    applicare_updatedata($idbangsal);
                }
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idbangsal) ? 'Save Success.!' : 'Update Success.!', empty($idbangsal) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/bangsal'));
        }
    }
    ///////////////////////MASTERDATA BED///////////////////
    public function settingbed()
    {
        return [
            'content_view'      => 'masterdata/v_bed',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'ruang',
            'active_menu_level' => 'bed'
        ];
    }
    public function bed()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BED)) //lihat define di atas
        {
            $data                = $this->settingbed();
            $this->db->order_by("rbl.namabangsal, rbd.nobed");
            $data['data_list']   = $this->mgenerikbap->select_multitable("rbd.idbed, rbl.namabangsal, rbd.nobed, rbs.statusbed", "rs_bed, rs_bangsal, rs_bed_status")->result();
            $data['title_page']  = 'Bed';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Bed';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_bed()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BED)) //lihat define di atas
        {
            $this->load->helper('form');
            $data                 = $this->settingbed();
            $data['data_bangsal'] = $this->db->get('rs_bangsal')->result();
            $data['data_status']  = $this->db->get('rs_bed_status')->result();
            $data['data_edit']    = '';
            $data['title_page']   = 'Add Bed';
            $data['mode']         = 'add';
            $data['plugins']      = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']    = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function edit_bed()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BED)) //lihat define di atas
        {
            $this->load->helper('form');
            $data                  = $this->settingbed();
            $data['data_bangsal']  = $this->db->get('rs_bangsal')->result();
            $data['data_status']   = $this->db->get('rs_bed_status')->result();
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit']     = $this->db->get_where('rs_bed', ['idbed' => $id])->row_array();
            $data['title_page']    = 'Edit Bed';
            $data['mode']          = 'edit';
            $data['plugins']       = [PLUG_DROPDOWN, PLUG_CHECKBOX];
            $data['script_js']     = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }

    public function save_bed() // SIMPAN BED
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BED)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('idbangsal', '', 'required');
            $this->form_validation->set_rules('nobed', '', 'required');
            $this->form_validation->set_rules('idstatusbed', '', 'required');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idbed     = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idbed')));
                $idbangsal = $this->input->post('idbangsal');
                $nobed     = $this->input->post('nobed');
                $idstatusbed = $this->input->post('idstatusbed');
                $data = ['idbangsal' => $idbangsal, 'nobed' => $nobed, 'idstatusbed' => $idstatusbed];
                //Simpan
                $this->mgenerikbap->setTable('rs_bed');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idbed);
                aplicare_updatebed($idbangsal);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idbed) ? 'Save Success.!' : 'Update Success.!', empty($idbed) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/bed'));
        }
    }
    // pencarian data pasien
    public function getdata_pasien()
    {
        echo json_encode($this->db->query("select pp.idperson, pp.namalengkap from person_person pp, person_pasien pas where pp.idperson = pas.idperson and pas.norm like '%" . $this->input->get('non') . "%' or pp.idperson = pas.idperson and pas.norm like '%" . $this->input->get('non') . "%' limit " . $this->input->get('page') . ", 20")->result());
    }
    ///////////////////////MASTERDATA ATURAN PAKAI ///////////////////
    //-------------------------- ^^ Standar
    public function settingaturanpakai()
    {
        return [
            'content_view'      => 'masterdata/v_aturanpakai',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'aturanpakai',
            'active_menu_level' => ''
        ];
    }
    // mahmud, clear -> get aturan obat json
    public function get_barangaturanpakai()
    {
        $this->mgenerikbap->setTable('vrs_barang_aturanpakai');
        echo json_encode($this->mgenerikbap->select('*')->result());
    }
    public function aturanpakai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI)) //lihat define di atas
        {
            $data                = $this->settingaturanpakai();
            $this->mgenerikbap->setTable('vrs_barang_aturanpakai');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'aturanpakai';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Aturan pakai';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_aturanpakai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI)) //lihat define di atas
        {
            $this->load->helper('form');
            $data = $this->settingaturanpakai();
            $data['pemakaian'] = $this->mmasterdata->get_data_enum('rs_barang_aturanpakai', 'pemakaian');
            $data['waktupemakaian'] = $this->mmasterdata->get_data_enum('rs_barang_aturanpakai', 'waktupemakaian');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Aturan pakai';
            $data['mode']       = 'add';
            $data['jsmode']     = 'aturanpakai';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_aturanpakai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI)) //lihat define di atas
        {
            $data                 = $this->settingaturanpakai();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_barang_aturanpakai');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idbarangaturanpakai' => $id])->row_array();
            $data['pemakaian'] = $this->mmasterdata->get_data_enum('rs_barang_aturanpakai', 'pemakaian');
            $data['waktupemakaian'] = $this->mmasterdata->get_data_enum('rs_barang_aturanpakai', 'waktupemakaian');
            $data['title_page'] = 'Edit Aturan pakai';
            $data['mode'] = 'edit';
            $data['jsmode']     = 'aturanpakai';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_aturanpakai() // SIMPAN aturanpakai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('signa', '', 'required');
            $this->form_validation->set_rules('pemakaian', '', 'required');
            $this->form_validation->set_rules('waktupakai', '', 'required');
            $this->form_validation->set_rules('kalisehari', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idaturanpakai =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idaturanpakai')));
                $data = [
                    'signa' => $this->input->post('signa'),
                    'jumlah' => $this->input->post('jumlah'),
                    'pemakaian' => $this->input->post('pemakaian'),
                    'waktupemakaian' => $this->input->post('waktupakai'),
                    'kalisehari' => $this->input->post('kalisehari')
                ];
                //Simpan
                $this->mgenerikbap->setTable('rs_barang_aturanpakai');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idaturanpakai);
                // var_dump($data);
                // print_r($idaturanpakai);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idaturanpakai) ? 'Save Success.!' : 'Update Success.!', empty($idaturanpakai) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/aturanpakai'));
        }
    }
    /////////////////////KLINIK PERUJUK///////////////
    public function settingklinikperujuk()
    {
        return [
            'content_view'      => 'masterdata/v_klinikperujuk',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'bpjs',
            'active_menu_level' => 'klinikperujuk'
        ];
    }
    public function klinikperujuk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KLINIKPERUJUK)) //lihat define di atas
        {
            $data                = $this->settingklinikperujuk();
            $data['datalist']    = $this->db->get('bpjs_klinikperujuk')->result_array();
            $data['title_page']  = 'Klinik Perujuk';
            $data['mode']        = 'view';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = ['master/js_klinikperujuk'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function aplicare_getdata()
    {
        echo json_encode(['data_list' => $this->bpjsbap->requestBedlist(JENISCONSID_PRODUCTION), 'data_db' => $this->mviewql->viewbangsalaplicare()]);
    }

    public function aplicare_updatebed($idbangsal = '')
    {
        if ($idbangsal == '') {
            $uploadedJSON = json_encode([
                'koderuang' => $this->input->post('krj'),
                'namaruang' => $this->input->post('nr'),
                'kapasitas' => $this->input->post('k'),
                'tersedia' => $this->input->post('tl') + $this->input->post('tp') + $this->input->post('tc'),
                'tersediapria' => $this->input->post('tl'),
                'tersediawanita' => $this->input->post('tp'),
                'tersediapriawanita' => $this->input->post('tc'),
                'kodekelas' => $this->input->post('kkj')
            ]);
            echo json_encode($this->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON));
        } else {
            $this->load->model('mviewql');
            $b = $this->mviewql->viewbangsalaplicare($idbangsal);
            if (array_key_exists(0, $b)) {
                $bangsal = $b[0];
                $uploadedJSON = json_encode([
                    'koderuang' => $bangsal["koderuangjkn"],
                    'namaruang' => $bangsal["namabangsal"],
                    'kapasitas' => $bangsal["kapasitas"],
                    'tersedia' => $bangsal["tersedialakilaki"] + $bangsal["tersediaperempuan"] + $bangsal["tersediacampur"],
                    'tersediapria' => $bangsal["tersedialakilaki"],
                    'tersediawanita' => $bangsal["tersediaperempuan"],
                    'tersediapriawanita' => $bangsal["tersediacampur"],
                    'kodekelas' => $bangsal["kodekelasjkn"]
                ]);
                $this->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON);
            }
        }
    }

    public function aplicare_createbed()
    {
        $uploadedJSON = json_encode([
            'koderuang' => $this->input->post('krj'),
            'namaruang' => $this->input->post('nr'),
            'kapasitas' => $this->input->post('k'),
            'tersedia' => $this->input->post('tl') + $this->input->post('tp') + $this->input->post('tc'),
            'tersediapria' => $this->input->post('tl'),
            'tersediawanita' => $this->input->post('tp'),
            'tersediapriawanita' => $this->input->post('tc'),
            'kodekelas' => $this->input->post('kkj')
        ]);
        echo json_encode($this->bpjsbap->createBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }

    public function aplicare_deletebed()
    {
        $uploadedJSON = json_encode([
            'koderuang' => $this->input->post('krj'),
            'kodekelas' => $this->input->post('kkj')
        ]);
        echo json_encode($this->bpjsbap->deleteBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }

    //****************************************************

    // mahmud, clear :: ambil data paket laborat
    public function getpaketlab()
    {
        echo json_encode($this->db->get_where('rs_paket_pemeriksaan', ['idpaketpemeriksaanparent' => 'null'])->result());
    }
    // 
    // mahmud, clear -> get kemasan racikan json
    public function get_kemasan()
    {
        $this->mgenerikbap->setTable('rs_barang_kemasan');
        echo json_encode($this->mgenerikbap->select('*')->result());
    }
    // ambil data obat
    public function getjsonmasterobat()
    {
        echo json_encode($this->mgenerikbap->select_multitable('idbarang,hargabeli, namabarang, kode, namasatuan', 'rs_barang, rs_satuan')->result());
    }
    ///**********distribusi barang
    //mahmud, clear 
    //-------------------------- ^^ Standar
    public function settingdistribusibarang()
    {
        return [
            'content_view'      => 'masterdata/v_distribusibarang',
            'active_menu'       => 'masterdata',
            'active_sub_menu'   => 'inventaris',
            'active_menu_level' => 'distribusibarang'
        ];
    }
    public function belanjabarang()
    {
        if ($this->session->userdata('levelgudang') == 'gudang') {
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Barang Datang';
            $data['mode']        = 'distribusibarang';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = 'tampilbelanja';
            $data['script_js']   = ['master/js_inventaris_barangdatang'];
            $data['active_menu_levelchild'] = 'barangdatang';
            $this->load->view('v_index', $data);
        } else if (empty(!$this->session->userdata('unitterpilih'))) {
            return $this->pesanbarang();
        } else {
            aksesditolak();
        }
    }
    public function pesanbarang()
    {
        if (empty(!$this->session->userdata('levelgudang')) or $this->session->userdata('levelgudang') != 'gudang') //lihat define di atas
        {
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Permintaan Barang';
            $data['mode']        = 'distribusipesanbarang';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = 'tampilpesanan';
            $data['script_js']   = ['master/js_inventaris_permintaanbarang'];
            $data['active_menu_levelchild'] = 'permintaanbarang';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    // inventaris 
    public function dt_barangdatang()
    {
        $getData = $this->mdatatable->dt_barangdatang_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];

                if ($this->pageaccessrightbap->checkAccessRight(V_HUTANGFARMASI)) {
                    $menu = '<a idbf="' . $obj->idbarangfaktur . '" id="detailfaktur" class="btn btn-info btn-xs" ' . ql_tooltip("detail faktur") . ' ><i class="fa fa-eye"></i></a>';
                    if ($obj->statusfaktur == 'selesai' || $obj->statusfaktur == 'diterima') {
                        if ($obj->kekurangan > 0) {
                            $menu .= ' <a id="bayarinkaso" alt="' . $obj->idbarangfaktur . '" alt2="' . $obj->tagihan . '" alt3="' . $obj->kekurangan . '" alt4="' . $obj->potongan . '" class="btn btn-success btn-xs" ' . ql_tooltip('Bayar Inkaso') . '> <i class="fa fa-money"></i> </a> ';
                            $menu .= ' <a id="bayarlunasinkaso" alt="' . $obj->idbarangfaktur . '" alt2="' . $obj->totaltagihan . '" alt3="' . $obj->nofaktur . '" class="btn btn-warning btn-xs" ' . ql_tooltip('Bayar Lunas Inkaso') . '> <i class="fa fa-check"></i> </a> ';
                        } else {
                            $menu .= ' <a id="batallunasinkaso" alt="' . $obj->idbarangfaktur . '"  class="btn btn-danger btn-xs" ' . ql_tooltip('Batal Pelunasan') . '> <i class="fa fa fa-minus-circle"></i> </a>';
                        }
                    }
                }

                if ($this->pageaccessrightbap->checkAccessRight(V_BELANJAFARMASI)) {
                    $menu  = '';
                    $menu .= ' <a idbf="' . $obj->idbarangfaktur . '" id="detailfaktur" class="btn btn-info btn-xs" ' . ql_tooltip("detail faktur") . ' ><i class="fa fa-eye"></i></a> ' . $this->menuStatusBelanja($obj->idbarangfaktur, $obj->statusfaktur);
                    $menu .= (($obj->statusfaktur == 'selesai') ? '' : ' <a onClick="detailbelanja(' . $obj->idbarangfaktur . ')" class="btn btn-warning btn-xs" ' . ql_tooltip('Pembetulan') . '><i class="fa fa-cart-plus"></i> </a> ');
                    $menu .= ' <a onClick="cetakbarangbelanja(' . $obj->idbarangfaktur . ')" class="btn btn-primary btn-xs" ' . ql_tooltip('Cetak') . '><i class="fa fa-print"></i> </a> ';
                }
                $row[] = ++$no;
                $row[] = ((empty($obj->namapbf)) ? '' : $obj->namapbf . ' - ') . $obj->distributor;
                $row[] = $obj->nofaktur;
                $row[] = $obj->tanggalfaktur;
                $row[] = $obj->tanggalpelunasan;
                $row[] = $obj->tanggaljatuhtempo;
                $row[] = convertToRupiah($obj->masahutang);
                $row[] = $obj->statusfaktur;
                $row[] = convertToRupiah($obj->totaltagihan);
                $row[] = convertToRupiah($obj->dibayar);
                $row[] = convertToRupiah($obj->kekurangan);
                $row[] = '<span class="' . $this->statusKeteranganBelanja($obj->totaltagihan, $obj->dibayar, $obj->tempo) . '">' . $this->keteranganBelanja($obj->totaltagihan, $obj->dibayar, $obj->tempo) . '</span>';
                $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_barangdatang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_barangdatang(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function dt_returbarang()
    {
        $getData = $this->mdatatable->dt_permintaanbarang_(true);
        $query = $this->db->last_query();
        $data = [];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->nofaktur;
                $row[] = $obj->waktu;
                $row[] = $obj->unitasal . '/' . $obj->unittujuan;
                $row[] = $obj->statusbarangpemesanan;
                $row[] = enum_rs_barang_pemesanan("'" . $obj->jenistransaksi . "'");
                $row[] = (($obj->statusbarangpemesanan == 'menunggu') ? (($this->session->userdata('unitterpilih') != $obj->unittujuan) ? ' <a onclick="terimapengembalian(' . $obj->idbarangpemesanan . ')" ' . ql_tooltip('Barang Diterima') . ' class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>' : ' <a  onclick="pembetulanpengembalian(' . $obj->idbarangpemesanan . ')" ' . ql_tooltip('pembetulan') . ' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> ') : '  ') . ' <a  onclick="cetakbarangpesanan(' . $obj->idbarangpemesanan . ')" ' . ql_tooltip('cetak') . ' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>'; //(($this->session->userdata('unitterpilih')!=$obj->unittujuan)? ' <a onclick="kirimbarangpesanan('.$obj->idbarangpemesanan.',\'pembetulankirim\')" '.ql_tooltip('pembetulan').' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>' : '' )
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_permintaanbarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_permintaanbarang(),
            "q" => $query,
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    private function menuStatusBelanja($id, $status)
    {
        $menu = '<div class="btn-group" ' . ql_tooltip($status) . '><button type="button" class="btn btn-xs faktur_' . $status . '  dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>';

        if ($status !== 'diterima' && $status !== 'selesai') {
            $menu .= '<ul class="dropdown-menu">';
            // if($status=='diterima' || $status=='selesai'){
            //    $menu .= '<li><a id="ubahstatusfaktur" stat="diterima" alt="'.$id.'"  class="'.(($status=='diterima')?'active':'').'">Diterima</a></li><li><a id="ubahstatusfaktur" stat="selesai" alt="'.$id.'" class="'.(($status=='selesai')?'active':'').'">Selesai</a></li>'; 
            // }else{
            $menu .= '<li><a id="ubahstatusfaktur" stat="pesan" alt="' . $id . '" class="' . (($status == 'pesan') ? 'active' : '') . '" href="#">Pesan</a></li>'
                . '<li><a id="ubahstatusfaktur" stat="rencana" alt="' . $id . '" class="' . (($status == 'rencana') ? 'active' : '') . '"  href="#">Rencana</a></li>'
                . '<li><a id="ubahstatusfaktur" stat="diterima" alt="' . $id . '" class="' . (($status == 'diterima') ? 'active' : '') . '" href="#">Diterima</a></li>'
                . '<li><a id="ubahstatusfaktur" stat="selesai" alt="' . $id . '" class="' . (($status == 'selesai') ? 'active' : '') . '" href="#">Selesai</a></li>'
                . '<li><a id="ubahstatusfaktur" stat="batal" alt="' . $id . '" class="' . (($status == 'batal') ? 'active' : '') . '" href="#">Batal</a></li>';
            // }
            $menu .= '</ul>';
        }
        $menu .= '</div>';
        return $menu;
    }

    //mahmud, clear
    private function keteranganBelanja($tagihan, $dibayar, $tempo)
    {
        $index =  $this->setKeteranganBelanja($tagihan, $dibayar, $tempo);
        $status = ['Belum Dibayar', 'Telat Bayar', 'Dibayar'];
        return $status[$index];
    }
    //mahmud, clear
    private function setKeteranganBelanja($tagihan, $dibayar, $tempo)
    {
        $tagihan = floatval($tagihan); //parsefloat
        $dibayar = floatval($dibayar); //parsefloat
        $index =  ((intval($tempo)) < 1 && (($dibayar < $tagihan) ? 1 : (($dibayar == 0) ? 0 : 2))); //parseint
        return $index;
    }
    //mahmud, clear
    private function statusKeteranganBelanja($tagihan, $dibayar, $tempo)
    {
        $index =  $this->setKeteranganBelanja($tagihan, $dibayar, $tempo);
        $status = ['belumdibayar', 'telatbayar', 'dibayar'];
        return $status[$index];
    }

    //ambil data detail belanja barang
    public function jsondetailbelanjabarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsondetailbelanjabarang()->result());
    }
    //    ambil data pesan barang
    public  function jsontampilpesananbarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsonbarangpesanan()->result());
    }
    // inventaris 
    public function dt_permintaanbarang()
    {
        $getData = $this->mdatatable->dt_permintaanbarang_(true);
        $data = [];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->nofaktur;
                $row[] = $obj->waktu;
                $row[] = $obj->unitasal . '/' . $obj->unittujuan;
                $row[] = $obj->statusbarangpemesanan;
                $row[] = enum_rs_barang_pemesanan($obj->jenistransaksi);
                $row[] = (($obj->statusbarangpemesanan == 'menunggu') ? (($this->session->userdata('unitterpilih') != $obj->unittujuan) ? ' <a onclick="kirimbarangpesanan(' . $obj->idbarangpemesanan . ',false)" ' . ql_tooltip('kirim pesanan') . ' class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>' : ' <a id="batalpermintaan" alt="' . $obj->idbarangpemesanan . '" class="btn btn-danger btn-xs" ' . ql_tooltip('batal') . ' ><i class="fa fa-minus-circle"></i></a> <a  onclick="kirimbarangpesanan(' . $obj->idbarangpemesanan . ',\'pembetulanpesan\')" ' . ql_tooltip('pembetulan') . ' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a> ') : (($this->session->userdata('unitterpilih') != $obj->unittujuan) ? '  <a onclick="kirimbarangpesanan(' . $obj->idbarangpemesanan . ',\'pembetulankirim\')" ' . ql_tooltip('pembetulan') . ' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>' : (($obj->statusbarangpemesanan == 'batal') ? '<a id="buatpermintaanulang"  alt="' . $obj->idbarangpemesanan . '" class="btn btn-success btn-xs" ' . ql_tooltip('pesan') . ' ><i class="fa fa-plus"></i></a>' : ''))) . ' <a  onclick="cetakbarangpesanan(' . $obj->idbarangpemesanan . ')" ' . ql_tooltip('cetak') . ' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_permintaanbarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_permintaanbarang(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function batal_permintaanbarang()
    {
        $arrmsg = $this->db->update('rs_barang_pemesanan', ['statusbarangpemesanan' => $this->input->post('sts')], ['idbarangpemesanan' => $this->input->post('i')]);
        pesan_success_danger($arrmsg, "Batal Permintaan Berhasil.", "Batal Permintaan Gagal.", "js");
    }

    public function jsontampilpesananbarangstok()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsonbarangpesananstok()->result());
    }

    public  function jsontampilpengembalianbarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsonbarangpengembalian()->result());
    }


    public function distribusibelanjabarang()
    {
        if ($this->session->userdata('levelgudang') == 'gudang') //lihat define di atas
        {
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Barang Datang';
            $data['mode']        = 'belanja';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $data['jsmode']      = 'belanja';
            $data['jenisdistribusi'] = 'masuk';
            $data['script_js']   = ['js_distribusibarang'];
            $data['unit'] = $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
            $data['unittujuan'] = [];

            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function distribusipesanbarang()
    {
        if (empty(!$this->session->userdata('levelgudang')) && $this->session->userdata('levelgudang') != 'gudang') //lihat define di atas
        {
            $levelgudang = ($this->session->userdata('levelgudang') == 'depo') ? 'gudang' : 'depo';
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Permintaan Barang';
            $data['mode']        = 'pesan';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $data['jsmode']      = 'pesan';
            $data['active_menu_levelchild'] = 'permintaanbarang';
            $data['script_js']   = ['js_distribusibarang'];
            $data['unit'] = $this->db->get_where('rs_unit_farmasi', ['levelgudang' => 'gudang'])->result();
            $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();

            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function kirim_distribusibarang()
    {
        // if ( $this->session->userdata('levelgudang') != 'unit') //lihat define di atas
        // {
        $data                = $this->settingdistribusibarang();
        $data['title_page']  = 'Kirim Pesanan Barang';
        $data['mode']        = 'kirim';
        $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
        $data['jsmode']      = 'kirim';
        $data['script_js']   = ['js_distribusibarang'];
        $data['unit'] = $this->db->get_where('rs_unit_farmasi', ['levelgudang' => $this->session->userdata('levelgudang')])->result();
        $data['unittujuan'] = $this->db->get_where('rs_unit_farmasi')->result(); //masih salah seharus nya otomatis unit yang dituju #NOTE
        //            $data['jenisdistribusi']='transformasiasal';
        $this->load->view('v_index', $data);
        // }
        // else
        // {
        //     aksesditolak();
        // }
    }
    //mahmud, clear
    public function save_barangpembelianfaktur()
    {
        if ($this->session->userdata('levelgudang') == 'gudang') //lihat define di atas
        {
            //        siapkan barang faktur
            $post = $this->input->post();
            //            $idfaktur = json_decode($this->encryptbap->decrypt_urlsafe("FAKTURBELANJA", $post['idbarangfaktur']));
            $idfaktur = $post['idbarangfaktur'];
            $statusfaktur = ($post['setterimabarang']) ? 'diterima' : 'rencana';
            $statustrx = $post['statusfaktur'];
            $data_barangfaktur = [
                'nofaktur'            => $post['nofaktur'],
                'nopesan'             => $post['nopesan'],
                'idbarangdistributor' => $post['idbarangdistributor'],
                'tanggalfaktur'       => date('Y-m-d', strtotime($post['tanggalfaktur'])),
                'tanggaljatuhtempo'   => date('Y-m-d', strtotime($post['tanggaljatuhtempo'])),
                'tagihan'             => unconvertToRupiah($post['tagihan']),
                'ppn'                 => unconvertToRupiah($post['ppn']),
                'potongan'            => unconvertToRupiah($post['potongan']),
                'idpersonpetugas'     => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))),
                'idunit'              => $this->session->userdata('idunitterpilih')
            ];
            $arrMsg   = (empty(!$idfaktur)) ? $this->db->update('rs_barang_faktur', $data_barangfaktur, ['idbarangfaktur' => $idfaktur]) : $this->db->insert('rs_barang_faktur', $data_barangfaktur);
            $idfaktur = (empty(!$idfaktur)) ? $idfaktur : $this->db->insert_id();
            //simpan barang pembelian
            $idbarang = $post['idbarang'];
            $kode     = $post['kode'];
            $batchno  = $post['batchno'];
            $hargappn = $post['hargappn'];
            $harga    = $post['harga'];
            $persendiskon = $post['persendiskon'];
            $nominaldiskon = $post['nominaldiskon'];
            $kadaluarsa  = $post['kadaluarsa'];

            //      siapkan data distribusi
            $jumlah   = $post['jumlah'];
            $jumlahold = $post['jumlahold'];
            $idunit   = $post['idunitasal'];
            $jenisdistribusi = $post['jenisdistribusi'];
            $idpersonpetugas = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
            for ($x = 0; $x < count($kode); $x++) {   //simpan barang pembelian
                $dtbarangpembelian = ['idbarang' => $idbarang[$x], 'idbarangfaktur' => $idfaktur, 'batchno' => $batchno[$x], 'kadaluarsa' => $kadaluarsa[$x], 'hargabeli' => unconvertToRupiah($hargappn[$x]), 'hargaasli' => unconvertToRupiah($harga[$x]), 'persendiskon' => $persendiskon[$x], 'nominaldiskon' => unconvertToRupiah($nominaldiskon[$x])];
                $this->mgenerikbap->setTable('rs_barang_pembelian');
                $arrmsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($dtbarangpembelian, $post['idbarangpembelian'][$x]);

                $idbarangpembelian = (empty($post['idbarangpembelian'][$x])) ? $this->db->insert_id() : $post['idbarangpembelian'][$x]; //set idbarang pembelian
                //simpan barang distribusi
                $dtbarangdistribusi = ['idbarangpembelian' => $idbarangpembelian, 'idunit' => $idunit, 'jumlah' => unconvertToRupiah($jumlah[$x]), 'jenisdistribusi' => $jenisdistribusi, 'idpersonpetugas' => $idpersonpetugas, 'statusdistribusi' => (($statustrx == 'diterima') ? 'tidakdiubah' : 'bisadiubah')];
                // jika perubahan, status sudah diterima dan jumlah lama tidak sama jumlah terbaru
                if ($statustrx == 'diterima' && (unconvertToRupiah($jumlah[$x]) != unconvertToRupiah($jumlahold[$x]))) {
                    $arrmsg = $this->db->insert('rs_barang_distribusi', ['idbarangpembelian' => $idbarangpembelian, 'idunit' => $idunit, 'jumlah' => unconvertToRupiah($jumlahold[$x]), 'jenisdistribusi' => 'keluar', 'idpersonpetugas' => $idpersonpetugas, 'statusdistribusi' => 'tidakdiubah']); //keluar
                    $arrmsg = $this->db->insert('rs_barang_distribusi', ['idbarangpembelian' => $idbarangpembelian, 'idunit' => $idunit, 'jumlah' => unconvertToRupiah($jumlah[$x]), 'jenisdistribusi' => 'masuk', 'idpersonpetugas' => $idpersonpetugas, 'statusdistribusi' => 'tidakdiubah']); //masuk
                } else {
                    $this->mgenerikbap->setTable('rs_barang_distribusi');
                    $arrmsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($dtbarangdistribusi, $post['idbarangdistribusi'][$x]);
                }
            }
            ($statusfaktur == 'diterima') ? $this->db->update('rs_barang_faktur', ['statusfaktur' => 'diterima'], ['idbarangfaktur' => $idfaktur]) : '';
            $mode   = (empty($idfaktur)) ? 'Belanja ' : 'Update Belanja ';
            pesan_success_danger($arrmsg, $mode . 'barang berhasil.!', $mode . 'barang gagal.!');
            redirect(base_url('cmasterdata/belanjabarang'));
        } else {
            aksesditolak();
        }
    }
    //mahmud, clear
    public function updatefaktur()
    {
        $arrMsg = $this->db->update('rs_barang_faktur', ['statusfaktur' => $this->input->post('stat')], ['idbarangfaktur' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Update Faktur Berhasil.!', 'Update Faktur Gagal.!', 'js');
    }
    //mahmud, clear
    public function setbayarinkaso()
    {
        $post = $this->input->post();
        $iduser = $this->get_iduserlogin();
        if (isset($post['sb'])) {
            $data = ['statusbayar' => $post['sb'], 'iduserpelunasan' => $iduser];
            if ($post['sb'] == 'lunas') {
                $data['tanggalpelunasan'] = $post['tgl'];
            }
            $arrMsg = $this->db->update('rs_barang_faktur', $data, ['idbarangfaktur' => $this->input->post('id')]);
        } else {
            $arrMsg = $this->db->update('rs_barang_faktur', ['dibayar' => $this->input->post('nom'), 'statusbayar' => 'dibayar', 'iduserpelunasan' => $iduser], ['idbarangfaktur' => $this->input->post('id')]);
        }
        pesan_success_danger($arrMsg, 'Inkaso Berhasil.!', 'Inkaso Gagal.!', 'js');
    }

    // save barang pemesanan
    public function save_barangpemesanan()
    {
        if (empty(!$this->session->userdata('levelgudang')) &&  $this->session->userdata('levelgudang') != 'gudang') {
            //        buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired
            $callid = generaterandom(8);
            $this->ql->antrian_autonumber('FBP' . date('Ymd'), $callid, date('Y-m-d', strtotime("+2 weeks")));
            //      siapkan barang pemesanan
            if ($this->input->post('modeubah') !== 'pembetulanpesan') {
                $iduserlog =  $this->get_iduserlogin();
                $barangpemesanan = [
                    'idunit'      => $this->input->post('idunitasal'),
                    'idunittujuan' => $this->input->post('idunittujuan'),
                    'waktu'       => $this->input->post('tanggalfaktur'),
                    'nofaktur'    => fakturpermintaanbarang() . $this->db->get_where('helper_autonumber', ['callid' => $callid])->row_array()['number'],
                    'idpersonpetugas' => $iduserlog
                ];

                $this->db->insert('rs_barang_pemesanan', $barangpemesanan);
            }
            $idbarangpemesanan = ((empty($this->input->post('ip'))) ? $this->db->insert_id() : $this->input->post('ip')); // siapkan idbarang pesanan
            //      siapkan barang pemesanan detail
            $idbarang = $this->input->post('idbarang');
            $kode  = $this->input->post('kode');
            $jumlah   = $this->input->post('jumlah');
            $jumlahsebelum   = $this->input->post('jumlahsebelum');
            $keterangan   = $this->input->post('keterangan');
            for ($x = 0; $x < count($kode); $x++) {
                $pesanan = ['idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x], 'jumlah' => $jumlah[$x], 'keterangan' => $keterangan[$x]];
                if ($jumlahsebelum[$x] == 0) :
                    $arrmsg = $this->db->replace('rs_barang_pemesanan_detail', $pesanan);
                elseif ($jumlah[$x] == 0 && $jumlahsebelum[$x] != 0) :
                    $arrmsg = $this->db->delete('rs_barang_pemesanan_detail', ['idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x]]);
                elseif ($jumlah[$x] != 0) :
                    $arrmsg = $this->db->update('rs_barang_pemesanan_detail', $pesanan, ['idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x]]);
                endif;
            }
            pesan_success_danger($arrmsg, 'Pesanan berhasil ditambahkan.!', 'Pesanan gagal ditambahkan.!');
            redirect(base_url('cmasterdata/pesanbarang'));
        } else {
            aksesditolak();
        }
    }

    //    save kirim pesanan barang unit
    public  function savepesananbarangunit()
    {

        if ($this->session->userdata('levelgudang') != 'unit') {
            //        $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$this->input->post('idbarangpemesanan')]);
            $idunitasal       = $this->input->post("idunitasal");
            $idbarangpemesanan = $this->input->post("idbarangpemesanan");
            $idunittujuan     = $this->input->post("idunittujuan");
            $idbarangpembelian = $this->input->post('idbarangpembelian');
            $jumlah           = $this->input->post('jumlah');
            $jumlahpembetulan = $this->input->post('jumlahpembetulan');
            $modeubah         = $this->input->post('modeubah');
            $length           = $this->input->post('length');

            for ($x = 0; $x < count($length); $x++) {

                if (!(empty($jumlah[$x])) || !(empty($jumlahpembetulan[$x]))) {

                    if ($modeubah == 'pembetulankirim') :

                        if ($jumlahpembetulan[$x] != $jumlah[$x]) {
                            if (!empty($jumlahpembetulan[$x])) {
                                $pembetulan = ['idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlahpembetulan[$x], 'jenisdistribusi' => 'pembetulankeluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                                $arrmsg = $this->db->insert('rs_barang_distribusi', $pembetulan);
                            }
                            $keluar = ['idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                            $arrmsg = $this->db->insert('rs_barang_distribusi', $keluar);
                            $arrmsg = $this->db->update('rs_barang_pemesanan', ['statusbarangpemesanan' => 'selesai'], ['idbarangpemesanan' => $idbarangpemesanan]);
                        } else {
                            //                        
                        }
                    else :
                        $data = ['idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                        $arrmsg = $this->db->insert('rs_barang_distribusi', $data);
                        $arrmsg = $this->db->update('rs_barang_pemesanan', ['statusbarangpemesanan' => 'selesai'], ['idbarangpemesanan' => $idbarangpemesanan]);
                    endif;
                }
            }
            pesan_success_danger(($modeubah == 'pembetulankirim') ? 1 : $arrmsg, 'Simpan Berhasil.!', 'Simpan Gagal.!');
            redirect(base_url('cmasterdata/pesanbarang'));
        } else {
            aksesditolak();
        }
    }


    // save barang retur
    public function save_barangretur()
    {
        if (empty(!$this->session->userdata('levelgudang')) && $this->session->userdata('levelgudang') == 'depo') {
            //        buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired
            $callid = generaterandom(8);
            $this->ql->antrian_autonumber('R' . date('Ymd'), $callid, date('Y-m-d', strtotime("+2 weeks")));
            //      siapkan barang pemesanan
            $idperson =  $this->get_iduserlogin();
            if ($this->input->post('modeubah') !== 'pembetulanpesan') {
                $dt_rsbp = [
                    'idunit'      => $this->input->post('idunitasal'),
                    'idunittujuan' => $this->input->post('idunittujuan'),
                    'waktu'       => $this->input->post('tanggalfaktur'),
                    'nofaktur'    => fakturpengembalianbarang() . $this->db->get_where('helper_autonumber', ['callid' => $callid])->row_array()['number'],
                    'idpersonpetugas' => $idperson,
                    'jenistransaksi' => 'retur'
                ];

                $this->db->insert('rs_barang_pemesanan', $dt_rsbp);
            }
            $idbarangpemesanan = ((empty($this->input->post('ip'))) ? $this->db->insert_id() : $this->input->post('ip')); // siapkan idbarang pesanan
            //      siapkan barang pemesanan detail
            $idbarang = $this->input->post('idbarang');
            $kode     = $this->input->post('kode');
            $jumlah   = $this->input->post('jumlah');
            $jumlahsebelum = $this->input->post('jumlahsebelum');
            $keterangan   = $this->input->post('keterangan');
            // siapkan data distribusi
            $idbarangpembelian = $this->input->post('idbarangpembelian');
            for ($x = 0; $x < count($kode); $x++) {
                $pesanan    = ['idbarangpembelian' => $idbarangpembelian[$x], 'idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x], 'jumlah' => $jumlah[$x], 'keterangan' => $keterangan[$x]];
                $distribusi = ['idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $this->input->post('idunittujuan'), 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $this->input->post('idunitasal'), 'idbarangpemesanan' => $idbarangpemesanan, 'idpersonpetugas' => $idperson, 'statusdistribusi' => 'bisadiubah'];
                if ($jumlahsebelum[$x] == 0) :
                    $arrmsg = $this->db->replace('rs_barang_pemesanan_detail', $pesanan);
                    $arrmsg = $this->db->insert('rs_barang_distribusi', $distribusi);
                elseif ($jumlah[$x] == 0 && $jumlahsebelum[$x] != 0) :
                    $arrmsg = $this->db->delete('rs_barang_pemesanan_detail', ['idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x]]);
                // $arrmsg = $this->db->delete('rs_barang_distribusi',$distribusi);
                elseif ($jumlah[$x] != 0) :
                    $arrmsg = $this->db->update('rs_barang_pemesanan_detail', $pesanan, ['idbarangpemesanan' => $idbarangpemesanan, 'idbarang' => $idbarang[$x]]);
                    $arrmsg = $this->db->update('rs_barang_distribusi', $distribusi, ['idbarangpemesanan' => $idbarangpemesanan, 'idbarangpembelian' => $idbarangpembelian[$x]]);
                endif;
            }
            pesan_success_danger($arrmsg, 'Pengembalian berhasil.', 'Pengembalian gagal.');
            redirect(base_url('cmasterdata/returbarang'));
        } else {
            aksesditolak();
        }
    }


    //    save terima pengembalian  barang unit
    public  function save_terimaretur()
    {

        if ($this->session->userdata('levelgudang') != 'unit') {
            $idbarangpemesanan = $this->input->post('ip');
            $hasil = $this->db->select('idbarangpembelian')->get_where('rs_barang_pemesanan_detail', ['idbarangpemesanan' => $idbarangpemesanan])->result_array();
            foreach ($hasil as $arr) {
                $arrmsg = $this->db->update('rs_barang_distribusi', ['statusdistribusi' => 'tidakdiubah'], ['idbarangpembelian' => $arr['idbarangpembelian'], 'idbarangpemesanan' => $idbarangpemesanan]);
            }
            pesan_success_danger($arrmsg, 'Berhasil Diterima.', 'Gagal Diterima.', 'js');
        } else {
            aksesditolak();
        }
    }

    //  mahmud, clear :: MENAMPILKAN STOK BARANG
    public function stokbarang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_STOKBARANG)) {
            $data = [
                'content_view'     => 'masterdata/v_barang',
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'inventaris',
                'active_menu_level' => 'stokbarang',
                'title_page'       => 'Stok Barang',
                'mode'             => 'stok',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE],
                'script_js'        => ['datatable/js_masterbarang']
            ];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function transformasibarang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_TRANSFORMASIBARANG) && ($this->session->userdata('levelgudang') == 'gudang')) {
            $data = [
                'content_view'     => 'masterdata/v_distribusibarang',
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'inventaris',
                'active_menu_level' => 'transformasibarang',
                'title_page'       => 'Transformasi Barang',
                'mode'             => 'transformasibarang',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE],
                'jsmode'           => 'transformasibarang',
                'script_js'        => ['master/js_inventaris_transformasi']
            ];
            //             $data['stokgudang']= $this->db->query("select waktu, grupwaktu, group_concat(if(jenisdistribusi='transformasiasal',concat(d.namabarang, ', ', a.jumlah, ' ', e.namasatuan, ' exp:', b.kadaluarsa), NULL)) as transformasiasal, group_concat(if(jenisdistribusi='transformasihasil',concat(d.namabarang, ', ', a.jumlah, ' ', e.namasatuan, ' exp:', b.kadaluarsa), NULL) separator '<br/>') as transformasihasil from rs_barang_distribusi a, rs_barang_pembelian b, rs_barang d, rs_satuan e
            // where (a.jenisdistribusi='transformasiasal' or a.jenisdistribusi='transformasihasil') and b.idbarangpembelian=a.idbarangpembelian and d.idbarang=b.idbarang and e.idsatuan=d.idsatuan
            //  GROUP by a.grupwaktu order by a.idbarangdistribusi desc")->result();
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function dt_transformasibarang()
    {
        $getData = $this->mdatatable->dt_transformasibarang_(true);
        $db = $this->db->last_query();
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->transformasiasal;
                $row[] = $obj->transformasihasil;
                $row[] = $obj->waktu;
                $row[] = '<a id="view" waktu="' . $obj->waktu . '" grupwaktu="' . $obj->grupwaktu . '" setundo="0" ' . ql_tooltip('lihat transformasi') . ' class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_transformasibarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_transformasibarang(),
            "data" => $data,
            "db" => $db
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function addtransformasibarang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_TRANSFORMASIBARANG) && $this->session->userdata('levelgudang') == 'gudang') {
            $data = [
                'content_view'     => 'masterdata/v_distribusibarang',
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'inventaris',
                'active_menu_level' => 'stokbaranggudang',
                'title_page'       => 'Transformasi Barang',
                'mode'             => 'addtransformasibarang',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE],
                'jsmode'           => 'addtransformasibarang',
                'script_js'        => ['js_distribusibarang'],
                'unit'             => $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result(),
                'jenisdistribusi'  => 'transformasi'
            ];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    //mahmud, clear
    //****SAVE TRANSFORMASI BARANG
    public function save_transformasibarang()
    {
        // SIAPKAN DATA
        $idpersonpetugas =  $this->get_iduserlogin();
        $idunit = $this->input->post('idunitasal');
        $idunittujuan = $this->input->post('idunittujuan');
        $barangkeluar = $this->input->post('barangkeluar');
        $index = $this->input->post('index');
        //siapkan data distribusi keluar
        $jumlahkeluar     = $this->input->post('jumlahkeluar'); // sample: 1,2
        $idbarangpembelian = $this->input->post('idbarangpembelian'); // sample: 1,2,
        $kadaluarsa = $this->input->post('kadaluarsa'); // sample: 1,2,
        $batchno = $this->input->post('batchno'); // sample: 1,2,
        $hargabeli = $this->input->post('hargabeli'); // sample: 1,2,

        // siapkan data rs barang pembelian
        $idbarang = $this->input->post('idbarang'); // sample: 1,2,3,4,5,6

        //siapkan data distribusi masuk
        $jumlahdistribusi = $this->input->post('jumlahdistribusi'); // sample: 1,2,3,4,5,6
        $jumlahpembetulan = $this->input->post('jumlahpembetulan'); // sample: 1,2,3,4,5,6

        //siapkan pembeda transformasi
        $waktu      = date('Y-m-d');

        $parent = 0;
        $grupwaktu  = generaterandom(4, true);

        for ($x = 0; $x < count($barangkeluar); $x++) {
            $grupasal  = generaterandom(4, true);
            // save rs_barang_distribusi dengan jenisdistribusi=transformasiasal
            if (!empty($this->input->post('jumlahpembetulan' . $index[$parent])[$x])) {
                $this->db->insert('rs_barang_distribusi', ['waktu' => $waktu, 'grupwaktu' => $grupwaktu, 'grupasal' => $grupasal, 'idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunit, 'jumlah' => $this->input->post('jumlahkeluar')[$x], 'jenisdistribusi' => 'pembetulantransformasiasal', 'idunittujuan' => $idunittujuan, 'idpersonpetugas' => $idpersonpetugas]);
                $rbdasal = ['waktu' => $waktu, 'grupwaktu' => $grupwaktu, 'grupasal' => $grupasal, 'idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunit, 'jumlah' => $this->input->post('jumlahkeluar')[$x], 'jenisdistribusi' => 'transformasiasal', 'idunittujuan' => $idunittujuan, 'idpersonpetugas' => $idpersonpetugas];
                $this->db->insert('rs_barang_distribusi', $rbdasal);
            } else {
                $rbdasal = ['waktu' => $waktu, 'grupwaktu' => $grupwaktu, 'grupasal' => $grupasal, 'idbarangpembelian' => $idbarangpembelian[$x], 'idunit' => $idunit, 'jumlah' => $this->input->post('jumlahkeluar')[$x], 'jenisdistribusi' => 'transformasiasal', 'idunittujuan' => $idunittujuan, 'idpersonpetugas' => $idpersonpetugas];
                $this->db->insert('rs_barang_distribusi', $rbdasal);
            }


            for ($i = 0; $i < count($this->input->post('barangmasuk' . $index[$parent])); $i++) {
                // save rs_barang_distribusi :: pembetulantransformasihasil
                if (($this->input->post('jumlahpembetulan' . $index[$parent])[$i] != $this->input->post('jumlahdistribusi' . $index[$parent])[$i]) && empty(!$this->input->post('jumlahpembetulan' . $index[$parent])[$i])) {
                    $idbarangpembelianhasil = $this->input->post('idbarangpembelianasal' . $index[$parent])[$i];
                    $rbpembetulan = ['waktu' => $waktu, 'grupwaktu' => $grupwaktu, 'grupasal' => $grupasal, 'idbarangpembelian' => $idbarangpembelianhasil, 'idunit' => $idunit, 'jumlah' => $this->input->post('jumlahpembetulan' . $index[$parent])[$i], 'jenisdistribusi' => 'pembetulantransformasihasil', 'idunittujuan' => $idunittujuan, 'idpersonpetugas' => $idpersonpetugas];
                    $this->db->insert('rs_barang_distribusi', $rbpembetulan);
                }

                if (!isset($this->input->post('jumlahpembetulan' . $index[$parent])[$i])) { //insert ke barang pembelian pembelian
                    $dtbarangpembelian = ['idbarang' => $this->input->post('idbarang' . $index[$parent])[$i], 'batchno' => $batchno[$parent], 'kadaluarsa' => format_date_to_sql($kadaluarsa[$parent]), 'parentidbarangpembelian' => $idbarangpembelian[$parent], 'hargabeli' => $hargabeli[$parent]];
                    $this->db->insert('rs_barang_pembelian', $dtbarangpembelian);
                    $idbarangpembelianhasil = $this->db->insert_id();
                }

                // save rs_barang_distribusi :: transformasihasil
                $rbdhasil = ['waktu' => $waktu, 'grupwaktu' => $grupwaktu, 'grupasal' => $grupasal, 'idbarangpembelian' => $idbarangpembelianhasil, 'idunit' => $idunit, 'jumlah' => $this->input->post('jumlahdistribusi' . $index[$parent])[$i], 'jenisdistribusi' => 'transformasihasil', 'idunittujuan' => $idunittujuan, 'idpersonpetugas' => $idpersonpetugas];
                $this->db->insert('rs_barang_distribusi', $rbdhasil);
            }
            ++$parent;
        }
        redirect(base_url('cmasterdata/transformasibarang'));
    }

    public function returbarang()
    {
        if (empty(!$this->session->userdata('levelgudang')) or $this->session->userdata('levelgudang') == 'gudang') {
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Pengembalian Barang';
            $data['mode']        = 'distribusireturbarang';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DATE];
            $data['jsmode']      = 'tampilretur';
            $data['script_js']   = ['master/js_inventaris_returbarang'];
            $data['active_menu_levelchild'] = 'pengembalianbarang';
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function distribusireturbarang()
    {
        if (empty(!$this->session->userdata('levelgudang')) && $this->session->userdata('levelgudang') != 'gudang') {
            $levelgudang = ($this->session->userdata('levelgudang') == 'depo') ? 'gudang' : 'depo';
            $data                = $this->settingdistribusibarang();
            $data['title_page']  = 'Pengembalian Barang';
            $data['mode']        = 'retur';
            $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $data['jsmode']      = 'retur';
            $data['active_menu_levelchild'] = 'pengembalianbarang';
            $data['script_js']   = ['js_distribusibarang'];
            $data['unit'] = $this->db->get_where('rs_unit_farmasi', ['levelgudang' => $levelgudang])->result();
            $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();

            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    ///////////////////////MASTERDATA BARANG PABRIK///////////////////
    //-------------------------- ^^ Standar
    public function settingbarangpabrik()
    {
        return [
            'content_view'      => 'masterdata/v_barangpabrik',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'barangpabrik',
            'active_menu_level' => ''
        ];
    }
    public function barangpabrik()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGPABRIK)) //lihat define di atas
        {
            $data                = $this->settingbarangpabrik();
            $data['data_list']   = $this->db->get('rs_barangpabrik')->result();
            $data['title_page']  = 'Data Pabrik';
            $data['mode']        = 'view';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_barangpabrik()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGPABRIK)) //lihat define di atas
        {
            $data               = $this->settingbarangpabrik();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Tambah Pabrik';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_barangpabrik()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGPABRIK)) //lihat define di atas
        {
            $data                 = $this->settingbarangpabrik();
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->db->get_where('rs_barangpabrik', ['idbarangpabrik' => $id])->row_array();
            $data['mode'] = 'edit';
            $data['title_page'] = 'Ubah Pabrik';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_barangpabrik()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANGPABRIK)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namapabrik', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $ibp =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idbarangpabrik')));
                $data = ['namapabrik' => $this->input->post('namapabrik')];

                $this->mgenerikbap->setTable('rs_barangpabrik');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $ibp);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($iddistributor) ? 'Save Success.!' : 'Update Success.!', empty($ibp) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/barangpabrik'));
        }
    }

    ///////////////////////MASTER DATA PBF ////////////////////////////////
    //-------------------------- ^^ Standar
    public function settingpedagangbesarfarmasi()
    {
        return [
            'content_view'      => 'masterdata/v_pedagangbesarfarmasi',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'pedagangbesarfarmasi',
            'active_menu_level' => ''
        ];
    }
    public function pedagangbesarfarmasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEDAGANGBESARFARMASI)) //lihat define di ql_helper
        {
            $data                = $this->settingpedagangbesarfarmasi();
            $this->mgenerikbap->setTable('rs_barang_pbf');
            $data['data_list']   = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'PBF (Pedagang Besar Farmasi)';
            $data['mode']        = 'view';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = [];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }
    public function add_pedagangbesarfarmasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEDAGANGBESARFARMASI)) //lihat define di atas
        {
            $data               = $this->settingpedagangbesarfarmasi();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Tambah PBF';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_pedagangbesarfarmasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEDAGANGBESARFARMASI)) //lihat define di atas
        {
            $data                 = $this->settingpedagangbesarfarmasi();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_barang_pbf');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idbarangpbf' => $id])->row_array();
            $data['mode'] = 'edit';
            $data['title_page'] = 'Ubah PBF';
            $data['plugins']    = [];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_pedagangbesarfarmasi() // SIMPAN PEDAGANG BESAR
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEDAGANGBESARFARMASI)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namapbf', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idbarangpbf =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idbarangpbf')));
                $this->mgenerikbap->setTable('rs_barang_pbf');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate(['namapbf' => $this->input->post('namapbf')], $idbarangpbf);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idbarangpbf) ? 'Save Success.!' : 'Update Success.!', empty($idbarangpbf) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/pedagangbesarfarmasi'));
        }
    }
    ///////////////////////MASTERDATA DISTRIBUTOR BARANG ///////////////////
    //-------------------------- ^^ Standar
    public function settingdistributor()
    {
        return [
            'content_view'      => 'masterdata/v_barangdistributor',
            'active_menu'       => 'farmasi',
            'active_sub_menu'   => 'distributor',
            'active_menu_level' => ''
        ];
    }
    public function distributor()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DISTRIBUTOR)) //lihat define di atas
        {
            $this->load->model('mmasterdata');
            $data                = $this->settingdistributor();
            $data['data_list']   = $this->mmasterdata->get_data_distributor();
            $data['title_page']  = 'Distributor';
            $data['mode']        = 'view';
            $data['plugins']     = [PLUG_DATATABLE];
            $data['script_js']   = ['js_barang_distributor'];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    //upload file ke nas storage
    public function upload_file_NPWP()
    {
        $idbarangdistributor = $this->input->post('idbarangdistributor');
        $jenisberkas = $this->input->post('jenisberkas');
        $id_eNPWP  = time();
        $tmpName  = $_FILES['fileNPWP']['tmp_name'];
        $extension = pathinfo($_FILES['fileNPWP']['name'], PATHINFO_EXTENSION);
        $fileName = $id_eNPWP . '.' . $extension;
        $newFileName = '/keuangan/distributor_NPWP/' . $fileName;
        $config = $this->ftp_upload();

        $upload = $this->ftp->connect($config);
        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS . $newFileName, 'ascii', 0775);
        $upload = $this->ftp->close();

        //simpan file
        if ($upload) {
            $data['idberkas'] = $id_eNPWP;
            $data['idbarangdistributor'] = $idbarangdistributor;
            $data['jenisberkas'] = $jenisberkas;
            $data['path']  = $newFileName;
            $upload = $this->db->replace('rs_barang_distributor_berkas', $data);
        }

        $uploaded =  (($upload) ? ['error' => false, 'status' => 'success', 'message' => 'Unggah NPWP Berhasil.'] : ['error' => true, 'status' => 'danger', 'message' => 'Unggah NPWP Gagal.']);
        echo json_encode($uploaded);
    }

    //hapus file di nas storage
    public function delete_NPWP()
    {
        $idberkas =  $this->input->post('idberkas');
        $path =  $this->input->post('path');

        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);
        $delete = $this->ftp->delete_file(FOLDERNASSIMRS . $path);
        $delete = $this->ftp->close();

        //hapus file 
        if ($delete) {
            $where['idberkas'] = $idberkas;
            $delete = $this->db->delete('rs_barang_distributor_berkas', $where);
        }
        $deleted =  (($delete) ? ['error' => false, 'status' => 'success', 'message' => 'Hapus Berkas NPWP Berhasil.'] : ['error' => true, 'status' => 'danger', 'message' => 'Hapus Berkas NPWP Gagal.']);
        echo json_encode($deleted);
    }

    public function add_distributor()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DISTRIBUTOR)) //lihat define di atas
        {
            $data               = $this->settingdistributor();
            $this->load->helper('form');
            $data['data_edit'] = '';
            $data['title_page'] = 'Tambah Distributor';
            $data['mode']       = 'add';
            $data['datapbf']    = $this->db->get('rs_barang_pbf')->result();
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js']  = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function edit_distributor()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DISTRIBUTOR)) //lihat define di atas
        {
            $data                 = $this->settingdistributor();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('rs_barang_distributor');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*', ['idbarangdistributor' => $id])->row_array();
            $data['datapbf']    = $this->db->get('rs_barang_pbf')->result();
            $data['mode'] = 'edit';
            $data['title_page'] = 'Ubah Distributor';
            $data['plugins']    = [PLUG_DROPDOWN];
            $data['script_js'] = ['js_masterdata'];
            $this->load->view('v_index', $data);
        }
    }
    public function save_distributor() // SIMPAN JENIS ICD
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DISTRIBUTOR)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('distributor', '', 'required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $iddistributor =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('iddistributor')));
                $data = [
                    'idbarangpbf' => $this->input->post('idbarangpbf'),
                    'distributor' => $this->input->post('distributor'),
                    'alamat' => $this->input->post('alamat'),
                    'telepon' => $this->input->post('telepon'),
                    'norekening' => $this->input->post('norekening')
                ];
                $this->mgenerikbap->setTable('rs_barang_distributor');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $iddistributor);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($iddistributor) ? 'Save Success.!' : 'Update Success.!', empty($iddistributor) ? 'Save  Failed.!' : 'Update Failed.!');
            }
            redirect(base_url('cmasterdata/distributor'));
        }
    }

    //-- SETTING perpustakaan 
    public function settingperpustakaan()
    {
        return [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'perpustakaan',
            'content_view'     => 'masterdata/v_perpustakaan',
            'script_js'        => ['js_perpustakaan']
        ];
    }
    // -- master buku
    public function buku()
    {
        $data = $this->settingperpustakaan();
        $data['plugins']     = [PLUG_DATATABLE, PLUG_DROPDOWN];
        $data['title_page'] = 'Buku';
        $data['active_menu_level'] = 'buku';
        $data['jsmode']      = 'buku';
        $this->load->view('v_index', $data);
    }
    public function savebuku()
    {

        $this->load->model('mtriggermanual');
        $id = $this->input->post('idbuku');
        $data = ['judulbuku' => $this->input->post('judulbuku'), 'jumlahbuku' => $this->input->post('jumlahbuku'), 'jumlahhalaman' => $this->input->post('jumlahhalaman'), 'isbn' => $this->input->post('isbn'), 'tahunterbit' => $this->input->post('tahunterbit'), 'ringkasan' => $this->input->post('ringkasan'), 'idpenerbit' => $this->input->post('penerbit'), 'idpenulis' => $this->input->post('penulis'), 'idkategori' => $this->input->post('kategori')];
        if (empty($id)) {
            $arrMsg = $this->db->insert('pp_buku', $data);
            $this->mtriggermanual->aipp_buku($this->db->insert_id(), $data['idkategori'], $data['jumlahbuku']);
            $mode = 'Simpan ';
        } else {
            $arrMsg = $this->db->update('pp_buku', $data, ['idbuku' => $id]);
            $this->mtriggermanual->aupp_buku($id, $data['jumlahbuku']);
            $mode = 'Update ';
        }
        pesan_success_danger($arrMsg, $mode . 'Buku Berhasil.', $mode . 'Buku Gagal.', 'js');
    }
    public function getbuku()
    {
        $id = $this->input->post('id');
        echo json_encode($this->db->query("SELECT DISTINCT a.*, b.penulis, concat (ifnull(c.penerbit,''),', ', ifnull(c.kotapenerbit,''),', ', ifnull(a.tahunterbit,''),', ', ifnull(a.jumlahhalaman,'0'),' hlm') penerbit, d.kategori FROM pp_buku a, pp_penulis b, pp_penerbit c, pp_kategori d where b.idpenulis=a.idpenulis and c.idpenerbit=a.idpenerbit and d.idkategori=a.idkategori" . (($id == '') ? "" : " and a.idbuku='" . $id . "' "))->result());
    }

    public function hapusbuku()
    {
        $arrMsg = $this->db->delete('pp_buku', ['idbuku' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Buku Berhasil.', 'Hapus Buku Gagal.', 'js');
    }
    // -- master penulis
    //mahmud, clear
    public function penulis()
    {
        $data = $this->settingperpustakaan();
        $data['active_menu_level'] = 'penulis';
        $data['plugins']        = [PLUG_DATATABLE];
        $data['title_page']    = 'Penulis';
        $data['jsmode']      = 'penulis';
        $this->load->view('v_index', $data);
    }
    //simpan data penulis
    public function savepenulis()
    {
        $id = $this->input->post('idpenulis');
        $data = ['penulis' => $this->input->post('penulis')];
        if (empty($id)) {
            $arrMsg = $this->db->insert('pp_penulis', $data);
            $mode = 'Simpan';
        } else {
            $arrMsg = $this->db->update('pp_penulis', $data, ['idpenulis' => $id]);
            $mode = 'Ubah';
        }
        pesan_success_danger($arrMsg, ' Penulis Berhasil.', ' Penulis Gagal.', 'js');
    }
    // ambil data penerbit
    public function getpenulis()
    {
        $id = $this->input->post('id');
        echo json_encode($this->db->query("select * from pp_penulis" . ((empty($id)) ? "" : " where idpenulis='" . $id . "'"))->result());
    }
    //hapus penulis
    public function hapuspenulis()
    {
        $arrMsg = $this->db->delete('pp_penulis', ['idpenulis' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Penulis Berhasil.', 'Hapus Penulis Gagal.', 'js');
    }
    // -- master penerbit
    //mahmud, clear
    public function penerbit()
    {
        $data = $this->settingperpustakaan();
        $data['active_menu_level'] = 'penerbit';
        $data['plugins']     = [PLUG_DATATABLE];
        $data['title_page'] = 'Penerbit';
        $data['jsmode']      = 'penerbit';
        $this->load->view('v_index', $data);
    }
    public function savepenerbit()
    {
        $id = $this->input->post('idpenerbit');
        $data = ['penerbit' => $this->input->post('penerbit'), 'kotapenerbit' => $this->input->post('kota'), 'alamatpenerbit' => $this->input->post('alamat')];
        if (empty($id)) {
            $arrMsg = $this->db->insert('pp_penerbit', $data);
            $mode = 'Simpan';
        } else {
            $arrMsg = $this->db->update('pp_penerbit', $data, ['idpenerbit' => $id]);
            $mode = 'Ubah';
        }
        pesan_success_danger($arrMsg, ' Penerbit Berhasil.', ' Penerbit Gagal.', 'js');
    }
    public function getpenerbit()
    {
        $id = $this->input->post('id');
        echo json_encode($this->db->query("select * from pp_penerbit" . ((empty($id)) ? "" : " where idpenerbit='" . $id . "'"))->result());
    }
    public function hapuspenerbit()
    {
        $arrMsg = $this->db->delete('pp_penerbit', ['idpenerbit' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Penerbit Berhasil.', 'Hapus Penerbit Gagal.', 'js');
    }
    // -- master kategori buku
    public function kategori()
    {
        $data = $this->settingperpustakaan();
        $data['active_menu_level'] = 'kategori';
        $data['plugins']     = [PLUG_DATATABLE];
        $data['title_page'] = 'Kategori';
        $data['jsmode']      = 'kategori';
        $this->load->view('v_index', $data);
    }
    public function savekategori()
    {
        $id = $this->input->post('idkategori');
        $data = ['kategori' => $this->input->post('kategori')];
        if (empty($id)) {
            $arrMsg = $this->db->insert('pp_kategori', $data);
            $this->load->model('mtriggermanual');
            $arrMsg = $this->mtriggermanual->aipp_kategori($this->db->insert_id());
            $mode = 'Simpan';
        } else {
            $arrMsg = $this->db->update('pp_kategori', $data, ['idkategori' => $id]);
            $mode = 'Ubah';
        }

        pesan_success_danger($arrMsg, $mode . ' Kategori Berhasil.', $mode . ' Kategori Gagal.', 'js');
    }
    public function getkategori()
    {
        $id = $this->input->post('id');
        echo json_encode($this->db->query("select * from pp_kategori" . ((empty($id)) ? "" : " where idkategori='" . $id . "'"))->result());
    }
    public function hapuskategori()
    {
        $arrMsg = $this->db->delete('pp_kategori', ['idkategori' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Kategori Berhasil.', 'Hapus Kategori Gagal.', 'js');
    }

    public function getdataenumjson()
    {
        $this->load->model('mkombin');
        $table = '';
        $column = '';
        switch ($this->input->post('t')) {
            case 'ppas_a':
                $table = 'person_pasien_alergi';
                $column = 'jenisalergi';
                break;

            default:
                echo json_encode(['error' => 'true']);
                return;
                break;
        }
        echo json_encode($this->mkombin->get_data_enum($table, $column));
    }
    // cari pasien berdasarkan nama atau norm
    public function json_serachpasien()
    {
        $cari = $this->input->post('cari');
        $sql = "select ppas.norm as id, concat('RM: ',ppas.norm,' - ',ifnull(pper.identitas,''),' ',pper.namalengkap) as text from person_pasien ppas, person_person pper where pper.idperson=ppas.idperson and pper.status_person='A' and ppas.status_pasien='A' and pper.namalengkap like '%" . $cari . "%' or pper.idperson=ppas.idperson and pper.status_person='A' and ppas.status_pasien='A' and ppas.norm like '%" . $cari . "%' order by ppas.norm desc limit 10";
        echo json_encode($this->db->query($sql)->result_array());
    }

    // cari pasien berdasarkan nama atau norm
    public function caripasien()
    {
        $cari = $this->input->post('searchTerm');
        $sql = "select ppas.norm as id, concat('RM: ',ppas.norm,' - ',ifnull(pper.identitas,''),' ',pper.namalengkap) as text from person_pasien ppas, person_person pper where pper.idperson=ppas.idperson and pper.status_person='A' and ppas.status_pasien='A' and pper.namalengkap like '%" . $cari . "%' or pper.idperson=ppas.idperson and pper.status_person='A' and ppas.status_pasien='A' and ppas.norm like '%" . $cari . "%' order by ppas.norm desc limit 10";
        echo json_encode($this->db->query($sql)->result_array());
    }
    // cari harga barang per id
    public function cekhargabelimasterbarang()
    {
        $q = $this->db->select('kode,namabarang, hargabeli')->get_where('rs_barang', ['idbarang' => $this->input->post('idbarang'), 'hargabeli < ' => $this->input->post('hargabeli')]);
        if ($q->num_rows() > 0) {
            echo json_encode(['status' => 'ok', 'data' => $q->row_array()]);
        } else {
            echo json_encode(['status' => 'kosong']);
        }
    }


    //  ASET PERUSAHAAN 

    public function inventarisnonmedis()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'inventarisnonmedis',
            'content_view'     => 'masterdata/v_inventaris_nonmedis',
            'script_js'        => ['master/inventaris/js_inventaris_nonmedis'],
            'title_page'       => 'Inventaris Non Medis',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_inventarisnonmedis()
    {
        $getData = $this->mdatatable->dt_inventarisnonmedis_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->kodeblok;
                $row[] = $obj->namablok;
                $row[] = $obj->koderuang;
                $row[] = $obj->namaruanginventaris;
                $row[] = convertToRupiah($obj->jumlahnonmedis);
                $row[] = '<a id="detailinventaris" namaruang="' . $obj->namaruanginventaris . '" alt="' . $obj->idruanginventaris . '" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_inventarisnonmedis(),
            "recordsFiltered" => $this->mdatatable->filter_dt_inventarisnonmedis(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    //detail inventaris nonmedis
    public function dt_detailinventarisnonmedis()
    {
        $getData = $this->mdatatable->dt_detailinventarisnonmedis_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->namabaranginventaris;
                $row[] = $obj->merekbarang;
                $row[] = $obj->modelbarang;
                $row[] = $obj->nomorseri;
                $row[] = $obj->nomorinventaris;
                $row[] = $obj->nomorsertifikat;
                $row[] = $obj->tanggalpembelian;
                $row[] = convertToRupiah($obj->nilaiaset);
                $row[] = convertToRupiah($obj->nilaipenyusutan);
                $row[] = $obj->status;
                $row[] = (($obj->status == 'pindah') ? '' : '<a id="editinventaris" alt="' . $obj->idinventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>');
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_detailinventarisnonmedis(),
            "recordsFiltered" => $this->mdatatable->filter_dt_detailinventarisnonmedis(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }


    //mahmud, clear
    public  function inventariskantor()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'inventaris',
            'content_view'     => 'masterdata/v_inventaris_inventaris',
            'script_js'        => ['master/inventaris/js_inventaris_inventaris'],
            'title_page'       => 'Inventaris Medis',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_inventariskantor()
    {
        $getData = $this->mdatatable->dt_inventariskantor_(true);
        $data = [];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    $obj->namabaranginventaris,
                    $obj->merekbarang,
                    $obj->modelbarang,
                    $obj->nomorseri,
                    $obj->namablok . ' [' . $obj->namaruanginventaris . ']',
                    $obj->nomorinventaris,
                    $obj->nomorsertifikat,
                    $obj->tanggalpembelian,
                    convertToRupiah($obj->nilaiaset),
                    convertToRupiah($obj->nilaipenyusutan),
                    $obj->status,
                ];
                $row[] = (($obj->status == 'pindah') ? '' : '<a id="editinventaris" alt="' . $obj->idinventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>');
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_inventariskantor(),
            "recordsFiltered" => $this->mdatatable->filter_dt_inventariskantor(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function onreadyform_inventaris()
    {
        $idinventaris = $this->input->post('idv');
        $dataedit = $this->db->select('ii.*, ibd.tanggalpembelian, ibd.nilaiaset, ibd.nilaipenyusutan, ibd.tanggalpengujian, ibd.nomorseri, ibd.nomorsertifikat, rs.namasatuan')->join('inventaris_barangdetail ibd', 'ibd.idbarangdetail=ii.idbarangdetail')->join('rs_satuan rs', 'rs.idsatuan=ibd.idsatuan')->get_where('inventaris_inventaris ii', ['idinventaris' => $idinventaris])->row_array();
        $hasil = array(
            'status' => $this->mmasterdata->get_data_enum('inventaris_inventaris', 'status'),
            'barang' => empty($idinventaris) ? [] : $this->db->select(' concat(idbarangdetail,",",ifnull(ibd.nomorseri,""),",",ifnull(ibd.nomorsertifikat,""),",",ifnull(ibd.nilaiaset,0),",",ifnull(ibd.nilaipenyusutan,0),",",ifnull(ibd.tanggalpembelian,""),",",ifnull(ibd.tanggalpengujian,""),",",ifnull(rs.namasatuan,"")) as id, concat( "No.Seri: ",ifnull(ibd.nomorseri,"")," ",", No.Sertifikat: ", ifnull(ibd.nomorsertifikat,""),", Merek: ",ifnull(imm.merekbarang,""),"," ,", Model: ",ifnull(im.modelbarang,""),", ", ifnull(ib.kodebaranginventaris,"")," - ", ib.namabaranginventaris )  txt')->join('inventaris_model im', 'im.idmodelbarang=ibd.idmodelbarang')->join('inventaris_merek imm', 'imm.idmerekbarang=im.idmerekbarang')->join('inventaris_barang ib', 'ib.idbaranginventaris=ibd.idbaranginventaris')->join('rs_satuan rs', 'rs.idsatuan=ibd.idsatuan')->get_where('inventaris_barangdetail ibd', ['ibd.idbarangdetail' => $dataedit['idbarangdetail']])->result_array(),
            'ruang' => empty($idinventaris) ? [] : $this->db->select('idruanginventaris as id, concat( "Blok: ",ifnull(ib.kodeblok,""), "-" ,ib.namablok ,", Ruang: ",  ifnull(koderuang,"")," ", namaruanginventaris )  txt')->join('inventaris_blok ib', 'ib.idblokinventaris=ir.idblokinventaris')->get_where('inventaris_ruang ir', ['ir.idruanginventaris' => $dataedit['idruanginventaris']])->result_array(),
            'dataedit' => $dataedit
        );
        echo json_encode($hasil);
    }
    public  function save_inventaris()
    {
        $iduserlog =  $this->get_iduserlogin();
        //nomorinventaris dibuat otomatis di trigger => bi_inventaris_inventaris
        $post = $this->input->post();
        //tambah inventaris
        if (empty($post['idinventaris'])) {
            for ($x = 0; $x < $post['jumlah']; $x++) {
                $data = [
                    'idbarangdetail' => $post['idbarangdetail'],
                    'status' => $post['status'],
                    'keterangan' => $post['keterangan'],
                    'nourut' => $x,
                    'idruanginventaris' => $post['idruanginventaris'],
                    'iduserterakhir' => $iduserlog
                ];
                $arrMsg = $this->db->insert('inventaris_inventaris', $data);
            }
        } else // ubah atau pindah inventaris
        {

            if ($post['status'] == 'pindah') //jika status pindah
            {
                //insert pindah inventaris
                $data = [
                    'idbarangdetail' => $post['idbarangdetailsebelum'],
                    'status' => 'baik',
                    'keterangan' => $post['keterangan'],
                    'nourut' => 0,
                    'idruanginventaris' => $post['idpindahruanginventaris'],
                    'idinventarissebelum' => $post['idinventaris'],
                    'iduserterakhir' => $iduserlog
                ];
                $arrMsg = $this->db->insert('inventaris_inventaris', $data);
            }
            //update inventaris lama
            $data = [
                'status' => $post['status'],
                'keterangan' => $post['keterangan'],
                'iduserterakhir' => $iduserlog
            ];
            $arrMsg = $this->db->update('inventaris_inventaris', $data, ['idinventaris' => $post['idinventaris']]);
        }
        $mode = empty($post['idinventaris']) ? 'Tambah' : 'Ubah';
        pesan_success_danger($arrMsg, $mode . ' Berhasil.', $mode . ' Gagal.', 'js');
    }

    //  KATEGORI ASET
    // mahmud, clear
    public  function kategorialat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'kategorialat',
            'content_view'     => 'masterdata/v_inventaris_barangkategori',
            'script_js'        => ['master/inventaris/js_inventaris_barangkategori'],
            'title_page'       => 'Kategori Alat',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_inv_barangkategori()
    {
        $getData = $this->mdatatable->dt_inv_barangkategori_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    $obj->kategori
                ];
                $row[] = '<a id="edit" alt="' . $obj->idbarangkategori . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idbarangkategori . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_inv_barangkategori(),
            "recordsFiltered" => $this->mdatatable->filter_dt_inv_barangkategori(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformkategori()
    {
        echo json_encode($this->db->get_where('inventaris_barangkategori', ['idbarangkategori' => $this->input->post('i')])->row_array());
    }

    public  function save_inv_barangkategori()
    {
        $post = $this->input->post();
        $data = ['kategori' => $post['namakategori']];
        $save = (empty($post['idkategori'])) ? $this->db->insert('inventaris_barangkategori', $data) : $this->db->update('inventaris_barangkategori', $data, ['idbarangkategori' => $post['idkategori']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_inv_barangkategori()
    {
        $arrMsg = $this->db->delete('inventaris_barangkategori', ['idbarangkategori' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //  Ruang Alat inventaris
    // mahmud, clear
    public  function ruanginvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'ruanginvalat',
            'content_view'     => 'masterdata/v_inventaris_ruang',
            'script_js'        => ['master/inventaris/js_inventaris_ruang'],
            'title_page'       => 'Ruang Inventaris Alat',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    // Data Table Ruang  Inventaris Alat 
    public function dt_ruanginvalat()
    {
        $getData = $this->mdatatable->dt_ruanginvalat_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    $obj->kodeblok . '-' . $obj->namablok,
                    (($obj->koderuang == '') ? '' : $obj->koderuang . '-') . $obj->namaruanginventaris
                ];
                $row[] = '<a id="edit" alt="' . $obj->idruanginventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idruanginventaris . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_ruanginvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_ruanginvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformruanginvalat()
    {
        $ruang = $this->db->get_where('inventaris_ruang ir', ['ir.idruanginventaris' => $this->input->post('i')])->row_array();
        $blok  = $this->db->select('idblokinventaris as id, concat(kodeblok,"-",namablok) as txt')->get('inventaris_blok')->result_array();
        echo json_encode(['ruang' => $ruang, 'blok' => $blok]);
    }

    public  function save_ruanginvalat()
    {
        $post = $this->input->post();
        $data = ['idblokinventaris' => $post['idblok'], 'namaruanginventaris' => $post['namaruang'], 'koderuang' => $post['koderuang']];
        $this->mgenerikbap->setTable('inventaris_ruang');
        $save = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['idruang']);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_ruanginvalat()
    {
        $arrMsg = $this->db->delete('inventaris_ruang', ['idruanginventaris' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //  Unit Alat inventaris
    // mahmud, clear
    public  function blokinvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'blokinvalat',
            'content_view'     => 'masterdata/v_inventaris_blok',
            'script_js'        => ['master/inventaris/js_inventaris_blok'],
            'title_page'       => 'Data Blok',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index', $data);
    }

    // Data Table Blok  Inventaris Alat 
    public function dt_blokinvalat()
    {
        $getData = $this->mdatatable->dt_blokinvalat_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    $obj->kodeblok,
                    $obj->namablok
                ];
                $row[] = '<a id="edit" alt="' . $obj->idblokinventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idblokinventaris . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_blokinvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_blokinvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformblokinvalat()
    {
        echo json_encode($this->db->get_where('inventaris_blok ib', ['ib.idblokinventaris' => $this->input->post('i')])->row_array());
    }

    public  function save_blokinvalat()
    {
        $post = $this->input->post();
        $data = ['kodeblok' => $post['kodeblok'], 'namablok' => $post['namablok']];
        $this->mgenerikbap->setTable('inventaris_blok');
        $save = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['idblok']);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_blokinvalat()
    {
        $arrMsg = $this->db->delete('inventaris_blok', ['idblokinventaris' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //Merek Barang Inventaris
    public function merekinvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'merekbarang',
            'content_view'     => 'masterdata/v_inventaris_merekbarang',
            'script_js'        => ['master/inventaris/js_inventaris_merekbarang'],
            'title_page'       => 'Merek Barang',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index', $data);
    }

    // Data Table Merek Barang  Inventaris Alat 
    public function dt_merekinvalat()
    {
        $getData = $this->mdatatable->dt_merekinvalat_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    $obj->merekbarang
                ];
                $row[] = '<a id="edit" alt="' . $obj->idmerekbarang . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>';
                //                <a id="delete" alt="'.$obj->idmerekbarang.'" class="btn btn-xs btn-danger" '. ql_tooltip('hapus').'><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_merekinvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_merekinvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformmerekinvalat()
    {
        echo json_encode($this->db->get_where('inventaris_merek', ['idmerekbarang' => $this->input->post('i')])->row_array());
    }
    public  function save_merekinvalat()
    {
        $post = $this->input->post();
        $data = ['merekbarang' => $post['merek']];
        $save = (empty($post['idmerek'])) ? $this->db->insert('inventaris_merek', $data) : $this->db->update('inventaris_merek', $data, ['idmerekbarang' => $post['idmerek']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_merekinvalat()
    {
        $arrMsg = $this->db->delete('inventaris_merek', ['idmerekbarang' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //Model Alat Inventaris
    public function modelinvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'modelbarang',
            'content_view'     => 'masterdata/v_inventaris_modelbarang',
            'script_js'        => ['master/inventaris/js_inventaris_modelbarang'],
            'title_page'       => 'Model Barang',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    // Data Table Model Barang  Inventaris Alat 
    // mahmud, clear
    public function dt_modelinvalat()
    {
        $getData = $this->mdatatable->dt_modelinvalat_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    $obj->merekbarang, $obj->modelbarang
                ];
                $row[] = '<a id="edit" alt="' . $obj->idmodelbarang . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idmodelbarang . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_modelinvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_modelinvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformmodelinvalat()
    {
        $model = $this->db->get_where('inventaris_model', ['idmodelbarang' => $this->input->post('i')])->row_array();
        $merek = $this->db->select('idmerekbarang as id, merekbarang as txt')->get('inventaris_merek')->result_array();
        echo json_encode(['model' => $model, 'merek' => $merek]);
    }
    public  function save_modelinvalat()
    {
        $post = $this->input->post();
        $data = ['idmerekbarang' => $post['idmerek'], 'modelbarang' => $post['modelbarang']];
        $save = (empty($post['idmodel'])) ? $this->db->insert('inventaris_model', $data) : $this->db->update('inventaris_model', $data, ['idmodelbarang' => $post['idmodel']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_modelinvalat()
    {
        $arrMsg = $this->db->delete('inventaris_model', ['idmodelbarang' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //Master Inventaris Alat
    public function masterinvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'masterinvalat',
            'content_view'     => 'masterdata/v_inventaris_masteralat',
            'script_js'        => ['master/inventaris/js_inventaris_masteralat'],
            'title_page'       => 'List Master Barang/Alat',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_masterinvalat()
    {
        $getData = $this->mdatatable->dt_masterinvalat_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no, $obj->kodebaranginventaris . '-' . $obj->namabaranginventaris,
                    $obj->jenisalat,
                    $obj->kategori
                ];

                $row[] = '<a id="edit" alt="' . $obj->idbaranginventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idbaranginventaris . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_masterinvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_masterinvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformmasterinvalat()
    {
        $barang = $this->db->get_where('inventaris_barang', ['idbaranginventaris' => $this->input->post('i')])->row_array();
        $jenis    = $this->mmasterdata->get_data_enum('inventaris_barang', 'jenisalat');
        $kategori = $this->db->select('idbarangkategori as id, kategori as txt')->get('inventaris_barangkategori')->result_array();
        echo json_encode(['barang' => $barang, 'kategori' => $kategori, 'jenis' => $jenis]);
    }
    public  function save_masterinvalat()
    {
        $post = $this->input->post();
        $data = [
            'idbarangkategori' => $post['kategori'],
            'jenisalat' => $post['jenis'],
            'namabaranginventaris' => $post['namabarang'],
            'kodebaranginventaris' => $post['kodebarang']
        ];

        $this->mgenerikbap->setTable('inventaris_barang');
        $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['idbarang']);
        $mode = empty($post['idbarang']) ? 'Simpan' : 'Ubah';
        pesan_success_danger($simpan,  $mode . ' Berhasil.', $mode . ' Gagal.', 'js');
    }
    public function hapus_masterinvalat()
    {
        $this->mgenerikbap->setTable('inventaris_barang');
        $arrMsg = $this->mgenerikbap->delete($this->input->post('a'));
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //Master Inventaris Alat
    public function iteminvalat()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'iteminvalat',
            'content_view'     => 'masterdata/v_inventaris_itemalat',
            'script_js'        => ['master/inventaris/js_inventaris_itemalat'],
            'title_page'       => 'List Item Barang/Alat',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_iteminvalat()
    {
        $getData = $this->mdatatable->dt_iteminvalat_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no,
                    '<b>' . $obj->kodebaranginventaris . '</b> ' . $obj->namabaranginventaris,
                    $obj->merekbarang . '-<b>' . $obj->modelbarang . '</b>',
                    $obj->jumlah,
                    $obj->namasatuan,
                    $obj->tanggalpembelian,
                    $obj->tanggalpengujian,
                    $obj->tanggalrekalibrasi,
                    convertToRupiah($obj->nilaiaset),
                    convertToRupiah($obj->nilaipenyusutan),
                    $obj->bataspenyusutan
                ];
                $row[] = '<a id="edit" alt="' . $obj->idbarangdetail . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idbarangdetail . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $row[] = $obj->nomorseri;
                $row[] = $obj->nomorsertifikat;
                $row[] = $obj->jenisalat;
                $row[] = $obj->kategori;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_iteminvalat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_iteminvalat(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformiteminvalat()
    {
        $detail = $this->db->get_where('inventaris_barangdetail', ['idbarangdetail' => $this->input->post('i')])->row_array();
        $barang = $this->db->select('idbaranginventaris as id, concat(kodebaranginventaris,"-", namabaranginventaris) as txt')->get('inventaris_barang')->result_array();
        $model = $this->db->select('im.idmodelbarang as id, concat(imr.merekbarang," - ",im.modelbarang) as txt')->join('inventaris_merek imr', 'imr.idmerekbarang=im.idmerekbarang')->get('inventaris_model im')->result_array();
        $satuan = $this->db->select('idsatuan as id, namasatuan as txt')->get('rs_satuan')->result_array();
        echo json_encode(['barang' => $barang, 'model' => $model, 'satuan' => $satuan, 'detail' => $detail]);
    }
    public  function save_iteminvalat()
    {
        $post = $this->input->post();
        $data = [
            'idmodelbarang' => $post['idmodelbarang'],
            'nomorseri' => $post['nomorseri'],
            'nomorsertifikat' => $post['nomorsertifikat'],
            'jumlah' => unconvertToRupiah($post['jumlah']),
            'idsatuan' => $post['idsatuan'],
            'tanggalpembelian' => $post['tglpembelian'],
            'tanggalpengujian' => $post['tglpengujian'],
            'tanggalrekalibrasi' => $post['tglrekalibrasi'],
            'nilaiaset' => unconvertToRupiah($post['nilaiaset']),
            'nilaipenyusutan' => unconvertToRupiah($post['nilaipenyusutan']),
            'bataspenyusutan' => unconvertToRupiah($post['bataspenyusutan'])
        ];

        if (empty($post['idbarangdetail'])) {
            $data['idbaranginventaris'] = $post['idbaranginventaris'];
        }
        $this->mgenerikbap->setTable('inventaris_barangdetail');
        $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['idbarangdetail']);
        pesan_success_danger($simpan, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_iteminvalat()
    {
        $arrMsg = $this->db->update('inventaris_barangdetail', ['isdelete' => 1], ['idbarangdetail' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    //    Pengeluaran - master barang
    // mahmud, clear
    public  function masterbarangpengeluaran()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'pengeluaran',
            'active_menu_level' => 'pengeluaranmasterbarang',
            'content_view'     => 'masterdata/v_pengeluaran_masterbarang',
            'script_js'        => ['master/pengeluaran/js_masterbarang'],
            'title_page'       => 'Master Barang',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_masterbarangpengeluaran()
    {
        $getData = $this->mdatatable->dt_masterbarangpengeluaran_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [++$no, $obj->namabarang, $obj->namasatuan];
                $row[] = '<a id="edit" alt="' . $obj->idbarang . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idbarang . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_masterbarangpengeluaran(),
            "recordsFiltered" => $this->mdatatable->filter_dt_masterbarangpengeluaran(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformmasterbarangpengeluaran()
    {
        $data = [
            'satuan' => $this->db->select('idsatuan id, namasatuan txt')->get('rs_satuan')->result_array(),
            'edit' => $this->db->get_where('rs_pengeluaran_masterbarang', ['idbarang' => $this->input->post('i')])->row_array()
        ];
        echo json_encode($data);
    }

    public  function save_masterbarangpengeluaran()
    {
        $post = $this->input->post();
        $iduser = $this->get_iduserlogin();
        $data = ['namabarang' => $post['namabarang'], 'idsatuan' => $post['idsatuan'], 'iduserupdate' => $iduser];
        $save = (empty($post['idbarang'])) ? $this->db->insert('rs_pengeluaran_masterbarang', $data) : $this->db->update('rs_pengeluaran_masterbarang', $data, ['idbarang' => $post['idbarang']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_masterbarangpengeluaran()
    {
        $arrMsg = $this->db->update('rs_pengeluaran_masterbarang', ['isdelete' => '1'], ['idbarang' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    //    KATEGORI BIAYA
    // mahmud, clear
    public  function kategoribiaya()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'pengeluaran',
            'active_menu_level' => 'biayakategori',
            'content_view'     => 'masterdata/v_biaya_kategori',
            'script_js'        => ['master/js_biaya_kategori'],
            'title_page'       => 'Kategori Biaya',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_kategoribiaya()
    {
        $getData = $this->mdatatable->dt_kategoribiaya_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [++$no, $obj->kategoribiaya];
                $row[] = '<a id="edit" alt="' . $obj->idkategoribiaya . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idkategoribiaya . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_kategoribiaya(),
            "recordsFiltered" => $this->mdatatable->filter_dt_kategoribiaya(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyform_kategoribiaya()
    {
        echo json_encode($this->db->get_where('rs_biaya_kategori', ['idkategoribiaya' => $this->input->post('i')])->row_array());
    }

    public  function save_kategoribiaya()
    {
        $post = $this->input->post();
        $data = ['kategoribiaya' => $post['kategoribiaya']];
        $save = (empty($post['idkategoribiaya'])) ? $this->db->insert('rs_biaya_kategori', $data) : $this->db->update('rs_biaya_kategori', $data, ['idkategoribiaya' => $post['idkategoribiaya']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_kategoribiaya()
    {
        $arrMsg = $this->db->update('rs_biaya_kategori', ['isdelete' => '1'], ['idkategoribiaya' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }


    // SUB KATEGORI BIAYA
    // mahmud, clear
    public  function subkategoribiaya()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'pengeluaran',
            'active_menu_level' => 'biayasubkategori',
            'content_view'     => 'masterdata/v_biaya_subkategori',
            'script_js'        => ['master/js_biaya_subkategori'],
            'title_page'       => 'Sub Kategori Biaya',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_subkategoribiaya()
    {
        $getData = $this->mdatatable->dt_subkategoribiaya_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [++$no, $obj->kategoribiaya, $obj->subkategoribiaya];
                $row[] = '<a id="edit" alt="' . $obj->idsubkategoribiaya . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idsubkategoribiaya . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_subkategoribiaya(),
            "recordsFiltered" => $this->mdatatable->filter_dt_subkategoribiaya(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyform_subkategoribiaya()
    {
        $data = [
            'edit' => $this->db->get_where('rs_biaya_subkategori', ['idsubkategoribiaya' => $this->input->post('i')])->row_array(),
            'kategori' => $this->db->select('idkategoribiaya as id, kategoribiaya as txt')->where('isdelete', '0')->get('rs_biaya_kategori')->result_array()
        ];
        echo json_encode($data);
    }

    public  function save_subkategoribiaya()
    {
        $post = $this->input->post();
        $data = ['subkategoribiaya' => $post['subkategoribiaya'], 'idkategoribiaya' => $post['idkategoribiaya']];
        $save = (empty($post['idsubkategoribiaya'])) ? $this->db->insert('rs_biaya_subkategori', $data) : $this->db->update('rs_biaya_subkategori', $data, ['idsubkategoribiaya' => $post['idsubkategoribiaya']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_subkategoribiaya()
    {
        $arrMsg = $this->db->update('rs_biaya_subkategori', ['isdelete' => '1'], ['idsubkategoribiaya' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    // SUB KATEGORI BIAYA
    // mahmud, clear
    public  function subsubkategoribiaya()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'pengeluaran',
            'active_menu_level' => 'biayasubsubkategori',
            'content_view'     => 'masterdata/v_biaya_subsubkategori',
            'script_js'        => ['master/js_biaya_subsubkategori'],
            'title_page'       => 'Sub-sub Kategori Biaya',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_subsubkategoribiaya()
    {
        $getData = $this->mdatatable->dt_subsubkategoribiaya_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [++$no, $obj->kategoribiaya, $obj->subkategoribiaya, $obj->subsubkategoribiaya];
                $row[] = '<a id="edit" alt="' . $obj->idsubsubkategoribiaya . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->idsubsubkategoribiaya . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_subsubkategoribiaya(),
            "recordsFiltered" => $this->mdatatable->filter_dt_subsubkategoribiaya(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyform_subsubkategoribiaya()
    {
        $data = [
            'edit' => $this->db->get_where('rs_biaya_subsubkategori', ['idsubsubkategoribiaya' => $this->input->post('i')])->row_array(),
            'subkategori' => $this->db->select('idsubkategoribiaya as id, subkategoribiaya as txt')->where('isdelete', '0')->get('rs_biaya_subkategori')->result_array()
        ];
        echo json_encode($data);
    }

    public  function save_subsubkategoribiaya()
    {
        $post = $this->input->post();
        $data = ['subsubkategoribiaya' => $post['subsubkategoribiaya'], 'idsubkategoribiaya' => $post['idsubkategoribiaya']];
        $save = (empty($post['idsubsubkategoribiaya'])) ? $this->db->insert('rs_biaya_subsubkategori', $data) : $this->db->update('rs_biaya_subsubkategori', $data, ['idsubsubkategoribiaya' => $post['idsubsubkategoribiaya']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.', 'js');
    }
    public function hapus_subsubkategoribiaya()
    {
        $arrMsg = $this->db->update('rs_biaya_subsubkategori', ['isdelete' => '1'], ['idsubsubkategoribiaya' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    public  function get_combostatusklaim()
    {
        echo json_encode($this->db->select("idstatusklaim as id, statusklaim as txt")->where('isselesai', 0)->get("rs_statusklaim")->result());
    }
    public function get_combolistbank()
    {
        echo json_encode($this->db->select(" concat(idbank,',',ifnull(kodebank,''),',',ifnull(biayaadmin,0) ) as id, concat(ifnull(kodebank,''),' : ',namabank) as txt")->get("bank_bank")->result());
    }

    public  function get_enumjenispembayaran()
    {
        $data = $this->mmasterdata->get_data_enum('keu_tagihan', 'jenispembayaran');
        $result = [];
        foreach ($data as $value) {
            $enum = [];
            $enum['id'] = $value;
            $enum['txt'] = $value;
            $result[] = $enum;
        }
        echo json_encode($result);
    }

    public  function get_enumjenistagihan()
    {
        echo json_encode($this->mmasterdata->get_data_enum('keu_tagihan', 'jenistagihan'));
    }


    // --- MASTER TASI
    // mahmud, clear
    public  function mastertarifoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIFOPERASI)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'tarif',
                'active_menu_level' => 'mastertarifoperasi',
                'content_view'     => 'masterdata/v_mastertarifoperasi',
                'script_js'        => ['js_mastertarifoperasi'],
                'tarif_orto'       => $this->db->get('rs_mastertarif_operasi_bpjsorthopedi')->row_array(),
                'tarif_nonsc'      => $this->db->get('rs_mastertarif_operasi_bpjsnonsc')->result_array(),
                'tarif_sc'         => $this->db->get('rs_mastertarif_operasi_bpjssc')->result_array(),
                'kelas'            => $this->db->get('rs_kelas')->result_array(),
                'title_page'       => 'Tarif Operasi Umum',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
            ];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_mastertarifoperasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERTARIFOPERASI)) //lihat define di atas
        {
            $getData = $this->mdatatable->dt_mastertarifoperasi_(true);
            $data = [];
            $no = $_POST['start'];
            if ($getData) {
                foreach ($getData->result() as $obj) {
                    $menu  = '<a id="formtarif" idmastertarifoperasi="' . $obj->idmastertarifoperasi . '" class="btn btn-xs btn-warning" ' .  ql_tooltip('Ubah') . '><i class="fa fa-edit"></i></a>';
                    $menu .= ' <a id="hapusmastertarif" idmastertarifoperasi="' . $obj->idmastertarifoperasi . '" class="btn btn-xs btn-danger" ' .  ql_tooltip('Hapus') . '><i class="fa fa-trash"></i></a>';

                    $row = [];
                    $row[] = $obj->jenistarifoperasi;
                    $row[] = $obj->jenistindakan;
                    $row[] = $obj->jenistarif;
                    $row[] = convertToRupiah($obj->jasamedis);
                    $row[] = convertToRupiah($obj->jasars);
                    $row[] = convertToRupiah($obj->total);
                    $row[] = $menu;
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_mastertarifoperasi(),
                "recordsFiltered" => $this->mdatatable->filter_dt_mastertarifoperasi(),
                "data" => $data
            );
            //output dalam format JSON
            echo json_encode($output);
        } else {
            aksesditolak();
        }
    }

    public function save_mastertarifoperasi()
    {
        $data['idjenistarifoperasi'] = $this->input->post('jenistarifoperasi');
        $data['idjenistarif'] = $this->input->post('jenistarif');
        $data['jenistindakan'] = $this->input->post('jenistindakan');
        $data['jasamedis']    = unconvertToRupiah($this->input->post('jasamedis'));
        $data['jasars']       = unconvertToRupiah($this->input->post('jasars'));

        if (empty($this->input->post('idmastertarifoperasi'))) {
            $arrMsg = $this->db->insert('rs_mastertarif_operasi', $data);
        } else {
            $arrMsg = $this->db->update('rs_mastertarif_operasi', $data, ['idmastertarifoperasi' => $this->input->post('idmastertarifoperasi')]);
        }

        pesan_success_danger($arrMsg, 'Save Tarif Berhasil.', 'Save Tarif Gagal.', 'js');
    }

    public function delete_mastertarifoperasi()
    {
        $arrMsg = $this->db->delete('rs_mastertarif_operasi', ['idmastertarifoperasi' => $this->input->post('idmastertarifoperasi')]);
        pesan_success_danger($arrMsg, 'Hapus Tarif Berhasil.', 'Hapus Tarif Gagal.', 'js');
    }

    public function form_readymastertarifoperasi()
    {
        $idop = $this->input->post('idop');

        $data['edit'] = ((empty($idop)) ? '' : $this->db->get_where('rs_mastertarif_operasi', ['idmastertarifoperasi' => $idop])->row_array());
        $data['jenistindakan'] = ['tunggal', 'ganda', 'multiple'];
        $data['jenistarif']    = $this->db->select('idjenistarif as id, jenistarif as text')->get('rs_jenistarif')->result();
        $data['jenistarifoperasi'] = $this->db->select('idjenistarifoperasi as id, jenistarifoperasi as text')->get('rs_jenistarif_operasi')->result();
        echo json_encode($data);
    }

    //fungsi ubah tarif persentasi ortopedi
    public function settarif_operasi_ortopedi()
    {
        $persentase = $this->input->post('persentase');
        $arrMsg = $this->db->update('rs_mastertarif_operasi_bpjsorthopedi', ['persentase' => $persentase]);
        pesan_success_danger($arrMsg, 'Ubah Persentase Berhasil.', 'Ubah Persentase Gagal.', 'js');
    }

    //fungsi hapus tarif operasi nonsc bpjs
    public function hapustarif_trfnonsc()
    {
        $id = $this->input->post('idplafon');
        $arrMsg = $this->db->delete('rs_mastertarif_operasi_bpjsnonsc', ['idplafon' => $id]);
        pesan_success_danger($arrMsg, 'Hapus Tarif Berhasil.', 'Hapus Tarif Gagal.', 'js');
    }
    //fungsi update tarif OP non sc bpjs
    public function updatetarif_trfnonsc()
    {
        $dtupdate['plafon'] = $this->input->post('plafon');
        $dtupdate['tarif']  = unconvertToRupiah($this->input->post('tarif'));
        $where = ['idplafon' => $this->input->post('idplafon')];
        $arrMsg = $this->db->update('rs_mastertarif_operasi_bpjsnonsc', $dtupdate, $where);
        pesan_success_danger($arrMsg, 'Ubah Tarif Berhasil.', 'Ubah Tarif Gagal.', 'js');
    }

    //fungsi update tarif OP sc bpjs
    public function updatetarif_trfsc()
    {
        $dtupdate['tarif']  = unconvertToRupiah($this->input->post('tarif'));
        $where = ['idkelas' => $this->input->post('idkelas')];
        $arrMsg = $this->db->update('rs_mastertarif_operasi_bpjssc', $dtupdate, $where);
        pesan_success_danger($arrMsg, 'Ubah Tarif Berhasil.', 'Ubah Tarif Gagal.', 'js');
    }

    // --- MASTER DISKON
    // mahmud, clear
    public  function masterdiskon()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'tarif',
                'active_menu_level' => 'masterdiskon',
                'content_view'     => 'masterdata/v_masterdiskon',
                'script_js'        => ['js_masterdiskon'],
                'title_page'       => 'Master Diskon Pasien',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
            ];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_masterdiskon()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $getData = $this->mdatatable->dt_masterdiskon_(true);
            $data = [];
            if ($getData) {
                foreach ($getData->result() as $obj) {
                    $row = [];
                    $row[] = $obj->icd;
                    $row[] = $obj->namaicd;
                    $row[] = $obj->member;
                    $row[] = $obj->kelas;
                    $row[] = $obj->jenistarif;
                    $row[] = $obj->tanggalmulai;
                    $row[] = $obj->tanggalselesai;
                    $row[] = $obj->jenisdiskon;
                    $row[] = ($obj->jenisinput) ? 'Manual' : 'Otomatis';
                    $row[] = convertToRupiah($obj->nominal) . (($obj->jenisdiskon == 'persentase') ? '%' : '');
                    $row[] = '<a id="edit" alt="' . $obj->idmasterdiskon . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>';
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_masterdiskon(),
                "recordsFiltered" => $this->mdatatable->filter_dt_masterdiskon(),
                "data" => $data
            );
            //output dalam format JSON
            echo json_encode($output);
        } else {
            aksesditolak();
        }
    }
    public function  formreadymasterdiskon()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {

            $dtmember = $this->db->select("idmember as id, concat('<b>',member,'</b> ',ifnull(keterangan,'')) as txt")->get_where('rs_mastermember', ['isdelete' => 0])->result_array();
            $data = [
                'jenistarif' => $this->db->select('idjenistarif as id, jenistarif as txt')->get_where('rs_jenistarif', ['jenistarif' => 'diskon'])->result_array(),
                'jenisdiskon' => $this->mmasterdata->get_data_enum('rs_masterdiskon', 'jenisdiskon'),
                'kelas' => $this->db->select('idkelas as id, kelas as txt')->get_where('rs_kelas', ['kelas' => 'rawat jalan'])->result_array(),
                'diskon' => $this->db->select("rm.*, (select ri.namaicd from rs_icd ri where ri.icd=rm.icd) as namaicd, (select concat('<b>',ifnull(member,''),'</b> ', ifnull(keterangan,'')) from rs_mastermember where idmember=rm.idmember) as member")->get_where("rs_masterdiskon rm", ['rm.idmasterdiskon' => $this->input->post('id')])->row_array()
            ];
            echo json_encode($data);
        } else {
            aksesditolak();
        }
    }

    public  function save_masterdiskon()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $post = $this->input->post();
            $data = [
                'idkelas' => $post['idkelas'],
                'idjenistarif' => $post['idjenistarif'],
                'jenisdiskon' => $post['jenisdiskon'],
                'icd' => $post['icd'],
                'tanggalmulai' => $post['tglmulai'],
                'tanggalselesai' => $post['tglakhir'],
                'nominal' => $post['nominal'],
                'jenisinput' => $post['jenisinput']
            ];
            if (isset($post['idmember'])) {
                $data['idmember'] = $post['idmember'];
            }
            $this->mgenerikbap->setTable('rs_masterdiskon');
            $simpan = $this->mgenerikbap->update_or_insert_replaceonduplicate($data, $post['idmasterdiskon']);
            $modesimpan = empty($post['idmasterdiskon']) ? 'Tambah' : 'Ubah';
            pesan_success_danger($simpan, $modesimpan . ' Diskon Berhasil.', $modesimpan . ' Diskon Gagal.', 'js');
        } else {
            aksesditolak();
        }
    }



    // --- MASTER DISKON
    // mahmud, clear
    public  function masterdiskonpegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'masterdata',
                'active_sub_menu'  => 'tarif',
                'active_menu_level' => 'masterdiskonpegawai',
                'content_view'     => 'masterdata/v_masterdiskonpegawai',
                'script_js'        => ['js_masterdiskonpegawai'],
                'title_page'       => 'Master Diskon Pegawai',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
            ];
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function dt_masterdiskonpegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $getData = $this->mdatatable->dt_masterdiskonpegawai_(true);
            $data = [];
            $no = $_POST['start'];
            if ($getData) {
                foreach ($getData->result() as $obj) {
                    $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->namadiskon;
                    $row[] = $obj->kelas;
                    $row[] = $obj->jenistarif;
                    $row[] = $obj->potongan;
                    $row[] = '<a id="edit" alt="' . $obj->iddiskonpegawai . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="hapus" alt="' . $obj->iddiskonpegawai . '" diskon=" <b>' . $obj->namadiskon . '</b>, " class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_masterdiskonpegawai(),
                "recordsFiltered" => $this->mdatatable->filter_dt_masterdiskonpegawai(),
                "data" => $data
            );
            //output dalam format JSON
            echo json_encode($output);
        } else {
            aksesditolak();
        }
    }
    public function  formreadymasterdiskonpegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $data['jenistarif'] = $this->db->select('idjenistarif as id, jenistarif as txt')->get_where('rs_jenistarif')->result_array();
            $data['jenisdiskon'] = $this->mmasterdata->get_data_enum('rs_masterdiskonpegawai', 'jenisdiskon');
            $data['kelas']      = $this->db->select('idkelas as id, kelas as txt')->get_where('rs_kelas')->result_array();
            $data['diskon']     = $this->db->get_where('rs_masterdiskonpegawai rm', ['rm.iddiskonpegawai' => $this->input->post('id')])->row_array();
            echo json_encode($data);
        } else {
            aksesditolak();
        }
    }

    public  function save_masterdiskonpegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $post = $this->input->post();
            $data = [
                'idkelas'       =>  $post['idkelas'],
                'idjenistarif'  =>  $post['idjenistarif'],
                'namadiskon'    =>  $post['namadiskon'],
                'potongan'      =>  $post['potongan']
            ];
            $this->mgenerikbap->setTable('rs_masterdiskonpegawai');
            $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['iddiskonpegawai']);
            $modesimpan = empty($post['iddiskonpegawai']) ? 'Tambah' : 'Ubah';
            pesan_success_danger($simpan, $modesimpan . ' Diskon Berhasil.', $modesimpan . ' Diskon Gagal.', 'js');
        } else {
            aksesditolak();
        }
    }

    public function hapus_diskonpegawai()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON)) //lihat define di atas
        {
            $hapus = $this->db->delete('rs_masterdiskonpegawai', ['iddiskonpegawai' => $this->input->post('id')]);
            pesan_success_danger($hapus, 'Hapus Diskon Berhasil.', ' Hapus Diskon Gagal.', 'js');
        } else {
            aksesditolak();
        }
    }
    // --- MASTER MEMBER
    // mahmud, clear
    public  function mastermember()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'tarif',
            'active_menu_level' => 'mastermember',
            'content_view'     => 'masterdata/v_mastermember',
            'script_js'        => ['js_mastermember'],
            'title_page'       => 'Master Member',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_mastermember()
    {
        $getData = $this->mdatatable->dt_mastermember_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->member;
                $row[] = $obj->keterangan;
                $row[] = $obj->hariberlaku . ' Hari';
                $row[] = '<a id="edit" alt="' . $obj->idmember . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="hapus" alt="' . $obj->idmember . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_mastermember(),
            "recordsFiltered" => $this->mdatatable->filter_dt_mastermember(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  formreadymastermember()
    {
        echo json_encode($this->db->get_where('rs_mastermember', ['idmember' => $this->input->post('id')])->row_array());
    }
    public  function save_mastermember()
    {
        $post = $this->input->post();
        $data = [
            'member' => $post['member'],
            'hariberlaku' => $post['hariberlaku'],
            'keterangan' => $post['keterangan']
        ];
        $this->mgenerikbap->setTable('rs_mastermember');
        $simpan = $this->mgenerikbap->update_or_insert_replaceonduplicate($data, $post['idmember']);
        $modesimpan = empty($post['idmember']) ? 'Tambah' : 'Ubah';
        pesan_success_danger($simpan, $modesimpan . ' Member Berhasil.', $modesimpan . ' Member Gagal.', 'js');
    }
    //    
    public function hapus_mastermember()
    {
        $arrMsg = $this->db->update('rs_mastermember', ['isdelete' => '1'], ['idmember' => $this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    /**
     * Insert data
     */

    public function insert_rs_penjual()
    {
        $dt['penjual'] = $this->input->post('namapenjual');
        $dt['alamat']  = $this->input->post('alamat');
        $arrMsg = $this->db->insert('rs_penjual', $dt);
        pesan_success_danger($arrMsg, 'Insert Data Berhasil.', 'Insert Data Gagal.', 'js');
    }

    public function insert_rs_barang_pengeluaran()
    {
        $dt['namabarang'] = $this->input->post('namaitem');
        $arrMsg = $this->db->insert('rs_barang_pengeluaran', $dt);
        pesan_success_danger($arrMsg, 'Insert Data Berhasil.', 'Insert Data Gagal.', 'js');
    }

    public function insert_rs_satuan()
    {
        $dt['namasatuan'] = $this->input->post('satuan');
        $dt['singkatansatuan'] = $this->input->post('singkatan');
        $arrMsg = $this->db->insert('rs_satuan', $dt);
        pesan_success_danger($arrMsg, 'Insert Data Berhasil.', 'Insert Data Gagal.', 'js');
    }

    //--- Fill Dropdown

    //Fill dropdown inventaris Barang
    public function fillinventarisbarang()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("ib.namabaranginventaris", "$cari");
        $sql = $this->db->or_like("ib.kodebaranginventaris", "$cari");
        $sql = $this->db->limit(20);
        $sql = $this->db->order_by('ib.namabaranginventaris', 'asc');
        $sql = $this->db->select(' concat(idbarangdetail,",",ifnull(ibd.nomorseri,""),",",ifnull(ibd.nomorsertifikat,""),",",ifnull(ibd.nilaiaset,0),",",ifnull(ibd.nilaipenyusutan,0),",",ifnull(ibd.tanggalpembelian,""),",",ifnull(ibd.tanggalpengujian,""),",",ifnull(rs.namasatuan,"")) as id, concat( " <b>", ib.namabaranginventaris,"</b>" , ,"<br>Kode: "," <b> ",ifnull(ib.kodebaranginventaris,"") , "</b> <br> No.Seri: ","<b>",ifnull(ibd.nomorseri,""),"</b>"," <br> No.Sertifikat: ", "<b>",ifnull(ibd.nomorsertifikat,""),"</b>"," <br> Merek:  ","<b>",ifnull(imm.merekbarang,""),"</b>"," <br> Model: ","<b>",ifnull(im.modelbarang,""),"</b>" )  text')
            ->join('inventaris_model im', 'im.idmodelbarang=ibd.idmodelbarang')
            ->join('inventaris_merek imm', 'imm.idmerekbarang=im.idmerekbarang')
            ->join('inventaris_barang ib', 'ib.idbaranginventaris=ibd.idbaranginventaris')
            ->join('rs_satuan rs', 'rs.idsatuan=ibd.idsatuan')
            ->get('inventaris_barangdetail ibd');
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }
    //Fill dropdown inventaris Ruang
    public function fillinventarisruang()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("ib.kodeblok", "$cari")
            ->or_like("ib.namablok", "$cari")
            ->or_like("ir.koderuang", "$cari")
            ->or_like("ir.namaruanginventaris", "$cari");
        $sql = $this->db->limit(20);
        $sql = $this->db->order_by('ir.namaruanginventaris', 'asc');
        $sql = $this->db->select('idruanginventaris as id, concat( "<b>",namaruanginventaris,"</b>","<br> Kode Ruang: ","<b>",ifnull(koderuang,""),"</b>" ,"<br> Nama Blok: ","<b>",ib.namablok,"</b>","<br> Kode Blok: ","<b>",ifnull(ib.kodeblok,""),"</b>")  text')
            ->join('inventaris_blok ib', 'ib.idblokinventaris=ir.idblokinventaris')
            ->get('inventaris_ruang ir')
            ->result_array();
        echo json_encode($sql);
    }
    // fill dropdown icd
    public function fillIcd()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("icd", "$cari")
            ->or_like("namaicd", "$cari")
            ->limit(40)
            ->order_by('namaicd', 'asc')
            ->select("icd as id, trim(concat('<b>',icd,'</b> - ', namaicd )) as text")
            ->get("rs_icd");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }

    /**Pencarian ICD Tindakan**/
    public function fillicdtindakan()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->where('idjenisicd', 3)
            ->group_start()
            ->like("icd", "$cari")
            ->or_like("namaicd", "$cari")
            ->or_like("aliasicd", "$cari")
            ->group_end()->limit(20)
            ->offset($halaman * 30)
            ->order_by('namaicd', 'asc')
            ->select("ri.icd as id, trim(concat(ifnull(ri.icd, ''), ' ', ri.namaicd, ' ', ifnull(ri.aliasicd, ''))) as text")
            ->get("rs_icd ri")
            ->result_array();
        echo json_encode($sql);
    }
    public function fillicdtindakan_new()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        // $sql = $this->db->where('idjenisicd', 3)  // [Andri] temporary disable filter by 'id jenis icd'
        $sql = $this->db->where('idicd','9')
            ->group_start()
            ->like("icd", "$cari")
            ->or_like("namaicd", "$cari")
            ->or_like("aliasicd", "$cari")
            ->group_end()->limit(20)
            ->offset($halaman * 30)
            ->select("ri.icd as id, trim(concat(ifnull(ri.icd, ''), ' | ', ri.namaicd, ' ', ifnull(ri.aliasicd, ''))) as text")
            ->get("rs_icd ri")
            ->result_array();
        echo json_encode($sql);
    }

    /**Pencarian ICD Diagnosa**/
    public function fillicddiagnosa()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->where('idjenisicd', 2)
            ->group_start()
            ->like("icd", "$cari")
            ->or_like("namaicd", "$cari")
            ->or_like("aliasicd", "$cari")
            ->group_end()->limit(20)
            ->offset($halaman * 30)
            ->order_by('namaicd', 'asc')
            ->select("ri.icd as id, trim(concat(ifnull(ri.icd, ''), ' ', ri.namaicd, ' ', ifnull(ri.aliasicd, ''))) as text")
            ->get("rs_icd ri")
            ->result_array();
        echo json_encode($sql);
    }

    public function fillicddiagnosa_new()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');
        $sql = $this->db->where('idjenisicd', 2)
            ->where('idicd','10')
            ->group_start()
            ->like("icd", "$cari")
            ->or_like("namaicd", "$cari")
            ->or_like("aliasicd", "$cari")
            ->group_end()->limit(20)
            ->offset($halaman * 30)
            ->order_by('namaicd', 'asc')
            ->select("ri.icd as id, trim(concat(ifnull(ri.icd, ''), ' | ', ri.namaicd, ' ', ifnull(ri.aliasicd, ''))) as text")
            ->get("rs_icd ri")
            ->result_array();
        echo json_encode($sql);
    }

    // FIll Dropdown Tipe Akun COA
    public function fillTipeakun()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("kode", "$cari")->or_like("nama", "$cari")->limit(20)->order_by('kode', 'asc')->select('kode as id, concat("[", kode," ] ",nama) as text');
        $sql = $this->db->get("keu_tipeakun kt");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }


    //pencarian data barang yang akan digunakan
    public function fillpenggunaanbarang()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');

        $idunit = $this->session->userdata('idunitterpilih');
        $sql = $this->db->query("SELECT concat(rb.idbarang,',',rb.kode,',',rb.namabarang,',',rbp.kadaluarsa,',',rbp.batchno,',',rs.namasatuan,',',rbp.idbarangpembelian) as id, concat(rb.namabarang,' stok : ',sum(rbs.stok),' ',rs.namasatuan,' | Kadaluarsa: ',rbp.kadaluarsa) as text from rs_barang_pembelian rbp "
            . "join rs_barang_stok rbs on rbs.idbarangpembelian=rbp.idbarangpembelian "
            . "join rs_barang rb on rb.idbarang = rbp.idbarang "
            . "left join rs_satuan rs on rs.idsatuan = rb.idsatuan "
            . "WHERE rb.ishidden=0 and rb.namabarang like '%" . $cari . "%' and rbs.idunit = " . $idunit . " and rbs.stok > 0 "
            . "GROUP by rbp.idbarang, rbp.idbarangpembelian")->result();
        echo json_encode($sql);
    }

    /**Pencarian data obat dan bhp**/
    public function fillobatbhp()
    {
        $cari = $this->input->post('searchTerm');
        $halaman = $this->input->post('page');

        $idunit = $this->session->userdata('idunitterpilih');
        if (empty($idunit)) {
            $sql = $this->db->like("namabarang", "$cari")->or_like("kode", "$cari")->limit(20)->offset($halaman * 30)->order_by('namabarang', 'asc')->select("rb.idbarang as id, trim(concat(ifnull(rb.kode, ''), ' ', rb.namabarang, ' ', ifnull(rs.namasatuan, ''))) as text")->join('rs_satuan rs', 'rs.idsatuan= rb.idsatuan')->get("rs_barang rb")->result_array();
        } else {
            $sql = $this->db->query("SELECT concat(rb.idbarang) as id, concat(rb.namabarang,' - ',sum(rbs.stok),' ',rs.namasatuan,' | harga :  ', rb.hargajual) as text from rs_barang_pembelian rbp, rs_barang_stok rbs, rs_barang rb, rs_satuan rs WHERE rb.ishidden=0 and rb.namabarang like '%" . $cari . "%' and  rbp.idbarang=rb.idbarang and rbs.idunit = " . $idunit . " and rbs.idbarangpembelian=rbp.idbarangpembelian and rs.idsatuan=rb.idsatuan GROUP by rbp.idbarang, rbp.idbarangpembelian")->result();
        }
        echo json_encode($sql);
    }


    //fill barang
    public function fillbarang()
    {
        $cari = $this->input->post('searchTerm');
        $sql  = $this->db->like("namabarang", "$cari")->or_like("kode", "$cari")->limit(20)->order_by('namabarang', 'asc')->select("rb.idbarang as id, trim(concat(ifnull(rb.kode, ''), ' ', rb.namabarang, ' ', ifnull(rs.namasatuan, ''))) as text")->join('rs_satuan rs', 'rs.idsatuan= rb.idsatuan')->get("rs_barang rb")->result_array();
        echo json_encode($sql);
    }


    // Fill Dropdown Akun Coa
    public function fillAkun()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("kode", "$cari")->or_like("nama", "$cari")->limit(20)->order_by('kode', 'asc')->select('kode as id, concat("[", kode," ] ",nama) as text');
        $sql = $this->db->get("keu_akun ka");
        $fetchData = fill_push_array(['id' => '', 'text' => 'Pilih Akun'], $sql->result_array(), 'id', 'text');
        echo json_encode($fetchData);
    }
    // fill cari member 
    public function fillcarimember()
    {
        $cari = $this->input->post('searchTerm');
        $dtmember = $this->db
            ->select("concat(idmember,',',hariberlaku) as id, concat('<b>',member,'</b> <br> <u>Keterangan:</u> <small>',ifnull(keterangan,''),'</small>','<br> <u>Hari Berlaku:</u> ',hariberlaku,' Hari') as text")
            ->where('isdelete', 0)
            ->group_start()->like("member", $cari)->or_like("keterangan", $cari)->group_end()
            ->get("rs_mastermember")
            ->result_array();
        echo json_encode(fill_push_array(['id' => '', 'text' => 'Pilih Member'], $dtmember, 'id', 'text'));
    }

    //fill cari dokter
    public function fillcaridokter()
    {
        $cari   = $this->input->post('searchTerm');
        $dtdokter = $this->db->select("ppg.idpegawai as id, concat(ifnull(ppg.titeldepan,''),' ',ifnull(pper.namalengkap,''),' ',ifnull(ppg.titelbelakang,'')) as text ")
            ->join('person_person pper', 'pper.idperson=ppg.idperson')
            ->join('person_grup_pegawai pgp', 'pgp.idgruppegawai=ppg.idgruppegawai')
            ->where(['pgp.isdokter' => 1, 'ppg.statuskeaktifan' => 'aktif'])
            ->limit(10)
            ->group_start()->like("pper.namalengkap", $cari)->group_end()
            ->get('person_pegawai ppg')->result_array();
        echo json_encode($dtdokter);
    }

    //fill cari paketpemeriksaanrule
    public function fillpaketpemeriksaanrule()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("rppr.idpaketpemeriksaanrule as id, rppr.namarule as text ")->like('namarule', $cari)->get('rs_paket_pemeriksaan_rule rppr')->result_array();
        echo json_encode($data);
    }

    //fill cari pasien
    public function fillcaripasien()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("ppas.norm as id, concat('<b>',ppas.norm,'</b> - ', pper.namalengkap) as text ")->like('norm', $cari)->limit(20)->join('person_person pper', 'pper.idperson=ppas.idperson')->get('person_pasien ppas')->result_array();
        echo json_encode($data);
    }

    //fill cari bahasa
    public function fillcaribahasa()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("pb.idbahasa as id, pb.bahasa as text ")
            ->like('bahasa', $cari)
            ->limit(20)
            ->get('person_bahasa pb')
            ->result_array();
        echo json_encode($data);
    }

    //fill cari suku
    public function fillcarisuku()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("ps.idsuku as id, ps.suku as text ")
            ->like('suku', $cari)
            ->limit(20)
            ->get('person_suku ps')
            ->result_array();
        echo json_encode($data);
    }

    //fill pasien periksa
    public function fillpasienperiksa()
    {
        $tanggal = date('Y-m-d');

        $cari    = $this->input->post('cari');

        $query   = $this->db->select("pp.idpendaftaran as id, concat('<b>',pp.norm,'</b> ',namapasien(ppas.idperson), ' <br> <b>Jadwal Periksa</b> : ',rp.waktu,' <br> <b> Poli : </b>', namaunit(pp.idunit)) as text ")
            ->from('person_pendaftaran pp')
            ->join('person_pasien ppas', 'ppas.norm = pp.norm','left')
            ->join('person_person pper', 'pper.idperson=ppas.idperson','left')
            ->join('rs_pemeriksaan rp', 'rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null','left')
            ->group_start()
            ->like('pp.norm', $cari)
            ->or_like('pper.namalengkap', $cari)
            ->group_end()
            // ->group_start()
            ->where(["pp.idstatuskeluar !=" =>  3, "pp.idstatuskeluar != " => 4, "pp.idstatuskeluar !=" => 5, 'date(pp.waktuperiksa)' => $tanggal])
            // ->group_end()
            ->limit(20)
            ->get()
            ->result_array();
            

        // print_r($this->db->last_query());
        echo json_encode($query);
    }

    //fill cari petugas ranap
    public function fillpetugasranap()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("concat(a.idpegawai, ',' ,ifnull(a.sip,''), ',' ,ifnull(b.profesi,''), ',' ,ifnull(b.idprofesi,'') ) as id, namadokter(a.idpegawai) as text")
            ->join('person_person pp', 'pp.idperson = a.idperson')
            ->join("person_grup_profesi b", "b.idprofesi = a.idprofesi")
            ->like('pp.namalengkap', $cari)
            ->limit('10')
            ->order_by('pp.namalengkap', 'asc')
            ->get("person_pegawai a")
            ->result_array();
        echo json_encode($data);
    }

    public function fillkeuakunbank()
    {
        $sql = "SELECT b.kode as id, b.nama as txt FROM keu_akun_kasbank a JOIN keu_akun b on b.kode = a.kode";
        $query = $this->db->query($sql)->result();
        echo json_encode($query);
    }

    public function fillpenjual()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("idpenjual as id, concat(penjual,'<br> ','<small>',ifnull(alamat,''),'</small>') as text")
            ->like('penjual', $cari)
            ->limit(20)
            ->get("rs_penjual")
            ->result();
        echo json_encode($query);
    }

    public function fillpegawai()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("pp.idpegawai as id, concat(ifnull(titeldepan,' '),namalengkap,' ',ifnull(titelbelakang,'')) as text")
            ->join("person_person pper", "pper.idperson = pp.idperson")
            ->like('pper.namalengkap', $cari)
            ->limit(20)
            ->get("person_pegawai pp")
            ->result();
        echo json_encode($query);
    }
    public function fill_paket_laboratorium_ralan()
    {
        $cari = $this->input->post('searchTerm');

        $query = $this->db
            ->select("rpp.idpaketpemeriksaan as id, rpp.namapaketpemeriksaan as text")
            ->join("rs_mastertarif_paket_pemeriksaan rmp", "rmp.idpaketpemeriksaan = rpp.idpaketpemeriksaan and rmp.total > 0 and rmp.idkelas = 1")
            ->where(['idjenisicd' => 4, 'idpaketpemeriksaanparent' => 0])
            ->like('rpp.namapaketpemeriksaan', $cari)
            ->get("rs_paket_pemeriksaan rpp")
            ->result();
        echo json_encode($query);
    }
    public function fillpenanggungkasir()
    {
        $q  = "select concat(ifnull(titeldepan,' '),namalengkap,' ',ifnull(titelbelakang,'')) as text from person_pegawai pp join person_person pper on pper.idperson = pp.idperson";
        $dt = $this->db->query($q)->result_array();
        echo json_encode($dt);
    }

    public function fillcariloket()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("idloket as id, namaloket as text")
            ->like('namaloket', $cari)
            ->group_start()
            ->where('isambil', 0)
            ->group_end()
            ->limit(20)
            ->get("antrian_loket")
            ->result();
        echo json_encode($query);
    }

    public function fillsatuan()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("idsatuan as id, namasatuan as text")
            ->like('namasatuan', $cari)
            ->limit(20)
            ->get("rs_satuan")
            ->result();
        echo json_encode($query);
    }

    public function fillaturanpakai()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("idbarangaturanpakai as id, signa as text")
            ->like('signa', $cari)
            ->or_like('aturanpakai', $cari)
            ->limit(20)
            ->get("vrs_barang_aturanpakai")
            ->result();
        echo json_encode($query);
    }

    public function fillbarangpengeluaran()
    {
        $cari = $this->input->post('searchTerm');
        $query = $this->db
            ->select("idbarangpengeluaran as id, namabarang as text")
            ->like('namabarang', $cari)
            ->limit(20)
            ->get("rs_barang_pengeluaran")
            ->result();
        echo json_encode($query);
    }

    /**
     * ALL SDKI (Standar Diagnosis Keperawatan Indonesia)
     * @author Tito Otniel, S.Kom.
     **/

    public function list_data_sdki()
    {
        # Data rs_master_sdki_kategori
        $querkategori = $this->db->query("SELECT * FROM rs_master_sdki_kategori")->result_array();

        $idkategori = [];
        $namakategori  = [];
        foreach ($querkategori as $obj) {
            $idkategori[] = $obj['id_kategori_sdki'];
            $namakategori[] = $obj['nama_kategori_sdki'];
        }

        $idkategoriexplode = empty(rtrim(implode(',', $idkategori), ',')) ? 0 : rtrim(implode(',', $idkategori), ',');

        $idsubkategori = [];
        $namasubkategori = [];

        $quersubkategori = $this->db->query("SELECT * FROM rs_master_sdki_subkategori INNER JOIN rs_master_sdki_kategori ON rs_master_sdki_subkategori.id_kategori_sdki = rs_master_sdki_kategori.id_kategori_sdki WHERE rs_master_sdki_subkategori.id_kategori_sdki IN (" . $idkategoriexplode . ")")->result_array();

        foreach ($quersubkategori as $obj) {
            $idsubkategori[] = $obj['id_subkategori_sdki'];
            $namasubkategori[] = $obj['nama_kategori_sdki'];
        }

        $idsubkategoriexplode = empty(rtrim(implode(',', $idsubkategori), ',')) ? 0 : rtrim(implode(',', $idsubkategori), ',');

        $querychildkategori = $this->db->query("SELECT * FROM rs_master_sdki_childkategori INNER JOIN rs_master_sdki_subkategori ON rs_master_sdki_subkategori.id_subkategori_sdki = rs_master_sdki_childkategori.id_subkategori_sdki WHERE rs_master_sdki_childkategori.id_subkategori_sdki IN (" . $idsubkategoriexplode . ")")->result_array();

        $idchildkategori = [];
        $kode_childkategori = [];
        $nama_childkategori = [];
        foreach ($querychildkategori as $obj) {
            $idchildkategori[] = $obj['id_sdki_childkategori'];
            $kode_childkategori[] = $obj['kode_childkategori'];
            $nama_childkategori[] = $obj['nama_childkategori'];
        }

        $data = [
            "kategori" => $querkategori,
            "subkategori" => $quersubkategori,
            "childkategori" => $querychildkategori
        ];

        // print_r($data);
        // die;
        return $data;
    }

    public function sdki()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SDKI)) {
            $data['content_view']       = 'masterdata/v_sdki';
            $data['active_menu']        = 'masterdata';
            $data['active_sub_menu']    = 'sdki';
            $data['active_menu_level']  = 'mastersdki';
            $data['datas']              = $this->list_data_sdki();
            $data['title_page']         = 'Standar Diagnosis Keperawatan Indonesia (SDKI)';
            $data['plugins']            = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA];
            $data['script_js']          = ['js_sdki'];

            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function tambahsdki()
    {
        $jenis = $_POST['jenis'];

        if ($jenis == "parent") {
            // $this->form_validation->set_rules('kodekategori', 'Kode Kategori', 'required',
            // array('required' => 'Harap Pilih  %s.'));
            // print_r($_POST);
            if ($_POST['namakategori'] == "undefined") {
                $return = array('result' => 'failed', 'msg' => "Harap Lengkapi Data Terlebih Dahulu!");
                echo json_encode($return);
            } else {
                if (!empty($this->input->post('namakategori'))) {
                    $data = [
                        "nama_kategori_sdki" => $_POST["namakategori"]
                    ];

                    $this->db->insert("rs_master_sdki_kategori", $data);

                    $return = array('Sukses' => 'Success', 'msg' => "Simpan Data Berhasil!");
                    echo json_encode($return);
                }
            }
        }

        if ($jenis == "subkategori") {
            if ($_POST['namakategori'] == "undefined") {
                $return = array('result' => 'failed', 'msg' => "Harap Lengkapi Data Terlebih Dahulu!");
                echo json_encode($return);
            } else {
                if (!empty($this->input->post('namakategori'))) {
                    $data = [
                        "id_kategori_sdki" => $_POST["kodekategori"],
                        "nama_subkategori_sdki" => $_POST["namakategori"]
                    ];

                    $this->db->insert("rs_master_sdki_subkategori", $data);
                    $return = array('Sukses' => 'Success', 'msg' => "Simpan Data Berhasil!");
                    echo json_encode($return);
                }
            }
        }

        if ($jenis == "childsubkategori") {
            // $this->form_validation->set_rules('kodekategori', 'childsubkategori', 'required',
            // array('required' => 'Harap Pilih  %s.'));
            if ($_POST['kodediagnosa'] == "" || $_POST['diagnosa'] == "") {
                $return = array('result' => 'failed', 'msg' => "Harap Lengkapi Data Terlebih Dahulu!");
                echo json_encode($return);
            } else {
                if (!empty($this->input->post('kodediagnosa')) || !empty($this->input->post('diagnosa'))) {
                    $data = [
                        "id_subkategori_sdki" => $_POST["kodekategori"],
                        "kode_childkategori" => $_POST["kodediagnosa"],
                        "nama_childkategori" => $_POST["diagnosa"],
                    ];

                    $this->db->insert("rs_master_sdki_childkategori", $data);
                    $return = array('Sukses' => 'Success', 'msg' => "Simpan Data Berhasil!");
                    echo json_encode($return);
                }
            }
        }
    }

    public function hapus_kategori_sdki()
    {
        $idkat = $_POST['idkat'];
        $jenis = $_POST['jenis'];

        if ($jenis == "kategori") {
            $queryhapuskategori = $this->db->query("DELETE FROM rs_master_sdki_kategori WHERE id_kategori_sdki =" . $idkat);
            $return = array('Sukses' => 'sukses', 'msg' => "Hapus Data Berhasil!", "Ini" => "kategori");
            echo json_encode($return);
        } else if ($jenis == "subkategori") {
            $queryhapuskategori = $this->db->query("DELETE FROM rs_master_sdki_subkategori WHERE id_subkategori_sdki =" . $idkat);
            $return = array('Sukses' => 'sukses', 'msg' => "Hapus Data Berhasil!", "Ini" => "subkategori");
            echo json_encode($return);
        } else if ($jenis == "childkategori") {
            $queryhapuskategori = $this->db->query("DELETE FROM rs_master_sdki_childkategori WHERE id_sdki_childkategori  =" . $idkat);
            $return = array('Sukses' => 'sukses', 'msg' => "Hapus Data Berhasil!", "Ini" => "childkategori");
            echo json_encode($return);
        }
    }

    public function cari_sdki()
    {
        $diagnosa = $this->input->get('q');

        $query = $this->db->query("
        SELECT rs_master_sdki_childkategori.id_sdki_childkategori, CONCAT(rs_master_sdki_childkategori.kode_childkategori, '-' , rs_master_sdki_childkategori.nama_childkategori) as diagnosasdki 
        FROM `rs_master_sdki_childkategori` 
        INNER JOIN rs_master_sdki_subkategori ON rs_master_sdki_childkategori.id_subkategori_sdki = rs_master_sdki_subkategori.id_subkategori_sdki 
        INNER JOIN rs_master_sdki_kategori ON rs_master_sdki_subkategori.id_kategori_sdki = rs_master_sdki_kategori.id_kategori_sdki 
        WHERE rs_master_sdki_childkategori.nama_childkategori like '%" . $diagnosa . "%' OR rs_master_sdki_childkategori.kode_childkategori like '%" . $diagnosa . "%'")->result_array();

        echo json_encode($query);
    }

    /**  
     * SDKI-SLKI-SIKI 
     * @author Tito Otniel, S.Kom. <titootniel26@gmail.com>
     **/

    public function list_data_slki()
    {
        $query = $this->db->query("SELECT * FROM rs_master_slki")->result_array();
        return $query;
    }

    public function list_data_kriteria_hasil_slki()
    {
        $query = $this->db->query("select *,count(rs_master_slki.id_slki) as jumlahtingkatan FROM rs_master_slki
        INNER JOIN rs_master_kriteria_hasil_slki on rs_master_kriteria_hasil_slki.id_slki = rs_master_slki.id_slki
        INNER JOIN rs_master_tingkatan_kriteria_hasil_slki on rs_master_kriteria_hasil_slki.id_kriteria_hasil = rs_master_tingkatan_kriteria_hasil_slki.id_kriteria_hasil
        group by rs_master_slki.id_slki")->result_array();

        return $query;
    }

    public function list_data_tautan_sdki_slki()
    {
        $query = $this->db->query("select rs_master_sdki_childkategori.nama_childkategori as namasdki, (SELECT rs_master_slki.nama_slki FROM rs_master_slki WHERE rs_master_slki.id_slki = rs_master_tautan_slki_sdki.idslkiluaranutama group by rs_master_slki.id_slki) as luaranutama,
        rs_master_tautan_slki_sdki.id_tautan_slki_sdki, 
        GROUP_CONCAT(' <li> ', rs_master_slki.nama_slki, ' </li> ' SEPARATOR ' <br> ') as luarantambahan from rs_master_tautan_slki_sdki
        INNER JOIN rs_master_tautan_slki_sdki_luaran_tambahan on rs_master_tautan_slki_sdki.id_tautan_slki_sdki = rs_master_tautan_slki_sdki_luaran_tambahan.id_tautan_slki_sdki
        INNER JOIN rs_master_sdki_childkategori on rs_master_sdki_childkategori.id_sdki_childkategori = rs_master_tautan_slki_sdki.id_sdki
        INNER JOIN rs_master_slki on rs_master_slki.id_slki = rs_master_tautan_slki_sdki_luaran_tambahan.id_slki_luaran_tambahan
        group by rs_master_tautan_slki_sdki.id_tautan_slki_sdki")->result_array();

        return $query;
    }


    public function slki()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SDKI)) {
            $data['content_view']       = 'masterdata/v_slki';
            $data['active_menu']        = 'masterdata';
            $data['active_sub_menu']    = 'diagnosakeperawatan';
            $data['active_menu_level']  = 'masterslki';
            $data['dataslki']              = $this->list_data_slki();
            $data['listdatakriteriahasilslki'] = $this->list_data_kriteria_hasil_slki();
            $data['listdatatautansdkislki'] = $this->list_data_tautan_sdki_slki();
            $data['title_page']         = 'Standar Luaran Keperawatan Indonesia (SLKI)';
            $data['plugins']            = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA];
            $data['script_js']          = ['js_slki'];

            // echo "<pre>";
            // print_r($data);
            // echo "</pre>";
            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function simpan_data_slki()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('kodeslki', '', 'required');
        $this->form_validation->set_rules('namaslki', '', 'required');
        $this->form_validation->set_rules('ekspetasi', '', 'required');
        $this->form_validation->set_rules('definisislki', '', 'required');


        if (validation_input()) {
            #Check Agar Data Gak Duplikasi
            $querycheck = $this->db->query("SELECT * FROM rs_master_slki WHERE kode_slki ='" . $post['kodeslki'] . "'")->result_array();

            if (empty($querycheck)) {
                $data = [
                    "kode_slki" => $post['kodeslki'],
                    "nama_slki" => $post['namaslki'],
                    "ekspektasi" => $post['ekspetasi'],
                    "definisislki" => $post['definisislki']
                ];

                $query = $this->msqlbasic->insert_data('rs_master_slki', $data);

                if ($query) {
                    $result = ["status" => "Success", "message" => "Simpan Data Berhasil!"];
                    echo json_encode($result);
                } else {
                    $result = ["status" => "Failed", "message" => "Simpan Data Gagal!"];
                    echo json_encode($result);
                }
            } else {
                $result = ["status" => "Failed", "message" => "Data Sudah Ada!"];
                echo json_encode($result);
            }
        } else {
            $result = ["status" => "Failed", "message" => "Inputan Masih Ada Yang Kurang!"];
            echo json_encode($result);
        }
    }

    public function hapus_data_slki()
    {
        $post = $this->input->post();

        $query = $this->db->query("DELETE FROM rs_master_slki WHERE id_slki =" . $post['iddatasdki']);

        if ($query) {
            $result = ["status" => "Success", "message" => "Hapus Data Berhasil!"];
            echo json_encode($result);
        } else {
            $result = ["status" => "Failed", "message" => "Hapus Data Gagal!"];
            echo json_encode($result);
        }
    }

    public function cariSLKI()
    {
        $namaslki = $this->input->get('q');
        $query = $this->db->query("SELECT * FROM rs_master_slki WHERE kode_slki LIKE '%" . $namaslki . "%' OR nama_slki LIKE '%" . $namaslki . "%'")->result_array();
        echo json_encode($query);
    }

    public function simpanKriteriaHasilSLKI()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('idslki', '', 'required');

        if (validation_input()) {
            $datarsmasterkriteriahasilslki = [
                "id_slki" => $post['idslki']
            ];

            $insertintorsmasterkristeriahasilslki = $this->db->insert('rs_master_kriteria_hasil_slki', $datarsmasterkriteriahasilslki);
            $id_kriteria_hasil  = $this->db->insert_id();

            $a = 1;
            $b = 1;
            for ($i = 0; $i < $post['jumlahtingkatan']; $i++) {
                $datainsertintotingkatankriteriahasilsdki = [
                    "id_kriteria_hasil" => $id_kriteria_hasil,
                    "penilaian1" => $post['penilaian1tingkatan' . $a],
                    "penilaian2" => $post['penilaian2tingkatan' . $a],
                    "penilaian3"  => $post['penilaian3tingkatan' . $a],
                    "penilaian4"  => $post['penilaian4tingkatan' . $a],
                    "penilaian5"  => $post['penilaian5tingkatan' . $a]
                ];

                $a++;

                $insertintorsmastertingkatankriteriahasilslki = $this->db->insert('rs_master_tingkatan_kriteria_hasil_slki', $datainsertintotingkatankriteriahasilsdki);
                $idtingkatan = $this->db->insert_id();

                foreach ($post['bentukriteriahasil' . $b] as $item) {

                    $datainsertintorsmasterbentukkriteriahasilslki = [
                        "id_tingkatan" => $idtingkatan,
                        "bentuk_kriteria_hasil" => $item
                    ];

                    $insertintorsmasterbentukkriteriahasilslki = $this->db->insert('rs_master_bentuk_kriteria_hasil_slki', $datainsertintorsmasterbentukkriteriahasilslki);
                }
                $b++;
            }

            $arrMsg = ["result" => "success"];
            pesan_success_danger($arrMsg, 'Save Kriteria Hasil Success.!', $arrMsg . 'Save Kriteria Hasil Failed.!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function detailKriteriaHasilSLKI()
    {
        $post = $this->input->post();

        $datadatatingkatankristeriahasil = $this->db->query("SELECT * FROM rs_master_slki
        INNER JOIN rs_master_kriteria_hasil_slki ON rs_master_slki.id_slki = rs_master_kriteria_hasil_slki.id_slki
        INNER JOIN rs_master_tingkatan_kriteria_hasil_slki on rs_master_tingkatan_kriteria_hasil_slki.id_kriteria_hasil = rs_master_kriteria_hasil_slki.id_kriteria_hasil
        WHERE rs_master_slki.id_slki =" . $post['idslki'])->result_array();

        $getdatakriteriahasilslki = $this->db->query("SELECT GROUP_CONCAT(rs_master_bentuk_kriteria_hasil_slki.bentuk_kriteria_hasil SEPARATOR ' <br><br> ') as bentukriteriahasil 
        FROM `rs_master_tingkatan_kriteria_hasil_slki`
        INNER JOIN rs_master_bentuk_kriteria_hasil_slki on rs_master_tingkatan_kriteria_hasil_slki.id_tingkatan = rs_master_bentuk_kriteria_hasil_slki.id_tingkatan
        INNER JOIN rs_master_kriteria_hasil_slki on rs_master_kriteria_hasil_slki.id_kriteria_hasil = rs_master_tingkatan_kriteria_hasil_slki.id_kriteria_hasil
        INNER JOIN rs_master_slki on rs_master_slki.id_slki = rs_master_kriteria_hasil_slki.id_slki
        where rs_master_slki.id_slki = " . $post['idslki'] . "
        GROUP BY rs_master_tingkatan_kriteria_hasil_slki.id_tingkatan")->result();

        $getdataslki = $this->db->query("select *,count(rs_master_slki.id_slki) as jumlahtingkatan FROM rs_master_slki
                INNER JOIN rs_master_kriteria_hasil_slki on rs_master_kriteria_hasil_slki.id_slki = rs_master_slki.id_slki
                INNER JOIN rs_master_tingkatan_kriteria_hasil_slki on rs_master_kriteria_hasil_slki.id_kriteria_hasil = rs_master_tingkatan_kriteria_hasil_slki.id_kriteria_hasil
                WHERE rs_master_slki.id_slki =" . $post['idslki'] . " group by rs_master_slki.id_slki")->result_array();

        $data = [
            "datatingkatankriteriahasil" => $datadatatingkatankristeriahasil,
            "datakriteriahasilslki" => $getdatakriteriahasilslki,
            "dataslki" => $getdataslki
        ];

        echo json_encode($data);
    }

    public function hapus_kriteria_hasil_slki()
    {
        $idslki = $this->input->post('idslki');

        $deletekriteriaslki = $this->msqlbasic->delete_db('rs_master_kriteria_hasil_slki', ["id_slki" => $idslki]);

        $result = "";

        if ($deletekriteriaslki) {
            $result = [
                "result" => "success",
                "msg" => "Hapus Data Sukses"
            ];
        } else {
            $result = [
                "result" => "danger",
                "msg" => "Hapus Data Gagal"
            ];
        }

        echo json_encode($result);
    }

    public function cek_duplikat_slki()
    {
        $idslki = $this->input->post('idslki');

        $query = $this->msqlbasic->show_db('rs_master_kriteria_hasil_slki', ["id_slki" => $idslki]);

        $result = "";

        if (!empty($query)) {
            $result = [
                "result" => "Failed",
                "msg" => "Data Kriteria Hasil Sudah Ada!"
            ];
        } else {
            $result = [
                "result" => "Success",
                "msg" => "Data Kriteria Hasil Dapat Dimasukkan!"
            ];
        }

        echo json_encode($result);
    }

    public function save_tautan_sdki_slki()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('idsdki', 'idsdki', 'required');
        $this->form_validation->set_rules('idluaranutama', 'idluaranutama', 'required');
        $this->form_validation->set_rules('luarantambahan[]', 'luarantambahan', 'required');

        $arrMsg = "";
        if ($this->form_validation->run() == FALSE) {
            pesan_success_danger($arrMsg, 'Save Tautan SDKI SLKI Success.!', $arrMsg . 'Save Tautan SDKI SLKI Failed.!');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $arrMsg = $this->db->insert('rs_master_tautan_slki_sdki', ["id_sdki" => $post['idsdki'], "idslkiluaranutama" => $post['idluaranutama']]);
            $lastinsertid = $this->db->insert_id();

            for ($i = 0; $i < count($post['luarantambahan']); $i++) {
                $arrMsg = $this->db->insert('rs_master_tautan_slki_sdki_luaran_tambahan', ["id_tautan_slki_sdki" => $lastinsertid, "id_slki_luaran_tambahan" => $post['luarantambahan'][$i]]);
            }

            pesan_success_danger($arrMsg, 'Save Tautan SDKI SLKI Success.!', $arrMsg . 'Save Tautan SDKI SLKI Failed.!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function delete_data_tautan_sdki()
    {
        $idtautansdkislki = $this->input->post('idtautansdkislki');

        $query = $this->db->where('id_tautan_slki_sdki ', $idtautansdkislki);
        $this->db->delete('rs_master_tautan_slki_sdki');

        $query = $this->db->where('id_tautan_slki_sdki ', $idtautansdkislki);
        $this->db->delete('rs_master_tautan_slki_sdki_luaran_tambahan');

        if ($query) {
            echo json_encode(["result" => "success", "msg" => "Berhasil Menghapus Tautan SDKI dan SLKI"]);
        } else {
            echo json_encode(["result" => "danger", "msg" => "Gagal Menghapus Tautan SDKI dan SLKI"]);
        }
    }

    public function get_detail_child_kategori_sdki()
    {
        $idchildkategori = $this->input->post('idchildkategori');

        $data['datachildkategori'] = $this->db->query("SELECT * FROM rs_master_sdki_childkategori WHERE rs_master_sdki_childkategori.id_sdki_childkategori=" . $idchildkategori)->row_array();

        echo json_encode($data);
    }

    public function siki()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SDKI)) {
            $data['content_view']       = 'masterdata/v_siki';
            $data['active_menu']        = 'masterdata';
            $data['active_sub_menu']    = 'diagnosakeperawatan';
            $data['active_menu_level']  = 'mastersiki';
            $data['title_page']         = 'Standar Intervensi Keperawatan Indonesia (SIKI)';
            $data['plugins']            = [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA];
            $data['script_js']          = ['js_siki'];
            $data['datasiki'] = $this->db->query("SELECT rs_master_siki.kode_siki, rs_master_siki.nama_siki,rs_master_siki.id_siki FROM rs_master_siki")->result_array();
            $data['tautanslkisdki'] = $this->db->query("SELECT * FROM rs_master_siki_tautan_sdki_siki INNER JOIN rs_master_sdki_childkategori on rs_master_siki_tautan_sdki_siki.id_sdki = rs_master_sdki_childkategori.id_sdki_childkategori")->result_array();

            $this->load->view('v_index', $data);
        } else {
            aksesditolak();
        }
    }

    public function tambah_siki()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('kodesiki', 'kodesiki', 'required');
        $this->form_validation->set_rules('namasiki', 'namasiki', 'required');
        $this->form_validation->set_rules('definisi', 'definisi', 'required');

        $arrMsg = "";
        if ($this->form_validation->run() == FALSE) {
            pesan_success_danger($arrMsg, 'Save Data SIKI Success.!', $arrMsg . 'Save Data SIKI Failed.!');
        } else {
            // nblr();
            $data = [
                "kode_siki" => $post['kodesiki'],
                "nama_siki" => $post['namasiki'],
                "definisi_siki" => $post['definisi'],
                "observasi_siki" => isset($post['observasi']) ? $post['observasi'] : NULL,
                "terapeutik_siki" => isset($post['terapeutik']) ? $post['terapeutik'] : NULL,
                "edukasi_siki" => isset($post['edukasi']) ? $post['edukasi'] : NULL,
                "koloborasi_siki" => isset($post['koloborasi']) ? $post['koloborasi'] : NULL
            ];

            if(empty($post['idsiki'])){
                $arrMsg = $this->db->insert('rs_master_siki', $data);
            }else{
                $this->db->where('id_siki', $post['idsiki']);
                $this->db->update('rs_master_siki', $data);
                $arrMsg = 1;
            }

            pesan_success_danger($arrMsg, 'Save Data SIKI Success.!', $arrMsg . 'Save Data SIKI Failed.!');
        }

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function detail_siki()
    {
        $idsiki = $this->input->post('idsiki');

        $query = $this->db->query("SELECT * FROM rs_master_siki WHERE rs_master_siki.id_siki = " . $idsiki)->row();

        $ex_observasi = explode("|",$query->observasi_siki);
        $ex_terapeutik_siki = explode("|",$query->terapeutik_siki);
        $ex_edukasi_siki = explode("|",$query->edukasi_siki);
        $ex_koloborasi_siki = explode("|",$query->koloborasi_siki);

        $data = [
            'kode_siki'=>$query->kode_siki,
            'nama_siki'=>$query->nama_siki,
            'definisi_siki'=>$query->definisi_siki,
            'observasi_siki'=> $this->convert_to_list_otek($ex_observasi),
            'terapeutik_siki'=> $this->convert_to_list_otek($ex_terapeutik_siki),
            'edukasi_siki'=> $this->convert_to_list_otek($ex_edukasi_siki),
            'koloborasi_siki'=> $this->convert_to_list_otek($ex_koloborasi_siki)
        ];

        $datas = ['item'=> $data];

        echo json_encode($datas);
    }

    public function convert_to_list_otek($array_data)
    {
        $html = '';
        foreach($array_data as $row){
            $html .= '- '.$row .'<br>'; 
        }

        return $html;
    }

    public function hapus_data_siki()
    {
        $idsiki = $this->input->post('idsiki');

        $this->db->where('id_siki', $idsiki);
        $query = $this->db->delete('rs_master_siki');

        if ($query) {
            echo json_encode(["result" => "success", "msg" => "Berhasil Menghapus Data SIKI"]);
        } else {
            echo json_encode(["result" => "danger", "msg" => "Gagal Menghapus Data SIKI"]);
        }
    }

    public function get_data_siki()
    {
        $namasiki = $this->input->get("q");
        $query = $this->db->query("SELECT * FROM rs_master_siki WHERE rs_master_siki.kode_siki LIKE '%" . $namasiki . "%' OR rs_master_siki.nama_siki LIKE '%" . $namasiki . "%'")->result();
        echo json_encode($query);
    }

    public function save_tautan_sdki_siki()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('idsdki', 'idsdki', 'required');
        $this->form_validation->set_rules('intervensiutama[]', 'intervensiutama[]', 'required');
        $this->form_validation->set_rules('intervensipendukung[]', 'intervensipendukung[]', 'required');

        $arrMsg = "";
        if ($this->form_validation->run() == FALSE) {
            pesan_success_danger($arrMsg, 'Save Tautan SDKI SIKI Success.!', $arrMsg . 'Save Tautan SDKI SIKI  Failed.!');
            return redirect($_SERVER['HTTP_REFERER']);
        } else {
            $internvensiutama = array_unique($post['intervensiutama']);
            $intervensipendukung = array_unique($post['intervensipendukung']);

            $arrMsg =  $this->db->insert('rs_master_siki_tautan_sdki_siki', [
                "id_sdki" => $post['idsdki']
            ]);

            $idtautansdkislki = $this->db->insert_id();

            foreach ($internvensiutama as $item) {
                $arrMsg = $this->db->insert('rs_master_siki_tautan_sdki_siki_intervensi_utama', [
                    "id_tautan_sdki_siki" => $idtautansdkislki,
                    "id_siki" => $item
                ]);
            }

            foreach ($intervensipendukung as $item) {
                $arrMsg = $this->db->insert('rs_master_siki_tautan_sdki_siki_intervensi_pendukung', [
                    "id_tautan_sdki_siki" => $idtautansdkislki,
                    "id_siki" => $item
                ]);
            }

            pesan_success_danger($arrMsg, 'Save Tautan SDKI SIKI Success.!', $arrMsg . 'Save Tautan SDKI SIKI  Failed.!');
            return redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function get_data_detail_tautan_sdki_siki()
    {
        $idtautansdkisiki = $this->input->post('idtautansdkislki');

        $data['datasdki'] = $this->db->query("SELECT rs_master_sdki_childkategori.kode_childkategori, rs_master_sdki_childkategori.nama_childkategori, rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki FROM rs_master_sdki_childkategori
        JOIN rs_master_siki_tautan_sdki_siki on rs_master_sdki_childkategori.id_sdki_childkategori = rs_master_siki_tautan_sdki_siki.id_sdki
        WHERE rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki =" . $idtautansdkisiki)->row_array();

        $data['intervensiutama'] = $this->db->query("SELECT rs_master_siki.nama_siki as intervensiutama FROM `rs_master_siki_tautan_sdki_siki_intervensi_utama`
        INNER JOIN rs_master_siki_tautan_sdki_siki on rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki = rs_master_siki_tautan_sdki_siki_intervensi_utama.id_tautan_sdki_siki
        INNER JOIN rs_master_siki on rs_master_siki.id_siki = rs_master_siki_tautan_sdki_siki_intervensi_utama.id_siki
        WHERE rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki =" . $idtautansdkisiki)->result_array();

        $data['intervensipendukung'] = $this->db->query("SELECT rs_master_siki.nama_siki as intervensipendukung FROM rs_master_siki_tautan_sdki_siki_intervensi_pendukung
        INNER JOIN rs_master_siki_tautan_sdki_siki on rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki = rs_master_siki_tautan_sdki_siki_intervensi_pendukung.id_tautan_sdki_siki
        INNER JOIN rs_master_siki on rs_master_siki.id_siki = rs_master_siki_tautan_sdki_siki_intervensi_pendukung.id_siki
        WHERE rs_master_siki_tautan_sdki_siki.id_tautan_sdki_siki =" . $idtautansdkisiki)->result_array();

        echo json_encode($data);
    }

    public function save_definisi_childkategori_sdki()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('idsdkichildkategori', 'idsdkichildkategori', 'required');
        $this->form_validation->set_rules('definisichildkategorisdki', 'definisichildkategorisdki', 'required');


        $arrMsg = "";
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(["msg" => "danger", "txt" => "Gagal Menyimpan Data!"]);
        } else {
            $arrMsg = $this->msqlbasic->update_db('rs_master_sdki_childkategori', ['id_sdki_childkategori' => $post['idsdkichildkategori']], ["definisi" => $post['definisichildkategorisdki']]);

            if ($arrMsg) {
                echo json_encode(["msg" => "success", "txt" => "Berhasil Menyimpan Data!"]);
            } else {
                echo json_encode(["msg" => "danger", "txt" => "Gagal Menyimpan Data!"]);
            }
        }
    }

    public function hapus_tautan_sdki_siki()
    {
        $idtautansdkisiki = $this->input->post('idtautansdkisiki');

        $query = $this->db->where('id_tautan_sdki_siki',$idtautansdkisiki);
        $query = $this->db->delete('rs_master_siki_tautan_sdki_siki');

        $result = "";
        if($query)
        {
            $result = [
                "msg" => "success",
                "txt" => "Berhasil Menghapus Data"
            ];
        }
        else
        {
            $result = [
                "msg" => "danger",
                "txt" => "Gagal Menghapus Data"
            ];

        }
        echo json_encode($result);
    }

    /////////////////NEW INM///////////////
    public function inventorynonmedis()
    {
        // error_reporting();
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'inventorynonmedis',
            'content_view'     => 'masterdata/v_inm',
            'script_js'        => ['master/inventaris/js_inm'],
            'title_page'       => 'Inventory Non Medis',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_inventorynonmedis()
    {
        $getData = $this->mdatatable->dt_inventorynonmedis_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->kodeblok;
                $row[] = $obj->namablok;
                $row[] = $obj->koderuang;
                $row[] = $obj->namaruanginventaris;
                $row[] = convertToRupiah($obj->jumlahnonmedis);
                $row[] = '<a id="detailinventaris" namaruang="' . $obj->namaruanginventaris . '" alt="' . $obj->idruanginventaris . '" class="btn btn-xs btn-info"><i class="fa fa-eye"></i> Detail</a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_inventorynonmedis(),
            "recordsFiltered" => $this->mdatatable->filter_dt_inventorynonmedis(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    //detail inventaris nonmedis
    public function dt_detailinventorynonmedis()
    {
        $getData = $this->mdatatable->dt_detailinventorynonmedis_(true);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = $obj->namabaranginventaris;
                $row[] = $obj->merekbarang;
                $row[] = $obj->modelbarang;
                $row[] = $obj->nomorseri;
                $row[] = $obj->nomorinventaris;
                $row[] = $obj->nomorsertifikat;
                $row[] = $obj->tanggalpembelian;
                $row[] = convertToRupiah($obj->nilaiaset);
                $row[] = convertToRupiah($obj->nilaipenyusutan);
                $row[] = $obj->status;
                $row[] = (($obj->status == 'pindah') ? '' : '<a id="editinventaris" alt="' . $obj->idinventaris . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a>');
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_detailinventorynonmedis(),
            "recordsFiltered" => $this->mdatatable->filter_dt_detailinventorynonmedis(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }


    public function fillunitname()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("concat('bangsal-',rb.idbangsal) as id,rb.namabangsal as text")->like('namabangsal', $cari)->get('rs_bangsal rb')->result_array();
        $join1 = $this->db->last_query();
        $data2 = $this->db->select("concat('unit-',ru.idunit) as id,ru.namaunit as text")->like('namaunit', $cari)->get('rs_unit ru')->result_array();

        $join2 = $this->db->last_query();
        $union_query = $this->db->query('('.$join1.') UNION ('.$join2.')')->result_array();

        echo json_encode($union_query);

        // $data = $this->db->select("ru.idunit as id,ru.namaunit as text")->like('namaunit', $cari)->limit(20)->get('rs_unit ru')->result_array();
        // echo json_encode($data);
        
    }
    public function fillsupplier()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("rs.id_supplier as id,rs.supplier_name as text")->where('supplier_cat','nonmedis')->like('supplier_name', $cari)->limit(20)->get('rs_master_supplier rs')->result_array();
        echo json_encode($data);
    }
    public function fillmerk()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("rm.idmerekbarang as id,rm.merekbarang as text")->like('merekbarang', $cari)->limit(20)->get('inventaris_merek rm')->result_array();
        echo json_encode($data);
    }
    public function fillitem()
    {
        $cari = $this->input->post('searchTerm');
        $data = $this->db->select("rb.idbaranginventaris as id,rb.namabaranginventaris as text")->like('namabaranginventaris', $cari)->limit(20)->get('inventaris_barang rb')->result_array();
        echo json_encode($data);
    }

    public function insert_inm()
    {
        $iduser = $this->get_iduserlogin();
        $data = [
            'id_unit' => $this->input->post('unit'),
            'no_inventory' => $this->input->post('noinventory'),
            'id_barang' => $this->input->post('item'),
            'id_merk' => $this->input->post('merk'),
            'kondisi' => $this->input->post('status'),
            'id_distributor' => $this->input->post('supplier'),
            'harga' => $this->input->post('harga'),
            'tahun_pengadaan' => $this->input->post('tahun'),
            'user_id' => $iduser
        ];
        $arrMsg =  $this->db->insert('inventaris_non_medis', $data);
        pesan_success_danger($arrMsg, "Berhasil Insert Inventory Non-Medis.", "Inventory Non-Medis Gagal Insert.", "js");
    }

    public function dt_inm()
    {
        $this->load->model('mdatatable');
        $id_unit = $this->input->post('unit');
        $getData = $this->mdatatable->dt_inventorynonmedis($id_unit);
        $data = [];
        $no = $_POST['start'];
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->unit_name;
                $row[] = $obj->inv_item;
                $row[] = $obj->no_inventory;
                $row[] = $obj->kondisi;
                $row[] = $obj->supplier_name;
                $row[] = $obj->tahun_pengadaan;
                $row[] = "Rp. ".number_format($obj->harga,0);
                $row[] = '<a class="btn btn-danger btn-xs" id="delete" alt="' . $obj->id_inventory . '"><i class="fa fa-trash"></i> Hapus</a>'.'    '.'<a id="edit" alt="' . $obj->id_inventory . '" class="btn btn-warning btn-xs"><i class="fa fa-edit"> Edit</i></a>';
                $data[] = $row;
            }
        }
        // <a id="hapus" alt="' . $obj->idmember . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '>
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_inventorynonmedis($id_unit),
            "recordsFiltered" => $this->mdatatable->filter_dt_inventorynonmedis($id_unit),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    public function inm_ceknoinventory()
    {
        $data = $this->db->query("select * from inventaris_non_medis where no_inventory='" . $this->input->post('noinventory') . "'");
        if ($data->num_rows() > 0) {
            $data = 'sudahada';
        } else {
            $data = 'belumada';
        }
        echo json_encode($data);
    }

    public function hapus_inm()
    {
        $arrMsg = $this->db->delete('inventaris_non_medis', ['id_inventory' => $this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    public function unduhexcel()
    {
         $id_unit = $this->input->post('unitid');
         $data['judul']      = 'Inventory Non-Medis';
         $x=(explode("-",$str));
         if ($x[0]=='unit') {
            $unitname           = $this->db->where('idunit',$x[1])->get('rs_unit')->result_array();
            $new_name           = str_replace(' ', '', $unitname[0]['namaunit']);
            $data['namaunit']   = $this->db->query("SELECT upper(ru.namaunit)namaunit from rs_unit ru where ru.idunit=".$x[1])->row_array();
            $data['uname']      = strtoupper($unitname[0]['namaunit']);
         } else {
            $unitname           = $this->db->where('idbangsal',$x[1])->get('rs_bangsal')->result_array();
            $new_name           = str_replace(' ', '', $unitname[0]['namabangsal']);
            $data['namaunit']   = $this->db->query("SELECT upper(rb.namabangsal)namaunit from rs_bangsal rb where rb.idbangsal=".$x[1])->row_array();
            $data['uname']      = strtoupper($unitname[0]['namabangsal']);
         }
         $kaunit             = "kanit".strtolower($new_name);
         $data['kanit']      = $this->mkombin->getKonfigurasi($kaunit);
         $data['manager']    = $this->mkombin->getKonfigurasi('mngkeuangan');
         $this->load->model('mdatatable');
         $getdata            = $this->mdatatable->dt_inm($id_unit);
         $data['laporan']    = '';
         if($getdata)
         {
            $data['laporan'] = $getdata->result(); 
         }
         return $this->load->view('report/inventaris/excel_inm',$data);
    }

    public function onreadyform_inm()
    {
        $idinventory = $this->input->post('idv');
        $dataedit = $this->db->select('inm.*,inm.id_inventory,im.merekbarang,rb.namabangsal as unit_name,ms.supplier_name,ib.namabaranginventaris as inv_item,inm.no_inventory,inm.kondisi,harga,inm.tahun_pengadaan')->join("rs_master_supplier ms","ms.id_supplier=inm.id_distributor")->join("inventaris_merek im","im.idmerekbarang=inm.id_merk")->join("inventaris_barang ib","ib.idbaranginventaris=inm.id_barang")->join("rs_bangsal rb","rb.idbangsal=inm.id_unit")->get_where('inventaris_non_medis inm', ['id_inventory' => $idinventory])->row_array();
        $hasil = array(
            'status' => $this->mmasterdata->get_data_enum('inventaris_inventaris', 'status'),
            'dataedit' => $dataedit,
            'listunit' => empty($idinventory) ? [] : $this->db->select("rb.idbangsal as id,rb.namabangsal as text")->get_where('rs_bangsal rb', ['rb.idbangsal' => $dataedit['id_unit']])->result_array(),
            'listsupplier' => empty($idinventory) ? [] : $this->db->select("rs.id_supplier as id,rs.supplier_name as text")->get_where('rs_master_supplier rs', ['rs.id_supplier' => $dataedit['id_distributor']])->result_array(),
            'listmerk' => empty($idinventory) ? [] : $this->db->select("rm.idmerekbarang as id,rm.merekbarang as text")->get_where('inventaris_merek rm', ['rm.idmerekbarang' => $dataedit['id_merk']])->result_array(),
            'listitem' => empty($idinventory) ? [] : $this->db->select("rb.idbaranginventaris as id,rb.namabaranginventaris as text")->get_where('inventaris_barang rb', ['rb.idbaranginventaris' => $dataedit['id_barang']])->result_array()
        );
        echo json_encode($hasil);
    }

    ///Master Supplier -nurhi
    public function mssupplier()
    {
        $data =  [
            'active_menu'      => 'masterdata',
            'active_sub_menu'  => 'inventariskantor',
            'active_menu_level' => 'mssupplier',
            'content_view'     => 'masterdata/v_mastersupplier',
            'script_js'        => ['master/js_supplier'],
            'title_page'       => 'List Master Supplier',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN]
        ];
        $this->load->view('v_index', $data);
    }

    public function dt_mssupplier()
    {
        $getData = $this->mdatatable->dt_mssupplier_(true);
        $data = [];
        $no = 0;
        if ($getData) {
            foreach ($getData->result() as $obj) {
                $row = [
                    ++$no, $obj->supplier_name,
                    $obj->supplier_address,
                    $obj->supplier_cat
                ];

                $row[] = '<a id="edit" alt="' . $obj->id_supplier . '" class="btn btn-xs btn-warning" ' . ql_tooltip('ubah') . '><i class="fa fa-edit"></i></a> <a id="delete" alt="' . $obj->id_supplier . '" class="btn btn-xs btn-danger" ' . ql_tooltip('hapus') . '><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_mssupplier(),
            "recordsFiltered" => $this->mdatatable->filter_dt_mssupplier(),
            "data" => $data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function  onreadyformmastersupplier()
    {
        $supp = $this->db->get_where('rs_master_supplier', ['id_supplier' => $this->input->post('i')])->row_array();
        $jenis    = $this->mmasterdata->get_data_enum('inventaris_barang', 'jenisalat');
        // print_r($jenis); return false;

        // $kategori = $this->db->select('idbarangkategori as id, kategori as txt')->get('inventaris_barangkategori')->result_array();
        echo json_encode(['supplier' => $supp, 'jenis' => $jenis]);
    }
    public  function save_mssupplier()
    {
        $post = $this->input->post();
        $data = [
            'supplier_name' => $post['supplier_name'],
            'supplier_address' => $post['supplier_address'],
            'tlp' => $post['tlp'],
            'supplier_cat' => $post['jenis']
        ];
        if (empty($post['id_supplier'])) {
            $arrMsg =  $this->db->insert('rs_master_supplier', $data);
            pesan_success_danger($arrMsg, "Berhasil Insert Supplier.", "Gagal Insert Supplier.", "js");
        } else {
            $arrMsg =  $this->db->where('id_supplier', $post['id_supplier'])->update('rs_master_supplier',$data);
            pesan_success_danger($arrMsg, "Berhasil Update Supplier.", "Gagal Update Supplier.", "js");
        }
    }
    public function hapus_mastersupplier()
    {
        $arrMsg =  $this->db->where('id_supplier', $this->input->post('a'))->delete('rs_master_supplier');
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }

    public function get_edit_siki()
    {
        $post   = $this->input->post();
        $idsiki =  $post['idsiki'];

        $query = $this->show_master_siki_byidsiki($idsiki);

        echo json_encode($query);
    }

    public function show_master_siki_byidsiki($idsiki)
    {
        $sql = "SELECT * FROM rs_master_siki WHERE id_siki='".$idsiki."' LIMIT 1";
        $query = $this->db->query($sql)->row_array();
        return $query;
    }
}    
/* End of file ${TM_FILENAME:${1/(.+)/Cmasterdata.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:applicatagalion).+)/Cmasterdata/:application/controllers/${1/(.+)/Cmasterdata.php/}} */