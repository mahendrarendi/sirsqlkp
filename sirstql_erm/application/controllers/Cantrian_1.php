<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama

class Cantrian_1 extends MY_controller 
{
    function __construct()
    {
	   parent::__construct();
       // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
    }
    ///////////////////////ADMINISTRASI PEMANGGILAN ANTRIAN PASIEN///////////////////
    //-------------------------- vv Standar
    public function setting_administrasipemanggilanantrian()
    {
        return [    'content_view'      => 'antrian/v_administrasipemanggilanantrian',
                    'active_menu'       => 'antrian',
                    'active_sub_menu'   => 'administrasipemanggilanantrian',
                    'active_menu_level' => '',
                    'script_js'         => ['js_antrian_1'],
                    'plugins'           => [ PLUG_DATATABLE, PLUG_DATE, PLUG_DROPDOWN]
               ];
    }
    public function administrasipemanggilanantrian()
    {
            $data                   = $this->setting_administrasipemanggilanantrian();
            $idstasiun    = $this->session->userdata('sess_stasiun');
            $wherestasiun = empty($idstasiun) ? '' : " where idstasiun in (".$idstasiun.") " ;
            $data['stasiun']        = $this->db->query("select * from antrian_stasiun ".$wherestasiun)->result();
            $data['title_page']     = 'Administrasi Pemanggilan Antrian';
            $data['mode']           = 'view';
            $data['table_title']    = 'Administrasi Pemanggilan Antrian ';
            $this->load->view('v_index', $data);   
    }
    public function loadantrian()
    {
        $idsta = $this->input->post("s");
        $all   = $this->input->post("all");
        $tanggal = $this->input->post("tgl");
        $idl     = $this->input->post("L");
        $whereantri = (($this->pageaccessrightbap->checkAccessRight(V_PEMANGGILANANSEMUATRIAN))?'' : ' and a.status !="antri" ');
        $whereloket = ((empty($all))? ((!empty($idl))? ' and a.idloket="'.$idl.'" ':'') : '');
        $hasil = $this->db->query("select idantrian, namaloket, no, status, ifnull(namalengkap, '') as namalengkap, l.fontawesome, l.idjenisloket, idpendaftaran, idpemeriksaan from antrian_antrian a join antrian_loket l on l.idloket=a.idloket left join person_person p on p.idperson=a.idperson where tanggal='".$tanggal."' and idstasiun='".$idsta."' ".$whereantri.$whereloket." and a.status !='ok' and a.status != 'batal' order by if(status='panggil' or status='sudah panggil',1,2), namaloket, no")->result();
        echo json_encode($hasil);
    }
    
    public function loadloket()
    {
        $stalok   = $this->statistikloket();
        $idsta    = $this->input->post("s");
        $this->db->order_by("namaloket");
        $hasil    = $this->mgenerikbap->select_multitable("idloket, namaloket, idstasiun, fontawesome", "antrian_loket", ['idstasiun'=>$idsta])->result();
        echo json_encode(["loket" => $hasil, "statistik" => $stalok]);
    }
    
    public function statistikloket()
    {
        $idsta    = $this->input->post("s");
        $tanggal = $this->input->post("tgl");
        $hasil = $this->db->query("select a.idloket, count(idantrian) as antriandiambil, sum(if(status='antri',1,0)) as antri from antrian_antrian a join antrian_loket l on l.idloket=a.idloket where tanggal='".$tanggal."' and idstasiun=$idsta group by idloket")->result();
        return $hasil;
    }
    
    public function next()
    {
        $post = $this->input->post();
        $idloketpemanggilan = $this->session->userdata('idloketpemanggilan');
        $log_waktu = $this->inputwaktulayanan_penyerahanresep($this->input->post('a'));
        $arrMsg    = $this->db->query("update antrian_antrian a join (select idantrian, no, status from antrian_antrian where idloket='".$this->input->post('l')."' and tanggal=date(now()) and status='antri' order by no limit 1) aa set a.status='panggil', a.idloketpemanggilan ='".$idloketpemanggilan."' where a.idantrian=aa.idantrian");
        $a = $post['l'];
        /** Memey */
        // $antrian = $this->db->query("select antrian_antrian.idantrian, antrian_antrian.no, antrian_antrian.status, antrian_antrian.idloketpemanggilan, waktu_pendaftaran_admisi.id_pendaftaran_admisi, waktu_pendaftaran_admisi.waktu_ambilantrian, waktu_pendaftaran_admisi.nomor_antrian, waktu_pendaftaran_admisi.waktu_panggil_antrian from waktu_pendaftaran_admisi right join antrian_antrian ON antrian_antrian.idantrian=waktu_pendaftaran_admisi.idantrian where antrian_antrian.idantrian='".$a."' and antrian_antrian.status='panggil'")->row_array();
        $antrian = $this->db->query("select antrian_antrian.idantrian, antrian_antrian.no, antrian_antrian.status, antrian_antrian.idloketpemanggilan, waktu_pendaftaran_admisi.id_pendaftaran_admisi, waktu_pendaftaran_admisi.waktu_ambilantrian, waktu_pendaftaran_admisi.jenis_antrian, waktu_pendaftaran_admisi.nomor_antrian, waktu_pendaftaran_admisi.waktu_panggil_antrian from waktu_pendaftaran_admisi right join antrian_antrian ON antrian_antrian.idantrian=waktu_pendaftaran_admisi.idantrian where antrian_antrian.idloket='".$a."' and antrian_antrian.status='panggil'")->row_array();
        $idantrian   = $antrian['idantrian'];
        $waktu_panggil_antrian = $antrian['waktu_panggil_antrian'];
        if ($waktu_panggil_antrian == '0000-00-00 00:00:00') {
            $this->db->where('idantrian',$idantrian);
            $update = $this->db->update('waktu_pendaftaran_admisi',['waktu_panggil_antrian'=>date('Y-m-d H:i:s')]);
        }else {
            $this->db->where('idantrian',$idantrian);
            $update = $this->db->update('waktu_pendaftaran_admisi',['waktu_panggil_antrian'=>$waktu_panggil_antrian]);
        }

        if($log_waktu['status'] == 'notif')
        {
            echo json_encode($log_waktu);
        }
        else
        {
            pesan_success_danger($arrMsg, 'Panggil antrian berhasil.!', 'Panggil antrian gagal.!', 'js');
        }
        
    }
    
    public function panggilulang()
    {
        $post = $this->input->post();
        $idloketpemanggilan = $this->session->userdata('idloketpemanggilan');
        $log_waktu = $this->inputwaktulayanan_penyerahanresep($this->input->post('a'));
        $this->mgenerikbap->setTable('antrian_antrian');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['status'=>'panggil','idloketpemanggilan'=>$idloketpemanggilan], $this->input->post('a'));
        $a = $post['a'];

        /** Memey */
        $antrian = $this->db->query("select antrian_antrian.idantrian, antrian_antrian.no, antrian_antrian.status, antrian_antrian.idloketpemanggilan, waktu_pendaftaran_admisi.id_pendaftaran_admisi, waktu_pendaftaran_admisi.waktu_ambilantrian, waktu_pendaftaran_admisi.jenis_antrian, waktu_pendaftaran_admisi.nomor_antrian, waktu_pendaftaran_admisi.waktu_panggil_antrian from waktu_pendaftaran_admisi right join antrian_antrian ON antrian_antrian.idantrian=waktu_pendaftaran_admisi.idantrian where antrian_antrian.idantrian='".$a."' and antrian_antrian.status='panggil'")->row_array();
        $idantrian   = $antrian['idantrian'];
        $waktu_panggil_antrian = $antrian['waktu_panggil_antrian'];
        if ($waktu_panggil_antrian == '0000-00-00 00:00:00') {
            $this->db->where('idantrian',$idantrian);
            $update = $this->db->update('waktu_pendaftaran_admisi',['waktu_panggil_antrian'=>date('Y-m-d H:i:s')]);
        }else {
            $this->db->where('idantrian',$idantrian);
            $update = $this->db->update('waktu_pendaftaran_admisi',['waktu_panggil_antrian'=>$waktu_panggil_antrian]);
        }

        if(!empty($log_waktu['status']))
        {
            if($log_waktu['status'] == 'notif')
            {
                echo json_encode($log_waktu);
            }
            else
            {
                pesan_success_danger($arrMsg, 'Panggil ulang berhasil.!', 'Panggil ulang gagal.!', 'js');
            }
        }
        else
        {
            pesan_success_danger($arrMsg, 'Panggil ulang berhasil.!', 'Panggil ulang gagal.!', 'js');
        }
        
    }
 
    
    public function pemanggilanhasil()
    {
        $this->db->where('idloket!=',39);
        $arrMsg = $this->db->update('antrian_antrian',['status'=>'panggilhasil'],['idpemeriksaan'=>$this->input->post('i')]);
        pesan_success_danger($arrMsg, 'Pemanggilan Hasil Berhasil.', 'Pemanggilan Hasil Gagal.', 'js');
    }
    /**
     * Klik proses di pemanggilan antrian
     */
    public function sedangproses()
    {
        $idantrian    = $this->input->post('a');
        $idjenisloket = $this->input->post('idjenisloket');
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $this->mgenerikbap->setTable('antrian_antrian');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['status'=>'sedang proses'], $idantrian);
        
        //save log task antrean
        $this->bridging_save_log_antrean($idjenisloket,$idantrian,'sedang proses',$idpemeriksaan,$idpendaftaran);
        pesan_success_danger($arrMsg, 'Sedang Proses berhasil.!', 'Sedang Proses gagal.!', 'js'); 
    }
    /**
     * Klik batal di pemanggilan antrian
     */
    public function batal()
    {
        $idantrian     = $this->input->post('a');
        $idjenisloket  = $this->input->post('idjenisloket');
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $keterangan    = $this->input->post('keterangan');
        //ubah 
        $this->mgenerikbap->setTable('antrian_antrian');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['ket_pembatalan'=>$keterangan,'status'=>'batal'], $idantrian);
        //save log task antrean
        $this->bridging_save_log_antrean($idjenisloket,$idantrian,'BATAL',$idpemeriksaan,$idpendaftaran);       
        
        pesan_success_danger($arrMsg, 'Pembatalan berhasil.!', 'Pembatalan gagal.!', 'js'); 
    }
    
    /**
     * Klik ok di pemanggilan antrian
     */
    public function ok()
    {
        $idantrian    = $this->input->post('a');
        $idjenisloket = $this->input->post('idjenisloket');
        $idpemeriksaan = $this->input->post('idpemeriksaan');
        $idpendaftaran = $this->input->post('idpendaftaran');
        //input log penyerahan resep
        $this->inputwaktulayanan_penyerahanresep($idantrian);
        //ubah 
        $this->mgenerikbap->setTable('antrian_antrian');
        $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate(['status'=>'ok'], $idantrian);
        //save log task antrean
        $this->bridging_save_log_antrean($idjenisloket,$idantrian,'OK',$idpemeriksaan,$idpendaftaran);       
        
        pesan_success_danger($arrMsg, 'OK berhasil.!', 'OK gagal.!', 'js'); 
    }
    
    private function inputwaktulayanan_penyerahanresep($idantrian)
    {
        //simpan waktu ok farmasi
        if($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAAN_WAKTULAYANANFARMASI))
        {
            $dt = $this->db->query("select a.idpendaftaran, p.norm, p.waktuperiksa, p.statustagihan from antrian_antrian a join person_pendaftaran p on p.idpendaftaran=a.idpendaftaran where a.idantrian='".$idantrian."'");
            if($dt->num_rows() > 0)
            {
                $data  = $dt->row_array();
                $waktu = $this->db->get_where("rs_waktu_layanan",['idpendaftaran'=>$data['idpendaftaran'],'norm'=>$data['norm'],'kegiatan'=>'penyerahanresepfarmasi','unitlayanan'=>'farmasi']);
                if($waktu->num_rows() > 0)
                {
                   $simpan = $this->db->update('rs_waktu_layanan',['waktuselesai'=>date('H:i:s')],['idpendaftaran'=>$data['idpendaftaran'],'norm'=>$data['norm'],'kegiatan'=>'penyerahanresepfarmasi','unitlayanan'=>'farmasi']);
                }
                else
                {
                    $simpan = $this->insert_waktulayanan($data['norm'],$data['idpendaftaran'],'penyerahanresepfarmasi',$data['waktuperiksa'],'farmasi');
                }
                
                //set notifikasi lunas belum lunas
                if($data['statustagihan'] != 'lunas')
                {
                    $res['status']        = 'notif';
                    $res['statustagihan'] = $data['statustagihan'];
                    $res['message']       = 'Pasien Belum Melunasi Biaya Tagihan';
                    
                    return $res;
                }
                else
                {
                    $res['status']        = 'bukan';                    
                    return $res;
                }
            }
        }
    }


    // mahmud, clear
    public function panggil_antrianorderugd()
    {
        echo json_encode($this->db->query("select a.idloket, l.namaloket, l.fontawesome, count(idantrian) as antriandiambil, sum(if(status!='ok',1,0)) as antri from antrian_antrian a join antrian_loket l on l.idloket=a.idloket where tanggal=date(now()) and l.namaloket like '%ugd%' or tanggal=date(now()) and l.namaloket like'%poli umum%' group by idloket")->result());
    }
    // mahmud, clear --> generate no antrian
    private function generate_no_antrian($antrianStatus,$idstasiun,$idloket,$callid)
    {
        $this->mgenerikbap->setTable('helper_autonumber');
        $data = [
            'id'      =>$antrianStatus.date('Ymd').'-'.$idstasiun.'-'.$idloket,
            'number'  =>NULL,
            'callid'  =>$callid,
            'expired' =>date('Y-m-d')
        ];
        return $this->mgenerikbap->insert($data);
    }
    // mahmud, clear -> create antrian_
    private function create_antrian($idpemeriksaan,$antriantable,$idperson,$idunit)
    {
        $callid = generaterandom(8);
        $akronimunit = $this->mgenerikbap->select_multitable("rj.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();
        if (empty($akronimunit))
        {
            $unit = $this->mgenerikbap->select_multitable("namaunit","rs_unit",['idunit'=>$idunit])->row_array();
            echo json_encode(['err'=>'ERR_LOKET', 'namaunit' => $unit['namaunit']]);
        }
        else
        {
            $this->generate_no_antrian('AntrianFarmasi',$akronimunit['idstasiun'], $akronimunit['idloket'], $callid);
            $noantrian = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array();
            $data = ['idloket'=>$akronimunit['idloket'], 'tanggal'=>date('Y-m-d'), 'no'=>$noantrian['number'], 'status'=>'antri','idperson'=>$idperson];
            $arrMsg = $this->db->insert($antriantable,$data);
            $loket = $this->db->query("select * from antrian_loket where idloket='$akronimunit[idloket]'")->row_array();
            echo json_encode(($arrMsg) ? ['status' => 'success', 'message' => 'Antrian Success..!', 'noantrian'=>$noantrian['number'], "namaloket"=>$loket['namaloket'] ] : ['status' => 'danger', 'message' => 'Antrian Failed..!']);
        }
    }
    // mahmud, clear -> create antrian farmasi
    public function create_antrianfarmasi()
    {
        $callid = generaterandom(8);
        $select = $this->db->query("select ppd.idunit, rp.idpemeriksaan, ppas.idperson  from person_pendaftaran ppd, rs_pemeriksaan rp, person_pasien ppas where ppd.norm='".$this->input->post('norm')."' and rp.idpendaftaran = ppd.idpendaftaran and ppas.norm = ppd.norm and ppd.idstatuskeluar='1'")->row_array();
        $this->generate_no_antrian('AntrianFarmasi','8', '40', $callid);
            $noantrian = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array();
            $data = ['idloket'=>'40', 'tanggal'=>date('Y-m-d'), 'no'=>$noantrian['number'], 'status'=>'antri','idperson'=>$select['idperson']];
            $arrMsg = $this->db->insert('antrian_farmasi',$data);
            $loket = $this->db->query("select * from antrian_loket where idloket='40'")->row_array();
            echo json_encode(($arrMsg) ? ['status' => 'success', 'message' => 'Antrian Success..!', 'noantrian'=>$noantrian['number'] ] : ['status' => 'danger', 'message' => 'Antrian Failed..!']);
    }
}
/* End of file ${TM_FILENAME:${1/(.+)/Cadmission.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cadmission/:application/controllers/${1/(.+)/Cadmission.php/}} */






