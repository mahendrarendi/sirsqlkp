<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Cadministrasi extends MY_controller 
{
    function __construct()
    {
	parent::__construct();
    // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}

        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation','session'));
        $this->load->model('msqlbasic');
    }
    ///////////////////////ADMINISTRASI USER///////////////////
    //-------------------------- vv Standar
    public function settinguser()
    {
        return [    'content_view'      => 'administrasi/v_user',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'user',
                    'active_menu_level' => '',
                    'plugins'           => [ PLUG_DATATABLE ]
               ];
    }
    public function user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas
        {
            $data                   = $this->settinguser(); //letakkan di baris pertama
            $this->mgenerikbap->setTable('login_user');
            $data['title_page']     = 'User';
            $data['mode']           = 'view';
            $data['table_title']    = 'List User';
            $data['jsmode']         = 'masteruser';
            $data['script_js']      = ['js_datatable'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function load_datauser()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas
        {
            $this->load->model('mdatatable');
            $list = $this->mdatatable->dt_user_(true);
            $data = array();
            $no = $_POST['start'];
            foreach ($list->result() as $field) {
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $username     = $this->encryptbap->encrypt_urlsafe($field->namauser, "json");
                $id           = $this->encryptbap->encrypt_urlsafe($field->iduser, "json");
                $tabel        = $this->encryptbap->encrypt_urlsafe('login_user', "json");
                $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_USER, "json");
                $row = array();
                $row[] = $field->namauser;
                $row[] = $field->password_reset;
                $row[] = '<a data-toggle="tooltip" title="" data-original-title="Reset Password" id="reset_password" class="btn btn-warning btn-xs" alt="'.$username.'" alt2="'.$id.'" ><i class="fa fa-refresh" ></i> Reset</a> <a data-toggle="tooltip" title="" data-original-title="Delete User" id="delete_data" nobaris="'.($no-1).'" class="btn btn-danger btn-xs" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'"><i class="fa fa-trash"></i> Delete</a>';
                $data[] = $row;
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_user(),
                "recordsFiltered" => $this->mdatatable->filter_dt_user(),
                "data" => $data,
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    // reset password
    public function reset_password()
    {
        $username = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('u')));
        $password = time();
        $data     = ['password'=>password_hash($password, PASSWORD_DEFAULT),'password_reset'=>$password];
        $arrMsg   = $this->db->update('login_user',$data,['namauser'=>$username]);
        pesan_success_danger($arrMsg, 'Reset Password Success.!', 'Reset Password Failed.!', 'js');        
    }
    public function add_user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas
        {
            $data               = $this->settinguser(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page'] = 'Add User';
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_CHECKBOX];
            $data['data_edit']  ='';
            $data['jsmode']     = 'masteruser';
            $data['script_js']  = ['js_administrasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function edit_user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas
        {
            $data               = $this->settinguser(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page'] = 'Edit User';
            $this->mgenerikbap->setTable('login_user');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("USER", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*',['iduser'=>$id])->row_array();
            $data['mode']       = 'add';
            $data['plugins']    = [PLUG_CHECKBOX];
            $data['jsmode']     = 'masteruser';
            $data['script_js']  = ['js_administrasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function save_user()
    {	
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas            
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('username','','required');
            $iduser   = json_decode($this->encryptbap->decrypt_urlsafe("USER", $this->input->post('iduser')));
            empty($iduser) ? $this->form_validation->set_rules('password','','required') : '' ;
            //Jika Valid maka lanjutkan proses simpan
            if (validation_input())
            {
                //Persiapkan data
                $namauser   = $this->input->post('username');
                $password   = passwordhash($this->input->post('password')); 
                $data = ['namauser' => $namauser, 'password' => $password];
                //Simpan
                $simpan = $this->db->insert('login_user',$data);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, 'Save Success.!', 'Save Failed.!');
            }
            redirect(base_url('cadministrasi/user'));
        }
    }
    // DELETE DATA
    public function single_delete()
    {
        if ($this->pageaccessrightbap->checkAccessRight($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('hal')))) //lihat define di atas
        {
            $this->mgenerikbap->setTable($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('t')));
            $arrMsg = $this->mgenerikbap->delete($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('i')));
            //jika hapus berhasil status success selain itu status error
            pesan_success_danger($arrMsg, 'Delete Success.!', 'Delete Failed.!', 'js');
        }
    }
    //untuk edit lihat ke edit_akses_user()
    //-------------------------- ^^ Standar
    
    ///////////////////////ADMINISTRASI HAK AKSES USER///////////////////
    public function setting_akses_user()
    {
        return [    'content_view'      => 'administrasi/v_akses_user',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'akses_user',
                    'active_menu_level' => ''
               ];
    }
    public function akses_user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)) //lihat define di atas
        {
            $data                = $this->setting_akses_user();
            $data['data_list']  =  $this->mgenerikbap->select_multitable("lhau.idhakaksesuser, lu.namauser, lhau.peranperan","login_hakaksesuser, login_user")->result();
            $data['title_page']  = 'Akses User';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Akses User';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['jsmode']      = 'aksesuser';
            $data['script_js']   = ['js_datatable'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function load_dtaksesuser()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas
        {
            $this->load->model('mdatatable');
            $list = $this->mdatatable->dt_aksesuser_(true);
            $data = array();
            $no = $_POST['start'];
            foreach ($list->result() as $field) {
                
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                $id        =  $this->encryptbap->encrypt_urlsafe(json_encode($field->idhakaksesuser));
                $tabel     = $this->encryptbap->encrypt_urlsafe(json_encode('login_hakaksesuser'));
                $idhalaman = $this->encryptbap->encrypt_urlsafe(V_USERAKSES, "json");
                $no++;
                $row = array();
                $row[] = $no;
                $row[] = $field->namauser;
                $row[] = $field->peranperan;
                $row[] = '<a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cadministrasi/edit_akses_user/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a> <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.($no-1).'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'"><i class="fa fa-trash"></i> Delete</a>';
                $data[] = $row;
                
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_aksesuser(),
                "recordsFiltered" => $this->mdatatable->filter_dt_aksesuser(),
                "data" => $data,
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    public function add_akses_user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)) //lihat define di atas
        {
            $data               = $this->setting_akses_user();
            $this->load->helper('form');
            $data['data_list']  = $this->db->get('login_user')->result();
            $data['data_unit']  = $this->db->get('rs_unit_farmasi')->result_array();
            $data['data_akses'] = $this->db->get('login_peran')->result_array();
            $data['data_stasiun'] = $this->db->get('antrian_stasiun')->result_array();
            $data['title_page'] = 'Add Akses User';
            $data['mode']       = 'add';
            $data['jsmode']     = 'masteraksesuser';
            $data['plugins']    = [ PLUG_DROPDOWN, PLUG_CHECKBOX ];
            $data['script_js']  = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    public function edit_akses_user()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)) //lihat define di atas
        {
            $data                 = $this->setting_akses_user();
            $this->load->helper('form');
            $data['data_list']  = $this->db->get('login_user')->result();
            $data['data_unit']  = $this->db->get('rs_unit_farmasi')->result_array();
            $data['data_akses'] = $this->db->get('login_peran')->result_array();
            $data['data_stasiun'] = $this->db->get('antrian_stasiun')->result_array();
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $this->mgenerikbap->setTable('login_hakaksesuser');
            $data['data_edit'] = $this->mgenerikbap->select('*',['idhakaksesuser'=>$id])->row_array();
            $data['title_page'] = 'Edit Akses User';
            $data['mode'] = 'edit';
            $data['jsmode']      = 'masteraksesuser';
            $data['plugins'] = [ PLUG_DROPDOWN, PLUG_CHECKBOX ];
            $data['script_js'] = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    // SIMPAN HAK AKSES USER
    public function save_akses_user()
    {	
        if ($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('iduser','','required');
            $this->form_validation->set_rules('peranperan','','');
            $this->form_validation->set_rules('unituser','','');
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $idhakaksesuser = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idhakaksesuser')));
                $iduser = $this->input->post('iduser');
                $peran = $this->input->post('peranperan');
                $unit  = $this->input->post('unituser');
                $stasiun  = $this->input->post('stasiun');
                empty($peran) ? $peranperan='' : $peranperan = implode(',', $peran) ; //jika peran tidak kosong implode peran
                empty($unit) ? $unituser='' : $unituser = implode(',', $unit) ; //jika unit tidak kosong implode unit
                empty($stasiun) ? $stasiun_='' : $stasiun_ = implode(',', $stasiun) ; //jika stasiun tidak kosong implode stasiun
                $data = ['iduser'=>$iduser , 'peranperan'=>$peranperan, 'unituser'=>$unituser,'stasiun'=>$stasiun_];
                $this->mgenerikbap->setTable('login_hakaksesuser'); //set table yang akan dipakai
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idhakaksesuser); //Simpan
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idhakaksesuser) ? 'Save Success.!' :'Update Success.!' , empty($idhakaksesuser) ? 'Save  Failed.!' :'Update Failed.!' );
            }
            redirect(base_url('cadministrasi/akses_user'));
        }
    }
   //* ADMINISTRASI PERAN */
   //-------------------------- ^^ Standar
    public function settingperan()
    {
        return [    'content_view'      => 'administrasi/v_peran',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'peran',
                    'active_menu_level' => ''
               ];
    }
    public function peran()
    {
	if ($this->pageaccessrightbap->checkAccessRight(V_PERAN)) //lihat define di atas
        {
            $data                = $this->settingperan();
            $this->mgenerikbap->setTable('login_peran');
            $data['data_list']      = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Peran';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Peran';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['script_js']   = [];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function add_peran()
    {   
        if ($this->pageaccessrightbap->checkAccessRight(V_PERAN)) //lihat define di atas
        {
            $data               = $this->settingperan();
            $this->load->helper('form');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Peran';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    public function edit_peran()
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PERAN)) //lihat define di atas
        {
            $data                 = $this->settingperan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('login_peran');		
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*',['idperan'=>$id])->row_array();
            $data['title_page']= 'Edit Peran';
            $data['mode']      = 'edit';
            $data['plugins']   = [];
            $data['script_js'] = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    public function save_peran() // SIMPAN PERAN
    {	
         if ($this->pageaccessrightbap->checkAccessRight(V_PERAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namaperan','','required');
            if ( validation_input() ) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idperan =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idperan')));
                $namaperan = $this->input->post('namaperan');
                $data = ['namaperan'=>$namaperan];
                //Simpan
                $this->mgenerikbap->setTable('login_peran');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idperan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idperan) ? 'Save Success.!' :'Update Success.!' , empty($idperan) ? 'Save  Failed.!' :'Update Failed.!' );
            }
            redirect(base_url('cadministrasi/peran'));
        }
    }   
    //* /ADMINISTRASI PERAN */ 
    
    ///////////////////////ADMINISTRASI HALAMAN ///////////////////
    //-------------------------- ^^ Standar
    public function settinghalaman()
    {
        return [    'content_view'      => 'administrasi/v_halaman',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'halaman',
                    'active_menu_level' => ''
               ];
    }
    public function halaman()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_HALAMAN)) //lihat define di atas
        {
            $data                = $this->settinghalaman();
            $data['data_list']   = $this->db->query("SELECT `lh`.`idhalaman`, (select GROUP_CONCAT(DISTINCT (select namaperan from login_peran where idperan=lhk.idperan) SEPARATOR ', ') from login_hakakseshalaman lhk where idhalaman=lh.idhalaman) as peranperan, `lh`.`namahalaman` FROM login_halaman lh order by lh.idhalaman")->result();
            $data['title_page']  = 'Halaman';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Halaman';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['script_js']   = [];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function add_halaman()
    {   
        if ($this->pageaccessrightbap->checkAccessRight(V_HALAMAN)) //lihat define di atas
        {
        $data               = $this->settinghalaman();
        $this->load->helper('form');
        $data['data_edit']  = '';
        $data['title_page'] = 'Add Halaman';
        $data['data_peran'] = $this->db->get('login_peran')->result_array();
        $data['mode']       = 'add';
        $data['jsmode']     = 'akseshalaman';
        $data['plugins']    = [PLUG_CHECKBOX];
        $data['script_js']  = ['js_administrasi'];
        $this->load->view('v_index',$data);
        }
    }
    public function edit_halaman()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_HALAMAN)) //lihat define di atas
        {
        $data                 = $this->settinghalaman();
        $this->load->helper('form');
        $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
        $data['data_edit']  = $this->db->query("SELECT `lh`.`idhalaman`, (select GROUP_CONCAT(idperan SEPARATOR ', ') from login_hakakseshalaman where idhalaman=lh.idhalaman) as peranperan, `lh`.`namahalaman` FROM `login_halaman` `lh` WHERE `lh`.`idhalaman` = ".$id)->row_array();
        $data['data_peran'] = $this->db->get('login_peran')->result_array();
        $data['title_page'] = 'Edit Halaman';
        $data['mode']       = 'edit';
        $data['jsmode']     = 'akseshalaman';
        $data['plugins']    = [PLUG_CHECKBOX];
        $data['script_js']  = ['js_administrasi'];
        $this->load->view('v_index',$data);
        }
    }
    public function save_halaman() // SIMPAN PERAN
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_HALAMAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('namahalaman','','required');
            if ( validation_input() ) //Jika Valid maka lanjutkan proses simpan
            {
               //Persiapkan data
                $idhalaman =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idhalaman')));
                $namahalaman = $this->input->post('namahalaman');
                $data = ['namahalaman'=>$namahalaman];
                //Simpan
                $this->mgenerikbap->setTable('login_halaman');
                $simpan    = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idhalaman);
                $idhalaman = empty($idhalaman) ? $this->db->insert_id() : $idhalaman;

                $jml = $this->input->post('jumlah');
                foreach ($jml as $key => $value) {
                    $idperan=$this->input->post('peranperan'.$key);
                    $idperansebelum=$this->input->post('peransebelum'.$key);
                    $data = ['idhalaman'=>$idhalaman , 'idperan'=>$idperan];

                    if(empty(!$idperansebelum) || empty(!$idperan))
                    {
                        if( empty($idperansebelum) && empty(!$idperan) )
                        {
                            $simpan = $this->db->insert('login_hakakseshalaman',$data);
                        }
                        else if(empty(!$idperansebelum) && empty($idperan) )
                        {
                            $simpan = $this->db->delete('login_hakakseshalaman',['idhalaman'=>$idhalaman,'idperan'=>$idperansebelum]);
                        }
                        else
                        {
                            $simpan = $this->db->update('login_hakakseshalaman',$data,['idhalaman'=>$idhalaman,'idperan'=>$idperansebelum]);
                        }
                    }
                }

                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idhalaman) ? 'Save Success.!' :'Update Success.!' , empty($idhalaman) ? 'Save  Failed.!' :'Update Failed.!' );
            }
            redirect(base_url('cadministrasi/halaman'));
        }
    }   
    ///////////////////////ADMINISTRASI HAK AKSES HALAMAN ///////////////////
    
    ///////////////////////ADMINISTRASI HUBUNGAN ///////////////////
    //-------------------------- ^^ Standar
    public function settinghubungan()
    {
        return [    'content_view'      => 'administrasi/v_hubungan',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'hubungan',
                    'active_menu_level' => ''
               ];
    }
    public function hubungan()
    {
    if ($this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)) //lihat define di atas
        {
            $data                = $this->settinghubungan();
            $this->mgenerikbap->setTable('person_hubungan');
            $data['data_list']   = $this->mgenerikbap->select('*')->result();
            $data['title_page']  = 'Hubungan';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Hubungan';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['script_js']   = [];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function add_hubungan()
    {   
        if ($this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)) //lihat define di atas
        {
            $data               = $this->settinghubungan();
            $this->load->helper('form');
            $data['data_edit']  = '';
            $data['title_page'] = 'Add Hubungan';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    public function edit_hubungan()
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)) //lihat define di atas
        {
            $data              = $this->settinghubungan();
            $this->load->helper('form');
            $this->mgenerikbap->setTable('person_hubungan');        
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['data_edit'] = $this->mgenerikbap->select('*',['idhubungan'=>$id])->row_array();
            $data['title_page']= 'Edit Hubungan';
            $data['mode']      = 'edit';
            $data['plugins']   = [];
            $data['script_js'] = ['js_administrasi'];
            $this->load->view('v_index',$data);
        }
    }
    public function save_hubungan() // SIMPAN HUBUNGAN
    {   
         if ($this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)) //lihat define di atas
        {
            //Validasi Masukan
            $this->load->library('form_validation');
            $this->form_validation->set_rules('hubungan','','required');
            if (validation_input()) //Jika Valid maka lanjutkan proses simpan
            {
                //Persiapkan data
                $idhubungan =  json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idhubungan')));
                $data = ['namahubungan'=>$this->input->post('hubungan')];
                //Simpan
                $this->mgenerikbap->setTable('person_hubungan');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idhubungan);
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idhubungan) ? 'Save Success.!' :'Update Success.!' , empty($idhubungan) ? 'Save  Failed.!' :'Update Failed.!' );
            }
            redirect(base_url('cadministrasi/hubungan'));
        }
    }
    
    //*  Konfigurasi*/
    //-------------------------- ^^ Standar
    public function settingkonfigurasi()
    {
        return [    'content_view'      => 'administrasi/v_konfigurasi',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'konfigurasi',
                    'active_menu_level' => ''
               ];
    }
    public function konfigurasi()
    {
	if ($this->pageaccessrightbap->checkAccessRight(V_KONFIGURASI)) //lihat define di atas
        {
            $data                = $this->settingkonfigurasi();
            $data['title_page']  = 'Konfigurasi';
            $data['mode']        = 'view';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['script_js']   = ['js_konfigurasi'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function formreadykonfigurasi()
    {
        echo json_encode($this->db->get_where('konfigurasi',['nama'=>$this->input->post('id')])->row_array());
    }
    public function save_konfigurasi()
    {
        $nama   =  $this->input->post('nama');
        $id     =  $this->input->post('id');
        $nilai  = $this->input->post('nilai');
        $ket    = $this->input->post('keterangan');
        $this->mgenerikbap->setTable('konfigurasi');
        $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate(['nama'=>$nama,'nilai'=>$nilai,'keterangan'=>$ket], $id);
        pesan_success_danger($simpan,'Save Success.!'  , 'Save  Failed.!','js');
    }

    //menampilkan data  konfigurasi
    public function dt_konfigurasi()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_konfigurasi_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->nama;
                    $row[] = $obj->nilai;
                    $row[] = $obj->keterangan;
                    $row[] = '<a data-toggle="tooltip" id="edit" data-original-title="Ubah" class="btn btn-warning btn-xs" alt="'.$obj->nama.'" ><i class="fa fa-pencil"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_konfigurasi(),
            "recordsFiltered" => $this->mdatatable->filter_dt_konfigurasi(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

//     return [    'content_view'      => 'administrasi/v_halaman',
//     'active_menu'       => 'administrasi',
//     'active_sub_menu'   => 'halaman',
//     'active_menu_level' => ''
// ];

    public function settingtandatangandokter()
    {
        return [    'content_view'      => 'administrasi/v_tandatangan_dokter',
                    'active_menu'       => 'administrasi',
                    'active_sub_menu'   => 'tandatangan_dokter',
                    'active_menu_level' => ''
               ];
    }


    public function tandatangandokter()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)) //lihat define di atas
        {
            $data                = $this->settingtandatangandokter();
            $data['data_list']   = $this->db->query("SELECT * FROM person_signature join person_person on person_signature.id_person = person_person.idperson join person_pegawai on person_pegawai.idperson = person_person.idperson")->result_array();
            $data['title_page']  = 'Tanda Tangan Dokter';
            $data['mode']        = 'view';
            $data['script_js']   = ['js_signature','signaturepad'];
            $data['table_title'] = 'List Tanda Tangan Dokter';
            $data['plugins']     = [ PLUG_DATATABLE ];

            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function uploadsignature()
    {
        $data = $this->settingtandatangandokter();
        $data['title_page']  = 'Tambah Tanda Tangan Dokter';
        $data['mode']        = 'upload';
        $data['titlepage']   = 'ADD SIGNATURE';
        $data['script_js']   = ['js_signature','signaturepad'];
        $data['plugins']     = [PLUG_DROPDOWN, PLUG_CHECKBOX];
        $data['data_list']   = $this->db->query("select *from person_person join person_pegawai on person_person.idperson = person_pegawai.idperson WHERE person_pegawai.idprofesi = 1")->result_array();
        $this->load->view('v_index',$data);
    }

    public function simpantandatangan()
    {
        $queryimagename = $this->db->select('*')->where('id_person',$_POST['idperson'])->get('person_signature')->result_array();

        $checkdataifexit = count($queryimagename);
        
        if($checkdataifexit == 1)
        {
            return redirect('/cadministrasi/tandatangandokter');
        }
        else{
        # base64_decode
        $folderPath = "./tanda_tangan/";
        $image_parts = explode(";base64,", $this->input->post('signed'));
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $uniqueid = uniqid();
        $tmpName = $folderPath . uniqid() . '.'.'png';
        file_put_contents($tmpName, $image_base64);
        $newFileName = "/tanda_tangan/signature-".$_POST['idperson'].".png";

        # upload ke nas storage
        $config = $this->ftp_upload();
        $upload = $this->ftp->connect($config);
        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
        $upload = $this->ftp->close();

        #store into database
        $data = ["id_person" => $_POST['idperson'], "signature" => "signature-".$_POST['idperson'].".png"];
        $this->msqlbasic->insert_data('person_signature',$data);

        #delete file tmp
        $deletefiletmp = unlink($tmpName);
        
        return redirect('/cadministrasi/tandatangandokter');
        }

    }

    public function edittandatangan($id)
    {
        $data = $this->settingtandatangandokter();
        $data['title_page']  = 'Ubah Tanda Tangan Dokter';
        $data['mode']        = 'edit';
        $data['title_page']   = 'EDIT SIGNATURE';
        $data['plugins']     = [PLUG_DROPDOWN, PLUG_CHECKBOX];
        $data['script_js']   = ['js_signature','signaturepad'];
        $data['data_list']   = $this->db->query("SELECT * FROM person_signature join person_person on person_signature.id_person = person_person.idperson join person_pegawai on person_pegawai.idperson = person_person.idperson where person_signature.id_signature =".$id)->result_array();
        // $data['data_dokter']   = $this->db->query("select *from person_person join person_pegawai on person_person.idperson = person_pegawai.idperson WHERE person_pegawai.idprofesi = 1")->result_array();
        $this->load->view('v_index',$data);
    }

    public function hapustandatangan($id)
    {
        $queryimagename = $this->db->select('*')->where('id_signature',$id)->get('person_signature')->result_array();
        
        $namagambar = '';
        foreach($queryimagename as $row)
        {
            $namagambar = $row['signature'];
        }
        
        $pathname = "/sirstql/tanda_tangan/";
        $urlnas = $pathname.$namagambar;

        //hapus file di NAS
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect( $config );
        $delete = $this->ftp->delete_file($urlnas);
        $delete = $this->ftp->close();

        $this->db->where('id_signature',$id);
        $this->db->delete('person_signature');

        return redirect('/cadministrasi/tandatangandokter');
    }

    public function storeedittandatangan()
    {
        #query get nama image lama
        $queryimagename = $this->db->select('*')->where('id_signature',$_POST['idsignature'])->get('person_signature')->result_array();
        
        $namagambar = '';
        $idsignature = '';
        foreach($queryimagename as $row)
        {
            $namagambar = $row['signature'];
            $idsignature = $row['id_signature'];
        }

        #hapus file lama di nas storage
        $pathname = "/sirstql/tanda_tangan/";
        $urlnas = $pathname.$namagambar;      
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect( $config );
        $delete = $this->ftp->delete_file($urlnas);
        $delete = $this->ftp->close();
        
         # base64_decode
         $folderPath = "./tanda_tangan/";
         $image_parts = explode(";base64,", $this->input->post('signed'));
         $image_type_aux = explode("image/", $image_parts[0]);
         $image_type = $image_type_aux[1];
         $image_base64 = base64_decode($image_parts[1]);
         $uniqueid = uniqid();
         $tmpName = $folderPath . uniqid() . '.'.'png';
         file_put_contents($tmpName, $image_base64);
         $newFileName = "/tanda_tangan/signature-".$idsignature.".png";

        # upload ke nas storage
         $upload = $this->ftp->connect($config);
         $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
         $upload = $this->ftp->close();

         #update data gambar baru ke database
         $query = $this->db->where('id_signature', $_POST['idsignature'])->update('person_signature',["signature" => "signature-".$idsignature.".png"]);
         
         #delete file tmp
         $deletefiletmp = unlink($tmpName);
         return redirect('/cadministrasi/tandatangandokter');
    }

    
    //*  /Konfigurasi*/
}
/* End of file ${TM_FILENAME:${1/(.+)/Cadministrasi.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cadministrasi/:application/controllers/${1/(.+)/Cadministrasi.php/}} */






