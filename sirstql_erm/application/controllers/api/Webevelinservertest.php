<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Webevelinservertest extends REST_Controller {
    private $Xconsid;
    private $Xconssecret;
    private $Xtimestamp;
    private $Xjeniskoneksi;
    private $Xsignature;
    
    function __construct()
    {
        parent::__construct();
       
    }

    private function decodebase64()
    {
        $decode = base64_encode(hash_hmac('sha256',$this->Xconsid.'&'.$this->Xtimestamp,$this->Xconssecret, true));
        return ($decode==$this->Xsignature) ? true : false ;
    }

    private function cek_signature()
    {
        $headers = apache_request_headers();
        $this->Xconsid    = $headers['X-Cons-Id'];
        $this->Xtimestamp = $headers['X-Timestamp'];
        $this->Xsignature = $headers['X-Signature'];
        $this->Xjeniskoneksi = $headers['X-Jeniskoneksi'];

        if($this->Xsignature)
        {
            switch ($this->Xjeniskoneksi) {
                case 'EVELINQLYK': $this->Xconssecret='evelinQL'; break;
            }
            return ($this->decodebase64()) ? true : false ;
        }
        return false;
        
    }
    /**
     * Get Post 
     * @param type $id
     * @return type
     */
    private function get_post($id)
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }
    
    public function vclaimlist_post()
    {
        if($this->cek_signature()){  

            $tgl_periksa = sql_clean($this->get_post('tglperiksa'));
            
            $jns_pemeriksaan = empty($this->get_post('jnslayanan')) ? 'rajal' : sql_clean($this->get_post('jnslayanan'));

            
            $query = $this->db->select("ppd.jenisrujukan,ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
            $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
            $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
            $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
            $query = $this->db->where('ppd.carabayar != "mandiri" ');
            $query = $this->db->where('ppd.carabayar != "asuransi lain" ');
            $query = $this->db->where('ppd.idstatuskeluar != "3" ');
            
            $query = $this->db->where('date(ppd.waktuperiksa) = "'.$tgl_periksa.'"');
            
            $query = $this->db->where('ppd.jenispemeriksaan = "'.$jns_pemeriksaan.'" ');

            $query = $this->db->group_by('ppd.idpendaftaran');
            $query = $this->db->get('person_pendaftaran ppd');

            $response = $query->result();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }
    }

    public function testnew_vclaimlistselesai_post()
    {
        if($this->cek_signature()){  

            // $tgl_periksa = sql_clean($this->get_post('tglperiksa'));
            $tgl_periksa = '2023-03-30';
            
            $jns_pemeriksaan = empty($this->get_post('jnslayanan')) ? 'rajal' : sql_clean($this->get_post('jnslayanan'));
                    
            $query = $this->db->select("ppd.jenisrujukan,ppd.idpendaftaran,rp.idpemeriksaan,ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
            $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
            $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
            $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
            // $query = $this->db->where('ppd.carabayar != "mandiri" ');
            // $query = $this->db->where('ppd.carabayar != "asuransi lain" ');

            $query = $this->db->group_start();
                $query = $this->db->where('ppd.carabayar','jknpbi');
                $query = $this->db->or_where('ppd.carabayar','jknnonpbi');
            $query = $this->db->group_end();
            
            $query = $this->db->where('ppd.idstatuskeluar = "2" ');
            
            $query = $this->db->where('date(ppd.waktuperiksa) = "'.$tgl_periksa.'"');
            
            $query = $this->db->where('ppd.jenispemeriksaan = "'.$jns_pemeriksaan.'" ');

            $query = $this->db->group_by('ppd.idpendaftaran');
            $query = $this->db->get('person_pendaftaran ppd');

            // $response = $query->result();
            $response = $this->db->last_query();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }
    }

    /**
     * new list selesai
     */
    public function vclaimlistselesai_post()
    {
        if($this->cek_signature()){  

            $tgl_periksa = sql_clean($this->get_post('tglperiksa'));
            
            $jns_pemeriksaan = empty($this->get_post('jnslayanan')) ? 'rajal' : sql_clean($this->get_post('jnslayanan'));
                    
            $query = $this->db->select("ppd.jenisrujukan,ru.idunit,ppd.idstatuskeluar,ppd.norm,ru.idunit,ppas.nojkn, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
            $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
            $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
            $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
            
            // $query = $this->db->where('ppd.carabayar != "mandiri" ');
            // $query = $this->db->where('ppd.carabayar != "asuransi lain" ');

            $query = $this->db->group_start();
                $query = $this->db->where('ppd.carabayar','jknpbi');
                $query = $this->db->or_where('ppd.carabayar','jknnonpbi');
            $query = $this->db->group_end();
            
            $query = $this->db->where('ppd.idstatuskeluar = "2" ');
            
            $query = $this->db->where('date(ppd.waktuperiksa) = "'.$tgl_periksa.'"');
            
            $query = $this->db->where('ppd.jenispemeriksaan = "'.$jns_pemeriksaan.'" ');

            $query = $this->db->group_by('ppd.idpendaftaran');
            $query = $this->db->get('person_pendaftaran ppd');

            $response = $query->result();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }
    }


    /**new SEP */
    public function vclaimsep_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            
            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            # CEK SEP - KOSONG
            $query = $this->get_row_cek_sep($norm,$tglperiksa,$idunit);
            
            if( !empty($query->nosep) ){
                $nosep = str_replace(" ","",$query->nosep);
            }else{
                $nosep = '';
            }

            if( empty($nosep) ){
                $data_api = ['metaData'=>['code'=>404,'message'=>'No Sep Belum Di input']];
            }else{
                $api_sep   = $this->bpjsbap->get_vclaim_sep($nosep);
                $nokartu   = $api_sep['response']->peserta->noKartu;
                if( $api_sep['metaData']->code == '200' ){
                    $norujukan      = $api_sep['response']->noRujukan; 
                    $api_rujukan    = $this->bpjsbap->get_vclaim_rujukan($norujukan);
                    $cek_coderujukan = (object) $api_rujukan['metaData'];

                    
                    
                    if( $cek_coderujukan->code == '200' ){

                        $data_api       = ['sep'=>$api_sep,'rujukan'=>$api_rujukan];
                      
                        $nosurat        = $api_sep['response']->kontrol->noSurat; 

                        if( empty($nosurat) ){
                            $bulan = format_date($tglperiksa,"m");
                            $tahun = format_date($tglperiksa,"Y");
                            $rekons = $this->bpjsbap->get_vclaim_rencanakontrol_bynokartu($bulan,$tahun,$nokartu,2);
                            
                            $nosuratkontrol = '';
                            if( $rekons['metaData']->code == '200' ){
                                foreach( $rekons['response']->list as $row_rekon ){
                                    if($row_rekon->tglRencanaKontrol == $tglperiksa && $row_rekon->jnsPelayanan == 'Rawat Jalan'){
                                        $nosuratkontrol = $row_rekon->noSuratKontrol;
                                    }
                                }
                            }
                                
                            if(!empty($nosuratkontrol)){
                                $data_api['rekon'] = $this->bpjsbap->get_vclaim_rencanakontrol($nosuratkontrol);
                            }
    
                        }else{
                            $rekon = $this->bpjsbap->get_vclaim_rencanakontrol($nosurat);
                            if($rekon['metaData']->code == '200'){
                                $data_api['rekon'] = $rekon;
                            }
                        }
                        

                    }else{
                        $rekon_bysep     = $this->bpjsbap->get_vclaim_rencanakontrol_bynosep($nosep);
                        $rujukan_bykartu = $this->bpjsbap->get_vclaim_rujukan_bykartu($nokartu);
                        $data_api        = ['sep'=>$api_sep,'rujukan'=>$rujukan_bykartu,'rekon'=>$rekon_bysep];
                    }
                }else{
                    $data_api = $api_sep;
                }
                
            }

            $response =  $data_api;
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);


        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    public function vclaimresume_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            # CEK SEP - KOSONG
            $query_sep = $this->get_row_cek_sep($norm,$tglperiksa,$idunit);
            
            $idpendaftaran          = $query_sep->idpendaftaran;
            $jenisrujukan           = $query_sep->jenisrujukan;

            #cek catatan rujuk tidak kembali
            $found_rujuk = $this->cek_catatan_rujukan($idpendaftaran);

            if( !empty( $found_rujuk ) ){
                $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran,'idpemeriksaan'=>$found_rujuk[0]['idpemeriksaan']])->row_array();
            }else{
                $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            }
    
            
            $vitalsign      = $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $tindakan       = $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $laboratorium   = $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $radiologi      = $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $obat           = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
            $grup           = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  
                                                FROM rs_barangpemeriksaan b 
                                                WHERE b.idpendaftaran = '".$idpendaftaran."' GROUP by grup")->result();
            $diagnosa       =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
            
            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $sip = $this->db->select('sip')->from('person_pegawai')->where('idpegawai',$identitas['idpegawai'])->get()->row_array();
            $identitas['sip_dokter'] = $sip['sip'];
    
            $response = [
                'datapendaftaran' => $query_sep,
                'identitas'=>$identitas,
                'vitalsign'=>$vitalsign,
                'tindakan'=>$tindakan,
                'laboratorium'=>$laboratorium,
                'radiologi'=>$radiologi,
                'obat'=>$obat,
                'diagnosa'=>$diagnosa,
                'ketradiologi'=>$ketradiologi,
                'grup'=>$grup,
                'jenisrujukan'=>$jenisrujukan
            ];

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{

            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }

    }

    #cek catatan rujukan
    public function cek_catatan_rujukan($idpendaftaran){


        /** START - CEK CATATAN RUJUKAN */
        $cek_catatan_rujuk =  $this->db->query("SELECT * 
                                                FROM rs_pemeriksaan 
                                                WHERE idpendaftaran = '".$idpendaftaran."'
                                           ")->result();
        $found_rujuk = [];
        foreach( $cek_catatan_rujuk as $rujuk ){
            if( $rujuk->catatanrujuk === 'rujuk internal tidak kembali'){
                $found_rujuk[] = [
                                    'idpendaftaran'=>$rujuk->idpendaftaran,
                                    'idpemeriksaan'=>$rujuk->idpemeriksaan
                                ]; 
            }
        }

        return $found_rujuk;

        /** END - CEK CATATAN RUJUKAN */
    }

    # Cek Query SEP CEK
    public function get_row_cek_sep($norm,$tglperiksa,$idunit=""){

        //get result
        $query          = $this->mevelin->get_pendaftaran_bynorm_periksa_list($norm,$tglperiksa,$idunit);
            
        $nosep_pendaftaran = "";
        foreach( $query as $row_pendaftaran_norm ){
            $row_sep = $row_pendaftaran_norm->nosep;
            if( strlen( str_replace(' ','',$row_sep) ) === 19 ){
                $nosep_pendaftaran = $row_sep; 
            }
        }

        if( strlen( str_replace(' ','',$nosep_pendaftaran) ) === 19 ){
            $query = $this->mevelin->get_pendaftaran_by_sep($nosep_pendaftaran,$idunit);
        }else{
            // get row
            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
        }
        // end Query SEP CEK

        return $query;
    }


    public function vclaimresumesuratkontrol_post(){

        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm               = sql_clean($this->get_post('norm'));
            $tglperiksasekarang = sql_clean($this->get_post('tglperiksa'));
            
            // $query_sekarang     = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksasekarang);
            // $jenisrujukan       = $query_sekarang->jenisrujukan;
            
            # CEK SEP - KOSONG
            $query_sekarang = $this->get_row_cek_sep($norm,$tglperiksasekarang);
            $jenisrujukan   = $query_sekarang->jenisrujukan;

            $this->db->select('*');
            $this->db->from('person_pendaftaran');
            $this->db->where('norm',$norm);
            $this->db->where('carabayar !=', 'mandiri');
            $this->db->where('idunit',$query_sekarang->idunit);
            $this->db->where('idstatuskeluar',2);
            $this->db->where('statustagihan !=','terbuka');
            $this->db->where('date(waktuperiksa) <=',$tglperiksasekarang);
            $this->db->order_by('waktuperiksa', 'DESC');
            $this->db->limit(2);

            $cek_riwayat_pemeriksaan = $this->db->get()->result();

            $riwayat_terakhir = $cek_riwayat_pemeriksaan[1];
            $tglperiksa       = format_date($riwayat_terakhir->waktuperiksa,"Y-m-d"); 

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa);
            # CEK SEP - KOSONG
            $query          = $this->get_row_cek_sep($norm,$tglperiksa);
            
            $idpendaftaran  = $query->idpendaftaran;

            $identitas      = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $vitalsign      = $this->mviewql->viewhasilpemeriksaan(1,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $tindakan       = $this->mviewql->viewhasilpemeriksaan(3,$identitas['idunit'],$idpendaftaran,'verifikasi');            
            $laboratorium   = $this->mviewql->viewhasilpemeriksaan(4,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $radiologi      = $this->mviewql->viewhasilpemeriksaan(5,$identitas['idunit'],$idpendaftaran,'verifikasi');
            $obat           = $this->mkombin->pemeriksaan_listbhp($idpendaftaran,'verifikasi');
            $grup           = $this->db->query("SELECT count(b.grup) as jumlahgrup, b.grup  
                                                FROM rs_barangpemeriksaan b 
                                                WHERE b.idpendaftaran = '".$idpendaftaran."' GROUP by grup")->result();
            $diagnosa       =  $this->mviewql->viewhasilpemeriksaan(2,$identitas['idunit'],$idpendaftaran,'verifikasi');
            
            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }
    
            $sip = $this->db->select('sip')->from('person_pegawai')->where('idpegawai',$identitas['idpegawai'])->get()->row_array();
            $identitas['sip_dokter'] = $sip['sip'];
            
            $response = [
                'datapendaftaran' => $query,
                'identitas'=>$identitas,
                'vitalsign'=>$vitalsign,
                'tindakan'=>$tindakan,
                'laboratorium'=>$laboratorium,
                'radiologi'=>$radiologi,
                'obat'=>$obat,
                'diagnosa'=>$diagnosa,
                'ketradiologi'=>$ketradiologi,
                'grup'=>$grup,
                'jenisrujukan'=>$jenisrujukan
            ];

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{

            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        
        }

    }




    public function vclaimrencanakontrol_post()
    {
        if( $this->cek_signature() ){
            $this->load->model('mevelin');
            
            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            
            # CEK SEP - KOSONG
            $query = $this->get_row_cek_sep($norm,$tglperiksa,$idunit);

            // $nosep = str_replace(" ","",$query->nosep);
            if( !empty($query->nosep) ){
                $nosep = str_replace(" ","",$query->nosep);
            }else{
                $nosep = '';
            }

            if( empty($nosep) ){
                $data_api = ['metaData'=>['code'=>404,'message'=>'No Sep Belum Di input']];
            }else{
                $api_sep   = $this->bpjsbap->get_vclaim_sep($nosep);
                if( $api_sep['metaData']->code == '200' ){
                    $nosurat        = $api_sep['response']->kontrol->noSurat; 
                    $api_rencanakontrol  = $this->bpjsbap->get_vclaim_rencanakontrol($nosurat);
                    
                    $data_api       = $api_rencanakontrol ;
                }else{
                    $data_api = $api_sep;
                }
                
            }

            $this->response(["metadata" =>  [ 'response'=>$data_api, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function vclaimradiologi_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            
            # CEK SEP - KOSONG
            $query = $this->get_row_cek_sep($norm,$tglperiksa,$idunit);

            $idpendaftaran  = $query->idpendaftaran;
            $idunit         = $query->idunit;

            $data = [];
            $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $data['elektromedik']     = $this->mviewql->viewhasilpemeriksaan(5,$idunit,$idpendaftaran,'verifikasi');
            $data['diagnosa']         = $this->mviewql->viewhasilpemeriksaan(2,$idunit,$idpendaftaran,'verifikasi');
            $data['dokterpenilai']    = $this->mkombin->getKonfigurasi('drpenilairo');
            $data['sipdokterpenilai'] = $this->mkombin->getKonfigurasi('sippenilairo');
            $data['header']           = $this->mkombin->getKonfigurasi('headerro');

            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $data['ketradiologi'] = $ketradiologi;
            
            $this->response(["metadata" =>  [ 'response'=>$data, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

    public function testvclaimradiologi_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $this->load->model('mviewql');
            $this->load->model('mkombin');

            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);

            $idpendaftaran  = $query->idpendaftaran;
            $idunit         = $query->idunit;

            /**
             * cek nosep kosong
             */
            $this->db->select('idpendaftaran'); 
            $this->db->from('person_pendaftaran');
            $this->db->where('idunit',$idunit);
            $this->db->where('norm',$norm);
            $this->db->where('date(waktuperiksa)',$tglperiksa);
            $this->db->group_start();
                $this->db->where('nosep =','');
                $this->db->or_where('nosep','0');
            $this->db->group_end();

            $cek_pendafataran = $this->db->get()->row();
            
            if(!empty( $cek_pendafataran )){
                $idpendaftaran = $cek_pendafataran->idpendaftaran;
            }


            $data = [];
            $data['identitas']        = $this->db->get_where('vrs_identitas_periksa_pasien',['idpendaftaran'=>$idpendaftaran])->row_array();
            $data['elektromedik']     = $this->mviewql->viewhasilpemeriksaan(5,$idunit,$idpendaftaran,'verifikasi');
            $data['diagnosa']         = $this->mviewql->viewhasilpemeriksaan(2,$idunit,$idpendaftaran,'verifikasi');
            $data['dokterpenilai']    = $this->mkombin->getKonfigurasi('drpenilairo');
            $data['sipdokterpenilai'] = $this->mkombin->getKonfigurasi('sippenilairo');
            $data['header']           = $this->mkombin->getKonfigurasi('headerro');

            $getket         = $this->db->query("select if(p.isverifklaim=1,p.keteranganradiologi_verifklaim,p.keteranganradiologi) keteranganradiologi, if(p.isverifklaim=1,p.saranradiologi_verifklaim,p.saranradiologi) saranradiologi, if(p.isverifklaim=1,r.keterangan_verifklaim, r.keterangan) keterangan, if(p.isverifklaim=1,r.anamnesa_verifklaim,r.anamnesa ) anamnesa , if(p.isverifklaim=1,r.diagnosa_verifklaim, r.diagnosa) diagnosa  from person_pendaftaran p, rs_pemeriksaan r where p.idpendaftaran='".$idpendaftaran."' and r.idpendaftaran=p.idpendaftaran")->result();
            $ketradiologi   = [];
            
            if(empty(!$getket)){

                $kkr='';$ks='';$kk='';$ka='';$kd='';
                foreach ($getket as $obj) {
                    $kkr = $obj->keteranganradiologi;
                    $ks  = $obj->saranradiologi;
                    $kk .= $obj->keterangan;
                    $ka .= $obj->anamnesa;
                    $kd .= $obj->diagnosa; 
                }

                $ketradiologi = ['keteranganradiologi'=>$kkr,'saranradiologi'=>$ks,'keterangan'=>$kk,'anamnesa'=>$ka,'diagnosa'=>$kd];
            
            }

            $data['ketradiologi'] = $ketradiologi;
            
            $this->response(["metadata" =>  [ 'response'=>$data, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }

    }


    public function vclaimnota_post()
    {
        if( $this->cek_signature() ){

            $this->load->model('mevelin');

            $norm       = sql_clean($this->get_post('norm'));
            $tglperiksa = sql_clean($this->get_post('tglperiksa'));
            $idunit     = sql_clean($this->get_post('idunit'));

            // $query = $this->mevelin->get_pendaftaran_bynorm_periksa($norm,$tglperiksa,$idunit);
            
            # CEK SEP - KOSONG
            $query = $this->get_row_cek_sep($norm,$tglperiksa,$idunit);

            $idpendaftaran     = $query->idpendaftaran;


            // $jenispemeriksaan  = $query->jenispemeriksaan;
            $jenispemeriksaan  = 'rajal';

            // if( $jenispemeriksaan == 'rajalnap' ){
                // $query_nota = $this->mevelin->vlcaim_cetakversiranap($idpendaftaran,$jenispemeriksaan);
            // }else{
            $query_nota = $this->mevelin->vclaim_cetaklangsung($idpendaftaran,$jenispemeriksaan);
            // }
            
            $response = $query_nota;

            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function vclaim_tanggalrekon_post(){
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            $nomerrekon   = sql_clean($this->get_post('nomerrekon'));
            
            // $query_unit = $this->mevelin->get_poli();
            
            $getdata_tgl = $this->mevelin->get_tanggalrekon($nomerrekon);

            $response   = $getdata_tgl;
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    public function vclaimdatapoli_post(){
        if( $this->cek_signature() ){

            $this->load->model('mevelin');
            
            $query_unit = $this->mevelin->get_poli();

            $response   = $query_unit;
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        
        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    
    
}