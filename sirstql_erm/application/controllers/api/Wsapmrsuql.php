<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wsapmrsuql extends REST_Controller {
    private $Xconsid;
    private $Xconssecret;
    private $Xtimestamp;
    private $Xjeniskoneksi;
    private $Xsignature;
    
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
//        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
//        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
    //mahmud, clear
    private function decodebase64()
    {
        $decode = base64_encode(hash_hmac('sha256',$this->Xconsid.'&'.$this->Xtimestamp,$this->Xconssecret, true));
        return ($decode==$this->Xsignature) ? true : false ;
    }
    /**
     * Cek Signature
     * @return boolean
     * mahmud, clear
     */
    private function cek_signature()
    {
        $headers = apache_request_headers();
        if(file_exists("./QLKP.php")){
            // For RS QLKP
            $this->Xconsid    = $headers['X-cons-id'];
            $this->Xtimestamp = $headers['X-timestamp'];
            $this->Xsignature = $headers['X-signature'];
            $this->Xjeniskoneksi = $headers['X-jeniskoneksi'];
        } else {
            // For RS QL
            $this->Xconsid    = $headers['X-Cons-Id'];
            $this->Xtimestamp = $headers['X-Timestamp'];
            $this->Xsignature = $headers['X-Signature'];
            $this->Xjeniskoneksi = $headers['X-Jeniskoneksi'];
        }

        if($this->Xsignature)
        {
            switch ($this->Xjeniskoneksi) {
                case 'APMQLYK': $this->Xconssecret='aPMQLralan'; break;
            }
            return ($this->decodebase64()) ? true : false ;
        }
        return false;
        
    }
    /**
     * Get Post 
     * @param type $id
     * @return type
     */
    private function get_post($id)
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }
    
    //-- cek login anjungan pasien mandiri
    public function ceklogin_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean(trim($this->get_post('norm')));
            $response = $this->db
                    ->get_where('apm_login',['norm'=>$norm])
                    ->row_array();
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //-- get data profil pasien
    //mahmud, clear
    # [+] Update by Ikhsan MS    
    public function profil_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean(trim($this->get_post('norm')));
            
            # Pasien Blacklist
            $sql                = "SELECT * FROM person_pasien_blacklistunit WHERE norm='".$norm."' ";
            $query_blacklist    = $this->db->query($sql);
            $row_blacklist      = $query_blacklist->row_array();

            $response = $this->db
                    ->join('person_person pp','pp.idperson = ppas.idperson')
                    ->get_where('person_pasien ppas',['norm'=>$norm])
                    ->row_array();
            
            $response['blacklist'] = ( $row_blacklist > 0 ) ? true : false ;             
            $response['blacklist_data'] = $row_blacklist;             
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //-- get data pemeriksaan pasien
    //mahmud, clear    
    public function pemeriksaansaya_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean(trim($this->get_post('norm')));
            $tahun = sql_clean(trim($this->get_post('tahun')));
            
            $tahun = ((empty($tahun)) ? date('Y') : $tahun );
            
            $response = $this->db->select("date(waktuperiksa) as tanggalperiksa, namaloket(rp.idloket) as namaloket, namaunit(rp.idunit) as namaunit,namadokter(rp.idpegawaidokter) as namadokter, if(rp.status ='selesai','',(select concat(rj.tanggal, ' - ',rj.jamakhir) from rs_jadwal rj where rj.idjadwal = rp.idjadwal)) as praktek, rp.noantrian, rp.status");
            $response = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null');
            $response = $this->db->where('pp.norm',$norm);
            $response = $this->db->where('pp.tahunperiksa',$tahun);
            $response = $this->db->order_by('pp.idpendaftaran', 'DESC');
            $response = $this->db->get('person_pendaftaran pp')->result_array();
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //-- cek status pasien apakah masih dalam pemeriksaan
    public  function isperiksa_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean(trim($this->get_post('norm')));
            $cek = $this->db->query("select pp.carabayar,rp.idpemeriksaan,rp.idloket,rp.noantrian,pp.idpendaftaran, date(waktuperiksa) as tanggal, namaunit(pp.idunit) as namaunit , rp.status
            from person_pendaftaran pp
            join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran  and rp.idpemeriksaansebelum is null
            WHERE pp.norm = '".$norm."' and (idstatuskeluar = 0 or idstatuskeluar = 1 ) and statustagihan != 'lunas' and rp.status != 'batal'");
            if($cek->num_rows() > 0)
            {
                $dt = $cek->row_array();

                # programmer ganteng -IMS
                $sql = " SELECT idperson FROM  person_pasien WHERE norm='".$norm."'";
                $get_data_person_pasien = $this->db->query($sql);
                $row_person = $get_data_person_pasien->row();

                $idperson = $row_person->idperson;
                $idpemeriksaan = $dt['idpemeriksaan'];
                $ap = $this->db->query("SELECT namadokter(b.idpegawaidokter) as dokter,b.idloket, b.idjadwal,b.noantrian, DATE_FORMAT(b.waktu, '%d/%m/%Y') waktu, b.waktu as tglperiksa ,c.idunit, c.namaloket, d.namalengkap,usia(d.tanggallahir) as usia, date_format(d.tanggallahir,'%d %b %Y') as tanggallahir, d.alamat, b.norm FROM rs_pemeriksaan b, antrian_loket c, person_person d WHERE b.idpemeriksaan='".$idpemeriksaan."' and c.idloket=b.idloket and d.idperson='".$idperson."'")->row_array();
                
                $data_antrian_dokter = $this->db->query("SELECT namadokter(rj.idpegawaidokter) as namadokter,rp.status ,rj.tanggal, aa.idantrian, aa.no, aa.kodebooking, al.namaloket, al.lamapelayanan, rj.quota_jkn, rj.quota_nonjkn,"
                    . " (select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal) as totalantrean,"
                    . " (select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal) as totalantrean,"
                    . " (select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ) as antrianjkn "
                    . "FROM rs_pemeriksaan rp, rs_jadwal rj, antrian_antrian aa, antrian_loket al "
                    . "where rp.idpemeriksaan='".$idpemeriksaan."' and rj.idjadwal = rp.idjadwal and aa.idpemeriksaan='".$idpemeriksaan."' and al.idloket=aa.idloket")
                    ->row_array();

                $count_batal = $this->db->query("SELECT COUNT(rp.norm) as count_batalperiksa
                                                FROM rs_pemeriksaan rp 
                                                LEFT JOIN person_pendaftaran pp ON pp.idpendaftaran = rp.idpendaftaran
                                                WHERE rp.norm='".$norm."' AND date(rp.waktu) = '".format_date($ap['tglperiksa'])."' AND pp.caradaftar = 'anjunganonline' AND rp.status='batal'")->row_array();
                
                $ap['data_antrian_dokter']  = $data_antrian_dokter;
                $ap['idpemeriksaan']        = $idpemeriksaan;
                $ap['jumlahdibatalkan']     = $count_batal['count_batalperiksa'];
                // $ap['data_riwayat_pasien'] = $this->get_cek_riwayat_pasien($norm,$ap['idunit']);

                # end programmer ganteng -IMS

                $notif_pasien = ( $dt['carabayar'] == 'jknnonpbi' || $dt['carabayar'] == 'jknpbi' ) ? 'Silahkan Langsung Menuju Bagian Pendaftaran dengan Membawa Surat Kontrol/Rujukan dan Kartu BPJS untuk Konfirmasi data dan Cetak SEP serta Mengambil Antrian BPJS Rujukan untuk Rujukan Baru dan BPJS Surat Kontrol untuk pasien kontrol' : 'Silahkan Langsung Menuju Mesin Cetak Anjungan Mandiri di Samping Farmasi';
                $this->response(["metadata" =>  [ 'response'=>'isperiksa', 'code' => 200,'data'=>$ap,'message' => 'Tanggal Periksa Anda : '.$dt['tanggal'].' Poli '.ucwords(strtolower($dt['namaunit'])).'. <br> '.$notif_pasien]], REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(["metadata" =>  [ 'response'=>'tidakperiksa','code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
            }
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Ikhsan
     * Batal Periksa
     */
    public function batalperiksa_post()
    {
        if($this->cek_signature())
        {
            $this->load->model('mgenerikbap');

            $id         = sql_clean(trim($this->get_post('idperiksa')));
            $status     = sql_clean(trim($this->get_post('status')));

            $arrMsg = $this->ql->rs_pemeriksaan_insert_or_update($id, ['status' => $status]);
            $data   = $this->db->query("select idpendaftaran, norm from rs_pemeriksaan where idpemeriksaan='".$id."'")->row_array();

            $datal = [
                'url'     => 'https://app.queenlatifa.co.id/',
                'log'     => 'user: Pembatalan Mandiri Via WEB APM;status:' . $status . ';idpemeriksaan:' . $id . ';idpendaftaran:' . $data['idpendaftaran'] . ';norm:' . $data['norm'],
                'expired' => date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];

            $this->mgenerikbap->setTable('login_log');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);

            $this->response(["metadata" =>  [ 'response'=>'success', 'code' => 200,'message' => 'Anda Berhasil Membatalkan Pemeriksaan']], REST_Controller::HTTP_OK);

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function get_cek_riwayat_pasien($norm,$idunit)
    {        
        $response = []; 
        //cari tanggal pendaftaran pasien
        $sql = "select rp.idpemeriksaan,rp.idpendaftaran, namadokter(rp.idpegawaidokter) as dokter_dpjp, namadokter(rp.iddokterpemeriksa) as dokter_pemeriksa, rp.idunit, rp.status, rp.rekomendasi, ru.namaunit
        from rs_pemeriksaan rp
        join rs_unit ru on ru.idunit = rp.idunit
        WHERE norm='".$norm."' AND ru.idunit='".$idunit."' AND rp.status='selesai' 
        ORDER BY rp.idpemeriksaan DESC LIMIT 1";
        
        $query = $this->db->query($sql);

        if($query->num_rows() > 0){
            //riwayat pasien rawat jalan
            $response['ralan_pemeriksaan'] = $query->row_array();
            $idpendaftaran = $response['ralan_pemeriksaan']['idpendaftaran'];
            
            //cari tanggal pendaftaran pasien
            $sql_person_pendaftaran = "SELECT idpendaftaran, date(waktuperiksa) as waktuperiksa
            FROM `person_pendaftaran` 
            WHERE idpendaftaran='".$idpendaftaran."' ORDER by idpendaftaran desc limit 1";
            
            $query_person_pendaftaran = $this->db->query($sql_person_pendaftaran);

            $response['pendaftaran'] = $query_person_pendaftaran->row_array();

        }else{
            $response['status']  = 'info';
            $response['message'] = 'Riwayat Tidak Ditemukan';
        }

        return $response;
    }

    # ikhsan
    # create  function total antrian - [ 10 juli 2022 ]
    public function get_antrian_by_dokter()
    {
        //code here
    } 

    # ikhsan
    # create function asalrujukan API 
    public function asalrujukan_post()
    {
        if($this->cek_signature()){
            
            $sql    = $this->db->select("idklinik as id, concat(ifnull(kodeklinik,''),' ', ifnull(namaklinik,'') ) as text");
            $sql    = $this->db->get("bpjs_klinikperujuk")->result_array();
            
            if( $sql > 0 ){
                $this->response(["metadata" =>  [ 'response'=>'success', 'code' => 200,'data'=>$sql,'message' => 'success']], REST_Controller::HTTP_OK);
            }else{
                $this->response(["metadata" =>  [ 'response'=>'failed', 'code' => 200,'message' => 'failed']], REST_Controller::HTTP_OK);
            }

        }else{
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //-- get data poli
    public function poli_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean(trim($this->get_post('norm')));

            $cek = $this->db->query("SELECT rs.status,rs.idpendaftaran,rs.idpemeriksaan,pp.idunit,pp.idpendaftaran, date(pp.waktuperiksa) as tanggal, namaunit(pp.idunit) as namaunit 
                                    FROM person_pendaftaran pp 
                                    LEFT JOIN rs_pemeriksaan rs ON rs.idpendaftaran = pp.idpendaftaran 
                                    WHERE pp.norm = '10146389' and pp.statustagihan = 'lunas' AND rs.status != 'batal' 
                                    ORDER BY pp.idpendaftaran DESC LIMIT 1");
            
            $dt = $cek->row_array();

            $response = $this->db->select('idunit, namaunit')->get_where('rs_unit',['tipeunit !='=>'gudang','tipeunit !='=>'farmasi'])->result_array();
            $response['riwayat'] = $this->get_cek_riwayat_pasien($norm,$dt['idunit']);
            
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
     //-- get data carabayar
    public function carabayar_post()
    {
        if($this->cek_signature())
        {
            $response = $this->db->get('rs_carabayar')->result_array();
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //get data jadwal poli
    public  function carijadwalpoli_post()
    {     
        if($this->cek_signature())
        {
            $idunit     = sql_clean(trim($this->get_post('idunit')));
            $tanggal    = sql_clean(trim($this->get_post('tanggal')));

            $response = $this->db->select('rj.idjadwal, namadokter(rj.idpegawaidokter) as namadokter, namaloket(rj.idloket) as namaloket, time(rj.tanggal) as jammulai, rj.jamakhir,(select COUNT(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal AND rp.status!="batal") as totalantrean, rj.quota')
                    ->get_where('rs_jadwal rj',['rj.idunit'=>$idunit,'date(rj.tanggal)'=>$tanggal])->result_array();

            $this->response(["metadata" =>  [ 'response'=>$response,'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);           
            
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    //-- save pendaftaran Pasien Rawat Jalan
    //mahmud, clear
    public function savependaftaranralan_post()
    {
        if($this->cek_signature())
        {
            $norm = sql_clean($this->get_post('norm'));
            $idunit = sql_clean($this->get_post('idunit'));
            $tanggal = sql_clean($this->get_post('tanggal'));
            $idjadwal = sql_clean($this->get_post('idjadwal'));
            $carabayar = sql_clean($this->get_post('carabayar'));

             # var ikhsan 
             $norujukan     = sql_clean($this->get_post('nomorrujukan'));
             $nomorbpjs     = sql_clean($this->get_post('nomorbpjs'));
             $nomorwa       = sql_clean($this->get_post('nomorwa'));              
             $jenisrujukan  = $this->get_post('jenisrujukan');              
             $tipesurat     = $this->get_post('tipesurat');              
             $idasalrujukan   = $this->get_post('idasalrujukan');              
             # end var ikhsan
            
            $dp['norm'] = $norm;
            $dp['idunit'] = $idunit;
            $dp['waktuperiksa'] = ql_formatdatetime($tanggal);
            $dp['waktu'] = date('Y-m-d');
            $dp['carabayar'] = $carabayar;
            $dp['idkelas'] = 1;
            $dp['jenispemeriksaan'] = 'rajal';
            $dp['caradaftar'] = 'anjunganonline';
            
            $save_daftar = $this->db->insert('person_pendaftaran',$dp);
            if($save_daftar)
            {
                $idpendaftaran = $this->db->insert_id();
                $callid = generaterandom(8);
                
                //Buat No Pesan
                $akronimunit = $this->db->select("idloket")->get_where('rs_unit',['idunit'=>$idunit])->row_array();
                $expired = date('Y-m-d', strtotime($tanggal));
                $id 	 = date('Ymd',strtotime($tanggal)).$akronimunit['idloket'];
                
                //create nopesan
                $dataha = ['id'=>$id,'number'=>null,'callid'=>$callid,'expired'=>$expired];
                $save   = $this->db->insert('helper_autonumber',$dataha);
                if($save)
                {
                    //siapkan data no antrian
                    $nopesan = $this->db->select('number')->get_where('helper_autonumber',['callid'=>$callid])->row_array(); 
                    //update pendaftaran, set nopesan
                    $save = $this->db->update('person_pendaftaran',['nopesan'=>$nopesan['number']],['idpendaftaran'=>$idpendaftaran]);
                    if($save)
                    {              
                        $waktuperiksa = date('Y-m-d H:i:s',strtotime($tanggal));
                        $dr =  [
                            'idpendaftaran'=> $idpendaftaran,
                            'idjadwal'  => $idjadwal,
                            'nopesan'   => $nopesan['number'],
                            'tahunbulan'=> date('Yn'),
                            'waktu'     => $waktuperiksa,
                            'norm'      => $norm,
                            'status'    => 'pesan',
                            'callid'    => $callid,
                            'noantrian' => '0'
                        ];
                        
                        $savepemeriksaan = $this->db->insert('rs_pemeriksaan',$dr);
                        if($savepemeriksaan)
                        {
                            $idpemeriksaan = $this->db->insert_id();
                            $akronimunit   = $this->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket, idpendaftaran, rj.tanggal","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();
                            // $save = $this->db->update('rs_pemeriksaan',['waktu'=>$akronimunit['tanggal']],['idpemeriksaan'=>$idpemeriksaan]);
                            $this->db->update('rs_pemeriksaan',['waktu'=>$akronimunit['tanggal']],['idpemeriksaan'=>$idpemeriksaan]);
                            
                            $idperson = $this->db->select('idperson')->get_where('person_pasien',['norm'=>$norm])->row_array()['idperson'];
                            $expired = $tanggal;
                            $id      = 'Antrian'.date('Ymd',strtotime($tanggal)).'-'.$akronimunit['idstasiun'].'-'.$akronimunit['idloket'];                            
                            $callid = generaterandom(8);
                            $dataha = ['id'=>$id,'number'=>null,'callid'=>$callid,'expired'=>$expired];
                            $savehelper = $this->db->insert('helper_autonumber',$dataha);
                            
                            if($savehelper)
                            {
                                $noantrian = $this->db->select('number')->get_where('helper_autonumber',['callid'=>$callid,'expired'=>$expired,'id'=>$id])->row_array()['number'];
                            
                                $dataa = ['idloket'=>$akronimunit['idloket'], 'tanggal'=>$expired,'kodebooking'=>$callid, 'no'=>$noantrian, 'status'=>'antri', 'idperson'=>$idperson,'idpendaftaran'=>$idpendaftaran,'idpemeriksaan'=>$idpemeriksaan];
                                // $save = $this->db->insert('antrian_antrian',$dataa);
                                $this->db->insert('antrian_antrian',$dataa);
                                
                                /*** Query Ikhsan **/
                                if($carabayar == 'jknnonpbi' || $carabayar == 'jknpbi' || $carabayar == 'bpjstenagakerja'){   
                                    
                                    
                                    # save data apm daftar
                                    $data_apm = [
                                        'datecreated'   =>date('Y-m-d H:i:s'),
                                        'tglperiksa'    =>$waktuperiksa,
                                        'norm'          =>$norm,
                                        'nowa'          =>$nomorwa,
                                        'nobpjs'        =>$nomorbpjs,
                                        'norujukan'     =>$norujukan,
                                        'carabayar'     =>$carabayar,
                                        'tipesurat'     =>$tipesurat,
                                    ];

                                    if( $tipesurat == 'nomor kontrol' ){
                                        $data_apm['jenisrujukan'] = $jenisrujukan;
                                    }
                                    if( $tipesurat == 'nomor rujukan' ){
                                        $data_apm['idasalrujukan'] = $idasalrujukan;
                                    }
                                    
                                    // $save = $this->db->insert('apm_daftar',$data_apm);
                                    $this->db->insert('apm_daftar',$data_apm);

                                }

                                #save person_person
                                $save = $this->db->update('person_person',['notelpon'=>$nomorwa],['idperson'=>$idperson]);
                                
                                /** end query Ikhsan **/

                                $save = $this->db->update('person_pendaftaran',['isantri'=>1],['idpendaftaran'=>$idpendaftaran]);
                                $save = $this->db->update('rs_pemeriksaan',['noantrian'=>$noantrian,'status'=>'antrian'],['idpendaftaran'=>$idpendaftaran]);
                                
                                /**
                                 * condition antrol bpjs
                                 * [+] Penambahan Fungsi Antrol APM
                                 */
                                if($carabayar == 'mandiri'){

                                    $this->db->update('person_pendaftaran',['idjeniskunjungan'=>3],['idpendaftaran'=>$idpendaftaran]);

                                    $this->antrianset_antrianperiksa_apm($idunit,$idpemeriksaan,$idperson);
                                    $this->bridging_antrol_rest($idpendaftaran);
                                }

                                if($save)
                                {
                                    $this->response(["metadata" => ["response"=>true,'code' => 200,'message' => 'Pendaftaran Berhasil']], REST_Controller::HTTP_OK); 
                                }
                                $this->response(["metadata" => ["response"=>false,'code' => 304,'message' => 'Pendaftaran Gagal']], REST_Controller::HTTP_NOT_MODIFIED);
                            }
                            $this->response(["metadata" => ["response"=>false,'code' => 304,'message' => 'Pendaftaran Gagal']], REST_Controller::HTTP_NOT_MODIFIED);
                        }
                        $this->response(["metadata" => ["response"=>false,'code' => 304,'message' => 'Pendaftaran Gagal']], REST_Controller::HTTP_NOT_MODIFIED);
                    }
                    $this->response(["metadata" => ["response"=>false,'code' => 304,'message' => 'Pendaftaran Gagal']], REST_Controller::HTTP_NOT_MODIFIED);
                }
                $this->response(["metadata" => ["response"=>false,'code' => 304,'message' => 'Pendaftaran Gagal']], REST_Controller::HTTP_NOT_MODIFIED);
                
            }
            else
            {
                $this->response(["metadata" =>  ["response"=>'Pendaftaran Gagal.','code' => 304,'message' => 'Simpan Pendaftaran Gagal.']], REST_Controller::HTTP_NOT_MODIFIED);
            }
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    public function antrolbpjsamp_post(){
        
        if($this->cek_signature())
        {
            $idpendaftaran = '420346';
            
            
            // $response = $idpendaftaran; 
            $response = $this->bridging_antrol_rest($idpendaftaran); 



            $this->response(["metadata" =>  [ 'response'=>$response,'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);           
            
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Khusus pasien mandiri
     */
    public function bridging_antrol_rest($idpendaftaran){
        $sql = "select rp.idpendaftaran, pp.norm, aa.kodebooking, pp.carabayar, rp.noantrian,  pp.idjeniskunjungan, 
        date(pp.waktuperiksa) as tanggalperiksa, pp.ispasienlama, ppas.nojkn, pper.nik, pper.notelpon, 
        ifnull(bbv.nomorreferensi,'') as nomorreferensi, bbv.quota_jkn, bbv.quota_jkn_sisa, bbv.quota_nonjkn, bbv.quota_nonjkn_sisa, 
        fdatetime_to_milisecond(aa.estimasidilayani) as estimasidilayani, 
        ppag.bpjs_kodedokter, namadokter(ppag.idpegawai) as namadokter, ru.kodebpjs as kodeunit,  ru.namaunit,
        rj.jammulai, rj.jamakhir
            from rs_pemeriksaan rp 
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran 
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join antrian_antrian aa on aa.idpemeriksaan = rp.idpemeriksaan
            join person_pegawai ppag on ppag.idpegawai = rp.idpegawaidokter
            join rs_unit ru on ru.idunit = rp.idunit
            join rs_jadwal rj on rj.idjadwal = rp.idjadwal
            join bpjs_bridging_vclaim bbv on bbv.idpendaftaran = pp.idpendaftaran
            join antrian_loket al on al.idloket = rp.idloket and al.bridging_jkn = 1
            WHERE rp.idpendaftaran = '".$idpendaftaran."' 
                and rp.idpemeriksaansebelum is null 
                and al.bridging_jkn = 1" ; // sql
        $data = $this->db->query($sql)->result_array();

        // return $this->bpjsbap->get_vclaim_peserta('3404061809360001');

        if($data){
            foreach( $data as $arr)
            {
                $request = array(
                    "kodebooking"=> $arr['kodebooking'],
                    "jenispasien"=> (($arr['carabayar'] == "jknpbi" OR $arr['carabayar'] == "jknnonpbi") ? "JKN" : "NON JKN" ),
                    "nomorkartu"=> $arr['nojkn'],
                    "nik"=> $arr['nik'],
                    "nohp"=> $arr['notelpon'],
                    "kodepoli"=> $arr['kodeunit'],
                    "namapoli"=> $arr['namaunit'],
                    "pasienbaru"=> (($arr['ispasienlama'] == 0) ? 1 : 0),
                    "norm"=> $arr['norm'],
                    "tanggalperiksa"=> $arr['tanggalperiksa'],
                    "kodedokter"=> $arr['bpjs_kodedokter'] ,
                    "namadokter"=> $arr['namadokter'] ,
                    "jampraktek"=> date('H:i',strtotime($arr['jammulai'])).'-'.date('H:i',strtotime($arr['jamakhir'])),
                    // "jeniskunjungan"=> (empty($arr['idjeniskunjungan']) ? 3 : $arr['idjeniskunjungan'] ), // --> 1 (Rujukan FKTP), 2 (Rujukan Internal), 3 (Kontrol), 4 (Rujukan Antar RS)
                    "jeniskunjungan"=> 3, // --> 1 (Rujukan FKTP), 2 (Rujukan Internal), 3 (Kontrol), 4 (Rujukan Antar RS)
                    "nomorreferensi"=> $arr['nomorreferensi'],
                    "nomorantrean"=> $arr['noantrian'],
                    "angkaantrean"=> $arr['noantrian'],
                    "estimasidilayani"=> $arr['estimasidilayani'],
                    "sisakuotajkn"=> $arr['quota_jkn_sisa'],
                    "kuotajkn"=> $arr['quota_jkn'],
                    "sisakuotanonjkn"=> $arr['quota_nonjkn_sisa'],
                    "kuotanonjkn"=> $arr['quota_nonjkn'],
                    "keterangan"=> "Peserta harap 30 menit lebih awal guna pencatatan administrasi."
                );
                $url        = '/antrean/add';
                $uploadData = json_encode($request);
                $method     = 'POST';
                $response   = $this->bpjsbap->requestBpjsAntrol($url,$uploadData,$method); 
                //jika berhasil tambah antrean
                if($response['metaData']->code == 200)
                {
                    $this->db->update('person_pendaftaran',['sudahsingkronjkn'=>1],['idpendaftaran'=>$arr['idpendaftaran'] ]);
                } 

                //simpan log
                $data_log['idpendaftaran'] = $arr['idpendaftaran'];
                $data_log['code']    = $response['metaData']->code;
                $data_log['message'] = $response['metaData']->message;
                $this->db->insert('bpjs_error_kirimserver_antrian',$data_log);
            }   
        }
    }

    public function antrianset_antrianperiksa_apm($idunit,$idpemeriksaan,$idperson)
    {
        // $callid         = generaterandom(8);
        // //buat no antrian periksa
        // $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson);

        //get data antrian
        $get_antrian = $this->db->query("SELECT rp.idpendaftaran, aa.kodebooking, aa.no,  rj.quota_nonjkn, rj.quota_jkn,
        ifnull((select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal),0) as totalantrean,
        ifnull((select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ),0) as antrianjkn,
        ifnull((select pp.norujukan from person_pendaftaran pp WHERE pp.idpendaftaran = rp.idpendaftaran),'') as nomorreferensi
        FROM antrian_antrian aa
        join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
        join rs_jadwal rj on rj.idjadwal = rp.idjadwal
        join rs_unit ru on ru.idunit = rj.idunit
        WHERE aa.idpemeriksaan ='".$idpemeriksaan."'")->row_array();

        //update rs_pemeriksaan
        // $this->db->update('rs_pemeriksaan',['noantrian'=>$get_antrian['no'],'kodebooking'=>$get_antrian['kodebooking'],'status'=>'antrian'], ['idpemeriksaan'=>$idpemeriksaan]);

        //insert bridging vclaim
        $idpendaftaranBridging = $get_antrian['idpendaftaran'];

        $insert_by =  'Anjungan Online Pasien Mandiri';

        $databridging['idpendaftaran']     = $get_antrian['idpendaftaran'];
        $databridging['quota_jkn']     = $get_antrian['quota_jkn'];
        $databridging['quota_nonjkn']  = $get_antrian['quota_nonjkn'];
        $databridging['quota_jkn_sisa']  = intval($get_antrian['quota_jkn']) - intval($get_antrian['antrianjkn']) ;
        $databridging['quota_nonjkn_sisa']  = intval($get_antrian['quota_nonjkn']) - ( intval($get_antrian['totalantrean']) - intval($get_antrian['antrianjkn']) ) ;
        $databridging['nomorreferensi']     = $get_antrian['nomorreferensi'];
        // $databridging['insert_by']     = $this->session->userdata('username');
        $databridging['insert_by']     = $insert_by;

        
        if($idpendaftaranBridging)
        {
            $cek_idpendaftaranBridging = $this->db->query("SELECT * FROM `bpjs_bridging_vclaim` WHERE idpendaftaran='".$idpendaftaranBridging."' limit 1;")->row_array();
            $new_idpendaftaranBridging =null;
            
            if($cek_idpendaftaranBridging){
                $new_idpendaftaranBridging = $cek_idpendaftaranBridging['idpendaftaran'];
            } else{
                $new_idpendaftaranBridging = null;
            }
            
        $this->mgenerikbap->setTable('bpjs_bridging_vclaim'); //set table yang akan dipakai
        $this->mgenerikbap->update_or_insert_ignoreduplicate($databridging, $new_idpendaftaranBridging);       
        }// $this->db->replace('bpjs_bridging_vclaim',$databridging);
        
        $akronimunit = $this->mgenerikbap->select_multitable("al.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();

        $datapasien = $this->mgenerikbap->select_multitable("norm, concat(ifnull(identitas,''),' ',ifnull(namalengkap,'')) as namalengkap, usia(tanggallahir) as usia, date_format(tanggallahir,'%d %b %Y') as tanggallahir, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();
        // ambil nama dokter
        $dtdokter = $this->db->query("SELECT DATE_FORMAT(rp.waktu, '%d/%m/%Y') as waktu,  CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
        // antrian sep
        //$antrisep = $this->ql->buat_antriansep($idpemeriksaan);
        
        $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$insert_by.';buat antrian-poli;idunit:'.$idunit.';idpemeriksaan:'.$idpemeriksaan.';'.';norm:'.$datapasien['norm'].';namaloket:'.$akronimunit['namaloket'],
                'expired' =>date('Y-m-d', strtotime("+2 weeks", strtotime(date('Y-m-d'))))
            ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            
        // $result = array(
        //     'noantrian'=>$get_antrian['no'],
        //     'kodebooking'=>$get_antrian['kodebooking'],
        //     'datapasien'=>$datapasien,
        //     'namaloket'=>$akronimunit['namaloket'],
        //     'dokter'=>$dtdokter
        //     );
        
        // if($modeapp=='local')
        // {
        //     echo json_encode($result);
        // }
        // else
        // {
        //     return $result;      
        // }
        
    }
    
    
}