<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */

/**
 * Web Service Antrean - BPJS (Diakses oleh sistem RS)
 */
class Wsfkrtl extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
                // $this->load->library('ql');
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
//        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
//        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
    
    public function token_get() // --ok
    {
        // echo "<pre>";
        // print_r ($result);
        // echo "</pre>";
        // return false;

        $result = $this->db->query("select fkrtl_token('".$this->read_username()."', '".$this->read_password()."') as token")->result_array();
        if ($result[0]['token'] != null)
        {
            $this->response([
                "response" =>  [ "token" => $result[0]["token"]],
                "metadata" => ["message" => "Ok","code" => 200]
                            ], REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response(["metadata" =>  [   'message' => 'Username atau Password Tidak Sesuai',
                                                'code' => 201
                                            ]
                            ], REST_Controller::HTTP_OK);
        }
    }
    
    private function cek_token() // -- ok
    {
        $result = $this->db->query("select fkrtl_cektoken('".$this->read_token()."','".$this->read_username()."') as cektoken")->result_array();
        return $result[0]['cektoken'] == 1;
    }
    
    // antrian pendaftaran
//    private function antrianpendaftaran()
//    {
//        $callid = generaterandom(8);
//        $idloket = 2;//pendaftaran bpjs
//        $idstasiun = 1;//pendaftaran
//        $tp  = date('Ymd', strtotime(trim($this->get_post('tanggalperiksa'))));
//
//        $this->mgenerikbap->setTable('helper_autonumber');
//        $data = [
//            'id'      =>'Antrian'.$tp.'-'.$idstasiun.'-'.$idloket,
//            'number'  =>NULL,
//            'callid'  =>$callid,
//            'expired' =>$tp
//        ];
//        $this->mgenerikbap->insert($data);
//        $noantrian = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array()['number'];
//        $data = ['idloket'=>$idloket, 'tanggal'=>$tp,'kodebooking'=>$callid, 'no'=>$noantrian, 'status'=>'antri'];
//        $saveantri = $this->db->insert('antrian_antrian',$data);
//        $idantrian = $this->db->insert_id();
//        //buat estimasi pelayanan
//        $loket = $this->db->query("select lamapelayanan, namaloket from antrian_loket where idloket='".$idloket."'")->row_array();
//        $this->estimasilayanan($loket['lamapelayanan'],$noantrian,$tp,$idantrian);
//        $estimasi = $this->db->select(' UNIX_TIMESTAMP(estimasidilayani) * 1000 as estimasi')->where('idantrian',$idantrian)->get('antrian_antrian')->row_array()['estimasi'];
//
//        $dataantrian = ['nomorantrean'=>$data['no'],'kodebooking'=>$data['kodebooking'],'jenisantrean'=>(int)trim($this->get_post('jenisrequest')), 'estimasidilayani'=>(int)$estimasi, 'namapoli'=>$loket['namaloket'],'namadokter'=>""];
//        $this->response([ "response"=>$dataantrian,"metadata" =>  [ 'code' => 200,'message' => 'Ok']], REST_Controller::HTTP_OK);
//    }
    

    private function pendaftaranpoli_savedaftarpemeriksaan($idpendaftaran,$idjadwal,$nopesan,$norm,$callid,$tanggalperiksa)
    {   
        $this->ql->person_pendaftaran_insert_or_update($idpendaftaran,['nopesan'=>$nopesan]);
        $data = [
            'idpendaftaran'=>$idpendaftaran,
            'idjadwal'  =>$idjadwal,
            'nopesan'   =>$nopesan,
            'tahunbulan'=> date('Yn'),
            'waktu'     =>(empty(format_date_to_sql($tanggalperiksa))) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime(format_date_to_sql($tanggalperiksa))),
            'norm'      =>$norm,
            'status'    =>'pesan',
            'callid'    =>$callid,
            'noantrian' =>'0'
        ];
        $simpan = $this->ql->rs_pemeriksaan_insert_or_update('',$data);
        $idpemeriksaan = $this->db->insert_id();

        $simpan = $this->ql->addpaketvitalsign($idpemeriksaan,$idjadwal,$idpendaftaran);

        $this->load->model('mtriggermanual');
        $this->mtriggermanual->airs_pemeriksaan($idjadwal);
    }

    private function antrianset_antrianperiksa($idunit,$idpemeriksaan,$idperson)
    {
        $callid  = generaterandom(8);
        //buat no antrian periksa
        $this->ql->antrianset_noantriperiksa($callid,$idunit,$idpemeriksaan,$idperson);
        //ambil no antrian 
        $noantrian = $this->ql->antrian_get_number($callid);
        $this->db->update('rs_pemeriksaan',['noantrian'=>$noantrian,'status'=>'antrian'],['idpemeriksaan'=>$idpemeriksaan]);
        $antrian = $this->getdataantrian($idpemeriksaan); 
        $this->db->update('rs_pemeriksaan',['kodebooking'=>$antrian['kodebooking']],['idpemeriksaan'=>$idpemeriksaan]);
        
        //buat estimasi pelayanan
        
        //select data estimasi
        $estimasi = $this->db->select(' UNIX_TIMESTAMP(estimasidilayani) * 1000 estimasi')->where('idantrian',$antrian['idantrian'])->get('antrian_antrian')->row_array()['estimasi'];
        
        $post=(object)$this->get_post_callback();

        //insert bridging vclaim
        $idpendaftaranBridging = $antrian['idpendaftaran']; // ID
        $databridging['quota_jkn']     = intval($antrian['quota_jkn']);
        $databridging['quota_nonjkn']  = intval($antrian['quota_nonjkn']);
        $databridging['quota_jkn_sisa']  = intval($antrian['quota_jkn']) - intval($antrian['antrianjkn']);
        $databridging['quota_nonjkn_sisa']  = intval($antrian['quota_nonjkn']) - ( intval($antrian['totalantrean']) - intval($antrian['antrianjkn']) );
        
        $this->mgenerikbap->setTable('bpjs_bridging_vclaim'); //set table yang akan dipakai
        $this->mgenerikbap->update_or_insert_ignoreduplicate($databridging, $idpendaftaranBridging);
        // $this->db->replace('bpjs_bridging_vclaim',$databridging);
        
        //buat response
        // $result = array(
        $result='';
        $result = [
            'nomorantrean'  =>  $antrian['no'],
            'angkaantrean'  =>  $antrian['no'],
            'kodebooking'   =>  $antrian['kodebooking'],
            'norm'          =>  (int)trim($post->norm),
            'estimasidilayani'=>(int)$estimasi,
            'namapoli'      =>  $antrian['namaloket'],
            'namadokter'    =>  $antrian['namadokter'],
            'sisakuotajkn'  =>  intval($antrian['quota_jkn']) - intval($antrian['antrianjkn']),
            'kuotajkn'      =>  intval($antrian['quota_jkn']),
            'sisakuotanonjkn' => intval($antrian['quota_nonjkn']) - ( intval($antrian['totalantrean']) - intval($antrian['antrianjkn']) ),
            'kuotanonjkn'   => intval($antrian['quota_nonjkn']),
            'keterangan'    =>  "Peserta harap 60 menit lebih awal guna pencatatan administrasi."
        ];
        return $result;
    }
    // end antrian poli
    
    //get data antrian
    private function getdataantrian($idpemeriksaan)
    {
        $quary = $this->db->query("SELECT namadokter(rj.idpegawaidokter) as namadokter, rj.tanggal, aa.idantrian, aa.no, aa.kodebooking, al.namaloket, al.lamapelayanan, rj.quota_jkn, rj.quota_nonjkn, rp.idpendaftaran,"
                . " (select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal) as totalantrean,"
                . " (select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal) as totalantrean,"
                . " (select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ) as antrianjkn "
                . "FROM rs_pemeriksaan rp, rs_jadwal rj, antrian_antrian aa, antrian_loket al "
                . "where rp.idpemeriksaan='".$idpemeriksaan."' and rj.idjadwal = rp.idjadwal and aa.idpemeriksaan='".$idpemeriksaan."' and al.idloket=aa.idloket")
                ->row_array();
        return $quary;
    }
    
    //set estimasi layanan
    // private function estimasilayanan($lamapelayanan,$noantrian,$tanggalperiksa,$idantrian)
    // {
    //     $menit = $lamapelayanan * $noantrian;
    //     $estimasi = date('Y-m-d H-i-s', strtotime('+ '.$menit.' minutes', strtotime($tanggalperiksa)));
    //     $update=$this->db->update('antrian_antrian',['estimasidilayani'=>$estimasi],['idantrian'=>$idantrian]);
    //     return $update;
    // }
    
    /*
     * Mengambil rekap antrean harian
     */
    public function rekapantrean_post() //--ok 
    {
        if ($this->cek_token())
        { 
            $post=(object)$this->get_post_callback();

            $tanggalperiksa= trim($post->tanggalperiksa); /* (YYYY-MM-DD) */
            $kodepoli      = trim($post->kodepoli);
            $polieksekutif = trim($post->polieksekutif); /* 1=Poli Eksekutif , 0=Poli Biasa */
            if(empty($tanggalperiksa) or empty($kodepoli))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Data Tidak Tersedia']],REST_Controller::HTTP_OK); 
            }
            else
            {
                if(validateDate($tanggalperiksa,'Y-m-d'))
                {
                    $rekapantrean  = $this->db->query("SELECT al.namaloket as namapoli, count(1) totalantrean,sum(if(rp.status='selesai' or rp.status='sedang ditangani',1,0)) jumlahterlayani,  UNIX_TIMESTAMP(max(aa.lastupdate)) * 1000 as lastupdate FROM antrian_antrian aa, antrian_loket al, rs_pemeriksaan rp, rs_unit ru where aa.tanggal='".$tanggalperiksa."' and al.idloket = aa.idloket and rp.idpemeriksaan=aa.idpemeriksaan and ru.akronimunit=al.akronimunit and ru.polieksekutif='".$polieksekutif."' and ru.kodebpjs='".$kodepoli."' GROUP by al.idloket")->result_array();
                    if(empty($rekapantrean))
                    {
                        $this->response(["metadata" =>['code' => 201,'message' => 'Data Tidak Tersedia']],REST_Controller::HTTP_OK); 
                    }
                    else
                    {
                        $rekap = [];
                        foreach ($rekapantrean as $arr) {
                            $rekap[] = [ 'namapoli'=>$arr['namapoli'], 'totalantrean'=>(int)$arr['totalantrean'], 'jumlahterlayani'=>(int)$arr['jumlahterlayani'], 'lastupdate'=>(int)$arr['lastupdate'] ];
                        }
                        $this->response(["response"=>$rekap,"metadata" =>['code' => 200,'message' => 'Ok']],REST_Controller::HTTP_OK);
                    }

                }
                else
                {
                    $this->response(["metadata" =>['code' => 201,'message' => 'Data Tidak Tersedia']],REST_Controller::HTTP_OK);
                }
                
            }
        }
        else 
        {
            $this->response(["metadata" =>  ['code' => 201,'message' => 'Token Expired']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    /*
     * Mengambil kode booking operasi hanya yang belum terlaksana
     * response berhasil
     * "kodebooking"   =>"",
     * "tanggaloperasi"=> date('Y-m-d'),
     * "jenistindakan" => "operasi gigi",
     * "kodepoli"      => "001",
     * "namapoli"      => "Poli Bedah Mulut",
     * "terlaksana"    => 0
     * mahmud, clear
     */
    public function jadwaloperasi_pasien_post()
    {
        $post=(object)$this->get_post_callback();

        if ($this->cek_token()){ 
            $nopeserta= trim($post->nopeserta);
            if(empty($nopeserta)){
                $this->response(["metadata" =>['code' => 201,'message' => 'No. Peserta Tidak Ditemukan atau Belum terdaftar Jadwal Operasi']],REST_Controller::HTTP_OK); 
            }
            else{
                // cek nomor kartu ==> type numerik dan 13 digit 
                $cekinfo = ((strlen($nopeserta)==13) && is_numeric($nopeserta));
                if(!$cekinfo){
                    $this->response([
                        "metadata" =>['code' => 201,'message' => 'Nomor Kartu Tidak Valid']
                        ],REST_Controller::HTTP_OK);
                } 
                else 
                {
                    $res = $this->db->query("SELECT rjo.kodebooking, date(rjo.waktuoperasi) as tanggaloperasi, rjto.jenistindakan, ru.kodebpjs as kodepoli, ru.namaunit as namapoli, rjo.terlaksana   FROM rs_jadwal_operasi rjo, person_pendaftaran pp,person_pasien ppas, rs_unit ru, rs_jenistindakanoperasi rjto where pp.idpendaftaran=rjo.idpendaftaran and ppas.norm=pp.norm and ru.idunit=rjo.idunit and rjto.idjenistindakan=rjo.idjenistindakan and ppas.nojkn='".$nopeserta."'")->result_array();
                    if(empty($res))
                    {
                        $this->response(["metadata" =>['code' => 201,'message' => 'No. Peserta Tidak Ditemukan atau Belum terdaftar Jadwal Operasi']],REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $resarr=[];
                        foreach ($res as $arr) {
                            $resarr[] = [ 'kodebooking'=>$arr['kodebooking'],'tanggaloperasi'=>$arr['tanggaloperasi'],'jenistindakan'=>$arr['jenistindakan'],'kodepoli'=>$arr['kodepoli'],'namapoli'=>$arr['namapoli'],'terlaksana'=>(int)$arr['terlaksana'] ];
                        }
                        $this->response(["response"=>["list" => $resarr ,"metadata" =>['code' => 200,'message' => 'Ok'] ] ],REST_Controller::HTTP_OK);
                    }
                }

            }
        }else{
            $this->response(["metadata" =>['code' => 201,'message' => 'Token Expired']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    /*
     * Mengambil jadwal operasi harian
     * TERLAKSANA (1=sudah, 0=belum, 2=batal)
     * NOPESERTA = NOMOR KARTU BPJS (dikosongkan jika pasien non BPJS)
     * Last Update = Timestamp(milisecond)
     * [
        "kodebooking"   =>"123456ZXC",
        "tanggaloperasi"=> date('Y-m-d'),
        "jenistindakan" => "operasi gigi",
        "kodepoli"      => "001",
        "namapoli"      => "Poli Bedah Mulut",
        "terlaksana"    => 0,
        "nopeserta"     => "0000000924782",
        "lastupdate"    => time()
       ],
     * mahmud, clear
     */

    public function jadwaloperasi_post()
    {
        if ($this->cek_token())
        { 
            $post=(object)$this->get_post_callback();

            $tglawal = trim($post->tanggalawal);/* YYYY-MM-DD */
            $tglakhir= trim($post->tanggalakhir);/* YYYY-MM-DD */
            if(empty($tglawal) or empty($tglakhir)){
                $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal harap diisi.!']],REST_Controller::HTTP_OK); 
            }else{
                if(validateDate($tglawal,'Y-m-d') and validateDate($tglakhir,'Y-m-d')){
                    if( strtotime($tglawal) > strtotime($tglakhir) )
                    {
                        $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal Akhir Tidak Boleh Lebih Kecil Dari Tanggal Awal']],REST_Controller::HTTP_OK); 
                    }
                    else
                    {
                        $res = $this->db->query("SELECT rjo.kodebooking, date(rjo.waktuoperasi) as tanggaloperasi,rjto.jenistindakan, ru.kodebpjs as kodepoli, ru.namaunit as namapoli, rjo.terlaksana, ppas.nojkn as nopeserta, UNIX_TIMESTAMP(max(rjo.lastupdate)) * 1000 as lastupdate  FROM rs_jadwal_operasi rjo, person_pendaftaran pp,person_pasien ppas, rs_unit ru, rs_jenistindakanoperasi rjto where pp.idpendaftaran=rjo.idpendaftaran and ppas.norm=pp.norm and ru.idunit=rjo.idunit and rjto.idjenistindakan=rjo.idjenistindakan and date(rjo.waktuoperasi) between '".$tglawal."' and '".$tglakhir."' GROUP by rjo.idjadwaloperasi")->result_array();
                        if(empty($res))
                        {
                            $this->response(["metadata" =>['code' => 201,'message' => 'Data Tidak Ada']],REST_Controller::HTTP_OK); 
                        }
                        else
                        {
                            $resarr=[];
                            foreach ($res as $arr) {
                                $resarr[]=['kodebooking'=>$arr['kodebooking'], 'tanggaloperasi'=>$arr['tanggaloperasi'],'jenistindakan'=>$arr['jenistindakan'], 'kodepoli'=>$arr['kodepoli'], 'namapoli'=>$arr['namapoli'], 'terlaksana'=>(int)$arr['terlaksana'], 'nopeserta'=>$arr['nopeserta'],'lastupdate'=>(int)$arr['lastupdate']];
                            }
                            $this->response(["response"=>["list" =>$resarr],"metadata" =>['code' => 200,'message' => 'Ok']],REST_Controller::HTTP_OK);     
                        }
                    }
                    
                }else{
                    $this->response(["metadata" =>['code' => 201,'message' => 'Format Tanggal Tidak Sesuai, format yang benar adalah yyyy-mm-dd']],REST_Controller::HTTP_OK); 
                }
            }
        }
        else 
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Token Expired' ]], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    /**
     * Menampilkan status antrean per poli (digunakan untuk perencanaan kedatangan pasien)
     * Method Post
     * "kodepoli"       : "{memakai kode subspesialis BPJS}",
     * "kodedokter"     : {kode dokter BPJS},
     * "tanggalperiksa" : "{tanggal rencana berobat}",
     * "jampraktek"     : "{waktu praktek dokter yang diambil dari Aplikasi HFIS}"
     * mahmud, clear
     */
    public function status_antrian_post()
    {
        if ($this->cek_token())
        { 
            $post=(object)$this->get_post_callback();

            $kodepoli   = trim($post->kodepoli);
            $kodedokter = trim($post->kodedokter);
            $tanggalperiksa   = trim($post->tanggalperiksa); // ex : "2020-01-28",
            $jampraktek = trim($post->jampraktek);     // ex : "08:00-16:00"
            
            if(empty($kodepoli))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Kode Subspesialis BPJS Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($kodedokter))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Kode Dokter BPJS Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($tanggalperiksa))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal Rencana Berobat Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($jampraktek))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Waktu Praktek Dokter Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else
            {
                if(validateDate($tanggalperiksa,'Y-m-d'))
                {
                    if($tanggalperiksa < date('Y-m-d')) // cek jika backdate
                    {
                        $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal Periksa Tidak Berlaku']],REST_Controller::HTTP_OK); 
                    }
                    else{
                        $jam   = explode("-", $jampraktek);
                        $res = $this->db->query("SELECT ru.namaunit, namadokter(rj.idpegawaidokter) as namadokter, rj.quota_nonjkn, rj.quota_jkn,
                            (select max(rp.noantrian) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal) as totalantrean,
                            (select ifnull(max(rp.noantrian),0) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal and rp.status != 'antrian' and rp.status != 'batal') as antreanpanggil,
                            (select sum(1) from rs_pemeriksaan rp join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran WHERE rp.idjadwal = rj.idjadwal and (pp.carabayar='jknpbi' or pp.carabayar='jknnonpbi') ) as antrianjkn
                        FROM rs_jadwal rj
                        join rs_unit ru on ru.idunit = rj.idunit
                        join person_pegawai pp on pp.idpegawai = rj.idpegawaidokter
                        WHERE ru.kodebpjs = '".$kodepoli."' and pp.bpjs_kodedokter = '".$kodedokter."' and date(rj.tanggal) = '".$tanggalperiksa."' and rj.jammulai = '".$jam[0]."' and rj.jamakhir = '".$jam[1]."' limit 1")->result_array();
                        
                        if(empty($res))
                        {
                            $this->response(["metadata" =>['code' => 201,'message' => 'Poli Tidak Ditemukan']],REST_Controller::HTTP_OK); 
                        }
                        else
                        {
                            $resarr='';
                            $resarr = [
                                'namapoli'=>$res[0]['namaunit'], 
                                'namadokter'=>$res[0]['namadokter'],
                                'totalantrean'=>$res[0]['totalantrean'],
                                'sisaantrean'=> intval($res[0]['totalantrean']) - intval($res[0]['antreanpanggil']),
                                'antreanpanggil'=>$res[0]['antreanpanggil'],
                                'sisakuotajkn' => intval($res[0]['quota_jkn']) - intval($res[0]['antrianjkn']),
                                'kuotajkn'=>$res[0]['quota_jkn'], 
                                'sisakuotanonjkn' => intval($res[0]['quota_nonjkn']) - ( intval($res[0]['totalantrean']) - intval($res[0]['antrianjkn']) ) ,
                                'kuotanonjkn'=>$res[0]['quota_nonjkn'],
                                'keterangan'=>''
                            ];
                            $this->response(["response"=>$resarr,"metadata" =>['code' => 200,'message' => 'Ok']],REST_Controller::HTTP_OK);     
                        }
                    }
                    
                }
                else
                {
                    $this->response(["metadata" =>['code' => 201,'message' => 'Format Tanggal Tidak Sesuai, format yang benar adalah yyyy-mm-dd']],REST_Controller::HTTP_OK); 
                }
            }
        }
        else 
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Token Expired' ]], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    /**
     * Membatalkan antrean pasien
     * mahmud, clear
     */
    public function batal_antrian_post()
    {
        $post=(object)$this->get_post_callback();

        $kodebooking   = trim($post->kodebooking);
        $keterangan    = trim($post->keterangan);
        if(empty($kodebooking))
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Kode Booking Harap Diisi.' ]], REST_Controller::HTTP_OK);
        }
        else if(empty($keterangan))
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Keterangan Harap Diisi.' ]], REST_Controller::HTTP_OK);
        }
        else
        {
            //cek apakah kodebooking tersedia
            $dtperiksa = $this->db->select('rp.idpemeriksaan, rp.idpendaftaran, aa.status')->join('rs_pemeriksaan rp','rp.idpemeriksaan = aa.idpemeriksaan')->get_where('antrian_antrian aa',['aa.kodebooking'=>$kodebooking]);
            if($dtperiksa->num_rows() > 0)
            {               
                $data = $dtperiksa->row_array();
                if($data['status'] == "antri")
                {
                    //update tabel antrian_antrian
                    $save = $this->db->update('antrian_antrian',['status'=>'batal'],['kodebooking'=>$kodebooking]);
                    //update tabel rs_pemeriksaan
                    $save = $this->db->update('rs_pemeriksaan',['status'=>'batal'],['idpemeriksaan'=>$data['idpemeriksaan'] ]);
                    //update tabel person_pendaftaran
                    $save = $this->db->update('person_pendaftaran',['idstatuskeluar'=>3,'keterangan_pembatalan'=>$keterangan],['idpendaftaran'=>$data['idpendaftaran'] ]);                
                    //update tabel bpjs_bridging_antrean_task_log                
                    $save = $this->ql->insert_antrean_task_log(99,$data['idpendaftaran']);

                    if($save)
                    {
                        $this->response(["metadata" =>  [ 'code' => 200,'message' => 'Ok' ]], REST_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Membatalkan Antrean Gagal.' ]], REST_Controller::HTTP_OK);
                    }
                } 
                else if($data['status'] == "batal")
                {
                    $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Antrean Tidak Ditemukan atau Sudah Dibatalkan' ]], REST_Controller::HTTP_OK);
                } 
                else
                {
                    $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Pasien Sudah Dilayani, Antrean Tidak Dapat Dibatalkan' ]], REST_Controller::HTTP_OK);
                } 
                
            }
            else
            {
                $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Antrean Tidak Ditemukan' ]], REST_Controller::HTTP_OK);
            }
        }
    }
    
    /**
     * Memastikan pasien sudah datang di RS
     * mahmud, clear
     */
    public function checkin_antrian_post()
    {
        $post=(object)$this->get_post_callback();
        $datainsert = json_encode($this->get_post_callback());
        $cllbck = array(
            'callback' => $datainsert  
         );
        $this->db->insert('log_callback_checkin', $cllbck);

        $kodebooking   = trim($post->kodebooking);
        $waktu         = trim($post->waktu);
        if(empty($kodebooking))
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Kode Booking Harap Diisi.' ]], REST_Controller::HTTP_OK);
        }
        else if(empty($waktu))
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Waktu Harap Diisi.' ]], REST_Controller::HTTP_OK);
        }
        else
        {
            //cek apakah kodebooking tersedia
            $dtperiksa = $this->db->select('rp.idpendaftaran')->join('rs_pemeriksaan rp','rp.idpemeriksaan = aa.idpemeriksaan')->get_where('antrian_antrian aa',['aa.kodebooking'=>$kodebooking]);
            if( !empty($dtperiksa->row_array()) )
            {
                $data = $dtperiksa->row_array();
                //update tabel bpjs_bridging_antrean_task_log                
                $save = $this->ql->insert_antrean_task_log(3,$data['idpendaftaran']);
                
                if($save)
                {
                    $this->response(["metadata" =>  [ 'code' => 200,'message' => 'Ok' ]], REST_Controller::HTTP_OK);
                }
                else
                {
                    $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Checkin Gagal.' ]], REST_Controller::HTTP_OK);
                }

            }
            else
            {
                $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Kode Booking Tidak Ditemukan.' ]], REST_Controller::HTTP_OK);
            }
        }
    }
    
    
    /**
     * Informasi identitas pasien baru yang belum punya rekam medis (tidak ada norm di Aplikasi VClaim)
     * Mahmud, clear
     */
    public function info_pasien_baru_post()
    {
        //        "nomorkartu": "{no kartu pasien JKN}",
        //        "nik": "{nika pasien}",
        //        "nomorkk": "{no kk pasien}",
        //        "nama": "{nama pasien}",
        //        "jeniskelamin": "{jenis kelamin pasien", -- L/P
        //        "tanggallahir": "{tanggal lahir pasien}",
        //        "nohp": "{no hp pasien}",
        //        "alamat": "{alamat pasien}",
        //        "kodeprop": "{kode propinsi BPJS}",
        //        "namaprop": "{nama propinsi}",
        //        "kodedati2": "{kode kota/kab BPJS}",
        //        "namadati2": "{nama kota/kab}",
        //        "kodekec": "{kode kecamatan BPJS}",
        //        "namakec": "{nama kecamatan}",
        //        "kodekel": "{kode kelurahan BPJS}",
        //        "namakel": "{nama kelurahan}",
        //        "rw": "{no RT}",
        //        "rt": "{no RW}"
           $post = $this->input->post();
           $nomorkartu = $this->is_empty_data($post['nomorkartu'],'Nomor Kartu');
           $nik        = $this->is_empty_data($post['nik'],'NIK');
           $nomorkk    = $this->is_empty_data($post['nomorkk'],'Nomor KK');
           $nama       = $this->is_empty_data($post['nama'],'Nama');
           $jeniskelamin = $this->is_empty_data($post['jeniskelamin'],'Jenis Kelamin');
           $tanggallahir = $this->is_empty_data($post['tanggallahir'],'Tanggal Lahir');
           $nohp         = $this->is_empty_data($post['nohp'],'No HP');
           $alamat       = $this->is_empty_data($post['alamat'],'Alamat ');
           $kodeprop     = $this->is_empty_data($post['kodeprop'],'Kode Propinsi');
           $namaprop     = $this->is_empty_data($post['namaprop'],'Nama Propinsi');
           $kodedati2 = $this->is_empty_data($post['kodedati2'],'Kode Dati 2');
           $namadati2 = $this->is_empty_data($post['namadati2'],'Dati 2');
           $kodekec = $this->is_empty_data($post['kodekec'],'Kode Kecamatan');
           $namakec = $this->is_empty_data($post['namakec'],'Kecamatan');
           $kodekel = $this->is_empty_data($post['kodekel'],'Kode Kelurahan');
           $namakel = $this->is_empty_data($post['namakel'],'Kelurahan');
           $rw = $this->is_empty_data($post['rw'],'RW');
           $rt = $this->is_empty_data($post['rt'],'RT');
           
           //cari kelurahan
           $iddesakelurahan = 0;
           //cari kecataman
           $idkecamatan = 0;
            
            // cek nomor kartu ==> type numerik dan 13 digit 
            $cekinfo = ((strlen($nomorkartu)==13) && is_numeric($nomorkartu));
            if(!$cekinfo){
                $this->response([
                    "metadata" =>['code' => 201,'message' => 'Format Nomor Kartu Tidak Sesuai']
                    ],REST_Controller::HTTP_OK);
            }

            // cek NIK ==> type numerik dan 16 digit 
            $cekinfo = ((strlen($nik)==16) && is_numeric($nik));
            if(!$cekinfo){
                $this->response([
                    "metadata" =>['code' => 201,'message' => 'Format NIK Tidak Sesuai']
                    ],REST_Controller::HTTP_OK);
            }

            // cek tanggal lahir => format Y-m-d dan tidak boleh lebih besar dari tanggal sekarang
            date_default_timezone_set("Asia/Bangkok");
            $currentDate = date("Y-m-d",time());
            $cekinfo = (validateDate($tanggallahir,'Y-m-d') && ($tanggallahir <= $currentDate));
            if(!$cekinfo){
                $this->response([
                    "metadata" =>['code' => 201,'message' => 'Format Tanggal Lahir Tidak Sesuai']
                    ],REST_Controller::HTTP_OK);
            }
           
            //cek apakah pasien sudah terdaftar di rs
            $cekpasien = $this->db->select('norm')
                    ->join('person_pasien ppas','ppas.idperson=pp.idperson')
                    ->group_start()->where('pp.nik',$nik)->or_where('ppas.nojkn',$nomorkartu)->group_end()
                    ->where('pp.status_person','A')
                    ->where('ppas.status_pasien','A')
                    ->get('person_person pp');

           //jika pasien sudah terdaftar di rs
           if($cekpasien->num_rows() > 0)
           {
               $datapasien = $cekpasien->row_array();
               $norm = $datapasien['norm'];
               
               $this->response([
                "metadata" =>['code' => 201,'message' => 'Data Peserta Sudah Pernah Dientrikan']
                ],REST_Controller::HTTP_OK);
           }
           else
           {
                //1. insert data pasien ke tabel person_person  -- dapat idperson
                //2. insert data pasien ke tabel person_pasien  -- dapat norm
                //3. insert data pasien ke tabel bpjs_bpjs

                //insert tabel person_person
                $dtperson = [
                    'nik' => $nik,
                    'nomorkk' => $nomorkk,
                    'namalengkap'=>$nama,
                    'tanggallahir' => $tanggallahir,
                    'notelpon' => $nohp,
                    'jeniskelamin' => (($jeniskelamin == 'L') ? 'laki-laki' : 'wanita' ),
                    'alamat' =>$alamat,
                    'rt'=>$rt,
                    'rw'=>$rw,
                    'iddesakelurahan'=>$iddesakelurahan,
                    'idkecamatan'=>$idkecamatan
                ];
                $this->db->trans_start(); //mulai proses
                $save = $this->db->insert('person_person',$dtperson);
                if(!$save){ $this->simpan_data_gagal();} //jika simpan gagal
                $idperson = $this->db->insert_id();

                //insert tabel person_pasien
                $save = $this->db->insert('person_pasien',array('idperson' => $idperson,'nojkn' => $nomorkartu) );
                if(!$save){ $this->simpan_data_gagal();} //jika simpan gagal
                $norm = $this->db->insert_id();

                //insert tabel bpjs_bpjs
                $save = $this->db->insert('bpjs_bpjs',array('norm' => $norm));
                if(!$save){ $this->simpan_data_gagal();} //jika simpan gagal
                $this->db->trans_complete(); // selesai proses
                
                $this->response([
                "response" =>['norm'=>$norm],
                "metadata" =>['code' => 200,'message' => 'Harap datang ke admisi untuk melengkapi data rekam medis']
                ],REST_Controller::HTTP_OK);
            }
    }
    
    private function simpan_data_gagal()
    {
        $this->response(["metadata" =>['code' => 201,'message' => 'Simpan Data Gagal.']],REST_Controller::HTTP_OK); 
        $this->db->trans_complete(); // selesai proses
        return false;
    }
    
    private function is_empty_data($param,$msg)
    {
        if(empty($param))
        {
            $this->response(["metadata" =>['code' => 201,'message' => $msg.' Belum Diisi.']],REST_Controller::HTTP_OK); 
            return false;
        }
        else
        {
            return $param;
        }
    }


    /**
     * RS mengirimkan url masing-masing ws yang sudah dibuat untuk diakses oleh sistem BPJS
     * Fungsi : Mengambil antrean
     * Method Post
     *   "nomorkartu": "{noka pasien BPJS,diisi kosong jika NON JKN}",
     *   "nik": "{nik pasien}",
     *   "nohp": "{no hp pasien}",
     *   "kodepoli": "{memakai kode subspesialis BPJS}",
     *   "norm": "{no rekam medis pasien}",
     *   "tanggalperiksa": "{tanggal periksa}",
     *   "kodedokter": {kode dokter BPJS},
     *   "jampraktek": "{jam praktek dokter}",
     *   "jeniskunjungan": {1 (Rujukan FKTP) => Faskes 1 ke RS, 2 (Rujukan Internal) => Internal RS, 3 (Kontrol) => Pasien Kontrol, 4 (Rujukan Antar RS) => dari RS ke RS},
     *   "nomorreferensi": "{norujukan/kontrol pasien JKN,diisi kosong jika NON JKN}"
     * mahmud, on dev
     * Alur -> 1. Cari Jadwal
     *         2. Buat Pendaftaran Pasien
     *         3. Insert Tabel bpjs_bridging_vclaim
     *         4. Buat No Pesan
     *         5. Update Nopesan ke Data Pendaftaran
     *         6. Buat Antrian Pemeriksaan
     *         7. Tampilkan response
     */
    public function ambil_antrian_post()
    {
        
        // error_reporting(0);
        
        if ($this->cek_token()) //cek token
        { 
            $post=(object)$this->get_post_callback();
            // print_r($post);
            // die;
            $nomorkartu     = trim($post->nomorkartu);
            $nik            = trim($post->nik);
            $nohp           = trim($post->nohp);
            $kodepoli       = trim($post->kodepoli);
            $norm           = trim($post->norm);
            $tanggalperiksa = trim($post->tanggalperiksa);
            $kodedokter     = trim($post->kodedokter);
            $jampraktek     = explode('-', trim($post->jampraktek));
            $jeniskunjungan = trim($post->jeniskunjungan);
            $nomorreferensi = trim($post->nomorreferensi);

           

            if(empty($nik))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'NIK Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($nohp))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'No.HP Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($kodepoli))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Spesialis Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($norm))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Norm Harap Diisi, Hubungi Bagian Pendaftaran RS apabila belum pernah mendaftar atau lupa norm.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($tanggalperiksa))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal Periksa Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($kodedokter))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Dokter Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty($jeniskunjungan))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Jenis Kunjungan Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else if(empty(!$nomorkartu) && empty($nomorreferensi))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'No Referensi Harap Diisi, isi dengan (norujukan/kontrol) pasien JKN.']],REST_Controller::HTTP_OK); 
            }
            else
            {
                if(validateDate($tanggalperiksa,'Y-m-d')) // cek format tanggal
                {
                    
                    //cari jadwal
                    $jadwal = $this->db->query("select u.idunit, idjadwal "
                            . "from rs_jadwal j "
                            . "join rs_unit u on u.idunit=j.idunit "
                            . "join person_pegawai p on p.idpegawai = j.idpegawaidokter and p.bpjs_kodedokter = '".$kodedokter."' "
                            . "where date(j.tanggal)='".$tanggalperiksa."' and j.jammulai='".$jampraktek[0]."' and j.jamakhir='".$jampraktek[1]."' and status='terkonfirmasi' and kodebpjs='".$kodepoli."' and p.bpjs_kodedokter = '".$kodedokter."' ")
                            ->row_array();
                    // jika jadwal ditemukan
                    if (empty(!$jadwal)) 
                    {
                        $waktuperiksa   = date('Y-m-d H:i:s', strtotime($tanggalperiksa));
                        $waktu          = date('Y-m-d H:i:s');//waktu mendaftar
                        $tglperiksa     = date('d/m/Y', strtotime($tanggalperiksa));
                        $idpoliklinik   = $jadwal['idunit'];
                        $idjadwal       = $jadwal['idjadwal'];

                        //siapkan data pendaftaran, idkelas [1=Rawat Jalan]
                        $data = array(
                            'norm'=>$norm,
                            'caradaftar'=> 'mobilejkn',
                            'sudahsingkronjkn' => 1,
                            'nokontrol' => (($jeniskunjungan == 3) ? $nomorreferensi : 0 ),
                            'norujukan' => (($jeniskunjungan == 3) ? 0 : $nomorreferensi ),
                            'idjeniskunjungan' => $jeniskunjungan,
                            'idunit'=>$idpoliklinik,
                            'waktu'=>$waktu,
                            'waktuperiksa'=>$waktuperiksa,
                            'jenispemeriksaan'=>'rajal',
                            'carabayar'=>'jknnonpbi',
                            'idkelas'=>1 //idkelas [1=Rawat Jalan]
                        );

                        //validasi tanggal rujukan
//                        $cektglrujukan = $this->db->query("select pp.tanggalrujukan from person_pendaftaran pp where pp.norm='".$norm."' and pp.norujukan='".$nomorreferensi."' and idstatuskeluar='2' limit 1")->row_array()['tanggalrujukan'];
//                        if(empty(!$cektglrujukan) && tanggallebihdari_hari($cektglrujukan,$tanggalperiksa,90) )
//                        { 
//                            $this->response(["metadata" =>['code' => 201,'message' => 'Tanggal Periksa Tidak Boleh Melebihi 90 Hari dari Tanggal Rujukan.']], REST_Controller::HTTP_OK); 
//                        }

                        // cek apakah rujukan pasien belum terlayani
                        $cekdaftar = $this->db->query("select rp.idpemeriksaan from rs_pemeriksaan rp join  person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran where rp.norm='".$norm."' and pp.norujukan='".$nomorreferensi."' and pp.waktuperiksa='".$tanggalperiksa."' and rp.`status`!='selesai' and rp.status !='batal'");
                        if($cekdaftar->num_rows() > 0)
                        { 
                            $this->response(["metadata" =>['code' => 201,'message' => 'Nomor Antrean Hanya Dapat Diambil 1 Kali Pada Tanggal Yang Sama']], REST_Controller::HTTP_OK); 
                        }
                        // cek apakah pasien belum terlayani
                        $cekdaftar = $this->db->query("select rp.idpemeriksaan from rs_pemeriksaan rp join  person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran where rp.norm='".$norm."' and rp.`status`!='selesai' and rp.status !='batal'");
                        if($cekdaftar->num_rows() > 0)
                        { 
                            $this->response(["metadata" =>['code' => 201,'message' => 'Pasien Masih Terdeteksi Belum Dilayani, Mohon Menghubungi Bagian Pendaftaran RS.']], REST_Controller::HTTP_OK); 
                        }

                        // cek apakah melebihi jam pendaftaran poli
                        //$cekdaftar = $this->db->query("select rp.idpemeriksaan from rs_pemeriksaan rp join  person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran where rp.norm='".$norm."' and pp.norujukan='".$nomorreferensi."' and pp.waktuperiksa='".$tanggalperiksa."' and rp.`status`!='selesai' and rp.status !='batal'");
                        $cekdaftar = $this->db->query("select idunit, namaunit from rs_unit where kodebpjs ='".$kodepoli."'");
                        date_default_timezone_set("Asia/Bangkok");
                        // if(time() >= $jampraktek[1])  // jika jam ambil antrian lebih besar dari jam tutup poli
                        if((date("Y-m-d",time()) >= $tanggalperiksa) && (date("h:i:s",time()) >= $jampraktek[1] ))
                        {  //_andri_
                            if($cekdaftar->num_rows() > 0)
                            {
                                $data = $cekdaftar->row_array();
                                $this->response(["metadata" =>['code' => 201,'message' => 'Pendaftaran Ke Poli '.$data['namaunit'].' Sudah Tutup Jam '.$jampraktek[1] ]], REST_Controller::HTTP_OK); 
                            } else
                            {
                                $this->response(["metadata" =>['code' => 201,'message' => 'Pendaftaran Ke Poli ini Sudah Tutup Jam '.$jampraktek[1]]], REST_Controller::HTTP_OK); 
                            }
                        }

                        // cek apakah Belum terdaftar Rekam Medisnya
                        // $cekdaftar = $this->db->query("select norm from person_pasien where norm='".$norm."'");
                        if($cekdaftar->num_rows() == 0) //cek norm
                        { 
                            $this->response(["metadata" =>['code' => 202,'message' => 'Data pasien ini tidak ditemukan, silahkan Melakukan Registrasi Pasien Baru']], REST_Controller::HTTP_OK); 
                        }

                        //simpan pendaftaran
                        $this->ql->person_pendaftaran_insert_or_update(null, $data);

                        //select data pendaftaran
                        $result_pdf = $this->db->query("select pp.idpendaftaran, perpas.idperson from person_pendaftaran pp join person_pasien perpas ON pp.norm=perpas.norm where pp.norm='".$norm."' and waktu='".$waktu."' limit 0,1")->result_array();
                        // $result_pdf = $this->db->query("select idpendaftaran from person_pendaftaran where norm='".$norm."' and waktu='".$waktu."' limit 0,1")->result_array();
                        if (is_array($result_pdf))
                        {
                            $idpendaftaran  = $result_pdf[0]['idpendaftaran'];
                            
                            //simpan ke tabel bridging vclaim
                            $dtbridging = [
                                'nomorreferensi' => $nomorreferensi,
                                'idpendaftaran' => $idpendaftaran,
                            ];
                            $this->db->insert('bpjs_bridging_vclaim',$dtbridging);
                            
                            //buat nopesan
                            $callid         = generaterandom(8);
                            $this->ql->antrianset_nopesan($waktuperiksa,$callid,$idpoliklinik); //buat no pesan
                            $this->mgenerikbap->setTable('helper_autonumber');
                            $nopesan        = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array(); //siapkan data no antrian

                            //simpan nopesan ke tabel pemeriksaan
                            $updatedtperiksa= $this->pendaftaranpoli_savedaftarpemeriksaan($idpendaftaran,$idjadwal,$nopesan['number'],$norm,$callid,$tglperiksa);

                            $idpemeriksaan = $this->db->select('idpemeriksaan')->get_where('rs_pemeriksaan',['idpendaftaran'=>$idpendaftaran])->row_array()['idpemeriksaan'];
                            $this->db->query("update person_pendaftaran set isantri=1 where idpendaftaran='".$idpendaftaran."'");
                            
                            //buat antrian periksa
                            $dataantrian = $this->antrianset_antrianperiksa($idpoliklinik,$idpemeriksaan,$result_pdf[0]['idperson']);

                            $this->response([ "response"=>$dataantrian,"metadata" =>  [ 'code' => 200,'message' => 'Ok']], REST_Controller::HTTP_OK);
                        }
                        else 
                        {
                            $this->response(["metadata" =>['code' => 201,'message' => 'Proses pendaftaran gagal']], REST_Controller::HTTP_OK);
                        }
                    }
                    else
                    { 
                        $namadokter = $this->db->query("SELECT perpeg.titeldepan, perper.namalengkap, perpeg.titelbelakang FROM person_pegawai perpeg
                        JOIN person_person perper ON perper.idperson= perpeg.idperson 
                        AND perpeg.bpjs_kodedokter='".$kodedokter."'")->row_array();
                        $this->response(["metadata" =>['code' => 201,'message' => 'Jadwal Dokter '.$namadokter['titeldepan'].' '.$namadokter['namalengkap'].' '.$namadokter['titelbelakang'].' Tersebut Belum Tersedia, Silahkan Reschedule Tanggal dan Jam Praktek Lainnya' ]], REST_Controller::HTTP_OK);
                    }
                }
                else
                {
                    $this->response(["metadata" =>['code' => 201,'message' => 'Format Tanggal Tidak Sesuai, format yang benar adalah yyyy-mm-dd']],REST_Controller::HTTP_OK); 
                }
            }
        }
        else 
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Token Expired' ]], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    
    /**
     * Melihat sisa antrean di hari H pelayanan
     * Method Post
     * "kodebooking" : "{kodebooking yang unik yang diambil dari WS Ambil Antrean}"
     * mahmud, clear
     */
    public function sisa_antrian_post()
    {
        if ($this->cek_token())
        { 
            $post=(object)$this->get_post_callback();

            $kodebooking = trim($post->kodebooking);
            
            if(empty($kodebooking))
            {
                $this->response(["metadata" =>['code' => 201,'message' => 'Kode Booking Harap Diisi.']],REST_Controller::HTTP_OK); 
            }
            else
            {
                $res = $this->db->query("SELECT  aa.no, ru.namaunit, namadokter(rj.idpegawaidokter) as namadokter, al.lamapelayanan,
                    (select ifnull(max(rp.noantrian),0) from rs_pemeriksaan rp WHERE rp.idjadwal = rj.idjadwal and rp.status != 'antrian' and rp.status != 'batal') as antreanpanggil
                FROM antrian_antrian aa
                join antrian_loket al on al.idloket = aa.idloket
                join rs_pemeriksaan rp on rp.idpemeriksaan = aa.idpemeriksaan
                join rs_unit ru on ru.idunit = rp.idunit
                join rs_jadwal rj on rj.idjadwal = rp.idjadwal
                WHERE aa.kodebooking='".$kodebooking."'")->result_array();

                if(empty($res))
                {
                    $this->response(["metadata" =>['code' => 201,'message' => 'Antrean Tidak Ditemukan']],REST_Controller::HTTP_OK); 
                }
                else
                {
                    $sisa   = (($res[0]['antreanpanggil'] <= $res[0]['no'] ) ? intval($res[0]['no']) - intval($res[0]['antreanpanggil']) : '-' );                    
                    $waktutunggu = intval($res[0]['lamapelayanan']) * (intval($sisa) - 1);
                    $resarr ='';
                    $resarr = [
                        'nomorantrean' =>$res[0]['no'],
                        'namapoli'=>$res[0]['namaunit'], 
                        'namadokter'=>$res[0]['namadokter'],
                        'sisaantrean'=> $sisa,
                        'antreanpanggil'=>$res[0]['antreanpanggil'],
                        'waktutunggu'=>$waktutunggu,
                        'keterangan'=> ''
                    ];
                    $this->response(["response"=>$resarr,"metadata" =>['code' => 200,'message' => 'Ok']],REST_Controller::HTTP_OK);     
                }
            }
        }
        else 
        {
            $this->response(["metadata" =>  [ 'code' => 201,'message' => 'Token Expired' ]], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * Get Post Data Callback
     * @return type
     */
    private function get_post_callback() 
    {
        $g = file_get_contents('php://input');
        $post = json_decode($g);        
        return $post;
    }

    /**
     * Get Post Data
     * @param type $id
     * @return type
     */
    private function get_post($id) // -- ok
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }

    /**
     * Read Token
     * @return type
     */
    private function read_token() // -- ok
    {
        $headers = apache_request_headers();
        if(file_exists("./QLKP.php")){
            return (array_key_exists('x-token', $headers)) ? $headers['x-token'] : ''; // for QL KP
        } else {
            return (array_key_exists('X-Token', $headers)) ? $headers['X-Token'] : ''; // for QL J
        }
    }
    /**
     * Read Username
     * @return type
     */
    private function read_username()
    {
        $headers = apache_request_headers();
        if(file_exists("./QLKP.php")){
            return (array_key_exists('x-username', $headers)) ? $headers['x-username'] : ''; // for QL KP
        } else {
            return (array_key_exists('X-Username', $headers)) ? $headers['X-Username'] : ''; // for QL J
        }
    }
    
    /**
     * Read Password
     * @return type
     */
    private function read_password()
    {
        $headers = apache_request_headers();
        if(file_exists("./QLKP.php")){
            return (array_key_exists('x-password', $headers)) ? $headers['x-password'] : '';  // for QL KP
        } else {
            return (array_key_exists('X-Password', $headers)) ? $headers['X-Password'] : '';  // for QL J
        }
    }
    
}