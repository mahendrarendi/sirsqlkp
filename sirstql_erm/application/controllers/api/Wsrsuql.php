<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wsrsuql extends REST_Controller {
    private $Xconsid;
    private $Xconssecret;
    private $Xtimestamp;
    private $Xjeniskoneksi;
    private $Xsignature;
    
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
//        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
//        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
    //mahmud, clear
    private function decodebase64()
    {
        $decode = base64_encode(hash_hmac('sha256',$this->Xconsid.'&'.$this->Xtimestamp,$this->Xconssecret, true));
        return ($decode==$this->Xsignature) ? true : false ;
    }
    //mahmud, clear
    // ceka akses api
    private function cek_signature()
    {
        $headers = apache_request_headers();
        $this->Xconsid    = $headers['X-cons-id'];
        $this->Xtimestamp = $headers['X-timestamp'];
        $this->Xsignature = $headers['X-signature'];
        $this->Xjeniskoneksi = $headers['X-jeniskoneksi'];
        
        if($this->Xsignature)
        {
            switch ($this->Xjeniskoneksi) {
                case 'QLSURVEY': $this->Xconssecret='aPPsurv3y'; break;
                case 'QL': $this->Xconssecret='xx'; break;
            }
            return ($this->decodebase64()) ? true : false ;
        }
        return false;
        
    }

    private function get_post($id)
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }
    //mahmud, clear
    public function reqdatamastersurvey_post()
    {
        if($this->cek_signature())
        {
            $response = array(
                'harapan'=>$this->db->get('survey_harapan sh')->result_array(),
                'pertanyaan'=>$this->db->select('sp.*, sj.idjawaban, sj.jawaban')->join('survey_jawaban sj','sj.idjenispertanyaan=sp.idjenispertanyaan')->get('survey_pertanyaan sp')->result_array(),
                'pendidikan'=>$this->db->get('demografi_pendidikan')->result_array(),
                'pekerjaan'=>$this->db->get('demografi_pekerjaan')->result_array(),
                'layanan'=>$this->db->get('survey_jenissurvey')->result_array()
                );
            $this->response(["metadata" =>  [ 'response'=>$response, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }
    
    //mahmud, clear
    public function savedatasurvey_post()
    {
        if($this->cek_signature())
        {
            if($this->savedatasurvey_()) //bug saat simpan, berhasil simpan tetapi terbaca false
            {
                $this->response(["metadata" =>  ["response"=>'Survey berhasil disimpan, terimakasih atas partisipasinya.!','code' => 200,'message' => 'Ok']], REST_Controller::HTTP_OK);    
            }
            else
            {
                $this->response(["metadata" =>  ["response"=>'Error save.!','code' => 200,'message' => 'Error.!']], REST_Controller::HTTP_OK);
            }
            
        }
        else
        {
            $this->response(["metadata" =>  ['code' => 401,'message' => 'UNAUTHORIZED.!']], REST_Controller::HTTP_UNAUTHORIZED);
        }
    }

    private function savedatasurvey_()
    {
        //siapkan data
            $post = $this->get_post('data');
            $browser = sql_clean(trim($post['browser']));
            $mobile = sql_clean(trim($post['mobile']));
            $os = sql_clean(trim($post['os']));
            $ip = sql_clean(trim($post['ip']));
            $idpendidikan = sql_clean(trim($post['survey']['pendidikan']));
            $idpekerjaan = sql_clean(trim($post['survey']['pekerjaan']));
            $nama = sql_clean(trim($post['survey']['nama']));
            $usia = sql_clean(trim($post['survey']['usia']));

            $idjenissurvey = sql_clean(trim($post['survey']['jenissurvey']));
            $pertanyaan = $post['survey']['pertanyaan'];
            $harapan = $post['survey']['harapan'];

            //simpan data pengisi
            $save = $this->db->insert('survey_identitas',['nama'=>$nama,'usia'=>$usia,'idpekerjaan'=>$idpekerjaan,'idpendidikan'=>$idpendidikan,'browser'=>$browser,'ip'=>$ip,'os'=>$os,'mobile'=>$mobile]);
            $ididentitas = $this->db->insert_id();
            if(!$save){$this->response(["metadata" =>  ['code' => 200,'message' => 'Gagal simpan.']], REST_Controller::HTTP_OK); return;}

            //simpan data jawaban
            $no=0;
            for($x=0; $X<count($post['survey']['pertanyaan']); $X++)
            {
                ++$no;
                $this->db->insert('survey_detailjawaban',['idjawaban'=>$post['survey']['idjawaban'.$no],'idpertanyaan'=>$post['survey']['pertanyaan'][$x],'ididentitas'=>$ididentitas,'idjenissurvey'=>$idjenissurvey]);
                if(!$save){$this->response(["metadata" =>  ['code' => 200,'message' => 'Gagal simpan.']], REST_Controller::HTTP_OK); return;}
            }
            //simpan data harapan
            for($y=0; $y< count($harapan); $y++)
            {
                $idharapan = $post['survey']['harapan'][$y];
                for($z=0; $z<count($post['survey']['isiharapan'.$idharapan]); $z++)
                {
                    $isiharapan = sql_clean(trim($post['survey']['isiharapan'.$idharapan][$z]));
                    if(!empty($isiharapan))
                    {
                        $this->db->insert('survey_detailharapan',['isiharapan'=> $isiharapan,'idharapan'=>$idharapan,'ididentitas'=>$ididentitas,'idjenissurvey'=>$idjenissurvey]);
                        if(!$simpanharapan){$this->response(["metadata" =>  ['code' => 200,'message' => 'Gagal simpan.']], REST_Controller::HTTP_OK); return;}
                    }
                }      
            }
            return true;
    }

    public function antrian_post()
    {
        $tgl = (empty($this->get_post('tgl'))) ? 'now()' : "'".$this->get_post('tgl')."'";
        $sql = "select `l`.`idstasiun` AS `idstasiun`,
                `l`.`idloket` AS `idloket`,
                concat(`l`.`namaloket`, ' ', if(not isnull(j.tanggal), concat('(', DATE_FORMAT(j.tanggal, '%H:%i'), ' - ', DATE_FORMAT(j.jamakhir, '%H:%i'), ')', if(j.jamakhir<time($tgl), ' -selesai-', '')), '')) AS `namaloket`,
                fontawesome, 
                (select count(1) from antrian_antrian where tanggal = date($tgl) and idloket = a.idloket ) as totaldiambil, 
                max(if(a.status='sudah panggil' or a.status='sedang proses',`a`.`no`, 0)) AS `no`,
                (select count(idpemeriksaan) from rs_pemeriksaan where idloket = a.idloket AND status='batal' AND date(waktu) = date($tgl) ) as batal
                from antrian_antrian a 
                join antrian_loket l on l.idloket=a.idloket 
                join rs_jadwal j on j.idloket=l.idloket 
                where a.tanggal=date($tgl) and date(j.tanggal)=date($tgl) 
                group by idloket ORDER by (j.jamakhir<time($tgl)), time($tgl)-time(j.tanggal) desc, namaloket, a.no";
                
        $antrian = $this->db->query($sql)->result_array();
        if ($antrian)
        {
            $data['antrian'] = $antrian;
            $data['waktu']   = DATE('Y-m-d H:i:s');
            $this->response(["metadata" =>  [ 'response'=>$data, 'code' => 200,'message' => 'OK']], REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response(["metadata" =>  [ 'response'=>'', 'code' => 200,'message' => 'Data Tidak Ada']], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    
}