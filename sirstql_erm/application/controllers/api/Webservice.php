<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
/**
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Webservice extends REST_Controller {
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
//        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
//        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
//        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function riwayatdiagnosa_get()
    {
        $result = $this->db->query("select p.waktuperiksa,u.namaunit, h.icd, i.namaicd FROM rs_hasilpemeriksaan_my h, person_pendaftaran_my p, rs_icd_my i, rs_unit u where p.idpendaftaran=h.idpendaftaran and p.norm='".$this->get_post('id')."' and i.icd=h.icd and i.idicd='10' and u.idunit=p.idunit")->result_array();
        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Riwayat Diagnosa Tidak Ditemukan.!'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function antrian_get()
    {
        $tgl = (empty($this->get_post('tgl'))) ? 'now()' : "'".$this->get_post('tgl')."'";
        $antrian = $this->db->query("select `l`.`idstasiun` AS `idstasiun`,`l`.`idloket` AS `idloket`,concat(`l`.`namaloket`, ' ', if(not isnull(j.tanggal), concat('(', DATE_FORMAT(j.tanggal, '%H:%i'), ' - ', DATE_FORMAT(j.jamakhir, '%H:%i'), ')', if(j.jamakhir<time($tgl), ' -selesai-', '')), '')) AS `namaloket`,fontawesome, (select count(1) from antrian_antrian where tanggal = date($tgl) and idloket = a.idloket ) as totaldiambil, max(if(a.status='sudah panggil' or a.status='sedang proses' or a.status='ok',`a`.`no`, 0)) AS `no` from antrian_antrian a join antrian_loket l on l.idloket=a.idloket join rs_jadwal j on j.idloket=l.idloket where a.tanggal=date($tgl) and date(j.tanggal)=date($tgl) group by idloket ORDER by (j.jamakhir<time($tgl)), time($tgl)-time(j.tanggal) desc, namaloket, a.no")->result_array();
        if ($antrian)
        {
            $this->response($antrian, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Antrian Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function icdlab_get()
    {
        $this->mgenerikbap->setTable("rs_icd");
        $this->db->where("idjenisicd", 4);
        $icd = $this->mgenerikbap->select("*")->result_array();
        if ($icd)
        {
            $this->response($icd, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'ICD Tidak ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function masterjadwal_get()
    {
        $id = $this->get_post('id');
        $this->mgenerikbap->setTable("vrs_masterjadwal");
        $jadwal = $this->mgenerikbap->select("*")->result_array();
        if ($jadwal)
        {
            $this->response($jadwal, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Master Jadwal Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function jadwalsepekan_get()
    {
        $id = $this->get_post('id');
        $this->mgenerikbap->setTable("vrs_jadwal1pekan");
        $jadwal = $this->mgenerikbap->select("*")->result_array();
        if ($jadwal)
        {
            $this->response($jadwal, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Jadwal Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    public function pemeriksaanlabranap_get()
    {
        $id = $this->get_post('id');
        $this->db->select("rhp.idrencanamedishasilpemeriksaan as idhasilpemeriksaan, ppd.norm, pper.namalengkap, rhp.icd, rhp.nilai, rhp.nilaitext, rhp.remark");
        $this->db->from("person_person pper, person_pasien pps, person_pendaftaran ppd, rs_inap_rencana_medis_hasilpemeriksaan rhp, rs_icd ricd");
        $this->db->where("ricd.icd = rhp.icd");
        $this->db->where("ppd.idpendaftaran = rhp.idpendaftaran");
        $this->db->where("pps.norm = ppd.norm");
        $this->db->where("pper.idperson = pps.idperson");
        $this->db->where("idrencanamedispemeriksaan", $id);
        $this->db->where("ricd.idjenisicd", 4);            
        
        $pemeriksaan = $this->db->get()->result_array();
        
        $query = $this->db->last_query();
        if ($pemeriksaan)
        {
            $this->response($pemeriksaan, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Pemeriksaan Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    
    
    public function pemeriksaanlabranap_put()
    {
        $id     = $this->get_post("id"); // get idhasilpemeriksaan - ranap
        if ($id == -1)
        {
            $this->response([
                        'status' => TRUE,
                        'message' => 'Id = -1, Percobaan Update Berhasil'
                    ], REST_Controller::HTTP_OK);
        }
        else if (!empty($id))
        {
            $data   =   [ 
                            "nilai"     => $this->get_post('n'),
                            "nilaitext" => $this->get_post('txt'),
                            "remark"    => $this->get_post('rmk')
                        ];
            $this->mgenerikbap->setTable("rs_inap_rencana_medis_hasilpemeriksaan");
            $pemeriksaan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);
            
            if ($pemeriksaan)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Update Berhasil'
                    ], REST_Controller::HTTP_OK);
            }
            else 
            {
                $this->response([
                        'status' => FALSE,
                        'message' => 'Update Pemeriksaan Gagal'
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else 
        {
            $this->response([
                        'status' => FALSE,
                        'message' => 'Update Pemeriksaan Gagal, Id tidak boleh kosong'
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    
    /**
     * Get Pemeriksaan Laboratorium Rawat Jalan
     */
    public function pemeriksaanlab_get()
    {
        $id = $this->get_post('id');
        $this->db->select("rhp.idhasilpemeriksaan, ppd.norm, pper.namalengkap, rhp.icd, rhp.nilai, rhp.nilaitext, rhp.remark");
        $this->db->from("person_person pper, person_pasien pps, person_pendaftaran ppd, rs_hasilpemeriksaan rhp, rs_icd ricd");
        $this->db->where("ricd.icd = rhp.icd");
        $this->db->where("ppd.idpendaftaran = rhp.idpendaftaran");
        $this->db->where("pps.norm = ppd.norm");
        $this->db->where("pper.idperson = pps.idperson");
        $this->db->where("idpemeriksaan", $id);
        $this->db->where("ricd.idjenisicd", 4);
        $pemeriksaan = $this->db->get()->result_array();
        
        $query = $this->db->last_query();
        if ($pemeriksaan)
        {
            $this->response($pemeriksaan, REST_Controller::HTTP_OK);
        }
        else 
        {
            $this->response([
                    'status' => FALSE,
                    'message' => 'Pemeriksaan Tidak Ditemukan'
                ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    /**
     * Put Pemeriksaan Ranap Laboratorium
     */
    public function pemeriksaanlab_put()
    {
        $id     = $this->get_post("id"); // get idhasilpemeriksaan - ralan
        if ($id == -1)
        {
            $this->response([
                        'status' => TRUE,
                        'message' => 'Id = -1, Percobaan Update Berhasil'
                    ], REST_Controller::HTTP_OK);
        }
        else if (!empty($id))
        {
            $data   = [ 
                        "nilai"     => $this->get_post('n'),
                        "nilaitext" => $this->get_post('txt'),
                        "remark"    => $this->get_post('rmk')
                    ];
            
            $this->mgenerikbap->setTable("rs_hasilpemeriksaan");
            $pemeriksaan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $id);
            
            
            if ($pemeriksaan)
            {
                $this->response([
                        'status' => TRUE,
                        'message' => 'Update Berhasil'
                    ], REST_Controller::HTTP_OK);
            }
            else 
            {
                $this->response([
                        'status' => FALSE,
                        'message' => 'Update Pemeriksaan Gagal'
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
        else 
        {
            $this->response([
                        'status' => FALSE,
                        'message' => 'Update Pemeriksaan Gagal, Id tidak boleh kosong'
                    ], REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    
    private function get_post($id)
    {
        $i  = $this->get($id);
        if (empty($i))
        {
            $i  = $this->post($id);
        }
        return $i;
    }
    
//    public function users_get_post()
//    {
//        // Users from a data store e.g. database
//        $users = [
//            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
//            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
//            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
//        ];
//        $id = $this->get_post('id');
//        // If the id parameter doesn't exist return all the users
//        if ($id === NULL)
//        {
//            // Check if the users data store contains users (in case the database result returns NULL)
//            if ($users)
//            {
//                // Set the response and exit
//                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
//            }
//            else
//            {
//                // Set the response and exit
//                $this->response([
//                    'status' => FALSE,
//                    'message' => 'No users were found'
//                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
//            }
//        }
//        // Find and return a single record for a particular user.
//        $id = (int) $id;
//        // Validate the id.
//        if ($id <= 0)
//        {
//            // Invalid id, set the response and exit.
//            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
//        }
//        // Get the user from the array, using the id as key for retrieval.
//        // Usually a model is to be used for this.
//        $user = NULL;
//        if (!empty($users))
//        {
//            foreach ($users as $key => $value)
//            {
//                if (isset($value['id']) && $value['id'] === $id)
//                {
//                    $user = $value;
//                }
//            }
//        }
//        if (!empty($user))
//        {
//            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
//        }
//        else
//        {
//            $this->set_response([
//                'status' => FALSE,
//                'message' => 'User could not be found'
//            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
//        }
//    }
//    public function users_post()
//    {
//        // $this->some_model->update_user( ... );
//        $message = [
//            'id' => 100, // Automatically generated by the model
//            'name' => $this->post('name'),
//            'email' => $this->post('email'),
//            'message' => 'Added a resource'
//        ];
//        $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
//    }
//    public function users_delete()
//    {
//        $id = (int) $this->get_post('id');
//        // Validate the id.
//        if ($id <= 0)
//        {
//            // Set the response and exit
//            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
//        }
//        // $this->some_model->delete_something($id);
//        $message = [
//            'id' => $id,
//            'message' => 'Deleted the resource'
//        ];
//        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
//    }
}