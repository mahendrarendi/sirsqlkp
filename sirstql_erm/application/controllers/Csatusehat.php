<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREAss
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Csatusehat extends MY_controller
{
    function __construct()
    {
       parent::__construct();
        // jika user belum login maka akses ditolak
        if ($this->session->userdata('sitiql_session') != 'aksesloginberhasil') {
            pesan_belumlogin();
        }
       $this->load->model('msatusehat');
       $this->load->helper('satusehat');
       $this->load->library('form_validation');

    }

    /////////////////////// API PASIEN SATUSEHAT ///////////////////
    /**
     * Pasien SatuSehat
     */
    public function find_patient()
    {
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_NAKES)) //lihat define di atas
        { 
           $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'by_id' );
           $data = [
               'content_view'      => 'satusehat/v_cari_pasien',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'satusehat_pasien',
               'active_menu_level' => '',
               'plugins'           => [],
               'tabActive'         => $tabActive,
              ];

           $data['title_page']     = 'Pencarian Data Pasien';
           $data['mode']           = 'view';
           $data['script_js']      = ['satusehat/cari_pasien'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    //tampil data peserta
    public function pasien_getdata_by_nik()
    {
        // $nik = "9201394901000008"; 
        $nik = $this->input->post('nik');
        $addurl = '/Patient?identifier=https://fhir.kemkes.go.id/id/nik|'.$nik; 
        $method = 'GET';
        $token = $this->satusehat->createSignature()['Authorization'];
        $peserta = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($peserta);
    }
    public function pasien_getdata_by_namegenderbirth()
    {
        $name = str_replace(' ','%',$this->input->post('name')); 
        $gender = $this->input->post('gender'); 
        $birth = $this->input->post('birth'); 
        $method = 'GET';
        $addurl = "/Patient?name=".$name."&birthdate=".$birth."&gender=".$gender; 
        $token = $this->satusehat->createSignature()['Authorization'];
        $peserta = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($peserta);
    }
    public function pasien_getdata_by_id()
    {
        $id = $this->input->post('id');
        $method = 'GET';
        $addurl = '/Patient/'.$id; 
        $token = $this->satusehat->createSignature()['Authorization'];
        $peserta = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($peserta);
    }
    /////////////////////// GET TENAGA KESEHATAN SATUSEHAT ///////////////////
    /**
     * MENAMPILKAN DATA-DATA TENAGA KESEHATAN DARI API SATUSEHAT
     * dev by Danang
     */

    public function find_practitioner()
    {
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_NAKES)) //lihat define di atas
        { 
           $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'by_id' );
           $data = [
               'content_view'      => 'satusehat/v_cari_practitioner',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'satusehat_nakes',
               'active_menu_level' => '',
               'plugins'           => [PLUG_DATE],
               'tabActive'         => $tabActive,
              ];

           $data['title_page']     = 'Pencarian Data Tenaga Kesehatan';
           $data['mode']           = 'view';
           $data['script_js']      = ['satusehat/cari_practitioner'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function nakes_getdata_by_nik()
    {
        $nik = $this->input->post('nik'); // nomor [nik/nokartu]
        $addurl = '/Practitioner?identifier=https://fhir.kemkes.go.id/id/nik|'.$nik;
        $method = 'GET';
        $token = $this->satusehat->createSignature()['Authorization'];
        $nakes = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($nakes);
    }
    public function nakes_getdata_by_namegenderbirth()
    {
        $name = str_replace(' ','%',$this->input->post('name')); 
        $gender = $this->input->post('gender'); 
        $birth = $this->input->post('birth');
        $method = 'GET';
        $addurl = '/Practitioner?name='.$name.'&gender='.$gender.'&birthdate='.$birth; 
      
        $token = $this->satusehat->createSignature()['Authorization'];
        $nakes = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($nakes);
    }
    public function nakes_getdata_by_id()
    {
        $id = $this->input->post('id'); 
        $method = 'GET';
        $addurl = '/Practitioner/'.$id; 
        $token = $this->satusehat->createSignature()['Authorization'];
        $nakes = $this->satusehat->requestSatuSehat($addurl, $method, $token);
        echo json_encode($nakes);
    }
    public function update_ihs_nakes()
    {
        $idihs = $this->input->post('ihspractitioner');
        $nik = $this->input->post('nik');
        $update = $this->msatusehat->update_ihs_nakes($nik, $idihs);
        pesan_success_danger($update,"IHS Berhasil disimpan","IHS gagal disimpan","js");
    }

    /////////////////////// GET LOKASI SATUSEHAT ///////////////////
    /**
     * START GET LOKASI SATUSEHAT
     * MENAMPILKAN DATA-DATA LOKASI DARI API SATUSEHAT
     * dev by Danang
     */
    public function find_location()
    {   
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas
        { 
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'by_identifier' );
            $data = [
               'content_view'      => 'satusehat/v_cari_lokasi',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'location_satusehat',
               'active_menu_level' => 'find_location',
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE],
               'tabActive'         => $tabActive,
              ];

            $data['title_page']     = 'Cari Lokasi';
            $data['mode']           = 'view';
            $data['script_js']      = ['satusehat/registrasi_struktur_lokasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function location_getidentifier()
    {
        $param = $this->input->post('param');
        
        $url = "/Location?identifier=http://sys-ids.kemkes.go.id/location/1000001%7C".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $location = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($location);
    }

    public function location_getname()
    {
        $param = $this->input->post('param');
        
        $url = "/Location?name=".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $location = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($location);
    }

    public function location_getorgid()
    {
        $param = $this->input->post('param');
        
        $url = "/Location?name=".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $location = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($location);
    }

    public function location_getid()
    {
        $param = $this->input->post('param');
        
        $url = "/Location/".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $location = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($location);
    }

    /////////////////////// CRUD LOKASI ///////////////////
    /**
     * MENYIAPKAN DATA LOKASI SEBELUM DIKIRIM KE SATU SEHAT
     * DISIMPAN KE TABEL satusehat_location dan satusehat_sub_location
     * dev by Danang
     */
    public function location()
     {
        $get = $this->input->get();
        $sub_location = $this->msatusehat->get_listsublocation();
        $location = $this->msatusehat->get_listlocation();
        
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas
        { 
           $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'data_lokasi' );
           $data = [
                //terminology
               'status'            => location_get_status(),
               'operational_status'=> location_get_operational_status(),
               'mode'              => location_get_mode(),
               'physical_type'     => location_get_physical_type(),
               'service_class'     => location_get_service_class(),
               'type'              => location_get_type(),

               //list data
               'location'           => $location,
               'sub_location'       => $sub_location,
               'title_page'        => 'Struktur Lokasi',
               'mode'              => 'view',
               'script_js'         => ['satusehat/registrasi_struktur_lokasi'],
               'tabActive'         => $tabActive,

               'content_view'      => 'satusehat/v_lokasi_ruang',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'location_satusehat',
               'active_menu_level' => 'location',
               'plugins'           => [PLUG_DATATABLE]
              ];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
     }

     public function add_location()
     {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas
        { 
           $data = [
               'content_view'      => 'satusehat/v_lokasi_ruang',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'satusehat_location',
               'active_menu_level' => '',

               'bangsal'       => $this->msatusehat->get_bangsal(),
               'unit'          => $this->msatusehat->get_unit(),
               'stasiun'       => $this->msatusehat->get_stasiun(),

               //get terminology array
               'location_get_identifier_use'    => location_get_identifier_use(),
               'location_get_status'            => location_get_status(),
               'location_get_operational_status'=> location_get_operational_status(),
               'location_get_mode'              => location_get_mode(),
               'location_get_service_class'     => location_get_service_class(),
               'location_get_physical_type'     => location_get_physical_type(),
               'location_get_service_class'     => location_get_service_class(),
               'location_get_type'              => location_get_type(),

               'plugins'           => [PLUG_DATE]
              ];

           $data['title_page']     = 'Tambah Struktur Lokasi';
           $data['mode']           = 'add_lokasi';
           $data['data_edit']       = '';
           $data['script_js']      = ['satusehat/registrasi_struktur_lokasi'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
     }
     public function edit_location(){
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas            
        {
            $this->load->helper('form');
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $this->mgenerikbap->setTable('satusehat_location');
            
            $data = [
                'content_view'      => 'satusehat/v_lokasi_ruang',
                'active_menu'       => 'satusehat',
                'active_sub_menu'   => 'satusehat_location',
                'active_menu_level' => '',
 
                'bangsal'       => $this->msatusehat->get_bangsal(),
                'unit'          => $this->msatusehat->get_unit(),
                'stasiun'       => $this->msatusehat->get_stasiun(),
 
                //get terminology array
                'location_get_identifier_use'    => location_get_identifier_use(),
                'location_get_status'            => location_get_status(),
                'location_get_operational_status'=> location_get_operational_status(),
                'location_get_mode'              => location_get_mode(),
                'location_get_service_class'     => location_get_service_class(),
                'location_get_physical_type'     => location_get_physical_type(),
                'location_get_service_class'     => location_get_service_class(),
                'location_get_type'              => location_get_type(),
 
                'plugins'           => [PLUG_DROPDOWN],
                'data_edit'         => $this->mgenerikbap->select('*', ['idlocation' => $id])->row_array(),
               ];
            $data['title_page']     = 'Edit Struktur Lokasi';
            $data['mode']           = 'edit_lokasi';
            $data['script_js']      = ['satusehat/registrasi_struktur_lokasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
     }
     public function save_location(){
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas            
        {
            // if(validation_input()){
                $idlocation         = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id')));
                $tiperuang          = $this->input->post('tiperuang');
                $idpoli             = !empty($this->input->post('idunit')) ? $this->input->post('idunit') : '';
                $namaruang          = $this->input->post('namaruang');
                $koderuang          = $this->input->post('koderuang');            
                $deskripsi          = $this->input->post('deskripsi');
                $telepon            = $this->input->post('telepon');
                $status             = $this->input->post('status');
                $statusoperasional  = $this->input->post('statusoperasional');
                $tipefisik          = $this->input->post('tipefisik');
                $mode               = $this->input->post('mode');
                $kelas              = $this->input->post('kelas');
                $tipefungsi         = $this->input->post('tipefungsi');
                $tr                 = '';
                if($tiperuang === "get_bangsal"){
                    $tr = "bangsal";
                }else if ($tiperuang === "get_stasiun"){
                    $tr = "stasiun";
                }else{
                    $tr = "unit";
                }
                $datalocation = [
                    'namaruang' => $namaruang,
                    'idpoli' => $idpoli,
                    'koderuangan' => $koderuang,
                    'deskripsi' => $deskripsi,
                    'telepon' => $telepon,
                    'status' => $status,
                    'operationalstatus' => $statusoperasional,
                    'physicaltype' => $tipefisik,
                    'mode' => $mode,  
                    'type' => $tipefungsi,
                    'jenisruang' => $tr
                ];
                // print_r($datalocation);
                if($tiperuang === "get_bangsal"){
                    // $this->form_validation->set_rules('kelas','','required');
                    // if(validation_input()){
                        $kelasArray = ['kelas' => $kelas];
                        $datalocation = array_merge($datalocation, $kelasArray);
                        if(!empty($idlocation)){
                            $query =  $this->db->update('satusehat_location',$datalocation, array('idlocation'=>$idlocation));
                            echo json_encode(['msg'=>'success update','txt'=>'Data berhasil diupdate pada local database']);
                        }else{
                            $query = $this->db->insert('satusehat_location', $datalocation);
                            echo json_encode(['msg'=>'success insert','txt'=>'Data Berhasil disimpan']);
                        }
                        // }
                }else{
                    if(!empty($idlocation)){
                        $query =  $this->db->update('satusehat_location',$datalocation, array('idlocation'=>$idlocation));
                        echo json_encode(['msg'=>'success update','txt'=>'Data berhasil diupdate pada local database']);
                    }else{
                        $query = $this->db->insert('satusehat_location', $datalocation);
                        echo json_encode(['msg'=>'success insert','txt'=>'Data Berhasil disimpan']);
                    }
                }
            // }
        }
    }

     public function get_kelas() {
        $kelas_data = $this->msatusehat->get_kelas();
        echo json_encode($kelas_data);
     }
     public function get_unit() {
        $this->mgenerikbap->setTable('satusehat_location');
        $lq = $this->mgenerikbap->select('idpoli')->result();
        $uq = $this->msatusehat->get_unit();
        $location = array_map(function($item){
            return $item->idpoli;
        }, $lq);
        $unit = array_map(function($item){
            return [
                'idunit' => $item->idunit,
                'namaruang' => $item->namaruang
            ];          
        }, $uq);
        $unitIDs = array_map(function($item) {
            return $item['idunit'];
        }, $unit);
        $data = array_diff($unitIDs, $location);

        $resultWithID = array_values(array_filter($unit, function($item) use ($data) {
            return in_array($item['idunit'], $data);
        }));
        echo json_encode($resultWithID); 
    }
    
    //  public function get_unit() {
    //     $this->mgenerikbap->setTable('satusehat_location');
    //     $lq = $this->mgenerikbap->select('namaruang')->result();
    //     $uq = $this->msatusehat->get_unit();
    //     $location = array_map(function($item){
    //         return $item->namaruang;
    //     }, $lq);
    //     $unit = array_map(function($item){
    //         return $item->namaruang;           
    //     }, $uq);
    //     $data = array_diff($unit,$location);
    //     echo json_encode($data);
    // }
    public function get_bangsal() {
        $this->mgenerikbap->setTable('satusehat_location');
        $lq = $this->mgenerikbap->select('namaruang')->result();
        $bq = $this->msatusehat->get_bangsal();
        $location = array_map(function($item){
            return $item->namaruang;
        }, $lq);
        $bangsal = array_map(function($item){
            return $item->namaruang;           
        }, $bq);
        $data = array_diff($bangsal,$location);
        echo json_encode($data);
    }

    public function get_stasiun() {
        $this->mgenerikbap->setTable('satusehat_location');
        $lq = $this->mgenerikbap->select('namaruang')->result();
        $sq = $this->msatusehat->get_stasiun();
        $location = array_map(function($item){
            return $item->namaruang;
        }, $lq);
        $stasiun = array_map(function($item){
            return $item->namaruang;           
        }, $sq);
        $data = array_diff($stasiun,$location);
        echo json_encode($data);
    }
     public function add_sub_location()
     {
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas
        { 
           $data = [
               'content_view'      => 'satusehat/v_lokasi_ruang',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'satusehat_location',
               'active_menu_level' => '',
               //get terminology array
               'location_get_identifier_use'    => location_get_identifier_use(),
               'location_get_status'            => location_get_status(),
               'location_get_operational_status'=> location_get_operational_status(),
               'location_get_mode'              => location_get_mode(),
               'location_get_physical_type'     => location_get_physical_type(),
               'location_get_service_class'     => location_get_service_class(),
               'location_get_type'              => location_get_type(),

               'data_edit'         => '',
               'plugins'           => []
              ];

           $data['title_page']     = 'Tambah Struktur Sub Lokasi';
           $data['mode']           = 'add_sub_lokasi';
           $data['script_js']      = ['satusehat/registrasi_struktur_lokasi'];
           $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
     }
     public function get_sub_location(){
        $post_location = $this->input->post('value');
        $data_sub_bangsal = $this->msatusehat->get_bed($post_location);
        $data_sub_stasiun = $this->msatusehat->get_loket($post_location);
        $slq = $this->mgenerikbap->select_multitable('namaruang', 'satusehat_sub_location')->result();
        $sub_location = array_map(function($item){
            return $item->namaruang;
        }, $slq);

        if (empty($data_sub_bangsal)) {
            $l = array_map(function($item){
                return $item->namaruang;           
            }, $data_sub_stasiun);
            $loket = array_diff($l,$sub_location);
            echo json_encode($loket);
        }else{
            $b = array_map(function($item){
                return $item->namaruang;           
            }, $data_sub_bangsal);
            $bed = array_diff($b,$sub_location);
            echo json_encode($bed);
        }
     }

    public function get_bangsal_sub() {
        $bq = $this->msatusehat->get_bangsal();
        echo json_encode($bq);
    }

    public function get_stasiun_sub() {
        $sq = $this->msatusehat->get_stasiun();
        echo json_encode($sq);
    }

    public function get_location_in_sub_location(){
        $sl = $this->msatusehat->get_location_in_sub_location();
        echo json_encode($sl);
    }
    public function edit_sub_location(){
        if ($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)) //lihat define di atas            
        {            
            $this->load->helper('form');
            $idsublocation = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data_edit = $this->msatusehat->get_one_sub_location($idsublocation);
            // print_r($data_edit);
            $data = [
            'content_view'      => 'satusehat/v_lokasi_ruang',
            'active_menu'       => 'satusehat',
            'active_sub_menu'   => 'satusehat_location',
            'active_menu_level' => '',
            //get terminology array
            'location_get_identifier_use'    => location_get_identifier_use(),
            'location_get_status'            => location_get_status(),
            'location_get_operational_status'=> location_get_operational_status(),
            'location_get_mode'              => location_get_mode(),
            'location_get_physical_type'     => location_get_physical_type(),
            'location_get_service_class'     => location_get_service_class(),
            'location_get_type'              => location_get_type(),
            
            'data_edit'         => $data_edit, 
            'plugins'           => []
           ];

            $data['title_page']     = 'Edit Struktur Sub Lokasi';
            $data['mode']           = 'edit_sub_lokasi';
            $data['script_js']      = ['satusehat/registrasi_struktur_lokasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
     }
    public function save_sub_location(){
        if ($this->pageaccessrightbap->checkAccessRight(V_USER)) //lihat define di atas            
        {
            // if(validation_input()){
                $idsublocation      = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id')));
                $ruangutama         = $this->input->post('ruangutama');
                $namasubruang       = $this->input->post('namasubruang');         
                $deskripsi          = $this->input->post('deskripsi');
                $koderuang          = $this->input->post('koderuang');   
                $telepon            = $this->input->post('telepon');
                $status             = $this->input->post('status');
                $statusoperasional  = $this->input->post('statusoperasional');
                $tipefisik          = $this->input->post('tipefisik');
                $mode               = $this->input->post('mode');
                $tipefungsi         = $this->input->post('tipefungsi');
                $datalocation = [
                    'namaruang' => $namasubruang,
                    'koderuangan' => $koderuang,
                    'deskripsi' => $deskripsi,
                    'telepon' => $telepon,
                    'status' => $status,
                    'operationalstatus' => $statusoperasional,
                    'physicaltype' => $tipefisik,
                    'mode' => $mode,  
                    'type' => $tipefungsi
                ];
                if(!empty($idsublocation)){
                    $query =  $this->db->update('satusehat_sub_location',$datalocation, array('idsublocation'=>$idsublocation));
                    echo json_encode(['msg'=>'success update','txt'=>'Data berhasil diupdate pada local database']);
                }else{
                    $datalocation['idlocation'] = $ruangutama;
                    $query = $this->db->insert('satusehat_sub_location', $datalocation);
                    echo json_encode(['msg'=>'success insert','txt'=>'Data Berhasil disimpan']);
                }

            // }
        }
    }
   
    /////////////////////// GET ORGANISASI SATUSEHAT ///////////////////
    /**
     * MENAMPILKAN DATA-DATA ORGANISASI DARI API SATUSEHAT
     * dev by Danang
     */
    public function find_organization()
    {   
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_ORGANISASI)) //lihat define di atas
        { 
            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'by_id' );
            $data = [
               'content_view'      => 'satusehat/v_cari_organisasi',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'organization_satusehat',
               'active_menu_level' => 'find_organization',
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE],
               'tabActive'         => $tabActive,
              ];

            $data['title_page']     = 'Cari Organisasi';
            $data['mode']           = 'view';
            $data['script_js']      = ['satusehat/registrasi_struktur_organisasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function organization_getid()
    {
        
        $param = $this->input->post('param');
        
        $url = "/Organization/".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $organization = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($organization);
        // echo json_encode( ['hsl'=>$organization] );
        // print_r($organization);
    }

    public function organization_getname()
    {
        
        $param = $this->input->post('param');
        
        $url = "/Organization?name=".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $organization = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($organization);
    }

    public function organization_getpartof()
    {
        
        $param = $this->input->post('param');
        
        $url = "/Organization?partof=".$param;
        
        $token = $this->satusehat->createSignature()['Authorization'];
        $method = 'GET';
        $organization = $this->satusehat->requestSatuSehat($url, $method, $token);
        echo json_encode($organization);
    }

    /////////////////////// CRUD ORGANISASI ///////////////////
    /**
     * MENYIAPKAN DATA ORGANISASI SEBELUM DIKIRIM KE SATU SEHAT
     * DISIMPAN KE TABEL satusehat_organization dan satusehat_sub_organization
     * dev by Danang
     */
    public function organization()
    {   
        $get = $this->input->get();
        if ($this->pageaccessrightbap->checkAccessRight(V_ORGANISASI)) //lihat define di atas
        { 
            $type_terminology       = organization_get_type();

            $tabActive = ((isset($get['tabActive'])) ? $get['tabActive'] : 'org' );
            // $org = $this->msatusehat->get_listorganization();
            // $suborg = $this->msatusehat->get_listsuborganization();
            $data = [
               'content_view'      => 'satusehat/v_organisasi',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'organization_satusehat',
               'active_menu_level' => 'organization',
            //    'org'               => $org,
            //    'suborg'            => $suborg,
               'type'              => $type_terminology,
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE],
               'tabActive'         => $tabActive,
              ];

            $data['title_page']     = 'Struktur Organisasi';
            $data['mode']           = 'view';
            $data['script_js']      = ['satusehat/registrasi_struktur_organisasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function get_org(){
        $data = $this->msatusehat->get_listorganization();
        if($data){
            echo json_encode($data);
        }
    }
    public function get_sub_org(){
        $data = $this->msatusehat->get_listsuborganization();
        if($data){
            echo json_encode($data);
        }
    }

    public function add_edit_organization($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ORGANISASI)) //lihat define di atas
        { 
            $get_data='';
            if($id!=null){
                $get_data       = $this->msatusehat->get_editorganization($id);
            }
            $type       = organization_get_type();
            
            $data = [
               'content_view'      => 'satusehat/v_organisasi',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'list_organisasi',
               'active_menu_level' => '',
               'mode'              => ( $id !=null ) ? 'edit_organisasi' : 'add_organisasi',
               'paramid'           => ( $id !=null ) ? $id : '',
               'get_edit'          => $get_data,
               'type'              => $type,
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE]
              ];

            $data['title_page']     = 'Tambah Organisasi';
            $data['script_js']      = ['satusehat/registrasi_struktur_organisasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function save_organization()
    {
        $post = $this->input->post();
        if(!empty($post['paramid'])){
            $id=$post['paramid'];
        }
        
        $data = [
            'status'         => $post['status'],
            'type'           => $post['type'],
            'name'           => $post['name'],
            'phone'          => $post['phone'],
            'email'          => $post['email'],
        ];
        if(!empty($post['ihs'])){
            $ihs=$post['ihs'];
            $data['ihs'] = $ihs;
        }

        if(empty($post['paramid'])){
            $query = $this->db->insert('satusehat_organization', $data);
            echo json_encode(['msg'=>'success insert','txt'=>'Data Berhasil disimpan']);

        }else{
            $query = $this->db->update('satusehat_organization',$data, array('idorganization'=>$id));
            echo json_encode(['msg'=>'success update','txt'=>'Data berhasil diupdate pada local database']);
        }  
    }

    public function remove_organization()
    {
        $post = $this->input->post();
        $id   = (int) $this->security->sanitize_filename($post['id']);
        $delete  = $this->db->delete('satusehat_organization',['idorganization'=>$id]);
        if( $delete > 0 ){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    public function add_edit_sub_organization($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_ORGANISASI)) //lihat define di atas
        { 
            $org = $this->msatusehat->get_listorganization();
            $get_data='';
            if($id!=null){
                $get_data       = $this->msatusehat->get_editsuborganization($id);
            }
            $type_terminology       = organization_get_type();
            $data = [
               'content_view'      => 'satusehat/v_organisasi',
               'active_menu'       => 'satusehat',
               'active_sub_menu'   => 'list_organisasi',
               'active_menu_level' => '',
               'mode'              => ( $id !=null ) ? 'edit_sub_organisasi' : 'add_sub_organisasi',
               'paramid'           => ( $id !=null ) ? $id : '',
               'org_induk'         => ( $id !=null ) ? '' : $org,
               'get_edit'          => $get_data,
               'type'              => $type_terminology,
               'plugins'           => [PLUG_DATE,PLUG_DATATABLE]
              ];

            $data['title_page']     = ( $id !=null ) ? 'Edit Sub Organisasi' : 'Tambah Sub Organisasi';
            $data['script_js']      = ['satusehat/registrasi_struktur_organisasi'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }

    public function save_sub_organization()
    {
        $post = $this->input->post();
        if(!empty($post['paramid'])){
            $id=$post['paramid'];
        }
        $data = [
            'idorganization' => $post['organisasi_induk'],
            'status'         => $post['status'],
            'type'           => $post['type'],
            'name'           => $post['name'],
            'phone'          => $post['phone'],
            'email'          => $post['email'],
        ];
        if(empty($post['paramid'])){
            $query = $this->db->insert('satusehat_sub_organization', $data);
            echo json_encode (['msg'=>'success insert', 'txt'=>'Data Berhasil di Simpan']);    
        }else{
            $query = $this->db->update('satusehat_sub_organization',$data, array('idsuborganization'=>$id));
            echo json_encode(['msg'=>'success update','txt'=>'Data berhasil diupdate pada local database']);
        }  
    }

    public function remove_sub_organization()
    {
        $post = $this->input->post();
        $id   = (int) $this->security->sanitize_filename($post['id']);
        $delete  = $this->db->delete('satusehat_sub_organization',['idsuborganization'=>$id]);
        if( $delete > 0 ){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    /////////////////////// PENGIRIMAN DATA SATUSEHAT ///////////////////
    /**
     * dev by Danang
     */

     public function insert_location(){
        $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('locId')));
        $tipe = $this->input->post('tipe');
        $OrgIHSinduk = $this->satusehat->getOrganizationID();
        $data_location = [];
        if($tipe === "loc"){
            $this->mgenerikbap->setTable('satusehat_location');
            $data_location = $this->mgenerikbap->select('*',['idlocation' => $id])->row_array();
        }else if($tipe === "subloc"){
            $this->mgenerikbap->setTable('satusehat_sub_location');
            $data_location = $this->mgenerikbap->select('*',['idsublocation' => $id])->row_array();
        }
        $operational_status = location_get_operational_status();
        $mode               = location_get_mode();
        $physical_type      = location_get_physical_type();
        $service_class      = location_get_service_class();
        $type               = location_get_type();
        $status             = location_get_status();
        $final_data = [
            "resourceType" => "Location",
            "identifier" => [
                [
                    "system" => "http://sys-ids.kemkes.go.id/location/".$OrgIHSinduk,
                    "value" => strtoupper($data_location['koderuangan'])
                ]
            ],
            "status" => get_array_value($status[$data_location['status']],'Location.status'),//M
            "name" => $data_location['namaruang'],//M
            "description" => $data_location['deskripsi'],//
            "mode" => get_array_value($mode[$data_location['mode']],'Location.mode'),//M
            "telecom" => [
                [
                    "system" => "phone",
                    "value" => $data_location['telepon'],
                    "use" => "work"
                ],
            ],
            "address" => [
                "use" => "work",
                "line" => ["Jl. Ringroad Barat No.118, Mlangi, Nogotirto"],
                "city" => "Yogyakarta",
                "postalCode" => "55294",
                "country" => "ID",
                "extension" => [
                    [
                        "url" => "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                        "extension" => [
                            [
                                "url" => "province",
                                "valueCode" => "34"
                            ],
                            [
                                "url" => "city",
                                "valueCode" => "3404"
                            ],
                            [
                                "url" => "district",
                                "valueCode" => "340401"
                            ],
                            [
                                "url" => "village",
                                "valueCode" => "3404012004"
                            ],
                            [
                                "url" => "rt",
                                "valueCode" => "00"
                            ],
                            [
                                "url" => "rw",
                                "valueCode" => "00"
                            ]
                        ]
                    ]
                ]
            ],
            // "operationalStatus" => [
            //     "coding" => [
            //         [
            //             "system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            //             "code" => get_array_value($operational_status[$data_location['operationalstatus']],'Location.operationalStatus.code'),
            //             "display" => get_array_value($operational_status[$data_location['operationalstatus']],'Location.operationalStatus.display')
            //         ]
            //     ]
            // ],
            "position" => [
                "longitude" => -7.7625690238598155,
                "latitude" => 110.3367570559625,
                "altitude" =>  0
            ],
            "physicalType" => [
                "coding" => [
                    [
                        "system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
                        "code" => get_array_value($physical_type[$data_location['physicaltype']],'Location.physicalType.coding.code'),
                        "display" => get_array_value($physical_type[$data_location['physicaltype']],'Location.physicalType.coding.display')
                    ]
                ]
            ],//M
            "managingOrganization" => [
                "reference" => "Organization/".$OrgIHSinduk
            ],//M
        ];
        // if (isset($data_location['kelas'])) {
        //     $final_data['extension'] = [
        //         "serviceClass" => [
        //             "coding" =>
        //                 [
        //                     "system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
        //                     "code" => get_array_value($service_class[$data_location['kelas']], 'Location.extension.serviceClass.code'),
        //                     "display" => get_array_value($service_class[$data_location['kelas']], 'Location.extension.serviceClass.display')
        //                 ]
        //         ]
        //     ];
        // }
        // if (isset($data_location['partof'])) {
        //     $final_data['PartOf'] = [
        //         "reference" => "Location/" . $data_location['partof']
        //     ];
        // }
        $addurl = '/Location';
        $data   = json_encode($final_data);
        $token  = $this->satusehat->createSignature()['Authorization'];
        $method = 'POST';
        $result = $this->satusehat->createSatuSehat($addurl,$data,$token,$method); 
        $dr     = json_decode($result['response'], true);
        if (isset($dr['id'])) {
            $ihs = $dr['id'];  
            if($tipe === "loc"){
                $this->msatusehat->update_ihs_location($id,$ihs);
            }else if($tipe === "subloc"){
                $this->msatusehat->update_ihs_sub_location($id,$ihs);
            };          
        }

        $data_log = [
            'type'=> 'Insert struktur Lokasi',
            'status_code'=> $result['http_code'],
            'updated_by'=> $this->session->userdata('username'),
            'response'=>serialize($result['response']),
        ];
        $this->msatusehat->insert_log($data_log);
        
        if($data_log['status_code']===400){
            $data_log['msg'] = 'error';
            $data_log['txt'] = 'Terjadi Error';
        }else{
            $data_log['msg'] = 'success';
            $data_log['txt'] = 'Data berhasil bridging ke satusehat';
        }
        echo json_encode($data_log);        
    }

    public function remove_location()
    {
        $post = $this->input->post();
        $id   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['id']));
        $delete  = $this->db->delete('satusehat_location',['idlocation'=>$id]);
        if( $delete > 0 ){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    public function remove_sub_location()
    {
        $post = $this->input->post();
        $id   = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['id']));
        $delete  = $this->db->delete('satusehat_sub_location',['idsublocation'=>$id]);
        if( $delete > 0 ){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }
    public function update_location(){
        $id                 = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('id')));
        $namaruang          = $this->input->post('namaruang');
        $koderuang          = $this->input->post('koderuang');            
        $deskripsi          = $this->input->post('deskripsi');
        $telepon            = $this->input->post('telepon');
        $status             = $this->input->post('status');
        $statusoperasional  = $this->input->post('statusoperasional');
        $tipefisik          = $this->input->post('tipefisik');
        $mode               = $this->input->post('mode');
        $kelas              = $this->input->post('kelas');
        $tipefungsi         = $this->input->post('tipefungsi');
        $ihs                = $this->input->post('ihs');
        $x                  = $this->input->post('x');
        $data_location = [
            'namaruang' => $namaruang,
            'koderuangan' => $koderuang,
            'deskripsi' => $deskripsi,
            'telepon' => $telepon,
            'status' => $status,
            'operationalstatus' => $statusoperasional,
            'physicaltype' => $tipefisik,
            'mode' => $mode,  
            'type' => $tipefungsi,
            'ihs' => $ihs,
        ];
        $OrgIHSinduk = $this->satusehat->getOrganizationID();
        $operational_status = location_get_operational_status();
        $mode               = location_get_mode();
        $physical_type      = location_get_physical_type();
        $service_class      = location_get_service_class();
        $type               = location_get_type();
        $status             = location_get_status();
        $final_data = [
            "resourceType" => "Location",
            "id"=>$data_location['ihs'],
            "identifier" => [
                [
                    "system" => "http://sys-ids.kemkes.go.id/location/".$OrgIHSinduk,
                    "value" => strtoupper($data_location['koderuangan'])
                ]
            ],
            "status" => get_array_value($status[$data_location['status']],'Location.status'),//M
            "name" => $data_location['namaruang'],//M
            "description" => $data_location['deskripsi'],//
            "mode" => get_array_value($mode[$data_location['mode']],'Location.mode'),//M
            "telecom" => [
                [
                    "system" => "phone",
                    "value" => $data_location['telepon'],
                    "use" => "work"
                ],
            ],
            "address" => [
                "use" => "work",
                "line" => ["Jl. Ringroad Barat No.118, Mlangi, Nogotirto"],
                "city" => "Yogyakarta",
                "postalCode" => "55294",
                "country" => "ID",
                "extension" => [
                    [
                        "url" => "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                        "extension" => [
                            [
                                "url" => "province",
                                "valueCode" => "34"
                            ],
                            [
                                "url" => "city",
                                "valueCode" => "3404"
                            ],
                            [
                                "url" => "district",
                                "valueCode" => "340401"
                            ],
                            [
                                "url" => "village",
                                "valueCode" => "3404012004"
                            ],
                            [
                                "url" => "rt",
                                "valueCode" => "00"
                            ],
                            [
                                "url" => "rw",
                                "valueCode" => "00"
                            ]
                        ]
                    ]
                ]
            ],
            // "operationalStatus" => [
            //     "coding" => [
            //         [
            //             "system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            //             "code" => get_array_value($operational_status[$data_location['operationalstatus']],'Location.operationalStatus.code'),
            //             "display" => get_array_value($operational_status[$data_location['operationalstatus']],'Location.operationalStatus.display')
            //         ]
            //     ]
            // ],
            "physicalType" => [
                "coding" => [
                    [
                        "system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
                        "code" => get_array_value($physical_type[$data_location['physicaltype']],'Location.physicalType.coding.code'),
                        "display" => get_array_value($physical_type[$data_location['physicaltype']],'Location.physicalType.coding.display')
                    ]
                ]
            ],//M
            "managingOrganization" => [
                "reference" => "Organization/".$OrgIHSinduk
            ],//M
        ];
        // if (isset($data_location['kelas'])) {
        //     $final_data['extension'] = [
        //         "serviceClass" => [
        //             "coding" =>
        //                 [
        //                     "system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
        //                     "code" => get_array_value($service_class[$data_location['kelas']], 'Location.extension.serviceClass.code'),
        //                     "display" => get_array_value($service_class[$data_location['kelas']], 'Location.extension.serviceClass.display')
        //                 ]
        //         ]
        //     ];
        // }
        if ($x === 'subloc') {
            $sub_loc = $this->msatusehat->get_one_sub_loc($id);
            $final_data['PartOf'] = [
                "reference" => "Location/" . $sub_loc['ihsParent']
            ];
        }
        $addurl = '/Location/'.$data_location['ihs'];
        $data   = json_encode($final_data);
        $token  = $this->satusehat->createSignature()['Authorization'];
        $method = 'PUT';
        $result = $this->satusehat->createSatuSehat($addurl,$data,$token,$method); 
        $dr     = json_decode($result['response'], true);

        $data_log = [
            'type'=> 'Update struktur Lokasi',
            'status_code'=> $result['http_code'],
            'updated_by'=> $this->session->userdata('username'),
            'response'=>serialize($result['response']),
        ];
        $this->msatusehat->insert_log($data_log);
        // echo json_encode($final_data);

        if($result['http_code'] === 200){
            echo json_encode(['msg' => 'success update', 'txt' => 'Data berhasil diupdate disatusehat']);
        }else{
            echo json_encode(['msg' => 'error', 'txt' => 'Terjadi Error']);
        }
        // }
    }
    public function insert_organization(){
        $id =  $this->input->post('orgId');
        $orgOrsuborg = $this->input->post('tipe');
        $OrgIHSinduk = $this->satusehat->getOrganizationID();
        $data_organization = [];
        if($orgOrsuborg === "org"){
            $this->mgenerikbap->setTable('satusehat_organization');
            $data_organization = $this->mgenerikbap->select('*',['idorganization' => $id])->row_array();
        }else if($orgOrsuborg === "suborg"){
            $this->mgenerikbap->setTable('satusehat_sub_organization');
            $data_organization = $this->mgenerikbap->select('*',['idsuborganization' => $id])->row_array();
            $sub_org = $this->msatusehat->get_one_sub_org($id);
        }
        $isActive = $data_organization['status'] === "1" ? true : false;
        $type = organization_get_type();
        $final_data = [
            "resourceType" => "Organization",
            "active" => $isActive,
            "identifier" => [
                [
                    "use" => "official",
                    // "system" => "http://sys-ids.kemkes.go.id/organization/1000079374",
                    "system" => "http://sys-ids.kemkes.go.id/organization/897d5669-2692-41ed-be93-6a7a097da53f",
                    "value" => $data_organization['name']
                ]
            ],
            "type" => [
                [
                    "coding" => [
                        [
                            "system" => "http://terminology.hl7.org/CodeSystem/organization-type",
                            "code" => get_array_value($type[$data_organization['type']],'Organization.type.code'),
                            "display" => get_array_value($type[$data_organization['type']],'Organization.type.display')
                        ]
                    ]
                ]
            ],
            "name" => $data_organization['name'],
            "telecom" => [
                [
                    "system" => "phone",
                    "value" => $data_organization['phone'],
                    "use" => "work"
                ],
                [
                    "system" => "email",
                    "value" => $data_organization['email'],
                    "use" => "work"
                ],
            ],
            "address" => [
                [
                    "use" => "work",
                    "type" => "both",
                    "line" => ["Jl. Ringroad Barat No.118, Mlangi, Nogotirto"],
                    "city" => "Yogyakarta",
                    "postalCode" => "55294",
                    "country" => "ID",
                    "extension" => [
                        [
                            "url" => "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                            "extension" => [
                                [
                                    "url" => "province",
                                    "valueCode" => "34"
                                ],
                                [
                                    "url" => "city",
                                    "valueCode" => "3404"
                                ],
                                [
                                    "url" => "district",
                                    "valueCode" => "340401"
                                ],
                                [
                                    "url" => "village",
                                    "valueCode" => "3404012004"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        if ($orgOrsuborg === "org") {
            $final_data['PartOf'] = [
                "reference" => "Organization/".$OrgIHSinduk
            ];
        }else if($orgOrsuborg === "suborg"){
            $final_data['PartOf'] = [
                "reference" => "Organization/".$sub_org['ihsParent']
            ];
        }
        $addurl = '/Organization';
        $data   = json_encode($final_data);
        $token  = $this->satusehat->createSignature()['Authorization'];
        $method = 'POST';
        $result = $this->satusehat->createSatuSehat($addurl,$data,$token,$method); 
        $dr     = json_decode($result['response'], true);
        if (isset($dr['id'])) {
            $ihs = $dr['id'];  
            if($orgOrsuborg === "org"){
                $this->msatusehat->update_ihs_organization($id,$ihs);
            }else if($orgOrsuborg === "suborg"){
                $this->msatusehat->update_ihs_sub_organization($id,$ihs);
            };          
        }
        $data_log = [
            'type'=> 'Insert Organisasi',
            'status_code'=> $result['http_code'],
            'updated_by'=> $this->session->userdata('username'),
            'response'=>serialize($result['response']),
        ];
        $this->msatusehat->insert_log($data_log);
        if($data_log['status_code']===400){
            $data_log['msg'] = 'error';
            $data_log['txt'] = 'Terjadi Error';
        }else{
            $data_log['msg'] = 'success';
            $data_log['txt'] = 'Data berhasil bridging ke satusehat';
        }
        echo json_encode($data_log);
        // echo json_encode($final_data);
    }
    public function update_organization(){

        $post = $this->input->post();
        $x = $post['x'];
        $data_organization = [
            'status'         => $post['status'],
            'type'           => $post['type'],
            'name'           => $post['name'],
            'phone'          => $post['phone'],
            'email'          => $post['email'],
            'ihs'            => $post['ihs']
        ];
        $isActive = $data_organization['status'] === "1" ? true : false;
        $type = organization_get_type();
        $OrgIHSinduk = $this->satusehat->getOrganizationID();
        $final_data = [
            "resourceType" => "Organization",
            "id"=>$data_organization['ihs'],
            "active" => $isActive,
            "identifier" => [
                [
                    "use" => "official",
                    // "system" => "http://sys-ids.kemkes.go.id/organization/1000079374",
                    "system" => "http://sys-ids.kemkes.go.id/organization/897d5669-2692-41ed-be93-6a7a097da53f",
                    "value" => $data_organization['name']
                ]
            ],
            "type" => [
                [
                    "coding" => [
                        [
                            "system" => "http://terminology.hl7.org/CodeSystem/organization-type",
                            "code" => get_array_value($type[$data_organization['type']],'Organization.type.code'),
                            "display" => get_array_value($type[$data_organization['type']],'Organization.type.display')
                        ]
                    ]
                ]
            ],
            "name" => $data_organization['name'],
            "telecom" => [
                [
                    "system" => "phone",
                    "value" => $data_organization['phone'],
                    "use" => "work"
                ],
                [
                    "system" => "email",
                    "value" => $data_organization['email'],
                    "use" => "work"
                ],
            ],
            "address" => [
                [
                    "use" => "work",
                    "type" => "both",
                    "line" => ["Jl. Ringroad Barat No.118, Mlangi, Nogotirto"],
                    "city" => "Yogyakarta",
                    "postalCode" => "55294",
                    "country" => "ID",
                    "extension" => [
                        [
                            "url" => "https://fhir.kemkes.go.id/r4/StructureDefinition/administrativeCode",
                            "extension" => [
                                [
                                    "url" => "province",
                                    "valueCode" => "34"
                                ],
                                [
                                    "url" => "city",
                                    "valueCode" => "3404"
                                ],
                                [
                                    "url" => "district",
                                    "valueCode" => "340401"
                                ],
                                [
                                    "url" => "village",
                                    "valueCode" => "3404012004"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        if ($x === "org") {
            $final_data['PartOf'] = [
                "reference" => "Organization/".$OrgIHSinduk
            ];
        }else if($x === "suborg"){
            $sub_org = $this->msatusehat->get_one_sub_org($post['paramid']);
            $final_data['PartOf'] = [
                "reference" => "Organization/".$sub_org['ihsParent']
            ];
        }
        $addurl = '/Organization/'.$data_organization['ihs'];
        $data   = json_encode($final_data);
        $token  = $this->satusehat->createSignature()['Authorization'];
        $method = 'PUT';
        $result = $this->satusehat->createSatuSehat($addurl,$data,$token,$method); 
        $data_log = [
            'type'=> 'Update Organisasi',
            'status_code'=> $result['http_code'],
            'updated_by'=> $this->session->userdata('username'),
            'response'=>serialize($result['response']),
        ];
        $this->msatusehat->insert_log($data_log);
        if($result['http_code'] === 200){
            echo json_encode(['msg' => 'success update', 'txt' => 'Data berhasil diupdate disatusehat']);
        }else{
            echo json_encode(['msg' => 'error', 'txt' => 'Terjadi Error']);
        }
    }
    public function cekUuid(){
        $uuid = guidv4();
        echo $uuid;
    }
}