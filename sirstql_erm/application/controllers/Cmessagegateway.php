<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cmessagegateway extends CI_controller 
{
    function __construct()
    {
        parent::__construct();
    }
    
    private function getPass($userid)
    {
        $upass = ["2ffc4c7b16ddcc2f07b214bb547ce00ae18af8f2f3e8b6e6f416b1d89933bb13" => "43880c9c8c7afcd70a5cb670e5981697ef0558831108548b4d988e8ff4a9f7b7"];
        return $upass[$userid];
    }
    
    //kirim ke pengguna gateway ini user dan password di atas
    //sebelum dikirim, password dan user di md5 dan sha512 dulu dengan salt dd.mm.YYYY, dapat juga (tidak wajib) ditambah salt lain di belakangnya
    //php
    //$s = uniqid();
    //$u = "2ffc4c7b16ddcc2f07b214bb547ce00ae18af8f2f3e8b6e6f416b1d89933bb13";
//    $p = substr(hash('sha512', hash('md5', "43880c9c8c7afcd70a5cb670e5981697ef0558831108548b4d988e8ff4a9f7b7".date("d.m.Y")).$salt), 0, 32);
    //mysql:
    //set @salt = "jalan makin membara";select "2ffc4c7b16ddcc2f07b214bb547ce00ae18af8f2f3e8b6e6f416b1d89933bb13" as userid, left(sha2(concat(md5(concat("43880c9c8c7afcd70a5cb670e5981697ef0558831108548b4d988e8ff4a9f7b7", DATE_FORMAT(now(), "%d.%m.%Y"))), @salt), 512), 32) as pass;
    private function otentikasi()
    {
        $s = ifnull($this->input->post('s', TRUE), '');
        return  $this->input->post('p', TRUE) == $this->hide_credential($this->getPass($this->input->post('u', TRUE)), $s);
    }
    
    private function hide_credential($credential, $salt)
    {
        return substr(hash('sha512', hash('md5', $credential.date("d.m.Y")).$salt), 0, 32);
    }
    
    function get_data()
    {
        $this->output->set_header("Content-Type: application/json");
        $this->output->set_header("Cache-Control: no-store");
        if ($this->input->post('u', TRUE) == "test")
        {
            echo json_encode([["a" => 1234, "b" => "Ini adalah hasil tes 1", "c" => "00000000", "d" => date("Y-m-d H:i:s")], ["a" => 1235, "b" => "Ini adalah hasil tes 2", "c" => "00000000", "d" => date("Y-m-d H:i:s")]]);
        }
        else if ($this->otentikasi())
        {            
            echo json_encode($this->db->query('call pget_messagegateway()')->result());
        }
        else 
        {
            echo json_encode(array("AUTH FAILED"));
        }
    }
    
    
    
    function set_data()
    {
        $this->output->set_header("Content-Type: application/json");
        $this->output->set_header("Cache-Control: no-store");
        if ($this->otentikasi())
        {
            $this->load->model('mmessagegateway');
            $this->mmessagegateway->update_messagegateway(["sms" => date("Y-m-d H:i:s")], ["idmessage" => $this->input->post('i', TRUE)]);
            echo json_encode(["Pesan" => "Flag Berhasil"]);
        }
        else 
        {
            echo json_encode(array());
        }
    }
}