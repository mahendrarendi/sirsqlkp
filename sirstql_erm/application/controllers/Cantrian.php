<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama

class Cantrian extends MY_controller 
{
    function __construct()
    {
        parent::__construct();    
    }
    ///////////////////////PENDAFTARAN ANTRIAN PASIEN///////////////////
    //-------------------------- vv Standar
    public function setting_pendaftaran()
    {
        return [    'content_view'      => 'antrian/v_ambilantrian',
                    'active_menu'       => 'admission',
                    'active_sub_menu'   => 'pendaftaran',
                    'active_menu_level' => '',
                    'plugins'           => [ PLUG_DATATABLE ]
               ];
    }
    
    public function ambilantrian()
    {
        $data = [ 'content_view'        => 'antrian/v_getantrian',
                    'active_menu'       => 'antrian',
                    'active_sub_menu'   => 'ambilantrian',
                    'title_page'        => 'Ambil Antrian',
                    'stasiun'           => $this->db->get("antrian_stasiun")->result(),
                    'active_menu_level' => '',
                    'plugins'           => [  ]
               ];
        $this->load->view('v_index', $data);
    }
    
    public function ambilantrianpasien()
    {
        $data = [ 'content_view'        => 'antrian/v_getantrianpasien',
                    'active_menu'       => 'antrian',
                    'active_sub_menu'   => 'ambilantrianpasien',
                    'title_page'        => 'Ambil Antrian Pasien',
                    'stasiun'           => $this->db->get("antrian_stasiun")->result(),
                    'active_menu_level' => '',
                    'plugins'           => [  ]
               ];
        $this->load->view('v_index', $data);
    }
    
    public function pengambilanantrian()
    {
            
            $x = $this->uri->segment(3);
            $data                   = $this->setting_pendaftaran();
            $data['loket']          = $this->db->query("select *, (select count(*) from antrian_loket where idstasiun='$x') as jml from antrian_loket where idstasiun='$x'")->result();
            $data['title_page']     = 'Loket Antrian Pasien';
            $data['mode']           = 'pengambilanantrian';
            $data['table_title']    = 'Loket Antrian Pasien ';
            $this->load->view('antrian/v_index', $data);  
            
    }
    
    public function pengambilanantrianpasien()
    {
            
            $this->load->model('mkombin');
            $ipserver = $this->mkombin->getKonfigurasi('ipserver');
            if($ipserver !='http://localhost/')
            {
                redirect($ipserver.'antrian/cantrian/pengambilanantrianpasien/'.$this->uri->segment(3));
            }
            else
            {
                $x = $this->uri->segment(3);
                $data = [ 'content_view'    => 'antrian/v_ambilantrianpasien',
                        'active_menu'       => 'admission',
                        'active_sub_menu'   => 'pendaftaran',
                        'active_menu_level' => '',
                        'plugins'           => [ PLUG_DATATABLE ]
                   ];
                $data['loket']          = $this->db->query("select *, (select count(*) from antrian_loket where idstasiun='$x') as jml from antrian_loket where idstasiun='$x'")->result();
                $data['title_page']     = 'Loket Antrian Pasien';
                $data['mode']           = 'pengambilanantrian';
                $data['table_title']    = 'Loket Antrian Pasien ';
                $this->load->view('antrian/v_index', $data);
            }
            
    }

    public function saveantrian()
    {
        $callid     = generaterandom(8);
        $idloket    = $this->input->post('idloket');
        $idstasiun  = $this->input->post('idstasiun');
        $jnsantri   = (($idloket==39)? "Antrisep" : "Antrian");
        $this->generate_no_antrian($jnsantri,$idstasiun, $idloket, $callid);
        $noantrian  = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array();
        $data = ['idloket'=>$idloket, 'tanggal'=>date('Y-m-d'), 'no'=>$noantrian['number'], 'status'=>'antri'];
        $arrMsg = $this->db->insert('antrian_antrian',$data);

        /** Memey */
        if ($arrMsg) {
            $insert_idantrian = $this->db->insert_id();   
            $loket  = $this->db->query("select * from antrian_loket where idloket='$idloket'")->row_array();
            $daftar = ['waktu_ambilantrian'=>indonesia_date(),'idantrian'=>$insert_idantrian,'jenis_antrian'=>$loket['namaloket'],'nomor_antrian'=>$noantrian['number'],'tanggal'=>date('Y-m-d')];
            $pendaftaran_admisi = $this->db->insert('waktu_pendaftaran_admisi',$daftar);
            echo json_encode(($pendaftaran_admisi) ? ['status' => 'success', 'message' => 'Antrian Success..! Antrian No:'.$noantrian['number'], 'noantrian'=>$noantrian['number'], 'tanggal'=>date('d-M-Y H:i:s'),"namaloket"=>$loket['namaloket'] ] : ['status' => 'danger', 'message' => 'Antrian Failed..!']);
        }
    }

    // mahmud, clear --> generate no antrian
    private function generate_no_antrian($antrianStatus,$idstasiun,$idloket,$callid)
    {
        $this->mgenerikbap->setTable('helper_autonumber');
        $data = [
            'id'      =>$antrianStatus.date('Ymd').'-'.$idstasiun.'-'.$idloket,
            'number'  =>NULL,
            'callid'  =>$callid,
            'expired' =>date('Y-m-d')
        ];
        return $this->mgenerikbap->insert($data);
    }
    
    public function viewantrian()
    {
        $this->load->model('mkombin');
        $data = [ 'content_view'        => 'antrian/v_viewantrian',
                    'active_menu'       => 'antrian',
                    'active_sub_menu'   => 'viewantrian',
                    'title_page'        => 'List Display Antrian',
                    'stasiun'           => $this->db->get("antrian_stasiun")->result(),
                    'active_menu_level' => '',
                    'plugins'           => [  ]
               ];
        $this->load->view('v_index', $data);
    }
    
    
    public function tampilantrianlama()
    {
        $x = $this->uri->segment(3);
            $data                   = $this->setting_pendaftaran();
            $data['title_page']     = 'Antrian Pasien';
            $data['mode']           = 'tampilantrian';
            $data['table_title']    = 'Antrian Pasien ';
            $data['script_js']      = ['js_antrian'];
            $this->load->view('antrian/v_index', $data);   
    }
    public function tampilantrian()
    {
        // $x = $this->uri->segment(3);
        $this->load->model('mkombin');
        $ipserver = $this->mkombin->getKonfigurasi('ipserver');
        if($ipserver !='http://localhost/')
        {
            redirect($ipserver.'antrian/cantrian/tampilantrian/'.$this->uri->segment(3));
        }
        else
        {
            $data['content_view']  = 'antrian/tampilanbaru';
            $data['active_menu']       = 'admission';
            $data['active_sub_menu']   = 'pendaftaran';
            $data['title_page']     = 'Loket Antrian Pasien';
            $data['mode']   = 'view';
            $data['active_menu_level'] = '';
            $data['script_js']      = ['js_antrian'];
            $data['plugins']           = [] ;
            $this->load->view('antrian/v_tampilantrian', $data);
        }
    }
    
    public function tampilantrian_integrasiapplicare()
    {
        // $x = $this->uri->segment(3);
        $data['applicare']      = $this->bpjsbap->requestBedlist(JENISCONSID_PRODUCTION);
        $data['content_view']   = 'antrian/pemanggilan_integrasiapplicare';
        $data['active_menu']    = 'admission';
        $data['active_sub_menu']= 'pendaftaran';
        $data['title_page']     = 'Loket Antrian Pasien';
        $data['mode']           = 'view_aplicare';
        $data['active_menu_level'] = '';
        $data['script_js']      = ['js_antrian'];
        $data['plugins']        = [] ;
        $this->load->view('antrian/v_tampilantrian', $data);
    }
    
    public function panggilantrian()
    {
        $stasiun = $this->db->escape($this->input->post('s'));
        $panggil = $this->db->query("select a.status, l.idstasiun, l.idloket, l.namaloket, a.idantrian,fontawesome, a.no , 
            if(a.status='panggilkasir',
                concat(' Panggilan Kasir atas nama ',p.namalengkappemanggilan),
                if(a.status='panggil',
                concat(l.namaloket, ' nomor ', a.no,', ', if((p.namalengkappemanggilan is not null),concat('atas nama ',p.namalengkappemanggilan),''), '.') , 
                concat('Hasil Laboratorium atas nama ',p.namalengkappemanggilan)  
                ) 
            )AS pemanggilan 
        from antrian_antrian a 
        join  antrian_loket l on l.idloket=a.idloket
        left join person_person p on p.idperson = a.idperson 
        where a.tanggal = Date(now()) and a.status !='sedang proses' and ( a.status = 'panggilkasir' or a.status = 'panggilhasil' or a.status='panggil' ) and l.idstasiun = ".$stasiun." 
        group by a.idloket ORDER by a.idloket, a.no limit 1")->result();
        //jika statusnya sudah panggil, sedang proses atau ok, maka tidak masuk ke dalam prioritas ditampilkan sebagai dipanggil terakhir
        $antrian = $this->db->query("select `l`.`idstasiun` AS `idstasiun`,`l`.`idloket` AS `idloket`,`l`.`namaloket` AS `namaloket`,`a`.`idantrian` AS `idantrian`,fontawesome,max(if(a.status='sudah panggil' or a.status='ok' or a.status='sedang proses',`a`.`no`, 0)) AS `no`,concat(`l`.`namaloket`,' nomor ',`a`.`no`,', ', if((`p`.`namalengkappemanggilan` is not null),concat('atas nama ',`p`.`namalengkappemanggilan`),''), '.') AS `pemanggilan` from ((`antrian_antrian` `a` join `antrian_loket` `l` on((`l`.`idloket` = `a`.`idloket`))) left join `person_person` `p` on((`p`.`idperson` = `a`.`idperson`))) where ((`a`.`tanggal` = Date(now())) and (`a`.`status` != 'antri') and (`l`.`idstasiun` = ".$stasiun.")) group by a.idloket ORDER by a.idloket, a.no")->result();
        echo json_encode(['antrian'=>$antrian, 'panggil'=>$panggil]);
    }

    public function updateantrian()
    {
        // jika user belum login maka akses ditolak
        $id = $this->input->post('id');
        $x = $this->db->update('antrian_antrian',['status'=>'sudah panggil'],['idantrian'=>$id]);
        echo json_encode($x);
    }

    public function tampilanbaruantrian()
    {
        $data['content_view']     = 'antrian/v_ambilantrian';
        $data['active_menu']       = 'admission';
        $data['active_sub_menu']   = 'pendaftaran';
        $data['title_page']     = 'Loket Antrian Pasien';
        $data['mode']   = 'view';
        $data['active_menu_level'] = '';
        $data['plugins']           = [] ;
        $data['content_view']  = 'antrian/tampilanbaru';
        $this->load->view('antrian/baru', $data);
    }

    // mahmud, clear :: verifikasi kehadiran/cek kehadiran pasien
    public function antrian_verifdaftarperiksa()
    {
       
        $data['content_view']= 'antrian/v_verifpendaftaranperiksa';
        $data['title_page']  = 'Verifikasi Pendaftaran Periksa';
        $data['plugins']     = [] ;
        $data['script_js']   = ['antrian_antrian'];
        $this->load->view('antrian/v_index', $data); 
    }
    //  mahmud, clear :: 
    public function cek_verifdaftarperiksa()
    {
        $this->db->query("SELECT pp.idpendaftaran, rj.idunit, rp.idpemeriksaan, ppas.idperson FROM person_pendaftaran pp, rs_pemeriksaan rp, rs_jadwal rj, person_pasien ppas where pp.norm='".$this->input->post('rm')."' and rp.idpendaftaran = pp.idpendaftaran and rj.idjadwal = rp.idjadwal and date(rj.tanggal)='".date('Y-m-d')."' and ppas.norm = pp.norm");
    }

    // antrian farmasi
    public function farmasi_buatantrian()
    {  
       $this->load->model('mkombin');
       $norm=$this->input->post('norm');
       $farmasi = $this->mkombin->afr_cekadaresep($norm)->row_array();
       if(!$farmasi){echo json_encode(['status'=>'danger','message'=>'Generate Antrian gagal..! <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pastikan pasien sudah diperiksa/belum pulang..!']);return;}
       $callid  = generaterandom(8);
       $this->ql->antrian_autonumber('FRM'.$farmasi['expired'].$farmasi['idloket'],$callid,$farmasi['expired']);
       $dataa = ['idloket'=>$farmasi['idloket'], 'tanggal'=>$farmasi['expired'], 'no'=>$this->db->insert_id(), 'status'=>'antri','idperson'=>$farmasi['idperson'],'idpendaftaran'=>$farmasi['idpendaftaran']];
       $this->db->insert('antrian_antrian',$dataa);
       $loket = $this->db->query("select namaloket from antrian_loket where idloket='".$farmasi['idloket']."'")->row_array();
       $loket ['carabayar']=$farmasi['carabayar']; 
       $dtpasien = $this->db->query("select norm, concat(ifnull(identitas,''),'. ',namalengkap) as namalengkap, namaunit, alamat from vrs_identitas_periksa_pasien where norm='".$norm."' order by tglperiksa desc limit 0,1")->row_array();
       //update staus antrian farmasi di pemeriksaan
       $this->rs_pemeriksaan_update_where(['statusantrianfarmasi'=>'1'],['norm'=>$norm]);
       //save log task antrean
       //    $this->bridging_save_log_antrean(5,0,'sedang proses',0,$farmasi['idpendaftaran']); //[Andri] remove this to prevent duplicate task id 6
       echo json_encode(['no'=>$this->ql->antrian_get_number($callid),'loket'=>$loket,'datapasien'=>$dtpasien]);
    }
    
    public function buatantriankasir()
    {
       $callid  = generaterandom(8);
       $expired = DATE('Y-m-d');
       $idperson= $this->input->post('idper');
       $idpend  = $this->input->post('idp');
       
       $cek = $this->db->query("select idloket from antrian_antrian where idperson='".$idperson."' and idpendaftaran='".$idpend."' and idloket='56'");
       //cek jika pasien sudah antri
       if($cek->num_rows() > 0)
       {
           //update antrian untuk dipanggil
           $arrMsg = $this->db->update('antrian_antrian',['status'=>'panggilkasir'],['idperson'=>$idperson,'idpendaftaran'=>$idpend,'idloket'=>56]);
       }
       else
       {
            $loket = $this->db->select('idloket')->get_where('antrian_loket',['kodeloket'=>'KASIR'])->row_array();
            $this->ql->antrian_autonumber('KSR'.$expired.$loket['idloket'],$callid,$expired);
            $dataa = ['idloket'=>$loket['idloket'], 'tanggal'=>$expired, 'no'=>$this->db->insert_id(), 'status'=>'panggilkasir','idperson'=>$idperson,'idpendaftaran'=>$idpend];
            $arrMsg = $this->db->insert('antrian_antrian',$dataa);
       }
       
       pesan_success_danger($arrMsg, 'Pemanggilan Kasir Berhasil.', 'Pemanggilan Kasir Gagal.', 'js');
    }
    
    //pengaturan video
    public function pengaturan_video()
    {
        if($this->pageaccessrightbap->checkAccessRight(V_PENGATURAN_VIDEO))
        {
            $get = $this->input->get();
            $start = ((isset($get['halaman']) && $get['halaman'] > 1) ? ( ($get['halaman'] - 1) * 12)  : 0 );
        
            $data = [ 'content_view'        => 'antrian/v_pengaturan_video',
                    'active_menu'       => 'antrian',
                    'active_sub_menu'   => 'pengaturan_video',
                    'title_page'        => 'Pengaturan Video',
                    'halaman'           => $this->db->select('ceil(sum(1) / 12) as jumlah, sum(1) as total')->get("antrian_video")->row_array(),
                    'video'             => $this->db->limit(12, $start)->get('antrian_video')->result_array(),
                    // 'video'             => $this->db->query("SELECT * FROM antrian_video left join antrian_video_timeupload on antrian_video.idvideo = antrian_video_timeupload.id_video limit $start, 12")->result_array(),
                    'active_menu_level' => '',
                    'plugins'           => [PLUG_VALIDATION],       
                    'get'               => $get,
                    'script_js'         => ['js_antrian_pengaturan_video'],
               ];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    //upload video
    public function upload_video()
    {   
        $tmpName  = $_FILES['upload_files']['tmp_name'];     
        
        $fileName = str_replace('"', '', str_replace("'", '', $_FILES['upload_files']['name']));
        $newFileName = '/display_antrian/'.$fileName;
   
        $config = $this->ftp_upload();
        
        //upload ke nas storage
        $upload = $this->ftp->connect($config);
        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
        $upload = $this->ftp->close();
        
        //simpan data
        if($upload)
        {
            $arrMsg = $this->db->insert('antrian_video',['namavideo'=>$fileName,'dokumen'=>$newFileName,'date_created'=> date('Y-m-d H:i:s')]);
            pesan_success_danger($arrMsg, 'Unggah Video Berhasil.', 'Unggah Video Gagal.','js');
            return true;
        }
        
        $uploaded =  (($upload)? ['error'=>false,'status'=>'success','message'=>'Unggah Video Berhasil.'] : ['error'=>true,'status'=>'danger','message'=>'Unggah Video Berhasil.']);
        echo json_encode($uploaded);
    }

    //hapus video
    public function hapus_video()
    {
        
        $idvideo  = $this->input->post('idvideo');
        $fileName = $this->input->post('dokumen');
        
        //hapus data video di database
        $arrMsg = $this->db->delete('antrian_video',['idvideo'=>$idvideo]);
        
        //hapus video di NAS
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);
        $delete = $this->ftp->delete_file(FOLDERNASSIMRS.$fileName);
        $delete = $this->ftp->close();
        pesan_success_danger($arrMsg, 'Hapus Video Berhasil.', 'Hapus Video Gagal.','js');
        
    }

}
/* End of file ${TM_FILENAME:${1/(.+)/Cantrian.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cantrian/:application/controllers/${1/(.+)/Cantrian.php/}} */






