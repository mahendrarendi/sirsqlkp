<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cindikatormutu extends MY_controller
{

    protected $tb_mutu_indikator             = 'mutu_indikator';
    protected $tb_mutu_pendataan_indikator   = 'mutu_pendataan_indikator';
    protected $tb_mutu_hasil                 = 'mutu_hasil';
    protected $tb_mutu_pengaturan            = 'mutu_pengaturan';
    protected $tb_mutu_analisarencana        = 'mutu_analisarencana';
    protected $tb_login_user                 = 'login_user';
    protected $tb_akre_unit                  = 'akre_unit';
    protected $tb_mutu_daftar_ikp            = 'mutu_daftar_ikp';
    protected $tb_mutu_pengaturanikp         = 'mutu_pengaturanikp';
    protected $tb_mutu_rekapikp              = 'mutu_rekapikp';
    protected $tb_mutu_pendataan_ppi         = 'mutu_pendataan_ppi';
    protected $tb_mutu_daftarppi             = 'mutu_daftarppi';
    protected $tb_mutu_pengaturanppi         = 'mutu_pengaturanppi';
    protected $tb_mutu_rekap_ppi             = 'mutu_rekap_ppi';
    protected $tb_mutu_pelaporan_ikp         = 'mutu_pelaporan_ikp';
    protected $tb_mutu_investigasi_sederhana = 'mutu_investigasi_sederhana';
    protected $tb_mutu_lembarpdsa            = 'mutu_lembarpdsa';
    protected $tb_mutu_tindakan              = 'mutu_tindakan';
    protected $tb_mutu_dokumen_pdsa          = 'mutu_dokumen_pdsa';


    function __construct(){
        parent::__construct();

        /**
         * Not loged in redirect to login
         */
        if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
        
        $this->load->model('msqlbasic');
        $this->load->model('mmutu');

        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation','session','Pdf'));
    }

    public function index()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

            $list_daftarindikator = $this->mmutu->get_listdaftarimutrs($bulan,$tahun);
            
            $data = [
                'content_view'      => 'indikatormutu/v_indikatormutu',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'List Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'daftarindikator'   => $this->ajax_tablelistimutrs($list_daftarindikator,$bulan,$tahun),
                'active_sub_menu'   => 'imutrs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function listrekapimutrs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            // $list_daftarindikator = $this->mmutu->get_listdaftarimutrs($bulan,$tahun);
            $list_daftarindikator = $this->mmutu->get_listrekapimutrs($tahun);

            $data = [
                'content_view'      => 'indikatormutu/v_listrekapimutrs',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'List Rekap Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list_rekap'        => $this->ajax_tablerekapimutrs($list_daftarindikator,"",$tahun),
                'active_sub_menu'   => 'rekapimutrs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function listdaftarimutrs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            $list_daftarindikator = $this->mmutu->get_listdaftarimutrs($bulan,$tahun);

            $data = [
                'content_view'      => 'indikatormutu/v_listdaftarimutrs',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'List Daftar Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list'              => $list_daftarindikator,
                'active_sub_menu'   => 'daftarimutrs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function change_userpengturanmutu()
    {
        $post   = $this->input->post();
        $userid = $this->security->sanitize_filename($post['userid']);
        if( empty($userid) ){
            $username   = "Silahkan Pilih User Unit Terlebih Dahulu";
            $iduser     = $userid;
        }else{
            $sql = "SELECT user_id_login,nama_unit FROM ".$this->tb_akre_unit." WHERE user_id_login='".$userid."'";
            $get_unit_akre = $this->msqlbasic->custom_query($sql)[0];
    
            $username   = $get_unit_akre['nama_unit'];
            $iduser     = $get_unit_akre['user_id_login'];
        }

        echo json_encode(['hsl'=>$username,'userid'=>$iduser]);

    }

    public function pengaturan()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_pengaturan = $this->msqlbasic->show_db($this->tb_mutu_pengaturan);

            // $api_sep        = $this->bpjsbap->get_data_sep('0179R0140922V004682'); 
            // $norujukan      = ( $api_sep['metaData']->code == 200 ) ? $api_sep['response']->noRujukan : '';
            // $api_bpjs_sep   = $this->bpjsbap->get_vclaim_sep('0179R0141022V002419');
            // $this->load->model('mevelin');
            // echo '<pre>';
            // print_r( $this->mevelin->get_pendaftaran_bynorm_periksa('10158025','2022-10-10') );
            // print_r($api_bpjs_sep['response']->noRujukan);
            // echo '</pre>';

            $data = [
                'content_view'      => 'indikatormutu/v_pengaturan',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'Pengaturan Unit Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list_pengaturan'   => $get_pengaturan,
                'active_sub_menu'   => 'pengaturanimut',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_akses($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            $get_pengaturan         = $this->msqlbasic->row_db($this->tb_mutu_pengaturan,['id_pengaturan'=>$id]);
            $get_unit_akre          = $this->msqlbasic->show_db($this->tb_akre_unit);

            $data = [
                'content_view'      => 'indikatormutu/v_tambahakses',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'Tambah Akses Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( isset($id) ) ? 'edit':'',
                // 'allusers'          => get_allusers(),
                'allusers'          => $get_unit_akre,
                'get_pengaturan'    => $get_pengaturan,
                'allindikator'      => $this->mmutu->get_listdaftarimutrs($bulan,$tahun),
                'active_sub_menu'   => 'pengaturanimut',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_indikator($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $get_dataindikator       = $this->mmutu->get_editindikator($id);
            $all_pendataan_indikator = $this->mmutu->get_editpendataan($id);

            $data = [
                'content_view'      => 'indikatormutu/v_tambahindikator',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'Tambah Daftar Indikator Mutu RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( $id !=null ) ? 'edit' : 'tambah',
                'paramid'           => ( $id !=null ) ? $id : '',
                'get_edit'          => $get_dataindikator,
                'all_pendataan'     => $all_pendataan_indikator,
                'active_sub_menu'   => 'daftarimutrs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambahkan_pendataan()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('tipe_variable', 'Tipe Variable', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        $this->form_validation->set_rules('indkator_pendataan', 'Indikator Pendataan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('satuan', 'Satuan Pendataan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $tipe_variable      = $this->security->sanitize_filename($post['tipe_variable']);
            $indkator_pendataan = $this->security->sanitize_filename($post['indkator_pendataan']);
            $satuan             = $this->security->sanitize_filename($post['satuan']);

            $data = [
                'tipe_variable'         => $tipe_variable,
                'indkator_pendataan'    => $indkator_pendataan,
                'satuan'                => $satuan,
            ];
            
            echo json_encode($data);
        }
    }

    public function simpan_tambahindikator()
    {
        $post = $this->input->post();

        if( isset( $post['paramid'] ) ){
            $this->form_validation->set_rules('paramid', 'Pastikan Data Benar yang diedit', 'required',
                array('required' => 'Harap %s.')
            ); 
        }

        $this->form_validation->set_rules('judul_indikator', 'Judul Indikator', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('defenisi_operasional', 'Defenisi Operasional', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('kriteria_inklusi', 'Kriteria Inklusi', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('kriteria_ekslusi', 'Kriteria Ekslusi', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('cek_dataindikator', 'Numerator & Denumerator', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $judul_indikator        = $this->security->sanitize_filename($post['judul_indikator']);
            $defenisi_operasional   = $this->security->sanitize_filename($post['defenisi_operasional']);
            $kriteria_inklusi       = $this->security->sanitize_filename($post['kriteria_inklusi']);
            $kriteria_ekslusi       = $this->security->sanitize_filename($post['kriteria_ekslusi']);
            $cek_dataindikator      = $this->security->sanitize_filename($post['cek_dataindikator']);
            $target                 = $this->security->sanitize_filename($post['target']);
            $satuan_indikator       = $this->security->sanitize_filename($post['satuan_indikator']);

            $data_indikator = [
                'datecreated'           => indonesia_date(),
                'judul_indikator'       => $judul_indikator,
                'tanggal'               => date('Y-m-d'),
                'defenisi_operasional'  => $defenisi_operasional,
                'kriteria_inklusi'      => $kriteria_inklusi,
                'kriteria_ekslusi'      => $kriteria_ekslusi,
                'target'                => $target,
                'satuan'                => $satuan_indikator,
            ]; 

            if( isset($post['paramid']) ){
                
                unset( $data_indikator['datecreated'] );
                unset( $data_indikator['tanggal'] );

                $update_indikator = $this->msqlbasic->update_db($this->tb_mutu_indikator,['id_indikator'=>$post['paramid']],$data_indikator);
                $insert_indikator = $post['paramid'];

            }else{
                $insert_indikator = $this->msqlbasic->insert_data($this->tb_mutu_indikator, $data_indikator);
            }

            # array
            $tipe_variable          = $post['tipe_variable'];
            $indkator_pendataan     = $post['indkator_pendataan'];
            $satuan_variable        = $post['satuan'];            
            
            if( isset($post['paramid']) ){
                $delete_pendataan       = $this->msqlbasic->delete_db($this->tb_mutu_pendataan_indikator,['indikator_id'=>$insert_indikator]);
            }
            
            $found = false;
            foreach( $tipe_variable as $key => $tipe ){
                $data_pendataan = [
                    'indikator_id'  =>  $insert_indikator,
                    'tipe_variabel' =>  $tipe,
                    'indikator'     =>  $indkator_pendataan[$key],
                    'satuan'        =>  $satuan_variable[$key]
                ];

                $insert_pendataan = $this->msqlbasic->insert_data($this->tb_mutu_pendataan_indikator, $data_pendataan);
                
                if( $insert_pendataan > 0 ){
                    $found = true;
                }

            }

            if( $found ){
                $return = ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('cindikatormutu/listdaftarimutrs')];
            }else{
                $return = ['msg'=>'danger','txt'=>'Data Gagal di Simpan'];
            }

            echo json_encode( $return );

        }

    }

    public function remove_indikatormutu()
    {
        $post = $this->input->post();
        $idindikator  = $this->security->sanitize_filename($post['id']);

        $delete_indikator  = $this->msqlbasic->delete_db($this->tb_mutu_indikator,['id_indikator'=>$idindikator]);

        if( $delete_indikator > 0 ){
            $delete_pendataan  = $this->msqlbasic->delete_db($this->tb_mutu_pendataan_indikator,['indikator_id'=>$idindikator]);

            $delete_mutuhasil           = $this->msqlbasic->delete_db($this->tb_mutu_hasil,['indikator_id'=>$idindikator]);
            $delete_mutuanalisarencana  = $this->msqlbasic->delete_db($this->tb_mutu_analisarencana,['indikator_id'=>$idindikator]);

            // $update_pengaturan_indikatorid  = $this->msqlbasic->update_db($this->tb_mutu_hasil,['id_hasil'=>$cek_data_insert['id_hasil']],['hasil'=>$hasil]);
            
            # update pengaturan indikator
            $this->mmutu->get_pengaturan_indikator_row($idindikator);

            if( $delete_pendataan ){
                echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
            }else{
                echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
            }
        }

    }

    public function view_indikatormutu()
    {
        $post           = $this->input->post();
        $idindikator    = $this->security->sanitize_filename( $post['id'] );
        
        $get_view       = $this->mmutu->get_editindikator($idindikator);

        $get_allpendataan = $this->mmutu->get_editpendataan($idindikator);

        $id_indikator           = $get_view->id_indikator; 
        $judul_indikator        = $get_view->judul_indikator; 
        $defenisi_operasional   = $get_view->defenisi_operasional; 
        $kriteria_inklusi       = $get_view->kriteria_inklusi; 
        $kriteria_ekslusi       = $get_view->kriteria_ekslusi; 
        $target                 = $get_view->target; 
        $satuan                 = $get_view->satuan; 

        $table = '<table class="table table-striped table-hover">
            <tr><th width="30%">Judul Indikator</th><td>'.$judul_indikator.'</td></tr>
            <tr><th width="30%">Defenisi Operasional</th><td>'.$defenisi_operasional.'</td></tr>
            <tr><th width="30%">Kriteria Inklusi</th><td>'.$kriteria_inklusi.'</td></tr>
            <tr><th width="30%">Kriteria Ekslusi</th><td>'.$kriteria_ekslusi.'</td></tr>
            <tr><th width="30%">Target</th><td>'.$target.'</td></tr>
            <tr><th width="30%">Satuan</th><td>'.$satuan.'</td></tr>
            <tr>
                <th width="30%">Indikator Pendataan</th>
                <td>
                    <table class="table table-striped table-hover"> 
                        <thead>
                            <tr class="header-table-ql">
                                <th>Variabel</th>
                                <th>Indikator</th>
                                <th>Satuan</th>
                            </tr>
                        </thead>';

                        foreach($get_allpendataan as $row):
                            $table .= '<tr>
                                    <td>'.$row->variabel.'</td>
                                    <td>'.$row->indikator.'</td>
                                    <td>'.$row->satuan.'</td>
                                </tr>';
                        endforeach;
                        
                    '</table>
                </td>
            </tr>
        </table> ';
        
        echo json_encode(['hsl'=>$table]);
    }

    /**
     * Revisi #1
     */
    public function input_hasil_pendataan()
    {
        $post           = $this->input->post();
        $id_indikator   = $post['id'];

        # revisi
        $bulan          = $post['bulan'];
        $tahun          = $post['tahun'];
        
        $get_pendataan  = $this->mmutu->get_editpendataan($id_indikator);

        //old
        // $get_bt = $this->bulan_tahun();
        // $bulan = $get_bt['bulan'];
        // $tahun = $get_bt['tahun'];

        $body = '<div class="err-msg-pendataan"></div>
        <table class="table table-striped table-hover ql-dt">
                <thead>
                    <tr class="header-table-ql">
                        <th>No</th>
                        <th>Variable</th>
                        <th width="50%">Indikator</th>
                        <th >Hasil</th>
                        <th>Satuan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>';
                $no = 1;
                foreach( $get_pendataan as $row ):
                    $get_hasil = $this->mmutu->get_hasil_byidpendataan($row->id_pendataan,get_session_userid(),$bulan,$tahun);

                    $body .= '<tr>
                        <td>'.$no++.'</td>
                        <td>'.$row->variabel.'</td>
                        <td>'.$row->indikator.'</td>
                        <td>
                            <input required style="opacity: 0;" type="number" min="0" value="'.(empty( $get_hasil) ? '' : $get_hasil->hasil ).'" class="form-control hasil" name="hasil" placeholder="Hasil ..."/ required>
                        </td>
                        <td>'.$row->satuan.'</td>
                        <td class="actions">
                            <div class="wrap-button">
                                <a class="editpendataan" data-bln="'.$bulan.'" data-th="'.$tahun.'" data-id="'.$row->id_pendataan.'" href="javascript:void(0)"><i class="fa fa-pencil"></i></a>
                            </div>
                        </td>
                    </tr>';
                endforeach;
        $body .='</tbody>
            </table>';

        $data = [
            'title' => 'Pendataan Indikator - '.ql_namabulan((int) $bulan).' '.$tahun,
            'body'  => $body
        ];

        echo json_encode($data);

    }

    public function bulan_tahun()
    {
        $tanggal = date('d');
        $bulan = date('m');
        $tahun = date('Y');

        $data =[
            'tanggal' => $tanggal,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ];

        return $data;
    }

    public function simpanhasilpendataan()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('hasil', 'Hasil', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>strip_tags(validation_errors()));
            echo json_encode( $return );
        }else{
            $idpendataan   = $this->security->sanitize_filename($post['id']);
            $hasil         = $this->security->sanitize_filename($post['hasil']);
            $bulan         = $this->security->sanitize_filename($post['bulan']);
            $tahun         = $this->security->sanitize_filename($post['tahun']);

            $userid        = get_session_userid();

            $get_pendataan = $this->msqlbasic->row_db($this->tb_mutu_pendataan_indikator,['id_pendataan'=>$idpendataan]);

            $data_hasil    = [
                'bulan'        => $bulan,
                'tahun'        => $tahun,
                'pendataan_id' => $idpendataan,
                'indikator_id' => $get_pendataan['indikator_id'],
                'hasil'        => $hasil,
                'author'       => $userid,
            ]; 

            $cek_data_insert = $this->msqlbasic->row_db($this->tb_mutu_hasil,['pendataan_id'=>$idpendataan,'author'=>$userid,'bulan'=>$bulan,'tahun'=>$tahun]);

            if( !empty($cek_data_insert) ){
                $update        = $this->msqlbasic->update_db($this->tb_mutu_hasil,['id_hasil'=>$cek_data_insert['id_hasil']],['hasil'=>$hasil]);
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update'] );
            }else{
                $insert_hasil  = $this->msqlbasic->insert_data($this->tb_mutu_hasil,$data_hasil);
                if( $insert_hasil ){
                    echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Input'] );
                }else{
                    echo json_encode( ['msg'=>'success','txt'=>'Data Gagal di Input'] );
                } 
            }

            // $update        = $this->msqlbasic->update_db($this->tb_mutu_pendataan_indikator,['id_pendataan'=>$idpendataan],['hasil'=>$hasil]);
            
        }

    }

    public function filterimutrs()
    {
        $post = $this->input->post();
        $filterindikatormutu = $post['filterindikatormutu'];
        $array_data = explode('-', $filterindikatormutu);
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        $list_daftarindikator = $this->mmutu->get_listdaftarimutrs($bulan,$tahun);

        $list_table = $this->ajax_tablelistimutrs($list_daftarindikator,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }


    public function filter_rekap()
    {
        $post = $this->input->post();
        $filterrekapmutu = $post['filterrekapmutu'];
        $array_data = explode('-', $filterrekapmutu);
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        // $list_daftarindikator = $this->mmutu->get_listdaftarimutrs($bulan,$tahun);
        $list_daftarindikator = $this->mmutu->get_listrekapimutrs($tahun);
        

        $list_table = $this->ajax_tablerekapimutrs($list_daftarindikator,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }

    public function ajax_tablelistimutrs($daftarindikator,$bulan,$tahun)
    {
        ob_start();
        
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_akses(get_session_userid());

        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['indikator_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaximutrs.php';

        return ob_get_clean();

    }

    public function ajax_tablerekapimutrs($daftarindikator,$bulan="",$tahun)
    {
        ob_start();
        
        $jumlah_bulan = ql_bulans();

        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxrekapimutrs.php';

        return ob_get_clean();

    }

    public function analisa_ajax()
    {
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_akses(get_session_userid());

        $userid = empty( $obj_pengaturan ) ? '' : $obj_pengaturan['userid_akses'];

        $post             = $this->input->post();
        $id_indikator     = (int) $this->security->sanitize_filename($post['id']);
        $analisa          = $this->security->sanitize_filename($post['value']);
        $idpendataan      = $this->security->sanitize_filename($post['idpendataan']);
        
        $bulantahun       = $this->security->sanitize_filename($post['bulantahun']);
        $array_data = explode('-', $bulantahun);
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        $data_analisarencana = [
            'bulan'         => $bulan,
            'tahun'         => $tahun,
            'indikator_id'  => $id_indikator,
            'analisa'       => $analisa,
            'author'        => $userid
        ];
        
        $cek_analisarencana = $this->msqlbasic->row_db( $this->tb_mutu_analisarencana,['indikator_id'=>$id_indikator,'author'=>$userid,'bulan'=>$bulan,'tahun'=>$tahun] );

        if( !empty( $cek_analisarencana ) ){
            $update = $this->msqlbasic->update_db($this->tb_mutu_analisarencana, ['indikator_id'=>$id_indikator,'author'=>$userid,'bulan'=>$bulan,'tahun'=>$tahun],['analisa'=>$analisa]);
            echo json_encode( ['msg'=>'success','txt'=>'Analisa Berhasil disimpan'] );
        }else{
            $insert = $this->msqlbasic->insert_data($this->tb_mutu_analisarencana,$data_analisarencana);
            if( $insert ){
                echo json_encode( ['msg'=>'success','txt'=>'Analisa Berhasil disimpan'] );
            }else{
                echo json_encode( ['msg'=>'danger','txt'=>'Analisa gagal disimpan'] );
            } 
        }
        // $update = $this->msqlbasic->update_db($this->tb_mutu_indikator, ['id_indikator'=>$id_indikator],$data_indikator);
        // $update = $this->msqlbasic->update_db($this->tb_mutu_hasil, ['pendataan_id'=>$idpendataan,'indikator_id'=>$id_indikator,'author'=>$userid],$data_indikator);
        // echo json_encode(['msg'=>'success','txt'=>'Analisa Berhasil disimpan','result'=>$update]);
    }

    public function rencana_tindaklanjut_ajax()
    {
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_akses(get_session_userid());

        $userid = empty( $obj_pengaturan ) ? '' : $obj_pengaturan['userid_akses'];
        
        $post             = $this->input->post();
        $id_indikator     = (int) $this->security->sanitize_filename($post['id']);
        $rencana_tindaklanjut  = $this->security->sanitize_filename($post['value']);
        $idpendataan      = $this->security->sanitize_filename($post['idpendataan']);

        $bulantahun       = $this->security->sanitize_filename($post['bulantahun']);
        $array_data = explode('-', $bulantahun);
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        $data_analisarencana = [
            'bulan'                 => $bulan,
            'tahun'                 => $tahun,
            'indikator_id'          => $id_indikator,
            'rencana_tindaklanjut'  => $rencana_tindaklanjut,
            'author'                => $userid
        ];
        
        $cek_analisarencana = $this->msqlbasic->row_db( $this->tb_mutu_analisarencana,['indikator_id'=>$id_indikator,'author'=>$userid,'bulan'=>$bulan,'tahun'=>$tahun] );

        if( !empty( $cek_analisarencana ) ){
            $update = $this->msqlbasic->update_db($this->tb_mutu_analisarencana, ['indikator_id'=>$id_indikator,'author'=>$userid,'bulan'=>$bulan,'tahun'=>$tahun],['rencana_tindaklanjut'=>$rencana_tindaklanjut]);
            echo json_encode( ['msg'=>'success','txt'=>'Rencana Tindaklanjut Berhasil disimpan'] );
        }else{
            $insert = $this->msqlbasic->insert_data($this->tb_mutu_analisarencana,$data_analisarencana);
            if( $insert ){
                echo json_encode( ['msg'=>'success','txt'=>'Rencana Tindaklanjut Berhasil disimpan'] );
            }else{
                echo json_encode( ['msg'=>'danger','txt'=>'Rencana Tindaklanjut gagal disimpan'] );
            } 
        }

        // $update = $this->msqlbasic->update_db($this->tb_mutu_hasil, ['pendataan_id'=>$idpendataan,'indikator_id'=>$id_indikator,'author'=>$userid],$data_indikator);
        // $update = $this->msqlbasic->update_db($this->tb_mutu_indikator, ['id_indikator'=>$id_indikator],$data_indikator);
        // echo json_encode(['msg'=>'success','txt'=>'Rencana Tindaklanjut Berhasil disimpan','result'=>$update]);
    }

    public function tambah_indikator_item()
    {
        $post = $this->input->post();
        $indikator_id = (int) $this->security->sanitize_filename($post['indikator_id']);

        $get_indikator = $this->msqlbasic->row_db($this->tb_mutu_indikator,['id_indikator'=>$indikator_id]);
        
        $tr = '<tr>
            <td> <input type="hidden" name="idindikator[]" value="'.$get_indikator['id_indikator'].'"/>'.$get_indikator['judul_indikator'].'</td>
            <td><a data-toggle="tooltip" data-placement="bottom" title="Hapus Indikator" class="remove-indikator-item btn btn-xs btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
        </tr>';
        
        echo json_encode(['hsl'=> $tr]);

    }

    public function simpan_pengaturan_mutu()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('alluser', 'User Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('unit', 'Unit', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('idindikator[]', 'Indikator Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>strip_tags(validation_errors()));
            echo json_encode( $return );
        }else{
            $userid = (int) $this->security->sanitize_filename($post['alluser']);
            $unit   = $this->security->sanitize_filename($post['unit']);
            $idindikator = $post['idindikator']; //array

            $idindikator_array=[];
            foreach( $idindikator as $id ){
               $idindikator_array[] = (int) $this->security->sanitize_filename($id);
            }

            $data_pengaturan = [
                'unit' =>$unit,
                'userid_akses' =>$userid,
                'indikator_id' =>implode(',',$idindikator)
            ];

            if( isset($post['idpengaturan_hidden']) ){
                $idhidden      = $this->security->sanitize_filename($post['idpengaturan_hidden']);
                $update        = $this->msqlbasic->update_db($this->tb_mutu_pengaturan,['id_pengaturan'=>$idhidden],$data_pengaturan);
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('cindikatormutu/pengaturan')] );
            }else{
                $insert = $this->msqlbasic->insert_data($this->tb_mutu_pengaturan,$data_pengaturan);
                // $insert = true;
                if( $insert ){
                    echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('cindikatormutu/pengaturan')] );
                }else{
                    echo json_encode( ['msg'=>'danger','txt'=>'Data Gagal di Simpan'] );
                } 
            }
            
        }

    }

    public function remove_pengaturan()
    {
        $post = $this->input->post();
        $id   = (int) $this->security->sanitize_filename($post['id']);

        $get_pengaturan  = $this->msqlbasic->row_db($this->tb_mutu_pengaturan,['id_pengaturan'=>$id]);
        $idindikator = $get_pengaturan['indikator_id'];

        $delete_mutuhasil           = $this->msqlbasic->delete_db($this->tb_mutu_hasil,['indikator_id'=>$idindikator]);
        $delete_mutuanalisarencana  = $this->msqlbasic->delete_db($this->tb_mutu_analisarencana,['indikator_id'=>$idindikator]);
        
        $delete  = $this->msqlbasic->delete_db($this->tb_mutu_pengaturan,['id_pengaturan'=>$id]);

        if( $delete > 0 ){
            
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    public function get_pengaturan_akses($userid)
    {
        $query = $this->msqlbasic->row_db($this->tb_mutu_pengaturan,['userid_akses'=>$userid]);

        return $query;
    }

    public function view_detail_analisarencana()
    {
        $query = $this->mmutu->get_list_detailanalisarencana();

        $body = $this->popup_list_detail_analisarencana($query);

        $data = [
            'title'=>'Detail Analisa & Rencana Tindak Lanjut',
            'body' => $body,
        ];

        echo json_encode($data);
    }

    public function popup_list_detail_analisarencana($list_detailanalisarencana)
    {
        ob_start();
        include FCPATH. 'application/views/indikatormutu/v_popup-list-detail-analisarencana.php';
        return ob_get_clean();
    }

    public function lembarpdsa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $list_datapdsa = $this->mmutu->get_listpdsa();

            $data = [
                'content_view'      => 'indikatormutu/v_lembarpdsa',
                'active_menu'       => 'Indikatormutu',
                'title_page'        => 'Lembar Kerja PDSA',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list'              => $list_datapdsa,
                'active_sub_menu'   => 'lembarpdsa',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_datapdsa($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_datapdsa       = $this->mmutu->get_editpdsa($id);
            $all_pendataan_pdsa = $this->mmutu->get_edittindakan($id);
            // $all_dokumen = $this->mmutu->get_dokumen($id);

            $get_indikator           = $this->msqlbasic->show_db($this->tb_mutu_indikator);

            $data = [
                'content_view'      => 'indikatormutu/v_tambahpdsa',
                'active_menu'       => 'Indikatormutu',
                'title_page'        => 'Tambah Data PDSA',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( $id !=null ) ? 'edit' : 'tambah',
                'paramid'           => ( $id !=null ) ? $id : '',
                'get_edit'          => $get_datapdsa,
                'all_pendataan'     => $all_pendataan_pdsa,
                // 'all_dok'           => $all_dokumen,
                'allindikator'      => $get_indikator,
                'active_sub_menu'   => 'lembarpdsa',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambahkan_tindakan()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('tindakan', 'Tindakan', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        $this->form_validation->set_rules('pic', 'PIC', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $tindakan  = $this->security->sanitize_filename($post['tindakan']);
            $pic       = $this->security->sanitize_filename($post['pic']);
            $tanggal   = $this->security->sanitize_filename($post['tanggal']);

            $data = [
                'tindakan'  => $tindakan,
                'pic'       => $pic,
                'tanggal'   => $tanggal,
            ];
            
            echo json_encode($data);
        }
    }

    public function get_simpan_file_element()
    {
        $post       = $this->input->post();

        $pathname = FCPATH.'upload/';
        $filename = [];
        // $found = false;

        $count_uploaded_files = count( $_FILES['filedocument']['name'] );

        $files = $_FILES;
        for( $i = 0; $i < $count_uploaded_files; $i++ ){

            $_FILES['filedoc'] = [
                'name'     => $files['filedocument']['name'][$i],
                'type'     => $files['filedocument']['type'][$i],
                'tmp_name' => $files['filedocument']['tmp_name'][$i],
                'error'    => $files['filedocument']['error'][$i],
                'size'     => $files['filedocument']['size'][$i]
            ];

                $tmpName        = $files['filedocument']['tmp_name'][$i];       
                $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['filedocument']['name'][$i]));
                $newFileName    = $fileName;
                
                if( $files['filedocument']['type'][$i] == 'application/pdf'){   
            
                $names[]          = $fileName;    

                move_uploaded_file($tmpName, FCPATH.'upload/'.$newFileName);
                // $config = $this->ftp_upload();
                
                # upload ke nas storage
                // $upload = $this->ftp->connect($config);
                // $upload = $this->ftp->upload($tmpName, FCPATH.$newFileName);
                // $upload = $this->ftp->close();

            } //end if cek dokumen is pdf

            $data_element = [
                'filename'  => $newFileName,
                'pathname'  => $tmpName,
            ];
            // print_r($data_element);
            echo json_encode($data_element);
            
        } // .end for files
    }

    public function simpan_tambahpdsa()
    {
        $post = $this->input->post();

        if( isset( $post['paramid'] ) ){
            $this->form_validation->set_rules('paramid', 'Pastikan Data Benar yang diedit', 'required',
                array('required' => 'Harap %s.')
            ); 
        }

        $this->form_validation->set_rules('judul', 'Judul indikator', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('cara_perbaikan', 'Cara Perbaikan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('siklus', 'Siklus', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('plan_rencana', 'Plan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('plan_harap', 'Plan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('do', 'Do', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('study', 'Study', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('act', 'Act', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('penanggungjawab', 'Penanggungjawab', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $judul_indikator    = $this->security->sanitize_filename($post['judul']);
            $cara_perbaikan     = $this->security->sanitize_filename($post['cara_perbaikan']);
            $siklus             = $this->security->sanitize_filename($post['siklus']);
            $plan_rencana       = $this->security->sanitize_filename($post['plan_rencana']);
            $plan_harap         = $this->security->sanitize_filename($post['plan_harap']);
            $do                 = $this->security->sanitize_filename($post['do']);
            $study              = $this->security->sanitize_filename($post['study']);
            $act                = $this->security->sanitize_filename($post['act']);
            $penanggungjawab    = $this->security->sanitize_filename($post['penanggungjawab']);

            $data_pdsa = [
                'datecreated'           => indonesia_date(),
                'judul_indikator'       => $judul_indikator,
                'cara_perbaikan'        => $cara_perbaikan,
                'siklus'                => $siklus,
                'plan_rencana'          => $plan_rencana,
                'plan_harap'            => $plan_harap,
                'do'                    => $do,
                'study'                 => $study,
                'act'                   => $act,
                'penanggungjawab'       => $penanggungjawab,
            ]; 

            if( isset($post['paramid']) ){
                
                unset( $data_pdsa['datecreated'] );

                $update_pdsa = $this->msqlbasic->update_db($this->tb_mutu_lembarpdsa,['idpdsa'=>$post['paramid']],$data_pdsa);
                $insert_pdsa = $post['paramid'];

            }else{
                $insert_pdsa = $this->msqlbasic->insert_data($this->tb_mutu_lembarpdsa, $data_pdsa);
            }

            # array
            $tindakan       = $post['tindakan'];
            $pic            = $post['pic'];
            $tanggal        = $post['tanggal'];            
            
            if( isset($post['paramid']) ){
                $delete_pendataan     = $this->msqlbasic->delete_db($this->tb_mutu_tindakan,['idpdsa'=>$insert_pdsa]);

                // $delete_dokumen       = $this->msqlbasic->delete_db($this->tb_mutu_dokumen_pdsa,['idpdsa'=>$insert_pdsa]);
            }

            // if (isset($post['filename']) AND isset($post['pathname']))
            // {
            //     $filename       = $post['filename']; 
            //     $pathname       = $post['pathname']; 
                
                $found = false;
                foreach( $tanggal as $key => $tipe ){
                    $data_pendataan = [
                        'idpdsa'        =>  $insert_pdsa,
                        'tindakan'      =>  $tindakan[$key],
                        'pic'           =>  $pic[$key],
                        'tanggal'       =>  $tanggal[$key],
                    ];

                    $insert_pendataan = $this->msqlbasic->insert_data($this->tb_mutu_tindakan, $data_pendataan);
                    
                    if( $insert_pendataan > 0 ){
                        $found = true;
                    }

                }

            //     $found = false;
            //     foreach( $filename as $jk => $file ){
            //         $data_dokumen = [
            //             'idpdsa'        =>  $insert_pdsa,
            //             'filename'      =>  $file,
            //             'pathname'      =>  $pathname[$jk],
            //         ];

            //         $insert_dokumen = $this->msqlbasic->insert_data($this->tb_mutu_dokumen_pdsa, $data_dokumen);
                    
            //         if( $insert_dokumen > 0 ){
            //             $found = true;
            //         }
            //     }
            // } else {
                // $found = false;
                // foreach( $tanggal as $key => $tipe ){
                //     $data_pendataan = [
                //         'idpdsa'        =>  $insert_pdsa,
                //         'tindakan'      =>  $tindakan[$key],
                //         'pic'           =>  $pic[$key],
                //         'tanggal'       =>  $tanggal[$key],
                //     ];
    
                //     $insert_pendataan = $this->msqlbasic->insert_data($this->tb_mutu_tindakan, $data_pendataan);
                    
                //     if( $insert_pendataan > 0 ){
                //         $found = true;
                //     }
    
                // }
            // }

            if( $found ){
                $return = ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('Cindikatormutu/lembarpdsa')];
            }else{
                $return = ['msg'=>'danger','txt'=>'Data Gagal di Simpan'];
            }
            
            echo json_encode( $return );

        }

    }

    public function remove_datapdsa()
    {
        $post = $this->input->post();
        $id  = (int) $this->security->sanitize_filename($post['id']);

        $delete_datapdsa  = $this->msqlbasic->delete_db($this->tb_mutu_lembarpdsa,['idpdsa'=>$id]);

        if( $delete_datapdsa > 0 ){
            $delete_pendataan  = $this->msqlbasic->delete_db($this->tb_mutu_tindakan,['idpdsa'=>$id]);
            // $delete_dokumen  = $this->msqlbasic->delete_db($this->tb_mutu_dokumen_pdsa,['idpdsa'=>$id]);
            if( $delete_pendataan ){
                echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
            }else{
                echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
            }
            
        }

    }

    public function printlembarpdsa()
    {       
        $idpdsa  = $this->uri->segment('3');

        $data = [
            'idpdsa'=>$idpdsa,
        ];

        $this->pdf->filename = "lembarpdsa.pdf";
        $this->pdf->load_view_ql('indikatormutu/v_download_lembarpdsa',$data);
    }

    public function viewdokumen()
    {         
        $filename  = $this->uri->segment('3');

        $data = [
            'filename'=>$filename,
        ];

        $this->pdf->filename = $filename;
        $this->pdf->load_view_ql('indikatormutu/v_view_dokumen',$data,true);
    }

    //SURVEILANS IKP
    public function ikprs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            
            $post = $this->input->post();
            $get_bt = $this->bulan_tahun();
            $tanggal = $get_bt['tanggal'];
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];
            $userid   = get_session_userid();
            
            $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

            $list_daftarindikator = $this->mmutu->get_listdaftarikprs($tanggal,$bulan,$tahun,$userid);

            $data = [
                'content_view'      => 'indikatormutu/v_ikprs',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'List IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'daftarikprs'       => $this->ajax_tablelistikprs($list_daftarindikator,$tanggal,$bulan,$tahun,$userid),
                'active_sub_menu'   => 'ikprs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function ajax_tablelistikprs($daftarikprs,$tanggal,$bulan,$tahun,$userid)
    {
        ob_start();
        
        $kalender = CAL_GREGORIAN;
        $hari = cal_days_in_month($kalender, $bulan, $tahun);

        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxikprs.php';

        return ob_get_clean();

    }

    public function ajax_tablefilterikprs($daftarikprs,$bulan,$tahun)
    {
        ob_start();
        
        $kalender = CAL_GREGORIAN;
        $hari = cal_days_in_month($kalender, $bulan, $tahun);
        
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxikprs.php';

        return ob_get_clean();

    }

    public function pilihanikp_ajax()
    {
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_akses(get_session_userid());

        $userid = empty( $obj_pengaturan ) ? '' : $obj_pengaturan['userid_akses'];
        
        $post             = $this->input->post();

        $id_daftarikp     = (int) $this->security->sanitize_filename($post['iddaftarikp']);
        $pilihan          = $this->security->sanitize_filename($post['pilihan']);
        
        $tanggal        = $post['tanggal'];
        $bulan          = $post['bulan'];
        $tahun          = $post['tahun'];

        $data_pilihan = [
            'tanggal'       => $tanggal,
            'bulan'         => $bulan,
            'tahun'         => $tahun,
            'datecreated'   => indonesia_date(),
            'daftarikp_id'  => $id_daftarikp,
            'nilai_survey'  => $pilihan,
            'author'        => $userid,
        ];

        $cek_pilihan = $this->msqlbasic->row_db( $this->tb_mutu_rekapikp,['daftarikp_id'=>$id_daftarikp,'author'=>$userid,'tanggal'=>$tanggal,'bulan'=>$bulan,'tahun'=>$tahun] );
        
        if( !empty( $cek_pilihan ) ){
            $update = $this->msqlbasic->update_db($this->tb_mutu_rekapikp,['id_rekapikp'=>$cek_pilihan['id_rekapikp']],['nilai_survey'=>$pilihan]);   
            echo json_encode( ['msg'=>'success','txt'=>'Pilihan Berhasil disimpan'] );
        }else{
            $insert_pilihan = $this->db->insert('mutu_rekapikp',$data_pilihan);
            
            if( $insert_pilihan ){
                echo json_encode( ['msg'=>'success','txt'=>'Pilihan Berhasil disimpan'] );
            }else{
                echo json_encode( ['msg'=>'danger','txt'=>'Pilihan Gagal di Simpan'] );
            } 
        }
    }

    public function filterikprs()
    {
        $post           = $this->input->post();
        $filterikp      = $post['filterikp'];
        $array_data     = explode('-', $filterikp);
        $bulan          = $array_data[0];
        $tahun          = $array_data[1];
        
        $list_daftarikp = $this->mmutu->get_filterikprs($bulan,$tahun);

        $list_table = $this->ajax_tablefilterikprs($list_daftarikp,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }
    
    public function pelaporanikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $data = [
                'content_view'      => 'indikatormutu/v_pelaporanikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'Pelaporan IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list'              => '',
                'active_sub_menu'   => 'pelaporanikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }


     /**
     * mr.ganteng
     */
    public function tambah_pelaporanikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $ids = isset( $_GET['ids'] ) ? base64_decode( $_GET['ids'] ) : '';
            
            if( !empty($ids) ){

                $query_get = $this->db->select('*')
                    ->from('mutu_pelaporan_ikp')
                    ->where('idpelaporanikp',$ids)
                    ->get()->row();
            }else{
                $query_get = (object) array (
                    'idpelaporanikp' => '',
                    'norm' => '',
                    'nama' => '',
                    'umur' => '',
                    'jk' => '',
                    'penanggungjawab_pasien' => '',
                    'tglmasuk_rs' => '',
                    'jammasuk_rs' => '',
                    'tglinsiden' => '',
                    'jaminsiden' => '',
                    'insiden' => '',
                    'kronologi' => '',
                    'jenis_insiden' => '',
                    'orang_pertama_lapor' => '',
                    'insiden_terjadi' => '',
                    'insiden_terjadi_lainlain' => '',
                    'insiden_menyangkut_pasien' => '',
                    'form_insiden_menyangkut_pasien' => '',
                    'tempat_insiden' => '',
                    'insiden_terjadi_pada_pasien' => '',
                    'unit_terkait' => '',
                    'akibat_insiden_pasien' => '',
                    'tindakan_kejadian' => '',
                    'tindakan_dilakukan_oleh' => '',
                    'apakah_kejadian_pernah_terjadi' => '',
                    'form_kejadian_pasien' => '',
                    'grading_dampak' => '',
                    'grading_frequensi' => '',
                    'hasil_grading' => '',
                    'kategori_grading' => '',
                    'datecreated' => '',
                    'author' => '',
                  );
            }
                
            $data = [
                'content_view'      => 'indikatormutu/v_tambah_pelaporanikp',
                'active_menu'       => 'indikatormutu',
                'title_page'        => 'Tambah Pelaporan IKP',
                'active_menu_level' => 'indikatormutu',
                'mode'              =>  empty($ids) ? '' : 'edit',
                'list'              => '',
                'getquery_get'      => $query_get,
                'form_datapasien'   => $this->data_formsurvey_pasien(),
                'form_insiden'      => $this->data_formsurvey_insiden(),
                'active_sub_menu'   => 'pelaporanikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TIME,PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    /**
     * mr.ganteng
     */
    public function data_formsurvey_pasien()
    {
        $umur = [
            '0-1 bulan',
            '> 1 bulan - 1 tahun',
            '> 1 tahun - 5 tahun',
            '> 5 tahun - 15 tahun',
            '> 15 tahun - 30 tahun',
            '> 30 tahun - 65 tahun',
            '> 65 tahun',
        ];

        $jk = ['Laki-Laki','Perempuan'];
        
        $penanggungjawab_pasien = [
            'Pribadi',
            'Akses Pemerintah',
            'JAMKESMAS',
            'Asuransi Swasta',
            'Perusahaan',
            'Jaminan Kesehatan daerah',
        ];

        $data = compact('umur','jk','penanggungjawab_pasien');

        return $data;
    }

    /**
     * mr.ganteng
     */
    public function data_formsurvey_insiden()
    {
        $jenis_insiden  = [
            'Kejadian Nyaris Cidera/KNC (Near Miss)',
            'Kejadian Tidak Cidera/KTC (No Harm)',
            'Kejadian Tidak Diharapkan/KTD (Edverse Event)',
            'Kejadian sentinel (Sentinel Event)'
        ];

        $orang_pertama_lapor = [
            'Karyawan: Dokter, Perawat, Petugas Lain nya',
            'Pasien',
            'Keluarga Pasien/Pendamping Pasien',
            'Pengunjung',
            'Lain-Lain'
        ];

        $insiden_terjadi = ['Pasien','Lain-Lain'];

        $insiden_menyangkut_pasien = [
            'Pasien Rawat Inap',
            'Pasien Rawa Jalan',
            'Pasien UGD',
            'Lain-Lain',
        ];

        $insiden_terjadi_pada_pasien = [
            'Penyakit Dalam dan Subspesialisnya',
            'Anak dan Subspesialisnya',
            'Bedah dan Subspesialisnya',
            'Obsetri Genicology dan Subspesialisnya',
            'THT dan Subspesialisnya',
            'Mata dan Subspesialisnya',
            'Saraf dan Subspesialisnya',
            'Anastesi dan Subspesialisnya',
            'Kulit & Kelamin dan Subspesialisnya',
            'Jantung dan Subspesialisnya',
            'Paru dan Subspesialisnya',
            'Jiwa dan Subspesialisnya'
        ];

        $akibat_insiden_pasien = [
            'Kematian',
            'Cedera Irreversibel/Cidera Berat',
            'Cidera Reversibel/Cedera Sedang',
            'Cidera Ringan',
            'Tidak Ada Cidera'
        ];

        $tindakan_dilakukan_oleh = [
            'Dokter',
            'Perawat',
            'Petugas lainnya'
        ];

        $grading_dampak = [
            1 => 'Tidak cidera',
            2 => 'Cidera Ringan',
            3 => 'Cidera Sedang',
            4 => 'Cidera Luas/Beart',
            5 => 'Kematian'
        ];

        $grading_frequensi = [
            1=>'Sangat Jarang/rare (> 5 tahun/kali)',
            2=>'Jarang/unlikey (> 2-5 tahun/kali)',
            3=>'Mungkin/Posible (1-2 tahun/kali)',
            4=>'Sering/likely (beberapa kali/tahun)',
            5=>'Sangat sering/almost certain (tiap minggu/bulan)'
        ];

        return compact(
            'jenis_insiden',
            'orang_pertama_lapor',
            'insiden_terjadi',
            'insiden_menyangkut_pasien',
            'insiden_terjadi_pada_pasien',
            'akibat_insiden_pasien',
            'tindakan_dilakukan_oleh',
            'grading_dampak',
            'grading_frequensi',
        );

    }


    public function daftarikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $this->db->select('*');
            $this->db->from('mutu_daftar_ikp');
            $query = $this->db->get()->result();

            $data = [
                'content_view'      => 'indikatormutu/v_daftarikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'List Daftar IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list'              => $query,
                'active_sub_menu'   => 'daftarikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_daftarikp($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_dataikp='';
            if($id!=null){
                $get_dataikp       = $this->mmutu->get_editikp($id);
            }
            
            $data = [
                'content_view'      => 'indikatormutu/v_tambahdaftarikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'Tambah Daftar IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( $id !=null ) ? 'edit' : 'tambah',
                'paramid'           => ( $id !=null ) ? $id : '',
                'get_edit'          => $get_dataikp,
                'active_sub_menu'   => 'daftarikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function simpan_tambahdaftarikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU)) //lihat define di atas
        {
            $this->load->helper('form');
            $data['content_view'] = 'indikatormutu/v_daftarikp';
            $data['active_menu'] = 'Surveilans IKP';
            $data['data_edit']  = '';
            $data['title_page'] = 'Add IKP';
            $data['mode']       = 'add';
            $data['plugins']    = [];
            $data['script_js']  = ['js_indikatormutu'];
            $this->load->view('v_index', $data);
        }
    }

    public function simpan_tambahikp()
    {
        $post = $this->input->post();
        if(!empty($post['paramid'])){
            $id=$post['paramid'];
        }
        //Validasi judul
        $this->form_validation->set_rules('judulikp', 'Judul IKP', 'required',
                array('required' => 'Harap Isi %s.')
        );
        
        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>validation_errors());
            echo json_encode( $return );
        }else{
            
            $judulikp       = $this->security->sanitize_filename($post['judulikp']);

            $data_ikp = [
                'datecreated'    => indonesia_date(),
                'judulikp'       => $judulikp,
            ];
            if(empty($post['paramid'])){

                $query = $this->db->insert('mutu_daftar_ikp', $data_ikp);
        
            }else{
                unset($data_ikp['datecreated']);
                $query = $this->db->update('mutu_daftar_ikp',$data_ikp, array('id_daftarikp'=>$id));
            
            }  
            echo json_encode (['msg'=>'success', 'txt'=>'Data Berhasil di Simpan']);    

        }

    }

    public function remove_daftarikp()
    {
        //Data Post
        $post = $this->input->post();
        $id_ikp = $this->security->sanitize_filename($post['id']);
        //Get id from table
        $this->db->where('id_daftarikp', $id_ikp);
        $query=$this->db->delete('mutu_daftar_ikp');
        
        if($query){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
        }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
        
    }

    public function pengaturanikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_pengaturan = $this->msqlbasic->show_db($this->tb_mutu_pengaturanikp);
         
            $data = [
                'content_view'      => 'indikatormutu/v_pengaturanikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'Pengaturan IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list_pengaturan'   => $get_pengaturan,
                'active_sub_menu'   => 'pengaturanikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_aksesikp($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_pengaturanikp      = $this->msqlbasic->row_db($this->tb_mutu_pengaturanikp,['id_pengaturanikp'=>$id]);
            $get_unit_akre          = $this->msqlbasic->show_db($this->tb_akre_unit);
       
            $this->db->select('*');
            $this->db->from('mutu_daftar_ikp');    
            $query = $this->db->get()->result();

            $data = [
                'content_view'      => 'indikatormutu/v_tambahaksesikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'Tambah Akses IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( isset($id) ) ? 'edit':'',
                'allusers'          => $get_unit_akre,
                'get_pengaturanikp' => $get_pengaturanikp,
                'allindikator'      => $query,
                'active_sub_menu'   => 'pengaturanikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_ikp_item()
    {
        $post = $this->input->post();
        $indikator_id = (int) $this->security->sanitize_filename($post['indikator_id']);

        $get_indikator = $this->msqlbasic->row_db($this->tb_mutu_daftar_ikp,['id_daftarikp'=>$indikator_id]);
        
        $tr = '<tr>
            <td> <input type="hidden" name="idindikator[]" value="'.$get_indikator['id_daftarikp'].'"/>'.$get_indikator['judulikp'].'</td>
            <td><a data-toggle="tooltip" data-placement="bottom" title="Hapus IKP" class="remove-ikp-item btn btn-xs btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
        </tr>';
        
        echo json_encode(['hsl'=> $tr]);

    }

    public function simpan_pengaturan_ikp()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('alluser', 'User Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('unit', 'Unit', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('idindikator[]', 'IKP Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>strip_tags(validation_errors()));
            echo json_encode( $return );
        }else{
            $userid = (int) $this->security->sanitize_filename($post['alluser']);
            $unit   = $this->security->sanitize_filename($post['unit']);
            $idindikator = $post['idindikator']; //array

            $idindikator_array=[];
            foreach( $idindikator as $id ){
               $idindikator_array[] = (int) $this->security->sanitize_filename($id);
            }

            $data_pengaturan = [
                'unit' =>$unit,
                'userid_akses' =>$userid,
                'daftar_ikp_id' =>implode(',',$idindikator)
            ];

            if( isset($post['idpengaturanikp_hidden']) ){
                $idhidden      = $this->security->sanitize_filename($post['idpengaturanikp_hidden']);
                $update        = $this->msqlbasic->update_db($this->tb_mutu_pengaturanikp,['id_pengaturanikp'=>$idhidden],$data_pengaturan);
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('Cindikatormutu/pengaturanikp')] );
            }else{
                $insert = $this->msqlbasic->insert_data($this->tb_mutu_pengaturanikp,$data_pengaturan);
                $insert = true;
                if( $insert ){
                    echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('Cindikatormutu/pengaturanikp')] );
                }else{
                    echo json_encode( ['msg'=>'success','txt'=>'Data Gagal di Simpan'] );
                } 
            }
            
        }

    }

    public function remove_pengaturanikp()
    {
        $post = $this->input->post();
        $id   = (int) $this->security->sanitize_filename($post['id']);

        $get_pengaturan  = $this->msqlbasic->row_db($this->tb_mutu_pengaturanikp,['id_pengaturanikp'=>$id]);
        $idindikator = $get_pengaturan['daftar_ikp_id'];

        $delete_mutuhasil  = $this->msqlbasic->delete_db($this->tb_mutu_rekapikp,['daftarikp_id'=>$idindikator]);
        $delete  = $this->msqlbasic->delete_db($this->tb_mutu_pengaturanikp,['id_pengaturanikp'=>$id]);

        if( $delete > 0 ){
            
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    public function rekapikp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $post = $this->input->post();
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];
            $userid   = get_session_userid();

            $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

            $list_rekapikp= $this->mmutu->get_listrekapikprs($bulan,$tahun,$userid);
            
            $data = [
                'content_view'      => 'indikatormutu/v_rekapikp',
                'active_menu'       => 'Surveilans IKP',
                'title_page'        => 'Rekap IKP RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'rekapikprs'        => $this->ajax_tablerekapikprs($list_rekapikp,$bulan,$tahun,$userid),
                'active_sub_menu'   => 'rekapikp',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function get_pengaturan_aksesikp($userid)
    {
        $query = $this->msqlbasic->row_db($this->tb_mutu_pengaturanikp,['userid_akses'=>$userid]);

        return $query;
    }

    public function filter_rekapikp()
    {
        $post           = $this->input->post();
        $filterrekapikp = $post['filterrekapikp'];
        $array_data     = explode('-', $filterrekapikp);
        $bulan          = $array_data[0];
        $tahun          = $array_data[1];
        
        $list_daftarikp = $this->mmutu->get_filterrekapikp($bulan,$tahun);

        $list_table = $this->ajax_tablefilterrekapikp($list_daftarikp,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }

    public function ajax_tablefilterrekapikp($rekapikprs,$bulan,$tahun)
    {
        ob_start();
                
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        $jumlah_bulan = ql_bulans();

        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxrekapikprs.php';

        return ob_get_clean();

    }

    public function ajax_tablerekapikprs($rekapikprs,$bulan,$tahun,$userid)
    {
        ob_start();
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        $jumlah_bulan = ql_bulans();

        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxrekapikprs.php';

        return ob_get_clean();

    }

    public function view_detail_ikprs()
    {

        $query = $this->mmutu->get_list_detail_ikprs();

        $body = $this->popup_list_detail_ikprs($query);

        $data = [
            'title'=>'Detail Pilihan IKP RS',
            'body' => $body,
        ];

        echo json_encode($data);
    }

    public function popup_list_detail_ikprs($list_detail_ikprs)
    {
        ob_start();
        include FCPATH. 'application/views/indikatormutu/v_popup-list-detail-ikprs.php';
        return ob_get_clean();
    }


    //SURVEILENS PPI
    public function daftarppi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            $list_daftarppi = $this->mmutu->get_listdaftarppirs($bulan,$tahun);

            $data = [
                'content_view'      => 'indikatormutu/v_daftarppi',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'List Daftar PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list'              => $list_daftarppi,
                'active_sub_menu'   => 'daftarppi',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_ppi($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {

            $get_datappi      = $this->mmutu->get_editppi($id);
            $all_pendataan_ppi = $this->mmutu->get_editpendataanppi($id);

            $data = [
                'content_view'      => 'indikatormutu/v_tambahppi',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'Tambah Daftar PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( $id !=null ) ? 'edit' : 'tambah',
                'paramid'           => ( $id !=null ) ? $id : '',
                'get_edit'          => $get_datappi,
                'all_pendataan'     => $all_pendataan_ppi,
                'active_sub_menu'   => 'daftarppi',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambahkan_pendataanppi()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('tipe_variable', 'Tipe Variable', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        $this->form_validation->set_rules('ppi_pendataan', 'PPI Pendataan', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $tipe_variable      = $this->security->sanitize_filename($post['tipe_variable']);
            $ppi_pendataan      = $this->security->sanitize_filename($post['ppi_pendataan']);

            $data = [
                'tipe_variable'    => $tipe_variable,
                'ppi_pendataan'    => $ppi_pendataan,
            ];
            
            echo json_encode($data);
        }
    }

    public function simpan_tambahppi()
    {
        $post = $this->input->post();

        if( isset( $post['paramid'] ) ){
            $this->form_validation->set_rules('paramid', 'Pastikan Data Benar yang diedit', 'required',
                array('required' => 'Harap %s.')
            ); 
        }

        $this->form_validation->set_rules('judul_ppi', 'Judul PPI', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('definisi_operasional', 'Definisi Operasional', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('cek_datappi', 'Numerator & Denumerator', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $judul_ppi              = $this->security->sanitize_filename($post['judul_ppi']);
            $definisi_operasional   = $this->security->sanitize_filename($post['definisi_operasional']);
            $cek_datappi            = $this->security->sanitize_filename($post['cek_datappi']);
            $target                 = $this->security->sanitize_filename($post['target']);
            $satuan_ppi             = $this->security->sanitize_filename($post['satuan_ppi']);

            $data_ppi = [
                'datecreated'           => indonesia_date(),
                'judul_ppi'             => $judul_ppi,
                'tanggal'               => date('Y-m-d'),
                'definisi_operasional'  => $definisi_operasional,
                'target'                => $target,
                'satuan'                => $satuan_ppi,
            ]; 

            if( isset($post['paramid']) ){
                
                unset( $data_ppi['datecreated'] );
                unset( $data_ppi['tanggal'] );

                $update_ppi = $this->msqlbasic->update_db($this->tb_mutu_daftarppi,['id_daftarppi'=>$post['paramid']],$data_ppi);
                $insert_ppi = $post['paramid'];

            }else{
                $insert_ppi = $this->msqlbasic->insert_data($this->tb_mutu_daftarppi, $data_ppi);
            }

            # array
            $tipe_variable          = $post['tipe_variable'];
            $ppi_pendataan          = $post['ppi_pendataan'];       
            
            if( isset($post['paramid']) ){
                $delete_pendataan       = $this->msqlbasic->delete_db($this->tb_mutu_pendataan_ppi,['daftarppi_id'=>$insert_ppi]);
            }
            
            $found = false;
            foreach( $tipe_variable as $key => $tipe ){
                $data_pendataan = [
                    'daftarppi_id'  =>  $insert_ppi,
                    'tipe_variabel' =>  $tipe,
                    'ppirs'         =>  $ppi_pendataan[$key],
                ];

                $insert_pendataan = $this->msqlbasic->insert_data($this->tb_mutu_pendataan_ppi, $data_pendataan);
                
                if( $insert_pendataan > 0 ){
                    $found = true;
                }

            }

            if( $found ){
                $return = ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('Cindikatormutu/daftarppi')];
            }else{
                $return = ['msg'=>'danger','txt'=>'Data Gagal di Simpan'];
            }

            echo json_encode( $return );

        }

    }

    public function view_ppi()
    {
        $post           = $this->input->post();
        $idppi          = $this->security->sanitize_filename( $post['id'] );
        
        $get_view       = $this->mmutu->get_editppi($idppi);

        $get_allpendataan = $this->mmutu->get_editpendataanppi($idppi);

        $id_daftarppi           = $get_view->id_daftarppi; 
        $judul_ppi              = $get_view->judul_ppi; 
        $definisi_operasional   = $get_view->definisi_operasional;
        $target                 = $get_view->target; 
        $satuan                 = $get_view->satuan; 

        $table = '<table class="table table-striped table-hover">
            <tr><th width="30%">Judul PPI</th><td>'.$judul_ppi.'</td></tr>
            <tr><th width="30%">Definisi Operasional</th><td>'.$definisi_operasional.'</td></tr>
            <tr><th width="30%">Target</th><td>'.$target.'</td></tr>
            <tr><th width="30%">Satuan</th><td>'.$satuan.'</td></tr>
            <tr>
                <th width="30%">PPI Pendataan</th>
                <td>
                    <table class="table table-striped table-hover"> 
                        <thead>
                            <tr class="header-table-ql">
                                <th>Variabel</th>
                                <th>PPI</th>
                            </tr>
                        </thead>';

                        foreach($get_allpendataan as $row):
                            $table .= '<tr>
                                    <td>'.$row->variabel.'</td>
                                    <td>'.$row->ppirs.'</td>
                                </tr>';
                        endforeach;
                        
                    '</table>
                </td>
            </tr>
        </table> ';
        
        echo json_encode(['hsl'=>$table]);
    }

    public function simpanhasilpendataanppi()
    {
        $post = $this->input->post();
        
        $this->form_validation->set_rules('hasil', 'Hasil', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>strip_tags(validation_errors()));
            echo json_encode( $return );
        }else{
            $idpendataan   = $this->security->sanitize_filename($post['id']);
            $hasil         = $this->security->sanitize_filename($post['hasil']);
            $tanggal       = $this->security->sanitize_filename($post['tanggal']);
            $bulan         = $this->security->sanitize_filename($post['bulan']);
            $tahun         = $this->security->sanitize_filename($post['tahun']);

            $userid        = get_session_userid();

            $get_pendataan = $this->msqlbasic->row_db($this->tb_mutu_pendataan_ppi,['id_pendataan'=>$idpendataan]);

            $data_hasil    = [
                'tanggal'      => $tanggal,
                'bulan'        => $bulan,
                'tahun'        => $tahun,
                'datecreated'   => indonesia_date(),
                'pendataan_id' => $idpendataan,
                'daftarppi_id' => $get_pendataan['daftarppi_id'],
                'hasil'        => $hasil,
                'author'       => $userid,
            ]; 

            $cek_data_insert = $this->msqlbasic->row_db($this->tb_mutu_rekap_ppi,['pendataan_id'=>$idpendataan,'author'=>$userid,'tanggal'=>$tanggal,'bulan'=>$bulan,'tahun'=>$tahun]);

            if( !empty($cek_data_insert) ){
                $update        = $this->msqlbasic->update_db($this->tb_mutu_rekap_ppi,['id_rekap_ppi'=>$cek_data_insert['id_rekap_ppi']],['hasil'=>$hasil]);
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update'] );
            }else{
                $insert_hasil  = $this->msqlbasic->insert_data($this->tb_mutu_rekap_ppi,$data_hasil);
                if( $insert_hasil ){
                    echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Input'] );
                }else{
                    echo json_encode( ['msg'=>'success','txt'=>'Data Gagal di Input'] );
                } 
            }

            // $update        = $this->msqlbasic->update_db($this->tb_mutu_pendataan_indikator,['id_pendataan'=>$idpendataan],['hasil'=>$hasil]);
            
        }

    }

    public function remove_ppi()
    {
        $post = $this->input->post();
        $idppi  = $this->security->sanitize_filename($post['id']);

        $delete_ppi  = $this->msqlbasic->delete_db($this->tb_mutu_daftarppi,['id_daftarppi'=>$idppi]);

        if( $delete_ppi > 0 ){
            $delete_pendataan  = $this->msqlbasic->delete_db($this->tb_mutu_pendataan_ppi,['daftarppi_id'=>$idppi]);

            $delete_rekap      = $this->msqlbasic->delete_db($this->tb_mutu_rekap_ppi,['daftarppi_id'=>$idppi]);

            $this->mmutu->get_pengaturan_ppi_row($idppi);

            if( $delete_pendataan ){
                echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
            }else{
                echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
            }
        }

    }

    public function pengaturanppi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            $get_pengaturan = $this->msqlbasic->show_db($this->tb_mutu_pengaturanppi);

            $data = [
                'content_view'      => 'indikatormutu/v_pengaturanppi',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'Pengaturan Unit PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'list_pengaturan'   => $get_pengaturan,
                'active_sub_menu'   => 'pengaturanppi',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_aksesppi($id=null)
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];

            $get_pengaturan         = $this->msqlbasic->row_db($this->tb_mutu_pengaturanppi,['id_pengaturan'=>$id]);
            $get_unit_akre          = $this->msqlbasic->show_db($this->tb_akre_unit);

            $data = [
                'content_view'      => 'indikatormutu/v_tambahaksesppi',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'Tambah Akses PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => ( isset($id) ) ? 'edit':'',
                // 'allusers'          => get_allusers(),
                'allusers'          => $get_unit_akre,
                'get_pengaturan'    => $get_pengaturan,
                'allindikator'      => $this->mmutu->get_listdaftarppirs($bulan,$tahun),
                'active_sub_menu'   => 'pengaturanppi',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function tambah_ppi_item()
    {
        $post = $this->input->post();
        $daftarppi_id = (int) $this->security->sanitize_filename($post['daftarppi_id']);

        $get_ppi = $this->msqlbasic->row_db($this->tb_mutu_daftarppi,['id_daftarppi'=>$daftarppi_id]);
        
        $tr = '<tr>
            <td> <input type="hidden" name="idindikator[]" value="'.$get_ppi['id_daftarppi'].'"/>'.$get_ppi['judul_ppi'].'</td>
            <td><a data-toggle="tooltip" data-placement="bottom" title="Hapus PPI" class="remove-ppi-item btn btn-xs btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
        </tr>';
        
        echo json_encode(['hsl'=> $tr]);

    }

    public function simpan_pengaturan_ppi()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('alluser', 'User Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('unit', 'Unit', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('idindikator[]', 'IKP Akses', 'required',
                array('required' => 'Harap Isi %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>strip_tags(validation_errors()));
            echo json_encode( $return );
        }else{
            $userid = (int) $this->security->sanitize_filename($post['alluser']);
            $unit   = $this->security->sanitize_filename($post['unit']);
            $idindikator = $post['idindikator']; //array

            $idindikator_array=[];
            foreach( $idindikator as $id ){
               $idindikator_array[] = (int) $this->security->sanitize_filename($id);
            }

            $data_pengaturan = [
                'unit' =>$unit,
                'userid_akses' =>$userid,
                'daftarppi_id' =>implode(',',$idindikator)
            ];

            if( isset($post['idpengaturanppi_hidden']) ){
                $idhidden      = $this->security->sanitize_filename($post['idpengaturanppi_hidden']);
                $update        = $this->msqlbasic->update_db($this->tb_mutu_pengaturanppi,['id_pengaturan'=>$idhidden],$data_pengaturan);
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update','url'=>base_url('Cindikatormutu/pengaturanppi')] );
            }else{
                $insert = $this->msqlbasic->insert_data($this->tb_mutu_pengaturanppi,$data_pengaturan);
                $insert = true;
                if( $insert ){
                    echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Simpan','url'=>base_url('Cindikatormutu/pengaturanppi')] );
                }else{
                    echo json_encode( ['msg'=>'success','txt'=>'Data Gagal di Simpan'] );
                } 
            }
            
        }

    }

    public function remove_pengaturanppi()
    {
        $post = $this->input->post();
        $id   = (int) $this->security->sanitize_filename($post['id']);

        $get_pengaturan  = $this->msqlbasic->row_db($this->tb_mutu_pengaturanppi,['id_pengaturan'=>$id]);
        $idindikator = $get_pengaturan['daftarppi_id'];

        $delete_rekap           = $this->msqlbasic->delete_db($this->tb_mutu_rekap_ppi,['daftarppi_id'=>$idindikator]);
  
        $delete  = $this->msqlbasic->delete_db($this->tb_mutu_pengaturanppi,['id_pengaturan'=>$id]);

        if( $delete > 0 ){
            echo json_encode(['msg'=>'success','txt'=>'Data Berhasil dihapus']);
         }else{
            echo json_encode(['msg'=>'danger','txt'=>'Data Gagal dihapus']);
        }
    }

    public function ppirs()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            
            $post = $this->input->post();
            $get_bt = $this->bulan_tahun();
            $tanggal = $get_bt['tanggal'];
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];
            $userid   = get_session_userid();
            
            $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

            $list_daftarindikator = $this->mmutu->get_listppirs($tanggal,$bulan,$tahun,$userid);

            $data = [
                'content_view'      => 'indikatormutu/v_ppirs',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'List PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'daftarppirs'       => $this->ajax_tablelistppirs($list_daftarindikator,$tanggal,$bulan,$tahun,$userid),
                'active_sub_menu'   => 'ppirs',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function ajax_tablelistppirs($daftarppirs,$tanggal,$bulan,$tahun,$userid)
    {
        ob_start();
        
        $kalender = CAL_GREGORIAN;
        $hari = cal_days_in_month($kalender, $bulan, $tahun);

        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxppirs.php';

        return ob_get_clean();

    }

    public function input_hasil_pendataanppi()
    {
        $post           = $this->input->post();
        $id_daftarppi   = $post['iddaftarppi'];
        $tanggal        = $post['tanggal'];
        $bulan          = $post['bulan'];
        $tahun          = $post['tahun'];

        $get_pendataan  = $this->mmutu->get_editpendataanppi($id_daftarppi,$tanggal,$bulan,$tahun);

        $body = '<div class="err-msg-pendataan"></div>
        <table class="table table-striped table-hover ql-dt">
                <thead>
                    <tr class="header-table-ql">
                        <th>No</th>
                        <th>Variable</th>
                        <th width="50%">Indikator</th>
                        <th >Hasil</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>';
                $no = 1;
                foreach( $get_pendataan as $row ):
                    $get_hasil = $this->mmutu->get_hasil_byidpendataanppi($row->id_pendataan,get_session_userid(),$tanggal,$bulan,$tahun);

                    $body .= '<tr>
                        <td>'.$no++.'</td>
                        <td>'.$row->variabel.'</td>
                        <td>'.$row->ppirs.'</td>
                        <td>
                            <input required style="opacity: 0;" type="number" min="0" value="'.(empty( $get_hasil) ? '' : $get_hasil->hasil ).'" class="form-control hasil" name="hasil" placeholder="Hasil ..."/ required>
                        </td>
                        <td class="actions">
                            <div class="wrap-button">
                                <a class="editpendataanppi" data-tgl="'.$tanggal.'" data-bln="'.$bulan.'" data-th="'.$tahun.'" data-id="'.$row->id_pendataan.'" href="javascript:void(0)"><i class="fa fa-pencil"></i></a>
                            </div>
                        </td>
                    </tr>';
                endforeach;
        $body .='</tbody>
            </table>';

        $data = [
            'title' => 'Pendataan PPI - '.ql_namabulan((int) $bulan).' '.$tahun,
            'body'  => $body,
            'id_daftarppi' => $id_daftarppi,
            'tanggal' => $tanggal,
            'bulan' => $bulan
        ];

        echo json_encode($data);

    }

    public function filterppi()
    {
        $post           = $this->input->post();
        $filterppi     = $post['filterppi'];
        $array_data     = explode('-', $filterppi);
        $bulan          = $array_data[0];
        $tahun          = $array_data[1];
        
        $list_daftarppi = $this->mmutu->get_filterppi($bulan,$tahun);

        $list_table = $this->ajax_tablefilterppi($list_daftarppi,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }

    public function ajax_tablefilterppi($daftarppirs,$bulan,$tahun)
    {
        ob_start();
                
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxppirs.php';

        return ob_get_clean();

    }

    public function rekapppi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU))
        {
            # by bulan,tahun
            $post = $this->input->post();
            $get_bt = $this->bulan_tahun();
            $bulan = $get_bt['bulan'];
            $tahun = $get_bt['tahun'];
            $userid   = get_session_userid();

            $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

            $list_rekapppi= $this->mmutu->get_listrekapppirs($bulan,$tahun,$userid);
            
            $data = [
                'content_view'      => 'indikatormutu/v_rekapppi',
                'active_menu'       => 'Surveilans PPI',
                'title_page'        => 'Rekap PPI RS',
                'active_menu_level' => 'indikatormutu',
                'mode'              => '',
                'rekapppirs'        => $this->ajax_tablerekapppirs($list_rekapppi,$bulan,$tahun,$userid),
                'active_sub_menu'   => 'rekapppi',
                'script_js'         => ['js_indikatormutu'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function ajax_tablerekapppirs($rekapppirs,$bulan,$tahun,$userid)
    {
        ob_start();
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];
        
        $jumlah_bulan = ql_bulans();

        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxrekapppirs.php';

        return ob_get_clean();

    }

    public function view_detail_ppirs()
    {
        $post = $this->input->post();
        $get_bt = $this->bulan_tahun();
        $bulan = $get_bt['bulan'];
        $tahun = $get_bt['tahun'];
        $userid   = get_session_userid();

        $query = $this->mmutu->get_list_detail_ppirs($bulan,$tahun);

        $body = $this->popup_list_detail_ppirs($query,$bulan,$tahun);

        $data = [
            'title'=>'Detail Pilihan PPI RS',
            'body' => $body,
        ];

        echo json_encode($data);
    }

    public function popup_list_detail_ppirs($list_detail_ppirs,$bulan,$tahun)
    {
        ob_start();
        include FCPATH. 'application/views/indikatormutu/v_popup-list-detail-ppirs.php';
        return ob_get_clean();
    }

    public function filter_rekapppi()
    {
        $post           = $this->input->post();
        $filterrekapppi = $post['filterrekapppi'];
        $array_data     = explode('-', $filterrekapppi);
        $bulan          = $array_data[0];
        $tahun          = $array_data[1];
        
        $list_rekapppirs = $this->mmutu->get_filterrekapppi($bulan,$tahun);

        $list_table = $this->ajax_tablefilterrekapppi($list_rekapppirs,$bulan,$tahun);

        echo json_encode( ['hsl'=>$list_table] );
    }

    public function ajax_tablefilterrekapppi($rekapppirs,$bulan,$tahun)
    {
        ob_start();

        $jumlah_bulan = ql_bulans();
                
        $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

        $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
        
        $obj_pengaturan = $this->get_pengaturan_aksesikp(get_session_userid());
       
        $indikator_id_pengaturan = empty( $obj_pengaturan ) ? '' : explode( ',',$obj_pengaturan['daftar_ikp_id'] );

        $userid = empty( $obj_pengaturan ) ? ( is_superuser() ) ? '' : 'without_superuser' : $obj_pengaturan['userid_akses'];

        include FCPATH. 'application/views/indikatormutu/v_list-table-ajaxrekapppirs.php';

        return ob_get_clean();

    }

    /**
     * mr.ganteng
     */
    public function getdata_rm_pelaporanikp(){
        $post   = $this->input->post();
        $all    = $this->mmutu->get_all_norm( $post['searchTerm'] );
        echo json_encode($all);
    }

    // public function get_datapasien_byperson(){
    //     $post       = $this->input->post();
    //     $get_person = $this->mmutu->get_person_byid($post['val']);
    //     $tgl_lahir  = $get_person['tanggallahir']; //Y-m-d
    //     $umur_sekarang = ql_umursekarang($tgl_lahir);
        
    //     $get_person['umursekarang'] = $umur_sekarang;

    //     echo json_encode( $get_person );

    // }

    public function matrik_gradian_pelaporan($frequensi=0,$dampak=0)
    {

        switch ($frequensi) {
            case '5':
                $data = [
                    [
                        'nilai'=>1,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>2,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>3,
                        'warna'=>'yellow',
                        'ket'=>'Tinggi',
                    ],
                    [
                        'nilai'=>4,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ],
                    [
                        'nilai'=>5,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ]

                ];
                break;
            case '4':
                $data = [
                    [
                        'nilai'=>1,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>2,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>3,
                        'warna'=>'yellow',
                        'ket'=>'Tinggi',
                    ],
                    [
                        'nilai'=>4,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ],
                    [
                        'nilai'=>5,
                        'warna'=>'red',     
                        'ket'=>'Ekstrim',
                    ]

                ];

                break;
            case '3':
                $data = [
                    [
                        'nilai'=>1,
                        'warna'=>'blue',
                        'ket'=>'Rendah',
                    ],
                    [
                        'nilai'=>2,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>3,
                        'warna'=>'yellow',
                        'ket'=>'Tinggi',
                    ],
                    [
                        'nilai'=>4,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ],
                    [
                        'nilai'=>5,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ]

                ];
                break;
            case '2':
                $data = [
                    [
                        'nilai'=>1,
                        'warna'=>'blue',
                        'ket'=>'Rendah',
                    ],
                    [
                        'nilai'=>2,
                        'warna'=>'blue',
                        'ket'=>'Rendah',
                    ],
                    [
                        'nilai'=>3,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>4,
                        'warna'=>'yellow',
                        'ket'=>'Tinggi',
                    ],
                    [
                        'nilai'=>5,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ]

                ];
                break;
            case '1':
                $data = [
                    [
                        'nilai'=>1,
                        'warna'=>'blue',
                        'ket'=>'Rendah',
                    ],
                    [
                        'nilai'=>2,
                        'warna'=>'blue',
                        'ket'=>'Rendah',
                    ],
                    [
                        'nilai'=>3,
                        'warna'=>'green',
                        'ket'=>'Moderat',
                    ],
                    [
                        'nilai'=>4,
                        'warna'=>'yellow',
                        'ket'=>'Tinggi',
                    ],
                    [
                        'nilai'=>5,
                        'warna'=>'red',
                        'ket'=>'Ekstrim',
                    ]

                ];
                break;
            default :
                $data = [];
                break;
        }

        $data_row = [];
        foreach( $data as $row ){
            if( $row['nilai'] == $dampak ){
                $data_row = $row;
            }
        }

        return $data_row;
    
    }

    public function ajax_perhitungan_gradian_matrik()
    {
        $post = $this->input->post();

        $dampak    = $post['dampak'];
        $frequensi = $post['frequensi'];

        $array_hasil_perhitungan = $this->matrik_gradian_pelaporan($frequensi,$dampak);

        $data = [
            'hasil' => $dampak*$frequensi,
            'warna' => empty($array_hasil_perhitungan) ? '' : $array_hasil_perhitungan['warna'],
            'ket'   => empty($array_hasil_perhitungan) ? '' : $array_hasil_perhitungan['ket'],
        ];

        echo json_encode( $data );
    }

    public function simpan_pelaporan_ikp()
    {
        $post = $this->input->post();

        if(!empty($post['idpelaporan'])){
            $id=$post['idpelaporan'];
        }

        $user_id_login  = get_session_userid();
        $datecreated    = indonesia_date();

        // $id_person      = $post['norm'];

        // $norm = $this->db->select('norm')
        //                 ->from('person_pasien')
        //                 ->where('idperson',$post['norm'])
        //                 ->get()->row();

        // $norm = $this->mmutu->get_person_bynorm($norm->norm);

        $post['norm']        = $post['norm'];

        $post['tglmasuk_rs'] = format_date($post['tglmasuk_rs'],'Y-m-d');
        $post['tglinsiden']  = format_date($post['tglinsiden'],'Y-m-d');

        $post['datecreated'] = $datecreated;
        $post['author']      = $user_id_login;

        if( empty($post['idpelaporan']) ){
            $insert = $this->db->insert('mutu_pelaporan_ikp', $post);
            if( $insert ){
                $insert_id = $this->db->insert_id();
                $encode_id = base64_encode($insert_id);
                $return = ['msg'=>'success','txt'=>'Berhasil di Simpan','url'=>base_url('cindikatormutu/tambah_pelaporanikp?ids='.$encode_id)];
                echo json_encode($return);
            }else{
                $return = ['msg'=>'danger','txt'=>'Maaf Data gagal di Simpan'];
                echo json_encode($return);
            }
        }else{

            $datas = array (
                'norm' => $post['norm'],
                'nama' => $post['nama'],
                'umur' => $post['umur'],
                'jk' => $post['jk'],
                'penanggungjawab_pasien' => $post['penanggungjawab_pasien'],
                'tglmasuk_rs' => $post['tglmasuk_rs'],
                'jammasuk_rs' => $post['jammasuk_rs'],
                'tglinsiden' => $post['tglinsiden'],
                'jaminsiden' => $post['jaminsiden'],
                'insiden' => $post['insiden'],
                'kronologi' => $post['kronologi'],
                'jenis_insiden' => $post['jenis_insiden'],
                'orang_pertama_lapor' => $post['orang_pertama_lapor'],
                'insiden_terjadi' => $post['insiden_terjadi'],
                // 'insiden_terjadi_lainlain' => $post['insiden_terjadi_lainlain'],
                'insiden_menyangkut_pasien' => $post['insiden_menyangkut_pasien'],
                // 'form_insiden_menyangkut_pasien' => $post['form_insiden_menyangkut_pasien'],
                'tempat_insiden' => $post['tempat_insiden'],
                'insiden_terjadi_pada_pasien' => $post['insiden_terjadi_pada_pasien'],
                'unit_terkait' => $post['unit_terkait'],
                'akibat_insiden_pasien' => $post['akibat_insiden_pasien'],
                'tindakan_kejadian' => $post['tindakan_kejadian'],
                'tindakan_dilakukan_oleh' => $post['tindakan_dilakukan_oleh'],
                'apakah_kejadian_pernah_terjadi' => $post['apakah_kejadian_pernah_terjadi'],
                // 'form_kejadian_pasien' => $post['form_kejadian_pasien'],
                'grading_dampak' => $post['grading_dampak'],
                'grading_frequensi' => $post['grading_frequensi'],
                'hasil_grading' => $post['hasil_grading'],
                'kategori_grading' => $post['kategori_grading'],
                // 'datecreated' => $post['datecreated'],
                // 'author' => $post['author'],
              );

              if (!empty($post['insiden_terjadi_lainlain'])) {
                $datas['insiden_terjadi_lainlain'] = $post['insiden_terjadi_lainlain'];
              }

              if (!empty($post['form_insiden_menyangkut_pasien'])) {
                $datas['form_insiden_menyangkut_pasien'] = $post['form_insiden_menyangkut_pasien'];
              }

              if (!empty($post['form_kejadian_pasien'])) {
                $datas['form_kejadian_pasien'] = $post['form_kejadian_pasien'];
              }

            $update = $this->msqlbasic->update_db($this->tb_mutu_pelaporan_ikp,['idpelaporanikp'=>base64_decode($post['idpelaporan'])],$datas);
            
            $row_pelaporan = $this->db->select('*')
                ->from($this->tb_mutu_pelaporan_ikp)
                ->where('idpelaporanikp',base64_decode($post['idpelaporan']))
                ->get()->row_array();

            $dataikp = [
                // 'idperson' => $id_person,
                'norm'     => $row_pelaporan['norm'],
                'nama'     => $row_pelaporan['nama'],
                'tglmasuk_rs' => format_date( $row_pelaporan['tglmasuk_rs'], 'd-m-Y'), 
                'tglinsiden' => format_date($row_pelaporan['tglinsiden'],'d-m-Y') 
            ];
    
            $return = ['msg'=>'success','txt'=>'Berhasil di Simpan','code'=>'update','dataikp'=>json_encode($dataikp)];

            echo json_encode($return);
        }

    }

    public function list_datapelaporan_ikp()
    {

        $response = $this->mmutu->get_dt_pelaporan_ikp(true);

        $no = 1;
        $data = [];
        foreach($response->result() as $res){
            $encode_id = base64_encode($res->idpelaporanikp);

            $row = [
                $no++,
                $res->nama,
                $res->norm,
                $res->penanggungjawab_pasien,
                '<a href="javascript:void(0);" data-id="'.$res->idpelaporanikp.'" class="edit-pelaporan btn btn-primary btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="javascript:void(0);" data-id="'.$res->idpelaporanikp.'" class="hapus-pelaporan btn btn-danger btn-xs">
                    <i class="fa fa-trash"></i>
                </a>
                <a href="'.base_url("cindikatormutu/printlaporan/$encode_id").'" data-id="'.$res->idpelaporanikp.'" class="print-pelaporan btn btn-success btn-xs">
                    <i class="fa fa-file-pdf-o"></i>
                </a>'
            ];

            $data [] = $row;
            
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => intval( $this->mmutu->count_all() ),
            "recordsFiltered" => intval( $this->mmutu->count_filtered()),
            "data" => $data,
        );

        echo json_encode($output);
    }

    public function edit_pelaporan_ajax()
    {
        $post = $this->input->post();

        $row_pelaporan = $this->db->select('*')
                                ->from($this->tb_mutu_pelaporan_ikp)
                                ->where('idpelaporanikp',$post['idp_ikp'])
                                ->get()->row_array();

        // $query = $this->mmutu->get_person_bynorm($row_pelaporan['norm']);

        // $row_pelaporan['idperson'] = $query->idperson;

        $data = [
            // 'idperson' => $query->idperson,
            'norm'     => $row_pelaporan['norm'],
            'nama'     => $row_pelaporan['nama'],
            'tglmasuk_rs' => format_date( $row_pelaporan['tglmasuk_rs'], 'd-m-Y'), 
            'tglinsiden' => format_date($row_pelaporan['tglinsiden'],'d-m-Y') 
        ];

        $id_encrip = base64_encode($post['idp_ikp']);
        
        $return = [
            'url'=>base_url('cindikatormutu/tambah_pelaporanikp?ids='.$id_encrip),
            'dataikp'=> json_encode($data),
        ];

        echo json_encode( $return );

    }
    
    public function hapus_pelaporan_ajax()
    {
        $post = $this->input->post();
        $idp_ikp = $post['idp_ikp'];

        $delete = $this->msqlbasic->delete_db($this->tb_mutu_pelaporan_ikp,['idpelaporanikp'=>$idp_ikp]);
        
        if( $delete ){
            $return = ['msg'=>'success','txt'=>'Berhasil di Hapus'];
        }else{
            $return = ['msg'=>'danger','txt'=>'Maaf Data gagal di hapus'];
        }
        
        echo json_encode($return);
    }

    public function printlaporan()
    {
        $idpelaporan  = $this->uri->segment('3');
        
        $option_kejadian    = $this->data_formsurvey_insiden();
        $option_data        = $this->data_formsurvey_pasien();
        
        $data = [
            'id'=>$idpelaporan,
            'option_kejadian' => $option_kejadian,
            'option_data' => $option_data,
        ];

        $this->pdf->filename = "coba.pdf";
        $this->pdf->load_view_ql('indikatormutu/v_download_pelaporan',$data);
    }

    public function form_investigasi_sederhana()
    {
        $post           = $this->input->post();

        $idpelaporanikp = $this->input->post('id');

        $encode_id = base64_encode($idpelaporanikp);

        $row_investigasi = $this->db->query(" SELECT * FROM `mutu_investigasi_sederhana`
                                       WHERE idpelaporanikp=".$idpelaporanikp)->row_array();

        if ($row_investigasi == null) {
            $idinvestigasi = (empty($row_investigasi)) ? '' : $row_investigasi->idinvestigasi;
            $penyebab_langsung = (empty($row_investigasi)) ? '' : $row_investigasi->penyebab_langsung;
            $penyebab_latarbelakang = (empty($row_investigasi)) ? '' : $row_investigasi->penyebab_latarbelakang;
            $rekomendasi = (empty($row_investigasi)) ? '' : $row_investigasi->rekomendasi;
            $pj_rekomendasi = (empty($row_investigasi)) ? '' : $row_investigasi->pj_rekomendasi;
            $tindakan_dilakukan = (empty($row_investigasi)) ? '' : $row_investigasi->tindakan_dilakukan;
            $pj_tindakan = (empty($row_investigasi)) ? '' : $row_investigasi->pj_tindakan;
            $nama_kepalaruang = (empty($row_investigasi)) ? '' : $row_investigasi->nama_kepalaruang;
            $tglmulai_investigasi = (empty($row_investigasi)) ? '' : $row_investigasi->tglmulai_investigasi;
            $tglselesai_investigasi = (empty($row_investigasi)) ? '' : $row_investigasi->tglselesai_investigasi;
            $investigasi_lengkap = (empty($row_investigasi)) ? '' : $row_investigasi->investigasi_lengkap;
            $tglinvestigasi_lengkap = (empty($row_investigasi)) ? '' : $row_investigasi->tglinvestigasi_lengkap;
            $investigasi_lanjut = (empty($row_investigasi)) ? '' : $row_investigasi->investigasi_lanjut;
            $investigasi_grading = (empty($row_investigasi)) ? '' : $row_investigasi->investigasi_grading;
            $kronologi_kejadian = (empty($row_investigasi)) ? '' : $row_investigasi->kronologi_kejadian;
        }else {
            $idinvestigasi = $row_investigasi['idinvestigasi'];
            $penyebab_langsung = $row_investigasi['penyebab_langsung'];
            $penyebab_latarbelakang = $row_investigasi['penyebab_latarbelakang'];
            $rekomendasi = $row_investigasi['rekomendasi'];
            $pj_rekomendasi = $row_investigasi['pj_rekomendasi'];
            $tindakan_dilakukan = $row_investigasi['tindakan_dilakukan'];
            $pj_tindakan = $row_investigasi['pj_tindakan'];
            $nama_kepalaruang = $row_investigasi['nama_kepalaruang'];
            $tglmulai_investigasi = $row_investigasi['tglmulai_investigasi'];
            $tglselesai_investigasi = $row_investigasi['tglselesai_investigasi'];
            $investigasi_lengkap = $row_investigasi['investigasi_lengkap'];
            $tglinvestigasi_lengkap = $row_investigasi['tglinvestigasi_lengkap'];
            $investigasi_lanjut = $row_investigasi['investigasi_lanjut'];
            $investigasi_grading = $row_investigasi['investigasi_grading'];
            $kronologi_kejadian = $row_investigasi['kronologi_kejadian'];
        }
        
        $body = '<form id="frm-investigasi-pelaporan">
                 <table style="width:100%" class="table">
                 <tbody>
                    <input type="hidden" name="idpelaporanikp" value ="'.$idpelaporanikp.'">
                    <input type="hidden" name="idinvestigasi" value="">
                    <tr>
                        <td colspan="2">
                            <label>PENYEBAB LANGSUNG INSIDEN:</label><br>
                            <textarea class="form-control textarea" name="penyebab_langsung" cols="75" rows="2" placeholder="Tuliskan ..." required="">'.$penyebab_langsung.'</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>PENYEBAB YANG MELATARBELAKANGI / AKAR MASALAH INSIDEN::</label><br>
                            <textarea class="form-control textarea" name="penyebab_latarbelakang" cols="75" rows="2" placeholder="Tuliskan ..." required="">'.$penyebab_latarbelakang.'</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>REKOMENDASI:</label><br>
                            <textarea class="form-control textarea" name="rekomendasi" cols="75" rows="2" placeholder="Tuliskan ..." required="">'.$rekomendasi.'</textarea><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Penanggung Jawab :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Penanggung Jawab Rekomendasi" autocomplete="off" class="form-control datepicker-ql" name="pj_rekomendasi" value="'.$pj_rekomendasi.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>TINDAKAN YANG AKAN DILAKUKAN:</label><br>
                            <textarea class="form-control textarea" name="tindakan_dilakukan" cols="75" rows="2" placeholder="Tuliskan ..." required="">'.$tindakan_dilakukan.'</textarea><br>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Penanggung Jawab :</label>
                        </td>
                        <td>
                            <input type="text" placeholder="Penanggung Jawab Tindakan" autocomplete="off" class="form-control datepicker-ql" name="pj_tindakan" value="'.$pj_tindakan.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> KEPALA RUANG / KEPALA INSTALASI</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nama </label>
                        </td>
                        <td>
                            <input type="text" placeholder="Nama" autocomplete="off" class="form-control datepicker-ql" name="nama_kepalaruang" value="'.$nama_kepalaruang.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tanggal Mulai Investigasi </label>
                        </td>
                        <td>
                            <input type="date" class="form-control" name="tglmulai_investigasi" value="'.$tglmulai_investigasi.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tanggal Selesai Investigasi </label>
                        </td>
                        <td>
                            <input type="date" class="form-control" name="tglselesai_investigasi" value="'.$tglselesai_investigasi.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" bgcolor="#F4A460" style="text-align:center;">
                            <label>DI ISI OLEH SUB KOMITE KESELAMATAN PASIEN RS (KPRS)</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Investigasi lengkap </label>
                        </td>
                        <td>
                            <input type="radio" class="investigasi_lengkap" name="investigasi_lengkap" value="1"'.($investigasi_lengkap == 1 ? "checked" : "").' required> Ya &nbsp; </input>
                            <input type="radio" class="investigasi_lengkap" name="investigasi_lengkap" value="0"'.($investigasi_lengkap == 0 ? "checked" : "").' required> Tidak </input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tanggal Investigasi Lengkap</label>
                        </td>
                        <td>
                            <input type="date" class="form-control" name="tglinvestigasi_lengkap" value="'.$tglinvestigasi_lengkap.'" required>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Diperlukan Investigasi lanjut </label>
                        </td>
                        <td>
                            <input type="radio" class="investigasi_lanjut" name="investigasi_lanjut" value="1"'.($investigasi_lanjut == 1 ? "checked" : "").' required> Ya &nbsp; </input>
                            <input type="radio" class="investigasi_lanjut" name="investigasi_lanjut" value="0"'.($investigasi_lanjut == 0 ? "checked" : "").' required> Tidak </input>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Investigasi setelah grading ulang </label>
                        </td>
                        <td>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="0"'.($investigasi_grading == 0 ? "checked" : "").' required> Biru &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="1"'.($investigasi_grading == 1 ? "checked" : "").' required> Hijau &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="2"'.($investigasi_grading == 2 ? "checked" : "").' required> Kuning &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="3"'.($investigasi_grading == 3 ? "checked" : "").' required> Merah </input>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>KRONOLOGI KEJADIAN HASIL INVESTIGASI SEDERHANA:</label><br>
                            <textarea class="form-control textarea" name="kronologi_kejadian" cols="75" rows="5" placeholder="Tuliskan ..." required="">'.$kronologi_kejadian.'</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right;">
                            <hr style="border: 1px solid #c6c6c6;">
                            <button class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Kembali</button>
                            <button class="btn btn-success" type="submit"><i class="fa fa-paper-plane"></i> Simpan</button>
                            <a href="'.base_url("Cindikatormutu/printinvestigasi/$encode_id").'" data-id="'.$idpelaporanikp.'" class="print-investigasi"><button class="btn btn-primary" type="button"><i class="fa fa-file-pdf-o"></i> Cetak </button></a>
                        </td>
                    </tr>
                 </tbody>
                 </table>
                 </form>
                 ';
        echo json_encode(['hsl'=>$body]);
    }

    public function simpan_investigasi_pelaporan()
    {
        $post           = $this->input->post();
        $idpelaporanikp = $post['idpelaporanikp'];

        $data_investigasi = [
            'idpelaporanikp' =>$idpelaporanikp,
            'penyebab_langsung' => $post['penyebab_langsung'],
            'penyebab_latarbelakang' => $post['penyebab_latarbelakang'],
            'rekomendasi' => $post['rekomendasi'],
            'pj_rekomendasi' =>$post['pj_rekomendasi'],
            'tindakan_dilakukan' => $post['tindakan_dilakukan'],
            'pj_tindakan' =>$post['pj_tindakan'],
            'nama_kepalaruang' =>$post['nama_kepalaruang'],
            'tglmulai_investigasi' =>format_date($post['tglmulai_investigasi'],'Y-m-d'),
            'tglselesai_investigasi' =>format_date($post['tglselesai_investigasi'],'Y-m-d'),
            'investigasi_lengkap' =>$post['investigasi_lengkap'],
            'tglinvestigasi_lengkap' =>format_date($post['tglinvestigasi_lengkap'],'Y-m-d'),
            'investigasi_lanjut' =>$post['investigasi_lanjut'],
            'investigasi_grading' =>$post['investigasi_grading'],
            'kronologi_kejadian' => $post['kronologi_kejadian'],
            'datecreated'    => indonesia_date(),
        ];

        $cek_data_insert = $this->msqlbasic->row_db('mutu_investigasi_sederhana', ['idpelaporanikp' =>$idpelaporanikp]);

        if( !empty($cek_data_insert) ){
            $update        = $this->msqlbasic->update_db('mutu_investigasi_sederhana',['idinvestigasi'=>$cek_data_insert['idinvestigasi']],$data_investigasi);
            echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Update'] );
        }else{
            $query  = $this->db->insert('mutu_investigasi_sederhana', $data_investigasi);
            if( $query ){
                echo json_encode( ['msg'=>'success','txt'=>'Data Berhasil di Input'] );
            }else{
                echo json_encode( ['msg'=>'danger','txt'=>'Data Gagal di Input'] );
            } 
        }
    }

    public function printinvestigasi()
    {       
        $idpelaporanikp  = $this->uri->segment('3');

        $data = [
            'idpelaporanikp'=>$idpelaporanikp,
        ];

        $this->pdf->filename = "investigasi.pdf";
        $this->pdf->load_view_ql('indikatormutu/v_download_investigasi',$data);
    }
   
}
?>