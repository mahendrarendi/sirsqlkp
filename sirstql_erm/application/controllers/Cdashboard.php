<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama

class Cdashboard extends CI_Controller 
{

    function __construct()
    {
	parent::__construct();
    // jika user belum login maka akses ditolak
       if(!$this->session->userdata('username')){pesan_belumlogin();}
       $this->load->model('mviewql');
    }

    public function index()
    {
        
        $data['content_view']      = 'beranda/v_dashboard';
        $data['title_page']        = '';
        $data['active_menu']       = 'beranda';
        $data['active_sub_menu']   = '';
        $data['active_menu_level'] = '';
        $data['plugins']           = [PLUG_CHART, PLUG_DROPDOWN, PLUG_DATATABLE,PLUG_DATE];
        $data['script_js']         = ['dashboard'];
        $this->load->view('v_index',$data);
    }
    
    public function error_page()
    {
        $data['content_view'] = 'errors/html/error_404';
        $data['title_page'] = '404 error page';
        $data['active_menu'] = '';
        $data['active_sub_menu'] = '';
        $data['active_menu_level'] = '';
        $data['plugins'] = '';
        $data['script_js'] = [];
        $this->load->view('v_index',$data);
    }
    
    // mahmud, clear :: 
    public function getdashboarddata_bypoli()
    {
        $unit   = $this->input->post('unit');
        $tahun1 = intval($this->input->post('tahun')) - 1;
        $tahun2 = $this->input->post('tahun');
        $dtunit = $this->db->get_where('rs_unit',['idunit'=>$unit])->row_array();
        
        // proporsi pasien lama baru
        $tahun= $this->input->post('tahun');
        $ppasien = $this->db->query("select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=1 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=2 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL 
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=3 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=4 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=5 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=6 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=7 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=8 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=9 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=10 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=11 ".((empty($unit))?"":" and p.idunit='".$unit."'")."
                UNION ALL
                select ifnull(round((sum(if((`p`.`ispasienlama` = 0),1,0)) / count(1)) * 100, 2 ),0) as baru, ifnull(round((sum(if((`p`.`ispasienlama` = 0),0,1)) / count(1)) * 100, 2 ),0) as lama, count(1) total from person_pendaftaran p where p.idstatuskeluar = '2' and p.tahunperiksa='".$tahun."' and p.bulanperiksa=12 ".((empty($unit))?"":" and p.idunit='".$unit."'")."")->result();
        
        $pasienlama=[];
        $pasienbaru=[];
        $bulan = [];
        $blama =[];
        $bbaru =[];
        $no=0;
        foreach ($ppasien as $obj)
        {
           ++$no;
           $pasienlama[]= $obj->lama;
           $pasienbaru[]= $obj->baru;
           $bulan [] = ql_namabulan($no);
           $blama[]  = 'rgba(74, 148, 193, 1)';
           $bbaru[]  = 'rgba(243, 156, 17, 1)';
        }
        // proporsi jaminan pasien
        $pjwarna=[];
        $pjcarabayar=[];
        $pjdata=[];
        $no=0;
        
        $pjaminan = $this->db->query("select carabayar, sum(if((`p`.`ispasienlama` = 0),1,0)) + sum(if((`p`.`ispasienlama` = 1),1,0)) as jumlahpasien from ((`person_pendaftaran` `p` join `person_pasien` `ps`) join `person_person` `pp`) where ((`p`.`idstatuskeluar` = '2') and (`ps`.`norm` = `p`.`norm`) and (`pp`.`idperson` = `ps`.`idperson`)) and p.tahunperiksa='".$tahun2."' ".((empty($unit))?"":" and p.idunit='".$unit."'")." group by carabayar")->result();
        
        foreach ($pjaminan as $obj)
        {
            $pjwarna[] = qlwarna(++$no);
            $pjcarabayar[] = $obj->carabayar;
            $pjdata[] = $obj->jumlahpasien;
        }
        
        // CAKUPAN         
        $cakupan1   = [];
        $bgcakupan1 = [];
        $dtcakupan1 = $this->db->query("select sum(if((`p`.`ispasienlama` = 0),1,0)) + sum(if((`p`.`ispasienlama` = 1),1,0)) as jumlahpasien from ((`person_pendaftaran` `p` join `person_pasien` `ps`) join `person_person` `pp`) where ((`p`.`idstatuskeluar` = '2') and (`ps`.`norm` = `p`.`norm`) and (`pp`.`idperson` = `ps`.`idperson`)) and p.tahunperiksa=".$tahun1." ".((empty($unit))?"":" and p.idunit='".$unit."'")." group by p.bulanperiksa")->result();
        foreach ($dtcakupan1 as $obj)
        {
            $cakupan1[]  = $obj->jumlahpasien; 
            $bgcakupan1[]= 'rgba(74, 148, 193, 1)';
        }
        
        $cakupan2   = [];
        $bgcakupan2 = [];
        $dtcakupan2 = $ppasien;
        foreach ($dtcakupan2 as $obj)
        {
            $cakupan2[]  = $obj->total;
            $bgcakupan2[]= 'rgba(243, 156, 17, 1)';
        } 

        // diagnosa 10 terbanyak
        $diagnosa10bsr = [];
        $tindakan10bsr = [];
        $bln10besar=[];
        
        $data['ppasien'] = $ppasien;
        $data['tahun1']  = $tahun1;
        $data['tahun2']  = $tahun2;
        $data['pbaru']   = $pasienbaru;
        $data['plama']   = $pasienlama;
        $data['bulan']   = $bulan;
        $data['blama']   = $blama;
        $data['bbaru']   = $bbaru;
        $data['pjwarna'] = $pjwarna;
        $data['pjcarabayar'] = $pjcarabayar;
        $data['pjdata']  = $pjdata;
        $data['unit']    = $dtunit;
        $data['cakupan1']= $cakupan1;
        $data['cakupan2']= $cakupan2;
        $data['bgcakupan1'] = $bgcakupan1;
        $data['bgcakupan2'] = $bgcakupan2;
        $data['diagnosa10bsr'] = $diagnosa10bsr;
        $data['tindakan10bsr'] = $tindakan10bsr;
        $data['bln10besar']    = $bln10besar;
        
        //target cakupan
        $data['targetcakupan'] = [];
        $target = $this->mviewql->getKonfigurasi('targetcakupan');
        for($x = 0; $x < 12; $x++)
        {
            $data['targetcakupan'][] = $target;
        }
        echo json_encode($data);
    }

    public function diagnosatindakan10besar()
    {
        $unit  = $this->input->post('unit');
        $tahun = $this->input->post('tahun');
        // diagnosa 10 terbanyak
        $diagnosa10bsr = [];
        $tindakan10bsr = [];
        $bln10besar    = [];
        
        for($x=1; $x<=12;$x++)
        {
            $diagnosa10bsr[] = $this->mviewql->get_diagnosatersering($tahun,$unit,$x,10,2,10)->result();
            $tindakan10bsr[] = $this->mviewql->get_diagnosatersering($tahun,$unit,$x,10,3,9)->result();
            $bln10besar[]    = ql_namabulan($x);
        }
        
        $data['diagnosa10bsr'] = $diagnosa10bsr;
        $data['tindakan10bsr'] = $tindakan10bsr;
        $data['bln10besar']    = $bln10besar;
        echo json_encode($data);
    }

    /*
     * Unduh Excel Cakupan Poli
     */
    public function unduh_cakupanpoli()
    {
        $tahun          = $this->uri->segment(3);
        $unit           = ((empty($this->uri->segment(4))) ? null : $this->uri->segment(4) );
        $whereunit      = ((empty($this->uri->segment(4))) ? '' : " and p.idunit ='".$this->uri->segment(4)."' ");
        $sqlcakupanpoli = "select p.idunit,ru.namaunit, p.bulanperiksa AS `bulan`,p.tahunperiksa AS `tahun`, sum(if((`p`.`ispasienlama` = 0),1,0)) as totalpasienbaru, sum(if((`p`.`ispasienlama` = 1),1,0)) as totalpasienlama, sum(if((`p`.`ispasienlama` = 0),1,0)) + sum(if((`p`.`ispasienlama` = 1),1,0)) as total from (((`person_pendaftaran` `p` join `person_pasien` `ps`) join `person_person` `pp`) join `rs_unit` `ru`) where ((`p`.`idstatuskeluar` = '2') and (`ps`.`norm` = `p`.`norm`) and (`pp`.`idperson` = `ps`.`idperson`) and ( `ru`.`idunit`=`p`.`idunit`)) and p.tahunperiksa='".$tahun."' ".$whereunit." group by  p.idunit,p.bulanperiksa";
        $sqlcarabayar   = "select carabayar, sum(if((`p`.`ispasienlama` = 0),1,0)) + sum(if((`p`.`ispasienlama` = 1),1,0)) as jumlahpasien, ru.namaunit from (((`person_pendaftaran` `p` join `person_pasien` `ps`) join `person_person` `pp`) join `rs_unit` `ru`) where ((`p`.`idstatuskeluar` = '2') and (`ps`.`norm` = `p`.`norm`) and (`pp`.`idperson` = `ps`.`idperson`) and `ru`.`idunit`=`p`.`idunit`) and p.tahunperiksa='".$tahun."' ".$whereunit." group by p.carabayar, p.idunit";
        
        // diagnosa 10 terbanyak
        $diagnosa10bsr = [];
        $tindakan10bsr = [];
        $bln10besar=[];
        
        for($x=1; $x<=12;$x++)
        {
            $diagnosa10bsr[] = $this->mviewql->get_diagnosatersering($tahun,$unit,$x,10,2,10)->result();
            $tindakan10bsr[] = $this->mviewql->get_diagnosatersering($tahun,$unit,$x,10,3,9)->result();
            $bln10besar[]    = ql_namabulan($x);
        }
        
        $data['diagnosa10bsr']= $diagnosa10bsr;
        $data['tindakan10bsr']= $tindakan10bsr;
        $data['bln10besar']   = $bln10besar;
        $data['cakupanpoli']  = $this->db->query($sqlcakupanpoli)->result();
        $data['carabayar']    = $this->db->query($sqlcarabayar)->result();
        $data['tahun']        = $tahun;
        $data['mstrcarabayar']= $this->mviewql->get_data_enum('person_pendaftaran','carabayar');
        $this->load->view('report/dashboard',$data);
    }
    
    public function getdatatindakanall()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $tahun = date('Y',  strtotime($tahunbulan));
        $bulan = date('n',  strtotime($tahunbulan));
        $data  = $this->mviewql->get_data_tindakanrawtjalan_all($tahun,$bulan);
        echo json_encode($data);
    }
    
    public function unduhdatatindakanall()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $tahun = date('Y',  strtotime($tahunbulan));
        $bulan = date('n',  strtotime($tahunbulan));
        $data['laporan']    = $this->mviewql->get_data_tindakanrawtjalan_all($tahun,$bulan);
        $data['judul']      = 'Tindakan Rawat Jalan Bulan '.$tahunbulan;
        return $this->load->view('report/excel_tindakanralan_all',$data);
    }
    
    /**
     * Ganti Shif Jaga Kasir
     */
    public  function gantishifkasir()
    {
        $shif = $this->db->select('mulaishif, levelshif, label,shifberikutnya')->get_where('shifuser',['statusaktif'=>1])->row_array();
        
        $log = ' IP:'.$_SERVER['REMOTE_ADDR'].' Client-Info:'. exec('getmac');
        $arrMsg = $this->db->insert('shifuser_log',['iduser'=>json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('iduser'))),'levelshif'=>$shif['levelshif'],'log'=>$log]);
        $arrMsg = $this->db->update('shifuser',['statusaktif'=>0],[1=>1]);
        $arrMsg = $this->db->update('shifuser',['statusaktif'=>1],['levelshif'=>$shif['shifberikutnya']]);
        $this->session->sess_destroy();
        pesan_success_danger($arrMsg, 'Ganti Shif Berhasil', 'Ganti Shif Gagal', 'js');
    
    }
    
    /**
     * Menampilkan Shif Kasir yang aktif
     */
    public function tampilkanshifaktif()
    {
        echo json_encode($this->db->select('label, date(mulaishif) mulaishif')->get_where('shifuser',['statusaktif'=>1])->row_array());
    }
    
    
}

/* End of file ${TM_FILENAME:${1/(.+)/Cdashboard.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cdashboard/:application/controllers/${1/(.+)/Cdashboard.php/}} */






