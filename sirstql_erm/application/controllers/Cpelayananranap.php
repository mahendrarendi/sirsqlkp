<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
// defined('V_PEMERIKSAANRANAP') OR define('V_PEMERIKSAANRANAP', 24);
// defined('V_ADMINISTRASIANTRIAN') OR define('V_ADMINISTRASIANTRIAN', 29);

class Cpelayananranap extends MY_controller 
{
    function __construct()
    {
	   parent::__construct();
       // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
       $this->load->model('mkombin');
       $this->load->model('mviewql');
       $this->load->model('msqlbasic');
       $this->load->helper(array('form', 'url'));
       $this->load->library(array('form_validation', 'session'));
    }
    
    ///////////////////////LABORATORIUM RANAP///////////////////
    //-------------------------- vv Standar
    //---------- mahmud, clear
    public function setting_laboratoriumranap()
    {
        return [    'content_view'      => 'pelayanan/v_laboratoriumranap',
                    'active_menu'       => 'pelayanan',
                    'active_sub_menu'   => 'laboratoriumranap',
                    'active_menu_level' => ''
               ];
    }
    
    //---------- mahmud, clear
    public function laboratoriumranap()//list laboratorium ranap
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_LABORATORIUMRANAP)) //lihat define di atas
         { 
            $data                = $this->setting_laboratoriumranap(); //letakkan di baris pertama
            $data['title_page']  = 'Laboratorium Rawat Inap';
            $data['mode']        = 'view';
            $data['script_js']   = ['js_pemeriksaanranap-rencanaperawatanpaket','riwayat_perawatanranap','js_laboratoriumranap'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    //---------- mahmud, clear
    public function dt_laboratoriumranap()
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_LABORATORIUMRANAP)) //lihat define di atas
         { 
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_laboratoriumranap_(true);
            $data=[];
            if($getData){
                foreach ($getData->result() as $obj)
                {
                    $menu  = ' <a id="riwayatperawatan" idinap="'.$obj->idinap.'" idpendaftaran="'.$obj->idpendaftaran.'" norm="'.$obj->norm.'" class=" btn btn-info btn-xs" data-toggle="tooltip" data-original-title="Riwayat Perawatan"><i class="fa fa-history"></i></a>';
                    $row = [
                        "DT_RowId" => 'row_'.$obj->idpendaftaran,
                        "norm"=>$obj->norm,
                        "namalengkap"=>$obj->namalengkap,
                        "tanggallahir"=>$obj->tanggallahir,
                        "nosep" => $obj->nosep,
                        "waktu" => $obj->waktumasuk,
                        "namadokter" => $obj->namadokter,
                        "namabangsal" => $obj->namabangsal.' ('.$obj->nobed.')',
                        "menu" => $menu
                    ];
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_laboratoriumranap(),
                "recordsFiltered" => $this->mdatatable->filter_dt_laboratoriumranap(),
                "data" =>$data
            );
            //output dalam format JSON
            echo json_encode($output);
         }
         else
         {
             aksesditolak();
         }
    }
    
    public function dt_laboratoriumranapdetail()
    {
        if(empty($this->input->post('idp')))
        {
            $data = ['status'=>'info','message'=>'Belum Ada Pemeriksaan yang dilakukan.'];
        }
        else
        {
            $idp = explode('_', $this->input->post('idp'));

            $sql = "select rh.idpendaftaran, rh.idrencanamedispemeriksaan,rirmp.waktu, rirmp.status, rirmp.idinap, REPLACE(concat('<ol>',group_concat(concat('<li>',if(rh.icd is null,rp.namapaketpemeriksaan,concat(ri.icd,' ',ri.namaicd) ),'</li>') ),'</ol>'), ',' ,'') as tindakan FROM rs_inap_rencana_medis_hasilpemeriksaan  rh
            join rs_inap_rencana_medis_pemeriksaan rirmp on rirmp.idrencanamedispemeriksaan = rh.idrencanamedispemeriksaan
            left join rs_icd ri on ri.icd = rh.icd
            left join rs_paket_pemeriksaan rp on rp.idpaketpemeriksaan = rh.idpaketpemeriksaan
            WHERE ( (rh.idpaketpemeriksaanparent is null and ri.idjenisicd=4 ) or rh.icd is null) and date(rirmp.waktu) = '".$this->input->post('tanggal')."' and rh.idpendaftaran='".$idp[1]."'
                GROUP by rh.idpendaftaran, rh.idrencanamedispemeriksaan
                ORDER by rirmp.status ASC, rirmp.waktu DESC";

            $data = $this->db->query($sql)->result();
        }
        echo json_encode($data);
    }



    ///////////////////////ADMISSION PENDAFTARAN POLI///////////////////
    //-------------------------- vv Standar
    //---------- basit, clear
    public function setting_pemeriksaanranap()
    {
        return [    'content_view'      => 'pelayanan/v_pemeriksaanranap',
                    'active_menu'       => 'pelayanan',
                    'active_sub_menu'   => 'pemeriksaanranap',
                    'active_menu_level' => ''
               ];
    }
    
    //---------- mahmud, clear
    public function tambahrencanaperawatan()//list pemeriksaanranap
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP)) //lihat define di atas
         { 
            $this->load->library('user_agent');
            $data['content_view'] = 'pelayanan/v_pemeriksaanranap-tambahrencanaperawatan';
            $data['active_menu']  = 'pelayanan';
            $data['active_sub_menu']='pemeriksaanranap';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Perencanaan Perawatan Pasien Rawat Inap';
//            $data['mode']        = 'view';
            $data['script_js']   = ['js_pemeriksaanranap-addrencana'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap()//list pemeriksaanranap
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP)) //lihat define di atas
         { 
            $data                = $this->setting_pemeriksaanranap(); //letakkan di baris pertama
            $data['title_page']  = 'Pemeriksaan Rawat Inap';
            $data['mode']        = 'view';
            $data['table_title'] = 'List Pasien Rawat Inap';
            $data['script_js']   = ['js_pemeriksaanranap-rencanaperawatanpaket','js_pemeriksaanranap'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }

     //mahmud, clear
    public function getpasienranap() // cari pasien rawat inap
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $jenis   = $this->input->post('jenis');
            if ($jenis == '' || $jenis == 'bangsal')
            {
                $data       = $this->input->post('data');
            $tglawal    = ifnull($this->input->post('tglawal'), date('Y/m/d'));
            $tglakhir   = ifnull($this->input->post('tglakhir'), date('Y/m/d'));
            $pasien = $this->mkombin->rsinap_getdata($data,format_date_to_sql($tglawal),format_date_to_sql($tglakhir));
            
                echo json_encode(['pasien'=>$pasien,'debug'=>$this->db->last_query()]);
        }
            else if ($jenis == 'pasien')
            {
                $data       = $this->input->post('data');
                echo json_encode($this->mkombin->rsinap_getdata_pasien($data));
            }
            else if ($jenis == 'now')
            {
                echo json_encode($this->mkombin->rsinap_getdata_sekarang());
            }
        }
    }
    

    //---------- basit, clear
    public function carirawatinap()//list pemeriksaanranap
    {
        $this->db->where("rs_inap.status !=", 'batal');
        $this->db->where("rs_inap.status !=", 'selesai');
        $this->db->where("rs_inap.status !=", 'pulang');
        $this->db->where("rs_inap.status !=", 'pesan');
        $data = $this->db->query("select idinap, concat(namalengkap, '@', namabangsal, ' bed:', nobed) as datainap 
        from rs_inap ri 
        join person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson
        join rs_bed rs on rs.idbed = ri.idbed
        join rs_bangsal rb on rb.idbangsal = rs.idbangsal
        where ri.status != 'batal' and ri.status != 'selesai' and ri.status != 'pulang' and ri.status != 'pesan'")->result();
        
        echo json_encode($data);
    }
    
    //---------- basit, clear
    public function cariicd()//list pemeriksaanranap
    {
        $this->db->order_by("jenisicd, namaicd");
        $idjenisicd = $this->input->post('ji');
        if (!empty($idjenisicd))
        {
            $this->db->where("ricd.idjenisicd in ($idjenisicd)");
        }
        echo json_encode($this->mgenerikbap->select_multitable("icd, concat(jenisicd, ' - ', namaicd, ' [', icd, ']') as namaicd, aliasicd", "rs_icd, rs_jenisicd")->result());
    }
    
    //cari tindakan ranap
    public function cariicdranap()
    {
        
        $idjenisicd = $this->input->post('ji');
        $idinap     = $this->input->post('idinap');
        
        //set idkelas ranap
        $idkelas = $this->db->get_where('rs_inap',['idinap'=>$idinap])->row_array()['idkelas'];
        
        $this->db->order_by("jenisicd, namaicd");
        
        if (!empty($idjenisicd))
        {
            $where_jenisicd = " and ri.idjenisicd in ($idjenisicd)";
        }
        
        $sql = "select ri.icd, concat(jenisicd, ' - ', namaicd, ' [', ri.icd, ']') as namaicd, aliasicd from rs_icd ri 
                join rs_jenisicd rj on rj.idjenisicd = ri.idjenisicd
                join rs_mastertarif rm on rm.icd = ri.icd
                where rm.idkelas = '".$idkelas."' ".$where_jenisicd;
        
        $dt = $this->db->query($sql)->result_array();
        echo json_encode($dt);
    }
    
    //---------- basit, clear
    //simpan rencana perawatan inap
    public function simpan_rencana()
    {
        /*ambil no pendaftaran*/
        $this->mgenerikbap->setTable('rs_inap');
        $idp = $this->mgenerikbap->select("idpendaftaran", ["idinap" => $this->input->post('idinap')])->result_array();
        /*simpan dulu rencana barang (array)*/
        $idinap                 = $this->input->post('idinap');
        $idpendaftaran          = $idp[0]["idpendaftaran"];
        $icdrencaanaperawatan   = $this->input->post('icd');
//        $idbarangrencaanapenggunaan   = $this->input->post('idbarangpenggunaan');
        $arrMsg = 0;
        /*set timezone*/
        date_default_timezone_set('GMT');
        
        //insert rencana perawatan
        $index_perencanaan          = $this->input->post('index_perencanaan');
        if(empty(!$index_perencanaan))
        {
            foreach ($icdrencaanaperawatan as $keyicdrencana => $valueicdrencana) 
            {
                $idInaprencanamedisbarang   = array();
                $rencanabarangobatbhp       = $this->input->post('rencanabarangobatbhp'.$index_perencanaan[$keyicdrencana]);
                $rencanabarangjumlahpesan   = $this->input->post('rencanabarangjumlahpesan'.$index_perencanaan[$keyicdrencana]);
                $rencanabarangsatuanpakai   = $this->input->post('rencanabarangsatuanpakai'.$index_perencanaan[$keyicdrencana]);
                $rencanabarangjumlahpakai   = $this->input->post('rencanabarangjumlahpakai'.$index_perencanaan[$keyicdrencana]);

                $jumlahperhari  = $this->input->post('jumlahperhari')[$keyicdrencana];
                $selama         = $this->input->post('selama')[$keyicdrencana];
                $jumlahtindakan = $this->input->post('jumlahtindakan')[$keyicdrencana];


                for ($harike = 0; $harike < $selama; $harike++)
                {
                    for ($periodeke = 0; $periodeke < $jumlahperhari; $periodeke++)
                    {
                        $callid = generaterandom(10);
                        $waktu = date('Y-m-d H:i',strtotime('+'.$harike.' days +'.(24*$periodeke/$jumlahperhari).' hours', strtotime($this->input->post('tgl').':00:00')));
                        $datainaprencana = [
                            "idinap" => $this->input->post('idinap'),
                            "status" => "rencana",
                            "waktu"  => $waktu,
                            "callid" => $callid
                        ];
                        $this->mgenerikbap->setTable('rs_inap_rencana_medis_pemeriksaan');
                        $this->mgenerikbap->insert_ignoreduplicate($datainaprencana, "callid='$callid'");
                        $id = $this->mgenerikbap->select("idrencanamedispemeriksaan", ["callid" => $callid])->result_array();
                        $this->mgenerikbap->update_where(["callid" => "","status"=>"rencana"], ["callid" => $callid]);

                        /*simpan tindakan*/
                        $data = [
                            'icd'                           => $valueicdrencana,
                            'jumlah'                        => $jumlahtindakan,
                            'idpegawaidokter'               => $this->input->post('idpegawaidokter'),
                            'statuspelaksanaan'             => "rencana",
                            'idrencanamedispemeriksaan'     => $id[0]["idrencanamedispemeriksaan"],
                            'idpendaftaran'                 => $idp[0]["idpendaftaran"],
                            "callid" => $callid
                        ];
                        $this->mgenerikbap->setTable('rs_inap_rencana_medis_hasilpemeriksaan');
                        $arrMsg             = $this->mgenerikbap->insert_ignoreduplicate($data);
                        $idhasilpemeriksaan = $this->mgenerikbap->select("idrencanamedishasilpemeriksaan", ["callid" => $callid])->result_array();
                        $this->mgenerikbap->update_where(["callid" => ""], ["callid" => $callid]);

                        /*simpan bhp/obat, jika ada*/
                        if(empty(!$rencanabarangobatbhp))
                        {
                            foreach ($rencanabarangobatbhp as $keyrbarang => $valuerbarang)
                            {
                                $data = [
                                    'idbarang'                      => $valuerbarang,
                                    'idrencanamedishasilpemeriksaan'=> $idhasilpemeriksaan[0]["idrencanamedishasilpemeriksaan"],
                                    'jumlah'                        => $rencanabarangjumlahpakai[$keyrbarang],
                                    'idsatuanpemakaian'             => $rencanabarangsatuanpakai[$keyrbarang],
                                    'kekuatan'                      => 1,
                                    'statuspelaksanaan'             => "rencana",
                                    'idrencanamedispemeriksaan'     => $id[0]["idrencanamedispemeriksaan"],                                    
                                    'idpendaftaran'                 => $idp[0]["idpendaftaran"],
                                    "callid" => $callid
                                ];
                                $this->mgenerikbap->setTable('rs_inap_rencana_medis_barangpemeriksaan');
                                $arrMsg = $this->mgenerikbap->insert_ignoreduplicate($data);
                                $this->mgenerikbap->update_where(["callid" => ""], ["callid" => $callid]);
                            }
                        }
                    }
                }
            }
        }
        
        
        
        //insert perencanaan penggunaan obat bhp
        $obidbarang   = $this->input->post('obidbarang');
        if(empty(!$obidbarang))
        {
            foreach ($obidbarang as $keyob => $valueob) 
            {
                $idbarang   = $valueob;
                $pemberian  = $this->input->post('obpemberian')[$keyob];
                $perhari    = $this->input->post('obperhari')[$keyob];
                $satuan     = $this->input->post('obsatuan')[$keyob];
                $selama     = $this->input->post('obselama')[$keyob];

                for ($harike = 0; $harike < $selama; $harike++)
                {
                    for ($periodeke = 0; $periodeke < $perhari; $periodeke++)
                    {
                        $callid = generaterandom(10);
                        $waktu = date('Y-m-d H:i',strtotime('+'.$harike.' days +'.(24*$periodeke/$perhari).' hours', strtotime($this->input->post('tgl').':00:00')));
                        $datainaprencana = [
                            "idinap" => $this->input->post('idinap'),
                            "status" => "rencana",
                            "waktu"  => $waktu,
                            "callid" => $callid
                        ];
                        $this->mgenerikbap->setTable('rs_inap_rencana_medis_pemeriksaan');
                        $this->mgenerikbap->insert_ignoreduplicate($datainaprencana, "callid='$callid'");
                        $id = $this->mgenerikbap->select("idrencanamedispemeriksaan", ["callid" => $callid])->result_array();
                        $this->mgenerikbap->update_where(["callid" => "","status"=>"rencana"], ["callid" => $callid]);

                        /*simpan tindakan*/
                        $data = [
                            'idbarang'                      => $idbarang,
                            'jumlah'                        => $pemberian,
                            'kekuatan'                      => 1,
                            'idsatuanpemakaian'             => $satuan,
                            'statuspelaksanaan'             => "rencana",
                            'idrencanamedispemeriksaan'     => $id[0]["idrencanamedispemeriksaan"],
                            'idpendaftaran'                 => $idp[0]["idpendaftaran"],
                            "callid" => $callid
                        ];
                        $this->mgenerikbap->setTable('rs_inap_rencana_medis_barangpemeriksaan');
                        $arrMsg             = $this->mgenerikbap->insert_ignoreduplicate($data);
                        $idhasilpemeriksaan = $this->mgenerikbap->select("idrencanamedishasilpemeriksaan", ["callid" => $callid])->result_array();
                        $this->mgenerikbap->update_where(["callid" => ""], ["callid" => $callid]);
                    }
                }
            }
        }        
        
        pesan_success_danger($arrMsg,"Insert Rencana Keperawatan Success","Insert Rencana Keperawatan Failed","js");
    }
    
    //---------- basit, clear
    public function simpan_rencanapaket()
    {
        //insert atau update rs_inap_rencana_medis_pemeriksaan
        //set timezone
        date_default_timezone_set('GMT');
        $jumlahperhari  = $this->input->post('j');
        for ($harike = 0; $harike < $this->input->post('s'); $harike++)
        {
            for ($periodeke = 0; $periodeke < $jumlahperhari; $periodeke++)
            {
                $callid = generaterandom(10);
                $waktu = date('Y-m-d H:i',strtotime('+'.$harike.' days +'.(24*$periodeke/$jumlahperhari).' hours', strtotime($this->input->post('t').':00:00')));
                $datainaprencana = [
                    "idinap" => $this->input->post('in'),
                    "status" => "rencana",
                    "waktu"  => $waktu,
                    "callid" => $callid
                ];
                $this->mgenerikbap->setTable('rs_inap_rencana_medis_pemeriksaan');
                $this->mgenerikbap->insert_ignoreduplicate($datainaprencana, "callid='$callid'");
                $id = $this->mgenerikbap->select("idrencanamedispemeriksaan", ["callid" => $callid])->result_array();
                $this->mgenerikbap->update_where(["callid" => "","status"=>"rencana"], ["callid" => $callid]);
                
                //ambil no pendaftaran, idkelas, idkelasjaminan
                $this->mgenerikbap->setTable('rs_inap');
                $idp = $this->mgenerikbap->select("idpendaftaran, idkelas, idkelasjaminan", ["idinap" => $this->input->post('in')])->result_array();
                
                // ********* SIAPKAN DATA PAKET
                $idrencanamedishasilpemeriksaan  = $this->input->post("idrencanamedishasilpemeriksaan");
                $status                          = 'rencana';
                $icd                             = $this->input->post('ic');
                $idrencanamedispemeriksaan       = $id[0]["idrencanamedispemeriksaan"];
                $idpendaftaran                   = $idp[0]["idpendaftaran"];
                $ispaket                         = 'ispaket';
                $idkelas                         = $idp[0]["idkelas"];
                $idkelasjaminan                  = $idp[0]["idkelasjaminan"];
                $showmsg                         = 'not';
                $updateall                       = null;
                // simpan paket
                $arrMsg = $this->cekmasterpakettarif_ranap('Paket Laboratorium',$idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran,$updateall, $showmsg);
                
            }
        }
        $arrMsg = true;
        pesan_success_danger($arrMsg,"Insert Rencana Keperawatan Success","Insert Rencana Keperawatan Failed","js");

    }
    
    //---------- basit, clear
    public function simpan_rencanabhp()
    {
        //insert atau update rs_inap_rencana_medis_pemeriksaan
        //set timezone
        date_default_timezone_set('GMT');
        
        $jumlahperhari  = $this->input->post('j');
        for ($harike = 0; $harike < $this->input->post('s'); $harike++)
        {
            for ($periodeke = 0; $periodeke < $jumlahperhari; $periodeke++)
            {
                $callid = generaterandom(10);
                $waktu = date('Y-m-d H:i',strtotime('+'.$harike.' days +'.(24*$periodeke/$jumlahperhari).' hours', strtotime($this->input->post('t').':00:00')));
                $datainaprencana = [
                    "idinap" => $this->input->post('in'),
                    "status" => "rencana",
                    "waktu"  => $waktu,
                    "callid" => $callid
                ];
                $this->mgenerikbap->setTable('rs_inap_rencana_medis_pemeriksaan');
                $this->mgenerikbap->insert_ignoreduplicate($datainaprencana, "callid='$callid'");
                $id = $this->mgenerikbap->select("idrencanamedispemeriksaan", ["callid" => $callid])->result_array();
                $this->mgenerikbap->update_where(["callid" => ""], ["callid" => $callid]);
                $data = [
                    'idrencanamedisbarang'          => $this->input->post('ir'),
                    'jumlah'                        => $this->input->post('jm'),
                    'statuspelaksanaan'             => "rencana",
                    'idrencanamedispemeriksaan'     => $id[0]["idrencanamedispemeriksaan"]                
                ];
                $this->mgenerikbap->setTable('rs_inap_rencana_medis_barangpemeriksaan');
                $arrMsg = $this->mgenerikbap->insert_ignoreduplicate($data);
            }
        }
        pesan_success_danger($arrMsg,"Insert Pemakaian BHP/Obat Success","Insert Pemakaian BHP/Obat Failed","js");
    }
    
    //---------- basit, clear
    public function simpan_rencanabhp_single()
    {
        $data = [
            'idpendaftaran'                 => $this->input->post('idpendaftaran'),
            'idbarang'                      => $this->input->post('tambahbhpditindakan'),
            'jumlah'                        => 1,
            'statuspelaksanaan'             => "rencana",
            'idrencanamedispemeriksaan'     => $this->input->post('idrencanamedispemeriksaan'),
            'idrencanamedishasilpemeriksaan'=> $this->input->post('idrencanamedishasilpemeriksaan')              
        ];
        $arrMsg = $this->db->insert('rs_inap_rencana_medis_barangpemeriksaan',$data);
        pesan_success_danger($arrMsg,"Insert Pemakaian BHP/Obat Success","Insert Pemakaian BHP/Obat Failed","js");
    }
    
    //---------- basit, clear
    public function belumdiplot()
    {
        echo json_encode($this->mviewql->viewinapbelumdiplot());
    }
    
    //---------- basit, clear
    public function batalkan_rencanabhp()
    {
        $this->mgenerikbap->setTable('rs_inap_rencana_medis_barangpemeriksaan');
        $arrMsg = $this->mgenerikbap->update_where(["statuspelaksanaan" => "batal"], ["idrencanamedisbarang" => $this->input->post('i')]);
        pesan_success_danger($arrMsg,"Pembatalan Pemakaian BHP/Obat Success","Pembatalan Pemakaian BHP/Obat Failed","js");
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahsatuanpemakaian() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $i  = $this->input->post('i');
            $is = $this->input->post('is');
            $arrMsg = $this->db->update('rs_inap_rencana_medis_barang',['idsatuanpemakaian'=>$is],['idrencanamedisbarang'=>$i]) ;
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahkemasan() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $i  = $this->input->post('i');
            $is = $this->input->post('is');
            $arrMsg = $this->db->update('rs_inap_rencana_medis_barang',['idkemasan'=>$is],['idrencanamedisbarang'=>$i]) ;
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahpenggunaan() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $i  = $this->input->post('i');
            $is = $this->input->post('is');
            $arrMsg = $this->db->update('rs_inap_rencana_medis_barang',['penggunaan'=>$is],['idrencanamedisbarang'=>$i]) ;
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahjenisasal() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $i  = $this->input->post('i');
            $ja = $this->input->post('ja');
            $arrMsg = $this->db->update('rs_inap_rencana_medis_barang',['jenisasal'=>$ja],['idrencanamedisbarang'=>$i]) ;
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahdokterhasilpemeriksaan() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $i  = $this->input->post('i');
            $id = $this->input->post('id');
            $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan',['idpegawaidokter'=>$id],['idrencanamedishasilpemeriksaan'=>$i]) ;
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_ubahstatus() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $table = 'rs_inap_rencana_medis_pemeriksaan';
            $id    = $this->input->post('i'); //idrencanamedispemeriksaan
            $status = $this->input->post('status');
            
            //jika status akan diubah menjadi terlaksana
            if($status=='terlaksana'){
                $rencana = $this->mkombin->cek_statushasilperencanaanperawatan($id);
                if($rencana->num_rows() > 0)
                {
                    $result = $rencana->result_array();
                    echo json_encode(['rencana'=>$result,'status'=>'menunggu']);
                    return true;
                }
            }
            //jika status bukan terlaksana set semua rencana sesuai status
            if($status != 'terlaksana')
            {
                $where = ($status=='rencana') ? ['idrencanamedispemeriksaan'=>$id,'statuspelaksanaan !='=>'terlaksana'] : ['idrencanamedispemeriksaan'=>$id] ;
                $arrMsg = $this->db->update('rs_inap_rencana_medis_barangpemeriksaan',['statuspelaksanaan'=>$status],$where);
                $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan',['statuspelaksanaan'=>$status],$where);
            }
            $this->mgenerikbap->setTable($table);
            $arrMsg = $this->db->update($table,['status'=>$status],['idrencanamedispemeriksaan'=>$id]) ;
            
            $data   = $this->db->query("select ri.idpendaftaran, r.norm from rs_inap_rencana_medis_pemeriksaan rip join rs_inap ri on ri.idinap=rip.idinap join person_pendaftaran r on r.idpendaftaran=ri.idpendaftaran where idrencanamedispemeriksaan='$id'")->row_array();
            $datal  = [
                'url'     => get_url(),
                'log'     => 'Pemeriksaan RANAP- user:'.$this->session->userdata('username').';status:'.$status.';idpemeriksaan:'.$id.';idpendaftaran:'.$data['idpendaftaran'].';norm:'.$data['norm'],
                'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];
            $this->mgenerikbap->setTable('login_log');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
     //---------- basit, clear
    public function obatranap_checklist_ubahstatus() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {
            $table = 'rs_inap_rencana_medis_barangpemeriksaan';
            $id = $this->input->post('i');
            $status = $this->input->post('status');
            $this->mgenerikbap->setTable($table);
            $arrMsg = $this->db->update($table,['statuspelaksanaan'=>$status],['idrencanamedisbarangpemeriksaan'=>$id]) ;
            $data = $this->db->query("select ri.idpendaftaran, r.norm from rs_inap_rencana_medis_pemeriksaan rip join rs_inap ri on ri.idinap=rip.idinap join person_pendaftaran r on r.idpendaftaran=ri.idpendaftaran where idrencanamedispemeriksaan='$id'")->row_array();
            $datal = [
               'url'     => get_url(),
               'log'     => 'Pemeriksaan RANAP- user:'.$this->session->userdata('username').';status:'.$status.';idrencanamedisbarangpemeriksaan:'.$id.';idpendaftaran:'.$data['idpendaftaran'].';norm:'.$data['norm'],
               'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];
           $this->mgenerikbap->setTable('login_log');
           $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
        // }
    }
    
     //---------- basit, clear
    public function pemeriksaanranap_checklist_ubahstatus() // PEMERIKSANKLINIK UBAH STATUS
    {
        // siapkan data
        $idinap                          = $this->input->post('idn');
        $idrencanamedishasilpemeriksaan  = $this->input->post("idh");
        $status                          = $this->input->post("status");
        $icd                             = $this->input->post("icd");
        $idrencanamedispemeriksaan       = $this->input->post("a");
        $idpendaftaran                   = $this->input->post("b");

        // ambil data idkelas, idkelasjaminan dari rs_inap 
        $inap = $this->db->query("select idkelas, idkelasjaminan from rs_inap where idinap='".$idinap."'")->row_array();
        $idkelas                         = $inap['idkelas'];
        $idkelasjaminan                  = $inap['idkelasjaminan'];
        // 
        $this->cekmastertarif('', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan);
    }
    
     //---------- basit, clear
    public function pemeriksaanranap_checklist_ubahstatus_() // PEMERIKSANKLINIK UBAH STATUS
    {
        // if ($this->pageaccessrightbap->checkAccessRight('V_PEMERIKSAANRANAP'))
        // {

        $status = $this->input->post('status');
        $id     = $this->input->post('i');
        if($status=='terlaksana'){$this->obattindakanranap_checklist_ubahstatus_($id);}
        $table  = 'rs_inap_rencana_medis_pemeriksaan';
        $arrMsg = $this->db->update($table,['status'=>$status],['idrencanamedispemeriksaan'=>$id]);
        
        if($status=='batal')
        {
            $table = 'rs_inap_rencana_medis_hasilpemeriksaan';   
            $arrMsg = $this->db->update($table,['statuspelaksanaan'=>$status],['idrencanamedispemeriksaan'=>$id]);
            $table = 'rs_inap_rencana_medis_barangpemeriksaan';   
            $arrMsg = $this->db->update($table,['statuspelaksanaan'=>$status],['idrencanamedispemeriksaan'=>$id]);
        }
        $data = $this->db->query("select ri.idpendaftaran, r.norm from rs_inap_rencana_medis_pemeriksaan rip join rs_inap ri on ri.idinap=rip.idinap join person_pendaftaran r on r.idpendaftaran=ri.idpendaftaran where idrencanamedispemeriksaan='$id'")->row_array();
        $datal = [
            'url'     => get_url(),
            'log'     => 'Pemeriksaan RANAP- user:'.$this->session->userdata('username').';status:'.$status.';idrencanamedispemeriksaan:'.$id.';idpendaftaran:'.$data['idpendaftaran'].';norm:'.$data['norm'],
            'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');

        // }
    }
    // PEMERIKSAANRANAP UBAH STATUS OBAT dan TINDAKAN DARI JADWAL
    private function obattindakanranap_checklist_ubahstatus_($id) 
    {
            // ubah menjadi terlaksana 
            $table = 'rs_inap_rencana_medis_barangpemeriksaan';
            $this->mgenerikbap->setTable($table);
            $this->db->update($table,['statuspelaksanaan'=>'terlaksana'],['idrencanamedispemeriksaan'=>$id,'statuspelaksanaan'=>'rencana']) ;
            // SIAPKAN DATA INAP
            $inap = $this->db->query("SELECT rmh.idrencanamedispemeriksaan, ri.idinap, ri.idkelas, ri.idkelasjaminan, rmh.icd, rmh.idrencanamedishasilpemeriksaan, rmh.idpaketpemeriksaan FROM rs_inap ri join rs_inap_rencana_medis_pemeriksaan rip on ri.idinap=rip.idinap and rip.idrencanamedispemeriksaan = '".$id."'join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan and rmh.idrencanamedispemeriksaan='".$id."'")->result();
            foreach ($inap as $obj) 
            {
                $idrencanamedishasilpemeriksaan  = $obj->idrencanamedishasilpemeriksaan;
                $icd                             = $obj->icd;
                $idrencanamedispemeriksaan       = '';
                $idpendaftaran                   = '';
                $idkelas                         = $obj->idkelas;
                $idkelasjaminan                  = $obj->idkelasjaminan;
                $status                          ='terlaksana';
                // ubah menjadi terlaksana 
                if(empty($obj->idpaketpemeriksaan))
                {
                    $this->cekmastertarif('', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan,'updateall');
                }
                else
                {
                    if($icd=='null' || $icd==null)
                    {   
                        $pesansd=false;//pesan success danger
                        $this->pemeriksaanranap_updatehasilpaket($obj->idpaketpemeriksaan,'terlaksana',$obj->idrencanamedispemeriksaan,$idrencanamedishasilpemeriksaan,$pesansd);
                    }
                }
            }
    }
    
    public function pemeriksaanranap_batalselesai()
    {
        $id = $this->input->post('i');
        $data = $this->db->query("select idpemeriksaan, norm from rs_pemeriksaan where idpendaftaran='$id'")->row_array();
        $table = 'rs_pemeriksaan';
        $status = $this->input->post('status');
        $this->mgenerikbap->setTable($table);
        if($status=='selesai')
        {
            $arrMsg = $this->db->update($table,['status'=>$status],['idpendaftaran'=>$id]) ;
            $arrMsg = $this->db->update('person_pendaftaran',['idstatuskeluar'=>'2'],['idpendaftaran'=>$id]) ;
        }
        else if($status=='sedang ditangani')
        {
            $arrMsg = $this->db->update($table,['status'=>$status],['idpendaftaran'=>$id]) ;
            $arrMsg = $this->db->update('person_pendaftaran',['idstatuskeluar'=>'0'],['idpendaftaran'=>$id]) ;
        }
        else
        {
            
        $arrMsg = $this->db->update($table,['status'=>$status],['idpemeriksaan'=>$data['idpemeriksaan']]) ;
        $arrMsg = $this->db->update('person_pendaftaran',['idstatuskeluar'=>'3'],['idpendaftaran'=>$id]) ;
        }
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:'.$this->session->userdata('username').';status:'.$status.';idpemeriksaan:'.$data['idpemeriksaan'].';idpendaftaran:'.$id.';norm:'.$data['norm'],
            'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMsg, 'Update Success.!', 'Update Failed.!', 'js');
    }
    
    //basit, clear
    public function caripasien() // cari pasien
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $jenis   = $this->input->post('jenis');
            if ($jenis == '' || $jenis == 'bangsal')
            {
                $data    = $this->input->post('data');
                $tglawal = ifnull($this->input->post('tglawal'), date('Y/m/d'));
                $tglakhir= ifnull($this->input->post('tglakhir'), date('Y/m/d'));
                echo json_encode($this->mkombin->rsinap_getdata($data,format_date_to_sql($tglawal),format_date_to_sql($tglakhir)));
        }
            else if ($jenis == 'pasien')
            {
                $data       = $this->input->post('data');
                echo json_encode($this->mkombin->rsinap_getdata_pasien($data));
            }
            else if ($jenis == 'now')
            {
                echo json_encode($this->mkombin->rsinap_getdata_sekarang());
            }
        }
    }
    
    //mahmud, clear
    public function dt_pemeriksaanranap()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_pemeriksaanranap_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                ++$no;
                if($obj->status=='terlaksana' || $obj->status=='batal')
                {
                 $menu = '<a id="pemeriksaanranapView" nomori="'.$obj->idinap.'" alt="'.$obj->idrencanamedispemeriksaan.'" norm="'.$obj->norm.'" alt2="'.$obj->idpendaftaran.'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Hasil Periksa"><i class="fa fa-eye"></i></a> '
//                        .'<a id="pemeriksaanranapBHP" alt="'.$obj->idinap.'" norm="'.$obj->norm.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idkelas.'" alt5="'.$obj->idkelasjaminan.'" alt4="'.$obj->idrencanamedispemeriksaan.'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="BHP/Farmasi"><i class="fa fa-toggle-off"></i></a> '
                        .'<a id="pemeriksaanranapBatalterlaksana" nobaris="'.$no.'" alt2="'.$obj->idrencanamedispemeriksaan.'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Kembalikan ke Rencana" ><i class="fa  fa-times"></i></a> ';
                }
                else 
                {
                 $menu = '<a id="pemeriksaanranapPeriksa" nomori="'.$obj->idinap.'" alt="'.$obj->idrencanamedispemeriksaan.'" norm="'.$obj->norm.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idkelas.'" alt5="'.$obj->idkelasjaminan.'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="Pemeriksaan"><i class="fa fa-stethoscope"></i></a> '
//                        .'<a id="pemeriksaanranapBHP" alt="'.$obj->idinap.'" norm="'.$obj->norm.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idkelas.'" alt5="'.$obj->idkelasjaminan.'" alt4="'.$obj->idrencanamedispemeriksaan.'" class=" btn btn-success btn-xs" data-toggle="tooltip" data-original-title="BHP/Farmasi"><i class="fa fa-toggle-off"></i></a> '
                        .'<a id="pemeriksaanranapTerlaksana" nobaris="'.$no.'" alt2="'.$obj->idrencanamedispemeriksaan.'" class=" btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="Terlaksana" ><i class="fa fa-check"></i></a> '
                        .'<a id="pemeriksaanranapBatal" nobaris="'.$no.'" alt2="'.$obj->idrencanamedispemeriksaan.'" class=" btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Batal Periksa" ><i class="fa fa-minus-circle"></i></a>';
                }
                
                $row = [];
                    $row[] = $no;
                    $row[] = $obj->norm;
                    $row[] = $obj->namalengkap;
                    $row[] = $obj->nosep;
                    $row[] = $obj->waktu;
                    $row[] = $obj->namadokter;
                    $row[] = $obj->namabangsal;
                    $row[] = $obj->status;
                    $row[] = '<ol class="paddingleft15"><li>'.str_replace('<br>', '</li><li>', $obj->diagnosa).str_replace('<br>', '</li><li>', $obj->tindakan).'</li></ol>';
                    $row[] = $obj->catatan;
                    ;
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_pemeriksaanranap(),
            "recordsFiltered" => $this->mdatatable->filter_dt_pemeriksaanranap(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    
    
    //basit, clear
    public function caripasienchecklist() // cari pasien
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {  
            echo json_encode($this->mkombin->rsinap_getdata_checklist($this->input->post()));
        }
    }

    //basit, clear
    public function pemeriksaanranap_caribhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $id = $this->input->get('q');
            $this->mgenerikbap->setTable('rs_barang');
            $this->db->like("namabarang","$id");
            $idunit = $this->session->userdata('idunitterpilih');
            echo json_encode($this->mgenerikbap->select("idbarang as id, concat(ifnull(namabarang,''),' Harga@',ifnull(format(hargajual,0),0),' stok:',ifnull(format(totalstokbarangperunit(idbarang,".$idunit."),0),0)) as text")->result_array());
        }
    }
    
    public function pemeriksaanranap_diagnosa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $id = $this->input->get('q');
            $jenisicd = $this->input->get('jenisicd');
            $idinap   = $this->input->get('idinap');
            $idkelas  = $this->db->select('idkelas')->get_where('rs_inap',['idinap'=>$idinap])->row_array()['idkelas'];
            
            $data = $this->db->query("select ri.icd, ri.namaicd, ri.aliasicd from rs_icd ri join rs_mastertarif rm on rm.icd=ri.icd where ri.idjenisicd='$jenisicd' and rm.idkelas=".$idkelas." and (ri.icd like '%$id%' or ri.namaicd like '%$id%' or ri.aliasicd like '%$id%' )")->result();
            echo json_encode($data);
        }
    }
    public function pemeriksaanranap_cariicd()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $id = $this->input->get('q');
            $icd = $this->input->get('icd');
            $this->mgenerikbap->setTable('rs_icd');
            $this->db->where("idjenisicd='$icd'");
            $this->db->like("namaicd","$id");
            $this->db->or_like("icd","$id");
            $this->db->or_like("aliasicd","$id");
            echo json_encode($this->mgenerikbap->select("icd, namaicd, aliasicd")->result());
        }
    }
    
    //basit, clear
    public function pemeriksaanranap_cariicdpaket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            // $id = $this->input->get('q');
            $ji = $this->input->get('ji');
            $this->mgenerikbap->setTable('rs_paket_pemeriksaan');
            $this->db->where("idpaketpemeriksaanparent='0'");
            echo json_encode($this->mgenerikbap->select("idpaketpemeriksaan, icdpaket, namapaketpemeriksaan")->result());
        }
    }
    public function diagnosa_pilihpaket()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $id = $this->input->post('x');
            $this->mgenerikbap->setTable("rs_paket_pemeriksaan");
            $x = $this->mgenerikbap->select("idpaketpemeriksaan, namapaketpemeriksaan",['idjenisicd'=>$id,'idpaketpemeriksaanparent'=>null])->result();
            echo json_encode($x);
        }
    }
    
    //basit, clear
    public function tampilhargayangdiubah()
    {
        $id = $this->input->post('x');
        $data = $this->mgenerikbap->setTable('rs_inap_rencana_medis_hasilpemeriksaan');
        $data = $this->mgenerikbap->select('jasaoperator,nakes,jasars,bhp,akomodasi,margin,sewa',['idrencanamedishasilpemeriksaan'=>$id])->row_array();
        echo json_encode($data); 
    }
    
    //basit, clear
    public function savepaketygdiubah()
    {
        $data = [
            'jasaoperator'=>$this->input->post('jasaoperator'),
            'nakes'=>$this->input->post('nakes'),
            'jasars'=>$this->input->post('jasars'),
            'bhp'=>$this->input->post('bhp'),
            'akomodasi'=>$this->input->post('akomodasi'),
            'margin'=>$this->input->post('margin'),
            'sewa'=>$this->input->post('sewa')
        ];
        $this->mgenerikbap->setTable('rs_inap_rencana_medis_hasilpemeriksaan');
        $arrMsg = $this->mgenerikbap->update($data,$this->input->post('id'));
        pesan_success_danger($arrMsg,"Update Harga Success","Update Harga Failed","js");
    }
    public function pemeriksaanranap_caridetailpasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data['pasien'] = $this->mkombin->pemeriksaanranap_tampilpasien_byinap($this->input->post('i'));
            $data['rajal']  = $this->mkombin->get_caripemeriksaanpasien_bypendaftaran($data['pasien']['idpendaftaran']);
            $data['rencana']= $this->mkombin->rencanapemeriksaanranap($this->input->post('x'));
            echo json_encode($data);
        }
    }
    public function pemeriksaanranap_refreshbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $idinap = $this->input->post('i');
            echo json_encode($this->mkombin->pemeriksaanranap_listbhp($idinap));
        }
    }
    public function pemeriksaan_tampilpasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $norm = $this->input->post('x');
            $idrencanamedispemeriksaan = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('y'));
            $pasien = $this->mkombin->pemeriksaan_tampilpasien($norm,hilangkan_kutipganda($idrencanamedispemeriksaan));
            $periksa = $this->mkombin->get_caripemeriksaanpasien(hilangkan_kutipganda($idrencanamedispemeriksaan));
            echo json_encode(['p'=>$pasien, 'per'=>$periksa]);
        }
    }
    public function pemeriksaan_detail_tampil_pasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            echo json_encode($this->mkombin->pemeriksaandetail_tampil_pasien($this->input->post('x')));
        }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_periksa()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data = array(
                'content_view'      => 'pelayanan/v_pemeriksaanranap-periksa',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanranap',
                'active_menu_level' => ''
               );
            $data['data_paket'] = $this->db->query("select * from rs_paket_pemeriksaan rp where rp.idjenisicd!='1' and rp.idpaketpemeriksaanparent=0")->result();
            $data['data_paketvitalsign'] = $this->db->query("select * from rs_paket_pemeriksaan rp where rp.idjenisicd='1'")->result();
            $data['title_page']  = 'Pemeriksaan Rawat Inap';
            $data['mode']        = 'periksa';
            $data['script_js']   = ['riwayat_perawatanranap','js_pemeriksaanranap-cetakhasilradiologi','js_pemeriksaanranap-cetakhasillab','js_pemeriksaanranap-periksa','js_pemeriksaanranap-realstokpemberianobat','js_pemeriksaanranap-hasilradiografi','riwayat_periksa','js_riwayat_allpemeriksaan_pasien','js_asesmen_ranap'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_TEXTAREA, PLUG_DATE];
            $this->load->view('v_index', $data);
        }
    }
    
    //pemeriksaan klinik ralan
    public function pemeriksaanklinik_radiologiefilm()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data = [    
                'content_view'      => 'pelayanan/v_pemeriksaanranap_radiologiefilm',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanklinik',
                'active_menu_level' => ''
               ];
            
            $data['idefilm'] = $this->uri->segment(3);
            $data['idpendaftaran'] = $this->uri->segment(4);
            $data['idrencanamedispemeriksaan'] = $this->uri->segment(5);
            
            $pasien = $this->db->query("SELECT pp.norm, namapasien(ppas.idperson) as namapasien FROM person_pendaftaran pp 
            join person_pasien ppas on ppas.norm = pp.norm
            WHERE pp.idpendaftaran = '".$data['idpendaftaran']."'")->row_array();
            
            $data['title_page']  = 'Hasil Radiografi '.ucwords(strtolower($pasien['namapasien'])).', No.RM '.$pasien['norm'];
            $data['mode']        = 'periksa';
            $data['table_title'] = 'Periksa Pasien Ranap';
            $data['script_js']   = [];
            $data['plugins']     = [];
            $data['efilmselected'] = $this->db->get_where('rs_inap_hasilpemeriksaan_efilm',array('idpendaftaran'=>$data['idpendaftaran'],'idrencanamedispemeriksaan'=>$data['idrencanamedispemeriksaan'],'idhasilpemeriksaan_efilm'=>$data['idefilm'] ))->result_array();
            $data['efilm']   = $this->db->get_where('rs_inap_hasilpemeriksaan_efilm',array('idpendaftaran'=>$data['idpendaftaran'],'idrencanamedispemeriksaan'=>$data['idrencanamedispemeriksaan'] ))->result_array();
            $this->load->view('v_index', $data);
        }
    }
    
    //upload file ke nas storage
    public function upload_file_radiografi()
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $norm = $this->input->post('norm');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $idperencanaan = $this->input->post('idperencanaan');
        $id_efilm  = time();
        $tmpName   = $_FILES['fileEfilm']['tmp_name'];
        $extension = pathinfo($_FILES['fileEfilm']['name'], PATHINFO_EXTENSION);        
        $fileName  = $norm.'_inap_'.$idperencanaan.'_'.$id_efilm.'.'.$extension;
        $newFileName = '/radiologi/'.$fileName;
        $config = $this->ftp_upload();

        $upload = $this->ftp->connect($config);
        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
        $upload = $this->ftp->close();
        
        //simpan file
        if($upload)
        {
            $data['idhasilpemeriksaan_efilm'] = $id_efilm;
            $data['idpendaftaran'] = $idpendaftaran;
            $data['idrencanamedispemeriksaan'] = $idperencanaan;
            $data['efilm']  = $newFileName;
            $data['iduser'] = $iduser;
            $data['userip'] = ' IP:'.$_SERVER['REMOTE_ADDR'].' Client-Info:'. exec('getmac');
            $upload = $this->db->replace('rs_inap_hasilpemeriksaan_efilm',$data);
        }
        
        $uploaded =  (($upload)? ['error'=>false,'status'=>'success','message'=>'Unggah Hasil Radiografi Berhasil.'] :['error'=>true,'status'=>'danger','message'=>'Unggah Hasil Radiografi Berhasil.']);
        echo json_encode($uploaded);
    }
        
    //hapus file di nas storage
    public function delete_file_radiografi()
    {
        $id_efilm =  $this->input->post('idefilm');
        $idpendaftaran = $this->input->post('idpendaftaran');
        $idperencanaan = $this->input->post('idperencanaan');
        $fileName = $this->input->post('filename');
        
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect($config);
        $delete = $this->ftp->delete_file(FOLDERNASSIMRS.$fileName);
        $delete = $this->ftp->close();
        
        //hapus file 
        if($delete)
        {
            $where['idhasilpemeriksaan_efilm'] = $id_efilm;
            $where['idpendaftaran']  = $idpendaftaran;
            $where['idrencanamedispemeriksaan'] = $idperencanaan;
            $delete = $this->db->delete('rs_inap_hasilpemeriksaan_efilm',$where);
        }
        $deleted =  (($delete)? ['error'=>false,'status'=>'success','message'=>'Hapus Hasil Radiografi Berhasil.'] :['error'=>true,'status'=>'danger','message'=>'Hapus Hasil Radiografi Berhasil.']);
        echo json_encode($deleted);
    }
    
    //view file di dari nas storage
    public function view_file_radiografi()
    {
        $post = $this->input->post();
        $idpendaftaran  = $this->input->post('idpendaftaran');
        $idperencanaan  = $this->input->post('idperencanaan');
        $viewmode = ((isset($post['viewmode'])) ? $post['viewmode'] : '' );
        $dtefilm = $this->db->get_where('rs_inap_hasilpemeriksaan_efilm',['idpendaftaran'=>$idpendaftaran,'idrencanamedispemeriksaan'=>$idperencanaan])->result_array();
        $data = '';
        $no=0;
        if(empty($dtefilm))
        {
            $data = 'Hasil tidak ada.';
        }
        else
        {
            foreach ($dtefilm as $arr)
            {
                if($no == 2)
                {
                    $data .= '<div class="col-md-12">&nbsp;</div>';
                    $no=0;
                }
                $data .= '<div class="col-md-4">
                        <a href="'.base_url('cpelayananranap/pemeriksaanklinik_radiologiefilm/'.$arr['idhasilpemeriksaan_efilm'].'/'.$arr['idpendaftaran'].'/'.$arr['idrencanamedispemeriksaan']).'" target="_blank"><img class="img-responsive" style="max-height:195px;min-width:195px;display: inline;" src="'.URLNASSIMRS.$arr['efilm'].'" alt="Hasil Radiografi"></a>
                        </div><div class="col-md-1">
                    '.(($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI) && empty($viewmode) ) ? '
                        <a id="delete_file_radiografi" filename="'.$arr['efilm'].'" idpendaftaran="'.$arr['idpendaftaran'].'" idperencanaan="'.$arr['idrencanamedispemeriksaan'].'" idefilm="'.$arr['idhasilpemeriksaan_efilm'].'" class="btn btn-xs btn-danger" '.ql_tooltip('Hapus Hasil').'><i class="fa fa-trash"></i></a>
                    '  : '' ).'</div>';
                $no++;
            }
        }
        echo json_encode($data);
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_bhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data                = $this->setting_pemeriksaanranap();
            $data['title_page']  = 'BHP/Farmasi Rawat Inap';
            $data['mode']        = 'bhp';
            $data['script_js']   = ['bhp_ralan','js_pemeriksaanranap-bhp'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE , PLUG_TEXTAREA];
            $this->load->view('v_index', $data);
        }
    }
    
    //---------- basit, clear
    public function pemeriksaanranap_view()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data = array(
                'content_view'      => 'pelayanan/v_pemeriksaanranap-view',
                'active_menu'       => 'pelayanan',
                'active_sub_menu'   => 'pemeriksaanranap',
                'active_menu_level' => ''
               );
            $data['title_page']  = 'Hasil Pemeriksaan Rawat Inap';
            $data['mode']        = 'viewperiksa';
            $data['script_js']   = ['js_pemeriksaanranap-cetakhasillab','js_pemeriksaanranap-view','js_pemeriksaanranap-hasilradiografi','riwayat_periksa','js_riwayat_allpemeriksaan_pasien'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN , PLUG_TEXTAREA];
            $this->load->view('v_index', $data);
        }
    }
    public function pemeriksaanranap_saverujuk()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $idpemeriksaan = $this->input->post('idperiksa');
            $idjadwal      = $this->input->post('idjadwal');
            $status        = $this->input->post('status');
            $arrMsg = $this->rujuk_updatepemeriksaan($idpemeriksaan,$status);
            $arrMsg = $this->rujuk_savepemeriksaan($idpemeriksaan,$idjadwal,$status);
            pesan_success_danger($arrMsg, 'Rujuk Success.!', 'Rujuk Failed.!', 'js');   
        }
    }
    private function rujuk_updatepemeriksaan($id,$status)
    {
        $arrMsg = $this->db->update('rs_pemeriksaan',['status'=>$status],$id);
        if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Update Pemeriksaan Failed..!']);}
    }
    private function rujuk_savepemeriksaan($id,$jadwal,$status)
    {
        $pemeriksaan_sebelum = $this->db->query("select * from rs_pemeriksaan where idpemeriksaan='$id'")->row_array();
        $data = [
            'idpendaftaran'=>$pemeriksaan_sebelum['idpendaftaran'],
            'idjadwal'  =>$jadwal,
            'nopesan'  =>$pemeriksaan_sebelum['nopesan'],
            'tahunbulan' =>$pemeriksaan_sebelum['tahunbulan'],
            'norm'=>$pemeriksaan_sebelum['norm'],
            'status'=>$status,
            'noantrian'=> $this->rujuk_generatenoantrian($jadwal),
            'idpemeriksaansebelum'=>$pemeriksaan_sebelum['idpemeriksaan']
        ];
        $arrMsg = $this->rujuk_savepaketvitalsign($jadwal,$pemeriksaan_sebelum['idpendaftaran']);
        $this->mgenerikbap->setTable("rs_pemeriksaan");
        $arrMsg = $this->mgenerikbap->insert($data);
        if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Save Rujuk Pemeriksaan Failed..!']);}
    }
    private function rujuk_generatenoantrian($idjadwal)
    {
        $callid         = generaterandom(8);
        $idunit = $this->mgenerikbap->select_multitable("rj.idloket,idstasiun,idunit,namaloket","rs_jadwal,antrian_loket",['idjadwal'=>$idjadwal])->row_array();
        $this->mgenerikbap->setTable('helper_autonumber');
        $data = [
            'id'      =>'Antrian'.date('Ymd').'-'.$idunit['idstasiun'].'-'.$idunit['idloket'],
            'number'  =>null,
            'callid'  =>$callid,
            'expired' =>date('Y-m-d', strtotime("+3 months", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->insert($data);
        $noantrian = $this->mgenerikbap->select_multitable('number','helper_autonumber',['callid'=>$callid])->row_array()['number'];
        return $noantrian;
    }
    private function rujuk_savepaketvitalsign($idjadwal,$idpendaftaran)
    {
        $this->load->model('mviewql');
        $listpaket = $this->mviewql->viewpaketvitalsign($idjadwal);//liast vitalsign paket
        if(!empty($listpaket))
        {
            foreach ($listpaket as $obj) {
                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$obj->icd);
                if(empty($cekData->num_rows()))
                {
                    $data = ['idpendaftaran'=>$idpendaftaran,'icd'=>$obj->icd];
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                    if(!$arrMsg) { return json_encode(['status'=>'danger', 'message'=>'Save Paket Vitalsign Failed..!']);}
                }
            }
        }
    }
    public function pemeriksaanranap_save_periksa()//simpan pemeriksaan klinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            
            
            if ($this->input->post('simpan') == 'Simpan Data dan Set Terlaksana')
            {
                $datarimp['status']  = 'terlaksana';
            }
            $datarimp['saranradiologi']      = $this->input->post('saranradiologi');
            $datarimp['keteranganradiologi'] = $this->input->post('keteranganradiologi');
            
            $whererimp = ['idrencanamedispemeriksaan'=>$this->input->post('idrencanamedispemeriksaan')];
            //save rs_inap_rencana_medis_pemeriksaan
            $this->db->update('rs_inap_rencana_medis_pemeriksaan',$datarimp,$whererimp);
            
            //save rs_inap
            $idinap = $this->input->post('idinap');
            $dt_rsinap = [
                'catatan'=>$this->input->post('catatan'),
                'keteranganlaboratorium'=>$this->input->post('keteranganlaboratorium'),
                'keteranganobat'=>$this->input->post('keteranganobat'),
            ];
            $this->mgenerikbap->setTable('rs_inap');
            $arrMsg = $this->mgenerikbap->update($dt_rsinap,$idinap);
            pesan_success_danger($arrMsg, 'Save Pemeriksaan Success.!', $arrMsg.'Save Pemeriksaan Failed.!'); 
            redirect(base_url('cpelayananranap/'.$this->input->post('modepemeriksaan') ));
        }
    }
    
    public function simpanHasilExpertiseRanap()
    {
        $whererimp = ['idrencanamedispemeriksaan'=>$this->input->post('idrencanamedispemeriksaan')];
        $data = [
            'keteranganradiologi'=>$this->input->post('keterangan'),
            'saranradiologi'=>$this->input->post('saran')
        ];
        $arrMsg = $this->db->update('rs_inap_rencana_medis_pemeriksaan',$data,$whererimp);
        pesan_success_danger($arrMsg, 'Simpan Hasil Expertise Berhasil.', 'Simpan Hasil Expertise Gagal.', 'js');
    }
    
    public function pemeriksaanranap_save_bhp()//simpan pemeriksaan klinik
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            if ($this->input->post('simpan') == 'Simpan Data dan Set Terlaksana')
            {
                $this->db->update('rs_inap_rencana_medis_pemeriksaan',['status'=>'terlaksana'],['idrencanamedispemeriksaan'=>$this->input->post('idrencanamedispemeriksaan')]);
            }
            $dt_ranap_bhp = ['catatan'=>$this->input->post('catatan'), 'keteranganobat'=>$this->input->post('keteranganobat')];
            $arrMsg = $this->db->update('rs_inap',$dt_ranap_bhp,['idinap'=>$this->input->post('idinap')]);
            pesan_success_danger($arrMsg, 'Save BHP / Obat Success.!', $arrMsg.'Save BHP / Obat Failed.!'); 
            redirect(base_url('cpelayananranap/pemeriksaanranap'));
        }
    }
    
    private function pemeriksaanranap_save_pemeriksaan($id)
    {
        $datan = $this->db->query("select idpendaftaran, norm from rs_pemeriksaan where idpemeriksaan='$id'")->row_array();
        $datal = [
            'url'     => get_url(),
            'log'     => 'user:'.$this->session->userdata('username').';status:sedang ditangani;idpemeriksaan:'.$id.';idpendaftaran:'.$datan['idpendaftaran'].';norm:'.$datan['norm'],
            'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
        ];
        $this->mgenerikbap->setTable('login_log');
        $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        $data = ['status'=>'sedang ditangani', 'anamnesa'=>$this->input->post('anamnesa'), 'keterangan'=>$this->input->post('keterangan')];
        return $this->db->update("rs_pemeriksaan",$data, ['idpemeriksaan'=>hilangkan_kutipganda($id)]);
    }
    private function pemeriksaanranap_savediagnosa($idpendaftaran,$icd,$nilai,$istext)
    {
        for($x=0; $x < count($icd); $x++)
        {
            $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$icd[$x]);
            if($cekData->num_rows() < 0)
            {
                if($istext[$x]=='text')
                {
                    $data = ['idpendaftaran'=>$idpendaftaran, 'icd'=>$icd[$x], 'nilaitext'=>$nilai[$x]];
                }
                else
                {
                    $data = ['idpendaftaran'=>$idpendaftaran, 'icd'=>$icd[$x], 'nilai'=>$nilai[$x]];
                }
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
            }
            else
            {
                if($istext[$x]=='text')
                {
                    $data = ['nilaitext'=>$nilai[$x]];
                }
                else
                {
                    $data = ['nilai'=>$nilai[$x]];
                }
                    $arrMsg = $this->db->update("rs_inap_rencana_medis_hasilpemeriksaan",$data,['idpendaftaran'=>$idpendaftaran, 'icd'=>$icd[$x]]);
            }

        }
    }

    //mahmud, clear
    ////////////////////DIAGNOSA////////////////////
    public function inputdiagnosa_rencana_medis_hasilpemeriksaan()
    {
        
        if($this->input->post('d')=='hapus')
        {
            $this->pemeriksaan_hapusdiagnosa();
        }
        else
        {
            $icd                       = $this->input->post("a");
            $idpendaftaran             = $this->input->post("b");
            $idrencanamedispemeriksaan = $this->input->post("c");
            $idrencanamedishasilpemeriksaan = $this->input->post("e");
            $iduser                    = $this->get_iduserlogin();
            $data = [
              'idrencanamedispemeriksaan'=>$idrencanamedispemeriksaan,
              'idpendaftaran'=>$idpendaftaran,
              'icd'=>$icd,
              'statuspelaksanaan'=>'terlaksana',
              'iduser'=>$iduser
            ];
            $this->mgenerikbap->setTable('rs_inap_rencana_medis_hasilpemeriksaan');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idrencanamedishasilpemeriksaan);
            pesan_success_danger($arrMsg,"Add Diagnosa Success.","Add Diagnosa Failed.","js");
        }
        
    }
    //mahmud, clear
    public function pemeriksaan_listdiagnosa()
    {
        $data = $this->mviewql->view_hasildiagnosaranap($this->input->post('x'));
        echo json_encode($data);
    }
    //mahmud, clear
    public function pemeriksaan_hapusdiagnosa()
    {
        $x = $this->input->post("e");
        $arrMsg = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idrencanamedishasilpemeriksaan'=>$x]);
        pesan_success_danger($arrMsg,"Delete Diagnosa Success..!","Delete Diagnosa Failed..!","js");
    }
    ///////////////////////END  DIAGNOSA//////////////////
    //
    ////////////////////TINDAKAN////////////////////
    //basit, clear
    public function pemeriksaanranap_updatehasilpemeriksaan()
    {
        $idrencanamedishasilpemeriksaan  = $this->input->post("x");
        $status                          = $this->input->post("y");
        $icd                             = $this->input->post("z");
        $idrencanamedispemeriksaan       = $this->input->post("a");
        $idpendaftaran                   = $this->input->post("b");
        $idkelas                         = $this->input->post("k");
        $idkelasjaminan                  = $this->input->post("kj");
        $this->cekmastertarif('Tindakan', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan);
    }
    
    //basit, clear
    public function pemeriksaan_listtindakan()
    {
        $dt = $this->db->query("select b.idbarang, rh.idrencanamedishasilpemeriksaan, rh.idrencanamedispemeriksaan, rh.icd, rh.statuspelaksanaan, ri.namaicd, ri.aliasicd, rh.total, rh.jumlah, rh.idpegawaidokter, namadokter(rh.idpegawaidokter) as namadokter, "
                . "ifnull((select count(1) from rs_inap_rencana_medis_hasilpemeriksaan where idrencanamedispemeriksaan=rh.idrencanamedispemeriksaan and rh.icd = icd and statuspelaksanaan='rencana'), 0) as jumlahrencana, rmb.idrencanamedisbarangpemeriksaan, rmb.jumlah as jumlahbhp, rmb.harga as hargabhp, rmb.total as totalbhp, rmb.statuspelaksanaan as statuspelaksanaanbhp, b.namabarang, rmb.idsatuanpemakaian, ifnull(s.namasatuan, '') as namasatuan, "
                . "ifnull((select count(1) from rs_inap_rencana_medis_barangpemeriksaan where idrencanamedispemeriksaan=rmb.idrencanamedispemeriksaan and statuspelaksanaan='rencana'), 0) as jumlahrencanabhp "
                . "from rs_inap_rencana_medis_hasilpemeriksaan rh "
                . "join rs_icd ri on rh.icd = ri.icd "
                . "left join rs_inap_rencana_medis_barangpemeriksaan rmb on rmb.idrencanamedishasilpemeriksaan=rh.idrencanamedishasilpemeriksaan "
                . "left join rs_barang b on b.idbarang = rmb.idbarang "
                . "left join rs_satuan s on s.idsatuan = rmb.idsatuanpemakaian "
                . "where rh.idrencanamedispemeriksaan='".$this->input->post('x')."' and ri.idjenisicd='3'")->result();
        echo json_encode(['tindakan'=>$dt]);
    }
    
    //basit, clear
    public function pemeriksaan_hapustindakan()
    {
        $x = $this->input->post("x");
        $arrMsg = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idrencanamedishasilpemeriksaan'=>$x]);
        pesan_success_danger($arrMsg,"Delete Tindakan Success..!","Delete Tindakan Failed..!","js");
    }
    
    //basit, clear
    public function pemeriksaanranap_ubahjumlahtindakan()
    {
        $id = $this->input->post("id");
        $jumlah = $this->input->post("jumlah");
        $arrMsg = $this->db->update("rs_inap_rencana_medis_hasilpemeriksaan",['jumlah'=>$jumlah],['idrencanamedishasilpemeriksaan'=>$id]);
        pesan_success_danger($arrMsg,"Ubah Jumlah Tindakan Success..!","Ubah Jumlah Tindakan Failed..!","js");
    }
    ///////////////////////END  TINDAKAN//////////////////

    // mahmud, clear
    ////////////////////RADIOLOGI////////////////////
    public function pemeriksaanranap_updatehasilradiologi()
    {
        $idrencanamedishasilpemeriksaan  = $this->input->post("x");
        $status                          = $this->input->post("y");
        $icd                             = $this->input->post("z");
        $idrencanamedispemeriksaan       = $this->input->post("a");
        $idpendaftaran                   = $this->input->post("b");
        $idkelas                         = $this->input->post("k");
        $idkelasjaminan                  = $this->input->post("kj");
        $this->cekmastertarif('Radiologi', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan);
    }
    // mahmud, clear
    public function pemeriksaanranap_listradiologi()
    {
        $x = $this->input->post('x');
        $data = $this->mviewql->view_hasilradiologiranap($x);
        echo json_encode($data);
    }
    // mahmud, clear
    public function pemeriksaanranap_hapusradiologi()
    {
        $x = $this->input->post("x");
        $arrMsg = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idrencanamedishasilpemeriksaan'=>$x]);
        pesan_success_danger($arrMsg,"Hapus Tindakan Radiologi Berhasil..!","Hapus Tindakan Radiologi Gagal..!","js");
    }
    ///////////////////////END  RADIOLOGI//////////////////
    // mahmud, clear
    public function pemeriksaanranap_updatehasilpaket($y=null,$z=null,$a=null,$b=null,$pesansd=true)
    {
        $mode                           = $this->input->post("x");
        $idpaketpemeriksaan             = (($y==null)?$this->input->post("y"):$y);
        $status                         = (($z==null)?$this->input->post("z"):$z);
        $idrencanamedispemeriksaan      = (($a==null)?$this->input->post("a"):$a);
        $idrencanamedishasilpemeriksaan = (($b==null)?$this->input->post("b"):$b);
        $this->db->insert('debug',['debug'=>'idp'.$idrencanamedispemeriksaan.' idhp'.$idrencanamedishasilpemeriksaan]);
        $jenistarif = 0;
        if($status=='rencana')
        {
            $data = ['jasaoperator'=>0,'nakes'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'sewa'=>0,'subtotal'=>0,'statuspelaksanaan'=>$status];
            $this->db->where('idrencanamedishasilpemeriksaan',$idrencanamedishasilpemeriksaan);
            $this->db->update("rs_inap_rencana_medis_hasilpemeriksaan", $data);
        }
        else if($status=='terlaksana')
        {
            $tarif = $this->mgenerikbap->select_multitable("*","rs_mastertarif_paket_pemeriksaan",['idpaketpemeriksaan'=>$idpaketpemeriksaan])->row_array();
            $jenistarif = $tarif['idjenistarif'];
            $data = ['idjenistarif'=>$tarif['idjenistarif'],'jasaoperator'=>$tarif['jasaoperator'],'nakes'=>$tarif['nakes'],'jasars'=>$tarif['jasars'],'bhp'=>$tarif['bhp'],'akomodasi'=>$tarif['akomodasi'],'margin'=>$tarif['margin'],'sewa'=>$tarif['sewa'],'subtotal'=>$tarif['total'],'statuspelaksanaan'=>$status
            ];
            $this->db->where('idrencanamedishasilpemeriksaan',$idrencanamedishasilpemeriksaan);
            $arrmsg=$this->db->update("rs_inap_rencana_medis_hasilpemeriksaan", $data);
        }
        $data   = ['idjenistarif'=>$jenistarif,'statuspelaksanaan'=>$status];
        $where = ['idrencanamedispemeriksaan'=>$idrencanamedispemeriksaan,'idpaketpemeriksaanparent'=>$idpaketpemeriksaan];
        $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan',$data,$where);
        if($pesansd){pesan_success_danger($arrMsg,"Set Status ".$mode." Berhasil..!","Set Status ".$mode." Gagal..!","js");}
    }
    
    // mahmud, clear
    public function pemeriksaanranap_hapusPakettindakan()
    {   
        $where = [
            'idrencanamedispemeriksaan'=>$this->input->post('z'),
            'idpaketpemeriksaanparent'=>$this->input->post('x')
        ];
        $arrMsg  = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",$where);
        
        pesan_success_danger($arrMsg,"Hapus Paket ".$this->input->post('y')." Berhasil..!","Hapus Paket ".$this->input->post('y')." Gagal..!","js");
    }
    // mahmud, clear
    public function pemeriksaanranap_simpanHasil_icdPeriksa()
    {
        $jenisnilai = $this->input->post("w");
        $mode = $this->input->post("x");
        $idhasil = $this->input->post("y");
        $nilai = $this->input->post("z");
        if($jenisnilai!='text'){$data=['nilai'=>$nilai];}
        else{$data=['nilaitext'=>$nilai];}
        $arrMsg = $this->db->update("rs_inap_rencana_medis_hasilpemeriksaan",$data,['idrencanamedishasilpemeriksaan'=>$idhasil]);
        pesan_success_danger($arrMsg,"Set Hasil ".$mode." Berhasil..!","Set Hasil ".$mode." Gagal..!","js");
    }
    // mahmud, clear
    //////////////////////START LABORATORIUM///////////////
    public function simpan_waktupelayananranaplaborat()
    {
        $post = $this->input->post();
        $data = ['waktumulai'=>$post['waktumulai'],'waktuselesai'=>$post['waktuselesai']];
        
        if($post['mode']=='paket'){
            $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan',$data,['idrencanamedispemeriksaan'=>$post['x'],'idpaketpemeriksaan'=>$post['y']]);
        }else{
            $detail = $this->db->query("SELECT rirmh.idrencanamedishasilpemeriksaan as id FROM rs_inap_rencana_medis_hasilpemeriksaan rirmh join rs_icd ri on ri.icd=rirmh.icd where idrencanamedispemeriksaan='".$post['x']."' and ri.idjenisicd=4 and rirmh.idpaketpemeriksaanparent is null")->result_array();
            if(!empty($detail))
            {
                foreach ($detail as $arr)
                {
                    $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan rirmh',$data,['idrencanamedishasilpemeriksaan'=>$arr['id']]);
                }
            }            
        }
        pesan_success_danger($arrMsg,"Waktu Pengerjaan Berhasil Diubah.","Waktu Pengerjaan Gagal Diubah.","js");
    }
    public function pemeriksaanranap_updatehasillaboratorium()
    {
        $idrencanamedishasilpemeriksaan  = $this->input->post("x");
        $status                          = $this->input->post("y");
        $icd                             = $this->input->post("z");
        $idrencanamedispemeriksaan       = $this->input->post("a");
        $idpendaftaran                   = $this->input->post("b");
        $ispaket                         = $this->input->post("ispaket");
        $idkelas                         = $this->input->post("k");
        $idkelasjaminan                  = $this->input->post("kj");
        if($ispaket=='ispaket')
        {
            $this->cekmasterpakettarif_ranap('Paket Laboratorium',$idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran);
        }
        else
        {
            $this->cekmastertarif('Laboratorium', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan);
        }
    }
    // mahmud, clear
    public function pemeriksaan_listlaboratorium()
    {
        $idrencanamedispemeriksaan = $this->input->post('x');
        $mode = $this->input->post('m');
        $data = $this->mviewql->view_hasillaboratoriumrawatinap($idrencanamedispemeriksaan,$mode);
        echo json_encode($data);
    }
    
    //insert update get kesimpulan hasil
    public function insert_update_get_kesimpulanhasillaborat()
    {   
        $idrencanamedispemeriksaan = $this->input->post('idrencanamedispemeriksaan');
        $idpaketpemeriksaan = $this->input->post('idpaketpemeriksaan');
        $data = [
            'idrencanamedispemeriksaan'=>$idrencanamedispemeriksaan,
            'idpaketpemeriksaan'=>$idpaketpemeriksaan
        ];
        
        if($this->input->post('mode')=='getdata')
        {
            echo json_encode($this->db->select('c.idpaketpemeriksaanrule, a.*')
                    ->join('rs_paket_pemeriksaan b','b.idpaketpemeriksaan=a.idpaketpemeriksaan','left')
                    ->join('rs_paket_pemeriksaan_rule c','c.idpaketpemeriksaanrule=b.idpaketpemeriksaanrule','left')
                    ->get_where('rs_inap_keteranganhasilpemeriksaan a',['idrencanamedispemeriksaan'=>$idrencanamedispemeriksaan,'a.idpaketpemeriksaan'=>$idpaketpemeriksaan])->row_array());
        }
        else if($this->input->post('mode')=='insertdata')
        {
            $data['keteranganhasil'] = $this->input->post('kesimpulan');
            $arrMsg = $this->db->insert('rs_inap_keteranganhasilpemeriksaan',$data);
            pesan_success_danger($arrMsg, 'Simpan Kesimpulan Berhasil.', 'Simpan Kesimpulan Gagal.', 'js');
        }
        else if($this->input->post('mode')=='updatedata')
        {
            $arrMsg = $this->db->update('rs_inap_keteranganhasilpemeriksaan',['keteranganhasil' => $this->input->post('kesimpulan')],$data);
            pesan_success_danger($arrMsg, 'Update Kesimpulan Berhasil.', 'Update Kesimpulan Gagal.', 'js');
        }
    }
    // mahmud, clear
    public function pemeriksaan_hapuslaboratorium()
    {
        $x = $this->input->post("x");
        $arrMsg = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idrencanamedishasilpemeriksaan'=>$x]);
        pesan_success_danger($arrMsg,"Delete Laboratorium Success..!","Delete Laboratorium Failed..!","js");
    }
    ///////////////////////END  LABORATORIUM//////////////////
    // mahmud, clear
    ////////////////////VITALSIGN////////////////////
    public function pemeriksaanranap_updatehasilvitalsign()
    {
        $idrencanamedishasilpemeriksaan  = $this->input->post("x");
        $status                          = $this->input->post("y");
        $icd                             = $this->input->post("z");
        $idrencanamedispemeriksaan       = $this->input->post("a");
        $idpendaftaran                   = $this->input->post("b");
        $ispaket                         = $this->input->post("ispaket");
        $idkelas                         = $this->input->post("k");
        $idkelasjaminan                  = $this->input->post("kj");
        if($ispaket=='ispaket')
        {
            $this->cekmasterpakettarif_ranap('Paket Vital Sign',$idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran);
        }
        else
        {
            $this->cekmastertarif('Vital Sign', $idrencanamedishasilpemeriksaan, $status, ($status=="terlaksana" || is_null($idrencanamedishasilpemeriksaan))?$icd:-1,$idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan);
        }
    }
    // mahmud, clear
    public function pemeriksaanranap_listvitalsign()
    {
        $x = $this->input->post('x');
        $idunit = $this->db->query("select pp.idunit from rs_inap_rencana_medis_pemeriksaan rimp, rs_inap ri, person_pendaftaran pp where ri.idinap = rimp.idinap and pp.idpendaftaran = ri.idpendaftaran and rimp.idrencanamedispemeriksaan='$x'")->row_array();
        $hakaksesinput = $this->mgenerikbap->select_multitable("hakinputidjenisicd","rs_unit",array("idunit"=>$idunit['idunit']))->row_array();
        echo json_encode($this->db->query("select rpp2.idpaketpemeriksaan as idpaketpemeriksaanparent, rpp2.namapaketpemeriksaan as namapaketpemeriksaanparent ,rpp.idpaketpemeriksaan, rpp.namapaketpemeriksaan, rh.idrencanamedishasilpemeriksaan, rh.idgrup, rh.icd, rh.statuspelaksanaan, ri.namaicd, ri.aliasicd, rh.total, rh.jumlah, ifnull(ri.nilaidefault,'') as nilaidefault, ri.satuan, concat( ifnull(ri.nilaiacuanrendah,''),if(ri.nilaiacuantinggi='','','-'), ifnull(ri.nilaiacuantinggi,'')) as nilairujukan, instr('".$hakaksesinput['hakinputidjenisicd']."', ri.idjenisicd) as isbolehinput, ri.istext,  rh.nilai, rh.nilaitext, ifnull((select count(1) from rs_inap_rencana_medis_hasilpemeriksaan where idrencanamedispemeriksaan=rh.idrencanamedispemeriksaan and rh.icd = icd and statuspelaksanaan='rencana'), 0) as jumlahrencana from rs_inap_rencana_medis_hasilpemeriksaan rh left join rs_icd ri on ri.icd = rh.icd left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rh.idpaketpemeriksaan left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan=rpp.idpaketpemeriksaanparent where rh.icd = ri.icd and rh.idrencanamedispemeriksaan='$x' and ri.idjenisicd='1'")->result());
    }
    // mahmud, clear
    public function pemeriksaan_hapusvitalsign()
    {
        $x = $this->input->post("x");
        $arrMsg = $this->db->delete("rs_inap_rencana_medis_hasilpemeriksaan",['idrencanamedishasilpemeriksaan'=>$x]);
        pesan_success_danger($arrMsg,"Delete VitalSign Success..!","Delete VitalSign Failed..!","js");
    }
    ///END  VITALSIGN////////////

    /**
    * Pengurangan dan pengembalian stok ranap per unit
    * @param
    * type $irmbp      Id Rencana Medis Barang Pemeriksaan
    * type $status     Status rencana bhp
    */
    // mahmud, clear
    public function simpan_penguranganstokranap($irmbp,$status)
    {
//        $idunit = $this->session->userdata('idunitterpilih');
//        if(empty(!$idunit))
//        {
//            // ambil idinap, idbarang
//            $dtranap = $this->db->query("select a.*, b.idinap, b.idbarang from rs_inap_rencana_medis_barangpemeriksaan a, rs_inap_rencana_medis_barang b where a.idrencanamedisbarangpemeriksaan='".$irmbp."' and b.idrencanamedisbarang=a.idrencanamedisbarang")->row_array();
//            $iduserlog = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
//            
//            if($dtranap['statuspelaksanaan']=='terlaksana') // pembetulan stok jika sebelumnya terlaksana
//            {
//                $pembetulan_stok = $this->db->insert('rs_barang_distribusi',['idbarangpembelian'=>$dtranap['idbarangpembelian'],'idunit'=>$idunit,'jumlah'=>$dtranap['jumlah'],'jenisdistribusi'=>'pembetulankeluar','idpersonpetugas'=>$iduserlog,'statusdistribusi'=>'tidakdiubah']);
//                return $pembetulan_stok;
//            }
//            else
//            {
//                if($status=='terlaksana') //pengurangan stok
//                {
//                    $query = $this->db->query("select (select sum(rbs.stok) from rs_barang_stok rbs, rs_barang_pembelian rbp where rbs.stok > 0 and rbs.idunit='".$idunit."' and rbp.idbarangpembelian=rbs.idbarangpembelian and rbp.idbarang='".$dtranap['idbarang']."') stoktotal,  rbs.idbarangpembelian, rbs.stok from rs_barang_stok rbs, rs_barang_pembelian rbp where rbs.stok > 0 and rbs.idunit='".$idunit."' and rbp.idbarangpembelian=rbs.idbarangpembelian and rbp.idbarang='".$dtranap['idbarang']."'");
//                    if($query->num_rows() > 0)
//                    {
//                        $distribusi = $query->result_array();
//                        $jumlahrencana = floatval($dtranap['jumlah']);
//                        $stoktotal = floatval($distribusi[0]['stoktotal']);
//
//                        
//                    }
//                    else
//                    {
//                        echo json_encode(['status'=>'danger','message'=>'Stok Obat/BHP Belum Tersedia.']);
//                        return;              
//                    }
//                    return false;
//                }
//                return true;
//            }
//        }
//        return true;
    }
    // mahmud, clear
    ///SAVE RENCANA RANAP BHP////
    public function pemeriksaanranap_ubahstatusrencanabhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            // tambah rencana
            $i = $this->input->post('i');
            $x = $this->input->post('x');
            $idp = $this->input->post('idp');
            // update rencana
            $b = $this->input->post('b');
            $s = $this->input->post('s');
            $j = $this->input->post('j');
                if(!empty($b) && empty($i))
                {
                    return $this->update_ubahstatusrencanabhp($b,$s);
                }
                else
                {
                    return $this->insert_ubahstatusrencanabhp($i,$x,$idp);
                }
        }
    }
    
    // mahmud, clear
    private function update_ubahstatusrencanabhp($b,$s)
    {
        $idunit = $this->session->userdata('idunitterpilih');
        $isbatal= $this->db->select('statuspelaksanaan')->get_where('rs_inap_rencana_medis_barangpemeriksaan',['idrencanamedisbarangpemeriksaan'=>$b])->row_array();

        if( ($s =='terlaksana' || $s =='rencana')  and empty(!$idunit) and $isbatal['statuspelaksanaan'] != 'batal' )
        {
            $arrMsg = $this->distribusibarangobatranap($b,$s);
        }
        $arrMsg = $this->db->update("rs_inap_rencana_medis_barangpemeriksaan",['statuspelaksanaan'=>$s,'iduser'=> json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser'))) ],['idrencanamedisbarangpemeriksaan'=>$b]);
        
        pesan_success_danger($arrMsg, 'Ubah status '.$s.' BHP Berhasil.!','Ubah '.$s.' BHP Gagal.!','js');        
    }
    
    // mahmud, clear
    private function insert_ubahstatusrencanabhp($i,$x,$idp)
    {
        $arrMsg =  $this->db->insert("rs_inap_rencana_medis_barangpemeriksaan",['idrencanamedispemeriksaan'=>$i,'idbarang'=>$x,'idpendaftaran'=>$idp]);
        pesan_success_danger($arrMsg, 'Tambah rencana BHP Berhasil.!','Tambah rencana BHP Gagal.!','js');
        
    }
    
    // mahmud, clear
    public function pemeriksaanranap_updatejumlahrencanabhp()
    {
        $arrMsg =  $this->db->update("rs_inap_rencana_medis_barangpemeriksaan",['jumlah'=>$this->input->post('z')],['idrencanamedisbarangpemeriksaan'=>$this->input->post('y')]);pesan_success_danger($arrMsg, 'Ubah pemakaian rencana BHP Berhasil.!','Ubah pemakaian rencana BHP Gagal.!','js');
    }
    
//    // mahmud, clear
//    ///RANAP RENCANA BHP
//    public function pemeriksaanranap_carirencanabhp()
//    {
//        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
//        {
//            $data = $this->db->query("select namabarang, hargajual, ifnull(totalstokbarang(rb.idbarang),0) stok "
//                    . "FROM rs_inap_rencana_medis_barangpemeriksaan rimb "
//                    . "left join rs_barang rb on rb.idbarang = rimb.idbarang "
//                    . "WHERE  rimb.idrencanamedispemeriksaan='".$this->input->post('idrencanamedispemeriksaan')."'")->result();
//            echo json_encode($data);
//        }
//    }
    
    // mahmud, clear
    public function pemeriksaanranap_hapusrencanabhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $x = $this->input->post("x");
            $arrMsg = $this->db->delete("rs_inap_rencana_medis_barangpemeriksaan",['idrencanamedisbarangpemeriksaan'=>$x]);
            pesan_success_danger($arrMsg,"Hapus rencana Bhp/Obat berhasil..!","Hapus rencana Bhp/Obat gagal..!","js");
        }
    }
    // mahmud, clear
    public function pemeriksaanranap_listrencanabhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data = $this->db->query("select b.idbarang,rmb.harga, cast(rmb.total as INT) as total, rmb.idrencanamedisbarangpemeriksaan, rmb.jumlah, rmb.statuspelaksanaan, b.namabarang, rmb.idsatuanpemakaian, ifnull(s.namasatuan, '') as namasatuan "
                    . "FROM rs_inap_rencana_medis_barangpemeriksaan rmb "
                    . "left join rs_barang b on b.idbarang = rmb.idbarang "
                    . "left join rs_satuan s on s.idsatuan = rmb.idsatuanpemakaian "
                    . "where rmb.idrencanamedispemeriksaan='".$this->input->post("i")."' and isnull(rmb.idrencanamedishasilpemeriksaan)")->result();
            echo json_encode($data);
        }
    }
    // mahmud, clear
    private function cekmasterpakettarif_ranap($pesan, $idrencanamedishasilpemeriksaan, $status, $idpaketpemeriksaan, $idrencanamedispemeriksaan, $idpendaftaran,$updateall=null,$showmsg=null)
    {
        if(empty($idrencanamedishasilpemeriksaan))
        {
            
            $idjenistarif = 0;$jasaoperator = 0;$nakes=0;$jasars = 0;$bhp = 0;$akomodasi = 0;$margin = 0;$sewa=0;$total = 0;
            $data = [
                'idjenistarif'=>$idjenistarif,
                'jasaoperator'=>$jasaoperator,
                'nakes'=>$nakes,
                'jasars'=>$jasars,
                'bhp'=>$bhp,
                'akomodasi'=>$akomodasi,
                'margin'=>$margin,
                'sewa'=>$sewa,
                'subtotal'=>$total,
                'statuspelaksanaan'=>$status
            ];
            $idgrup = $this->db->query("select count(1) as jumlah from rs_inap_rencana_medis_hasilpemeriksaan where  idpaketpemeriksaan='".$idpaketpemeriksaan."' and idrencanamedispemeriksaan='$idrencanamedispemeriksaan' and idpendaftaran='$idpendaftaran'")->row_array()['jumlah'] + 1;
            if ($idpaketpemeriksaan != -1) // tambahkan idpaketpemeriksaan jika ada
            {
                $data['idpaketpemeriksaan'] = $idpaketpemeriksaan;                
                $data['idpaketpemeriksaanparent'] = $idpaketpemeriksaan;
                $data['idgrup'] = $idgrup;
            }
            $cekrencanapaket = $this->db->query("select * from rs_inap_rencana_medis_hasilpemeriksaan where idpaketpemeriksaan='$idpaketpemeriksaan' and idrencanamedispemeriksaan='$idrencanamedispemeriksaan' and idpendaftaran='$idpendaftaran'");
            if (is_null($idrencanamedishasilpemeriksaan) && empty($cekrencanapaket->num_rows()))//jika rencana belum ada
            {
                $dtpaket = $this->db->query("select * from rs_paket_pemeriksaan_detail where idpaketpemeriksaan ='$idpaketpemeriksaan'");
                if($dtpaket->num_rows()>0)
                {
                    $data["idrencanamedispemeriksaan"]  = $idrencanamedispemeriksaan;
                    $data["idpendaftaran"]              = $idpendaftaran;
                    //SIMPAN DETAIL PAKET PEMERIKSAAN
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data);
                    
                    // simpan perulangan data paket
                    $data=[];
                    $listpaket = $dtpaket->result();
                    foreach ($listpaket as $obj) {
                        $data = ['idjenistarif'=>$idjenistarif,'jasaoperator'=>0,'nakes'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'sewa'=>0,'subtotal'=>0,'statuspelaksanaan'=>$status];
                        $data["icd"] = $obj->icd;
                        $data['idpaketpemeriksaan'] = $obj->idpaketpemeriksaan;
                        $data['idpaketpemeriksaanparent'] = $idpaketpemeriksaan;
                        $data["idrencanamedispemeriksaan"]  = $idrencanamedispemeriksaan;
                        $data["idpendaftaran"]              = $idpendaftaran;
                        $data['idgrup'] = $idgrup;
                        $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data);
                    }
                    if($showmsg==null)
                    {   
                    pesan_success_danger($arrMsg,((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Berhasil..!",((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Gagal..!","js");
                    }
                }
                else
                {
                    $dtpaketparent = $this->db->query("select * from rs_paket_pemeriksaan where idpaketpemeriksaanparent ='$idpaketpemeriksaan'");
                    if($dtpaketparent->num_rows()>0)
                    {   
                        $data["idrencanamedispemeriksaan"]  = $idrencanamedispemeriksaan;
                        $data["idpendaftaran"]              = $idpendaftaran;
                        $listpaketparent=$dtpaketparent->result();
                        $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data);
                        //PERULANGAN PAKET PARENT
                        foreach ($listpaketparent as $obj)
                        {   
                            // simpan perulangan data paket
                            $data=[];
                            $listpaket = $listpaket=$this->db->query("select * from rs_paket_pemeriksaan_detail where idpaketpemeriksaan ='$obj->idpaketpemeriksaan'")->result();
                            foreach ($listpaket as $obj) {
                                $data = ['idjenistarif'=>$idjenistarif,'jasaoperator'=>0,'nakes'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'sewa'=>0,'subtotal'=>0,'statuspelaksanaan'=>$status];
                                $data["icd"] = $obj->icd;
                                $data['idpaketpemeriksaan'] = $obj->idpaketpemeriksaan;
                                $data['idpaketpemeriksaanparent'] = $idpaketpemeriksaan;
                                $data['idgrup'] = $idgrup;
                                $data["idrencanamedispemeriksaan"]  = $idrencanamedispemeriksaan;
                                $data["idpendaftaran"]              = $idpendaftaran;
                                $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data);
                            }
                            
                        }
                        if($showmsg==null)
                        { 
                            pesan_success_danger($arrMsg,((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Berhasil..!",((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Gagal..!","js");
                        }
                    }
                    else
                    {
                        pesan_danger($pesan." Tidak Ada..!","js");
                    }
                    
                }
            }
            else
            {
                pesan_danger($pesan." Sudah Ada..!","js");
            }
        }    
        else
        {
            $data = ['statuspelaksanaan'=>$status];
            $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data, (($updateall==null)? ['idrencanamedishasilpemeriksaan'=>$idrencanamedishasilpemeriksaan] : ['idrencanamedishasilpemeriksaan'=>$idrencanamedishasilpemeriksaan,'statuspelaksanaan'=>'rencana']));
            if($updateall==null)
            {
            pesan_success_danger($arrMsg,((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Berhasil..!",((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Gagal..!","js");
            }
        }
    }
    /////////////// HAPUS PAKET ICD //////////
    public function pemeriksaan_hapuspaketpemeriksaan()
    {
        $x = $this->input->post('x');
        $y = $this->input->post('y');
        $result = $this->db->query("delete from rs_inap_rencana_medis_hasilpemeriksaan where idgrup='$x' and idpendaftaran='$y'");
        pesan_success_danger($result,"Hapus Paket Berhasil..!","Hapus Paket Gagal..!","js");
    }
    //////////CEK TARIF PER PAKET ICD 
    private function cekmasterpakettarif($pesan,$idpendaftaran,$idpaket,$idpemeriksaan)
    {
        //siapkan data master tarif sesuai paket
        $dtmstrf = $this->mgenerikbap->select_multitable("*","rs_mastertarif_paket_pemeriksaan",['idpaketpemeriksaan'=>$idpaket])->row_array();
        //save paket
        if(empty($dtmstrf))
        {
            $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($idpaket);
            if(!empty($listpaket))
            {
                foreach ($listpaket as $key) {
                    $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$key->icd);
                    if(empty($cekData->num_rows()))
                    {
                        $data = ['idpemeriksaan'=>$idpemeriksaan, 'idpendaftaran'=>$idpendaftaran,'icd'=>$key->icd,'idpaketpemeriksaan'=>$key->idpaketpemeriksaan,'idgrup'=>$idpaket,'idjenistarif'=>0,'jasaoperator'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'total'=>0];
                        $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                    }
                    else
                    {
                        return pesan_success_danger(0,"", $pesan." Sudah Ada..!","js");
                    }
                }
                $data = ['idpemeriksaan'=>$idpemeriksaan, 'idpendaftaran'=>$idpendaftaran,'idpaketpemeriksaan'=>$idpaket,'idgrup'=>$idpaket,'idjenistarif'=>0,'jasaoperator'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'total'=>0];
                $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                pesan_success_danger($arrMsg,"Add ".$pesan." Success..!","Add ".$pesan." Failed..!","js");
            }
            else 
            {
                $listpaketparent = $this->mkombin->diagnosa_getpaketdiagnosaparent($idpaket);
                if(!empty($listpaketparent))
                {
                    foreach ($listpaketparent as $key) 
                    {
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($key->idpaketpemeriksaan);
                        if(!empty($listpaket))
                        {
                            foreach ($listpaket as $key) {
                                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$key->icd);
                                if(empty($cekData->num_rows()))
                                {
                                    $data = ['idpemeriksaan'=>$idpemeriksaan, 'idpendaftaran'=>$idpendaftaran,'icd'=>$key->icd,'idpaketpemeriksaan'=>$key->idpaketpemeriksaan,'idgrup'=>$idpaket,'idjenistarif'=>0,'jasaoperator'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'total'=>0];
                                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                                }
                            }
                        }
                    }
                    $data = ['idpemeriksaan'=>$idpemeriksaan, 'idpendaftaran'=>$idpendaftaran,'idpaketpemeriksaan'=>$idpaket,'idgrup'=>$idpaket,'idjenistarif'=>0,'jasaoperator'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'total'=>0];
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                    return pesan_success_danger($arrMsg,"Add ".$pesan." Success..!","Add ".$pesan." Failed..!","js");
                }
                else
                {
                    pesan_danger($pesan." Tidak Ada..!","js");
                }
            }            
        }
        else
        {
            pesan_danger($pesan." Tidak Ada","js");
        }
    }
    //////////CEK TARIF PER ICD 
    //basit, clear
    /**
     * 
     * @param type $pesan
     * @param type $idrencanamedishasilpemeriksaan  jika null maka mode tambah
     * @param type $status
     * @param type $icd, jika -1, maka semua tarif dinolkan
     */
    private function cekmastertarif($pesan, $idrencanamedishasilpemeriksaan, $status, $icd, $idrencanamedispemeriksaan, $idpendaftaran, $idkelas, $idkelasjaminan,$updateall=null)
    {
        $dtmstrf = $this->mgenerikbap->select_multitable("*","rs_mastertarif",['icd'=>$icd, 'idkelas' => $idkelas])->row_array();
        if(empty($dtmstrf)) //jika tafir tidak ada
        {
            $idjenistarif = 0;$jasaoperator = 0;$nakes=0;$jasars = 0;$bhp = 0;$akomodasi = 0;$margin = 0;$sewa=0;$total = 0; $potongan = 0;
        }
        else //selain itu
        {
            $idjenistarif = $dtmstrf['idjenistarif']; $jasaoperator = $dtmstrf['jasaoperator']; $jasars = $dtmstrf['jasars']; $nakes = $dtmstrf['nakes']; $bhp = $dtmstrf['bhp']; $akomodasi = $dtmstrf['akomodasi']; $margin = $dtmstrf['margin']; $sewa = $dtmstrf['sewa']; $total = $dtmstrf['total'];
            //jika kelas jaminan lebih besar dari kelas, maka potongannya sama dengan kelas jaminan
            //selain itu jika kelas jaminan = 0, maka potongan 0
            //selain itu potongannya sama dengan kelas
            if ($idkelasjaminan > $idkelas)
            {
                $dtmstrf2   = $this->mgenerikbap->select_multitable("*","rs_mastertarif",['icd'=>$icd, 'idkelas' => $idkelasjaminan])->row_array();
                $potongan   = (empty($dtmstrf2)) ? 0 : $dtmstrf2['total'];
            }
            else if ($idkelasjaminan == 0)
            {
                $potongan = 0;
            }
            else
            {
                $potongan = $total;
            }
        }
        $data = ['idjenistarif'=>$idjenistarif,'jasaoperator'=>$jasaoperator,'nakes'=>$nakes,'jasars'=>$jasars,'bhp'=>$bhp,'akomodasi'=>$akomodasi,'margin'=>$margin,'sewa'=>$sewa,'subtotal'=>$total,'subtotalpotongan'=>$potongan,'statuspelaksanaan'=>$status,'iduser'=>json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')))];
        if ($icd != -1) 
        {
            $data['icd'] = $icd;
        }
        if (is_null($idrencanamedishasilpemeriksaan))
        {
            $data["idrencanamedispemeriksaan"]  = $idrencanamedispemeriksaan;
            $data["idpendaftaran"]              = $idpendaftaran;
            $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan", $data);
        }
        else
        {
            $arrMsg = $this->db->update("rs_inap_rencana_medis_hasilpemeriksaan", $data, (($updateall==null)? ["idrencanamedishasilpemeriksaan"=>$idrencanamedishasilpemeriksaan] : ["idrencanamedishasilpemeriksaan"=>$idrencanamedishasilpemeriksaan,'statuspelaksanaan'=>'rencana'] ));
        }
        if($updateall==null)
        {   
        pesan_success_danger($arrMsg,((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Berhasil..!",((is_null($idrencanamedishasilpemeriksaan))?"Tambah":"Set Status")." $pesan [$status] Gagal..!","js");
        }
    }
    public function diagnosa_addsinglediagnosa()//tambah single diagnosa di hasil pemeriksaan
    {
        $icd = $this->input->post("x");
        $idpendaftaran = $this->input->post("y");
        $dtmstrf = $this->mgenerikbap->select_multitable("*","rs_mastertarif",['icd'=>$icd])->row_array();
        if(empty($dtmstrf)) //jika tafir tidak ada
        {
            $idjenistarif = 0;
            $jasaoperator = 0;
            $nakes = 0;
            $jasars = 0;
            $bhp = 0;
            $akomodasi = 0;
            $margin = 0;
            $sewa = 0;
            $total = 0;
        }
        else //selain itu
        {
            $idjenistarif = $dtmstrf['idjenistarif'];
            $jasaoperator = $dtmstrf['jasaoperator'];
            $nakes = $dtmstrf['nakes'];
            $jasars = $dtmstrf['jasars'];
            $bhp = $dtmstrf['bhp'];
            $akomodasi = $dtmstrf['akomodasi'];
            $margin = $dtmstrf['margin'];
            $sewa = $dtmstrf['sewa'];
            $total = $dtmstrf['total'];
        }
        //save data
        $data = ['idpendaftaran'=>$idpendaftaran, 'icd'=>$icd,'idjenistarif'=>$idjenistarif,'jasaoperator'=>$jasaoperator,'nakes'=>$nakes,'jasars'=>$jasars,'bhp'=>$bhp,'akomodasi'=>$akomodasi,'margin'=>$margin,'sewa'=>$sewa,'total'=>$total];

        $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$icd);
        if($cekData->num_rows() > 0)
        {
            pesan_success_danger(0,"","Diagnosa Tidak Dapat Ditambahkan..!","js");
        }
        else
        {
            $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
            pesan_success_danger($arrMsg,"Add Diagnosa Success..!","Add Diagnosa Failed..!","js");
        }
        
    }

    public function diagnosa_addpaketdiagnosa()
    {
        $idpaket = $this->input->post("x");
        $idpendaftaran = $this->input->post("y");
        //siapkan data master tarif sesuai paket
        $dtmstrf = $this->mgenerikbap->select_multitable("*","rs_mastertarif_paket_pemeriksaan",['idpaketpemeriksaan'=>$idpaket])->row_array();
        if(empty($dtmstrf))
        {
            pesan_danger("Tarif Diagnosa Belum Ditambahkan..!","js");
        }
        else
        {
            $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($idpaket);
            if(!empty($listpaket))
            {
                foreach ($listpaket as $key) {
                    $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$key->icd);
                    if(empty($cekData->num_rows()))
                    {
                        $data = ['idpendaftaran'=>$idpendaftaran,'icd'=>$key->icd,'idpaketpemeriksaan'=>$key->idpaketpemeriksaan,'idgrup'=>$idpaket,'idjenistarif'=>$dtmstrf['idjenistarif'],'jasaoperator'=>0,'nakes'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'sewa'=>0,'total'=>0];
                        $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                    }
                    else
                    {
                        pesan_success_danger(0,"","Paket Sudah Ada..!","js");
                    }
                }
                $data = ['idpendaftaran'=>$idpendaftaran,'idpaketpemeriksaan'=>$dtmstrf['idpaketpemeriksaan'],'idgrup'=>$idpaket,'idjenistarif'=>$dtmstrf['idjenistarif'],'jasaoperator'=>$dtmstrf['jasaoperator'],'nakes'=>$dtmstrf['nakes'],'jasars'=>$dtmstrf['jasars'],'bhp'=>$dtmstrf['bhp'],'akomodasi'=>$dtmstrf['akomodasi'],'margin'=>$dtmstrf['margin'],'sewa'=>$dtmstrf['sewa'],'total'=>$dtmstrf['total']];
                        $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                pesan_success_danger($arrMsg,"Add Diagnosa Success..!","Add Diagnosa Failed..!","js");
            }
            else
            {
                $listpaketparent = $this->mkombin->diagnosa_getpaketdiagnosaparent($idpaket);
                if(!empty($listpaketparent))
                {
                    foreach ($listpaketparent as $key) 
                    {
                        $listpaket = $this->mkombin->diagnosa_getpaketdiagnosa($key->idpaketpemeriksaan);
                        if(!empty($listpaket))
                        {
                            foreach ($listpaket as $key) {
                                $cekData = $this->mkombin->diagnosa_cekdiagnosa($idpendaftaran,$key->icd);
                                if(empty($cekData->num_rows()))
                                {
                                    $data = ['idpendaftaran'=>$idpendaftaran, 'icd'=>$key->icd,'idpaketpemeriksaan'=>$key->idpaketpemeriksaan,'idgrup'=>$idpaket,'idjenistarif'=>$dtmstrf['idjenistarif'],'jasaoperator'=>0,'nakes'=>0,'jasars'=>0,'bhp'=>0,'akomodasi'=>0,'margin'=>0,'sewa'=>0,'total'=>0];
                                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                                }
                                else
                                {
                                    pesan_success_danger(0,"","Paket Sudah Ada..!","js");
                                    return true;
                                }
                            }
                        }
                        else
                        {   
                            pesan_success_danger(0,"","Paket Diagnosa Tidakada..!","js");
                            return true;
                        }
                    }

                    $data = ['idpendaftaran'=>$idpendaftaran, 'idpaketpemeriksaan'=>$dtmstrf['idpaketpemeriksaan'],'idgrup'=>$idpaket,'idjenistarif'=>$dtmstrf['idjenistarif'],'jasaoperator'=>$dtmstrf['jasaoperator'],'nakes'=>$dtmstrf['nakes'],'jasars'=>$dtmstrf['jasars'],'bhp'=>$dtmstrf['bhp'],'akomodasi'=>$dtmstrf['akomodasi'],'margin'=>$dtmstrf['margin'],'sewa'=>$dtmstrf['sewa'],'total'=>$dtmstrf['total']];
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_hasilpemeriksaan",$data);
                    pesan_success_danger($arrMsg,"Add Diagnosa Success..!","Add Diagnosa Failed..!","js");
                        // echo json_encode($listpaket);
                }
                else
                {
                    pesan_success_danger(0,"","Paket Diagnosa Tidakada..!","js");
                    return true;
                }
            }
        }
    }
    public function ubah_harga_paket()
    {
        $harga = $this->input->post('x');
        $y = $this->input->post('y');
        $arrMsg = $this->db->update('rs_inap_rencana_medis_hasilpemeriksaan',['total'=>$harga],['idrencanamedishasilpemeriksaan'=>$y]);
        pesan_success_danger($arrMsg,'Update Harga Paket Success..!','Update Harga Paket Failed..!','js');
    }
    public function delete_hasil_periksa()
    {
        $idpendaftaran = $this->input->post('x');
        $icd = $this->input->post('y');
        $arrMsg = $this->db->delete('rs_inap_rencana_medis_hasilpemeriksaan',['icd'=>$icd, 'idpendaftaran'=>$idpendaftaran]);
        pesan_success_danger($arrMsg,'Delete Diagnosa Success..!','Delete Diagnosa Failed..!','js');
    }
    public function delete_paket_periksa()
    {
        $idpendaftaran = $this->input->post('x');
        $idgrup = $this->input->post('y');
        $arrMsg = $this->db->delete('rs_inap_rencana_medis_hasilpemeriksaan',['idgrup'=>$idgrup, 'idpendaftaran'=>$idpendaftaran]);
        pesan_success_danger($arrMsg,'Delete Diagnosa Success..!','Delete Diagnosa Failed..!','js');
    }
    /////////////SAVEBHP//////////
    public function pemeriksaan_inputbhp()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $idinap = $this->input->post('i');
            $idpendaftaran = $this->input->post('ip');
            $kondisipasien = $this->input->post('kondisi');
            $idbarang = $this->input->post('x');//idbarang
            $detail_bhp = $this->mgenerikbap->select_multitable("*","rs_barang",['idbarang'=>$idbarang])->row_array();
            if(!empty($detail_bhp))
            {
                if($kondisipasien=='pulang') // jika kondisi pasien sudah tidak dirawat
                {
                    $this->inputobatpulang($idinap,$idbarang,$idpendaftaran,$detail_bhp,1,'input');
                }
                else 
                {
                    $data = ['idinap'=>$idinap, 'idbarang'=>$idbarang,'jumlahperencanaan'=>'1','jumlahpemakaian'=>'1','idjenistarif'=>$detail_bhp['idjenistarif'],'harga'=>$detail_bhp['hargajual'],'kekuatan'=>$detail_bhp['kekuatan'],'idpendaftaran'=>$idpendaftaran];
                    $arrMsg = $this->db->insert("rs_inap_rencana_medis_barang",$data);
                    // input pemesanan barang
//                    $arrMsg = $this->barangpemesanan_inputbhp($idinap,$idbarang,'pesanobatranap',1);
                    pesan_success_danger($arrMsg, 'Add BHP Success.!','Add BHP Failed.!','js');   
                }              
            }
            else
            {
                pesan_danger("BHP Tidak Ditemukan..!","js");
            }
        }
    }
    /**
     * Iniput Obat Pulang
     * @param type $idinap ID Inap
     * @param type $idbarang ID Barang
     * @param type $idpendaftaran ID Pendaftaran
     * @param type $detail_bhp Detail Obat/BHP
     * @param type $jumlahpemakaian Jumlah Pemakaian
     * @param type $mode Mode Entri
     */
    private function inputobatpulang($idinap,$idbarang,$idpendaftaran,$detail_bhp,$jumlahpemakaian,$mode)
    {
        $data = [
            'idpendaftaran'=>$idpendaftaran,
            'idbarang'=>$detail_bhp['idbarang'],
            'jumlahpemakaian'=>$jumlahpemakaian,
            'idjenistarif'=>$detail_bhp['idjenistarif'],
            'harga'=>$detail_bhp['hargajual'],
            'kekuatan'=>$detail_bhp['kekuatan'],
            'keluhan'=>$detail_bhp['keluhan'],
            'jenisrawat'=>'ranap',
            'jenisinput'=>(($mode=='input') ? 'inputbaru' : 'sisaranap' )
        ];
        
        $arrMsg =  $this->db->insert("rs_barangpemeriksaan",$data);
        //jika mode input
        if($mode=='input'){
            // input pemesanan barang
//            $arrMsg = $this->barangpemesanan_inputbhp($idinap,$idbarang,'pesanobatpulang',1);
            pesan_success_danger($arrMsg, 'Add BHP Success.!','Add BHP Failed.!','js'); 
        }
    }
    //////////////SETTING BHP/////////////////
    //basit, clear
    public function pemeriksaan_settbhp()
    {
        $id = $this->input->post('x');
        $y = $this->input->post('y');
        $type = $this->input->post('type');
        $d = $this->input->post('d');
        $jumlahlama = $this->input->post('jl');        
        $idinap = $this->input->post('idinap');
        $idbarang = $this->input->post('idbarang');
        $idpendaftaran = $this->db->query("select idpendaftaran from rs_inap_rencana_medis_barang where idrencanamedisbarang=$id")->row_array()["idpendaftaran"];
        if($type=='grup')
        {
            $data = ['grup'=>$d,'jenisgrup'=>($d==0)?'bukan racikan':'komponen racikan'];
        }
        else if($type=='harga')
        {
            // $harga = ($d * (30/100)) + $d;
            $data = ['harga'=> $harga ];
            $this->db->update('rs_barang',['hargabeli'=>$d,'hargajual'=>$harga],['idbarang'=>$y]);
        }
        else if($type=='del')
        {
            $this->mgenerikbap->setTable('rs_inap_rencana_medis_barang');
            $arrMsg = $this->mgenerikbap->delete($id);
            return pesan_success_danger($arrMsg,'Delete BHP Success..!','Delete BHP Failed..!','js');
        }
        else if($type=='kekuatan')
        {
            // $harga = ($d * (30/100)) + $d;
            $data = ['kekuatan'=> $d];
        }
        else if($type=='pesan')
        {
            // $total = $d* ($y * (30/100)) + $y;;
            $data = ['jumlahpesan'=> $d + $jumlahlama ];
            // update or insert pemesanan barang
//            $this->barangpemesanan_inputbhp($idinap,$idbarang,'pesanobatranap',$d);
        }
        else if($type=='jumlah')
        {
            // $total = $d* ($y * (30/100)) + $y;;
            $data = ['jumlahpemakaian'=>$d];
        }
        else if($type=='catatan')
        {
            // $total = $d* ($y * (30/100)) + $y;;
            $data = ['catatan'=>$d];
        }
        else if($type=='retur')
        {
            // $harga = ($d * (30/100)) + $d;
            $data = ['waktupengembalian'=> date('Y-m-d H:i:s')];

//            $cek = $this->db->query('select rmb.jumlahsisa from rs_inap_rencana_medis_barang rmb where idrencanamedisbarang='.$id)->row_array();
            // update or insert pemesanan barang
//            $this->barangpemesanan_inputbhp($idinap,$idbarang,'returobatranap',$cek['jumlahsisa']);
        }
        else if($type=='sisauntukobatpulang')
        {
            $cek = $this->db->query('select rmb.jumlahpesan, rmb.jumlahsisa from rs_inap_rencana_medis_barang rmb where idrencanamedisbarang='.$id)->row_array();
            //jumlahpemakaian ditambah jumlahsisa
            $data = ['jumlahpesan'=> $cek['jumlahpesan'] - $cek['jumlahsisa'], 'catatan'=>'sisa untuk obat pulang'.$cek['jumlahsisa']];
            $detail_bhp = $this->mgenerikbap->select_multitable("*","rs_barang",['idbarang'=>$idbarang])->row_array();
            $this->inputobatpulang($idinap,$idbarang,$idpendaftaran,$detail_bhp,$cek['jumlahsisa'],'sisa');
        }
        $arrMsg = $this->db->update('rs_inap_rencana_medis_barang',$data,['idrencanamedisbarang'=>$id]);
        if($type=='grup')
        {
            if ($d>0)
            {
                $this->db->query("insert into rs_inap_rencana_medis_barang (idpendaftaran, idbarang, idinap, idjenistarif, grup, jenisgrup, kekuatan) SELECT idpendaftaran, 0 as idbarang, idinap, 0 as idjenistarif, grup, 'racikan' as jenisgrup, 1 as kekuatan FROM `rs_inap_rencana_medis_barang` irmb where idrencanamedisbarang=$id and ifnull((select false from rs_inap_rencana_medis_barang where idpendaftaran=irmb.idpendaftaran and idbarang=0 and grup=irmb.grup and jenisgrup='racikan' limit 1), true)"); 
            }
            $todelete = $this->db->query("select group_concat(distinct grup) as grup from rs_inap_rencana_medis_barang where idpendaftaran=$idpendaftaran and jenisgrup!='racikan'")->row_array();
            $this->db->query("delete from rs_inap_rencana_medis_barang where grup not in (".$todelete["grup"].") and grup>0 and jenisgrup='racikan' and idpendaftaran=$idpendaftaran");
        }
        pesan_success_danger($arrMsg,'Update BHP Success..!','Update BHP Failed..!','js');
    }
    
    // pemeriksan cetak aturan pakai obat
    public function pemeriksaan_cetak_aturanpakai()
    {
        echo json_encode($this->db->query("select rb.grup, DATE_FORMAT(waktuinput, '%d/%m/%Y') as tglperiksa, rb.penggunaan, concat(ifnull(ppp.identitas,''),' ',ppp.namalengkap)as namalengkap, p.norm, (select b.namabarang from rs_barang b WHERE b.idbarang=rb.idbarang) as namaobat, concat((select max(jumlah) from (SELECT date(waktu) as waktu, bp.idrencanamedisbarang, count(1) as jumlah FROM rs_inap_rencana_medis_barangpemeriksaan bp join rs_inap_rencana_medis_pemeriksaan p on p.idrencanamedispemeriksaan=bp.idrencanamedispemeriksaan where bp.idrencanamedisbarang='".$this->input->post('idb')."' group by date(waktu), idrencanamedisbarang) x), ' x sehari ', round(rbp.jumlah), ' ', s.namasatuan) as aturanpakai from rs_inap_rencana_medis_barang rb, rs_inap_rencana_medis_barangpemeriksaan rbp, person_pendaftaran p, person_pasien pp, person_person ppp, rs_satuan s where pp.norm = p.norm and ppp.idperson = pp.idperson and rb.idpendaftaran = p.idpendaftaran and rb.idrencanamedisbarang='".$this->input->post('idb')."' and s.idsatuan=rb.idsatuanpemakaian and rbp.idrencanamedisbarang=rb.idrencanamedisbarang order by idrencanamedispemeriksaan limit 1")->row_array());
    }
    
    
    
    
    
    // batal semua rencana bhp atau pemeriksaan
    public function batalrencana_periksaranap()
    {
        $idrencana = $this->input->post('i');
        $mode      = $this->input->post('mode');
        $pesan     = $this->input->post('pesan');
        $idinap = $this->input->post('idinap');
//        $idrencanamedispemeriksaan = $this->input->post('k');
        $ispaket   = $this->input->post('ispaket');
        if($mode=='rencanabhp' || $mode=='rencanabhptindakan')
        {
            $idinap = $this->input->post('idinap');
            $arrMsg = $this->db->query("UPDATE rs_inap_rencana_medis_barangpemeriksaan JOIN rs_inap_rencana_medis_pemeriksaan ON rs_inap_rencana_medis_barangpemeriksaan.idrencanamedispemeriksaan=rs_inap_rencana_medis_pemeriksaan.idrencanamedispemeriksaan JOIN rs_inap ON rs_inap_rencana_medis_pemeriksaan.idinap=rs_inap.idinap SET `rs_inap_rencana_medis_barangpemeriksaan`.`statuspelaksanaan` = 'batal' WHERE rs_inap_rencana_medis_pemeriksaan.idinap = '$idinap' AND `idrencanamedisbarang` = '$idrencana' AND `statuspelaksanaan` != 'terlaksana'");
        }
        else
        {   

                $sql = $this->db->query("select idpaketpemeriksaan from rs_paket_pemeriksaan where idpaketpemeriksaanparent='".$idrencana."'");
                if($sql->num_rows() > 0) // jika paketpemeriksaan memiliki parent
                {
                    $paket=$sql->result();
                    foreach ($paket as $obj) {
                        $arrMsg = $this->db->query("update rs_inap_rencana_medis_hasilpemeriksaan SET jasaoperator=0,nakes=0,jasars=0,bhp=0,akomodasi=0,margin=0,sewa=0,subtotal=0,statuspelaksanaan='batal',subtotalpotongan=0 WHERE idrencanamedispemeriksaan in (select idrencanamedispemeriksaan from rs_inap_rencana_medis_pemeriksaan where idinap=$idinap) and ".((!empty($ispaket))?"idpaketpemeriksaan='".$obj->idpaketpemeriksaan."'":"icd='$idrencana'"));
                    }
                }
                else
                {
                    $arrMsg = $this->db->query("update rs_inap_rencana_medis_hasilpemeriksaan SET jasaoperator=0,nakes=0,jasars=0,bhp=0,akomodasi=0,margin=0,sewa=0,subtotal=0,statuspelaksanaan='batal',subtotalpotongan=0 WHERE idrencanamedispemeriksaan in (select idrencanamedispemeriksaan from rs_inap_rencana_medis_pemeriksaan where idinap=$idinap) and ".((!empty($ispaket))?"idpaketpemeriksaan=$idrencana":"icd='$idrencana'"));
                }
            
        }
        pesan_success_danger($arrMsg,"Batal Semua ".$pesan." Berhasil...!","Batal Semua ".$pesan." Gagal...!","js");
    }
    // mahmud, clear -> status kamar
    public function lisbed()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_LISBED)) //lihat define di atas
        { 
        $data['content_view']      = 'pelayanan/v_listbed';
        $data['active_menu']       = 'pelayanan';
        $data['active_sub_menu']   = 'lisbed';
        $data['active_menu_level'] = '';
        $data['title_page']  = 'LIS BED';
        $data['mode']        = 'view';
        $data['table_title'] = 'List BED';
        $data['script_js']   = ['js_lisbed'];
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN];
        $this->load->view('v_index', $data);
        }
        else
        {
         aksesditolak();
        }
    }
    
    public function dt_bed()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_bed_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                
                $bedDipesan = ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Pesan" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="5" stat2="Bed Dipesan" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/beddipesan.png" width="25px"/></a> ';
                $bedKosong  = ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Kosongkan" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="1" stat2="Bed Kosong" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/bedkosong.png" width="25px"/></a> ';
                $bedRusak   = ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Rusak" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="6" stat2="Bed Rusak" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/bedrusak.png" width="25px"/></a> ';
                $bedMenunggu= ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Menunggu" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="4" stat2="Bed Menunggu" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/bedmenunggu.png" width="25px"/></a> ';
                $bedLaki    = ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Laki-laki" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="2" stat2="Bed Dipesan Laki-laki" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/bedlaki.png" width="25px"/></a> ';
                $bedWanita  = ' <a id="ubahstatusbed" style="cursor:pointer;" data-toggle="tooltip" data-original-title="Wanita" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="3" stat2="Bed Dipesan Wanita" stat1="'.$obj->statusbed.'"><img src="'.base_url().'assets/images/bedwanita.png" width="25px"/></a> ';

                if($obj->statusbed=='Bed Kosong'){ $bedKosong = "";}
                else if($obj->statusbed=='Bed Dipesan'){$bedDipesan="";}
                else if($obj->statusbed=='Bed Rusak'){$bedRusak = "";}
                else if($obj->statusbed=='Bed Menunggu'){$bedMenunggu = "";}
                else if($obj->statusbed=='Bed Laki-laki'){$bedLaki = "";}
                else if($obj->statusbed=='Bed Wanita'){$bedWanita = "";}
                
                $row = [];
                $row[] = ++$no;
                $row[] = $obj->namabangsal;
                $row[] = $obj->kelas;
                $row[] = $obj->statusbed;
                $row[] = $obj->nobed;
                $row[] = $bedDipesan.$bedKosong.$bedRusak.$bedMenunggu.$bedLaki.$bedWanita;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_bed(),
            "recordsFiltered" => $this->mdatatable->filter_dt_bed(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    // mahmud, clear sewa kamar dan status kamar
    public function sewakamar()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_SEWAKAMAR)) //lihat define di atas
        { 
        $data['content_view']      = 'pelayanan/v_sewakamar';
        $data['active_menu']       = 'pelayanan';
        $data['active_sub_menu']   = 'sewakamar';
        $data['active_menu_level'] = '';
        $data['title_page']  = 'Sewa Kamar';
        $data['mode']        = 'view';
        $data['dt_bangsal']  = $this->mgenerikbap->select_multitable("idbangsal, namabangsal", "rs_bangsal")->result();
        $data['table_title'] = 'List Sewa Kamar';
        $data['script_js']   = ['js_sewakamar'];
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE];
        $this->load->view('v_index', $data);
        }
        else
        {
         aksesditolak();
        }
    }
    // mahmud, clear --- get data sewa kamar
    public function getdtsewakamar()
    {
        $awal  = $this->input->post('awal');
        $akhir = $this->input->post('akhir');
        $bangsal = $this->input->post('bangsal');
        echo json_encode($this->db->query("select s.*, b.nobed, bs.statusbed, rb.namabangsal FROM rs_sewakamar s, rs_bed b, rs_bangsal rb, rs_bed_status bs where b.idbed = s.idbed and bs.idstatusbed = b.idstatusbed and rb.idbangsal = b.idbangsal".((!empty($awal)&&!empty($akhir))? " and date(s.waktumasuk) between '$awal' and '$akhir'" : "").((!empty($bangsal))? " and rb.idbangsal='$bangsal' " : ""))->result());
    }
    // ubah status sewakamar -- 
    public function ubahstatussewakamar()
    {
        $kekurangan = $this->input->post('kekurangan');
        $idsewa = $this->input->post('i');
        if(!empty($kekurangan)){$this->db->query('update rs_sewakamar set dibayar=dibayar+'.$kekurangan.', kekurangan=kekurangan+'.$kekurangan.' where idsewakamar='.$idsewa);}
        $status = $this->input->post('status');
        $arrMsg = $this->db->update('rs_sewakamar',['status'=>$status],['idsewakamar'=>$idsewa]);
        pesan_success_danger($arrMsg,"Ubah status sewa kamar berhasil..!","Ubah status sewa kamar gagal..!","js");
    }
    // sewakamar get data bed  ---
    public function sewakamar_getdtbed(){echo json_encode($this->db->query("select b.*, bs.idkelas, bs.sewakamar from rs_bed b, rs_bangsal bs where b.idstatusbed='1' and bs.idbangsal = b.idbangsal and b.idbangsal=".$this->input->post('i'))->result());}
    // sewakamar save 
    public function simpan_sewakamar()
    {
        $dtsewa = [
            'idinap'=>$this->input->post('idinap'),
            'namapenyewa'=>$this->input->post('namapenyewa'),
            'idbed'=>explode(',', $this->input->post('idbed'))[0],
            'idkelas'=>explode(',', $this->input->post('idbed'))[1],
            'catatan'=>$this->input->post('catatan'),
            'biayasewa'=>explode(',', $this->input->post('idbed'))[2],
            'dibayar'=>$this->input->post('bayar'),
            'kekurangan'=>$this->input->post('kekurangan'),
            'waktumasuk'=>$this->input->post('tglmasuk'),
            'waktukeluar'=>$this->input->post('tglkeluar'),
            'waktutagih'=>date('Y-m-d H:i:s'),
            'waktubayar'=>((!empty($this->input->post('bayar')))? date('Y-m-d H:i:s') : ''),
        ];
        $arrMsg = $this->db->insert('rs_sewakamar',$dtsewa);
        $idsewa = $this->db->insert_id();
        $infosewa = $this->db->query("select s.idsewakamar, s.namapenyewa,bs.namabangsal, s.catatan, s.biayasewa, s.dibayar, s.waktumasuk, s.waktukeluar, s.waktutagih, s.waktubayar, b.nobed from rs_sewakamar s, rs_bed b, rs_bangsal bs where b.idbed = s.idbed and bs.idbangsal = b.idbangsal and s.idsewakamar='$idsewa'")->row_array();
        if(!$arrMsg){pesan_danger($arrMsg,"Save Sewakamar Failed...!","js");}
        $arrMsg = $this->db->update('rs_bed',['idstatusbed'=>$this->input->post('jeniskelamin')],['idbed'=>explode(',', $this->input->post('idbed'))[0]]);
        echo json_encode(['status'=>(($arrMsg)?'success':'danger'),'message'=>(($arrMsg)?'Simpan sewa kamar berhasil...!':'Simpan sewa kamar gagal...!'),'dtsewa'=>$infosewa]);

    }
    //mahmud, clear --- get data status bed
    public function getdtstatusbed()
    {
        $bangsal = $this->input->post('bangsal');
        echo json_encode($this->db->query("select b.*, s.statusbed, bs.namabangsal, k.kelas from rs_bed b, rs_bed_status s, rs_bangsal bs, rs_kelas k where s.idstatusbed = b.idstatusbed and bs.idbangsal = b.idbangsal and k.idkelas = bs.idkelas".((!empty($bangsal))? " and b.idbangsal='$bangsal' " : ""))->result());
    }
    // mahmud, clear -- ubah status bed -- 
    public function ubahstatusbed()
    {
        $idbangsal = $this->input->post('ib');
        $idinap    = $this->input->post('ii');
        $arrMsg    = $this->db->update('rs_bed',['idstatusbed'=>$this->input->post('status')],['idbed'=>$this->input->post('i')]);
        if (!empty($idbangsal))
        {
            aplicare_updatebed($idbangsal);
        }
        if (!empty($idinap))
        {
           if(empty(!$this->input->post('statusinap'))){$data['status'] =  $this->input->post('statusinap');}
           $data['idstatusbed'] = $this->input->post('status');
            $arrMsg = $this->db->update('rs_inap',$data,['idinap'=>$idinap]);
        }
        pesan_success_danger($arrMsg,"Ubah status bed berhasil..!","Ubah status bed gagal..!","js");
    }
    //mahmud, clear --- get data rawat gabung
    public function getdtrawatgabung()
    {
        $tglawal = $this->input->post('awal');
        $tglakhir = $this->input->post('akhir');
        $bangsal = $this->input->post('bangsal');
        ((!empty($tglawal) && !empty($tglakhir))? $wheredate="and date(ri.waktumasuk) BETWEEN '$tglawal' and '$tglakhir'" : $wheredate="" );
        ((!empty($bangsal))? $wherebangsal=" and rb.idbangsal='$bangsal'" :$wherebangsal=" ");
        $sql = "SELECT  ppd.idpendaftaran, CONCAT( IFNULL(ppai.titeldepan, ''),' ', IFNULL(pperppai.namalengkap,''),' ', IFNULL(ppai.titelbelakang,'')) as namadokter, pp.namalengkap, ri.idinap, ppd.norm, ppd.nosep,  namabangsal, k.kelas, rb.nobed,if(ri.israwatgabung='1','Rawat Gabung','Pasien Induk') as israwatgabung, date_format(ri.waktumasuk,'%d-%m-%Y') as waktumasuk  FROM 
                    rs_inap ri 
                    left join rs_bed rb on ri.idbed=rb.idbed
                    left join rs_bangsal rbl on rbl.idbangsal=rb.idbangsal
                    left join person_pendaftaran ppd on ppd.idpendaftaran = ri.idpendaftaran 
                    left join person_pasien ppas on ppas.norm = ppd.norm 
                    left join person_person pp on pp.idperson = ppas.idperson 
                    left join person_pegawai ppai on ppai.idpegawai = ri.idpegawaidokter 
                    left join person_person pperppai on pperppai.idperson = ppai.idperson
                    left join rs_kelas k on k.idkelas = ri.idkelas
            WHERE ppd.isclosing=0 and ri.israwatgabung!='0'".$wheredate.$wherebangsal." ORDER BY ri.idbed, ppd.idpendaftaran asc";
        echo json_encode($this->db->query($sql)->result());
    }
    // mahmud, clear
    public function setting_administrasiranap()
    {
        return [    'content_view'      => 'pelayanan/v_pemeriksaanranap',
                    'active_menu'       => 'pelayanan',
                    'active_sub_menu'   => 'administrasiranap',
                    'active_menu_level' => ''
               ];
    }
    // mahmud, clear
    public function administrasiranap()//list pemeriksaanranap
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIRANAP)) //lihat define di atas
         { 
            $data                = $this->setting_administrasiranap(); //letakkan di baris pertama
            $data['title_page']  = 'Administrasi Rawat Inap';
            $data['mode']        = 'administrasi';
            $data['table_title'] = 'Pasien Rawat Inap';
            $data['script_js']   = ['js_cetakresumepasien','riwayat_perawatanranap','js_pemeriksaanranap-admin','riwayat_pengkajian_ulang'];
            $data['status']      = ['ranap'=>'Ranap','pesan'=>'Pesan','selesai'=>'Selesai','menunggu'=>'Menunggu','batal'=>'Batal','pulang'=>'Pulang'];
            $data['plugins']     = [ PLUG_DATATABLE,PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    // menampilkan data status bangsal, untuk pindah bed
    public function pindahkamar_listbangsal()
    {
        //  p.carabayar, if(p.ispasienlama='','Pasien Baru','Pasien Lama') as ispasienlama, pp.norm, pper.alamat, pp.nojkn, pper.nik, pper.namalengkap, pper.tanggallahir,
        // , p.keteranganobat, p.keteranganradiologi, p.keteranganlaboratorium
        $gender = $this->db->query("select pper.jeniskelamin  from  person_pasien pp, person_person pper where pp.norm='".$this->input->post('mr')."' and pp.idperson=pper.idperson")->row_array()['jeniskelamin'];
        $this->mgenerikbap->setTable("rs_bed_status");
        $statusbed = $this->mgenerikbap->select("*")->result();
        $this->db->where("( jeniskelaminbangsal = '".$gender."' OR jeniskelaminbangsal = 'campur' )", null, false);
        $bangsal = $this->mgenerikbap->select_multitable("idbangsal, namabangsal, kelas","rs_bangsal, rs_kelas")->result();
        $dtkelas = $this->db->query("select * from rs_kelas where idkelas between 2 and 5")->result();
        echo json_encode(['bangsal'=>$bangsal, 'statusbed'=>$statusbed, 'kelas'=>$dtkelas]);
    }
    // simpan data pindah kamar
    public function pindahkamar_simpan()
    {
        $idbangsal = $this->input->post('bangsal');
        $idbed = $this->input->post('bed');
        $idinap = $this->input->post('inap');
        // siapkan dasarkelasatarif
        $dk = $this->db->query("select dasarkelastarif from rs_bangsal where idbangsal=( select idbangsal from rs_bed b where b.idbed=$idbed)")->row_array()['dasarkelastarif'];
        // siapkan idkelas
        $idkelas = (($dk=='pasien')? $this->input->post('idkelas') : $this->db->query("select idkelas from rs_bangsal where idbangsal=( select idbangsal from rs_bed b where b.idbed=$idbed)")->row_array()['idkelas'] );
        $arrMsg = $this->db->update('rs_inap',['idbed'=>$idbed,'idkelas'=>$idkelas],['idinap'=>$idinap]);
        pesan_success_danger($arrMsg,"Pindah kamar rawat inap berhasil..!","Pindah kamar rawat inap gagal..!","js");
    }
    
    //basit, clear
    public function pemeriksaanranapchecklist()//list pemeriksaanranap
    {
         if ($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIRANAP)) //lihat define di atas
         { 
            $data                = $this->setting_administrasiranap(); //letakkan di baris pertama
            $data['content_view']= 'pelayanan/v_pemeriksaanranapchecklist';
            $data['active_sub_menu']    = 'pemeriksaanranapchecklist';
            $data['title_page']  = 'Rencana Perawatan Pasien Rawat Inap';
            $data['mode']        = 'view';
            $data['script_js']   = ['js_pemeriksaanranapchecklist'];
            $data['plugins']     = [PLUG_DROPDOWN, PLUG_DATE];
            $this->load->view('v_index', $data);
         }
         else
         {
             aksesditolak();
         }
    }
    //mahmud, clear -- list data administrasi ranap
    public function dt_administrasiranap()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_administrasiranap_(true);
        $db = $this->db->last_query();
        $data=[]; $no=0;
        if($getData){
            foreach ($getData->result() as $obj) {
                $menu1='';$menu2='';
                $row = [
                    ++$no,
                    $obj->norm,
                    $obj->namalengkap,
                    $obj->nosep,
                    $obj->waktu,
                    $obj->carabayar,
                    $obj->namadokter.' <a id="ubahdpjpranap" nomori="'.$obj->idinap.'" norm="'.$obj->norm.'" namalengkap="'.$obj->namalengkap.'" dokter="'.$obj->namadokter.'" bangsal="'.$obj->namabangsal.'" class="btn btn-xs btn-warning" '.  ql_tooltip('Ubah Dokter').'> <i class="fa fa-user-md"></i> </a>',
                    $obj->namabangsal.(($obj->status!=='pulang')?' <img width="32px" src="'.base_url().'assets/images/'.$obj->file.'" />':''),
                    $obj->pemeriksaan, 
                    convertToRupiah($obj->nominal),
                    $obj->status
                ];
                
                $menu2 .=' <a id="riwayatperawatan"  idinap="'.$obj->idinap.'" idpendaftaran="'.$obj->idpendaftaran.'" norm="'.$obj->norm.'" class=" btn btn-info btn-xs" '.ql_tooltip('Riwayat Perawatan').'><i class="fa fa-history"></i></a>';
                
                $menu1 .=' <a id="administrasiranapPeriksa"  nomori="'.$obj->idinap.'" class=" btn btn-success btn-xs" '.ql_tooltip('Lihat Pemeriksaan').'><i class="fa fa-stethoscope"></i></a>';
                $menu1 .=' <a id="assesmentranap"  nomori="'.$obj->idinap.'" class=" btn btn-warning btn-xs" '.ql_tooltip('Assesment Ranap').'><i class="fa fa-file"></i></a>';
                $menu1 .=' <a id="bhpfarmasiranap"  nomori="'.$obj->idinap.'" class=" btn btn-warning btn-xs" '.ql_tooltip('Detail Pemberian Obat/BHP').'><i class="fa fa-toggle-off"></i></a>';
                $menu1 .= (($obj->status=='selesai')?(($obj->statusbed=='Bed Kosong')?'':' <a id="ubahstatusbed" nomori="'.$obj->idinap.'" ibang="'.$obj->idbangsal.'" ibed="'.$obj->idbed.'" istatus="1" stat2="Bed Kosong" stat1="'.$obj->statusbed.'" class="btn btn-warning btn-xs"'.ql_tooltip('Kosongkan Bed').'><i class="fa fa-bed"></i></a> '):'');
                if($obj->status=='ranap')
                {
                    $menu2 .=' <a id="ranap_pindahkamar" norm="'.$obj->norm.'" idp="'.$obj->idpendaftaran.'" inap="'.$obj->idinap.'" bangsal="'.$obj->namabangsal.'" class=" btn bg-purple btn-xs" '.ql_tooltip('Pindah Kamar').'><i class="fa fa-bed"></i></a> ';
                    $menu2 .=' <a id="administrasiranapSelesai" alt="'.$obj->idinap.'" norm="'.$obj->norm.'" namapasien="'.$obj->namalengkap.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idbed.'" class=" btn btn-primary btn-xs" '.ql_tooltip('Selesai').'><i class="fa fa-check"></i></a> ';
                    $menu2 .=$menu1;
                    $menu2 .=' <a id="administrasiranapBatal" alt="'.$obj->idinap.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idbed.'" class=" btn btn-danger btn-xs" '.ql_tooltip('Batal').'><i class="fa fa-minus-circle"></i></a>';
                }
                else
                {
                    if($obj->status!='pulang')
                    {
                        if ($obj->lunas == 1){$menu2.='<a id="administrasiranapKeluarkamar" alt="'.$obj->idinap.'" alt3="'.$obj->idbed.'"alt4="'.$obj->idbangsal.'" class=" btn btn-danger btn-xs" '.ql_tooltip('Pasien Sudah Keluar Dari Kamar').'><i class="fa fa-user-times"></i></a> ';}
                        $menu2.= ' <a id="administrasiranapRanap" alt="'.$obj->idinap.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idbed.'" class=" btn btn-primary btn-xs" '.ql_tooltip('Kembali ke Status Inap').'><i class="fa fa-bed"></i></a> ';
                    }
                    else
                    {
                        $menu2.= ' <a id="administrasiranapBatalSelesai" alt="'.$obj->idinap.'" alt2="'.$obj->idpendaftaran.'" alt3="'.$obj->idbed.'" class=" btn btn-danger btn-xs" '.ql_tooltip('Batal Selesai').'><i class="fa fa-times"></i></a> ';
                    }
                    $menu2.=$menu1;
                } 
                
                $menu2 .= ' <a href="'.base_url('cpelayananranap/pemeriksaanranapdetail/ringkasanpasienpulang/'.$obj->idinap).'" class=" btn bg-maroon btn-xs" '.ql_tooltip('Ringkasan Pasien Pulang').'><i class="fa fa-trello"></i></a>';
                $menu2 .= ' <a href="'.base_url('cpelayananranap/pemeriksaanranapdetail/forminacbg/'.$obj->idinap).'" class=" btn btn-primary btn-xs" '.ql_tooltip('Formulir INA-CBG').'><i class="fa fa-delicious"></i></a>';
                $menu2 .= ' <a onclick="cetak_catatan_terintegrasi('.$obj->idpendaftaran.')" class=" btn btn-info btn-xs" '.ql_tooltip('Catatan Terintegrasi').'><i class="fa fa-align-justify"></i></a>';
                $menu2 .= ' <a onclick="pilih_riwayat_pengkajian_ulang(' . $obj->idpendaftaran . ')" class=" btn btn-danger btn-xs" ' . ql_tooltip('Riwayat Pengkajian Ulang') . '><i class="fa fa-file-text"></i></a>';
                
                $row[] = $menu2;
                $row[] = $obj->tanggallahir.' / '.$obj->usia;
                $row[] = $obj->notelpon;
                $row[] = $obj->alamat;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_administrasiranap(),
            "recordsFiltered" => $this->mdatatable->filter_dt_administrasiranap(),
            "data" =>$data,
            "db"=>$db
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    //mahmud, clear
    //RANAP Ringkasan Pasien Pulang (DISCARGE SUMMARY)
    public function pemeriksaanranapdetail()
    {   
        $data['content_view'] = 'pelayanan/v_pemeriksaanranapdetail';
        $data['mode']         = $this->uri->segment(3);
        $data['idinap']       = $this->uri->segment(4);
        $data['identitas']    = $this->mviewql->pemeriksaanranapdetail_identitas($this->uri->segment(4));
        $data['active_menu']  = 'pelayanan';
        $data['active_sub_menu']   = 'administrasiranap';
        $data['active_menu_level'] = '';
        $data['title_page']  = (($this->uri->segment(3) == 'ringkasanpasienpulang') ? 'Ringkasan Pasien Pulang (DISCHARGE SUMMARY)' : 'FORMULIR VERIFIKASI INA-CBG' );
        $data['script_js']   = ['js_cetakresumepasien','riwayat_perawatanranap','ranap/pemeriksaanranapdetail'];
        $data['plugins']     = [PLUG_DATATABLE,PLUG_DROPDOWN, PLUG_DATE];
        $this->load->view('v_index', $data);       
    }
    
    public function save_ringkasanpasienpulang()
    {
        $data['resikotinggi']       = $this->input->post('resikotinggi');
        $data['indikasirawatinap']  = $this->input->post('indikasirawatinap');
        $data['pilihan']            = $this->input->post('pilihan');
        $data['pilihanruang']       = $this->input->post('pilihanruang');
        $data['pilihankelas']       = $this->input->post('pilihankelas');
        $data['diagnosis']          = $this->input->post('diagnosis');
        $data['komordibitaslain']   = $this->input->post('komordibitaslain');
        $data['ringkasanriwayat_pemeriksaanfisik']  = $this->input->post('ringkasanriwayat_pemeriksaanfisik');
        $data['hasilpemeriksaanpenunjang']          = $this->input->post('hasilpemeriksaanpenunjang');
        $data['terapiselamaranap']  = $this->input->post('terapiselamaranap');
        $data['diagnosisDPJP']      = $this->input->post('diagnosisDPJP');
        $data['terapipulang']       = $this->input->post('terapipulang');
        $data['kondisisaatpulang']  = $this->input->post('kondisisaatpulang');
        $data['carapulang']         = $this->input->post('carapulang');
        $data['anjuran']            = $this->input->post('anjuran');
        $data['tindaklanjut']       = $this->input->post('tindaklanjut');
        $data['is_signature']       = $this->input->post('is_signature');
        $data['waktukeluar']        = $this->input->post('waktukeluar');

        $data['tindakanproseduroperasi'] = $this->input->post('tindakanproseduroperasi');
        
        $where['idinap'] = $this->input->post('idinap');
        $arrMsg = $this->db->update('rs_inap',$data,$where);
        pesan_success_danger($arrMsg, 'Simpan Data Ringkasan Pasien Pulang Berhasil.', 'Simpan Data Ringkasan Pasien Pulang Gagal.','js');
    }
    
    public function save_forminacbg()
    {
        $data['carapulang']        = $this->input->post('carapulang');
        $data['waktukeluar']       = $this->input->post('waktukeluar');
        
        $where['idinap'] = $this->input->post('idinap');
        $arrMsg = $this->db->update('rs_inap',$data,$where);
        pesan_success_danger($arrMsg, 'Simpan Data Form INA-CBG Berhasil.', 'Simpan Data Form INA-CBG Gagal.','js');
    }
    
    public function save_rs_inapdetail()
    {
        $data['icd']    = $this->input->post('icd');
        $data['idinap'] = $this->input->post('idinap');
        $data['jenis']  = $this->input->post('mode');
        
        if($data['jenis'] == 'icd10')
        {
            $data['level'] = $this->input->post('level');
        }
        
        $arrMsg = $this->db->replace('rs_inapdetail',$data);
        pesan_success_danger($arrMsg, 'Simpan '.ucwords($data['jenis']).' Berhasil.', 'Simpan '.ucwords($data['jenis']).' Gagal.', 'js');
    }
    
    public function view_rs_inapdetail()
    {
        $idinap = $this->input->post('idinap');
        $jenis  = $this->input->post('mode');
        $data   = $this->db->query("select ri.*,i.namaicd from rs_inapdetail ri join rs_icd i on i.icd = ri.icd where idinap='".$idinap."' and ri.jenis='".$jenis."' order by ri.level")->result_array();
        echo json_encode($data);
    }
    
    public function delete_rs_inapdetail()
    {
        $mode = $this->input->post('mode');
        $where['icd']    = $this->input->post('icd');
        $where['idinap'] = $this->input->post('idinap');
        $arrMsg = $this->db->delete('rs_inapdetail',$where);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    
    // end pemeriksaan ranap detail
    
    //mahmud clear
    // riwayat perawatan pasien ranap
    public function getdata_riwayatperawatanranap()
    {   $post = $this->input->post();
        $idpendaftaran = $post['idpendaftaran'];
        $idinap = $post['idinap'];
        $norm = $post['norm'];
        
        if($post['mode'] == '')
        {
            //data pasien
            $data['dtpasien'] = $this->mkombin->getdatapasienriwayatperiksa($norm);
            //data ranap
            $data['dtranap']  = $this->db->query("select "
            . "namadokter(ri.idpegawaidokter) as namadokter, "
            . "(select  concat((select rbs.namabangsal from rs_bangsal rbs where rbs.idbangsal=rb.idbangsal),' no. ',rb.nobed) from rs_bed rb where rb.idbed=ri.idbed) as kamar,"
            . "(select kelas from rs_kelas WHERE idkelas=ri.idkelas) as kelasperawatan,"
            . "(select kelas from rs_kelas WHERE idkelas=ri.idkelasjaminan) as kelasjaminan,"
            . "ri.catatan, ri.keteranganobat, ri.keteranganradiologi, ri.saranradiologi, ri.keteranganlaboratorium, ri.waktumasuk, ri.status "
            . "from rs_inap ri where ri.idinap='".$idinap."'")->row_array();
            
            //data penanggung
            $data['penanggung'] = $this->getdata_penanggungpasien($norm);
        }
        else if($post['mode'] == 'rawatinap')
        {
            //data rencana medis pemeriksaan ranap
            $data['rencanamedisranap'] = $this->mkombin->get_rencanamedisranap($idinap);
            
            $rencana_lanjutan = [];
            $rencana_tindakan = [];
            $rencana_anamnesa = [];
            $rencanabp=[];
            foreach ( $data['rencanamedisranap'] as $arr )
            {
                $rencana_lanjutan[] = $this->db
                        ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                        ->join('rs_icd b','b.icd=a.icd')
                        ->group_start()->where('b.idjenisicd',4)->or_where('b.idjenisicd',5)->group_end()
                        ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                        ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null])
                        ->result();
                $rencana_tindakan[] = $this->db
                        ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                        ->join('rs_icd b','b.icd=a.icd')
                        ->group_start()->where('b.idjenisicd',2)->or_where('b.idjenisicd',3)->group_end()
                        ->order_by('b.idjenisicd','asc')
                        ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                        ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null])
                        ->result();
                $rencana_anamnesa[] = $this->db
                        ->select("concat(b.namaicd,' <b>', ifnull(if(b.istext='text',a.nilaitext,a.nilai),''),'</b> ', ifnull(b.satuan,'') ) as hasil")
                        ->join('rs_icd b','b.icd=a.icd')
                        ->order_by('a.idrencanamedishasilpemeriksaan','asc')
                        ->get_where('rs_inap_rencana_medis_hasilpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana','a.icd !='=>null,'b.idjenisicd'=>1])
                        ->result();
                $rencanabp[] = $this->db
                        ->select("concat (ifnull(c.namabarang,''),' <b>', ifnull(a.jumlah,0),'</b> ',ifnull(d.namasatuan,'')) as hasil")
                        ->join('rs_inap_rencana_medis_barang b','b.idrencanamedisbarang=a.idrencanamedisbarang')
                        ->join('rs_barang c','c.idbarang=b.idbarang')
                        ->join('rs_satuan d','d.idsatuan=c.idsatuan')
                        ->get_where('rs_inap_rencana_medis_barangpemeriksaan a',['a.idrencanamedispemeriksaan'=>$arr['idrencanamedispemeriksaan'],'a.statuspelaksanaan'=>'terlaksana'])
                        ->result();
            }
            $data['rencana_lanjutan'] = $rencana_lanjutan;
            $data['rencana_tindakan'] = $rencana_tindakan;
            $data['rencana_anamnesa'] = $rencana_anamnesa;
            $data['rencanabp'] = $rencanabp;
        }
        else if($post['mode'] == 'rawatjalan')
        {
            //data rawat jalan
            $data['dtperiksa'] = $this->mkombin->getdatapendaftaranriwayatpasien($norm,'',$idpendaftaran);

            foreach ($data['dtperiksa'] as $list)
            {
                // list anamnesis
                $anamnesis[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_anamnesis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, rs_pemeriksaan rp where rh.idpendaftaran='".$idpendaftaran."' and rj.jenisicd='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
                // list periksa lanjutan
                $planjutan[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj, person_pendaftaran pp, rs_pemeriksaan rp where rh.idpendaftaran='".$idpendaftaran."' and rj.jenisicd!='tindakan' and rj.jenisicd!='diagnosa' and rj.jenisicd!='vital sign' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd and pp.idpendaftaran = rh.idpendaftaran and rp.idpemeriksaan = rh.idpemeriksaan order by rj.idjenisicd asc")->result();
                // list diagnosis
                $diagnosis[] = $this->db->query("select concat( ri.namaicd,' ',  ifnull(rh.nilai,''), ifnull(rh.nilaitext,''),' ',ri.satuan ) as hasil_diagnosis from rs_hasilpemeriksaan rh, rs_icd ri, rs_jenisicd rj where rh.idpendaftaran='".$idpendaftaran."' and rj.jenisicd!='radiologi' and rj.jenisicd!='vital sign' and rj.jenisicd!='laboratorium' and ri.icd = rh.icd and rj.idjenisicd = ri.idjenisicd order by rj.idjenisicd asc")->result();
                // list pemakaian obat
                $obat[] = $this->db->query("SELECT concat( ifnull(b.namabarang,''), ', dosis racik ', ifnull(rb.kekuatan,''),', jumlah ambil ', ifnull(rb.jumlahpemakaian,''), ' ',ifnull(s.namasatuan,'')) as obat FROM rs_barangpemeriksaan rb, rs_barang b, rs_satuan s WHERE rb.idpendaftaran='".$idpendaftaran."' and b.idbarang = rb.idbarang and s.idsatuan = b.idsatuan")->result();
                // list detail periksa
                $detailperiksa[] = $this->db->query("select rp.rekomendasi, pp.keteranganobat, pp.keteranganradiologi,pp.saranradiologi, pp.keteranganlaboratorium, rp.diagnosa,rp.anamnesa,rp.keterangan from rs_pemeriksaan rp, person_pendaftaran pp where pp.idpendaftaran = rp.idpendaftaran and rp.idpendaftaran='".$idpendaftaran."'")->result();


            }
            $data['dtanamnesis'] = $anamnesis;
            $data['planjutan']   = $planjutan;
            $data['dtdiagnosis'] = $diagnosis;
            $data['dtobat']      = $obat;
            $data['detailperiksa'] = $detailperiksa;
        }
        else if($post['mode'] == 'catatanterintegrasi')
        {
            $catatan = [];
            $dtranap = $this->mkombin->get_rencanamedisranaprowarray($idinap);            
            $data['catatan'] = $this->mkombin->get_catatanterintegrasiranap($dtranap);
            $data['dtranap'] = $dtranap;
            
        }
        
        echo json_encode($data);
    }
    
    // mahmud, clear
    public function administrasiranap_caripasien()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            // $jenis   = $this->input->post('jenis');
            $data        = $this->input->post('data');
            $status      = $this->input->post('status');
            $ordermode   = ifnull($this->input->post('ordermode'), 0);
            $bedmenunggu = (empty($data)) ? $this->db->query("select status from rs_inap where status='selesai' and idstatusbed='4'")->num_rows() : $this->db->query("select status from rs_inap ri join rs_bed rb on rb.idbed=ri.idbed where status='selesai' and ri.idstatusbed='4' and rb.idbangsal='".$data."' ")->num_rows();
            $dataranap   = $this->mkombin->administrasiranap_getdata($data,$status,$ordermode);
            echo json_encode(['dataranap'=>$dataranap, 'bedmenunggu'=>$bedmenunggu]);
        }
    }
    // mahmud, clear
    public function administrasi_ranap_ubahstatus()
    {
        $data['status'] = $this->input->post('status');
        $idbangsal = $this->db->select('idbangsal')->get_where('rs_bed',['idbed'=>$this->input->post('idbed')])->row_array()['idbangsal'];
        $idinap = $this->input->post('idinap');
        switch ($this->input->post('status'))
        {
            case 'selesai': 
                
//                $cekrencana = $this->mkombin->cekrencanaperawatan($idinap);
//                if($cekrencana->num_rows() > 0)
//                {
//                    $rencana = $cekrencana->result_array();
//                    echo json_encode(['rencana'=>$rencana,'status'=>'menunggu']);
//                    return true;
//                }
                //hapus perencanaan yang belum terlaksana
//                $this->mkombin->hapusrencanaperawatan($idinap);
                
                $data['waktukeluar'] = date('Y-m-d H:i:s'); 
                $data['idstatusbed'] = 4;
                $this->db->update('person_pendaftaran',['idstatuskeluar' => 2],['idpendaftaran'=>$this->input->post('idpendaftaran')]);
                $this->db->update('rs_bed',['idstatusbed' => 4],['idbed'=>$this->input->post('idbed')]);
                aplicare_updatebed($idbangsal);
                break;
            default: 
                $data['waktukeluar']   = '0000-00-00 00:00:00';
                $this->db->update('person_pendaftaran',['idstatuskeluar' => 1],['idpendaftaran'=>$this->input->post('idpendaftaran')]);
        }
        $arrMsg = $this->db->update('rs_inap',$data,['idinap'=>$idinap]);
        pesan_success_danger($arrMsg,"Ubah status rawat inap berhasil..!","Ubah status rawat inap gagal..!","js");
    }
    public function listbarangpemesanan(){echo json_encode($this->db->query("select rbp.*, (select namaunit from rs_unit ru where ru.idunit=rbp.idunittujuan) as unittujuan from rs_barang_pemesanan rbp where rbp.idinap='".$this->input->post('i')."'")->result_array());}
    public function detailbarangpemesanan()
    {
        echo json_encode($this->db->query(" select a.idbarangpemesanan, a.waktu, a.nofaktur, b.jumlah, b.keterangan,c.kode, c.namabarang, b.idbarang, ( select rbd.jumlah from rs_barang_distribusi rbd join rs_barang_pembelian rbp on rbp.idbarangpembelian=rbd.idbarangpembelian where rbd.idbarangpemesanan=a.idbarangpemesanan and rbp.idbarang=b.idbarang) jumlahdiberikan, (select namaunit from rs_unit where idunit=a.idunittujuan) unittujuan, (select namaunit from rs_unit where idunit=a.idunit) unitasal, (select namasatuan from rs_satuan where idsatuan=c.idsatuan) namasatuan from rs_barang_pemesanan a, rs_barang_pemesanan_detail b, rs_barang c where b.idbarangpemesanan=a.idbarangpemesanan and a.idbarangpemesanan='".$this->input->post('idp')."' and c.idbarang=b.idbarang")->result_array());
    }
    
    //mahmud, clear
    // ubah dokter dpjp pasien ranap
    public function ubahdpjprawatinap()
    {
        $arrMasg = $this->db->update('rs_inap',['idpegawaidokter'=>$this->input->post('idd')],['idinap'=>$this->input->post('iri')]);
        $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';status:ubahdpjp; idinap:'.$this->input->post('iri').' idpegawaidokter:'.$this->input->post('idd').'; IP:'.$_SERVER['REMOTE_ADDR'].' Client-Info:'. exec('getmac'),
                'expired' =>date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))))
            ];
        $this->mgenerikbap->setTable('login_log');
        $arrMasg = $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
        pesan_success_danger($arrMasg, "Ubah Dokter Pengganti Berhasil.", "Ubah Dokter Pengganti Gagal.", 'js');
    }
    
    //mahmud, clear
    public function getwaktuperiksalaborat()
    {
        if($this->input->post('mode')=='nonpaket'){
            $data = $this->db->query("SELECT rirmh.waktumulai, rirmh.waktuselesai FROM rs_inap_rencana_medis_hasilpemeriksaan rirmh join rs_icd ri on ri.icd=rirmh.icd where idrencanamedispemeriksaan='".$this->input->post('x')."' and ri.idjenisicd=4 and rirmh.idpaketpemeriksaanparent is null limit 1")->row_array();
        }else{
            $data = $this->db->select('waktumulai, waktuselesai')->get_where('rs_inap_rencana_medis_hasilpemeriksaan',['idpaketpemeriksaan'=>$this->input->post('y'),'idrencanamedispemeriksaan'=>$this->input->post('x')])->row_array();
        }
        echo json_encode($data);
    }
    
    public function onReadyPemesanandanpengembalianobat()
    {
        $data['unitasal']  = $this->session->userdata('unitterpilih');
        $data['unittujuan']= $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi'])->result(); 
        
        if($this->input->post('mode') == 'Retur')
        {
            $idinap = $this->input->post('i');
            $query = "select a.idbarangpemesanan,c.idbarangpembelian, a.idinap , c.stok, d.idbarang, d.batchno, d.kadaluarsa, e.namabarang, e.kode, f.namasatuan
            from rs_barang_pemesanan a 
            join rs_barang_distribusi b on b.idbarangpemesanan = a.idbarangpemesanan
            join rs_barang_stok c on c.idbarangpembelian = b.idbarangpembelian and c.idunit='".$this->session->userdata('idunitterpilih')."'
            join rs_barang_pembelian d on d.idbarangpembelian = c.idbarangpembelian
            join rs_barang e on e.idbarang = d.idbarang
            join rs_satuan f on f.idsatuan = e.idsatuan
            where a.jenistransaksi='pesan' and a.idinap='".$idinap."'
            GROUP by c.idbarangpembelian";
            $data['stok'] = $this->db->query($query)->result();
        }
        echo json_encode($data);
    }
    public function fillpesanbarangkefarmasi()
    {
        $id = $this->input->post('searchTerm');
        $query = $this->db->select(" concat(idbarang, ',', kode , ',' ,namabarang, ',', s.namasatuan ) as id, concat(ifnull(kode,''),' ',ifnull(namabarang,'')) as text");
        $query = $this->db->join('rs_satuan s','s.idsatuan = b.idsatuan');
        $query = $this->db->like("namabarang","$id");
        $query = $this->db->limit(20);
        $query = $this->db->get('rs_barang b')->result_array();
        echo json_encode($query);
    }
    
    public function riwayat_pemberianobatranap()
    {
        $idpendaftaran = $this->input->post('idp');
        $sql = "SELECT rirmp.waktu, rirmp.status, rirmb.idbarang, rb.namabarang, rs.namasatuan,rirmb.idjenistarif, rirmb.harga, rirmb.jumlah,ifnull(cast(rirmb.total as INT),0) as total,  rirmb.statuspelaksanaan "
                . "FROM rs_inap_rencana_medis_barangpemeriksaan rirmb  "
                . "join rs_inap_rencana_medis_pemeriksaan rirmp on rirmp.idrencanamedispemeriksaan = rirmb.idrencanamedispemeriksaan "
                . "join rs_barang rb on rb.idbarang = rirmb.idbarang "
                . "left join rs_satuan rs on rs.idsatuan = rirmb.idsatuanpemakaian "
                . "where rirmb.idpendaftaran = '".$idpendaftaran."'"
                . "order by rirmp.waktu desc";
        $dt = $this->db->query($sql)->result_array();
        echo json_encode($dt);
    }
    
    public function rs_inap_rencana_medis_barangpemeriksaan_updatesatuan()
    {
        $idr    = $this->input->post('idr');
        $arrMsg = $this->db->update('rs_inap_rencana_medis_barangpemeriksaan',['idsatuanpemakaian'=>$this->input->post('ids')],['idrencanamedisbarangpemeriksaan'=>$idr]);
        pesan_success_danger($arrMsg, 'Update Berhasil.', 'Update Gagal.','js');
    }
    
    public function ubah_rs_barang_pemesanan_detail()
    {
        $id     = $this->input->post('id');
        $jumlah = $this->input->post('jumlah');        
        $arrMsg = $this->db->update('rs_barang_pemesanan_detail',['jumlah'=>$jumlah],['idbarangpemesanandetail'=>$id]);
        pesan_success_danger($arrMsg, 'Ubah Jumlah Pesanan Berhasil.', 'Ubah Jumlah Pesanan Gagal.', 'js');
    }
    
    
    public function hapus_rs_barang_pemesanan_detail()
    {
        $id     = $this->input->post('id');
        $arrMsg = $this->db->delete('rs_barang_pemesanan_detail',['idbarangpemesanandetail'=>$id]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    
    public function pemberianobatranap_setpemberianobat()
    {
        $idbarang = $this->input->post('idbarang');
        $idp      = $this->input->post('idpembelian');
        $jumlah   = $this->input->post('jumlah');
        $id       = $this->input->post('id');
        $arrMsg   = $this->db->replace('rs_inap_rencana_medis_barangpemeriksaan_detail',['jumlah'=>$jumlah,'idbarangpembelian'=>$idp,'idrencanamedisbarangpemeriksaan'=>$id,'idunit'=>$this->session->userdata('idunitterpilih')]);
        pesan_success_danger($arrMsg, 'Pemberian Berhasil.', 'Pemberian gagal.', 'js');
        
    }
    
    
    public function update_rs_inap_rencana_medis_pemeriksaan()
    {
        $post  = $this->input->post();
        $kolom = explode(',', $post['kolom']);
        $isi   = explode(',', $post['isi']);
        $where = ['idrencanamedispemeriksaan' => $post['idrp']];
        $data = [];
        $no = 0;
        foreach ($kolom as $value)
        {
            $data[$kolom[$no]] = $isi[$no];
            $no++;
        }
        $update = $this->db->update('rs_inap_rencana_medis_pemeriksaan',$data,$where);
        pesan_success_danger($update, 'Perubahan Berhasil Disimpan', 'Perubahan Berhasil Disimpan', 'js');
    }
    
    public function insertupdate_rs_inap_soap()
    {
        $waktu= (( empty($this->input->post('waktu') )) ? date('Y-m-d H:i:s') : $this->input->post('waktu') ); 
        $data = [
            'idrencanamedispemeriksaan' => $this->input->post('idrp'),
            'idpegawai' => $this->input->post('idpetugas'),
            'idprofesi' => $this->input->post('idprofesi'),
            'sip'=> $this->input->post('sip'),
            'soa' => $this->input->post('soa_soa'),
            'p' => $this->input->post('soa_p'),
            'waktu' => $waktu
        ];
        $save = $this->db->replace('rs_inap_soap',$data);
        pesan_success_danger($save, 'SOAP Berhasil Disimpan', 'SOAP Gagal Disimpan', 'js');
    }
    public function edit_rs_inap_soap()
    {
        $idrp = $this->input->post('idrp');
        $waktu= $this->input->post('waktu');
        $data = $this->mkombin->get_rs_inap_soap($idrp,$waktu)->row();
        echo json_encode($data);
    }
    public function dt_soapranap()
    {
        $idrp = $this->input->post('idrp');
        $data = $this->mkombin->get_rs_inap_soap($idrp,'')->result_array();
        echo json_encode($data);
    }

    public function get_data_login_dpjp()
    {
        $idpegawaidokter = $this->input->post('idpegawaidokter');

        $querygetidperson = $this->db->query("SELECT * FROM  person_pegawai WHERE idpegawai =".$idpegawaidokter)->row_array();

        if(empty($querygetidperson))
        {
            echo json_encode(["result" => "Gagal", "msg" => "DPJP Belum Memiliki Akun!"]);
        }
        else
        {
            $quergetdatalogin = $this->db->query("SELECT * FROM `login_user` WHERE idperson  =".$querygetidperson['idperson'])->result_array();

            $result = ["result" => "Sukses", "data" => $quergetdatalogin];
            echo json_encode($result);
        }
    }

    public function login_dpjp()
    {
        $username = $this->security->sanitize_filename($this->input->post('username'));
        $password = $this->security->sanitize_filename($this->input->post('password'));
        $modelogin = $this->security->sanitize_filename($this->input->post('modelogin'));
        $idpegawaidokter =  $this->security->sanitize_filename($this->input->post('modelogin'));
        $hdnusernamedpjp = $this->security->sanitize_filename($this->input->post('hdnusernamedpjp'));

        $this->db->where('namauser', $hdnusernamedpjp);
        $query = $this->db->get('login_user');
        $user = $query->row_array();

        if($modelogin == 0)
        {
          $usernametimverif = "verifranap";
          $passwordtimverif = "verifranap";

          if($usernametimverif != $username || $passwordtimverif != $password)
          {
            $result = ["result" => "Gagal", "data" => "Username Atau Password Tim Verif Salah!"];
            echo json_encode($result);
          }
          else
          {
            $getsignature = $this->db->query("SELECT * FROM `person_signature` WHERE id_person = ".$user['idperson'])->result_array();

            if(!empty($getsignature))
            {
                $result = ["result" => "Sukses", "data" => "Login DPJP Berhasil", "signature" => $getsignature];
                echo json_encode($result);
            }
            else
            {
                $result = ["result" => "Sukses", "data" => "Dokter Belum Memiliki Tanda Tangan", "signature" => "nosignature"];
                echo json_encode($result);
            }
          }
        }
        else if($modelogin == 1)
        {
            if (password_verify($password, $user['password']) == true) 
            {
                $getsignature = $this->db->query("SELECT * FROM `person_signature` WHERE id_person = ".$user['idperson'])->result_array();
    
                if(!empty($getsignature))
                {
                    $result = ["result" => "Sukses", "data" => "Login DPJP Berhasil", "signature" => $getsignature];
                    echo json_encode($result);
                }
                else
                {
                    $result = ["result" => "Sukses", "data" => "Dokter Belum Memiliki Tanda Tangan", "signature" => "nosignature"];
                    echo json_encode($result);
                }
            }
            else
            {
                $result = ["result" => "Gagal", "data" => "Password DPJP Salah!"];
                echo json_encode($result);
            }
        }
    }

    public function save_asesmen_ulang_status_fungsional()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules(
            'idpendaftaranranap',
            'IDPendaftaranRanap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        $this->form_validation->set_rules(
            'idinap',
            'ID Inap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "idpendaftaranranap" => $post['idpendaftaranranap'],
                "idinap" => $post['idinap'],
                "ibmakan" => isset($post['chckibmakan'])  ? $post['chckibmakan'] : null,
                "ibmandi" => isset($post['chckibmandi'])  ? $post['chckibmandi'] : null,
                "ibperawatandiri" => isset($post['chckibperawatandiri'])  ? $post['chckibperawatandiri'] : null,
                "ibberpakaian" => isset($post['chckibberpakaian'])  ? $post['chckibberpakaian'] : null,
                "ibbuangairkecil" => isset($post['chckibbuangairkecil'])  ? $post['chckibbuangairkecil'] : null,
                "ibbuangairbesar" => isset($post['chckibbuangairbesar'])  ? $post['chckibbuangairbesar'] : null,
                "ibpenggunaantoilet" => isset($post['chckibpenggunaantoilet'])  ? $post['chckibpenggunaantoilet'] : null,
                "ibtransfer" => isset($post['chckibtransferbedtochairandback'])  ? $post['chckibtransferbedtochairandback'] : null,
                "ibmobilitas" => isset($post['chckibmobilitas'])  ? $post['chckibmobilitas'] : null,
                "ibnaikturuntangga" => isset($post['chckibnaikturuntangga'])  ? $post['chckibnaikturuntangga'] : null,
                "txtibmakan" => isset($post['txtjumlahskoribmakan'])  ? $post['txtjumlahskoribmakan'] : null,
                "txtibmandi" => isset($post['txtjumlahskoribmandi'])  ? $post['txtjumlahskoribmandi'] : null,
                "txtibperawatandiri" => isset($post['txtjumlahskoribperawatandiri'])  ? $post['txtjumlahskoribperawatandiri'] : null,
                "txtibberpakaian" => isset($post['jmlskoribberpakaian'])  ? $post['jmlskoribberpakaian'] : null,
                "txtibbuangairkecil" => isset($post['txtjumlahskorbuangairkecil'])  ? $post['txtjumlahskorbuangairkecil'] : null,
                "txtibbuangairbesar" => isset($post['txtjumlahskorbuangairbesar'])  ? $post['txtjumlahskorbuangairbesar'] : null,
                "txtibpenggunaantoilet" => isset($post['txtjumlahskorpenggunaantoilet'])  ? $post['txtjumlahskorpenggunaantoilet'] : null,
                "txtibtransfer" => isset($post['txtjumlahskortransferbedtochairandback'])  ? $post['txtjumlahskortransferbedtochairandback'] : null,
                "txtibmobilitas" => isset($post['txtjumlahskormobilitiyonlevel'])  ? $post['txtjumlahskormobilitiyonlevel'] : null,
                "txtibnaikturuntangga" => isset($post['txtjumlahskornaikturuntangga'])  ? $post['txtjumlahskornaikturuntangga'] : null,
                "txttotalskorib" => isset($post['txttotalskorindeksbarthel'])  ? $post['txttotalskorindeksbarthel'] : null,
                "interpretasihasilib" => isset($post['chckinterpretasihasilib'])  ? $post['chckinterpretasihasilib'] : null,
                "isfedukasikeluargadanlibatkankeluarga" => isset($post['chckintervebsistatusfungsionaledukasikeluargadanlibatkankeluarga'])  ? $post['chckintervebsistatusfungsionaledukasikeluargadanlibatkankeluarga'] : null,
                "isfkonsultasikanpadadpjp" => isset($post['chckintervebsistatusfungsionalkonsultasikanpadadpjpjikaskor'])  ? $post['chckintervebsistatusfungsionalkonsultasikanpadadpjpjikaskor'] : null,
                "isftidakadaintervensi" => isset($post['chckintervebsistatusfungsionaltidakadaintervensi'])  ? $post['chckintervebsistatusfungsionaltidakadaintervensi'] : null,
                "isflainnya" => isset($post['chckintervebsistatusfungsionallainnya'])  ? $post['chckintervebsistatusfungsionallainnya'] : null,
                "txtisflainnya" => isset($post['txtintervensistatusfungsional'])  ? $post['txtintervensistatusfungsional'] : null,
                "tglasesmenulangstatusfungsional" => isset($post['tglasesmenulangstatusfungsional'])  ? $post['tglasesmenulangstatusfungsional'] : null,
                "jamasesmenstatusfungsional" => isset($post['jamasesmenstatusfungsional'])  ? $post['jamasesmenstatusfungsional'] . ":00" : null,
                "txtperawatapelaksanaasesmenstatusfungsional" => isset($post['perawatpelaksanaasesmenulangstatusfungsional'])  ? $post['perawatpelaksanaasesmenulangstatusfungsional'] : null,
                "txtrencanapengkajianulang" => isset($post['txtrencanapengkajianulang']) ? $post['txtrencanapengkajianulang'] : null,
                "chckintervebsistatusfungsionalrencanapengkajianulang" => isset($post['chckintervebsistatusfungsionalrencanapengkajianulang']) ? $post['chckintervebsistatusfungsionalrencanapengkajianulang'] : null
            ];

            $this->db->insert('rs_asesmen_ulang_status_fungsional', $data);

            echo json_encode(['msg' => 'success', 'txt' => 'Simpan Data Asesmen Ulang Status Fungsional Berhasil!']);
        } else {
            echo json_encode(['msg' => 'danger', 'txt' => 'Error To Insert This Data!']);
        }
    }

    public function save_asesmen_ulang_risiko_jatuh()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules(
            'idpendaftaranranap',
            'IDPendaftaranRanap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        $this->form_validation->set_rules(
            'idinap',
            'ID Inap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == TRUE) {
            $data = [
                "idpendaftaranranap" => $post['idpendaftaranranap'],
                "idinap" => $post['idinap'],
                "pilhanpengkajianrisikojatuh" => isset($post['chckpengkajianrisikojatuh'])  ? $post['chckpengkajianrisikojatuh'] : null,
                "humptydumptyusia" => isset($post['chckhumptydumptyusia'])  ? $post['chckhumptydumptyusia'] : null,
                "humptydumptyjeniskelamin" => isset($post['chckhumptydumptyjeniskelamin'])  ? $post['chckhumptydumptyjeniskelamin'] : null,
                "humptydumptydiagnosis" => isset($post['chckhumptydumptydiagnosis'])  ? $post['chckhumptydumptydiagnosis'] : null,
                "humptydumptygangguankognitif" => isset($post['chckhumptydumptygangguankognitif'])  ? $post['chckhumptydumptygangguankognitif'] : null,
                "humptydumptyfaktorlingkungan" => isset($post['chckhumptydumptyfaktorlingkungan'])  ? $post['chckhumptydumptyfaktorlingkungan'] : null,
                "humptydumptypembedahansedasianestesi" => isset($post['chckhumptydumptypembedahansedasi'])  ? $post['chckhumptydumptypembedahansedasi'] : null,
                "humptydumptypenggunaanmedikamentosa" => isset($post['chckpenggunaanmedikamentosa'])  ? $post['chckpenggunaanmedikamentosa'] : null,
                "txtskorpasienhdusia"  => isset($post['txtjumlahskorhumptydumptyusia'])  ? $post['txtjumlahskorhumptydumptyusia'] : null,
                "txtskorpasienhdjeniskelamin"  => isset($post['txthumptydumptyjeniskelamin'])  ? $post['txthumptydumptyjeniskelamin'] : null,
                "txtskorpasienhddiagnosis"  => isset($post['txthumptydumptydiagnosis'])  ? $post['txthumptydumptydiagnosis'] : null,
                "txtskorpasienhdgangguankognitif"  => isset($post['txthumptydumptygangguankognitif'])  ? $post['txthumptydumptygangguankognitif'] : null,
                "txtskorpasienhdfaktorlingkungan"  => isset($post['txthumptydumptyfaktorlingkungan'])  ? $post['txthumptydumptyfaktorlingkungan'] : null,
                "txtskorpasienhdpembedahansedasianestesi"  => isset($post['txthumptydumptypembedahansedasi'])  ? $post['txthumptydumptypembedahansedasi'] : null,
                "txtskorpasienhdpenggunaanmedikamentosa"  => isset($post['txtpenggunaanmedikamentosa'])  ? $post['txtpenggunaanmedikamentosa'] : null,
                "txttotalskorhumptydumpty"  => isset($post['totalskorhumptydumpty'])  ? $post['totalskorhumptydumpty'] : null,
                "interpretasihasilhumptydumpty" => isset($post['chckinterpretasihasilhumptudumpty'])  ? $post['chckinterpretasihasilhumptudumpty'] : null,
                "riwayatjatuhmfs"  => isset($post['chckmorsefallriwayatjatuh'])  ? $post['chckmorsefallriwayatjatuh'] : null,
                "diagnosasekundermfs"  => isset($post['chckmorsefalldiagnosasekunder'])  ? $post['chckmorsefalldiagnosasekunder'] : null,
                "alatbantujalanmfs"  => isset($post['chckmorsefallalatbantujalan'])  ? $post['chckmorsefallalatbantujalan'] : null,
                "terpasanginfusmfs"  => isset($post['chckmorsefallterpasanginfus'])  ? $post['chckmorsefallterpasanginfus'] : null,
                "gayaberjalanmfs"  => isset($post['chckmorsefallgayaberjalan'])  ? $post['chckmorsefallgayaberjalan'] : null,
                "statusberjalanmfs"  => isset($post['chckmorsefallstatusmental'])  ? $post['chckmorsefallstatusmental'] : null,
                "txtskorpasienmfsriwayatjatuh"  => isset($post['txtmorsefallriwayatjatuh'])  ? $post['txtmorsefallriwayatjatuh'] : null,
                "txtskorpasienmfsdiagnosekunder"  => isset($post['txtmorsefalldiagnosasekunder'])  ? $post['txtmorsefalldiagnosasekunder'] : null,
                "txtskorpasienmfsalatbantujalan"  => isset($post['txtmorsefallalatbantujalan'])  ? $post['txtmorsefallalatbantujalan'] : null,
                "txtskorpasienmfsterpasanginfus"  => isset($post['txtmorsefallterpasanginfus'])  ? $post['txtmorsefallterpasanginfus'] : null,
                "txtskorpasienmfsgayaberjalan"  => isset($post['txtmorsefallgayaberjalan'])  ? $post['txtmorsefallgayaberjalan'] : null,
                "txtskorpasienmfsstatusmental"  => isset($post['txtmorsefallstatusmental'])  ? $post['txtmorsefallstatusmental'] : null,
                "txttotalskormfs"  => isset($post['txttotalskormorsefallscale'])  ? $post['txttotalskormorsefallscale'] : null,
                "intepretasihasilmfs"  => isset($post['chckinterprestasihasilmorsefallscale'])  ? $post['chckinterprestasihasilmorsefallscale'] : null,
                "riwayatjatuhomsapakahpasiendatangkerskarenajatuh" => isset($post['chckriwayatjatuhapakahpasiendatangkerumahsakit'])  ? $post['chckriwayatjatuhapakahpasiendatangkerumahsakit'] : null,
                "riwayatjatuhomsjikatidakapakahpasienmengalami" => isset($post['chckriwayatjatuhjikatidak'])  ? $post['chckriwayatjatuhjikatidak'] : null,
                "statusmentalomsapakahpasiendelirium" => isset($post['chckstatusmentapakahpasiendelirium'])  ? $post['chckstatusmentapakahpasiendelirium'] : null,
                "statusmentalomsapakahpasiendisorientasi" => isset($post['chckapakahpasiendisorientasi'])  ? $post['chckapakahpasiendisorientasi'] : null,
                "statusmentalapakahpasienmengalamiagitasi" => isset($post['chckapakahpasienmengalamniagitasi'])  ? $post['chckapakahpasienmengalamniagitasi'] : null,
                "penglihatanomsapakahpasienmemakaikacamata" => isset($post['chckapakahpasienmemakaikacamata'])  ? $post['chckapakahpasienmemakaikacamata'] : null,
                "penglihatanomsapakahpasienmengeluhanpenglihatannya" => isset($post['chckapakahpasienmengeluhkanpengelihatansuram'])  ? $post['chckapakahpasienmengeluhkanpengelihatansuram'] : null,
                "penglihatanomsapakahpasienmempunyaiglaukoma" => isset($post['chckapakahpasienmempunyaiglukoma'])  ? $post['chckapakahpasienmempunyaiglukoma'] : null,
                "kebiasaanberkemihomsapakahterdappatperubahanperilakuberkemih" => isset($post['chckkebiasaanberkemih1'])  ? $post['chckkebiasaanberkemih1'] : null,
                "transferoms" => isset($post['chcktransfer'])  ? $post['chcktransfer'] : null,
                "mobilitasoms" => isset($post['chckmobilitas'])  ? $post['chckmobilitas'] : null,
                "txtomsnilairiwayatjatuh" => isset($post['txtjumlahskorriwayatjatuh'])  ? $post['txtjumlahskorriwayatjatuh'] : null,
                "txtomsskorriwayatjatuh" => isset($post['txttotalskorriwayatjatuh'])  ? $post['txttotalskorriwayatjatuh'] : null,
                "txtomsnilaistatusmental" => isset($post['txtjumlahnilaistatusmental'])  ? $post['txtjumlahnilaistatusmental'] : null,
                "txtomsskorstatusmental" => isset($post['txttotalskorstatusmental'])  ? $post['txttotalskorstatusmental'] : null,
                "txtomsnilaipenglihatan" => isset($post['txtjumlahskorpenglihatan'])  ? $post['txtjumlahskorpenglihatan'] : null,
                "txtomsskorpenglihatan" => isset($post['txttotalskorpenglihatan'])  ? $post['txttotalskorpenglihatan'] : null,
                "txtomsnilaikebiasaanberkemih" => isset($post['txtjumlahkebiasaanberkemih'])  ? $post['txtjumlahkebiasaanberkemih'] : null,
                "txtomsskorkebiasaanberkemih" => isset($post['txttotalkebiasaanberkemih'])  ? $post['txttotalkebiasaanberkemih'] : null,
                "txtomsnilaitransfer" => isset($post['txtjumlahtransfer'])  ? $post['txtjumlahtransfer'] : null,
                "txtomsnilaimobilitasdantransfer" => isset($post['txttotaltransferdanmobilitas'])  ? $post['txttotaltransferdanmobilitas'] : null,
                "txtomsskormobilitas" => isset($post['txtjumlahmobilitas'])  ? $post['txtjumlahmobilitas'] : null,
                "txtomstotalnilai" => isset($post['jumlahskoromss'])  ? $post['jumlahskoromss'] : null,
                "txtomstotalskor" => isset($post['totalskoromsss'])  ? $post['totalskoromsss'] : null,
                "intepretasihasilomss"  => isset($post['chckintepretasiomsss'])  ? $post['chckintepretasiomsss'] : null,
                "irjrspastikanpasienterpasanggelangkuning" => isset($post['chckintervensirisikojatuhpastikanpasienterpasanggelangkuning'])  ? $post['chckintervensirisikojatuhpastikanpasienterpasanggelangkuning'] : null,
                "irjrspastikandiorientasikankondisilingkungan" => isset($post['chckintervensirisikojatuhpastikandiorintasikankondisilingkungan'])  ? $post['chckintervensirisikojatuhpastikandiorintasikankondisilingkungan'] : null,
                "irjrspengecekanbelmudahdijangkau" => isset($post['chckintervensirisikojatuhpengecekanbelmudahdijangkau'])  ? $post['chckintervensirisikojatuhpengecekanbelmudahdijangkau'] : null,
                "irjrsjnaikkanpagarpengmabantempattidur" => isset($post['chckintervensirisikojatuhnaikkanpagarpengamantempattidur'])  ? $post['chckintervensirisikojatuhnaikkanpagarpengamantempattidur'] : null,
                "irjrshandrailmudahdijangkaudankokoh" => isset($post['chckintervensirisikojatuhhandrailmudahdijangkau'])  ? $post['chckintervensirisikojatuhhandrailmudahdijangkau'] : null,
                "irjrsrodatempattidurberadapadapoisisiterkunci" => isset($post['chckintervensirisikojatuhrodatempatdiruberadapadaposisiterkuncunci'])  ? $post['chckintervensirisikojatuhrodatempatdiruberadapadaposisiterkuncunci'] : null,
                "irjrspastikantandapasienrisikojatuhterpasangpadaatastempattidur" => isset($post['chckintervensirisikojatuhpastikantandapasienrisikojatuhterpasang'])  ? $post['chckintervensirisikojatuhpastikantandapasienrisikojatuhterpasang'] : null,
                "irjrspastikanlampumenyalapadamalamhari" => isset($post['chckintervensirisikojatuhpastikanlampumenyalapadasaatmalamhari'])  ? $post['chckintervensirisikojatuhpastikanlampumenyalapadasaatmalamhari'] : null,
                "irjrsberikanedukasipencegahanjatuh" => isset($post['chckintervensirisikojatuhberikanedukasipencegahanjatuhpadapasien'])  ? $post['chckintervensirisikojatuhberikanedukasipencegahanjatuhpadapasien'] : null,
                "irjrslakukanpengkajianulangsetiap1x24jam" => isset($post['chckintervensirisikojatuhlakukanpengkajianulangsetiap'])  ? $post['chckintervensirisikojatuhlakukanpengkajianulangsetiap'] : null,
                "irjrtlakukansemuapencegahanjatuh" => isset($post['chckrisikotinggilakukansemuapencegahanjatuhrisikosedang'])  ? $post['chckrisikotinggilakukansemuapencegahanjatuhrisikosedang'] : null,
                "irjrtlibatkankeluargauntukselalumenunggu" => isset($post['chckrisikotinggilibatkankeluargauntukselalumenunggu'])  ? $post['chckrisikotinggilibatkankeluargauntukselalumenunggu'] : null,
                "irjrtberipenjelasanulangtentangpencegahanjatuh" => isset($post['chckrisikotinggiberipenjelasanulangtentang'])  ? $post['chckrisikotinggiberipenjelasanulangtentang'] : null,
                "irjrtlakukanpengkajianulangsetiap1x24jam" => isset($post['chckrisikotinggilakukanpengkajianulangsetiaphari'])  ? $post['chckrisikotinggilakukanpengkajianulangsetiaphari'] : null,
                "tanggalasesmenulangrisikojatuh" => isset($post['tglasesmenulangrisikojatuh'])  ? $post['tglasesmenulangrisikojatuh'] : null,
                "jamasesmenulangrisikojatuh" => isset($post['jamasesmenulangrisikojatuh'])  ? $post['jamasesmenulangrisikojatuh'].":00" : null,
                "perawatpelaksanaasesmenulangrisikojatuh" => isset($post['namaperawatapelaksana'])  ? $post['namaperawatapelaksana'] : null,
                "txtrencanapengkajianulang" => isset($post['txtrencanapengkajianulang']) ? $post['txtrencanapengkajianulang'] : null,
                "chckintervebsistatusfungsionalrencanapengkajianulang" => isset($post['chckrisikotinggirencanapengkajianulang']) ? $post['chckrisikotinggirencanapengkajianulang'] : null,
                "tbtidakadaintervensi" => isset($post['chcktidakadaintervernsi']) ? $post['chcktidakadaintervernsi'] : null
            ];

            $this->db->insert('rs_asesmen_ulang_risiko_jatuh', $data);

            echo json_encode(['msg' => 'success', 'txt' => 'Simpan Data Asesmen Ulang Risiko Jatuh Berhasil!']);
        } else {
            echo json_encode(['msg' => 'danger', 'txt' => 'Error To Insert This Data!']);
        }
    }

    public function get_data_asesmen_ulang_status_fungsional()
    {
        $post = $this->input->post();

        $query = $this->msqlbasic->show_db('rs_asesmen_ulang_status_fungsional', ["idpendaftaranranap" => $post['idpendaftaranranap']]);

        echo json_encode($query);
    }

    public function get_data_asesmen_ulang_risiko_jatuh()
    {
        $post = $this->input->post();

        $query = $this->msqlbasic->show_db('rs_asesmen_ulang_risiko_jatuh', ["idpendaftaranranap" => $post['idpendaftaranranap']]);

        echo json_encode($query);
    }

    public function save_asesmen_ulang_nyeri()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules(
            'idpendaftaranranap',
            'IDPendaftaranRanap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        $this->form_validation->set_rules(
            'idinap',
            'ID Inap',
            'required',
            array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == TRUE) {
            
            $data = [
                "idpendaftaranranap" => isset($post['idpendaftaranranap']) ? $post['idpendaftaranranap'] : NULL,
                "idinap" => isset($post['idinap']) ? $post['idinap'] : NULL,
                "wppunberdasarkanskalanyeri" => isset($post['chckbsn']) ? $post['chckbsn'] : NULL,
                "wppunsetelahintervensifarmakologis" => isset($post['chcksif']) ? $post['chcksif'] : NULL,
                "wppunsetelahintervensinonfarmakologis" => isset($post['chcksinf']) ? $post['chcksinf'] : NULL,
                "pengkajiannyeri" => isset($post['chckppn']) ? $post['chckppn'] : NULL,
                "nipsekspresiwajah" => isset($post['chckekw']) ? $post['chckekw'] : NULL,
                "nipstangisan" => isset($post['chcktangisan']) ? $post['chcktangisan'] : NULL,
                "nipspolapernafasan" => isset($post['chckpolaperna']) ? $post['chckpolaperna'] : NULL,
                "nipstangan" => isset($post['chcktangan']) ? $post['chcktangan'] : NULL,
                "nipskesadaran" => isset($post['chcknipskesadaran']) ? $post['chcknipskesadaran'] : NULL,
                "nipstotalskor" => isset($post['totalskornips']) ? $post['totalskornips'] : NULL,
                "nipsinterpretasihasil" => isset($post['chckbayimengalaminyeri']) ? $post['chckbayimengalaminyeri'] : NULL,
                "flaccwajah" => isset($post['chckflaccwajah']) ? $post['chckflaccwajah'] : NULL,
                "flaccwajahtotalskor" => isset($post['totalflaccwajah']) ? $post['totalflaccwajah'] : NULL,
                "flacckaki" => isset($post['chckflacckaki']) ? $post['chckflacckaki'] : NULL,
                "flacckakitotalskor" => isset($post['totalflacckaki']) ? $post['totalflacckaki'] : NULL,
                "flaccaktivitas" => isset($post['chckaktivitaspengkajian']) ? $post['chckaktivitaspengkajian'] : NULL,
                "flaccaktivitastotalskor" => isset($post['totalflaccaktivitas']) ? $post['totalflaccaktivitas'] : NULL,
                "flaccmenangis" => isset($post['chckpengkajianmenangis']) ? $post['chckpengkajianmenangis'] : NULL,
                "flaccmenangistotalskor" => isset($post['totalflaccmenangis']) ? $post['totalflaccmenangis'] : NULL,
                "flaccbersuara" => isset($post['chckpengkajianbersuara']) ? $post['chckpengkajianbersuara'] : NULL,
                "flaccbersuaratotalskor" => isset($post['totalflaccbersuara']) ? $post['totalflaccbersuara'] : NULL,
                "flacctotalskor" => isset($post['totalskorflacc']) ? $post['totalskorflacc'] : NULL,
                "flaccinterpretasihasil" => isset($post['chckinteprestrasihasilflacc']) ? $post['chckinteprestrasihasilflacc'] : NULL,
                "wbfps" => isset($post['chckwongbaker']) ? $post['chckwongbaker'] : NULL,
                "wbfpsinterpretasihasil" => isset($post['chckinterpretasiwbfps']) ? $post['chckinterpretasiwbfps'] : NULL,
                "wbfpsfymemperberat" => isset($post['chckfym']) ? $post['chckfym'] : NULL,
                "wbfpsfymeringankan" => isset($post['chckfymer']) ? $post['chckfymer'] : NULL,
                "wbfpskualitasnyeri" => isset($post['chckkualitasnyeri']) ? $post['chckkualitasnyeri'] : NULL,
                "wbfpsmenjalar" => isset($post['chckmenjalar']) ? $post['chckmenjalar'] : NULL,
                "wbfpsskalanyeri" => isset($post['chckskalanyeri']) ? $post['chckskalanyeri'] : NULL,
                "wbfpslamanyanyeri" => isset($post['chcklamanyanyeri']) ? $post['chcklamanyanyeri'] : NULL,
                "wbfpsfrekuensinyeri" => isset($post['chckfrekuensinyeri']) ? $post['chckfrekuensinyeri'] : NULL,
                "wbfpstxtfymemperberat" => isset($post['txtlainnyafaktoryangmemperberat']) ? $post['txtlainnyafaktoryangmemperberat'] : NULL,
                "wbfpstxtfymeringankan" => isset($post['txtlainnyafaktoryangmeringankan']) ? $post['txtlainnyafaktoryangmeringankan'] : NULL,
                "wbfpstxtkualitasnyeri" => isset($post['txtlainnyakualitasnyeri']) ? $post['txtlainnyakualitasnyeri'] : NULL,
                "wbfpstxtlokasinyeri" => isset($post['txtlokasinyeri']) ? $post['txtlokasinyeri'] : NULL,
                "wbfpstxtmenjalar" => isset($post['txtjelaskanmenjalar']) ? $post['txtjelaskanmenjalar'] : NULL,
                "inmengjarkanteknikrelaksasi" => isset($post['planintervensinyerimengajarkanteknikrelaksasi']) ? $post['planintervensinyerimengajarkanteknikrelaksasi'] : NULL,
                "inmeengaturposisiyangnyaman" => isset($post['planintervensinyerimegaturposisiyangnyamanbagipasien']) ? $post['planintervensinyerimegaturposisiyangnyamanbagipasien'] : NULL,
                "inmengontrollingkunganyangdapat" => isset($post['planintervensinyerimengontrollingkungan']) ? $post['planintervensinyerimengontrollingkungan'] : NULL,
                "inmemberikanedukasikepadapasiendankeluarga" => isset($post['planintervensinyerimemberikanedukasikepadapasiendankeluarga']) ? $post['planintervensinyerimemberikanedukasikepadapasiendankeluarga'] : NULL,
                "inkoloborasidengandokteruntukpemberianobatanalgetik" => isset($post['planintervensinyerikoloborasidengandokter']) ? $post['planintervensinyerikoloborasidengandokter'] : NULL,
                "oral" => isset($post['chckoralkdd']) ? $post['chckoralkdd'] : NULL,
                "iv" => isset($post['chckivkdd']) ? $post['chckivkdd'] : NULL,
                "im" => isset($post['chckimkdd']) ? $post['chckimkdd'] : NULL,
                "isdn" => isset($post['chckkddisdn']) ? $post['chckkddisdn'] : NULL,
                "inlainnya" => isset($post['planintervensinyerichcklainnya']) ? $post['planintervensinyerichcklainnya'] : NULL,
                "txtinlainnya" => isset($post['txtintervensinyerilainnya']) ? $post['txtintervensinyerilainnya'] : NULL,
                "intidakadaintervernsi" => isset($post['planintervensinyeritidakadaintervensi']) ? $post['planintervensinyeritidakadaintervensi'] : NULL,
                "inrencanapengkajianulang" => isset($post['planintervensinyerirencanapengkajianulang']) ? $post['planintervensinyerirencanapengkajianulang'] : NULL,
                "txtinrencanapengkajianulang" => isset($post['txtrencanapengkajianulang']) ? $post['txtrencanapengkajianulang'] : NULL,
                "tanggalselesai" => isset($post['txttglselesaiasesmen']) ? $post['txttglselesaiasesmen'] : NULL,
                "jamselesai" => isset($post['txtjamselesai']) ? $post['txtjamselesai'].":00" : NULL,
                "perawat" => isset($post['namaperawat']) ? $post['namaperawat'] : NULL
            ];

            $this->db->insert('rs_asesmen_ulang_nyeri', $data);
            echo json_encode(['msg' => 'success', 'txt' => 'Simpan Data Asesmen Ulang Nyeri Berhasil!']);
        } 
        else 
        {
            echo json_encode(['msg' => 'danger', 'txt' => 'Error To Insert This Data!']);
        }
    }

    public function get_data_asesmen_ulang_nyeri()
    {
        $idinap = $this->input->post('idinap');
        $idpendaftaranranap = $this->input->post('idpendaftaranranap');

        $data = $this->msqlbasic->show_db('rs_asesmen_ulang_nyeri',["idpendaftaranranap" => $idpendaftaranranap, "idinap" => $idinap]);

        echo json_encode($data);
    }

    public function hapus_pengkajian_ulang_risiko_jatuh()
    {
        $idpengkajianulangrisikojatuh = $this->input->post('id_pengkajian_ulang_risiko_jatuh');

        $this->db->where('id_pengkajian_ulang_risiko_jatuh',$idpengkajianulangrisikojatuh);
        $arrMsg = $this->db->delete('rs_asesmen_ulang_risiko_jatuh');

        if($arrMsg)
        {
            $result = [
                "msg" => "success",
                "txt" => "Berhasil Menghapus Data Pengkajian Ulang Risiko Jatuh"
            ];
            echo json_encode($result);
        }
        else
        {
            $result = [
                "msg" => "danger",
                "txt" => "Gagal Menghapus Data Pengkajian Ulang Risiko Jatuh"
            ];
            echo json_encode($result);
        }

    }

    public function hapus_pengkajian_ulang_status_fungsional()
    {
        $idpengkajianulangrisikofungsional = $this->input->post('idpengkajianulangstatusfungsional');

        $this->db->where('id_pengkajian_ulang_status_fungsional ',$idpengkajianulangrisikofungsional);
        $arrMsg = $this->db->delete('rs_asesmen_ulang_status_fungsional');

        if($arrMsg)
        {
            $result = [
                "msg" => "success",
                "txt" => "Berhasil Menghapus Data Pengkajian Ulang Status Fungsional"
            ];

            echo json_encode($result);
        }
        else
        {
            $result = [
                "msg" => "danger",
                "txt" => "Gagal Menghapus Data Pengkajian Ulang Status Fungsional"
            ];
            echo json_encode($result);
        }
    }

    public function hapus_pengkajian_ulang_nyeri()
    {
        $idpengkajianulangnyeri = $this->input->post('idpengkajianulangnyeri');

        $this->db->where('id_pengkajian_ulang_nyeri',$idpengkajianulangnyeri);
        $arrMsg = $this->db->delete('rs_asesmen_ulang_nyeri');

        if($arrMsg)
        {
            $result = [
                "msg" => "success",
                "txt" => "Berhasil Menghapus Data Pengkajian Ulang Nyeri"
            ];

            echo json_encode($result);
        }
        else
        {
            $result = [
                "msg" => "danger",
                "txt" => "Gagal Menghapus Data Pengkajian Ulang Nyeri"
            ];
            echo json_encode($result);
        }
    }

    public function setting_assesmentranap()
    {
        return [    'content_view'      => 'pelayanan/v_assesmentranap',
                    'active_menu'       => 'pelayanan',
                    'active_sub_menu'   => 'administrasiranap',
                    'active_menu_level' => ''
               ];
    }

    public function viewassesmentranap()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP))
        {
            $data                = $this->setting_assesmentranap();
            $data['title_page']  = 'Assesment Rawat Inap';
            $data['script_js']   = ['js_assesmenranap'];
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE , PLUG_TEXTAREA];
            $this->load->view('v_index', $data);
        }
    }

    public function save_awalmedis_ri()
    {
        $data = [
                    'idinap'            =>$this->input->post('idinap'), 
                    'idpendaftaranranap'=>$this->input->post('idpendaftaranranap'), 
                    'norm'              =>$this->input->post('norm'), 
                    'tekanan_darah'     =>$this->input->post('tekanandarah'), 
                    'laju_pernapasan'   =>$this->input->post('lajunapas'), 
                    'nadi'              =>$this->input->post('nadi'), 
                    'suhu'              =>$this->input->post('suhu'), 
                    'spo2'              =>$this->input->post('spo2'), 
                    'subjective'        =>$this->input->post('subjective'), 
                    'objective'         =>$this->input->post('objective'), 
                    'assessment'        =>$this->input->post('assessment'), 
                    'plan'              =>$this->input->post('plan'), 
                    'tanggal'           =>$this->input->post('tanggalawalmedisri'), 
                    'jam'               =>$this->input->post('jamawalmedisri'), 
                    'id_dpjp'           =>$this->input->post('dpjpawalmedisri')
        ];
        $cek_data = $this->db->query("SELECT * from emr_ranap_pengkajian_awal_medis where idinap='" . $this->input->post('idinap') . "'");
        if ($cek_data->num_rows() > 0) {
            $arrMsg = $this->db->where('idinap', $this->input->post('idinap'));
            $arrMsg = $this->db->update('emr_ranap_pengkajian_awal_medis', $data);

            $savepengkajian =  (($arrMsg)? ['error'=>false,'status'=>'success','message'=>'Edit Data Berhasil'] :['error'=>true,'status'=>'danger','message'=>'Edit Data Gagal']);
        } else {
            $arrMsg = $this->db->insert("emr_ranap_pengkajian_awal_medis",$data);
            $savepengkajian =  (($arrMsg)? ['error'=>false,'status'=>'success','message'=>'Data Berhasil Disimpan'] :['error'=>true,'status'=>'danger','message'=>'Gagal Simpan Data']);
        }

        echo json_encode($savepengkajian);
    }

    public function get_awalmedis_ranap()
    {
        $post       = $this->input->post('idinap');
        $data       = $this->db->where('idinap',$post)->get('emr_ranap_pengkajian_awal_medis')->row();

        echo json_encode($data);
    }

    public function save_cppt_ri()
    {
        $idcppt = $this->input->post('idcppt');
        $indexcppt = $this->input->post('indexcppt');
        $data = [
                    'idinap'            =>$this->input->post('idinap'), 
                    'idpendaftaranranap'=>$this->input->post('idpendaftaranranap'), 
                    'norm'              =>$this->input->post('norm'), 
                    'subjective'        =>$this->input->post('subjective'), 
                    'objective'         =>$this->input->post('objective'), 
                    'assessment'        =>$this->input->post('assessment'), 
                    'plan'              =>$this->input->post('plan'), 
                    'tanggalawalmedisri'=>$this->input->post('tanggalawalmedisri'), 
                    'jamawalmedisri'    =>$this->input->post('jamawalmedisri'), 
                    'ppari'             =>$this->input->post('ppari'), 
                    'cpptkonfirmasidpjp'=>$this->input->post('cpptkonfirmasidpjp')
        ];
        if (!empty($idcppt)) {
            $arrMsg = $this->db->where('id', $this->input->post('idcppt'));
            $arrMsg = $this->db->update('erm_ranap_cppt', $data);

            $savecppt =  (($arrMsg)? ['error'=>false,'status'=>'success','message'=>'Edit Data Berhasil'] :['error'=>true,'status'=>'danger','message'=>'Edit Data Gagal']);
        } else {
            $arrMsg = $this->db->insert("erm_ranap_cppt",$data);
            $idcppt = $this->db->insert_id();
            $savecppt =  (($arrMsg)? ['error'=>false,'status'=>'success','message'=>'Data Berhasil Disimpan','idcppt'=>$idcppt] :['error'=>true,'status'=>'danger','message'=>'Gagal Simpan Data']);
        }
        echo json_encode($savecppt);
    }

    public function get_cppt_ranap()
    {
        $post       = $this->input->post('idinap');
        $data       = $this->db->where('idinap',$post)->get('erm_ranap_cppt')->result();

        echo json_encode($data);
    }

    public function delete_cppt_ri()
    {
        $idcppt = $this->input->post('idcppt');

        $query = $this->db->query("DELETE FROM erm_ranap_cppt WHERE id = " . $idcppt);

        if ($query) {
            echo json_encode(["result" => "Sukses", "message" => "Berhasil Menghapus"]);
        } else {
            echo json_encode(["result" => "Gagal", "message" => "Gagal Menghapus"]);
        }
    }
}

/* End of file ${TM_FILENAME:${1/(.+)/Cpelayananranap.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cpelayananranap/:application/controllers/${1/(.+)/Cpelayananranap.php/}} */