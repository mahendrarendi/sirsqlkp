<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
class Canjunganpasien extends CI_Controller 
{
    function __construct()
    {
       parent::__construct();
    }
    // pengecekan login pasien
    public function ceklogin()
    {
        //siapkan data
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        if($username=='' && empty($username)) //jika username kosong tampilkan message
        {
            $arrMsg = ['status'=>'danger','message'=>'No Rekam medis tidak boleh kosong..!'];
        }
        else if($password =='' && empty($password))   //jika password kosong tampilkan message
        {
            $arrMsg = ['status'=>'danger','message'=>'Password tidak boleh kosong..!'];
        }
        else //selain itu cek username di table login_user
        {
            $this->mgenerikbap->setTable('login_user');
            $data = $this->mgenerikbap->select('*', ['namauser'=>$username]);
            if($data->num_rows() > 0) //jika user benar
            {
                $data = $data->row_array();
                if (password_verify($password,$data['password'])) //jika passwor benar 
                {
                    $arrMsg = ['status'=>'info','message'=>'Selamat datang, '.ucwords($username)];
                    $this->session->set_flashdata('message', $arrMsg);
                    $this->session->set_userdata('sessregis_pasien', $username);
                    // ambil data
                    $get_biodata = $this->db->query("SELECT namalengkap, jeniskelamin, tanggallahir,alamat FROM person_pasien pp, person_person pper WHERE pp.norm='".$username."' AND pper.idperson = pp.idperson")->row_array();
                    // siapkan data
                    $arrSess = [
                        'namalengkap'=>$get_biodata['namalengkap'],
                        'jeniskelamin'=>$get_biodata['jeniskelamin'],'tanggallahir'=>$get_biodata['tanggallahir'],'alamat'=>$get_biodata['alamat']];
                    $this->session->set_userdata($arrSess);//buat session biodata
                    
                    return $this->pendaftaranonline();
                }
                else //jika password salah
                {
                    $arrMsg = ['status'=>'danger','message'=>'Password salah.!' ] ;
                }
            }
            else //jika username salah
            {
                $arrMsg = ['status'=>'danger','message'=>'No Rekam medis salah.!' ] ;
            }
        }
        $this->session->set_flashdata('message', $arrMsg);
        $this->session->set_flashdata('username', $username);
        redirect('loginperiksa');
    }
    // menutup akses untuk halaman pendaftaran/registrasi periksa
    public function keluar()
    {
        $this->session->sess_destroy();
        redirect('canjunganpasien/jadwaldokter');
    }
    //halaman login untuk pasien yang akan mendaftar periksa
    public function loginperiksapasien()
    {
        $this->load->helper('form');
        $data['title_page']     = 'Login pendaftaran pasien';
        $data['script_js']      = ['anjungan_loginperiksa'];
        $this->load->view('anjungan_pasien/v_login', $data);
    }
    // daftar antrian saat ini
    public function index()
    {
        $this->output->cache(2);
        $data['dtantrian']      = json_decode($this->curl->simple_get(base_url('api/webservice/antrian')));
        $data['content_view']   = 'anjungan_pasien/v_listantrian';
        $data['active_menu']    = 'lihatantrian';
        $data['plugins']        = [ PLUG_DATATABLE ];
        $data['title_page']     = 'Antrian Pasien Terkini';
        $data['table_title']    = 'Antrian Pasien Terkini';
        $data['jsmode']         = '';
        $data['script_js']      = ['anjungan_jadwaldokter'];
        $this->load->view('anjungan_pasien/v_index', $data);
    }
    // list jadwal dokter dalam satu pekan kedepan
    public function jadwaldokter()
    {
        $this->output->cache(60);
        $data['dtjadwal']= json_decode($this->curl->simple_get(base_url('api/webservice/jadwalsepekan')));
        $data['content_view']   = 'anjungan_pasien/v_jadwaldokter';
        $data['active_menu']    = 'jadwaldokter';
        $data['plugins']        = [ PLUG_DATATABLE ];
        $data['title_page']     = 'Jadwal Dokter';
        $data['table_title']    = 'Jadwal Dokter 1 Pekan* [Mohon Perhatikan Hari dan Tanggalnya]';
        $data['jsmode']         = '';
        $data['script_js']      = ['anjungan_jadwaldokter'];
        $this->load->view('anjungan_pasien/v_index', $data);
    }
    // halaman pendaftaran periksa secara online
    public function pendaftaranonline()
    {
        // jika pasien berhasil login
        if($this->session->userdata('sessregis_pasien'))
        {
            $data['content_view']   = 'anjungan_pasien/v_pendaftaranonline';
            $data['active_menu']    = 'pendaftaranonline';
            $data['plugins']        = [ PLUG_DATE, PLUG_DROPDOWN ];
            $data['title_page']     = 'Pendaftaran Pasien';
            $data['table_title']    = 'Pendaftaran Pasien';
            $data['jsmode']         = '';
            $data['script_js']      = ['anjungan_pasien'];
            $this->load->view('anjungan_pasien/v_index', $data);
        }
        // selain itu
        else
        {
            $this->session->set_flashdata('message', ['status'=>'warning','message'=>'Silakan login dulu..!' ]);
            redirect('loginperiksa');
        }
    }
    // pendaftaran periksa pasien lama rawat jalan {pendaftaran oleh keluarga/pasien sendiri}
    public function register_pasien()
    {
        $this->load->view('anjungan_pasien/index');
    }
    public function register_ceknorm()
    {
        $periksa = $this->db->query("SELECT count(r.idpemeriksaan) as jumlah, pp.waktu from rs_pemeriksaan r left join person_pendaftaran pp on pp.idpendaftaran = r.idpendaftaran where r.norm='".$this->input->post('id')."' and r.status !='selesai' and r.status !='batal'")->row_array();
        if($periksa['jumlah'] > 0)
        {
            echo json_encode(['status'=>'danger','isi'=>'Pasien Masih Dalam Pemeriksaan <br> ('.$periksa['waktu'].') Segera menghubungi bagian pendaftaran']);
            return TRUE;
        }
        else
        {
            echo json_encode(['status'=>'success','isi'=>$this->db->query("SELECT pp.idperson, pp.namalengkap, pp.alamat, date_format(pp.tanggallahir,'%d-%M-%Y') as tanggallahir, pp.jeniskelamin FROM person_pasien p, person_person pp WHERE p.norm='".$this->input->post('id')."' AND pp.idperson = p.idperson")->row_array()]);
        }  
    }
    // ambil data cara bayar
    public function get_carabayar()
    {
        $this->load->model('mmasterdata');
        echo json_encode($this->mmasterdata->get_data_enum('person_pendaftaran','carabayar'));
    }
    // ambil data kelas layanan
    public function get_kelaslayanan()
    {
        echo json_encode($this->db->query("SELECT * FROM `rs_kelas`")->result());
    }
    public function cari_jadwalpoli() //cari poli di rs jadwal berdasarkan tanggal input
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->cari_jadwalpoli(date('Y-m-d', strtotime($this->input->post('date'))))); 
    }
    public function registrasi_savedata()
    {
        // cek pasien apakah sedang periksa atau tidak
        $periksa = $this->db->query("select count(r.idpemeriksaan) as jumlah, pp.waktu from rs_pemeriksaan r left join person_pendaftaran pp on pp.idpendaftaran = r.idpendaftaran where r.norm='".$this->input->post('rm')."' and r.status !='selesai' and r.status !='batal'")->row_array();
        if($periksa['jumlah'] > 0)//jika sedang periksa
        {
            echo json_encode(['status'=>'danger','message'=>'Pasien Masih Dalam Pemeriksaan ('.$periksa['waktu'].')']);
            return TRUE;
        }
        else
        {
            // JIKA PASIEN TIDAK DALAM KEADAAN PERIKSA
            // buat antrian pendftaran
            $callid = generaterandom(8);
            $pilihjadwal=explode(',', $this->input->post('jadwal')); //memecah data (id unit dan id jadwal)
            $data_person = (!empty($this->input->post('pp')) ? $this->input->post('pp') : $this->db->query("SELECT idperson from person_pasien WHERE norm='".$this->input->post('rm')."'")->row_array()['idperson'] );
            $idperson = $data_person;
            $idunit=$pilihjadwal[0]; //siapkan data idunit
            $idjadwal=$pilihjadwal[1];//siapkan data id jadwal
            $idloket = '39'; //39 untuk loket rawat jalan
            $idstasiun = '1'; //1 untuk stasiun di pendaftaran

            // simpan ke helper auto number untuk mendapatkan nomor antrian daftar
            if($this->input->post('carabayar')!='mandiri' && $this->input->post('caradaftar')!='online')
            {
                $idloket = 39;
                $idstasiun=1;
                $this->mgenerikbap->setTable('helper_autonumber');
                $callid         = generaterandom(8);
                $data = [
                'id'      =>'Antrian'.date('Ymd').'-'.$idstasiun.'-'.$idloket,
                'number'  =>null,
                'callid'  =>$callid,
                'expired' =>date('Y-m-d')
                ];
                $this->mgenerikbap->insert($data);
                $noantrisep = $this->mgenerikbap->select('number',['callid'=>$callid])->row_array();
                $data = ['idloket'=>$idloket, 'tanggal'=>date('Y-m-d'), 'no'=>$noantrisep['number'], 'status'=>'antri'];
                $arrMsg = $this->db->insert('antrian_antrian',$data);
                $loket = $this->db->query("select * from antrian_loket where idloket='$idloket'")->row_array();
                $nopesan = $noantrisep['number'];
                // $loket = $loket['namaloket'];
                $callid = $callid;

            }
            else //jika pasien yang mendaftar tidak mempunyai jaminan maka tidak dibuatkan nomor pesan
            {
                $nopesan = 'pasienmandiri';
                $loket = 'pasienmandiri';
                $callid = '';
            }

            // simpan pendaftaran pasien
            $data = [
                'norm'             =>$this->input->post('rm'),//nomor rekam medis pasien
                'idkelas'          =>$this->input->post('kelas'),//kelsas layanan periksa
                'idunit'           =>$idunit,//unit yang dituju
                'waktu'            =>date('Y-m-d H:i:s',strtotime($this->input->post('tanggal'))),//tanggal periksa
                'waktuperiksa'     =>date('Y-m-d H:i:s',strtotime($this->input->post('tanggal'))),
                'jenispemeriksaan' =>'rajal',//jenis pemeriksaan
                'carabayar'        =>$this->input->post('carabayar'),//cara pembayaran
                'caradaftar'       =>$this->input->post('caradaftar')
            ];
            $this->mgenerikbap->setTable('person_pendaftaran');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($data,'');
            $idpendaftaran = $this->db->insert_id();//set id penaftaran

            // update no pesan di pendaftaran
            $this->mgenerikbap->setTable('person_pendaftaran');
            $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate(['nopesan'=>$nopesan,'isantri'=>'1'],$idpendaftaran);
                
            
            //save antrian pemeriksaan
            $callid = generaterandom(8);
            $this->mgenerikbap->setTable('rs_unit');
            $loketpemeriksaan = $this->mgenerikbap->select("idloket",['idunit'=>$idunit])->row_array();
            $this->mgenerikbap->setTable('helper_autonumber');
            $data = ['id'=>date('Ymd',strtotime($this->input->post('tanggal'))).$loketpemeriksaan['idloket'],'number'=>null,'callid'=>$callid,'expired'=>date('Y-m-d',strtotime($this->input->post('tanggal')))];
            $simpan = $this->mgenerikbap->insert($data);
            // simpan pemeriksaan pasien
            $data = [
                'idpendaftaran'=>$idpendaftaran,
                'idjadwal'  =>$idjadwal,
                'nopesan'  =>$nopesan,
                'tahunbulan' =>date('Yn',strtotime($this->input->post('tanggal'))),
                'waktu' =>date('Y-m-d H:i:s',strtotime($this->input->post('tanggal'))),
                'norm'=>$this->input->post('rm'),
                'status'=>'antrian',
                'callid'=>$callid,
                'noantrian'=>'0'//nomor antrian periksa
            ];
            $this->mgenerikbap->setTable('rs_pemeriksaan');
            $simpan = $this->mgenerikbap->insert($data);

            $idpemeriksaan = $this->db->insert_id();
            // tambahkan paket vitalsign di pemeriksaan
            $this->load->model('mviewql');
            $listpaket = $this->mviewql->viewpaketvitalsign($idjadwal);//list vitalsign paket
            if(!empty($listpaket))
            {
                foreach ($listpaket as $obj)
                {
                    $dataperiksa = ['idpemeriksaan'=>$idpemeriksaan,'idpendaftaran'=>$idpendaftaran,'icd'=>$obj->icd];
                    $arrMsg = $this->db->insert("rs_hasilpemeriksaan",$dataperiksa);
                    if(!$arrMsg) { return json_encode(['status'=>'danger', 'isi'=>'Save Paket Vitalsign Failed..!']);}
                }
            }
            $this->load->model('mtriggermanual');
            $this->mtriggermanual->airs_pemeriksaan($idjadwal);
            $datal = [
                'url'     => get_url(),
                'log'     => 'user:'.$this->session->userdata('username').';status:'.$data['status'].';idpendaftaran:'.$idpendaftaran.';norm:'.$this->input->post('rm'),
                'expired' =>date('Y-m-d', strtotime("+1 weeks", strtotime($this->input->post('tanggal'))))
            ];
            $this->mgenerikbap->setTable('login_log');
            $arrMsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($datal);
            // set antrian periksa
            $loket = $this->db->query("select * from antrian_loket where idloket='39'")->row_array();
            $this->setNomorantrianAnjungan($idunit,$idpemeriksaan,$idperson,$loket['namaloket'],$nopesan);
        }
    }
    // antrian periksa anjungan
    // data-> idunit,idpemeriksaan,idperson,nama loket pendaftaran, nopesan pendaftaran
    public function setNomorantrianAnjungan($idunit,$idpemeriksaan,$idperson,$namaloket,$nopesan)
    {
        $callid         = generaterandom(8);
        $akronimunit = $this->mgenerikbap->select_multitable("rj.idloket,idstasiun,ru.akronimunit,namaloket","rs_pemeriksaan,rs_unit,rs_jadwal,antrian_loket",['idpemeriksaan'=>$idpemeriksaan])->row_array();
        if (empty($akronimunit))
        {
            $unit = $this->mgenerikbap->select_multitable("namaunit","rs_unit",['idunit'=>$idunit])->row_array();
            echo json_encode(['err'=>'ERR_LOKET', 'namaunit' => $unit['namaunit']]);
        }
        else
        {
            $this->mgenerikbap->setTable('helper_autonumber');
            $data = [
                'id'      =>'Antrian'.date('Ymd',strtotime($this->input->post('tanggal'))).'-'.$akronimunit['idstasiun'].'-'.$akronimunit['idloket'],
                'number'  =>null,
                'callid'  =>$callid,
                'expired' =>date('Y-m-d',strtotime($this->input->post('tanggal')))
            ];
            $this->mgenerikbap->insert($data);
            $noantrian = $this->mgenerikbap->select_multitable('number','helper_autonumber',['callid'=>$callid])->row_array()['number'];//siapkan data no antrian
            // jika pasien bpjs maka status pesan jika bukan status antri
            $datau = [
                'noantrian' => $noantrian,
                'status'    => 'antrian'
            ];
            //tambah ke pemeriksaan dengan status pesan, sehingga pasien harus verifikasi data di pendaftaran dulu
            $this->mgenerikbap->setTable('rs_pemeriksaan');
            $this->mgenerikbap->update_or_insert_ignoreduplicate($datau, $idpemeriksaan);
            $dataa = ['idloket'=>$akronimunit['idloket'], 'tanggal'=>date('Y-m-d',strtotime($this->input->post('tanggal'))), 'no'=>$noantrian, 'status'=>'antri', 'idperson'=>$idperson];
            $this->db->insert('antrian_antrian',$dataa);
            $datapasien = $this->mgenerikbap->select_multitable("norm, namalengkap, alamat","person_pasien, person_person",['pper.idperson'=>$idperson])->row_array();

            // ambil nama dokter
            $dtdokter = $this->db->query("SELECT CONCAT( IFNULL(pp.titeldepan, ''),' ', IFNULL(pper.namalengkap,''),' ', IFNULL(pp.titelbelakang,'')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai pp, person_person pper where rp.idpemeriksaan='$idpemeriksaan' and rj.idjadwal = rp.idjadwal and pp.idpegawai = rj.idpegawaidokter and pper.idperson = pp.idperson")->row_array();
            // end ambil nama dokter
            // data yang di kirim kan noantrian->antrianperiksa, data pasien, namaloket->namaloketpemeriksaan, dokter,loketdaftar->namaloketpendaftaran,antriandaftar->nomorantrianpendaftaran
            echo json_encode(['status'=>'success','noantrian'=>$noantrian, 'datapasien'=>$datapasien, 'namaloket'=>$akronimunit['namaloket'],'dokter'=>$dtdokter,'loketdaftar'=>$namaloket, 'antridaftar'=>$nopesan]);
        }
        
    }
        
    
    
}
/* End of file ${TM_FILENAME:${1/(.+)/Canjunganpasien.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Canjunganpasien/:application/controllers/${1/(.+)/Canjunganpasien.php/}} */






