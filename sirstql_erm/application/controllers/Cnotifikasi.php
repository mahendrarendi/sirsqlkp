<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
// define dan nilai di tabel login_halaman harus sama
// defined('V_PEMERIKSAANKLINIK') OR define('V_PEMERIKSAANKLINIK', 24);
// defined('V_ADMINISTRASIANTRIAN') OR define('V_ADMINISTRASIANTRIAN', 29);

class Cnotifikasi extends MY_controller 
{
    function __construct()
    {
	   parent::__construct();
       // jika user belum login maka akses ditolak
       if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
    }
    public function notif_farmasi(){echo json_encode($this->db->query("SELECT count(1) as jumlah FROM person_pendaftaran p left join rs_barangpemeriksaan bp on p.idpendaftaran=bp.idpendaftaran left join rs_barang b on b.idbarang=bp.idbarang left join rs_pemeriksaan rp on rp.idpendaftaran=p.idpendaftaran and rp.status='sedang ditangani' where date(rp.waktu)=date(now()) and not isnull(keteranganobat) and trim(keteranganobat)<>'' and isnull(idbarangpemeriksaan) and (jenis='obat' or isnull(jenis)) and rp.status='sedang ditangani'")->result());}
    // mahmud, clear
    public function notif_antrianugd()
    {
        echo json_encode($this->db->query("select a.idloket, l.namaloket, l.fontawesome, count(idantrian) as antriandiambil, sum(if(status!='ok',1,0)) as antri from antrian_antrian a join antrian_loket l on l.idloket=a.idloket where tanggal=date(now()) and l.namaloket like '%ugd%' or tanggal=date(now()) and l.namaloket like'%poli umum%' group by idloket")->result());
    }
    
    
    public function notif_pelayananfarmasi()
    {
        $query = "SELECT  rp.idpemeriksaan, rp.norm, namapasien(ppas.idperson) as namapasien FROM rs_pemeriksaan rp 
            join person_pasien ppas on ppas.norm = rp.norm
            join rs_unit ru on ru.idunit = rp.idunit and ru.notifikasilayananfarmasi = 1
            join person_pendaftaran pp on pp.idpendaftaran = rp.idpendaftaran
            left join rs_barangpemeriksaan rb on rb.idpemeriksaan = rp.idpemeriksaan
            where ru.notifikasilayananfarmasi = 1 and rp.dilayanifarmasi = 0 and  (not isnull(keteranganobat) and trim(keteranganobat)<>'' or not isnull(idbarangpemeriksaan) )
            GROUP by rp.idpemeriksaan";
        $sql = $this->db->query($query)->result_array();
        echo json_encode($sql);        
    }
    
    public function update_pelayananfarmasi()
    {
        $id = $this->input->post('i');
        $msg = $this->db->update('rs_pemeriksaan',['dilayanifarmasi'=>1],['idpemeriksaan'=>$id]);
        echo json_encode($msg);
    }
}
/* End of file ${TM_FILENAME:${1/(.+)/Cnotifikasi.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Cnotifikasi/:application/controllers/${1/(.+)/Cnotifikasi.php/}} */






