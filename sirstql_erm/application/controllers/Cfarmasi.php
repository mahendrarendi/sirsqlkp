<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Cfarmasi extends MY_controller 
{
    function __construct()
    {
	    parent::__construct();
        // jika user belum login maka akses ditolak
        if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
        $this->load->model('mmasterdata');
        $this->load->model('mdatatable');
        $this->load->model('mviewql');
        
    }
    
    ///////////////////// Data Stok Barang /////////////////
    public function datastokbarang()
    {
        if (empty(!$this->session->userdata('unitterpilih')))
        {
            if( $this->session->userdata('levelgudang') != 'depo')
            {
                $data['unitfarmasi'] = (($this->session->userdata('levelgudang') == 'farmasi') ? $this->db->get_where('rs_unit_farmasi',['idunitparent'=>$this->session->userdata('idunitterpilih')])->result_array()  : $this->db->get('rs_unit_farmasi')->result_array() );
            }
            
            $data['content_view']      = 'farmasi/v_datastokbarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'datastokbarang';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Stok Barang '.$this->session->userdata('unitterpilih');
            $data['mode']        = 'view';
            $data['plugins']     = [ PLUG_DATATABLE,PLUG_DROPDOWN ];
            $data['jsmode']      = 'datastokbarang';
            $data['script_js']   = ['js_inventarisbarang'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public  function dt_datastokbarang() //menampilkan stok barang sementara
    {
        $list   = $this->mdatatable->dt_datastokbarang_(true);
        $data=[];
        $no=$_POST['start'];
        if($list){
            foreach ($list->result() as $obj) {         
                $row=[];
                $row[] = ++$no;    
                $row[] = $obj->kode;
                $row[] = $obj->namabarang;
                $row[] = convertToRupiah($obj->stok);
                $row[] = $obj->namasatuan;
                $row[] = $obj->namasediaan;
                $row[] = $obj->jenis;
                $row[] = $obj->tipeobat;
                $row[] = floattostr($obj->kekuatan);
                $data[] = $row;
            }
        }
        echo json_encode([
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_datastokbarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_datastokbarang(),
            "data" => $data
        ]);
    }
    
    ///////////////////////MASTERDATA BARANG ///////////////////
    //-------------------------- ^^ Standar
    public function settingbarang()
    {
        return [    'content_view'      => 'farmasi/v_barang',
                    'active_menu'       => 'farmasi',
                    'active_sub_menu'   => 'barang',
                    'active_menu_level' => ''
               ];
    }
    public function barang()
    {
	if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                = $this->settingbarang();
            $data['title_page']  = 'Barang';
            $data['mode']        = 'view';
            $data['table_title'] = 'Data Barang';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['jsmode']      = 'list';
            $data['script_js']   = ['js_inventarisbarang'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    // -- tampilkan data master barang
    public  function dt_barang() //menampilkan stok barang sementara
    {
        $list   = $this->mdatatable->dt_barang_(true);

        $no = $_POST['start'];
        foreach ($list->result() as $obj) {
            $this->encryptbap->generatekey_once("HIDDENTABEL");
            $id           = $this->encryptbap->encrypt_urlsafe($obj->idbarang, "json");
            $tabel        = $this->encryptbap->encrypt_urlsafe('rs_barang', "json");
            $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_BARANG, "json");
            
            $menu  = '<a data-toggle="tooltip" id="btnRiwayatBarang" kodebrg="'.$obj->kode.'" namabrg="'.$obj->namabarang.'" dt="'.$id.'" data-original-title="History" class="btn btn-info btn-xs"><i class="fa fa-history"></i></a>';
            if($obj->ishidden == 0)
            {
                $menu .= ' <a data-toggle="tooltip" title="" data-original-title="Edit" class="btn btn-warning btn-xs" href="'.base_url('cfarmasi/edit_barang/'.$id).'" ><i class="fa fa-pencil"></i></a>';
                $menu .= ' <a data-toggle="tooltip" id="btnHiddenBarang" status="'.$obj->ishidden.'" kodebrg="'.$obj->kode.'" namabrg="'.$obj->namabarang.'" idbarang="'.$obj->idbarang.'" data-original-title="Hidden Barang" class="btn btn-danger btn-xs"><i class="fa fa-ban"></i></a>';
                
                if($this->pageaccessrightbap->checkAccessRight(V_MENU_HAPUS_DATABARANG))
                {
                    $menu .= ' <a data-toggle="tooltip" title="" data-original-title="Delete" id="delete_data" nobaris="'.($no-1).'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'"> <i class="fa fa-trash"></i></a>';
                }
            }
            else
            {
                $menu .= ' <a data-toggle="tooltip" id="btnHiddenBarang" status="'.$obj->ishidden.'" kodebrg="'.$obj->kode.'" namabrg="'.$obj->namabarang.'" idbarang="'.$obj->idbarang.'" data-original-title="Open Hidden Barang" class="btn btn-success btn-xs"><i class="fa fa-ban"></i></a>';
            }
                        
            $row=[];
            $row[] = $obj->kode;
            $row[] = $obj->namabarang;
            $row[] = convertToRupiah($obj->stok);
            $row[] = '<span class="rop-'.status_ROP($obj->stok,$obj->stokaman,$obj->stokminimal).'">'.$obj->rop.'</span>';
            $row[] = $obj->jenistarif;
            $row[] = $obj->namasatuan;
            $row[] = $obj->namasediaan;
            $row[] = $obj->jenis;
            $row[] = $obj->tipeobat;
            $row[] = convertToRupiah($obj->hargabeli);
            $row[] = convertToRupiah($obj->hargajual);      
            $row[] = convertToRupiah($obj->hargaumum);
            $row[] = convertToRupiah($obj->het);      
            $row[] = $obj->kekuatan;
            $row[] = ($obj->statusbarang) ? '<span class="label label-success">Digunakan</span>' : '<span class="label label-danger">Tidak Digunakan</span>' ;
            $row[] = $menu; 
            $data[] = $row;
        }
        echo json_encode([
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_barang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_barang(),
            "data" => $data
        ]);
    }
    
    public function dt_riwayatobat()
    {
        $getData = $this->mdatatable->dt_riwayatobat_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->namapbf;
                    $row[] = $obj->distributor;
                    $row[] = $obj->waktu;
                    $row[] = $obj->batchno;
                    $row[] = $obj->kadaluarsa;
                    $row[] = convertToRupiah($obj->hargabeli);
                    $row[] = convertToRupiah($obj->nominaldiskon);
                    $row[] = $obj->jumlah;
                    $row[] = $obj->jenisdistribusi;
                    $row[] = $obj->unitasal;
                    $row[] = $obj->unittujuan;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_riwayatobat(),
            "recordsFiltered" => $this->mdatatable->filter_dt_riwayatobat(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    /** Update Memey */
    // -- add data barang
    public function add_barang()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                  = $this->settingbarang(); //letakkan di baris pertama
            $this->load->helper('form');
            $data['title_page']    = 'Add Barang';
            $data['mode']          = 'add';
            $data['jsmode']        = 'form';
            $data['dt_jenis']      = $this->mmasterdata->get_data_enum('rs_barang','jenis');
            $data['dt_satuan']     = $this->db->get('rs_satuan')->result();
            $data['dt_jenistarif'] = $this->db->get('rs_jenistarif')->result();
            $data['dt_pabrik']     = $this->db->get('rs_barangpabrik')->result();
            $data['data_sediaan']  = $this->db->get('rs_sediaan')->result();
            $data['dt_tipeobat']   = $this->mmasterdata->get_data_enum('rs_barang','tipeobat');
            $data['dt_jenisjaminan']   = $this->mmasterdata->get_data_enum('rs_barang','jenisjaminan');
            $data['data_edit']     = '';
            $data['plugins']       = [ PLUG_DROPDOWN , PLUG_CHECKBOX ];
            $data['script_js']     = ['js_inventarisbarang'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
    /** End */
    // --  
    public function barang_carigolbyjenis()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $this->mgenerikbap->setTable('rs_barang_golongan');
            $jenis = $this->input->post('jenis');
            echo json_encode($this->mgenerikbap->select('*',['jenis'=>$jenis])->result());
        }
        else
        {
            aksesditolak();
        }
    }

    /** Update Memey */
    public function edit_barang() //edit data pegawai
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $data                 = $this->settingbarang(); //letakkan di baris pertama
            $this->load->helper('form'); //load form helper
            $data['title_page']   = 'Edit Barang'; //judul halaman
            $data['mode']         = 'edit';
            $data['jsmode']       = 'form';
            $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->uri->segment('3')));
            $data['dt_jenis']      = $this->mmasterdata->get_data_enum('rs_barang','jenis');
            $data['dt_satuan']     = $this->db->get('rs_satuan')->result();
            $data['dt_jenistarif'] = $this->db->get('rs_jenistarif')->result();
            $data['dt_pabrik']     = $this->db->get('rs_barangpabrik')->result();
            $data['data_sediaan']  = $this->db->get('rs_sediaan')->result();
            $data['dt_tipeobat']   = $this->mmasterdata->get_data_enum('rs_barang','tipeobat');
            $data['dt_jenisjaminan']   = $this->mmasterdata->get_data_enum('rs_barang','jenisjaminan');
            $data['data_edit']     = $this->db->query("select * from rs_barang a where a.idbarang='".$id."'")->row_array();
            $data['data_golongan'] = $this->db->get_where('rs_barang_golongan',['jenis'=>$data['data_edit']['jenis']])->result();
            $data['plugins']       = [ PLUG_DROPDOWN,PLUG_CHECKBOX];
            $data['script_js']     = ['js_inventarisbarang'];
            $this->load->view('v_index', $data);
        }
        else
        {
            aksesditolak();
        }
    }
   
    public function save_barang() // SIMPAN KECAMATAN
    {	
        if ($this->pageaccessrightbap->checkAccessRight(V_BARANG)) //lihat define di atas
        {
            $this->load->library('form_validation');
            //Validasi Masukan
            $this->form_validation->set_rules('idsatuan','','required');
            $this->form_validation->set_rules('idsediaan','','required');
            $this->form_validation->set_rules('idjenistarif','','required');
            $this->form_validation->set_rules('namabarang','','required');
	    $this->form_validation->set_rules('keluhan','','');
            $this->form_validation->set_rules('jenis','','required');
            $this->form_validation->set_rules('golongan','','required');
            $this->form_validation->set_rules('hargabeli','','required');
            $this->form_validation->set_rules('persenmargin','','');
            // $this->form_validation->set_rules('tatacara','','');
            $this->form_validation->set_rules('kekuatan','','required');
            
            if (validation_input()) //Jika tidak Valid
            {
                //Persiapkan data
                $post = $this->input->post();
                $idbarang = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $post['idbarang']));
                $data['namabarang'] = $post['namabarang'];
                $data['stokminimal']= $post['stokminimal'];
                $data['stokhabis']  = $post['stokhabis'];
                $data['stokaman']   = $post['stokaman'];
                $data['rop']        = $post['rop'];
                $data['idjenistarif']=$post['idjenistarif'];
                $data['idsatuan']   = $post['idsatuan'];
                $data['idsediaan']  = $post['idsediaan'];
                $data['jenis']      = $post['jenis'];
                $data['idgolongan'] = $post['golongan'];
                $data['tipeobat']   = $post['tipeobat'];
                $data['jenisjaminan']   = $post['jenisjaminan'];
                $data['hargabeli']  = $post['hargabeli'];
                $data['persenmargin']=$post['persenmargin'];                
                $data['persenmargin_hargaumum']=$post['persenmargin_hargaumum'];
                $data['het']        = $post['het'];
                $data['kekuatan']   = $post['kekuatan'];
                $data['keluhan']    = $post['keluhan'];
                $data['kandungan']  = $post['kandungan'];
                $data['indikasi']   = $post['indikasi'];
                $data['kontraindikasi'] = $post['kontraindikasi'];
                $data['efeksamping']    = $post['efeksamping'];
                $data['idbarangpabrik'] = $post['idbarangpabrik'];
                $data['isfornas']       = (($post['isfornas']) ? 1 : 0 );
                $data['margin_manual']  = (($post['margin_manual']) ? 1 : 0 );
                $this->mgenerikbap->setTable('rs_barang');
                $simpan = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $idbarang);
                
                $datalog = [
                    'url'     => get_url(),
                    'log'     => 'user:'.$this->session->userdata('username').';'.((empty($idbarang)) ? ' Insert Barang ' : ' Ubah Barang ; ' ).'Nama:'.$post['namabarang'].'; Harga Beli:'.$post['hargabeli'].'; Margin BPJS :'.$post['persenmargin'].';Margin Umum:'.$post['persenmargin_hargaumum'].';HET : '.$post['het'],
                    'expired' => date('Y-m-d', strtotime("+48 weeks", strtotime(date('Y-m-d'))))
                ];
                $this->mgenerikbap->setTable('login_log');
                $this->mgenerikbap->update_or_insert_ignoreduplicate($datalog);
                
                //jika simpan status success selain itu status error
                pesan_success_danger($simpan, empty($idbarang) ? 'Save Success.!' :'Update Success.!' , empty($idbarang) ? 'Save  Failed.!' :'Update Failed.!' );
            }
            redirect(base_url('cfarmasi/barang'));
        }
    }
    /** End */
    
    public function hapusbarang()
    {
        if ($this->pageaccessrightbap->checkAccessRight($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('hal'))))
        {
            $tableName=$this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('t'));
            $id = $this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('i'));
            $arrMsg = $this->db->update($tableName,['statusbarang'=>'0'],['idbarang'=>$id]);
            pesan_success_danger($arrMsg, 'Delete Success.!', 'Delete Failed.!', 'js');
        }
    }
    
    public function hiddenbarang()
    {
        $idbarang = $this->input->post('idbarang');
        $status   = (($this->input->post('status')==0) ? 1 : 0 );
        $msg      = (($status == '1')? 'Show':'Hidden');
        $arrMsg   = $this->db->update('rs_barang',['ishidden'=>$status],['idbarang'=>$idbarang]);
        pesan_success_danger($arrMsg, $msg.' Success.!', $msg.'Failed.!', 'js');
    }


    //total kekayaan obat dan bhp berdasarkan unit
    public function totalkekayanobatdanbhp()
    {
        if(empty(!$this->session->userdata('idunitterpilih')))
        {
            $data = $this->db->query("select sum(a.stok * b.hargabeli) as totalkekayaan from rs_barang_stok a join rs_barang_pembelian b on b.idbarangpembelian=a.idbarangpembelian WHERE idunit = '".$this->session->userdata('idunitterpilih')."' and stok > 0");
            if($data->num_rows() > 0)
            {
                $data = $data->row_array();
            }
            else
            {
                $data = null;
            }
        }
        else 
        {
            $data = null;
        }
        
        echo json_encode($data);
    }
    
   //transformasi barang
   public function transformasibarang()
    {
       if($this->pageaccessrightbap->checkAccessRight(V_TRANSFORMASIBARANG) && ( $this->session->userdata('levelgudang') == 'farmasi' ))
       {
           $data = [
            'content_view'     =>'farmasi/v_distribusibarang',
            'active_menu'      =>'farmasi',
            'active_sub_menu'  =>'transformasibarang',
            'active_menu_level'=>'',
            'title_page'       =>'Transformasi Barang',
            'mode'             =>'transformasibarang',
            'plugins'          =>[ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            'jsmode'           =>'transformasibarang',
            'script_js'        =>['farmasi/js_inventaris_transformasi']
            ];
           $this->load->view('v_index',$data);
       }
       else
       {
           aksesditolak();
       }
    }
    public function dt_transformasibarang()
    {
        $getData = $this->mdatatable->dt_transformasibarang_(true);
        $db = $this->db->last_query();
        $data=[]; $no=0;
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->transformasiasal;
                    $row[] = $obj->transformasihasil;
                    $row[] = $obj->waktu;
                    $row[] = '<a id="view" waktu="'.$obj->waktu.'" grupwaktu="'.$obj->grupwaktu.'" setundo="0" '.ql_tooltip('lihat transformasi').' class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>';
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_transformasibarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_transformasibarang(),
            "data" =>$data,
            "db"=>$db
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function addtransformasibarang()
    {
       if($this->pageaccessrightbap->checkAccessRight(V_TRANSFORMASIBARANG) && $this->session->userdata('levelgudang') == 'farmasi' )
       {
           $data = [
            'content_view'     =>'farmasi/v_distribusibarang',
            'active_menu'      =>'farmasi',
            'active_sub_menu'  =>'transformasibarang',
            'active_menu_level'=>'',
            'title_page'       =>'Transformasi Barang',
            'mode'             =>'addtransformasibarang',
            'plugins'          =>[ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            'jsmode'           =>'addtransformasibarang',
            'script_js'        =>['farmasi/js_farmasi','farmasi/js_distribusibarang'],
            'unit'             => $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result(),
            'jenisdistribusi'  => 'transformasi'
            ];
           $this->load->view('v_index',$data);
       }
       else
       {
           aksesditolak();
       }
    }
    
    //mahmud, clear
    //****SAVE TRANSFORMASI BARANG
    public function save_transformasibarang()
    {
        // SIAPKAN DATA
        $idpersonpetugas =  $this->get_iduserlogin();
        $idunit = $this->input->post('idunitasal');
        $idunittujuan = $this->input->post('idunittujuan');
        $barangkeluar=$this->input->post('barangkeluar');
        $index=$this->input->post('index');
        //siapkan data distribusi keluar
        $jumlahkeluar     =$this->input->post('jumlahkeluar');// sample: 1,2
        $idbarangpembelian=$this->input->post('idbarangpembelian'); // sample: 1,2,
        $kadaluarsa=$this->input->post('kadaluarsa'); // sample: 1,2,
        $batchno=$this->input->post('batchno'); // sample: 1,2,
        $hargabeli=$this->input->post('hargabeli'); // sample: 1,2,
        
        // siapkan data rs barang pembelian
        $idbarang=$this->input->post('idbarang');// sample: 1,2,3,4,5,6
        
        //siapkan data distribusi masuk
        $jumlahdistribusi =$this->input->post('jumlahdistribusi');// sample: 1,2,3,4,5,6
        $jumlahpembetulan =$this->input->post('jumlahpembetulan');// sample: 1,2,3,4,5,6
        
        //siapkan pembeda transformasi
        $waktu      = date('Y-m-d');
        
        $parent=0;
        $grupwaktu  = generaterandom(4, true);
       
        for($x=0;$x<count($barangkeluar);$x++)
        {
            $grupasal  = generaterandom(4, true);
            // save rs_barang_distribusi dengan jenisdistribusi=transformasiasal
            if( !empty($this->input->post('jumlahpembetulan'.$index[$parent])[$x]) ){
               $this->db->insert('rs_barang_distribusi',['waktu'=>$waktu,'grupwaktu'=>$grupwaktu,'grupasal'=>$grupasal,'idbarangpembelian'=>$idbarangpembelian[$x],'idunit'=>$idunit,'jumlah'=>$this->input->post('jumlahkeluar')[$x],'jenisdistribusi'=>'pembetulantransformasiasal','idunittujuan'=>$idunittujuan,'idpersonpetugas'=>$idpersonpetugas]);
               $rbdasal=['waktu'=>$waktu,'grupwaktu'=>$grupwaktu,'grupasal'=>$grupasal,'idbarangpembelian'=>$idbarangpembelian[$x],'idunit'=>$idunit,'jumlah'=>$this->input->post('jumlahkeluar')[$x],'jenisdistribusi'=>'transformasiasal','idunittujuan'=>$idunittujuan,'idpersonpetugas'=>$idpersonpetugas];
               $this->db->insert('rs_barang_distribusi',$rbdasal);
            }else{
               $rbdasal=['waktu'=>$waktu,'grupwaktu'=>$grupwaktu,'grupasal'=>$grupasal,'idbarangpembelian'=>$idbarangpembelian[$x],'idunit'=>$idunit,'jumlah'=>$this->input->post('jumlahkeluar')[$x],'jenisdistribusi'=>'transformasiasal','idunittujuan'=>$idunittujuan,'idpersonpetugas'=>$idpersonpetugas];
               $this->db->insert('rs_barang_distribusi',$rbdasal);
            }
            
            
            for($i=0;$i<count($this->input->post('barangmasuk'.$index[$parent]));$i++)
            {                
                // save rs_barang_distribusi :: pembetulantransformasihasil
                if(($this->input->post('jumlahpembetulan'.$index[$parent])[$i] != $this->input->post('jumlahdistribusi'.$index[$parent])[$i]) && empty(!$this->input->post('jumlahpembetulan'.$index[$parent])[$i]) ){
                    $idbarangpembelianhasil = $this->input->post('idbarangpembelianasal'.$index[$parent])[$i];
                    $rbpembetulan=['waktu'=>$waktu,'grupwaktu'=>$grupwaktu,'grupasal'=>$grupasal,'idbarangpembelian'=>$idbarangpembelianhasil,'idunit'=>$idunit,'jumlah'=>$this->input->post('jumlahpembetulan'.$index[$parent])[$i],'jenisdistribusi'=>'pembetulantransformasihasil','idunittujuan'=>$idunittujuan,'idpersonpetugas'=>$idpersonpetugas];
                    $this->db->insert('rs_barang_distribusi',$rbpembetulan);
                }

                if( !isset($this->input->post('jumlahpembetulan'.$index[$parent])[$i] ) ){ //insert ke barang pembelian pembelian
                    $dtbarangpembelian=['idbarang'=>$this->input->post('idbarang'.$index[$parent])[$i],'batchno'=>$batchno[$parent],'kadaluarsa'=>format_date_to_sql($kadaluarsa[$parent]),'parentidbarangpembelian'=>$idbarangpembelian[$parent],'hargabeli'=>$hargabeli[$parent]];
                    $this->db->insert('rs_barang_pembelian',$dtbarangpembelian);
                    $idbarangpembelianhasil=$this->db->insert_id();
                }
                
                // save rs_barang_distribusi :: transformasihasil
                $rbdhasil=['waktu'=>$waktu,'grupwaktu'=>$grupwaktu,'grupasal'=>$grupasal,'idbarangpembelian'=>$idbarangpembelianhasil,'idunit'=>$idunit,'jumlah'=>$this->input->post('jumlahdistribusi'.$index[$parent])[$i],'jenisdistribusi'=>'transformasihasil','idunittujuan'=>$idunittujuan,'idpersonpetugas'=>$idpersonpetugas];
                $this->db->insert('rs_barang_distribusi',$rbdhasil);
            }
            ++$parent;
        }
        redirect(base_url('cfarmasi/transformasibarang'));
    }
    
    public function jsonvpembelian()
    {
        $idbarangfaktur = $this->input->post('idbarangfaktur');
        $idfaktur = $idbarangfaktur;
        $dt_faktur    = $this->db->select('rbf.*, (select distributor from rs_barang_distributor where idbarangdistributor=rbf.idbarangdistributor) distributor')->get_where('rs_barang_faktur rbf',['rbf.idbarangfaktur'=>$idbarangfaktur])->row_array();
        $dt_pembelian = $this->db->query("SELECT rbd.idbarangdistribusi, rbd.idbarangpembelian, rb.idbarang,kode,rbp.het, namabarang, rbp.hargabeli,rbp.hargaasli, namasatuan, CAST(jumlah AS UNSIGNED) as jumlah,rbp.batchno, rbp.kadaluarsa, rbp.persendiskon, rbp.nominaldiskon from rs_barang_faktur rbf, rs_barang_pembelian rbp, rs_barang_distribusi rbd, rs_barang rb, rs_satuan rs where rbp.idbarangfaktur=rbf.idbarangfaktur and (rbd.idbarangpembelian=rbp.idbarangpembelian and jenisdistribusi='masuk') and rb.idbarang=rbp.idbarang and rs.idsatuan=rb.idsatuan and rbf.idbarangfaktur = '".$idbarangfaktur."'")->result();
        echo json_encode(['pembelian'=>$dt_pembelian,'faktur'=>$dt_faktur,'idfaktur'=>$idfaktur]);
    }
    
    public function json_pilihdistributor()
    {
        $offset = ($this->input->get('page') * 20 ) - 20;
        echo json_encode($this->db->query("SELECT idbarangdistributor, distributor from rs_barang_distributor where distributor like '%".$this->input->get('q')."%' limit 20 offset ".$offset)->result());
    }
    
    public function jsonbrwlname()
    {
        // cari barang : retur, pesan, transformasi tujuan
        $namaobat = $this->input->post('namaobat');
        $mode   = $this->input->post('mode');
        $idunit = (($mode=='retur') ? $this->session->userdata('idunitterpilih') : $this->input->post('unittujuan') );        
        $query= (($mode)=='retur' ? "SELECT concat(rb.idbarang,',',rb.kode,',',rb.namabarang,',',namasatuan,',',rb.hargabeli,',',0,',',rbp.kadaluarsa,',',rbp.batchno,',',0,',',bs.idbarangpembelian) as id, concat(rb.kode,' ',rb.namabarang,' ED:',rbp.kadaluarsa,' BATCH.No:',rbp.batchno,' Stok:',bs.stok,' ',namasatuan) as text  from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian  and bs.idunit='".$idunit."' where rb.ishidden=0 and rs.idsatuan=rb.idsatuan and bs.stok > 0 and namabarang like '%".$namaobat."%' limit 20" : "SELECT concat(rb.idbarang,',',kode,',',namabarang,',',namasatuan,',', rb.hargabeli,',',0,',',rb.het) as id, concat(kode,' ',namabarang,' ( stok ', ifnull(sum(bs.stok),0) ,' ',namasatuan ,' )') as text from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian and bs.idunit='".$idunit."' where rs.idsatuan=rb.idsatuan  and namabarang like '%".$namaobat."%' GROUP by rb.idbarang limit 20" );
        echo json_encode($this->db->query($query)->result());
    }
    
    public  function jsontampilpengembalianbarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsonbarangpengembalian()->result());
    }
    
    // cari harga barang per id
    public function cekhargabelimasterbarang()
    {
        if($this->session->userdata('levelgudang') == 'gudang')
        {
            $q = $this->db->select('kode,namabarang, hargabeli')->get_where('rs_barang',['idbarang'=>$this->input->post('idbarang')]);
            if($q->num_rows() > 0)
            {
                echo json_encode(['status'=>'ok','data'=>$q->row_array()]);
            }
            else
            {
                echo json_encode(['status'=>'kosong']);
            }
        }
        else
        {
            echo json_encode(['status'=>'kosong']);
        }
    }
    
    
    //barang datang - pesan ke distributor
    public function barangdatang()
    {
//        if( $this->session->userdata('levelgudang') == 'gudang' )
//        {
                $data['content_view']      = 'farmasi/v_distribusibarang';
                $data['active_menu']       = 'farmasi';
                $data['active_sub_menu']   = 'barangdatang';
                $data['active_menu_level'] = '';
                $data['title_page']  = 'Barang Datang';
                $data['mode']        = 'distribusibarang';
                $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
                $data['jsmode']      = 'tampilbelanja';
                $data['script_js']   = ['farmasi/js_inventaris_barangdatang'];
                $data['active_menu_levelchild']='barangdatang';
                $this->load->view('v_index',$data);
           
//        }
//        else if(empty(! $this->session->userdata('unitterpilih') ))
//        {
//           return $this->pesanbarang();
//        }
//        else
//        {
//            aksesditolak();
//        }
            
    }
    
    //tambah barang datang    
    public function tambahbarangdatang()
    {
//        if ( $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_distribusibarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'barangdatang';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Barang Datang';
            $data['mode']        = 'belanja';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = 'belanja';
            $data['jenisdistribusi']='masuk';
            $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_distribusibarang'];
            $data['unit'] = $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
            $data['unittujuan'] = [];
            
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    //mahmud, clear
    public function save_barangpembelianfaktur()
    {
//        if ( $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
//        {
//        siapkan barang faktur
            $post = $this->input->post();
//            $idfaktur = json_decode($this->encryptbap->decrypt_urlsafe("FAKTURBELANJA", $post['idbarangfaktur']));
            $idfaktur=$post['idbarangfaktur'];
            $statusfaktur = ($post['setterimabarang']) ? 'diterima' : 'rencana' ;
            $statustrx = $post['statusfaktur'];
            $data_barangfaktur = [
                'nofaktur'            => $post['nofaktur'],
                'nopesan'             => $post['nopesan'],
                'idbarangdistributor' => $post['idbarangdistributor'],
                'tanggalfaktur'       => date('Y-m-d', strtotime($post['tanggalfaktur'])),
                'tanggaljatuhtempo'   => date('Y-m-d', strtotime($post['tanggaljatuhtempo'])),
                'tagihan'             => unconvertToRupiah($post['tagihan']),
                'ppn'                 => unconvertToRupiah($post['ppn']),
                'potongan'            => unconvertToRupiah($post['potongan']),
                'idpersonpetugas'     => json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('iduser'))),
                'idunit'              => $this->session->userdata('idunitterpilih')
            ];
            $arrMsg   = (empty(!$idfaktur))?$this->db->update('rs_barang_faktur',$data_barangfaktur,['idbarangfaktur'=>$idfaktur]):$this->db->insert('rs_barang_faktur',$data_barangfaktur);
            $idfaktur = (empty(!$idfaktur)) ? $idfaktur : $this->db->insert_id();
                //simpan barang pembelian
                $idbarang = $post['idbarang'];
                $kode     = $post['kode'];
                $batchno  = $post['batchno'];
                $hargappn = $post['hargappn'];
                $harga    = $post['harga'];
                $persendiskon = $post['persendiskon'];
                $nominaldiskon= $post['nominaldiskon'];
                $kadaluarsa  = $post['kadaluarsa'];
                $het = $post['het'];
                
                //siapkan data distribusi
                $jumlah   = $post['jumlah'];
                $jumlahold= $post['jumlahold'];
                $idunit   = $post['idunitasal'];
                $jenisdistribusi= $post['jenisdistribusi'];
                $idpersonpetugas=json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('iduser')));
                for ( $x=0; $x<count($kode); $x++)
                {   //simpan barang pembelian
                    $dtbarangpembelian = ['idbarang'=>$idbarang[$x],'idbarangfaktur'=>$idfaktur,'batchno'=>$batchno[$x],'kadaluarsa'=>$kadaluarsa[$x],'hargabeli'=>unconvertToRupiah($hargappn[$x]),'hargaasli'=>unconvertToRupiah($harga[$x]),'persendiskon'=>$persendiskon[$x],'nominaldiskon'=>unconvertToRupiah($nominaldiskon[$x]),'het'=>unconvertToRupiah($het[$x])];
                    $this->mgenerikbap->setTable('rs_barang_pembelian');
                    $arrmsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($dtbarangpembelian, $post['idbarangpembelian'][$x]);
                   
                    $idbarangpembelian=(empty($post['idbarangpembelian'][$x]))?$this->db->insert_id():$post['idbarangpembelian'][$x];//set idbarang pembelian
                    //simpan barang distribusi
                    $dtbarangdistribusi = ['idbarangpembelian'=>$idbarangpembelian,'idunit'=>$idunit,'jumlah'=>unconvertToRupiah($jumlah[$x]),'jenisdistribusi'=>$jenisdistribusi,'idpersonpetugas'=>$idpersonpetugas, 'statusdistribusi'=> (($statustrx=='diterima') ? 'tidakdiubah' : 'bisadiubah') ];
                    // jika perubahan, status sudah diterima dan jumlah lama tidak sama jumlah terbaru
                    if($statustrx=='diterima' && (unconvertToRupiah($jumlah[$x]) != unconvertToRupiah($jumlahold[$x])) )
                    {
                        $arrmsg = $this->db->insert('rs_barang_distribusi', ['idbarangpembelian'=>$idbarangpembelian,'idunit'=>$idunit,'jumlah'=>unconvertToRupiah($jumlahold[$x]),'jenisdistribusi'=>'keluar','idpersonpetugas'=>$idpersonpetugas, 'statusdistribusi'=> 'tidakdiubah']);//keluar
                        $arrmsg = $this->db->insert('rs_barang_distribusi',['idbarangpembelian'=>$idbarangpembelian,'idunit'=>$idunit,'jumlah'=>unconvertToRupiah($jumlah[$x]),'jenisdistribusi'=>'masuk','idpersonpetugas'=>$idpersonpetugas, 'statusdistribusi'=> 'tidakdiubah']);//masuk
                    }
                    else
                    {
                        $this->mgenerikbap->setTable('rs_barang_distribusi');
                        $arrmsg = $this->mgenerikbap->update_or_insert_ignoreduplicate($dtbarangdistribusi, $post['idbarangdistribusi'][$x]);
                    }
                }
                ($statusfaktur=='diterima') ? $this->db->update('rs_barang_faktur',['statusfaktur'=>'diterima'],['idbarangfaktur'=>$idfaktur]) : '' ;
            $mode   = (empty($idfaktur))?'Belanja ':'Update Belanja ';
            pesan_success_danger($arrmsg,$mode.'barang berhasil.!',$mode.'barang gagal.!');
            redirect(base_url('cfarmasi/barangdatang'));
//        }
//       else
//       {
//           aksesditolak();
//       }
    }
    
    //ambil data detail belanja barang / barang datang
    public function jsondetailbelanjabarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsondetailbelanjabarang()->result());
    }
    
    //mahmud, clear
    //update faktur barang datang
    public function updatefaktur()
    {
        $arrMsg = $this->db->update('rs_barang_faktur',['statusfaktur'=>$this->input->post('stat')],['idbarangfaktur'=>$this->input->post('id')]);
        pesan_success_danger($arrMsg, 'Update Faktur Berhasil.!', 'Update Faktur Gagal.!', 'js');
    }
    
    public function distribusibelanjabarang()
    {
//        if ( $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_distribusibarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'barangdatang';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Barang Datang';
            $data['mode']        = 'belanja';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = 'belanja';
            $data['jenisdistribusi']='masuk';
            $data['script_js']   = ['farmasi/js_distribusibarang'];
            $data['unit'] = $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
            $data['unittujuan'] = [];
            
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    public function updatefaktur_ad_barangpembelian()
    {
        $post = $this->input->post();
        if($post['mode']=='ubah'){
            $arrmsg = $this->db->update('rs_barang_faktur',['potongan'=>$post['potongan'],'ppn'=>$post['ppn'],'tagihan'=>$post['tagihan']],['idbarangfaktur'=>$post['idbf']]);
        }else{
            $arrmsg = $this->db->update('rs_barang_faktur',['statusfaktur'=>'batal'],['idbarangfaktur'=>$post['idbf']]);           
        }
        echo json_encode($arrmsg);
    }
    
    // ubah hapus pembelian
    public function setdelete_barangpembelian()
    {
        $post = $this->input->post();
        $arrmsg = $this->db->delete('rs_barang_pembelian',['idbarangpembelian'=>$post['idbp']]);
        if($post['status']!='diterima'){
            $arrmsg = $this->db->delete('rs_barang_distribusi',['idbarangpembelian'=>$post['idbp']]);
        }else{
            $arrmsg = $this->db->query("insert into rs_barang_distribusi (`idbarangpembelian`, `idunit`, `jumlah`, `jenisdistribusi`, `idunittujuan`, `waktu`, `grupwaktu`, `grupasal`, `idbarangpemesanan`, `idpersonpetugas`, `statusdistribusi`) select idbarangpembelian, idunit, jumlah, 'keluar', idunittujuan, waktu, grupwaktu, grupasal, idbarangpemesanan, idpersonpetugas, statusdistribusi from rs_barang_distribusi where idbarangpembelian='".$post['idbp']."'");
        }
        echo json_encode($arrmsg);
    }
    
    //list data di gudang - permintaan dari farmasi
    public function permintaanfarmasi()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
        {
            $data['content_view']      = 'farmasi/v_permintaanfarmasi';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaanfarmasi';
            $data['active_menu_level'] = '';            
            $data['title_page']  = 'Permintaan Farmasi';
            $data['mode']        = 'permintaanfarmasi';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'tampilpesanan';
            $data['script_js']   = ['farmasi/js_permintaanfarmasi'];
            $data['active_menu_levelchild']='permintaanbarang';
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dt_permintaanfarmasi()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
        {
            $getData = $this->mdatatable->dt_permintaanbarang_(true);
            $data=[];
            if($getData){
                foreach ($getData->result() as $obj) {
                    $menu ='';
                    if($obj->statusbarangpemesanan == 'menunggu'){
                        $menu  .= ' <a onclick="kirimbarangpesanan('.$obj->idbarangpemesanan.',false)" '.ql_tooltip('kirim pesanan').' class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>';
                    }
                    if($obj->statusbarangpemesanan == 'selesai'){
                        $menu .= ' <a onclick="kirimbarangpesanan('.$obj->idbarangpemesanan.',\'pembetulankirim\')" '.ql_tooltip('pembetulan').' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>';
                    }
                    $menu .= ' <a  onclick="cetakbarangpesanan('.$obj->idbarangpemesanan.')" ' .ql_tooltip('cetak').' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                    $row = [];
                        $row[] = $obj->nofaktur;
                        $row[] = $obj->waktu;
                        $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                        $row[] = $obj->statusbarangpemesanan;
                        $row[] = enum_rs_barang_pemesanan($obj->jenistransaksi);
                        $row[] = $menu;
                    $data[] = $row;
                }
            }
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_permintaanbarang(),
                "recordsFiltered" => $this->mdatatable->filter_dt_permintaanbarang(),
                "data" =>$data
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public  function jsontampilpesananbarang()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->jsonbarangpesanan()->result());
    }
    
    public function getdata_pesananfarmasi()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->getdata_pesananfarmasi()->result());
    }
    //kirim barang dari gudang ke farmasi
    public function kirim_distribusibarang()
    {
         if ( $this->session->userdata('levelgudang') == 'gudang') //lihat define di atas
         {
            $data['content_view']      = 'farmasi/v_kirimpesananfarmasi';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaanfarmasi';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Kirim Pesanan Farmasi';
            $data['mode']        = 'kirimpermintaanfarmasi';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = '';
            $data['script_js']   = ['farmasi/js_kirimpesananfarmasi'];
            $data['unit']        = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'gudang' ])->result();
            $data['unittujuan']  = $this->db->get_where('rs_unit_farmasi',['levelgudang'=>'farmasi'])->result();//masih salah seharus nya otomatis unit yang dituju #NOTE
            $this->load->view('v_index',$data);
         }
         else
         {
             aksesditolak();
         }
    }
    
    public  function savekirimpesananfarmasi()
    {

       if( $this->session->userdata('levelgudang') == 'gudang' )
       { 
        $idunitasal       =$this->input->post("idunitasal");
        $idbarangpemesanan=$this->input->post("idbarangpemesanan");
        $idunittujuan     =$this->input->post("idunittujuan");
        $idbarangpembelian=$this->input->post('idbarangpembelian');
        $jumlah           =$this->input->post('jumlah');
        $jumlahpembetulan =$this->input->post('jumlahpembetulan');
        $modeubah         =$this->input->post('modeubah');
        $length           =$this->input->post('length');

        for($x=0; $x < count($length); $x++)
        {
             
            if(!(empty($jumlah[$x])) || !(empty($jumlahpembetulan[$x])) )
            {
             
                if($modeubah=='pembetulankirim'):
                   
                    if($jumlahpembetulan[$x] != $jumlah[$x])
                    {
                        if(!empty($jumlahpembetulan[$x]))
                        {
                            $pembetulan = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlahpembetulan[$x], 'jenisdistribusi' => 'pembetulankeluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                            $arrmsg = $this->db->insert('rs_barang_distribusi',$pembetulan);
                        }
                        $keluar = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                        $arrmsg = $this->db->insert('rs_barang_distribusi',$keluar);
                        $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$idbarangpemesanan]);
                    }
                    else
                    {
//                        
                    }
                else :
                    $data = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                    $arrmsg = $this->db->insert('rs_barang_distribusi',$data);
                    $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$idbarangpemesanan]);
                endif;

            }
        }
        pesan_success_danger( ($modeubah=='pembetulankirim') ? 1 : $arrmsg,'Simpan Berhasil.!','Simpan Gagal.!');
        redirect(base_url('cfarmasi/permintaanfarmasi'));
       }
       else
       {
           aksesditolak();
       }
    }
    
    public function distribusipesanbarang()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') != 'gudang' ) //lihat define di atas
        {
            $levelgudang = ( $this->session->userdata('levelgudang') == 'depo' ) ? 'gudang' :'depo' ;
            $data['content_view']      = 'farmasi/v_distribusibarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaanfarmasi';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Permintaan Barang';
            $data['mode']        = 'pesan';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = 'pesan';
            $data['script_js']   = ['farmasi/js_distribusibarang'];
            $data['unit'] = $this->db->get_where('rs_unit',['levelgudang'=> $levelgudang])->result(); 
            $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit')->result();
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    
    public function pesankegudang()
    {
//    if (empty(! $this->session->userdata('levelgudang') ) OR $this->session->userdata('levelgudang') != 'gudang' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_distribusibarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'pesankegudang';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Pesan Barang ke Gudang';
            $data['mode']        = 'pesanbarangkegudang';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'tampilpesanan';
            $data['script_js']   = ['farmasi/js_pesanbarangkegudang'];
            $data['active_menu_levelchild']='permintaanbarang';
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    public function dt_pesankegudang()
    {
        $getData = $this->mdatatable->dt_pesankegudang_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {
                $menu  = '';
                if($obj->statusbarangpemesanan=='menunggu'){
                    $menu .= ' <a id="batalpermintaan" alt="'.$obj->idbarangpemesanan.'" class="btn btn-danger btn-xs" '.ql_tooltip('batal').' ><i class="fa fa-minus-circle"></i></a>';
                    $menu .= ' <a  onclick="pembetulanpesan('.$obj->idbarangpemesanan.',\'pembetulanpesan\')" '.ql_tooltip('pembetulan').' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>';
                }
                if($obj->statusbarangpemesanan == 'batal'){
                    $menu .= ' <a id="buatpermintaanulang"  alt="'.$obj->idbarangpemesanan.'" class="btn btn-success btn-xs" '.ql_tooltip('pesan').' ><i class="fa fa-plus"></i></a>';
                }
                $menu .= ' <a  onclick="cetakbarangpesanan('.$obj->idbarangpemesanan.')" ' .ql_tooltip('cetak').' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                $row = [];
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->waktu;
                    $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                    $row[] = $obj->statusbarangpemesanan;
                    $row[] = enum_rs_barang_pemesanan($obj->jenistransaksi);
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_pesankegudang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_pesankegudang(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function addpesankegudang()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') != 'gudang' ) //lihat define di atas
        {
            $levelgudang = ( $this->session->userdata('levelgudang') == 'depo' ) ? 'gudang' :'depo' ;
            $data['content_view']      = 'farmasi/v_distribusibarang';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaanfarmasi';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Permintaan Barang';
            $data['mode']        = 'addpesankegudang';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = 'addpesankegudang';
            $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_distribusibarang'];
            $data['unit'] = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'gudang'])->result(); 
            $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
            
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    // save barang pemesanan dari farmasi ke gudang
    public function save_pesankegudang()
    {
        if( empty(! $this->session->userdata('levelgudang')) &&  $this->session->userdata('levelgudang') != 'gudang' )
        {
//        buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired
        $callid = generaterandom(8) ;
        $this->ql->antrian_autonumber('FBP'.date('Ymd'),$callid,date('Y-m-d',strtotime("+2 weeks")));
//      siapkan barang pemesanan
        if($this->input->post('modeubah')!=='pembetulanpesan')
        {
            $iduserlog =  $this->get_iduserlogin();
            $barangpemesanan = [
                'idunit'      => $this->input->post('idunitasal'),
                'idunittujuan'=> $this->input->post('idunittujuan'),
                'tanggalpemesanan' => $this->input->post('tanggalfaktur'),
                'nofaktur'    => fakturpermintaanbarang().$this->db->get_where('helper_autonumber',['callid'=>$callid])->row_array()['number'],
                'idpersonpetugas'=>$iduserlog
            ];

            $this->db->insert('rs_barang_pemesanan',$barangpemesanan);
        }
        $idbarangpemesanan = ((empty($this->input->post('ip')))? $this->db->insert_id() : $this->input->post('ip')); // siapkan idbarang pesanan
        //      siapkan barang pemesanan detail
        $idbarang = $this->input->post('idbarang');
        $kode  = $this->input->post('kode');
        $jumlah   = $this->input->post('jumlah');
        $jumlahsebelum   = $this->input->post('jumlahsebelum');
        $keterangan   = $this->input->post('keterangan');
        for ( $x=0; $x<count($kode); $x++)
        {   
            $pesanan = ['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x],'jumlah'=>$jumlah[$x],'keterangan'=>$keterangan[$x]];
            if($jumlahsebelum[$x]==0):
                $arrmsg = $this->db->replace('rs_barang_pemesanan_detail',$pesanan);
            elseif($jumlah[$x]==0 && $jumlahsebelum[$x]!=0):
                $arrmsg = $this->db->delete('rs_barang_pemesanan_detail',['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            elseif($jumlah[$x]!=0):
                $arrmsg = $this->db->update('rs_barang_pemesanan_detail',$pesanan,['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            endif;    
        }
        pesan_success_danger($arrmsg,'Pesanan berhasil ditambahkan.!','Pesanan gagal ditambahkan.!');
        redirect(base_url('cfarmasi/pesankegudang'));
       }
       else
       {
           aksesditolak();
       }
    }
    
    public function batal_permintaanbarangkegudang()
    {
        $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>$this->input->post('sts')],['idbarangpemesanan'=>$this->input->post('i')]);
        pesan_success_danger($arrmsg,"Batal Permintaan Berhasil.","Batal Permintaan Gagal.","js");
    }
    
    
    //list data di gudang - retur ke distributor
    public function returkedistributor()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
        {
            $data['content_view']      = 'farmasi/v_returkedistributor';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'returkedistributor';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Retur Barang';
            $data['mode']        = 'view';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'view';
            $data['script_js']   = ['farmasi/js_returkedistributor'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dt_returkedistributor()
    {
        $getData = $this->mdatatable->dt_returkedistributor_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {

                $menu  = '';
                if($obj->status == 'rencana'){
                    $menu .= ' <a id="selesairetur" idpengembalian="'.$obj->idbarangpengembalian.'" class="btn btn-xs btn-success" '. ql_tooltip('Selesai Retur').'><i class="fa fa-check"></i></a>';
                    $menu .= ' <a id="hapusretur" idpengembalian="'.$obj->idbarangpengembalian.'" class="btn btn-xs btn-danger" '. ql_tooltip('Hapus').'><i class="fa fa-trash"></i></a>';
                }
                
                $row = [];
                    $row[] = $obj->distributor;
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->tanggalfaktur;
                    $row[] = $obj->tanggalretur;
                    $row[] = $obj->status;
                    $row[] = $obj->keterangan;
                    $row[] = '<ol>'. str_replace(',','', $obj->returdetail).'</ol>';
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_returkedistributor(),
            "recordsFiltered" => $this->mdatatable->filter_dt_returkedistributor(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function tambahreturkedistributor()
    {
        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') == 'gudang' ) //lihat define di atas
        {
            $data['content_view']      = 'farmasi/v_returkedistributor';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'returkedistributor';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Retur Barang';
            $data['mode']        = 'tambah';
            $data['plugins']     = [PLUG_DATE, PLUG_DROPDOWN];
            $data['jsmode']      = 'tambah';
            $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_returkedistributor'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function getdatafakturpembelian()
    {
        $tanggal = $this->input->post('tanggal'); 
        $query   = $this->db->query("SELECT idbarangfaktur ,nofaktur FROM `rs_barang_faktur` WHERE tanggalfaktur = '".$tanggal."'")->result();
        echo json_encode($query);
    }
    
    public function getdatadetailfakturpembelian()
    {
        $idbarangfaktur = $this->input->post('id');
        $query = $this->db->query("select a.idbarang, a.idbarangpembelian, a.batchno, a.kadaluarsa, b.kode, b.namabarang, c.namasatuan
                from rs_barang_pembelian a 
                join rs_barang b on b.idbarang = a.idbarang
                join rs_satuan c on c.idsatuan = b.idsatuan 
                where idbarangfaktur='".$idbarangfaktur."'")->result();
        echo json_encode($query);
    }
    
    //save retur barang ke distributor
    public function save_returkedistributor()
    {
        $callid = generaterandom(8) ;
        $index  = $this->input->post('index');
        $idpembelian = $this->input->post('idpembelian');
        $idbarang    = $this->input->post('idbarang');
        $jumlah      = $this->input->post('jumlah');
        
        $dtretur = [
          'idbarangfaktur' => $this->input->post('idbarangfaktur'),
          'tanggalretur'   => $this->input->post('tanggalretur'),
          'callid'         => $callid,
          'keterangan'     => $this->input->post('keterangan')
        ];
        $save_retur = $this->db->insert('rs_barang_pengembalian',$dtretur);
        $idbarangpengembalian = $this->getinsert_id('rs_barang_pengembalian',$callid,'idbarangpengembalian');
        foreach ($index as $key => $value)
        {
            if(empty(!$jumlah[$key]))
            {
                $data = [
                    'idbarangpengembalian' =>$idbarangpengembalian,
                    'idbarangpembelian'=>$idpembelian[$key],
                    'idbarang' =>$idbarang[$key],
                    'jumlah' => $jumlah[$key]
                ];
                $arrmsg = $this->db->insert('rs_barang_pengembalian_detail',$data);
            }
        }
        pesan_success_danger($arrmsg,'Retur berhasil ditambahkan.','Retur gagal ditambahkan.');
        redirect(base_url('cfarmasi/returkedistributor'));
       
    }
    
    public function selesaireturdistributor()
    {
        $id = $this->input->post('id');
        
        $arrmsg = $this->db->update('rs_barang_pengembalian',['status'=>'diretur'],['idbarangpengembalian'=>$id]);
        $dtdetail = $this->db->get_where('rs_barang_pengembalian_detail',['idbarangpengembalian'=>$id])->result();
        
        $idunit = $this->session->userdata('idunitterpilih');
        $iduser = $this->get_iduserlogin();
        foreach ($dtdetail as $obj)
        {
            $datadistribusi = [
                'idbarangpembelian' => $obj->idbarangpembelian,
                'idunit' => $idunit,
                'jumlah' => $obj->jumlah,
                'jenisdistribusi' => 'keluar',
                'idpersonpetugas' => $iduser,
                'statusdistribusi'=> 'tidakdiubah'
            ];
            $arrmsg = $this->db->insert('rs_barang_distribusi',$datadistribusi);
        }
        
        pesan_success_danger($arrmsg, 'Retur Berhasil.', 'Retur Gagal.', 'js');
    }
    
    public function hapusreturdistributor()
    {
        $id = $this->input->post('id');
        $arrmsg = $this->db->delete('rs_barang_pengembalian',['idbarangpengembalian'=>$id]);
        $arrmsg = $this->db->delete('rs_barang_pengembalian_detail',['idbarangpengembalian'=>$id]);
        pesan_success_danger($arrmsg, 'Hapus Rencana Retur Berhasil.', 'Hapus Rencana Retur Gagal.', 'js');
    }
    
    
    public function pesankefarmasi()
    {
//    if (empty(! $this->session->userdata('levelgudang') ) OR $this->session->userdata('levelgudang') != 'gudang' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_pesankefarmasi';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'pesankefarmasi';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Pesan Barang ke Farmasi';
            $data['mode']        = 'view';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'view';
            $data['script_js']   = ['farmasi/js_pesanbarangkefarmasi'];
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    public function dt_pesankefarmasi()
    {
        $getData = $this->mdatatable->dt_permintaanbarang_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {
                $menu  = '';
                if($obj->statusbarangpemesanan=='menunggu')
                {
                    $menu .= ' <a id="batalpermintaan" alt="'.$obj->idbarangpemesanan.'" class="btn btn-danger btn-xs" '.ql_tooltip('batal').' ><i class="fa fa-minus-circle"></i></a>';
                    $menu .= ' <a  onclick="pembetulanpesan('.$obj->idbarangpemesanan.',\'pembetulanpesan\')" '.ql_tooltip('pembetulan').' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>';
                }
                
                if($obj->statusbarangpemesanan == 'batal')
                {
                    $menu .= ' <a id="buatpermintaanulang"  alt="'.$obj->idbarangpemesanan.'" class="btn btn-success btn-xs" '.ql_tooltip('pesan').' ><i class="fa fa-plus"></i></a>';
                }
                
                $menu .= ' <a  onclick="cetakbarangpesanan('.$obj->idbarangpemesanan.')" ' .ql_tooltip('cetak').' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                $row = [];
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->waktu;
                    $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                    $row[] = $obj->statusbarangpemesanan;
                    $row[] = enum_rs_barang_pemesanan($obj->jenistransaksi);
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_permintaanbarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_permintaanbarang(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function addpesankefarmasi()
    {
//        if (empty(! $this->session->userdata('levelgudang') ) && $this->session->userdata('levelgudang') == 'depo' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_pesankefarmasi';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'pesankefarmasi';
            $data['active_menu_level'] = '';
            $data['title_page']        = 'Pesan Barang Ke Farmasi';
            $data['mode']              = 'tambahpesanan';
            $data['plugins']           = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']            = 'pesan';
            $data['script_js']         = ['farmasi/js_farmasi','farmasi/js_pesanbarangkefarmasi'];
            $data['unit']              = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi'])->result(); 
            $data['unittujuan']        =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
            
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    public function fillpesanbarangkefarmasi()
    {
        // cari barang : retur, pesan, transformasi tujuan
        $namaobat = $this->input->post('namaobat');
        $mode   = $this->input->post('mode');
        $idunit = (($mode=='retur') ? $this->session->userdata('idunitterpilih') : $this->input->post('unittujuan') );        
        $query= (($mode)=='retur' ? "SELECT concat(rb.idbarang,',',rb.kode,',',rb.namabarang,',',namasatuan,',',rb.hargabeli,',',0,',',rbp.kadaluarsa,',',rbp.batchno,',',0,',',bs.idbarangpembelian) as id, concat(rb.kode,' ',rb.namabarang,' ED:',rbp.kadaluarsa,' BATCH.No:',rbp.batchno,' Stok:',bs.stok,' ',namasatuan) as text  from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian  and bs.idunit='".$idunit."' where rs.idsatuan=rb.idsatuan and bs.stok > 0 and namabarang like '%".$namaobat."%' limit 20" : "SELECT concat(rb.idbarang,',',kode,',',namabarang,',',namasatuan,',', rb.hargabeli,',',0) as id, concat(kode,' ',namabarang,' ( stok ', ifnull(sum(bs.stok),0) ,' ',namasatuan ,' )') as text from rs_barang rb join rs_satuan rs on rs.idsatuan=rb.idsatuan  left join rs_barang_pembelian rbp on rbp.idbarang = rb.idbarang left join rs_barang_stok bs on bs.idbarangpembelian = rbp.idbarangpembelian and bs.idunit='".$idunit."' where rs.idsatuan=rb.idsatuan  and namabarang like '%".$namaobat."%' GROUP by rb.idbarang limit 20" );
        echo json_encode($this->db->query($query)->result());
    }

    private function rencana_rs_inap_rencana_medis_barang($idinap,$idbarang)
    {
        $barang = $this->db->get_where('rs_barang',['idbarang'=>$idbarang])->row_array();
        $pasien = $this->db->get_where('rs_inap',['idinap'=>$idinap])->row_array();
        $cekbarang = $this->db->select('idbarang')->get_where('rs_inap_rencana_medis_barang',['idbarang'=>$idbarang,'idinap'=>$idinap]);
        if( empty($cekbarang->num_rows()))
        {
            $data = [
                'idbarang' => $barang['idbarang'],
                'idjenistarif' =>$barang['idjenistarif'],
                'jumlahperencanaan' => 0,
                'jumlahpemakaian'=> 0,
                'harga' => $barang['hargajual'],
                'idpendaftaran'=>$pasien['idpendaftaran'],
                'idinap'=>$pasien['idinap']
              ];
              $insert = $this->db->insert('rs_inap_rencana_medis_barang',$data);
        }
    }

    // save barang pemesanan dari depo ke farmasi
    public function save_pesankefarmasi()
    {
        if( empty(! $this->session->userdata('levelgudang')) &&  $this->session->userdata('levelgudang') == 'depo' )
        {
        
        $post = $this->input->post();
        $idbpesan = $this->getidpesandeporanap($post,'pesan'); 
        $idinap = ((isset($post['idinap'])) ? $post['idinap'] : 0 );
        if(empty(!$idbpesan))
        {
            $idbarangpemesanan = $idbpesan;
        }
        else
        {
            //buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired
            $callid = generaterandom(8) ;
            $this->ql->antrian_autonumber('FBP'.date('Ymd'),$callid,date('Y-m-d',strtotime("+2 weeks")));
            
            //siapkan barang pemesanan
            if($this->input->post('modeubah')!=='pembetulanpesan'){
                $iduserlog =  $this->get_iduserlogin();
                $barangpemesanan = [
                    'idunit'      => $this->input->post('idunitasal'),
                    'idunittujuan'=> $this->session->userdata('idunitterpilih'),
                    'waktu'       => $this->input->post('tanggalfaktur'),
                    'tanggalpemesanan' => $this->input->post('tanggalfaktur'),
                    'nofaktur'    => fakturpermintaanbarang().$this->db->get_where('helper_autonumber',['callid'=>$callid])->row_array()['number'],
                    'idpersonpetugas'=>$iduserlog,
                    'idinap'      => $idinap
                ];

                $this->db->insert('rs_barang_pemesanan',$barangpemesanan);
                
            }
            $idbarangpemesanan = ((isset($post['ip']) &&  empty(!$this->input->post('ip')) ) ? $this->input->post('ip') : $this->db->insert_id() );// siapkan idbarang pesanan
        }
        
        //      siapkan barang pemesanan detail
        $idbarang     = $this->input->post('idbarang');
        $kode         = $this->input->post('kode');
        $jumlah       = $this->input->post('jumlah');
        $jumlahsebelum= $this->input->post('jumlahsebelum');
        $keterangan   = $this->input->post('keterangan');
        for ( $x=0; $x<count($kode); $x++)
        {   
            $pesanan = ['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x],'jumlah'=>$jumlah[$x],'keterangan'=>$keterangan[$x]];
            if($jumlahsebelum[$x]==0):
                
                if(empty(!$idinap))
                {
                    //add rencana medis barang
                    $this->rencana_rs_inap_rencana_medis_barang($idinap,$idbarang[$x]);
                }
            
                $arrmsg = $this->db->replace('rs_barang_pemesanan_detail',$pesanan);
            elseif($jumlah[$x]==0 && $jumlahsebelum[$x]!=0):
                $arrmsg = $this->db->delete('rs_barang_pemesanan_detail',['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            elseif($jumlah[$x]!=0):
                $arrmsg = $this->db->update('rs_barang_pemesanan_detail',$pesanan,['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            endif;    
        }
        if( isset($post['modesave']) && $post['modesave'] == 'ranap' ){
            pesan_success_danger($arrmsg,'Pesanan berhasil ditambahkan.!','Pesanan gagal ditambahkan.!','js');
        }else{
            pesan_success_danger($arrmsg,'Pesanan berhasil ditambahkan.!','Pesanan gagal ditambahkan.!');
            redirect(base_url('cfarmasi/pesankefarmasi'));
        }
       }
       else
       {
           aksesditolak();
       }
    }
    
    
    public function permintaandepo()
    {
//    if (empty(! $this->session->userdata('levelgudang') ) OR $this->session->userdata('levelgudang') != 'gudang' ) //lihat define di atas
//        {
            $data['content_view']      = 'farmasi/v_permintaandepo';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaandepo';
            $data['active_menu_level'] = '';
            
            $data['title_page']  = 'Permintaan Barang Depo';
            $data['mode']        = 'view';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
            $data['jsmode']      = 'tampilpesanan';
            $data['script_js']   = ['farmasi/js_permintaandepo'];
            $data['active_menu_levelchild']='permintaanbarang';
            $this->load->view('v_index',$data);
//        }
//        else
//        {
//            aksesditolak();
//        }
    }
    
    public function dt_permintaandepo()
    {
        $getData = $this->mdatatable->dt_permintaandepo_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {
                
                $menu ='';
                    if($obj->statusbarangpemesanan == 'menunggu'){
                        $menu  .= ' <a onclick="kirimbarangpesanan('.$obj->idbarangpemesanan.',false)" '.ql_tooltip('kirim pesanan').' class="btn btn-xs btn-success"><i class="fa fa-check"></i></a>';
                    }
                    if($obj->statusbarangpemesanan == 'selesai'){
                        $menu .= ' <a onclick="kirimbarangpesanan('.$obj->idbarangpemesanan.',\'pembetulankirim\')" '.ql_tooltip('pembetulan').' class="btn btn-xs btn-warning"><i class="fa fa-edit"></i></a>';
                    }
                    $menu .= ' <a  onclick="cetakbarangpesanan('.$obj->idbarangpemesanan.')" ' .ql_tooltip('cetak').' class="btn btn-xs btn-primary"><i class="fa fa-print"></i></a>';
                    
                $row = [];
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->waktu;
                    $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                    $row[] = $obj->statusbarangpemesanan;
                    $row[] = enum_rs_barang_pemesanan($obj->jenistransaksi);
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_permintaandepo(),
            "recordsFiltered" => $this->mdatatable->filter_dt_permintaandepo(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function getdata_pesanandepo()
    {
        $this->load->model('mkombin');
        echo json_encode($this->mkombin->getdata_pesanandepo()->result());
    }
    public function kirim_pesanandepo()
    {
        if ( $this->session->userdata('levelgudang') == 'farmasi') //lihat define di atas
         {
            $data['content_view']      = 'farmasi/v_permintaandepo';
            $data['active_menu']       = 'farmasi';
            $data['active_sub_menu']   = 'permintaandepo';
            $data['active_menu_level'] = '';
            $data['title_page']  = 'Kirim Pesanan Depo';
            $data['mode']        = 'kirim';
            $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            $data['jsmode']      = 'kirim';
            $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_pesanbarangkefarmasi'];
            $data['unit']        = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi' ])->result();
            $data['unittujuan']  = $this->db->get_where('rs_unit_farmasi',['levelgudang'=>'depo'])->result();//masih salah seharus nya otomatis unit yang dituju #NOTE
            $this->load->view('v_index',$data);
         }
         else
         {
             aksesditolak();
         }
    }
    
    
    public  function savekirimpesanandepo()
    {

       if( $this->session->userdata('levelgudang') == 'farmasi' )
       { 
        $idunitasal       =$this->input->post("idunitasal");
        $idbarangpemesanan=$this->input->post("idbarangpemesanan");
        $idunittujuan     =$this->input->post("idunittujuan");
        $idbarangpembelian=$this->input->post('idbarangpembelian');
        $jumlah           =$this->input->post('jumlah');
        $jumlahpembetulan =$this->input->post('jumlahpembetulan');
        $modeubah         =$this->input->post('modeubah');
        $length           =$this->input->post('length');

        for($x=0; $x < count($length); $x++)
        {
             
            if(!(empty($jumlah[$x])) || !(empty($jumlahpembetulan[$x])) )
            {
             
                if($modeubah=='pembetulankirim'):
                   
                    if($jumlahpembetulan[$x] != $jumlah[$x])
                    {
                        if(!empty($jumlahpembetulan[$x]))
                        {
                            $pembetulan = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlahpembetulan[$x], 'jenisdistribusi' => 'pembetulankeluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                            $arrmsg = $this->db->insert('rs_barang_distribusi',$pembetulan);
                        }
                        $keluar = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                        $arrmsg = $this->db->insert('rs_barang_distribusi',$keluar);
                        $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$idbarangpemesanan]);
                    }
                    else
                    {
//                        
                    }
                else :
                    $data = ['idbarangpembelian'=>$idbarangpembelian[$x], 'idunit' => $idunitasal, 'jumlah' => $jumlah[$x], 'jenisdistribusi' => 'keluar', 'idunittujuan' => $idunittujuan, 'idbarangpemesanan' => $idbarangpemesanan];
                    $arrmsg = $this->db->insert('rs_barang_distribusi',$data);
                    $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$idbarangpemesanan]);
                endif;

            }
        }
        pesan_success_danger( ($modeubah=='pembetulankirim') ? 1 : $arrmsg,'Simpan Berhasil.!','Simpan Gagal.!');
        redirect(base_url('cfarmasi/permintaandepo'));
       }
       else
       {
           aksesditolak();
       }
    }
    
    public function batal_permintaanbarang()
    {
        $arrmsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>$this->input->post('sts')],['idbarangpemesanan'=>$this->input->post('i')]);
        pesan_success_danger($arrmsg,"Batal Permintaan Berhasil.","Batal Permintaan Gagal.","js");
    }
    
    
    public function permintaanreturdepo()
    {
        $data['content_view']      = 'farmasi/v_returkefarmasi';
        $data['active_menu']       = 'farmasi';
        $data['active_sub_menu']   = 'permintaanreturdepo';
        $data['active_menu_level'] = '';

        $data['title_page']  = 'Retur Barang';
        $data['mode']        = 'viewfarmasi';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
        $data['jsmode']      = 'viewfarmasi';
        $data['script_js']   = ['farmasi/js_returkefarmasi'];
        $this->load->view('v_index',$data);
    }

    public function returkefarmasi()
    {
        $data['content_view']      = 'farmasi/v_returkefarmasi';
        $data['active_menu']       = 'farmasi';
        $data['active_sub_menu']   = 'returkefarmasi';
        $data['active_menu_level'] = '';

        $data['title_page']  = 'Retur Barang';
        $data['mode']        = 'view';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
        $data['jsmode']      = 'view';
        $data['script_js']   = ['farmasi/js_returkefarmasi'];
        $this->load->view('v_index',$data);
    }
    
    //tampil data permintaan retur dari depo
    public function dt_permintaanreturdepo()
    {
        $getData = $this->mdatatable->dt_listreturdepo_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {

                $menu  = '';
                if($obj->statusbarangpemesanan == 'menunggu'){
                    $menu .= ' <a id="terimareturbarangdaridepo" idp="'.$obj->idbarangpemesanan.'" class="btn btn-xs btn-success" '. ql_tooltip('Terima Barang').'><i class="fa fa-check"></i></a>';
                }
                
                $row = [];
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->waktu;
                    $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                    $row[] = $obj->statusbarangpemesanan;
                    $row[] = '<ol>'. str_replace(',','', $obj->returdetail).'</ol>';
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_listreturdepo(),
            "recordsFiltered" => $this->mdatatable->filter_dt_listreturdepo(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function terimareturbarangdaridepo()
    {
        $iduser  = $this->get_iduserlogin();
        $idpesan = $this->input->post('idp');
        
        $retur = $this->db->query("SELECT a.idunit, a.idunittujuan , b.idbarangpembelian, b.idbarang, b.jumlah
            FROM rs_barang_pemesanan a
            join rs_barang_pemesanan_detail b on b.idbarangpemesanan = a.idbarangpemesanan
            WHERE a.idbarangpemesanan ='".$idpesan."' "); 
        if($retur->num_rows() > 0)
        {
            $dt = $retur->result_array();
            foreach ($dt as $arr)
            {
                $data = [
                    'idbarangpembelian' =>$arr['idbarangpembelian'],
                    'idunit' =>$arr['idunit'],
                    'jumlah' =>$arr['jumlah'],
                    'jenisdistribusi' => 'pengembalian',
                    'idunittujuan' =>$arr['idunittujuan'],
                    'idbarangpemesanan' =>$idpesan,
                    'idpersonpetugas' => $iduser,
                    'statusdistribusi' => 'tidakdiubah',
                ];
                
                $arrMsg = $this->db->insert('rs_barang_distribusi',$data);
            }
            $arrMsg = $this->db->update('rs_barang_pemesanan',['statusbarangpemesanan'=>'selesai'],['idbarangpemesanan'=>$idpesan]);
            pesan_success_danger($arrMsg, 'Terima Retur Berhasil.', 'Terima Retur Gagal.', 'js');
        }
    }

    //tampil data pesan retur ke farmasi
    public function dt_returkefarmasi()
    {
        $getData = $this->mdatatable->dt_returkefarmasi_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {

                $menu  = '';
                if($obj->statusbarangpemesanan == 'menunggu'){
                    $menu .= ' <a id="hapusretur" idp="'.$obj->idbarangpemesanan.'" class="btn btn-xs btn-danger" '. ql_tooltip('Hapus').'><i class="fa fa-trash"></i></a>';
                }
                
                $row = [];
                    $row[] = $obj->nofaktur;
                    $row[] = $obj->waktu;
                    $row[] = $obj->unitasal.'/'.$obj->unittujuan;
                    $row[] = $obj->statusbarangpemesanan;
                    $row[] = '<ol>'. str_replace(',','', $obj->returdetail).'</ol>';
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_returkefarmasi(),
            "recordsFiltered" => $this->mdatatable->filter_dt_returkefarmasi(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function hapusreturfarmasi()
    {
        $idp = $this->input->post('idp');
        $delete = $this->db->delete('rs_barang_pemesanan',['idbarangpemesanan'=>$idp]);
        $delete = $this->db->delete('rs_barang_pemesanan_detail',['idbarangpemesanan'=>$idp]);
        pesan_success_danger($delete, 'Hapus Retur Berhasil.', 'Hapus Retur Gagal.', 'js');
    }
    
    public function addreturkefarmasi()
    {
        $data['content_view']      = 'farmasi/v_returkefarmasi';
        $data['active_menu']       = 'farmasi';
        $data['active_sub_menu']   = 'returkefarmasi';
        $data['active_menu_level'] = '';
        $data['title_page']  = 'Retur Barang Ke Farmasi';
        $data['mode']        = 'tambahretur';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
        $data['jsmode']      = 'retur';
        $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_returkefarmasi'];
        $data['unit'] = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi'])->result(); 
        $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
        $this->load->view('v_index',$data);
    }
    
    // save retur barang pemesanan dari depo ke farmasi
    public function save_returkefarmasi()
    {
        if( empty(! $this->session->userdata('levelgudang')) &&  $this->session->userdata('levelgudang') == 'depo' )
        {
//        buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired;;;
        $callid = generaterandom(8) ;
        $this->ql->antrian_autonumber('FBR'.date('Ymd'),$callid,date('Y-m-d',strtotime("+2 weeks")));
//      siapkan barang pemesanan
//        if($this->input->post('modeubah')!=='pembetulanretur'){
            $iduserlog =  $this->get_iduserlogin();
            $barangpemesanan = [
                'idunit'      => $this->input->post('idunitasal'),
                'idunittujuan'=> $this->input->post('idunittujuan'),
                'waktu'       => $this->input->post('tanggalretur'),
                'jenistransaksi' => 'retur',
                'nofaktur'    => fakturpengembalianbarang().$this->db->get_where('helper_autonumber',['callid'=>$callid])->row_array()['number'],
                'idpersonpetugas'=>$iduserlog,
                'keterangan' => $this->input->post('keterangan'),
                'callid' => $callid
            ];
            $this->db->insert('rs_barang_pemesanan',$barangpemesanan);
//        }
        $idbarangpemesanan = ((empty($this->input->post('ip')))? $this->db->select('idbarangpemesanan')->get_where('rs_barang_pemesanan',['callid'=>$callid])->row_array()['idbarangpemesanan'] : $this->input->post('ip')); // siapkan idbarang pesanan
        $this->db->update('rs_barang_pemesanan',['callid'=>''],['idbarangpemesanan'=>$idbarangpemesanan]);
        //      siapkan barang pemesanan detail
        $idbarang = $this->input->post('idbarang');
        $index    = $this->input->post('index');
        $jumlah   = $this->input->post('jumlah');
        $jumlahsebelum   = $this->input->post('jumlahsebelum');
        $idp = $this->input->post('idp');
        
        foreach ($index as $x => $value)
        {      
            $pesanan = ['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x],'jumlah'=>$jumlah[$x] ,'idbarangpembelian'=>$idp[$x]];
            if($jumlahsebelum[$x]==0):
                $arrmsg = $this->db->replace('rs_barang_pemesanan_detail',$pesanan);
            elseif($jumlah[$x]==0 && $jumlahsebelum[$x]!=0):
                $arrmsg = $this->db->delete('rs_barang_pemesanan_detail',['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            elseif($jumlah[$x]!=0):
                $arrmsg = $this->db->update('rs_barang_pemesanan_detail',$pesanan,['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x]]);
            endif;    
        }
        pesan_success_danger($arrmsg,'Retur berhasil ditambahkan.!','Retur gagal ditambahkan.!');
        redirect(base_url('cfarmasi/returkefarmasi'));
       }
       else
       {
           aksesditolak();
       }
    }
    
    public function save_returfarmasiobatranap()
    {
        if( empty(! $this->session->userdata('levelgudang')) &&  $this->session->userdata('levelgudang') == 'depo' )
        {
//        buat nofaktur otomatis (cara mengisi)--> $id,$callid,$expired;;;
            
        $post = $this->input->post();
        $idbpesan = $this->getidpesandeporanap($post,'retur'); 
        
        if(empty(!$idbpesan))
        {
            $idbarangpemesanan = $idbpesan;
        }
        else
        {
            $callid = generaterandom(8) ;
            $this->ql->antrian_autonumber('FBR'.date('Ymd'),$callid,date('Y-m-d',strtotime("+2 weeks")));
                $iduserlog =  $this->get_iduserlogin();
                $barangpemesanan = [
                    'idunit'         => $this->session->userdata('idunitterpilih'),
                    'idunittujuan'   => $this->input->post('idunitasal'),
                    'tanggalpemesanan'=>$this->input->post('tanggalfaktur'),
                    'jenistransaksi' => 'retur',
                    'nofaktur'       => fakturpengembalianbarang().$this->db->get_where('helper_autonumber',['callid'=>$callid])->row_array()['number'],
                    'idpersonpetugas'=> $iduserlog,
                    'idinap'         => $this->input->post('idinap'),
                    'keterangan'     => $this->input->post('keterangan'),
                    'callid'         => $callid
                ];
            $this->db->insert('rs_barang_pemesanan',$barangpemesanan);
            $idbarangpemesanan = ((empty($this->input->post('ip')))? $this->db->select('idbarangpemesanan')->get_where('rs_barang_pemesanan',['callid'=>$callid])->row_array()['idbarangpemesanan'] : $this->input->post('ip')); // siapkan idbarang pesanan
            $this->db->update('rs_barang_pemesanan',['callid'=>''],['idbarangpemesanan'=>$idbarangpemesanan]);
        }
        //      siapkan barang pemesanan detail
        $idbarang = $this->input->post('idbarang');
        $index    = $this->input->post('index');
        $jumlah   = $this->input->post('jumlah');
        $idp      = $this->input->post('idp');
        
        foreach ($index as $x => $value)
        {      
            $pesanan = ['idbarangpemesanan'=>$idbarangpemesanan,'idbarang'=>$idbarang[$x],'jumlah'=>$jumlah[$x] ,'idbarangpembelian'=>$idp[$x]];
            $arrmsg = $this->db->replace('rs_barang_pemesanan_detail',$pesanan);
        }
        pesan_success_danger($arrmsg,'Retur berhasil ditambahkan.!','Retur gagal ditambahkan.!','js');
       }
       else
       {
           aksesditolak();
       }
    }


    public function dt_kartustokdepo()
    {
        $getData = $this->mdatatable->dt_kartustokdepo_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj) {

                $menu  = '<a id="returbarangdepo" idp="'.$obj->idbarangpembelian.'" class="btn btn-xs btn-warning" '.ql_tooltip('Retur Barang').'><i class="fa fa-undo"></i></a>';                
                $row = [];
                    $row[] = $obj->kode;
                    $row[] = $obj->namabarang;
                    $row[] = $obj->batchno;
                    $row[] = $obj->kadaluarsa;
                    $row[] = convertToRupiah($obj->stok);
                    $row[] = $obj->namasatuan;                    
                    $row[] = $menu;
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_kartustokdepo(),
            "recordsFiltered" => $this->mdatatable->filter_dt_kartustokdepo(),
            "data" =>$data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function addreturkefarmasibyfaktur()
    {
        $data['content_view']      = 'farmasi/v_returkefarmasi';
        $data['active_menu']       = 'farmasi';
        $data['active_sub_menu']   = 'returkefarmasi';
        $data['active_menu_level'] = '';
        $data['title_page']  = 'Retur Barang Ke Farmasi';
        $data['mode']        = 'tambahreturbyfaktur';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
        $data['jsmode']      = 'returbyfaktur';
        $data['script_js']   = ['farmasi/js_farmasi','farmasi/js_returkefarmasi'];
        $data['unit'] = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi'])->result(); 
        $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
        $this->load->view('v_index',$data);
    }    
    
    public function getdatadetailfakturpesanan()
    {
        $idpesan = $this->input->post('id');
        $query = $this->db->query("SELECT d.idbarang, d.namabarang, d.kode, e.stok, c.idbarangpembelian,f.batchno, f.kadaluarsa, g.namasatuan
        FROM rs_barang_pemesanan a        
        join rs_barang_distribusi c on c.idbarangpemesanan = a.idbarangpemesanan
        join rs_barang_pembelian f on f.idbarangpembelian = c.idbarangpembelian
        join rs_barang d on d.idbarang = f.idbarang
        join rs_satuan g on g.idsatuan = d.idsatuan
        join rs_barang_stok e on e.idbarangpembelian = c.idbarangpembelian and e.idunit = '".$this->session->userdata('idunitterpilih')."'
        WHERE a.idbarangpemesanan = '".$idpesan."' and e.idunit = '".$this->session->userdata('idunitterpilih')."'")->result();
        echo json_encode($query);
    }
    
    //get data barang depo by idbarangpembelian
    public function getbarangdepobyidbp()
    {
        $this->load->model('mkombin');
        $data = $this->mkombin->getbarangdepobyidbp($this->input->post('idp'));
        echo json_encode($data);
    }
    
    public function getdatafakturpesanandepo()
    {
        $tanggal = $this->input->post('tanggal'); 
        $query   = $this->db->query("SELECT idbarangpemesanan,nofaktur FROM rs_barang_pemesanan WHERE date(waktu) = '".$tanggal."' and idunittujuan='".$this->session->userdata('idunitterpilih')."' and statusbarangpemesanan='selesai'")->result();
        echo json_encode($query);
    }
    
    
    public function riwayatbarang()
    {
        $data['content_view']      = 'farmasi/v_riwayatbarang';
        $data['active_menu']       = 'farmasi';
        $data['active_sub_menu']   = 'riwayatbarang';
        $data['active_menu_level'] = '';
        $data['title_page']  = 'Riwayat Barang 7 hari terakhir';
        $data['mode']        = '';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE ];
        $data['script_js']   = ['farmasi/js_riwayatbarang'];
        $data['unit'] = $this->db->get_where('rs_unit_farmasi',['levelgudang'=> 'farmasi'])->result(); 
        $data['unittujuan'] =  $this->db->where('idunit', $this->session->userdata('idunitterpilih'))->get('rs_unit_farmasi')->result();
        $this->load->view('v_index',$data);
    }
    
    public function dt_riwayatbarang()
    {
        $tanggal  = $this->input->post('tanggal');
        $tanggal1 = ql_tanggaltujuan('-6 day',$tanggal);
        $tanggal2 = $tanggal;
        //data barang
        $barang = $this->db->query("select c.kode, b.idbarangpembelian, c.namabarang,b.batchno, b.kadaluarsa, a.stok, d.namasatuan
        from rs_barang_stok a
        join rs_barang_pembelian b on b.idbarangpembelian = a.idbarangpembelian
        join rs_barang c on c.idbarang = b.idbarang
        join rs_satuan d on d.idsatuan = c.idsatuan
        WHERE a.stok > 0 and idunit = '".$this->session->userdata('idunitterpilih')."'");
        //riwayat barang
        $riwayat = $this->db->query("select date_format(waktu,'%d/%m/%Y') as tanggal, a.idbarangpembelian, statusriwayaatobat(a.jenisdistribusi,a.idunit,a.idunittujuan,".$this->session->userdata('idunitterpilih').",sum(a.jumlah)) as riwayat from rs_barang_distribusi a 
        WHERE  (idunit = ".$this->session->userdata('idunitterpilih')." or idunittujuan = ".$this->session->userdata('idunitterpilih').") and date(a.waktu) between '".$tanggal1."' AND '".$tanggal2."'
        GROUP by a.jenisdistribusi, a.idunit, a.idbarangpembelian, date(a.waktu)
        ORDER by idbarangpembelian asc, date(a.waktu) asc");
        //tanggal riwayat
        $tanggal = $this->db->query("select date_format(waktu,'%d/%m/%Y') as tanggal from rs_barang_distribusi where date(waktu) between '".$tanggal1."' AND '".$tanggal2."' group by date(waktu)");
        
        $data['barang'] = $barang->result_array();
        $data['riwayat']= $riwayat->result_array();
        $data['tanggal']= $tanggal->result_array();
        
        echo json_encode($data);
    }
    
    
    
    public function settingstokopname()
    {
        return [    'content_view'      => 'farmasi/v_stokopname',
                    'active_menu'       => 'farmasi',
                    'active_menu_level' => ''
               ];
    }
    public function stokopname()
    {
        
	if ($this->pageaccessrightbap->checkAccessRight(V_BARANG) && empty(!$this->session->userdata('idunitterpilih')) ) //lihat define di atas
        {
            $data                = $this->settingstokopname();
            $data['title_page']  = 'Stok Opname '.ucwords(strtolower($this->session->userdata('unitterpilih')));
            $data['mode']        = 'stokopnamegudang';
            $data['active_sub_menu']= 'stokopname';
            $data['plugins']     = [ PLUG_DATATABLE ];
            $data['jsmode']      = 'stokopnamegudang';
            $data['script_js']   = ['farmasi/stokopname'];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dt_stokperbandingan()
    {
        $getData = $this->mdatatable->dt_stokperbandingan_(true);
        $data=[];
        if($getData){
            foreach ($getData->result() as $obj)
            {
                $row = [];
                    $row[] = $obj->namabarang;
                    $row[] = $obj->kode;
                    $row[] = convertToRupiah($obj->stoklama);
                    $row[] = convertToRupiah($obj->stokbaru);
                    $row[] = $obj->namasatuan; 
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_stokperbandingan(),
            "recordsFiltered" => $this->mdatatable->filter_dt_stokperbandingan(),
            "data" =>$data,
        );
        //output dalam format JSON
        echo json_encode($output);        
    }
    
    public function create_stokopname()
    {
        if(empty(!$this->session->userdata('idunitterpilih')))
        {
            $idunit = $this->session->userdata('idunitterpilih');
            $arrMsg = $this->db->query("INSERT INTO `rs_barang_stok_opname`(`idunit`, `idbarangpembelian`, `stok`, `idbarang`, `batchno`, `kadaluarsa`) "
                    . "select rbs.idunit, rbs.idbarangpembelian, rbs.stok, rbp.idbarang, rbp.batchno, rbp.kadaluarsa from rs_barang_stok rbs join rs_barang_pembelian rbp on rbp.idbarangpembelian = rbs.idbarangpembelian WHERE idunit = '".$idunit."'");

    //        $arrMsg = $this->db->query("call pstokopname('".$idunit."') "); 
            $stok = $this->db->query("SELECT rbp.idbarangpembelian, idunit, stok FROM rs_barang_stok rbs join rs_barang_pembelian rbp on rbp.idbarangpembelian = rbs.idbarangpembelian and rbp.idbarang > 0 WHERE idunit = '".$idunit."' and stok > 0")->result();
            foreach ($stok as $obj)
            {
                $data['idbarangpembelian'] = $obj->idbarangpembelian;
                $data['idunit'] = $obj->idunit;
                $data['jumlah'] = $obj->stok;
                $data['jenisdistribusi']  = 'stokopname';
                $data['statusdistribusi'] = 'tidakdiubah';
                $arrMsg = $this->db->insert('rs_barang_distribusi',$data);
            }

            pesan_success_danger($arrMsg, 'Stok Opname Data Berhasil.', 'Stok Opname Data Gagal.','js');
        }
        
    }
    
    public function sethargabelimasterbarang()
    {
        $save = $this->db->update('rs_barang',['hargabeli'=>$this->input->post('hargabeli')],['idbarang'=>$this->input->post('idbarang')]);
        pesan_success_danger($save,"Ubah Harga Berhasil.","Ubah Harga Gagal.","js");
    }
    
    public function penggunaanbarang()
    {
        $data['content_view'] = 'farmasi/v_penggunaanbarang';
        $data['active_menu']  = 'farmasi';
        $data['active_menu_level'] = '';
        $data['active_sub_menu']= 'penggunaanbarang';        
        $data['title_page']  = (($this->session->userdata('levelgudang') == 'depo') ? 'Penggunaan Barang' : 'Penyesuaian Stok' ) .' '.ucwords(strtolower($this->session->userdata('unitterpilih')));
        $data['mode']        = 'view';
        $data['plugins']     = [ PLUG_DATATABLE, PLUG_DATE];
        $data['jsmode']      = 'view';
        $data['script_js']   = ['farmasi/js_penggunaanbarang'];
        $this->load->view('v_index',$data);
    }
    
    public function dt_penggunaanbarang()
    {
        $getData = $this->mdatatable->dt_penggunaanbarang_(true);
        $lastquery = $this->db->last_query();
        $data=[];
        if($getData){
            $no = $_POST['start'];
            foreach ($getData->result() as $obj)
            {
                $row = [];
                    $row[] = ++$no;
                    $row[] = $obj->keterangan;
                    $row[] = $obj->namabarang;
                    $row[] = convertToRupiah($obj->jumlah);
                    $row[] = convertToRupiah($obj->hargajual); 
                    $row[] = convertToRupiah($obj->hargajual * $obj->jumlah);
                    $row[] = $obj->namasatuan; 
                    $row[] = $obj->userbuat; 
                    $row[] = $obj->waktudibuat; 
                    $row[] = '<a id="hapuspenggunaan" idbarang="'.$obj->idbarang.'" idbarangpembelian="'.$obj->idbarangpembelian.'"  idbarangpenggunaan="'.$obj->idbarangpenggunaan.'" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> hapus</a>'; //<a id="pembetulan" class="btn btn-warning btn-sm" data-toggle="tooltip" data-original-title="Pembetulan"><i class="fa fa-pencil"></i> pembetulan</a>
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_penggunaanbarang(),
            "recordsFiltered" => $this->mdatatable->filter_dt_penggunaanbarang(),
            "data" =>$data,
            "last" => $lastquery
        );
        //output dalam format JSON
        echo json_encode($output);        
    }
    
    public function add_penggunaanbarang()
    {
        $data['content_view'] = 'farmasi/v_penggunaanbarang';
        $data['active_menu']  = 'farmasi';
        $data['active_menu_level'] = '';
        $data['active_sub_menu']= 'penggunaanbarang';
        
        $data['title_page']  = 'Form Input '.(($this->session->userdata('levelgudang') == 'depo') ? 'Penggunaan Barang' : 'Penyesuaian Stok' ) .ucwords(strtolower($this->session->userdata('unitterpilih')));
        $data['mode']        = 'forminput';
        $data['plugins']     = [PLUG_DROPDOWN,PLUG_VALIDATION];
        $data['jsmode']      = 'forminput';
        $data['script_js']   = ['farmasi/js_penggunaanbarang'];
        $this->load->view('v_index',$data);
    }
    
    public function delete_penggunaanbarang()
    {
        $idunit = $this->session->userdata('idunitterpilih');
        $idbarangpenggunaan = $this->input->post('idbarangpenggunaan');
        $idbarang = $this->input->post('idbarang');
        $idbarangpembelian = $this->input->post('idbarangpembelian');
        
        
        //cek jumlah penggunaan detail
        $cekjumlah = $this->db->query("SELECT ifnull(sum(1),0) as jumlah FROM `rs_barang_penggunaan_detail` WHERE idbarangpenggunaan = '".$idbarangpenggunaan."' AND idunitfarmasi = '".$idunit."'")->row_array();
        
        if($cekjumlah['jumlah'] > 1)
        {
            $delete = $this->db->delete('rs_barang_penggunaan',['idbarangpenggunaan'=>$idbarangpenggunaan,'idunitfarmasi'=>$idunit,'idbarang'=>$idbarang,'idbarangpembelian'=>$idbarangpembelian]);
        }
        
        $delete = $this->db->delete('rs_barang_penggunaan_detail',['idbarangpenggunaan'=>$idbarangpenggunaan,'idunitfarmasi'=>$idunit]);
        pesan_success_danger($delete, 'Hapus Data Berhasil.', 'Hapus Data Gagal.','js');
    }
    
    public function save_penggunaanbarang()
    {
        $idbarangpenggunaan = $this->input->post('idbarangpenggunaan');
        $idunit = $this->session->userdata('idunitterpilih');
        
        $callid = "";
        if(empty($idbarangpenggunaan)){
            $callid = generaterandom(10);
        }else{
            $callid = '';
        }
        $data['iduserbuat'] = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER",$this->session->userdata('iduser')));
        $data['keterangan'] = $this->input->post('keterangan');
        $data['waktudibuat']= date('Y-m-d H:i:s');
        $data['idunitfarmasi'] = $idunit;
        $data['callid']     = $callid;
        
        $tabel = 'rs_barang_penggunaan';
        if(empty($idbarangpenggunaan))
        {
            $save = $this->db->insert($tabel,$data);
            $id = $this->db->select('idbarangpenggunaan')->get_where($tabel,['idunitfarmasi'=>$idunit,'callid'=>$callid])->row_array();
            $idbarangpenggunaan = $id['idbarangpenggunaan'];
            
            //call id dihilangkan otomatis per hari dari even jadwal harian di mysql
        }
//        else            
//        {
//            $save = $this->db->update('rs_barang_penggunaan',$data,['idbarangpenyesuaian'=>$idbarangpenyesuaian,'idunitfarmasi'=>$idunit]);
//        }
        
        
        $idbarang = $this->input->post('idbarang');
        $idbarangpembelian = $this->input->post('idbarangpembelian');
        $jumlah = $this->input->post('jumlah');
        foreach ($idbarang as $key=>$value)
        {
            $datadetail['idbarangpenggunaan']  = $idbarangpenggunaan;
            $datadetail['idbarang']            = $idbarang[$key];
            $datadetail['idbarangpembelian']   = $idbarangpembelian[$key];
            $datadetail['jumlah']              = $jumlah[$key];
            $datadetail['idunitfarmasi']       = $idunit;
            $save = $this->db->replace('rs_barang_penggunaan_detail',$datadetail);
        }        
        pesan_success_danger($save, 'Simpan Data Berhasil.', "Simpan Data Gagal.","js");
    }
    
}    
/* End of file ${TM_FILENAME:${1/(.+)/Cfarmasi.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:applicatagalion).+)/Cfarmasi/:application/controllers/${1/(.+)/Cfarmasi.php/}} */









