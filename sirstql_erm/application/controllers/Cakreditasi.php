<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cakreditasi extends MY_controller
{
    protected $tb_parent_akreditasi     = 'akre_kategori_title';
    protected $tb_sub_akreditasi        = 'akre_kategori_subparent';
    protected $tb_child_akreditasi      = 'akre_kategori_child';
    protected $tb_akre_tambah_data      = 'akre_tambah_data';
    protected $tb_akre_elemen_penilaian = 'akre_elemen_penilaian';
    protected $tb_login_user            = 'login_user';
    protected $tb_akre_pengaturan_pic   = 'akre_pengaturan_pic';
    protected $tb_rs_unit               = 'rs_unit';
    protected $tb_akre_unit             = 'akre_unit';
    protected $tb_akre_regulasi         = 'akre_regulasi';
    protected $tb_akre_item_regulasi    = 'akre_item_regulasi';
    protected $tb_akre_pengaturan_umum  = 'akre_pengaturan_umum';


    function __construct(){
        parent::__construct();

        /**
         * Not loged in redirect to login
         */
        if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
        
        $this->load->model('msqlbasic');

        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation','session'));

    }

    public function setting_dokumenakreditasi()
    {
        $data = [
            'content_view'      => 'akreditasi/v_dokumenakreditasi',
            'active_menu'       => 'akreditasi',
            'active_menu_level' => '',
        ];

        return $data;
    }

    public function cek_login_userakses()
    {
        return $this->pageaccessrightbap->ql_check_username( $this->pageaccessrightbap->ql_pic_akreditasi() );
    }

    public function dokumenakreditasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DOKUMENAKREDITASI))
        {
            $author_asesor = ( $this->get_session_username() == 'asesor' ) ? true : false;
            
            $data                       = $this->setting_dokumenakreditasi();
            $data['title_page']         = 'Dokumen Akreditasi';
            $data['mode']               = 'dok_akreditasi';
            $data['active_sub_menu']    = 'dokumenakreditasi';
            $data['script_js']          = ['js_akreditasi'];
            $data['kategori']           = $this->get_parent_kategori();
            $data['listdokumen']        = $this->listing_dokumentasi();
            $data['cek_user_akses']     = $this->cek_login_userakses();
            $data['asesor']             = $author_asesor;
            $data['plugins']            = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
            
            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function setting_kategoriakreditasi()
    {
        $data = [
            'content_view'      => 'akreditasi/v_kategoriakreditasi',
            'active_menu'       => 'akreditasi',
            'active_menu_level' => ''
        ];

        return $data;
    }

    public function setting_pengaturanpic()
    {
        $data = [
            'content_view'      => 'akreditasi/v_pengaturanpic',
            'active_menu'       => 'pengaturanpic',
            'active_menu_level' => ''
        ];

        return $data;
    }

    /**
     * new sembunyikan tabs
     * ikhsan
     * [+] Update 21092022
     */
    public function simpan_sembunyikan_tabs()
    {
        $post = $this->input->post();
        $hidden_akses_tab = $post['hidden_akses_tab'];

        if(!empty($hidden_akses_tab)){   
            $idusers = [];
            foreach( $hidden_akses_tab as $rowuser ){
                $idusers[] = $this->security->sanitize_filename( $rowuser );
            }    
            $iduser = implode(',',$idusers); 
        }else{
            $iduser = '';
        }

        $data = [
            'key_pengaturan' => 'hidden_data_pic',
            'value_pengaturan' => $iduser 
        ];

        $cek_available = $this->msqlbasic->row_db($this->tb_akre_pengaturan_umum,['key_pengaturan'=>'hidden_data_pic']);

        if( !empty($cek_available) ){
            $delete = $this->msqlbasic->delete_db(
                $this->tb_akre_pengaturan_umum,
                ['id'=>$cek_available['id']]
            );
        }

        $query = $this->msqlbasic->insert_data(
            $this->tb_akre_pengaturan_umum, 
            $data
        );
            
            
        if( $query ){
            $return = [
                'result'=>'success',
                'msg'=>"Data Berhasil di Simpan",
            ];
        }else{
            $return = ['result'=>'danger','msg'=>"Data Gagal di Simpan"];
        }

        echo json_encode($return);
    }

    /**
     * Update Pengaturan PIC
     * [+] Pengaturan Per Unit Berdasarkan PIC
     * [+] Update 21092022
     */
    public function pengaturanpic()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGATURANPIC)){

            $all_user               = $this->msqlbasic->show_db($this->tb_login_user);
            $pengaturan_pic         = $this->msqlbasic->row_db($this->tb_akre_pengaturan_pic);

            // [+] Update 21092022
            $pengaturan_umum_hidden    = $this->msqlbasic->row_db($this->tb_akre_pengaturan_umum,['key_pengaturan'=>'hidden_data_pic']);

            $list_unit              = $this->all_unit();


            $data                   = $this->setting_pengaturanpic();
            $data['title_page']     = 'Pengaturan';
            $data['mode']           = 'setting_pic';
            $data['active_sub_menu']= 'pengaturanpic';
            $data['pengaturan_pic'] = $pengaturan_pic;
            $data['alluser']        = $all_user;
            $data['allpic']         = all_pic();
            $data['list_unit']      = $list_unit;
            $data['iduserhiddenbab']= $pengaturan_umum_hidden;
            $data['script_js']      = ['js_akreditasi'];
            $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];


            $this->load->view('v_index', $data);

        }else{
            aksesditolak();
        }

    }

    public function get_simpan_pengaturanpic()
    {
        $this->form_validation->set_rules('allusers[]', 'All User', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('result'=>'failed','msg'=>validation_errors());
            echo json_encode( $return );
        }else{

            $post = $this->input->post();
            
            $iduser = [];
            $allusers = $post['allusers'];
            foreach( $allusers as $rowuser ){
                $iduser[] = $this->security->sanitize_filename( $rowuser );
            }     

            $data = ['author' => implode(',',$iduser) ];

            $cek_available = $this->msqlbasic->row_db($this->tb_akre_pengaturan_pic);

            if( !empty($cek_available) ){
                $delete = $this->msqlbasic->delete_db(
                    $this->tb_akre_pengaturan_pic,
                    ['id'=>$cek_available['id']]
                );
            }

            $query = $this->msqlbasic->insert_data(
                $this->tb_akre_pengaturan_pic, 
                $data
            );
            
            
            if( $query ){
                $return = [
                    'result'=>'success',
                    'msg'=>"Data Berhasil di Simpan",
                ];
            }else{
                $return = ['result'=>'danger','msg'=>"Data Gagal di Simpan"];
            }

            echo json_encode($return);

        }
    }

    public function get_all_users()
    {
        // $sql    = "SELECT * FROM ".$this->tb_login_user." WHERE `namauser` LIKE '%pic%' ";
        // $query  = $this->msqlbasic->custom_query($sql);
        $query      = $this->msqlbasic->row_db($this->tb_akre_pengaturan_pic);
        $authors    = explode(',', $query['author']);

        $datas = [];
        foreach( $authors as $row ){
            $datauser   = $this->msqlbasic->row_db($this->tb_login_user,['iduser'=>$row]);
            
            if( $datauser['namauser'] == 'superuser' || $datauser['namauser'] == 'asesor' ) continue;

            $datas[]    = $datauser;
        }

        return $datas;
    }

    public function kategoriakreditasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_KATEGORIAKREDITASI)){
        
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();

        $data                   = $this->setting_kategoriakreditasi();
        $data['title_page']     = 'Kategori Akreditasi';
        $data['mode']           = 'kat_akreditasi';
        $data['active_sub_menu']= 'kategoriakreditasi';
        $data['select_level']   = $this->get_level_select($author);
        $data['list_data']      = $this->list_data_kategori();
        $data['author']         = $this->get_session_username();
        $data['alluser']        = $this->get_all_users();
        $data['script_js']      = ['js_akreditasi'];
        $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];

        
        $this->load->view('v_index', $data);
        }
        else{
            aksesditolak();
        }
    }

    public function get_session_userid()
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        return $iduser;
    }

    public function get_session_username()
    {
        $username = $this->session->userdata('username');
        return $username;
    }

    public function setting_tambahdata()
    {
        $data = [
            'content_view'      => 'akreditasi/v_tambahdata',
            'active_menu'       => 'akreditasi',
            'active_menu_level' => ''
        ];

        return $data;
    }

    public function tambahdata()
    {
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();

        $data = $this->setting_tambahdata();
        $data['title_page']     = 'Tambah Data Akreditasi';
        $data['mode']           = 'tambah_akreditasi';
        $data['active_sub_menu']= 'dokumenakreditasi';
        $data['select_parent']  = $this->get_parent_kategori(true,['author'=>$author]);
        $data['author']         = $this->get_session_username();
        $data['alluser']        = $this->get_all_users();
        $data['script_js']      = ['js_akreditasi'];
        $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
                
        $this->load->view('v_index', $data);
    }

    public function setting_uploaddocumen()
    {
        $data = [
            'content_view'      => 'akreditasi/v_uploaddocumen',
            'active_menu'       => 'akreditasi',
            'active_menu_level' => ''
        ];

        return $data;
    }

    public function uploaddocumen()
    {
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();

        $data = $this->setting_uploaddocumen();
        $data['title_page']     = 'Tambah Data Akreditasi';
        $data['mode']           = 'uploaddocumen';
        $data['active_sub_menu']= 'dokumenakreditasi';
        $data['select_parent']  = $this->get_parent_kategori(TRUE,['author'=>$author]);
        $data['script_js']      = ['js_akreditasi'];
        $data['plugins']        = [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ];
                
        $this->load->view('v_index', $data);
    }

    public function storeData(){
        
        $namaKategori       = (string)$this->input->post('namakategori');
        $idkategorititle    = $this->input->post('idkategorititle');
        $rolecategory       = $this->input->post('rolecategory');
        $username_login     = $this->input->post('username_login');

        /** 
        * PR khusus super user
        * DONE - 23/07/2022
        */
        if( $username_login == 'superuser' ){
            $author = $this->input->post('author_id');
        }else{
            $author = $this->get_session_userid();
        }

        if($rolecategory == "parent") {
            $query = $this->db->query("Insert into akre_kategori_title (nama_kategori_title,id_level,author) values('".$namaKategori."',0,'".$author."')");
            // return $query;
        }
        else if($rolecategory == "subparent"){
            $query = $this->db->query("Insert into akre_kategori_subparent (id_kategori_title, nama_kategori) values(".$idkategorititle.",'".$namaKategori."')");
            // return $query;
        }
        else if($rolecategory == "childcategory"){
            $query = $this->db->query("INSERT INTO akre_kategori_child (id, id_kategori_subparent, nama_kategori, isi) VALUES (NULL,".$idkategorititle.", '".$namaKategori."', '')");
            // return $query;
        }

        if( $query ){
            $return = ['result'=>'success','msg'=>'Kategori Berhasil Di Simpan'];
        }else{
            $return = ['result'=>'danger','msg'=>'Kategori Gagal Di Simpan'];
        }

        echo json_encode($return);
    }

    public function hapusData(){
        $role = (string)$this->input->post('role');
        $id = $this->input->post('id');

        
        if($role == "parent"){
            $query = "";
            $query .= $this->db->query("DELETE FROM akre_kategori_title where akre_kategori_title.id =".$id);
            $query .= $this->db->query("DELETE FROM akre_kategori_child WHERE akre_kategori_child.id_kategori_subparent =  (SELECT akre_kategori_subparent.id FROM akre_kategori_subparent  WHERE akre_kategori_subparent.id_kategori_title = ".$id." LIMIT 1)");
            $query .= $this->db->query("DELETE FROM akre_kategori_subparent WHERE akre_kategori_subparent.id_kategori_title =".$id);
            // return $query;
        }
        else if($role == "subparent"){
            $query = "";
            $query .= $this->db->query("DELETE FROM akre_kategori_subparent where akre_kategori_subparent.id =".$id);
            $query .= $this->db->query("DELETE FROM akre_kategori_child WHERE akre_kategori_child.id_kategori_subparent =".$id);
            // return $query;
        }
        else if($role == "child"){
            $query = $this->db->query("DELETE FROM akre_kategori_child where akre_kategori_child.id =".$id);
            // return $query;
        }

        if( $query ){
            echo json_encode(['result'=>'success','msg'=>"Kategori Berhasil Di Hapus :)"]);
        }else{
            echo json_encode(['result'=>'danger','msg'=>"Kategori Gagal di Hapus :("]);
        }
    }
    
    public function get_level_select($author="")
    {
        if(empty($author)){
            $queryakrekategorititle = $this->msqlbasic->show_db($this->tb_parent_akreditasi);
        }else{
            $queryakrekategorititle = $this->msqlbasic->show_db($this->tb_parent_akreditasi,['author'=>$author]);
        }

        $grup_query = [];
        foreach( $queryakrekategorititle as $i => $row ){
            $andwhere_sub   = ['id_kategori_title'=>$row['id']]; 
            $query_sub      =  $this->msqlbasic->show_db($this->tb_sub_akreditasi,$andwhere_sub);
            $row['sub']     = $query_sub;

            $grup_query[] = $row;
        }

        return $grup_query;
    }

    public function list_data_kategori()
    {
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();

        $group_query = [];
        foreach( $this->get_level_select($author) as $group_subcategory ){
                         
            $group_sub=[];
            foreach($group_subcategory['sub'] as $subparent){

                $andwhere_child   = ['id_kategori_subparent'=>$subparent['id']]; 
                $query_child      =  $this->msqlbasic->show_db($this->tb_child_akreditasi,$andwhere_child);

                $subparent['child'] = $query_child;

                $group_sub[] = $subparent;
            }

            $group_subcategory['sub'] = $group_sub;
            $group_query[] = $group_subcategory;

        }

        return $group_query;
        
    }

    public function get_parent_kategori($found=false,$andwhere=array())
    {

        if( !$found ){
            // if( !empty($andwhere['author']) ){
            //     $andwhere;
            // }else{
            //     $andwhere = array();
            // }

            // $query  = $this->msqlbasic->show_db($this->tb_parent_akreditasi,$andwhere);
            $sql    = "SELECT * FROM ".$this->tb_parent_akreditasi." ";

            $user_hidden = get_hidden_kategori_per_pic();

            if( !empty($andwhere) ){
                if( !empty($andwhere['author']) ){
                    $sql    .= " WHERE author ='".$andwhere['author']."' ";
                }
            }
            if( !empty($user_hidden) ){
                $sql    .= " WHERE author NOT IN (".$user_hidden.") ";
            }

            $query  = $this->msqlbasic->custom_query($sql);

        }else{
            
            $sql    = "SELECT a.id_kategori_title as idparent,b.nama_kategori_title FROM ".$this->tb_akre_tambah_data." a ";
            $sql    .= " LEFT JOIN ".$this->tb_parent_akreditasi." b ON b.id = a.id_kategori_title ";
            if( !empty($andwhere) ){
                if( !empty($andwhere['author']) ){
                    $sql    .= " WHERE b.author ='".$andwhere['author']."' ";
                }
            }

            $sql    .= " GROUP BY a.id_kategori_title ";

            $query  = $this->msqlbasic->custom_query($sql);
        }
        
        return $query;
    }

    public function get_sub_kategori()
    {   
        $parentid   = $this->input->post('parentid');
        $mode       = $this->input->post('mode');
        if( $mode == 'uploaddocumen' ){
            $sql    = "SELECT a.id_kategori_subparent as id,b.nama_kategori FROM ".$this->tb_akre_tambah_data." a ";
            $sql    .= " LEFT JOIN ".$this->tb_sub_akreditasi." b ON b.id = a.id_kategori_subparent ";
            $sql    .= " WHERE a.id_kategori_title = '".$parentid."' GROUP BY a.id_kategori_subparent ";
            $query  = $this->msqlbasic->custom_query($sql);
        }else{
            $query = $this->msqlbasic->show_db($this->tb_sub_akreditasi, ['id_kategori_title' => $parentid]);
        }
        
        echo json_encode($query);
    }

    public function get_child_kategori()
    {
        $subparentid = $this->input->post('subparentid');
        $mode       = $this->input->post('mode');
        if( $mode == 'uploaddocumen' ){
            $sql    = "SELECT a.id_kategori_child as id,b.nama_kategori FROM ".$this->tb_akre_tambah_data." a ";
            $sql    .= " LEFT JOIN ".$this->tb_child_akreditasi." b ON b.id = a.id_kategori_child ";
            $sql    .= " WHERE a.id_kategori_subparent = '".$subparentid."' AND a.id_kategori_child != 0 GROUP BY a.id_kategori_child ";
            $query  = $this->msqlbasic->custom_query($sql);
        }else{
            $query = $this->msqlbasic->show_db($this->tb_child_akreditasi, ['id_kategori_subparent' => $subparentid]);
        }
        echo json_encode($query);
    }

    public function get_simpan_dokumentasi()
    {
        /** 
        * PR khusus super user belum bisa
        * [ DONE ] - 23/07/2022 
        */
        if( !empty($this->input->post('username')) &&  $this->input->post('username') == 'superuser' ){
            $this->form_validation->set_rules('user-pic', 'PIC', 'required',
                    array('required' => 'Harap Pilih  %s.')
            );
        }

        $this->form_validation->set_rules('parent-kategori', 'Parent Kategori', 'required',
                array('required' => 'Harap Pilih  %s.')
        );
        $this->form_validation->set_rules('sub-kategori', 'Sub Kategori', 'required',
                array('required' => 'Harap Pilih  %s.')
        );
        $this->form_validation->set_rules('standar-judul', 'Standar Judul', 'required',
                array('required' => 'Harap isi %s.')
        );
        $this->form_validation->set_rules('itemelemen[]', 'Item Element', 'required',
                array('required' => 'Harap isi %s.')
        );
        
        if ($this->form_validation->run() == FALSE) {
            $return = array('result'=>'failed','msg'=>validation_errors());
            echo json_encode( $return );
        }else{
            $post = $this->input->post();
            $parent_kat     = $this->security->sanitize_filename($post['parent-kategori']);
            $sub_kat        = $this->security->sanitize_filename($post['sub-kategori']);
            $child_kat      = $this->security->sanitize_filename($post['child-sub-kategori']);
            $standar_judul  = $this->security->sanitize_filename($post['standar-judul']);
            $username_login = $this->security->sanitize_filename($post['username']);
            
            $datecreated    = date('Y-m-d');
            
            /** 
            * PR khusus super user belum bisa
            * [ DONE ] - 23/07/2022 
            */
            if( $username_login == 'superuser' ){
                $author = $this->security->sanitize_filename($post['user-pic']);
            }else{
                $author = $this->get_session_userid();
            }

           
            $itemelemen     = $post['itemelemen'];
            foreach( $itemelemen as $rowelemen ){
                $this->security->sanitize_filename( $rowelemen );
            }

            /**
             * Cek database ada data sub parent dengan id child = 0
             * jika tidak insert parent dan sub parent terlebih dahulu abaikan child
             */
            $query_cek_insert_subparent = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_title'=>$parent_kat,'id_kategori_subparent'=>$sub_kat,'id_kategori_child' =>0]
            );

            if( empty($query_cek_insert_subparent) ){
                $child_kat = 0;
            }else{
                $child_kat;
            } //end if query cek insert parent

        
            $data_tambah_akre = [
                "id_kategori_title" => (int) $parent_kat,
                "id_kategori_subparent" => (int) $sub_kat,
                "id_kategori_child" => (int) $child_kat,
                "standar_judul" =>   $standar_judul,
                "datecreated" => $datecreated,
                "author" => $author
            ];
            
            # cek data last insert
            $cek_last_insert = $this->cek_data_insert(
                [
                    "id_kategori_title" => $parent_kat,
                    "id_kategori_subparent" =>  $sub_kat,
                    "id_kategori_child" => $child_kat
                ]
            );

            if( !empty($cek_last_insert) ){
                echo json_encode(array('result'=>'failed','msg'=>'Data Sudah di Masukan'));
            }else{
    
                $query = $this->msqlbasic->insert_data($this->tb_akre_tambah_data, $data_tambah_akre);
                
                if( $query > 0 ){
                    $found_elemen = false;
                    foreach( $itemelemen as $rowelemen ){

                        $data_element = [
                            'id_tambah_data' =>(int) $query,
                            'elemen_penilaian' =>$rowelemen,
                        ];

                        $query_element = $this->msqlbasic->insert_data($this->tb_akre_elemen_penilaian, $data_element);

                        if( $query_element > 0 ){
                            $found_elemen = true;
                        }
                    }

                    if( $found_elemen ){
                        $return = array('result'=>'success','msg'=>'Data Berhasil Di Simpan','url'=>base_url().'cakreditasi/dokumenakreditasi');
                    }else{
                        $return = array('result'=>'failed','msg'=>'Gagal Simpan Elemen Penilaian');
                    }

                }else{

                    $return = array('result'=>'failed','msg'=>'Gagal Disimpan');

                }
                
                echo json_encode( $return );
            }
            
        }

    }

    public function cek_data_insert($data=array())
    {
        $parent_kat = $data['id_kategori_title'];
        $sub_kat    = $data['id_kategori_subparent'];
        $child_kat  = $data['id_kategori_child'];

        if( !empty($parent_kat) && !empty($sub_kat) && !empty($child_kat) ){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                $data
            );
        }else if(!empty($parent_kat) && !empty($sub_kat) && empty($child_kat) ){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_title'=>$parent_kat,'id_kategori_subparent'=>$sub_kat]
            );
        }else if( !empty($parent_kat) && empty($sub_kat) && empty($child_kat) ){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_title'=>$parent_kat]
            );
        }else if(empty($parent_kat) && !empty($sub_kat) && !empty($child_kat)){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_subparent'=>$sub_kat,'id_kategori_child'=>$child_kat]
            );
        }else if( empty($parent_kat) && !empty($sub_kat) && empty($child_kat) ){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,['id_kategori_subparent'=>$sub_kat]
            );
        }else if(empty($parent_kat) && empty($sub_kat) && !empty($child_kat)){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_child'=>$child_kat]
            );
        }else if(!empty($parent_kat) && empty($sub_kat) && !empty($child_kat)){
            $query = $this->msqlbasic->row_db(
                $this->tb_akre_tambah_data,
                ['id_kategori_title'=>$parent_kat,'id_kategori_child'=>$child_kat]
            );
        }

        return $query;

    }

    public function get_simpan_file_dokumentasi()
    {
        $this->form_validation->set_rules('parent-kategori', 'Parent Kategori', 'required',
                array('required' => 'Harap Pilih  %s.')
        );

        $this->form_validation->set_rules('sub-kategori', 'Sub Kategori', 'required',
            array('required' => 'Harap Pilih  %s.')
        );

        $this->form_validation->set_rules('judul_penilaian', 'Judul Penilaian', 'required',
            array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            // echo 'Harap isi dengan benar form';
            echo json_encode(['result'=>'failed','msg'=>'Harap isi dengan benar form']);
        }else{
            
            $post               = $this->input->post();
            $parent_kategori    = (int) $this->security->sanitize_filename($post['parent-kategori']);
            $sub_kategori       = (int) $this->security->sanitize_filename($post['sub-kategori']);
            $child_kategori     = (int) $this->security->sanitize_filename($post['child-sub-kategori']);
            $judul_penilaian    = $this->security->sanitize_filename($post['judul_penilaian']);

            $paths = FOLDERNASSIMRS.'/pdf_akreditasi/';
            $names = [];
            $found = false;

            $count_uploaded_files = count( $_FILES['filedocument']['name'] );

            $files = $_FILES;
            for( $i = 0; $i < $count_uploaded_files; $i++ ){

                $_FILES['filedoc'] = [
                    'name'     => $files['filedocument']['name'][$i],
                    'type'     => $files['filedocument']['type'][$i],
                    'tmp_name' => $files['filedocument']['tmp_name'][$i],
                    'error'    => $files['filedocument']['error'][$i],
                    'size'     => $files['filedocument']['size'][$i]
                ];
        
                $tmpName        = $files['filedocument']['tmp_name'][$i];       
                $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['filedocument']['name'][$i]));
                $newFileName    = '/pdf_akreditasi/'.$fileName;
                
                if( $files['filedocument']['type'][$i] == 'application/pdf'){   
                
                    // $paths[]          = $newFileName;                
                    $names[]          = $fileName;    

                    $config = $this->ftp_upload();
                    
                    # upload ke nas storage
                    $upload = $this->ftp->connect($config);
                    $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                    $upload = $this->ftp->close();
                    
                    $found = true;

                } //end if cek dokumen is pdf
                
            } // .end for files

            if( $found ){
                # cek data akre
                $cek_data_akre = $query = $this->msqlbasic->row_db(
                    $this->tb_akre_tambah_data,
                    [
                        "id_kategori_title" => $parent_kategori,
                        "id_kategori_subparent" =>  $sub_kategori,
                        "id_kategori_child" => $child_kategori
                        ]
                    );
                    
                    $id_tambah_akre = $cek_data_akre['id_data_kategori'];
                    
                    $data_element = [
                        'id_tambah_data' =>(int) $id_tambah_akre,
                        'elemen_penilaian' =>$judul_penilaian,
                        'filename' => serialize( $names ),
                        'pathname' => $paths,
                    ];

                    $query = $this->msqlbasic->insert_data($this->tb_akre_elemen_penilaian, $data_element);

                    if( $query > 0 ){
                        echo json_encode([
                            'result'=>'success',
                            'msg'=>'Data Berhasil di Simpan',
                            'url'=>base_url().'cakreditasi/dokumenakreditasi'
                        ]);
                    }else{
                        echo json_encode([
                            'result'=>'failed',
                            'msg'=>'Data Gagal di Simpan'
                        ]);
                    } //end if query insert is true

            }else{
                echo json_encode(['result'=>'failed','msg'=>'Gagal Upload File, Tipe File Harus PDF']);
            } //end if found file is pdf

        } // endif validation
    }
    
    public function listing_dokumentasi($andwhere="")
    {
        $sql = "SELECT masters.*,elm.elemen_penilaian,elm.filename,elm.pathname,elm.nilai
                FROM ".$this->tb_akre_tambah_data." masters
                LEFT JOIN ".$this->tb_akre_elemen_penilaian." elm ON elm.id_tambah_data = masters.id_data_kategori WHERE 1 ".$andwhere;
        
        $query = $this->msqlbasic->custom_query($sql); 

        return $query;
    }

    /*
    public function checking_userpic_byid(){
        $author = $this->get_session_userid();

        // $cek_user_login = row_db($this->tb_login_user,['id'=>$author]);
    }*/

    /**
     * [+] Update 21092022
     */
    public function get_handle_click_kategori()
    {
        $post       = $this->input->post();
        $idparent   = (int) $this->security->sanitize_filename( $post['idparent'] );

        $user_hidden = get_hidden_kategori_per_pic();
        
        /* old query
            $andwhere   = " AND masters.id_kategori_title='".$idparent."'";
            $query      = $this->listing_dokumentasi($andwhere);
        */
        if( $idparent == 0 ){
            if( empty($user_hidden) ){
                $query_show = $this->msqlbasic->show_db($this->tb_akre_tambah_data);
            }else{
                $query_show = $this->msqlbasic->show_db_where_not_in($this->tb_akre_tambah_data,'author',explode(",",$user_hidden));
            }
        }else{
            if( empty($user_hidden) ){
                $query_show = $this->msqlbasic->show_db($this->tb_akre_tambah_data,['id_kategori_title'=>$idparent]);
            }else{
                $sql = "SELECT * FROM ".$this->tb_akre_tambah_data." WHERE  id_kategori_title='".$idparent."' AND author NOT IN (".$user_hidden.")";
                $query_show = $this->msqlbasic->custom_query($sql);
            }
        }

        $data_new = [];
        foreach( $query_show as $akre ){
            
            
            $parent = $akre['id_kategori_title'];
            $sub    = (int) $akre['id_kategori_subparent'];
            $child  = $akre['id_kategori_child'];
            
            $sub_name_query = $this->msqlbasic->row_db($this->tb_sub_akreditasi,['id'=>$sub]);
            $sub_name = $sub_name_query['nama_kategori'] ?? null;

            // $sub_name =  $this->msqlbasic->row_db($this->tb_sub_akreditasi,['id'=>$sub])['nama_kategori'];
            

            $child_name = 'nonchild';
            if( !empty($child) ){
                $child;     
                $child_name = 'child';
                $get_child_name = $this->msqlbasic->row_db($this->tb_child_akreditasi,['id'=>$child] )['nama_kategori'];
            }

            
            if( $child_name == 'nonchild' ){
                $data_new['data'][$sub_name]['judul'] = $akre['standar_judul'];
                $data_new['data'][$sub_name]['data_id'] = $akre['id_data_kategori'];
                $data_new['data'][$sub_name]['author'] = $akre['author'];
            }else{
                $data_new['data'][$sub_name][$child_name][] = [
                    'judul' => $akre['standar_judul'],
                    'childname' => $get_child_name,
                    'data_id' => $akre['id_data_kategori'],
                    'author' => $akre['author'],
                ];
            }
        }


        $datas =[]; 
        if( !empty( $data_new['data'] ) ){
            foreach( $data_new['data'] as $key => $data_without_file ){
                
                $data_without_file['subparent'] = $key;
                
                if( !empty($data_without_file['child']) ){
                    $childs = $data_without_file['child'];
                    
                    if( !empty( $data_without_file['data_id'] ) ){

                        
                        $dataid                     = $data_without_file['data_id'];
                        $query_files                = $this->msqlbasic->show_db($this->tb_akre_elemen_penilaian,['id_tambah_data'=>$dataid] ); 
                        $data_without_file['files'] = $query_files;
                        
                        foreach( $childs as $i => $row_child ){
                            $child_judul        = $row_child['judul']; 
                            $child_name         = $row_child['childname']; 
                            $child_dataid       = $row_child['data_id']; 
                            $query_files        = $this->msqlbasic->show_db($this->tb_akre_elemen_penilaian,['id_tambah_data'=>$child_dataid  ] );
                            $data_without_file['child'][$i]['files'] = $query_files;
                        }
                    }// end if data_id
                    
                }else{
                    $judul          = $data_without_file['judul'];
                    $dataid         = $data_without_file['data_id'];
                    $query_files    = $this->msqlbasic->show_db($this->tb_akre_elemen_penilaian,['id_tambah_data'=>$dataid  ] ); 
                    $data_without_file['files']     = $query_files;
                } 

                $datas[] = $data_without_file;   
            }

        }
        echo json_encode( ['result'=>$this->loop_tbody($datas)] );
        
    }

    public function namefile_maxlength($str){
        if (strlen($str) > 10) return substr($str, 0, 100) . '...';
    }

    public function loop_tbody($query_listbykat)
    {
        ob_start();
        // $cek_user_akses = $this->cek_login_userakses();
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();
        $author_asesor = ( $this->get_session_username() == 'asesor' ) ? true : false;

        include FCPATH. 'application/views/akreditasi/v_list-table-akreditasi.php';
        return ob_get_clean();
    }

    public function get_handle_delete_file()
    {
        $post = $this->input->post();
        $id_element = (int) $post['id_element_akre'];        
        
        $get_data = $this->msqlbasic->row_db($this->tb_akre_elemen_penilaian,['id'=>$id_element]);
        
        $found = false;
        
        if( !empty( $get_data ) ){
            if( !empty($get_data['filename']) ){
                $path = $get_data['pathname'];
                $filename_array = $get_data['filename'];
                foreach( unserialize( $filename_array ) as $namefile ){
                    $urlnas = $path.$namefile;
                    
                    //hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();

                    if( $delete ){
                        $found = true;
                    }
                    
                } //end foreach serialize
                
            } //end if get_data filename
            
        } //end if get_data

        /*if( $found ){
            $query = $this->msqlbasic->delete_db($this->tb_akre_elemen_penilaian,['id'=>$id_element]);
            if( $query ){
                $return = ['result'=>'success','msg'=>'Berhasil di Hapus'];
            }else{
                $return = ['result'=>'danger','msg'=>'gagal hapus database file'];
            }

        }else{
            $return = ['result'=>'danger','msg'=>'gagal hapus file'];
        }*/

        $query = $this->msqlbasic->delete_db($this->tb_akre_elemen_penilaian,['id'=>$id_element]);
        if( $query ){
            $return = ['result'=>'success','msg'=>'Berhasil di Hapus'];
        }else{
            $return = ['result'=>'danger','msg'=>'gagal hapus database file'];
        }


        echo json_encode( $return );
        
    }

    public function get_handle_delete_group_akreditasi()
    {
        $post       = $this->input->post();
        $id_data    = (int) $post['id_data'];

        $cek_data_file = $this->msqlbasic->show_db($this->tb_akre_elemen_penilaian,['id_tambah_data'=>$id_data]);

        if( !empty( $cek_data_file ) ){
            foreach( $cek_data_file as $row_file ){
                $id_file_akre = $row_file['id'];
                $found = false;
                if( !empty( $row_file['filename'] ) ){
                    $path = $row_file['pathname'];
                    foreach( unserialize($row_file['filename']) as $file ){
                        $urlnas = $path.$file;
                        
                        //hapus file di NAS
                        $config = $this->ftp_upload();
                        $delete = $this->ftp->connect( $config );
                        $delete = $this->ftp->delete_file( $urlnas );
                        $delete = $this->ftp->close();

                        if( $delete ){
                            $found = true;
                        }
                    } //end foreach serialize

                } //end if cek files name serialize

                // if( $found ){
                $this->msqlbasic->delete_db($this->tb_akre_elemen_penilaian,['id'=>$id_file_akre]); 
                // }

            } //end foreach cek data file

        } //end if empty cek data file

        $query  = $this->msqlbasic->delete_db($this->tb_akre_tambah_data,['id_data_kategori'=>$id_data]);

        if( $query ){
            $return = ['result'=>'success','msg'=>'Data Berhasil Dihapus :)'];
        }else{
            $return = ['result'=>'danger','msg'=>'Gagal Menghapus Data'];
        }

        echo json_encode( $return );
        
    }

    public function get_simpan_file_element()
    {
        $this->form_validation->set_rules('idakre', 'idakre', 'required',
            array('required' => 'data kosong  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['result'=>'danger','msg'=>'Gagal Silahkan Ulangi Kembali']);
        }else{

            $post       = $this->input->post();
            $idakre    = (int) $this->security->sanitize_filename($post['idakre']);

            $paths = FOLDERNASSIMRS.'/pdf_akreditasi/';
            $names = [];
            $found = false;

            $count_uploaded_files = count( $_FILES['filedocument']['name'] );

            $files = $_FILES;
            for( $i = 0; $i < $count_uploaded_files; $i++ ){

                $_FILES['filedoc'] = [
                    'name'     => $files['filedocument']['name'][$i],
                    'type'     => $files['filedocument']['type'][$i],
                    'tmp_name' => $files['filedocument']['tmp_name'][$i],
                    'error'    => $files['filedocument']['error'][$i],
                    'size'     => $files['filedocument']['size'][$i]
                ];
                        
                if( $files['filedocument']['type'][$i] == 'application/pdf'){   
                    
                    $tmpName        = $files['filedocument']['tmp_name'][$i]; 
                    $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['filedocument']['name'][$i]));
                    $newFileName    = '/pdf_akreditasi/'.$fileName;
                
                    $names[]        = $fileName;    

                    $config = $this->ftp_upload();
                    
                    # upload ke nas storage
                    $upload = $this->ftp->connect($config);
                    $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                    $upload = $this->ftp->close();
                    
                    $found = true;

                } //end if cek dokumen is pdf
                
            } // .end for files

            if( $found ){
                
                $data_element = [
                    'filename'  => serialize( $names ),
                    'pathname'  => $paths,
                ];

                #update
                $query = $this->msqlbasic->update_db($this->tb_akre_elemen_penilaian,['id'=>$idakre],$data_element);
                
                if( $query > 0 ){
                    echo json_encode([
                        'result'=>'success',
                        'msg'=>'Data Berhasil di Simpan',
                        'url'=>base_url().'cakreditasi/dokumenakreditasi'
                    ]);
                }else{
                    echo json_encode([
                        'result'=>'primary',
                        'msg'=>'Data Tidak Di Update'
                    ]);
                } //end if query insert is true

            }else{
                echo json_encode(['result'=>'danger','msg'=>'Gagal Upload File, Tipe File Harus PDF']);
            } //end if found file is pdf

        } //end validation
    }

    public function get_handle_delete_perfile()
    {
        $post       = $this->input->post();
        $idfile     = (int) $this->security->sanitize_filename($post['idfile']);
        $keyfile    = (int) $this->security->sanitize_filename($post['keyfile']);
        $idkategori = (int) $this->security->sanitize_filename($post['idkategori']);
        
        $query_files = $this->msqlbasic->row_db($this->tb_akre_elemen_penilaian,['id'=>$idfile]);

        $path = $query_files['pathname'];
        $filename_array = $query_files['filename'];
        $found = false;
        $names =[];
        foreach( unserialize( $filename_array ) as $key => $namefile ){
            
            if( $key == $keyfile ){
                $urlnas = $path.$namefile;
                
                //hapus file di NAS
                $config = $this->ftp_upload();
                $delete = $this->ftp->connect( $config );
                $delete = $this->ftp->delete_file( $urlnas );
                $delete = $this->ftp->close();
            }else{
                $names[]= $namefile;
            }
                    
        } //end foreach serialize

        $data_element = [
            'filename'  => empty($names) ? '' : serialize( $names ),
            'pathname'  => empty($names) ? '' : $path,
        ];

        #update
        $query  = $this->msqlbasic->update_db($this->tb_akre_elemen_penilaian,['id'=>$idfile],$data_element);
        $return = ['result'=>'success' ,'msg'=>'Berhasil di Hapus','parentid'=>$idkategori];
        echo json_encode( $return );
    }

    public function get_edit_standar_judul()
    {
        $post        = $this->input->post();
        $id_standar  = (int) $this->security->sanitize_filename($post['id_standar']);
        $idkategori  = (int) $this->security->sanitize_filename($post['idkategori']);
        $query       = $this->msqlbasic->row_db($this->tb_akre_tambah_data,['id_data_kategori'=>$id_standar]);
        
        $standar_judul = $query['standar_judul'];

        $data = ['standar_judul'=>$standar_judul,'id_standar'=>$id_standar,'idkategori'=>$idkategori];
        
        echo json_encode( $data );

    }

    public function get_simpan_editstandarjudul()
    {
        $this->form_validation->set_rules('editstandarjudul', 'Standar Judul', 'required',
                array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['result'=>'danger','msg'=>'Harap isi dengan benar form']);
        }else{
            $post               = $this->input->post();
            $editstandarjudul   = (string) $this->security->sanitize_filename( $post['editstandarjudul'] );
            $id_standar         = (int) $this->security->sanitize_filename( $post['id_standar'] );
            $idkategori         = (int) $this->security->sanitize_filename( $post['idkategori'] );

            $data = ['standar_judul' => $editstandarjudul];

            #update
            $query = $this->msqlbasic->update_db(
                $this->tb_akre_tambah_data,
                ['id_data_kategori'=>$id_standar],
                $data
            );
                
            if( $query > 0 ){
                echo json_encode([
                    'result'=>'success',
                    'msg'=>'Data Berhasil di Update',
                    'idkategori'=>$idkategori,
                ]);
            }else{
                echo json_encode([
                    'result'=>'danger',
                    'msg'=>'Data Tidak Di Update',
                ]);
            } //end if query insert is true

        } // end.validation

    }

    public function get_edit_element_judul()
    {
        $post        = $this->input->post();
        $idfile      = (int) $this->security->sanitize_filename($post['idfile']);
        $idkategori  = (int) $this->security->sanitize_filename($post['idkategori']);
        $query       = $this->msqlbasic->row_db($this->tb_akre_elemen_penilaian,['id'=>$idfile]);
        
        $elemen_penilaian = $query['elemen_penilaian'];

        $data = ['elemen_penilaian'=>$elemen_penilaian,'idfile'=>$idfile,'idkategori'=>$idkategori];
        
        echo json_encode( $data );
    }

    public function get_simpan_editelementpenilaian()
    {
        $this->form_validation->set_rules('editpenilaian', 'Elemen Penilaian', 'required',
                array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['result'=>'danger','msg'=>'Harap isi dengan benar form']);
        }else{
            $post               = $this->input->post();
            $editpenilaian      = (string) $this->security->sanitize_filename( $post['editpenilaian'] );
            $idfile             = (int) $this->security->sanitize_filename( $post['idfile'] );
            $idkategori         = (int) $this->security->sanitize_filename( $post['idkategori'] );

            $data = ['elemen_penilaian' => $editpenilaian];

            #update
            $query = $this->msqlbasic->update_db(
                $this->tb_akre_elemen_penilaian,
                ['id'=>$idfile],
                $data
            );
                
            if( $query > 0 ){
                echo json_encode([
                    'result'=>'success',
                    'msg'=>'Data Berhasil di Update',
                    'idkategori'=>$idkategori,
                ]);
            }else{
                echo json_encode([
                    'result'=>'danger',
                    'msg'=>'Data Tidak Di Update',
                ]);
            } //end if query insert is true

        } // end.validation
    }

    public function get_simpan_editnilai()
    {
        $this->form_validation->set_rules('nilai-element', 'Nilai', 'required',
            array('required' => 'Harap Pilih Nilai  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['result'=>'danger','msg'=>'Harap isi dengan benar Nilai']);
        }else{
            $post               = $this->input->post();
            $idakre             = (int) $this->security->sanitize_filename( $post['idakre'] );
            $nilai_element      = (string) $this->security->sanitize_filename( $post['nilai-element'] );
            $idkategori         = (string) $this->security->sanitize_filename( $post['idkategori'] );
            
            $data = ['nilai' => $nilai_element];

            #update
            $query = $this->msqlbasic->update_db(
                $this->tb_akre_elemen_penilaian,
                ['id'=>$idakre],
                $data
            );
                
            if( $query > 0 ){
                echo json_encode([
                    'result'=>'success',
                    'msg'=>'Data Berhasil di Update',
                    'idkategori'=>$idkategori,
                ]);
            }else{
                echo json_encode([
                    'result'=>'danger',
                    'msg'=>'Data Tidak Di Update',
                ]);
            } //end if query insert is true

        } // end.validation
    }

    public function get_simpan_file_tambahandocument()
    {
        $this->form_validation->set_rules('idakre', 'idakre', 'required',
            array('required' => 'data kosong  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['result'=>'danger','msg'=>'Gagal Silahkan Ulangi Kembali']);
        }else{

            $post           = $this->input->post();
            $idakre         = (int) $this->security->sanitize_filename($post['idakre']);
            $idkategori     = (int) $this->security->sanitize_filename($post['idkategori']);


            $paths = FOLDERNASSIMRS.'/pdf_akreditasi/';
            $names = [];
            $found = false;

            $count_uploaded_files = count( $_FILES['tambahandocument']['name'] );

            $files = $_FILES;
            for( $i = 0; $i < $count_uploaded_files; $i++ ){

                $_FILES['filedoc'] = [
                    'name'     => $files['tambahandocument']['name'][$i],
                    'type'     => $files['tambahandocument']['type'][$i],
                    'tmp_name' => $files['tambahandocument']['tmp_name'][$i],
                    'error'    => $files['tambahandocument']['error'][$i],
                    'size'     => $files['tambahandocument']['size'][$i]
                ];
                        
                if( $files['tambahandocument']['type'][$i] == 'application/pdf'){   
                    
                    $tmpName        = $files['tambahandocument']['tmp_name'][$i]; 
                    $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['tambahandocument']['name'][$i]));
                    $newFileName    = '/pdf_akreditasi/'.$fileName;
                
                    $names[]        = $fileName;  
                    
                    $config = $this->ftp_upload();
                    
                    # upload ke nas storage
                    $upload = $this->ftp->connect($config);
                    $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                    $upload = $this->ftp->close();
                    
                    $found = true;

                } //end if cek dokumen is pdf
                
            } // .end for files

            if( $found ){

                $getrow_element = $this->msqlbasic->row_db($this->tb_akre_elemen_penilaian,['id'=>$idakre]);
                $row_filename   = unserialize($getrow_element['filename']); //array
                
                $merge_names    = array_merge( $row_filename, $names );
                
                $data_element   = [
                    'filename'  => serialize( $merge_names ),
                    'pathname'  => $paths,
                ];

                #update
                $query = $this->msqlbasic->update_db($this->tb_akre_elemen_penilaian,['id'=>$idakre],$data_element);
                
                if( $query > 0 ){
                    echo json_encode([
                        'result'=>'success',
                        'msg'=>'Data Berhasil di Simpan',
                        'url'=>base_url().'cakreditasi/dokumenakreditasi'
                    ]);
                }else{
                    echo json_encode([
                        'result'=>'primary',
                        'msg'=>'Data Tidak Di Update'
                    ]);
                } //end if query insert is true

            }else{
                echo json_encode(['result'=>'danger','msg'=>'Gagal Upload File, Tipe File Harus PDF']);
            } //end if found file is pdf

        } //end validation 
    }

    public function berandaakreditasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DOKUMENAKREDITASI))
        {
            
            $data = [
                'content_view'      => 'akreditasi/v_berandaakreditasi',
                'active_menu'       => 'akreditasi',
                'active_menu_level' => '',
                'title_page'        => 'Beranda Akreditasi',
                'mode'              => 'beranda_akreditasi',
                'active_sub_menu'   => 'berandaakreditasi',
                'data_nilai'        => $this->total_nilai_by_kategori(),
                'script_js'         => ['js_akreditasi'],
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE,PLUG_CHART ]
            ];

            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function total_nilai_by_kategori()
    {
        $query = $this->listing_dokumentasi();

        $data_nilai = [];
        foreach( $query as $row ){
            
            if( $row['nilai'] == 'tdd' ) continue;

            $data_nilai[$row['id_kategori_title']][] = $row['nilai'];
        }

        $data_total = [];
        foreach($data_nilai as $key => $row){
            $sum          = array_sum($row);
            $count        = count($row);
            $get_row_parent = $this->msqlbasic->row_db($this->tb_parent_akreditasi,['id'=>$key,'id_level'=>'0']);
            
            
            if( empty($get_row_parent) ) continue;

            // if( $get_row_parent['id'] == null ) continue;

            $data_total[] = ['id_parent'=>$get_row_parent['id'],'name'=>$get_row_parent['nama_kategori_title'],'total_nilai'=>$sum,'count_element'=>$count];      
        }
        
        return $data_total;
    }

    public function get_chart_akreditasi()
    {
        $post = $this->input->post();
        $active_menu = $post['active_menu'];
        if( $active_menu == 'berandaakreditasi' ){
            $query = $this->total_nilai_by_kategori();

            $names = [];
            $nilais = [];
            
            foreach( $query as $row ){
                $names[]    = $row['name'];
                $average    = (int) $row['total_nilai']/(int) $row['count_element'];
                // $nilais[]   = (int) $row['total_nilai'];
                $nilais[]   = round( $average*10 ,2);
                
            }

            echo json_encode(['msg'=>'success','label_datas'=>$names,'datas'=>$nilais]);
        }
    }

    public function cek_pic_superuser_unit($cek_kondisi=false)
    {
        $id_userlogin           = $this->get_session_userid();

        $sql                    = "SELECT user_id_login,id_pic FROM " .$this->tb_akre_unit." ";
        $get_akreunit           = $this->msqlbasic->custom_query($sql);


        $found = false;
        foreach( $get_akreunit as $row ){
            
            if( !empty($row['id_pic']) ){
                $array_pic = unserialize($row['id_pic']);    
                $cekpic = ( in_array($id_userlogin,$array_pic) ) ? true : false ;
            }else{
                $cekpic = false;
            }
            
            
            if($cek_kondisi){

                if( $this->get_session_username() == 'superuser' || $cekpic == true ){
                    $found = true;
                }
            }else{
                if( $this->get_session_username() == 'superuser' ){
                    $found = true;
                }
            }
        }
        return $found;
    }

    public function dokumenunit()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DOKUMENUNIT))
        {
            $id_userlogin           = $this->get_session_userid();
            $get_akreunit           = $this->msqlbasic->row_db($this->tb_akre_unit,['user_id_login'=>$id_userlogin]);
            $user_idlogin_akre      = !empty($get_akreunit['user_id_login']) ? $get_akreunit['user_id_login'] : '' ;

            $all_regulasi           = $this->all_regulasi($user_idlogin_akre);
            $pic_regulasi           =  $this->show_all_userunit_regulasi(true);
           
            $cek_pic_superuser_unit = $this->cek_pic_superuser_unit(true);

            $cek_akre_userlogin     = $this->msqlbasic->row_db($this->tb_akre_unit,['user_id_login'=>$id_userlogin]);
            $cek_user_akre           = !empty( $cek_akre_userlogin ) ? true : false; 

            $data = [
                'content_view'      => 'akreditasi/v_dokumenunit',
                'active_menu'       => 'akreditasi',
                'active_menu_level' => '',
                'title_page'        => 'Dokumen Unit',
                'active_menu_level' => 'dokumenunit',
                'mode'              => '',
                'active_sub_menu'   => 'dokumenunit',
                'userid_akre'       => $user_idlogin_akre,
                'cek_akses'         => $cek_pic_superuser_unit,
                'cek_userlogin_akre' => $cek_user_akre,
                'script_js'         => ['js_akreditasi'],
                'listdokumen'       => $this->ajaxtable_dokumen_unit($all_regulasi),
                'pic_regulasi'      => $pic_regulasi,
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];
                        
            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
        
    }

    public function all_regulasi($idauthor="")
    {
        if( empty($idauthor) ){
            $author     = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();
            $andwhere   = [];
        }else{
            $author     = $idauthor;
            $andwhere   = ['author'=>$author];
        }

        /*if( empty($author) ){
            $andwhere = [];
        }else{
            // $andwhere = ['author'=>$author];
        }*/

        $query = $this->msqlbasic->show_db($this->tb_akre_regulasi,$andwhere);

        $datas = [];
        foreach( $query as $row ){
            $role       = $row['role'];
            $idkategori = $row['idkategori'];
            $author     = $row['author'];

            $sql_user   = "SELECT namauser FROM ".$this->tb_login_user." WHERE iduser ='".$author."' LIMIT 1 ";
            $query_user = $this->msqlbasic->custom_query( $sql_user )[0];
           

            /**
             * Akre unit
             */
            $sql_akre_unit = "SELECT nama_unit,id_pic,user_id_login FROM ".$this->tb_akre_unit." WHERE user_id_login ='".$author."' LIMIT 1 ";
            $query_akre_unit = $this->msqlbasic->custom_query( $sql_akre_unit )[0];

            switch ($role) {
                case role_parent():
                    $kategori = $this->msqlbasic->row_db($this->tb_parent_akreditasi,['id'=>$idkategori]);
                    $nama_kat = !empty($kategori['nama_kategori_title']) ? $kategori['nama_kategori_title'] : '';
                    break;
                case role_subparent():
                    $kategori = $this->msqlbasic->row_db($this->tb_sub_akreditasi,['id'=>$idkategori]);
                    $nama_kat = !empty($kategori['nama_kategori']) ? $kategori['nama_kategori'] : '';
                    break;
                case role_childparent():
                    $kategori = $this->msqlbasic->row_db($this->tb_child_akreditasi,['id'=>$idkategori]);
                    // $nama_kat = $kategori['nama_kategori_title'];
                    $nama_kat = !empty($kategori['nama_kategori']) ? $kategori['nama_kategori'] : '';
                    break;
            }

            $query_item = $this->msqlbasic->show_db($this->tb_akre_item_regulasi,['idregulasi'=>$row['id']]);

     
            $row['nama_author']             = $query_user['namauser'];
            $row['nama_unitpelayanan']      = $query_akre_unit['nama_unit'];
            $row['picarray']                = unserialize( $query_akre_unit['id_pic'] );
            $row['namakategori']            = $nama_kat;
            $row['item_regulasi']           = $query_item;

            $datas[] = $row; 
            
        }

        return $datas;
    }

    public function tambah_unit()
    {
        $post = $this->input->post();
        $id   = $post['id'];
        if( $id != 0 ){
            $return = ['title'=>'Update Unit','body'=>$this->form_unit($id)];
        }else{
            $return = ['title'=>'Tambah Unit','body'=>$this->form_unit()];
        }

        echo json_encode($return);
    }

    public function form_unit($id="")
    {
        //edit
        $get_pic = '';
        $get_userlogin = '';
        $get_nama = '';
        if( !empty($id) ){
            $get_unit   = $this->msqlbasic->row_db($this->tb_akre_unit,['idakre_unit'=>$id]);
            $get_pic    = unserialize( $get_unit['id_pic'] ); 
            $get_userlogin    =  $get_unit['user_id_login']; 
            $get_nama    = $get_unit['nama_unit']; 
        }


        ob_start();
        $formdocumen = '
                <div class="err-msg-unit"></div>
                <form class="form" id="frm-tambah-unit">';
       
                if( !empty($id) ){
                    $formdocumen .= '<input type="hidden" name="idhidden" value="'.$id.'">';
                }            
                    
        $formdocumen .= '<div class="form-group">
                        <label>Unit <span class="required">*</span></label>
                        <input required type="text" value="'.$get_nama.'" autocomplete="off" class="form-control" name="unit" placeholder="Masukan Unit ..."></td>
                    </div>
                    <div class="form-group">
                        <label>Akses PIC Dokumen (opsional)</label>
                        <select name="pic[]" class="form-control qlselect-multi" multiple="multiple">';
                            foreach($this->users_pic() as $pic){
                                $selected = '';
                                if( !empty($get_pic) ){
                                    $selected = ( in_array($pic['iduser'],$get_pic) ) ? 'selected' : '';
                                }
                                $get_kat    = $this->msqlbasic->row_db($this->tb_parent_akreditasi,['author'=>$pic['iduser']]);
                                $nama_kat   = $get_kat['nama_kategori_title'] ?? '' ;
                                
                                if( $pic['namauser'] == 'superuser' ) continue;

                                $formdocumen .= '<option value="'.$pic['iduser'].'" '.$selected.'>'.$nama_kat.' - '.$pic['namauser'].'</option>';
                            }
        $formdocumen .= '</select>
                    </div>
                    <div class="form-group">
                        <label>Akses User Login <span class="required">*</span></label>
                        <select name="userlogin" required class="form-control qlselect-multi">
                            <option value="">Pilih</option>';
                            foreach($this->show_all_userlogin() as $userlogin){
                                $selected = '';
                                if( !empty($get_userlogin) ){
                                    // $selected = ( in_array($userlogin['iduser'],$get_userlogin) ) ? 'selected' : '';
                                    $selected = ( $userlogin['iduser'] == $get_userlogin ) ? 'selected' : '';
                                }
                                $formdocumen .= '<option value="'.$userlogin['iduser'].'" '.$selected.'>'.$userlogin['namauser'].'</option>';
                            }
        $formdocumen .= '</select>
                    </div>
                   
                    <div class="form-group text-right"">
                        <button type="button" class="btn btn-warning batal-popup" data-dismiss="modal">Batal</button>
                        <button type="submit" id="simpan-files" class="btn btn-primary">Simpan</button>
                    </div>
                </form>';
        
        echo $formdocumen;

        return ob_get_clean();
    }

    public function users_pic($idunit="")
    {
        $all_user               = $this->msqlbasic->show_db($this->tb_login_user);
        $pengaturan_pic         = $this->msqlbasic->row_db($this->tb_akre_pengaturan_pic);

        $userpic = [];
        foreach($all_user as $row){
            $author_id  = explode(',', $pengaturan_pic['author'] );
            if( !in_array( $row['iduser'],$author_id ) ) continue;
 
            unset($row['idperson']);
            unset($row['password']);
            unset($row['password_reset']);
            unset($row['cookie']);
            unset($row['loginterakhir']);

            $userpic[]  = $row; 
        }

        return $userpic;
    }

    public function show_all_userlogin()
    {
        $all_user = $this->msqlbasic->show_db($this->tb_login_user);
        $userlogin = [];
        foreach($all_user as $row){
 
            unset($row['idperson']);
            unset($row['password']);
            unset($row['password_reset']);
            unset($row['cookie']);
            unset($row['loginterakhir']);

            $userlogin[]  = $row; 
        }

        return $userlogin;
    }

    public function show_all_standar_bylogin($iduserlogin="")
    {
        if( !empty( $iduserlogin ) ){
            $cek_akre_unit  = $this->msqlbasic->row_db($this->tb_akre_unit,['user_id_login'=>$iduserlogin]);
            if( !empty( $cek_akre_unit ) ){
                $id_pic         = unserialize( $cek_akre_unit['id_pic'] );
                $new_array_pic  = implode(',',$id_pic);
                $sql     = "SELECT id,nama_kategori_title FROM ".$this->tb_parent_akreditasi." ";
                $sql    .= " WHERE author IN "."(". $new_array_pic .")". " ";
                $query_parent  = $this->msqlbasic->custom_query($sql);
            }else{
                $query_parent = [];
            }
        }else{
            // $new_array_pic = implode(',',$this->get_regulasi_pic()) ;
            $query_parent = [] ;
        }

        return $this->list_group_standarwith_subchild($query_parent);

    }

    public function get_regulasi_pic()
    {
        $cek_pic_superuser_unit = $this->cek_pic_superuser_unit();

        $author = ( $this->get_session_username() == 'superuser' || $cek_pic_superuser_unit == true ) ? '' : $this->get_session_userid();
        
        if( empty( $author ) ){
            $andwhere_akre = [];
        }else{
            $andwhere_akre = ['user_id_login'=>$author];
        }

        $all_unit  = $this->msqlbasic->show_db($this->tb_akre_unit,$andwhere_akre);
        $merge_unit = [];
        foreach( $all_unit as $row ){
            if(!empty($row['id_pic'])){
                foreach( unserialize( $row['id_pic'] ) as $pic ){
                    $merge_unit[]= $pic;
                }
            }
        }

        $new_array_pic = array_unique($merge_unit);

        return $new_array_pic;
    }

    public function list_group_standarwith_subchild($query_parent)
    {
        $query = [];
        foreach( $query_parent as $i => $row ){
            $andwhere_sub   = ['id_kategori_title'=>$row['id']]; 
            $query_sub      =  $this->msqlbasic->show_db($this->tb_sub_akreditasi,$andwhere_sub);
            $row['sub']     = $query_sub;

            $query[] = $row;
        }

        $group_query = [];
        foreach( $query as $group_subcategory ){
                         
            $group_sub=[];
            foreach($group_subcategory['sub'] as $subparent){

                $andwhere_child   = ['id_kategori_subparent'=>$subparent['id']]; 
                $query_child      =  $this->msqlbasic->show_db($this->tb_child_akreditasi,$andwhere_child);

                $subparent['child'] = $query_child;

                $group_sub[] = $subparent;
            }

            $group_subcategory['sub'] = $group_sub;
            $group_query[] = $group_subcategory;

        }
        
        return $group_query;
    }

    public function simpan_unit_pelayanan()
    {
        $post  = $this->input->post();

        $this->form_validation->set_rules('unit', 'Unit', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('userlogin', 'user login', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'failed','txt'=>validation_errors());
            echo json_encode( $return );
        }else{
            $unit               = $this->security->sanitize_filename($post['unit']);
            $pic_array          = isset($post['pic']) ? serialize( $post['pic'] ) : '';
            $userlogin          = $this->security->sanitize_filename($post['userlogin']);

            /**
             * Edit
             */
            $idhidden           = isset($post['idhidden']) ? $post['idhidden'] : '';
            
            $get_akre_row = $this->msqlbasic->row_db($this->tb_akre_unit,['user_id_login'=>$userlogin]);
            
            if( !empty($get_akre_row) ){
                if( !empty($get_akre_row['id_pic']) ){
                    $unserialize_pic = unserialize( $get_akre_row['id_pic'] );
                    if( isset($post['pic']) ){
                        $cek_akre_user_unit = ( $unserialize_pic == $post['pic'] ) ? true : false;
                    }else{
                        $cek_akre_user_unit = false;
                    } 
                }else{
                    $cek_akre_user_unit = false;
                }
            }else{
                $cek_akre_unit = false;
            }

            if( $cek_akre_user_unit ){
                $return = ['msg'=>'danger','txt'=>'Gagal, Nama User Sudah Tefdaftar Silahkan Cari User Lain'];
                echo json_encode( $return );
                die();
            }

            if( empty( $idhidden ) ){
                $data = [
                    'datecreated'   => indonesia_date(),
                    'id_pic'        => $pic_array,
                    'user_id_login' => $userlogin,
                    'nama_unit'     => $unit
                ];


                
                $insert = $this->msqlbasic->insert_data($this->tb_akre_unit,$data);
    
                if( $insert ){
                    $return = ['msg'=>'success','txt'=>'Berhasil disimpan'];
                }else{
                    $return = ['msg'=>'danger','txt'=>'Gagal Menyimpan'];
                }
    
                echo json_encode($return);
            }else{
                $data = [
                    'id_pic'        => $pic_array,
                    'user_id_login' => $userlogin,
                    'nama_unit'     => $unit
                ];
                $getrow_akre = $this->msqlbasic->row_db($this->tb_akre_unit,['idakre_unit'=>$idhidden]);

                $update_regulasi = $this->msqlbasic->update_db($this->tb_akre_regulasi,['author'=>$getrow_akre['user_id_login']],['author'=>$userlogin]);
                $update = $this->msqlbasic->update_db($this->tb_akre_unit,['idakre_unit'=>$idhidden],$data);
    
                $return = ['msg'=>'success','txt'=>'Berhasil disimpan'];
                
                echo json_encode($return);
            }

        }
    }

    public function all_unit(){
        
        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();
        
        if( empty( $author ) ){
            $andwhere_akre = [];
        }else{
            $andwhere_akre = ['user_id_login'=>$author];
        }

        $all_user  = $this->msqlbasic->show_db($this->tb_login_user );
        $all_unit  = $this->msqlbasic->show_db($this->tb_akre_unit,$andwhere_akre);

        $data = [];
        foreach( $all_unit as $row ){
            $user_pic = [];
            $user_login = '';
            foreach( $all_user as $user ){
                
                unset($user['idperson']);
                unset($user['password']);
                unset($user['password_reset']);
                unset($user['cookie']);
                unset($user['loginterakhir']);

                if( !empty($row['id_pic']) ){

                    if( in_array( $user['iduser'], unserialize( $row['id_pic'] ) ) ){
                        $get_kat    = $this->msqlbasic->row_db($this->tb_parent_akreditasi,['author'=>$user['iduser']]);
                        $nama_kat   = $get_kat['nama_kategori_title'].' - '.$user['namauser'];
                        $user['namauser'] = $nama_kat;
                        $user_pic[] = $user;
                    }
                    
                }
                    if( $user['iduser'] == $row['user_id_login'] ){
                        $user_login = $user['namauser'];
                    }
            }

            $row['user_pic'] = $user_pic;
            $row['user_login'] = $user_login;

            $data[] = $row;
            
        }

        return $data;

    }

    public function delete_unit()
    {
        $post   = $this->input->post();
        $idunit = $this->security->sanitize_filename($post['idunit']);

        $delete = $this->msqlbasic->delete_db($this->tb_akre_unit,['idakre_unit'=>$idunit]);
        
        if( $delete ){
            $return = ['msg'=>'success','txt'=>'Data Berhasil Dihapus'];
        }else{
            $return = ['msg'=>'danger','txt'=>'Data Gagal Dihapus'];
        }

        echo json_encode( $return );
    }

    public function tambah_regulasi()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_DOKUMENAKREDITASI))
        {
            $cek_pic_superuser_unit = $this->cek_pic_superuser_unit(true);

            $author = ( $this->get_session_username() == 'superuser' || $cek_pic_superuser_unit == true ) ? '' : $this->get_session_userid();

            $pic_regulasi =  $this->show_all_userunit_regulasi();

            $data = [
                'content_view'      => 'akreditasi/v_tambahregulasi',
                'active_menu'       => 'akreditasi',
                'active_menu_level' => '',
                'title_page'        => 'Tambah Regulasi',
                'active_menu_level' => 'dokumentasiunit',
                'mode'              => '',
                'user_login'        => $author,
                'pic_regulasi'      => $pic_regulasi,
                'active_sub_menu'   => 'dokumentasiunit',
                'script_js'         => ['js_akreditasi'],
                'list_standar'      => $this->show_all_standar_bylogin($author),
                'plugins'           => [ PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE ],
            ];

            
            $this->load->view('v_index', $data);
        }else{
            aksesditolak();
        }
    }

    public function show_all_userunit_regulasi($found=false){

        $author = ( $this->get_session_username() == 'superuser' ) ? '' : $this->get_session_userid();
        
        $andwhere_akre = [];

        // if( empty( $author ) ){
        //     $andwhere_akre = [];
        // }else{
        //     if( $found ){
        //         $andwhere_akre = [];
        //     }else{
        //         $andwhere_akre = ['user_id_login'=>$author];
        //     }
        // }

        $all_unit  = $this->msqlbasic->show_db($this->tb_akre_unit,$andwhere_akre);

        $useridlogin = [0];
        foreach( $all_unit as $row ){
            $useridlogin[] = $row['user_id_login'];
        }
        
        $id_pic = $this->get_regulasi_pic();

        $sql    = "SELECT a.iduser,a.namauser,b.nama_unit FROM ".$this->tb_login_user." a ";
        $sql   .= " LEFT JOIN ".$this->tb_akre_unit." b ON b.user_id_login = a.iduser ";
        $sql   .= " WHERE a.iduser IN "."(". implode(',',$useridlogin) .")". " ";
        

        $query  = $this->msqlbasic->custom_query($sql);

        return $query;

    }

    public function get_add_rekomendasi()
    {
        $post       = $this->input->post();
    
        $idelement      = (int) $this->security->sanitize_filename($post['idelement']);
        $idkategori     = (int) $this->security->sanitize_filename($post['idkategori']);

        $query          = $this->msqlbasic->row_db($this->tb_akre_elemen_penilaian,['id'=>$idelement]);
        
        $rekomendasi    = $query['rekomendasi'];

        $data           = ['rekomendasi'=>$rekomendasi,'id_element'=>$idelement,'idkategori'=>$idkategori];
        
        echo json_encode( $data );
        
    }

    public function get_simpan_rekomendasi()
    {
        $post = $this->input->post();


        $tambahrekomendasi  = (string) $this->security->sanitize_filename($post['tambahrekomendasi']);
        $id_element         = (int) $this->security->sanitize_filename($post['id_element']);
        $idkategori         = (int) $this->security->sanitize_filename($post['idkategori']);

        $data = ['rekomendasi' => $tambahrekomendasi];

        #update
        $query = $this->msqlbasic->update_db(
            $this->tb_akre_elemen_penilaian,
            ['id'=>$id_element],
            $data
        );
                
        if( $query > 0 ){
            echo json_encode([
                'msg'=>'success',
                'txt'=>'Data Berhasil di Update',
                'idkategori'=>$idkategori,
            ]);
        }else{
            echo json_encode([
                'msg'=>'danger',
                'txt'=>'Data Tidak Di Update',
            ]);
        } //end if query insert is true
    
    }

    public function simpan_regulasi()
    {
        $post = $this->input->post();


        // $this->form_validation->set_rules('regulasi[]', 'Regulasi', 'required',
        //         array('required' => 'Harap Isi %s.')
        // );

        $this->form_validation->set_rules('pic-unit', 'PIC Unit', 'required',
                array('required' => 'Harap Isi %s.')
        );

        $this->form_validation->set_rules('parentkat', 'Standar Kategori', 'required',
                array('required' => 'Harap Pilih %s.')
        );

        $this->form_validation->set_rules('hiddenrole', 'Role', 'required',
                array('required' => 'Tidak Boleh Kosong %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            $return = array('msg'=>'danger','txt'=>validation_errors());
            echo json_encode( $return );
        }else{
            $parentkat  = $this->security->sanitize_filename( $post['parentkat'] );
            $hiddenrole = $this->security->sanitize_filename( $post['hiddenrole'] );
            $picunit    = $this->security->sanitize_filename( $post['pic-unit'] );
            // $regulasi   = $post['regulasi']; //array
            
            // $string_regulasi =[];
            // foreach( $regulasi as $row ){
            //     $string_regulasi[] = $this->security->sanitize_filename( $row );
            // }

            # cek duplicate data regulasi
            $cek_last_insert_regulasi = $this->msqlbasic->row_db($this->tb_akre_regulasi,['author'=>$picunit,'idkategori'=>$parentkat]);

            if( !empty( $cek_last_insert_regulasi ) ){
                // $return = array('msg'=>'danger','txt'=>'Maaf Data Regulasi Sudah Terinput');
                // echo json_encode( $return );
                // die();
                $lastid_insert  = $cek_last_insert_regulasi['id'];
                $insert         = $lastid_insert;
            }else{

                // parent,subparent,childparent
                // funsinya ada di ql_helper
                $data = [
                    'datecreated' => indonesia_date(),
                    'idkategori' => $parentkat,
                    'role' => $hiddenrole, 
                    'author' => $picunit
                ];
                
                $insert = $this->msqlbasic->insert_data($this->tb_akre_regulasi,$data);
            }


            if( $insert > 0 ){
                // $found = false;
                // foreach( $string_regulasi as $row_regulasi ){
                //     $data_item = [
                //         'idregulasi'=> $insert,
                //         // 'itemregulasi' => $row_regulasi,
                //     ];

                //     $insert_item_regulasi = $this->msqlbasic->insert_data($this->tb_akre_item_regulasi,$data_item);

                //     if( $insert_item_regulasi ){
                //         $found = true;
                //     }
                // }
                $data_item = ['idregulasi'=> $insert];
                $insert_item_regulasi = $this->msqlbasic->insert_data($this->tb_akre_item_regulasi,$data_item);

                if( $insert_item_regulasi > 0 ){
                    $return = ['msg'=>'success','txt'=>'Data Berhasil Di Simpan'];
                }else{
                    $return = ['msg'=>'danger','txt'=>'Data Gagal Di Simpan'];
                }
                echo json_encode( $return );

            }else{
                $return = ['msg'=>'danger','txt'=>'Data Gagal Di Simpan'];
                echo json_encode( $return );
            }


        }
         
    }

    public function simpan_dokumen_unit()
    {
        $this->form_validation->set_rules('iditemregulasi', 'ID Regulasi', 'required',
                array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['msg'=>'danger','txt'=>validation_errors()]);
        }else{
            
            $post               = $this->input->post();
            $iditemregulasi    = (int) $this->security->sanitize_filename($post['iditemregulasi']);

            

            $paths = FOLDERNASSIMRS.'/pdf_akreditasi_unit/';
            $names = [];
            $found = false;

            $count_uploaded_files = count( $_FILES['filedocument']['name'] );

            $files = $_FILES;

            for( $i = 0; $i < $count_uploaded_files; $i++ ){

                $_FILES['filedoc'] = [
                    'name'     => $files['filedocument']['name'][$i],
                    'type'     => $files['filedocument']['type'][$i],
                    'tmp_name' => $files['filedocument']['tmp_name'][$i],
                    'error'    => $files['filedocument']['error'][$i],
                    'size'     => $files['filedocument']['size'][$i]
                ];
        
                $tmpName        = $files['filedocument']['tmp_name'][$i];       
                $fileName       = date('D').rand(10,99).'-'.str_replace('"', '', str_replace("'", '', $files['filedocument']['name'][$i]));
                $newFileName    = '/pdf_akreditasi_unit/'.$fileName;
                
                if( $files['filedocument']['type'][$i] == 'application/pdf'){   
                
                    $names[]          = $fileName;    

                    $config = $this->ftp_upload();
                    
                    # upload ke nas storage
                    $upload = $this->ftp->connect($config);
                    $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                    $upload = $this->ftp->close();
                    
                    $found = true;

                } //end if cek dokumen is pdf
                
            } // .end for files

            if( $found ){

                    # Cek data Regulasi item file
                    $cek_item_regulasi = $this->msqlbasic->row_db($this->tb_akre_item_regulasi,['id'=>$iditemregulasi]);
                    
                    $name_file_array = $names;

                    if( !empty($cek_item_regulasi['filename']) ){
                        $file_exist = unserialize( $cek_item_regulasi['filename'] );
                        $name_file_array = array_merge($names,$file_exist);
                    }
                                    
                    $data_item_regulasi = [
                        'filename' => serialize( $name_file_array ),
                        'pathname' => $paths,
                    ];

                    $query = $this->msqlbasic->update_db($this->tb_akre_item_regulasi, ['id'=>$iditemregulasi] ,$data_item_regulasi);

                    if( $query > 0 ){
                        echo json_encode([
                            'msg'=>'success',
                            'txt'=>'Data Berhasil di Simpan',
                            'url'=>base_url().'cakreditasi/dokumenunit'
                        ]);
                    }else{
                        echo json_encode([
                            'msg'=>'danger',
                            'txt'=>'Data Gagal di Simpan'
                        ]);
                    } //end if query insert is true

            }else{
                echo json_encode(['msg'=>'danger','txt'=>'Gagal Upload File, Tipe File Harus PDF']);
            } //end if found file is pdf

        } // endif validation
    }

    public function delete_file_unit()
    {
        $post = $this->input->post();
        $keyfile = $post['keyfile'];
        $iditemregulasi = $post['iditemregulasi'];
        $get_item_regulasi = $this->msqlbasic->row_db( $this->tb_akre_item_regulasi,['id'=>$iditemregulasi] );
        
        $path = $get_item_regulasi['pathname'];
        $files = unserialize( $get_item_regulasi['filename'] );

        $urlnas = $path.$files[$keyfile];
                    
        //hapus file di NAS
        $config = $this->ftp_upload();
        $delete = $this->ftp->connect( $config );
        $delete = $this->ftp->delete_file( $urlnas );
        $delete = $this->ftp->close();

        unset( $files[$keyfile] );
        $reindex_file = array_values($files);

        $data_item_regulasi = [
            'filename' => empty( $reindex_file ) ? '' : serialize( $reindex_file ),
        ];

        $query = $this->msqlbasic->update_db($this->tb_akre_item_regulasi, ['id'=>$iditemregulasi] ,$data_item_regulasi);

        if( $query > 0 ){
            echo json_encode([
                'msg'=>'success',
                'txt'=>'Data Berhasil di Hapus',
                'url'=>base_url().'cakreditasi/dokumenunit'
            ]);
        }else{
            echo json_encode([
                'msg'=>'danger',
                'txt'=>'Data Gagal di Hapus'
            ]);
        } //end if query insert is true
    }

    public function delete_item_regulasi()
    {
        $post           = $this->input->post();
        $iditemregulasi = $post['iditemregulasi'];

        $get_item_regulasi = $this->msqlbasic->row_db( $this->tb_akre_item_regulasi,['id'=>$iditemregulasi] );

        $path = $get_item_regulasi['pathname'];
        $files = unserialize( $get_item_regulasi['filename'] );

        if( !empty( $files ) ){
            foreach( $files as $file ){
                $urlnas = $path.$file;

                // hapus file di NAS
                $config = $this->ftp_upload();
                $delete = $this->ftp->connect( $config );
                $delete = $this->ftp->delete_file( $urlnas );
                $delete = $this->ftp->close();

            }
        }

        $query_delete = $this->msqlbasic->delete_db($this->tb_akre_item_regulasi,['id'=>$iditemregulasi]);

        if( $query_delete > 0 ){
            echo json_encode([
                'msg'=>'success',
                'txt'=>'Data Berhasil di Hapus',
            ]);
        }else{
            echo json_encode([
                'msg'=>'danger',
                'txt'=>'Data Gagal di Hapus'
            ]);
        } //end if query insert is true
    }

    public function delete_regulasi()
    {
        $post       = $this->input->post();
        $idregulasi = $post['idregulasi'];
        
        $get_regulasi       = $this->msqlbasic->row_db( $this->tb_akre_regulasi,['id'=>$idregulasi] );
        $get_item_regulasi  = $this->msqlbasic->show_db( $this->tb_akre_item_regulasi,['idregulasi'=>$idregulasi] );

        foreach( $get_item_regulasi as $row_item ){

            $files    = unserialize( $row_item['filename'] );
            $path     = $row_item['pathname'];
            $iditem   = $row_item['id'];

            if( !empty( $files ) ){
                foreach( $files as $file ){
                    $urlnas = $path.$file;
                    
                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            $query_item_delete = $this->msqlbasic->delete_db($this->tb_akre_item_regulasi,['id'=>$iditem]);

        }

        $query_regulasi_delete = $this->msqlbasic->delete_db($this->tb_akre_regulasi,['id'=>$idregulasi]);
        
        if( $query_regulasi_delete > 0 ){
            echo json_encode([
                'msg'=>'success',
                'txt'=>'Data Berhasil di Hapus',
            ]);
        }else{
            echo json_encode([
                'msg'=>'danger',
                'txt'=>'Data Gagal di Hapus'
            ]);
        } 

    }

    public function search_unitakreditasi()
    {
        $post = $this->input->post();
        $idloginunit = $post['idloginunit'];

        $all_regulasi    = $this->all_regulasi($idloginunit);

        echo json_encode(['hsl'=> $this->ajaxtable_dokumen_unit($all_regulasi) ]) ;
        
    }

    public function ajaxtable_dokumen_unit($listdokumen)
    {
        ob_start();
        $pengaturan_pic  = $this->msqlbasic->row_db($this->tb_akre_pengaturan_pic);
        $array_pic = explode(",",$pengaturan_pic['author']);
        
        $cek_pic_superuser_unit = $this->cek_pic_superuser_unit();

        $cek_pic_superuser = ( $this->get_session_username() == 'superuser' || $cek_pic_superuser_unit == true ) ? true : false ;
        $author = $this->get_session_userid();

        include FCPATH. 'application/views/akreditasi/v_ajax-table-dokumenunit.php';
        return ob_get_clean();
    }

    public function change_pic_regulasi()
    {
        $post = $this->input->post();
        $iduserlogin = $post['iduserlogin'];
        $all_standar = $this->show_all_standar_bylogin($iduserlogin);

        $option = '<option value="">Pilih</option>';
        foreach( $all_standar as $parent_kat ){
            $option .= "<option value='".$parent_kat['id']."'>".$parent_kat['nama_kategori_title']."</option>";
            if( !empty($parent_kat['sub']) ){
                foreach( $parent_kat['sub'] as $sub_kat ){
                    $option .= '<option value="'.$sub_kat['id'].'" data-role="subparent">__'.$sub_kat['nama_kategori'].'</option>';
                    if( !empty($sub_kat['child']) ){
                        foreach( $sub_kat['child'] as $child_kat ){
                            $option .= '<option value="'.$child_kat['id'].'" data-role="childparent">____'.$child_kat['nama_kategori'].'</option>';
                        }
                    }
                }
            }
        }
        echo json_encode(['hsl'=>$option]);
    }

    /*public function popup_edit_regulasi()
    {
        $post           = $this->input->post();
        $iditemregulasi = $post['iditemregulasi'];

        $get_item_regulasi  = $this->msqlbasic->row_db($this->tb_akre_item_regulasi,['id'=>$iditemregulasi]);
        $nama_item_regulasi = $get_item_regulasi['itemregulasi'];

        $return             = ['hsl'=>$nama_item_regulasi];

        echo json_encode( $return );
    }*/

    /*public function simpan_editpopup_regulasi()
    {
        $post = $this->input->post();

        $this->form_validation->set_rules('editpopupregulasi', 'Regulasi', 'required',
                array('required' => 'Harap Isi  %s.')
        );

        $this->form_validation->set_rules('iditemregulasi', 'Item Regulasi', 'required',
                array('required' => 'Harap Isi  %s.')
        );

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['msg'=>'danger','txt'=>validation_errors()]);
        }else{
        
            $editpopupregulasi  = $this->security->sanitize_filename($post['editpopupregulasi']);
            $iditemregulasi     = $this->security->sanitize_filename($post['iditemregulasi']);

            $data_item_regulasi = [
                'itemregulasi' => $editpopupregulasi,
            ];

            $query = $this->msqlbasic->update_db($this->tb_akre_item_regulasi, ['id'=>$iditemregulasi] ,$data_item_regulasi);

            echo json_encode([
                'msg'=>'success',
                'txt'=>'Data Berhasil di Update',
            ]);

        }
        

    }*/

}
