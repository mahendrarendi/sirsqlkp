<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Ctagihan extends MY_controller 
{
    function __construct()
    {
	parent::__construct();
        // jika user belum login maka akses ditolak
        if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
//        $this->load->model('mmasterdata');
//        $this->load->model('mdatatable');
//        $this->load->model('mviewql');
        
    }
    /**
     * Halaman Faktur Lunas Usaha 
     */
    public function fakturlunasusaha()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASUSAHA) )
        {
            $data = [
                'content_view'      => 'tagihan/v_hutangusaha',
                'active_menu'       => 'tagihan',
                'active_sub_menu'   => 'fakturlunasusaha',
                'active_menu_level' => '',
                'modeview'          => 'fakturlunas',
                'title_page'        => 'Faktur Lunas Usaha',
                'plugins'           => [ PLUG_DATATABLE, PLUG_DATE , PLUG_DROPDOWN],
                'script_js'         => ['tagihan/js_hutangusaha']
            ];
            $this->load->view('v_index',$data);
           
        }
        else
        {
            aksesditolak();
        }
            
    }
    /**
     * Total Dibayar Hutang Usaha
     */
    public function totdibayarhutangusaha()
    {
        $data = $this->db->query("SELECT sum(dibayar) as totaldibayar  FROM rs_fakturnonfarmasi WHERE tanggalpelunasan BETWEEN '".$this->input->post('tanggal1')."' and '".$this->input->post('tanggal2')."' ")->row_array();
        echo json_encode($data);
    }
    
    /**
     * Total elum Dibayar Hutang Usaha
     */
    public function totbelumdibayarhutangusaha()
    {
        $data = $this->db->query("SELECT sum(kekurangan) as totaldibayar  FROM rs_fakturnonfarmasi WHERE tanggalfaktur BETWEEN '".$this->input->post('tanggal1')."' and '".$this->input->post('tanggal2')."' ")->row_array();
        echo json_encode($data);
    }


    /**
     * Halaman Hutang Usaha 
     */
    public function hutangusaha()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_HUTANGUSAHA) )
        {
            $data = [
                'content_view'      => 'tagihan/v_hutangusaha',
                'active_menu'       => 'tagihan',
                'active_sub_menu'   => 'hutangusaha',
                'modeview'          => 'hutangusaha',
                'active_menu_level' => '',
                'title_page'        => 'Hutang Usaha',
                'plugins'           => [ PLUG_DATATABLE, PLUG_DATE , PLUG_DROPDOWN],
                'script_js'         => ['tagihan/js_hutangusaha']
            ];
            $this->load->view('v_index',$data);
           
        }
        else
        {
            aksesditolak();
        }
            
    }
    /**
     * Menampilkan Hutang Usaha per distributor
     * mahmud, clear
     */
    public function dt_hutangusaha()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_HUTANGUSAHA) )
        {
            $this->load->model('mdatatable');
            $getData = $this->mdatatable->dt_hutangusaha_(true);
            $data=[]; $no=$_POST['start'];
            if($getData){
                foreach ($getData->result() as $obj) {
                    $distributor=$obj->distributor;
                    $tagihan = convertToRupiah($obj->tagihan);
                    $ppn = convertToRupiah($obj->ppn);
                    $potongan = convertToRupiah($obj->potongan);
                    $totaltagihan = convertToRupiah($obj->totaltagihan);
                    $dibayar = convertToRupiah($obj->dibayar);
                    $kekurangan = convertToRupiah($obj->kekurangan);
                    $row = [ 
                        ++$no,
                        $distributor,
                        $tagihan,
                        $ppn,
                        $potongan,
                        $totaltagihan,
                        $dibayar,
                        $kekurangan,
                        '<a id="detail" alt="'.$distributor.'" alt2="'.$tagihan.'" alt3="'.$ppn.'" alt4="'.$potongan.'" alt5="'.$totaltagihan.'" alt6="'.$dibayar.'" alt7="'.$kekurangan.'" alt8="'.$obj->idbarangdistributor.'" class="btn btn-info btn-xs" '.  ql_tooltip('Detail').'><i class="fa fa-eye"></i></a>'
                    ];
                    $data[] = $row;
                }
            } 
            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->mdatatable->total_dt_hutangusaha(),
                "recordsFiltered" => $this->mdatatable->filter_dt_hutangusaha(),
                "data" =>$data
            );
            //output dalam format JSON
            echo json_encode($output);
        }
        else
        {
            aksesditolak();
        }
    }
    /**
     * menampilkan data distributor ke form input dropdown select2
     */
    public function filldistributor()
    {
        echo json_encode($this->db->query("SELECT idbarangdistributor as id, distributor as text from rs_barang_distributor where distributor like '%".$this->input->post('searchTerm')."%' limit 20 ")->result());
    }
    
    /**
     * menampilkan data coa beban usaha ke form input dropdown select2
     */
    public function fillcoabebanusaha()
    {
        $cari = $this->input->post('searchTerm');
        
        $sql = $this->db->where("kt.tipe","Biaya");
        $sql = $this->db->group_start()->like("ka.kode","$cari");
        $sql = $this->db->or_like("ka.kode","$cari")->group_end();
        $sql = $this->db->limit(20);
        $sql = $this->db->order_by('ka.kode');
        $sql = $this->db->select("ka.kode as id, concat(ka.kode, ' ',ka.nama) as text");
        $sql = $this->db->join("keu_tipeakun kt","kt.idtipeakun=ka.idtipeakun");
        $sql = $this->db->get("keu_akun ka");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }

        /**
     * SIMPAN HUTANG USAHA
     * mahmud, clear
     */
    public  function savehutangusaha()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_HUTANGUSAHA) )
        {
            $post = $this->input->post();
            $data = [
                'nofaktur'=>$post['nofaktur'],
                'tanggalfaktur'=>$post['tanggalfaktur'],
                'tanggaljatuhtempo'=>$post['jatuhtempo'],
                'tanggalpelunasan'=>$post['tanggalpelunasan'],
                'tagihan'=>unconvertToRupiah($post['tagihan']),
                'potongan'=>unconvertToRupiah($post['potongan']),
                'ppn'=>unconvertToRupiah($post['ppn']),
                'dibayar'=>unconvertToRupiah($post['dibayar']),
                'coabebanusaha'=>$post['coabebanusaha'],
                'keterangan'=>$post['keterangan'],
                'idbarangdistributor'=>$post['iddistributor'],
                'waktumulailayanan' => $post['waktumulailayanan'] == null ? '' : $post['waktumulailayanan'],
                'waktuberakhirlayanan' => $post['waktuberakhirlayanan'] == null ? '' : $post['waktuberakhirlayanan']
            ];
            $this->mgenerikbap->setTable('rs_fakturnonfarmasi');
            $save = $this->mgenerikbap->update_or_insert_ignoreduplicate($data, $post['idfaktur']);
            pesan_success_danger($save, 'Simpan Berhasil.', 'Simpan Gagal.','js');
        }
        else
        {
            aksesditolak();
        }
    }
    
    /**
     * menampilkan Detail Hutang Usaha Per Distributor
     * mahmud, clear
     */    
    public function dt_detailhutangusaha()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_hutangusaha_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $menu  = '<div class="btn-toolbar">';
                $menu .= (($obj->statusbayar=='lunas') ? '' : '<a class="btn btn-warning btn-xs" id="editfaktur" alt="'.$obj->idfaktur.'" '.  ql_tooltip('Ubah').' ><i class="fa fa-edit"></i></a>');
                $menu .= (($obj->statusbayar=='lunas') ? ' <a class="btn btn-danger btn-xs" id="batallunas" alt="'.$obj->idfaktur.'" '.  ql_tooltip('Batal Lunas').' ><i class="fa fa-minus-circle"></i></a>' : '' );
                $menu .= (($obj->statusbayar!='lunas') ? ' <a class="btn btn-success btn-xs" id="bayarlunas" alt="'.$obj->idfaktur.'" alt3="'.$obj->nofaktur.'" alt2="'.$obj->totaltagihan.'"  '.  ql_tooltip('Lunas').' ><i class="fa fa-check"></i></a>' : '' );
                $menu .= '<a class="btn btn-danger btn-xs" id="hapusfaktur" alt="'.$obj->idfaktur.'"data-toggle="tooltip" data-original-title="Hapus"><i class="fa fa-trash"></i></a>';
                $menu .= '</div>';

                $waktulayanan = '';
                if($obj->waktumulailayanan == null || $obj->waktuberakhirlayanan == null || $obj->waktumulailayanan == "0000-00-00" || $obj->waktuberakhirlayanan == "0000-00-00" ){
                    $waktulayanan = '-';
                }
                else{
                    $waktulayanan =$obj->waktumulailayanan.' s/d '.$obj->waktuberakhirlayanan;
                }
                
                $row = [
                    ++$no,
                    $obj->nofaktur,
                    $waktulayanan,
                    $obj->tanggalfaktur,
                    $obj->tanggaljatuhtempo,
                    '<b style="background-color: #FFFF00;"><i>  '.$obj->tanggalpelunasan.'  </i></b>',
                    convertToRupiah($obj->tagihan),
                    convertToRupiah($obj->ppn),
                    convertToRupiah($obj->potongan),
                    convertToRupiah($obj->totaltagihan),
                    convertToRupiah($obj->dibayar),
                    convertToRupiah($obj->kekurangan),
                    $obj->statusbayar,
                    $menu
                    ];
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_hutangusaha(),
            "recordsFiltered" => $this->mdatatable->filter_dt_hutangusaha(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }

    /**
     * Hapus Faktur
     */
    //Tito, belum clear
    public function hapusFaktur()
    {   
        $idf = $this->input->post('idf');

        if(isset($idf))
        {
            $sql = "DELETE FROM `rs_fakturnonfarmasi` WHERE rs_fakturnonfarmasi.idfaktur ='".$idf."'";
            $arrMsg = $this->db->query($sql);
        }
        pesan_success_danger($arrMsg, 'Hapus Faktur Berhasil.!', 'Hapus Faktur Gagal.!', 'js');
    }
    
    /**
     * Bayar Hutang Usaha
     */
    //mahmud, clear
    public function bayarhutangusaha()
    {
        $post = $this->input->post();
        $iduser = $this->get_iduserlogin();
        if(isset($post['sb']))
        {
            $data = ['statusbayar'=> $post['sb'],'iduserpelunasan'=>$iduser];
            if($post['sb']=='lunas'){ $data['tanggalpelunasan'] = $post['tgl']; }
            $arrMsg = $this->db->update('rs_fakturnonfarmasi',$data,['idfaktur'=>$this->input->post('id')]);
        }
        else
        {
            $arrMsg = $this->db->update('rs_barang_faktur',['dibayar'=>$this->input->post('nom'),'statusbayar'=>'dibayar','iduserpelunasan'=>$iduser],['idbarangfaktur'=>$this->input->post('id')]);
        }
        pesan_success_danger($arrMsg, 'Pelunasan Berhasil.!', 'Pelunasan Gagal.!', 'js');
        
    }
    
    /**
     * Form Ready Hutang Usaha
     */
    public function formhutangusahaready()
    {
        echo json_encode($this->db->select('*, (select distributor from rs_barang_distributor x where x.idbarangdistributor = rf.idbarangdistributor limit 1) as distributor, (select concat(ka.kode," " , ka.nama) from keu_akun ka where ka.kode=rf.coabebanusaha) as bebanusaha ')->get_where('rs_fakturnonfarmasi rf',['idfaktur'=>$this->input->post('i')])->row_array());
    }
    
    
    /**
     * Halaman Faktur Lunas Farmasi 
     */
    public function fakturlunasfarmasi()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASFARMASI) )
        {
            $data = [
                'content_view'      => 'tagihan/v_hutangfarmasi',
                'active_menu'       => 'tagihan',
                'active_sub_menu'   => 'fakturlunasfarmasi',
                'active_menu_level' => '',
                'modeview'          => 'fakturlunas',
                'title_page'        => 'Faktur Lunas Farmasi',
                'plugins'           => [ PLUG_DATATABLE, PLUG_DATE , PLUG_DROPDOWN],
                'script_js'         => ['tagihan/js_hutangfarmasi']
            ];
            $this->load->view('v_index',$data);
           
        }
        else
        {
            aksesditolak();
        }
            
    }
    /**
     * Total Dibayar Hutang Farmasi
     */
    public function totdibayarhutangfarmasi()
    {
        $data = $this->db->query("SELECT sum(dibayar) as totaldibayar  FROM rs_barang_faktur WHERE tanggalpelunasan BETWEEN '".$this->input->post('tanggal1')."' and '".$this->input->post('tanggal2')."' ")->row_array();
        echo json_encode($data);
    }
    
    /**
     * Total  Hutang Farmasi Belum Dibayar
     */
    public function totbelumdibayarhutangfarmasi()
    {
        $data = $this->db->query("SELECT sum(kekurangan) as totaldibayar  FROM rs_barang_faktur WHERE tanggalfaktur BETWEEN '".$this->input->post('tanggal1')."' and '".$this->input->post('tanggal2')."' ")->row_array();
        echo json_encode($data);
    }

    /**
     * Halaman Hutang Farmasi 
     * mahmud, clear
     */
    public function hutangfarmasi()
    {
        if( $this->pageaccessrightbap->checkAccessRight(V_HUTANGFARMASI) )
        {
            $data = [
                'content_view'      => 'tagihan/v_hutangfarmasi',
                'active_menu'       => 'tagihan',
                'active_sub_menu'   => 'hutangfarmasi',
                'active_menu_level' => '',
                'title_page'        => 'Hutang Farmasi',
                'modeview'          => 'hutangfarmasi',
                'mode'              => '',
                'plugins'           => [ PLUG_DATATABLE, PLUG_DATE ],
                'jsmode'            => 'tampilbelanja',
                'script_js'         => ['tagihan/js_hutangfarmasi']
            ];
            $this->load->view('v_index',$data);
           
        }
        else
        {
            aksesditolak();
        }
    }
    /**
     * Menampilkan hutang farmasi dengan data table
     * mahmud, clear
     */
    public function dt_hutangfarmasi()
    {
        $this->load->model('mdatatable');
        $getData = $this->mdatatable->dt_hutangfarmasi_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) 
            {
                $distributor=$obj->distributor;
                $tagihan = convertToRupiah($obj->tagihan);
                $ppn = convertToRupiah($obj->ppn);
                $potongan = convertToRupiah($obj->potongan);
                $totaltagihan = convertToRupiah($obj->totaltagihan);
                $dibayar = convertToRupiah($obj->dibayar);
                $kekurangan = convertToRupiah($obj->kekurangan);
                $row = [
                    ++$no,$distributor,$tagihan,$ppn,$potongan,$totaltagihan,$dibayar,$kekurangan,
                    '<a id="detail" alt="'.$distributor.'" alt2="'.$tagihan.'" alt3="'.$ppn.'" alt4="'.$potongan.'" alt5="'.$totaltagihan.'" alt6="'.$dibayar.'" alt7="'.$kekurangan.'" alt8="'.$obj->idbarangdistributor.'" class="btn btn-info btn-xs" '.  ql_tooltip('Detail').'><i class="fa fa-eye"></i></a>'
                ];
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_hutangfarmasi(),
            "recordsFiltered" => $this->mdatatable->filter_dt_hutangfarmasi(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    /**
     * menampilkan total hutang farmasi di detail hutang per distributor
     * mahmud, clear
     */
    public function totalhutangfarmasi()
    {
        $post = $this->input->post();
        $sql = "SELECT sum(tagihan) as tagihan, sum(totaltagihan) as totaltagihan, sum(potongan) as potongan, sum(kekurangan) as kekurangan, sum(dibayar) as dibayar, sum(ppn) as ppn FROM rs_barang_faktur where idbarangdistributor='".$post['idbd']."' and tanggalfaktur between '".$post['tanggal1']."' and '".$post['tanggal2']."'";
        $data = $this->db->query($sql)->row_array();
        echo json_encode($data);
    }
}    
/* End of file ${TM_FILENAME:${1/(.+)/Ctagihan.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:applicatagalion).+)/Cmasterdata/:application/controllers/${1/(.+)/Ctagihan.php/}} */









