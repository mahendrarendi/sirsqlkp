<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends MY_controller//CI_Controller 
{

    function getUnitByUser()
    {
        echo json_encode($this->mkombin->getUnitByUsername());
    }
    
    function __construct()
    {
        parent::__construct();
        $this->session->set_userdata('isServerBPJS', 'false');
        $this->session->set_userdata('isServerBPJS_SEP', 'false');
        $this->session->set_userdata('isServerBPJS_Rujukan', 'false');
        $this->load->model('mkombin');
        $this->load->library('user_agent');
        $this->load->helper(array('Cookie', 'String'));
    }
    private function cek_isexists($idwebserviceurl,$parameter)
    {
        $this->db->query("select count(1) as isexists from webservice_status where idwebserviceurl='".$idwebserviceurl."' and parameter='". $parameter ."'")->result_array();
    }
    
    
    public  function ceknobpjs($no)
    {
        print_r($this->bpjsbap->cekrequestRujukanSingle($no));
    }
    
    public function webservice_vclaim()
    {
        $this->load->library('bpjsbap');
        $this->load->model('mviewql');
        //aktif
        //cek apakah tanggal sekarang sudah masuk infonya
        $parameter = ql_tanggalsekarang();
        $cek = $this->cek_isexists('1',$parameter);
        if ($cek[0]['isexists'] == 0)
        {
            //jika belum eksekusi
            $this->db->query("insert into webservice_status (idwebserviceurl, parameter) values (1, '". $parameter ."') ON DUPLICATE KEY UPDATE idwebservicestatus = idwebservicestatus");
        }
        $cek = $this->cek_isexists('7',$parameter);
        if ($cek[0]['isexists'] == 0)
        {
            //jika belum eksekusi
            $this->insertupdatePasienCovid19($this->mviewql->viewpasiencovid19($parameter));
            
            $this->db->query("insert into webservice_status (idwebserviceurl, parameter) values (7, '". $parameter ."') ON DUPLICATE KEY UPDATE idwebservicestatus = idwebservicestatus");
        }
        $parameter  = ql_tanggalkemarin('', 'd-m-Y');
        $parameter2 = ql_tanggalkemarin('', 'Y-m-d');
        $cek = $this->cek_isexists('4',$parameter);
        if ($cek[0]['isexists'] == 0)
        {
            //jika belum eksekusirequestKelas
            $this->bpjsbap->insertupdatedeletePelayananSiranap(JENISCONSID_DEVELOPMENT, 'irj', $parameter, json_encode($this->mviewql->viewkunjungansiranap($parameter2, 'irj')), true);
            $this->db->query("insert into webservice_status (idwebserviceurl, parameter) values (4, '". $parameter ."') ON DUPLICATE KEY UPDATE idwebservicestatus = idwebservicestatus");
        }

        
    }
    private function insertupdatePasienCovid19($data)
    {
        if(empty(!$data))
        {
            foreach ($data as $arrData)
            {
                $arrPasien = ['noc'=>$arrData['nik'], 'nomr'=>$arrData['norm'],'initial'=>$arrData['namalengkap'],'nama_lengkap'=>$arrData['namalengkap'],'tglmasuk'=>$arrData['tanggalperiksa'],'gender'=>$arrData['gender'],'birthdate'=>$arrData['tanggallahir'],'kewarganegaraan'=>'1','sumber_penularan'=>$arrData['idsebabpenularan'],'kecamatan'=>$arrData['idkecamatan'],'tglkeluar'=>$arrData['tanggalperiksa'],'status_keluar'=>$arrData['statuskeluar'],'tgl_lapor'=>date('Y-m-d H:i:s'),'status_rawat'=>$arrData['idstatusrawatwabah'],'status_isolasi'=>$arrData['idpenangananwabah'],'email'=>'-','notelp'=>$arrData['notelpon'],'sebab_kematian'=>'','jenis_pasien'=>'1'];
                $save = (($arrData['serviceintegrasi']==1)? $this->bpjsbap->requestPostCovid19('/Pasien',json_encode($arrPasien)) : $this->bpjsbap->requestPutCovid19('/Pasien',json_encode($arrPasien)) );
                if($save)
                {
                    $data = $this->db->query("select rh.icd, if(icdlevel='primer','1','2') from rs_hasilpemeriksaan rh, rs_icd ri where idpemeriksaan='".$arrData['idpemeriksaan']."' and ri.icd=rh.icd and ri.idjenisicd=2 order by icdlevel");
                    if($data->num_rows()>0)
                    {
//                        $icd_awal = $data->row_array()['icd'];
                        $no=0;
                        foreach ($data->result_array() as $arricd)
                        {
                            ++$no;
                            if($no>1){
                                $dataIcd = ['nomr'=>$arrData['norm'],'icd'=>$arricd['icd'],'level'=>'2'];
                            }else{
                                $dataIcd = ['nomr'=>$arrData['norm'],'icd'=>$arricd['icd'],'level'=>'1'];
                            }
                            $save = (($arrData['serviceintegrasi']==1)? $this->bpjsbap->requestPostCovid19('/Pasien/diagnosis',json_encode($dataIcd)) : $this->bpjsbap->requestPutCovid19('/Pasien/diagnosis',json_encode($dataIcd)) );
                        }
                    }
                }
                $save = $this->db->update('rs_pemeriksaan_wabah',['serviceintegrasi'=>'3'],['idpemeriksaan'=>$arrData['idpemeriksaan']]);
            }
            return $save;
        }
    }
    public function index()
    {
        return aksesditolak();
    }
    
    //cek login dari aplikasi khusus login simrs --> start_simrs
    //mahmud, clear
    public function rsuQL12345698678657546543543HJVJHGCHF()
    {
        $username = $_GET['123456'];
        $password = $_GET['7891011'];
        $data = $this->db->join('login_hakaksesuser lh','lh.iduser = lu.iduser')->get_where('login_user lu',['lu.namauser'=>$username]);
            
        //jika user benar
        if($data->num_rows() > 0)
        {
            $data = $data->row_array();
            if (password_verify($password,$data['password'])) //jika passwor benar 
            {
                $data['namauser'] = $_GET['123456'];  // variabel username
                $data['password'] = $_GET['7891011']; // variavel password
                $data['idunitterpilih'] = $_GET['234567'];    
                $data['unitterpilih']   = $_GET['345678'];        
                $data['levelgudang']    = $_GET['456789'];
                $data['idloketpemanggilan']  = $_GET['56789101']; 
                $data['loketpemanggilan']    = $_GET['6789101112'];
                $this->create_session($data);
            }
            else //jika password salah
            {
                $arrMsg = ['status'=>'danger','message'=>'Password Salah.' ] ;
                return aksesditolak();
            }
        }
        else //jika username salah
        {
            $arrMsg = ['status'=>'danger','message'=>'Username Salah.' ] ;
            return aksesditolak();
        }
    }
    
    //create session login user
    //mahmud, clear
    public function create_session($data)
    {
        $stasiun = $this->db->select('stasiun')->get_where('login_hakaksesuser',['iduser'=>$data['iduser']])->row_array();
        $this->session->set_flashdata('message', ['status'=>'info','message'=>'Welcome, '.ucwords($data['namauser'])]);
        $this->session->set_userdata('username', $data['namauser']);
        $this->session->set_userdata('idunitterpilih', $data['idunitterpilih']);
        $this->session->set_userdata('idloketpemanggilan',$data['idloketpemanggilan']);
        $this->session->set_userdata('loketpemanggilan',$data['loketpemanggilan']);
        $this->session->set_userdata('unitterpilih', $data['unitterpilih']);
        $this->session->set_userdata('levelgudang', $data['levelgudang']);
        $this->session->set_userdata('sitiql_session','aksesloginberhasil');
		$this->session->set_userdata('id_person_login',$data['idperson']);
		
        $this->encryptbap->generatekey_once("SESSUSER");
        $this->session->set_userdata('sess_stasiun',$stasiun['stasiun']);
        $this->session->set_userdata('iduser', $this->encryptbap->encrypt_urlsafe(json_encode($data['iduser'])));
        $this->session->set_userdata('pper', $this->encryptbap->encrypt_urlsafe(json_encode($data['idperson'])));
        $this->pageaccessrightbap->registerAccessRight("idperan", "login_hakakseshalaman", "idhalaman", "peranperan", "login_hakaksesuser", "iduser", $data['iduser']);
        $this->session->set_userdata('peranperan', $data['peranperan']);
        $this->log_userlogin($data['iduser'],'Login');
        return $this->redirect_afterlog();  
        
    }
    private function createsession_notifikasi()
    {
        if($this->pageaccessrightbap->checkAccessRight(V_BARANGNOTIFIKASI))
        {
            $tanggaljual = ql_tanggaltujuan('-48 day');
            $jumlah = $this->db->query("select count(1) as jumlah from rs_barang where tanggaljual <= '".$tanggaljual."'")->row_array()['jumlah'];
            $isinotifikasi = ((empty($jumlah))? []  : 
                [
                    ['icon'=>' fa-random text-red','message'=>convertToRupiah($jumlah).' Barang belum terjual selama 29 hari.','url'=>base_url().'creport/notif_barangbelumterjual']
                ] 
            );
            $jumlahnotif = ((empty(count($isinotifikasi)))?'':count($isinotifikasi));
            $data = [
                'tanggaljual'=>$tanggaljual,
                'jumlah'     =>$jumlahnotif,
                'header'     =>'Anda '.((empty($jumlahnotif))?'tidak ':'').'memiliki '.$jumlahnotif.' notifikasi',
                'isi'        =>$isinotifikasi];
        $this->session->set_userdata('notif', $data);
        }
    }
    private function redirect_afterlog()
    {
        if ($this->agent->is_referral())
        {
            return redirect($this->agent->referrer());
        }
        else
        {
            return redirect('dashboard');
        }
    }
    
    public function log_out()
    {
        delete_cookie('sitiql');
        $this->session->sess_destroy();
        return aksesditolak();
    }
    
    //---------- basit, clear
    public function ubahpassword()
    {
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $this->mgenerikbap->setTable('login_user');
        $data = $this->mgenerikbap->select('*', ['iduser'=>$iduser])->row_array();
        if (password_verify($this->input->post('katasandilama'),$data['password']))
        {
            
            $this->db->update('login_user',['password_reset'=>'','password' => password_hash($this->input->post('katasandibaru'), PASSWORD_DEFAULT)],['iduser'=>$iduser]);// simpan key di database
            echo json_encode(true);
        }
    }
    
    //-------- mahmud, clear
    /*Insert Log User Login/Logout*/
    private function log_userlogin($iduser,$status)
    {
        $log = 'STATUS:'.$status.' , IP:'.$this->input->ip_address().' Client-Info:'. exec('getmac');
        $expired = date('Y-m-d', strtotime("+4 weeks", strtotime(date('Y-m-d'))));
        $save = $this->db->insert('log_userlogin',['iduser'=>$iduser,'log'=>$log,'expired'=>$expired]);
        return $save;
    }
    
    public function insert_unit()
    {
        $unit = $this->db->select('idunit, namaunit')->get_where('rs_unit',['levelgudang'=>'unit'])->result_array();
        $uploadedJSON = json_encode(['dataunit'=>$unit]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertunit',$uploadedJSON);
        var_dump($data);
    }
    
    public function insert_dokter()
    {
        $dokter = $this->db->query("SELECT pp.idpegawai, concat(ifnull(pp.titeldepan,''), ' ', pper.namalengkap,' ', ifnull(pp.titelbelakang,'')) as namalengkap FROM person_pegawai pp
        join person_grup_profesi pgp on pgp.idprofesi = pp.idprofesi
        join person_person pper on pper.idperson = pp.idperson
        where pp.idprofesi = 1")->result_array();
        $uploadedJSON = json_encode(['datadokter'=>$dokter]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertdokter',$uploadedJSON);
        var_dump($data);
    }
    
    public function insert_jadwal()
    {
        $dtjadwal = $this->db->query("SELECT rj.idunit,ru.namaunit, date(rj.tanggal) as tanggal, time(rj.tanggal) as jammulai, rj.jamakhir, rj.idpegawaidokter, namadokter(rj.idpegawaidokter) as namadokter, al.namaloket, date_format(rj.tanggal,'%w') as hari
        FROM rs_jadwal rj
        join antrian_loket al on al.idloket = rj.idloket
        join rs_unit ru on ru.idunit = rj.idunit
        join person_pegawai ppai on ppai.idpegawai = rj.idpegawaidokter
        WHERE ppai.idprofesi = 1 and al.namaloket != 'ugd' and al.namaloket != 'poli umum'  and time(rj.tanggal) != '00:00:00' and rj.jamakhir != '00:00:00' and date(rj.tanggal) >= date(now()) 
        ORDER by rj.tanggal asc limit 10")->result_array();
        $uploadedJSON = json_encode(['datajadwal'=>$dtjadwal]);
        //and rj.idunit = 8
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertjadwal',$uploadedJSON);
        var_dump($data);
    }
    
    public function cekakses()
    {
        $uploadedJSON = json_encode(['a'=>'B']);
        $data = $this->bpjsbap->crudWebappql('/apm/insertjadwal',$uploadedJSON);
        var_dump($data);
    }
    
    
    public function singkronisasijadwal_uploadperunit()
    {
        $idunit = $this->input->post('idunit');
        $this->load->model('mviewql');
        $dtjadwal = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('upload_jadwal',$idunit);
        $uploadedJSON = json_encode(['datajadwal'=>$dtjadwal]);
        $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertjadwal',$uploadedJSON);
        
        //update singkronisasi jadwal jika berhasil
        if($data->metadata->code == '200')
        {
            foreach ($dtjadwal as $arr)
            {
                $this->db->update('rs_jadwal',['singkronisasi'=>1],['idjadwal'=>$arr['idjadwal'] ]);
            }
        }
        
        pesan_success_danger_ws($data->metadata->code,$data->metadata->message,'js');
    }

    public function bridging_jkn_kirim_antrian()
    {
        $this->bridging_antrol_kirim_antrian_otomatis();
    }

    public function bridging_jkn_kirim_taskid()
    {
        $this->bridging_antrol_kirim_taskid_otomatis();
    }
    
    
    
    
}
/* End of file ${TM_FILENAME:${1/(.+)/Clogin.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Clogin/:application/controllers/${1/(.+)/Clogin.php/}} */






