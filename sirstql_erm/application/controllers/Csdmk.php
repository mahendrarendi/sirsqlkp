<?php

use MY_controller;
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * DATA PLUGIN  & pemanggilan
 * chart/grafik = PLUG_CHART
 * datatable    = PLUG_DATATABLE
 * checkbox     = PLUG_CHECKBOX
 * dropdown     = PLUG_DROPDOWN
 * date/tanggal = PLUG_DATE
 * daterange    = PLUG_DATERANGE
 * textarea_word= PLUG_TEXTAREA
 * time/jam     = PLUG_TIME
 *_-----------------------------------------------------------------------------------
 * cara memanggil plugin lebih dari satu : $data['plugins'] = [PLUG_CHART , PLUG_DATATABLE , PLUG_DROPDOWN ]; 
 * cara memanggil 1 plugin               : $data['plugins'] = [PLUG_CHART]; 
 * jika tidak menggunakan plugin         : $data['plugins'] = [];
 */
//define dan nilai di tabel login_halaman harus sama
class Csdmk extends MY_controller 
{
    function __construct()
    {
	parent::__construct();
        // jika user belum login maka akses ditolak
        if($this->session->userdata('sitiql_session')!='aksesloginberhasil'){pesan_belumlogin();}
        $this->load->model('mmasterdata');
        $this->load->model('mdatatable');
        $this->load->model('msqlbasic');
    }
    
    public function dashboard()
    {
        $data =  [
                'active_menu'      => 'sdmk',
                'active_sub_menu'  => 'dashboard',
                'active_menu_level'=> '', 
                'content_view'     => 'sdmk/v_dashboard',
                'script_js'        => ['sdmk/js_dashboard'],
                'mode'             => 'view',
                'title_page'       => 'Data SDMK',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
            ];
        $this->load->view('v_index',$data);
    }
    
    // Data Pegawai
    // Mahmud
    // on Dev
    public function datapegawai()
    {
        $data =  [
                'active_menu'      => 'sdmk',
                'active_sub_menu'  => 'datapegawai',
                'active_menu_level'=> '', 
                'content_view'     => 'sdmk/v_datapegawai',
                'script_js'        => ['sdmk/datapegawai'],
                'mode'             => 'view',
                'title_page'       => 'Data Pegawai',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
            ];
        $this->load->view('v_index',$data);
    }
    
    public function dt_datapegawai()
    {
        $getData = $this->msdmk->dt_datapegawai_(true);
        $data=[]; 
        $no=0;
        if($getData)
        {
            foreach ($getData->result() as $obj)
            {
                $row = [ 
                    ++$no,
                    $obj->nama,
                    $obj->keterangan,
                    $obj->dibuat
                ];
                $row[]  = '<a id="edit" alt="'.$obj->iddepartemen.'" class="btn btn-xs btn-warning" '. ql_tooltip('ubah').'><i class="fa fa-edit"></i></a> <a id="delete" alt="'.$obj->iddepartemen.'" class="btn btn-xs btn-danger" '. ql_tooltip('hapus').'><i class="fa fa-trash"></i></a>';
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->msdmk->total_dt_datapegawai(),
            "recordsFiltered" => $this->msdmk->filter_dt_datapegawai(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function add_datapegawai()
    {
        $data =  [
                'active_menu'      => 'sdmk',
                'active_sub_menu'  => 'datapegawai',
                'active_menu_level'=> '', 
                'content_view'     => 'sdmk/v_datapegawai',
                'script_js'        => ['sdmk/js_datapegawai'],
                'mode'             => 'add',
                'data_edit'        => '',
                'title_page'       => 'Data Pegawai',
                'datakesehatan'    => $this->db->get('sdmk_kategori_datakesehatan')->result_array(),
                'plugins'          => [PLUG_DROPDOWN, PLUG_DATE]
            ];
        $this->load->view('v_index',$data);
    }
    
    
    // Simpan Departemen
    public  function save_departemen()
    {
        $post = $this->input->post();
        $data = [
            'iddepartemen'=>$post['iddepartemen'],
            'nama'=>$post['nama'],
            'keterangan'=>$post['keterangan'],
            'dibuat'=>date('Y-m-d')
        ];
        $save = (empty($post['iddepartemen'])) ? $this->db->insert('keu_departemen',$data) : $this->db->update('keu_departemen',$data,['iddepartemen'=>$post['iddepartemen']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
    }
    // Hapus Departemen
    public function hapus_itemdep()
    {
        $arrMsg = $this->db->delete('keu_departemen',['iddepartemen'=>$this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    // On Edit Departemen
    public function onreadyformdepartemen()
    {
        $barang =$this->db->get_where('keu_departemen',['iddepartemen'=>$this->input->post('i')])->row_array();
        echo json_encode(['barang'=>$barang]);
    }
    // End Master Departemen
    
    // Master Akun
    // Eka Annas
    public function akun()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_AKUN)) //lihat define di atas
        {
            $periode = $this->db->get('keu_periode')->row_array();
            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'jurnalumum',
                'active_menu_level'=> 'akunkeu', 
                'content_view'     => 'keuangan/v_akun',
                'script_js'        => ['keuangan/jurnalumum/akun'],
                'title_page'       => 'Daftar Akun - <i>Saldo Periode '.ql_namabulan($periode['bulan']).' '.$periode['tahun'].'</i>',
                'bulan'            => $periode['bulan'],
                'tahun'            => $periode['tahun'],
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    public function fillAkunTipe()
    {
        $detailtipeakun =$this->db->get_where('keu_tipeakun',['idtipeakun'=>$this->input->post('i')])->row_array();
        $tipe = $this->db->select('tipe as id, tipe as txt')->get('keu_tipe')->result_array();
        echo json_encode(['tipe'=>$tipe,'detailtipeakun'=>$detailtipeakun]);
    }
    public function fillTipe()
    {
        $data['detailakun'] = $this->db->get_where('keu_akun',['idakun'=>$this->input->post('i')])->row_array();
        $data['tipe']       = $this->db->select('idtipeakun as id, concat("[", kode," ] ",nama) as txt')->get('keu_tipeakun')->result_array();
        if(empty(!$this->input->post('i')))
        {
            $data['coabank']   = $this->db->get_where('keu_akun_kasbank',['kode'=>$data['detailakun']['kode']])->row_array();
            $data['coakasir']  = $this->db->get_where('keu_akun_kaskasir',['kode'=>$data['detailakun']['kode']])->row_array();
        }
        echo json_encode($data);
    }
    public function fillgetakun()
    {
        $cari = $this->input->post('searchTerm');
        $sql = $this->db->like("kode","$cari")
                ->or_like("nama","$cari")
                ->limit(10)
                ->order_by('kode','asc')
                ->select("kode as id, concat(kode,' ',nama) as text")
                ->get("keu_akun");
        $fetchData = $sql->result_array();
        echo json_encode($fetchData);
    }
    public function getAkun()
    {
        $tipe = $this->input->post("i");
        $max_kode = $this->db->query("SELECT MAX(kode) as kode_akhir FROM keu_akun WHERE idtipeakun='".$tipe."' ")->result_array();
        echo json_encode(['max_kode'=>$max_kode]);
    }
    public function getAkunTipe()
    {
        $detail =$this->db->get_where('keu_tipeakun',['idtipeakun'=>$this->input->post('i')])->row_array();

        echo json_encode(['akun'=>$detail]);
    }
    public function getMaxKode()
    {
        $tipe = $this->input->post("i");
        $max_kode = $this->db->query("SELECT MAX(kode) as kode_akhir FROM keu_akun WHERE tipe='".$tipe."' ")->result_array();
        echo json_encode(['max_kode'=>$max_kode]);
    }
    public function getMaxKodeNull()
    {
        $tipe = $this->input->post("i");
        $max_kode = $this->db->query("SELECT kode FROM keu_tipeakun WHERE idtipeakun='".$tipe."' ")->result_array();
        echo json_encode(['max_kode'=>$max_kode]);
    }
    public function save_tipeakun()
    {
        $post = $this->input->post();
        $kode = $post["kode"];
        $nama = $post["nama"];
        $tipe = $post["tipe"];
        $data = [
            "kode"=>$kode,
            "nama"=>$nama,
            "tipe"=>$tipe,
        ];
        $save = (empty($post['idtipeakun'])) ? $this->db->insert('keu_tipeakun',$data) : $this->db->update('keu_tipeakun',$data,['idtipeakun'=>$post['idtipeakun']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
    }
    public function save_akun()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_AKUN)) //lihat define di atas
        {
            $post = $this->input->post();
            $tipe = $post["tipeakun"];
            $kode = $post["kodeakun"];
            $nama = $post["namaakun"];

            //save coa bank, agar tampil di kasir
            if(isset($post['coabank']) && $post['coabank'] == 'on')
            {
                $this->db->replace('keu_akun_kasbank',['kode'=>$kode]);
            }

            //save coa kasir, untuk konfigurasi coa kasir
            if( isset($post['coakasir']) && $post['coakasir'] == 'on')
            {
                $this->db->replace('keu_akun_kaskasir',['kode'=>$kode,'iduserkasir'=>$post['idkasir'],'carabayar'=>$post['carabayar']]);
            }

            $data = [
                "kode"=>$kode,
                "nama"=>$nama,
                "idtipeakun"=>$tipe,
            ];
            $save = (empty($post['idakun'])) ? $this->db->insert('keu_akun',$data) : $this->db->update('keu_akun',$data,['idakun'=>$post['idakun']]);
            pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dataakunsaldo()
    {
        $dt = $this->mkeuangan->view_dataakunsaldo();
        echo json_encode($dt);
    }

    public function dataakun()
    {
        $periode = $this->db->get('keu_periode')->row_array();
        $sql = "SELECT c.idakun, b.tipe as tipe_akun, a.kode as kode_tipe, a.idtipeakun, a.nama as nama_tipe, c.kode as kode_akun, c.nama as nama_akun, ifnull(d.debetawal,0) as debetawal, ifnull(d.kreditawal,0) as kreditawal
        FROM keu_tipeakun a 
        LEFT JOIN keu_tipe b ON b.tipe =a.tipe 
        LEFT JOIN keu_akun c ON c.idtipeakun=a.idtipeakun 
        LEFT JOIN keu_tutup_buku d on d.idakun = c.idakun and d.tahun = '".$periode['tahun']."' and d.bulan = '".$periode['bulan']."'
        ORDER BY a.kode";
        echo json_encode($this->db->query($sql)->result());
    }

    public function saldoawal()
    {
        $periode = $this->db->get('keu_periode')->row_array();
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'jurnalumum',
            'active_menu_level'=> 'saldoawal', 
            'content_view'     => 'keuangan/v_saldoawal',
            'script_js'        => ['keuangan/jurnalumum/saldoawal'],
            'title_page'       => 'Saldo Awal Akun - Periode '.ql_namabulan($periode['bulan']).' '.$periode['tahun'],
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }

    public function onreadysaldo()
    {
        $periode = $this->db->get('keu_periode')->row_array();
        $detail  = $this->db->query("SELECT ka.idakun, ka.kode, ka.nama, ifnull(ktb.debetawal,0) as debetawal, ifnull(ktb.kreditawal,0) as kreditawal, ka.idtipeakun, ka.isdebit, ka.idunit, ka.idbangsal FROM keu_akun ka left join keu_tutup_buku ktb on ktb.idakun = ka.idakun and ktb.tahun = '".$periode['tahun']."' and ktb.bulan = '".$periode['bulan']."' where ka.idakun = '".$this->input->post('i')."' ")->row_array();
        echo json_encode(['detail'=>$detail]);
    }
    public function save_saldoawal()
    {
        $post = $this->input->post();
        $data = [
            'debetawal'=>str_replace(".","",$post["debetawal"]),
            'kreditawal'=>str_replace(".","",$post["kreditawal"]),
        ];
        $save = $this->db->update('keu_akun',$data,['idakun'=>$post['idakun']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
    }

    // End Master Akun


    // Master Jurnal
    // Eka Annas
    // Clear
    public function jurnal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_JURNAL)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'jurnalumum',
                'active_menu_level'=> 'jurnalkeu', 
                'content_view'     => 'keuangan/v_jurnal',
                'script_js'        => ['keuangan/jurnalumum/jurnal'],
                'title_page'       => 'Jurnal Umum',
                'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE, PLUG_TEXTAREA]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
     public function dt_jurnal()
    {
        $this->load->model('mkeuangan');
        $getData = $this->mkeuangan->dt_jurnal_(true);
        $data=[]; 
        $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = [
                    $obj->tanggal,
                    limit_text_length($obj->keterangan,100),
                    $obj->referensi,
                    $obj->no_referensi,
                    $obj->nama
                ];
                if($obj->status=="0"){
                    $row[] ='<span class="label label-warning">Pending </span>';
                    $row[] = '
                        <a id="approve" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-success" '. ql_tooltip('approve').'><i class="fa fa-check"></i></a>
                        <a id="detail" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-warning" '. ql_tooltip('detail').'><i class="fa fa-eye"></i></a>
                        <a id="delete" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-danger" '. ql_tooltip('hapus').'><i class="fa fa-trash"></i></a>
                        <a id="edit" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-primary" '. ql_tooltip('edit').'><i class="fa fa-edit"></i></a> ';
                }else{
                    $row[] ='<span class="label label-success">Approved </span>';
                    $row[] = '<a id="detail" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-warning" '. ql_tooltip('detail').'><i class="fa fa-eye"></i></a> <a id="delete" alt="'.$obj->kdjurnal.'" class="btn btn-xs btn-danger" '. ql_tooltip('hapus').'><i class="fa fa-trash"></i></a>';
                };
                
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mkeuangan->total_dt_jurnal(),
            "recordsFiltered" => $this->mkeuangan->filter_dt_jurnal(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    // Data Jurnal on JSON
    public function datajurnal()
    {
        $sql = "SELECT k.kdjurnal, k.idjurnal, k.tanggal, k.keterangan, k.referensi, k.no_referensi, d.nama, k.status,a.kode, a.nama as namaakun, c.deskripsi, c.debet, c.kredit, c.idjurnaldet FROM keu_jurnal k LEFT JOIN keu_departemen d ON d.iddepartemen=k.iddepartemen
            LEFT JOIN keu_jurnaldetail c ON c.kdjurnal=k.kdjurnal LEFT JOIN keu_akun a ON a.idakun=c.idakun";
        echo json_encode($this->db->query($sql)->result());
    }
    // Tampil Detail Jurnal
    public function onreadyformjurnal()
    {
        $detail =$this->db->get_where('keu_jurnal',['idjurnal'=>$this->input->post('i')])->row_array();
        $departemen = $this->db->select('iddepartemen as id, nama as txt')->get('keu_departemen')->result_array();
        $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get('keu_akun')->result_array();
        echo json_encode(['detail'=>$detail,'departemen'=>$departemen,'akun'=>$akun]);
    }
    // Tampil data combobox Akun
    public function fillAkun()
    {
        $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get('keu_akun')->result_array();
        echo json_encode(['akun'=>$akun]);
    }
    // Simpan Jurnal
    public function save_jurnal()
    {   
        $callidjurnal = generaterandom(10);
        $post = $this->input->post();
        $akun = $post["akun"];
        if(isset($post["penyesuaian"]))
        {
            $penyesuaian =  '1';
        }else{
            $penyesuaian = '0';
        }
        $data = [
            "tanggal"=>$post["tanggal"],
            "keterangan"=>$post["keterangan"],
            "no_referensi"=>$post["no_referensi"],
            "penyesuaian"=>$penyesuaian,
            "status"=>"0",
            "note"=>$post["note"],
            "iddepartemen"=>$post["iddepartemen"],
            "kdjurnal"=>$callidjurnal,
            "totalkredit"=>str_replace(".","",$post["totalkredit"]),
            "totaldebet"=>str_replace(".","",$post["totaldebet"]),
        ];
        $this->db->insert('keu_jurnal',$data);

        $datalog = [
            "kdjurnal"=>$callidjurnal,
            "tanggal"=>$post["tanggal"],
            "status"=>"0",
            "iduserkasir"=>$this->session->userdata('iduser')
        ];
        $this->db->insert('keu_jurnallog',$datalog);
        foreach ($akun as $akunkey => $valueakun) {
            $akunid = $post["akun"];
            $deskripsi = $post["deskripsi"];
            $debet = str_replace(".","",$post["debet"]);
            $kredit = str_replace(".","",$post["kredit"]);
            
            $data_akun = $this->mkeuangan->data_akun("WHERE a.idakun = '$akunid[$akunkey]' ");
            if(count($data_akun)>0)
            {
                foreach ($data_akun as $da) {
                    $tipe = $da->tipe;
                }

            }else{
                $tipe="0";
            }
            $datainsert = [
                "idakun"=>$akunid[$akunkey],
                "deskripsi"=>$deskripsi[$akunkey],
                "debet"=>$debet[$akunkey],
                "kredit"=>$kredit[$akunkey],
                "kdjurnal"=>$callidjurnal,
                "tipe"=>$tipe
            ];
            $save = $this->db->insert('keu_jurnaldetail',$datainsert);
        }
        pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
    }
    function saveedit_jurnal()
    {
        $post = $this->input->post();
        $kdjurnal = $post["kdjurnal"];
        if(!empty($post["akun"]))
        {
            $akun = $post["akun"];
        }
        if(isset($post["penyesuaian"]))
        {
            $penyesuaian =  '1';
        }else{
            $penyesuaian = '0';
        }
        $data = [
            "tanggal"=>$post["tanggal"],
            "keterangan"=>$post["keterangan"],
            "no_referensi"=>$post["no_referensi"],
            "penyesuaian"=>$penyesuaian,
            "status"=>"0",
            "note"=>$post["note"],
            "iddepartemen"=>$post["iddepartemen"],
            "totalkredit"=>str_replace(".","",$post["totalkredit"]),
            "totaldebet"=>str_replace(".","",$post["totaldebet"]),
        ];
        $datawhere = ["kdjurnal"=>$kdjurnal];
        $savee = $this->db->update('keu_jurnal',$data,$datawhere);
        

        if(!empty($post["akun"])){
            $this->db->delete('keu_jurnaldetail',$datawhere);
            foreach ($akun as $akunkey => $valueakun) {
                $akunid = $post["akun"];
                $tahun = date('Y');
                $deskripsi = $post["deskripsi"];
                $debet = str_replace(".","",$post["debet"]);
                $kredit = str_replace(".","",$post["kredit"]);
                $data_akun = $this->mkeuangan->data_akun("WHERE a.idakun = '$akunid[$akunkey]' ");
                if(count($data_akun)>0)
                {
                    foreach ($data_akun as $da) {
                        $tipe = $da->tipe;
                    }
    
                }else{
                    $tipe="0";
                }
                $datainsert = [
                    "idakun"=>$akunid[$akunkey],
                    "tahun"=>$tahun,
                    "deskripsi"=>$deskripsi[$akunkey],
                    "debet"=>$debet[$akunkey],
                    "kredit"=>$kredit[$akunkey],
                    "kdjurnal"=>$kdjurnal,
                    "tipe"=>$tipe
                ];
                $save = $this->db->insert('keu_jurnaldetail',$datainsert);
            }
        }
        pesan_success_danger($savee, 'Berhasil.', 'Gagal.','js');
        
    }
    // Hapus Item Transaksi Jurnal
    public function hapus_itemjurnal()
    {
        $arrMsg = $this->db->delete('keu_jurnal',['kdjurnal'=>$this->input->post('a')]);
        $arrMsgs = $this->db->delete('keu_jurnaldetail',['kdjurnal'=>$this->input->post('a')]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    // Hapus Jurnal
    public function hapus_detailitemjurnal()
    {
        $post = $this->input->post();
        $arrMsg = $this->db->delete('keu_jurnaldetail',['idjurnaldet'=>$this->input->post('a')]);
        $data = [
            "totalkredit"=>$post["c"],
            "totaldebet"=>$post["d"]
        ];
        $this->db->update('keu_jurnal',$data,['kdjurnal'=>$post['b']]);
        pesan_success_danger($arrMsg, 'Hapus Berhasil.', 'Hapus Gagal.', 'js');
    }
    // Approval Jurnal
    public function approve_itemjurnal()
    {

        $post = $this->input->post();
        $datalog = [
            "kdjurnal"=>$this->input->post('a'),
            "tanggal"=>date('Y-m-d'),
            "status"=>"1",
            "iduserkasir"=>$this->session->userdata('iduser')
        ];
        $datawhere = [
            "status"=>"1"
        ];
        $save = $this->db->insert('keu_jurnallog',$datalog);
        $this->db->update('keu_jurnal',$datawhere,['kdjurnal'=>$post['a']]);
        pesan_success_danger($save, 'Berhasil.', 'Gagal.','js');
    }
    public function onreloadhistoryakun()
    {
        $idakun = $this->input->post("idakun");
        $akun = $this->db->select('nama')->get_where('keu_akun',['idakun'=>$idakun])->row_array();
        echo json_encode(['akun'=>$akun]);
    }
    // Tampil Detail Item Transaksi Jurnal
    public function onreadyformjurnaldetail()
    {
        $jurnal =$this->db->query("SELECT keu_departemen.iddepartemen, keu_jurnal.idjurnal, keu_jurnal.penyesuaian, keu_jurnal.note, keu_jurnal.tanggal, keu_jurnal.keterangan, keu_jurnal.referensi, keu_jurnal.no_referensi, keu_jurnal.penyesuaian, keu_jurnal.status, keu_jurnal.note, keu_jurnal.totalkredit, keu_jurnal.totaldebet, 
            keu_jurnal.kdjurnal, keu_departemen.nama FROM keu_jurnal LEFT JOIN keu_departemen ON keu_departemen.iddepartemen=keu_jurnal.iddepartemen WHERE keu_jurnal.kdjurnal='".$this->input->post('i')."'")->row_array();
        $departemen = $this->db->select('iddepartemen as id, nama as txt')->get('keu_departemen')->result_array();
        $jurnaldetail =$this->db->query("SELECT a.idjurnaldet, a.idakun, a.deskripsi, a.debet, a.kredit, a.kdjurnal, b.nama, b.kode FROM keu_jurnaldetail a LEFT JOIN keu_akun b ON b.idakun=a.idakun WHERE a.kdjurnal='".$this->input->post('i')."'")->result_array();      
        $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get('keu_akun')->result_array();
        echo json_encode(['jurnal'=>$jurnal,'departemen'=>$departemen,'jurnaldetail'=>$jurnaldetail,'akun'=>$akun]);
    }
    // Tampil Edit Item Transaksi Jurnal
    public function onreadyformjurnaldetailitem()
    {
        $jurnal =$this->db->query("SELECT a.idjurnaldet, a.idakun, a.deskripsi, a.debet, a.kredit, a.kdjurnal, b.nama, b.kode FROM keu_jurnaldetail a LEFT JOIN keu_akun b ON b.idakun=a.idakun WHERE a.kdjurnal='".$this->input->post('i')."'")->result_array();
        echo json_encode(['jurnal'=>$jurnal]);
    }
    // End Master Jurnal

    public function historyakun()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'laporan',
            'active_menu_level'=> 'historyakunkeu', 
            'content_view'     => 'keuangan/v_historyakun',
            'script_js'        => ['keuangan/laporan/historyakun'],
            'title_page'       => 'History Akun',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }
    public function dt_historyakun()
    {
        $dt = $this->mkeuangan->view_historyakun();
        echo json_encode($dt);        
    }
    public function laporankas()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'laporan',
            'active_menu_level'=> 'laporankaskeu', 
            'content_view'     => 'keuangan/v_laporankas',
            'script_js'        => ['keuangan/laporan/laporankas'],
            'title_page'       => 'Laporan Kas',
            'plugins'          => [PLUG_DATATABLE, PLUG_DROPDOWN, PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }
    public function onreadyformlapkas()
    {
        $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get_where('keu_akun',['idtipeakun'=>'28'])->result_array();
        echo json_encode(['akun'=>$akun]);
    }
    public function dt_pendapatan_detail()
    {
        $this->load->model("mkeuangan");

        $data=[]; 
        $no=0;
        $saldoakhir="0";
        $getData = $this->mkeuangan->dt_pendaptan_det_(true);
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = array();
                $saldoakhir+=$obj->nominal;
                $row[] = ++$no;
                $row[] = $obj->tanggal;
                $row[] = $obj->norm;
                $row[] = $obj->namalengkap;
                $row[] = convertToRupiah($obj->nominal);
                $row[] = convertToRupiah($saldoakhir);
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mkeuangan->total_dt_pendapatan_det(),
            "recordsFiltered" => $this->mkeuangan->filter_dt_pendapatan_det(),
            "data" =>$data
        );
        echo json_encode($output);
    }
    public function dt_pendapatan_detail_trans()
    {
        $data=[]; 
        $no=0;
        $saldoakhir="0";
        $getData = $this->mkeuangan->dt_pendaptan_det_trans_(true);
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = array();
                $saldoakhir+=$obj->nominal;
                $row[] = ++$no;
                $row[] = $obj->tanggal;
                $row[] = $obj->norm;
                $row[] = $obj->namalengkap;
                $row[] = convertToRupiah($obj->nominal);
                $row[] = convertToRupiah($saldoakhir);
                $data[] = $row;
            }
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mkeuangan->total_dt_pendapatan_det_trans(),
            "recordsFiltered" => $this->mkeuangan->filter_dt_pendapatan_det_trans(),
            "data" =>$data
        );
        echo json_encode($output);
    }
    public function laporanlabarugi()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'laporan',
            'active_menu_level'=> 'laporanlabarugi', 
            'content_view'     => 'keuangan/v_laporanlabarugi',
            'script_js'        => ['keuangan/laporan/laporanlabarugi'],
            'title_page'       => 'Laporan Laba Rugi',
            'plugins'          => [PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }
    public function data_laporanlabarugi()
    {
        $data = $this->mkeuangan->dt_laporanlabarugi();
        echo json_encode($data);
    }
    
    public function laporanneraca()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'laporan',
            'active_menu_level'=> 'laporanneraca', 
            'content_view'     => 'keuangan/v_laporanneraca',
            'script_js'        => ['keuangan/laporan/laporanneraca'],
            'title_page'       => 'Laporan Neraca',
            'bulan'            => ql_namabulan(),
            'plugins'          => [PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function data_laporanneraca()
    {
        $data = $this->mkeuangan->dt_laporanneraca();
        echo json_encode($data);
    }
    
    public function rincian_akun()
    {
        $idakun         = $this->uri->segment(3);
        $data['awal']   = $this->uri->segment(4);
        $data['selesai']= $this->uri->segment(5);
        $data['akun']   = $this->db->get_where('keu_akun',['idakun'=>$idakun])->row_array();  
        $data['riwayat']= $this->mkeuangan->get_rincianakun($idakun);
        $data['active_menu'] = 'keuangan';
        $data['active_sub_menu']   = '';
        $data['active_menu_level'] ='';
        $data['content_view']      = 'keuangan/v_rincianakun';
        $data['script_js']         = ['keuangan/laporan/rincianakun'];
        $data['title_page']        = 'Riwayat Akun';
        $data['plugins']           = [PLUG_DATE,PLUG_TEXTAREA,PLUG_DROPDOWN];
        $this->load->view('v_index',$data);
    }
    
    public function onready_ubahrincianakun()
    {
        $kdjurnal = $this->input->post('kd');
        $dt = $this->db->query("SELECT kj.*, kjd.tahun,kjd.bulan, kjd.idjurnaldet, kjd.idakun, kjd.deskripsi, kjd.debet, kjd.kredit, kd.nama, ka.kode as coa, ka.nama as namacoa FROM keu_jurnal kj 
JOIN keu_jurnaldetail kjd on kjd.kdjurnal = kj.kdjurnal
JOIN keu_akun ka on ka.idakun = kjd.idakun
JOIN keu_departemen kd on kd.iddepartemen = kj.iddepartemen
WHERE kj.kdjurnal = '".$kdjurnal."'")->result_array();
        echo json_encode($dt);
    }
    
    public function create_update_jurnal()
    {
        //update jurnal
        $dtjurnal['keterangan'] = $this->input->post('keterangan');
        $dtjurnal['totalkredit']= unconvertToRupiah($this->input->post('totalkredit'));
        $dtjurnal['totaldebet'] = unconvertToRupiah($this->input->post('totaldebet'));
        $dtjurnal['note'] = $this->input->post('note');
        $wherejurnal['idjurnal'] = $this->input->post('idjurnal');        
        $save = $this->db->update('keu_jurnal',$dtjurnal,$wherejurnal);
        
        //update jurnal detail
        $jumlah = $this->input->post('jumlahjurnaldetail');
        for($x =0; $x<count($jumlah);$x++)
        {
            $coa = $this->input->post('coa');
            $debet = $this->input->post('debet');
            $kredit = $this->input->post('kredit');
            
            $table = 'keu_jurnaldetail';
            //update detail jurnal
            $dtcoa = $this->db->query("select ka.idakun, kt.tipe from keu_akun ka join keu_tipeakun kt on kt.idtipeakun = ka.idtipeakun where ka.kode = '".$coa[$x]."'")->row_array();
            
            $detail['idakun'] = $dtcoa['idakun'];
            $detail['tipe']   = $dtcoa['tipe'];
            $detail['debet']  = unconvertToRupiah($debet[$x]);
            $detail['kredit'] = unconvertToRupiah($kredit[$x]);
            $detail['tahun']    = $this->input->post('tahun');
            $detail['bulan']    = $this->input->post('bulan');
            $detail['kdjurnal'] = $this->input->post('kdjurnal');
            
            $idjurnaldet = $this->input->post('idjurnaldet');
            
            if(empty($idjurnaldet[$x])){
                $save = $this->db->insert($table,$detail);
            }else{
                $where['idjurnaldet'] = $idjurnaldet[$x];
                $save = $this->db->update($table,$detail,$where);
            }
        }
        
        pesan_success_danger($save, 'Ubah Jurnal Berhasil.', 'Ubah Jurnal Berhasil.', 'js');
        
    }
    
    
    /**
     * Mahmud, clear
     * Rekap Transaksi
     */
    public function rekaptransaksi()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'pemasukan',
            'active_menu_level'=> 'rekaptransaksikeu', 
            'content_view'     => 'keuangan/v_rekaptransaksi',
            'script_js'        => ['keuangan/rekaptransaksi'],
            'title_page'       => 'Rekap Transaksi',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE,PLUG_DROPDOWN, PLUG_TEXTAREA]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function dt_rekaptransaksi()
    {

        $getData = $this->mkeuangan->dt_rekaptransaksi_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $nominal = $obj->tagihan - $obj->dibayar;
                $row = [ 
                    $obj->waktutagih, 
                    $obj->norm,
                    $obj->namalengkap, 
                    $obj->kelas, 
                    $obj->carabayar, 
                    $obj->jenispembayaran,
                    $obj->statustagihan.'<br><label class="label label-default">Dibayar:'.convertToRupiah($obj->dibayar).' / Kekurangan:'.convertToRupiah($nominal).' / Tot.Tagihan:'.convertToRupiah($obj->tagihan).'</label><br><label class="label label-default"> '.ql_statusperiksa($obj->idstatuskeluar).' - '.$obj->statusperiksa.'</label>'
                ];
                if($obj->statusrekap=='0')
                {
                    $row[] = '<a id="rekap" alt="'.$obj->idpendaftaran.'" jns="'.$obj->jenisperiksa.'" bayar="'.$obj->jenispembayaran.'" class="btn btn-xs btn-warning" '. ql_tooltip('Rekap').'><i class="fa fa-check"></i> Rekap</a>';
                }else{
                    $row[] = '<span class="label label-success">Sudah Rekap </div>';
                }

                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mkeuangan->total_dt_rekaptransaksi(),
            "recordsFiltered" => $this->mkeuangan->filter_dt_rekaptransaksi(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function onreadyrekaptransaksi()
    {
        $sql = "SELECT a.iduser as id, a.namauser as txt FROM login_user a LEFT JOIN login_hakaksesuser b ON b.iduser=a.iduser WHERE b.peranperan in (9,14)";
        $listkasir = $this->db->query($sql)->result();
        echo json_encode(['listkasir'=>$listkasir]);
    }
    public function onreadypendapatdet()
    {

        $kasir = $this->input->post("i");
        // $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get_where('keu_akun',['idtipeakun'=>'28'])->result_array();
       
        $sql = "SELECT a.iduser as id, a.namauser as txt FROM login_user a LEFT JOIN login_hakaksesuser b ON b.iduser=a.iduser WHERE b.peranperan='9' AND a.iduser='$kasir'";
        $listkasir = $this->db->query($sql)->result();
        echo json_encode(['listkasir'=>$listkasir]);
    }
    public function onreadypendapatdet_trans()
    {

        $kasir = $this->input->post("i");
        $bank = $this->input->post("x");
        // $akun = $this->db->select('idakun as id, concat(kode," | ",nama) as txt')->get_where('keu_akun',['idtipeakun'=>'28'])->result_array();
       
        $sql = "SELECT a.iduser as id, a.namauser as txt FROM login_user a LEFT JOIN login_hakaksesuser b ON b.iduser=a.iduser WHERE b.peranperan='9' AND a.iduser='$kasir'";
        $sqlbank = "SELECT idbank as id, namabank as txt FROM bank_bank WHERE idbank='$bank'";
        $listkasir = $this->db->query($sql)->result();
        $listbank = $this->db->query($sqlbank)->result();
        echo json_encode(['listkasir'=>$listkasir,'listbank'=>$listbank]);
    }
    
    public function create_rekaptransaksi_pengeluaran()
    {
        $callidjurnal   = time().generaterandom(3);
        $tanggal        = $this->input->post('tanggal');
        $departemen     = $this->input->post('iddepartemen');
        $noreferensi    = $this->input->post('no_referensi');        
        $nominal        = $this->input->post('nominal');        
        $keterangan     = $this->input->post('keterangan');
        $totaldebet     = $this->input->post('totaldebet');
        $totalkredit    = $this->input->post('totalkredit');
        $kode_ref = 'idpengeluaran_';
        //insert jurnal
        $arrMsg = $this->insert_keu_jurnal($tanggal,$keterangan,$noreferensi,'Pengeluaran',$departemen,$callidjurnal,$totaldebet,$totalkredit,$kode_ref);
            
        //insert jurnal detail
        $jumlahjurnaldetail   = $this->input->post('jumlahjurnaldetail');
        $coa    = $this->input->post('coa');
        $debet  = $this->input->post('debet');
        $kredit = $this->input->post('kredit');
        $desc   = $this->input->post('deskripsi');
        $tahun  = date('Y', strtotime($tanggal) ); 
        $bulan  = date('n', strtotime($tanggal) ); 
        for($jurnaldetail=0; $jurnaldetail< count($jumlahjurnaldetail); $jurnaldetail++)
        {
           //insert jurnal detail
           $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,$debet[$jurnaldetail],$kredit[$jurnaldetail],$desc[$jurnaldetail],$coa[$jurnaldetail],'',$bulan);
        }
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $arrMsg = $this->insert_keu_jurnallog($tanggal,$iduser,$callidjurnal);
        pesan_success_danger($arrMsg, 'Rekap Transaksi Berhasil.', 'Rekap Transaksi Gagal.', 'js');
    }
    
    /**
     * Mahmud, clear
     * Buat Jurnal Rekap Transaksi
     */
    public function create_rekaptransaksi_ralan()
    {
        $callidjurnal   = time().generaterandom(3);
        $tanggal        = $this->input->post('tanggal');
        $departemen     = $this->input->post('iddepartemen');
        $noreferensi    = $this->input->post('no_referensi');
        $idkasir        = $this->input->post('kasir');
        $jenispembayaran= $this->input->post('jenispembayaran');
        $nominal        = $this->input->post('nominal');
        $bank           = $this->input->post('bank');
        $keterangan     = $this->input->post('keterangan');
        $totaldebet     = $this->input->post('totaldebet');
        $totalkredit    = $this->input->post('totalkredit');
        $idpendaftaran  = $this->input->post('idpendaftaran');
        
        $jumlahjurnal   = $this->input->post('jumlahjurnal');
        
        $debet  = $this->input->post('debet');
        $kredit = $this->input->post('kredit');
        $arrMsg = 0;
        for($jurnal=0; $jurnal < count($jumlahjurnal); $jurnal++)
        {
            $tanggaljurnal = date('Y-m-d', strtotime($tanggal[$jurnal]));
            
            //insert transaksi masuk
            $arrMsg = $this->insert_keu_transaksimasuk($idpendaftaran,$noreferensi[$jurnal],$nominal[$jurnal],$tanggaljurnal,$jenispembayaran[$jurnal],$bank[$jurnal],$idkasir[$jurnal]);
            
            //insert jurnal
            $kode_ref = 'idtagihan_';
            $arrMsg = $this->insert_keu_jurnal($tanggaljurnal,$keterangan[$jurnal],$noreferensi[$jurnal],'Penjualan',$departemen[$jurnal],$callidjurnal,$totaldebet[$jurnal],$totalkredit[$jurnal],$kode_ref);
            
            //insert jurnal detail
            $jumlahjurnaldetail = $this->input->post('jumlahjurnaldetail'.$jurnal);
            $coa    = $this->input->post('coa'.$jurnal);
            $debet  = $this->input->post('debet'.$jurnal);
            $kredit = $this->input->post('kredit'.$jurnal);
            $desc   = $this->input->post('deskripsi'.$jurnal);
            $tahun  = date('Y', strtotime($tanggal[$jurnal]) ); 
            $bulan  = date('n', strtotime($tanggal[$jurnal]) ); 
            for($jurnaldetail=0; $jurnaldetail< count($jumlahjurnaldetail); $jurnaldetail++)
            {
               //insert jurnal detail
               $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,$debet[$jurnaldetail],$kredit[$jurnaldetail],$desc[$jurnaldetail],$coa[$jurnaldetail],$jenispembayaran[$jurnal],$bulan);
            }
            
            //insert status rekap
            $arrMsg = $this->insert_keu_statusrekap($idpendaftaran,$jenispembayaran[$jurnal]);
            
            //insert jurnal log
            $arrMsg = $this->insert_keu_jurnallog($tanggaljurnal,$idkasir[$jurnal],$callidjurnal);
            
            //insert jurnal 2 dan 3 untuk carabayar tunai/transfer
            if($jenispembayaran[$jurnal] == 'tunai' or $jenispembayaran[$jurnal] == 'transfer')
            {
                //jurnal ke 2
                $callidjurnal = time().generaterandom(3);
                $arrMsg = $this->insert_keu_jurnal($tanggaljurnal,$keterangan[$jurnal],$noreferensi[$jurnal],'Penjualan',$departemen[$jurnal],$callidjurnal,$totaldebet[$jurnal],$totalkredit[$jurnal],$kode_ref);
                $coakas = '';
                
                
                if($jenispembayaran[$jurnal] == 'tunai' or $jenispembayaran[$jurnal] == 'transfer')
                {
                    $coakas = $this->db->query("SELECT kode FROM keu_akun_kaskasir where iduserkasir = '".$idkasir[$jurnal]."' and carabayar = 'umum'")->row_array()['kode'];
                }                
                $desc   = 'Penerimaan; Pembayaran Pasien; '.$tanggal[$jurnal];
                //insert jurnal debet
                $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,$totaldebet[$jurnal],0,$desc,$coakas,$jenispembayaran[$jurnal],$bulan);
                
                $coapiutang = $this->mviewql->getKonfigurasi('coakaspiutang');
                $desc   = 'Pemindahan - ke akun ['.$coakas.']; Pembayaran Pasien; '.$tanggal[$jurnal];
                //insert jurnal kredit
                $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,0,$totalkredit[$jurnal],$desc,$coapiutang,$jenispembayaran[$jurnal],$bulan);
                
                //insert jurnal log
                $arrMsg = $this->insert_keu_jurnallog($tanggaljurnal,$idkasir[$jurnal],$callidjurnal);
                                
                //jurnal ke 3 - memasukan saldo ke akun kas atau akun kas bank                
                $callidjurnal   = time().generaterandom(3);
                $arrMsg = $this->insert_keu_jurnal($tanggaljurnal,$keterangan[$jurnal],$noreferensi[$jurnal],'Penjualan',$departemen[$jurnal],$callidjurnal,$totaldebet[$jurnal],$totalkredit[$jurnal],$kode_ref);
                $coakas = '';
                
                if($jenispembayaran[$jurnal] == 'tunai')
                {
                    $coakas = $this->mviewql->getKonfigurasi('coakas');
                }
                else
                {
                    $coakas = $bank[$jurnal];
                }
                $coapiutang = $this->db->query("SELECT kode FROM keu_akun_kaskasir where iduserkasir = '".$idkasir[$jurnal]."' and carabayar = 'umum'")->row_array()['kode'];
                $desc   = 'Penerimaan; Pembayaran Pasien; '.$tanggal[$jurnal];
                //insert jurnal debet
                $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,$totaldebet[$jurnal],0,$desc,$coakas,$jenispembayaran[$jurnal],$bulan);
                
                $desc   = 'Pemindahan - ke akun ['.$coakas.']; Pembayaran Pasien; '.$tanggal[$jurnal];
                //insert jurnal kredit
                $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,0,$totalkredit[$jurnal],$desc,$coapiutang,$jenispembayaran[$jurnal],$bulan);
                
                //insert jurnal log
                $arrMsg = $this->insert_keu_jurnallog($tanggaljurnal,$idkasir[$jurnal],$callidjurnal);
            }            
        }
        pesan_success_danger($arrMsg, 'Rekap Transaksi Berhasil.', 'Rekap Transaksi Gagal.', 'js');
        
    }
    //insert keu transaksimasuk
    private function insert_keu_transaksimasuk($idpendaftaran,$noreferensi,$nominal,$tanggaljurnal,$jenispembayaran,$bank,$idkasir)
    {
        $datainsert = [
            'idpendaftaran'    =>  $idpendaftaran,
            'idtagihan'        =>  $noreferensi,
            'nominal'          =>  unconvertToRupiah($nominal),
            'tanggal'          =>  $tanggaljurnal,
            'jenispembayaran'  =>  $jenispembayaran,
            'bank'             =>  $bank,
            'iduserkasir'      =>  $idkasir
        ];
        $arrMsg = $this->db->insert('keu_transaksimasuk',$datainsert);
        return $arrMsg;
    }
    
    //insert keu jurnal
    private function insert_keu_jurnal($tanggaljurnal,$keterangan,$noreferensi,$ref,$departemen,$callidjurnal,$totaldebet,$totalkredit,$kode_ref)
    {
        //insert jurnal
        $data = [
            "tanggal"       =>  $tanggaljurnal,
            "keterangan"    =>  $keterangan,
            "no_referensi"  =>  $noreferensi,
            "referensi"     =>  $ref,
            "idreferensi"   =>  $noreferensi,
            "kode_referensi"=>  $kode_ref,
            "status"        =>  "1",
            "iddepartemen"  =>  $departemen,
            "kdjurnal"      =>  $callidjurnal,
            "totalkredit"   =>  unconvertToRupiah($totaldebet),
            "totaldebet"    =>  unconvertToRupiah($totalkredit),
        ];
        $arrMsg = $this->db->insert('keu_jurnal',$data);
        return $arrMsg;
    }
    
    private function insert_keu_statusrekap($idpendaftaran,$carabayar)
    {
        $datainsertrekap = [
            'idpendaftaran'=>$idpendaftaran,
            'carabayar'=>$carabayar,
            'status'=>'1'
        ];
        $arrMsg = $this->db->insert('keu_statusrekap',$datainsertrekap);
        return $arrMsg;
    }
    
    //insert jurnal detail
    private function insert_keu_jurnaldetail($tahun,$callidjurnal,$debet,$kredit,$desc,$coa,$carabayar,$bulan)
    {
        $sqlakun = "SELECT  idakun,(SELECT tipe FROM keu_tipeakun WHERE idtipeakun=keu_akun.idtipeakun) as tipeakun FROM keu_akun WHERE kode='".$coa."' ";
            
        $listakun = $this->db->query($sqlakun)->row_array();
        $datajurnaldet = [
            'tahun'     =>$tahun,
            'bulan'     =>$bulan,
            'kdjurnal'  =>$callidjurnal,
            'idakun'    =>$listakun['idakun'],
            'tipe'      =>$listakun['tipeakun'],
            'debet'     =>unconvertToRupiah($debet),
            'kredit'    =>unconvertToRupiah($kredit),
            'deskripsi' =>$desc,
            'carabayar' =>$carabayar
        ];
        $arrMsg = $this->db->insert('keu_jurnaldetail',$datajurnaldet);
        return $arrMsg;
    }
    
    //insert jurnal log
    private function insert_keu_jurnallog($tanggaljurnal,$idkasir,$callidjurnal)
    {
        //insert jurnal log
        $datainsertjurnal = [
            'status'        =>'1',
            'tanggal'       =>$tanggaljurnal,
            'iduserkasir'   =>$idkasir,
            'kdjurnal'      => $callidjurnal
        ];
        $arrMsg = $this->db->insert('keu_jurnallog',$datainsertjurnal);
        return $arrMsg;
    }
    
    public function onreadyformrekappengeluaran()
    {
        $idpengeluaran = $this->input->post('i');
        $data['rekap'] = $this->db->query("SELECT rk.kategoribiaya, rp.idpengeluaran, rp.tanggal, REPLACE(format(FLOOR(rp.nominalbiaya),0),',','.') as total,
            concat((select group_concat(b.namabarang,', ', REPLACE(format(a.jumlah,0),',','.'), ' ',c.namasatuan,' @',REPLACE(format(FLOOR(a.harga),0),',','.'),' =  ',REPLACE(format(FLOOR(a.subtotal),0),',','.') SEPARATOR '<br>') 
             from rs_pengeluaran_detail a 
             join rs_barang_pengeluaran b on b.idbarangpengeluaran = a.idbarangpengeluaran
             join rs_satuan c on c.idsatuan = a.idsatuan
             where a.idpengeluaran = rp.idpengeluaran),' <br>Potongan : ',rp.potongan)  as keterangan,
            concat('Penjual :',rpj.penjual,'<br>', ifnull(rpj.alamat,'') ) as penjual, 
            if(pper.namalengkap is null , '' , concat('Pembeli : ',ifnull(pper.namalengkap,''))) as pegawai 
            FROM rs_pengeluaran rp
            join rs_penjual rpj on rpj.idpenjual = rp.idpenjual
            left join person_pegawai pp on pp.idpegawai = rp.idpegawai
            left join person_person pper on pper.idperson = pp.idperson
            left join rs_kategoribiaya rk on rk.idkategoribiaya = rp.idkategoribiaya
            WHERE rp.idpengeluaran = '".$idpengeluaran."'")->row_array();
        $data['departemen']  = $this->mkeu_departemen->getfill('')->result();
        echo json_encode($data);
    }
    
    public function onreadyformrekaprecomend()
    {
        $idpendaftaran   = $this->input->post('i');
        $jenisperiksa    = $this->input->post('j');
        $jenispembayaran = $this->input->post('p');
        $tagihan = $this->db->query("SELECT 
        k.jenispembayaran as jenispembayaran,
                            k.idbanktujuan as banktujuan,
                            date_format(k.tanggaltagih, '%d-%m-%Y') as tanggal,
                            fnamacoa(k.idbanktujuan) as bank,
                            k.iduserkasir as kasir,
                            (select lu.namauser from login_user lu where lu.iduser = k.iduserkasir) as petugas,
                            k.jenisperiksa as jenisperiksa,
                            k.idtagihan as nonota,
                            CONCAT('Tanggal Bayar: ',
                                    date_format(k.tanggaltagih, '%d-%m-%Y'),
                                    ' RM : ',
                                    (select p.norm from person_pendaftaran p WHERE p.idpendaftaran=k.idpendaftaran)
                                    )as ket_jurnal,
                            'debit' as jenis,
                            fgetkonfigurasicoa(k.jenispembayaran) as coa, 
                            fnamacoa(fgetkonfigurasicoa(k.jenispembayaran)) as namacoa,
                            sum(k.dibayar - k.kembalian) as total,
                            (select concat('RM: ',p.norm,' Tgl.Periksa: ', date(p.waktuperiksa), ' idpendaftaran:',p.idpendaftaran) 
                        from person_pendaftaran p WHERE p.idpendaftaran=k.idpendaftaran) as deskripsi,
                            'awal' as jenisjurnal
                        FROM keu_tagihan k
                        WHERE k.idpendaftaran = '$idpendaftaran' and jenisperiksa = '".$jenisperiksa."' and jenispembayaran = '".$jenispembayaran."' 
                        GROUP BY idpendaftaran, jenispembayaran")->result_array();
        
        $listdetailjurnal = [];
        $keterangan = [];
        if($jenisperiksa == 'rajal') //detail rawat jalan
        {
            foreach ($tagihan as $arr)
            {
                $jaminan = (($arr['jenispembayaran'] == 'tunai' or $arr['jenispembayaran'] == 'transfer' or $arr['jenispembayaran'] == 'potonggaji') ? " AND jaminanasuransi = '0'" : " AND jaminanasuransi = '1'" );
                $listdetailjurnal[] = $this->db->query("SELECT
                                date_format(k.tanggaltagih, '%d/%m/%Y') as tanggal,
                                fgetkonfigurasicoa(k.jenispembayaran) as coa,
                                fnamacoa(fgetkonfigurasicoa(k.jenispembayaran)) as namacoa,
                                CONCAT('Tanggal : ', date_format(k.tanggaltagih, '%d/%m/%Y'),' RM : ',(select p.norm from person_pendaftaran p WHERE p.idpendaftaran=k.idpendaftaran))as ket_jurnal,
                                'debet' as jenis,
                                SUM(k.dibayar-k.kembalian) as total FROM keu_tagihan k
                            WHERE k.idpendaftaran = '$idpendaftaran' AND k.jenisperiksa = '".$jenisperiksa."' AND k.jenispembayaran = '".$arr['jenispembayaran']."'
                            GROUP BY idpendaftaran
                            UNION
                            SELECT 
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                (SELECT namapaketpemeriksaan FROM rs_paket_pemeriksaan WHERE idpaketpemeriksaan=rs_hasilpemeriksaan.idpaketpemeriksaan AND rs_hasilpemeriksaan.total IS NOT NULL) AS ket_jurnal, 
                                'kredit' as jenis,
                                total 
                            FROM `rs_hasilpemeriksaan` 
                            WHERE idpendaftaran='$idpendaftaran' AND idpaketpemeriksaan IS NOT NULL  AND total >0 ".$jaminan."
                            UNION
                            SELECT 
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                (SELECT namaicd FROM rs_icd WHERE icd=rs_hasilpemeriksaan.icd) AS ket_jurnal, 
                                'kredit' as jenis,
                                sum(total) as total
                            FROM `rs_hasilpemeriksaan` 
                            WHERE idpendaftaran='$idpendaftaran' and idpaketpemeriksaan IS NULL AND total >0 ".$jaminan."
                            group by icd
                            UNION
                            SELECT 
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                '' AS ket_jurnal, 
                                'kredit' as jenis, 
                                pembulatanratusan(SUM(total)) as total 
                            FROM rs_barangpemeriksaan 
                            WHERE idpendaftaran='$idpendaftaran' AND total >0 ".$jaminan."
                            GROUP by coapendapatan 

                            UNION
                            SELECT
                                '' as tanggal,
                                fgetkonfigurasicoa('pembulatan') as coa,
                                fnamacoa(fgetkonfigurasicoa('pembulatan')) as namacoa,
                                '' as ket_jurnal,
                                'kredit' as jenis,
                                sum(k.pembulatan) as total FROM keu_tagihan k
                            WHERE k.idpendaftaran = '$idpendaftaran' AND k.jenisperiksa = '".$jenisperiksa."' AND k.jenispembayaran = '".$arr['jenispembayaran']."'
                            GROUP BY idpendaftaran

                            UNION
                            SELECT 
                                '' as tanggal,
                                fgetcoadiskon(coapendapatan) as coa,
                                fnamacoa(fgetcoadiskon(coapendapatan)) as namacoa,
                                '' AS ket_jurnal,
                                'debet' as jenis,
                                pembulatanratusan(potongantagihan) as potongantagihan
                            FROM `rs_hasilpemeriksaan` 
                            WHERE idpendaftaran='$idpendaftaran' AND potongantagihan >0 ".$jaminan."

                            UNION
                            SELECT 
                                '' as tanggal,
                                fgetcoadiskon(coapendapatan) as coa,
                                fnamacoa(fgetcoadiskon(coapendapatan)) as namacoa,
                                '' AS ket_jurnal, 
                                'debet' as jenis,
                                pembulatanratusan(potongantagihan) as potongantagihan 
                            FROM `rs_barangpemeriksaan` 
                            WHERE idpendaftaran='$idpendaftaran' AND potongantagihan >0 ".$jaminan."
                            ")->result_array();

                //detail transaksi
                $keterangan[] = $this->db->query("select 
                   if(rh.idpaketpemeriksaan is not null AND rh.total IS NOT NULL ,(SELECT namapaketpemeriksaan FROM rs_paket_pemeriksaan WHERE idpaketpemeriksaan=rh.idpaketpemeriksaan AND rh.total IS NOT NULL),(select namaicd from rs_icd where icd = rh.icd)) as nama, rh.total from rs_hasilpemeriksaan rh where  idpendaftaran = '$idpendaftaran' and total > 0 ".$jaminan."
                    union all
                    select (select namabarang from rs_barang where idbarang = rb.idbarang) as nama, rb.total from rs_barangpemeriksaan rb where idpendaftaran = '$idpendaftaran' and total > 0 ".$jaminan."
                    union all
                    select 'Potongan Pemeriksaan/Tindakan' as nama, rh.potongantagihan from rs_hasilpemeriksaan rh where idpendaftaran = '$idpendaftaran' and potongantagihan > 0 ".$jaminan."
                    union all
                    select 'Potongan Farmasi' as nama, rb.total from rs_barangpemeriksaan rb where idpendaftaran = '$idpendaftaran' and potongantagihan > 0 ".$jaminan."
                    union all
                    select 'Pembulatan' as nama ,sum(pembulatan) as total from keu_tagihan where idpendaftaran = '$idpendaftaran' and jenisperiksa ='".$jenisperiksa."' AND jenispembayaran = '".$arr['jenispembayaran']."' ")->result_array();
                
            }
        }
        elseif($jenisperiksa == 'ranap') // detail rawat inap
        {
            foreach ($tagihan as $arr)
            {
                $jaminan = (($arr['jenispembayaran'] == 'tunai' or $arr['jenispembayaran'] == 'transfer') ? " AND jaminanasuransi = '0'" : " AND jaminanasuransi = '1'" );
                $listdetailjurnal[] = $this->db->query("SELECT
                                date_format(k.tanggaltagih, '%d/%m/%Y') as tanggal,
                                fgetkonfigurasicoa(k.jenispembayaran) as coa,
                                fnamacoa(fgetkonfigurasicoa(k.jenispembayaran)) as namacoa,
                                CONCAT('Tanggal : ', date_format(k.tanggaltagih, '%d/%m/%Y'),' RM : ',(select p.norm from person_pendaftaran p WHERE p.idpendaftaran=k.idpendaftaran))as ket_jurnal,
                                'debet' as jenis,
                                SUM(k.dibayar-k.kembalian) as total FROM keu_tagihan k
                            WHERE k.idpendaftaran = '$idpendaftaran' AND k.jenisperiksa = '".$jenisperiksa."' AND k.jenispembayaran = '".$arr['jenispembayaran']."'
                            GROUP BY idpendaftaran
                            UNION
                            SELECT 
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                (CASE WHEN idpaketpemeriksaan IS NULL THEN
                                        (SELECT namaicd FROM rs_icd WHERE icd=rirmhp.icd)
                                    WHEN idpaketpemeriksaan IS NOT NULL THEN
                                        (SELECT namapaketpemeriksaancetak FROM rs_paket_pemeriksaan WHERE idpaketpemeriksaan=rirmhp.idpaketpemeriksaan AND rirmhp.total IS NOT NULL)END) AS ket_jurnal, 
                                'kredit' as jenis,
                                sum(rirmhp.total) as total
                            FROM rs_inap_rencana_medis_hasilpemeriksaan rirmhp
                            WHERE idpendaftaran='$idpendaftaran' AND total >0 
                            GROUP by rirmhp.icd
                            UNION
                            SELECT
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                (select namaicd from rs_icd where icd = rs_inap_biayanonpemeriksaan.icd) AS ket_jurnal, 
                                'kredit' as jenis, 
                                pembulatanratusan(sum(total)) as total 
                            FROM rs_inap_biayanonpemeriksaan 
                            WHERE idpendaftaran='$idpendaftaran' AND total >0 AND status = 'terlaksana'
                            GROUP by icd
                            UNION
                            SELECT 
                                '' as tanggal,
                                coapendapatan as coa,
                                fnamacoa(coapendapatan) as namacoa,
                                '' AS ket_jurnal, 
                                'kredit' as jenis, 
                                pembulatanratusan((total)) as total 
                            FROM rs_inap_rencana_medis_barang 
                            WHERE idpendaftaran='$idpendaftaran' AND total >0 
                            GROUP by coapendapatan

                            UNION
                            SELECT
                                '' as tanggal,
                                fgetkonfigurasicoa('pembulatan') as coa,
                                fnamacoa(fgetkonfigurasicoa('pembulatan')) as namacoa,
                                '' as ket_jurnal,
                                'kredit' as jenis,
                                sum(k.pembulatan) as total FROM keu_tagihan k
                            WHERE k.idpendaftaran = '$idpendaftaran' AND k.jenisperiksa = '".$jenisperiksa."' AND k.jenispembayaran = '".$arr['jenispembayaran']."'
                            GROUP BY idpendaftaran
                            ")->result_array();

                //detail transaksi
                $keterangan[] = $this->db->query("SELECT
                 (select namaicd from rs_icd where icd = rh.icd) as nama, sum(rh.total) as total FROM rs_inap_rencana_medis_hasilpemeriksaan rh where rh.idpendaftaran = '$idpendaftaran' and rh.total > 0 ".$jaminan."
                union all
                select (select namabarang from rs_barang where idbarang = rirmb.idbarang) as nama, total from rs_inap_rencana_medis_barang rirmb where idpendaftaran='$idpendaftaran' and total > 0
                union all 
                select 'Pembulatan' as nama ,sum(pembulatan) as total from keu_tagihan where idpendaftaran = '$idpendaftaran' and jenisperiksa ='".$jenisperiksa."' AND jenispembayaran = '".$arr['jenispembayaran']."' ")->result_array();
                
            }
            
        }
        
        
        $where_dept = array('kode'=>$jenisperiksa);
        $departemen = $this->mkeu_departemen->getfill($where_dept)->result();
        
        $data['tagihan']    = $tagihan;
        $data['keterangan'] = $keterangan;
        $data['departemen'] = $departemen;
        $data['jurnal']     = $listdetailjurnal;
        echo json_encode($data);
    }
    
    /**
     * Mahmud, clear
     * Pendapatan Kas
     */
    public function pendapatankas()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'pemasukan',
            'active_menu_level'=> 'pendapatankas', 
            'content_view'     => 'keuangan/v_pendapatankas',
            'script_js'        => ['keuangan/pendapatankas'],
            'title_page'       => 'Pendapatan Kas',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE, PLUG_DROPDOWN, PLUG_TEXTAREA]
        ];
        $this->load->view('v_index',$data);
    }
    public function dt_pendapatankas()
    {

        $getData = $this->mkeuangan->dt_pendapatankas_(true);
        $data = []; 
        $no   = $_POST['start'];
        if($getData)
        {
            foreach ($getData->result() as $obj)
            {
                
                $menu = (($obj->sudahrekap > 0 && $obj->bolehdijurnal == 1) ? '<a id="detailjurnal" '. ql_tooltip('Detail Jurnal') .' idpendapatan="'.$obj->idpendapatan.'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a> <a id="hapusjurnal" '. ql_tooltip('Hapus Jurnal') .' kdjurnal="'.$obj->kdjurnal.'" info="'.$obj->pendapatan.' Tanggal '.$obj->tanggal.'" class="btn btn-warning btn-xs"><i class="fa fa-minus-circle"></i></a>' : '' );
                $menu .= (($obj->sudahrekap == 0 && $obj->bolehdijurnal == 1) ? ' <a id="buatjurnal" '. ql_tooltip('Buat Jurnal') .' idpendapatan="'.$obj->idpendapatan.'" class="btn btn-primary btn-xs"><i class="fa fa-check"></i></a>' : '' ) ;
                $menu .= (($obj->sudahrekap == 0 && $obj->bolehdijurnal == 1 && $obj->isgenerate == 0) ? ' <a id="ubah" '. ql_tooltip('Ubah Pendapatan') .' idpendapatan="'.$obj->idpendapatan.'" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a>' : '' );                
                $menu .= (($obj->sudahrekap == 0 || $obj->bolehdijurnal == 0) ? ' <a id="hapus" '. ql_tooltip('Hapus Pendapatan') .' idpendapatan="'.$obj->idpendapatan.'" pendapatan="'.$obj->pendapatan.'" tanggal = "'.$obj->tanggal.'"  class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>' : '' );
                
                $shif  = (($obj->levelshif == 1) ? 'label-success' : (($obj->levelshif == 2) ? 'label-primary' : 'label-danger' ) ) ;
                
                $row = array();
                $row[] = $obj->jenispendapatan;
                $row[] = $obj->tanggal;
                $row[] = $obj->pendapatan;
                $row[] = $obj->penyetor;
                $row[] = $obj->keterangan;
                $row[] = convertToRupiah($obj->nominal);
                $row[] = $obj->jenistransaksi. (($obj->jenistransaksi == 'transfer') ? '<br>'.$obj->idcoabank.' - '.$obj->namabank : '');
                $row[] = $obj->tanggalshif .'<br> <label class="label '.$shif.'"> Shif '.$obj->levelshif.'</label>';
                $row[] = $obj->namauser;
                $row[] = $menu;
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mkeuangan->total_dt_pendapatankas(),
            "recordsFiltered" => $this->mkeuangan->filter_dt_pendapatankas(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function generate_pendapatankas()
    {
        $tanggal = $this->input->post('tanggal');
        $shif    = $this->input->post('shif');
        $jenis   = $this->input->post('jenispendapatan');
        $data    = $this->mkeuangan->getdata_generatependapatankas($tanggal,$shif,$jenis);
        
        $pendapatan = (($jenis == "penjualanobatbebas") ? "Penjualan Obat Bebas" : "Pemeriksaan Rawat Jalan" );
        $jenisnew = (($jenis == 'penjualanobatbebas') ? 'Penjualan Obat Bebas' : 'Rawat Jalan' );
        $iduser     = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $penyetor   = $this->session->userdata('username');
        if(empty($data))
        {
            pesan_danger('Data Kosong.', 'js');
        }
        else
        {
            foreach ($data as $arr)
            {
                
                $del['tanggalshif']    = $arr['tanggalshif'];
                $del['jenistransaksi'] = $arr['jenispembayaran'];
                $del['levelshif']      = $shif;
                $del['jenispendapatan']= $jenisnew;
                $del['isgenerate']     = 1;
                if($jenis == "penjualanobatbebas" && empty(!$arr['idbanktujuan']))
                {
                    $del['idcoabank']   = $arr['idbanktujuan'];
                }  
                $arrMsg = $this->db->delete('keu_pendapatankas',$del);
                
                $dt['pendapatan']      = 'Pendapatan '.$pendapatan;
                $dt['tanggal']         = $arr['tanggal'];
                $dt['penyetor']        = $penyetor;
                $dt['nominal']         = $arr['dibayar'];
                $dt['jenistransaksi']  = $arr['jenispembayaran'];
                $dt['keterangan']      = 'Pendapatan Kas Rawat Jalan ('.$arr['jenispembayaran'].') Shif '.$shif.' Tanggal '.$tanggal;
                $dt['idcoabank']       = (($jenis == "penjualanobatbebas") ? $arr['idbanktujuan'] : "" );
                $dt['levelshif']       = $shif;
                $dt['tanggalshif']     = $arr['tanggalshif'];
                $dt['iduser']          = $arr['iduserkasir'];
                $dt['jenispendapatan'] = $jenisnew;
                $dt['bolehdijurnal']   = (($jenis == "penjualanobatbebas") ? 1 : 0 );
                $dt['isgenerate']      = 1;
                $arrMsg = $this->db->insert('keu_pendapatankas',$dt);
            }
            pesan_success_danger($arrMsg, 'Generate '.$jenisnew.' Berhasil.', 'Generate '.$jenisnew.' Gagal.', 'js');
        }
        
        
    }
    
    public function save_pendapatankas()
    {
        $jenistransaksi = ((empty($this->input->post('transfer'))) ? 'tunai' : 'transfer' );
        $data['pendapatan'] = $this->input->post('pendapatan');
        $data['tanggal']    = $this->input->post('tanggal');
        $data['penyetor']   = $this->input->post('penyetor');
        $data['nominal']    = $this->input->post('nominal');
        $data['jenistransaksi'] = $jenistransaksi;
        $data['keterangan'] = $this->input->post('keterangan');
        $data['jenispendapatan'] = $this->input->post('jenispendapatan');
        $data['idcoabank']  = $this->input->post('idcoabank');
        $data['iduser']     = $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        
        $idpendapatan = $this->input->post('idpendapatan');
        $where['idpendapatan'] = $idpendapatan;
        if(empty($idpendapatan))
        {
            $save = $this->db->insert('keu_pendapatankas',$data);
        }
        else
        {
            $save = $this->db->update('keu_pendapatankas',$data,$where);
        }
        pesan_success_danger($save,'Simpan Pendapatan Berhasil.', 'Simpan Pendapatan Gagal.', 'js');
    }
    
    public function formready_pendapatankas()
    {
        if($this->input->post('mode') == 'view')
        {
            $data['kasir'] = $this->db->query("SELECT kak.iduserkasir as id,  lu.namauser as txt FROM keu_akun_kaskasir kak join login_user lu on lu.iduser = kak.iduserkasir GROUP by iduserkasir")->result();
        }
        else
        {
            $data['dataedit'] = $this->db->get_where('keu_pendapatankas',['idpendapatan'=>$this->input->post('id')])->row_array();
            $data['coa']      = $this->db->query("SELECT kak.kode as id, ka.nama as txt FROM keu_akun_kasbank kak join keu_akun ka on ka.kode = kak.kode")->result_array();
            $data['jenis']    = $this->db->select('jenispendapatankas as id, jenispendapatankas as txt')->get('keu_pendapatankas_jenis')->result_array();
        }
        echo json_encode($data);
    }
    
    public function delete_pendapatankas()
    {
        $idpendapatan = $this->input->post('idpendapatan');
        $arrMsg = $this->db->delete('keu_pendapatankas',['idpendapatan'=>$idpendapatan]);
        pesan_success_danger($arrMsg, 'Hapus Pendapatan Berhasil.', 'Hapus Pendapatan Gagal.', 'js');
    }
    
    public function delete_pendapatankas_jurnal()
    {
        $kdjurnal = $this->input->post('kdjurnal');
        $arrMsg = $this->db->delete('keu_jurnal',['kdjurnal'=>$kdjurnal]);
        pesan_success_danger($arrMsg, 'Hapus Jurnal Berhasil.', 'Hapus Jurnal Gagal.', 'js');
    }
    
    public function onreadyformjurnalpendapatankas()
    {
        $idpendapatan = $this->input->post('i');
        $data['rekap'] = $this->db->query("SELECT tanggal, pendapatan, keterangan, nominal, idpendapatan,jenistransaksi,jenispendapatan, idcoabank, if(idcoabank !=0,(select nama from keu_akun where kode = idcoabank),0) as bank, iduser FROM `keu_pendapatankas` WHERE idpendapatan = '".$idpendapatan."'")->row_array();
        $data['departemen']  = $this->mkeu_departemen->getfill('')->result();
        echo json_encode($data);
    }
    
    public function onreadyformdetailjurnalpendapatankas()
    {
        $idpendapatan = $this->input->post('i');
        $data = $this->db->query("select a.keterangan, a.idreferensi, a.referensi, a.tanggal, a.iddepartemen, a.totalkredit, a.totaldebet, a.note, b.idakun, b.deskripsi, c.kode as kodeakun, c.nama as namaakun, d.nama as departemen, b.debet, b.kredit
        from keu_jurnal a 
        join keu_jurnaldetail b on b.kdjurnal = a.kdjurnal
        join keu_akun c on c.idakun = b.idakun
        join keu_departemen d on d.iddepartemen = a.iddepartemen
        WHERE kode_referensi = 'idpendapatankas_' and idreferensi = '".$idpendapatan."'")->result_array();
        echo json_encode($data);
    }
    
    public function rekaptransaksi_createjurnal()
    {
        $callidjurnal   = time().generaterandom(3);
        $tanggal        = $this->input->post('tanggal');
        $departemen     = $this->input->post('iddepartemen');
        $noreferensi    = $this->input->post('no_referensi');        
        $nominal        = $this->input->post('nominal');        
        $keterangan     = $this->input->post('keterangan');
        $totaldebet     = $this->input->post('totaldebet');
        $totalkredit    = $this->input->post('totalkredit');
        $kode_ref       = $this->input->post('kode_ref');
        $referensi       = $this->input->post('referensi');
        //insert jurnal
        $arrMsg = $this->insert_keu_jurnal($tanggal,$keterangan,$noreferensi,$referensi,$departemen,$callidjurnal,$totaldebet,$totalkredit,$kode_ref);
            
        //insert jurnal detail
        $jumlahjurnaldetail   = $this->input->post('jumlahjurnaldetail');
        $coa    = $this->input->post('coa');
        $debet  = $this->input->post('debet');
        $kredit = $this->input->post('kredit');
        $desc   = $this->input->post('deskripsi');
        $tahun  = date('Y', strtotime($tanggal) ); 
        $bulan  = date('n', strtotime($tanggal) ); 
        for($jurnaldetail=0; $jurnaldetail< count($jumlahjurnaldetail); $jurnaldetail++)
        {
           //insert jurnal detail
           $arrMsg = $this->insert_keu_jurnaldetail($tahun,$callidjurnal,$debet[$jurnaldetail],$kredit[$jurnaldetail],$desc[$jurnaldetail],$coa[$jurnaldetail],'',$bulan);
        }
        $iduser = json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('iduser')));
        $arrMsg = $this->insert_keu_jurnallog($tanggal,$iduser,$callidjurnal);
        pesan_success_danger($arrMsg, 'Rekap Transaksi Berhasil.', 'Rekap Transaksi Gagal.', 'js');
    }
    
    // /pendapatan kas
    
    public function pendapatan_detail($tanggal,$idkasir,$jenistagihan)
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'pemasukan',
            'active_menu_level'=> 'pendapatankas', 
            'content_view'     => 'keuangan/v_pendapatandet',
            'script_js'        => ['keuangan/pendapatandet'],
            'title_page'       => 'Pendapatan Detail',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE],
            'tanggal'          => $tanggal,
            'idkasir'          => $idkasir,
            'jenistagihan'     => $jenistagihan
        ];
        $this->load->view('v_index',$data);
    }
    public function pendapatan_detail_trans($tanggal,$idkasir,$jenistagihan,$bank)
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'pendapatankaskeu',
            'active_menu_level'=> 'pendapatankas', 
            'content_view'     => 'keuangan/v_pendapatandet_trans',
            'script_js'        => ['keuangan/pendapatandet_trans'],
            'title_page'       => 'Pendapatan Detail',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE],
            'tanggal'          => $tanggal,
            'idkasir'          => $idkasir,
            'jenistagihan'     => $jenistagihan,
            'bank'             => $bank
        ];
        $this->load->view('v_index',$data);
    }
    
    /**
     * Mahmud, clear
     * Kategori Biaya
     */
    public function kategoribiaya()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'pengeluaran',
            'active_menu_level'=> 'kategoribiaya', 
            'content_view'     => 'keuangan/v_pengeluaran',
            'mode'             => 'kategori',
            'jsmode'           => 'kategori',
            'dtkategori'       => $this->db->get_where('rs_kategoribiaya',['isdelete'=>0])->result_array(),
            'script_js'        => ['keuangan/pengeluaran'],
            'title_page'       => 'Kategori Biaya',
            'plugins'          => [PLUG_DATATABLE]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function insert_rs_kategoribiaya()
    {
        if(empty(!$this->input->post('idkategori')))
        {
            $data['idkategoribiaya'] = $this->input->post('idkategori');
        }        
        $data['kategoribiaya']   = $this->input->post('kategori');
        $arrMsg = $this->db->replace('rs_kategoribiaya',$data);
        pesan_success_danger($arrMsg, 'Simpan Berhasil.', 'Simpan Gagal.', 'js');
    }
    
    public function hapus_kategoribiaya()
    {
        $where['idkategoribiaya']   = $this->input->post('id');
        $arrMsg = $this->db->update('rs_kategoribiaya',['isdelete'=>1],$where);
        pesan_success_danger($arrMsg, 'Simpan Berhasil.', 'Simpan Gagal.', 'js');
    }
    
    /**
     * Mahmud, clear
     * Pengeluaran
     */    
    public function pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'pengeluaran',
                'active_menu_level'=> 'pengeluaran', 
                'content_view'     => 'keuangan/v_pengeluaran',
                'mode'             => 'view',
                'jsmode'           => 'view',
                'script_js'        => ['keuangan/pengeluaran'],
                'title_page'       => 'Pengeluaran',
                'plugins'          => [PLUG_DATATABLE, PLUG_DATE,PLUG_DROPDOWN, PLUG_TEXTAREA]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function dt_pengeluaran()
    {
        $getData = $this->mdatatable->dt_pengeluaran_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                
                $menu = (($obj->statusrekap == 0) ? "<a id='rekappengeluaran' idpengeluaran='".$obj->idpengeluaran."' ".  ql_tooltip('Buat Jurnal')." class='btn btn-xs btn-primary'><i class='fa fa-check'></i></a> " : "" );
                $menu .= (($obj->statusrekap == 0) ? " <a href='".base_url('ckeuangan/edit_pengeluaran/'.$obj->idpengeluaran)."' ".  ql_tooltip('Ubah')." class='btn btn-xs btn-warning'><i class='fa fa-edit'></i></a>" : "" );
                $menu .= (($obj->statusrekap != 0) ? " <a id='hapusjurnal' info='kategori : ".$obj->kategoribiaya." <br> Keterangan : ".$obj->keterangan." <br> Nominal : ".convertToRupiah($obj->nominalbiaya)."' kdjurnal='".$obj->kdjurnal."' ".ql_tooltip('Hapus Jurnal')." class='btn btn-xs btn-warning'><i class='fa fa-minus-circle'></i></a>" : "" );
                $menu .= " <a id='hapustransaksi' info='kategori : ".$obj->kategoribiaya." <br> Keterangan : ".$obj->keterangan." <br> Nominal : ".convertToRupiah($obj->nominalbiaya)."' idpengeluaran='".$obj->idpengeluaran."' kdjurnal='".$obj->kdjurnal."'  ".   ql_tooltip('Hapus Transaksi')." class='btn btn-xs btn-danger'><i class='fa fa-trash'></i></a>";
                $row = array();
                $row[] = $obj->kategoribiaya;
                $row[] = $obj->tanggal;
                $row[] = $obj->notransaksi;
                $row[] = $obj->penjual;
                $row[] = convertToRupiah($obj->nominalbiaya);
                $row[] = $obj->catatan;
                $row[] = $obj->keterangan;
                $row[] = $obj->pembeli;
                $row[] = $menu . 
                        " <a ".  ql_tooltip('Detail')." href='".base_url('ckeuangan/detail_pengeluaran/'.$obj->idpengeluaran)."' class='btn btn-xs btn-info'><i class='fa fa-eye'></i></a>";
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_pengeluaran(),
            "recordsFiltered" => $this->mdatatable->filter_dt_pengeluaran(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    public function add_pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'pengeluaran',
                'active_menu_level'=> 'list_pengeluaran', 
                'mode'             => 'add',
                'jsmode'           => 'add',
                'content_view'     => 'keuangan/v_pengeluaran',
                'script_js'        => ['keuangan/pengeluaran','form_penjual','form_barang_pengeluaran','form_satuan'],
                'kategori'         => $this->db->get_where('rs_kategoribiaya',['isdelete'=>0])->result(),
                'title_page'       => 'Tambah Pengeluaran',
                'plugins'          => [PLUG_DATE,PLUG_DROPDOWN]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function detail_pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $idpengeluaran = $this->uri->segment(3);

            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'pengeluaran',
                'active_menu_level'=> 'list_pengeluaran', 
                'mode'             => 'detail',
                'jsmode'           => 'detail',
                'content_view'     => 'keuangan/v_pengeluaran',
                'script_js'        => [],
                'title_page'       => 'Detail Pengeluaran',
                'plugins'          => [],
                'dtpengeluaran'    => $this->mkeuangan->view_rs_pengeluaran_join(['idpengeluaran'=>$idpengeluaran]),
                'dtdetail'         => $this->mkeuangan->view_rs_pengeluaran_detail_join(['idpengeluaran'=>$idpengeluaran])
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function edit_pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $idpengeluaran = $this->uri->segment(3);

            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'pengeluaran',
                'active_menu_level'=> '', 
                'mode'             => 'add',
                'jsmode'           => 'edit',
                'kategori'         => $this->db->get_where('rs_kategoribiaya',['isdelete'=>0])->result(),
                'content_view'     => 'keuangan/v_pengeluaran',
                'script_js'        => ['keuangan/pengeluaran','form_penjual','form_barang_pengeluaran','form_satuan'],
                'title_page'       => 'Ubah Pengeluaran',
                'plugins'          => [PLUG_DATE,PLUG_DROPDOWN]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function onready_editpengeluaran()
    {
        $idpengeluaran = $this->input->post('i');
        $data['dtpengeluaran'] = $this->mkeuangan->view_rs_pengeluaran_join(['idpengeluaran'=>$idpengeluaran]);
        $data['dtdetail']      = $this->mkeuangan->view_rs_pengeluaran_detail_join(['idpengeluaran'=>$idpengeluaran]);
        echo json_encode($data);
    }
    
    public function hapus_pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            if(!empty($this->input->post('kdjurnal')) )
            {
                $arrMsg = $this->db->delete('keu_jurnal',['kdjurnal'=>$this->input->post('kdjurnal')]);
                $arrMsg = $this->db->delete('keu_jurnaldetail',['kdjurnal'=>$this->input->post('kdjurnal')]);
            }
            $arrMsg = $this->db->delete('rs_pengeluaran',['idpengeluaran'=>$this->input->post('idpengeluaran')]);
            pesan_success_danger($arrMsg, "Hapus Transaksi Berhasil.", "Hapus Transaksi Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function hapus_pengeluaran_jurnal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $arrMsg = $this->db->delete('keu_jurnal',['kdjurnal'=>$this->input->post('kdjurnal')]);
            $arrMsg = $this->db->delete('keu_jurnaldetail',['kdjurnal'=>$this->input->post('kdjurnal')]);
            pesan_success_danger($arrMsg, "Hapus Jurnal Berhasil.", "Hapus Jurnnal Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    public function delete_rs_pengeluaran_detail()
    {
        $arrMsg = $this->db->delete('rs_pengeluaran_detail',['idpengeluarandetail'=>$this->input->post('ipdetail')]);
        pesan_success_danger($arrMsg, "Hapus Item Berhasil.", "Hapus Item Gagal.","js");
    }
    
    public function save_pengeluaran()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)) //lihat define di atas
        {
            $idpengeluaran     = $this->input->post('idpengeluaran');
            $dt['tanggal']     = $this->input->post('tanggalbeli');
            $dt['notransaksi'] = $this->input->post('notransaksi');
            $dt['total']       = unconvertToRupiah($this->input->post('total'));
            $dt['potongan']    = unconvertToRupiah($this->input->post('potongan'));
            $dt['catatan']     = $this->input->post('catatan');
            $dt['keterangan']  = str_replace("'","`",$this->input->post('keterangan'));
            $dt['idpenjual']   = $this->input->post('idpenjual');
            $dt['idpegawai']   = $this->input->post('idpegawai');
            $dt['idkategoribiaya'] = $this->input->post('idkatgeori');

            $dtd['idpengeluaran'] = $idpengeluaran;
            //insert pengeluaran
            $this->db->trans_start();
            if(empty($idpengeluaran))
            {
                $arrMsg = $this->db->insert('rs_pengeluaran',$dt);            
                $dtd['idpengeluaran'] = $this->db->insert_id();
            }
            else
            {            
                $arrMsg = $this->db->update('rs_pengeluaran',$dt,$dtd);
            }        

            $ipdetail = $this->input->post('idpengeluarandetail');
            $item   = $this->input->post('item');
            $satuan = $this->input->post('satuan');
            $jumlah = $this->input->post('jumlah');
            $harga  = $this->input->post('hargasatuan');
            $subtotal = $this->input->post('subtotal');

            for($x=0; $x<count($satuan); $x++)
            {
                $dtd['idbarangpengeluaran'] = $item[$x];
                $dtd['idsatuan']      = $satuan[$x];
                $dtd['jumlah']        = unconvertToRupiah($jumlah[$x]);
                $dtd['harga']         = unconvertToRupiah($harga[$x]);
                $dtd['subtotal']      = unconvertToRupiah($subtotal[$x]);
                if(empty($ipdetail[$x])){
                    $arrMsg = $this->db->insert('rs_pengeluaran_detail',$dtd);
                }else{
                    $arrMsg = $this->db->update('rs_pengeluaran_detail',$dtd,['idpengeluarandetail'=>$ipdetail[$x]]);
                }

            }

            $this->db->trans_complete();
            pesan_success_danger($arrMsg, 'Simpan Data Berhasil.', 'Simpan Data Gagal.');
            redirect(base_url('ckeuangan/pengeluaran'));
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    /**
     * Mahmud, clear
     * Cash Equivalen
     */    
    public function cash_equivalen()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $data =  [
                'active_menu'      => 'keuangan',
                'active_sub_menu'  => 'cash_equivalen',
                'active_menu_level'=> '', 
                'content_view'     => 'keuangan/v_cash_equivalen',
                'mode'             => 'view',
                'jsmode'           => 'view',
                'script_js'        => ['keuangan/cash_equivalen'],
                'title_page'       => 'Cash Equivalen',
                'bulan'            => ql_namabulan(),
                'plugins'          => [PLUG_DATATABLE, PLUG_DATE,PLUG_DROPDOWN]
            ];
            $this->load->view('v_index',$data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function view_cash_ekuivalen()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $tahun = $this->input->post('tahun');
            $bulan = $this->input->post('bulan');

            $tanggal = $tahun.'-'.$bulan.'-'.$this->input->post('tgl1');


            $where['kc.tahun'] = $tahun;
            $where['kc.bulan'] = $bulan;
            $where['kc.tanggal >= '] = $tanggal;
            $data['trx']   = $this->mkeuangan->view_cash_equivalen_join($where);

            $data['saldo'] = $this->db->query("SELECT ifnull(sum(if(isdebit = 0, nominal,0)),0) as kredit , ifnull(sum(if(isdebit = 1, nominal,0)),0) as debit, ifnull((select saldoawal from keu_cashequivalen_saldoawal WHERE tahun = '".$tahun."' and bulan = '".$bulan."'),0) as saldoawal FROM `keu_cashequivalen` WHERE tahun = '".$tahun."' and bulan = '".$bulan."' and tanggal < '".$tanggal."'")->row_array();
            $data['post']  = $this->input->post();
            $data['tanggal'] = $tanggal;
            $data['last']  = $this->db->last_query();
            echo json_encode($data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    
    public function onreadyform_cashequivalen()
    {
        $dt['jenis'] = $this->db->select('jenistransaksi as id, jenistransaksi as txt')->get('keu_cash_equivalen_jenistransaksi')->result();
        $where = array(
          'id'    => $this->input->post('i'),
          'tahun' => $this->input->post('tahun'),
          'bulan' => $this->input->post('bulan')
        );
        $dt['edit']  = ((empty($this->input->post('i'))) ? '' : $this->db->get_where('keu_cashequivalen',$where)->row_array() ) ;
        echo json_encode($dt);
    }
    
    public function save_keu_cashequivalen()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $id = $this->input->post('idcashequivalen');
            $data['tanggal']        = $this->input->post('tanggal');
            $data['jenis']          = $this->input->post('jenistransaksi');
            $data['nama_transaksi'] = $this->input->post('transaksi');
            $data['referensi']      = $this->input->post('referensi');
            $data['nominal']        = unconvertToRupiah($this->input->post('nominal'));
            $where['id']    = $id;
            $where['tahun'] = $this->input->post('tahun');
            $where['bulan'] = $this->input->post('bulan');

            if(empty(!$id) && $this->input->post('tanggalsebelum') == $this->input->post('tanggal'))
            {            
                $simpan = $this->db->update('keu_cashequivalen',$data,$where);
            }
            else
            {            
                $this->db->delete('keu_cashequivalen',$where);
                $simpan = $this->db->insert('keu_cashequivalen',$data);
            }

            pesan_success_danger($simpan, "Simpan Berhasil.", "Simpan Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function delete_keu_cashequivalen()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $where = array(
              'id'    => $this->input->post('i'),
              'tahun' => $this->input->post('tahun'),
              'bulan' => $this->input->post('bulan')
            );
            $hapus = $this->db->delete('keu_cashequivalen',$where);
            pesan_success_danger($hapus, "Hapus Berhasil.", "Hapus Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function view_saldoawal_cash_ekuivalen()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $where['tahun'] = $this->input->post('tahun');
            $data = $this->db->get_where('keu_cashequivalen_saldoawal',$where)->result_array();
            echo json_encode($data);
        }
        else
        {
            aksesditolak();
        }
    }
    
    public function save_keu_cashequivalen_saldoawal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $post = $this->input->post();

            $data['bulan'] = $this->input->post('bulansaldoawal');
            $data['tahun'] = $this->input->post('tahun');
            $data['saldoawal'] = unconvertToRupiah($this->input->post('nominal'));

            if( isset($post['tahun']) && empty(!$post['tahun']) )
            {
                $simpan = $this->db->replace('keu_cashequivalen_saldoawal',$data);
            }
            else
            {
                $data   = ['saldoawal' => unconvertToRupiah($post['nominal'])];
                $where  = ['bulan'=>$post['bulansaldoawal1'],'tahun'=>$post['tahun1']];
                $simpan = $this->db->update('keu_cashequivalen_saldoawal',$data,$where);
            }

            pesan_success_danger($simpan, "Simpan Berhasil.", "Simpan Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }


    public function delete_keu_cashequivalen_saldoawal()
    {
        if ($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)) //lihat define di atas
        {
            $where = array(
              'tahun' => $this->input->post('tahun'),
              'bulan' => $this->input->post('bulan')
            );
            $hapus = $this->db->delete('keu_cashequivalen_saldoawal',$where);
            pesan_success_danger($hapus, "Hapus Berhasil.", "Hapus Gagal.","js");
        }
        else
        {
            aksesditolak();
        }
    }

    /**
     * mahmud, clear
     * Konfigurasi
     */
    public function coakaskasir()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'konfigurasi',
            'active_menu_level'=> 'coakaskasir', 
            'content_view'     => 'keuangan/v_konfigurasi',
            'mode'             => 'viewcoakasir',
            'jsmode'           => 'viewcoakasir',
            'script_js'        => ['keuangan/konfigurasi'],
            'title_page'       => 'Konfigurasi COA Kas Kasir',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE,PLUG_DROPDOWN, PLUG_TEXTAREA]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function dt_konfigcoakasir()
    {
        $getData = $this->mdatatable->dt_konfigcoakasir_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = array();
                $row[] = $obj->kode;
                $row[] = $obj->nama;
                $row[] = $obj->carabayar;
                $row[] = $obj->namauser;
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_konfigcoakasir(),
            "recordsFiltered" => $this->mdatatable->filter_dt_konfigcoakasir(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    /**
     * Mahmud, clear
     * COA kas Bank
     */
    public function coakasbank()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'konfigurasi',
            'active_menu_level'=> 'coakasbank', 
            'content_view'     => 'keuangan/v_konfigurasi',
            'mode'             => 'viewcoabank',
            'jsmode'           => 'viewcoabank',
            'script_js'        => ['keuangan/konfigurasi'],
            'title_page'       => 'Konfigurasi COA Kas Bank',
            'plugins'          => [PLUG_DATATABLE, PLUG_DATE,PLUG_DROPDOWN, PLUG_TEXTAREA]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function dt_konfigcoabank()
    {
        $getData = $this->mdatatable->dt_konfigcoabank_(true);
        $data=[]; $no=$_POST['start'];
        if($getData){
            foreach ($getData->result() as $obj) {
                $row = array();
                $row[] = $obj->kode;
                $row[] = $obj->nama;
                $data[] = $row;
            }
        } 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mdatatable->total_dt_konfigcoabank(),
            "recordsFiltered" => $this->mdatatable->filter_dt_konfigcoabank(),
            "data" =>$data
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    
    /*
     * Mahmud, clear
     * Seting Periode Jurnal
     */
    public function periode()
    {
        $data =  [
            'active_menu'      => 'keuangan',
            'active_sub_menu'  => 'konfigurasi',
            'active_menu_level'=> 'periode', 
            'content_view'     => 'keuangan/v_konfigurasi',
            'mode'             => 'viewperiode',
            'jsmode'           => 'viewperiode',
            'script_js'        => ['keuangan/konfigurasi'],
            'title_page'       => 'Periode Laporan Keuangan',
            'bulan'            => ql_namabulan(),
            'periode'          => $this->db->get('keu_periode')->row_array(),
            'plugins'          => [PLUG_DATE]
        ];
        $this->load->view('v_index',$data);
    }
    
    public function update_periode()
    {
        $arrMsg = $this->db->update('keu_periode',['tahun'=>$this->input->post('periodetahun'),'bulan'=>$this->input->post('periodebulan')]);
        pesan_success_danger($arrMsg, 'Ubah Periode Laporan Berhasil.', 'Ubah Periode Laporan Gagal.');
        redirect(base_url('ckeuangan/periode'));
    }
    
    /**
     * Tutup Buku
     * mahmud, on DEV
     */
    public function create_tutup_buku()
    {
        $this->input->post('bulansekarang');
        $this->input->post('tahunsekarang');
        $this->input->post('bulanselanjutnya');
        $this->input->post('tahunselanjutnya');
        //menunggu sql ambil saldo akhir periode saat ini untuk di pindah ke saldo awal periode selanjutnya
    }

    /**
     * Berkas Pegawai
     * Tito Otniel Simarmata
    */

    public function berkas()
    {
        $data =  [
            'active_menu'      => 'sdmk',
            'active_sub_menu'  => 'berkas',
            'active_menu_level'=> 'Berkas Pegawai', 
            'content_view'     => 'sdmk/v_berkaspegawai',
            'mode'             => 'viewberkas',
            // 'jsmode'           => 'viewperiode',
            'script_js'        => ['sdmk/js_berkaspegawai'],
            'title_page'          => 'Berkas Pegawai',
            // 'periode'          => $this->db->get('keu_periode')->row_array(),
           'plugins'              => [PLUG_DROPDOWN, PLUG_CHECKBOX, PLUG_DATATABLE]
        ];
        $this->load->view('v_index',$data);
    }

    
    /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function get_data_berkas_pegawai()
    {
        $query = $this->db->query("select sbp.id_berkas, sbp.riwayat, sbp.ijazahterlegalisir, sbp.str, sbp.siksip, sbp.buktiverifijazah, sbp.suratkeputusansuratperjanjian, sbp.uraiantugas, sbp.rkkspkk, sbp.buktipenilaiankinerja, sbp.datapelatihan, sbp.buktilamarankerja, ppg.bpjs_kodedokter, ppg.idpegawai, ppg.nip,ppg.sip, ppg.statuskeaktifan, pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai from person_pegawai ppg inner join person_person pper on pper.idperson = ppg.idperson inner join person_grup_pegawai pgp on pgp.idgruppegawai = ppg.idgruppegawai inner join sdmk_berkaspegawai sbp on ppg.idpegawai = sbp.id_pegawai left join person_grup_profesi pgp2 on pgp2.idprofesi = ppg.idprofesi")->result_array();
        $data = ["datas" => $query];
        $this->load->view('sdmk/v_tabelberkaspegawai', $data);
    }
    
     /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function tambahberkas()
    {
        $data =  [
            'active_menu'              => 'sdmk',
            'active_sub_menu'          => 'berkas',
            'active_menu_level'        => 'Berkas Pegawai', 
            'content_view'             => 'sdmk/v_berkaspegawai',
            'mode'                     => 'tambahberkas',
            'datapegawai'              => $this->db->query("select ppg.bpjs_kodedokter, ppg.idpegawai,ppg.nip,ppg.sip, ppg.statuskeaktifan,pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai from person_pegawai ppg inner join person_person pper on pper.idperson = ppg.idperson inner join person_grup_pegawai pgp on pgp.idgruppegawai = ppg.idgruppegawai left join person_grup_profesi pgp2 on pgp2.idprofesi = ppg.idprofesi where ppg.is_upload_berkas = 0")->result_array(),
            'script_js'                => ['sdmk/js_datapegawai','sdmk/js_berkaspegawai'],
            'title_page'               => 'Berkas Pegawai',
            // 'periode'               => $this->db->get('keu_periode')->row_array(),
           'plugins'                   => [PLUG_DROPDOWN, PLUG_CHECKBOX]
        ];

        // print_r($data);
        $this->load->view('v_index',$data);
    }

     /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function save_berkas()
    {
        $post = $this->input->post();
        $idpegawai = (int) $this->security->sanitize_filename($post['idpegawai']);

        if($idpegawai == 0)
        {
            $response = 
            [
                "result" => "error",
                "msg" => "Anda Belum Memilih Pegawai"
            ];
            
            echo json_encode($response);
        }
        else{
            $querygetdatasebelumnya = $this->db->query("SELECT * FROM sdmk_berkaspegawai WHERE 	id_pegawai = ".$idpegawai)->result_array();
            
            if(count($querygetdatasebelumnya) < 0 || count($querygetdatasebelumnya) == 0 )
            {
                #Insert Ke tabel sdmk_berkaspegawai
                $data = 
                [
                    "id_pegawai" => $post['idpegawai'],
                ];
                $queryidpegawai = $this->db->insert('sdmk_berkaspegawai',$data);

                #Update Status berkas
                $data = [
                    "is_upload_berkas" => 1
                ];
                $queryupdatepersonpegawai = $this->db->where('idpegawai',$idpegawai)->update('person_pegawai',$data);
            }


            $paths = FOLDERNASSIMRS.'/berkas_pegawai/';
            
            # Count each of uploaded file
            $count_uploaded_riwayat = empty($_FILES['riwayat']['name']) ? 0 : count($_FILES['riwayat']['name']);
            $count_uploaded_ijazahterlegalisir = empty($_FILES['ijazahterlegalisir']['name']) ? 0 : count($_FILES['ijazahterlegalisir']['name']);
            $count_uploaded_str = empty($_FILES['str']['name']) ? 0 : count($_FILES['str']['name']);
            $count_uploaded_siksip = empty($_FILES['siksip']['name']) ? 0 : count($_FILES['siksip']['name']);
            $count_uploaded_buktiverifikasiijazah = empty($_FILES['buktiverifikasiijazah']['name']) ? 0 : count($_FILES['buktiverifikasiijazah']['name']);
            $count_uploaded_skskspksk = empty($_FILES['skskspksk']['name']) ? 0 : count($_FILES['skskspksk']['name']);
            $count_uploaded_uraiantugas = empty($_FILES['uraiantugas']['name']) ? 0 : count($_FILES['uraiantugas']['name']);
            $count_uploaded_rkkspkk = empty($_FILES['rkkspkk']['name']) ? 0 : count($_FILES['rkkspkk']['name']);
            $count_uploaded_buktipenilaiankinerja = empty($_FILES['buktipenilaiankinerja']['name']) ? 0 : count($_FILES['buktipenilaiankinerja']['name']);
            $count_uploaded_datapelatihan = empty($_FILES['datapelatihan']['name']) ? 0 : count($_FILES['datapelatihan']['name']);
            $count_uploaded_buktilamarankerja = empty($_FILES['buktilamarankerja']['name']) ? 0 : count($_FILES['buktilamarankerja']['name']);
            $count_uploaded_buktiorientasi = empty($_FILES['buktiorientasi']['name']) ? 0 : count($_FILES['buktiorientasi']['name']);
            $count_uploaded_sertifikatvaksinasi = empty($_FILES['sertifikatvaksin']['name']) ? 0 : count($_FILES['sertifikatvaksin']['name']);
            $files = $_FILES;
            
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
            
            $namesriwayat = [];
            #Upload riwayat ke NAS
            if($count_uploaded_riwayat != 0)
            {
                for($i = 0; $i < $count_uploaded_riwayat; $i++)
                {
                    $_FILES['riwayat'] = [
                        'name'     => $files['riwayat']['name'][$i],
                        'type'     => $files['riwayat']['type'][$i],
                        'tmp_name' => $files['riwayat']['tmp_name'][$i],
                        'error'    => $files['riwayat']['error'][$i],
                        'size'     => $files['riwayat']['size'][$i]
                    ];

                    if( $files['riwayat']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['riwayat']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['riwayat']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/riwayat/'.$fileName;

                        $namesriwayat[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                    }   
                }

        
                $merge_names_riwayat    = array_merge($namesriwayat);

                $data_riwayat   = [
                    'riwayat'  => serialize($merge_names_riwayat)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data_riwayat);            
            }


            $namesijazahterlegalisir = [];
            #Upload Ijazah Terlegalisir Ke NAS
            if($count_uploaded_ijazahterlegalisir != 0)
            {
                for($i = 0; $i < $count_uploaded_ijazahterlegalisir; $i++)
                {
                    $_FILES['ijazahterlegalisir'] = [
                        'name'     => $files['ijazahterlegalisir']['name'][$i],
                        'type'     => $files['ijazahterlegalisir']['type'][$i],
                        'tmp_name' => $files['ijazahterlegalisir']['tmp_name'][$i],
                        'error'    => $files['ijazahterlegalisir']['error'][$i],
                        'size'     => $files['ijazahterlegalisir']['size'][$i]
                    ];

                    if( $files['ijazahterlegalisir']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['ijazahterlegalisir']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['ijazahterlegalisir']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/ijazahterlegalisir/'.$fileName;

                        $namesijazahterlegalisir[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['ijazahterlegalisir']; //array
            
                $merge_names_ijazahterlegalisir    = array_merge($namesijazahterlegalisir);

                $data_ijazahterlegalisir= [
                    'ijazahterlegalisir'  => serialize($merge_names_ijazahterlegalisir)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data_ijazahterlegalisir);            
            }
        
            $namesstr = [];
            #Upload STR Ke NAS
            if($count_uploaded_str != 0)
            {
                for($i = 0; $i < $count_uploaded_str; $i++)
                {
                    $_FILES['str'] = [
                        'name'     => $files['str']['name'][$i],
                        'type'     => $files['str']['type'][$i],
                        'tmp_name' => $files['str']['tmp_name'][$i],
                        'error'    => $files['str']['error'][$i],
                        'size'     => $files['str']['size'][$i]
                    ];
    
                    if( $files['str']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['str']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['str']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/str/'.$fileName;
    
                        $namesstr[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['str']; //array
            
                $merge_names    = array_merge($namesstr);
    
                $data= [
                    'str'  => serialize($merge_names)
                ];
    
                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);       
            }

            
            $namessiksip = [];
            #Upload SIK/SIP ke NAS
            if($count_uploaded_siksip != 0)
            {
                for($i = 0; $i < $count_uploaded_siksip; $i++)
                {
                    $_FILES['siksip'] = [
                        'name'     => $files['siksip']['name'][$i],
                        'type'     => $files['siksip']['type'][$i],
                        'tmp_name' => $files['siksip']['tmp_name'][$i],
                        'error'    => $files['siksip']['error'][$i],
                        'size'     => $files['siksip']['size'][$i]
                    ];

                    if( $files['siksip']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['siksip']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['siksip']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/siksip/'.$fileName;

                        $namessiksip[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['siksip']; //array
            
                $merge_names    = array_merge($namessiksip);

                $data= [
                    'siksip'  => serialize($merge_names)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }

            $namesbuktiverifikasiijazah = [];
            #Upload Bukti Verikasi Ijazah ke NAS
            if($count_uploaded_buktiverifikasiijazah != 0)
            {
                for($i = 0; $i < $count_uploaded_buktiverifikasiijazah; $i++)
                {
                    $_FILES['buktiverifikasiijazah'] = [
                        'name'     => $files['buktiverifikasiijazah']['name'][$i],
                        'type'     => $files['buktiverifikasiijazah']['type'][$i],
                        'tmp_name' => $files['buktiverifikasiijazah']['tmp_name'][$i],
                        'error'    => $files['buktiverifikasiijazah']['error'][$i],
                        'size'     => $files['buktiverifikasiijazah']['size'][$i]
                    ];

                        if( $files['buktiverifikasiijazah']['type'][$i] == 'application/pdf'){
                            $tmpName        = $files['buktiverifikasiijazah']['tmp_name'][$i]; 
                            $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['buktiverifikasiijazah']['name'][$i]));
                            $newFileName    = '/berkas_pegawai/buktiverifikasiijazah/'.$fileName;
        
                            $namesbuktiverifikasiijazah[]        = $fileName;  
                                
                            $config = $this->ftp_upload();
                            
                            # upload ke nas storage
                            $upload = $this->ftp->connect($config);
                            $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                            $upload = $this->ftp->close();
                            

                        }   
                    }

                    $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                    $row_filename   = $getrow['buktiverifijazah']; //array
                
                    $merge_names    = array_merge($namesbuktiverifikasiijazah);
        
                    $data= [
                        'buktiverifijazah'  => serialize($merge_names)
                    ];
        
                    $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
                }

                $namesbuktiverisfikasiskskspksk = [];
                #Upload Bukti Verikasi skskspksk ke NAS
                    if($count_uploaded_skskspksk != 0)
                    {
                        for($i = 0; $i < $count_uploaded_skskspksk; $i++)
                        {
                            $_FILES['skskspksk'] = 
                            [
                                'name'     => $files['skskspksk']['name'][$i],
                                'type'     => $files['skskspksk']['type'][$i],
                                'tmp_name' => $files['skskspksk']['tmp_name'][$i],
                                'error'    => $files['skskspksk']['error'][$i],
                                'size'     => $files['skskspksk']['size'][$i]
                            ];
            
                            if( $files['skskspksk']['type'][$i] == 'application/pdf')
                            {
                                $tmpName        = $files['skskspksk']['tmp_name'][$i]; 
                                $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['skskspksk']['name'][$i]));
                                $newFileName    = '/berkas_pegawai/sksuratkeputusan/'.$fileName;
            
                                $namesbuktiverisfikasiskskspksk[]        = $fileName;  
                                    
                                $config = $this->ftp_upload();
                                
                                # upload ke nas storage
                                $upload = $this->ftp->connect($config);
                                $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                                $upload = $this->ftp->close();
                                
        
                            }   
                        }

                        $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                        $row_filename   = $getrow['suratkeputusansuratperjanjian']; //array
                    
                        $merge_names    = array_merge($namesbuktiverisfikasiskskspksk);
            
                        $data= [
                            'suratkeputusansuratperjanjian'  => serialize( $merge_names)
                        ];
            
                        $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   

                    }
                
            $uraiantugas = [];
            #Upload Uraian Tugas ke NAS
            if($count_uploaded_uraiantugas != 0)
            {
                for($i = 0; $i < $count_uploaded_uraiantugas; $i++)
                {
                    $_FILES['uraiantugas'] = [
                        'name'     => $files['uraiantugas']['name'][$i],
                        'type'     => $files['uraiantugas']['type'][$i],
                        'tmp_name' => $files['uraiantugas']['tmp_name'][$i],
                        'error'    => $files['uraiantugas']['error'][$i],
                        'size'     => $files['uraiantugas']['size'][$i]
                    ];

                    if( $files['uraiantugas']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['uraiantugas']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['uraiantugas']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/uraiantugas/'.$fileName;

                        $uraiantugas[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['uraiantugas']; //array
            
                $merge_names    = array_merge($uraiantugas);

                $data= [
                    'uraiantugas'  => serialize($merge_names)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   

            }

            $uraianrkspkk = [];
            # Upload Uraian RKSPKK ke NAS
            if($count_uploaded_rkkspkk != 0)
            {
                for($i = 0; $i < $count_uploaded_rkkspkk; $i++)
                {
                    $_FILES['rkkspkk'] = [
                        'name'     => $files['rkkspkk']['name'][$i],
                        'type'     => $files['rkkspkk']['type'][$i],
                        'tmp_name' => $files['rkkspkk']['tmp_name'][$i],
                        'error'    => $files['rkkspkk']['error'][$i],
                        'size'     => $files['rkkspkk']['size'][$i]
                    ];

                    if( $files['rkkspkk']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['rkkspkk']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['rkkspkk']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/rkkspkk/'.$fileName;

                        $uraianrkspkk[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['rkkspkk']; //array
            
                $merge_names    = array_merge($uraianrkspkk);

                $data= [
                    'rkkspkk'  => serialize( $merge_names)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   

            }

       
            $buktipenilainkinerja = [];
            # Upload Uraian Bukti Penilian Kinerja ke NAS
            if($count_uploaded_buktipenilaiankinerja != 0)
            {
                for($i = 0; $i < $count_uploaded_buktipenilaiankinerja; $i++)
                {
                    $_FILES['buktipenilaiankinerja'] = [
                        'name'     => $files['buktipenilaiankinerja']['name'][$i],
                        'type'     => $files['buktipenilaiankinerja']['type'][$i],
                        'tmp_name' => $files['buktipenilaiankinerja']['tmp_name'][$i],
                        'error'    => $files['buktipenilaiankinerja']['error'][$i],
                        'size'     => $files['buktipenilaiankinerja']['size'][$i]
                    ];

                    if( $files['buktipenilaiankinerja']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['buktipenilaiankinerja']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['buktipenilaiankinerja']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktipenilaiankinerja/'.$fileName;

                        $buktipenilainkinerja[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['buktipenilaiankinerja']; //array
            
                $merge_names    = array_merge($buktipenilainkinerja);

                $data= [
                    'buktipenilaiankinerja'  => serialize( $merge_names)
                ];

                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }

            $datapelatihan = [];
            # Upload Uraian Data Pelatihan  ke NAS
            if($count_uploaded_datapelatihan != 0)
            {
                for($i = 0; $i < $count_uploaded_datapelatihan; $i++)
                {
                    $_FILES['datapelatihan'] = [
                        'name'     => $files['datapelatihan']['name'][$i],
                        'type'     => $files['datapelatihan']['type'][$i],
                        'tmp_name' => $files['datapelatihan']['tmp_name'][$i],
                        'error'    => $files['datapelatihan']['error'][$i],
                        'size'     => $files['datapelatihan']['size'][$i]
                    ];
    
                    if( $files['datapelatihan']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['datapelatihan']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['datapelatihan']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/datapelatihan/'.$fileName;
    
                        $datapelatihan[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['datapelatihan']; //array
            
                $merge_names    = array_merge($datapelatihan);
    
                $data= [
                    'datapelatihan'  => serialize( $merge_names)
                ];
    
                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }

            # Upload Uraian Bukti Lamaran Kerja ke NAS
            $namesbuktilamarankerja = [];
            if($count_uploaded_buktilamarankerja != 0)
            {
                for($i = 0; $i < $count_uploaded_buktilamarankerja; $i++)
                {
                    $_FILES['buktilamarankerja'] = [
                        'name'     => $files['buktilamarankerja']['name'][$i],
                        'type'     => $files['buktilamarankerja']['type'][$i],
                        'tmp_name' => $files['buktilamarankerja']['tmp_name'][$i],
                        'error'    => $files['buktilamarankerja']['error'][$i],
                        'size'     => $files['buktilamarankerja']['size'][$i]
                    ];
    
                    if( $files['buktilamarankerja']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['buktilamarankerja']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['buktilamarankerja']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktilamarankerja/'.$fileName;
    
                        $namesbuktilamarankerja[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['buktilamarankerja']; //array
                
                $merge_names    = array_merge($namesbuktilamarankerja);
                
                $data= [
                    'buktilamarankerja'  => serialize($merge_names)
                ];            
                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }

            #Upload Bukti Orientasi Kerja Ke Nas
            $namesbuktiorientasikerja = [];
            if($count_uploaded_buktiorientasi != 0)
            {
                for($i = 0; $i < $count_uploaded_buktiorientasi; $i++)
                {
                    $_FILES['buktiorientasi'] = [
                        'name'     => $files['buktiorientasi']['name'][$i],
                        'type'     => $files['buktiorientasi']['type'][$i],
                        'tmp_name' => $files['buktiorientasi']['tmp_name'][$i],
                        'error'    => $files['buktiorientasi']['error'][$i],
                        'size'     => $files['buktiorientasi']['size'][$i]
                    ];
    
                    if( $files['buktiorientasi']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['buktiorientasi']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['buktiorientasi']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktiorientasi/'.$fileName;
    
                        $namesbuktiorientasikerja[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['buktiorientasi']; //array
                
                $merge_names    = array_merge($namesbuktiorientasikerja);
                
                $data= [
                    'buktiorientasi'  => serialize($merge_names)
                ];            
                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }

            #Upload Bukti Orientasi Kerja Ke Nas
            $namessertifikatvaksin = [];
            if($count_uploaded_sertifikatvaksinasi != 0)
            {
                for($i = 0; $i < $count_uploaded_sertifikatvaksinasi; $i++)
                {
                    $_FILES['sertifikatvaksin'] = [
                        'name'     => $files['sertifikatvaksin']['name'][$i],
                        'type'     => $files['sertifikatvaksin']['type'][$i],
                        'tmp_name' => $files['sertifikatvaksin']['tmp_name'][$i],
                        'error'    => $files['sertifikatvaksin']['error'][$i],
                        'size'     => $files['sertifikatvaksin']['size'][$i]
                    ];
    
                    if( $files['sertifikatvaksin']['type'][$i] == 'application/pdf'){
                        $tmpName        = $files['sertifikatvaksin']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['sertifikatvaksin']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/sertifikatvaksin/'.$fileName;
    
                        $namessertifikatvaksin[]        = $fileName;  
                            
                        $config = $this->ftp_upload();
                        
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
                        
                    }   
                }

                $getrow = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai]);
                $row_filename   = $getrow['sertifikatvaksin']; //array
                
                $merge_names    = array_merge($namessertifikatvaksin);
                
                $data= [
                    'sertifikatvaksin'  => serialize($merge_names)
                ];            
                $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_pegawai'=>$idpegawai],$data);   
            }
        
            $response = [
                "result" => "Sukses",
                "msg" => "Upload Data Sukses",
                'url'=>base_url().'csdmk/berkas'
            ];
            
            echo json_encode($response);
        }


    }
  
    
     /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function updateberkas()
    {
        $post = $this->input->post();
        $idberkas = $_POST['idberkas'];

        #Count each of uploaded file
        $count_uploaded_riwayat = empty($_FILES['riwayat']['name']) ? 0 : count($_FILES['riwayat']['name']);
        $count_uploaded_ijazahterlegalisir = empty($_FILES['ijazahterlegalisir']['name']) ? 0 : count($_FILES['ijazahterlegalisir']['name']);
        $count_uploaded_str = empty($_FILES['str']['name']) ? 0 : count($_FILES['str']['name']);
        $count_uploaded_siksip = empty($_FILES['siksip']['name']) ? 0 : count($_FILES['siksip']['name']);
        $count_uploaded_verifikasiijazah = empty($_FILES['verifikasiijazah']['name']) ? 0 : count($_FILES['verifikasiijazah']['name']);
        $count_uploaded_siksuratkeputusan = empty($_FILES['siksuratkeputusan']['name']) ? 0 : count($_FILES['siksuratkeputusan']['name']);
        $count_uploaded_uraiantugasstaff = empty($_FILES['inputuraiantugasstaff']['name']) ? 0 : count($_FILES['inputuraiantugasstaff']['name']);
        $count_uploaded_rkkspkk = empty($_FILES['inpurkkspkk']['name']) ? 0 : count($_FILES['inpurkkspkk']['name']);
        $count_uploaded_buktipenilaiankinerja = empty($_FILES['inputtambahrkksp']['name']) ? 0 : count($_FILES['inputtambahrkksp']['name']);
        $count_uploaded_datapelatihan = empty($_FILES['inputdatapelatihan']['name']) ? 0 : count($_FILES['inputdatapelatihan']['name']);
        $count_uploaded_buktilamarankerja =  empty($_FILES['inputbuktilamarankerja']['name']) ? 0 : count($_FILES['inputbuktilamarankerja']['name']);
        $count_uploaded_buktiorientasi = empty($_FILES['inputambahbuktiorientasikerja']['name']) ? 0 : count($_FILES['inputambahbuktiorientasikerja']['name']);
        $count_uploaded_sertifikatvaksinasi = empty($_FILES['sertifikatvaksin']['name']) ? 0 : count($_FILES['sertifikatvaksin']['name']);
        $files = $_FILES;
    
        $namesinputijazahterlegalisir = [];
        
        if($count_uploaded_riwayat > 0)
        {
            for($i=0; $i<$count_uploaded_riwayat; $i++)
            {
                    $_FILES['riwayat'] = [
                        'name'     => $files['riwayat']['name'][$i],
                        'type'     => $files['riwayat']['type'][$i],
                        'tmp_name' => $files['riwayat']['tmp_name'][$i],
                        'error'    => $files['riwayat']['error'][$i],
                        'size'     => $files['riwayat']['size'][$i]
                    ];

                    if($files['riwayat']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['riwayat']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['riwayat']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/riwayat/'.$fileName;
        
                        $namesriwayat[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }

            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['riwayat'])) ? 0 : unserialize($getrow_element['riwayat']);
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['riwayat']);
                $merge_names    = array_merge($row_filename, $namesriwayat);
            }
            else
            {
                $merge_names    = array_merge($namesriwayat);
            }
 
            $data= [
                'riwayat'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }



        $namesijazahterlegalisir = [];
        if($count_uploaded_ijazahterlegalisir > 0)
        {
            for($i=0; $i < $count_uploaded_ijazahterlegalisir; $i++)
            {
                    $_FILES['ijazahterlegalisir'] = [
                        'name'     => $files['ijazahterlegalisir']['name'][$i],
                        'type'     => $files['ijazahterlegalisir']['type'][$i],
                        'tmp_name' => $files['ijazahterlegalisir']['tmp_name'][$i],
                        'error'    => $files['ijazahterlegalisir']['error'][$i],
                        'size'     => $files['ijazahterlegalisir']['size'][$i]
                    ];

                    if($files['ijazahterlegalisir']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['ijazahterlegalisir']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['ijazahterlegalisir']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/ijazahterlegalisir/'.$fileName;
        
                        $namesijazahterlegalisir[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);


            $countelement = empty(unserialize($getrow_element['ijazahterlegalisir'])) ? 0 : unserialize($getrow_element['ijazahterlegalisir']);
           
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['ijazahterlegalisir']);
                $merge_names    = array_merge($row_filename, $namesijazahterlegalisir);
            }
            else
            {
                $merge_names    = array_merge($namesijazahterlegalisir);
            }
 
            $data= [
                'ijazahterlegalisir'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesstr = [];
        if($count_uploaded_str > 0)
        {
            for($i=0; $i < $count_uploaded_str; $i++)
            {
                    $_FILES['str'] = [
                        'name'     => $files['str']['name'][$i],
                        'type'     => $files['str']['type'][$i],
                        'tmp_name' => $files['str']['tmp_name'][$i],
                        'error'    => $files['str']['error'][$i],
                        'size'     => $files['str']['size'][$i]
                    ];

                    if($files['str']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['str']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['str']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/str/'.$fileName;
        
                        $namesstr[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);


            $countelement = empty(unserialize($getrow_element['str'])) ? 0 : unserialize($getrow_element['str']);
          
            $merge_names = '';  
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['str']);
                $merge_names    = array_merge($row_filename, $namesstr);
            }
            else
            {
                $merge_names    = array_merge($namesstr);
            }
 
            $data= [
                'str'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namessiksip = [];
        if($count_uploaded_siksip > 0)
        {
            for($i=0; $i < $count_uploaded_siksip; $i++)
            {
                    $_FILES['siksip'] = [
                        'name'     => $files['siksip']['name'][$i],
                        'type'     => $files['siksip']['type'][$i],
                        'tmp_name' => $files['siksip']['tmp_name'][$i],
                        'error'    => $files['siksip']['error'][$i],
                        'size'     => $files['siksip']['size'][$i]
                    ];

                    if($files['siksip']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['siksip']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['siksip']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/siksip/'.$fileName;
        
                        $namessiksip[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            
            $countelement = empty(unserialize($getrow_element['siksip'])) ? 0 : unserialize($getrow_element['siksip']);
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['siksip']);
                $merge_names    = array_merge($row_filename, $namessiksip);
            }
            else
            {
                $merge_names    = array_merge($namessiksip);
            }
 
            $data= [
                'siksip'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesbuktiverikasiijazah = [];
        if($count_uploaded_verifikasiijazah > 0)
        {
            for($i=0; $i < $count_uploaded_verifikasiijazah; $i++)
            {
                    $_FILES['verifikasiijazah'] = [
                        'name'     => $files['verifikasiijazah']['name'][$i],
                        'type'     => $files['verifikasiijazah']['type'][$i],
                        'tmp_name' => $files['verifikasiijazah']['tmp_name'][$i],
                        'error'    => $files['verifikasiijazah']['error'][$i],
                        'size'     => $files['verifikasiijazah']['size'][$i]
                    ];

                    if($files['verifikasiijazah']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['verifikasiijazah']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['verifikasiijazah']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktiverifikasiijazah/'.$fileName;
        
                        $namesbuktiverikasiijazah[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            
            $countelement = empty(unserialize($getrow_element['buktiverifijazah'])) ? 0 : unserialize($getrow_element['buktiverifijazah']);
        
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['buktiverifijazah']);
                $merge_names    = array_merge($row_filename, $namesbuktiverikasiijazah);
            }
            else
            {
                $merge_names    = array_merge($namesbuktiverikasiijazah);
            }
 
            $data= [
                'buktiverifijazah'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namessiksuratkeputusan = [];
        if($count_uploaded_siksuratkeputusan > 0)
        {
            for($i=0; $i < $count_uploaded_siksuratkeputusan; $i++)
            {
                    $_FILES['siksuratkeputusan'] = [
                        'name'     => $files['siksuratkeputusan']['name'][$i],
                        'type'     => $files['siksuratkeputusan']['type'][$i],
                        'tmp_name' => $files['siksuratkeputusan']['tmp_name'][$i],
                        'error'    => $files['siksuratkeputusan']['error'][$i],
                        'size'     => $files['siksuratkeputusan']['size'][$i]
                    ];

                    if($files['siksuratkeputusan']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['siksuratkeputusan']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['siksuratkeputusan']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/sksuratkeputusan/'.$fileName;
        
                        $namessiksuratkeputusan[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['suratkeputusansuratperjanjian'])) ? 0 : unserialize($getrow_element['suratkeputusansuratperjanjian']);
            
            $merge_names = '';
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['suratkeputusansuratperjanjian']);
                $merge_names    = array_merge($row_filename, $namessiksuratkeputusan);
            }
            else
            {
                $merge_names    = array_merge($namessiksuratkeputusan);
            }
 
            $data= [
                'suratkeputusansuratperjanjian'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $nameuraiantugas = [];
        if($count_uploaded_uraiantugasstaff > 0)
        {
            for($i=0; $i < $count_uploaded_uraiantugasstaff; $i++)
            {
                    $_FILES['inputuraiantugasstaff'] = [
                        'name'     => $files['inputuraiantugasstaff']['name'][$i],
                        'type'     => $files['inputuraiantugasstaff']['type'][$i],
                        'tmp_name' => $files['inputuraiantugasstaff']['tmp_name'][$i],
                        'error'    => $files['inputuraiantugasstaff']['error'][$i],
                        'size'     => $files['inputuraiantugasstaff']['size'][$i]
                    ];

                    if($files['inputuraiantugasstaff']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inputuraiantugasstaff']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inputuraiantugasstaff']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/uraiantugas/'.$fileName;
        
                        $nameuraiantugas[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['uraiantugas'])) ? 0 : unserialize($getrow_element['uraiantugas']);
      
            $merge_names = '';
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['uraiantugas']);
                $merge_names    = array_merge($row_filename, $nameuraiantugas);
            }
            else
            {
                $merge_names    = array_merge($nameuraiantugas);
            }
 
            $data= [
                'uraiantugas'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesrkkspkk = [];
        if($count_uploaded_rkkspkk > 0)
        {
            for($i=0; $i < $count_uploaded_rkkspkk; $i++)
            {
                    $_FILES['inpurkkspkk'] = [
                        'name'     => $files['inpurkkspkk']['name'][$i],
                        'type'     => $files['inpurkkspkk']['type'][$i],
                        'tmp_name' => $files['inpurkkspkk']['tmp_name'][$i],
                        'error'    => $files['inpurkkspkk']['error'][$i],
                        'size'     => $files['inpurkkspkk']['size'][$i]
                    ];

                    if($files['inpurkkspkk']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inpurkkspkk']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inpurkkspkk']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/rkkspkk/'.$fileName;
        
                        $namesrkkspkk[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['rkkspkk'])) ? 0 : unserialize($getrow_element['rkkspkk']);
      
           
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['rkkspkk']);
                $merge_names    = array_merge($row_filename, $namesrkkspkk);
            }
            else
            {
                $merge_names    = array_merge($namesrkkspkk);
            }
 
            $data= [
                'rkkspkk'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesbuktipenilaiankinerja = [];
        if($count_uploaded_buktipenilaiankinerja > 0)
        {
            for($i=0; $i < $count_uploaded_buktipenilaiankinerja; $i++)
            {
                    $_FILES['inputtambahrkksp'] = [
                        'name'     => $files['inputtambahrkksp']['name'][$i],
                        'type'     => $files['inputtambahrkksp']['type'][$i],
                        'tmp_name' => $files['inputtambahrkksp']['tmp_name'][$i],
                        'error'    => $files['inputtambahrkksp']['error'][$i],
                        'size'     => $files['inputtambahrkksp']['size'][$i]
                    ];

                    if($files['inputtambahrkksp']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inputtambahrkksp']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inputtambahrkksp']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktipenilaiankinerja/'.$fileName;
        
                        $namesbuktipenilaiankinerja[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['buktipenilaiankinerja'])) ? 0 : unserialize($getrow_element['buktipenilaiankinerja']);
          
            
            $merge_names = '';
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['buktipenilaiankinerja']);
                $merge_names    = array_merge($row_filename, $namesbuktipenilaiankinerja);
            }
            else
            {
                $merge_names    = array_merge($namesbuktipenilaiankinerja);
            }
 
            $data= [
                'buktipenilaiankinerja'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesdatapelatihan = [];
        if($count_uploaded_datapelatihan > 0)
        {
            for($i=0; $i < $count_uploaded_datapelatihan; $i++)
            {
                    $_FILES['inputdatapelatihan'] = [
                        'name'     => $files['inputdatapelatihan']['name'][$i],
                        'type'     => $files['inputdatapelatihan']['type'][$i],
                        'tmp_name' => $files['inputdatapelatihan']['tmp_name'][$i],
                        'error'    => $files['inputdatapelatihan']['error'][$i],
                        'size'     => $files['inputdatapelatihan']['size'][$i]
                    ];

                    if($files['inputdatapelatihan']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inputdatapelatihan']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inputdatapelatihan']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/datapelatihan/'.$fileName;
        
                        $namesdatapelatihan[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['datapelatihan'])) ? 0 : unserialize($getrow_element['datapelatihan']);
          
            
            $merge_names = '';  
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['datapelatihan']);
                $merge_names    = array_merge($row_filename, $namesdatapelatihan);
            }
            else
            {
                $merge_names    = array_merge($namesdatapelatihan);
            }
 
            $data= [
                'datapelatihan'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        $namesbuktilamarankerja = [];
        if($count_uploaded_buktilamarankerja > 0)
        {
            for($i=0; $i < $count_uploaded_buktilamarankerja; $i++)
            {
                    $_FILES['inputbuktilamarankerja'] = [
                        'name'     => $files['inputbuktilamarankerja']['name'][$i],
                        'type'     => $files['inputbuktilamarankerja']['type'][$i],
                        'tmp_name' => $files['inputbuktilamarankerja']['tmp_name'][$i],
                        'error'    => $files['inputbuktilamarankerja']['error'][$i],
                        'size'     => $files['inputbuktilamarankerja']['size'][$i]
                    ];

                    if($files['inputbuktilamarankerja']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inputbuktilamarankerja']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inputbuktilamarankerja']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktilamarankerja/'.$fileName;
        
                        $namesbuktilamarankerja[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['buktilamarankerja'])) ? 0 : unserialize($getrow_element['buktilamarankerja']);
         
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['buktilamarankerja']);
                $merge_names    = array_merge($row_filename, $namesbuktilamarankerja);
            }
            else
            {
                $merge_names    = array_merge($namesbuktilamarankerja);
            }
 
            $data= [
                'buktilamarankerja'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }
        //End OF Bukti Lamaran Kerja

        $namesbuktiorientasi = [];
        if($count_uploaded_buktiorientasi > 0)
        {
            for($i=0; $i < $count_uploaded_buktiorientasi; $i++)
            {
                    $_FILES['inputambahbuktiorientasikerja'] = [
                        'name'     => $files['inputambahbuktiorientasikerja']['name'][$i],
                        'type'     => $files['inputambahbuktiorientasikerja']['type'][$i],
                        'tmp_name' => $files['inputambahbuktiorientasikerja']['tmp_name'][$i],
                        'error'    => $files['inputambahbuktiorientasikerja']['error'][$i],
                        'size'     => $files['inputambahbuktiorientasikerja']['size'][$i]
                    ];

                    if($files['inputambahbuktiorientasikerja']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['inputambahbuktiorientasikerja']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['inputambahbuktiorientasikerja']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/buktiorientasi/'.$fileName;
        
                        $namesbuktiorientasi[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['buktiorientasi'])) ? 0 : unserialize($getrow_element['buktiorientasi']);
         
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['buktiorientasi']);
                $merge_names    = array_merge($row_filename, $namesbuktiorientasi);
            }
            else
            {
                $merge_names    = array_merge($namesbuktiorientasi);
            }
 
            $data= [
                'buktiorientasi'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }
        
        
        $namessertifikatvaksin = [];
        if($count_uploaded_sertifikatvaksinasi > 0)
        {
            for($i=0; $i < $count_uploaded_sertifikatvaksinasi; $i++)
            {
                    $_FILES['sertifikatvaksin'] = [
                        'name'     => $files['sertifikatvaksin']['name'][$i],
                        'type'     => $files['sertifikatvaksin']['type'][$i],
                        'tmp_name' => $files['sertifikatvaksin']['tmp_name'][$i],
                        'error'    => $files['sertifikatvaksin']['error'][$i],
                        'size'     => $files['sertifikatvaksin']['size'][$i]
                    ];

                    if($files['sertifikatvaksin']['type'][$i] == "application/pdf")
                    {
                        $tmpName        = $files['sertifikatvaksin']['tmp_name'][$i]; 
                        $fileName       = date('D').rand(1000,9999).'-'.str_replace('"', '', str_replace("'", '', $files['sertifikatvaksin']['name'][$i]));
                        $newFileName    = '/berkas_pegawai/sertifikatvaksin/'.$fileName;
        
                        $namessertifikatvaksin[]        = $fileName;  
                                
                        $config = $this->ftp_upload();
                            
                        # upload ke nas storage
                        $upload = $this->ftp->connect($config);
                        $upload = $this->ftp->upload($tmpName, FOLDERNASSIMRS.$newFileName, 'ascii', 0775);
                        $upload = $this->ftp->close();
            }
        }
                
            $getrow_element = $this->msqlbasic->row_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas]);
            $countelement = empty(unserialize($getrow_element['sertifikatvaksin'])) ? 0 : unserialize($getrow_element['sertifikatvaksin']);
         
            $merge_names = '';
            
            if($countelement != 0)
            {
                $row_filename   = unserialize($getrow_element['sertifikatvaksin']);
                $merge_names    = array_merge($row_filename, $namessertifikatvaksin);
            }
            else
            {
                $merge_names    = array_merge($namessertifikatvaksin);
            }
 
            $data= [
                'sertifikatvaksin'  => serialize($merge_names)
            ];            

            $query = $this->msqlbasic->update_db('sdmk_berkaspegawai',['id_berkas'=>$idberkas],$data);   
        }

        
        

        $response = [
            "result" => "Sukses",
            "msg" => "Upload Data Sukses",
            'url'=>base_url().'csdmk/berkas'
        ];
        
        echo json_encode($response);

    }

     /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function hapusSeluruhBerkas()
    {
        $idberkas = $_POST['idberkas'];

        $query = $this->db->query("SELECT * FROM sdmk_berkaspegawai where id_berkas =".$idberkas)->result_array();

   

        foreach($query as $row)
        {
            $riwayat = empty(unserialize($row['riwayat'])) ? 0 : unserialize($row['riwayat']);
            $ijazahterlegalisir = empty(unserialize($row['ijazahterlegalisir'])) ? 0 : unserialize($row['ijazahterlegalisir']);
            $str = empty(unserialize($row['str'])) ? 0 : unserialize($row['str']);
            $siksip = empty(unserialize($row['siksip'])) ? 0 : unserialize($row['siksip']);
            $buktiverifijazah = empty(unserialize($row['buktiverifijazah'])) ? 0 : unserialize($row['buktiverifijazah']);
            $suratkeputusansuratperjanjian = empty(unserialize($row['suratkeputusansuratperjanjian'])) ? 0 : unserialize($row['suratkeputusansuratperjanjian']);    
            $uraiantugas = empty(unserialize($row['uraiantugas'])) ? 0 : unserialize($row['uraiantugas']);  
            $rkkspkk = empty(unserialize($row['rkkspkk'])) ? 0 : unserialize($row['rkkspkk']);
            $buktipenilaiankinerja = empty(unserialize($row['buktipenilaiankinerja'])) ? 0 : unserialize($row['buktipenilaiankinerja']);
            $datapelatihan = empty(unserialize($row['datapelatihan'])) ? 0 : unserialize($row['datapelatihan']);
            $buktilamarankerja = empty(unserialize($row['buktilamarankerja'])) ? 0 : unserialize($row['buktilamarankerja']);
            $buktiorientasi = empty(unserialize($row['buktiorientasi'])) ? 0 : unserialize($row['buktiorientasi']);
            $sertifikatvaksin = empty(unserialize($row['sertifikatvaksin'])) ? 0 : unserialize($row['sertifikatvaksin']);

            #Update Status Upload Di Tabel Person Pegawai
            $queryupdateppg = $this->db->query("UPDATE person_pegawai SET is_upload_berkas = 0 WHERE idpegawai =".$row['id_pegawai']);

            //Riwayat
            if($riwayat > 0)
            {
                foreach(unserialize($row['riwayat']) as $namariwayat)
                {
                    $pathname = "/sirstql/berkas_pegawai/riwayat/";
                    $urlnas = $pathname.$namariwayat;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Ijazah Terlegalisir
            if($ijazahterlegalisir > 0)
            {
                foreach(unserialize($row['ijazahterlegalisir']) as $namaijazahterlegalisir)
                {
                    $pathname = "/sirstql/berkas_pegawai/ijazahterlegalisir/";
                    $urlnas = $pathname.$namaijazahterlegalisir;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //STR
            if($str > 0)
            {
                foreach(unserialize($row['str']) as $namastr)
                {
                    $pathname = "/sirstql/berkas_pegawai/str/";
                    $urlnas = $pathname.$namastr;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //SIKSIP
            if($siksip > 0)
            {
                foreach(unserialize($row['siksip']) as $namasiksip)
                {
                    $pathname = "/sirstql/berkas_pegawai/siksip/";
                    $urlnas = $pathname.$namasiksip;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Bukti Verifikasi Ijazah
            if($buktiverifijazah > 0)
            {
                foreach(unserialize($row['buktiverifijazah']) as $namabuktiverifikasiijazah)
                {
                    $pathname = "/sirstql/berkas_pegawai/buktiverifikasiijazah/";
                    $urlnas = $pathname.$namabuktiverifikasiijazah;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Surat Keputusan Perjanjian
            if($suratkeputusansuratperjanjian > 0)
            {
                foreach(unserialize($row['suratkeputusansuratperjanjian']) as $namabuktisuratkeputusansuratperjanjian)
                {
                    $pathname = "/sirstql/berkas_pegawai/sksuratkeputusan/";
                    $urlnas = $pathname.$namabuktisuratkeputusansuratperjanjian;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Uraian Tugas
            if($uraiantugas > 0)
            {
                foreach(unserialize($row['uraiantugas']) as $namauraiantugas)
                {
                    $pathname = "/sirstql/berkas_pegawai/uraiantugas/";
                    $urlnas = $pathname.$namauraiantugas;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //RKKSPKK
            if($rkkspkk > 0)
            {
                foreach(unserialize($row['rkkspkk']) as $namarkkspkk)
                {
                    $pathname = "/sirstql/berkas_pegawai/rkkspkk/";
                    $urlnas = $pathname.$namarkkspkk;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Bukti Penilaian Kinerja
            if($buktipenilaiankinerja > 0)
            {
                foreach(unserialize($row['buktipenilaiankinerja']) as $namabuktipenilaiankinerja)
                {
                    $pathname = "/sirstql/berkas_pegawai/buktipenilaiankinerja/";
                    $urlnas = $pathname.$namabuktipenilaiankinerja;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Data Pelatihan
            if($datapelatihan > 0)
            {
                foreach(unserialize($row['datapelatihan']) as $namadatapelatihan)
                {
                    $pathname = "/sirstql/berkas_pegawai/datapelatihan/";
                    $urlnas = $pathname.$namadatapelatihan;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Bukti Lamaran Kerja
            if($buktilamarankerja > 0)
            {
                foreach(unserialize($row['buktilamarankerja']) as $namabuktilamarankerja)
                {
                    $pathname = "/sirstql/berkas_pegawai/buktilamarankerja/";
                    $urlnas = $pathname.$namabuktilamarankerja;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            //Sertifikat Vaksin
            if($buktilamarankerja > 0)
            {
                foreach(unserialize($row['buktilamarankerja']) as $namabuktilamarankerja)
                {
                    $pathname = "/sirstql/berkas_pegawai/buktilamarankerja/";
                    $urlnas = $pathname.$namabuktilamarankerja;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            if($buktiorientasi > 0)
            {
                foreach(unserialize($row['buktiorientasi']) as $namabuktiorientasi)
                {
                    $pathname = "/sirstql/berkas_pegawai/buktiorientasi/";
                    $urlnas = $pathname.$namabuktiorientasi;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }

            if($sertifikatvaksin > 0)
            {
                foreach(unserialize($row['sertifikatvaksin']) as $namasertifikatvaksin)
                {
                    $pathname = "/sirstql/berkas_pegawai/sertifikatvaksin/";
                    $urlnas = $pathname.$namasertifikatvaksin;

                    // hapus file di NAS
                    $config = $this->ftp_upload();
                    $delete = $this->ftp->connect( $config );
                    $delete = $this->ftp->delete_file( $urlnas );
                    $delete = $this->ftp->close();
                }
            }



        }

        $query = $this->db->query("DELETE FROM sdmk_berkaspegawai WHERE id_berkas =".$idberkas);

        echo json_encode(["msg" => "Data Berhasil Dihapus"]);
    }

     /**
     * Get Data Berkas Pegawai
     * Tito Otniel Simarmata
    */
    public function hapusBerkasPerItem()
    {
        $idberkas = $_POST['idberkas'];
        $modehapus = $_POST['modehapus'];
        $namaberkas = $_POST['namaberkas'];
     
        $query = $this->db->query("SELECT * FROM sdmk_berkaspegawai where id_berkas =".$idberkas)->row_array();
        
        if($modehapus == "riwayat")
        {
            $pathname = "/sirstql/berkas_pegawai/riwayat/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesriwayat = [];
            foreach(unserialize($query['riwayat']) as $namariwayat)
            {
                $namesriwayat[] = $namariwayat;
                $diffriwayat = array_diff($namesriwayat, array($namaberkas));
                $data = ["riwayat" => serialize($diffriwayat)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

    
        if($modehapus == "ijazahterlegalisir")
        {
            $pathname = "/sirstql/berkas_pegawai/ijazahterlegalisir/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesijazahterlegalisir = [];
            foreach(unserialize($query['ijazahterlegalisir']) as $namaijazahterlegalisir)
            {
                $namesijazahterlegalisir[] = $namaijazahterlegalisir;
                $diffijazahterlegalisir = array_diff($namesijazahterlegalisir, array($namaberkas));
                $data = ["ijazahterlegalisir" => serialize($diffijazahterlegalisir)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "str")
        {
            $pathname = "/sirstql/berkas_pegawai/str/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesstr = [];
            foreach(unserialize($query['str']) as $namastr)
            {
                $namesstr[] = $namastr;
                $diffstr = array_diff($namesstr, array($namaberkas));
                $data = ["str" => serialize($diffstr)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "siksip")
        {
            $pathname = "/sirstql/berkas_pegawai/siksip/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namessiksip = [];
            foreach(unserialize($query['siksip']) as $namasiksip)
            {
                $namessiksip[] = $namasiksip;
                $diffsiksip = array_diff($namessiksip, array($namaberkas));
                $data = ["siksip" => serialize($diffsiksip)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }
        
        if($modehapus == "buktiverifikasiijazah")
        {
            $pathname = "/sirstql/berkas_pegawai/buktiverifikasiijazah/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesbuktiverifikasiijazah = [];
            foreach(unserialize($query['buktiverifijazah']) as $namabuktiverifikasiijazah)
            {
                $namesbuktiverifikasiijazah[] = $namabuktiverifikasiijazah;
                $diffbuktiverifikasiijazah = array_diff($namesbuktiverifikasiijazah, array($namaberkas));
                $data = ["buktiverifijazah" => serialize($diffbuktiverifikasiijazah)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "suratkeputusansuratperjanjian")
        {
            $pathname = "/sirstql/berkas_pegawai/sksuratkeputusan/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namessuratkeputusansuratperjanjian = [];
            foreach(unserialize($query['suratkeputusansuratperjanjian']) as $namasuratkeputusansuratperjanjian)
            {
                $namessuratkeputusansuratperjanjian[] = $namasuratkeputusansuratperjanjian;
                $diffnamasuratkeputusansuratperjanjian = array_diff($namessuratkeputusansuratperjanjian, array($namaberkas));
                $data = ["suratkeputusansuratperjanjian" => serialize($diffnamasuratkeputusansuratperjanjian)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "uraiantugas")
        {
            $pathname = "/sirstql/berkas_pegawai/uraiantugas/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesuraintugas = [];
            foreach(unserialize($query['uraiantugas']) as $namauraiantugas)
            {
                $namesuraiantugas[] = $namauraiantugas;
                $diffuraiantugas = array_diff($namesuraiantugas, array($namaberkas));
                $data = ["uraiantugas" => serialize($diffuraiantugas)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "rkkspkk")
        {
            $pathname = "/sirstql/berkas_pegawai/rkkspkk/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesrkkspkk = [];
            foreach(unserialize($query['rkkspkk']) as $namarkkspkks)
            {
                $namesrkkspkk[] = $namarkkspkks;
                $diffnamarkkspkks = array_diff($namesrkkspkk, array($namaberkas));
                $data = ["rkkspkk" => serialize($diffnamarkkspkks)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "buktipenilaiankinerja")
        {
            $pathname = "/sirstql/berkas_pegawai/buktipenilaiankinerja/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesbuktipenilaiankinerja = [];
            foreach(unserialize($query['buktipenilaiankinerja']) as $namabuktipenilaiankinerja)
            {
                $namesbuktipenilaiankinerja[] = $namabuktipenilaiankinerja;
                $diffnamabuktipenilaiankinerja = array_diff($namesbuktipenilaiankinerja, array($namaberkas));
                $data = ["buktipenilaiankinerja" => serialize($diffnamabuktipenilaiankinerja)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        
        if($modehapus == "datapelatihan")
        {
            $pathname = "/sirstql/berkas_pegawai/datapelatihan/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesdatapelatihan = [];
            foreach(unserialize($query['datapelatihan']) as $namadatapelatihan)
            {
                $namesdatapelatihan[] = $namadatapelatihan;
                $diffdatapelatihan = array_diff($namesdatapelatihan, array($namaberkas));
                $data = ["datapelatihan" => serialize($diffdatapelatihan)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        
        if($modehapus == "buktilamarankerja")
        {
            $pathname = "/sirstql/berkas_pegawai/buktilamarankerja/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesbuktilamarankerja = [];
            foreach(unserialize($query['buktilamarankerja']) as $namabuktilamarankerja)
            {
                $namesbuktilamarankerja[] = $namabuktilamarankerja;
                $diffbuktilamarankerja = array_diff($namesbuktilamarankerja, array($namaberkas));
                $data = ["buktilamarankerja" => serialize($diffbuktilamarankerja)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "buktiorientasi")
        {
            $pathname = "/sirstql/berkas_pegawai/buktiorientasi/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namesbuktiorientasi = [];
            foreach(unserialize($query['buktiorientasi']) as $namabuktiorientasi)
            {
                $namesbuktiorientasi[] = $namabuktiorientasi;
                $diffbuktiorientasi = array_diff($namesbuktiorientasi, array($namaberkas));
                $data = ["buktiorientasi" => serialize($diffbuktiorientasi)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }

        if($modehapus == "sertifikatvaksin")
        {
            $pathname = "/sirstql/berkas_pegawai/sertifikatvaksin/";
            $urlnas = $pathname.$namaberkas;
            
            // hapus file di NAS
            $config = $this->ftp_upload();
            $delete = $this->ftp->connect( $config );
            $delete = $this->ftp->delete_file( $urlnas );
            $delete = $this->ftp->close();
            
            $namessertifikatvaksin = [];
            foreach(unserialize($query['sertifikatvaksin']) as $namasertifikatvaksin)
            {
                $namessertifikatvaksin[] = $namasertifikatvaksin;
                $diffsertifikatvaksin = array_diff($namessertifikatvaksin, array($namaberkas));
                $data = ["sertifikatvaksin" => serialize($diffsertifikatvaksin)];
                $updatedb = $this->db->where('id_berkas',$idberkas)->update('sdmk_berkaspegawai',$data);
            }
        }
    }

    public function tampilberkas()
    {
        $idberkas = $_POST['idberkas'];

        $query = $this->db->query("select sbp.sertifikatvaksin, sbp.buktiorientasi, sbp.id_berkas, sbp.riwayat, sbp.ijazahterlegalisir, sbp.str, sbp.siksip, sbp.buktiverifijazah, sbp.suratkeputusansuratperjanjian, sbp.uraiantugas, sbp.rkkspkk, sbp.buktipenilaiankinerja, sbp.datapelatihan, sbp.buktilamarankerja, ppg.bpjs_kodedokter, ppg.idpegawai, ppg.nip,ppg.sip, ppg.statuskeaktifan, pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai from person_pegawai ppg inner join person_person pper on pper.idperson = ppg.idperson inner join person_grup_pegawai pgp on pgp.idgruppegawai = ppg.idgruppegawai inner join sdmk_berkaspegawai sbp on ppg.idpegawai = sbp.id_pegawai left join person_grup_profesi pgp2 on pgp2.idprofesi = ppg.idprofesi WHERE sbp.id_berkas ='".$idberkas."'")->result_array();
        $data = ["datas" => $query];
        // print_r($data);
        
        $this->load->view('sdmk/v_modalberkaspegawai', $data);
    }

}

/* End of file ${TM_FILENAME:${1/(.+)/Csdmk.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Csdmk/:application/controllers/${1/(.+)/Csdmk.php/}} */