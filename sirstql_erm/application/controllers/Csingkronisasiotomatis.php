<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Csingkronisasiotomatis extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('mviewql');
    }
    
    
    public function wsuploadjadwaldokter_per2jamlimit20()
    {
        $dtjadwal = $this->mviewql->getdata_singkronisasijadwaldokterwebsite('upload_jadwalper2jam',0);
        if(empty(!$dtjadwal))
        {
            $uploadedJSON = json_encode(['datajadwal'=>$dtjadwal]);
            $data = $this->bpjsbap->crudWebql('/jadwaldokterws/insertjadwal',$uploadedJSON);

            //update singkronisasi jadwal jika berhasil
            if($data->metadata->code == '200')
            {
                foreach ($dtjadwal as $arr)
                {
                    $this->db->update('rs_jadwal',['singkronisasi'=>1],['idjadwal'=>$arr['idjadwal'] ]);
                }
            }
        }
    }
    
    
}
/* End of file ${TM_FILENAME:${1/(.+)/Clogin.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Clogin/:application/controllers/${1/(.+)/Clogin.php/}} */






