<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Msqlbasic extends CI_Model 
    {
        protected  $tb_meta = 'akre_meta_pengaturan';

        function show_db($table,$andwhere=array()){
            $query = $this->db->select('*')
                    ->where($andwhere)
                    ->get($table)
                    ->result_array();
            return $query; 
        }

        //[+] Update 21092022
        function show_db_where_not_in($table,$field,$array=array()){
            $query = $this->db->select('*')
                    ->where_not_in($field,$array)
                    ->get($table)
                    ->result_array();
            return $query; 
        }

        function row_db($table,$andwhere=array()){
            $query = $this->db->select('*')
                    ->where($andwhere)
                    ->get($table)
                    ->row_array();
            return $query; 
        }

        function insert_data($table, $data){
            $this->db->insert($table,$data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }

        function custom_query($sql){
            return $this->db->query($sql)->result_array();
        }

        function delete_db($table,$data){
            return $this->db->delete($table, $data);
        }

        function update_db($table,$where,$data){            
            $this->db->where($where);
            $this->db->update($table, $data);
            return $this->db->affected_rows();
        }

        function get_meta($idkey,$metakey){
            $get = $this->row_db($this->tb_meta,['idkey_meta'=>$idkey,'key_meta'=>$metakey]);
            return unserialize( $get['value_meta'] );
        }

        function get_cek_meta_array($idkey,$metakey){
            $get = $this->row_db($this->tb_meta,['idkey_meta'=>$idkey,'key_meta'=>$metakey]);
            return $get;
        }

        function insert_meta($idkey,$metakey,$value){
            $cek_value = '';
            if( is_array($value) ){
                $cek_value = serialize($value);
            }elseif(is_object($value)){
                $cek_value = serialize($value); 
            }else{
                $cek_value = $value;
            }

            $data = ['idkey_meta'=>$idkey,'key_meta'=>$metakey,'value_meta'=>$cek_value];
            
            return $this->insert_data($this->tb_meta, $data);
        }

        function update_meta($idkey,$metakey,$value){
            
            $cek_value = '';
            if( is_array($value) ){
                $cek_value = serialize($value);
            }elseif(is_object($value)){
                $cek_value = serialize($value); 
            }else{
                $cek_value = $value;
            }
            
            $get = $this->row_db($this->tb_meta,['idkey_meta'=>$idkey,'key_meta'=>$metakey]);
            
            
            if( !empty( $get ) ){
                $data = ['value_meta'=>$cek_value];
                return $this->update_db($this->tb_meta,['idkey_meta'=>$idkey,'key_meta'=>$metakey],$data);
            }else{
                $data = ['idkey_meta'=>$idkey,'key_meta'=>$metakey,'value_meta'=>$cek_value];
                return $this->insert_data($this->tb_meta, $data);
            }
        }
        
        function delete_meta($idkey,$metakey){
            return $this->delete_db($this->tb_meta,['idkey_meta'=>$idkey,'key_meta'=>$metakey]);
        }
        
    }