<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mviewql extends CI_Model 
{
    function viewbangsalaplicare_idbed($idbed)
    {
        $sql = "SELECT koderuangjkn, namabangsal, kodekelasjkn, count(idbed) as kapasitas, if(jeniskelaminbangsal='laki-laki', sum(ifnull(iskosong, 0)), 0) as tersedialakilaki, if(jeniskelaminbangsal='wanita', sum(ifnull(iskosong, 0)), 0) as tersediaperempuan, if(jeniskelaminbangsal='campur', sum(ifnull(iskosong, 0)), 0) as tersediacampur FROM `rs_bangsal` b join rs_kelas k on b.idkelas=k.idkelas left join rs_bed bed on bed.idbangsal=b.idbangsal left join rs_bed_status bs on bs.idstatusbed=bed.idstatusbed where b.idbangsal = (select idbangsal from rs_bed where idbed=$idbed) group by b.idbangsal";
        return $this->db->query($sql)->result_array();
    }
    
    function viewbangsalaplicare($idbangsal = '')
    {
        $sql = "SELECT koderuangjkn, namabangsal, kodekelasjkn, count(idbed) as kapasitas, if(jeniskelaminbangsal='laki-laki', sum(ifnull(iskosong, 0)), 0) as tersedialakilaki, if(jeniskelaminbangsal='wanita', sum(ifnull(iskosong, 0)), 0) as tersediaperempuan, if(jeniskelaminbangsal='campur', sum(ifnull(iskosong, 0)), 0) as tersediacampur FROM `rs_bangsal` b join rs_kelas k on b.idkelas=k.idkelas left join rs_bed bed on bed.idbangsal=b.idbangsal left join rs_bed_status bs on bs.idstatusbed=bed.idstatusbed ".((empty($idbangsal))?'':'where b.idbangsal = '.$idbangsal)." group by b.idbangsal";
        return $this->db->query($sql)->result_array();
    }
    
    //$jenis irj, igd
    function viewkunjungansiranap($kemarin, $jenis)
    {
        switch ($jenis)
        {
            case 'irj': $sql = "SELECT bs.namabangsal as KLINIK, sum(carabayar='jknpbi' or carabayar='jknnonpbi') as JKN, sum(!(carabayar='jknpbi' or carabayar='jknnonpbi')) as NON_JKN FROM rs_pemeriksaan p join person_pendaftaran pd on p.idpendaftaran=pd.idpendaftaran join rs_jadwal j on j.idjadwal=p.idjadwal join rs_unit u on u.idunit=j.idunit join rs_bangsal_siranap bs on bs.kodetipepasiensiranap=u.kodetipepasiensiranap where p.status<>'batal' and date(p.waktu)='".$kemarin."' group by bs.kodetipepasiensiranap, p.tahunbulan"; break;
            default: break;
        }
        return $this->db->query($sql)->result_array();
    }
    
    function viewbangsalsiranap($tanggal)
    {
        $sql = "SELECT kodekelassiranap as kode_ruang, kodetipepasiensiranap as tipe_pasien, count(1) as total_TT, if(bed.idstatusbed=2,sum(not ifnull(iskosong,0)), 0) as terpakai_male, if(bed.idstatusbed=3,sum(not ifnull(iskosong, 0)), 0) as terpakai_female, if(jeniskelaminbangsal='laki-laki', sum(ifnull(iskosong, 0)), 0)+ceil(if(jeniskelaminbangsal='campur',sum(ifnull(iskosong, 0))/2,0)) as kosong_male, if(jeniskelaminbangsal='wanita', sum(ifnull(iskosong, 0)), 0)+floor(if(jeniskelaminbangsal='campur',sum(ifnull(iskosong, 0))/2,0)) as kosong_female, 0 as waiting, '".$tanggal." 00:00:00' as tgl_update FROM `rs_bangsal` b join rs_kelas k on b.idkelas=k.idkelas left join rs_bed bed on bed.idbangsal=b.idbangsal left join rs_bed_status bs on bs.idstatusbed=bed.idstatusbed where kodetipepasiensiranap<>'' group by kodekelassiranap, kodetipepasiensiranap";
        return $this->db->query($sql)->result_array();
    }
    //select tanggalbulan, tanggal, namabarang, jenistarif, concat(sum(jumlah), ' ', singkatansatuan) as jumlah from (SELECT rmp.idinap, tanggalbulan(waktu) as tanggalbulan, date(waktu) as tanggal, idsatuan, idbarang, idjenistarif, rmbp.jumlah, singkatansatuan FROM `rs_inap_rencana_medis_pemeriksaan` rmp join `rs_inap_rencana_medis_barangpemeriksaan` rmbp on rmbp.idrencanamedispemeriksaan=rmp.idrencanamedispemeriksaan join `rs_inap_rencana_medis_barang` rmb on rmb.idrencanamedisbarang=rmbp.idrencanamedisbarang join rs_satuan s on s.idsatuan=rmb.idsatuanpemakaian where statuspelaksanaan='terlaksana' and rmp.idinap=1) rekap join rs_barang b on b.idbarang=rekap.idbarang join rs_jenistarif jt on jt.idjenistarif=rekap.idjenistarif group by tanggal, rekap.idsatuan, rekap.idbarang
    
    function viewinapbelumdiplot()
    {
        // $sql = 'select * from vrs_inapbelumdiplot';
        $sql = "select group_concat('[No RM:',`pps`.`norm`,', Nama:',`p`.`namalengkap`,', Masuk:',`tanggalterbilang`(`ri`.`waktumasuk`),']' separator ',') AS `belumdiplot`  from (((((`rs_inap` `ri` join `rs_bed` `bed` on((`bed`.`idbed` = `ri`.`idbed`))) join `person_pendaftaran` `pp` on((`pp`.`idpendaftaran` = `ri`.`idpendaftaran`))) join `person_pasien` `pps` on((`pps`.`norm` = `pp`.`norm`))) join `person_person` `p` on((`p`.`idperson` = `pps`.`idperson`))) left join `rs_inap_rencana_medis_pemeriksaan` `irmp` on((`irmp`.`idinap` = `ri`.`idinap`))) where (  `ri`.`status` ='ranap' and `ri`.`waktukeluar` = '0000-00-00' and bed.idbangsal='".$this->input->post('idbangsal')."') group by `ri`.`idinap` having (count(`irmp`.`idinap`) = 0)";
        return $this->db->query($sql)->result();
    }
    
    function viewhasilpemeriksaan($idjenisicd,$idunit,$idpendaftaran,$modelist)
    {
        $wheremodelist = (($modelist=='verifikasi') ? ' and hp.isverifklaim=1 ' : ' and hp.istagihan=1' );
        $whereidperiksa = ((empty($idpemeriksaan)) ? '' : ' and hp.idpemeriksaan = '.$idpemeriksaan );
        $paketparent = (($idjenisicd=='4')?" or idpendaftaran='".$idpendaftaran."'".$whereidperiksa." and hp.idpaketpemeriksaan!=0 and rpp.idjenisicd='4' ".$wheremodelist:"");
        $hakaksesinput = $this->mgenerikbap->select_multitable("hakinputidjenisicd","rs_unit",array("idunit"=>$idunit))->row_array();
        $sql = "select distinct i.*, ifnull(ifnull(i.idjenisicd,rpp.idjenisicd),rpp2.idjenisicd) idjenisicd2 , hp.idgrup, hp.gruppaketperiksa, hp.idpaketpemeriksaan,  rpp.idpaketpemeriksaan,rpp.idpaketpemeriksaanparent, hp.idhasilpemeriksaan, jenisicd,nilaidefault,concat(ifnull(hp.nilai,''),ifnull(hp.nilaitext,'')) as nilai,hp.total,hp.potongantagihan, rpp.namapaketpemeriksaan, rpp2.namapaketpemeriksaan as namapaketpemeriksaanparent, rpp.isubahwaktuhasil, "
                . "instr('".$hakaksesinput['hakinputidjenisicd']."', i.idjenisicd) as isbolehinput, ifnull(rpp.idpaketpemeriksaanrule,rpp2.idpaketpemeriksaanrule) as idpaketpemeriksaanrule, kesimpulan_hasilicd(hp.icd,hp.nilaitext) as kesimpulanhasil, hp.jaminanasuransi, khp.keteranganhasil from rs_hasilpemeriksaan hp 
                left join rs_icd i on i.icd=hp.icd
                left join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd 
                left join rs_paket_pemeriksaan_detail rppd on rppd.icd = hp.icd and rppd.idpaketpemeriksaan = hp.idpaketpemeriksaan  
                left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = hp.idpaketpemeriksaan 
                left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan = rpp.idpaketpemeriksaanparent  
                left join rs_keteranganhasilpemeriksaan khp on khp.idpemeriksaan=hp.idpemeriksaan and khp.idpaketpemeriksaan=hp.idgrup and khp.gruppaketperiksa=hp.gruppaketperiksa "
                . "where hp.idpendaftaran='".$idpendaftaran."'".$whereidperiksa." and ji.idjenisicd=".$idjenisicd.$wheremodelist.$paketparent."  order by ".(($idjenisicd!=2)? (($idjenisicd==4)? " hp.idhasilpemeriksaan" : " rpp.idpaketpemeriksaan,i.idjenisicd,icd " ) : " hp.idhasilpemeriksaan asc ");
        return $this->db->query($sql)->result();
    }

    function getDiagnosisDPJP($idpendaftaran)
    {
        $sql = "select diagnosa_verifklaim from rs_pemeriksaan
                where idpendaftaran=".$idpendaftaran."ORDER BY idpemeriksaan DESC";
        return $this->db->query($sql)->result();
    }
    
    function viewcetakhasilpemeriksaan($idjenisicd,$idunit, $idpendaftaran,$modelist,$idpemeriksaan='',$idgrup='')
    {
        $wheremodelist = (($modelist=='verifikasi') ? ' and hp.isverifklaim=1 ' : ' and hp.istagihan=1' );
        $whereidperiksa = ((empty($idpemeriksaan)) ? '' : ' and hp.idpemeriksaan = '.$idpemeriksaan );
        $wheregruppaket= ($idgrup != '00') ? ' and hp.idgrup= '.$idgrup : ' ' ;
        $paketparent = (($idjenisicd=='4')?" or idpendaftaran=".$idpendaftaran.$whereidperiksa.$wheregruppaket." and hp.idpaketpemeriksaan!=0 and rpp.idjenisicd='4' ".$wheremodelist:"");
        $sql = "select distinct i.*, ifnull(ifnull(i.idjenisicd,rpp.idjenisicd),rpp2.idjenisicd) idjenisicd2 , hp.idgrup, hp.gruppaketperiksa, hp.idpaketpemeriksaan,  rpp.idpaketpemeriksaan,rpp.idpaketpemeriksaanparent, hp.idhasilpemeriksaan, jenisicd,nilaidefault,concat(ifnull(hp.nilai,''),ifnull(hp.nilaitext,'')) as nilai,hp.total,hp.potongantagihan, rpp.namapaketpemeriksaan, rpp2.namapaketpemeriksaan as namapaketpemeriksaanparent, "
                . " ifnull(rpp.idpaketpemeriksaanrule,rpp2.idpaketpemeriksaanrule) as idpaketpemeriksaanrule, kesimpulan_hasilicd(hp.icd,hp.nilaitext) as kesimpulanhasil, hp.jaminanasuransi, khp.keteranganhasil from rs_hasilpemeriksaan hp 
                left join rs_icd i on i.icd=hp.icd
                left join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd 
                left join rs_paket_pemeriksaan_detail rppd on rppd.icd = hp.icd and rppd.idpaketpemeriksaan = hp.idpaketpemeriksaan  
                left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = hp.idpaketpemeriksaan 
                left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan = rpp.idpaketpemeriksaanparent  
                left join rs_keteranganhasilpemeriksaan khp on khp.idpemeriksaan=hp.idpemeriksaan and khp.idpaketpemeriksaan=hp.idgrup and khp.gruppaketperiksa=hp.gruppaketperiksa "
                . "where hp.idpendaftaran=".$idpendaftaran.$whereidperiksa.$wheregruppaket." and ji.idjenisicd=".$idjenisicd.$wheremodelist.$paketparent."  order by ".(($idjenisicd!=2)? (($idjenisicd==4)? " hp.idhasilpemeriksaan" : " rpp.idpaketpemeriksaan,i.idjenisicd,icd " ) : " hp.idhasilpemeriksaan asc ");
        return $this->db->query($sql)->result();
    }
    
    function viewpaketvitalsign($idjadwal,$idjadwalgrup='')
    {
        if(empty($idjadwalgrup))
        {
            $sql = "select i.icd,pp.idpaketpemeriksaan from rs_jadwal j  
            join rs_unit u on u.idunit=j.idunit 
            join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=u.idpaketpemeriksaan 
            join rs_paket_pemeriksaan_detail ppd on ppd.idpaketpemeriksaanparent=pp.idpaketpemeriksaan 
            join rs_icd i on i.icd=ppd.icd where j.idjadwal='$idjadwal'";
        }
        else
        {
            $sql = "select i.icd,pp.idpaketpemeriksaan from rs_unit u
            join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=u.idpaketpemeriksaan 
            join rs_paket_pemeriksaan_detail ppd on ppd.idpaketpemeriksaan=pp.idpaketpemeriksaan 
            join rs_icd i on i.icd=ppd.icd where u.idjadwalgrup='$idjadwalgrup'";
        }
    	return $this->db->query($sql)->result();
    }
    
    //rincian tarif ralan
    function viewdetailtarifralan($idpendaftaran)
    {
        $sql="(SELECT jenistarif,orderjenistarif, namapaketpemeriksaan as nama, hp.iskenapajak, hp.jasaoperator, (hp.subtotal- hp.jasaoperator) as akomodasi, sewa as sewa, (subtotal-sewa) as nonsewa, sum(hp.jumlah) as jumlah, 1 as kekuatan, 1 as kekuatanbarang "
                . "FROM `rs_hasilpemeriksaan` hp "
                . "join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=hp.idpaketpemeriksaan and not isnull(hp.idpaketpemeriksaan) "
                . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                . "where idpendaftaran='$idpendaftaran' and total>0   "
                . "group by hp.idpaketpemeriksaan)"
            . " union all "
            . "(SELECT jenistarif,orderjenistarif, concat(namaicd, if(jt.jenistarif='Diskon', (select if(jenisdiskon='persentase', concat(' ',ifnull(nominal,'0'), '%'),'') nominal from rs_masterdiskon rm where rm.icd=hp.icd and rm.idkelas=1) ,'') , '<br>', if(jt.jenistarif='Konsultasi' , namadokter(hp.iddokterjasamedis) , '')) as nama, hp.iskenapajak,hp.jasaoperator, (hp.subtotal- hp.jasaoperator) as akomodasi, sewa as sewa, ( if(jt.jenistarif='Diskon',potongantagihan,subtotal) -sewa) as nonsewa, sum(hp.jumlah) jumlah, 1 as kekuatan, 1 as kekuatanbarang "
                . "FROM `rs_hasilpemeriksaan` hp "
                . "join rs_icd i on i.icd=hp.icd and isnull(hp.idpaketpemeriksaan) "
                . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                . "where idpendaftaran='$idpendaftaran' and (total>0 or potongantagihan>0) "
                . "group by  hp.icd ,hp.iddokterjasamedis, hp.idpemeriksaan)"
            . " union all "
            . "(SELECT jenistarif,orderjenistarif, namabarang as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, harga as nonsewa, sum(bp.jumlahpemakaian) as jumlah, bp.kekuatan, b.kekuatan as kekuatanbarang "
                . "FROM `rs_barangpemeriksaan` bp "
                . "join rs_barang b on b.idbarang=bp.idbarang "
                . "join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif "
                . "where idpendaftaran='$idpendaftaran'  and total>0 and bp.jenisrawat = 'rajal' "
                . "group by bp.idbarang) ";
        $sql .= "order by orderjenistarif, nama";
        return $this->db->query($sql)->result();
    }
    
    //rincian detail ranap
    function viewdetailranap($idpendaftaran)
    {        
        $sql="(SELECT date(bnp.tanggal) as tanggal, jenistarif,namaicd as nama, count(1) as jumlah, bnp.total as subtotal "
                . "FROM rs_inap_biayanonpemeriksaan bnp "
                . "join rs_icd i on i.icd=bnp.icd "
                . "join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif "
                . "where bnp.status='terlaksana' and bnp.idpendaftaran='$idpendaftaran' and bnp.total>0 "
                . "group by bnp.tanggal, i.icd ) "
            . " union all "
            . "(SELECT date(rirmp.waktu) as tanggal, jenistarif, namaicd as nama, sum(rmh.jumlah) as jumlah, rmh.subtotal "
                . "FROM rs_inap_rencana_medis_pemeriksaan rirmp "
                . "join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan  = rirmp.idrencanamedispemeriksaan "
                . "join rs_icd i on i.icd=rmh.icd "
                . "join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif "
                . "where idpendaftaran='$idpendaftaran' and rirmp.status ='terlaksana' and rmh.total>0 and statuspelaksanaan='terlaksana' "
                . "group by date(rirmp.waktu),i.icd )"
            . " union all "
            . "(SELECT date(rirmp.waktu) as tanggal, jenistarif, namapaketpemeriksaan as nama, sum(rmh.jumlah) as jumlah, rmh.subtotal "
                . "FROM rs_inap_rencana_medis_pemeriksaan rirmp "
                . "join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan  = rirmp.idrencanamedispemeriksaan "
                . "join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=rmh.idpaketpemeriksaan and not isnull(rmh.idpaketpemeriksaan) "
                . "join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif "
                . "where idpendaftaran='$idpendaftaran' and rirmp.status ='terlaksana' and rmh.total>0 and statuspelaksanaan='terlaksana' "
                . "group by date(rirmp.waktu), rmh.idpaketpemeriksaan)"
            . " union all "
            . "(SELECT date(rirmp.waktu) as tanggal, concat(jenistarif, ' Rawat Inap') as jenistarif, namabarang as nama, sum(rirmb.jumlah) as jumlah, rirmb.harga as subtotal  
                FROM rs_inap_rencana_medis_pemeriksaan rirmp 
                join rs_inap_rencana_medis_barangpemeriksaan rirmb  on rirmb.idrencanamedispemeriksaan = rirmp.idrencanamedispemeriksaan
                join rs_barang b on b.idbarang=rirmb.idbarang 
                join rs_jenistarif jt on jt.idjenistarif=rirmb.idjenistarif 
                where rirmb.idpendaftaran='$idpendaftaran' and rirmb.statuspelaksanaan ='terlaksana' and rirmp.status = 'terlaksana' and rirmb.total>0 
                group by date(rirmp.waktu),b.idbarang ) "
            . " union all "
                . "(SELECT date(bp.tanggalinput) as tanggal, concat(jenistarif, ' Pulang') as jenistarif,namabarang as nama, sum(bp.jumlahpemakaian) as jumlah, bp.harga as subtotal "
                . "FROM `rs_barangpemeriksaan` bp "
                . "join rs_barang b on b.idbarang=bp.idbarang "
                . "join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif "
                . "where idpendaftaran='$idpendaftaran'  and total>0 and bp.jenisrawat = 'ranap' "
                . "group by bp.idbarang) "
            . " union all "
            . "(SELECT date(bnp.tanggal) as tanggal, jenistarif, i.namaicd as nama, count(1) as jumlah, bnp.total as subtotal "
                . "FROM rs_operasi ro "
                . "join rs_operasi_biayanonpemeriksaan bnp on bnp.idoperasi = ro.idoperasi "
                . "join rs_icd i on i.icd=bnp.icd "
                . "join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif "
                . "JOIN person_pendaftaran ppendaftaran ON ppendaftaran.idpendaftaran = bnp.idpendaftaran "
                . "where  bnp.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and ppendaftaran.carabayar='mandiri' and total>0 "
                . "group by bnp.tanggal, i.icd)"
            . " union all "
            . "(SELECT date(ro.tanggalmulai) as tanggal, jenistarif,rs.namaalat as nama,  sum(rso.jumlah) as jumlah, (rso.harga + rso.margin) as subtotal "
                . "FROM rs_operasi ro "
                . "join rs_operasi_sewaalat rso on rso.idoperasi = ro.idoperasi "
                . "join rs_sewaalat rs on rs.idsewaalat=rso.idsewaalat "
                . "join rs_jenistarif jt on jt.idjenistarif=rso.idjenistarif "
                . "where rso.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and rso.total>0 "
                . "group by date(ro.tanggalmulai),rso.idoperasisewaalat )"
            . " union all "
            . "(SELECT date(ro.tanggalmulai) as tanggal, concat(jenistarif, ' Operasi') as jenistarif, rb.namabarang as nama, sum(jumlahpemakaian) as jumlah, rob.harga as subtotal "
                . "FROM rs_operasi ro "                
                . "join rs_operasi_barangpemeriksaan rob on rob.idoperasi = ro.idoperasi "
                . "join rs_barang rb on rb.idbarang=rob.idbarang "
                . "join rs_jenistarif jt on jt.idjenistarif=rob.idjenistarif "
                . "where rob.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by date(ro.tanggalmulai), rob.idbarang)"
            . " union all "
            . "(SELECT date(tanggalmulai) as tanggal, jenistarif, upper(concat(rjo.jenistarifoperasi,' ',rot.jenistindakan)) as nama, count(1) as jumlah,  total as subtotal "
                . "FROM rs_operasi ro "
                . "join rs_operasi_tarifoperasi rot on rot.idoperasi = ro.idoperasi "
                . "join rs_jenistarif_operasi rjo on rjo.idjenistarifoperasi=rot.idjenistarifoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by date(ro.tanggalmulai),rot.idjenistarifoperasi )"
            . " union all "
            . "(SELECT date(tanggalmulai) as tanggal, jenistarif, rot.keterangan as nama, count(1) as jumlah,  sum(nominal) as subtotal "
                . "FROM rs_operasi ro "
                . "join rs_operasi_tarifoperasi_bpjs rot on rot.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and nominal>0 "
                . "group by date(ro.tanggalmulai),rot.idjenistarif )";
        
        $sql .= "order by jenistarif,tanggal,nama";
        return $this->db->query($sql)->result();             
    }
    


    /**
     * View Total Tarif Pemeriksaan
     * @param type $idpendaftaran ID Pendaftaran
     * @param type $modelist Mode List 
     * @param type $jenisnota Jenis Nota, default=''
     * @return type
     */

    // Contoh
    // idpendaftaran (p) ='271716'
    // jenisnota (j) = rajal
    // modelist (i) = ""
    function viewtotaltarifpemeriksaan($idpendaftaran,$modelist,$jenisnota)
    {
        $sql = "";
        $wheremodelist = '';

        if($modelist=='klaim')
        {
            $wheremodelist = ' and isverifklaim=1 ';
        }
        else if($modelist=='jaminan')
        {
            $wheremodelist = ' and jaminanasuransi=1 ';
        }
        else if($modelist=='mandiri')
        {
            $wheremodelist = ' and jaminanasuransi=0 ';
        }
        else
        {
            $wheremodelist = ' and istagihan=1 ';
        }
        
        if($jenisnota=='' OR $jenisnota=='gabung' OR $jenisnota=='rajal')//jika jenis nota gabung, rajal, atau kosong
        {
            $sql .="(SELECT jenistarif,orderjenistarif, namapaketpemeriksaan as nama, hp.iskenapajak, hp.jasaoperator, (hp.subtotal- hp.jasaoperator) as akomodasi, sewa as sewa, (subtotal-sewa) as nonsewa, sum(hp.jumlah) as jumlah, 1 as kekuatan, 1 as kekuatanbarang "
                    . "FROM `rs_hasilpemeriksaan` hp "
                    . "join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=hp.idpaketpemeriksaan and not isnull(hp.idpaketpemeriksaan) "
                    . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' ".$wheremodelist." ".(($modelist=='klaim') ? "" : " and total>0 " ). "  "
                    . "group by hp.idpaketpemeriksaan)"
                . " union all "
                . "(SELECT jenistarif,orderjenistarif, concat(namaicd, if(jt.jenistarif='Diskon', (select if(jenisdiskon='persentase', concat(' ',ifnull(nominal,'0'), '%'),'') nominal from rs_masterdiskon rm where rm.icd=hp.icd and rm.idkelas=1) ,'') , '<br>', if(jt.jenistarif='Konsultasi' , (select namadokter(rp.idpegawaidokter) from rs_pemeriksaan rp where rp.idpendaftaran=hp.idpendaftaran and rp.idpemeriksaansebelum is null), '')) as nama, hp.iskenapajak,hp.jasaoperator, (hp.subtotal- hp.jasaoperator) as akomodasi, sewa as sewa, ( if(jt.jenistarif='Diskon',potongantagihan,subtotal) -sewa) as nonsewa, sum(hp.jumlah) jumlah, 1 as kekuatan, 1 as kekuatanbarang "
                    . "FROM `rs_hasilpemeriksaan` hp "
                    . "join rs_icd i on i.icd=hp.icd and isnull(hp.idpaketpemeriksaan) "
                    . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' ".$wheremodelist." ".(($modelist=='klaim') ? "" : " and (total>0 or potongantagihan>0) " ). "  "
                    . "group by  hp.icd ,hp.iddokterjasamedis, hp.idpemeriksaan)"
                . " union all "
                . "(SELECT jenistarif,orderjenistarif, namabarang as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, harga as nonsewa, sum(bp.jumlahpemakaian) as jumlah, bp.kekuatan, b.kekuatan as kekuatanbarang "
                    ."FROM `rs_barangpemeriksaan` bp "
                    ."join rs_barang b on b.idbarang=bp.idbarang "
                    ."join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif "
                    ."where idpendaftaran='$idpendaftaran' ".$wheremodelist.(($modelist=='klaim') ? "" : " and total>0 " )." and bp.jenisrawat = 'rajal' "
                    ."group by bp.idbarang) ";
        }     
        if($modelist!='klaim')
        {
            if($jenisnota=='gabung' OR $jenisnota=='ranap')//jika jenis nota gabung, ranap, atau kosong
            {            
                $sql .= (($jenisnota=="ranap" ) ? " " : " union all ")
                     . "(SELECT jenistarif,orderjenistarif, namaicd as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, count(1) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_inap_biayanonpemeriksaan` bnp join rs_icd i on i.icd=bnp.icd join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif where bnp.status='terlaksana' and idpendaftaran='$idpendaftaran' ".$wheremodelist." and total>0 group by i.icd)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, namaicd as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, sum(rmh.jumlah) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_inap_rencana_medis_hasilpemeriksaan` rmh join rs_icd i on i.icd=rmh.icd join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif where idpendaftaran='$idpendaftaran' ".$wheremodelist." and total>0 and statuspelaksanaan='terlaksana' group by i.icd)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, concat(ifnull(namapaketpemeriksaan,''),' ranap') as nama,0 iskenapajak, 0 jasaoperator, 0 as akomodasi, sewa as sewa, (subtotal-sewa) as nonsewa, sum(rmh.jumlah) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_inap_rencana_medis_hasilpemeriksaan` rmh join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=rmh.idpaketpemeriksaan and not isnull(rmh.idpaketpemeriksaan) join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif where idpendaftaran='$idpendaftaran' ".$wheremodelist." and total>0 and statuspelaksanaan='terlaksana' group by rmh.idpaketpemeriksaan)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, namabarang as nama,0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, sum(rmb.jumlah) as jumlah, rmb.kekuatan, b.kekuatan as kekuatanbarang FROM `rs_inap_rencana_medis_barangpemeriksaan` rmb join rs_barang b on b.idbarang=rmb.idbarang join rs_jenistarif jt on jt.idjenistarif=rmb.idjenistarif where idpendaftaran='$idpendaftaran' and jenisasal != 'pasien' and total>0 group by b.idbarang ) "
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, namaicd as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, count(1) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_operasi_biayanonpemeriksaan` bnp join rs_icd i on i.icd=bnp.icd join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif where  bnp.idpendaftaran='$idpendaftaran' and total>0 group by i.icd)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, rs.namaalat as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, count(1) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM rs_operasi_sewaalat rso join rs_sewaalat rs on rs.idsewaalat=rso.idsewaalat join rs_jenistarif jt on jt.idjenistarif=rso.idjenistarif where  rso.idpendaftaran='$idpendaftaran' and total>0 group by rs.namaalat)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, rb.namabarang as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, count(1) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM rs_operasi_barangpemeriksaan rob join rs_barang rb on rb.idbarang=rob.idbarang join rs_jenistarif jt on jt.idjenistarif=rob.idjenistarif where  rob.idpendaftaran='$idpendaftaran' and total>0 group by rb.namabarang)"
                     . " union all "
                     . "(SELECT jenistarif,orderjenistarif, upper(concat(rjo.jenistarifoperasi,' ',rot.jenistindakan)) as nama, 0 iskenapajak, 0 jasaoperator, 0 as akomodasi, 0 as sewa, total as nonsewa, count(1) as jumlah, 1 as kekuatan, 1 as kekuatanbarang FROM rs_operasi_tarifoperasi rot join rs_jenistarif_operasi rjo on rjo.idjenistarifoperasi=rot.idjenistarifoperasi join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif where  rot.idpendaftaran='$idpendaftaran' and total>0 group by rjo.jenistarifoperasi)";
                
                
            }
        }
        $sql .= " order by orderjenistarif, nama";
        return $this->db->query($sql)->result();
    }

    function viewtotaltarifpemeriksaanCLAIM($idpendaftaran,$modelist,$jenisnota)
    {
        $sql = "";
        $wheremodelist = ''; 
        
        if($modelist=='klaim')
        {
            $wheremodelist = ' and isverifklaim=1 ';
        }
        else if($modelist=='jaminan')
        {
            $wheremodelist = ' and jaminanasuransi=1 ';
        }
        else if($modelist=='mandiri')
        {
            $wheremodelist = ' and jaminanasuransi=0 ';
        }
        else
        {
            $wheremodelist = ' and istagihan=1 ';
        }
        
        if($jenisnota=='' OR $jenisnota=='gabung' OR $jenisnota=='rajal')//jika jenis nota gabung, rajal, atau kosong
        {
            $sql =""
                ."(SELECT "
                    ."   '' as tanggal,
                        jenistarif,
                        orderjenistarif, 
                        namapaketpemeriksaan as nama, 
                        hp.iskenapajak, 
                        hp.jasaoperator, 
                        (hp.subtotal- hp.jasaoperator) as akomodasi, 
                        sewa as sewa, 
                        (subtotal-sewa) as nonsewa, 
                        sum(hp.jumlah) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM `rs_hasilpemeriksaan` hp "
                    . "join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=hp.idpaketpemeriksaan and not isnull(hp.idpaketpemeriksaan) "
                    . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' ".$wheremodelist." ".(($modelist=='klaim') ? "" : " and total>0 " ). "  "
                    . "group by hp.idpaketpemeriksaan)"
                . " union all "
                . "(SELECT 
                        '' as tanggal, 
                        jenistarif,
                        orderjenistarif, 
                        concat(namaicd, if(jt.jenistarif='Diskon', (select if(jenisdiskon='persentase', concat(' ',ifnull(nominal,'0'), '%'),'') nominal from rs_masterdiskon rm where rm.icd=hp.icd and rm.idkelas=1) ,'') , '<br>', if(jt.jenistarif='Konsultasi' , (select namadokter(rp.idpegawaidokter) from rs_pemeriksaan rp where rp.idpendaftaran=hp.idpendaftaran and rp.idpemeriksaansebelum is null), '')) as nama, 
                        hp.iskenapajak,
                        hp.jasaoperator, 
                        (hp.subtotal- hp.jasaoperator) as akomodasi, 
                        sewa as sewa, 
                        ( if(jt.jenistarif='Diskon',potongantagihan,subtotal) -sewa) as nonsewa, 
                        sum(hp.jumlah) jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM `rs_hasilpemeriksaan` hp "
                    . "join rs_icd i on i.icd=hp.icd and isnull(hp.idpaketpemeriksaan) "
                    . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' ".$wheremodelist." ".(($modelist=='klaim') ? "" : " and (total>0 or potongantagihan>0) " ). "  "
                    . "group by  hp.icd ,hp.iddokterjasamedis, hp.idpemeriksaan)"
                . " union all "
                . "(SELECT 
                        '' as tanggal,
                        jenistarif,
                        orderjenistarif, 
                        namabarang as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        harga as nonsewa, 
                        sum(bp.jumlahpemakaian) as jumlah, 
                        bp.kekuatan, 
                        b.kekuatan as kekuatanbarang 
                    FROM `rs_barangpemeriksaan` bp join rs_barang b on b.idbarang=bp.idbarang join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif 
                    where idpendaftaran='$idpendaftaran' ".$wheremodelist."  ".(($modelist=='klaim') ? "" : " and total>0 " )." and bp.jenisrawat = 'rajal' "
                    . " group by bp.idbarang) "
                . " union all "
                ."(SELECT 
                        date(bnp.tanggal) as tanggal, 
                        jenistarif,
                        '' orderjenistarif, 
                        namaicd as nama,
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        bnp.total as nonsewa,
                        count(1) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_inap_biayanonpemeriksaan bnp "
                    . "join rs_icd i on i.icd=bnp.icd "
                    . "join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif "
                    . "where bnp.status='terlaksana' and bnp.idpendaftaran='$idpendaftaran' and bnp.total>0 "
                    . "group by bnp.tanggal, i.icd ) "
                . " union all "
                . "(SELECT 
                        date(rirmp.waktu) as tanggal, 
                        jenistarif, 
                        '' orderjenistarif,
                        namaicd as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        rmh.subtotal as nonsewa, 
                        sum(rmh.jumlah) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_inap_rencana_medis_pemeriksaan rirmp "
                    . "join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan  = rirmp.idrencanamedispemeriksaan "
                    . "join rs_icd i on i.icd=rmh.icd "
                    . "join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' and rirmp.status ='terlaksana' and rmh.total>0 and statuspelaksanaan='terlaksana' "
                    . "group by date(rirmp.waktu),i.icd )"
                    . " union all "
                . "(SELECT 
                        date(rirmp.waktu) as tanggal, 
                        jenistarif, 
                        '' orderjenistarif,
                        namapaketpemeriksaan as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        rmh.subtotal as nonsewa,
                        sum(rmh.jumlah) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_inap_rencana_medis_pemeriksaan rirmp "
                    . "join rs_inap_rencana_medis_hasilpemeriksaan rmh on rmh.idrencanamedispemeriksaan  = rirmp.idrencanamedispemeriksaan "
                    . "join rs_paket_pemeriksaan pp on pp.idpaketpemeriksaan=rmh.idpaketpemeriksaan and not isnull(rmh.idpaketpemeriksaan) "
                    . "join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran' and rirmp.status ='terlaksana' and rmh.total>0 and statuspelaksanaan='terlaksana' "
                    . "group by date(rirmp.waktu), rmh.idpaketpemeriksaan)"
                . " union all "
                . "(SELECT 
                        date(rirmp.waktu) as tanggal, 
                        concat(jenistarif, ' Rawat Inap') as jenistarif, 
                        '' orderjenistarif,
                        namabarang as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        rirmb.harga as nonsewa,  
                        sum(rirmb.jumlah) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang
                    FROM rs_inap_rencana_medis_pemeriksaan rirmp 
                    join rs_inap_rencana_medis_barangpemeriksaan rirmb  on rirmb.idrencanamedispemeriksaan = rirmp.idrencanamedispemeriksaan
                    join rs_barang b on b.idbarang=rirmb.idbarang 
                    join rs_jenistarif jt on jt.idjenistarif=rirmb.idjenistarif 
                    where rirmb.idpendaftaran='$idpendaftaran' and rirmb.statuspelaksanaan ='terlaksana' and rirmp.status = 'terlaksana' and rirmb.total>0 
                    group by date(rirmp.waktu),b.idbarang ) "
                . " union all "
                . "(SELECT 
                        date(bp.tanggalinput) as tanggal, 
                        concat(jenistarif, ' Pulang') as jenistarif,
                        orderjenistarif, 
                        namabarang as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        bp.harga as nonsewa,
                        sum(bp.jumlahpemakaian) as jumlah,
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM `rs_barangpemeriksaan` bp "
                    . "join rs_barang b on b.idbarang=bp.idbarang "
                    . "join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif "
                    . "where idpendaftaran='$idpendaftaran'  and total>0 and bp.jenisrawat = 'ranap' "
                    . "group by bp.idbarang) "
                . " union all "
                . "(SELECT 
                        date(bnp.tanggal) as tanggal, 
                        jenistarif, 
                        '' orderjenistarif, 
                        i.namaicd as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa,                          
                        bnp.total as nonsewa,
                        count(1) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_operasi ro "
                    . "join rs_operasi_biayanonpemeriksaan bnp on bnp.idoperasi = ro.idoperasi "
                    . "join rs_icd i on i.icd=bnp.icd "
                    . "join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif "
                    . "JOIN person_pendaftaran ppendaftaran ON ppendaftaran.idpendaftaran = bnp.idpendaftaran "
                    . "where  bnp.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and ppendaftaran.carabayar='mandiri' and total>0 "
                    . "group by bnp.tanggal, i.icd)"
                . " union all "
                . "(SELECT 
                        date(ro.tanggalmulai) as tanggal, 
                        jenistarif,
                        '' orderjenistarif, 
                        rs.namaalat as nama,  
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        (rso.harga + rso.margin) as nonsewa,
                        sum(rso.jumlah) as jumlah,  
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_operasi ro "
                    . "join rs_operasi_sewaalat rso on rso.idoperasi = ro.idoperasi "
                    . "join rs_sewaalat rs on rs.idsewaalat=rso.idsewaalat "
                    . "join rs_jenistarif jt on jt.idjenistarif=rso.idjenistarif "
                    . "where rso.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and rso.total>0 "
                    . "group by date(ro.tanggalmulai),rso.idoperasisewaalat )"
                . " union all "
                . "(SELECT 
                        date(ro.tanggalmulai) as tanggal, 
                        concat(jenistarif, ' Operasi') as jenistarif, 
                        '' orderjenistarif,
                        rb.namabarang as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa, 
                        rob.harga as nonsewa,
                        sum(jumlahpemakaian) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_operasi ro "                
                    . "join rs_operasi_barangpemeriksaan rob on rob.idoperasi = ro.idoperasi "
                    . "join rs_barang rb on rb.idbarang=rob.idbarang "
                    . "join rs_jenistarif jt on jt.idjenistarif=rob.idjenistarif "
                    . "where rob.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                    . "group by date(ro.tanggalmulai), rob.idbarang)"
                . " union all "
                . "(SELECT 
                        date(tanggalmulai) as tanggal, 
                        jenistarif, 
                        '' orderjenistarif,
                        upper(concat(rjo.jenistarifoperasi,' ',rot.jenistindakan)) as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa,  
                        total as nonsewa,  
                        count(1) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_operasi ro "
                    . "join rs_operasi_tarifoperasi rot on rot.idoperasi = ro.idoperasi "
                    . "join rs_jenistarif_operasi rjo on rjo.idjenistarifoperasi=rot.idjenistarifoperasi "
                    . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                    . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                    . "group by date(ro.tanggalmulai),rot.idjenistarifoperasi )"
                . " union all "
                . "(SELECT 
                        date(tanggalmulai) as tanggal, 
                        jenistarif, 
                        '' orderjenistarif, 
                        rot.keterangan as nama, 
                        0 iskenapajak, 
                        0 jasaoperator, 
                        0 as akomodasi, 
                        0 as sewa,  
                        sum(nominal) as nonsewa, 
                        count(1) as jumlah, 
                        1 as kekuatan, 
                        1 as kekuatanbarang "
                    . "FROM rs_operasi ro "
                    . "join rs_operasi_tarifoperasi_bpjs rot on rot.idoperasi = ro.idoperasi "
                    . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                    . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and nominal>0 "
                    . "group by date(ro.tanggalmulai),rot.idjenistarif )";
       }
        $sql .= "order by jenistarif, tanggal, nama";
        return $this->db->query($sql)->result();
        
    }
    
    function viewtotaltarifpemeriksaanlengkap($idpendaftaran)
    {
        $sql=   "  (SELECT idgrup, idpaketpemeriksaan, icd, jenisicd, jenistarif, nama, nilai, jasaoperator, nakes, jasars, bhp, akomodasi, margin, sewa, jumlahdiresepkan, jumlahpemakaian, harga, total, potongan, kekuatan, kekuatanbarang FROM "
                . "((SELECT idgrup, hp.idpaketpemeriksaan, hp.icd, jenisicd, jenistarif, namapaketpemeriksaan as nama, ifnull(nilaitext, nilai) as nilai, jasaoperator, nakes, jasars, "
                . "bhp, akomodasi, margin, sewa, 0 as jumlahdiresepkan, jumlah as jumlahpemakaian, 0 as harga, total, 0 as potongan, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_hasilpemeriksaan` hp join rs_paket_pemeriksaan pp "
                . "on pp.idpaketpemeriksaan=hp.idpaketpemeriksaan and not isnull(hp.idpaketpemeriksaan) and isnull(hp.icd) join rs_jenisicd ji on ji.idjenisicd=pp.idjenisicd "
                . "join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif  where idpendaftaran='$idpendaftaran')  union all (SELECT idgrup, hp.idpaketpemeriksaan, hp.icd, jenisicd, "
                . "jenistarif, concat(namaicd, ' / ', hp.icd,'<br>', if(jt.jenistarif='Konsultasi' , (select concat(ifnull(p.titeldepan, ''), ' ',pp.namalengkap, ' ', ifnull(p.titelbelakang, '')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai p, person_person pp where  rj.idjadwal = rp.idjadwal and p.idpegawai = rj.idpegawaidokter and pp.idperson = p.idperson and rp.idpemeriksaan=hp.idpemeriksaan and jt.idjenistarif=3), '')) as nama, ifnull(nilaitext, nilai) as nilai, jasaoperator, nakes, jasars, bhp, akomodasi, margin, sewa, 0 as jumlahdiresepkan, "
                . "jumlah as jumlahpemakaian, 0 as harga, total, 0 as potongan, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_hasilpemeriksaan` hp join rs_icd i  on i.icd=hp.icd and not isnull(hp.idpaketpemeriksaan) and not isnull(hp.icd) "
                . "join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd join rs_jenistarif jt on jt.idjenistarif=hp.idjenistarif  where idpendaftaran='$idpendaftaran')) x) "
                . " union all "
                . "(SELECT 0 as idgrup, 0 as idpaketpemeriksaan, 0 as icd, jenisicd, ifnull(jenistarif, 'Non Tarif') as jenistarif, concat(namaicd, ' / ', hp.icd,'<br>', if(jt.jenistarif='Konsultasi' , (select concat(ifnull(p.titeldepan, ''), ' ',pp.namalengkap, ' ', ifnull(p.titelbelakang, '')) as namadokter FROM rs_pemeriksaan rp, rs_jadwal rj, person_pegawai p, person_person pp where  rj.idjadwal = rp.idjadwal and p.idpegawai = rj.idpegawaidokter and pp.idperson = p.idperson and rp.idpemeriksaan=hp.idpemeriksaan and jt.idjenistarif=3), '')) as nama, "
                . "ifnull(nilaitext, nilai) as nilai, jasaoperator, nakes, jasars, bhp, akomodasi, margin, sewa, 0 as jumlahdiresepkan, jumlah as jumlahpemakaian, 0 as harga, total, 0 as potongan, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_hasilpemeriksaan` hp "
                . "join rs_icd i on i.icd=hp.icd  and isnull(hp.idpaketpemeriksaan) join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd left join rs_jenistarif jt "
                . "on jt.idjenistarif=hp.idjenistarif where idpendaftaran='$idpendaftaran') "
                . " union all "
                . "(SELECT 0 as idgrup, 0 as idpaketpemeriksaan, 0 as icd, jenis as jenisicd, "
                . "jenistarif, namabarang as nama, 0 as nilai, 0 as jasaoperator, 0 as nakes, 0 as jasars, 0 as bhp, 0 as akomodasi, 0 as margin, 0 as sewa, jumlahdiresepkan, jumlahpemakaian, harga, "
                . "total, 0 as potongan, bp.kekuatan, b.kekuatan as kekuatanbarang FROM `rs_barangpemeriksaan` bp join rs_barang b on b.idbarang=bp.idbarang  join rs_jenistarif jt on jt.idjenistarif=bp.idjenistarif where "
                . "idpendaftaran='$idpendaftaran') "
                . " union all "
                . "(select 0 as idgrup, 0 as idpaketpemeriksaan, bnp.icd, jenisicd, jenistarif, concat(namaicd, ' / ', i.icd) as nama, 0 as nilai, sum(jasaoperator) as jasaoperator, sum(nakes) as nakes, sum(jasars) as jasars, sum(bhp) as bhp, sum(akomodasi) as akomodasi, sum(margin) as margin, sum(sewa) as sewa, 0 as jumlahdiresepkan, 0 as jumlahpemakaian, 0 as harga, sum(total) as total, sum(potongan) as potongan, 1 as kekuatan, 1 as kekuatanbarang from `rs_inap_biayanonpemeriksaan` bnp join rs_icd i on i.icd=bnp.icd join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd where bnp.status='terlaksana' and idpendaftaran='$idpendaftaran' and total>0 group by i.icd )"
                . " union all "
                . "(SELECT 0 as idgrup, 0 as idpaketpemeriksaan, rmh.icd, jenisicd, jenistarif, concat(namaicd, ' / ', i.icd) as nama, 0 as nilai, sum(jasaoperator) as jasaoperator, sum(nakes) as nakes, sum(jasars) as jasars, sum(bhp) as bhp, sum(akomodasi) as akomodasi, sum(margin) as margin, sum(sewa) as sewa, 0 as jumlahdiresepkan, sum(jumlah) as jumlahpemakaian, 0 as harga, sum(total) as total, sum(potongan) as potongan, 1 as kekuatan, 1 as kekuatanbarang FROM `rs_inap_rencana_medis_hasilpemeriksaan` rmh join rs_icd i on i.icd=rmh.icd join rs_jenistarif jt on jt.idjenistarif=rmh.idjenistarif join rs_jenisicd ji on ji.idjenisicd=i.idjenisicd where idpendaftaran='$idpendaftaran' and total>0 and statuspelaksanaan='terlaksana' group by i.icd)"
                . " union all "
                . "(SELECT 0 as idgrup, 0 as idpaketpemeriksaan, 0 as icd, jenis as jenisicd, jenistarif, namabarang as nama, 0 as nilai, 0 as jasaoperator, 0 as nakes, 0 as jasars, 0 as bhp, 0 as akomodasi, 0 as margin, 0 as sewa, 0 as jumlahdiresepkan, sum(jumlahpemakaian) as jumlahpemakaian, harga, sum(total) as total, sum(potongan) as potongan, rmb.kekuatan, b.kekuatan FROM `rs_inap_rencana_medis_barang` rmb join rs_barang b on b.idbarang=rmb.idbarang join rs_jenistarif jt on jt.idjenistarif=rmb.idjenistarif where idpendaftaran='$idpendaftaran' and total>0 group by rmb.idrencanamedisbarang)"
                . " order by jenisicd, idgrup, (idgrup=idpaketpemeriksaan) desc, idpaketpemeriksaan, not isnull(icd), jenistarif, nama";
        return $this->db->query($sql)->result();
    }
    
    function view_tarif_byjenistarifranap($idpendaftaran)
    {
        $sql = " (SELECT  jt.jenistarif, cast(sum(bnp.total) as INT) as nominal FROM rs_operasi ro "
                . "join rs_operasi_biayanonpemeriksaan bnp on bnp.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=bnp.idjenistarif "
                . "join person_pendaftaran ppd on ppd.idpendaftaran=bnp.idpendaftaran "
                . "where  bnp.idpendaftaran='$idpendaftaran' and ppd.carabayar='mandiri' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by bnp.idjenistarif)"
            . " union all "
            . "(SELECT jt.jenistarif, cast(sum(rso.total) as INT) as nominal FROM rs_operasi ro "
                . "join rs_operasi_sewaalat rso on rso.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rso.idjenistarif "
                . "where rso.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by rso.idjenistarif)"
            . " union all "
            . "(SELECT concat(jt.jenistarif, ' Operasi') as jenistarif  , cast(sum(rob.total) as INT) as nominal FROM rs_operasi ro "
                . "join rs_operasi_barangpemeriksaan rob on rob.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rob.idjenistarif "
                . "where rob.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by rob.idjenistarif)"
            . " union all "
            . "(SELECT jt.jenistarif, cast(sum(rot.total) as INT) as nominal FROM rs_operasi ro "
                . "join rs_operasi_tarifoperasi rot on rot.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and total>0 "
                . "group by rot.idjenistarif)"
            . " union all "
            . "(SELECT jt.jenistarif, cast(sum(rot.nominal) as INT) as nominal FROM rs_operasi ro "
                . "join rs_operasi_tarifoperasi_bpjs rot on rot.idoperasi = ro.idoperasi "
                . "join rs_jenistarif jt on jt.idjenistarif=rot.idjenistarif "
                . "where rot.idpendaftaran='$idpendaftaran' and ro.statusoperasi = 'selesai' and nominal>0 "
                . "group by rot.idjenistarif)"
            . " union all ";
        
        // tindakan ranap
        $sql .= "(select rj.jenistarif, cast(sum(rirmh.total) as INT) as nominal 
                from rs_inap_rencana_medis_pemeriksaan rirmp
                join rs_inap_rencana_medis_hasilpemeriksaan rirmh on rirmh.idrencanamedispemeriksaan = rirmp.idrencanamedispemeriksaan
                join rs_jenistarif rj on rj.idjenistarif = rirmh.idjenistarif
                WHERE rirmh.idpendaftaran = ".$idpendaftaran." and rirmp.status ='terlaksana' and rirmh.total > 0 and rirmh.statuspelaksanaan = 'terlaksana'
                GROUP by rirmh.idjenistarif)"
              ." UNION ALL
                #biaya obat pulang
                (SELECT concat(rj.jenistarif, ' Pulang') as jenistarif,SUM(bp.total) as nominal 
                FROM `rs_barangpemeriksaan` bp 
                join rs_barang b on b.idbarang=bp.idbarang 
                join rs_jenistarif rj on rj.idjenistarif=bp.idjenistarif 
                where bp.idpendaftaran=".$idpendaftaran."  and bp.total>0 and bp.jenisrawat = 'ranap' 
                group  by bp.idjenistarif) union all
                #obat dan bhp ranap
                (select concat(rj.jenistarif, ' Rawat Inap') as jenistarif, cast(sum(rirmb.total) as INT) as nominal 
                from rs_inap_rencana_medis_pemeriksaan rirmp
                join rs_inap_rencana_medis_barangpemeriksaan rirmb on rirmb.idrencanamedispemeriksaan = rirmp.idrencanamedispemeriksaan 
                join rs_jenistarif rj on rj.idjenistarif = rirmb.idjenistarif
                WHERE rirmb.idpendaftaran = ".$idpendaftaran." and rirmp.status ='terlaksana' and rirmb.total > 0 and rirmb.statuspelaksanaan = 'terlaksana'
                GROUP by rirmb.idjenistarif)"
               ." UNION ALL
                #biayan non pemeriksaan
                (select rj.jenistarif, cast(sum(rip.total) as INT) as nominal 
                from rs_inap_biayanonpemeriksaan rip
                join rs_jenistarif rj on rj.idjenistarif = rip.idjenistarif
                WHERE rip.idpendaftaran = ".$idpendaftaran." and rip.total > 0 and rip.status = 'terlaksana'
                GROUP by rip.idjenistarif) order by jenistarif";
        return $this->db->query($sql)->result();
    }
    
    function viewinfotagihan($idpendaftaran,$jenisperiksa='')
    {
        $jnsperiksa = (($jenisperiksa == '') ? '' : " and t.jenisperiksa = '".$jenisperiksa."' " );
        
        $sql="select date_format(waktumasuk,'%d') as tglmasuk, t.jenispembayaran, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, pp.alamat, pp2.namalengkap as penanggung, pps.norm, t.idtagihan, t.waktutagih,t.pembulatan,t.kekurangan,t.kembalian, "
                . "ifnull(t.waktubayar, 'Belum Dibayar') as waktubayar, usia(ifnull(pp.tanggallahir, date(now()))) as usia, t.nominal, potongan as potongan, t.dibayar, concat(t.jenistagihan,' ',t.jenisperiksa ) as jenistagihan, concat(bg.namabangsal, ' ', b.nobed, '/', k3.kelas) as ruangkelas, concat(k.kelas, '/', k2.kelas) as kelas, "
                . "date_format(waktumasuk, '%d %b %Y %H:%i:%s') as waktumasuk, tanggalterbilang(waktukeluar) as waktukeluar, datediff( if(waktukeluar='0000-00-00 00:00:00', now() ,waktukeluar), waktumasuk) as dirawatselama, concat(ifnull(peg.titeldepan, ''), ' ',pp3.namalengkap, ' ', ifnull(peg.titelbelakang, '')) as dokterdbjp "
                . "from keu_tagihan t "
                . "join person_pendaftaran pdf on t.idpendaftaran=pdf.idpendaftaran "
                . "join person_pasien pps on pps.norm=pdf.norm "
                . "join person_person pp on pps.idperson=pp.idperson "
                . "left join person_pasien_penanggungjawab ppj on ppj.idpasienpenanggungjawab=pdf.idpasienpenanggungjawab "
                . "left join person_person pp2 on pp2.idperson=ppj.idperson "
                . "left join rs_inap ri on ri.idpendaftaran=pdf.idpendaftaran "
                . "left join rs_bed b on ri.idbed=b.idbed left join rs_bangsal bg on bg.idbangsal=b.idbangsal "
                . "left join rs_kelas k on k.idkelas=ri.idkelas "
                . "left join rs_kelas k2 on k2.idkelas=ri.idkelasjaminan "
                . "left join rs_kelas k3 on k3.idkelas=bg.idkelas "
                . "left join person_pegawai peg on peg.idpegawai=ri.idpegawaidokter "
                . "left join person_person pp3 on pp3.idperson=peg.idperson "
                . "where t.nominal !=0 and t.jenistagihan!='batal' and t.idpendaftaran='$idpendaftaran' ".$jnsperiksa;
        return $this->db->query($sql)->result();
    }

    function viewinfotagihanVersiCLAIM($idpendaftaran)
    {
        // $jnsperiksa = (($jenisperiksa == '') ? '' : " and t.jenisperiksa = '".$jenisperiksa."' " );
        $jnsperiksa = " and t.jenisperiksa = 'rajal'";
        
        $sql="select date_format(waktumasuk,'%d') as tglmasuk, t.jenispembayaran, concat(ifnull(pp.identitas,''),' ',ifnull(pp.namalengkap,'')) as namalengkap, pp.alamat, pp2.namalengkap as penanggung, pps.norm, t.idtagihan, date(t.waktutagih) as waktutagih,t.pembulatan,t.kekurangan,t.kembalian, "
                . "ifnull(t.waktubayar, 'Belum Dibayar') as waktubayar, usia(ifnull(pp.tanggallahir, date(now()))) as usia, t.nominal, potongan as potongan, t.dibayar, concat(t.jenistagihan,' ',t.jenisperiksa ) as jenistagihan, concat(bg.namabangsal, ' ', b.nobed, '/', k3.kelas) as ruangkelas, concat(k.kelas, '/', k2.kelas) as kelas, "
                . "date_format(waktumasuk, '%d %b %Y %H:%i:%s') as waktumasuk, tanggalterbilang(waktukeluar) as waktukeluar, datediff( if(waktukeluar='0000-00-00 00:00:00', now() ,waktukeluar), waktumasuk) as dirawatselama, concat(ifnull(peg.titeldepan, ''), ' ',pp3.namalengkap, ' ', ifnull(peg.titelbelakang, '')) as dokterdbjp "
                . "from keu_tagihan t "
                . "join person_pendaftaran pdf on t.idpendaftaran=pdf.idpendaftaran "
                . "join person_pasien pps on pps.norm=pdf.norm "
                . "join person_person pp on pps.idperson=pp.idperson "
                . "left join person_pasien_penanggungjawab ppj on ppj.idpasienpenanggungjawab=pdf.idpasienpenanggungjawab "
                . "left join person_person pp2 on pp2.idperson=ppj.idperson "
                . "left join rs_inap ri on ri.idpendaftaran=pdf.idpendaftaran "
                . "left join rs_bed b on ri.idbed=b.idbed left join rs_bangsal bg on bg.idbangsal=b.idbangsal "
                . "left join rs_kelas k on k.idkelas=ri.idkelas "
                . "left join rs_kelas k2 on k2.idkelas=ri.idkelasjaminan "
                . "left join rs_kelas k3 on k3.idkelas=bg.idkelas "
                . "left join person_pegawai peg on peg.idpegawai=ri.idpegawaidokter "
                . "left join person_person pp3 on pp3.idperson=peg.idperson "
                . "where t.nominal !=0 and t.jenistagihan!='batal' and t.idpendaftaran='$idpendaftaran' ".$jnsperiksa;
        return $this->db->query($sql)->result();
    }
    
   function viewlaporanrl53($tgl1,$tgl2)
   {
       // $dt = [];
       // for($x=0; $x<$month;$x++)
       // {
         $dt = $this->db->query("SELECT DATE_FORMAT(waktu, '%M') as bulan, 
                                        year(waktu) as tahun, 
                                        ri.icd, 
                                        namaicd, 
                                        sum(if(jeniskelamin='wanita' and kondisikeluar='hidup',1,0)) as wanitahidup,
                                        sum(if(jeniskelamin='laki-laki' and kondisikeluar='hidup',1,0)) as lakilakihidup, 
                                        sum(if(jeniskelamin='wanita' and kondisikeluar='meninggal',1,0)) as wanitameninggal, 
                                        sum(if(jeniskelamin='laki-laki' and kondisikeluar='meninggal',1,0)) as lakilakimeninggal, 
                                        count(1) as total 
                                FROM rs_hasilpemeriksaan rhp 
                                        join rs_icd ri on ri.icd=rhp.icd 
                                        join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran 
                                        join person_pasien pps on pps.norm=pp.norm 
                                        join person_person ppr on ppr.idperson=pps.idperson 
                                where idjenisicd=2 
                                    and (pp.jenispemeriksaan='ranap' OR pp.jenispemeriksaan='rajalnap') 
                                    and date(waktu) BETWEEN '$tgl1' and '$tgl2' 
                                group by month(waktu), year(waktu), ri.icd 
                                order by year(waktu), month(waktu), total desc limit 10")->result();
       // }
       return $dt;
   }
    
    function viewlaporanrl54($tgl1,$tgl2)
    {
        // group dan order tidak berdasarkan bulan
        // SELECT DATE_FORMAT('2019-01-01', '%M') as bulanawal,DATE_FORMAT('2019-10-30', '%M') as bulanakhir,year(waktu) as tahun, ri.icd, namaicd, sum(if(jeniskelamin='wanita' and iskasuslama=0,1,0)) as wanitabaru, sum(if(jeniskelamin='laki-laki' and iskasuslama=0,1,0)) as lakilakibaru, sum(if(jeniskelamin='wanita' and iskasuslama=0,1,0))+sum(if(jeniskelamin='laki-laki' and iskasuslama=0,1,0)) as total FROM rs_hasilpemeriksaan rhp join rs_icd ri on ri.icd=rhp.icd join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran join person_pasien pps on pps.norm=pp.norm join person_person ppr on ppr.idperson=pps.idperson where idjenisicd=2 and pp.jenispemeriksaan='rajal' and date(waktu) BETWEEN '2019-01-01' and '2019-10-30' group by year(waktu), ri.icd order by year(waktu), total desc limit 10



       // $dt = []; year(waktu)='$tahun' month(waktu)='".$x."'
       // for($x=0; $x<$month;$x++)
       // {
         $dt = $this->db->query("SELECT DATE_FORMAT(waktu, '%M') as bulan, year(waktu) as tahun, ri.icd, namaicd, sum(if(jeniskelamin='wanita' and iskasuslama=0,1,0)) as wanitabaru, sum(if(jeniskelamin='laki-laki' and iskasuslama=0,1,0)) as lakilakibaru, sum(if(jeniskelamin='wanita' and iskasuslama=0,1,0))+sum(if(jeniskelamin='laki-laki' and iskasuslama=0,1,0)) as total 
            FROM rs_hasilpemeriksaan rhp 
            join rs_icd ri on ri.icd=rhp.icd 
            join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran and pp.jenispemeriksaan='rajal' and pp.idstatuskeluar=2
            join person_pasien pps on pps.norm=pp.norm 
            join person_person ppr on ppr.idperson=pps.idperson 
            where idjenisicd=2 and pp.jenispemeriksaan='rajal' and date(waktu) BETWEEN '$tgl1' and '$tgl2' group by month(waktu), year(waktu), ri.icd order by year(waktu), month(waktu), total desc limit 10")->result();


       // }
       return $dt;
    }
    function rl4getdata($layanan,$tgl1,$tgl2)
    {
        $wherelayanan = ($layanan=='rajal') ? " (jenispemeriksaan='rajal' or jenispemeriksaan='rajalnap' )" : "jenispemeriksaan='".$layanan."'" ;
        $q = $this->db->query("SELECT idgolongansebabpenyakit, kbwanita, kbpria, awanita, apria, bwanita, bpria, cwanita, cpria, dwanita, dpria, ewanita, epria, fwanita, fpria, gwanita, gpria, hwanita, hpria, iwanita, ipria, kunjunganbaru, kunjungan FROM `vrs_laporan_all_rl4b` WHERE  date(waktuperiksa) BETWEEN '".$tgl1."' and '".$tgl2."' and ".$wherelayanan." and idgolongansebabpenyakit > 0 order by idgolongansebabpenyakit");
        return $q;
    }

    //-- report ISPA/PNEUMONIA
    function getreport_pneumoniaorispa_rajal($tgl1,$tgl2)
    {
        return $this->db->query("select
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as K1W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as K1L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825, 1, 0)),0) as K5W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825, 1, 0)),0) as K5L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and DATEDIFF(now(),pper.tanggallahir) <5, 1, 0)),0) as K5T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as pneumonia_k1W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as pneumonia_k1L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 369 and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4L,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 369 and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4W,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4T,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4TL,
            ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4TW 
            from rs_hasilpemeriksaan rh 
            join rs_icd ri on ri.icd = ri.icd 
            join rs_jenispenyakit rjp on rjp.idjenispenyakit = ri.idjenispenyakit
            join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran and pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum' and pp.idstatuskeluar='2'
            join person_pasien ppas on ppas.norm = pp.norm join person_person pper on pper.idperson = ppas.idperson where pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum' and ri.icd = rh.icd and ri.idjenisicd='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."'");
    }
    function getreport_pneumoniaorispabykabupaten_rajal($tgl1,$tgl2)
    {
        return $this->db->query("select ifnull(gkab.namakabupatenkota,'Luar DIY') as kabupatenkota, 
ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5W,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5L,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as L5T,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as K1W,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as K1L,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825, 1, 0)),0) as K5W,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825, 1, 0)),0) as K5L,
    ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='ispa' and DATEDIFF(now(),pper.tanggallahir) <5, 1, 0)),0) as K5T,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5W,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5L,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) >= 1825, 1, 0)),0) as pneumonia_5T,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as pneumonia_k1W,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369, 1, 0)),0) as pneumonia_k1L,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5W,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >=369 and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5L,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) < 1825 , 1, 0)),0) as pneumonia_k5T,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1L,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1W,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) < 369 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_k1T,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) >= 369 and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4L,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) >= 369 and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4W,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4T,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='laki-laki' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4TL,
        ifnull(sum(if(rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia' and pper.jeniskelamin='wanita' and DATEDIFF(now(),pper.tanggallahir) <= 1460 and pp.kondisikeluar='meninggal', 1, 0)),0) as meninggal_4TW
        from rs_hasilpemeriksaan rh
        join rs_icd ri on ri.icd = ri.icd and ri.idjenispenyakit !=''
        join rs_jenispenyakit rjp on rjp.idjenispenyakit = ri.idjenispenyakit
        left join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran and pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum' and pp.idstatuskeluar='2'
        left join person_pasien ppas on ppas.norm = pp.norm
        left join person_person pper on pper.idperson = ppas.idperson
        left join geografi_desakelurahan gd on gd.iddesakelurahan = pper.iddesakelurahan
        left join geografi_kecamatan gk on gk.idkecamatan = gd.idkecamatan
        left join geografi_kabupatenkota gkab on gkab.idkabupatenkota = gk.idkabupatenkota
        where 
        pp.jenispemeriksaan!='ranap' and pp.jenispemeriksaan!='belum' and ri.icd = rh.icd and ri.idjenisicd='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' GROUP by gkab.idkabupatenkota");
    }
    // mahmud, clear -> get data STP RS (SURVEILANS TERPADU PENYAKIT BERBASIS RUMAH SAKIT RAWAT JALAN/RANAP (KASUS BARU))
    public function report_getdatastprs_rajal($tgl1,$tgl2)
    {
        $tahun1 = date('Y', strtotime($tgl1));
        $tahun2 = date('Y', strtotime($tgl2));
        $sql = 'select rj.jenispenyakit, ri.icd ,
ifnull(sum(if( rh.iskasuslama="0" and pper.jeniskelamin ="laki-laki" , 1, 0)),0) as L,
ifnull(sum(if( rh.iskasuslama="0" and pper.jeniskelamin ="wanita"  , 1, 0)),0) as P,
ifnull(sum(if( rh.iskasuslama="0" and pper.jeniskelamin ="wanita" or rh.iskasuslama="0" and pper.jeniskelamin ="laki-laki", 1, 0)),0) as J,
#0 - 7 hr
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) <= 7 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L1,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) <= 7 and pper.jeniskelamin ="wanita", 1, 0)),0) as P1,
#8 - 28 hr
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) > 7 and DATEDIFF(now(),pper.tanggallahir) <= 28 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L2,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) > 7 and DATEDIFF(now(),pper.tanggallahir) <= 28 and pper.jeniskelamin ="wanita", 1, 0)),0) as P2,
# < 1 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) < 365 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L3,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) < 365 and pper.jeniskelamin ="wanita", 1, 0)),0) as P3,
#1 - 4 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 365 and DATEDIFF(now(),pper.tanggallahir) < 1461 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L4,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 365 and DATEDIFF(now(),pper.tanggallahir) < 1461 and pper.jeniskelamin ="wanita", 1, 0)),0) as P4,
#5 - 9 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 1461 and DATEDIFF(now(),pper.tanggallahir) < 3652 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L5,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 1461 and DATEDIFF(now(),pper.tanggallahir) < 3652 and pper.jeniskelamin ="wanita", 1, 0)),0) as P5,
#10 - 14 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 3652 and DATEDIFF(now(),pper.tanggallahir) < 5478 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L6,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 3652 and DATEDIFF(now(),pper.tanggallahir) < 5478 and pper.jeniskelamin ="wanita", 1, 0)),0) as P6,
#15 - 19 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 5478 and DATEDIFF(now(),pper.tanggallahir) < 7304 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L7,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 5478 and DATEDIFF(now(),pper.tanggallahir) < 7304 and pper.jeniskelamin ="wanita", 1, 0)),0) as P7,
#20 - 44 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 7304 and DATEDIFF(now(),pper.tanggallahir) < 16436 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L8,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 7304 and DATEDIFF(now(),pper.tanggallahir) < 16436 and pper.jeniskelamin ="wanita", 1, 0)),0) as P8,
#45 - 54 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 16436 and DATEDIFF(now(),pper.tanggallahir) < 20088 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L9,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 16436 and DATEDIFF(now(),pper.tanggallahir) < 20088 and pper.jeniskelamin ="wanita", 1, 0)),0) as P9,
#55 - 59 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 20088 and DATEDIFF(now(),pper.tanggallahir) <= 21549 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L10,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 20088 and DATEDIFF(now(),pper.tanggallahir) <= 21549 and pper.jeniskelamin ="wanita", 1, 0)),0) as P10,
#60 - 69 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) > 21549 and DATEDIFF(now(),pper.tanggallahir) < 25567 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L11,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) > 21549 and DATEDIFF(now(),pper.tanggallahir) < 25567 and pper.jeniskelamin ="wanita", 1, 0)),0) as P11,
# >= 70 th
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 25567 and pper.jeniskelamin ="laki-laki", 1, 0)),0) as L12,
ifnull(sum(if( rh.iskasuslama="0" and DATEDIFF(now(),pper.tanggallahir) >= 25567 and pper.jeniskelamin ="wanita", 1, 0)),0) as P12
            from rs_jenispenyakit rj
            left join rs_icd ri on ri.idjenispenyakit = rj.idjenispenyakit
            left join rs_hasilpemeriksaan rh on rh.icd = ri.icd
            left join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran and pp.tahunperiksa BETWEEN "'.$tahun1.'" and "'.$tahun2.'" and date(pp.waktuperiksa) BETWEEN "'.$tgl1.'" and "'.$tgl2.'" and (pp.jenispemeriksaan="rajal" or pp.jenispemeriksaan="rajalnap") and pp.idstatuskeluar="2"
            left join person_pasien ppas on ppas.norm = pp.norm
            left join person_person pper on pper.idperson = ppas.idperson
            GROUP by rj.idjenispenyakit, ri.icd';
        return $this->db->query($sql);
    }

    public function report_getdataskdrs_rajal($tgl1,$tgl2,$unit=null)
    {
        $limit=null;
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql = "SELECT  ru.namaunit as namapoli,
                        namapasien(ppas.idperson) as pasien,
                        namadokter(rp.idpegawaidokter) as namaDPJP,
                        rp.diagnosa,
                        pp.bulanperiksa,
                        pp.norm,
                        date(pp.waktuperiksa) as waktumasuk
                FROM person_pendaftaran pp
                    join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran  
                    join person_pasien ppas on ppas.norm = pp.norm
                    join rs_unit ru on ru.idunit = rp.idunit 
                where date(pp.waktuperiksa) BETWEEN '".$tgl1."' AND '".$tgl2."' 
                            and (idstatuskeluar = 2 
                                or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) 
                                or (jenispemeriksaan = 'ranap'  and pp.idstatuskeluar = 1) ) 
                            and rp.idunit='".$unit."'
        order by rp.diagnosa ASC ".$limit;
        return $this->db->query($sql);
    }

    public function report_getdataskdrs_ranap($tgl1,$tgl2,$unit=null)
    {
        $limit=null;
        $whereBangsal = ((empty($unit)) ? '' : ' and rib.idbangsal= '.$unit);
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql =  "select rba.namabangsal as namapoli,
                        CONCAT(rid.icd,' ', ric.namaicd) as diagnosa,
                        namapasien(ppas.idperson) as pasien,
                        pp.norm,
                        date(rin.waktumasuk) as waktumasuk
                from person_pendaftaran pp
                    JOIN rs_inap rin ON rin.idpendaftaran=pp.idpendaftaran
                    join person_pasien ppas on ppas.norm = pp.norm
                    JOIN rs_inapdetail rid ON rid.idinap=rin.idinap
                    JOIN rs_icd ric ON ric.icd=rid.icd
                    left JOIN rs_bed rib ON rib.idbed=rin.idbed 
                    left JOIN rs_bangsal rba ON rba.idbangsal=rib.idbangsal 
                WHERE date(rin.waktumasuk) between '".$tgl1."' and  '".$tgl2."'
                    and (pp.jenispemeriksaan = 'rajalnap' or pp.jenispemeriksaan = 'ranap') 
                    and (pp.idstatuskeluar=1 or pp.idstatuskeluar = 2 )
                    and rin.status!='pesan'
                    and rin.status!='batal'
                    and rid.level='primer' ".$whereBangsal."
                ORDER BY rid.icd ASC;".$limit;        
        return $this->db->query($sql);
    }

    // mahmud, clear -> report balita pneumonia
    public function getreport_balitapneumonia($tgl1,$tgl2)
    {
        $sql = "select DATE_FORMAT(pp.waktu, '%Y-%m-%d') as masuk,ifnull(DATE_FORMAT(inap.waktukeluar, '%Y-%m-%d'),'') as keluar, ri.icd,pp.kondisikeluar , pper.namalengkap, pper.alamat, if(pper.jeniskelamin='wanita',usia(pper.tanggallahir),'') as wanita, if(pper.jeniskelamin='laki-laki',usia(pper.tanggallahir),'') as laki from rs_hasilpemeriksaan rh 
            join rs_icd ri on ri.icd = ri.icd 
            join rs_jenispenyakit rjp on rjp.idjenispenyakit = ri.idjenispenyakit and rjp.jenispenyakit ='pneumonia'
            join person_pendaftaran pp on pp.idpendaftaran = rh.idpendaftaran and pp.jenispemeriksaan!='belum' and pp.idstatuskeluar='2'
            left join rs_inap inap on inap.idpendaftaran = pp.idpendaftaran
            join person_pasien ppas on ppas.norm = pp.norm 
            join person_person pper on pper.idperson = ppas.idperson and date(pper.tanggallahir) BETWEEN '".date('Y-m-d', strtotime(date('Y-m-d').' - 5 year' ))."' and '".date('Y-m-d')."'
            where pp.jenispemeriksaan!='belum' and pp.jenispemeriksaan!='belum' and ri.icd = rh.icd and ri.idjenisicd='2' and date(pp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."'";
        return $this->db->query($sql);
    }
    // mahmud, clear :: AMBIL DATA SIHA
    public function siha_getdatapasien($tgl1,$tgl2)
    {
        $sql="select concat('http://141113126034.ip-dynamic.com:8080/sirstql/cpelayanan/pemeriksaanklinik_view/',rp.idpemeriksaan,'/',ppas.norm) as riwayat, ppd.idpendaftaran, pp.namalengkap, pp.tanggallahir, pp.jeniskelamin from person_pendaftaran ppd, person_pasien ppas, person_person pp, rs_pemeriksaan rp where ppd.idstatuskeluar=1 and date(rp.waktu) BETWEEN '".$tgl1."' and '".$tgl2."' and ppas.norm = ppd.norm and pp.idperson = ppas.idperson and rp.idpendaftaran = ppd.idpendaftaran and ppd.idstatuskeluar='2'";
        $dtperiksa = $this->db->query($sql)->result();
        $diagnosa = [];
        foreach ($dtperiksa as $obj) {
            $diagnosa [] = $this->db->query("select rh.icd, ri.namaicd from rs_hasilpemeriksaan rh, rs_icd ri where rh.idpendaftaran = '".$obj->idpendaftaran."' and ri.icd = rh.icd and ri.idjenispenyakit='54' OR rh.idpendaftaran = '".$obj->idpendaftaran."' and ri.icd = rh.icd and ri.idjenispenyakit='12' OR rh.idpendaftaran = '".$obj->idpendaftaran."' and ri.icd = rh.icd and ri.idjenispenyakit='5'")->result();
        }
        $data = ['dtperiksa'=>$dtperiksa,'dtdiagnosa'=>$diagnosa];
        return $data;
    }

    // mahmud, clear :: report cakupan poli 
    public function cakupanpoli_jumlahpasien($tanggal,$jenisperiksa)
    {
        $bulan = ql_getmonth($tanggal);
        $tahun = ql_getyear($tanggal);
        $wherejenisperiksa = (($jenisperiksa=='rajal') ? " p.jenispemeriksaan='rajal' " : " p.jenispemeriksaan='rajalnap' " );
        // by unit => select (SELECT namaunit from rs_unit where idunit=rj.idunit) unit, p.carabayar, sum(if(ispasienlama='1',0,1)) as pasienbaru, sum(if(ispasienlama='1',1,0)) as pasienlama, sum(1) jumlahpasien, sum(1) jumlahbayar from person_pendaftaran p join rs_pemeriksaan rp on rp.idpendaftaran=p.idpendaftaran join rs_jadwal rj on rj.idjadwal=rp.idjadwal where  MONTH(p.waktu) BETWEEN '1' and '12' AND YEAR(p.waktu) = '2020' AND idstatuskeluar = '2' and rj.idunit=16 and carabayar IN ( select * from (select carabayar from person_pendaftaran where idstatuskeluar=2  GROUP BY carabayar ) as carabayar ) GROUP by p.carabayar
        $sql = "select DATE_FORMAT(p.waktu,'%M-%Y') as waktu, p.carabayar, sum(if(ispasienlama='1',0,1)) as pasienbaru, sum(if(ispasienlama='1',1,0)) as pasienlama, sum(1) jumlahpasien, sum(1) jumlahbayar from person_pendaftaran p where ".$wherejenisperiksa." and MONTH(waktu) = '".$bulan."' AND YEAR(waktu) = '".$tahun."' AND idstatuskeluar = '2' and carabayar IN ( select * from (select carabayar from person_pendaftaran where idstatuskeluar=2 GROUP BY carabayar ) as carabayar ) GROUP by p.carabayar";
        return $this->db->query($sql);
    }

    // mahmud, clear :: report cakupanperpoli 
    public function cakupanperpoli($mode,$tanggal)
    {
        $bulan = date('m', strtotime($tanggal));
        $tahun = date('Y', strtotime($tanggal));
        $wheredate=(($mode=='bulan')?"MONTH(waktuperiksa) = '".$bulan."' AND YEAR(waktuperiksa) = '".$tahun."'":"DATE(waktuperiksa) = '".$tanggal."'");
        $wheredate2=(($mode=='bulan')?"MONTH(p.waktuperiksa) = '".$bulan."' AND YEAR(p.waktuperiksa) = '".$tahun."'":"DATE(p.waktuperiksa) = '".$tanggal."'");
        $sql = "SELECT kec.namakecamatan,(select namakabupatenkota from geografi_kabupatenkota where idkabupatenkota=kec.idkabupatenkota) kabupaten,  UPPER(p.carabayar) as carabayar, COUNT(p.idpendaftaran) pasienperiksa,DATE_FORMAT(p.waktuperiksa,'%d/%M/%Y') as waktu,
        (select count(ispasienlama) from person_pendaftaran where ".$wheredate." AND idstatuskeluar = '2' and ispasienlama='1') as pasienlama,
        (select count(ispasienlama) from person_pendaftaran where ".$wheredate." AND idstatuskeluar = '2' and ispasienlama!='1') as pasienbaru,
        (select count(ispasienlama) from person_pendaftaran where ".$wheredate." AND idstatuskeluar = '2') as jumlahpasien
        FROM
        person_pendaftaran p, person_pasien ppas, person_person pper , geografi_desakelurahan desa, geografi_kecamatan kec
        WHERE ".$wheredate2." AND p.idstatuskeluar = '2' and  ppas.norm=p.norm and pper.idperson=ppas.idperson and desa.iddesakelurahan= pper.iddesakelurahan and kec.idkecamatan = desa.idkecamatan GROUP by p.carabayar,kec.idkecamatan order by  p.carabayar, kec.idkabupatenkota,kec.idkecamatan, p.waktuperiksa";
        return $this->db->query($sql);
    }

    
    /**
     * Get Diagnosa Tersering
     * @param type $tahun
     * @param type $unit
     * @param type $bulan
     * @param type $limit
     * @param type $jenisicd
     * @param type $idicd
     * @return type
     */
    public function get_diagnosatersering($tahun,$unit=null,$bulan=null,$limit=null,$jenisicd=null,$idicd=null)
    {
        $wu    = empty($unit) ? "" : " pp.idunit='".$unit."' and "; 
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql = "SELECT ri.icd, idicd, pp.bulanperiksa, ri.namaicd, sum(rhp.jumlah)  as total, sum(rhp.jumlah)  / (select sum(x.jumlah)  as total from rs_hasilpemeriksaan x join rs_icd y on y.icd=x.icd join person_pendaftaran z on z.idpendaftaran=x.idpendaftaran where y.idjenisicd=ri.idjenisicd and z.tahunperiksa='".$tahun."' and z.bulanperiksa='".$bulan."' and z.bulanperiksa=pp.bulanperiksa) * 100 as percentase
        FROM rs_hasilpemeriksaan rhp 
        join rs_icd ri on ri.icd=rhp.icd
        join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran
        where pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."' and (pp.idstatuskeluar=2 or pp.jenispemeriksaan='rajalnap' and idstatuskeluar=1) and ".$wu." idjenisicd in ('".$jenisicd."') and ri.idicd='".$idicd."'
        group by rhp.icd,idicd,pp.bulanperiksa order by idjenisicd, pp.bulanperiksa, total desc ".$limit;
        return $this->db->query($sql);
    }

    /**
     * [+] get data elektromedik
     */
    public function get_elektromediksering($tahun,$unit=null,$bulan=null,$limit=null,$jenisicd=null)
    {
        $wu    = empty($unit) ? "" : " pp.idunit='".$unit."' and "; 
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql = "SELECT ri.icd, idicd, pp.bulanperiksa, ri.namaicd, sum(rhp.jumlah)  as total, sum(rhp.jumlah)  / (select sum(x.jumlah)  as total from rs_hasilpemeriksaan x join rs_icd y on y.icd=x.icd join person_pendaftaran z on z.idpendaftaran=x.idpendaftaran where y.idjenisicd=ri.idjenisicd and z.tahunperiksa='".$tahun."' and z.bulanperiksa='".$bulan."' and z.bulanperiksa=pp.bulanperiksa) * 100 as percentase
        FROM rs_hasilpemeriksaan rhp 
        join rs_icd ri on ri.icd=rhp.icd
        join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran
        where pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."' and (pp.idstatuskeluar=2 or pp.jenispemeriksaan='rajalnap' and idstatuskeluar=1) and ".$wu." idjenisicd in ('".$jenisicd."')
        group by rhp.icd,idicd,pp.bulanperiksa order by idjenisicd, pp.bulanperiksa, total desc ".$limit;
        return $this->db->query($sql);
    }

    public function viewpasiencovid19($tanggalperiksa)
    {
        return $this->db->query("select rp.idpemeriksaan, kec.kode_kemkes idkecamatan, rpw.serviceintegrasi, (select idstatusrawatwabah from rs_penangananwabah x where x.idpenangananwabah=rpw.idpenangananwabah) idstatusrawatwabah,rpw.idpenangananwabah, rpw.idsebabpenularan, date_format(rp.waktuperiksadokter,'%Y-%m-%d') tanggalperiksa, rp.norm, pper.nik, pper.namalengkap, pper.tanggallahir,  if(pper.jeniskelamin='laki-laki','1','2') gender, pper.iddesakelurahan, pper.notelpon, if(daftar.kondisikeluar='dirujuk','3',if(daftar.kondisikeluar='sembuh','1',if(daftar.kondisikeluar='meninggal','2',if(daftar.kondisikeluar='hidup','0','4')))) statuskeluar
from rs_pemeriksaan rp, rs_pemeriksaan_wabah rpw, person_pasien pp, person_person pper, person_pendaftaran daftar, geografi_desakelurahan desa, geografi_kecamatan kec
where rpw.idpemeriksaan=rp.idpemeriksaan and rpw.serviceintegrasi!=3 and date(rp.waktuperiksadokter) ='".$tanggalperiksa."' and pp.norm=rp.norm and pper.idperson=pp.idperson and daftar.idpendaftaran=rp.idpendaftaran and desa.iddesakelurahan=pper.iddesakelurahan and kec.idkecamatan=desa.idkecamatan")->result_array();
    }
    
    /**
     * Get Data Laboratorium Rawat Inap
     * @param type $idrencanamedispemeriksaan
     */
    public function view_hasillaboratoriumrawatinap($idrencanamedispemeriksaan,$mode,$idpaketpemeriksaan=null)
    {
        if($mode=='view'){
            $idunit = $this->db->query("select pp.idunit from rs_inap_rencana_medis_pemeriksaan rimp, rs_inap ri, person_pendaftaran pp where ri.idinap = rimp.idinap and pp.idpendaftaran = ri.idpendaftaran and rimp.idrencanamedispemeriksaan='$idrencanamedispemeriksaan'")->row_array();
            $hakaksesinput = $this->mgenerikbap->select_multitable("hakinputidjenisicd","rs_unit",array("idunit"=>$idunit['idunit']))->row_array();
        }
        $where_idpaket = ($idpaketpemeriksaan !=null && $mode=='cetakpaket') ? " and rh.idpaketpemeriksaanparent='".$idpaketpemeriksaan."' " : "";
        $where_nonpaket = (($mode=='cetaknonpaket') ? ' and rh.idpaketpemeriksaanparent is null  ' : '' );
        $data = $this->db->query("select "
                . "rh.idpaketpemeriksaanparent, "
                . "rpp2.namapaketpemeriksaan as namapaketpemeriksaanparent,"
                . "rpp.idpaketpemeriksaan, rpp.namapaketpemeriksaan, "
                . "rh.idrencanamedishasilpemeriksaan, rh.idgrup, "
                . "rh.icd, rh.statuspelaksanaan, ri.namaicd, ri.aliasicd,kesimpulan_hasilicd(rh.icd,rh.nilaitext) as kesimpulanhasil, ri.metode,"
                . "rh.total, rh.jumlah, ifnull(ri.nilaidefault,'') as nilaidefault, "
                . "ri.satuan, concat( ifnull(ri.nilaiacuanrendah,''),if(ri.nilaiacuantinggi='','','-'), ifnull(ri.nilaiacuantinggi,'')) as nilairujukan, "
                . (($mode=='view')?"instr('".$hakaksesinput['hakinputidjenisicd']."', rpp.idjenisicd) as isbolehinput, " : "")
                . "ri.istext,  rh.nilai, rh.nilaitext, rikh.keteranganhasil "
                . "from rs_inap_rencana_medis_hasilpemeriksaan rh "
                . "left join rs_icd ri on ri.icd = rh.icd "
                . "left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rh.idpaketpemeriksaan "
                . "left join rs_paket_pemeriksaan rpp2 on rpp2.idpaketpemeriksaan=rpp.idpaketpemeriksaanparent "
                . "left join rs_inap_keteranganhasilpemeriksaan rikh on rikh.idrencanamedispemeriksaan=rh.idrencanamedispemeriksaan and rikh.idpaketpemeriksaan=rh.idpaketpemeriksaanparent"
                . " where rh.idrencanamedispemeriksaan='$idrencanamedispemeriksaan' and (ri.idjenisicd='4' or rpp.idjenisicd='4') ". $where_idpaket.$where_nonpaket
                . "order by idrencanamedishasilpemeriksaan asc")->result();
        return $data;
    }
    
    public function view_hasilradiologiranap($idrencanamedispemeriksaan)
    {
        $data = $this->db->query("select rh.idrencanamedishasilpemeriksaan, rh.icd, rh.statuspelaksanaan, ri.namaicd, ri.aliasicd, rh.total, rh.jumlah, ifnull((select count(1) from rs_inap_rencana_medis_hasilpemeriksaan where idrencanamedispemeriksaan=rh.idrencanamedispemeriksaan and rh.icd = icd and statuspelaksanaan='rencana'), 0) as jumlahrencana  from rs_inap_rencana_medis_hasilpemeriksaan rh, rs_icd ri where rh.icd = ri.icd and rh.idrencanamedispemeriksaan='".$idrencanamedispemeriksaan."' and ri.idjenisicd='5'")->result();
        return $data;
    }
    
    public function view_hasildiagnosaranap($idrencanamedispemeriksaan)
    {
        $data = $this->db->query("select rh.idrencanamedishasilpemeriksaan, rh.icd, rh.statuspelaksanaan, ri.namaicd, ri.aliasicd from rs_inap_rencana_medis_hasilpemeriksaan rh join rs_icd ri on rh.icd = ri.icd  where rh.idrencanamedispemeriksaan='".$idrencanamedispemeriksaan."' and ri.idjenisicd='2'")->result();
        return $data;
    }
    
    public function view_pendapatan_potonggaji_ralan($tgl1,$tgl2)
    {
        $sql = $this->db->query("select kt.penanggung, pp.idpendaftaran, pp.norm, namapasien(ppas.idperson) as pasien, kt.tanggaltagih, kt.idpendaftaran, (kt.dibayar-kt.kembalian) as nominal 
            from keu_tagihan kt 
        join person_pendaftaran pp on pp.idpendaftaran = kt.idpendaftaran
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson
        WHERE jenispembayaran = 'potonggaji' and kt.tanggaltagih BETWEEN '".$tgl1."' and '".$tgl2."' order by pper.namalengkap");
        return $sql;
    }
    
    public function view_pendapatan_potonggaji_penjualanbebas($tgl1,$tgl2)
    {
        $sql = $this->db->query("SELECT kt.penanggung, rb.idbarangpembelibebas, date(kt.waktubayar) as tanggal,(kt.dibayar - kt.kembalian) as nominal, if( rb.idpegawai is null or  rb.idpegawai = 0, rb.nama,namadokter(pp.idpegawai)) as pegawai
        FROM keu_tagihan_bebas kt 
        join rs_barang_pembelibebas rb on rb.idbarangpembelibebas = kt.idbarangpembelibebas
        left join person_pegawai pp on pp.idpegawai = rb.idpegawai
        left join person_person pper on pper.idperson = pp.idperson
        WHERE kt.carabayar ='potonggaji' and date(waktubayar) BETWEEN '".$tgl1."' and '".$tgl2."' and kt.status = 'selesai' order by pper.namalengkap,rb.nama");
        return $sql;
    }
    
    public function view_kunjunganpasienumum($tahun,$bulan)
    {
        $sql = $this->db->query("SELECT pp.jenispemeriksaan, pp.norm, pper.nik, pper.namalengkap, date(pp.waktuperiksa) as tanggalperiksa, pper.alamat, pper.notelpon, pper.tanggallahir, pper.jeniskelamin
        FROM  person_pendaftaran pp
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and ( pp.idstatuskeluar='2' or pp.jenispemeriksaan = 'rajalnap' ) and pp.carabayar = 'mandiri'");
        return $sql;
    }
    
    public function view_kunjunganpasienkedokteranjiwa($tahun,$bulan)
    {
        $sql = $this->db->query("SELECT pp.norm, pper.nik, pper.namalengkap, date(pp.waktuperiksa) as tanggalperiksa,pper.tanggallahir, pper.jeniskelamin, namaunit(rp.idunit) as poli, al.namaloket
        FROM  person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran
        join person_pasien ppas on ppas.norm = pp.norm
        join person_person pper on pper.idperson = ppas.idperson        
        join antrian_loket al on al.idloket = rp.idloket
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and  pp.idstatuskeluar='2' and rp.idunit = 27
        ORDER by pper.jeniskelamin asc");
        return $sql;
    }

    public function view_laporankondisikeluar($tahun)
    {
        $idstatuskeluar1 = "1";
        $idstatuskeluar2 = "2";
        $jenispemeriksaan = "rajalnap";

        $query = $this->db->query(" select 	pp.kondisikeluar,
		                                    sum(1) as jumlahpasien 
                                    FROM 
                                            person_pendaftaran pp 
                                    WHERE 
                                            pp.tahunperiksa='".$tahun."' 
                                            AND (pp.idstatuskeluar='".$idstatuskeluar2."'
                                                OR (pp.idstatuskeluar='".$idstatuskeluar1."' AND pp.jenispemeriksaan='".$jenispemeriksaan."')
                                                ) 
                                    GROUP by pp.kondisikeluar")->result_array();
        return $query;
    }


    public function view_rs_barang_stok_perbandingan_join()
    {
        $sql = $this->db->select('rb.namabarang, rb.kode, rbsp.stoklama, rbsp.stokbaru, rs.namasatuan');
        $sql = $this->db->join('rs_barang_stok_perbandingan rbsp','rbsp.idbarang = rb.idbarang');        
        $sql = $this->db->join('rs_satuan rs','rs.idsatuan = rb.idsatuan','left');
        $sql = $this->db->where('rbsp.idunit',$this->session->userdata('idunitterpilih'));
        $sql = $this->db->get('rs_barang rb')->result_array();
        return $sql;
    }
    
    //get nilai konfigurasi
    public function getKonfigurasi($nama)
    {
        $sql = $this->db->select('nilai')->get_where('konfigurasi',['nama'=>$nama])->row_array();
        $result = $sql['nilai'];
        return $result;
    }
    
    public function get_data_enum($table, $column,$dbname='db_sirstql') // ambil data enum dari database 
    {
        if(file_exists("./QLKP.php")){
            $dbname='db_sirstql_270721';
        } else {
            $dbname='db_sirstql';
        }
        
            $query = $this->db->query("SELECT COLUMN_TYPE  FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$column."'");

            if($query->num_rows() > 0)
            {

                    foreach($query->result_array() AS $row) 
                    {
                    $result = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE'])-6))));
            }
                    return $result;
            }
            else
            {
                    return NULL;
            }
    }
    
    //-- get data laporan penggunaan obat bulanan - Rawat Jalan
    public function view_laporanpenggunaanobatperbulan($tahunbulan)
    {
        $tahun = date('Y',  strtotime($tahunbulan));
        $bulan = date('n',  strtotime($tahunbulan));
        $sql = "select rb.namabarang, rb.hargajual as hargabpjs, rb.hargaumum, rb.hargabeli,
            (SELECT ifnull(sum(rbp.jumlahpemakaian),0) FROM person_pendaftaran pp 
            JOIN rs_barangpemeriksaan rbp on rbp.idpendaftaran = pp.idpendaftaran
            WHERE  pp.tahunperiksa= '$tahun' and pp.bulanperiksa = '$bulan' and pp.idstatuskeluar = 2 and rbp.idbarang = rb.idbarang) as resep,
            (SELECT ifnull(sum(rbp.jumlahpemakaian),0)
            FROM keu_tagihan_bebas ktb 
            JOIN rs_barang_pembelibebas a on a.idbarangpembelibebas = ktb.idbarangpembelibebas 
            JOIN rs_barang_pembelianbebas rbp on rbp.idbarangpembelibebas = a.idbarangpembelibebas
            WHERE a.tahun = '$tahun' and a.bulan = '$bulan' and ktb.status ='selesai'and rbp.idbarang = rb.idbarang) as bebas
            from rs_barang rb";
        
        $dt = $this->db->query($sql)->result();
        return $dt;
    }
    
    //-- get data laporan penggunaan obat bulanan - Rawat Inap
    public function view_laporanpenggunaanobatperbulanRanap($tahunbulan)
    {
        $tahunbulanStr = date('Yn',  strtotime($tahunbulan));
        
        $sql = "SELECT 	rb.namabarang, 
                        ifnull(sum(rirmb.jumlah),0) as jumlah,
                        rb.hargabeli,
                        rb.hargajual as hargabpjs, 
                        rb.hargaumum 
                FROM rs_inap_rencana_medis_barangpemeriksaan rirmb 
                    join rs_inap_rencana_medis_pemeriksaan rirmp on rirmp.idrencanamedispemeriksaan = rirmb.idrencanamedispemeriksaan
                    join rs_barang rb on rb.idbarang = rirmb.idbarang
                    left join rs_satuan rs on rs.idsatuan = rirmb.idsatuanpemakaian
                where rirmb.statuspelaksanaan='terlaksana'
                    AND rirmp.tahunbulan = '$tahunbulanStr'
                GROUP BY rirmb.idbarang
                ORDER BY rb.namabarang ASC";
        $dt = $this->db->query($sql)->result();
        return $dt;
    }
    
    //-- get data laporan distribusi gudang
    public function view_laporandistribusigudang()
    {
        if($this->input->post('mode') == 'detaildistribusigudang'){
            $tanggal1 = $this->input->post('tanggal1');
            $tanggal2 = $this->input->post('tanggal2');
            $where = "WHERE tanggalfaktur BETWEEN '".$tanggal1."' and '".$tanggal2."'";
        }else{
            $tahun = date('Y',  strtotime($this->input->post('tahunbulan')));
            $bulan = date('n',  strtotime($this->input->post('tahunbulan')));
            $where = "WHERE tahunfaktur = '".$tahun."' and bulanfaktur='".$bulan."'";
        }
        $sql = "SELECT namabarang, sum(jumlah) as jumlah, namasatuan, jenisdistribusi FROM vrs_laporan_distribusi_gudang
            ".$where."
        GROUP BY jenisdistribusi, idbarang
        ORDER by idbarang asc";
        $dt = $this->db->query($sql)->result();
        return $dt;
    }
    
    //-- get data tindakan rawat jalan
    public function get_data_tindakanrawtjalan_all($tahun,$bulan)
    {
        $query = $this->db->query("select rh.icd, ri.namaicd, rh.idpaketpemeriksaan, rp.namapaketpemeriksaan, rh.idgrup,rh.total
        from person_pendaftaran pp 
        join rs_hasilpemeriksaan rh on rh.idpendaftaran = pp.idpendaftaran and rh.total > 0
        left join rs_icd ri on ri.icd = rh.icd
        left join rs_paket_pemeriksaan rp on rp.idpaketpemeriksaan = rh.idpaketpemeriksaan
        WHERE pp.tahunperiksa = '".$tahun."' and pp.bulanperiksa = '".$bulan."' and (pp.idstatuskeluar = 2 or (pp.jenispemeriksaan ='rajalnap' and pp.idstatuskeluar != 0 and pp.idstatuskeluar != 3 and pp.idstatuskeluar != 4) ) order by rh.icd, rh.idpaketpemeriksaan asc ")->result_array();
        
        return $query;
    }
    
    //--get data jadwal untuk singkronisasi ke website QL
    public function getdata_singkronisasijadwaldokterwebsite($mode,$idunit,$idjadwal=0)
    {
        $sqlinmode = '';
        
        if($mode == 'upload_jadwal')
        {
            $sqlinmode = " and time(rj.tanggal) != '00:00:00' and rj.jamakhir != '00:00:00' and date(rj.tanggal) >= date(now()) and rj.idunit = '".$idunit."' and rj.singkronisasi=0
        ORDER by rj.tanggal asc limit 20";
        }
        else if($mode == 'hapus_jadwal' or $mode == 'update_jadwal')
        {
            $sqlinmode = " and rj.idjadwal = '" . $idjadwal . "' ";
        }
        else if($mode == 'upload_jadwalper2jam')
        {
            $sqlinmode = " and time(rj.tanggal) != '00:00:00' and rj.jamakhir != '00:00:00' and date(rj.tanggal) >= date(now())  and rj.singkronisasi='0' and ru.singkronjadwalweb = '1'
        ORDER by rj.tanggal asc limit 20  ";
        }
        
        $sql = "SELECT rj.idjadwal, rj.idunit,ru.namaunit, date(rj.tanggal) as tanggal, time(rj.tanggal) as jammulai, rj.jamakhir, rj.idpegawaidokter, namadokter(rj.idpegawaidokter) as namadokter, al.namaloket, date_format(rj.tanggal,'%w') as hari, pper.jeniskelamin
        FROM rs_jadwal rj
        join antrian_loket al on al.idloket = rj.idloket
        join rs_unit ru on ru.idunit = rj.idunit
        join person_pegawai ppai on ppai.idpegawai = rj.idpegawaidokter
        join person_person pper on pper.idperson = ppai.idperson
        WHERE ppai.idprofesi = 1 and al.namaloket != 'ugd' and al.namaloket != 'poli umum' ".$sqlinmode;
        
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function pemeriksaanranapdetail_identitas($idinap)
    {
        $data = "";

        $cekSignature = $this->db->query( 
                            "SELECT ri.* 
                            FROM rs_inap ri 
                            JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter
                            join person_signature persign on persign.id_person = perpeg.idperson
                            WHERE ri.idinap = '".$idinap."'"
                        )->row_array();

        $cekBangsal = $this->db->query( 
                            "SELECT ri.* 
                            FROM rs_inap ri 
                            JOIN rs_bed rb on rb.idbed = ri.idbed
                            JOIN rs_bangsal bs on bs.idbangsal = rb.idbangsal
                            WHERE ri.idinap = '".$idinap."'"
                        )->row_array();
        
        $sql = "SELECT ".((empty($cekSignature))?"'' as signature," : "persign.signature,")."ri.idpendaftaran, ri.is_signature, ri.idpegawaidokter, namadokter(ri.idpegawaidokter) as namadokter, pp.norm, pp.waktuperiksa as mulaipoli, pper.namalengkap, pper.tanggallahir, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.alamat, ri.waktumasuk, ri.waktukeluar, rk.kelas,"
                .((empty($cekBangsal))?"'' as nobed, '' as namabangsal," : "rb.nobed, bs.namabangsal,")
                ."ri.indikasirawatinap, ri.tindakanproseduroperasi, ri.administrasimasuk, ri.diagnosis, ri.pilihan, ri.pilihanruang, ri.pilihankelas, ri.komordibitaslain, ri.ringkasanriwayat_pemeriksaanfisik,ri.terapiselamaranap,ri.carapulang,ri.kondisisaatpulang,ri.anjuran,ri.tindaklanjut,ri.resikotinggi,ri.hasilpemeriksaanpenunjang,ri.terapipulang, ri.diagnosisDPJP
                FROM rs_inap ri 
                JOIN person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
                JOIN person_pasien ppas on ppas.norm = pp.norm
                JOIN person_person pper on pper.idperson = ppas.idperson
                JOIN rs_kelas rk on rk.idkelas = ri.idkelas"
                .((empty($cekBangsal))?"" : " JOIN rs_bed rb on rb.idbed = ri.idbed")
                ." JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter"
                .((empty($cekSignature))?"" : " JOIN person_signature persign on persign.id_person = perpeg.idperson")
                .((empty($cekBangsal))?"" : " JOIN rs_bangsal bs on bs.idbangsal = rb.idbangsal")
                ." WHERE ri.idinap = '".$idinap."'";

        $data = $this->db->query($sql)->row_array();
        return $data;

        //old

        // $sql = "SELECT persign.signature,ri.idpendaftaran, ri.is_signature, ri.idpegawaidokter, namadokter(ri.idpegawaidokter) as namadokter, pp.norm, pper.namalengkap, pper.tanggallahir, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.alamat, ri.waktumasuk, ri.waktukeluar, rk.kelas, rb.nobed, bs.namabangsal,
        //         ri.indikasirawatinap, ri.administrasimasuk, ri.diagnosis, ri.komordibitaslain, ri.ringkasanriwayat_pemeriksaanfisik,ri.terapiselamaranap,ri.carapulang,ri.kondisisaatpulang,ri.anjuran,ri.tindaklanjut,ri.resikotinggi,ri.hasilpemeriksaanpenunjang,ri.terapipulang, ri.diagnosisDPJP
        //         FROM rs_inap ri 
        //         JOIN person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
        //         JOIN person_pasien ppas on ppas.norm = pp.norm
        //         JOIN person_person pper on pper.idperson = ppas.idperson
        //         JOIN rs_kelas rk on rk.idkelas = ri.idkelas
        //         JOIN rs_bed rb on rb.idbed = ri.idbed
        //         JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter
        //         join person_signature persign on persign.id_person = perpeg.idperson
        //         JOIN rs_bangsal bs on bs.idbangsal = rb.idbangsal
        //         WHERE ri.idinap = '".$idinap."'";


        // if(empty($this->db->query($sql)->row_array()))
        // {
        //     $sqltanpasignature = "SELECT ri.idpendaftaran, ri.is_signature, ri.idpegawaidokter, namadokter(ri.idpegawaidokter) as namadokter, pp.norm, pper.namalengkap, pper.tanggallahir, usia(pper.tanggallahir) as usia, pper.jeniskelamin, pper.alamat, ri.waktumasuk, ri.waktukeluar, rk.kelas, rb.nobed, bs.namabangsal,
        //     ri.indikasirawatinap, ri.administrasimasuk, ri.diagnosis, ri.komordibitaslain, ri.ringkasanriwayat_pemeriksaanfisik,ri.terapiselamaranap,ri.carapulang,ri.kondisisaatpulang,ri.anjuran,ri.tindaklanjut,ri.resikotinggi,ri.hasilpemeriksaanpenunjang,ri.terapipulang, ri.diagnosisDPJP
        //     FROM rs_inap ri 
        //     JOIN person_pendaftaran pp on pp.idpendaftaran = ri.idpendaftaran
        //     JOIN person_pasien ppas on ppas.norm = pp.norm
        //     JOIN person_person pper on pper.idperson = ppas.idperson
        //     JOIN rs_kelas rk on rk.idkelas = ri.idkelas
        //     JOIN rs_bed rb on rb.idbed = ri.idbed
        //     JOIN person_pegawai perpeg on perpeg.idpegawai = ri.idpegawaidokter
        //     JOIN rs_bangsal bs on bs.idbangsal = rb.idbangsal
        //     WHERE ri.idinap = '".$idinap."'"; 

        //     $data = $this->db->query($sqltanpasignature)->row_array();
        // }
        // else
        // {
        //     $data = $this->db->query($sql)->row_array();
        // }
        
        // return $data;
    }
    
    public function view_cakupanpasiencarabayar()
    {
        $tahunbulan = $this->input->post('tahunbulan');
        $tahun = date('Y',  strtotime($tahunbulan));
        $bulan = date('n',  strtotime($tahunbulan));
        $query = $this->db->query("select sum(1) as jumlahpasien, pp.carabayar from person_pendaftaran pp WHERE pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."' and pp.idstatuskeluar=2 GROUP by pp.carabayar")->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolispesialis_pasienlamabaru($tahun,$bulan,$idunit)
    {
        $whereunit = ((empty($idunit)) ? "" : "and pp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(ispasienlama=0,1,0)) as pasienbaru, sum(if(ispasienlama=1,1,0)) as pasienlama, namadokter(rp.idpegawaidokter) as namadokter
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) ".$whereunit."
        GROUP by rp.idpegawaidokter";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolispesialis_jenisperiksa($tahun,$bulan,$idunit)
    {
        $whereunit = ((empty($idunit)) ? "" : "and pp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(pp.jenispemeriksaan ='ranap' or pp.jenispemeriksaan = 'rajalnap',1,0)) as pasienranap,  sum(if(pp.jenispemeriksaan = 'rajal',1,0)) as pasienralan, namadokter(rp.idpegawaidokter) as namadokter
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) ".$whereunit."
        GROUP by rp.idpegawaidokter";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolispesialis_carabayar($tahun,$bulan,$idunit)
    {
        $whereunit = ((empty($idunit)) ? "" : "and pp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(pp.carabayar = 'mandiri',1,0)) as mandiri,
        sum(if(pp.carabayar = 'jknnonpbi',1,0)) as jknnonpbi,
        sum(if(pp.carabayar = 'jknpbi',1,0)) as jknpbi,
        sum(if(pp.carabayar = 'asuransi lain',1,0)) as asuransilain,
        sum(if(pp.carabayar = 'jamkesos',1,0)) as jamkesos,
        sum(if(pp.carabayar = 'jampersal',1,0)) as jampersal,
        sum(if(pp.carabayar = 'jasaraharja',1,0)) as jasaraharja,
        sum(if(pp.carabayar = 'bpjstenagakerja',1,0)) as bpjstenagakerja,
        namadokter(rp.idpegawaidokter) as namadokter
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.idpemeriksaansebelum is null
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) ".$whereunit."
        GROUP by rp.idpegawaidokter";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolikhusus_pasienlamabaru($tahun,$bulan,$idunit)
    {
        $whereunit = ((empty($idunit)) ? "" : "and rp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(ispasienlama=0,1,0)) as pasienbaru, sum(if(ispasienlama=1,1,0)) as pasienlama
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.polikhususakhir = 1
        WHERE pp.tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."'   and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) ".$whereunit;
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolikhusus_jenisperiksa($tahun,$bulan,$idunit)
    {       
        $whereunit = ((empty($idunit)) ? "" : "and rp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(pp.jenispemeriksaan ='ranap' or pp.jenispemeriksaan = 'rajalnap',1,0)) as pasienranap,  sum(if(pp.jenispemeriksaan = 'rajal',1,0)) as pasienralan
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.polikhususakhir = 1
        WHERE pp.tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."'   and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1 ) ) ".$whereunit;
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function getdata_cakupanpolikhusus_carabayar($tahun,$bulan,$idunit)
    {
        $whereunit = ((empty($idunit)) ? "" : "and rp.idunit = '".$idunit."'" );
        $sql = "SELECT sum(if(pp.carabayar = 'mandiri',1,0)) as mandiri,
        sum(if(pp.carabayar = 'jknnonpbi' or pp.carabayar = 'jknpbi',1,0)) as jkn, 
        sum(if(pp.carabayar = 'asuransi lain',1,0)) as asuransilain,
        sum(if(pp.carabayar = 'jamkesos',1,0)) as jamkesos,
        sum(if(pp.carabayar = 'jampersal',1,0)) as jampersal,
        sum(if(pp.carabayar = 'jasaraharja',1,0)) as jasaraharja,
        sum(if(pp.carabayar = 'bpjstenagakerja',1,0)) as bpjstenagakerja,
        namadokter(rp.idpegawaidokter) as namadokter
        FROM person_pendaftaran pp
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.polikhususakhir = 1
        WHERE tahunperiksa = '".$tahun."' and bulanperiksa = '".$bulan."' and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) ".$whereunit;
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function get_diagnosaterseringpolikhusus($tahun,$unit=null,$bulan=null,$limit=null,$jenisicd=null,$idicd=null)
    {
        $wu    = empty($unit) ? "" : " rp.idunit='".$unit."' and "; 
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql = "SELECT ri.icd, idicd, pp.bulanperiksa, ri.namaicd, sum(rhp.jumlah)  as total, sum(rhp.jumlah)  / (select sum(x.jumlah)  as total from rs_hasilpemeriksaan x join rs_icd y on y.icd=x.icd join person_pendaftaran z on z.idpendaftaran=x.idpendaftaran where y.idjenisicd=ri.idjenisicd and z.tahunperiksa='".$tahun."' and z.bulanperiksa='".$bulan."' and z.bulanperiksa=pp.bulanperiksa) * 100 as percentase
        FROM rs_hasilpemeriksaan rhp 
        join rs_icd ri on ri.icd=rhp.icd
        join person_pendaftaran pp on pp.idpendaftaran=rhp.idpendaftaran
        join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran and rp.polikhususakhir = 1
        where pp.tahunperiksa='".$tahun."' and pp.bulanperiksa='".$bulan."' and (pp.jenispemeriksaan='rajal' or pp.jenispemeriksaan='rajalnap') and ".$wu." idjenisicd in ('".$jenisicd."') and ri.idicd='".$idicd."'
        group by rhp.icd,idicd,pp.bulanperiksa order by idjenisicd, pp.bulanperiksa, total desc ".$limit;
        return $this->db->query($sql);
    }
//$tahun,$idunit,$bulan,0
    public function get_diagnosaDPJPpolikhusus($tahun,$unit=null,$bulan=null,$limit=null)
    {
        $wu    = empty($unit) ? "" : " rp.idunit='".$unit."' and "; 
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql = "SELECT  rp.diagnosa,
                        pp.bulanperiksa,
                        COUNT(rp.diagnosa) as total
                FROM person_pendaftaran pp
                    join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran 
                where pp.tahunperiksa='".$tahun."' 
                        and pp.bulanperiksa='".$bulan."' 
                        and (idstatuskeluar = 2 
                            or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) 
                            or (jenispemeriksaan = 'ranap'  and pp.idstatuskeluar = 1) ) 
                        and rp.polikhususakhir = 1
                        and ".$wu." rp.polikhususakhir = 1
        group by rp.diagnosa 
        order by rp.diagnosa ASC ".$limit;
        return $this->db->query($sql);
    }
    
    /**
     * Cakupan Kamar Operasi by Carabayar
     * @param type $tahunbulan  -> example 202101
     */
    public function getdata_cakupankamaroperasi_carabayar($tahunbulan)
    {
        $sql = "SELECT 
                sum(if(jaminan = 'mandiri',1,0)) as umum, 
                sum(if(jaminan = 'jknpbi',1,0)) as jknpbi, 
                sum(if(jaminan = 'jknnonpbi',1,0)) as jknnonpbi, 
            sum(if(jaminan = 'asuransi lain',1,0)) as asuransilain,     
            sum(if(jaminan = 'jamkesos',1,0)) as jamkesos,
            sum(if(jaminan = 'jampersal',1,0)) as jampersal,
            sum(if(jaminan = 'jasaraharja',1,0)) as jasaraharja,
            sum(if(jaminan = 'bpjstenagakerja',1,0)) as bpjstenagakerja, 
            sum(1) as total,
                namadokter(rjo.iddokteroperator) as namadokter
        FROM rs_jadwal_operasi rjo 
        WHERE rjo.tahunbulan = '".$tahunbulan."' and rjo.terlaksana = 1
        GROUP by rjo.iddokteroperator";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    /**
     * Cakupan Kamar Operasi by Jenis Tindakan
     * @param type $tahunbulan
     * @return type
     */
    public function getdata_cakupankamaroperasi_jenistindakan($tahunbulan)
    {
        $sql = "select rj.jenistindakan, ro.jenisoperasi from rs_jadwal_operasi rjo 
                join rs_operasi ro on ro.idjadwaloperasi = rjo.idjadwaloperasi
                join rs_jenistindakanoperasi rj on rj.idjenistindakan = rjo.idjenistindakan
                WHERE rjo.tahunbulan = '".$tahunbulan."' and rjo.terlaksana = 1
                order by jenistindakan";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    
    /**
     * Cakupan Kamar Operasi by Dokter Anastesi
     * @param type $tahunbulan
     */
    public function getdata_cakupankamaroperasi_dranastesi($tahunbulan)
    {
        $sql = "select sum(1) as jumlah, namadokter(ro.idpegawaidokteranestesi) as namadokter from rs_jadwal_operasi rjo 
                join rs_operasi ro on ro.idjadwaloperasi = rjo.idjadwaloperasi
                join rs_jenistindakanoperasi rj on rj.idjenistindakan = rjo.idjenistindakan
                WHERE rjo.tahunbulan = '".$tahunbulan."' and rjo.terlaksana = 1 and ro.idpegawaidokteranestesi > 0
                GROUP by ro.idpegawaidokteranestesi";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    /**
     * Cakupan Kamar Operasi by Dokter Anak/Umum
     * @return type
     */
    public function getdata_cakupankamaroperasi_laporansc($tahunbulan)
    {
        $sql = "select sum(1) as jumlah, namadokter(ro.idpegawaidokteranak) as namadokter from rs_jadwal_operasi rjo 
                join rs_operasi ro on ro.idjadwaloperasi = rjo.idjadwaloperasi
                join rs_jenistindakanoperasi rj on rj.idjenistindakan = rjo.idjenistindakan
                WHERE rjo.tahunbulan = '".$tahunbulan."' and rjo.terlaksana = 1 and ro.isoperasisc = 1
                GROUP by ro.idpegawaidokteranak";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    
    public function getdata_cakupanranap_pasienlamabaru($tahun,$bulan,$pilihBangsal=null)
    {
        $whereBangsal = ((empty($pilihBangsal)) ? '' : ' and rib.idbangsal IN'.$pilihBangsal);
        $sql = "select  sum(if(pp.ispasienlama = 1,1,0)) as pasienlama, 
                        sum(if(pp.ispasienlama=0,1,0)) as pasienbaru
                from person_pendaftaran pp 
                    JOIN rs_inap rin ON rin.idpendaftaran=pp.idpendaftaran
                    left JOIN rs_bed rib ON rib.idbed=rin.idbed 
                WHERE pp.tahunperiksa='".$tahun."' 
                    and pp.bulanperiksa='".$bulan."' 
                    and (pp.jenispemeriksaan = 'rajalnap' or pp.jenispemeriksaan = 'ranap') 
                    and (pp.idstatuskeluar=1 or pp.idstatuskeluar = 2 )
                    and rin.status!='pesan'
                    and rin.status!='batal' ".$whereBangsal;
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }

    public function getdata_cakupanranap_perDPJP($tahun,$bulan,$pilihBangsal=null)
    {
        $whereBangsal = ((empty($pilihBangsal)) ? '' : ' and rib.idbangsal IN'.$pilihBangsal);
        $sql = "select namadokter(rin.idpegawaidokter) as dokter,
                        sum(1) as jumlahpasien
                from person_pendaftaran pp 
                    JOIN rs_inap rin ON rin.idpendaftaran=pp.idpendaftaran 
                    left JOIN rs_bed rib ON rib.idbed=rin.idbed 
                WHERE pp.tahunperiksa='".$tahun."' 
                        and pp.bulanperiksa='".$bulan."' 
                        and (pp.jenispemeriksaan = 'rajalnap' or pp.jenispemeriksaan = 'ranap') 
                        and (pp.idstatuskeluar=1 or pp.idstatuskeluar = 2 )
                        and rin.status!='pesan'
                        and rin.status!='batal' ".$whereBangsal."
                GROUP BY rin.idpegawaidokter
                ORDER BY jumlahpasien DESC";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    
    public function getdata_cakupanranap_pasiencarabayar($tahun,$bulan,$pilihBangsal=null)
    {
        $whereBangsal = ((empty($pilihBangsal)) ? '' : ' and rib.idbangsal IN'.$pilihBangsal);
        $sql = "select sum(1) as jumlahpasien, 
                        pp.carabayar
                from person_pendaftaran pp 
                    JOIN rs_inap rin ON rin.idpendaftaran=pp.idpendaftaran 
                    left JOIN rs_bed rib ON rib.idbed=rin.idbed 
                WHERE pp.tahunperiksa='".$tahun."' 
                    and pp.bulanperiksa='".$bulan."' 
                    and (pp.jenispemeriksaan = 'rajalnap' or pp.jenispemeriksaan = 'ranap') 
                    and (pp.idstatuskeluar=1 or pp.idstatuskeluar = 2 )
                    and rin.status!='pesan'
                    and rin.status!='batal' ".$whereBangsal."
                GROUP by pp.carabayar";
        $hasil = $this->db->query($sql)->result_array();
        return $hasil;
    }
    
    public function get_diagnosatindakan_ranap($tahun,$bulan=null,$limit=null,$jenisicd=null,$idicd=null,$pilihBangsal=null)  // example data from poli spesialis
    {
        $whereBangsal = ((empty($pilihBangsal)) ? '' : ' and rib.idbangsal IN'.$pilihBangsal);
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql =  "SELECT 
                    ri.icd, ri.idicd,
                    pp.bulanperiksa, 
                    ri.namaicd, 
                    sum(inap_rhp.jumlah)  as total
                FROM 
                    rs_inap_rencana_medis_hasilpemeriksaan inap_rhp 
                    JOIN rs_icd ri on ri.icd=inap_rhp.icd
                    JOIN rs_inap rinap on rinap.idpendaftaran=inap_rhp.idpendaftaran
                    JOIN person_pendaftaran pp on pp.idpendaftaran=inap_rhp.idpendaftaran
                    JOIN rs_inap_rencana_medis_pemeriksaan inap_p on inap_p.idrencanamedispemeriksaan=inap_rhp.idrencanamedispemeriksaan
                    left JOIN rs_bed rib ON rib.idbed=rinap.idbed 
                where 
                    pp.tahunperiksa='".$tahun."' 
                    and pp.bulanperiksa='".$bulan."'
                    and ri.idjenisicd in ('".$jenisicd."')
                    AND (
                        inap_rhp.statuspelaksanaan='terlaksana'
                        OR inap_p.status='terlaksana'
                    ) ".$whereBangsal."
                group by 
                    inap_rhp.icd, 
                    ri.idicd,    
                    pp.bulanperiksa
                order by 
                    ri.idjenisicd,
                    pp.bulanperiksa,
                    total desc".$limit;        
        return $this->db->query($sql);
    }

    public function get_diagnosaprimer_ranap($tahun,$bulan=null,$limit=null,$pilihBangsal=null)
    {
        $whereBangsal = ((empty($pilihBangsal)) ? '' : ' and rib.idbangsal IN'.$pilihBangsal);
        $limit = ((empty($limit)) ? '' : ' limit '.$limit );
        $sql =  "select pp.idpendaftaran,
                        rid.idinap inapRID,
                        rid.level,
                        rid.icd,
                        ric.namaicd,
                    COUNT(rid.icd) as total
                from person_pendaftaran pp
                    JOIN rs_inap rin ON rin.idpendaftaran=pp.idpendaftaran
                    JOIN rs_inapdetail rid ON rid.idinap=rin.idinap
                    JOIN rs_icd ric ON ric.icd=rid.icd
                    left JOIN rs_bed rib ON rib.idbed=rin.idbed 
                WHERE pp.tahunperiksa='".$tahun."' 
                    and pp.bulanperiksa='".$bulan."' 
                    and (pp.jenispemeriksaan = 'rajalnap' or pp.jenispemeriksaan = 'ranap') 
                    and (pp.idstatuskeluar=1 or pp.idstatuskeluar = 2 )
                    and rin.status!='pesan'
                    and rin.status!='batal'
                    and rid.level='primer' ".$whereBangsal."
                GROUP BY rid.icd  
                ORDER BY total DESC, rid.icd ASC;".$limit;        
        return $this->db->query($sql);
    }

}
