<?php
 
class Mkeuangan extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function dt_departemen()
    {
        $query = $this->db->get('keu_departemen');
        if($query->num_rows() > 0){return $query;}
        return false;
    }

    public function dt_departemen_($limit=false)
    {
        $column = [];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_departemen();   
    }
    public function total_dt_departemen(){ return $this->recordsTotal($this->dt_departemen());}
    public function filter_dt_departemen(){return $this->recordsFiltered($this->dt_departemen_());}

    public function data_jurnal()
    {
        $query = $this->db->query("SELECT k.kdjurnal, k.idjurnal, k.tanggal, k.keterangan, k.referensi, k.no_referensi, d.nama, k.status,a.kode, a.nama as namaakun, c.deskripsi, c.debet, c.kredit, c.idjurnaldet FROM keu_jurnal k LEFT JOIN keu_departemen d ON d.iddepartemen=k.iddepartemen
            LEFT JOIN keu_jurnaldetail c ON c.kdjurnal=k.kdjurnal LEFT JOIN keu_akun a ON a.idakun=c.idakun");

        return $query->result();
    }
    public function data_akun($where)
    {
        $query = $this->db->query("SELECT a.idakun, a.idtipeakun, b.tipe FROM keu_akun a LEFT JOIN keu_tipeakun b ON b.idtipeakun=a.idtipeakun ".$where);
        return $query->result();
    }
    public function dt_jurnal()
    {
        $query = $this->db->select("k.kdjurnal, k.idjurnal,k.tanggal, k.keterangan, k.referensi, k.no_referensi, d.nama, k.status, k.totaldebet, k.totalkredit");
        $query = $this->db->join('keu_departemen d','d.iddepartemen=k.iddepartemen','left');
        $query = $this->db->where('k.tanggal',$this->input->post('tanggal'));
        $query = $this->db->where('k.referensi',$this->input->post('referensi'));
        $query = $this->db->get('keu_jurnal k');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_jurnal_($limit=false)
    {
        $column = ['k.idjurnal','k.tanggal','k.keterangan','k.totaldebet','k.totalkredit','k.referensi','k.no_referensi','d.nama','k.status'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_jurnal();   
    }
    public function total_dt_jurnal(){ return $this->recordsTotal($this->dt_jurnal());}
    public function filter_dt_jurnal(){return $this->recordsFiltered($this->dt_jurnal_());}


    public function keu_periode()
    {
        $periode = $this->db->get('keu_periode')->row_array();
        return $periode;
    }
    public function view_historyakun()
    {
        $post = $this->input->post();
        $idakun    = $post['idakun'];
        $tglawal   = $post['tanggal_mulai'];
        $tglakhir  = $post['tanggal_selesai'];
        $periode   = $this->keu_periode();
        
        $query = $this->db->select('b.tanggal, a.idjurnaldet, a.kdjurnal, a.idakun, a.debet, a.kredit, a.tahun, a.deskripsi, if(c.isdebit = 1,(a.debet - a.kredit),(a.kredit-a.debet) ) as saldo ');
        $query = $this->db->join('keu_jurnal b','b.kdjurnal=a.kdjurnal','left');
        $query = $this->db->join('keu_tipe c','c.tipe = a.tipe','left');
        $query = $this->db->where('a.idakun',$idakun);
        $query = $this->db->where('b.status',1);
        $query = $this->db->where('date(b.tanggal) BETWEEN "'.$tglawal.'" and "'.$tglakhir.'"');
        $query = $this->db->get('keu_jurnaldetail a')->result_array();
        
        $data['laporan'] = $query;
        $data['saldoawal'] = $this->db
                ->select("  if(kt.isdebit = 1,(debetawal - kreditawal), (kreditawal - debetawal) )  as saldo ")
                ->join('keu_tipe kt','kt.tipe = ktb.tipe')
                ->get_where('keu_tutup_buku ktb',['tahun'=>$periode['tahun'],'bulan'=>$periode['bulan'],'idakun'=>$idakun])
                ->row_array();
        return $data;
    }

    
    public function get_historyakun($idakun, $tglawal, $tglakhir)
    {
        $idakun    = $idakun;
        $tglawal   = $tglawal;
        $tglakhir  = $tglakhir;
        $periode   = $this->keu_periode();
        
        $query = $this->db->select('b.tanggal, a.idjurnaldet, a.kdjurnal, a.idakun, a.debet, a.kredit, a.tahun, a.deskripsi, if(c.isdebit = 1,(a.debet - a.kredit),(a.kredit-a.debet) ) as saldo ');
        $query = $this->db->join('keu_jurnal b','b.kdjurnal=a.kdjurnal','left');
        $query = $this->db->join('keu_tipe c','c.tipe = a.tipe','left');
        $query = $this->db->where('a.idakun',$idakun);
        $query = $this->db->where('b.status',1);
        $query = $this->db->where('date(b.tanggal) BETWEEN "'.$tglawal.'" and "'.$tglakhir.'"');
        $query = $this->db->get('keu_jurnaldetail a')->result_array();
        
        $data['laporan'] = $query;
        $data['saldoawal'] = $this->db
                ->select("  if(kt.isdebit = 1,(debetawal - kreditawal), (kreditawal - debetawal) )  as saldo ")
                ->join('keu_tipe kt','kt.tipe = ktb.tipe')
                ->get_where('keu_tutup_buku ktb',['tahun'=>$periode['tahun'],'bulan'=>$periode['bulan'],'idakun'=>$idakun])
                ->row_array();
        return $data;
    }
    
    public function view_dataakunsaldo()
    {
        
       $periode = $this->keu_periode();
       $tahun = $periode['tahun'];
       $bulan = $periode['bulan'];
       $sql="SELECT 
        c.idakun, 
        b.tipe as tipe_akun,
        a.kode as kode_tipe, 
        a.idtipeakun, 
        a.nama as nama_tipe, 
        c.kode as kode_akun, 
        c.nama as nama_akun, 
        e.debetawal, 
        e.kreditawal,
        if(a.isdebit = 1 , 
            (ifnull(e.debetawal,0)+ifnull(sum(d.debet),0)) - (ifnull(e.kreditawal,0)+ifnull(sum(d.kredit),0)),
            (ifnull(e.kreditawal,0)+ifnull(sum(d.kredit),0)) - (ifnull(e.debetawal,0)+ifnull(sum(d.debet),0))
        ) as saldo,
        sum(d.debet)+c.debetawal as debet_tot,
        sum(d.kredit)+c.kreditawal as kredit_tot
        FROM keu_tipeakun a 
        LEFT JOIN keu_tipe b ON b.tipe =a.tipe 
        LEFT JOIN keu_akun c ON c.idtipeakun=a.idtipeakun 
        LEFT JOIN keu_jurnaldetail d ON d.idakun = c.idakun and d.tahun = '".$tahun."' and d.bulan = '".$bulan."'
        LEFT JOIN keu_jurnal kj on kj.kdjurnal = d.kdjurnal and kj.status = 1
        LEFT JOIN keu_tutup_buku e ON e.idakun = c.idakun and e.tahun = '".$tahun."' and e.bulan = '".$bulan."'
        GROUP BY c.idakun
        ORDER BY a.kode, c.kode";
        $data = $this->db->query($sql)->result();
        return $data;
    }
    
    
    
    /**
     * Rekap transaksi 
     * @return boolean
     */
    public function dt_rekaptransaksi()
    {
        $tanggal = $this->input->post('tanggal');
        $tahun   = date('Y', strtotime($tanggal)); 
        $idkasir = $this->input->post('idkasir');
        $status = $this->input->post('status');
        $query = $this->db->select(" t.jenisperiksa,
        concat((select status from rs_pemeriksaan where idpendaftaran = t.idpendaftaran limit 1)) as statusperiksa,  
        ppd.jenispemeriksaan, t.idpendaftaran, ppd.resumeprint, t.waktutagih, 
        ppd.norm, ppd.nosep,
        ppd.idstatuskeluar,
        if(not isnull(ri.idkelas) and ri.idkelas<ri. idkelasjaminan, 'jkn-naik-kelas', ppd.carabayar) as carabayar,
        concat(ifnull(pper.identitas, ''), ' ', ifnull(pper.namalengkap, '')) as namalengkap, 
        concat(t.idtagihan, '/',  statustagihan , '/',  t.jenistagihan ) as statustagihan, 
        sum( (t.nominal + t.pembulatan - t.potongan) - if(t.statusbayar='dibayar',t.kekurangan,0)) as tagihan, 
        sum(t.dibayar - t.kembalian) as dibayar,    
        t.idtagihan,
        ispemeriksaanselesaisemua(t.idpendaftaran) as isselesaisemua, 
        if(t.jenisperiksa = 'rajal','Rawat Jalan' , (select kelas from rs_kelas rk where rk.idkelas = ri.idkelas) )as kelas,
        statustagihan  as  cekstatustagih , 
        (CASE WHEN isnull(sr.status) THEN '0' ELSE '1' END )AS statusrekap,
        ri.idinap, ri.status, t.statusbayar,  t.jenispembayaran ");
        
        $query = $this->db->join('person_pendaftaran ppd','ppd.idpendaftaran = t.idpendaftaran');        
//        $query = $this->db->join('rs_kelas k','k.idkelas = ppd.idkelas');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->join('rs_inap ri','ri.idpendaftaran = t.idpendaftaran','left');
        $query = $this->db->join('keu_statusrekap sr','sr.idpendaftaran = t.idpendaftaran and sr.carabayar = t.jenispembayaran','left');
        $query = $this->db->where('t.tanggaltagih',$tanggal);
        $query = $this->db->where('t.statusbayar','dibayar');
        if(!empty($idkasir))
        {
            $query = $this->db->where('t.iduserkasir',$idkasir);
        }
        
        if(isset($status))
        {
            if($status =='1')
            {
                $query = $this->db->where("sr.status",$status);
            }else if($status =='0')
            {
                $query = $this->db->where("sr.status IS NULL");
            }
        }
        
        $query = $this->db->where("ppd.tahunperiksa",$tahun);
        $query = $this->db->group_by('t.idpendaftaran, t.jenispembayaran');
        $query = $this->db->get('keu_tagihan t');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_rekaptransaksi_($limit=false)
    {
        $column = ['t.idtagihan', 't.waktutagih', 'ppas.norm','ppd.idkelas','pper.namalengkap','ppd.nosep','t.dibayar','ppd.statustagihan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_rekaptransaksi();   
    }
    public function total_dt_rekaptransaksi(){ return $this->recordsTotal($this->dt_rekaptransaksi());}
    public function filter_dt_rekaptransaksi(){return $this->recordsFiltered($this->dt_rekaptransaksi_());}
    public function like($column_search=[],$post_search)
    {
        $i = 0;
        if(empty(!$column_search))
        {
            foreach ($column_search as $item)
            {
                if(isset($post_search))
                {
                    if($i===0)
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $post_search);
                    }
                    else
                    {
                        $this->db->or_like($item, $post_search);
                    }

                    if(count($column_search) - 1 == $i) 
                    $this->db->group_end(); 
                }
                $i++;
            }
        }
    }
    public function order($column_order=[])
    {
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
    }
    public function limit($length, $start)
    {
        if($length != -1){$this->db->limit($length, $start);}
    }
    public function recordsTotal($query) {
        if($query){return $query->num_rows();}
        return 0;
    }
    public function recordsFiltered($query)
    {
        if($query){return $query->num_rows();}
        return 0;
    }
    

    //    Pendapanat Kas
    public function dt_pendapatankas()
    {
        $query = $this->db->select('kp.*, lu.namauser, ka.nama as namabank, ifnull(kj.idreferensi,0) as sudahrekap, kj.kdjurnal');
        $query = $this->db->join('login_user lu','lu.iduser = kp.iduser','left');
        $query = $this->db->join('keu_akun ka','ka.kode = kp.idcoabank','left');
        $query = $this->db->join("keu_jurnal kj","kj.idreferensi = kp.idpendapatan and kj.kode_referensi='idpendapatankas_'","left");
        
        $post = $this->input->post();
        if(isset($post['idkasir']) && empty(!$post['idkasir']))
        {
            $query = $this->db->where('kp.iduser',$post['idkasir']);
        }
        $query = $this->db->where("kp.tanggal BETWEEN '".$post['tanggal']."' and '".$post['tanggal2']."'");
        
        if(isset($post['orderby']))
        {
            $query = $this->db->order_by('kp.jenispendapatan asc, kp.tanggal asc');
        }
        
        $query = $this->db->get('keu_pendapatankas kp');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    
    public function dt_pendapatankas_($limit=false)
    {
        $column = ['jenispendapatan','pendapatan', 'kp.tanggal','penyetor','nominal','jenistransaksi',false,false,'lu.namauser'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pendapatankas();   
    }
    
    public function total_dt_pendapatankas(){ return $this->recordsTotal($this->dt_pendapatankas_());}
    public function filter_dt_pendapatankas(){return $this->recordsFiltered($this->dt_pendapatankas_());}
    
    public function dashboard_pendapatankas()
    {
        $tanggal = $this->input->post('tanggal');
        $iduser  = $this->input->post('iduser');
        $wheretanggal = ((empty($tanggal)) ? "" : " and  kp.tanggalshif = '".$tanggal."'" );
        $whereuser    = ((empty($iduser)) ? "" : " and kp.iduser = '".$iduser."' ");
        
        $sql = "SELECT lu.namauser, sum(kp.nominal) as total, kp.levelshif, kp.jenistransaksi
                FROM keu_pendapatankas kp
                JOIN login_user lu on lu.iduser = kp.iduser
                WHERE 1=1 ".$wheretanggal.$whereuser."
                GROUP by kp.levelshif, kp.iduser, kp.jenistransaksi
                ORDER by kp.levelshif, kp.iduser";        
        $query = $this->db->query($sql);
        return $query->result_array();      
    }
    
    public function dashboard2_pendapatankas()
    {
        $tanggal = $this->input->post('tanggal');
        $iduser  = $this->input->post('iduser');
        $wheretanggal = ((empty($tanggal)) ? "" : " and  kp.tanggalshif = '".$tanggal."'" );
        $whereuser    = ((empty($iduser)) ? "" : " and kp.iduser = '".$iduser."' ");
        
        $sql = "SELECT kp.jenispendapatan, 
                sum(if(kp.jenistransaksi='transfer',kp.nominal,0)) as transfer,
                sum(if(kp.jenistransaksi='tunai',kp.nominal,0)) as tunai,
                sum(if(kp.jenistransaksi='jknpbi',kp.nominal,0)) as jknpbi,
                sum(if(kp.jenistransaksi='jknnonpbi',kp.nominal,0)) as jknnonpbi,
                sum(if(kp.jenistransaksi='asuransi',kp.nominal,0)) as asuransi
                FROM keu_pendapatankas kp
                WHERE 1=1 ".$wheretanggal.$whereuser."
                GROUP by kp.jenispendapatan";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getdata_generatependapatankas($tanggal,$shif,$jenis)
    {
        $sql = '';
        if($jenis == 'penjualanobatbebas')
        {
            $sql = "select 'Penjualan Obat Bebas' as pendapatan, date(k.waktubayar) as tanggal, k.tanggalshif, k.levelshif,
                sum(k.dibayar - k.kembalian) as dibayar,
                k.carabayar as jenispembayaran, k.idbanktujuan, k.iduserkasir
            FROM keu_tagihan_bebas k 
            where k.tanggalshif='".$tanggal."' and k.status='selesai' and k.levelshif = '".$shif."' and k.jenistagihan = 'dibayar'
            GROUP by k.carabayar, k.idbanktujuan,k.iduserkasir";
        }
        else if($jenis == 'rawatjalan')
        {
            $sql = "select 'Rawat Jalan' as pendapatan, date(k.waktubayar) as tanggal, k.tanggalshif, k.levelshif, sum(k.dibayar - k.kembalian) as dibayar, k.jenispembayaran,k.iduserkasir
            FROM keu_tagihan k,person_pendaftaran pp 
            WHERE k.tanggalshif ='".$tanggal."' and k.levelshif = '".$shif."' and k.jenistagihan!='batal' and k.jenisperiksa='rajal' and pp.idpendaftaran=k.idpendaftaran  and k.statusbayar = 'dibayar'
            GROUP by k.jenispembayaran, k.iduserkasir";
        }
        if(empty($sql))
        {
            return false;
        }
        else
        {
            $query = $this->db->query($sql)->result_array();
            return $query;
        }
        
    }
    //  /Pendapatan Kas
    
    /**
     * pasien rawat jalan - pendapatan kas
     * @param type $tanggal  tanggalshif 1
     * @param type $tanggal2 tanggalshif 2
     * @param type $shifke
     */
    public function pasienpendapatankas_ralan($tanggal,$tanggal2)
    {
        $sql = "select k.tanggalshif, pp.norm, pper.namalengkap, pper.alamat, namaunit(pp.idunit) as namaunit, sum(k.dibayar - k.kembalian) as dibayar, lu.namauser as petugas, ka.nama as edc
            FROM keu_tagihan k
            join person_pendaftaran pp on pp.idpendaftaran = k.idpendaftaran
            join person_pasien ppas on ppas.norm = pp.norm
            join person_person pper on pper.idperson = ppas.idperson
            join login_user lu on lu.iduser = k.iduserkasir
            join keu_akun ka on ka.kode = k.idbanktujuan
            WHERE k.tanggalshif BETWEEN '".$tanggal."' and '".$tanggal2."' and k.jenistagihan!='batal' and k.jenisperiksa='rajal' and k.statusbayar = 'dibayar' and k.jenispembayaran ='transfer'
            GROUP by k.idpendaftaran order by k.tanggalshif";
        
        $query = $this->db->query($sql);
        if($query)
        {
            return $query->result_array();
        }
        return false;
    }
    
    /**
     * penjualan bebas - pendapatan kas
     * @param type $tanggalshif
     * @param type $shifke
     */
    public function pasienpendapatankas_bebas($tanggal,$tanggal2)
    {
        $sql = "select k.tanggalshif, if(rbp.idpegawai is null ,rbp.nama , namadokter(rbp.idpegawai) ) as pembeli, sum(k.dibayar - k.kembalian) as dibayar, ka.nama as edc, lu.namauser as petugas
            FROM keu_tagihan_bebas k 
            join rs_barang_pembelibebas rbp on rbp.idbarangpembelibebas = k.idbarangpembelibebas
            join keu_akun ka on ka.kode = k.idbanktujuan
            join login_user lu on lu.iduser = k.iduserkasir
            where  k.tanggalshif BETWEEN '".$tanggal."' and '".$tanggal2."' and k.status='selesai' and k.jenistagihan = 'dibayar' and k.carabayar = 'transfer'
            GROUP by k.idtagihan
            ORDER by k.waktubayar desc";
        $query = $this->db->query($sql);
        if($query)
        {
            return $query->result_array();
        }
        return false;
    }
    
    //    Jenis Pendapanat Kas
    public function dt_jenispendapatankas()
    {
        $query = $this->db->select('*');
        $query = $this->db->get('keu_pendapatankas_jenis');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_jenispendapatankas_($limit=false)
    {
        $column = [false,'jenispendapatankas'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_jenispendapatankas();   
    }
    public function total_dt_jenispendapatankas(){ return $this->recordsTotal($this->dt_jenispendapatankas());}
    public function filter_dt_jenispendapatankas(){return $this->recordsFiltered($this->dt_jenispendapatankas_());}
    // end jenis pendapatan kas
    
    public function dt_pendaptan_det()
    {
        $tanggal = $this->input->post('tanggal');
        $idkasir = $this->input->post('kasir');
        $jenisbayar = $this->input->post('jenisbayar');
        $query = $this->db->select("a.idpendaftaran,
                                    a.tanggal,
                                    c.norm,
                                    e.namalengkap,
                                    a.nominal");
        
        $query = $this->db->join('keu_tagihan b','b.idpendaftaran = a.idpendaftaran');        
        $query = $this->db->join('person_pendaftaran c','c.idpendaftaran = a.idpendaftaran');
        $query = $this->db->join('person_pasien d','d.norm = c.norm');
        $query = $this->db->join('person_person e','e.idperson = d.idperson');
        
        $query = $this->db->where('a.tanggal',$tanggal);
        $query = $this->db->where('a.iduserkasir',$idkasir);
        $query = $this->db->where('a.jenispembayaran',$jenisbayar);

        $query = $this->db->group_by(['a.idpendaftaran']);
        $query = $this->db->get('keu_transaksimasuk a');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_pendaptan_det_($limit=false)
    {
        $column = ['tanggal', 'b.jenispembayaran'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pendaptan_det();   
    }
    public function total_dt_pendapatan_det(){ return $this->recordsTotal($this->dt_pendaptan_det_());}
    public function filter_dt_pendapatan_det(){return $this->recordsFiltered($this->dt_pendaptan_det_());}

    public function dt_pendaptan_det_trans()
    {
        $tanggal = $this->input->post('tanggal');
        $idkasir = $this->input->post('kasir');
        $jenisbayar = $this->input->post('jenisbayar');
        $bank = $this->input->post('bank');
        $query = $this->db->select("a.idpendaftaran,
                                    a.tanggal,
                                    a.bank,
                                    c.norm,
                                    e.namalengkap,
                                    a.nominal");
        
        $query = $this->db->join('keu_tagihan b','b.idpendaftaran = a.idpendaftaran');        
        $query = $this->db->join('person_pendaftaran c','c.idpendaftaran = a.idpendaftaran');
        $query = $this->db->join('person_pasien d','d.norm = c.norm');
        $query = $this->db->join('person_person e','e.idperson = d.idperson');
        
        $query = $this->db->where('a.tanggal',$tanggal);
        $query = $this->db->where('a.iduserkasir',$idkasir);
        $query = $this->db->where('a.jenistagihan',$jenisbayar);
        $query = $this->db->where('a.bank',$bank);

        $query = $this->db->group_by(['a.idpendaftaran']);
        $query = $this->db->get('keu_transaksimasuk a');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_pendaptan_det_trans_($limit=false)
    {
        $column = ['tanggal', 'b.jenistagihan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pendaptan_det_trans();   
    }
    public function total_dt_pendapatan_det_trans(){ return $this->recordsTotal($this->dt_pendaptan_det_trans_());}
    public function filter_dt_pendapatan_det_trans(){return $this->recordsFiltered($this->dt_pendaptan_det_trans_());}
    
    
    /** Perbedaan Akun Riil dan Akun Nominal*/
    /*Setiap akun pada pos neraca disebut akun riil karena saldo
    akhir periode yang lalu merupakan saldo awal periode berikutnya,
    sedangkan setiap akun pada pos laporan laba-rugi disebut akun
    nominal karena saldo akhir periode yang lalu bukan merupakan
    saldo awal periode berikutnya sehingga dibutuhkan jurnal penutup
    agar cut off time dapat dilakukan.*/
    
    public function dt_laporanlabarugi()
    {
        $awal   = $this->input->post('awal');
        $sampai = $this->input->post('sampai');
        $sql    = "SELECT 
        c.idakun, 
        a.tipe as tipe_akun,
        a.kode as kode_tipe, 
        a.idtipeakun, 
        a.nama as nama_tipe, 
        c.kode as kode_akun, 
        c.nama as nama_akun, 
        if(d.carabayar = 'UMUM',
        	if(a.isdebit = 1 , 
            (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0)) - (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)),
            (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)) - (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0))
        	)   
         ,0) as umum,
        if(d.carabayar = 'JAMINAN',
        	if(a.isdebit = 1 , 
            (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0)) - (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)),
            (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)) - (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0))
        	)   
         ,0) as jaminan,
         if(d.carabayar = 'BPJS',
        	if(a.isdebit = 1 , 
            (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0)) - (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)),
            (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)) - (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0))
        	)   
         ,0) as bpjs,
        if(a.isdebit = 1 , 
            (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0)) - (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)),
            (ifnull(c.kreditawal,0)+ifnull(sum(d.kredit),0)) - (ifnull(c.debetawal,0)+ifnull(sum(d.debet),0))
        ) as saldo,
        if(a.isdebit = 1 , 
            ifnull(sum(d.debet),0) - ifnull(sum(d.kredit),0),
           ifnull(sum(d.kredit),0) - ifnull(sum(d.debet),0)
        ) as saldoberjalan
        FROM keu_tipeakun a 
        JOIN keu_akun c ON c.idtipeakun=a.idtipeakun 
        JOIN keu_jurnaldetail d ON d.idakun = c.idakun 
        JOIN keu_jurnal e on e.kdjurnal = d.kdjurnal and e.status = 1
        WHERE a.tipe in ('pendapatan','pendapatan lain','biaya','biaya lain') and e.tanggal BETWEEN '".$awal."' and '".$sampai."' and e.status = 1
        GROUP BY c.idakun, d.carabayar
        ORDER BY a.kode, c.kode";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function dt_laporanneraca()
    {
        $bulan   = $this->input->post('bulan');
        $tahun   = $this->input->post('tahun');
        $saldo   = $this->input->post('saldo');
        $left_join = "";
        if($saldo == 1)
        {
            $left_join = "";
        }
        else
        {
            $left_join = "left";
        }
        
        $sql = "SELECT v.isdebit, v.idakun, v.tipe_akun, v.kode_tipe, v.idtipeakun, v.nama_tipe,v.kode_akun,v.nama_akun,
        sum(debet) as debet,sum(kredit) as kredit,0 as debetawal,0 as kreditawal
        FROM vrs_jurnaldetail v
        WHERE   tahun = '".$tahun."' and bulan = '".$bulan."' and status=1 and tipe in ('aset','kewajiban','equitas') 
        GROUP BY idakun
        
        union
        
        SELECT kt.isdebit, ka.idakun, kt.tipe as tipe_akun, kt.kode as kode_tipe, kt.idtipeakun, kt.nama as nama_tipe,ka.kode as kode_akun,ka.nama as nama_akun,
        0 as debet,0 as kredit, ifnull(ktb.debetawal,0) as debetawal, ifnull(ktb.kreditawal,0) as kreditawal
        FROM keu_akun ka        
        join keu_tipeakun kt on kt.idtipeakun = ka.idtipeakun       
        
        ".$left_join." join keu_tutup_buku ktb on ktb.idakun = ka.idakun  and ktb.tahun = '".$tahun."' and ktb.bulan = '".$bulan."'
        WHERE   kt.tipe in ('aset','kewajiban','equitas') 
        GROUP BY ka.idakun
        
        ORDER BY kode_tipe, kode_akun";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function get_rincianakun($idakun,$awal,$akhir)
    {
        $sql = "SELECT k.kdjurnal, kt.isdebit, k.tanggal, concat('No Ref.',k.no_referensi,' Ref: ',k.referensi,'<br> desc: ',k.keterangan ) as deskripsi, kj.debet, kj.kredit
            FROM keu_jurnaldetail kj
            JOIN keu_jurnal k on k.kdjurnal = kj.kdjurnal and k.status=1
            JOIN keu_tipe kt on kt.tipe = kj.tipe
            WHERE kj.idakun = '".$idakun."' and k.status=1 and k.tanggal BETWEEN '".$awal."' and '".$akhir."' order by k.tanggal";
        $query = $this->db->query($sql)->result_array();
        return $query;
    }
    
    public function view_rs_pengeluaran_join($where='')
    {
        $query = $this->db->select("rp.idpenjual,rp.is_transfer,rp.idkategoribiaya,rp.idpegawai, p.penjual as namapenjual, rp.idpengeluaran, rp.tanggal, rp.notransaksi, rp.total,rp.potongan,rp.ppn, rp.catatan, rp.keterangan, rk.kategoribiaya, concat(p.penjual,'<br>' ,'<small>',ifnull(p.alamat,''),'</small>' ) as penjual, concat(ifnull(pp.titeldepan,''),' ',pper.namalengkap,' ',ifnull(pp.titelbelakang,'') ) as pembeli");
        $query = $this->db->join("rs_kategoribiaya rk"," rk.idkategoribiaya = rp.idkategoribiaya");
        $query = $this->db->join("rs_penjual p "," p.idpenjual = rp.idpenjual","left");
        $query = $this->db->join("person_pegawai pp "," pp.idpegawai = rp.idpegawai","left");
        $query = $this->db->join("person_person pper "," pper.idperson = pp.idperson","left");
        if(empty(!$where))
        {
            $query = $this->db->where($where);
        }
        $query = $this->db->get("rs_pengeluaran rp");
        return $query->result_array();
    }
    
    public function view_rs_pengeluaran_detail_join($where='')
    {
        $query = $this->db->select("rpd.*, CAST(rpd.harga as INTEGER) as harga, rs.namasatuan as satuan, rbp.namabarang as namaitem");
        $query = $this->db->join("rs_barang_pengeluaran rbp","rbp.idbarangpengeluaran = rpd.idbarangpengeluaran");
        $query = $this->db->join("rs_satuan rs","rs.idsatuan = rpd.idsatuan");
        if(empty(!$where))
        {
            $query = $this->db->where($where);
        }
        $query = $this->db->get("rs_pengeluaran_detail rpd");
        return $query->result_array();
    }
    
    public function view_cash_equivalen_join($where='')
    {
        $query = $this->db->select("kc.*, if(kc.isdebit = 1,kc.nominal,0) as debit, if(kc.isdebit=0,kc.nominal,0) as kredit,  date_format(kc.tanggal,'%M') as namabulan");
        if(empty(!$where))
        {
            $query = $this->db->where($where);
        }
        $query = $this->db->order_by("bulan,tanggal,id",'asc');
        $query = $this->db->get("keu_cashequivalen kc");
        return $query->result_array();
    }
    
    public function dashboard_pengeluaran()
    {
        $tgl1 = $this->input->post('tgl1');
        $tgl2 = $this->input->post('tgl2');
        $sql = "select rk.kategoribiaya , (select ifnull(sum(CAST(rp.nominalbiaya as int)),0) FROM rs_pengeluaran rp WHERE date(rp.tanggal) BETWEEN '".$tgl1."' and '".$tgl2."' and rp.idkategoribiaya = rk.idkategoribiaya) as total from rs_kategoribiaya rk";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    public function getdata_jasamedisralan()
    {
        $post = $this->input->get();
        if(isset($post['idpegawaidokter']) && empty(!$post['idpegawaidokter']))
        {
            $idpegawaidokter = $post['idpegawaidokter'];
            $tanggal1 = $post['tanggal1'];
            $tanggal2 = $post['tanggal2'];
            $tahun = date('Y', strtotime($tanggal1));
            $bulan = date('m', strtotime($tanggal1));

            $sql = "SELECT pp.norm, namapasien(ppas.idperson) as namapasien, rp.waktu as tanggalperiksa,pp.carabayar, ri.icd, IFNULL(ri.namaicd, rpp.namapaketpemeriksaan) AS namaicd,rh.jasaoperator,rh.nakes as dokter, rh.jasars, rh.jumlah,  ((rh.jasars + rh.jasaoperator + rh.nakes) * rh.jumlah) as total, namaunit(rp.idunit) as namaunit 
                    FROM person_pendaftaran pp
                    join person_pasien ppas on ppas.norm = pp.norm
                    join rs_pemeriksaan rp on rp.idpendaftaran = pp.idpendaftaran
                    join rs_hasilpemeriksaan rh on rh.idpemeriksaan = rp.idpemeriksaan and rh.iddokterjasamedis > 0 and (rh.total > 0 or rh.icd = 'UGD95')
                    left join rs_icd ri on ri.icd = rh.icd
                    left join rs_paket_pemeriksaan rpp on rpp.idpaketpemeriksaan = rh.idpaketpemeriksaan
                    WHERE pp.tahunperiksa = '".$tahun."' and pp.bulanperiksa = '".$bulan."' and date(pp.waktuperiksa) between '".$tanggal1."' and '".$tanggal2."' and (idstatuskeluar = 2 or (pp.jenispemeriksaan = 'rajalnap' and pp.idstatuskeluar = 1) or (jenispemeriksaan = 'ranap' and pp.idstatuskeluar = 1) ) and rh.iddokterjasamedis = '".$idpegawaidokter."'
                    order by pp.waktuperiksa, pp.norm, rp.idunit";

            $data = $this->db->query($sql)->result_array();
            return $data;
        }
        else
        {
            return 0;
        }
        
    }
}