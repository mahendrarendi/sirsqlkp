<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Msatusehat extends CI_Model
{
    protected $tb_satusehat_organization   = 'satusehat_organization';
    protected $tb_satusehat_suborganization   = 'satusehat_sub_organization';

    public function get_listorganization()
    {
        $this->db->select('*');
        $this->db->from('satusehat_organization');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_listsuborganization()
    {
        $this->db->select('
            sbo.*, so.name as organisasiInduk, so.ihs as ihsParent 
        ');
        $this->db->from('satusehat_sub_organization sbo');
        $this->db->join('satusehat_organization so', 'so.idorganization = sbo.idorganization');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_editorganization($id='')
    {
        $sql = "SELECT idorganization,status,type,name,phone,email,ihs";
        $sql .= " FROM ".$this->tb_satusehat_organization ;
        if(!empty($id)){
            $sql .= " WHERE idorganization=$id";
        }
        return $this->db->query($sql)->row();
    }

    public function get_editsuborganization($id='')
    {
        $sql = "SELECT sbo.*,so.name as organisasiinduk FROM satusehat_sub_organization sbo
                JOIN satusehat_organization so ON sbo.idorganization = so.idorganization";
        
        // $sql = "SELECT idsuborganization,status,type,name,phone,email";
        // $sql .= " FROM ".$this->tb_satusehat_suborganization ;
        if(!empty($id)){
            $sql .= " WHERE idsuborganization=$id";
        }
        return $this->db->query($sql)->row();
    }
    //bed / kamar
    public function get_bed($sub_location){
        $sql = "SELECT CONCAT(?,' Kamar ',rbd.nobed) as namaruang from rs_bed rbd 
                JOIN rs_bangsal rbl ON rbd.idbangsal = rbl.idbangsal 
                JOIN rs_bed_status rbs ON rbs.idstatusbed = rbd.idstatusbed
                where rbl.namabangsal = ?";
    $query = $this->db->query($sql, array($sub_location,$sub_location));
    
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return NULL;
        }
    }   
    public function get_loket($sub_location)
	{				
        $sql = "SELECT al.namaloket as namaruang FROM antrian_loket as al 
                JOIN antrian_stasiun k on k.idstasiun = al.idstasiun 
                where k.namastasiun = ?";
        $query = $this->db->query($sql, array($sub_location));
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return NULL;
        }
	}

    //bangsal
    public function get_bangsal(){
        $query = $this->db->query("SELECT rsb.namabangsal as namaruang from rs_bangsal rsb
                left join person_pegawai pg on rsb.idpegawaika = pg.idpegawai 
                left join person_person pp on pp.idperson = pg.idperson 
                left join rs_kelas rsk on rsk.idkelas = rsb.idkelas ");
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return NULL;
        }
    }

    //unit
    public function get_unit()
	{
		$query = $this->db->query("SELECT namaunit as namaruang, idunit FROM rs_unit");
		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}
	}

    // stasiun
    public function get_stasiun(){
        $query = $this->db->query("SELECT DISTINCT k.namastasiun as namaruang from antrian_stasiun k 
                join antrian_loket l on k.idstasiun = l.idstasiun;");
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return NULL;
        }
    }
    // kelas
    public function get_kelas(){
        $this->db->select('kelas');
        return $this->db->get('rs_kelas')->result();
    }
    public function get_listlocation()
    {
        $this->db->select('*');
        $this->db->from('satusehat_location');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_listsublocation()
    {
        $this->db->select('
            sbo.*, so.ihs as ihsParent 
        ');
        $this->db->from('satusehat_sub_location sbo');
        $this->db->join('satusehat_location so', 'so.idlocation = sbo.idlocation');
        $query = $this->db->get();
        return $query->result();
    }
    //get one location
    public function get_one_sub_location($idsublocation){
        $sql = "SELECT sl.namaruang as ruangutama, sbl.* from satusehat_location sl 
                join satusehat_sub_location sbl 
                on sl.idlocation = sbl.idlocation 
                where sbl.idsublocation = ?";
        $query = $this->db->query($sql, array($idsublocation));
        return $query->row_array();
    }
    public function get_location_in_sub_location(){
        $jr=['stasiun','bangsal'];
        $this->db->select('idlocation,namaruang');
        $this->db->where_in('jenisruang', $jr);
        return $this->db->get('satusehat_location')->result();
    }
    public function update_ihs_nakes($nik,$idihs){
        $sql = "UPDATE person_pegawai 
                JOIN person_person ON person_pegawai.idperson = person_person.idperson 
                SET person_pegawai.ihspractitioner = ? 
                WHERE person_person.nik = ?";
        $query = $this->db->query($sql, array($idihs, $nik));
        return $query;
    }
    public function update_ihs_organization($id,$ihs){
        $this->db->set('ihs', $ihs);
        $this->db->where('idorganization', $id);
        $query =$this->db->update('satusehat_organization');
        return $query;
    }
    public function update_ihs_sub_organization($id,$ihs){
        $this->db->set('ihs', $ihs);
        $this->db->where('idsuborganization', $id);
        $query =$this->db->update('satusehat_sub_organization');
        return $query;
    }
    public function update_ihs_location($id,$ihs){
        $this->db->set('ihs', $ihs);
        $this->db->where('idlocation', $id);
        $query =$this->db->update('satusehat_location');
        return $query;
    }
    public function update_ihs_sub_location($id,$ihs){
        $this->db->set('ihs', $ihs);
        $this->db->where('idsublocation', $id);
        $query =$this->db->update('satusehat_sub_location');
        return $query;
    }
    public function insert_log($data){
        $this->db->insert('satusehat_log',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id; 
    }
    public function get_one_sub_org($idsuborg){
        $this->db->select('
            sbo.*, so.ihs as ihsParent 
        ');
        $this->db->from('satusehat_sub_organization sbo');
        $this->db->join('satusehat_organization so', 'so.idorganization = sbo.idorganization');
        $this->db->where('sbo.idsuborganization', $idsuborg);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function get_one_sub_loc($idsubloc){
        $this->db->select('
            sbo.*, so.ihs as ihsParent 
        ');
        $this->db->from('satusehat_sub_location sbo');
        $this->db->join('satusehat_location so', 'so.idlocation = sbo.idlocation');
        $this->db->where('sbo.idsublocation', $idsubloc);
        $query = $this->db->get();
        return $query->row_array();
    }

    //get one location (unit/poli)
    public function get_poli($idunit){
        $query = $this->db->select('*')
                ->from('satusehat_location')
                ->where('idpoli', $idunit)
                ->get()
                ->row_array();
        return $query;
    }

    /** ENCOUNTER */
    public function insert_log_encounter($data){
        $this->db->insert('satusehat_log_encounter',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id; 
    }
    public function update_log_encounter($data,$idencounter){
        $this->db->where('idencounter', $idencounter)
                ->update('satusehat_log_encounter', $data);
        return $this->db->affected_rows() > 0;
    }
    public function get_status_history_encounter($idpemeriksaan){
        $this->db->select('status_history');
        $this->db->from('satusehat_log_encounter');
        $this->db->where('idpemeriksaan', $idpemeriksaan);
        $query = $this->db->get();
        return $query->row_array();
    }
    public function get_one_encounter($idpendaftaran){
        $sql = "SELECT ppend.idencounter, ppend.idpendaftaran, rsp.idpemeriksaan as idperiksa, ppend.waktu as waktuDaftar, rsp.waktu as waktuPeriksa, ppp.ihs as ihsPasien, ppp.namalengkap as namaPasien,
                        ppeg.ihspractitioner as ihsDokter, CONCAT(IFNULL(ppeg.titeldepan, ''), ' ',
                        IFNULL(ppd.namalengkap, ''), ' ', IFNULL(ppeg.titelbelakang, '')) as namaDokter,
                        ru.idunit as idPoli
                FROM person_pendaftaran ppend 
                JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                JOIN rs_unit ru ON rsp.idunit = ru.idunit
                JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                JOIN person_person ppp ON ppp.idperson = ppas.idperson
                JOIN person_pegawai ppeg ON ppeg.idpegawai = rsp.idpegawaidokter
                JOIN person_person ppd ON ppd.idperson = ppeg.idperson
                WHERE ppend.idpendaftaran = ?";
        $query = $this->db->query($sql, array($idpendaftaran));
        return $query->row_array();
    }/** CONDITION */

    public function insert_log_condition($data){
        $this->db->insert('satusehat_log_condition',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id; 
    }
    public function get_condition($idpendaftaran){
        $sql = "SELECT 
                    rhp.icd, 
                    ri.namaicd,
                    rhp.icdlevel,
                    ppp.ihs as ihsPasien,
                    ppp.namalengkap as namaPasien, 
                    ppend.idencounter, 
                    ppeg.ihspractitioner as ihsDokter,
                    CONCAT(IFNULL(ppeg.titeldepan, ''), ' ',
                        IFNULL(ppd.namalengkap, ''), ' ', IFNULL(ppeg.titelbelakang, '')) as namaDokter, 
                    rhp.idhasilpemeriksaan,
                    ppend.idpendaftaran
                FROM 
                    person_pendaftaran ppend 
                    JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                    JOIN rs_hasilpemeriksaan rhp ON rhp.idpemeriksaan = rsp.idpemeriksaan
                    JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                    JOIN person_person ppp ON ppp.idperson = ppas.idperson
                    JOIN person_pegawai ppeg ON ppeg.idpegawai = rsp.idpegawaidokter
                    JOIN person_person ppd ON ppd.idperson = ppeg.idperson
                    JOIN rs_icd ri ON ri.icd = rhp.icd 
                WHERE 
                    ppend.idpendaftaran = '".$idpendaftaran."'
                    AND ri.idjenisicd = '2'
                    AND ri.idicd = '10'
                    AND rhp.isverifklaim = 1
                    AND ppend.idencounter != ''";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_one_condition($idhasilpemeriksaan){
        $sql = "SELECT 
                    ppend.idencounter, 
                    ppend.idpendaftaran, 
                    rhp.idhasilpemeriksaan,
                    rsp.idpemeriksaan as idperiksa, 
                    rsp.waktu as waktuPeriksa, 
                    ppp.ihs as ihsPasien, 
                    ppp.namalengkap as namaPasien, 
                    rhp.icd, 
                    (SELECT namaicd FROM rs_icd WHERE icd = rhp.icd) as textIcd, 
                    rhp.icdlevel,
                    rhp.idcondition
                FROM 
                    person_pendaftaran ppend 
                    JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                    JOIN rs_hasilpemeriksaan rhp ON rhp.idpemeriksaan = rsp.idpemeriksaan
                    JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                    JOIN person_person ppp ON ppp.idperson = ppas.idperson
                WHERE 
                    rhp.idhasilpemeriksaan = '".$idhasilpemeriksaan."'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    public function cek_data_condition($idencounter,$icdlevel){
        $sql = "SELECT slc.idcondition, slc.idhasilpemeriksaan
                FROM satusehat_log_condition slc
                WHERE slc.idencounter ='".$idencounter."' AND slc.icdlevel = '".$icdlevel."'";
        $query = $this->db->query($sql)->row_array();
        return $query;
    }
    /** OBSERVATION */
    public function insert_log_observation($data){
        $this->db->insert('satusehat_log_observation',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id; 
    }
    public function get_one_observation($idhasilpemeriksaan){
        $sql = "SELECT 
                    rhp.idobservation,
                    ppend.idencounter, 
                    rhp.idpendaftaran,
                    rhp.idpemeriksaan,
                    ppp.ihs as ihsPasien, 
                    ppp.namalengkap as namaPasien, 
                    ppeg.ihspractitioner as ihsDokter,
 					COALESCE(nilai, nilaitext) AS nilai_obs,
                    CONCAT(IFNULL(ppeg.titeldepan, ''), ' ',
                        IFNULL(ppd.namalengkap, ''), ' ', IFNULL(ppeg.titelbelakang, '')) as namaDokter,
                    rhp.icd, 
                    (SELECT kode_loinc FROM rs_icd WHERE icd = rhp.icd) as kode_loinc,
                    (SELECT namaicd FROM rs_icd WHERE icd = rhp.icd) as indotxt_loinc,
                    (SELECT aliasicd FROM rs_icd WHERE icd = rhp.icd) as engtxt_loinc,
                    (SELECT satuan_loinc FROM rs_icd WHERE icd = rhp.icd) as satuan_loinc,
                    (SELECT satuan_kode_loinc FROM rs_icd WHERE icd = rhp.icd) as satuan_kode_loinc 
                FROM 
                    person_pendaftaran ppend 
                    JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                    JOIN rs_hasilpemeriksaan rhp ON rhp.idpemeriksaan = rsp.idpemeriksaan
                    JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                    JOIN person_person ppp ON ppp.idperson = ppas.idperson
                    JOIN person_pegawai ppeg ON ppeg.idpegawai = rsp.idpegawaidokter
                    JOIN person_person ppd ON ppd.idperson = ppeg.idperson
                WHERE 
                    rhp.idhasilpemeriksaan = '".$idhasilpemeriksaan."'
                    AND ppend.idencounter != ''";
        $query = $this->db->query($sql);
        return $query->row_array();
    }

    /** PROCEDURE */
    public function insert_log_procedure($data){
        $this->db->insert('satusehat_log_procedure',$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id; 
    }
    public function get_procedure($idpendaftaran){
        $sql = "SELECT 
                    rhp.icd, 
                    ri.namaicd,
                    ppp.ihs as ihsPasien,
                    ppp.namalengkap as namaPasien, 
                    ppend.idencounter, 
                    ppeg.ihspractitioner as ihsDokter,
                    CONCAT(IFNULL(ppeg.titeldepan, ''), ' ',
                        IFNULL(ppd.namalengkap, ''), ' ', IFNULL(ppeg.titelbelakang, '')) as namaDokter, 
                    rhp.idhasilpemeriksaan,
                    ppend.idpendaftaran, 
                    rhp.idhasilpemeriksaan
                FROM 
                    person_pendaftaran ppend 
                    JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                    JOIN rs_hasilpemeriksaan rhp ON rhp.idpemeriksaan = rsp.idpemeriksaan
                    JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                    JOIN person_person ppp ON ppp.idperson = ppas.idperson
                    JOIN person_pegawai ppeg ON ppeg.idpegawai = rsp.idpegawaidokter
                    JOIN person_person ppd ON ppd.idperson = ppeg.idperson
                    JOIN rs_icd ri ON ri.icd = rhp.icd 
                WHERE 
                    ppend.idpendaftaran = '".$idpendaftaran."'
                    AND ri.idjenisicd = '2'
                    AND ri.idicd = '9'
                    AND rhp.isverifklaim = 1
                    AND ppend.idencounter != ''";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_one_procedure($idhasilpemeriksaan){
        $sql = "SELECT 
                    ppend.idencounter, 
                    ppend.idpendaftaran, 
                    rhp.idhasilpemeriksaan,
                    rsp.idpemeriksaan as idperiksa, 
                    rsp.waktu as waktuPeriksa, 
                    ppp.ihs as ihsPasien, 
                    ppp.namalengkap as namaPasien, 
                    rhp.icd, 
                    (SELECT namaicd FROM rs_icd WHERE icd = rhp.icd) as textIcd, 
                    rhp.icdlevel,
                    rhp.idprocedure
                FROM 
                    person_pendaftaran ppend 
                    JOIN rs_pemeriksaan rsp ON rsp.idpendaftaran = ppend.idpendaftaran 
                    JOIN rs_hasilpemeriksaan rhp ON rhp.idpemeriksaan = rsp.idpemeriksaan
                    JOIN person_pasien ppas ON ppas.norm = ppend.norm 
                    JOIN person_person ppp ON ppp.idperson = ppas.idperson
                WHERE 
                    rhp.idhasilpemeriksaan = '".$idhasilpemeriksaan."'";
        $query = $this->db->query($sql);
        return $query->row_array();
    }
    public function cek_data_procedure($idencounter,$icdlevel){
        $sql = "SELECT slp.idprocedure, slp.idhasilpemeriksaan
                FROM satusehat_log_procedure slp
                WHERE slp.idencounter ='".$idencounter."' AND ";
        $query = $this->db->query($sql)->row_array();
        return $query;
    }
}
?>