<?php
 
class Mmessagegateway extends CI_Model {
 
    public function __construct()
    {
        parent::__construct();
    }
 
    public function update_messagegateway($data, $filter = array())
    {
        if (count($filter) > 0) 
        {
            $this->db->where($filter);
        }
        $this->db->update('message_gateway', $data);
    } 
}
