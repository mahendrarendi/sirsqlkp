<?php
 
class Mod_keu_departemen extends CI_Model {
    
    private $table = 'keu_departemen';
    
    public function listdata($where = array())
    {
        $query = $this->db->select('iddepartemen,kode,nama');
        if(empty(!$where))
        {
            $query = $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        return $query;
    }
    
    public function getfill($where)
    {
        $query = $this->db->select('iddepartemen as id, nama as txt');
        if(empty(!$where))
        {
            $query = $this->db->where($where);
        }
        $query = $this->db->get($this->table);
        return $query;
    }
    
}




