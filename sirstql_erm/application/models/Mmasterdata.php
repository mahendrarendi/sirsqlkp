<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmasterdata extends CI_Model 
{
	

	public function get_data_kabupaten()
	{
						
		$query = $this->db->query("SELECT * FROM geografi_kabupatenkota p LEFT JOIN geografi_propinsi k on k.idpropinsi = p.idpropinsi");

		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	
	public function get_data_kecamatan()
	{
		$query = $this->db->query('SELECT k.idkecamatan, k.namakecamatan, k.kodepos, kk.namakabupatenkota, p.namapropinsi FROM geografi_kecamatan d
			LEFT JOIN geografi_kecamatan k ON k.idkecamatan = d.idkecamatan
			LEFT JOIN geografi_kabupatenkota kk ON kk.idkabupatenkota = k.idkabupatenkota
			LEFT JOIN geografi_propinsi p ON p.idpropinsi = kk.idpropinsi');

		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	
	public function get_data_kelurahan()
	{
		$query = $this->db->query('SELECT d.iddesakelurahan, d.namadesakelurahan, k.namakecamatan, k.kodepos, kk.namakabupatenkota, p.namapropinsi FROM geografi_desakelurahan d
			LEFT JOIN geografi_kecamatan k ON k.idkecamatan = d.idkecamatan
			LEFT JOIN geografi_kabupatenkota kk ON kk.idkabupatenkota = k.idkabupatenkota
			LEFT JOIN geografi_propinsi p ON p.idpropinsi = kk.idpropinsi');
			
		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	public function get_data_pegawai()
	{
						
		$query = $this->db->query("SELECT * FROM person_pegawai g LEFT JOIN person_grup_pegawai k on k.idgruppegawai = g.idgruppegawai");

		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	
	// ambil data enum dari database
	public function get_data_enum($table, $column,$dbname='db_sirstql') // ambil data enum dari database 
	{
        if(file_exists("./QLKP.php")){
            $dbname='db_sirstql_270721';
        } else {
            $dbname='db_sirstql';
        }
        
		$query = $this->db->query("SELECT COLUMN_TYPE  FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_SCHEMA = '".$dbname."' AND TABLE_NAME = '".$table."' AND COLUMN_NAME = '".$column."'");

		if($query->num_rows() > 0)
		{
			
			foreach($query->result_array() AS $row) 
			{
    			$result = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE'])-6))));
    		}
		 	return $result;
		}
		else
		{
			return NULL;
		}
	}
	//cari nama karyawan
	public function cari_namakaryawan($id)
	{
		$query=$this->db->query("SELECT pp.idperson, namalengkap, titeldepan, titelbelakang FROM person_pegawai p, person_person pp WHERE titelbelakang ='$id' OR titeldepan ='$id' OR namalengkap ='$id' ");
		if($query->num_rows() > 0)
		{
		 	return $query->result();
		}
		else
		{
			return NULL;
		}
	}
	
        public function get_data_instalasi()
	{
						
		$query = $this->db->query("SELECT * FROM rs_instalasi p LEFT JOIN rs_unit k on k.idinstalasi = p.idinstalasi");

		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
       
        public function get_data_unit()
	{
		$query = $this->db->query('SELECT d.idunit, d.namaunit, d.akronimunit, f.namalengkap, l.namainstalasi, t.namaloket, kk.namapaketpemeriksaan FROM rs_unit d
			LEFT JOIN person_pegawai k ON k.idpegawai = d.idpegawaika
                        LEFT JOIN person_person f ON f.idperson = k.idperson
			LEFT JOIN rs_instalasi kk ON kk.idinstalasi = l.idinstalasi
			LEFT JOIN rs_paket_pemeriksaan p ON p.idpaketpemeriksaan = kk.idpaketpemeriksaan');
			
		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	public function get_data_unit11()
	{
		$query = $this->db->query('SELECT d.idunit, d.namaunit, j.namalengkap, kk.namainstalasi, p.namapaketpemeriksaan FROM rs_unit d
			LEFT JOIN person_pegawai k ON k.idpegawai = d.idpegawaika
                        LEFT JOIN person_person j ON k.idperson = j.idperson
			LEFT JOIN rs_instalasi kk ON kk.idinstalasi = d.idinstalasi
			LEFT JOIN rs_paket_pemeriksaan p ON p.idpaketpemeriksaan = kk.idpaketpemeriksaan');
			
		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}

	}
	public function masterdata_getdataunit()
	{
		$query = $this->db->query("SELECT `ru`.`idunit`, `ru`.`namaunit`, ru.kodebpjs, `ru`.`akronimunit`, `pper`.`namalengkap`, `ri`.`namainstalasi`, `rpp`.`namapaketpemeriksaan`, `ru`.`hakinputidjenisicd`FROM rs_unit ru
                left join person_pegawai ppg on ppg.idpegawai = ru.idpegawaidokter
                left join `person_person` `pper` on pper.idperson = ppg.idperson
                left join rs_instalasi ri on ri.idinstalasi = ru.idinstalasi
                left join `rs_paket_pemeriksaan` `rpp` on `rpp`.`idpaketpemeriksaan` = `ru`.`idpaketpemeriksaan` ");
		if($query->num_rows() > 0){
		 	return $query->result();
		}else{
			return NULL;
		}
	}

        public function get_data_loket()
	{				
            $query = $this->db->query("SELECT al.*, k.namastasiun, ru.namaunit, als.jenis as jenisloket FROM antrian_loket as al "
                    . "LEFT JOIN antrian_stasiun k on k.idstasiun = al.idstasiun "
                    . "left join rs_unit ru on ru.idunit = al.idunit "
                    . "left join antrian_loket_jenis als on als.kode = al.idjenisloket "
                    . "order by namastasiun, namaloket");
            if($query->num_rows() > 0){
                return $query->result();
            }else{
                return NULL;
            }
	}

	public function get_data_distributor()
	{				
		$query = $this->db->query("SELECT 	rbd.idbarangdistributor, 
											rbd.distributor, 
											rbd.alamat, 
											rbd.telepon, 
											rbd.norekening, 
											rbd.idbarangpbf, 
											rbd.email, 
											rbdb.idberkas, 
											rbdb.jenisberkas, 
											rbdb.path 
									FROM rs_barang_distributor rbd 
									LEFT JOIN rs_barang_distributor_berkas rbdb on rbdb.idbarangdistributor = rbd.idbarangdistributor
									GROUP BY rbd.idbarangdistributor
									ORDER BY rbd.distributor ASC ");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return NULL;
		}
	}
}

/* End of file ${TM_FILENAME:${1/(.+)/Madministrasi.php/}} */
/* Location: ./${TM_FILEPATH/.+((?:application).+)/Madministrasi/:application/models/${1/(.+)/Madministrasi.php/}} */


