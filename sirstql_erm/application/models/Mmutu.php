<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mmutu extends CI_Model 
{
    protected $tb_mutu_indikator            = 'mutu_indikator';
    protected $tb_mutu_pendataan_indikator  = 'mutu_pendataan_indikator';
    protected $tb_mutu_hasil                = 'mutu_hasil';
    protected $tb_mutu_analisarencana       = 'mutu_analisarencana';
    protected $tb_mutu_pengaturan           = 'mutu_pengaturan';
    protected $tb_mutu_daftar_ikp           = 'mutu_daftar_ikp';
    protected $tb_mutu_rekapikp             = 'mutu_rekapikp';
    protected $tb_mutu_daftarppi            = 'mutu_daftarppi';
    protected $tb_mutu_pendataan_ppi        = 'mutu_pendataan_ppi';
    protected $tb_mutu_pengaturanppi        = 'mutu_pengaturanppi';
    protected $tb_mutu_pengaturanikp        = 'mutu_pengaturanikp';
    protected $tb_mutu_rekap_ppi            = 'mutu_rekap_ppi';
    protected $tb_mutu_pelaporan_ikp        = 'mutu_pelaporan_ikp';
    protected $tb_mutu_lembarpdsa           = 'mutu_lembarpdsa';
    protected $tb_mutu_tindakan             = 'mutu_tindakan';
    protected $tb_mutu_dokumen_pdsa         = 'mutu_dokumen_pdsa';

    public function get_listdaftarimutrs($bulan,$tahun)
    {
        $sql = "SELECT id_indikator,tanggal,judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        // $sql .= " WHERE Month(tanggal) = '".$bulan."' AND YEAR(tanggal) ='".$tahun."' ";

        return $this->db->query($sql)->result();
    }

    public function get_listrekapimutrs($tahun)
    {
        $sql = "SELECT id_indikator,tanggal,judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        // $sql .= " WHERE YEAR(tanggal) ='".$tahun."' ";

        return $this->db->query($sql)->result();
    }

    public function get_editindikator($id)
    {
        $sql = "SELECT id_indikator, judul_indikator,defenisi_operasional,kriteria_inklusi,kriteria_ekslusi,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_indikator ;
        $sql .= " WHERE id_indikator = '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_editpendataan($id)
    {
        $sql = "SELECT id_pendataan,hasil, indikator_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,indikator,satuan ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator ;
        $sql .= " WHERE indikator_id = '".$id."'";

        return $this->db->query($sql)->result();
    }

    public function get_editpendataan_row($id)
    {
        $sql = "SELECT id_pendataan,hasil, indikator_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,indikator,satuan ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator ;
        $sql .= " WHERE indikator_id = '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_hasil_mutu($id_indikator,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND  (b.bulan BETWEEN 01 AND '".$bulan."') AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND (b.bulan BETWEEN 01 AND '".$bulan."') AND b.tahun='".$tahun."' ";
        }

        // return $sql;
        return $this->db->query($sql)->result();
    }

    public function get_hasil_mutu_listrs($id_indikator,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.bulan ='".$bulan."' AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND b.bulan = '".$bulan."' AND b.tahun='".$tahun."' ";
        }

        // return $sql;
        return $this->db->query($sql)->result();
    }

    public function get_hasil_mutu_row($id_pendataan,$id_indikator,$author)
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND b.pendataan_id ='".$id_pendataan."' ";
        
        return $this->db->query($sql)->row();
    }

    public function get_hasil_byidpendataan($idpendataan,$author,$bulan,$tahun)
    {
        $sql = " SELECT * FROM ".$this->tb_mutu_hasil ." WHERE pendataan_id ='".$idpendataan."' AND author='".$author."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";
        return $this->db->query($sql)->row();
    }

    public function get_hasil_rekap($id_indikator)
    {
        $sql = " SELECT * FROM ".$this->tb_mutu_hasil ." WHERE indikator_id ='".$id_indikator."' ";
        return $this->db->query($sql)->result();
    }

    public function get_analisarencana_row($id_indikator,$author,$bulan,$tahun)
    {   
        $where = [
            'indikator_id'=>$id_indikator,
            'author'    => $author,
            'bulan'     => $bulan,
            'tahun'     => $tahun
        ];

        $this->db->select('*');
        $this->db->from($this->tb_mutu_analisarencana);
        $this->db->where($where);
        return $this->db->get()->row();
    }

    public function get_list_detailanalisarencana()
    {
        $post           = $this->input->post();
        $id_indikator   = $this->security->sanitize_filename($post['id_indikator']);
        
        $filterindikatormutu = $post['filterindikatormutu'];
        $array_data = explode('-', $filterindikatormutu);
        
        $bulan = $array_data[0];
        $tahun = $array_data[1];

        $this->db->select('*');
        $this->db->from($this->tb_mutu_analisarencana);
        $this->db->where(['indikator_id'=>$id_indikator,'bulan'=>$bulan,'tahun'=>$tahun]);
        return $this->db->get()->result();
        // return $this->db->last_query();

    }

    public function get_nama_unit_pengaturanmutu($userid)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturan);
        $this->db->where(['userid_akses'=>$userid]);
        $query = $this->db->get()->row();
        
        $nama_unit = ( !empty($query) ) ? $query->unit : '';

        return $nama_unit;
    }

    public function get_pengaturan_indikator_row($indikator_id)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturan);
        $query = $this->db->get()->result();
        
        foreach( $query as $row ){
            $indikator_arr = explode(",",$row->indikator_id);
            $key = array_search($indikator_id,$indikator_arr);
            
            unset($indikator_arr[$key]);

            $im_indikatorid = ( empty($indikator_arr) ) ? 0 : implode(',',$indikator_arr);
            
            $this->db->where(['id_pengaturan'=>$row->id_pengaturan]);
            $this->db->update($this->tb_mutu_pengaturan, ['indikator_id'=>$im_indikatorid]);
        }
    }
    
    public function get_listpdsa()
    {
        $sql = "SELECT idpdsa, judul_indikator,cara_perbaikan,siklus,plan_rencana,plan_harap,do,study,act,penanggungjawab ";
        $sql .= " FROM ".$this->tb_mutu_lembarpdsa ;

        return $this->db->query($sql)->result();
    }

    public function get_editpdsa($id)
    {
        $sql = "SELECT idpdsa, judul_indikator,cara_perbaikan,siklus,plan_rencana,plan_harap,do,study,act,penanggungjawab ";
        $sql .= " FROM ".$this->tb_mutu_lembarpdsa ;
        $sql .= " WHERE idpdsa = '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_edittindakan($id)
    {
        $sql = "SELECT id_tindakan,idpdsa,tindakan,pic,tanggal ";
        $sql .= " FROM ".$this->tb_mutu_tindakan ;
        $sql .= " WHERE idpdsa = '".$id."'";

        return $this->db->query($sql)->result();
    }

    public function get_dokumen($id)
    {
        $sql = "SELECT filename, pathname ";
        $sql .= " FROM ".$this->tb_mutu_dokumen_pdsa ;
        $sql .= " WHERE idpdsa = '".$id."'";

        return $this->db->query($sql)->result();
    }

    //SURVILENS IKP

    public function get_editikp($id='')
    {
        $sql2 = "SELECT id_daftarikp, judulikp";
        $sql2 .= " FROM ".$this->tb_mutu_daftar_ikp ;
        if(!empty($id)){
            $sql2 .= " WHERE id_daftarikp=$id";
        }

        return $this->db->query($sql2)->row();
    }

    public function get_listdaftarikprs($tanggal,$bulan,$tahun,$userid)
    {
        $sql3 = "SELECT id_daftarikp,judulikp";
        $sql3 .= " FROM ".$this->tb_mutu_daftar_ikp ;
        // $sql3 .= "WHERE userid='".$userid."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";   
        return $this->db->query($sql3)->result();
    }

    public function get_filterikprs($bulan,$tahun)
    {
        $sql3 = "SELECT id_daftarikp,judulikp";
        $sql3 .= " FROM ".$this->tb_mutu_daftar_ikp ;
        return $this->db->query($sql3)->result();
    }

    public function get_filterrekapikp($bulan,$tahun)
    {
        $sql3 = "SELECT id_daftarikp,judulikp";
        $sql3 .= " FROM ".$this->tb_mutu_daftar_ikp ;
        return $this->db->query($sql3)->result();
    }

    public function get_listhasilikprs($tanggal,$bulan,$tahun,$daftarikp_id,$author)
    {
        $sql = "SELECT SUM(nilai_survey) ";
        $sql .= " FROM ".$this->tb_mutu_rekapikp ;
        $sql .= " WHERE tanggal='".$tanggal."' AND bulan='".$bulan."' AND tahun='".$tahun."' AND daftarikp_id='".$daftarikp_id."' AND author='".$author."' ";
        return $this->db->query($sql)->row();       
    }

    public function get_hasil_ikp($id_indikator,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.indikator_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_indikator." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_hasil." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.indikator_id='".$id_indikator."' AND b.author='".$author."' AND b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
        }
        // return $sql;
        return $this->db->query($sql)->result();
    }

    public function get_listrekapikprs($bulan,$tahun,$userid)
    {
        $sql = "SELECT id_daftarikp,judulikp";
        $sql .= " FROM ".$this->tb_mutu_daftar_ikp ; 
        // $sql .= "WHERE bulan='".$bulan."' AND tahun='".$tahun."' ";
        return $this->db->query($sql)->result();
    }

    public function get_rekapikprs($bulan,$tahun,$daftarikp_id,$author)
    {
        $sql = "SELECT * ";
        $sql .= " FROM ".$this->tb_mutu_rekapikp ;
        $sql .= " WHERE bulan='".$bulan."' AND tahun='".$tahun."' AND daftarikp_id='".$daftarikp_id."' AND author='".$author."' ";
        return $this->db->query($sql)->row();
    }

     /**
     * mr.ganteng
     */
    public function get_all_norm($searchTerm)
    {
        $query = $this->db->select('ppas.norm,pper.namalengkap,pper.idperson')
                ->from('person_pasien ppas')
                ->join('person_person pper','ppas.idperson = pper.idperson','LEFT')
                ->where('ppas.status_pasien','A')
                ->where("ppas.norm like '%".$searchTerm."%' ")
                ->limit(20)
                ->get()->result_array();

        $data = array();
        foreach($query as $row){
            $data[] = array("id"=>$row['idperson'], "text"=>$row['norm'].' | '.$row['namalengkap']);
        }
        
        return $data;               
    }

    public function get_person_byid($idperson)
    {
        $query = $this->db->select('namalengkap,idperson,tanggallahir')
                ->from('person_person')
                ->where("idperson",$idperson)
                ->limit(1)
                ->get()->row_array();
        
        return $query;               
    }

    public function get_person_bynorm($norm){
        $norm = $this->db->select('norm,idperson')
                        ->from('person_pasien')
                        ->where('norm',$norm)
                        ->get()->row();
        return $norm;
    }

    public function _get_query_datatable_pelaporan_ikp(){
       $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array
       $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;
       $userid = get_session_userid();
       
       if( $is_superuser ){
            $query = $this->db->select('idpelaporanikp,norm,nama,penanggungjawab_pasien')
                              ->from('mutu_pelaporan_ikp')
                              ->get();
            return $query;
       } else {
            $query = $this->db->select('idpelaporanikp,norm,nama,penanggungjawab_pasien')
                              ->from('mutu_pelaporan_ikp')
                              ->WHERE('author',$userid)
                              ->get();
            return $query;
       }
    }

    public function get_dt_pelaporan_ikp($limit = false)
    {
    
        $post           = $this->input->post();

        $start          = $post['start'];
        $length         = $post['length'];

        $search         = $post['search'];
        $value          = $search['value'];
        $regex          = $search['regex'];

        $column = [
            'norm',
            'nama', 
            'penanggungjawab_pasien',
        ];

        $column_order = [
            'norm',
            'nama', 
            'penanggungjawab_pasien',
        ];

        $order = ['idpelaporanikp'=>'desc'];

        $i = 0;
        foreach ($column as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($column) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

       // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        }else if (isset($order)) {
            $orders = $order;
            $this->db->order_by(key($orders), $orders[key($orders)]);
        }
        
        if($limit && $length != '' && $length != '-1'){
            $this->db->limit($length,$start);
        }
        
        return $this->_get_query_datatable_pelaporan_ikp();   

    }

    public function count_filtered()
    {
        $query = $this->get_dt_pelaporan_ikp(true);
        return $query->num_rows();
    }

    public function count_all()
    {
        $query = $this->get_dt_pelaporan_ikp();
        $results = $query->result();
        return $query->num_rows();
    }
    
    public function get_pilihanikprs($id_daftarikp)
    {
        $sql = "SELECT judulikp, id_daftarikp ";
        $sql .= " FROM ".$this->tb_mutu_daftar_ikp ;
        $sql .= " WHERE id_daftarikp='".$id_daftarikp."'";
        return $this->db->query($sql)->row();
    }

    public function get_list_detail_ikprs()
    {
        $post           = $this->input->post();
        $id_daftarikp   = $post['id_daftarikp'];
        $bulan          = $post['bulan'];
        $tahun          = $post['tahun'];
      
        $this->db->select('*');
        $this->db->from($this->tb_mutu_rekapikp  );
        $this->db->where(['daftarikp_id'=>$id_daftarikp,'bulan'=>$bulan,'tahun'=>$tahun]);
        $this->db->group_by('author');
        return $this->db->get()->result();

    }

    public function get_nama_unit_pengaturanikp($userid)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturanikp);
        $this->db->where(['userid_akses'=>$userid]);
        $query = $this->db->get()->row();
        
        $nama_unit = ( !empty($query) ) ? $query->unit : '';

        return $nama_unit;
    }


    //SURVILENS PPI

    public function get_listdaftarppirs($bulan,$tahun)
    {
        $sql = "SELECT id_daftarppi,tanggal,judul_ppi,definisi_operasional,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_daftarppi ;
        // $sql .= " WHERE Month(tanggal) = '".$bulan."' AND YEAR(tanggal) ='".$tahun."' ";

        return $this->db->query($sql)->result();
    }

    public function get_editppi($id)
    {
        $sql = "SELECT id_daftarppi,tanggal,judul_ppi,definisi_operasional,target,satuan ";
        $sql .= " FROM ".$this->tb_mutu_daftarppi ;
        $sql .= " WHERE id_daftarppi= '".$id."'";

        return $this->db->query($sql)->row();
    }

    public function get_editpendataanppi($id)
    {
        $sql = "SELECT id_pendataan,hasil, daftarppi_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi;
        $sql .= " WHERE daftarppi_id = '".$id."'";

        return $this->db->query($sql)->result();
    }

    public function get_editpendataanppi_row($id)
    {
        $sql = "SELECT id_pendataan,hasil, daftarppi_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi;
        $sql .= " WHERE daftarppi_id = '".$id."'";
        return $this->db->query($sql)->row();
    }

    public function get_editpendataanppi_row2($id,$bulan,$tahun)
    {
        $sql = "SELECT id_pendataan,hasil, daftarppi_id,tipe_variabel,IF(tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi;
        $sql .= " WHERE daftarppi_id = '".$id."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";
        return $this->db->query($sql)->row();
    }

    public function get_hasil_byidpendataanppi($author,$bulan,$tahun)
    {
        $sql = "SELECT * ";
        $sql .= " FROM ".$this->tb_mutu_rekap_ppi ;
        $sql .= " WHERE bulan='".$bulan."' AND tahun='".$tahun."' AND author='".$author."' ";
        return $this->db->query($sql)->row();
    }

    public function get_pengaturan_ppi_row($daftarppi_id)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturanppi);
        $query = $this->db->get()->result();
        
        foreach( $query as $row ){
            $indikator_arr = explode(",",$row->daftarppi_id);
            $key = array_search($daftarppi_id,$indikator_arr);
            
            unset($indikator_arr[$key]);

            $im_indikatorid = ( empty($indikator_arr) ) ? 0 : implode(',',$indikator_arr);
            
            $this->db->where(['id_pengaturan'=>$row->id_pengaturan]);
            $this->db->update($this->tb_mutu_pengaturanppi, ['daftarppi_id'=>$im_indikatorid]);
        }
    }

    public function get_hasil_ppi($id_daftarppi,$tanggal="",$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.daftarppi_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_rekap_ppi." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' b.tanggal='".$tanggal."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND  b.tanggal='".$tanggal."' b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.author='".$author."' AND b.tanggal='".$tanggal."' AND  b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
        }
  
        return $this->db->query($sql)->result();
    }

    public function get_hasil_ppi2($id_daftarppi,$bulan="",$tahun="")
    {
        $sql = "SELECT a.id_pendataan,a.daftarppi_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_rekap_ppi." b ON b.pendataan_id = a.id_pendataan ";
        $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
        return $this->db->query($sql)->result();
    }

    public function get_hasil_ppi3($id_daftarppi,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.daftarppi_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_rekap_ppi." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND  b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.author='".$author."' AND  b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
        }

        return $this->db->query($sql)->result();
    }

    public function get_hasil_ppirekap($id_daftarppi,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.daftarppi_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_rekap_ppi." b ON b.pendataan_id = a.id_pendataan ";
        if( empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND  (b.bulan BETWEEN 01 AND '".$bulan."') AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.author='".$author."' AND  (b.bulan BETWEEN 01 AND '".$bulan."') AND b.tahun='".$tahun."' ";
        }

        return $this->db->query($sql)->result();
    }

    public function get_hasil_ppirekap2($id_daftarppi,$bulan="",$tahun="",$author="")
    {
        $sql = "SELECT a.id_pendataan,a.daftarppi_id,a.tipe_variabel,IF(a.tipe_variabel = 0,'Numerator','Denumerator') as variabel,ppirs, ";
        $sql .= " b.* ";
        $sql .= " FROM ".$this->tb_mutu_pendataan_ppi." a " ;
        $sql .= " LEFT JOIN ".$this->tb_mutu_rekap_ppi." b ON b.pendataan_id = a.id_pendataan ";
        if(empty($author) ){
            if(empty($bulan)){
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.tahun='".$tahun."' ";
            }else{
                $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
            }
        }else{
            $sql .= " WHERE a.daftarppi_id='".$id_daftarppi."' AND b.author='".$author."' AND b.bulan='".$bulan."' AND b.tahun='".$tahun."' ";
        }

        return $this->db->query($sql)->result();
    }

    public function get_listppirs($tanggal,$bulan,$tahun,$userid)
    {
        $sql3 = "SELECT id_daftarppi,tanggal,judul_ppi,definisi_operasional,target,satuan";
        $sql3 .= " FROM ".$this->tb_mutu_daftarppi ;
        // $sql3 .= "WHERE userid='".$userid."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";   
        return $this->db->query($sql3)->result();
    }

    public function get_filterppi($bulan,$tahun)
    {
        $sql3 = "SELECT id_daftarppi,judul_ppi";
        $sql3 .= " FROM ".$this->tb_mutu_daftarppi ;
        return $this->db->query($sql3)->result();
    }

    public function get_listrekapppirs($bulan,$tahun,$userid)
    {
        $sql3 = "SELECT id_daftarppi,tanggal,judul_ppi,definisi_operasional,target,satuan";
        $sql3 .= " FROM ".$this->tb_mutu_daftarppi ;
        // $sql3 .= "WHERE userid='".$userid."' AND bulan='".$bulan."' AND tahun='".$tahun."' ";   
        return $this->db->query($sql3)->result();
    }

    public function get_nama_unit_pengaturanppi($userid)
    {
        $this->db->select('*');
        $this->db->from($this->tb_mutu_pengaturanppi);
        $this->db->where(['userid_akses'=>$userid]);
        $query = $this->db->get()->row();
        
        $nama_unit = ( !empty($query) ) ? $query->unit : '';

        return $nama_unit;
    }

    public function get_list_detail_ppirs($bulan,$tahun)
    {
        $post           = $this->input->post();
        $id_daftarppi   = $post['id_daftarppi'];
        $bulan          = $post['bulan'];
        $tahun          = $post['tahun'];
      
        $this->db->select('*');
        $this->db->from($this->tb_mutu_rekap_ppi  );
        $this->db->where(['daftarppi_id'=>$id_daftarppi,'bulan'=>$bulan,'tahun'=>$tahun]);
        $this->db->group_by('author');
        return $this->db->get()->result();

    }

    public function get_filterrekapppi($bulan,$tahun)
    {
        $sql3 = "SELECT id_daftarppi,judul_ppi";
        $sql3 .= " FROM ".$this->tb_mutu_daftarppi ;
        return $this->db->query($sql3)->result();
    }

}
?>