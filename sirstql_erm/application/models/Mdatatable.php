<?php
 
class Mdatatable extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }
 
    private function _get_datatables($order,$column_order,$column_search,$arrQ_builder,$table)
    {
        $i = 0;
     
        foreach ($column_search as $item) // looping awal
        {
            if(isset($_POST['search']['value'])) // jika datatable mengirimkan pencarian dengan metode POST
            {
                 
                if($i===0) // loop awal
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($order))
        {
            $order = $order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        $this->db->from($table);
        // -- jika ada join antar table
        if(!empty($arrQ_builder))
        {
            switch ($arrQ_builder) {
                case 'ambildatapendaftaran':
                    
                    break;
                case 'mastertarif':
                    $this->db->join('rs_icd', 'rs_icd.icd = rs_mastertarif.icd','left');
                    $this->db->join('rs_jenistarif', 'rs_jenistarif.idjenistarif = rs_mastertarif.idjenistarif','left');
                    $this->db->join('rs_kelas', 'rs_kelas.idkelas = rs_mastertarif.idkelas','left');
                    $this->db->join('keu_tipeakun', 'keu_tipeakun.kode = rs_mastertarif.kodetipeakun','left');
                    // if(!empty($this->input->post('total')))
                    // {
                    //     $this->db->where('total',$this->input->post('total'));
                    // }
                    break;
                case 'masterbarang':
                    $this->db->join('rs_jenistarif', 'rs_jenistarif.idjenistarif = rs_barang.idjenistarif','left');
                    $this->db->join('rs_satuan', 'rs_satuan.idsatuan = rs_barang.idsatuan','left');
                    $this->db->join('rs_sediaan', 'rs_sediaan.idsediaan = rs_barang.idsediaan','left');
//                    SELECT rbd.idbarangpembelian,rbp.idbarang, sum(CASE WHEN rbd.jenisdistribusi='masuk' THEN rbd.jumlah ELSE - jumlah END ) stok FROM rs_barang_distribusi rbd , rs_barang_pembelian rbp 
//where rbp.idbarangpembelian=rbd.idbarangpembelian and rbd.jenisdistribusi!='transformasihasil' and rbd.jenisdistribusi!='transformasiasal' GROUP by rbp.idbarang
                    break;
                case 'rsjadwal':
                    $this->db->join('rs_unit', 'rs_unit.idunit = rs_jadwal.idunit','left');
                    $this->db->join('person_pegawai', 'person_pegawai.idpegawai = rs_jadwal.idpegawaidokter','left');
                    $this->db->join('person_person', 'person_person.idperson = person_pegawai.idperson','left');
                    $this->db->join('antrian_loket', 'antrian_loket.idloket = rs_jadwal.idloket','left');
                    $this->db->join('rs_jadwal_grup', 'rs_jadwal_grup.idjadwalgrup = rs_jadwal.idjadwalgrup','left');
                    $this->db->order_by("tanggal","desc");
                    break;
                case 'datainduk':
                    $this->db->join('person_pasien', 'person_pasien.idperson = person_person.idperson','right');
                    $this->db->where('person_person.status_person','A');
                    $this->db->where('person_pasien.status_pasien','A');
                    break;
                case 'rs_barangpemeriksaan':
                    $this->db->select("person_pendaftaran.waktu, rs_barang.namabarang, rs_barang.kode, rs_satuan.namasatuan, rs_jenistarif.jenistarif, rs_barangpemeriksaan.jumlahpemakaian, rs_barangpemeriksaan.harga, rs_barangpemeriksaan.total, rs_barangpemeriksaan.kekuatan, rs_barangpemeriksaan.idbarangpemeriksaan");
                    $this->db->join('rs_barang', 'rs_barang.idbarang = rs_barangpemeriksaan.idbarang','left');
                    $this->db->join('person_pendaftaran', 'person_pendaftaran.idpendaftaran = rs_barangpemeriksaan.idpendaftaran','left');
                    $this->db->join('rs_jenistarif', 'rs_jenistarif.idjenistarif = rs_barang.idjenistarif','left');
                    $this->db->join('rs_satuan', 'rs_satuan.idsatuan = rs_barang.idsatuan','left');
                    if($this->input->post('awal') && $this->input->post('akhir'))
                    {
                        $this->db->where('date(waktu) >="'.$this->input->post('awal').'" ');
                        $this->db->where('date(waktu) <="'.$this->input->post('akhir').'"');
                    }
                    break;
                case 'person_pasien':
                    $this->db->join('person_person', 'person_person.idperson = person_pasien.idperson','left');
                    $this->db->join('demografi_pendidikan', 'demografi_pendidikan.idpendidikan = person_person.idpendidikan','left');
                    $this->db->join('demografi_pekerjaan', 'demografi_pekerjaan.idpekerjaan = person_person.idpekerjaan','left');
                    break;
                case 'rs_icd':
                    $this->db->join('rs_icd_golongansebabpenyakit', 'rs_icd_golongansebabpenyakit.idgolongansebabpenyakit = rs_icd.idgolongansebabpenyakit','left');
                    $this->db->join('rs_jenispenyakit rj', 'rj.idjenispenyakit = rs_icd.idjenispenyakit','left');
                    # code...
                    break;
                
                
                default:
                    # code...
                    break;
            }
        }
    }
    
    public function get_datatables($order,$column_order,$column_search,$arrQ_builder,$table)
    {
        $this->_get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
        
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    public function count_filtered($order,$column_order,$column_search,$arrQ_builder,$table)
    {
        $this->_get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all($order,$column_order,$column_search,$arrQ_builder,$table)
    {
        $this->_get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
        return $this->db->count_all_results();
    }
    public function draw($order,$column_order,$column_search,$arrQ_builder,$table)
    {
        if(isset($_POST['draw'])) 
        {
        $this->_get_datatables($order,$column_order,$column_search,$arrQ_builder,$table);
            // $this->db->where();
        } 
    }
    
    /*Start Custome Data Table*/
    public function like($column_search=[],$post_search)
    {
        $i = 0;
        if(empty(!$column_search))
        {
            foreach ($column_search as $item)
            {
                if(isset($post_search))
                {
                    if($i===0)
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $post_search);
                    }
                    else
                    {
                        $this->db->or_like($item, $post_search);
                    }

                    if(count($column_search) - 1 == $i) 
                    $this->db->group_end(); 
                }
                $i++;
            }
        }
    }

    public function order($column_order=[])
    {
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
    }
    public function limit($length, $start)
    {
        if($length != -1){$this->db->limit($length, $start);}
    }
    public function recordsTotal($query)
    {
        if($query){return $query->num_rows();}
        return 0;
    }
    
    public function recordsFiltered($query)
    {
        if($query){return $query->num_rows();}
        return 0;
    }
    /*End Custome Setting Datatable*/

    /**
    * ambil data jadwal operasi
    */
    public function dt_operasi()
    {
        $tahunbulan = date('Ym',strtotime($_POST['tanggal1']));
        $query = $this->db->select('rjo.idpendaftaran, rjo.idjadwaloperasi, rjo.jaminan,'
                . '(select idoperasi from rs_operasi where idjadwaloperasi=rjo.idjadwaloperasi limit 1) as idoperasi, '
                . 'rjo.terlaksana, pp.norm, namadokter(rjo.iddokteroperator) dokter,pper.alamat, pper.identitas,pper.namalengkap, namaunit(rjo.idunit)namaunit,rjio.jenistindakan, rjo.waktuoperasi, rjo.kodebooking, usia(pper.tanggallahir) as usia, pper.tanggallahir, pper.alamat, ppas.nojkn, rjo.verifjkn,'
                . 'pp.jenispemeriksaan, pp.waktuperiksa')
                ->join('person_pendaftaran pp','pp.idpendaftaran=rjo.idpendaftaran')
                ->join('rs_pemeriksaan rp','rp.idpemeriksaan=rjo.idpemeriksaan')
                // ->join('rs_jadwal rj','rj.idjadwal=rp.idjadwal') // Andri disable this code to fix bug [0000634] -> Perbaikan bug : Beberapa jadwal operasi tidak muncul
                ->join('person_pasien ppas','ppas.norm=pp.norm')
                ->join('person_person pper','pper.idperson=ppas.idperson')
                ->join('rs_jenistindakanoperasi rjio','rjio.idjenistindakan=rjo.idjenistindakan');
            
            // $query = $this->db->where('rjo.tahunbulan',$tahunbulan);
            $query = $this->db->where('date(rjo.waktuoperasi) BETWEEN "'.$_POST["tanggal1"].'" and "'.$_POST["tanggal2"].'" ');
            $query = $this->db->where('rjo.terlaksana!=',3);
            $query = $this->db->get('rs_jadwal_operasi rjo');
        
        if($query){
            return $query;
        }
        return false;
    }
    /**
     * Get Jadwal Pasie Operasi
     * @param type $limit Limit Data
     * @return type
     */
    public function dt_operasi_($limit=false)
    {
        $column = ['rjo.waktuoperasi',
                    'rjo.kodebooking',
                    false,
                    false,
                    'rjio.jenistindakan',
                    false,
                    'pp.norm',
                    'pper.namalengkap',
                    'rjo.terlaksana',
                    false];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_operasi();
    }
    
    public function totdt_operasi()
    {
        return $this->recordsTotal($this->dt_operasi());
    }

    public function filterdt_operasi()
    {
        return $this->recordsFiltered($this->dt_operasi_());
    }
    
    /**end ambil data jadwal operasi*/
    
    
    // penjualan Bebas
    public function dt_pembelianbebas()
    {
        $query = $this->db->select(' ktb.idtagihan,ktb.idbarangpembelibebas,ktb.waktubayar,ktb.nominal,ktb.kekurangan,ktb.jenistagihan, ktb.keterangan,rbp.waktu, rbp.nama as namanonpeg, ppai.titeldepan,pp.namalengkap,ppai.titelbelakang, rbp.idpegawai, ktb.status')
                ->join('rs_barang_pembelibebas rbp','rbp.idbarangpembelibebas = ktb.idbarangpembelibebas')
                ->join('person_pegawai ppai','ppai.idpegawai=rbp.idpegawai','left')
                ->join('person_person pp','pp.idperson=ppai.idperson','left');
        // if($_POST['tanggal']) { $query = $this->db->where('DATE(rbp.waktu)',$_POST["tanggal"]);}
        if($_POST['tanggal1'] && $_POST['tanggal2']) { 
            $tanggal1 = $_POST['tanggal1'];
            $tanggal2 = $_POST['tanggal2'];
            $query = $this->db->where("DATE(rbp.waktu) BETWEEN '".$tanggal1."' AND '".$tanggal2."'");
        }
        if($_POST['ispegawai']){ $query = $this->db->where('rbp.idpegawai is  NOT NULL');}
        $query = $this->db->get('keu_tagihan_bebas ktb');
        
        if($query->num_rows() > 0)
        {
            return $query;
        }
        return false;
    }

    public function dt_pembelianbebas_($limit=false)
    {
        $column = ['ktb.idtagihan', isset($_POST['ispegawai']) ? (($_POST['ispegawai']=='on') ? 'pp.namalengkap' : 'rbp.nama' ) : 'rbp.nama','rbp.waktu','ktb.potongan','ktb.keterangan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pembelianbebas();   
    }

    public function totdt_pembelianbebas()
    {
        return $this->recordsTotal($this->dt_pembelianbebas());
    }

    public function filterdt_pembelianbebas()
    {
        return $this->recordsFiltered($this->dt_pembelianbebas_());
    }

    // SKDP
    /**
     * [+] rs.idpendaftaransebelum
     */
    public function dt_skdp()
    {
        $query = $this->db->select('rs.idpendaftaransebelum,concat(ifnull(rs.tanggal,"")," ",ifnull(rs.jam,"")) waktuinput, rs.noskdpql, rs.nokontrol, ppas.nojkn, pp.norm, concat(ifnull(pper.identitas,"")," " ,pper.namalengkap) namalengkap, rs.tanggalskdp, ru.namaunit, rs.pertimbangan, namadokter(rs.idpetugas) as petugas, (select namauser from login_user where iduser=rs.iduser) as user')
        ->join('person_pendaftaran pp','pp.idpendaftaran=rs.idpendaftaransebelum','left')
        ->join('rs_unit ru','ru.idunit=pp.idunit','left')
        ->join('person_pasien ppas','ppas.norm=pp.norm','left')
        ->join('person_person pper ','pper.idperson=ppas.idperson','left');
        if(isset($_POST['tanggal'])) { $query = $this->db->where('DATE(rs.tanggal)',$_POST["tanggal"]);}

        $query = $this->db->get('rs_skdp rs');
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }

    public function dt_skdp_($limit=false)
    {
        $column = ['rs.noskdpql','rs.nokontrol','ppas.nojkn','pp.norm','pper.namalengkap'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_skdp();   
    }

    public function totdt_skdp()
    {
        return $this->recordsTotal($this->dt_skdp());
    }

    public function filterdt_skdp()
    {
        return $this->recordsFiltered($this->dt_skdp_());
    }


    // inventaris barang datang
    public function dt_barangdatang()
    {
        $post = $this->input->post();
        if(isset($post['lunas']))
        {
            $lunas = $post['lunas'];
        }
        else
        {
            $lunas=0;
        }
        
        $query = $this->db->select(' a.*, b.distributor,datediff(a.tanggaljatuhtempo,a.tanggalfaktur) masahutang, datediff(a.tanggaljatuhtempo,now()) tempo,pbf.namapbf')->join('rs_barang_distributor b','b.idbarangdistributor = a.idbarangdistributor','left')->join('rs_barang_pbf pbf','pbf.idbarangpbf=b.idbarangpbf','left');
        if($lunas)
        {
            $query = $this->db->where('a.tanggalpelunasan between "'.$post['tanggal1'].'" and "'.$post['tanggal2'].'" ');
            $query = $this->db->where('a.kekurangan',0);
        }
        else
        {
            if($_POST['filtertanggal']=='tanggalfaktur')
            {
                $query = $this->db->where("a.tanggalfaktur BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'");
            }
            else
            {
                $query = $this->db->where("a.tanggalinput BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'");
            }
        }
        
        $query = ((isset($post['idbf']))? $this->db->where('a.idbarangdistributor',$post['idbf']) :'');
        $query = (( !isset($post['idbf']))? $this->db->where('a.idunit',$this->session->userdata('idunitterpilih')) : '');
        $query = $this->db->get('rs_barang_faktur a');
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_barangdatang_($limit=false)
    {
        $column = ['b.distributor','b.distributor','a.nofaktur','a.tanggalfaktur','a.tanggalpelunasan','a.tanggaljatuhtempo','a.tanggaljatuhtempo','a.statusfaktur','a.totaltagihan','a.dibayar','a.kekurangan'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_barangdatang();   
    }
    public function total_dt_barangdatang(){ return $this->recordsTotal($this->dt_barangdatang());}
    public function filter_dt_barangdatang(){return $this->recordsFiltered($this->dt_barangdatang_());}
    
    
    //menampilkan data table pesanan farmasi ke gudang
    public function dt_pesankegudang()
    {
        $query = $this->db->select('bp.nofaktur,bp.jenistransaksi, bp.waktu, '
                . '(select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, '
                . '(select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,'
                . 'bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan, bp.idpersonpetugas');
        if( isset($_POST['tanggal1']) && isset($_POST['tanggal2']))
        { 
            $query = $this->db->where("date(bp.waktu) BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'");
        }        
        $query = $this->db->group_start();
        $query = $this->db->where('bp.idunittujuan',$this->session->userdata('idunitterpilih') );
        $query = $this->db->where( 'bp.jenistransaksi !=','retur');
        $query = $this->db->where( 'bp.jenistransaksi !=','returobatranap');
        $query = $this->db->where( 'bp.jenistransaksi !=','returobatpulang');
        $query = $this->db->group_end();
        $query = $this->db->get('rs_barang_pemesanan bp');
        if($query->num_rows() > 0){ return $query; }
        return false;
    }
    
    public function dt_pesankegudang_($limit=false)
    {
        $column = ['bp.nofaktur','bp.waktu','bp.idunit','bp.statusbarangpemesanan','bp.jenistransaksi','bp.idpersonpetugas'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pesankegudang();   
    }
    public function total_dt_pesankegudang(){ return $this->recordsTotal($this->dt_pesankegudang());}
    public function filter_dt_pesankegudang(){return $this->recordsFiltered($this->dt_pesankegudang_());}
    
     //menampilkan data table pesanan depo ke farmasi
    public function dt_permintaandepo()
    {
        $query = $this->db->select('bp.nofaktur,bp.jenistransaksi, bp.waktu, '
                . '(select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, '
                . '(select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,'
                . 'bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan, bp.idpersonpetugas');
        if( isset($_POST['tanggal1']) && isset($_POST['tanggal2']))
        { 
            $query = $this->db->where("date(bp.waktu) BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'");
        }        
        $query = $this->db->group_start();
        $query = $this->db->where('bp.idunit',$this->session->userdata('idunitterpilih') );
        $query = $this->db->where( 'bp.jenistransaksi !=','retur');
        $query = $this->db->where( 'bp.jenistransaksi !=','returobatranap');
        $query = $this->db->where( 'bp.jenistransaksi !=','returobatpulang');
        $query = $this->db->group_end();
        $query = $this->db->get('rs_barang_pemesanan bp');
        if($query->num_rows() > 0){ return $query; }
        return false;
    }
    
    public function dt_permintaandepo_($limit=false)
    {
        $column = ['bp.nofaktur','bp.waktu','bp.idunit','bp.statusbarangpemesanan','bp.jenistransaksi','bp.idpersonpetugas'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_permintaandepo();   
    }
    public function total_dt_permintaandepo(){ return $this->recordsTotal($this->dt_permintaandepo());}
    public function filter_dt_permintaandepo(){return $this->recordsFiltered($this->dt_permintaandepo_());}

// Inventaris - permintaan barang
    public function dt_permintaanbarang()
    {

        $query = $this->db->select('bp.nofaktur,bp.jenistransaksi, bp.waktu, (select namaunit from rs_unit_farmasi where idunit=bp.idunit) unitasal, (select namaunit from rs_unit_farmasi where idunit=bp.idunittujuan) unittujuan,bp.idunittujuan,bp.idunit, bp.statusbarangpemesanan, bp.idbarangpemesanan, bp.idpersonpetugas');
        
        
        
        if( isset($_POST['tanggal1']) && isset($_POST['tanggal2'])){ 
            $query = $this->db->where("date(bp.waktu) BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'");
        }
        
       
        if(isset($_POST['mode']))
        {
            $query = $this->db->group_start();
            if($_POST['mode']=='retur')
            {
                
                $query = $this->db->where('bp.idunit',$this->session->userdata('idunitterpilih') );
                $query = $this->db->where( 'bp.jenistransaksi !=','pesan');
                $query = $this->db->where( 'bp.jenistransaksi !=','pesanobatranap');
                $query = $this->db->where( 'bp.jenistransaksi !=','pesanobatpulang');
                $query = $this->db->or_where('bp.idunittujuan',$this->session->userdata('idunitterpilih') );
                $query = $this->db->where( 'bp.jenistransaksi !=','pesan');
                $query = $this->db->where( 'bp.jenistransaksi !=','pesanobatranap');
                $query = $this->db->where( 'bp.jenistransaksi !=','pesanobatpulang');
                
            }
            else
            {
                $query = $this->db->where('bp.idunit',$this->session->userdata('idunitterpilih') );
                $query = $this->db->where( 'bp.jenistransaksi !=','retur');
                $query = $this->db->where( 'bp.jenistransaksi !=','returobatranap');
                $query = $this->db->where( 'bp.jenistransaksi !=','returobatpulang');
                $query = $this->db->or_where('bp.idunittujuan',$this->session->userdata('idunitterpilih') );
                $query = $this->db->where( 'bp.jenistransaksi !=','retur');
                $query = $this->db->where( 'bp.jenistransaksi !=','returobatranap');
                $query = $this->db->where( 'bp.jenistransaksi !=','returobatpulang');
            }
            $query = $this->db->group_end();
        }
        $query = $this->db->get('rs_barang_pemesanan bp');
        
        if($query->num_rows() > 0){ return $query; }
        return false;
    }
    public function dt_permintaanbarang_($limit=false)
    {
        $column = ['bp.nofaktur','bp.waktu','bp.idunit','bp.statusbarangpemesanan','bp.jenistransaksi','bp.idpersonpetugas'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_permintaanbarang();   
    }
    public function total_dt_permintaanbarang(){ return $this->recordsTotal($this->dt_permintaanbarang());}
    public function filter_dt_permintaanbarang(){return $this->recordsFiltered($this->dt_permintaanbarang_());}
    
    
    
    
    
    // inventaris - transformasibarang
    public function dt_transformasibarang()
    {
        $query = $this->db->select("a.waktu, a.grupwaktu, group_concat(if(jenisdistribusi='transformasiasal',concat(d.namabarang, ', ', a.jumlah, ' ', e.namasatuan, ' exp:', b.kadaluarsa), NULL) separator '<br/>' ) as transformasiasal, group_concat(if(jenisdistribusi='transformasihasil',concat(d.namabarang, ', ', a.jumlah, ' ', e.namasatuan, ' exp:', b.kadaluarsa), NULL) separator '<br/>') as transformasihasil");
        $query = $this->db->join('rs_barang_pembelian b','b.idbarangpembelian=a.idbarangpembelian')->join('rs_barang d','d.idbarang=b.idbarang')->join('rs_satuan e','e.idsatuan=d.idsatuan');
        $date  = (isset($_POST['tanggal1']) && isset($_POST['tanggal2'])) ? true : false ;
        $query = $this->db->where("a.jenisdistribusi","transformasiasal");
        ($date) ? $this->db->where("date(a.waktu) BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'") : '';
        $query = $this->db->or_where("a.jenisdistribusi","transformasihasil");
        ($date) ? $this->db->where("date(a.waktu) BETWEEN '".$_POST['tanggal1']."' AND '".$_POST['tanggal2']."'") : '';
        $query = $this->db->group_by('a.grupwaktu');
        $query = $this->db->get('rs_barang_distribusi a');
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_transformasibarang_($limit=false)
    {
        $column = ['a.grupwaktu','a.waktu','a.grupwaktu','a.waktu'];
        (isset($_POST['cari'])) ? $this->like($column,$_POST['cari']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_transformasibarang();   
    }
    public function total_dt_transformasibarang(){ return $this->recordsTotal($this->dt_transformasibarang());}
    public function filter_dt_transformasibarang(){return $this->recordsFiltered($this->dt_transformasibarang_());}


    // list jadwal poli
    public function dt_jadwalpoli()
    {
        $aw = $this->input->post('cariawal');
        $ak = $this->input->post('cariakhir');
        $query = $this->db->select("rj.idjadwal, ru.namaunit, concat(ifnull(ppeg.titeldepan,''),' ',pp.namalengkap,' ',ifnull(ppeg.titelbelakang,'')) namalengkap, al.namaloket, rj.tanggal, rj.jamakhir, rj.quota, rj.status, rj.catatan, rjg.namagrup");
        $query = $this->db->join('rs_unit ru', 'ru.idunit = rj.idunit');
        $query = $this->db->join('person_pegawai ppeg', 'ppeg.idpegawai = rj.idpegawaidokter');
        $query = $this->db->join('person_person pp', 'pp.idperson = ppeg.idperson');
        $query = $this->db->join('antrian_loket al', 'al.idloket = rj.idloket');
        $query = $this->db->join('rs_jadwal_grup rjg', 'rjg.idjadwalgrup = rj.idjadwalgrup','left');
        (isset($aw) && isset($ak) ? $query = $this->db->where('rj.tahunbulan BETWEEN "'.gettahunbulan($aw).'" AND "'.gettahunbulan($ak).'"') : '' );
        (isset($aw) && isset($ak) ? $query = $this->db->where('date(rj.tanggal) BETWEEN "'.$aw.'"  AND "'.$ak.'"') : '' );
        $query = $this->db->order_by('rj.tanggal','desc');
        $query = $this->db->get('rs_jadwal rj');
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_jadwalpoli_($limit=false)
    {
        $column = ['ru.namaunit', 'al.namaloket', 'pp.namalengkap','rj.idjadwal','rj.idjadwal','rj.idjadwal','rj.idjadwal','rj.idjadwal','rj.idjadwal'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_jadwalpoli();   
    }
    public function total_dt_jadwalpoli(){ return $this->recordsTotal($this->dt_jadwalpoli());}
    public function filter_dt_jadwalpoli_(){return $this->recordsFiltered($this->dt_jadwalpoli_());}

    // tampil data kasir
    public function dt_kasir()
    {
        $tgl  = $this->input->post('tgl');
        $tgl2 = $this->input->post('tgl2');
        $idunit = $this->input->post('idunit');
        $idbangsal = $this->input->post('idbangsal');

        $tahun = date('Y', strtotime($tgl));
        $query = $this->db->select("ri.status  as  statusinap , ri.idbed, rbed.idbed,rbed.idbangsal,rbangsal.idbangsal,
        ppd.jenispemeriksaan, t.idpendaftaran, ppd.resumeprint, t.waktutagih,pper.idperson, 
        ppd.norm, ppd.nosep,
        ppd.statuspulang, pper.cetakkartu,
        (SELECT count(1) FROM keu_statusrekap WHERE idpendaftaran = t.idpendaftaran) as statusrekap, 
        ppd.idstatuskeluar,
        k.kelas , 
        carabayar,
        concat(ifnull(pper.identitas, ''), ' ', ifnull(pper.namalengkap, '')) as namalengkap, 
        concat(t.idtagihan, '/',  statustagihan , '/',  t.jenistagihan ) as statustagihan, 
        sum( (t.nominal + t.pembulatan - t.potongan) - if(t.statusbayar='dibayar',t.kekurangan,0)) as tagihan, 
        sum( t.dibayar - t.kembalian) as dibayar,    
        t.idtagihan,
        ispemeriksaanselesaisemua(t.idpendaftaran) as isselesaisemua, 
        statustagihan as cekstatustagih , 
        (select GROUP_CONCAT(ru.namaunit, ' : ' , rp.status,'<br>') from rs_pemeriksaan rp, rs_unit ru WHERE rp.idpendaftaran=t.idpendaftaran and ru.idunit=rp.idunit) as statuspemeriksaan,
        ri.idinap, ri.status, t.statusbayar,  t.jenistagihan, ppd.sudahcetakSEP, ppas.nojkn ");
        //if(not isnull(ri.idkelas) and ri.idkelas<ri. idkelasjaminan, 'jkn-naik-kelas', 
        $query = $this->db->join('person_pendaftaran ppd','ppd.idpendaftaran = t.idpendaftaran');
        $query = $this->db->join('rs_kelas k','k.idkelas = ppd.idkelas');
        // $query = $this->db->join('rs_pemeriksaan rsp','rsp.idpendaftaran=t.idpendaftaran');  
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->join('rs_inap ri','ri.idpendaftaran = t.idpendaftaran','left');

        $query = $this->db->join('rs_bed rbed','rbed.idbed = ri.idbed','left');
        $query = $this->db->join('rs_bangsal rbangsal','rbangsal.idbangsal = rbed.idbangsal','left');
           
        // $query = $this->db->join('rs_kelas k','k.idkelas = ppd.idkelas');
        
        $query = $this->db->where("t.tanggaltagih BETWEEN '".$tgl."' and '".$tgl2."'");
        $query = $this->db->where('t.jenistagihan!=','batal');
        $query = $this->db->where('ppd.idstatuskeluar !=',0);
        $query = $this->db->where('ppd.idstatuskeluar !=',3);
        $query = $this->db->where('ppd.idstatuskeluar !=',4);
        if(empty(!$this->input->post('norm')))
        {
            $query = $this->db->where('ppd.norm',$this->input->post('norm'));            
        }
        if(empty(!$idunit))
        {
            $query = $this->db->where('ppd.idunit',$idunit);
        }
        if(empty(!$idbangsal))
        {
            $query = $this->db->where('rbangsal.idbangsal',$idbangsal);
        }
        //$query = $this->db->where('ppd.idstatuskeluar != 3');
        $query = ($this->input->post('isbelumdibayar')=='on') ? $this->db->where('t.statusbayar','belumdibayar') : '';
        $query = $this->db->group_by(['t.idpendaftaran']);
        $query = $this->db->get('keu_tagihan t');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    
    public function dt_kasir_($limit=false)
    {
        $column = ['t.idtagihan', 't.waktutagih', 'ppas.norm','pper.namalengkap','ppd.nosep','k.kelas','ppd.carabayar','ppd.statuspulang','ppd.statustagihan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_kasir();   
    }
    public function totdt_kasir(){ return $this->recordsTotal($this->dt_kasir());}
    public function filterdt_kasir(){return $this->recordsFiltered($this->dt_kasir_());}
    
//    Tampil inventaris medis
    public function dt_inventariskantor()
    {
        $query = $this->db->select("ii.idinventaris, ii.noinventaris as nomorinventaris,ibd.nomorseri,ibd.nomorsertifikat,ir.namaruanginventaris, im.modelbarang,imr.merekbarang,ibd.tanggalpembelian,ibd.nilaiaset,ibd.nilaipenyusutan,ii.status,ibl.namablok,ib.namabaranginventaris, ii.keterangan");
        
        $query = $this->db->join('inventaris_barangdetail ibd','ibd.idbarangdetail=ii.idbarangdetail');
        $query = $this->db->join('inventaris_barang ib','ib.idbaranginventaris=ibd.idbaranginventaris');
        $query = $this->db->join('inventaris_model im','im.idmodelbarang=ibd.idmodelbarang','left');
        $query = $this->db->join('inventaris_merek imr','imr.idmerekbarang=im.idmerekbarang','left');
        $query = $this->db->join('inventaris_ruang ir','ir.idruanginventaris=ii.idruanginventaris','left');
        $query = $this->db->join('inventaris_blok ibl','ibl.idblokinventaris=ir.idblokinventaris','left');
        $query = $this->db->where('ibd.jenisalat','medis');
        
        $query = $this->db->get('inventaris_inventaris ii');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_inventariskantor_($limit=false)
    {
        $column = ['ib.namabaranginventaris','imr.merekbarang','im.modelbarang','ibd.nomorseri','ir.namaruanginventaris','ii.noinventaris','ibd.nomorsertifikat','ibd.tanggalpembelian','ibd.nilaiaset','ibd.nilaipenyusutan','ii.status'];
        (isset($_POST['search']['regex'])) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_inventariskantor();   
    }
    public function total_dt_inventariskantor(){ return $this->recordsTotal($this->dt_inventariskantor());}
    public function filter_dt_inventariskantor(){return $this->recordsFiltered($this->dt_inventariskantor_());}
    
    
    //inventaris non medis
    public function dt_inventarisnonmedis()
    {
        $query = $this->db->select("ib.kodeblok, ib.namablok , ir.koderuang, ir.namaruanginventaris, ir.idruanginventaris, ir.jumlahnonmedis");
        $query = $this->db->order_by("ir.jumlahnonmedis","desc");
        $query = $this->db->join('inventaris_ruang ir','ir.idblokinventaris = ib.idblokinventaris');        
        $query = $this->db->get('inventaris_blok ib');
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    
    public function dt_inventarisnonmedis_($limit=false)
    {
        $column = ['ib.kodeblok','ib.kodeblok','ib.namablok','ir.koderuang','ir.namaruanginventaris','ir.jumlahnonmedis'];
        (isset($_POST['search']['regex'])) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_inventarisnonmedis();   
    }
    public function total_dt_inventarisnonmedis(){return $this->recordsTotal($this->dt_inventarisnonmedis());}
    public function filter_dt_inventarisnonmedis(){return $this->recordsFiltered($this->dt_inventarisnonmedis_());}
    
    //detail inventaris non medis    
    public function dt_detailinventarisnonmedis()
    {
        $query = $this->db->select("ii.idinventaris, ii.noinventaris as nomorinventaris,ibd.nomorseri,ibd.nomorsertifikat, im.modelbarang,imr.merekbarang,ibd.tanggalpembelian,ibd.nilaiaset,ibd.nilaipenyusutan,ii.status,ib.namabaranginventaris, ii.keterangan");        
        $query = $this->db->join('inventaris_barangdetail ibd','ibd.idbarangdetail=ii.idbarangdetail');
        $query = $this->db->join('inventaris_barang ib','ib.idbaranginventaris=ibd.idbaranginventaris');
        $query = $this->db->join('inventaris_model im','im.idmodelbarang=ibd.idmodelbarang','left');
        $query = $this->db->join('inventaris_merek imr','imr.idmerekbarang=im.idmerekbarang','left');
        $query = $this->db->where('ii.idruanginventaris',$this->input->post('idruang'));
        $query = $this->db->where('ii.status !=','pindah');
        $query = $this->db->where('ii.status !=','recall');
        $query = $this->db->where('ii.status !=','hilang');
        $query = $this->db->where('ibd.jenisalat ','nonmedis');
        
        $query = $this->db->get('inventaris_inventaris ii');
        
        if($query->num_rows() > 0){
            return $query;
        }
        return false;
    }
    public function dt_detailinventarisnonmedis_($limit=false)
    {
        $column = ['ib.namabaranginventaris','imr.merekbarang','im.modelbarang','ibd.nomorseri','ii.noinventaris','ibd.nomorsertifikat','ibd.tanggalpembelian','ibd.nilaiaset','ibd.nilaipenyusutan','ii.status'];
        (isset($_POST['search']['regex'])) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_detailinventarisnonmedis();   
    }
    public function total_dt_detailinventarisnonmedis(){ return $this->recordsTotal($this->dt_detailinventarisnonmedis());}
    public function filter_dt_detailinventarisnonmedis(){return $this->recordsFiltered($this->dt_detailinventarisnonmedis_());}


//    Tampil data kategori aset
    public function dt_inv_barangkategori()
    {
        $query = $this->db->get('inventaris_barangkategori a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_inv_barangkategori_($limit=false)
    {
        $column = ['a.idbarangkategori','a.kategori'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_inv_barangkategori();   
    }
    public function total_dt_inv_barangkategori(){ return $this->recordsTotal($this->dt_inv_barangkategori());}
    public function filter_dt_inv_barangkategori(){return $this->recordsFiltered($this->dt_inv_barangkategori_());}
    
    //Tampil data Ruang Inventaris Alat
    //mahmud, clear
    public function dt_ruanginvalat()
    {
        $query = $this->db->select('ir.*, ib.kodeblok, ib.namablok')->join('inventaris_blok ib','ib.idblokinventaris=ir.idblokinventaris')->get('inventaris_ruang ir');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_ruanginvalat_($limit=false)
    {
        $column = ['ir.idruanginventaris','ib.namablok','ir.namaruanginventaris'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_ruanginvalat();   
    }
    public function total_dt_ruanginvalat(){ return $this->recordsTotal($this->dt_ruanginvalat());}
    public function filter_dt_ruanginvalat(){return $this->recordsFiltered($this->dt_ruanginvalat_());}
    
    //Tampil data Unit Inventaris Alat
    //mahmud, clear
    public function dt_blokinvalat()
    {
        $query = $this->db->get('inventaris_blok ib');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_blokinvalat_($limit=false)
    {
        $column = ['ib.idblokinventaris','ib.kodeblok','ib.namablok'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_blokinvalat();   
    }
    public function total_dt_blokinvalat(){ return $this->recordsTotal($this->dt_blokinvalat());}
    public function filter_dt_blokinvalat(){return $this->recordsFiltered($this->dt_blokinvalat_());}
    
    //Tampil data Merek Barang Inventaris Alat
    //mahmud, clear
    public function dt_merekinvalat()
    {
        $query = $this->db->get('inventaris_merek');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_merekinvalat_($limit=false)
    {
        $column = ['idmerekbarang','merekbarang'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_merekinvalat();   
    }
    public function total_dt_merekinvalat(){ return $this->recordsTotal($this->dt_merekinvalat());}
    public function filter_dt_merekinvalat(){return $this->recordsFiltered($this->dt_merekinvalat_());}
    
    //Tampil data Model Inventaris Alat
    //mahmud, clear
    public function dt_modelinvalat()
    {
        $query = $this->db->select('imd.*, im.merekbarang')->join('inventaris_merek im','im.idmerekbarang=imd.idmerekbarang')->get('inventaris_model imd');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_modelinvalat_($limit=false)
    {
        $column = ['imd.idmodelbarang','im.merekbarang','imd.modelbarang'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_modelinvalat();   
    }
    public function total_dt_modelinvalat(){ return $this->recordsTotal($this->dt_modelinvalat());}
    public function filter_dt_modelinvalat(){return $this->recordsFiltered($this->dt_modelinvalat_());}
    
    //Tampil Master Inventaris Alat
    //mahmud, clear
    public function dt_masterinvalat()
    {
        $query = $this->db->select('ib.*, ibk.kategori')
                ->join('inventaris_barangkategori ibk','ibk.idbarangkategori=ib.idbarangkategori','left')
                ->get('inventaris_barang ib');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_masterinvalat_($limit=false)
    {
        $column = ['ib.idbaranginventaris','ib.namabaranginventaris','ib.jenisalat','ibk.kategori'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_masterinvalat();   
    }
    public function total_dt_masterinvalat(){ return $this->recordsTotal($this->dt_masterinvalat());}
    public function filter_dt_masterinvalat(){return $this->recordsFiltered($this->dt_masterinvalat_());}
    
    //Tampil Item Inventaris Alat
    //mahmud, clear
    public function dt_iteminvalat()
    {
        $query = $this->db->select('ib.*,ibd.idbarangdetail, ibk.kategori, im.modelbarang,imr.merekbarang,ibd.nomorseri, ibd.nomorsertifikat, ibd.jumlah,rs.namasatuan, ibd.tanggalpembelian, ibd.tanggalpengujian,ibd.tanggalrekalibrasi, ibd.nilaiaset, ibd.nilaipenyusutan, ibd.bataspenyusutan')
                ->join(' inventaris_barang ib','ib.idbaranginventaris=ibd.idbaranginventaris')
                ->join('inventaris_model im','im.idmodelbarang=ibd.idmodelbarang','left')
                ->join('inventaris_merek imr','imr.idmerekbarang=im.idmerekbarang','left')
                ->join('inventaris_barangkategori ibk','ibk.idbarangkategori=ib.idbarangkategori','left')
                ->join('rs_satuan rs','rs.idsatuan=ibd.idsatuan','left')
                ->where('ibd.isdelete',0)
                ->get('inventaris_barangdetail ibd');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_iteminvalat_($limit=false)
    {
        $column = ['ibd.idbarangdetail','ib.namabaranginventaris','im.modelbarang','ibd.jumlah','rs.namasatuan','ibd.tanggalpembelian','ibd.tanggalpengujian','ibd.tanggalrekalibrasi','ibd.nilaiaset','ibd.nilaipenyusutan','ibd.bataspenyusutan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_iteminvalat();   
    }
    public function total_dt_iteminvalat(){ return $this->recordsTotal($this->dt_iteminvalat());}
    public function filter_dt_iteminvalat(){return $this->recordsFiltered($this->dt_iteminvalat_());}
    
    
    //    Tampil Riwayat Obat
    public function dt_riwayatobat()
    {
        $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idb')));
        $query = $this->db->select('rbd2.distributor,rbp2.namapbf, rbd.waktu, rbp.batchno, rbp.kadaluarsa,rbp.hargabeli,rbp.nominaldiskon,rbd.jumlah, rbd.jenisdistribusi, (select namaunit from rs_unit_farmasi where idunit=rbd.idunit) unitasal, (select namaunit from rs_unit_farmasi where idunit=rbd.idunittujuan) unittujuan');
        $query = $this->db->join('rs_barang_distribusi rbd','rbd.idbarangpembelian=rbp.idbarangpembelian');
        $query = $this->db->join('rs_barang_faktur rbf','rbf.idbarangfaktur=rbp.idbarangfaktur','left');
        $query = $this->db->join('rs_barang_distributor rbd2','rbd2.idbarangdistributor=rbf.idbarangdistributor','left');
        $query = $this->db->join('rs_barang_pbf rbp2','rbp2.idbarangpbf=rbd2.idbarangpbf','left');
        $query = $this->db->where('rbp.idbarang',$id);
        $query = $this->db->get('rs_barang_pembelian rbp');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_riwayatobat_($limit=false)
    {
        $column = ['rbp2.namapbf','rbd2.distributor','rbd.waktu','rbp.batchno','rbp.kadaluarsa','rbp.hargabeli','rbp.nominaldiskon','rbd.jumlah','rbd.jenisdistribusi'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_riwayatobat();   
    }
    public function total_dt_riwayatobat(){ return $this->recordsTotal($this->dt_riwayatobat());}
    public function filter_dt_riwayatobat(){return $this->recordsFiltered($this->dt_riwayatobat_());}
    
    //    Tampil Master Barang Pengeluaran
    //mahmud, clear
    public function dt_masterbarangpengeluaran()
    {
        $id = json_decode($this->encryptbap->decrypt_urlsafe("HIDDENTABEL", $this->input->post('idb')));
        $query = $this->db->select('b.idbarang, b.namabarang, s.namasatuan');
        $query = $this->db->join('rs_satuan s','s.idsatuan=b.idsatuan');
        $query = $this->db->where('b.isdelete','0');
        $query = $this->db->get('rs_pengeluaran_masterbarang b');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_masterbarangpengeluaran_($limit=false)
    {
        $column = ['b.idbarang','b.namabarang','s.namasatuan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_masterbarangpengeluaran();   
    }
    public function total_dt_masterbarangpengeluaran(){ return $this->recordsTotal($this->dt_masterbarangpengeluaran());}
    public function filter_dt_masterbarangpengeluaran(){return $this->recordsFiltered($this->dt_masterbarangpengeluaran_());}
    
    //    Tampil Master Biaya Kategori
    public function dt_kategoribiaya()
    {
        $query = $this->db->where('a.isdelete','0');
        $query = $this->db->get('rs_biaya_kategori a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_kategoribiaya_($limit=false)
    {
        $column = ['a.idkategoribiaya','a.kategoribiaya'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_kategoribiaya();   
    }
    public function total_dt_kategoribiaya(){ return $this->recordsTotal($this->dt_kategoribiaya());}
    public function filter_dt_kategoribiaya(){return $this->recordsFiltered($this->dt_kategoribiaya_());}
    //    Tampil Master Biaya Sub Kategori
    public function dt_subkategoribiaya()
    {
        $query = $this->db->select('a.idsubkategoribiaya, a.subkategoribiaya, b.kategoribiaya');
        $query = $this->db->join('rs_biaya_kategori b','b.idkategoribiaya=a.idkategoribiaya');
        $query = $this->db->where('a.isdelete','0');
        $query = $this->db->get('rs_biaya_subkategori a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_subkategoribiaya_($limit=false)
    {
        $column = ['a.idsubkategoribiaya','b.kategoribiaya','a.subkategoribiaya'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_subkategoribiaya();   
    }
    public function total_dt_subkategoribiaya(){ return $this->recordsTotal($this->dt_subkategoribiaya());}
    public function filter_dt_subkategoribiaya(){return $this->recordsFiltered($this->dt_subkategoribiaya_());}
    //    Tampil Master Biaya Sub Sub Kategori
    public function dt_subsubkategoribiaya()
    {
        $query = $this->db->select('a.idsubsubkategoribiaya, a.subsubkategoribiaya, b.subkategoribiaya, c.kategoribiaya');
        $query = $this->db->join('rs_biaya_subkategori b','b.idsubkategoribiaya=a.idsubkategoribiaya');
        $query = $this->db->join('rs_biaya_kategori c','c.idkategoribiaya=b.idkategoribiaya');
        $query = $this->db->where('a.isdelete','0');
        $query = $this->db->get('rs_biaya_subsubkategori a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_subsubkategoribiaya_($limit=false)
    {
        $column = ['a.idsubsubkategoribiaya','c.kategoribiaya','b.subkategoribiaya','a.subsubkategoribiaya'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_subsubkategoribiaya();   
    }
    public function total_dt_subsubkategoribiaya(){ return $this->recordsTotal($this->dt_subsubkategoribiaya());}
    public function filter_dt_subsubkategoribiaya(){return $this->recordsFiltered($this->dt_subsubkategoribiaya_());}
    
//    ADMINISTRASI RANAP
    public function dt_administrasiranap()
    {
        $post = $this->input->post();
        $query = $this->db->select("sum( (kk.nominal + kk.pembulatan - kk.potongan) - if(kk.statusbayar='dibayar',kk.kekurangan,0)) as nominal, "
                . "(ifnull(totalkekurangan(ri.idpendaftaran,'ranap'), 0)=0) as lunas, "
                . "ri.idpendaftaran, ri.idbed,rb.idbangsal, sb.statusbed, sb.file, "
                . "concat('Rencana: ',ifnull((select count(rip.idinap) from rs_inap_rencana_medis_pemeriksaan rip where rip.status='rencana' and rip.idinap=ri.idinap),''),'</br>Terlaksana: ',ifnull((select count(rip.idinap) from rs_inap_rencana_medis_pemeriksaan rip where rip.status='terlaksana' and rip.idinap=ri.idinap),'')) as pemeriksaan, "
                . "ri.idinap, ppd.idpendaftaran, ppd.norm, "
                . "concat('Masuk: ',ri.waktumasuk,if(ri.waktukeluar='0000-00-00 00:00:00','',concat('</br>Keluar: ',ri.waktukeluar))) as waktu, "
                . "namadokter(ri.idpegawaidokter) as namadokter, "
                . "namapasien(ppas.idperson) as namalengkap, pp.tanggallahir, usia(pp.tanggallahir) as usia, pp.notelpon, pp.alamat,"
                . "ppd.nosep, concat(rbl.namabangsal,"
                . "' (',rb.nobed,')') as namabangsal, "
                . "ri.idkelas,ri.status,"
                . "ppd.carabayar");
        $query = $this->db->join('person_pendaftaran ppd','ppd.idpendaftaran = ri.idpendaftaran','left');
        $query = $this->db->join('keu_tagihan kk','kk.idpendaftaran = ppd.idpendaftaran','left');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm','left');
        $query = $this->db->join('person_person pp','pp.idperson = ppas.idperson','left');
        $query = $this->db->join('person_pegawai ppai','ppai.idpegawai = ri.idpegawaidokter','left'); 
        $query = $this->db->join('person_person pperppai','pperppai.idperson = ppai.idperson','left');
        $query = $this->db->join('rs_bed rb','ri.idbed=rb.idbed','left');
        $query = $this->db->join('rs_bed_status sb','sb.idstatusbed=ri.idstatusbed','left');
        $query = $this->db->join('rs_bangsal rbl','rbl.idbangsal=rb.idbangsal','left');
        
        
        $query = ((empty($post['idbangsal']))? '' :  $this->db->where('rb.idbangsal',$post['idbangsal']) );
        
        
        if($post['status']=='ranap'){
            $query = $this->db->group_start();
            $query = $this->db->where('ri.status',$post['status']);
            $query = $this->db->or_where('ri.idstatusbed',4);
            $query = $this->db->group_end();
//            $query = ((empty($post['idbangsal']))? '' :  $this->db->where('rb.idbangsal',$post['idbangsal']) );
        }
        else
        {          
            $startDate  = $post['tanggal_mulai'];   
            $endDate    = $post['tanggal_akhir'];
              
            $query = $this->db->where('ri.status',$post['status']);
            if($post['status']=='selesai'){
                // $query = $this->db->where('date(ri.waktukeluar) ',$post['tanggal']);
                // $this->db->where('DATE(ri.waktukeluar) >=',$startDate);
                // $this->db->where('DATE(ri.waktukeluar) <=',$endDate);
                $query = $this->db->where("if( DATE(ri.waktukeluar) = '0000-00-00',DATE(ri.waktumasuk),DATE(ri.waktukeluar) ) >=",$startDate);
                $query = $this->db->where("if( DATE(ri.waktukeluar) = '0000-00-00',DATE(ri.waktumasuk),DATE(ri.waktukeluar) ) <=",$endDate);
            }else if($post['status']=='batal' || $post['status']=='menunggu'){
                // $query = $this->db->where('date(ri.waktumasuk) ',$post['tanggal']);
                $this->db->where('DATE(ri.waktumasuk) >=',$startDate);
                $this->db->where('DATE(ri.waktumasuk) <=',$endDate);
            }
        }
        
        $query = $this->db->group_by('ri.idinap');
        $query = $this->db->get('rs_inap ri');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_administrasiranap_($limit=false)
    {
        $column = ['ri.idinap','ppas.norm','pp.namalengkap','ppd.nosep','ri.waktumasuk','pperppai.namalengkap','rbl.namabangsal','ri.status','ri.status','ri.status'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_administrasiranap();   
    }
    public function total_dt_administrasiranap(){ return $this->recordsTotal($this->dt_administrasiranap());}
    public function filter_dt_administrasiranap(){return $this->recordsFiltered($this->dt_administrasiranap_());}
    
    //list data klaim veririkasi pasien 
    public function dt_verifikasiklaim()
    {
        $post = $this->input->post();
        $tglawal  = $post['tglawal'];
        $tglakhir = $post['tglakhir'];
        
        $query = $this->db->select("ppd.norm, pper.identitas, pper.namalengkap, pper.nik, pper.notelpon, ppd.waktu as waktudaftar, rp.waktuperiksadokter as waktuperiksa, date(rp.waktu) as tglperiksa, ppd.caradaftar, ru.namaunit as poli, ppd.jenispemeriksaan,  ppd.carabayar, ppd.idstatusklaim, ppd.idpendaftaran, ppd.resumeprint, ifnull(ri.idinap,'0') as idinap, ppd.nosep, ppd.norujukan, ppd.nokontrol ");
        $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
        $query = $this->db->join('rs_unit ru','ru.idunit = ppd.idunit');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
        $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
        $query = $this->db->join('rs_inap ri','ri.idpendaftaran = ppd.idpendaftaran','left');
        
        $query = $this->db->where('ppd.tahunperiksa BETWEEN "'.ql_getyear($tglawal).'" and "'.ql_getyear($tglakhir).'"');
        $query = $this->db->where('ppd.bulanperiksa BETWEEN "'.ql_getmonth_withoutzero($tglawal).'" and "'.ql_getmonth_withoutzero($tglakhir).'"');
        $query = $this->db->where('ppd.carabayar != "mandiri" ');
        $query = $this->db->where('date(ppd.waktuperiksa) BETWEEN "'.$tglawal.'" and "'.$tglakhir.'"');
        
        $query = $this->db->group_by('ppd.idpendaftaran');
        $query = $this->db->get('person_pendaftaran ppd');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_verifikasiklaim_($limit=false)
    {
        $column = ['ppd.idpendaftaran','ppd.norm','pper.namalengkap', 'pper.nik', 'pper.notelpon','ppd.waktu','rp.waktuperiksadokter','ppd.caradaftar','ru.namaunit','ppd.jenispemeriksaan','ppd.carabayar','ppd.nosep','ppd.norujukan', 'rp.diagnosa', 'ppd.nokontrol'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_verifikasiklaim();   
    }
    public function totaldt_verifikasiklaim(){ return $this->recordsTotal($this->dt_verifikasiklaim());}
    public function filterdt_verifikasiklaim(){return $this->recordsFiltered($this->dt_verifikasiklaim_());}
    
//    log Pemeriksaan
    public function dt_logpemeriksaan()
    {
        $post = $this->input->post();
        $query = $this->db->select("lh.waktuinput,pp.norm, lh.url, lh.deskripsi, lh.icd, rb.kode, rb.namabarang, lh.idpendaftaran, lu.namauser");
        $query = $this->db->join('person_pendaftaran pp','pp.idpendaftaran=lh.idpendaftaran');
        $query = $this->db->join('login_user lu','lu.iduser=lh.iduser');
        $query = $this->db->join('rs_barang rb','rb.idbarang=lh.idbarang','left');
        ((empty($post['tanggal']))? '' : $query = $this->db->where('date(lh.waktuinput)',$post['tanggal']) );
        $query = $this->db->get('log_hasilpemeriksaan lh');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_logpemeriksaan_($limit=false)
    {
        $column = ['pp.norm','pp.norm','lh.waktuinput','lh.url','lh.deskripsi','lu.namauser'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_logpemeriksaan();   
    }
    public function total_dt_logpemeriksaan(){ return $this->recordsTotal($this->dt_logpemeriksaan());}
    public function filter_dt_logpemeriksaan(){return $this->recordsFiltered($this->dt_logpemeriksaan_());}
    
    /**
     * master tarif
     * @return boolean
     */
    public function dt_mastertarif()
    {
        $post = $this->input->post();
        $query = $this->db->select("rm.idmastertarif, rm.icd, ri.namaicd, rk.kelas, kt.kode, kt.nama, ka.kode as kodeakun, ka.nama as namaakun, rj.jenistarif, rm.jasaoperator, rm.nakes, rm.jasars, rm.bhp, rm.akomodasi, rm.margin, rm.sewa, rm.total");
        $query = $this->db->join('rs_icd ri', 'ri.icd = rm.icd','left')
                ->join('rs_jenistarif rj', 'rj.idjenistarif = rm.idjenistarif','left')
                ->join('rs_kelas rk', 'rk.idkelas = rm.idkelas','left')
                ->join('keu_tipeakun kt', 'kt.kode = rm.tipecoapendapatan','left')
                ->join('keu_akun ka', 'ka.kode = rm.coapendapatan','left');
        if($this->input->post('mode') == 'ralan')
        {
            $query = $this->db->where('rm.idkelas',1);
        }
        else
        {
            $query = $this->db->where('rm.idkelas !=',1);
        }
        $query = $this->db->get('rs_mastertarif rm');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_mastertarif_($limit=false)
    {
        $column = ['rm.icd', 'ri.namaicd', 'rk.kelas', 'rj.jenistarif','kt.nama','ka.nama', 'rm.jasaoperator', 'rm.nakes', 'rm.jasars', 'rm.bhp', 'rm.akomodasi', 'rm.margin', 'rm.sewa', 'rm.total',false];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_mastertarif();   
    }
    public function total_dt_mastertarif(){ return $this->recordsTotal($this->dt_mastertarif());}
    public function filter_dt_mastertarif(){return $this->recordsFiltered($this->dt_mastertarif_());}
    
    /**
     * konfigurasi
     * @return boolean
     */
    public function dt_konfigurasi()
    {
        $query = $this->db->get('konfigurasi k');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_konfigurasi_($limit=false)
    {
        $column = ['k.nama','k.nama', 'k.nilai', 'k.keterangan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_konfigurasi();   
    }
    public function total_dt_konfigurasi(){ return $this->recordsTotal($this->dt_konfigurasi());}
    public function filter_dt_konfigurasi(){return $this->recordsFiltered($this->dt_konfigurasi_());}
    
    /**Master Tarif Operasi**/
    public function dt_mastertarifoperasi()
    {
        $query = $this->db->select("rmo.idmastertarifoperasi, rjo.jenistarifoperasi, rj.jenistarif, rmo.jenistindakan, rmo.jasamedis, rmo.jasars, rmo.total");        
        $query = $this->db->join('rs_jenistarif_operasi rjo','rjo.idjenistarifoperasi=rmo.idjenistarifoperasi');
        $query = $this->db->join('rs_jenistarif rj','rj.idjenistarif=rmo.idjenistarif');
        $query = $this->db->get('rs_mastertarif_operasi rmo');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_mastertarifoperasi_($limit=false)
    {
        $column = ['rjo.jenistarifoperasi','rj.jenistarif', 'rmo.jenistindakan', 'rmo.jasamedis', 'rmo.jasars', 'rmo.total'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_mastertarifoperasi();   
    }
    public function total_dt_mastertarifoperasi(){ return $this->recordsTotal($this->dt_mastertarifoperasi());}
    public function filter_dt_mastertarifoperasi(){return $this->recordsFiltered($this->dt_mastertarifoperasi_());}
    
    /**Master Diskon**/
    public function dt_masterdiskon()
    {
        $query = $this->db->select("rmds.*, k.kelas, rj.jenistarif, ri.namaicd, concat(if(rmm.member=null,'', concat('<b>',rmm.member,'</b> ') ), if(rmm.keterangan=null,'', concat('[' ,rmm.keterangan,']') ) ) as member");
        $query = $this->db->join('rs_icd ri','ri.icd=rmds.icd','left');
        $query = $this->db->join('rs_kelas k','k.idkelas=rmds.idkelas','left');
        $query = $this->db->join('rs_jenistarif rj','rj.idjenistarif=rmds.idjenistarif','left');
        $query = $this->db->join('rs_mastermember rmm','rmm.idmember=rmds.idmember','left');
        $query = $this->db->get('rs_masterdiskon rmds');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_masterdiskon_($limit=false)
    {
        $column = ['rmds.icd','ri.namaicd', 'rmm.member', 'rj.jenistarif','rmds.tanggalmulai','rmds.tanggalselesai','rmds.jenisdiskon','rmds.jenisinput','rmds.nominal'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_masterdiskon();   
    }
    public function total_dt_masterdiskon(){ return $this->recordsTotal($this->dt_masterdiskon());}
    public function filter_dt_masterdiskon(){return $this->recordsFiltered($this->dt_masterdiskon_());}
    
    /**Master Diskon Pegawai**/
    public function dt_masterdiskonpegawai()
    {
        $query = $this->db->select("rm.*, k.kelas, rj.jenistarif");
        $query = $this->db->join('rs_kelas k','k.idkelas=rm.idkelas','left');
        $query = $this->db->join('rs_jenistarif rj','rj.idjenistarif=rm.idjenistarif');
        $query = $this->db->get('rs_masterdiskonpegawai rm');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_masterdiskonpegawai_($limit=false)
    {
        $column = ['rm.iddiskonpegawai','rm.namadiskon','k.kelas','rj.jenistarif', 'rm.potongan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_masterdiskonpegawai();   
    }
    public function total_dt_masterdiskonpegawai(){return $this->recordsTotal($this->dt_masterdiskonpegawai());}
    public function filter_dt_masterdiskonpegawai(){return $this->recordsFiltered($this->dt_masterdiskonpegawai_());}
    
    
    /**Master Member**/
    public function dt_mastermember()
    {   
        $query = $this->db->get_where('rs_mastermember rmm',['isdelete'=>0]);
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_mastermember_($limit=false)
    {
        $column = ['rmm.idmember','rmm.member','rmm.keterangan', 'rmm.hariberlaku'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_mastermember();
    }
    public function total_dt_mastermember(){ return $this->recordsTotal($this->dt_mastermember());}
    public function filter_dt_mastermember(){return $this->recordsFiltered($this->dt_mastermember_());}
    
    /**Blacklist Pasien**/
    public function dt_blacklistpasien()
    {   
        $query = $this->db->select('ppb.*, pper.identitas,pper.namalengkap, ru.namaunit, ppb.alasan');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppb.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->join('rs_unit ru','ru.idunit = ppb.idunit');
        $query = $this->db->get_where('person_pasien_blacklistunit ppb');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_blacklistpasien_($limit=false)
    {
        $column = ['ppb.norm','pper.namalengkap','ru.namaunit',false];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_blacklistpasien();
    }
    public function total_dt_blacklistpasien(){ return $this->recordsTotal($this->dt_blacklistpasien());}
    public function filter_dt_blacklistpasien(){return $this->recordsFiltered($this->dt_blacklistpasien_());}
    
    /**Person Member - (member pasien)**/
    public function dt_personmember()
    {   
        $query = $this->db->select('ppm.norm,pper.namalengkap, pper.tanggallahir, pper.alamat');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppm.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->group_by('ppm.norm');
        $query = $this->db->get_where('person_pasien_member ppm',['isdelete'=>0]);
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_personmember_($limit=false)
    {
        $column = ['ppm.norm','ppm.norm','pper.namalengkap','pper.tanggallahir'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_personmember();
    }
    public function total_dt_personmember(){ return $this->recordsTotal($this->dt_personmember());}
    public function filter_dt_personmember(){return $this->recordsFiltered($this->dt_personmember_());}
    
    /**Person Member Detail - (detail member pasien)**/
    public function dt_personmemberdetail()
    {   
        $query = $this->db->select('ppm.*, pper.namalengkap, pper.tanggallahir, rmm.member, rmm.keterangan, rmm.hariberlaku');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppm.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->join('rs_mastermember rmm','rmm.idmember = ppm.idmember','left');
        $query = $this->db->get('person_pasien_member ppm');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_personmemberdetail_($limit=false)
    {
        $column = ['ppm.norm','pper.namalengkap','pper.tanggallahir'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_personmemberdetail();
    }
    public function total_dt_personmemberdetail(){ return $this->recordsTotal($this->dt_personmemberdetail());}
    public function filter_dt_personmemberdetail(){return $this->recordsFiltered($this->dt_personmemberdetail_());}
    
    /**HUTANG FARMASI**/
    public function dt_hutangfarmasi()
    {
        $post = $this->input->post();
        $query = $this->db->select(" rbf.idbarangdistributor, rbd.distributor, sum(rbf.tagihan) as tagihan, sum(rbf.potongan) as potongan, sum(rbf.totaltagihan) as totaltagihan, sum(rbf.dibayar) as dibayar, sum(rbf.ppn) as ppn, sum(rbf.kekurangan) as kekurangan");
        $query = $this->db->join('rs_barang_distributor rbd','rbd.idbarangdistributor=rbf.idbarangdistributor','left');
        
        if($post['lunas']){
            $query = $this->db->where('rbf.tanggalpelunasan between "'.$post['tanggal1'].'" and "'.$post['tanggal2'].'" ');
            $query = $this->db->where('rbf.kekurangan',0);
        }else{
            $query = $this->db->where('rbf.tanggalfaktur between "'.$post['tanggal1'].'" and "'.$post['tanggal2'].'" ');
        }
        $query = $this->db->where('rbf.idunit',24);
        $query = $this->db->group_by('rbf.idbarangdistributor');
        $query = $this->db->get('rs_barang_faktur rbf');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_hutangfarmasi_($limit=false)
    {
        $column = ['rbf.idbarangfaktur','rbd.distributor'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit(15,$_POST['start']);}
        return $this->dt_hutangfarmasi();   
    }
    public function total_dt_hutangfarmasi(){ return $this->recordsTotal($this->dt_hutangfarmasi());}
    public function filter_dt_hutangfarmasi(){return $this->recordsFiltered($this->dt_hutangfarmasi_());}
    
    
    /**HUTANG NON FARMASI (hutang usaha)**/
    public function dt_hutangusaha()
    {
        $post = $this->input->post();
        $query = $this->db->select("rf.idfaktur, rf.nofaktur,rf.tanggalfaktur,rf.tanggalpelunasan,rf.statusbayar,rf.tanggaljatuhtempo, rf.idbarangdistributor, rbd.distributor, sum(rf.tagihan) as tagihan, sum(rf.potongan) as potongan, sum(rf.totaltagihan) as totaltagihan, sum(rf.dibayar) as dibayar, sum(rf.ppn) as ppn, sum(rf.kekurangan) as kekurangan, date_format(rf.waktumulailayanan,'%Y-%m-%d') as waktumulailayanan, date_format(rf.waktuberakhirlayanan,'%Y-%m-%d') as waktuberakhirlayanan");
        $query = $this->db->join('rs_barang_distributor rbd','rbd.idbarangdistributor=rf.idbarangdistributor','');
        if($post['lunas'])
        {
            $query = $this->db->where('rf.tanggalpelunasan between "'.$post['tanggal1'].'" and "'.$post['tanggal2'].'" ');
            $query = $this->db->where('rf.statusbayar','lunas');
        }
        else
        {
            $query = $this->db->where('rf.tanggalfaktur between "'.$post['tanggal1'].'" and "'.$post['tanggal2'].'" ');
        }
        
        if(isset($post['iddistributor']))
        {
            $query = $this->db->where('rf.idbarangdistributor',$post['iddistributor']);
            $query = $this->db->group_by('rf.idfaktur');
        }
        else
        {
            $query = $this->db->group_by('rf.idbarangdistributor');
        }

        
        $query = $this->db->get('rs_fakturnonfarmasi rf');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_hutangusaha_($limit=false)
    {
        $column = isset($_POST['iddistributor']) ?  ['rf.idfaktur','rf.nofaktur','rf.tanggalfaktur','rf.tanggaljatuhtempo','rf.tanggalpelunasan','rf.tagihan','rf.ppn','rf.potongan','rf.totaltagihan','rf.dibayar','rf.kekurangan'] : ['rf.idfaktur','rbd.distributor'];
        
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_hutangusaha();   
    }
    public function total_dt_hutangusaha(){ return $this->recordsTotal($this->dt_hutangusaha());}
    public function filter_dt_hutangusaha(){return $this->recordsFiltered($this->dt_hutangusaha_());}
    
    /**Paket Pemeriksaan Rule**/
    public function dt_paketpemeriksaanrule()
    {
        $post = $this->input->post();
        $query = $this->db->get('rs_paket_pemeriksaan_rule');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_paketpemeriksaanrule_($limit=false)
    {
        $column = ['idpaketpemeriksaanrule','namarule'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_paketpemeriksaanrule();
    }
    public function total_dt_paketpemeriksaanrule(){ return $this->recordsTotal($this->dt_paketpemeriksaanrule());}
    public function filter_dt_paketpemeriksaanrule(){return $this->recordsFiltered($this->dt_paketpemeriksaanrule_());}
    
    
    /**Paket Pemeriksaan Rule Pesan**/
    public function dt_paketpemeriksaanrulepesan()
    {
        $post  = $this->input->post();
        $query = $this->db->join('rs_paket_pemeriksaan_rule rppr','rppr.idpaketpemeriksaanrule=rpprp.idpaketpemeriksaanrule','left');
        $query = $this->db->order_by('rpprp.idpaketpemeriksaanrule','asc');
        $query = $this->db->order_by('rpprp.skor','asc');
        $query = $this->db->get('rs_paket_pemeriksaan_rule_pesan rpprp');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_paketpemeriksaanrulepesan_($limit=false)
    {
        $column = ['namarule','skor'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_paketpemeriksaanrulepesan();
    }
    public function total_dt_paketpemeriksaanrulepesan(){ return $this->recordsTotal($this->dt_paketpemeriksaanrulepesan());}
    public function filter_dt_paketpemeriksaanrulepesan(){return $this->recordsFiltered($this->dt_paketpemeriksaanrulepesan_());}
    
    
    /**Jenis Cetak Paket Pemeriksaan**/
    public function dt_jeniscetakpaketpemeriksaan()
    {
        $query = $this->db->get('rs_jeniscetaklaboratorium');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_jeniscetakpaketpemeriksaan_($limit=false)
    {
        $column = ['idjeniscetak','kode','jenis'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_jeniscetakpaketpemeriksaan();
    }
    
    public function total_dt_jeniscetakpaketpemeriksaan(){ return $this->recordsTotal($this->dt_jeniscetakpaketpemeriksaan());}
    public function filter_dt_jeniscetakpaketpemeriksaan(){return $this->recordsFiltered($this->dt_jeniscetakpaketpemeriksaan_());}
    
    
    /**Master Tarif Paket Pemeriksaan**/
    public function dt_mastertarifpaketpemeriksaan()
    {
        $post  = $this->input->post();
        $query = $this->db->select("a.idpaketpemeriksaan as idpaket, a.namapaketpemeriksaan, b.*, c.jenisicd, d.jenistarif, e.kelas, d.jenistarif , ka.kode, ka.nama")
                ->join("rs_mastertarif_paket_pemeriksaan b", "b.idpaketpemeriksaan=a.idpaketpemeriksaan")
                ->join("rs_jenisicd c", "c.idjenisicd = a.idjenisicd")
                ->join("rs_jenistarif d", "d.idjenistarif=b.idjenistarif")
                ->join("rs_kelas e", "e.idkelas=b.idkelas")
                ->join("keu_akun ka", "ka.kode= b.coapendapatan","left")
                ->where('a.idpaketpemeriksaanparent',0)
                ->where('a.isdelete',null)
                ->order_by('a.namapaketpemeriksaan','asc')
                ->get("rs_paket_pemeriksaan a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_mastertarifpaketpemeriksaan_($limit=false)
    {
        $column = ['a.namapaketpemeriksaan','c.jenisicd','e.kelas','d.jenistarif','b.coapendapatan','b.jasaoperator','b.nakes','b.jasars','b.bhp','b.akomodasi','b.margin','b.sewa','b.total'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_mastertarifpaketpemeriksaan();
    }
    public function total_dt_mastertarifpaketpemeriksaan(){ return $this->recordsTotal($this->dt_mastertarifpaketpemeriksaan());}
    public function filter_dt_mastertarifpaketpemeriksaan(){return $this->recordsFiltered($this->dt_mastertarifpaketpemeriksaan_());}
    
    /**Paket Pemeriksaan**/
    public function dt_paketpemeriksaan()
    {
        $post  = $this->input->post();
        $query = $this->db->select("a.idpaketpemeriksaan, a.namapaketpemeriksaan, a.namapaketpemeriksaancetak, d.namarule, e.jenis, c.jenisicd")
                ->join("rs_jenisicd c", "c.idjenisicd = a.idjenisicd")
                ->join("rs_paket_pemeriksaan_rule d", "d.idpaketpemeriksaanrule = a.idpaketpemeriksaanrule","left")
                ->join("rs_jeniscetaklaboratorium e", "e.idjeniscetak = a.idjeniscetak","left")
                ->where('a.idpaketpemeriksaanparent',0)
                ->where('a.isdelete',null)
                ->order_by('a.namapaketpemeriksaan','asc')
                ->get("rs_paket_pemeriksaan a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_paketpemeriksaan_($limit=false)
    {
        $column = ['a.namapaketpemeriksaan','a.namapaketpemeriksaancetak','c.jenisicd'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_paketpemeriksaan();
    }
    public function total_dt_paketpemeriksaan(){return $this->recordsTotal($this->dt_paketpemeriksaan());}
    public function filter_dt_paketpemeriksaan(){return $this->recordsFiltered($this->dt_paketpemeriksaan_());}
    
    
    /**Pemeriksaan Ranap**/
    public function dt_pemeriksaanranap()
    {
        $post  = $this->input->post();
        $query = $this->db->select("idrencanamedispemeriksaan, ri.idpendaftaran, "
                . "namadokter(ri.idpegawaidokter) as namadokter, "
                . "(select namapasien(b.idperson) from person_pendaftaran a, person_pasien b where a.idpendaftaran=ri.idpendaftaran and b.norm=a.norm) as namalengkap, "
                . "ri.idinap, (select norm from person_pendaftaran where idpendaftaran=ri.idpendaftaran) as norm, "
                . "(select nosep from person_pendaftaran where idpendaftaran=ri.idpendaftaran) as nosep, rip.waktu, namabangsal, rip.status, ri.idkelas, ri.idkelasjaminan,"
                . "concat('Rencana Perawatan : ',ifnull((select COUNT(1) FROM rs_inap_rencana_medis_hasilpemeriksaan a WHERE a.statuspelaksanaan='rencana' and a.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan),0),'<br>Perawatan Terlaksana : ',ifnull((select COUNT(1) FROM rs_inap_rencana_medis_hasilpemeriksaan a WHERE a.statuspelaksanaan='terlaksana' and a.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan),0),'<br>Rencana Obat/BHP : ',ifnull((select COUNT(1) FROM rs_inap_rencana_medis_barangpemeriksaan a WHERE a.statuspelaksanaan='rencana' and a.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan),0),'<br>Obat/BHP Terlaksana : ',ifnull((select COUNT(1) FROM rs_inap_rencana_medis_barangpemeriksaan a WHERE a.statuspelaksanaan='terlaksana' and a.idrencanamedispemeriksaan=rip.idrencanamedispemeriksaan),0)) as catatan,"
                . " (SELECT group_concat(a.icd, b.namaicd, a.jumlah , b.satuan SEPARATOR '<br>') tindakan
                    FROM rs_inap_rencana_medis_hasilpemeriksaan a 
                    join rs_icd b on b.icd = a.icd 
                    WHERE a.idrencanamedispemeriksaan = rip.idrencanamedispemeriksaan) as diagnosa,"
                ."(select group_concat( cc.namabarang,' ' ,aa.jumlah, ' ' , dd.namasatuan SEPARATOR '<br>') as tindakan
                    from rs_inap_rencana_medis_barangpemeriksaan aa
                    join rs_barang cc on cc.idbarang = aa.idbarang
                    join rs_satuan dd on dd.idsatuan = cc.idsatuan
                    WHERE aa.idrencanamedispemeriksaan = rip.idrencanamedispemeriksaan) as tindakan")
                ->join("rs_inap_rencana_medis_pemeriksaan rip","ri.idinap=rip.idinap",'left')
                ->join("rs_bed rb","ri.idbed=rb.idbed",'left')
                ->join("rs_bangsal rbl", "rbl.idbangsal=rb.idbangsal","left");
        
        if(!empty($post['idinap']))
        { 
            $query = $this->db->where('ri.idinap',$post['idinap']);
        }
        if( !empty($post['tanggalawal']) && !empty($post['tanggalakhir']))
        { 
            $tahunbulan1 = tahunbulan($post['tanggalawal']);
            $tahunbulan2 = tahunbulan($post['tanggalakhir']);
            
            $query = $this->db->where(" rip.tahunbulan BETWEEN '".$tahunbulan1."' and '".$tahunbulan2."' " );
            $query = $this->db->where(" date(rip.waktu) BETWEEN '".$post['tanggalawal'] ."' and '".$post['tanggalakhir']."' ");
        }
        
        if(!empty($post['idbangsal'])){ $query = $this->db->where('rb.idbangsal',$post['idbangsal']);}
        $query = $this->db->order_by('ri.waktumasuk','desc')
                ->order_by('rip.waktu','desc')
                ->get("rs_inap ri");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_pemeriksaanranap_($limit=false)
    {
        $column = ['(select namapasien(b.idperson) from person_pendaftaran a, person_pasien b where a.idpendaftaran=ri.idpendaftaran and b.norm=a.norm)','rip.idrencanamedispemeriksaan','rip.waktu','rip.waktu','rip.waktu','rip.waktu','ri.idpegawaidokter','rbl.namabangsal','rip.status'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pemeriksaanranap();
    }
    public function total_dt_pemeriksaanranap(){return $this->recordsTotal($this->dt_pemeriksaanranap());}
    public function filter_dt_pemeriksaanranap(){return $this->recordsFiltered($this->dt_pemeriksaanranap_());}
    
    /**Surat Kelahiran**/
    public function dt_suratkelahiran()
    {
//        $post  = $this->input->post();
//        $filter= explode('-', $post['bulan']);
//        $bulan = $filter[1];
//        $tahun= $filter[0];
        $query = $this->db->select("rs.kodesurat,pper.tanggallahir, pper.jeniskelamin,rs.panjangbayi, rs.beratbadan, rs.norm, namapasien(ppas.idperson) as namapasien, rs.anakke,  rs.panjangbayi, rs.beratbadan, rs.catatan, (SELECT namapasien(ay.idperson) FROM person_pasien_penanggungjawab ay WHERE norm =rs.norm and idhubungan=1) as ayah, (SELECT namapasien(ib.idperson) FROM person_pasien_penanggungjawab ib WHERE norm = rs.norm and idhubungan=2) as ibu");
        $query = $this->db->join("person_pasien ppas","ppas.norm=rs.norm");
        $query = $this->db->join("person_person pper","pper.idperson=ppas.idperson");
//        $query = $this->db->where("month(pper.tanggallahir)",$bulan);
//        $query = $this->db->where("year(pper.tanggallahir)",$tahun);
        $query = $this->db->get("rs_suratkelahiran rs");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_suratkelahiran_($limit=false)
    {
        $column = ['rs.kodesurat','rs.kodesurat', 'rs.norm', 'pper.namalengkap','pper.tanggallahir','pper.jeniskelamin','rs.panjangbayi','rs.beratbadan', 'rs.anakke',  'rs.catatan'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_suratkelahiran();
    }
    public function total_dt_suratkelahiran(){return $this->recordsTotal($this->dt_suratkelahiran());}
    public function filter_dt_suratkelahiran(){return $this->recordsFiltered($this->dt_suratkelahiran_());}
    
    /**Data ICD**/
    public function dt_icd()
    {
        $post  = $this->input->post();
        $query = $this->db->select("ri.icd, ri.namaicd, ri.aliasicd, ri.statuskronis, ri.nilaiacuanrendah, ri.nilaiacuantinggi, ri.satuan, ri.golonganicd, rj.jenispenyakit, rg.nodtd, rg.golongansebabpenyakit, ri.idicd");
        $query = $this->db->join("rs_jenispenyakit rj","rj.idjenispenyakit=ri.idjenispenyakit","left");
        $query = $this->db->join("rs_icd_golongansebabpenyakit rg","rg.idgolongansebabpenyakit=ri.idgolongansebabpenyakit","left");
        
        $query = $this->db->get("rs_icd ri");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_icd_($limit=false)
    {
        $column = ['ri.icd','ri.namaicd','ri.aliasicd','ri.aliasicd','ri.statuskronis','ri.golonganicd','rj.jenispenyakit','rg.golongansebabpenyakit'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_icd();
    }
    public function total_dt_icd(){return $this->recordsTotal($this->dt_icd());}
    public function filter_dt_icd(){return $this->recordsFiltered($this->dt_icd_());}
    
    /**
     * data induk
     */
    
    public function dt_datainduk()
    {
        $post  = $this->input->post();
        $query = $this->db->select("pp.nik,ppas.norm, namapasien(pp.idperson) as namalengkap,pp.tanggallahir,pp.jeniskelamin,pp.notelpon,pp.alamat, pp.idperson, pp.cetakkartu");
        $query = $this->db->join("person_pasien ppas","ppas.idperson=pp.idperson");        
        $query = $this->db->where(['pp.status_person'=>'A','ppas.status_pasien'=>'A']);        
        $query = $this->db->get("person_person pp");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_datainduk_($limit=false)
    {
        $column = ['pp.nik','ppas.norm','pp.namalengkap','pp.tanggallahir','pp.jeniskelamin'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit(25,$_POST['start']);}
        return $this->dt_datainduk();
    }
    public function total_dt_datainduk(){return $this->recordsTotal($this->dt_datainduk());}
    public function filter_dt_datainduk(){return $this->recordsFiltered($this->dt_datainduk_(true));}
    
    
    public function dt_statuspemeriksaanpasienralan()
    {
        $post  = $this->input->post();
        $query = $this->db->select("ppd.norm, namapasien(ppas.idperson) as namapasien,(select GROUP_CONCAT(ru.namaunit, ' : ' , rp.status,'<br>') from rs_pemeriksaan rp, rs_unit ru WHERE rp.idpendaftaran=ppd.idpendaftaran and ru.idunit=rp.idunit) as detailperiksa, ppd.nosep, ppd.waktu, ppd.waktuperiksa, ppd.carabayar, namaunit(ppd.idunit) as namaunit, fstatuspendaftaran(ppd.idstatuskeluar) as status, ppd.jenispemeriksaan");
        $query = $this->db->join("person_pasien ppas","ppas.norm=ppd.norm"); 
        $query = $this->db->where("ppd.idstatuskeluar",$post['idstatuskeluar']);
        if(empty(!$post['idunit']))
        {
            $query = $this->db->where("ppd.idunit",$post['idunit']);
        }
        $query = $this->db->where("ppd.carabayar",$post['carabayar']);
        $query = $this->db->where("date(ppd.waktuperiksa) between '".$post['tanggal1']."' and '".$post['tanggal2']."'");        
        $query = $this->db->get("person_pendaftaran ppd");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_statuspemeriksaanpasienralan_($limit=false)
    {
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_statuspemeriksaanpasienralan();
    }
    public function total_dt_statuspemeriksaanpasienralan(){return $this->recordsTotal($this->dt_statuspemeriksaanpasienralan());}
    public function filter_dt_statuspemeriksaanpasienralan(){return $this->recordsFiltered($this->dt_statuspemeriksaanpasienralan_(true));}
    
    //list sewa alat operasi
    public function dt_operasisewaalat()
    {
        $query = $this->db->select("rs.*, ifnull(ros.idoperasisewaalat,0) as idoperasisewaalat, rs.idsewaalat, if(ros.idsewaalat is null,'-','sewa') as status ,ifnull(ros.jumlah,0) as jumlah");
        $query = $this->db->join("rs_operasi_sewaalat ros","ros.idsewaalat=rs.idsewaalat and ros.idoperasi='".$this->input->post("idoperasi")."'","left");  
        $query = $this->db->get("rs_sewaalat rs");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_operasisewaalat_($limit=false)
    {
        $column = ['rs.namaalat','rs.alias','ros.jumlah','rs.harga','ros.subtotal','ros.idsewaalat'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_operasisewaalat();
    }
    public function total_dt_operasisewaalat(){return $this->recordsTotal($this->dt_operasisewaalat());}
    public function filter_dt_operasisewaalat(){return $this->recordsFiltered($this->dt_operasisewaalat_(true));}
    
    //list diagnosa operasi pra
    public function dt_operasidiagnosa()
    {
        $query = $this->db->select("ri.namaicd, ri.aliasicd, rod.icd, rod.level, rod.iddiagnosaoperasi, rod.jenisdiagnosa");
        $query = $this->db->order_by("rod.level","asc");  
        $query = $this->db->join("rs_icd ri","ri.icd=rod.icd");  
        $query = $this->db->where("rod.jenisdiagnosa","pra");
        $query = $this->db->where("rod.idoperasi",$this->input->post("idoperasi"));
        $query = $this->db->get("rs_operasi_diagnosa rod");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_operasidiagnosa_($limit=false)
    {
        $column = ['rod.icd','ri.namaicd','ri.aliasicd','rod.level'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_operasidiagnosa();
    }
    public function total_dt_operasidiagnosa(){return $this->recordsTotal($this->dt_operasidiagnosa());}
    public function filter_dt_operasidiagnosa(){return $this->recordsFiltered($this->dt_operasidiagnosa_(true));}
    
    //list diagnosa operasi post
    public function dt_postoperasidiagnosa()
    {
        $query = $this->db->select("ri.namaicd, ri.aliasicd, rod.icd, rod.level, rod.iddiagnosaoperasi, rod.jenisdiagnosa");
        $query = $this->db->order_by("rod.level","asc");  
        $query = $this->db->join("rs_icd ri","ri.icd=rod.icd");  
        $query = $this->db->where("rod.jenisdiagnosa","post");  
        $query = $this->db->where("rod.idoperasi",$this->input->post("idoperasi"));
        $query = $this->db->get("rs_operasi_diagnosa rod");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_postoperasidiagnosa_($limit=false)
    {
        $column = ['rod.icd','ri.namaicd','ri.aliasicd','rod.level'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_postoperasidiagnosa();
    }
    public function total_dt_postoperasidiagnosa(){return $this->recordsTotal($this->dt_postoperasidiagnosa());}
    public function filter_dt_postoperasidiagnosa(){return $this->recordsFiltered($this->dt_postoperasidiagnosa_(true));}
    
    //list tindakan operasi
    public function dt_operasitindakan()
    {
        $query = $this->db->select("ri.namaicd, rot.icd, rot.idtindakanoperasi, rot.jumlah");
        $query = $this->db->join("rs_icd ri","ri.icd=rot.icd");  
        $query = $this->db->where("rot.idoperasi",$this->input->post("idoperasi"));
        $query = $this->db->get("rs_operasi_tindakan rot");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_operasitindakan_($limit=false)
    {
        $column = ['rot.icd','ri.namaicd','rot.jumlah'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_operasitindakan();
    }
    public function total_dt_operasitindakan(){return $this->recordsTotal($this->dt_operasitindakan());}
    public function filter_dt_operasitindakan(){return $this->recordsFiltered($this->dt_operasitindakan_(true));}
    
    //list barang pemeriksaan
    public function dt_operasibarangpemeriksaan()
    {
        $query = $this->db->select("rob.idbarang, rob.idbarangoperasi,rb.kode, rb.namabarang, rob.jumlahpemakaian, rob.harga, rob.total, rs.namasatuan, rob.selesai");
        $query = $this->db->join("rs_barang rb","rb.idbarang=rob.idbarang");  
        $query = $this->db->join("rs_satuan rs","rs.idsatuan=rb.idsatuan");  
        $query = $this->db->where("rob.idoperasi",$this->input->post("idoperasi"));
        $query = $this->db->where("rb.isobatoperasi",0);
        $query = $this->db->get("rs_operasi_barangpemeriksaan rob");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_operasibarangpemeriksaan_($limit=false)
    {
        $column = ['rob.idbarangoperasi','rb.namabarang','rob.jumlahpemakaian','rob.harga','rs.namasatuan','rob.total'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
//        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_operasibarangpemeriksaan();
    }
    public function total_dt_operasibarangpemeriksaan(){return $this->recordsTotal($this->dt_operasibarangpemeriksaan());}
    public function filter_dt_operasibarangpemeriksaan(){return $this->recordsFiltered($this->dt_operasibarangpemeriksaan_(true));}
    
    //report waktu layanan
    //mahmud, clear
    public function dt_waktulayanan()
    {
        $query = $this->db->select("wl.idpendaftaran, wl.norm, namapasien(ppas.idperson) as namapasien, wl.tanggalperiksa, ru.namaunit, (select group_concat('<li>',x.unitlayanan,' (',x.kegiatan,')  Tgl layanan : ',x.tanggallayanan,' ',x.waktumulai,' - ',x.waktuselesai,' => <b>',x.waktulayanan,'</b> by ',xx.namauser,' </li>') from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran=wl.idpendaftaran order by x.tanggallayanan , x.waktu asc) as detail");
        $query = $this->db->join('person_pasien ppas','ppas.norm=wl.norm');
        $query = $this->db->join('person_person pper','pper.idperson=ppas.idperson');
        $query = $this->db->join('rs_unit ru','ru.idunit=wl.idunit','left');        
        if($this->input->post('mode') == 'view')
        {
            $query = $this->db->where('wl.tanggalperiksa',$this->input->post('tanggal'));
        }
        else
        {
            //jika mode unduh
            $tahunbulan = date('Y-m',strtotime($this->input->post('tahunbulan')));
            $tanggal1   = $tahunbulan.'-01';
            $tanggal2   = $tahunbulan.'-31';
            $query = $this->db->where('wl.tanggalperiksa BETWEEN "'.$tanggal1.'" AND "'.$tanggal2.'" ');
        }
        
        
        if(empty(!$this->input->post('idunit')))
        {
            $query = $this->db->where('wl.idunit',$this->input->post('idunit'));
        }
        
        $query = $this->db->group_by('wl.idpendaftaran');
        
        //jika mode unduh
        if($this->input->post('mode') != 'view')
        {
            $query = $this->db->order_by('wl.idunit','asc');
            $query = $this->db->order_by('wl.tanggalperiksa','asc');
        }
        
        $query = $this->db->get('rs_waktu_layanan wl');
        if($query->num_rows() > 0){return $query;}
        return false;
    }    
    public function dt_waktulayanan_($limit=false)
    {
        $column = ['wl.norm','pper.namalengkap','wl.tanggalperiksa','ru.namaunit'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_waktulayanan();   
    }
    public function total_dt_waktulayanan(){return $this->recordsTotal($this->dt_waktulayanan());}
    public function filter_dt_waktulayanan(){return $this->recordsFiltered($this->dt_waktulayanan_());}
    
    //report waktu pendaftaran
    //Memey,
    public function dt_waktupendaftaran()
    {
        $query = $this->db->select("id_pendaftaran_admisi, idantrian, waktu_ambilantrian, jenis_antrian, nomor_antrian, waktu_panggil_antrian,tanggal");
        
        if($this->input->post('mode') == 'view')
        {
            $query = $this->db->where('tanggal',$this->input->post('tanggal'));
        }
        else
        {
            //jika mode unduh
            $tahunbulan = date('Y-m',strtotime($this->input->post('tahunbulan')));
            $tanggal1   = $tahunbulan.'-01';
            $tanggal2   = $tahunbulan.'-31';
            $query = $this->db->where('tanggal BETWEEN "'.$tanggal1.'" AND "'.$tanggal2.'" ');
        }
        // $query = $this->db->group_by('idantrian');
        
        //jika mode unduh
        if($this->input->post('mode') != 'view')
        {
            $query = $this->db->order_by('tanggal','asc');
            $query = $this->db->order_by('idantrian','asc');
        }
        
        $query = $this->db->get('waktu_pendaftaran_admisi');
        if($query->num_rows() > 0){return $query;}
        return false;
    }    
    public function dt_waktupendaftaran_($limit=false)
    {
        $column = ['idantrian','tanggal'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_waktupendaftaran();   
    }
    public function total_dt_waktupendaftaran(){return $this->recordsTotal($this->dt_waktupendaftaran());}
    public function filter_dt_waktupendaftaran(){return $this->recordsFiltered($this->dt_waktupendaftaran_());}
    
    
    //list data user
    public function dt_user()
    {
        $query = $this->db->select("lu.namauser,lu.iduser,lu.password_reset");
        $query = $this->db->get("login_user lu");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_user_($limit=false)
    {
        $column = ['lu.namauser'];
        if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_user();
    }
    public function total_dt_user(){return $this->recordsTotal($this->dt_user());}
    public function filter_dt_user(){return $this->recordsFiltered($this->dt_user_());}
    
    //LABORATORIUM RANAP
    public function dt_laboratoriumranap()
    {
        $post = $this->input->post();
        $query = $this->db->select("ri.idpendaftaran, ri.idinap, ppas.norm, namapasien(pper.idperson) as namalengkap, pper.tanggallahir, usia(pper.tanggallahir) as usia, pper.alamat, pper.notelpon, namadokter(ri.idpegawaidokter) as namadokter, ppd.nosep, rbl.namabangsal, rb.nobed, ri.waktumasuk");
        
        $query = $this->db->join('rs_inap_rencana_medis_pemeriksaan rirmp','rirmp.idinap = ri.idinap');
        $query = $this->db->join('rs_inap_rencana_medis_hasilpemeriksaan rirmh','rirmh.idrencanamedispemeriksaan = rirmp.idrencanamedispemeriksaan');
        $query = $this->db->join('rs_icd ricd','ricd.icd=rirmh.icd');
        $query = $this->db->join('person_pendaftaran ppd','ppd.idpendaftaran = ri.idpendaftaran');
        $query = $this->db->join('person_pasien ppas','ppas.norm = ppd.norm');
        $query = $this->db->join('person_person pper','pper.idperson = ppas.idperson');
        $query = $this->db->join('rs_bed rb','rb.idbed=ri.idbed');
        $query = $this->db->join('rs_bangsal rbl','rbl.idbangsal=rb.idbangsal');
        $query = ((empty($post['idbangsal']))? '' :  $this->db->where('rb.idbangsal',$post['idbangsal']) );
        $query = $this->db->where('ri.status','ranap');
        $query = $this->db->where('ricd.idjenisicd','4');
        $query = $this->db->where('date(rirmp.waktu)',$this->input->post('tanggal'));
        $query = $this->db->group_by('ri.idinap');
        $query = $this->db->get('rs_inap ri');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_laboratoriumranap_($limit=false)
    {
        $column = ['ri.idinap','ppas.norm','pper.namalengkap','ppd.nosep','ri.waktumasuk'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_laboratoriumranap();   
    }
    public function total_dt_laboratoriumranap(){ return $this->recordsTotal($this->dt_laboratoriumranap());}
    public function filter_dt_laboratoriumranap(){return $this->recordsFiltered($this->dt_laboratoriumranap_());}
    
    //Data Barang
    public function dt_barang()
    {
        $post = $this->input->post();
        
        $query = $this->db->select("rb.ishidden, rbp.idbarangpembelian,rb.idbarang,rb.statusbarang, rb.namabarang, rb.kode,ifnull(sum(rbs.stok),0) stok, ifnull(rb.rop,0) rop, "
                . "ifnull(rb.stokaman,0) stokaman, ifnull(rb.stokminimal,0) stokminimal, ifnull(rb.stokhabis,0) stokhabis, rb.hargabeli, rb.hargajual, rb.het,rb.hargaumum,"
                . "rb.kekuatan,rb.jenis, rj.jenistarif, rs.namasatuan, rsd.namasediaan, rb.tipeobat");
        $query = $this->db->join('rs_barang_pembelian rbp ',' rbp.idbarang = rb.idbarang','left');
        $query = $this->db->join('rs_barang_stok rbs','rbs.idbarangpembelian=rbp.idbarangpembelian and rbs.idunit='.$this->session->userdata('idunitterpilih'),'left');
        $query = $this->db->join('rs_jenistarif rj','rj.idjenistarif=rb.idjenistarif','left');
        $query = $this->db->join('rs_satuan rs','rs.idsatuan=rb.idsatuan','left');
        $query = $this->db->join('rs_sediaan rsd','rsd.idsediaan=rb.idsediaan','left');
        $query = $this->db->group_by('rb.idbarang');
        $query = $this->db->get('rs_barang rb');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_barang_($limit=false)
    {
        $column = ['rb.kode','rb.namabarang',false,false,'rj.jenistarif','rs.namasatuan','rsd.namasediaan','rb.jenis','rb.tipeobat','rb.hargabeli','rb.hargajual','rb.hargaumum','rb.het','rb.kekuatan','rb.statusbarang'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_barang();   
    }
    public function total_dt_barang(){ return $this->recordsTotal($this->dt_barang());}
    public function filter_dt_barang(){return $this->recordsFiltered($this->dt_barang_());}
    
    
    //Data Pegawai
    public function dt_pegawai()
    {
        $query = $this->db->select("ppg.bpjs_kodedokter,pper.nik,ppg.idpegawai,ppg.ihspractitioner,ppg.nip,ppg.sip, ppg.statuskeaktifan,pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai");
        // $query = $this->db->select("ppg.bpjs_kodedokter, ppg.idpegawai,ppg.nip,ppg.sip, ppg.statuskeaktifan,pgp2.profesi, concat(ifnull(ppg.titeldepan,''),' ',pper.namalengkap,' ', ifnull(ppg.titelbelakang,'')) as namalengkap,pgp.namagruppegawai");
        $query = $this->db->join('person_person pper','pper.idperson = ppg.idperson');
        $query = $this->db->join('person_grup_pegawai pgp','pgp.idgruppegawai = ppg.idgruppegawai');
        $query = $this->db->join('person_grup_profesi pgp2','pgp2.idprofesi = ppg.idprofesi','left');
        $query = $this->db->get('person_pegawai ppg');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_pegawai_($limit=false)
    {
        $column = ['ppg.idpegawai','ppg.nip','ppg.sip','ppg.bpjs_kodedokter','pper.namalengkap','pgp2.profesi','pgp.namagruppegawai','ppg.statuskeaktifan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pegawai();   
    }
    public function total_dt_pegawai(){ return $this->recordsTotal($this->dt_pegawai());}
    public function filter_dt_pegawai(){return $this->recordsFiltered($this->dt_pegawai_());}
    
    
    //Data Master Jadwal
    public function dt_masterjadwal()
    {
        $query = $this->db->get('vrs_masterjadwal vm');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_masterjadwal_($limit=false)
    {
        $column = ['vm.namaunit','vm.namalengkap','vm.ahad','vm.senin','vm.selasa','vm.rabu','vm.kamis','vm.jumat','vm.sabtu'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_masterjadwal();   
    }
    public function total_dt_masterjadwal(){ return $this->recordsTotal($this->dt_masterjadwal());}
    public function filter_dt_masterjadwal(){return $this->recordsFiltered($this->dt_masterjadwal_());}
    
    //List Data Bed
    public function dt_bed()
    {
        $query = $this->db
                ->select('b.*, s.statusbed, bs.namabangsal, k.kelas')
                ->join('rs_bed_status s','s.idstatusbed = b.idstatusbed')
                ->join('rs_bangsal bs','bs.idbangsal = b.idbangsal')
                ->join('rs_kelas k','k.idkelas = bs.idkelas');
        if(empty(!$this->input->post('idbangsal')))
        {
            $query = $this->db->where('b.idbangsal',$this->input->post('idbangsal'));
        }
        $query = $this->db->get('rs_bed b');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_bed_($limit=false)
    {
        $column = ['b.idbed','bs.namabangsal','k.kelas','s.statusbed','b.nobed'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_bed();   
    }
    public function total_dt_bed(){ return $this->recordsTotal($this->dt_bed());}
    public function filter_dt_bed(){return $this->recordsFiltered($this->dt_bed_());}
    
    //List Data Pendaftaran
    public function dt_pendaftaran($mode='')
    {
        $post = $this->input->post();
        $tglawal = ((empty($post['tglawal']))? date("Y-m-d") : format_date_to_sql($post['tglawal']));
        $tglakhir= ((empty($post['tglakhir']))? date("Y-m-d") : format_date_to_sql($post['tglakhir']));
        $tahun1  = empty($post['tglawal']) ? date('Y') : date('Y', strtotime($tglawal));
        $tahun2  = empty($post['tglakhir']) ? date('Y') : date('Y', strtotime($tglakhir));
        $jenistampilan = $this->input->post('jenistampilan');       
        
        $query = "";
        
        if($mode=='sum')
        {
            $query = $this->db->select('sum(1) as total_rows');
        }
        else
        {
            if(empty($jenistampilan))
            {
                $query = $this->db->select("rp.idpemeriksaan, ppd.statuspulang, date_format(ppd.waktuperiksa,' %d/%m/%Y') as tgljadwal, 
                ppd.pasienprolanis, ifnull(ppd.tanggal_kunjunganberikutnya,'') as tanggal_kunjunganberikutnya,  
                ppd.idjadwalgrup, (select if(count(1) > 0,'-pesan ranap-','') from rs_inap inap where inap.idpendaftaran=ppd.idpendaftaran) isranap, 
                pp.tanggallahir, pp.alamat,pp.notelpon, pp.cetakkartu, ppd.isberkassudahada,ppd.caradaftar,ppd.isantri, ppd.kategoriskrining,
                (select kelas from rs_kelas where idkelas = ppd.idkelas ) as kelas, 
                pp.idperson, ppd.idunit, pp.nik, 
                concat(ifnull(pp.identitas,''),' ', pp.namalengkap) namalengkap, ppd.idpendaftaran, ppd.norm, ppd.waktu, 
                ppd.waktuperiksa, ppd.idstatuskeluar, ppd.carabayar, ppd.norujukan, ppd.nosep, 
                ppd.jenispemeriksaan, ppd.isantri as noantrian, ru.namaunit, ppb.idunit as blacklist, ppd.sudahcetakSEP");
            }
            else
            {
                $query = $this->db->select("*");
            }
        }
        
        if(empty($jenistampilan))
        {
            $query = $this->db->join('person_pasien ppas','ppas.norm=ppd.norm');
            $query = $this->db->join('person_person pp','pp.idperson = ppas.idperson');
            $query = $this->db->join('rs_pemeriksaan rp','rp.idpendaftaran = ppd.idpendaftaran');
            $query = $this->db->join('rs_unit ru','ru.idunit = rp.idunit');
            $query = $this->db->join('person_pasien_blacklistunit ppb','ppb.norm = ppd.norm and ppb.idunit = ppd.idunit','left');
            $query = $this->db->where("ppd.tahunperiksa between '".$tahun1."' and '".$tahun2."'");
            $query = $this->db->group_start();
            $query = $this->db->where("date(ppd.waktu) between '".$tglawal."' and '".$tglakhir."'");
            $query = $this->db->or_where("date(ppd.waktuperiksa) between '".$tglawal."' and '".$tglakhir."'");
            $query = $this->db->group_end();
            $query = $this->db->get('person_pendaftaran ppd');
        }
        else
        {
            $query = $this->db->where("ppd.tahunperiksa between '".$tahun1."' and '".$tahun2."'");
            $query = $this->db->group_start();
            $query = $this->db->where("date(ppd.waktu) between '".$tglawal."' and '".$tglakhir."'");
            $query = $this->db->or_where("date(ppd.waktuperiksa) between '".$tglawal."' and '".$tglakhir."'");
            $query = $this->db->group_end();
            $query = $this->db->order_by('ppd.waktuperiksa','desc');
            $query = $this->db->get('vrs_pasienbelumterantrikan ppd');
        }        
        return $query;
    }
    public function dt_pendaftaran_($limit=false)
    {
        $column = [];
        if(empty($this->input->post('jenistampilan')))
        {
            $column = ['ppd.norm','pp.nik','pp.namalengkap','ppd.waktuperiksa','ppd.waktu','ppd.caradaftar'];
        }
        else    
        {
            $column = ['ppd.norm','ppd.nik','ppd.namalengkap','ppd.waktuperiksa','ppd.waktu','ppd.caradaftar'];
        }
        
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        // if($limit){$this->limit(5,0);}
        return $this->dt_pendaftaran();   
    }
    // request pendaftaran karna loadingnya lama
    // public function total_dt_pendaftaran(){ return $this->dt_pendaftaran('sum')->row_array()['total_rows'];}
    public function filter_dt_pendaftaran(){return 10;}// {return $this->recordsFiltered($this->dt_pendaftaran_());}

    
    //List Data Retur Barang ke Distributor
    public function dt_returkedistributor()
    {
        $query = $this->db->select("a.*, b.nofaktur, b.tanggalfaktur, c.distributor,
                        (select GROUP_CONCAT(concat('<li>', e.kode,' ', e.namabarang,' ', d.jumlah,' ',f.namasatuan,'</li>')) 
                         FROM rs_barang_pengembalian_detail d 
                         join rs_barang e on e.idbarang=d.idbarang
                         join rs_satuan f on f.idsatuan = e.idsatuan
                         where d.idbarangpengembalian=a.idbarangpengembalian) as returdetail")
                ->join('rs_barang_faktur b','b.idbarangfaktur = a.idbarangfaktur')
                ->join('rs_barang_distributor c','c.idbarangdistributor=b.idbarangdistributor')
                ->get('rs_barang_pengembalian a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_returkedistributor_($limit=false)
    {
        $column = ['c.distributor','b.nofaktur','b.tanggalfaktur','a.tanggalretur','a.status'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_returkedistributor();   
    }
    public function total_dt_returkedistributor(){ return $this->recordsTotal($this->dt_returkedistributor());}
    public function filter_dt_returkedistributor(){return $this->recordsFiltered($this->dt_returkedistributor_());}
    
    
    //List Data Retur Barang ke Farmasi
    public function dt_returkefarmasi()
    {
        $query = $this->db->select("a.idbarangpemesanan, a.nofaktur, a.waktu, a.statusbarangpemesanan, 
                        (SELECT namaunit from rs_unit_farmasi where idunit= a.idunittujuan) as unitasal,
                        (SELECT namaunit from rs_unit_farmasi where idunit= a.idunit) as unittujuan,
                        (SELECT GROUP_CONCAT( concat( '<li>', e.kode,' ', e.namabarang,' ', d.jumlah, ' ',f.namasatuan,'</li>') ) from rs_barang_pemesanan_detail d join rs_barang e on e.idbarang=d.idbarang join rs_satuan f on f.idsatuan = e.idsatuan where d.idbarangpemesanan=a.idbarangpemesanan) as returdetail")
                ->where('a.jenistransaksi','retur')
                ->where('a.idunit',$this->session->userdata('idunitterpilih'))
                ->get('rs_barang_pemesanan a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_returkefarmasi_($limit=false)
    {
        $column = ['a.nofaktur','a.waktu'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_returkefarmasi();   
    }
    public function total_dt_returkefarmasi(){ return $this->recordsTotal($this->dt_returkefarmasi());}
    public function filter_dt_returkefarmasi(){return $this->recordsFiltered($this->dt_returkefarmasi_());}
    
    //List Data Retur Barang dari depo ke  Farmasi
    public function dt_listreturdepo()
    {
        $query = $this->db->select("a.idbarangpemesanan, a.nofaktur, a.waktu, a.statusbarangpemesanan, 
                        (SELECT namaunit from rs_unit_farmasi where idunit= a.idunittujuan) as unitasal,
                        (SELECT namaunit from rs_unit_farmasi where idunit= a.idunit) as unittujuan,
                        (SELECT GROUP_CONCAT( concat( '<li>', e.kode,' ', e.namabarang,' ', d.jumlah, ' ',f.namasatuan,'</li>') ) from rs_barang_pemesanan_detail d join rs_barang e on e.idbarang=d.idbarang join rs_satuan f on f.idsatuan = e.idsatuan where d.idbarangpemesanan=a.idbarangpemesanan) as returdetail")
                ->where('a.jenistransaksi','retur')
                ->where('a.idunittujuan',$this->session->userdata('idunitterpilih'))
                ->where('a.tanggalpemesanan between "'.$this->input->post('tanggal1').'" and "'.$this->input->post('tanggal2').'" ')
                ->get('rs_barang_pemesanan a');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    public function dt_listreturdepo_($limit=false)
    {
        $column = ['a.nofaktur','a.waktu'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_listreturdepo();   
    }
    public function total_dt_listreturdepo(){ return $this->recordsTotal($this->dt_listreturdepo());}
    public function filter_dt_listreturdepo(){return $this->recordsFiltered($this->dt_listreturdepo_());}
    
    
    public function dt_kartustokdepo()
    {
        $query = $this->db->select("rbs.idbarangpembelian, b.kode, b.namabarang, rbs.stok, s.namasatuan, rbp.batchno, rbp.kadaluarsa");
        $query = $this->db->join("rs_barang_pembelian rbp","rbp.idbarangpembelian = rbs.idbarangpembelian");
        $query = $this->db->join("rs_barang b","b.idbarang = rbp.idbarang");
        $query = $this->db->join("rs_satuan s","s.idsatuan = b.idsatuan");
        $query = $this->db->where("rbs.idunit",$this->session->userdata('idunitterpilih'));
        $query = $this->db->get("rs_barang_stok rbs");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_kartustokdepo_($limit=false)
    {
        $column = ['b.kode','b.namabarang','rbp.batchno','rbp.kadaluarsa','rbs.stok','s.namasatuan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_kartustokdepo();   
    }
    public function total_dt_kartustokdepo(){ return $this->recordsTotal($this->dt_kartustokdepo());}
    public function filter_dt_kartustokdepo(){return $this->recordsFiltered($this->dt_kartustokdepo_());}
    
    
    public function dt_pesanobatfarmasiranap()
    {
        $query = $this->db->select("a.tanggalpemesanan, b.idbarangpemesanandetail,a.statusbarangpemesanan as status, ifnull((select f.batchno from rs_barang_distribusi e join rs_barang_pembelian f on f.idbarangpembelian = e.idbarangpembelian where e.idbarangpemesanan=b.idbarangpemesanan and e.idunittujuan = a.idunittujuan and f.idbarang = b.idbarang),0)  as batchno, "
                . "ifnull((select f.kadaluarsa from rs_barang_distribusi e join rs_barang_pembelian f on f.idbarangpembelian = e.idbarangpembelian where e.idbarangpemesanan=b.idbarangpemesanan and e.idunittujuan = a.idunittujuan and f.idbarang = b.idbarang),0)  as kadaluarsa, "
                . "ifnull((select sum(e.jumlah) from rs_barang_distribusi e join rs_barang_pembelian f on f.idbarangpembelian = e.idbarangpembelian where e.idbarangpemesanan=b.idbarangpemesanan and e.idunittujuan = a.idunittujuan and f.idbarang = b.idbarang),0)  as diberikan, "
                . "b.jumlah as pesan, c.namabarang, c.kode, d.namasatuan");
        $query = $this->db->join("rs_barang_pemesanan_detail b","b.idbarangpemesanan=a.idbarangpemesanan");
        $query = $this->db->join("rs_barang c","c.idbarang = b.idbarang");
        $query = $this->db->join("rs_satuan d","d.idsatuan = c.idsatuan");
        $query = $this->db->where("a.idunittujuan",$this->session->userdata('idunitterpilih'));
        $query = $this->db->where("a.idinap",$this->input->post('idinap'));
        $query = $this->db->where("jenistransaksi",$this->input->post('jenis'));
        $query = $this->db->order_by("a.tanggalpemesanan",'desc');
        $query = $this->db->get("rs_barang_pemesanan a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_pesanobatfarmasiranap_($limit=false)
    {
        $column = ['a.idbarangpemesanan','c.namabarang','b.jumlah'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pesanobatfarmasiranap();  
    }
    public function total_dt_pesanobatfarmasiranap(){ return $this->recordsTotal($this->dt_pesanobatfarmasiranap());}
    public function filter_dt_pesanobatfarmasiranap(){return $this->recordsFiltered($this->dt_pesanobatfarmasiranap_());}
    
    
    public function dt_returobatfarmasiranap()
    {
        $query = $this->db->select("a.tanggalpemesanan,b.idbarangpemesanandetail, a.statusbarangpemesanan as status, b.idbarangpembelian, b.jumlah as pesan, c.namabarang, c.kode, d.namasatuan, e.batchno, e.kadaluarsa");
        $query = $this->db->join("rs_barang_pemesanan_detail b","b.idbarangpemesanan=a.idbarangpemesanan");
        $query = $this->db->join("rs_barang_pembelian e","e.idbarangpembelian = b.idbarangpembelian");
        $query = $this->db->join("rs_barang c","c.idbarang = e.idbarang");
        $query = $this->db->join("rs_satuan d","d.idsatuan = c.idsatuan");
        $query = $this->db->where("a.idunit",$this->session->userdata('idunitterpilih'));
        $query = $this->db->where("a.idinap",$this->input->post('idinap'));
        $query = $this->db->where("jenistransaksi",$this->input->post('jenis'));
        $query = $this->db->order_by("a.tanggalpemesanan",'desc');
        $query = $this->db->get("rs_barang_pemesanan a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_returobatfarmasiranap_($limit=false)
    {
        $column = ['a.idbarangpemesanan','c.namabarang','b.jumlah'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_returobatfarmasiranap();  
    }
    public function total_dt_returobatfarmasiranap(){ return $this->recordsTotal($this->dt_returobatfarmasiranap());}
    public function filter_dt_returobatfarmasiranap(){return $this->recordsFiltered($this->dt_returobatfarmasiranap_());}
    
    public function dt_farmasipasienranap()
    {
        $query = $this->db->select("0 as idbarangpemesanan, 0 as idbarangpembelian, 0 as idinap , 0 as stok, a.idbarang, b.namabarang, b.kode, c.namasatuan, ifnull(a.jumlahperencanaan,0) as rencana, ifnull(a.jumlahpemakaian,0) as terlaksana, "
                . "ifnull(a.jumlahpemakaian,0) as terlaksana , 
        ifnull((select sum(rbs.stok) from rs_barang_pemesanan rbp
        join rs_barang_distribusi rbd on rbd.idbarangpemesanan = rbp.idbarangpemesanan
        join rs_barang_stok rbs on rbs.idbarangpembelian = rbd.idbarangpembelian and rbs.idunit = '".$this->session->userdata('idunitterpilih')."'
        join rs_barang_pembelian rbl on rbl.idbarangpembelian = rbs.idbarangpembelian
        WHERE rbp.jenistransaksi='pesan' and rbp.idinap=1 and rbl.idbarang = a.idbarang),0) as stok ");
        $query = $this->db->join("rs_barang b","b.idbarang = a.idbarang");
        $query = $this->db->join("rs_satuan c","c.idsatuan = b.idsatuan");
        $query = $this->db->where("a.idinap",$this->input->post('idinap'));
        $query = $this->db->get("rs_inap_rencana_medis_barang a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_farmasipasienranap_($limit=false)
    {
        $column = ['a.idbarang','b.namabarang'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_farmasipasienranap();  
    }
    public function total_dt_farmasipasienranap(){ return $this->recordsTotal($this->dt_farmasipasienranap());}
    public function filter_dt_farmasipasienranap(){return $this->recordsFiltered($this->dt_farmasipasienranap_());}
    
    
    public function dt_pengeluaran()
    {
                
        $post = $this->input->post();
        $query = $this->db->select("*");
        if($this->input->post('statusjurnal') == 1)
        {
            $query = $this->db->where("rp.statusrekap",0);
        }
        $query = $this->db->where(" rp.tanggal between '".$this->input->post('awal')."' and '".$this->input->post('akhir')."' ");
        $query = $this->db->order_by('rp.tanggal,rp.idpengeluaran','asc');
        $query = $this->db->get("vrs_rs_pengeluaran rp");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_pengeluaran_($limit=false)
    {
        $column = ['rp.kategoribiaya','rp.tanggal','rp.notransaksi','rp.penjual','rp.ppn','rp.is_transfer','rp.nominalbiaya',false,'rp.keterangan','rp.namalengkap'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_pengeluaran();  
    }
    public function total_dt_pengeluaran(){ return $this->recordsTotal($this->dt_pengeluaran());}
    public function filter_dt_pengeluaran(){return $this->recordsFiltered($this->dt_pengeluaran_());}
    
    
    public function dt_konfigcoakasir()
    {
        $query = $this->db->select("a.*, b.namauser , c.nama");
        $query = $this->db->join("login_user b "," b.iduser = a.iduserkasir");
        $query = $this->db->join("keu_akun c "," c.kode = a.kode");
        $query = $this->db->get("keu_akun_kaskasir a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_konfigcoakasir_($limit=false)
    {
        $column = ['a.kode','c.nama','a.carabayar','b.namauser'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_konfigcoakasir();  
    }
    public function total_dt_konfigcoakasir(){ return $this->recordsTotal($this->dt_konfigcoakasir());}
    public function filter_dt_konfigcoakasir(){return $this->recordsFiltered($this->dt_konfigcoakasir_());}
    
    /**
     * start Get Data Table - data konfigurasi coa bank
     * mahmud, clear
     */
    public function dt_konfigcoabank()
    {
        $query = $this->db->select("a.kode, b.nama");
        $query = $this->db->join("keu_akun b "," b.kode = a.kode");
        $query = $this->db->get("keu_akun_kasbank a");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_konfigcoabank_($limit=false)
    {
        $column = ['a.kode','b.nama'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_konfigcoabank();  
    }
    public function total_dt_konfigcoabank(){return $this->recordsTotal($this->dt_konfigcoabank());}
    public function filter_dt_konfigcoabank(){return $this->recordsFiltered($this->dt_konfigcoabank_());}
    //-- end get data table - data konfigurasi coa bank
    
    public function dt_belanjaObat()
    {
        // $post   = $this->input->post();
        // $like   = (empty(!$post['search']['value']))?"and rb.namabarang like '%".$post['search']['value']."%'":'';
        $query= $this->db->query("SELECT rbp.idbarang, 
                            rb.kode,
                            rb.namabarang,
                            rs.namasatuan, 
                            rbf.tanggalfaktur, 
                            rbf.nopesan, 
                            rbf.nofaktur, 
                            rbp.batchno, 
                            rbp.kadaluarsa, 
                            rbd.distributor, 
                            rbdi.jumlah 
                    from  rs_barang_faktur rbf 
                    JOIN  rs_barang_pembelian rbp ON rbp.idbarangfaktur=rbf.idbarangfaktur  
                    JOIN  rs_barang rb ON rb.idbarang=rbp.idbarang  
                    JOIN  rs_satuan rs ON rs.idsatuan=rb.idsatuan 
                    JOIN  rs_barang_distributor rbd ON rbd.idbarangdistributor=rbf.idbarangdistributor
                    JOIN  rs_barang_distribusi rbdi ON rbdi.idbarangpembelian=rbp.idbarangpembelian 
                    WHERE rbdi.jenisdistribusi='masuk'
                    ORDER BY rbf.idbarangdistributor, rbf.tanggalfaktur");
                        // and isnull(rbp.parentidbarangpembelian) 
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    /**
     * start Get Data Table - data obat slow moving
     * mahmud, clear
     */
    public function dt_slowmovingobat()
    {
        $tanggal = ql_bulankemarin(date('Y-m-d'));
        $query = $this->db->select("rb.kode, rb.namabarang, sum(rbs.stok) as stok, rs.namasatuan, rbp.batchno, rbp.kadaluarsa");
        $query = $this->db->join("rs_barang_pembelian rbp ","rbp.idbarang = rb.idbarang","left");
        $query = $this->db->join("rs_barang_stok rbs "," rbs.idbarangpembelian = rbp.idbarangpembelian and rbs.idunit=24","left");
        $query = $this->db->join("rs_satuan rs "," rs.idsatuan = rb.idsatuan","left");  
        
        $query = $this->db->where('rbs.stok >',0);
        $query = $this->db->where('rb.terakhir_penjualan <',$tanggal);
        $query = $this->db->group_by('rb.idbarang');
        $query = $this->db->get("rs_barang rb");
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_slowmovingobat_($limit=false)
    {
        $column = ['rb.kode','rb.namabarang','rbp.batchno','rbp.kadaluarsa',false,'rs.namasatuan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_slowmovingobat();  
    }
    public function total_dt_slowmovingobat(){return $this->recordsTotal($this->dt_slowmovingobat());}
    public function filter_dt_slowmovingobat(){return $this->recordsFiltered($this->dt_slowmovingobat_());}
    //-- end Get Data Table - data obat slow moving
    
    public function dt_penggunaanbarangincludetindakan()
    {
        $tanggal1 = $this->input->post('min');
        $tanggal2 = $this->input->post('max');
        $idunit   = $this->input->post('idunit');
        $idbarang = $this->input->post('idbarang');
        
        $sql = $this->db->select("rbp.keterangan, rbp.iduserbuat, rbp.waktudibuat, rbp.terakhirdiubah,lu.namauser,rb.kode,rb.kekuatan,rb.jenis,rb.namabarang,rbpd.jumlah, rs.namasatuan, rb.hargajual");
        $sql = $this->db->join('rs_barang_penggunaan_detail rbpd','rbpd.idbarangpenggunaan = rbp.idbarangpenggunaan and rbpd.idunitfarmasi = rbp.idunitfarmasi');
        $sql = $this->db->join('rs_barang rb','rb.idbarang = rbpd.idbarang');
        $sql = $this->db->join('rs_satuan rs','rs.idsatuan = rb.idsatuan','left');
        $sql = $this->db->join('login_user lu','lu.iduser = rbp.iduserbuat','left');
        $sql = $this->db->where('rbp.idunitfarmasi',$idunit);
        $sql = $this->db->where('date(rbp.waktudibuat) BETWEEN  "'.$tanggal1.'" and "'.$tanggal2.'" ');
        if(empty(!$idbarang))
        {
            $sql = $this->db->where('rb.idbarang',$idbarang);
        }
        $sql = $this->db->get('rs_barang_penggunaan rbp');
        if($sql->num_rows() > 0){return $sql;}
        return false;
    }
    
    public function dt_penggunaanbarangincludetindakan_($limit=false)
    {
        $column = ['rbp.waktudibuat','lu.namauser','rb.namabarang','rbpd.jumlah','rs.namasatuan','rb.jenis','rb.kekuatan','rb.hargajual'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_penggunaanbarangincludetindakan();  
    }
    public function total_dt_penggunaanbarangincludetindakan(){return $this->recordsTotal($this->dt_penggunaanbarangincludetindakan());}
    public function filter_dt_penggunaanbarangincludetindakan(){return $this->recordsFiltered($this->dt_penggunaanbarangincludetindakan_());}

    /**
     * start Get Data Table - data srok perbandingan
     * mahmud, clear
     */
    public function dt_stokperbandingan()
    {
        $sql = $this->db->select('rb.namabarang, rb.kode, rbsp.stoklama, rbsp.stokbaru, rs.namasatuan');
        $sql = $this->db->join('rs_barang_stok_perbandingan rbsp','rbsp.idbarang = rb.idbarang');        
        $sql = $this->db->join('rs_satuan rs','rs.idsatuan = rb.idsatuan','left');
        $sql = $this->db->where('rbsp.idunit',$this->session->userdata('idunitterpilih'));
        $sql = $this->db->get('rs_barang rb');
        if($sql->num_rows() > 0){return $sql;}
        return false;
    }
    
    public function dt_stokperbandingan_($limit=false)
    {
        $column = ['rb.namabarang','rb.kode','rbsp.stoklama','rbsp.stokbaru','rs.namasatuan'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_stokperbandingan();  
    }
    public function total_dt_stokperbandingan(){return $this->recordsTotal($this->dt_stokperbandingan());}
    public function filter_dt_stokperbandingan(){return $this->recordsFiltered($this->dt_stokperbandingan_());}
    //-- end Get Data Table - data srok perbandingan
    
    /**
     * start Get Data Table - data Akses user
     * mahmud, clear
     */
    public function dt_aksesuser()
    {
        $sql = $this->db->select('lh.*, lu.namauser');
        $sql = $this->db->join('login_user lu', 'lu.iduser = lh.iduser','left');
        $sql = $this->db->get('login_hakaksesuser lh');
        if($sql->num_rows() > 0){return $sql;}
        return false;
    }
    
    public function dt_aksesuser_($limit=false)
    {
        $column = [false,'lu.namauser',false];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_aksesuser();  
    }
    public function total_dt_aksesuser(){return $this->recordsTotal($this->dt_aksesuser());}
    public function filter_dt_aksesuser(){return $this->recordsFiltered($this->dt_aksesuser_());}
    //-- end Get Data Table - data Akses user
    
    /**
     * start Get Data Table - data geografi_propinsi
     * mahmud, clear
     */
    public function dt_geografi_propinsi()
    {
        $sql = $this->db->get('geografi_propinsi');
        if($sql->num_rows() > 0){return $sql;}
        return false;
    }
    
    public function dt_geografi_propinsi_($limit=false)
    {
        $column = [false,'namapropinsi',false];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_geografi_propinsi();  
    }
    public function total_dt_geografi_propinsi(){return $this->recordsTotal($this->dt_geografi_propinsi());}
    public function filter_dt_geografi_propinsi(){return $this->recordsFiltered($this->dt_geografi_propinsi_());}
    //-- end Get Data Table - data geografi_propinsi
    
    /**
     * start Get Data Table - data register kamar bedah
     * mahmud, clear
     */
    public function dt_registerkamarbedah()
    {
        $sql = $this->db->select("ro.*, ppd.norm, pper.namalengkap,pper.alamat, roj.jaminan, namadokter(roj.iddokteroperator) as dokteroperator, "
                . "namadokter(ro.idpegawaidokteranestesi) as dokteranestesi,"
                . "namadokter(ro.idpegawaidokteranak) as dokteranak,"
                . "namadokter(ro.idpegawaipenata) as penata,"
                . "namadokter(ro.idpegawaipenerimabayi) as penerimabayi,"
                . "namadokter(ro.idpegawaisirkuler) as onloop,"
                . "namadokter(ro.idpegawaiasisten) as asisten,"
                . "namadokter(ro.idpegawaiinstrumen) as instrumen,"
                . "(select b.namabangsal from rs_bed a join rs_bangsal b on b.idbangsal = a.idbangsal where a.idbed = roj.idbed) as ruang,"
                . "roj.waktuoperasi, (select group_concat(jenisanestesi) from rs_jenisanestesi where idjenisanestesi in (ro.jenisanestesi) ) as jenisanestesi");
        $sql = $this->db->join("person_pendaftaran ppd","ppd.idpendaftaran = ro.idpendaftaran");
        $sql = $this->db->join("person_pasien ppas","ppas.norm = ppd.norm");
        $sql = $this->db->join("person_person pper","pper.idperson = ppas.idperson");
        $sql = $this->db->join("rs_jadwal_operasi roj","roj.idjadwaloperasi = ro.idjadwaloperasi");
        $sql = $this->db->where('ro.statusoperasi','selesai');
        $sql = $this->db->group_start();
        $sql = $this->db->where("date(roj.waktuoperasi) BETWEEN '".$this->input->post('tanggal1')."' and '".$this->input->post('tanggal2')."' ");
        $sql = $this->db->group_end();
        $sql = $this->db->get('rs_operasi ro');
        if($sql->num_rows() > 0){return $sql;}
        return false;
    }
    
    public function dt_registerkamarbedah_($limit=false)
    {
        $column = [false,'ppd.norm','pper.namalengkap',false];
        isset($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_registerkamarbedah();  
    }
    public function total_dt_registerkamarbedah(){return $this->recordsTotal($this->dt_registerkamarbedah());}
    public function filter_dt_registerkamarbedah(){return $this->recordsFiltered($this->dt_registerkamarbedah_());}
    //-- end Get Data Table - data register kamar bedah
    
    
    //start data stok depo
    public function dt_datastokbarang()
    {
        $post = $this->input->post();
        $idunit = ((isset($post['idunit']) && empty(!$post['idunit'])) ? $post['idunit'] : $this->session->userdata('idunitterpilih') );
        
        $sql = $this->db->select("rb.kode,rb.namabarang, rbs.stok, rs.namasatuan, rb.idsediaan, rb.jenis, rb.kekuatan, rb.tipeobat, sed.namasediaan, sed.jenissediaan");
        $sql = $this->db->join("rs_barang rb "," rb.idbarang = rbs.idbarang");
        $sql = $this->db->join("rs_satuan rs "," rs.idsatuan = rb.idsatuan","left");
        $sql = $this->db->join("rs_sediaan sed "," sed.idsediaan = rb.idsediaan","left");
        $sql = $this->db->where('rbs.idunit',$idunit);
        $sql = $this->db->get('rs_barang_stok rbs');
        if($sql){return $sql;}
        return false;
    }
    
    public function dt_datastokbarang_($limit=false)
    {
        $column = [false,'rb.kode','rb.namabarang','rbs.stok','rs.namasatuan'];
        isset($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_datastokbarang();  
    }
    public function total_dt_datastokbarang(){return $this->recordsTotal($this->dt_datastokbarang());}
    public function filter_dt_datastokbarang(){return $this->recordsFiltered($this->dt_datastokbarang_());}
    //end data stok depo
    
    //start data penggunaan barang
    public function dt_penggunaanbarang()
    {
        $idunit = $this->session->userdata('idunitterpilih');
        $bulan = date('m', strtotime($this->input->post('tahunbulan')));
        $tahun = date('Y', strtotime($this->input->post('tahunbulan')));
        
        $sql = $this->db->select("rbp.idbarangpenggunaan,rbpd.idbarang, rbpd.idbarangpembelian, rbp.keterangan, rbp.iduserbuat, rbp.waktudibuat, rbp.iduserubah, rbp.terakhirdiubah,lu.namauser as userbuat, lu2.namauser as userubah,rb.namabarang,rbpd.jumlah, rs.namasatuan, rb.hargajual");
        $sql = $this->db->join('rs_barang_penggunaan_detail rbpd','rbpd.idbarangpenggunaan = rbp.idbarangpenggunaan and rbpd.idunitfarmasi = rbp.idunitfarmasi');
        $sql = $this->db->join('rs_barang rb','rb.idbarang = rbpd.idbarang');
        $sql = $this->db->join('rs_satuan rs','rs.idsatuan = rb.idsatuan','left');
        $sql = $this->db->join('login_user lu','lu.iduser = rbp.iduserbuat','left');
        $sql = $this->db->join('login_user lu2','lu2.iduser = rbp.iduserubah','left');
        $sql = $this->db->where('rbp.idunitfarmasi',$idunit);
        $sql = $this->db->where('month(rbp.waktudibuat)',$bulan);
        $sql = $this->db->where('year(rbp.waktudibuat)',$tahun);
        $sql = $this->db->get('rs_barang_penggunaan rbp');
        if($sql){return $sql;}
        return false;
    }
    
    public function dt_penggunaanbarang_($limit=false)
    {
        $column = [false,false,'rb.namabarang','rbpd.jumlah','rb.hargajual',false,'rs.namasatuan','lu.namauser','rbp.waktudibuat',false];
        isset($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_penggunaanbarang();
    }
    
    public function total_dt_penggunaanbarang(){return $this->recordsTotal($this->dt_penggunaanbarang());}
    public function filter_dt_penggunaanbarang(){return $this->recordsFiltered($this->dt_penggunaanbarang_());}
    //end data stok depo

    //start data antrol_log
    public function dt_dataAntrolLog()
    {
        $mode = $this->input->post('mode');
        $tanggalAwal = format_date_to_sql($this->input->post('tanggalAwal'));
        $tanggalAkhir = format_date_to_sql($this->input->post('tanggalAkhir'));

        if($mode == 'log_taskId')
        {
            $sql = $this->db->select("  perpas.norm,
                                        perpas.nojkn,
                                        perper.namalengkap,
                                        perper.nik,
                                        bekt.idpendaftaran,
                                        bekt.taskid,
                                        bekt.message,
                                        bekt.tanggal,
                                        bekt.jam,
                                        aa.kodebooking, 
                                        pp.nosep
                                    ");
            $sql = $this->db->join("rs_pemeriksaan rp","bekt.idpendaftaran=rp.idpendaftaran");
            $sql = $this->db->join("antrian_antrian aa","aa.idpemeriksaan=rp.idpemeriksaan");
            $sql = $this->db->join("person_pendaftaran pp","bekt.idpendaftaran=pp.idpendaftaran");
            $sql = $this->db->join("person_pasien perpas","pp.norm= perpas.norm", "left");
            $sql = $this->db->join("person_person perper","perpas.idperson=perper.idperson");
            $sql = $this->db->where("bekt.tanggal BETWEEN '".$tanggalAwal."' AND '".$tanggalAkhir."'");
            $sql = $this->db->where("rp.idpemeriksaansebelum is null");
            $sql = $this->db->order_by("bekt.idpendaftaran", "ASC");
            $sql = $this->db->order_by("bekt.taskid", "ASC");
            $sql = $this->db->get('bpjs_error_kirimserver_taskid bekt');
        } 
        else if($mode == 'log_kode_booking')
        {
            $sql = $this->db->select("  perpas.norm,
                                        perpas.nojkn,
                                        perper.namalengkap,
                                        perper.nik,
                                        beka.idpendaftaran,
                                        aa.kodebooking,
                                        beka.message,
                                        beka.tanggal,
                                        beka.jam,
                                        pp.nosep,
                                        pp.carabayar,
                                        pp.caradaftar,
                                        pp.idjeniskunjungan,
                                        bbj.nama as namajeniskunjungan,
                                        pp.norujukan,
                                        ifnull(bbv.insert_by,'') as user,
                                        pp.idstatuskeluar,
                                        pp.sudahsingkronjkn
                                    ");
            $sql = $this->db->join("rs_pemeriksaan rp","beka.idpendaftaran=rp.idpendaftaran");
            $sql = $this->db->join("bpjs_bridging_vclaim bbv","bbv.idpendaftaran=rp.idpendaftaran");
            $sql = $this->db->join("antrian_antrian aa","aa.idpemeriksaan=rp.idpemeriksaan");
            $sql = $this->db->join("person_pendaftaran pp","beka.idpendaftaran=pp.idpendaftaran");
            $sql = $this->db->join("person_pasien perpas","pp.norm= perpas.norm", "left");
            $sql = $this->db->join("person_person perper","perpas.idperson=perper.idperson");
            $sql = $this->db->join("bpjs_bridging_jeniskunjungan bbj","pp.idjeniskunjungan= bbj.kode", "left");
            $sql = $this->db->where("beka.tanggal BETWEEN '".$tanggalAwal."' AND '".$tanggalAkhir."'");
            $sql = $this->db->where("rp.idpemeriksaansebelum is null");
            $sql = $this->db->order_by('beka.idpendaftaran', 'DESC');
            $sql = $this->db->order_by('beka.tanggal', 'ASC');
            $sql = $this->db->order_by('beka.jam', 'DESC');
            $sql = $this->db->get('bpjs_error_kirimserver_antrian beka');
        } 
        else if($mode == 'log_antrian')
        {
            $sql = $this->db->select("  bba.taskid, 
                                        bba.waktu, 
                                        bba.sudahsingkronjkn, 
                                        aa.kodebooking, 
                                        beka.message, 
                                        beka.tanggal, 
                                        beka.jam, 
                                        bba.idpendaftaran
                                    ");
            $sql = $this->db->join("bpjs_error_kirimserver_antrian beka","beka.idpendaftaran=bba.idpendaftaran");
            $sql = $this->db->join("rs_pemeriksaan rp","bba.idpendaftaran=rp.idpendaftaran");
            $sql = $this->db->join("antrian_antrian aa","aa.idpemeriksaan=rp.idpemeriksaan");
            $sql = $this->db->where("date(bba.waktu) BETWEEN '".$tanggalAwal."' AND '".$tanggalAkhir."'");
            $sql = $this->db->where("rp.idpemeriksaansebelum is null");
            $sql = $this->db->get('bpjs_bridging_antrean_task_log bba');
        } 

        if($sql){return $sql;}
        return false;
    }
    
    public function dt_dataAntrolLog_($limit=false)
    {
        $mode = $this->input->post('mode');
        if($mode == 'log_taskId')
        {
            $column = ['bekt.idpendaftaran', 'perpas.norm', 'perper.namalengkap', 'bekt.jam', 'bekt.taskid', 'bekt.message'];
        } 
        else if($mode == 'log_kode_booking')
        {
            $column = ['beka.idpendaftaran','perpas.norm','perper.namalengkap','beka.jam','beka.message','bbj.nama','pp.norujukan','pp.carabayar','bbv.insert_by', "pp.sudahsingkronjkn"];
        }
        else if($mode == 'log_antrian')
        {
            $column = ['bba.taskid','bba.waktu','bba.sudahsingkronjkn','aa.kodebooking','beka.message','beka.tanggal'];
        }
        isset($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_dataAntrolLog();  
    }
    public function total_dt_dataAntrolLog(){return $this->recordsTotal($this->dt_dataAntrolLog());}
    public function filter_dt_dataAntrolLog(){return $this->recordsFiltered($this->dt_dataAntrolLog_());}
    //end data antrol_log
    

    /**Inventory non medis**/
     public function dt_inventorynonmedis($id_unit)
     {
         $query = $this->db->select("inm.id_inventory,im.merekbarang,rb.namabangsal as unit_name,ms.supplier_name,ib.namabaranginventaris as inv_item,inm.no_inventory,inm.kondisi,harga,inm.tahun_pengadaan");
         $sql = $this->db->join("rs_master_supplier ms","ms.id_supplier=inm.id_distributor");
         $sql = $this->db->join("inventaris_merek im","im.idmerekbarang=inm.id_merk");
         $sql = $this->db->join("inventaris_barang ib","ib.idbaranginventaris=inm.id_barang");
         $sql = $this->db->join("rs_bangsal rb","rb.idbangsal=inm.id_unit");
         if ($id_unit!='') {
            $query = $this->db->where('inm.id_unit',$id_unit);
         }
         $query = $this->db->get("inventaris_non_medis inm");
         
         if($query->num_rows() > 0){return $query;}
         return false;
     }
     public function dt_inventorynonmedis_($id_unit)
     {
         $column = ['id_inventory','id_unit', 'id_barang', 'id_merk','no_inventory','kondisi','id_distributor','tahun_pengadaan', 'harga',  'created_at'];
         if($_POST['search']['regex']){ $this->like($column,$_POST['search']['value']); }
         $this->order($column);
        //  if($limit){$this->limit($_POST['length'],$_POST['start']);}
         return $this->dt_inventorynonmedis($id_unit);
     }
     public function total_dt_inventorynonmedis($id_unit){return $this->recordsTotal($this->dt_inventorynonmedis($id_unit));}
     public function filter_dt_inventorynonmedis($id_unit){return $this->recordsFiltered($this->dt_inventorynonmedis_($id_unit));}

     public function dt_inm($idunit)
    {

        $query = $this->db->select("inm.id_unit, ib.namabaranginventaris as item_name, inm.no_inventory,inm.kondisi,ms.supplier_name,ms.tlp,ms.id_supplier as manual,inm.tahun_pengadaan,inm.harga");
        $sql = $this->db->join("rs_master_supplier ms","ms.id_supplier=inm.id_distributor");
        $sql = $this->db->join("inventaris_barang ib","ib.idbaranginventaris=inm.id_barang");
        $query = $this->db->where('inm.id_unit',$idunit);
        $query = $this->db->get("inventaris_non_medis inm");
        if($query->num_rows() > 0){return $query;}
        return false;
    }    

    public function dt_mssupplier()
    {
        $query = $this->db->select('*')
                // ->join('inventaris_barangkategori ibk','ibk.idbarangkategori=ib.idbarangkategori','left')
                ->get('rs_master_supplier ms');
        if($query->num_rows() > 0){return $query;}
        return false;
    }
    
    public function dt_mssupplier_($limit=false)
    {
        $column = ['ms.id_supplier','ms.supplier_name','ms.supplier_address','ms.tlp'];
        ($_POST['search']['regex']) ? $this->like($column,$_POST['search']['value']) : '';
        $this->order($column);
        if($limit){$this->limit($_POST['length'],$_POST['start']);}
        return $this->dt_mssupplier();   
    }
    public function total_dt_mssupplier(){ return $this->recordsTotal($this->dt_mssupplier());}
    public function filter_dt_mssupplier(){return $this->recordsFiltered($this->dt_mssupplier_());}
}