<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtriggermanual extends CI_Model 
{

    /**
     * After Insert tabel rs_antrian
     * @param type $idjadwal    ID Jadwal
     */
    function airs_pemeriksaan($idjadwal)
    {
        //sementara dinon aktifkan
        //setiap pasien yang mendapat noantrian melebihi quota akan otomatis menjadi batal
        //$this->db->query("UPDATE rs_pemeriksaan a, rs_jadwal j SET a.status = 'batal' WHERE a.idjadwal=".$idjadwal." AND j.idjadwal=a.idjadwal AND a.noantrian>j.quota");
        return true;
    }
    
    /**
     * After Insert tabel rs_hasilpemeriksaan
     * @param 
     * type $idp     ID Pendaftaran
     * type $idpm    ID Pemeriksaan
     * type $idhp    ID Hasilpemeriksaan
     */
    function airs_hasilpemeriksaan($idp,$idpm,$idhp,$jenisicd,$icd='')
    {
    	$queryidunit=$this->db->query("select idunit from rs_unit where namaunit='".$jenisicd."'")->row_array();

        $idunit = "";
        if(!empty($queryidunit))
        {
            $idunit = $queryidunit['idunit'];
        }
        
        
        $jmbukanuntukpenunjang = $this->db->query("select isjmbukanpenunjang from rs_icd where icd='".$icd."'")->result_array();
        if(empty($jmbukanuntukpenunjang))
        {
            $jmbukanuntukpenunjang = 0;
        }
        else
        {
            $jmbukanuntukpenunjang = $jmbukanuntukpenunjang['0']['isjmbukanpenunjang'];
        }
        
    	if($jenisicd=='Radiologi' || $jenisicd=='Laboratorium')
    	{
            if($jmbukanuntukpenunjang==1){
                $this->db->query("update rs_hasilpemeriksaan set iddokterjasamedis=(select  ifnull(x.iddokterpengganti,y.idpegawaidokter) FROM rs_pemeriksaan x, rs_jadwal y WHERE x.idpemeriksaan=$idpm and y.idjadwal=x.idjadwal) where idhasilpemeriksaan=$idhp");
            }else {
                $this->db->query("update rs_hasilpemeriksaan set iddokterjasamedis=(select idpegawaidokter FROM `rs_unit` WHERE idunit=$idunit) where idhasilpemeriksaan=$idhp");
            }
    	    
    	}
    	elseif($jenisicd=='Tindakan')
    	{
    		$this->db->query("update rs_hasilpemeriksaan set iddokterjasamedis=(select  ifnull(x.iddokterpengganti,y.idpegawaidokter) FROM rs_pemeriksaan x, rs_jadwal y WHERE x.idpemeriksaan=$idpm and y.idjadwal=x.idjadwal) where idhasilpemeriksaan=$idhp");
    	}
    	
    }
    /**
     * After Update tabel keu_tagihan
     * @param 
     * type $id      ID Pendaftaran
     * type $status  Status Pemeriksaan
     */
    function aukeu_tagihan($id,$status)
    {
        //pengurangan stok otomatis saat pembayaran, stok dikurangi berdasarkan idpembelian
        $barang  = $this->db->query("SELECT a.idbarangpembelian,a.jumlah,b.idbarangpemeriksaan, a.idunit FROM rs_barangpemeriksaan_detail a join rs_barangpemeriksaan b on b.idbarangpemeriksaan=a.idbarangpemeriksaan and b.idpendaftaran='".$id."'");
        $idperson= json_decode($this->encryptbap->decrypt_urlsafe("SESSUSER", $this->session->userdata('pper')));
        if($barang->num_rows() > 0)
        {
            foreach ($barang->result_array() as $arr){
                $save = $this->db->insert('rs_barang_distribusi',['idbarangpembelian'=>$arr['idbarangpembelian'],'idunit'=>$arr['idunit'],'waktu'=>date('Y-m-d H:i:s'),'jumlah'=>$arr['jumlah'],'jenisdistribusi'=> (($status=='selesai')?'keluar':'pembetulankeluar'),'idpersonpetugas'=>$idperson]); 
                $idbd = $this->db->insert_id();
                $save = $this->db->update('rs_barangpemeriksaan_detail',['idbarangdistribusi'=>$idbd,'status'=> (($status=='selesai')?'terlaksana': (($status=='batal') ? 'batal' : 'rencana') ) ],['idbarangpemeriksaan'=>$arr['idbarangpemeriksaan']]);
            }
            return $save;
        }
    }
    /**
     * After Insert tabel pp_buku
     * @param 
     * type $idbuku      ID Buku
     * type $idkategori  ID Kategori
     * type $jumlah      Jumlah Buku
     */
    function aipp_buku($idbuku,$idkategori,$jumlah)
    {
        $nourut = $this->db->query("select max(nourut) + 1 as nourut from pp_buku where idkategori='".$idkategori."'")->row_array()['nourut'];
        $kode = $this->db->query("select concat((select kodekategori from pp_kategori where idkategori='".$idkategori."' ),'/',$nourut,'/',".$jumlah.") as kode from pp_buku limit 1")->row_array()['kode'];
        return $this->db->update('pp_buku',['nourut'=>$nourut,'kodebuku'=>$kode],['idbuku'=>$idbuku]);
    }
    
    /**
     * After Update tabel pp_buku
     * @param 
     * type $idbuku      ID Buku
     * type $jumlah      Jumlah Buku
     */
    function aupp_buku($idbuku,$jumlah)
    {   $kode = explode('/', $this->db->select('kodebuku')->get_where('pp_buku',['idbuku'=>$idbuku])->row_array()['kodebuku']);
        return $this->db->update('pp_buku',['kodebuku'=>$kode[0].'/'.$kode[1].'/'.$jumlah],['idbuku'=>$idbuku]);
    }
    
    /**
     * After Update tabel pp_kategori
     * @param 
     * type $idkategori    ID Kategori
     */
    function aipp_kategori($idkategori)
    {   $kode = sprintf("%03s", intval($this->db->query('select max(kodekategori) kode from pp_kategori')->row_array()['kode']) + 1);
        return $this->db->update('pp_kategori',['kodekategori'=>$kode],['idkategori'=>$idkategori]);
    }

}