
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar">
              <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
              <a href="<?php echo base_url('cadministrasi/add_hakakseshalaman'); ?>" class="btn btn-default btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Halaman</th>
                  <th>Peran</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA HAK AKSES HALAMAN -->
                 <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idhakakseshalaman));
                    $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('login_hakakseshalaman'));
                    $idhalaman = $this->encryptbap->encrypt_urlsafe(V_HAKAKSESHALAMAN, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namahalaman.' ['.$obj->idhalaman.']</td>
                            <td>'.text_from_array(",", $obj->peranperan, $list_peran).'</td>
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Akses Halaman" class="btn btn-warning btn-xs" href="'.base_url('cadministrasi/edit_hakakseshalaman/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Akses Halaman" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                               <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA HALAMAN -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT HAKAKSESHALAMAN-->
           <?php 
              echo form_open('cadministrasi/save_hakakseshalaman', 'class="form-horizontal" id="FormHakakseshalaman"');
              //---Load Data untuk mode edit
              $options_edit='';
              $checked='';
              if(!empty($data_edit))
              {
                $options_edit=$data_edit['idhalaman'];
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                echo form_hidden('idhakakseshalaman', $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idhakakseshalaman'])));
                $checked =  explode(',', $data_edit['peranperan']);
              }
              // SET DATA OPTION
              $options=[];
              if(!empty($data_list))
              {
                $options=[];
                foreach ($data_list as $obj) 
                {
                  $options += [$obj->idhalaman=> $obj->namahalaman];
                }
              } 
              styleformgrup1kolom('Halaman', form_dropdown(['name'=>'idhalaman','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'idhalaman'],$options,$options_edit));
              styleformgrup1kolom("Peran", form_checkbox_bap([ 'name'  => 'peranperan[]', 'class'=> 'flat-red' ], $data_peran, "idperan", "namaperan", $checked));
              
              
              echo '<center style="padding-top: 8px">
                    <div class="row">
                      <a class="btn btn-primary btn-lg" onclick="simpan_hakakseshalaman()">SAVE</a>
                      <a class="btn btn-danger btn-lg" href="'.base_url('cadministrasi/hakakseshalaman').'">BACK</a>
                    </div>
                    </center>';
              echo form_close(); 
              ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->