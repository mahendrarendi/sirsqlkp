<style type="text/css">
   .signature-pad {
      border: 1px solid #ccc;
      border-radius: 5px;
      width: 100%;
      height: 260px;
   }
</style>
<section class="content">
   <div class="row">
      <div class="col-xs-12">
         <?php if ($mode == 'view') { ?>
            <div class="box">
               <div class="box-body">
                  <div class="col-md-6 row">
                     <a href="<?php echo base_url('cadministrasi/uploadsignature'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
                     <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                  </div>
                  <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                     <thead>
                        <tr class="header-table-ql">
                           <th style="width:25px;">No</th>
                           <th style="width:50px">Nama Dokter</th>
                           <th style="width:45px">Tanda Tangan</th>
                           <th style="width:50px;">Aksi</th>
                        </tr>
                     </thead>
                     <?php $nomor = 1; ?>
                     <?php foreach ($data_list as $row) { ?>
                        <tr>
                           <td><?php echo $nomor++ ?></td>
                           <td><?php echo $row['namalengkap'] ?></td>
                           <td><img class="img-responsive" src="<?= URLNASSIMRS ?>/tanda_tangan/<?php echo $row['signature'] ?>" style="width:50%;"></td>
                           <td>
                              <a data-toggle="tooltip" title="" data-original-title="Edit Signature" class="btn btn-warning btn-xs" href="<?php base_url() ?>edittandatangan/<?= $row['id_signature'] ?>"><i class="fa fa-pencil"></i> Edit</a>
                              <a data-toggle="tooltip" title="" data-original-title="Hapus Signature" class="btn btn-danger btn-xs" href="<?php base_url() ?>hapustandatangan/<?= $row['id_signature'] ?>"><i class="fa fa-eraser"></i> Hapus</a>
                           </td>
                        </tr>
                     <?php } ?>
                     <tbody>
                     </tbody>
                  </table>
               </div>
            </div>
         <?php } else if ($mode == 'upload') { ?>
            <div class="box">
               <!-- .box-header -->
               <div class="box-header">
                  <center>
                     <br>
                     <h2 class="box-title"><?php echo strtoupper($title_page); ?></h2>
                  </center>
                  <br>
               </div>
               <!-- box body -->
               <div class="box-body">
                  <div class="col-md-3">
                     <!-- <h1 >CAcacak</h1> -->
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label for="">Nama Dokter</label>
                        <select name="namadokter" id="namadokter" class="form-control ql-select2">
                           <option value="">-- PILIH DOKTER --</option>
                           <?php foreach ((array)$data_list as $row) { ?>
                              <option value="<?= $row['idperson'] ?>"><?= $row['namalengkap'] ?></option>
                           <?php } ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="">Gambar Tanda Tangan</label>
                        <div class="text-right">
                           <button type="button" class="btn btn-default btn-sm" id="undo"><i class="fa fa-undo"></i> Kembalikan</button>
                           <button type="button" class="btn btn-danger btn-sm" id="clear"><i class="fa fa-eraser"></i> Hapus</button>
                        </div>
                        <br>
                        <form method="POST" action="<?= base_url('cadministrasi/simpantandatangan') ?>">
                           <div class="wrapper">
                              <canvas id="signature-pad" class="signature-pad"></canvas>
                           </div>
                           <br>
                           <!-- Modal -->
                           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">Preview Tanda Tangan</h4>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                       <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Submit</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
               <center style="padding-top: 8px">
                  <div class="row">
                     <button class="btn btn-primary btn-lg" id="save-png"><i class="fa fa-floppy-o"></i>
                        SAVE</button>
                     <a class="btn btn-danger btn-lg" href="<?= base_url('cadministrasi/tandatangandokter'); ?>"><i class="fa fa-chevron-left"></i>&nbsp;BACK</a>
                  </div>
               </center>
            </div>
         <?php } else if ($mode == 'edit') { ?>
            <style>
               .btnpagedetail {
                  width: 17%;
               }
            </style>
            <div class="box">
               <div class="box-header">
                  <center>
                     <br>
                     <h2 class="box-title"><?php echo strtoupper($title_page); ?></h2>
                  </center>
                  <br>
               </div>
               <div class="box-body">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <?php foreach ($data_list as $row) { ?>
                           <label>Nama Dokter : <?= $row['namalengkap'] ?> </label>
                           <input type="hidden" value="<?php echo $row['id_signature'] ?>" name="idsignature">
                     </div>
                     <div class="form-group">
                        <label for="">Tanda Tangan Terkini</label>
                        <a href="<?php URLNASSIMRS ?>/tanda_tangan/<?php echo $row['signature'] ?>"><img class="img-responsive center-block" src="<?= URLNASSIMRS ?>/tanda_tangan/<?php echo $row['signature'] ?>" style="width:50%;"></a>
                     </div>
                     <div class="form-group">
                        <label for="">Ganti Tanda Tangan</label>
                        <div class="text-right">
                           <button type="button" class="btn btn-default btn-sm" id="undo"><i class="fa fa-undo"></i> Kembalikan</button>
                           <button type="button" class="btn btn-danger btn-sm" id="clear"><i class="fa fa-eraser"></i> Hapus</button>
                        </div>
                        <form method="POST" action="<?= base_url('cadministrasi/storeedittandatangan') ?>">
                           <div class="wrapper">
                              <canvas id="signature-pad" class="signature-pad"></canvas>
                           </div>
                           <br>
                           <!-- <button type="button" class="btn btn-primary btn-sm" id="save-png">Save as PNG</button> -->
                           <!-- <button type="button" class="btn btn-info btn-sm" id="save-jpeg">Save as JPEG</button>
                           <button type="button" class="btn btn-default btn-sm" id="save-svg">Save as SVG</button> -->
                           <!-- Modal -->
                           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       <h4 class="modal-title" id="myModalLabel">Preview Tanda Tangan</h4>
                                    </div>
                                    <div class="modal-body">
                                    </div>
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                       <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Submit</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            <?php } ?>
            <center style="padding-top: 8px">
               <div class="row">
                  <button class="btn btn-primary btn-lg" id="btnupdatepng"><i class="fa fa-floppy-o"></i>
                     SAVE</button>
                  <a class="btn btn-danger btn-lg" href="<?= base_url('cadministrasi/tandatangandokter'); ?>">BACK</a>
               </div>
            </center>
            </div>
         <?php } ?>
      </div>
   </div>
</section>