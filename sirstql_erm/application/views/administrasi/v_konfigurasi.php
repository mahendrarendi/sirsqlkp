<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <!-- mode view -->
       <?php if( $mode=='view'){ ?>
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
           <div class="col-md-6 row">
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
           <table id="konfigurasi" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
             <tr style="background:#edd38c;">
               <th style="width:60px;">No</th>
               <th>Nama</th>
               <th>Nilai</th>
               <th>Keterangan</th>
               <th></th>
             </tr>
             </thead>
             <tbody>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
       <!-- start mode add or edit -->
       <?php }?>

   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</section>
<!-- /.content -->