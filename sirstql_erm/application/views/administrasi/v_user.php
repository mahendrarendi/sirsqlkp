 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div class="box-body">
              <div class="col-md-6 row">
                  <a href="<?= base_url('cadministrasi/add_user')?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
                  <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
              </div>
            <table id="login_user" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>Username</th>
                <th>Password</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <!-- tampil dari controller loaddataicd yang diakses dari javascript datatable/js_administrasi_user -->
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- /mode view -->
        
        <!-- mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
            <div class="box-header">
                <center>
                  <br>
                <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
                </center>
                <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="<?= base_url('cadministrasi/save_user'); ?>" class="form-horizontal" id="FormUser" method="post" accept-charset="utf-8">
                    <input type="hidden" name="iduser" value="">
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Username <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="username" value="" class="form-control" id="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Password <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="password" value="" class="form-control" id="password">
                        </div>
                    </div>
                    <center style="padding-top: 8px">
                        <div class="row">
                            <a class="btn btn-primary btn-lg" onclick="simpan_user()">SAVE</a>
                            <a class="btn btn-danger btn-lg" href="<?= base_url('cadministrasi/user');?>">BACK</a>
                        </div>
                    </center>
                </form>
            </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->