<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
         
       <!-- mode view -->
       <?php if( $mode=='view'){ ?>
       <div class="box">
         <div class="box-body">
           <div class="col-md-6 row">
           <a href="<?php echo base_url('cadministrasi/add_halaman'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
           <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
         </div>
           <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
                 <tr class="header-table-ql">
               <th style="width:60px;">No</th>
               <th>Halaman</th>
               <th>Hak Akses</th>
               <th style="width:170px;">Aksi</th>
             </tr>
             </thead>
             <tbody>
             <?php
             if (!empty($data_list))
             {
               $no=0;
               foreach ($data_list as $obj) 
               {
                 $this->encryptbap->generatekey_once("HIDDENTABEL");
                 $id         =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idhalaman));
                 $tabel      = $this->encryptbap->encrypt_urlsafe(json_encode('login_halaman'));
                 $idhalaman  = $this->encryptbap->encrypt_urlsafe(V_HALAMAN, "json");
                 echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                 echo '  <td>'.$no.'</td>
                         <td>'.$obj->namahalaman.' ['.$obj->idhalaman.']</td>
                         <td>'.$obj->peranperan.'</td>
                         <td>
                            <a data-toggle="tooltip" title="" data-original-title="Edit Halaman" class="btn btn-warning btn-xs" href="'.base_url('cadministrasi/edit_halaman/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                            <a data-toggle="tooltip" title="" data-original-title="Delete Halaman" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                            <i class="fa fa-trash"></i> Delete</a></td>
                       </tr>';
               }
             }
             ?>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
       
       <!-- start mode add or edit -->
       <?php }else if( $mode=='edit' || $mode=='add') { ?>
       
       
       <div class="box">
        <!-- .box-header -->
        <div class="box-header">
        <center>
            <br>
            <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
           </center>
           <br>
        </div>           
       <!-- /.box-header -->
       
       <div class="box-body">
           <!--form-->
           <form action="<?= base_url('cadministrasi/save_halaman') ?>" class="form-horizontal" id="FormHalaman" method="post" accept-charset="utf-8">
               <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
               <input type="hidden" name="idhalaman" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe($data_edit['idhalaman'],'json') ); ?>">
               
                <div class="form-group">
                    <label for="namahalaman" class="col-sm-3 control-label"> Halaman <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="namahalaman" value="<?= ((empty($data_edit)) ? '' : $data_edit['namahalaman'] ); ?>" class="form-control" id="namahalaman">
                    </div>
                </div>
               
                <div class="form-group">
                    <label for="_name_txt" class="col-sm-3 control-label"> Akses Peran <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <?php
                        
                            $peranperan =  empty(!$data_edit) ? explode(',', $data_edit['peranperan']) : array();
                            $no=0;
                            foreach ($data_peran as $row)
                            {  
                                echo '<input type="hidden" name="jumlah[]" value="jml">';
                                echo '<input type="hidden" name="peransebelum'.$no.'" value="'. ((in_array($row['idperan'], $peranperan)) ? $row['idperan'] : 0) .'">';
                                echo '<input type="checkbox" name="peranperan'.$no.'" value="'.$row['idperan'].'" class="flat-red" '. ((in_array($row['idperan'], $peranperan)) ? 'checked' : '')   .' > '.$row['namaperan'].'<br/>';
                                $no++;

                            }
                        ?>
                    </div>
                </div>
               
                <center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_halaman()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="<?= base_url('cadministrasi/halaman'); ?>">BACK</a>
                  </div>
                </center>
               
           </form>
           <!--/form-->
           
       </div>
       <!-- /.box-body -->
     </div>

     <?php } ?>
   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</section>
<!-- /.content -->