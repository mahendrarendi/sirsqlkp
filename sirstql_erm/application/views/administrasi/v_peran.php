
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row col-md-6">
              <a href="<?php echo base_url('cadministrasi/add_peran'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
              <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Peran</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idperan, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('login_peran', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_PERAN, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namaperan.'</td>
                            <td>
                                <a data-toggle="tooltip" title="" data-original-title="Edit Peran" class="btn btn-warning btn-xs" href="'.base_url('cadministrasi/edit_peran/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                          </tr>';
                  }
                }
                ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /mode view -->
          
          <!-- mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
              <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="<?= base_url('cadministrasi/save_peran'); ?>" class="form-horizontal" id="FormPeran" method="post" accept-charset="utf-8">
                  <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                  <input type="hidden" name="idperan" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe($data_edit['idperan'],'json') ); ?>">
                  <div class="form-group">
                      <label for="_name_txt" class="col-sm-3 control-label"> peran <span class="asterisk">*</span></label>
                      <div class="col-sm-6">
                          <input type="text" name="namaperan" value="<?= ((empty($data_edit)) ? '' : $data_edit['namaperan']); ?>" class="form-control" id="namaperan">
                      </div>
                  </div>
                  <center style="padding-top: 8px">
                      <div class="row">
                          <a class="btn btn-primary btn-lg" onclick="simpan_peran()">SAVE</a>
                          <a class="btn btn-danger btn-lg" href="<?= base_url('cadministrasi/peran'); ?>">BACK</a>
                      </div>
                  </center>
              </form>
            </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->