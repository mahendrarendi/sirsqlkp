
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            <div class="box-body">
              <table id="login_hakaksesuser" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Username</th>
                  <th>Peran</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>                
                <!-- END TAMPIL DATA USER -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
              <!-- FORM ADD/EDIT USER-->
              <?php 
              echo form_open('cadministrasi/save_akses_user', 'class="form-horizontal" id="FormUser"');
              //---Load Data untuk mode edit
              $options_edit='';
              $checked='';
              $checkedu='';
              $checkeds='';
              if(!empty($data_edit))
              {
                $options_edit=$data_edit['iduser'];
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                echo form_hidden('idhakaksesuser', $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idhakaksesuser'])));
                $checked =  explode(',', $data_edit['peranperan']);
                $checkedu =  explode(',', $data_edit['unituser']);
                $checkeds =  explode(',', $data_edit['stasiun']);
              }
              // SET DATA OPTION
              $options=[];
              if(!empty($data_list))
              {
                $options=[];
                foreach ($data_list as $obj) {$options += [$obj->iduser=> $obj->namauser];}
              } 
              styleformgrup1kolom('Username', form_dropdown(['name'=>'iduser','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'username'],$options,$options_edit));
              styleformgrup1kolom("Peran", form_checkbox_bap([ 'name'  => 'peranperan[]', 'id' => 'akses', 'class'=> 'flat-red' ], $data_akses, "idperan", "namaperan", $checked));
              styleformgrup1kolom("Unit Farmasi", form_checkbox_bap([ 'name'  => 'unituser[]', 'id' => 'unit', 'class'=> 'flat-red' ], $data_unit, "idunit", "namaunit", $checkedu));
              styleformgrup1kolom("Stasiun", form_checkbox_bap([ 'name'  => 'stasiun[]', 'id' => 'stasiun', 'class'=> 'flat-red' ], $data_stasiun, "idstasiun", "namastasiun", $checkeds));
              
              echo '<center style="padding-top: 8px">
                    <div class="row">
                      <a class="btn btn-primary btn-lg" onclick="simpan_akses_user()">SAVE</a>
                      <a class="btn btn-danger btn-lg" href="'.base_url('cadministrasi/akses_user').'">BACK</a>
                    </div>
                    </center>';
              echo form_close(); 
              ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->