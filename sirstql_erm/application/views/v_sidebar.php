<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!--dashboard-->  
        <li class=" <?=$active_menu=='beranda' ? 'active':''; ?>">
            <a href="<?php echo base_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> <span>Beranda</span></a>
        </li>
        
        <!-- START MENU BPJS -->
        <?php if($this->pageaccessrightbap->checkAccessRight(V_BPJS)){ ?>
        <li class="treeview <?=$active_menu=='bpjs' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-link "></i> <span>Bridging BPJS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_BPJS)){ ?>  

                <!--menu bridging vclaim-->
                <li class="treeview active">
                    <a href="#"><i class="fa fa-circle-o"></i>Vclaim
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu ">
                        <li class="<?=$active_sub_menu=='bpjs_monitoring' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/monitoring'); ?>"><i class="fa fa-circle-o"></i> Monitoring </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_peserta' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/peserta'); ?>"><i class="fa fa-circle-o"></i> Cari Peserta </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_sep' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/sep'); ?>"><i class="fa fa-circle-o"></i> SEP </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_rencanakontrol' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/rencanakontrol'); ?>"><i class="fa fa-circle-o"></i> Rencana Kontrol / Inap </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_rujukan' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/rujukan'); ?>"><i class="fa fa-circle-o"></i> Rujukan </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_prb' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/rujukbalik'); ?>"><i class="fa fa-circle-o"></i> Rujuk Balik (PRB) </a></li>
                        <li class="<?=$active_sub_menu=='bpjs_ref' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/referensi'); ?>"><i class="fa fa-circle-o"></i> Referensi </a></li>
                    </ul>
                </li>
                
                <!--menu bridging antrian online-->
                <li class="treeview active">
                    <a href="#"><i class="fa fa-circle-o"></i>Antrian Online
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu ">
                        <li class="<?=$active_sub_menu=='antrol_dashboard' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/antrol_dashboard?tabActive=dashboardPerBulan'); ?>"><i class="fa fa-circle-o"></i> Dashboard </a></li>
                        <li class="<?=$active_sub_menu=='antrol_jadwaldokter' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/antrol_jadwal_dokter?tabActive=referensi_jadwal_dokter'); ?>"><i class="fa fa-circle-o"></i> Jadwal Dokter </a></li>
                        <li class="<?=$active_sub_menu=='antrol_referensi' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/antrol_referensi?tabActive=referensi_poli'); ?>"><i class="fa fa-circle-o"></i> Referensi </a></li>
                        <li class="<?=$active_sub_menu=='antrol_log' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/antrol_log?tabActive=log_taskId'); ?>"><i class="fa fa-circle-o"></i> Log </a></li>
                    </ul>
                </li>
            <?php } ?>
                
               <!--menu bridging applicare-->
                <?php if($this->pageaccessrightbap->checkAccessRight(V_APLICARE)){?>
                    <li class="<?=$active_menu_level=='aplicare' ? 'active':''; ?>"><a href="<?php echo base_url('cbpjs/aplicare'); ?>"><i class="fa fa-circle-o"></i>APLICARE </a></li>
                <?php } ?>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU BPJS -->

        <!-- START MENU SATUSEHAT -->
        <li class="treeview <?=$active_menu=='satusehat' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-link"></i> <span>Bridging Satusehat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
              <?php if($this->pageaccessrightbap->checkAccessRight(V_PASIEN)){ ?>
              <li class="<?=$active_sub_menu=='satusehat_pasien' ? 'active':''; ?>">
                  <a href="<?php echo base_url('csatusehat/find_patient'); ?>">
                  <i class="fa fa-circle-o"></i>Pencarian Data Pasien </a>
              </li>          
              <?php };if($this->pageaccessrightbap->checkAccessRight(V_NAKES)){?>
              <li class="<?=$active_sub_menu=='satusehat_nakes' ? 'active':''; ?>">
                  <a href="<?php echo base_url('csatusehat/find_practitioner'); ?>">
                  <i class="fa fa-circle-o"></i>Pencarian Data Nakes</a>
              </li>    
              <?php } ?>
            <!-- menu organization-->
              <?php if($this->pageaccessrightbap->checkAccessRight(V_ORGANISASI)){ ?>
              <li class="treeview active">
                <a href="#"><i class="fa fa-circle-o"></i>Struktur Organisasi
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class="<?=$active_menu_level=='find_organization' ? 'active':''; ?>"><a href="<?php echo base_url('csatusehat/find_organization'); ?>"><i class="fa fa-circle-o"></i> Cari Organisasi </a></li>
                  <li class="<?=$active_menu_level=='organization' ? 'active':''; ?>"><a href="<?php echo base_url('csatusehat/organization'); ?>"><i class="fa fa-circle-o"></i> List Organisasi</a></li>
                </ul>
              </li>
              <?php } ?>
            <!-- end menu organization-->

              <?php if($this->pageaccessrightbap->checkAccessRight(V_LOKASI_RUANG)){ ?>
              <li class="treeview active">
                    <a href="#"><i class="fa fa-circle-o"></i>Struktur Lokasi
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu ">
                        <li class="<?=$active_menu_level=='find_location' ? 'active':''; ?>"><a href="<?php echo base_url('csatusehat/find_location'); ?>"><i class="fa fa-circle-o"></i> Cari Lokasi </a></li>
                        <li class="<?=$active_menu_level=='location' ? 'active':''; ?>"><a href="<?php echo base_url('csatusehat/location'); ?>"><i class="fa fa-circle-o"></i> List Lokasi </a></li>
                    </ul>
                </li>
              <?php } ?>

          </ul>
        </li>
        <!-- END MENU SATUSEHAT -->
        
        <!-- START MENU ADMISSION -->
        <li class="treeview <?=$active_menu=='admission' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-get-pocket "></i> <span>Admisi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PENDAFTARANPOLIKLINIK)){?>
                <li class="<?=$active_sub_menu=='pendaftaran_poli' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/pendaftaran_poli'); ?>"><i class="fa fa-circle-o"></i> Pendaftaran Poliklinik </a></li>
            <?php } ?>
                
            <?php if($this->pageaccessrightbap->checkAccessRight(V_VERIFPASIENBPJS)){?>
                <li class="<?=$active_sub_menu=='verifikasiklaimjaminan' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/klaimverifikasijaminan'); ?>"><i class="fa fa-circle-o"></i> Klaim Verifikasi Jaminan</a></li>
                <li class="<?=$active_sub_menu=='klaimdatabulananbpjs' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/klaimdatabulananbpjs'); ?>"><i class="fa fa-circle-o"></i> Klaim Data Bulanan BPJS</a></li>
            <?php } ?>
                
            <?php if($this->pageaccessrightbap->checkAccessRight(V_ARUSREKAMMEDIS)){?>
                <li class="<?=$active_sub_menu=='arusrekammedis' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/arusrekammedis'); ?>"><i class="fa fa-circle-o"></i> Order RM</a></li>
            <?php } ?>
                
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PEGAWAI)){?><li class="<?=$active_sub_menu=='pegawai' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/pegawai'); ?>"><i class="fa fa-circle-o"></i> Pegawai </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)){?><li class="<?=$active_sub_menu=='singkronisasijadwal' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/singkronisasijadwal'); ?>"><i class="fa fa-circle-o"></i> Singkronisasi Jadwal </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_MASTERJADWAL)){?><li class="<?=$active_sub_menu=='masterjadwal' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/masterjadwal'); ?>"><i class="fa fa-circle-o"></i> Master Jadwal </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_INPUTJADWAL)){?><li class="<?=$active_sub_menu=='inputjadwal' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/inputjadwal'); ?>"><i class="fa fa-circle-o"></i> Input Jadwal </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_DATAINDUK)){?><li class="<?=$active_sub_menu=='datainduk' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/datainduk'); ?>"><i class="fa fa-circle-o"></i> Data Induk </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_BLACKLIST)){?><li class="<?=$active_sub_menu=='blacklist' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/blacklist'); ?>"><i class="fa fa-circle-o"></i> Blacklist Pasien </a></li><?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PERSONMEMBER)){?><li class="<?=$active_sub_menu=='personmember' ? 'active':''; ?>"><a href="<?php echo base_url('cadmission/personmember'); ?>"><i class="fa fa-circle-o"></i> Data Member </a></li><?php } ?>
          </ul>
        </li>
        <!-- END MENU ADMISSION -->
        
        <!-- START MENU PELAYANAN -->
        <li class="treeview <?=$active_menu=='pelayanan' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-medkit "></i> <span>Pelayanan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            
            <li class="">
                  <a class="<?=$active_sub_menu=='suratkelahiran' ? 'active':''; ?>" href="<?php echo base_url('cpelayanan/suratkelahiran'); ?>"><i class="fa fa-file-text-o"></i>Surat Kelahiran</a>
            </li>
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Rawat Jalan
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIANTRIAN)){?><li class="<?=$active_sub_menu=='administrasiantrian' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/administrasiantrian'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-users"></i> Administrasi Antrian</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANKLINIK)){?><li class="<?=$active_sub_menu=='pemeriksaanklinik' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/pemeriksaanklinik'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-user-md"></i> Pemeriksaan Klinik</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_PANELSKDP)){?><li class="<?=$active_sub_menu=='panelskdp' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/panelskdp'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-ticket"></i>SKDP </a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_DATAPASIENDIBLACKLIST)){?><li class="<?=$active_sub_menu=='pasiendiblacklist' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/pasiendiblacklist'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-list"></i> Data Pasien Diblacklist </a></li><?php } ?>
                  </ul>
            </li>
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Rawat Inap
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_LISBED)){?><li class="<?=$active_sub_menu=='lisbed' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayananranap/lisbed'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa fa-hospital-o"></i> Lis Bed</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIRANAP)){?><li class="<?=$active_sub_menu=='administrasiranap' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayananranap/administrasiranap'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-bed"></i> Administrasi Ranap</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANRANAP)){?><li class="<?=$active_sub_menu=='pemeriksaanranap' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayananranap/pemeriksaanranap'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-user-md"></i> Pemeriksaan Ranap</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_LABORATORIUMRANAP)){?><li class="<?=$active_sub_menu=='laboratoriumranap' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayananranap/laboratoriumranap'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-flask"></i> Laboratorium</a></li><?php } ?>
                  </ul>
            </li>
            
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Operasi
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_JADWALOPERASI)){?><li class="<?=$active_sub_menu=='operasi' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/jadwaloperasi'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-heartbeat"></i>Jadwal Operasi</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_REGISTERKAMARBEDAH)){?><li class="<?=$active_sub_menu=='registerkamarbedah' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/registerkamarbedah'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-th-list"></i>Register Kamar Bedah</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_BARANGOPERASI)){?><li class="<?=$active_sub_menu=='barangoperasi' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/barangoperasi'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-th-list"></i>Data Barang</a></li><?php } ?>
                  </ul>
            </li>
            
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Kasir
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_KASIROBATBEBAS)){?><li class="<?=$active_sub_menu=='kasirobatbebas' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/kasirpembelianbebas'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-money"></i> Kasir Farmasi Bebas</a></li><?php } ?>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_KASIR)){?><li class="<?=$active_sub_menu=='kasir' ? 'active':''; ?>"><a href="<?php echo base_url('cpelayanan/kasir'); ?>"><i class="fa fa-circle-o"></i> <i class="fa fa-money"></i> Kasir</a></li><?php } ?>
                  </ul>
            </li>
            
            
          </ul>
        </li>
        <!-- END MENU PELAYANAN -->

        <!-- START MENU ANTRIAN -->
        <?php if($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIPEMANGGILANANTRIAN) OR $this->pageaccessrightbap->checkAccessRight(V_PENGATURAN_VIDEO)){?>
        <li class="treeview <?=$active_menu=='antrian' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-ellipsis-h"></i> <span>Antrian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li><a href="<?php echo base_url('cantrian/antrian_verifdaftarperiksa'); ?>"><i class="fa fa-circle-o"></i>Verifikasi Pendaftaran</a></li>
            <li class="<?=$active_sub_menu=='ambilantrian' ? 'active':''; ?>"><a href="<?php echo base_url('cantrian/ambilantrian'); ?>"><i class="fa fa-circle-o"></i>Ambil Antrian</a></li>
            <li class="<?=$active_sub_menu=='ambilantrianpasien' ? 'active':''; ?>"><a href="<?php echo base_url('cantrian/ambilantrianpasien'); ?>"><i class="fa fa-circle-o"></i>Ambil Antrian Pasien <small class="label bg-yellow pull-right">u/ komputer antrian.</small></a></li>
            <li class="<?=$active_sub_menu=='viewantrian' ? 'active':''; ?>"><a href="<?php echo base_url('cantrian/viewantrian'); ?>"><i class="fa fa-circle-o"></i>Tampil Antrian</a></li>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_ADMINISTRASIPEMANGGILANANTRIAN)){?>
            <li class="<?=$active_sub_menu=='administrasipemanggilanantrian' ? 'active':''; ?>"><a href="<?php echo base_url('cantrian_1/administrasipemanggilanantrian'); ?>"><i class="fa fa-circle-o"></i>Pemanggilan Antrian </a></li>
            <?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PENGATURAN_VIDEO)){ ?>
            <li class="<?=$active_sub_menu=='pengaturan_video' ? 'active':''; ?>"><a href="<?php echo base_url('cantrian/pengaturan_video'); ?>"><i class="fa fa-circle-o"></i>Pengaturan Video</a></li>
            <?php } ?>
            
          </ul>
        </li>
        <!-- END MENU ANTRIAN -->
        <?php } ?>
        
        
        <!-- START MENU FARMASI -->
        <li class="treeview <?=$active_menu=='farmasi' ? 'active':''; ?>">
          <a href="#">
              <i class="fa fa-toggle-on"></i> <span> <?= ((empty(!$this->session->userdata('levelgudang')) && $this->session->userdata('levelgudang') =='gudang' ) ? 'Gudang' : '' ) ?> Farmasi </span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
                <?php if($this->pageaccessrightbap->checkAccessRight(V_BARANG)){?>
                    <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('cfarmasi/barang'); ?>"><i class="fa fa-circle-o"></i>Data Barang</a></li>
                <?php } ?>   
                    
                <?php if (empty(!$this->session->userdata('unitterpilih'))){ ?>
                    <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='datastokbarang' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/datastokbarang'); ?>"><i class="fa fa-circle-o"></i>Data Stok Barang</a></li>
                <?php } ?>
                    
                <?php if($this->pageaccessrightbap->checkAccessRight(V_TRANSFORMASIBARANG) and empty(!$this->session->userdata('levelgudang')) and $this->session->userdata('levelgudang')=='farmasi'){?>
                    <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='barangdatang' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/barangdatang'); ?>"><i class="fa fa-circle-o"></i>Barang Datang Farmasi</a></li>
                    <li class="<?=$active_sub_menu=='stokopname' ? 'active':''; ?>"><a href="<?php echo base_url('cfarmasi/stokopname'); ?>"><i class="fa fa-circle-o"></i>Stok Opname Farmasi</a></li>
                    <li class="<?=$active_sub_menu=='transformasibarang' ? 'active':''; ?>"><a href="<?php echo base_url('cfarmasi/transformasibarang'); ?>"><i class="fa fa-circle-o"></i>Transformasi Barang</a></li>
                    <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='pesankegudang' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/pesankegudang'); ?>"><i class="fa fa-circle-o"></i>Pesan Barang</a></li>
                    <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='permintaandepo' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/permintaandepo'); ?>"><i class="fa fa-circle-o"></i>Permintaan Barang Depo</a></li>
                    <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='permintaanreturdepo' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/permintaanreturdepo'); ?>"><i class="fa fa-circle-o"></i>Permintaan Retur Depo</a></li>
                <?php } ?>
                
                <?php if(empty(!$this->session->userdata('levelgudang')) and $this->session->userdata('levelgudang')=='gudang'){?>
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='barangdatang' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/barangdatang'); ?>"><i class="fa fa-circle-o"></i>Barang Datang Gudang</a></li>
                        <li class="<?=$active_sub_menu=='stokopname' ? 'active':''; ?>"><a href="<?php echo base_url('cfarmasi/stokopname'); ?>"><i class="fa fa-circle-o"></i>Stok Opname Gudang</a></li>
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='permintaanfarmasi' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/permintaanfarmasi'); ?>"><i class="fa fa-circle-o"></i>Permintaan Farmasi</a></li>
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='returkedistributor' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/returkedistributor'); ?>"><i class="fa fa-circle-o"></i>Retur</a></li>
                <?php } ?>
                        
                <?php if(empty(!$this->session->userdata('levelgudang')) and $this->session->userdata('levelgudang')=='depo'){?>
                        
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='barangdatang' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/barangdatang'); ?>"><i class="fa fa-circle-o"></i>Barang Datang Depo</a></li>
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='pesankefarmasi' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/pesankefarmasi'); ?>"><i class="fa fa-circle-o"></i>Pesan Barang</a></li>
                        <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='returkefarmasi' ? 'active':'' : ''; ?>"><a href="<?php echo base_url('cfarmasi/returkefarmasi'); ?>"><i class="fa fa-circle-o"></i>Retur Barang</a></li>
                <?php } ?>
                        
                <?php if(empty(!$this->session->userdata('levelgudang'))){ ?>
                   <li class="<?= isset($active_sub_menu) ? $active_sub_menu=='penggunaanbarang' ? 'active':'' : ''; ?>">
                       <a href="<?php echo base_url('cfarmasi/penggunaanbarang'); ?>"><i class="fa fa-circle-o"></i><?= (($this->session->userdata('levelgudang') == 'depo') ? 'Penggunaan Barang' : 'Penyesuaian Stok' ) ?></a>
                   </li>
                <?php } ?>
                        
                <?php if($this->pageaccessrightbap->checkAccessRight(V_SEDIAAN)){?>
                <li class="<?=$active_sub_menu=='sediaan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/sediaan'); ?>"><i class="fa fa-circle-o"></i>Sediaan</a></li>
                <?php } ?>

                <?php if($this->pageaccessrightbap->checkAccessRight(V_SATUAN)){?>
                <li class="<?=$active_sub_menu=='satuan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/satuan'); ?>"><i class="fa fa-circle-o"></i>Satuan</a></li>
                <?php } ?>

                <?php if($this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI)){?>
                <li class="<?=$active_sub_menu=='aturanpakai' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/aturanpakai'); ?>"><i class="fa fa-circle-o"></i>Aturan Pakai</a></li>
                <?php } ?>

                <?php if($this->pageaccessrightbap->checkAccessRight(V_DISTRIBUTOR)){?>
                <li class="<?=$active_sub_menu=='distributor' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/distributor'); ?>"><i class="fa fa-circle-o"></i>Distributor</a></li>
                <?php } ?>
                
                <?php if($this->pageaccessrightbap->checkAccessRight(V_PEDAGANGBESARFARMASI)){?>
                <li class="<?=$active_sub_menu=='pedagangbesarfarmasi' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/pedagangbesarfarmasi'); ?>"><i class="fa fa-circle-o"></i>PBF</a></li>
                <?php } ?>

                <?php if($this->pageaccessrightbap->checkAccessRight(V_BARANGPABRIK)){?>
                <li class="<?=$active_sub_menu=='barangpabrik' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/barangpabrik'); ?>"><i class="fa fa-circle-o"></i>Pabrik</a></li>
                <?php } ?>
          </ul>
        </li>
        <!-- END MENU FARMASI -->
        
        <?php if(($this->pageaccessrightbap->checkAccessRight(V_SDMKDASHBOARD))||($this->pageaccessrightbap->checkAccessRight(V_SDMKBERKAS))){ ?>
        <!-- START MENU SDMK -->
        <li class="treeview <?=$active_menu=='sdmk' ? 'active':''; ?>">
          <a href="#">
              <i class="fa fa-users"></i> <span> SDMK </span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
                <?php if($this->pageaccessrightbap->checkAccessRight(V_SDMKDASHBOARD)){ ?>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/dashboard'); ?>"><i class="fa fa-circle-o"></i>Dashboard</a></li>
                  <li class="<?=$active_sub_menu=='datapegawai' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/datapegawai'); ?>"><i class="fa fa-circle-o"></i>Data Pegawai</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/datafasyankes'); ?>"><i class="fa fa-circle-o"></i>Data Fasyankes</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/jadwalpegawai'); ?>"><i class="fa fa-circle-o"></i>Jadwal Pegawai</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/penilaian'); ?>"><i class="fa fa-circle-o"></i>Penilaian</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/pengajuancuti'); ?>"><i class="fa fa-circle-o"></i>Pengajuan Cuti</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/penggajian'); ?>"><i class="fa fa-circle-o"></i>Penggajian</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/pelatihan'); ?>"><i class="fa fa-circle-o"></i>Pelatihan</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/sip'); ?>"><i class="fa fa-circle-o"></i>SIP</a></li>
                  <li class="<?=$active_sub_menu=='barang' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/barang'); ?>"><i class="fa fa-circle-o"></i>Rating Karyawan</a></li>
                <?php } ?>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_SDMKBERKAS)){ ?>
                  <li class="<?=$active_sub_menu=='berkas' ? 'active':''; ?>"><a href="<?php echo base_url('csdmk/berkas'); ?>"><i class="fa fa-circle-o"></i>Berkas</a></li>
                <?php } ?>
          </ul>
        </li>
        <!-- END MENU SDMK -->
        <?php } ?>
        <!-- START MENU MASTER DATA -->
        <li class="treeview <?=$active_menu=='masterdata' ? 'active':''; ?>">
          <a href="#">
            <i class="fa  fa-cubes "></i> <span>Pusat Data</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu ">

            <!-- menu BPJS -->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_KLINIKPERUJUK) ){?>
            <li class="treeview <?=$active_sub_menu=='bpjs' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-home "></i> <span>Bridging</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                
                <?php if($this->pageaccessrightbap->checkAccessRight(V_KLINIKPERUJUK)){?><li class="<?=$active_menu_level=='klinikperujuk' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/klinikperujuk'); ?>"><i class="fa fa-circle-o"></i>KLINIK PERUJUK </a></li><?php } ?>
                
              </ul>
            </li>
            <?php } ?>
            <!-- end BPJS -->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_MANAGEMENASET)){ ?>
            <!-- Menu Inventaris Kantor -->
            <li class="treeview <?=$active_sub_menu=='inventariskantor' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Inventaris Kantor
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='inventaris' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/inventariskantor'); ?>"><i class="fa fa-circle"></i> Inventaris Medis</a></li>
                <li class="<?=$active_menu_level=='inventarisnonmedis' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/inventarisnonmedis'); ?>"><i class="fa fa-circle"></i> Inventaris Non Medis</a></li>
                <li class="<?=$active_menu_level=='iteminvalat' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/iteminvalat'); ?>"><i class="fa fa-circle"></i> List Item Alat</a></li>
                <li class="<?=$active_menu_level=='masterinvalat' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/masterinvalat'); ?>"><i class="fa fa-circle"></i> Master Alat</a></li>
                <li class="<?=$active_menu_level=='kategorialat' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kategorialat'); ?>"><i class="fa fa-circle"></i> Kategori Alat</a></li>
                <li class="<?=$active_menu_level=='ruanginvalat' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/ruanginvalat'); ?>"><i class="fa fa-circle"></i> Data Ruang</a></li>
                <li class="<?=$active_menu_level=='blokinvalat' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/blokinvalat'); ?>"><i class="fa fa-circle"></i> Data Blok</a></li>
                <li class="<?=$active_menu_level=='merekbarang' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/merekinvalat'); ?>"><i class="fa fa-circle"></i> Merek Alat</a></li>
                <li class="<?=$active_menu_level=='modelbarang' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/modelinvalat'); ?>"><i class="fa fa-circle"></i> Model/Tipe Alat</a></li>
              </ul>
            </li>
            <?php } ?>
            
            <!-- menu inventaris-->  
            <?php //if($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN) OR $this->pageaccessrightbap->checkAccessRight(V_ATURANPAKAI) OR $this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAANDETAIL) OR $this->pageaccessrightbap->checkAccessRight(V_BARANG) OR $this->pageaccessrightbap->checkAccessRight(V_SEDIAAN) OR $this->pageaccessrightbap->checkAccessRight(V_SATUAN) OR empty(!$this->session->userdata('unitterpilih')) ){ ?>
            
            <?php //} ?>
            <!-- end menu inventaris-->
            
            
            <!-- menu Ruang / room -->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_UNIT) OR $this->pageaccessrightbap->checkAccessRight(V_INSTALASI) OR $this->pageaccessrightbap->checkAccessRight(V_BANGSAL) OR $this->pageaccessrightbap->checkAccessRight(V_BED) OR $this->pageaccessrightbap->checkAccessRight(V_STASIUN) OR $this->pageaccessrightbap->checkAccessRight(V_KELAS) ){ ?>
            <li class="treeview <?=$active_sub_menu=='ruang' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Ruang
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='unit' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/unit'); ?>"><i class="fa fa-circle"></i> Unit</a></li>
                <li class="<?=$active_menu_level=='instalasi' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/instalasi'); ?>"><i class="fa fa-circle"></i> Instalasi</a></li>
                <li class="<?=$active_menu_level=='bangsal' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/bangsal'); ?>"><i class="fa fa-circle"></i> Bangsal</a></li>
                <li class="<?=$active_menu_level=='bed' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/bed'); ?>"><i class="fa fa-circle"></i> Bed</a></li>
                <li class="<?=$active_menu_level=='stasiun' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/stasiun'); ?>"><i class="fa fa-circle"></i> Stasiun</a></li>
                <li class="<?=$active_menu_level=='loket' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/loket'); ?>"><i class="fa fa-circle"></i> Loket</a></li>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_KELAS)){?><li class="<?=$active_menu_level=='kelas' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kelas'); ?>"><i class="fa fa-circle"></i> Kelas</a></li><?php } ?>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu Ruang / room -->
            <!-- menu Tarif-->
            <?php if(
                    $this->pageaccessrightbap->checkAccessRight(V_MASTERTARIF) OR 
                    $this->pageaccessrightbap->checkAccessRight(V_JENISTARIF) OR 
                    $this->pageaccessrightbap->checkAccessRight(V_MASTERTARIFPAKETPEMERIKSAAN) OR
                    $this->pageaccessrightbap->checkAccessRight(V_MASTERDISKON) OR
                    $this->pageaccessrightbap->checkAccessRight(V_MASTERMEMBER) OR 
                    $this->pageaccessrightbap->checkAccessRight(V_MASTERTARIFOPERASI)
             ){ ?>
            <li class="treeview <?=$active_sub_menu=='tarif' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Tarif
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='mastertarifralan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/mastertarifralan'); ?>"><i class="fa fa-circle"></i> Master Tarif Ralan</a></li>
                <li class="<?=$active_menu_level=='mastertarifranap' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/mastertarifranap'); ?>"><i class="fa fa-circle"></i> Master Tarif Ranap</a></li>
                <li class="<?=$active_menu_level=='masterdiskon' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/masterdiskon'); ?>"><i class="fa fa-circle"></i> Master Diskon Pasien</a></li>
                <li class="<?=$active_menu_level=='masterdiskonpegawai' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/masterdiskonpegawai'); ?>"><i class="fa fa-circle"></i> Master Diskon Pegawai</a></li>
                <li class="<?=$active_menu_level=='mastermember' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/mastermember'); ?>"><i class="fa fa-circle"></i> Master Member</a></li>
                <li class="<?=$active_menu_level=='mastertarifoperasi' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/mastertarifoperasi'); ?>"><i class="fa fa-circle"></i> Master Tarif Operasi</a></li>
                <li class="<?=$active_menu_level=='jenistarif' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/jenistarif'); ?>"><i class="fa fa-circle"></i> Jenis Tarif</a></li>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)){?>
                <li class="<?=$active_menu_level=='mastertarifpaketpemeriksaan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/mastertarifpaketpemeriksaan'); ?>"><i class="fa fa-circle"></i> Tarif Paket Pemeriksaan</a></li>
                <?php } ?>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu Tarif-->
            
            <!-- menu paket pemeriksaan-->
            
            <li class="treeview <?=$active_sub_menu=='paketpemeriksaan' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Paket Pemeriksaan
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($this->pageaccessrightbap->checkAccessRight(V_PAKETPEMERIKSAAN)){?>
                <li class="<?=$active_menu_level=='paketpemeriksaan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/paketpemeriksaan'); ?>"><i class="fa fa-circle"></i> Paket Pemeriksaan</a></li>
                <?php } ?>                
                <li class="<?=$active_menu_level=='paketpemeriksaanrule' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/paketpemeriksaanrule'); ?>"><i class="fa fa-circle"></i> Paket Pemeriksaan Rule</a></li>
                <li class="<?=$active_menu_level=='paketpemeriksaanrulepesan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/paketpemeriksaanrulepesan'); ?>"><i class="fa fa-circle"></i> Paket Pemeriksaan Rule Pesan</a></li>
                <li class="<?=$active_menu_level=='jeniscetakpaketpemeriksaan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/jeniscetakpaketpemeriksaan'); ?>"><i class="fa fa-circle"></i> Jenis Cetak Paket Pemeriksaan</a></li>
              </ul>
            </li>
            <!-- end menu paket pemeriksaan-->
            <!-- menu demografi-->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_GRUPPEGAWAI)){ ?>
            <li class="treeview <?=$active_sub_menu=='demografi' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Demografi
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='gruppegawai' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/gruppegawai'); ?>"><i class="fa fa-circle"></i> Grup Pegawai</a></li>
                <li class="<?=$active_menu_level=='pendidikan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/pendidikan'); ?>"><i class="fa fa-circle"></i> Pendidikan</a></li>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu demografi--> 
            <!-- menu wilayah -->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PROPINSI) 
                    OR $this->pageaccessrightbap->checkAccessRight(V_KABUPATEN) 
                    OR $this->pageaccessrightbap->checkAccessRight(V_KELURAHAN) 
                    OR $this->pageaccessrightbap->checkAccessRight(V_KECAMATAN)){ ?>
            <li class=" treeview <?=$active_sub_menu=='wilayah' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Wilayah
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='propinsi' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/propinsi'); ?>"><i class="fa fa-circle"></i> Propinsi</a></li>
                <li class="<?=$active_menu_level=='kabupaten' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kabupaten'); ?>"><i class="fa fa-circle"></i> Kabupaten</a></li>
                <li class="<?=$active_menu_level=='kecamatan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kecamatan'); ?>"><i class="fa fa-circle"></i> Kecamatan</a></li>
                <li class="<?=$active_menu_level=='kelurahan' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kelurahan'); ?>"><i class="fa fa-circle"></i> Kelurahan</a></li>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu wilayah -->

            <!-- menu ICD -->
            <?php if($this->pageaccessrightbap->checkAccessRight(V_ICD) OR $this->pageaccessrightbap->checkAccessRight(V_JENISICD) OR $this->pageaccessrightbap->checkAccessRight(V_JENISPENYAKIT)){ ?>
            <li class="treeview <?=$active_sub_menu=='icd' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>ICD
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='mastericd' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/icd'); ?>"><i class="fa fa-circle"></i> Master Icd</a></li>
                
                <li class="<?=$active_menu_level=='jenisicd' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/jenisicd'); ?>"><i class="fa fa-circle"></i> Jenis Icd</a></li>

                <li class="<?=$active_menu_level=='golongansebabpenyakit' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/golongansebabpenyakit'); ?>"><i class="fa fa-circle"></i> Golongan Sebab Penyakit</a></li>
                <li class="<?=$active_menu_level=='jenispenyakit' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/jenispenyakit'); ?>"><i class="fa fa-circle"></i> Jenis Penyakit STPRS</a></li>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu ICD -->

         
            <li class="treeview <?=$active_sub_menu=='diagnosakeperawatan' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Diagnosa Keperawatan
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='mastersdki' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/sdki'); ?>"><i class="fa fa-circle"></i>SDKI</a></li>
                <li class="<?=$active_menu_level=='masterslki' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/slki'); ?>"><i class="fa fa-circle"></i>SLKI</a></li>
                <li class="<?=$active_menu_level=='mastersiki' ? 'active' : ''; ?>"><a href="<?php echo base_url('cmasterdata/siki')?>"><i class="fa fa-circle"></i>SIKI</a></li>
              </ul>
            </li>
         
            

            <!-- menu perpustakaan-->
            <?php if($this->pageaccessrightbap->checkAccessRight(v_PERPUSTAKAAN)){ ?>
            <li class="treeview <?=$active_sub_menu=='perpustakaan' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-circle-o"></i>Perpustakaan
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="<?=$active_menu_level=='buku' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/buku'); ?>"><i class="fa fa-circle"></i> Buku</a></li>
                <li class="<?=$active_menu_level=='kategori' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/kategori'); ?>"><i class="fa fa-circle"></i> Kategori</a></li>
                <li class="<?=$active_menu_level=='penulis' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/penulis'); ?>"><i class="fa fa-circle"></i> Penulis</a></li>
                <li class="<?=$active_menu_level=='penerbit' ? 'active':''; ?>"><a href="<?php echo base_url('cmasterdata/penerbit'); ?>"><i class="fa fa-circle"></i> Penerbit</a></li>
              </ul>
            </li>
            <?php } ?>
            <!-- end menu perpustakaan-->
             
          </ul>
        </li>
        <!-- END MENU MASTER DATA -->

        <!-- START MENU LAPORAN -->
        <?php if(
                $this->pageaccessrightbap->checkAccessRight(V_LOG) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANFARMASI) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN) or
                $this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS) or
                $this->pageaccessrightbap->checkAccessRight(V_DEMOGRAFIKUNJUNGANPASIEN) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORAN_CAKUPANPERPOLI) or 
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLISPESIALIS) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLIPENUNJANG) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLIKHUSUS) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANKAMAROPERASI) or
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANRAWATINAP) or
                $this->pageaccessrightbap->checkAccessRight(V_WAKTUPENDAFTARAN)
                )
            {
        ?>
        <li class="treeview <?=$active_menu=='report' ? 'active':''; ?>">
          <a href="#"><i class="fa fa-bar-chart "></i> <span>Laporan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LOG)){?><li class="<?=$active_sub_menu=='log' ? 'active':''; ?>"><a href="<?php echo base_url('creport/log'); ?>"><i class="fa fa-circle-o"></i>Log User</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LOG)){?><li class="<?=$active_sub_menu=='logpemeriksaan' ? 'active':''; ?>"><a href="<?php echo base_url('creport/logpemeriksaan'); ?>"><i class="fa fa-circle-o"></i>Log Pemeriksaan</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_WAKTULAYANAN)){?><li class="<?=$active_sub_menu=='waktulayanan' ? 'active':''; ?>"><a href="<?php echo base_url('creport/waktulayanan'); ?>"><i class="fa fa-circle-o"></i>Waktu Layanan</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_WAKTUPENDAFTARAN)){?><li class="<?=$active_sub_menu=='waktupendaftaran' ? 'active':''; ?>"><a href="<?php echo base_url('creport/waktupendaftaran'); ?>"><i class="fa fa-circle-o"></i>Waktu Tunggu Pendaftaran</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANSTATUSPEMERIKSAANPASIENRALAN)){?><li class="<?=$active_sub_menu=='statuspemeriksaanpasienralan' ? 'active':''; ?>"><a href="<?php echo base_url('creport/statuspemeriksaanpasienralan'); ?>"><i class="fa fa-circle-o"></i>Status Periksa Pasien RALAN</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANFARMASI)){?><li class="<?=$active_sub_menu=='farmasi' ? 'active':''; ?>"><a href="<?php echo base_url('creport/laporanfarmasi'); ?>"><i class="fa  fa-circle-o"></i>Farmasi</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANKEUANGAN)){?><li class="<?=$active_sub_menu=='laporankeuangan' ? 'active':''; ?>"><a href="<?php echo base_url('creport/laporankeuangan'); ?>"><i class="fa  fa-circle-o"></i>Keuangan</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LPELAYANRS)){?><li class="<?=$active_sub_menu=='pelayananrs' ? 'active':''; ?>"><a href="<?php echo base_url('creport/pelayananrs'); ?>"><i class="fa  fa-circle-o"></i>Pelayanan Rumah Sakit</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_DEMOGRAFIKUNJUNGANPASIEN)){?><li class="<?=$active_sub_menu=='demografikunjunganpasien' ? 'active':''; ?>"><a href="<?php echo base_url('creport/demografikunjunganpasien'); ?>"><i class="fa  fa-circle-o"></i>Demografi Kunjungan Pasien</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORAN_CAKUPANPERPOLI)){?><li class="<?=$active_sub_menu=='cakupanperpoli' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupanperpoli'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Poli</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLISPESIALIS)){?><li class="<?=$active_sub_menu=='cakupanpolispesialis' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupanpolispesialis'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Poli Spesialis</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLIPENUNJANG)){?><li class="<?=$active_sub_menu=='cakupanpolipenunjang' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupanpolipenunjang'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Penunjang Medik</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANPOLIKHUSUS)){?><li class="<?=$active_sub_menu=='cakupanpolikhusus' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupanpolikhusus'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Poli Umum & UGD</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANKAMAROPERASI)){?><li class="<?=$active_sub_menu=='cakupankamaroperasi' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupankamaroperasi'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Kamar Operasi</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANKAMAROPERASI) or $this->pageaccessrightbap->checkAccessRight(V_LAPORANCAKUPANRAWATINAP)){?><li class="<?=$active_sub_menu=='cakupanrawatinap' ? 'active':''; ?>"><a href="<?php echo base_url('creport/cakupanrawatinap'); ?>"><i class="fa  fa-circle-o"></i>Cakupan Rawat Inap</a></li><?php } ?>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU LAPORAN -->
        
        <!-- START MENU KEUANGAN -->
        <?php if( $this->pageaccessrightbap->checkAccessRight(V_DEPARTEMEN) OR 
                $this->pageaccessrightbap->checkAccessRight(V_AKUN) OR 
                $this->pageaccessrightbap->checkAccessRight(V_JURNAL) OR 
                $this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN) OR
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANKAS) OR
                $this->pageaccessrightbap->checkAccessRight(V_LAPORANLABARUGI) OR 
                $this->pageaccessrightbap->checkAccessRight(V_REKAPTRANSAKSI) OR
                $this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN) OR
                $this->pageaccessrightbap->checkAccessRight(V_PENDAPATANKAS) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)){
        ?>
        <li class="treeview <?=$active_menu=='keuangan' ? 'active':''; ?>">
          <a href="#">
            <i class="fa  fa-book "></i> <span>Modul Keuangan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu ">
        
            <!-- menu KEUANGAN -->
            <?php if( $this->pageaccessrightbap->checkAccessRight(V_DEPARTEMEN) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_AKUN) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_JURNAL) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_LAPORANKAS) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_LAPORANLABARUGI) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_REKAPTRANSAKSI) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_PENDAPATANKAS) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN) OR 
                      $this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN) ){
            ?>
            <li class="treeview <?=$active_sub_menu=='jurnalumum' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-home "></i> <span>Jurnal Umum</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if($this->pageaccessrightbap->checkAccessRight(V_DEPARTEMEN)){?><li class="<?=$active_menu_level=='deptkeu' ? 'active':''; ?>"><a href="<?php echo base_url('ckeuangan/departemen'); ?>"><i class="fa fa-circle-o"></i>Departemen </a></li><?php } ?>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_AKUN)){?><li class="<?=$active_menu_level=='akunkeu' ? 'active':''; ?>"><a href="<?php echo base_url('ckeuangan/akun'); ?>"><i class="fa fa-circle-o"></i>Daftar Akun </a></li><?php } ?>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_AKUN)){?><li class="<?=$active_menu_level=='saldoawal' ? 'active':''; ?>"><a href="<?php echo base_url('ckeuangan/saldoawal'); ?>"><i class="fa fa-circle-o"></i>Saldo Awal Akun </a></li><?php } ?>
                <?php if($this->pageaccessrightbap->checkAccessRight(V_JURNAL)){?><li class="<?=$active_menu_level=='jurnalkeu' ? 'active':''; ?>"><a href="<?php echo base_url('ckeuangan/jurnal'); ?>"><i class="fa fa-circle-o"></i>Jurnal </a></li><?php } ?>

              </ul>
            </li>

            <li class="treeview <?=$active_sub_menu=='pemasukan' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-asterisk "></i> <span>Pemasukan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <?php if($this->pageaccessrightbap->checkAccessRight(V_REKAPTRANSAKSI)){?>
                <li class="<?=$active_menu_level=='rekaptransaksikeu' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/rekaptransaksi'); ?>"><i class="fa fa-circle-o"></i>Rekap Transaksi </a>
                </li>
              <?php } ?>
              <?php if($this->pageaccessrightbap->checkAccessRight(V_PENDAPATANKAS)){?>
                <li class="<?=$active_menu_level=='pendapatankas' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/pendapatankas'); ?>"><i class="fa fa-circle-o"></i>Pendapatan Kas </a>
                </li>
                
                <li class="<?=$active_menu_level=='jenispendapatankas' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/jenispendapatankas'); ?>"><i class="fa fa-circle-o"></i>Jenis Pendapatan Kas </a>
                </li>
              <?php } ?>
              </ul>
            </li>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PENGELUARAN)){?>
            <li class="<?=$active_sub_menu=='pengeluaran' ? 'active':''; ?>">
              <a href="<?php echo base_url('ckeuangan/pengeluaran'); ?>"><i class="fa fa-asterisk "></i> <span>Pengeluaran</span></a>              
            </li>
            <?php } ?>
            
            <?php if($this->pageaccessrightbap->checkAccessRight(V_CASHEQUIVALEN)){?>
            <li class="<?=$active_sub_menu=='cash_equivalen' ? 'active':''; ?>">
              <a href="<?php echo base_url('ckeuangan/cash_equivalen'); ?>"><i class="fa fa-asterisk "></i> <span>Cash Equivalen</span></a>
            </li> 
            <?php } ?>
            
            <li class="treeview <?=$active_sub_menu=='laporan' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-book "></i> <span>Laporan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <?php if($this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN)){?>
                <li class="<?=$active_menu_level=='historyakunkeu' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/historyakun'); ?>"><i class="fa fa-circle-o"></i>History Akun </a>
                </li>
              <?php } ?>
              <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANKAS)){?>
                <li class="<?=$active_menu_level=='laporankaskeu' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/laporankas'); ?>"><i class="fa fa-circle-o"></i>Laporan Kas </a>
                </li>
              <?php } ?>
              <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANLABARUGI)){?>
                <li class="<?=$active_menu_level=='laporanlabarugi' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/laporanlabarugi'); ?>"><i class="fa fa-circle-o"></i>Laporan Laba Rugi </a>
                </li>
              <?php } ?>
              <?php if($this->pageaccessrightbap->checkAccessRight(V_LAPORANLABARUGI)){?>
                <li class="<?=$active_menu_level=='laporanneraca' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/laporanneraca'); ?>"><i class="fa fa-circle-o"></i>Laporan Neraca </a>
                </li>
              <?php } ?>
              </ul>
            </li>
            
            <li class="treeview <?=$active_sub_menu=='konfigurasi' ? 'active':''; ?>">
              <a href="#"><i class="fa fa-gear "></i> <span>Konfigurasi</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              <?php if($this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN)){?>
                <li class="<?=$active_menu_level=='coakaskasir' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/periode'); ?>"><i class="fa fa-circle-o"></i>Periode Laporan </a>
                </li>
              <?php } ?>
                
              <?php if($this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN)){?>
                <li class="<?=$active_menu_level=='coakaskasir' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/coakaskasir'); ?>"><i class="fa fa-circle-o"></i>COA Kas Kasir </a>
                </li>
              <?php } ?>
                
               <?php if($this->pageaccessrightbap->checkAccessRight(V_HISTORYAKUN)){?>
                <li class="<?=$active_menu_level=='coakasbank' ? 'active':''; ?>">
                  <a href="<?php echo base_url('ckeuangan/coakasbank'); ?>"><i class="fa fa-circle-o"></i>COA Kas Bank </a>
                </li>
              <?php } ?>
              </ul>
            </li>
            
            <?php } ?>
            <!-- end konfigurasi coa -->
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU KEUANGAN -->
        
        
        <!--START TAGIHAN-->
        <?php if( $this->pageaccessrightbap->checkAccessRight(V_HUTANGFARMASI) or
                  $this->pageaccessrightbap->checkAccessRight(V_HUTANGUSAHA) or
                  $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASUSAHA) or
                  $this->pageaccessrightbap->checkAccessRight(V_FAKTURLUNASFARMASI)
        ){ ?>
        <li class="treeview <?=$active_menu=='tagihan' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-indent"></i> <span>Tagihan</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
          </a>
          <ul class="treeview-menu ">
            <li class="<?= $active_sub_menu=='fakturlunasfarmasi' ? 'active':'' ; ?>"><a href="<?php echo base_url('ctagihan/fakturlunasfarmasi'); ?>"><i class="fa fa-circle-o"></i>Faktur Lunas Farmasi</a></li>
            <li class="<?= $active_sub_menu=='fakturlunasusaha' ? 'active':'' ; ?>"><a href="<?php echo base_url('ctagihan/fakturlunasusaha'); ?>"><i class="fa fa-circle-o"></i>Faktur Lunas Usaha</a></li>   
            <li class="<?= $active_sub_menu=='hutangfarmasi' ? 'active':'' ; ?>"><a href="<?php echo base_url('ctagihan/hutangfarmasi'); ?>"><i class="fa fa-circle-o"></i>Hutang Farmasi</a></li>
            <li class="<?= $active_sub_menu=='hutangusaha' ? 'active':'' ; ?>"><a href="<?php echo base_url('ctagihan/hutangusaha'); ?>"><i class="fa fa-circle-o"></i>Hutang Usaha</a></li>   
          </ul>
        </li>
        <?php } ?>
        <!--END TAGIHAN-->
        
        
        <!-- START MENU ANJUNGAN PASIEN -->
        <?php if($this->pageaccessrightbap->checkAccessRight(V_ANJUNGANPASIEN)){?>
        <li class="treeview <?=$active_menu=='antrian' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-tv"></i> <span>Anjungan Pasien</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li class="<?=$active_sub_menu=='daftarantrian' ? 'active':''; ?>"><a target="_blank" href="<?php echo base_url('Canjunganpasien'); ?>"><i class="fa fa-circle-o"></i>Daftar Antrian</a></li>
            <li class="<?=$active_sub_menu=='pendaftaranonline' ? 'active':''; ?>"><a target="_blank" href="http://siti.ql/apm"><i class="fa fa-circle-o"></i>Pendaftaran Online </a></li>
            <li class="<?=$active_sub_menu=='register_pasien' ? 'active':''; ?>"><a target="_blank" href="<?php echo base_url('Canjunganpasien/register_pasien'); ?>"><i class="fa fa-circle-o"></i>Pendaftaran Anjungan </a></li>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU ANJUNGAN PASIEN -->

        <!-- START MENU MUTU INDIKATOR -->
        <?php if($this->pageaccessrightbap->checkAccessRight(V_INDIKATORMUTU)){?>
          <li class="treeview <?=$active_menu=='indikatormutu' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-trophy "></i> <span>MUTU</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Indikator Mutu
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <li class="<?=$active_sub_menu=='imutrs' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu'); ?>"><i class="fa fa-circle-o"></i>IMUT RS</a></li>
                    <li class="<?=$active_sub_menu=='rekapimutrs' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/listrekapimutrs'); ?>"><i class="fa fa-circle-o"></i>Rekap IMUT RS </a></li>
                    <?php if( is_superuser() ): ?>  
                      <li class="<?=$active_sub_menu=='daftarimutrs' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/listdaftarimutrs'); ?>"><i class="fa fa-circle-o"></i>Daftar IMUT RS </a></li>
                      <li class="<?=$active_sub_menu=='pengaturanimut' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/pengaturan'); ?>"><i class="fa fa-circle-o"></i>Pengaturan IMUT</a></li>
                      <li class="<?=$active_sub_menu=='lembarpdsa' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/lembarpdsa'); ?>"><i class="fa fa-circle-o"></i>Lembar Kerja PDSA</a></li>
                    <?php endif; ?>
                  </ul>
            </li>
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Surveilans IKP
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <li class="<?=$active_sub_menu=='ikprs' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/ikprs'); ?>"><i class="fa fa-circle-o"></i>IKP RS</a></li>
                    <li class="<?=$active_sub_menu=='rekapikp' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/rekapikp'); ?>"><i class="fa fa-circle-o"></i>Rekap IKP </a></li>
                    <li class="<?=$active_sub_menu=='pelaporanikp' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/pelaporanikp'); ?>"><i class="fa fa-circle-o"></i>Form Pelaporan IKP</a></li>
                    <?php if( is_superuser() ): ?>  
                      <li class="<?=$active_sub_menu=='daftarikp' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/daftarikp'); ?>"><i class="fa fa-circle-o"></i>Daftar IKP</a></li>
                      <li class="<?=$active_sub_menu=='pengaturanikp' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/pengaturanikp'); ?>"><i class="fa fa-circle-o"></i>Pengaturan IKP</a></li>
                    <?php endif; ?>
                  </ul>
            </li>
            <li class="treeview active">
                  <a href="#"><i class="fa fa-circle-o"></i>Surveilans PPI
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu ">
                    <li class="<?=$active_sub_menu=='ppirs' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/ppirs'); ?>"><i class="fa fa-circle-o"></i>PPI RS</a></li>
                    <li class="<?=$active_sub_menu=='rekapppi' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/rekapppi'); ?>"><i class="fa fa-circle-o"></i>Rekap PPI </a></li>
                    <?php if( is_superuser() ): ?>  
                      <li class="<?=$active_sub_menu=='daftarppi' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/daftarppi'); ?>"><i class="fa fa-circle-o"></i>Daftar PPI</a></li>
                      <li class="<?=$active_sub_menu=='pengaturanppi' ? 'active':''; ?>"><a  href="<?php echo base_url('Cindikatormutu/pengaturanppi'); ?>"><i class="fa fa-circle-o"></i>Pengaturan PPI</a></li>
                    <?php endif; ?>
                  </ul>
            </li>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU MUTU INDIKATOR -->
        
         <!-- START MENU AKREDITASI -->
         <?php 
            if( $this->pageaccessrightbap->checkAccessRight(V_AKREDITASI)) {       
        ?>
        <li class="treeview <?=$active_menu=='akreditasi' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Akreditasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_DOKUMENAKREDITASI)){?>
              <?php  if( $this->pageaccessrightbap->ql_check_username( $this->pageaccessrightbap->ql_pic_akreditasi() ) ) : ?>
                <li class="<?=$active_sub_menu=='berandaakreditasi' ? 'active':''; ?> <?= $active_sub_menu; ?>"><a href="<?php echo base_url('cakreditasi/berandaakreditasi'); ?>"><i class="fa fa-circle-o"></i>Beranda </a></li>
              <?php endif; ?>
            <?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_DOKUMENAKREDITASI)){?><li class="<?=$active_sub_menu=='dokumenakreditasi' ? 'active':''; ?>"><a href="<?php echo base_url('cakreditasi/dokumenakreditasi'); ?>"><i class="fa fa-circle-o"></i>Dokumen </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_DOKUMENUNIT)){?><li class="<?=$active_sub_menu=='dokumenunit' ? 'active':''; ?>"><a href="<?php echo base_url('cakreditasi/dokumenunit'); ?>"><i class="fa fa-circle-o"></i>Dokumen Unit </a></li><?php } ?>
            <?php 
              if($this->pageaccessrightbap->checkAccessRight(V_KATEGORIAKREDITASI)){ 
                if( $this->pageaccessrightbap->ql_check_username( $this->pageaccessrightbap->ql_pic_akreditasi() ) ) : ?>
                  <li class="<?=$active_sub_menu=='kategoriakreditasi' ? 'active':''; ?>"><a href="<?php echo base_url('cakreditasi/kategoriakreditasi'); ?>"><i class="fa fa-circle-o"></i>Kategori </a></li>
                <?php endif; ?>
              <?php } ?>
              <?php if($this->pageaccessrightbap->checkAccessRight(V_PENGATURANPIC)): ?>
                <li class="<?= $active_sub_menu=='pengaturanpic' ? 'active':''; ?>"><a href="<?php echo base_url('cakreditasi/pengaturanpic'); ?>"><i class="fa fa-circle-o"></i>Pengaturan</a></li>
              <?php endif; ?>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU AKREDITASI -->
        
        <!-- START MENU ADMINISTRASI -->
        <?php 
            if( $this->pageaccessrightbap->checkAccessRight(V_USER) or 
                $this->pageaccessrightbap->checkAccessRight(V_USERAKSES) or 
                $this->pageaccessrightbap->checkAccessRight(V_PERAN) or
                $this->pageaccessrightbap->checkAccessRight(V_HALAMAN) or
                $this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)
            ){       
        ?>
        <li class="treeview <?=$active_menu=='administrasi' ? 'active':''; ?>">
          <a href="#">
            <i class="fa fa-expeditedssl "></i> <span>Administrasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_HALAMAN)){?><li class="<?=$active_sub_menu=='halaman' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/halaman'); ?>"><i class="fa fa-circle-o"></i>Halaman </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_HUBUNGAN)){?><li class="<?=$active_sub_menu=='hubungan' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/hubungan'); ?>"><i class="fa fa-circle-o"></i>Hubungan </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_KONFIGURASI)){?><li class="<?=$active_sub_menu=='konfigurasi' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/konfigurasi'); ?>"><i class="fa fa-circle-o"></i>Konfigurasi </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_PERAN)){?><li class="<?=$active_sub_menu=='peran' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/peran'); ?>"><i class="fa fa-circle-o"></i>Peran </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_USER)){?><li class="<?=$active_sub_menu=='user' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/user'); ?>"><i class="fa fa-circle-o"></i> User </a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)){?><li class="<?=$active_sub_menu=='akses_user' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/akses_user'); ?>"><i class="fa fa-circle-o"></i>User Akses</a></li><?php } ?>
            <?php if($this->pageaccessrightbap->checkAccessRight(V_USERAKSES)){?><li class="<?=$active_sub_menu=='tandatangan_dokter' ? 'active':''; ?>"><a href="<?php echo base_url('cadministrasi/tandatangandokter'); ?>"><i class="fa fa-circle-o"></i>Tanda Tangan Dokter</a></li><?php } ?>
          </ul>
        </li>
        <?php } ?>
        <!-- END MENU ADMINISTRASI -->

       
        

        <!-- START MENU NOTIFIKASI -->
       <!--  <?php if($this->pageaccessrightbap->checkAccessRight(V_BARANGNOTIFIKASI)){?>
        <li class="treeview <?=$active_menu=='notifikasi' ? 'active':''; ?>">
          <a href="#"><i class="fa fa-bell-o "></i><span>Notifikasi</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
          <ul class="treeview-menu ">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_BARANGNOTIFIKASI)){
                if(!empty($this->session->userdata('notif')))
                {
                  $notif =  $this->session->userdata('notif')['isi'];
                    if(!empty($notif))
                      {
                        for ($x=0;$x<count($notif); $x++) {
                          echo '<li><a href="'.$notif[$x]['url'].'" class="text-small"><i class="fa '.$notif[$x]['icon'].'"></i>'.$notif[$x]['message'].'</a></li>';
                        }
                      }
                }
                    ?>
            <?php } ?>
          </ul>
        </li>
        <?php } ?> -->
        <!-- END MENU NOTIFIKASI -->

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
