<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= APP_NAME; ?> | <?= $title_page ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- Animation library for notifications   -->
  <link href="<?php echo base_url(); ?>assets/dist/css/animate.min.css" rel="stylesheet"/>
  <style type="text/css">
  .login-page{background-color: #6b6355;} 
  .login-box-body {background: #fff; padding: 35px; border-radius: 4px; border-top: 0; color: #666;}
  </style>
</head>
<body class="hold-transition login-page">
  <div class="col-xs-12 col-md-4"></div>
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url('loginperiksa');?>" ><b class="text-yellow"><i class="fa fa-expeditedssl fa-4x"></i></b></a>
  </div>
  <!-- Set notifikasi -->
  <div id="notif"></div>   
  <input type="hidden" id="set_status" value="<?= $this->session->flashdata('message')['status'] ?>">
  <input type="hidden" id="set_message" value="<?=  $this->session->flashdata('message')['message']?>">
  <div class="login-box-body">
    <p class="login-box-msg">Untuk registrasi periksa pasien, silakan login dahulu.!</p>
    <?= form_open('canjunganpasien/ceklogin'); ?>
      <div class="form-group has-feedback">
        <?= form_input(['name'=>'username', 'value'=>$this->session->flashdata('username'), 'type'=>'text', 'class'=>'form-control input-lg','placeholder'=>'No. Rekam medis', 'id'=>'username','autofocus'=>'auto']); ?>
        <span class="fa fa-user-secret fa-lg form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <?= form_input(['name'=>'password', 'type'=>'password', 'class'=>'form-control input-lg','placeholder'=>'Password', 'id'=>'password']); ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class=" col-xs-12"><?= form_input(['name'=>'save', 'type'=>'submit', 'class'=>'btn btn-warning btn-block btn-flat btn-lg','value'=>'Log In']); ?></div>
        <!-- /.col -->
      </div>
      <center>
      <a class="btn btn-xs" href="<?= base_url('canjunganpasien/jadwaldokter')?>"><i class="fa fa-calendar-o"></i> Jadwal</a>
      <a class="btn btn-xs" href="<?= base_url('lihatantrian')?>"><i class="fa fa-list-alt"></i>  Antrian</a>
      </center>
    <?= form_close(); ?>
  </div>
  <!-- /.login-box-body -->
  <center style="color:#fff;margin-top: 5px; font-size: 12.5px;"> <?= COPY_RIGHT; ?> </center>
</div>
<!-- /.login-box -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/plugins/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-notify.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/sirstql.min.js"></script>
</body>
</html>
