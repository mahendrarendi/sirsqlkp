
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left"><i class="fa fa-calendar-o"></i> <?php echo $table_title; ?></h3><br/>Halaman ini dibuat jam <?php echo date('d-m-Y H:i:s'); ?> dan kadaluarsa jam <?php echo date("d-m-Y H:i:s", strtotime("+1 hours")); ?>
            </div>
            <div class="box-body">
              <div class="row">
        <div class="col-xs-12">
          <div class="box box-solid">
            <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%" id="mytable">
            <thead>
            <tr >
              <th width="17%">POLIKLINIK</th>
              <th width="17%">DOKTER</th>
              <th>Ahad</th>
              <th>Senin</th>
              <th>Selasa</th>
              <th>Rabu</th>
              <th>Kamis</th>
              <th>Jum'at</th>
              <th>Sabtu</th>
            </tr>
            </thead>
            <tbody>
            <?php 
            if (!empty($dtjadwal) && $dtjadwal != null)
            { 
            $no=0;
            foreach ($dtjadwal as $obj) 
            {
            ?>
            <tr class="parent-grid-row">
              <td class="parent-grid-column"><?=$obj->namaunit?></td>
              <td><?=$obj->tenagamedis?></td>
              <td><?=ql_removeaftercharacters($obj->ahad,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->senin,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->selasa,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->rabu,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->kamis,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->jumat,'@')?></td>
              <td><?=ql_removeaftercharacters($obj->sabtu,'@')?></td>
            </tr>
            <?php
            }
            }
            ?>
            </tbody>
          </table>
          <b>*Jadwal Dapat Berubah sewaktu-waktu..!</b>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
    </div>
      </div>
    </div>
  </section>