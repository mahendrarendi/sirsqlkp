 <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta http-equiv='cache-control' content='no-cache'>
  <meta http-equiv='expires' content='0'>
  <meta http-equiv='pragma' content='no-cache'>
  <title>RSU Queen Latifa <?php echo $title_page==='' ? ' - Beranda' : '- ' ;  echo $title_page; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"><!-- Tell the browser to be responsive to screen width -->
  <link href="<?php echo base_url()?>assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap 3.3.7 -->
  <link href="<?php echo base_url()?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"><!-- Font Awesome -->
  <link href="<?php echo base_url()?>assets/dist/css/AdminLTE.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/dist/css/animate.min.css" rel="stylesheet"/><!-- Animation library for notifications   -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/images/favicon-16x16.png"><!--icon--> 
  <?php 
  if(!empty($plugins))
  {
    foreach ($plugins as $i => $value) 
    {
      $this->load->view('plugins/'.$plugins[$i].'css');//<!-- Plugins css tambahan-->
    }
  }
  ?>
  <link href="<?php echo base_url(); ?>assets/dist/css/sirstql.css" rel="stylesheet"/>
</head>
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-yellow layout-top-nav">
<!-- Site wrapper -->
<div class="wrapper">
<div class="modal"></div>
<input type="hidden" id="load_dttable" value="<?= ((!empty($jsmode)) ? $jsmode : '' );  ?>">
	  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url('canjunganpasien/jadwaldokter');?>" class="navbar-brand">RSU Queen Latifa</a>
            <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <?php if($this->session->userdata('sessregis_pasien')){ ?>
                  <a class="text-danger" href="<?=base_url('canjunganpasien/keluar') ?>"> <i class="fa fa-sign-out fa-lg"></i></a>
                <?php }else{ ?>
                  <a class="text-success" href="<?=base_url('loginperiksa') ?>"> <i class="fa fa-sign-in fa-lg"></i></a>
                <?php } ?>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  </header>

  <div class="content-wrapper">
    <div class="container" style="padding-top: 5px;">
      <a class="btn <?= (($active_menu=='jadwaldokter')?'btn-warning':'btn-default');?>" href="<?= base_url('canjunganpasien/jadwaldokter')?>"><i class="fa fa-calendar-o"></i> Jadwal</a>
      <a class="btn <?= (($active_menu=='lihatantrian')?'btn-warning':'btn-default');?>" href="<?= base_url('lihatantrian')?>"><i class="fa fa-list-alt"></i>  Antrian</a>
      <a class="btn <?= (($active_menu=='pendaftaranonline')?'btn-warning':'btn-default');?>" href="<?= base_url('canjunganpasien/pendaftaranonline')?>"><i class="fa  fa-plus-square-o"></i> Periksa</a>
      
     <?php $this->load->view($content_view); ?>
    </div>
  </div>
    
      <div id="notif"></div> <!-- Set notifikasi -->
      <input type="hidden" id="set_status" value="<?= $this->session->flashdata('message')['status'] ?>">
      <input type="hidden" id="set_message" value="<?= $this->session->flashdata('message')['message'] ?>">
      <input type="hidden" id="page_view" value="<?= $title_page; ?>" /> <!-- set isi page view -->
    
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('v_footer'); ?>
    <!-- END FOOTER -->
</div>
<script>
    sessionStorage.user='<?php echo $this->session->userdata('username'); ?>';
    //supaya pesan order tidak muncul
    sessionStorage.pesanorderugd=0;
    sessionStorage.pesanorderrm=0;
</script>
<script type="text/javascript"> var base_url ='<?php echo base_url(); ?>';</script> <!-- inisialisasi base url javascript -->
<script src="<?php echo base_url()?>assets/plugins/jquery/dist/jquery.min.js"></script> <!-- jQuery 3 -->
<script src="<?php echo base_url()?>assets/dist/js/qrcode.min.js"></script><!-- QRCODE -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script><!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script><!-- SlimScroll -->
<script src="<?php echo base_url()?>assets/plugins/fastclick/lib/fastclick.js"></script><!-- FastClick -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-notify.js"></script><!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/dist/js/sirstql.min.js"></script>
<?php 
 if(!empty($plugins))
  {
    foreach ($plugins as $i => $value) 
    {
      $this->load->view('plugins/'.$plugins[$i].'js'); //<!-- Plugins js tambahan-->
    }
  }
if( !empty($script_js) && $script_js!='')
{
  echo '<script src="'. base_url() .'assets/pages/'.$script_js.'.min.js"></script>'; // location file assets/pages/filename.js
}
?>
</body>
</html>

   