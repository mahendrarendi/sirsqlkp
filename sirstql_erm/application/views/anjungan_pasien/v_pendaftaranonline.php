
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left"><i class="fa fa-plus-square-o"></i> <?php echo $table_title; ?></h3>
            </div>
            <div class="box-body">
              <form method="POST" class="register-form" id="register-form">
                <input type="hidden" id="caradaftar" value="online">
                <h5 style="margin-bottom: none;">FORM PENDAFTARAN PERIKSA PASIEN</h5>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6">
                        <label for="name">No.RM :</label>
                        <input type="text" value="<?php echo $this->session->userdata('sessregis_pasien'); ?>" class="form-control" readonly style="cursor:not-allowed;" id="norm" />
                        <input type="hidden"  id="pp" value="" >
                    </div>
                </div>
                <div class="row">
                  <div class="form-group col-xs-12 col-sm-6 col-md-6">
                      <label for="name">Nama Pasien :</label>
                      <input type="text" id="nama" class="form-control" value="<?php echo $this->session->userdata('namalengkap'); ?>" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                  </div>
                  <div class="form-group col-xs-12 col-sm-3 col-md-3">
                      <label for="name">Jenis Kelamin:</label>
                      <input type="text" value="<?php echo $this->session->userdata('jeniskelamin'); ?>" id="kelamin" class="form-control" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                  </div>
                  <div class="form-group col-xs-12 col-sm-3 col-md-3">
                    <label for="name">Tanggal Lahir :</label>
                    <input type="text" id="lahir" value="<?php echo $this->session->userdata('tanggallahir'); ?>"  class="form-control" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                  </div>
                </div>
                <div class="row" >
                    <div class="form-group col-xs-12">
                        <label for="address">Alamat :</label>
                        <textarea id="alamat" class="form-control" placeholder="[otomatis]" readonly style="cursor:not-allowed;" ><?php echo $this->session->userdata('alamat'); ?></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-3 col-md-3">
                        <label for="state">Tanggal Periksa :</label>
                        <input type="text" onchange="cari_poliklinik(this.value)" id="date" class="form-control datepicker" name="">
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-3">
                        <label for="kelas">Kelas :</label>
                        <div class="form-select">
                            <select class="select2 form-control" name="kelas" id="kelas" style="width: 100%;">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-sm-3 col-md-3">
                        <label for="carabayar">Cara Bayar :</label>
                        <div class="form-select">
                            <select class="select2 form-control" name="carabayar" id="carabayar" style="width: 100%;">
                                <option value="0">Pilih Cara Pembayaran</option>
                                <option value="mandiri">Umum / Bayar Sendiri</option>
                                <option value="jknpbi">BPJS Subsidi Pemerintah</option>
                                <option value="jknnonpbi">BPJS Non Subsidi</option>
                                <option value="asuransi lain">Asuransi lain</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-6">
                        <label for="poli">Poli Tujuan :</label>
                        <div class="form-select">
                            <select class="select2 form-control" name="poli" id="poli" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <a class="btn btn-warning btn-lg" onclick="lanjut_registrasi()">DAFTAR PERIKSA</a>&nbsp;
                    <a class="btn btn-danger btn-lg" onclick="batal_registrasi()">BATAL</a>
                </div>
            </form>
            </div>
            <!-- /.box-body -->
          </div>
      </div>
    </div>
  </section>