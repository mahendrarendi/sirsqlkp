<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title pull-left"><i class="fa fa-list-alt"></i> <?php echo $table_title; ?></h3><br/>Halaman ini dibuat jam <?php echo date('d-m-Y H:i:s'); ?> dan kadaluarsa jam <?php echo date("d-m-Y H:i:s", strtotime("+2 minutes")); ?>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-xs-12">
              <div class="box box-solid">
                <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <!-- <th>Stasiun</th> -->
                  <th>Poliklinik</th>
                  <th>Antrian</th>
                  <th>Totaldiambil</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if (!empty($dtantrian) && $dtantrian != null)
                { 
                $no=0;
                foreach ($dtantrian as $obj) 
                {
                ?>
                <tr >
                  <td><i class="<?=$obj->fontawesome;?>"></i> <?=$obj->namaloket;?></td>
                  <td><?=$obj->no;?></td>
                  <td><?=$obj->totaldiambil;?></td>
                </tr>
                <?php
                }
                }
                ?>
                </tbody>
              </table>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
