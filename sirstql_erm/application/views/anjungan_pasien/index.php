<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SITIQL-PENDAFTARAN PERIKSA PASIEN RAWAT JALAN</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="<?= base_url()?>assets/registrasi/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link href="<?php echo base_url()?>assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap datepicker -->
    <link href="<?= base_url();?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?= base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url()?>assets/registrasi/css/style.css">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon-16x16.png">
    <style type="text/css">.btn-success {color: #fff;background-color: #4CAF50;border-color: #4CAF50;}.btn-warning {color: #fff;background-color: #ffa700;border-color: #ffa700;
        }.btn-danger {color: #fff;background-color: #e2302b;border-color: #e2302b;}/*SELECT 2*/
        .select2-container .select2-selection--single .select2-selection__rendered {
            /*padding-left: 0;*/
            padding-right: 0;
            height: auto;
            margin-top: 3px;
            font-size: 14px;
        }
        .select2-container .select2-selection--single {
            /*box-sizing: border-box;*/
            cursor: pointer;
            display: block;
             height: 32px;
            user-select: none;
            -webkit-user-select: none;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow {
            background-color: #d2d6de;
            height: 26px;
            position: absolute;
            top: 1px;
            right: 1px;
            width: 20px;
            height: 30px;
        }
    
        .modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('/sirstql/assets/ajax-loader.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
    
    </style>
    <link href="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.css" rel="stylesheet">
</head>
<body>
    
<div class="modal"></div>
    <input type="hidden" id="waktudaftar" value="<?= date('d-M-Y'); ?>">
    <input type="hidden"  id="pp" >
    <div class="main" style="background-color: #fff;">
    <div id="qrcode" style="width:100px; height:100px; margin-top:15px;display: none;"></div>
        <div class="signup-content">
        <div class="signup-img"><img class="images" src="<?= base_url()?>assets/registrasi/images/ambulan.jpg" alt="" style="max-height: 615px;"></div>
        <div class="signup-form">
            <form method="POST" class="register-form" id="register-form">
                <h2 style="margin-bottom: none;">PENDAFTARAN PERIKSA PASIEN RAWAT JALAN</h2>
                <div class="form-row" >
                    <div class="form-group" style="width: 100%">
                        <label for="name">No.RM :</label>
                        <input type="hidden" id="caradaftar" value="anjungan">
                        <input type="text" autofocus class="form-control" placeholder="Masukan No.RM pasien" id="norm" />
                    </div>
                    <div class="form-group">
                        <label for="father_name">&nbsp;</label>
                        <a class="btn btn-success btn-sm" onclick="register_cekrmanjungan()">Cek No.RM</a>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group" style="width: 100%">
                        <label for="name">Nama Pasien :</label>
                        <input type="text" id="nama" class="form-control col-sm-8" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="name">Jenis Kelamin:</label>
                        <input type="text" id="kelamin" class="form-control col-sm-8" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                    </div>
                    <div class="form-group">
                        <label for="name">Tanggal Lahir :</label>
                        <input type="text" id="lahir"  class="form-control col-sm-8" placeholder="[otomatis]" readonly style="cursor:not-allowed;" />
                    </div>
                </div>
                <div class="form-row" >
                    <div class="form-group" style="width: 100%">
                        <label for="address">Alamat :</label>
                        <textarea id="alamat" class="form-control" placeholder="[otomatis]" readonly style="cursor:not-allowed;" ></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <label for="carabayar">Cara Bayar :</label>
                        <div class="form-select">
                            <select class="select2 form-control" name="carabayar" id="carabayar" style="width: 100%;">
                                <option value="0">Pilih Cara Pembayaran</option>
                                <option value="mandiri">Umum / Bayar Sendiri</option>
                                <option value="jknpbi">BPJS Subsidi Pemerintah</option>
                                <option value="jknnonpbi">BPJS Non Subsidi</option>
                                <option value="asuransi lain">Asuransi lain</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="kelas">Kelas :</label>
                        <div class="form-select">
                            <select class="select2 form-control" name="kelas" id="kelas" style="width: 100%;">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="state">Tanggal Periksa :</label>
                        <input type="text" onchange="cari_poliklinik(this.value)" id="date" class="form-control datepicker" name="" readonly>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group" style="width: 100%;">
                        <label for="poli">Poli Tujuan : <i>#ketik poli yang dituju (contoh: syaraf )</i></label>
                        <div class="form-select">
                            <select class="select2 form-control" name="poli" id="poli" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                </div>
                <div>
                    <a class="btn btn-warning btn-lg" onclick="lanjut_registrasi()">DAFTAR PERIKSA</a>&nbsp;
                    <a class="btn btn-danger btn-lg" onclick="batal_registrasi()">BATAL</a>
                </div>
            </form>
        </div>
        </div>

    </div>
    <script type="text/javascript"> var base_url ='<?php echo base_url(); ?>';</script>

    <!-- JS -->
    <script src="<?php echo base_url()?>assets/plugins/jquery/dist/jquery.min.js"></script> 
    <script src="<?=base_url()?>assets/registrasi/js/main.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="<?= base_url();?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Select2 -->
    <script src="<?= base_url(); ?>assets/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/pages/anjungan_pasien.min.js?ver=1"></script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>