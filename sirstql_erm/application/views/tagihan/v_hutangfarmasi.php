<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
             
             <!--total faktur usaha-->
            <input type="hidden" id="modeview" value="<?= $modeview; ?>">
            <!--start total dibayar-->
            <div class="col-md-12">
            <?php if($modeview=='fakturlunas'){ ?>
                <h3>Hutang Dibayar :  <span id="nomtothutangusaha"> </span> </h3>
                <label class="label label-info" id="tanggalbayar"></label>
                <hr>
            <?php }else{ ?>
                <h3>Total Hutang :  <span id="nomtothutangusaha"> </span> </h3>
                <label class="label label-info" id="tanggalbayar"></label>
                <hr>
            <?php } ?>
            </div>
            <!--end total dibayar-->
             
           <div class="toolbar col-md-6 row">
              <input type="text" id="tanggal1" name="tanggal1" class="datepicker" size="7"/>
              <input type="text" id="tanggal2" name="tanggal2" class="datepicker" size="7"/>
              <a id="tampil" href="#" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
              
              <!--jika mode view faktur lunas-->
            <?php if($modeview=='fakturlunas'){ ?>
              <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
                <hr>
            <?php } ?>
              
              
              <!--Jika Mode View Hutang Farmasi-->
              <?php if($modeview=='hutangfarmasi'){ ?>
              <input type="checkbox" id="isdibayar"/> Faktur Lunas
              <?php } ?>
              
           </div>
           <table id="listhutang" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%"> 
             <thead>
             <tr class="bg bg-yellow-gradient">
               <th>No</th>
               <th>DISTRIBUTOR</th>
               <th>TAGIHAN</th>
               <th>PPN</th>
               <th>POTONGAN</th>
               <th>TOTAL TAGIHAN</th>
               <th>DIBAYAR</th>
               <th>KEKURANGAN</th>
               <th></th>
             </tr>
             </thead>
             <tbody>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </section>
 <!-- /.content -->
