
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div class="box-header">
            <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3>
            <a href="<?php echo base_url('cadministrasi/add_user'); ?>" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data</a> 
            <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm pull-right"><i class="fa  fa-refresh"></i> Refresh</a> 
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Username</th>
                <th style="width:170px;">Aksi</th>
              </tr>
              </thead>
              <tbody>
              <!-- START TAMPIL DATA USER -->
              <?php
              if (!empty($data_list))
              { 
                $no=0;
                foreach ($data_list as $obj) 
                {  
                  $this->encryptbap->generatekey_once("HIDDENTABEL");
                  $username     = $this->encryptbap->encrypt_urlsafe($obj->namauser, "json");
                  $id           = $this->encryptbap->encrypt_urlsafe($obj->iduser, "json");
                  $tabel        = $this->encryptbap->encrypt_urlsafe('login_user', "json");
                  $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_USER, "json");
                  //-------------------------------------------------------- lihat standar, tolong parameter di decrypt
                  echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                  echo '  <td>'.$no.'</td>
                          <td>'.$obj->namauser.'</td>
                          <td>
                             <a data-toggle="tooltip" title="" data-original-title="Reset Password" id="reset_password" class="btn btn-warning btn-xs" alt="'.$username.'" alt2="'.$id.'"  href="#"><i class="fa fa-refresh" ></i> Reset</a>
                             <a data-toggle="tooltip" title="" data-original-title="Delete User" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                        </tr>';
                }
              }
              ?>
              <!-- END TAMPIL DATA USER -->
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <div class="box-header">
            <center>
              <br>
            <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
            </center>
            <br>
            <?php echo validation_errors(); ?>
            <!-- FORM ADD/EDIT USER-->
            <?php 
            echo form_open('cadministrasi/save_user', 'class="form-horizontal" id="FormUser"'); 
            echo form_input(['name'=>'iduser', 'type'=>'hidden']);
            styleformgrup1kolom('Username',form_input(['name'=>'username', 'type'=>'text', 'class'=>'form-control','id'=>'username']));
            styleformgrup1kolom('Password',form_input(['name'=>'password', 'type'=>'text', 'class'=>'form-control','id'=>'password']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_user()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cadministrasi/user').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->