<link href="<?= base_url();?>assets/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">        
<link href="<?= base_url();?>assets/plugins/datatables/dataTables.responsive.css" rel="stylesheet">    
<link href="<?= base_url();?>assets/plugins/datatables/dataTables.tableTools.min.css" rel="stylesheet">
<!-- <link href="<?= base_url();?>assets/plugins/datatables/buttons.dataTables.min.css" rel="stylesheet"> -->
<!-- DATA TABLES CSS -->
<style type="text/css">
.table-striped>tbody>tr:nth-of-type(odd) {
    background-color: #ececec;
}
td.details-control {
    background: url('/sirstql/assets/images/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
    background: url('/sirstql/assets/images/details_close.png') no-repeat center center;
}
</style>
