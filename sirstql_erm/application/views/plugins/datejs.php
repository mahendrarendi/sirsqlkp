<!-- bootstrap datepicker -->
<script src="<?= base_url();?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- bootstrap moment -->
<script src="<?= base_url();?>assets/plugins/moment/min/moment.min.js"></script>

<!-- bootstrap datetimepicker -->
<script src="<?= base_url();?>assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datetimepicker.min.js"></script>