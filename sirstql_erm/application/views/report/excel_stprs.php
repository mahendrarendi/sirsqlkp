<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table>
  <tr><td colspan="14" align="center"><?= $judullaporan; ?></td></tr>
</table>
<br>
<br>
<br>
<table border="1">
  <thead>
    <tr>
      <th rowspan="3">No</th>
      <th rowspan="3">Kode</th>
      <th rowspan="3">Kasus</th>
      <th rowspan="3">Laki</th>
      <th rowspan="3">Perempuan</th>
      <th rowspan="3">Jml Kasus</th>
      <th rowspan="3">Kunjungan</th>
      <th colspan="24">RAWAT <?= strtoupper($jenis); ?></th>
    </tr>
    <tr>
      <th colspan="2">0-7 Hr</th>
      <th colspan="2">8-28 Hr</th>
      <th colspan="2"> &lt; 1 Th</th>
      <th colspan="2">1-4 Th</th>
      <th colspan="2">5-9 Th</th>
      <th colspan="2">10-14 Th</th>
      <th colspan="2">15-19 Th</th>
      <th colspan="2">20-44 Th</th>
      <th colspan="2">45-54 Th</th>
      <th colspan="2">55-59 Th</th>
      <th colspan="2">60-69 Th</th>
      <th colspan="2"> >=70 Th</th>
    </tr>
    <tr>
      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>

      <th>L</th>
      <th>P</th>
    </tr>
    </thead>
<?php if(!empty($stprs) && $stprs!=null)
{
  $no=0;
  foreach ($stprs as $obj) {
  echo "<tr><td>". ++$no ."</td>
  <td>".$obj->icd."</td>
  <td>".$obj->jenispenyakit."</td>
  <td>".$obj->L."</td>
  <td>".$obj->P."</td>
  <td>".$obj->J."</td>
  <td>0</td>
  <td>".$obj->L1."</td>
  <td>".$obj->P1."</td>
  <td>".$obj->L2."</td>
  <td>".$obj->P2."</td>
  <td>".$obj->L3."</td>
  <td>".$obj->P3."</td>
  <td>".$obj->L4."</td>
  <td>".$obj->P4."</td>
  <td>".$obj->L5."</td>
  <td>".$obj->P5."</td>
  <td>".$obj->L6."</td>
  <td>".$obj->P6."</td>
  <td>".$obj->L7."</td>
  <td>".$obj->P7."</td>
  <td>".$obj->L8."</td>
  <td>".$obj->P8."</td>
  <td>".$obj->L9."</td>
  <td>".$obj->P9."</td>
  <td>".$obj->L10."</td>
  <td>".$obj->P10."</td>
  <td>".$obj->L11."</td>
  <td>".$obj->P11."</td>
  <td>".$obj->L12."</td>
  <td>".$obj->P12."</td>
  </tr>";
  }

}
?>
</table>



