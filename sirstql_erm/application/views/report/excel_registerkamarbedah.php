<?php 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$title.xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    
    $table = '<table border="1">
            <thead>
                <tr>
                    <th rowspan="2">No</th>
                    <th rowspan="2">NORM</th>
                    <th rowspan="2">Nama Pasien</th>
                    <th rowspan="2" width="200">Alamat</th>
                    <th rowspan="2">Jaminan</th>
                    <th rowspan="2">Ruang</th>
                    <th rowspan="2">Waktu Rencana Operasi</th>
                    <th rowspan="2">Waktu Pelaksanaan Operasi</th>
                    <th rowspan="2">Jenis Anestesi</th>
                    <th rowspan="2">Obat Anestesi</th>
                    <th colspan="8">TIM Operasi</th>
                    <th rowspan="2">Diagnosa</th>
                    <th rowspan="2">Tindakan</th>
                    <th rowspan="2">Jenis Tindakan</th>
                    <th rowspan="2">Asesment Pra Anestesi</th>
                    <th rowspan="2">Kelengkapan Lap Anestesi</th>
                    <th rowspan="2">Kelengkapan Monitoring Pemulihan PSCA Anestesi</th>
                    <th rowspan="2">Konversi Anestesi Dari Lokal/Regional ke GA</th>
                    <th rowspan="2">Asesmen Pra Bedah</th>
                    <th rowspan="2">Marking</th>
                    <th rowspan="2">Kelengkapan SCC</th>
                    <th rowspan="2">Diskrepasi Diagnosa Pre dan Post</th>
                </tr>
                <tr>
                    <th>Operator</th>
                    <th>Anestesi</th>
                    <th>dr. Anak</th>
                    <th>Penata</th>
                    <th>Asisten</th>
                    <th>Instrumen</th>
                    <th>Onloop</th>
                    <th>RR/P.Bayi</th>
                </tr>
            </thead>
            <tbody>';
    
    $no =0;
    foreach ($register as $obj)
    {
        $table .= '<tr>
                    <td>'. ++$no .'</td>
                    <td>'.$obj->norm.'</td>
                    <td>'. $obj->namalengkap.'</td>
                    <td>'. $obj->alamat.'</td>
                    <td>'. $obj->jaminan.'</td>    
                    <td>'. $obj->ruang.'</td>            
                    <td>'. $obj->waktuoperasi.'</td>
                    <td>'. $obj->tanggalmulai.' '.$obj->jammulai.'</td>
                    <td>'. $obj->jenisanestesi.'</td>
                    <td>'. $obj->obatanestesi.'</td>                
                    <td>'. $obj->dokteroperator.'</td>
                    <td>'. $obj->dokteranestesi.'</td>
                    <td>'. $obj->dokteranak.'</td>
                    <td>'. $obj->penata.'</td>                
                    <td>'. $obj->onloop.'</td>
                    <td>'. $obj->asisten.'</td>
                    <td>'. $obj->instrumen.'</td>
                    <td>'. $obj->penerimabayi.'</td>                
                    <td>'. $obj->diagnosapascaoperasi.'</td>
                    <td>'. $obj->tindakanoperasi.'</td>
                    <td>'. $obj->jenistindakan.'</td>
                    <td>'. $obj->asesment_pra_anestesi.'</td>                
                    <td>'. $obj->kelengkapan_lap_anestesi.'</td>
                    <td>'. $obj->kelengkapan_monitoring_pemulihan_psca_anestesi.'</td>
                    <td>'. $obj->konversi_anestesi_dari_lokalregionalkega.'</td>
                    <td>'. $obj->asesmen_pra_bedah.'</td>                
                    <td>'. $obj->marking.'</td>
                    <td>'. $obj->kelengkapan_scc.'</td>
                    <td>'. $obj->diskrepasi_diagnosa_pre_dan_post.'</td>
                </tr>';
    }
            
    $table .= '</tfoot></table>';    
    
    echo $table;
    
    
    
?>