<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">            
          <div>
            <select id="idunit" onchange="loaddata()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%"></select>
            <select id="status" onchange="loaddata()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%">
                <option value="0">Pesan</option>
                <option value="1">Periksa</option>
                <option value="2">Selesai</option>
                <option value="3">Batal</option>
                <option value="4">Kadaluarsa</option>
            </select>
            <select id="carabayar" onchange="loaddata()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%">
                <?php 
                    foreach ($carabayar as $arr)
                    {
                        echo "<option value=".$arr.">".$arr."</option>";
                    }
                ?>
            </select>
            
            <input id="tglawal"  size="7" class="datepicker" placeholder="Tanggal awal"> 
            <input id="tglakhir" size="7" class="datepicker" placeholder="Tanggal akhir"> 
            <a class="btn btn-info btn-sm" onclick="loaddata()"><i class="fa fa-desktop"></i> Tampil</a> 
            <a style="margin-right: 6px;" onclick="refresh()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
          </div>
          <div class="box-body" > <!-- id="listpasien"-->
            <table id="tablestatus" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                  <tr class="bg bg-yellow-gradient">
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>NO.SEP</th>
                    <th>TGL. DAFTAR</th>
                    <th>TGL. PERIKSA</th>
                    <th>KLINIK TUJUAN (No.Antri)</th>
                    <th>CARABAYAR</th>
                    <th>JENIS PERIKSA</th>
                    <th>STATUS</th>
                    <th>PEMERIKSAAN</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tfoot>
                </table>
          </div>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->