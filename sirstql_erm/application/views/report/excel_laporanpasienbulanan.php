<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
<thead>
    <tr bgcolor="02BD00"><th rowspan="3">No</th>
        <th rowspan="3">Dokter</th>
        <th rowspan="3">Poliklinik</th>
        <th rowspan="3">Jaminan</th>
        <th colspan="6">Identitas Pasien</th>
        <th rowspan="3">Pasien baru/lama</th>
        <th colspan="5">Pemeriksaan</th>
        <th rowspan="3">Diagnosa</th>
        <th rowspan="3">Tindakan</th>
        <th rowspan="3">Terapi</th>
    </tr>
<tr bgcolor="02BD00">
  <th rowspan="2">No.RM</th>
  <th rowspan="2">Nama</th>
  <th rowspan="2">Tgl Lahir</th>
  <th rowspan="2">Usia</th>
  <th rowspan="2">Alamat</th>
  <th rowspan="2">Jenis Kelamin</th>
  <th colspan="2">Pemeriksaan Fisik</th> 
  <th colspan="3">Pemeriksaan Penunjang</th>
</tr>
<tr bgcolor="02BD00">
  <th>Vital Sign</th>
  <th>Data Objektif</th>
  <th>Laboratorium</th>
  <th>Radiologi</th>
  <th>USG</th>
</tr>
</thead>
<tbody>

<?php if(!empty($identitas) && $identitas!=null)
{
  $no=0;
  $html=''; $no=0;
 foreach ($identitas as $x)
 {
     $vitalsign='';$laborat='';$diagnosa='';$tindakan='';$obat='';
     foreach ($icd[$no] as $i)
     {
         $vitalsign.=(($i->idjenisicd=='1')? ''.$i->icd.' '.$i->namaicd .' '.$i->nilai.'<br>':'');
         $diagnosa.=(($i->idjenisicd=='2')? ''.$i->icd.' '.$i->namaicd .' '.$i->nilai.'<br>':'');
         $laborat.=(($i->idjenisicd=='4')? ''.$i->icd.' '.$i->namaicd .' '.$i->nilai.'<br>':'');
         $tindakan.=(($i->idjenisicd=='3')? ''.$i->icd.' '.$i->namaicd .' '.$i->nilai.'<br>':'');
     }
     foreach ($terapi[$no] as $b)
     {
        $obat.= $b->kode.' '.$b->namabarang.' '.$b->jumlahpemakaian.' '.$b->namasatuan.'<br>';
     }
     echo '<tr><td>', ++$no ,'</td><td>',$x->dokter,'</td><td>',$x->namaunit,'</td><td>',$x->carabayar,'</td><td>',$x->norm,'</td><td>',$x->namalengkap,'</td><td>',$x->tanggallahir,'</td><td>',$x->usia,'</td><td>',$x->alamat,'</td><td>',$x->jeniskelamin,'</td><td>',$x->ispasienlama,'</td><td>',$vitalsign,'</td><td>',$x->anamnesa,'</td><td>',$laborat,'</td><td>',$x->keteranganradiologi,'<br>',(($x->saranradiologi=='')?'':'Saran:<br>') ,$x->saranradiologi,'</td><td></td><td>',$diagnosa,'</td><td>',$tindakan,'</td><td>',$obat,'</td></tr>';
 }
}
?>


</tfoot>
</table>

