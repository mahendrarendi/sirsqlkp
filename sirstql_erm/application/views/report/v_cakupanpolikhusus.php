<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2" style="margin-right:-6px;">
                    <select class="form-control" name="idunit" id="idunit">
                        <?php 
                            foreach ($unit as $arr)
                            {
                                echo  "<option value=".$arr['idunit'].">". ucwords(strtolower($arr['namaunit'])) ."</option>";
                            }
                        ?>
                    </select>
                </div>
                <div class="col-md-5">
                  <input type="text" class="datepicker" id="tahunbulan" size="7" value="" readonly>
                  &nbsp;
                  <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                  <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Cakupan</a>
                </div>
              </div>

              <div class="box box-default box-solid">
                  <div class="box-header with-border">
                    <h5 class="text-bold" id="title1">Laporan Cakupan Poli ...</h5>
                  </div>
                  <div class="box-body">
                      <div class="col-xs-12" id="tablePasienlamabaru"></div>
                      
                      <div class="col-xs-12" id="tableJenisperiksa"></div>
                      
                      <div class="col-xs-12" id="tableCarabayar">
                      </div>
                  </div>
              </div>

            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold" id="title2">Cakupan Diagnosis</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="viewDiagnosis">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="header-table-ql"><th>No</th><th>Diagnosis DPJP</th><th>Jumlah Pasien</th></tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h5 class="text-bold" id="title3">Cakupan Tindakan</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="viewTindakan">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="header-table-ql"><th>No</th><th>Tindakan</th><th>Jumlah Pasien</th></tr>
                        </table>
                    </div>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h5 class="text-bold" id="title4">Cakupan Tindakan Elektromedik</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="viewTindakanElektromedik">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="header-table-ql"><th>No</th><th>Tindakan</th><th>Jumlah Pasien</th></tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
        