

<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=dashboard.xls");
header("Pragma: no-cache");
header("Expires: 0");

 $namaunit='';
 echo '<h1>CAKUPAN '.$tahun.'</h1><table border="1">';
 foreach ($cakupanpoli as $obj) 
 {
  if($namaunit!=$obj->namaunit)
  {
    echo '<tr bgcolor="#ffc107"><td colspan="7"><b>UNIT ',$obj->namaunit,'</b></td></tr>',
         '<tr bgcolor="#cddc39"><td rowspan="2">Bulan</td><td rowspan="2">Tahun</td><td colspan="3">Pasien</td><td colspan="2">Persentase Pasien</td></tr>',
         '<tr bgcolor="#cddc39"><td>Lama</td><td>Baru</td><td>Total</td><td>Lama</td><td>Baru</td></tr>';
  }
  echo '<tr><td>',$obj->bulan,'</td><td>',$obj->tahun,'</td><td>',$obj->totalpasienbaru,'</td><td>',$obj->totalpasienlama,'</td><td>',$obj->total,'</td><td>',round(($obj->totalpasienlama/$obj->total) * 100),'%</td><td>',round(($obj->totalpasienbaru/$obj->total) * 100),'%</td></tr>';

  $namaunit=$obj->namaunit;
 }
 echo '</table>';

 $namabayar='';
  echo '<h1>PROPORSI JAMINAN PASIEN</h1><table border="1">';
 if (is_array($carabayar) || is_object($carabayar))
 {
    foreach ($carabayar as $obj)
    {
      if($namabayar!=$obj->carabayar)
      {
        echo '<tr bgcolor="#ffc107"><td colspan="2"><b>',$obj->carabayar,'</b></td></tr>',
             '<tr><td>UNIT</td><td>JUMLAH PASIEN</td></tr>';
      }
      echo '<tr><td>',$obj->namaunit,'</td><td>',$obj->jumlahpasien,'</td></tr>';

      $namabayar=$obj->carabayar;
    }
}
echo '</table>';
$header='';$judul='';$judul2='';
foreach ( $bln10besar as $obj)
  {
      $header .='<td colspan="4" bgcolor="#ffc107" >'.$obj.'</td>';
      $judul .='<td>NO</td><td>DIAGNOSA</td><td>JUMLAH</td><td>PERSENTASE</td>';
      $judul2 .='<td>NO</td><td>TINDAKAN</td><td>JUMLAH</td><td>PERSENTASE</td>';
    
  }
  $no=0;$isitable='';$isitable2='';
  for($x=0; $x<10; $x++ )
  {
    $isitable.='<tr>';
    $isitable2.='<tr>';
    ++$no;
    for($i=0; $i<12;$i++)
    {
        // if(count($diagnosa10bsr[$i]!=0)){
          $isitable.='<td>'. $no .'</td><td>'.((empty($diagnosa10bsr[$i][$x]))?'':$diagnosa10bsr[$i][$x]->icd).' '.((empty($diagnosa10bsr[$i][$x]))?'':$diagnosa10bsr[$i][$x]->namaicd).'</td><td>'.((empty($diagnosa10bsr[$i][$x]))?'':$diagnosa10bsr[$i][$x]->total).'</td><td>'.((empty($diagnosa10bsr[$i][$x]))?'':round($diagnosa10bsr[$i][$x]->percentase).'%').'</td>';
          $isitable2.='<td>'. $no .'</td><td>'.((empty($tindakan10bsr[$i][$x]))?'':$tindakan10bsr[$i][$x]->icd).' '.((empty($tindakan10bsr[$i][$x]))?'':$tindakan10bsr[$i][$x]->namaicd).'</td><td>'.((empty($tindakan10bsr[$i][$x]))?'':$tindakan10bsr[$i][$x]->total).'</td><td>'.((empty($tindakan10bsr[$i][$x]))?'':round($tindakan10bsr[$i][$x]->percentase).'%').'</td>';
          // $isitable2.='<td>'+ $no +'</td><td>'+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].icd)+' '+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].namaicd)+'</td><td>'+((hasil.tindakan10bsr[i][x]==undefined)?'':hasil.tindakan10bsr[i][x].total)+'</td><td>'+((hasil.tindakan10bsr[i][x]==undefined)?'':angkadesimal(hasil.tindakan10bsr[i][x].percentase,2)+'%')+'</td>';
        // }
    }
    $isitable  .='</tr>';
    $isitable2 .='</tr>';
  }
  echo '<h1>10 DIAGNOSIS TERBANYAK</h1><table border="1" class="table table-striped table-bordered" style="width:100%"><tr>',$header,'</tr><tr>',$judul,'</tr>',$isitable,'</table>';
  echo '<h1>10 TINDAKAN TERBANYAK</h1><table border="1" class="table table-striped table-bordered" style="width:100%"><tr>',$header,'</tr><tr>',$judul2,'</tr>',$isitable2,'</table>';

 ?>

 

