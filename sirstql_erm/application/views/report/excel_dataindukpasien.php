<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
<tr><th colspan="15"><b>Laporan Data Pasien Dari Nomor Rekam Medis <?= $rmawal; ?> - <?= $rmakhir; ?></b></th></tr>
<tr>
  <th>NO</th>
  <th>NIK</th>
  <th>NO.RM</th>
  <th>NO.JKN</th>
  <th>NAMA LENGKAP</th>
  <th>TGL. LAHIR</th>
  <th>KELAMIN</th>
  <th>NO.TELEPON</th>
  <th>ALAMAT</th>
  <th>PENDIDIKAN</th>
  <th>PEKERJAAN</th>
  <th>STATUS</th>
  <th>AGAMA</th>
  <th>GolDarah</th>
  <th>RH</th>
</tr>
<?php $no=0; if(!empty($datapasien)){ foreach ($datapasien as $obj) { ?>
    <tr>
    <td><?= $no++; ?></td>
    <td><?= $obj->nik; ?></td>
    <td><?= $obj->norm; ?></td>
    <td><?= $obj->nojkn; ?></td>
    <td><?= $obj->namalengkap; ?></td>
    <td><?= $obj->tanggallahir; ?></td>
    <td><?= $obj->jeniskelamin; ?></td>
    <td><?= $obj->notelpon; ?></td>
    <td><?= $obj->alamat; ?></td>
    <td><?= $obj->namapendidikan; ?></td>
    <td><?= $obj->namapekerjaan; ?></td>
    <td><?= $obj->statusmenikah; ?></td>
    <td><?= $obj->agama; ?></td>
    <td><?= $obj->golongandarah; ?></td>
    <td><?= $obj->rh; ?></td>
  </tr>
<?php } } ?>
</table>