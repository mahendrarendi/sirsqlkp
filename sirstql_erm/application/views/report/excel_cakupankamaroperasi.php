<?php 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$fileName.xls");
    header("Pragma: no-cache");
    header("Expires: 0");    
    
    echo "<h3>".$fileName." </h3>";
    echo '<table class="table table-bordered table-hover table-striped">
            <tr class="header-table-ql"><td rowspan="2">NO</td><td rowspan="2">OPERATOR</td><td colspan="8" class="text-center">JAMINAN</td><td rowspan="2">JUMLAH</td></tr>
            <tr class="header-table-ql"><td>UMUM</td><td>N-PBI</td><td>PBI</td><td>ASURANSI LAIN</td><td>JAMKESOS</td><td>JAMPERSAL</td><td>JASARAHARJA</td><td>BPJamsostek</td></tr>
        <tbody>';
    if(isset($carabayar))
    {
        $no=0;
        foreach ($carabayar as $arr)
        {

            echo "<tr><td>". ++$no."</td><td>".$arr['namadokter']."</td><td>".$arr['umum']."</td><td>".$arr['jknpbi']."</td><td>".$arr['jknnonpbi']."</td><td>".$arr['asuransilain']."</td><td>".$arr['jamkesos']."</td><td>".$arr['jampersal']."</td><td>".$arr['jasaraharja']."</td><td>".$arr['bpjstenagakerja']."</td><td>".$arr['total']."</td></tr>";
        }
    }
    echo '</tfoot>
    </table>';
    
    echo '<table class="table table-bordered table-hover table-striped" style="width:60%;">
        <thead>
            <tr class="header-table-ql"><th rowspan="2">NO</th><th rowspan="2">JENIS</th>';
    foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 1 ) {echo "<th colspan='5'>".$arr['jenisoperasi']."</th>";} }
    
    echo '</tr>';
    echo '<tr class="header-table-ql">';
    foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 1 ) { foreach ($jenisoperasi as $arr){ if($arr['jeniskategori'] == 0 ) {echo "<th>".$arr['jenisoperasi']."</th>";} } }}
    echo '</tr>
        </thead>
        <tbody>';
            $no = 0;
            //loop jenis tindakan
            foreach ($jenistindakan as $arr)
            {
                echo "<tr><td>". ++$no."</td><td>".$arr['jenistindakan']."</td>";

                //set to array id jenis operasi
                $idjenisoperasi = explode(',', $arr['jenisoperasi']);

                //loop jenisoperasi kategori 1

                foreach ($jenisoperasi as $arr1)
                { 
                    if($arr1['jeniskategori'] == 1 )
                    {
                        //loop jenisoperasi kategori 0

                        foreach ($jenisoperasi as $arr2)
                        {
                            $jml_kategori0 = 0;
                            if($arr2['jeniskategori'] == 0 )
                            {
                                if(in_array($arr2['idjenisoperasi'],$idjenisoperasi))
                                {
                                   ++$jml_kategori0; 
                                }
                                echo "<td>".$jml_kategori0."</td>";
                            }
                        }
                    } 

                }
            }
            
       echo ' </tfoot>
    </table>';
       
       
    //laporan dokter anastesi
       
    echo "<h3>Laporan Dr Anastesi</h3>";
    echo '<table class="table table-bordered table-hover table-striped" style="width:60%;">
        <thead>
            <tr class="header-table-ql"><th>Nama Dokter</th><th>Jumlah</th></tr>
        </thead>
        <tbody>';
        $total = 0;
        foreach ($dranastesi as $arr)
        {
            $total += intval($arr['jumlah']);
            echo "<tr><td>".$arr['namadokter']."</td><td>".$arr['jumlah']."</td></tr>";
        }
        echo "<tr><td>Total</td><td>".$total."</td></tr>";
    echo '</tfoot></table>';
    
    //Laporan Sc dengan Dr spesialis Anak/Umum 
    echo "<h3>Laporan Sc dengan Dr spesialis Anak/Umum</h3>";
    echo '<table class="table table-bordered table-hover table-striped" style="width:60%;">
            <thead>
                <tr class="header-table-ql"><th>Nama Dokter</th><th>Jumlah</th></tr>
            </thead>
            <tbody>';
            $total = 0;
            foreach ($laporansc as $arr)
            {
                $total += intval($arr['jumlah']);
                echo "<tr><td>".$arr['namadokter']."</td><td>".$arr['jumlah']."</td></tr>";
            }
            echo "<tr><td>Total</td><td>".$total."</td></tr>";
    echo '</tfoot></table>';
?>