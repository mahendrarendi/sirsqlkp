<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo "<h1>Pendapatan Rawat Jalan RSU Queen Latifa</h1>";
echo "<table border='1'>";
        $hari = date('N', strtotime($date));
        echo "<tr><td>Hari</td><td>: ".harisistem( $hari)."</td></tr>";
        echo "<tr><td>Tanggal</td><td>: ".$date. ((!empty($date2) && $date!=$date2) ? ' sampai '.$date2 : '' )." </td></tr>";
        echo "<tr><td>Unit</td><td>: ".$unit['namaunit']."</td></tr>";
        echo "<tr><td>Cara Bayar</td><td>: ".$carabayar[0]."</td></tr>";
        for($a=0; $a<count($carabayar); $a++){
            echo '<tr class="bg-blue"><th colspan='. (count($jenistarif) + 9) .'>'.strtoupper($carabayar[$a]).'</th></tr>'; 
            echo '<tr><th rowspan="2">No</th><th rowspan="2">NAMA PASIEN</th><th rowspan="2">DOKTER DPJP</th><th colspan='. (count($jenistarif)+6) .'>BIAYA</th></tr>';
            $jnstarif='';$b=0;
            foreach ($jenistarif as $obj){ $jnstarif .= "<td>".$obj->jenistarif."</td>";}
            echo '<tr>',$jnstarif,'<td>Jaminan</td><td>NonJaminan</td><td>Potongan</td><td>TotalTagihan</td><td>Transfer</td><td>Selisih</td></tr>';
            $no=0; 
            $totTagihan=0; 
            $totPotongan=0; 
            $totTransfer=0; 
            $selisih=0; 
            $totSelisih=0; 
            $totJaminan=0; 
            $totNonjaminan=0;
            
            foreach ($pasien[$a] as $objpasien){
//                var_dump($objpasien);
                $tarif=''; 
                $totaltarif=0;
                $selisih=0; 
                $jaminan=0; 
                $nonjaminan=0;
                for ($c=0; $c<count($tindakan[$a][$no]); $c++){
                    
                    $total= $tindakan[$a][$no][$c][0]['total'] ;
                    $tarif.= '<td>'.intval($total).'</td>';
                    $jaminan += (($tindakan[$a][$no][$c][0]['jaminanasuransi']=='1') ? $total : 0 );
                    $nonjaminan += (($tindakan[$a][$no][$c][0]['jaminanasuransi']=='0') ? $total : 0 );
                    $totaltarif += $total;
                }
                $totPotongan += $objpasien->potongan; 
                $totTransfer += $objpasien->transfer;
                $nominal = (($jaminan + $nonjaminan) - $objpasien->potongan);
                $totTagihan += $nominal;
                $totJaminan += $jaminan;
                $totNonjaminan += $nonjaminan;
                $selisih = $totaltarif - ($jaminan + $nonjaminan);
                $totSelisih += $selisih;
                
                echo "<tr><td>".++$no."</td><td>".$objpasien->namalengkap."</td><td>".$objpasien->namadokter."</td>".$tarif."<td>".$jaminan."</td><td>".$nonjaminan."</td><td>".$objpasien->potongan."</td><td>".$totaltarif."</td><td>".$objpasien->transfer."</td><td>".$selisih."</td></tr>";
            }
            echo '<tr class="bg bg-gray"><td colspan="'. (count($jenistarif) + 3) .'">Total</td><td>'.$totJaminan.'</td><td>'.$totNonjaminan.'</td><td>'.$totPotongan.'</td><td>'.$totTagihan.'</td><td>'.$totTransfer.'</td><td>'.$totSelisih.'</td></tr>';
        }
echo "</table>";
//print_r($mode);
//print_r($title);
//var_dump( $carabayar);
//print_r($date);
//print_r($jenistarif);
?>

    




