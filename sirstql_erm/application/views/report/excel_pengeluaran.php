<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table>
  <tr>
    <td colspan = "8" align ="center"><strong>Pengeluaran Periode <?=$start?> s/d <?=$end?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table border="1">
  
  <tr bgcolor="02BD00">
    <td>KATEGORI</td>
    <td>TANGGAL TRANSAKSI</td>
    <td>NO.TRANSAKSI/NOTA</td>
    <td>PENJUAL</td>
    <td>TOTAL BIAYA</td>
    <td>PPN</td>
    <td>CATATAN</td>
    <td>KETERANGAN</td>
    <td>PEMBELI</td>
  </tr>
<?php 
$sum = 0;
if(!empty($pengeluaran) && $pengeluaran!=null)
{
    foreach ($pengeluaran as $arr) {
      if ($arr['ppn']=='11') {
        $nilaippn = $arr['nominalbiaya']*0.11;
      } else {
        $nilaippn = '0';
      }
      echo "<tr><td>".$arr['kategoribiaya']."</td><td>".$arr['tanggal']."</td><td>".$arr['notransaksi']."</td><td>".$arr['penjual']."</td><td>".number_format($arr['nominalbiaya'])."</td><td>".number_format($nilaippn)."</td><td>".$arr['catatan']."</td><td>".$arr['keterangan']."</td><td>".$arr['pembeli']."</td></tr>";

      $sum += $arr['nominalbiaya']+$nilaippn;
    }
}
?>
 <tr>
    <td colspan="4"><strong>TOTAL</strong></td>
    <td><strong><?= $sum ?></strong></td>
  </tr>
</table>
