<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");

$rows='';
$no     = 0;
$masuk  = 0;
$keluar = 0;
$namabarang = '';
if(!empty($laporan))
{
    foreach ($laporan as $obj) 
    {
        if($obj->jenisdistribusi == 'masuk' OR $obj->jenisdistribusi == 'pembetulanmasuk'){
            if($obj->jenisdistribusi == 'masuk'){
                $masuk += (float) $obj->jumlah;
            }else{
                $masuk -= (float) $obj->jumlah;
            }
        }
        
        if($obj->jenisdistribusi == 'keluar' OR $obj->jenisdistribusi == 'pembetulankeluar'){
            if($obj->jenisdistribusi == 'keluar'){
                $keluar += (float) $obj->jumlah;
            }else{
                $keluar -= (float) $obj->jumlah;
            }
        }
        
        
        if($namabarang != '' && $namabarang != $obj->namabarang)
        {
            $rows .='<tr><td>'. ++$no .'</td><td>'. $obj->namabarang .'</td><td>'. $masuk .'</td><td>'. $keluar .'</td><td>'. $obj->namasatuan .'</td></tr>';
            $masuk = 0;
            $keluar = 0;
        }
        $namabarang = $obj->namabarang;
    }
}
echo '<h3>'.$judul.'</h3>';
echo '<table  border="1" cellspacing="0" width="100%">'
    . '<thead>'
        . '<tr><td>No</td><td>Nama Barang</td><td>Masuk</td><td>Keluar</td><td>Satuan</td></tr></thead>'
        . '<tbody>',$rows,'</tfoot>'
    . '</table>';
?>