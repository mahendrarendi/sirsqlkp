<?php 
$paperOrientation='potrait';
$htmlText = ' <style>.setTable{font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;border:none;}"</style> <div style="top:0px;"><img width="50%;" src="'.base_url().'assets/images/headerkuitansismall.jpg" ></div><div style="float: none;"><table class="setTable" >';
if ($infotagihan != null)
{
    foreach ($infotagihan as $obj) {
        # code...
    $htmlText .='<tr><td width="10px">Nama</td><td>: '.$obj->namalengkap.'</td></tr>';
    $htmlText .='<tr><td width="10px">Alamat</td><td>: '.$obj->alamat.'</td></tr>';
    $htmlText .='<tr><td width="10px">Penanggung</td><td>: '.$obj->penanggung.'</td></tr>';
    $htmlText .='<tr><td width="10px">No RM</td><td>: '.$obj->norm.'</td></tr>';
    $fileName = $obj->norm;
    }
}
else
{
    $fileName = 'UNKNOWN';
}
$htmlText.='</table><table class="setTable" border="0" style="padding:2px;" border-collapse: collapse">';
$htmlText .= '<tr><td>No/Item</td><td >Jml</td><td>Nominal</td></tr>';
$no=0;$subtotal  = 0;$total=0;$tipesebelum='';
foreach ($detailtagihan as $obj) {
    if ($tipesebelum != $obj->jenistarif)
    {
        if ($tipesebelum != '') $htmlText .= '<tr><td width="30px" align="right" colspan="2">:: Subtotal ::</td><td align="left">'.$subtotal.'</td></tr>';
        $htmlText .= '<tr><td colspan="3">'.$obj->jenistarif.'</td></tr>';
        $subtotal  = 0;
    }
    $subtotal += (intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan)/intval($obj->kekuatanbarang));
    $total    += (intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan)/intval($obj->kekuatanbarang));
    $htmlText .= '<tr><td width="30px">'. ++$no .'. '.$obj->nama.((intval($obj->kekuatan)/intval($obj->kekuatanbarang)>1)? ' ['.intval($obj->kekuatan).'/'.intval($obj->kekuatanbarang).']' :'' ).'</td><td style="font-size:x-small">'.intval($obj->jumlah) .'</td><td>'. number_format_dot((intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan) / intval($obj->kekuatanbarang))).'</td></tr>'; 
    $tipesebelum = $obj->jenistarif;
}
    $htmlText .= '<tr><td width="30px" align="right" colspan="2">:: Subtotal ::</td><td align="left">'.number_format_dot($subtotal).'</td></tr>';
    $htmlText .= '<tr><td width="30px" align="right" colspan="2">== Total ==</td><td>'.number_format_dot($total).'</td></tr>';
    $htmlText .= '</table><table class="setTable">';
$selisih=0;$waktutagih='';
foreach ($infotagihan as $obj) {
$selisih   = $obj->nominal - $obj->dibayar;
$htmlText .= '<tr><td>No Nota</td><td>'.$obj->idtagihan.'</td></tr>';
$htmlText .= '<tr><td>Waktu Tagih</td><td>'.$obj->waktutagih.'</td></tr>';
$htmlText .= '<tr><td>Waktu Bayar</td><td>'.$obj->waktubayar.'</td></tr>';
$htmlText .= '<tr><td>Nominal</td><td>'.number_format_dot($obj->nominal).'</td></tr>';//$obj->nominal)
$htmlText .= '<tr><td>Dibayar</td><td>'.number_format_dot($obj->dibayar).'</td></tr>';
$htmlText .= '<tr><td>Selisih</td><td>'.number_format_dot($selisih).'</td></tr>';
$htmlText .= '<tr><td>Jenis Tagihan</td><td>'.$obj->jenistagihan.'</td></tr>';
$waktutagih=$obj->waktutagih;
}
$fileName .= '-'.$waktutagih;
$htmlText .= '<tr><td colspan="2" align="center">dicetak oleh '.$this->session->userdata('username').', pada '.date("Y-m-d H:i:s").'</td></tr>';
$htmlText .= '</table></div>';
ob_start();
$htmlTxt=$htmlText;
ob_end_clean();
CI()->load->library('pdf');
CI()->pdf->base_html($htmlTxt, $fileName,'', $paperOrientation);
?>