  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                
                <div class="col-xs-6  col-md-2">
                <div class="input-group">
                    <input style="height:34px;"  type="text" class="form-control datepicker" name="hari">
                    <div class="input-group-addon">
                        <input type="radio" class="flat-red" name="order" value="hari" checked/>
                    </div>
                  </div>
            </div>
            <div class="col-xs-6  col-md-2">
                <div class="input-group">
                    <input style="height:34px;"  type="text" class="form-control datepicker" name="bulan">
                    <div class="input-group-addon">
                        <input type="radio" class="flat-red" name="order" value="bulan"/>
                    </div>
                  </div>
            </div>
                <a onclick="generatedata()"  class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <!--<a onclick="generatedata('unduh')" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>-->
              </div>
              <h3>Laporan <?= $title_page ?></h3>
              <div id="tampildata"></div>
            </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        