<style>
.ql-inline {
    display: inline-block;
    width: calc(55% - 40px);
}
.ql-group {
    margin-bottom: 15px;
}
.ql-inline .form-group {
    padding: 0 5px;
}
.title-content-table {
    text-align: center;
}

.title-content-table h3 {
    margin-top: 0;
    margin-bottom: 15px;
}
.wrap-button button {
    margin: 0 2px;
}
.animasi-loading{
  -webkit-animation: fa-spin 2s infinite linear;
  animation: fa-spin 2s infinite linear;
}
</style>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form" id="frm-filterwaktu">
                                <div class="filter-waktu">
                                    <label>Filter : </label>
                                    <div class="ql-group">
                                        <div class="ql-inline">
                                            <div class="form-group">
                                                <input name="filterwaktu" value="<?= date('d-m-Y'); ?>" type="text" autocomplete="off" class="form-control ql-dateday">
                                            </div>
                                        </div>
                                        <div class="ql-inline">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-sm btn-primary" name="filter"><i class="fa fa-search"></i> Cari</button>
                                                <a href="<?= base_url('creport/waktupendaftaran'); ?>" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Refresh</a>
                                                <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-content">
                        <?= $filterwaktu; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>
    
</section>