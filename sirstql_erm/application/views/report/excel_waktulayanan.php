<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo "<h3>".$judul."</h3>";
$namaunit= '';
$countshow = 0;
$no     = 0;
echo  "<table border='1'>";
if(empty($laporan))
{
    echo "<tr><td colspan='5'>Data Tidak Ada</td></tr>";
}
else 
{
    foreach ($laporan as $obj) 
    {
       
       if($obj->namaunit != $namaunit || $countshow == 0)
       {
           $no = 0;
           $countshow++;
           echo  '<tr><th colspan="23"><center>'.$obj->namaunit.'</center></th></tr>';       

            echo '<tr>
            <th>NO</th>
            <th>ID Pendaftaran</th>
            <th>NORM</th>
            <th>Nama Pasien</th>
            <th>Tanggal Periksa</th>
            <th>Tanggal Layanan</th>
            <th>Waktu Mulai Pendaftaran</th>
            <th>Waktu Selesai Pendaftaran</th>
            <th>Lama Pendaftaran</th>
            <th>Petugas Pendaftaran</th>
           
            <th>Waktu Mulai Pemeriksaan Poli</th>
            <th>Waktu Selesai Pemeriksaan Poli</th>
            <th>Lama Pemeriksaan Poli</th>
            <th>Petugas Layanan Pemeriksaan Poli</th>

            <th>Waktu Mulai Layanan Pemeriksaan Dokter</th>
            <th>Waktu Selesai Layanan Pemeriksaan Dokter</th>
            <th>Lama Layanan Pemeriksaan Dokter</th>
            <th>Petugas Layanan Pemeriksaan Dokter</th>

            
            <th>Waktu Mulai Pemeriksaan Radiologi</th>
            <th>Waktu Selesai Pemeriksaan Radiologi</th>
            <th>Lama Pemeriksaan Radiologi</th>
            <th>Petugas Pemeriksaan Radiologi</th>

            <th>Waktu Mulai Pemeriksaan UGD</th>
            <th>Waktu Selesai Pemeriksaan UGD</th>
            <th>Lama Pemeriksaan UGD</th>
            <th>Petugas Pemeriksaan UGD</th>


            <th>Waktu Mulai Pemeriksaan Farmasi</th>
            <th>Waktu Selesai Pemeriksaan Farmasi</th>
            <th>Lama Pemeriksaan Farmasi</th>
            <th>Petugas Pemeriksaan Farmasi</th>

            <th>Waktu Mulai Penyeran Obat Farmasi</th>
            <th>Waktu Selesai Penyeran Obat Farmasi</th>
            <th>Lama Penyeran Obat Farmasi</th>
            <th>Petugas Penyeran Obat Farmasi</th>

            <th>Waktu Mulai Layanan Kasir</th>
            <th>Waktu Selesai Layanan Kasir</th>
            <th>Lama Layanan Kasir</th>
            <th>Petugas Layanan Kasir</th>
            <th>Jenis Obat</th>
            </tr>';
    
        }
        $tanggallayanan = '';
        $sql ="select max(x.tanggallayanan) as tanggallayanan from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran='".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."'  order by x.tanggallayanan , x.waktu asc";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $tanggallayanan .= '<li>'.$row->tanggallayanan.'</li>';
            }
        }

        //Pendaftaran
        $waktumulaipendaftaran = '';
        $waktuselesaipendaftaran = '';
        $waktulayanan = '';
        $namauser = '';
        $sqlwaktumulaidanseelesaipendaftaran = "select max(x.waktu), x.waktumulai, x.waktuselesai, x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%pendaftaran%' ORDER BY `x`.`waktu` DESC";
        $querywaktumulaidanselesaipendaftaran = $this->db->query($sqlwaktumulaidanseelesaipendaftaran);
        if ($querywaktumulaidanselesaipendaftaran->num_rows() > 0) {
            foreach ($querywaktumulaidanselesaipendaftaran->result() as $row) {
                $waktumulaipendaftaran .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaipendaftaran .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayanan .= '<li>'.$row->waktulayanan.'</li>';
                $namauser .= '<li>'.$row->namauser.'</li>';
            }
        }
        
        //Poli
        $waktumulaipoli = '';
        $waktuselesaipoli = '';
        $waktulayananpoli = '';
        $namauserpoli = '';
        $sqlpoli = "select max(x.waktu),x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%poli%' ORDER BY `x`.`waktu` DESC";
        $querypoli = $this->db->query($sqlpoli);
        if ($querypoli->num_rows() > 0) {
            foreach ($querypoli->result() as $row) {
                $waktumulaipoli .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaipoli .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayananpoli .= '<li>'.$row->waktulayanan.'</li>';
                $namauserpoli .= '<li>'.$row->namauser.'</li>';
            }
        }

        //Dokter
        $waktumulaidokter = '';
        $waktuselesaidokter = '';
        $waktulayanandokter = '';
        $namauserdokter = '';
        $sqldokter = "select max(x.waktu),x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%dokter%' ORDER BY `x`.`waktu` DESC";
        $querydokter = $this->db->query($sqldokter);
        if ($querydokter->num_rows() > 0) {
            foreach ($querydokter->result() as $row) {
                $waktumulaidokter .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaidokter .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayanandokter .= '<li>'.$row->waktulayanan.'</li>';
                $namauserdokter .= '<li>'.$row->namauser.'</li>';
            }
        }

        // Radiologi
        $waktupemeriksaanradiologi = '';
        $waktuselesaipemeriksaanradiologi = '';
        $waktulayananpemeriksaanradiologi = '';
        $namauserpemeriksaanradiologi = '';
        $sqlpemeriksaanradiologi = "select max(x.waktu),x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%radiologi%' ORDER BY `x`.`waktu` DESC";
        $querypemeriksaanradiologi = $this->db->query($sqlpemeriksaanradiologi);
        if ($querypemeriksaanradiologi->num_rows() > 0) {
            foreach ($querypemeriksaanradiologi->result() as $row) {
                $waktupemeriksaanradiologi .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaipemeriksaanradiologi .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayananpemeriksaanradiologi .= '<li>'.$row->waktulayanan.'</li>';
                $namauserpemeriksaanradiologi .= '<li>'.$row->namauser.'</li>';
            }
        }

        //UGD
        $waktupemeriksaanugd = '';
        $waktuselesaipemeriksaanugd = '';
        $waktulayananpemeriksaanugd = '';
        $namauserpemeriksaanugd = '';
        $sqlpemeriksaanugd = "select max(x.waktu),x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%ugd%' ORDER BY `x`.`waktu` DESC";
        $querypemeriksaanugd = $this->db->query($sqlpemeriksaanugd);
        if ($querypemeriksaanugd->num_rows() > 0) {
            foreach ($querypemeriksaanugd->result() as $row) {
                $waktupemeriksaanugd .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaipemeriksaanugd .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayananpemeriksaanugd .= '<li>'.$row->waktulayanan.'</li>';
                $namauserpemeriksaanugd .= '<li>'.$row->namauser.'</li>';
            }
        }


        //Pemeriksaan Farmasi
        $waktumulaifarmasi = '';
        $waktuselesaifarmasi = '';
        $waktulayananfarmasi = '';
        $namauserfarmasi = '';
        $sqlfarmasi = "select max(x.waktu),x.idpendaftaran, x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%pemeriksaanfarmasi%' ORDER BY `x`.`waktu` DESC";
        $queryfarmasi = $this->db->query($sqlfarmasi);
        if ($queryfarmasi->num_rows() > 0) {
            foreach ($queryfarmasi->result() as $row) {
                $waktumulaifarmasi .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaifarmasi .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayananfarmasi .= '<li>'.$row->waktulayanan.'</li>';
                $namauserfarmasi .= '<li>'.$row->namauser.'</li>';
            }
        }

        //Penyerahan Obat Farmasi
        $waktumulaipenyerahanobatfarmasi = '';
        $waktuselesaipenyerahanobatfarmasi = '';
        $waktulayananpenyerahanobatfarmasi = '';
        $namauserpenyerahanobatfarmasi = '';
        $sqlpenyerahanobatfarmasi = "select max(x.waktu),x.idpendaftaran, x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%penyerahanresepfarmasi%' ORDER BY `x`.`waktu` DESC";
        $querypenyerahanobatfarmasi = $this->db->query($sqlpenyerahanobatfarmasi);
        if ($querypenyerahanobatfarmasi->num_rows() > 0) {
            foreach ($querypenyerahanobatfarmasi->result() as $row) {
                $waktumulaipenyerahanobatfarmasi .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaipenyerahanobatfarmasi .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayananpenyerahanobatfarmasi .= '<li>'.$row->waktulayanan.'</li>';
                $namauserpenyerahanobatfarmasi .= '<li>'.$row->namauser.'</li>';
            }
        }

        //Kasir
        $waktumulaikasir = '';
        $waktuselesaikasir = '';
        $waktulayanankasir = '';
        $namauserkasir = '';
        $jenisracikan = '';
        $sqlkasir = "SELECT max(x.waktu),x.idpendaftaran, x.kegiatan, x.waktumulai,x.waktuselesai,x.waktulayanan, xx.namauser,CASE WHEN pp.adaracikan ='1' THEN 'Racikan' ELSE 'Non-Racikan' END AS racik from rs_waktu_layanan x join login_user xx on xx.iduser = x.iduser join person_pendaftaran pp on pp.idpendaftaran = x.idpendaftaran where x.idpendaftaran = '".$obj->idpendaftaran."' and x.tanggalperiksa = '".$obj->tanggalperiksa."' and x.kegiatan like '%selesaiperiksa%' ORDER BY `x`.`waktu` DESC";
        $querykasir = $this->db->query($sqlkasir);
        if ($querykasir->num_rows() > 0) {
            foreach ($querykasir->result() as $row) {
                $waktumulaikasir .= '<li>'.$row->waktumulai.'</li>';
                $waktuselesaikasir .= '<li>'.$row->waktuselesai.'</li>';
                $waktulayanankasir .= '<li>'.$row->waktulayanan.'</li>';
                $namauserkasir .= '<li>'.$row->namauser.'</li>';
                $jenisracikan .= '<li>'.$row->racik.'</li>';
            }
        }


        echo  '<tr>
        <td>'.++$no.'</td>
        <td>'.$obj->idpendaftaran.'</td>
        <td>'.$obj->norm.'</td>
        <td>'.$obj->namapasien.'</td>
        <td>'. $obj->tanggalperiksa .'</td>
        <td>'. $tanggallayanan.'</td>

        <td>'. $waktumulaipendaftaran.'</td>
        <td>'. $waktuselesaipendaftaran.'</td>
        <td>'. $waktulayanan.'</td>
        <td>'. $namauser.'</td>

        <td>'. $waktumulaipoli.'</td>
        <td>'. $waktuselesaipoli.'</td>
        <td>'. $waktulayananpoli.'</td>
        <td>'. $namauserpoli.'</td>

        <td>'. $waktumulaidokter.'</td>
        <td>'. $waktuselesaidokter.'</td>
        <td>'. $waktulayanandokter.'</td>
        <td>'. $namauserdokter.'</td>

        <td>'. $waktupemeriksaanradiologi.'</td>
        <td>'. $waktuselesaipemeriksaanradiologi.'</td>
        <td>'. $waktulayananpemeriksaanradiologi.'</td>
        <td>'. $namauserpemeriksaanradiologi.'</td>

        <td>'. $waktupemeriksaanugd.'</td>
        <td>'. $waktuselesaipemeriksaanugd.'</td>
        <td>'. $waktulayananpemeriksaanugd.'</td>
        <td>'. $namauserpemeriksaanugd.'</td>

        <td>'. $waktumulaifarmasi.'</td>
        <td>'. $waktuselesaifarmasi.'</td>
        <td>'. $waktulayananfarmasi.'</td>
        <td>'. $namauserfarmasi.'</td>

        <td>'. $waktumulaipenyerahanobatfarmasi.'</td>
        <td>'. $waktuselesaipenyerahanobatfarmasi.'</td>
        <td>'. $waktulayananpenyerahanobatfarmasi.'</td>
        <td>'. $namauserpenyerahanobatfarmasi.'</td>
        
        <td>'. $waktumulaikasir.'</td>
        <td>'. $waktuselesaikasir.'</td>
        <td>'. $waktulayanankasir.'</td>
        <td>'. $namauserkasir.'</td>
        <td>'. $jenisracikan.'</td>
        
        </tr>';
        $namaunit = $obj->namaunit;
    }
    
}

echo "</table>";
?>
