<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs" id="navactivegrafikdemografi">
      <li class="active"><a onclick="jenistampilan('pasien')" class="btn" data-toggle="tab" aria-expanded="true">Pasien</a></li>
      <li class=""><a onclick="jenistampilan('wilayah')" class="btn" data-toggle="tab" aria-expanded="false">Wilayah</a></li>
    </ul>

    <div class="tab-content">
      <div class="tab-pane active">
        <div class="box-body">
          
          <div class="col-xs-12 col-md-6">
            <!-- range tanggal -->
            <label class="label-control col-xs-4 col-md-3" style="text-align: right;">Range Tanggal</label>
            <div class="col-xs-4 col-md-3" style="padding-right: 0px;"><input id="tgl1" type="text" class="form-control datepicker"></div>
            <div class="col-xs-4 col-md-3"  style="padding-left: 0px;"><input id="tgl2" type="text" class="form-control datepicker"></div>
          </div>
          
          <div class="col-xs-12"></div>
            <div class="col-xs-12 col-md-6" style="margin-top: 4px;">
              <!-- jenis laporan -->
              <label class="label-control col-xs-4 col-md-3" style="text-align: right;">Jenis Laporan</label>
              <div class="col-xs-8 col-md-6">
              <select name="jenislaporan" class="form-control">
                <option>Agama</option>
                <option>Jenis Kelamin</option>
                <option>Pendidikan</option>
                <option>Pekerjaan</option>
                <option>Status Pernikahan</option>
                <option>Usia</option>
              </select> 
              </div>
            </div>

            <div class="col-xs-12"></div>

            <div class="col-xs-12 col-md-6" style="margin-top: 4px;">
              <!-- Tampilkan laporan -->
              <label class="label-control col-xs-4 col-md-3" style="text-align: right;">&nbsp;</label>
              <div class="col-xs-8 col-md-6">
              <a class="btn btn-xs btn-primary" onclick="getdataDemografiPasien()"><i class="fa fa-pie-chart"></i> Grafik</a>
              <a class="btn btn-xs btn-success" onclick="getdataDemografiPasien('tabel')"><i class="fa fa-table"></i> Tabel</a> 
              <a class="btn btn-xs btn-warning" onclick="resetDemografiPasien()"><i class="fa fa-refresh"></i> Reset</a>
              </div>
            </div>
      </div>
      </div>
    </div>

    <div class="row" style="padding-bottom: 30px;">
      <div class="col-xs-12" style="margin-bottom:20px;">
        <div  id="demografipasien">
            <div class="box-body">
              <p class="text-center text-bold">Demografi Pasien Berdasarkan Jenis Kelamin <br> Tanggal Pendaftaran Pasien : <br> Total Kunjungan :  </p>
              <div class="chart">
                <canvas id="chart_demografikunjunganpasien" style="height:300px"></canvas>
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>










