<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
<tr><th colspan="29"><b>DATA INVENTARIS</b></th></tr>
<tr>
    <th>No</th><th>NAMA ALAT</th><th>MEREK</th><th>TIPE</th><th>NO.SERI</th><th>RUANGAN</th><th>NO.INVENTARIS</th><th>NO.sertifikat</th><th>TANGGAL PEMBELIAN</th><th>NILAI ASET</th><th>PENYUSUTAN</th><th>STATUS</th>
</tr>
  <?php
    if(empty(!$inventaris)){
        $no=0;
        foreach ($inventaris as $obj) {
            echo "<tr>";
            echo "<td>". ++$no ."</td>";
            echo "<td>".$obj->namabaranginventaris."</td>";
            echo "<td>".$obj->merekbarang."</td>";
            echo "<td>".$obj->modelbarang."</td>";
            echo "<td>".$obj->nomorseri."</td>";
            echo "<td>".$obj->namablok .' ['. $obj->namaruanginventaris.']'."</td>";
            echo "<td>".$obj->nomorinventaris."</td>";
            echo "<td>".$obj->nomorsertifikat."</td>";
            echo "<td>".$obj->tanggalpembelian."</td>";
            echo "<td>".$obj->nilaiaset."</td>";
            echo "<td>".$obj->nilaipenyusutan."</td>"; 
            echo "<td>".$obj->status."</td>";
            echo "</tr>";
        }
    } 
  ?>
</table>