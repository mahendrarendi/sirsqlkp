  <!-- Main content -->
  <style>
.box.box-solid.box-default>.box-header {
    color: #efefef;
    background: #d2d6de;
    background-color: #888888;
}</style>
    <section class="content">
        <section class="col-xs-12">
            
            <!--Laporan Penggunaan Resep Bulanan-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Laporan Penggunaan Resep Bulanan Rawat Jalan</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-2">
                        <input type="text" size="7" class="form-control tahunbulan" id="tahunbulan_lp_resepbulanan" name="tahunbulan">
                    </div>
                    <a class="btn btn-info btn-sm" onclick="tampil_laporanpenggunaanresepbulanan()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_laporanpenggunaanresepbulanan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                  </div>
                <div class="box-body" id="view_laporanpenggunaanresepbulanan">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Tanggal</th>
                      <th>Jumlah Pasien</th>
                      <th>Jumlah Racikan atau Puyeran (R)</th>
                      <th>Jumlah Obat (R)</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="4">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <!--Laporan Distribusi Gudang-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Laporan Distribusi Gudang</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-2">
                        <input type="text" size="7" class="form-control tahunbulan" id="tahunbulan_distribusigudang" name="tahunbulan">
                    </div>
                    <a class="btn btn-info btn-sm" onclick="tampil_laporandistribusigudang()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_laporandistribusigudang()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                  </div>
                <div class="box-body" id="view_laporandistribusigudang">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Nama Barang</th>
                      <th>Masuk</th>
                      <th>Keluar</th>
                      <th>Satuan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="4">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <!--Laporan Distribusi Gudang-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Laporan Detail Distribusi Gudang</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-1">
                        <input type="text" size="7" class="datepicker" id="lddg_tanggal1" name="tanggal1">
                    </div>
                    
                    <div class="col-xs-12 col-md-1">
                        <input type="text" size="7" class="datepicker" id="lddg_tanggal2" name="tanggal2">
                    </div>
                    <a class="btn btn-info btn-sm" onclick="tampil_laporandetaildistribusigudang()"><i class="fa fa-desktop"></i></a>
                  </div>
                <div class="box-body" id="view_laporandetaildistribusigudang">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Nama Barang</th>
                      <th>Masuk</th>
                      <th>Keluar</th>
                      <th>Satuan</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="4">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <!--Laporan Penggunaan Obat Rawat Jalan Perbulan-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Laporan Penggunaan Obat Rawat Jalan Per Bulan</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-2">
                        <input type="text" size="7" class="form-control tahunbulan" id="tahunbulan_laporan1" name="tahunbulan">
                    </div>
                    <a class="btn btn-info btn-sm" onclick="tampil_laporanpenggunaanobatperbulan()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_laporanpenggunaanobatperbulan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                  </div>
                <div class="box-body" id="view_laporan1">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Nama Barang</th>
                      <th>Resep</th>
                      <th>Bebas</th>
                      <th>Jumlah</th>
                      <th>Harga Beli</th>
                      <th>Harga BPJS</th>
                      <!-- <th>Subtotal</th> -->
                      <th>Harga Umum</th>
                      <!-- <th>Subtotal</th> -->
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="8">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>

            <!--Laporan Penggunaan Obat Rawat Inap Perbulan-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Laporan Penggunaan Obat Rawat Inap Per Bulan</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-2">
                        <input type="text" size="7" class="form-control tahunbulan" id="tahunbulan_laporanObatRanap" name="tahunbulan">
                    </div>
                    <a class="btn btn-info btn-sm" onclick="tampil_laporanpenggunaanobatperbulanRanap()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_laporanpenggunaanobatperbulanRanap()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                  </div>
                <div class="box-body" id="view_laporanObatRanap">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Nama Barang</th>
                      <th>Jumlah</th>
                      <th>Harga Beli</th>
                      <th>Harga BPJS</th>
                      <th>Harga Umum</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="8">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <!--Penggunaan BHP/Alkes Include Tarif Tindakan-->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Penggunaan BHP/Alkes Include Tarif Tindakan</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-4">
                        <select id="pilihunitbarang_includetindakan" style="width:49%;">
                            <?php 
                            foreach ($unitfarmasi as $arr)
                            {
                                echo "<option value='".$arr['idunit']."'>".$arr['namaunit']."</option>";
                            }
                            ?>
                        </select>
                        <select id="pilihbarang_includetindakan" style="width:49%;">
                            <option value="">Pilih Barang</option>
                        </select>
                    </div>
                    <div class="col-xs-12 col-md-1">
                        <input type="text" size="7" class="datepicker" id="min_includetindakan" />
                    </div>
                    <div class="col-xs-12 col-md-1">
                        <input size="7" type="text" class="datepicker" id="max_includetindakan" />
                    </div>
                    <a id="tampil_penggunaanbarangincludetindakan" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
                    <a id="excel_penggunaanbarangincludetindakan" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                    <a id="reload_penggunaanbarangincludetindakan" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
                </div>
                <div class="box-body">
                  <table id="dtpenggunaanbarangincludetindkaan" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Waktu Input</th>
                      <th>Diinput Oleh</th>
                      <th>Barang</th>
                      <th>Jumlah</th>
                      <th>Satuan</th>
                      <th>Jenis</th>  
                      <th>Dosis</th>
                      <th>Harga</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="8">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Penjualan Obat Resep</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-4">
                        <select id="pilihpasien" style="width:49%;"></select> 
                        <select id="pilihobat" style="width:49%;"></select>
                    </div>
                    <div class="col-xs-12 col-md-1">
                        <input type="text" size="7" class="datepicker" id="min" />
                    </div>
                    <div class="col-xs-12 col-md-1">
                        <input size="6" type="text" class="datepicker" id="max" />
                    </div>
                    <a class="btn btn-info btn-sm" onclick="pemakaianobat.ajax.reload()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_pemakaianobat()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                    <a onclick="reloadpemakaianobat()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
                </div>
                <div class="box-body">
                  <table id="pemakaianobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Waktu</th>                      
                      <th>Pasien</th>
                      <th>Barang</th>
                      <th>Jumlah</th>
                      <th>Satuan</th>
                      <th>Jenis</th>  
                      <th>Dosis</th>
                      <th>Harga</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr><td colspan="7">No data available in table</td></tr>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Penjualan Obat Bebas</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div style="padding-top:4px;">
                    <div class="col-xs-12 col-md-3"><select id="pilihobatbebas" style="width:100%;"></select></div>
                    <div class="col-xs-12 col-md-1"><input type="text" size="7" class="datepicker" id="tgl1obatbebas" /></div>
                    <div class="col-xs-12 col-md-1"><input size="7" type="text" class="datepicker" id="tgl2obatbebas" /> </div>
                    <a class="btn btn-info btn-sm" onclick="pembelianobatbebas.ajax.reload()"><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_pemakaianobatbebas()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                    <a onclick="reloadpembelianobatbebas()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
                  </div>
                <div class="box-body">
                  <table id="pembelianobatbebas" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Waktu</th>
                      <th>Barang</th>
                      <th>Jumlah</th>
                      <th>Satuan</th>
                      <th>Jenis</th>  
                      <th>Dosis</th>
                      <th>Harga</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Belanja Obat</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                <div class="col-md-12">

                  <div class="col-xs-12 col-md-1">
                      <input type="text" size="7" class="datepicker" id="tgl1BelanjaObat" />
                  </div>
                  <div class="col-xs-12 col-md-1">
                      <input type="text" size="7" class="datepicker" id="tgl2BelanjaObat" /> 
                  </div>
                  <a onclick="belanjaobat.ajax.reload()" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
                  <a onclick="downloadExcel('belanjaobat')" class=" btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                </div>
                <div class="box-body">
                  <table id="belanjaobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th></th>
                      <th>Kode</th>
                      <th>BatchNo</th>
                      <th>ExpDate</th>
                      <th>Barang</th>
                      <th>Jumlah</th>
                      <th>Satuan</th>
                      <th>NoFaktur</th>
                      <th>NoPesan</th>
                      <th>TglFaktur</th>
                      <th>Distributor</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">Obat Kadaluarsa</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                <div class="col-md-12"><a onclick="obatkadaluarsa.ajax.reload()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a></div>
                <div class="box-body">
                  <table id="obatkadaluarsa" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>No</th>
                      <th>Kode</th>
                      <th>Barang</th>
                      <th>No.Batch</th>
                      <th>Kadaluarsa</th>
                      <th>Stok</th>
                      <th>Satuan</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            
            
              <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Defecta Obat</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                
                  <div class="col-md-12"><a onclick="defectaobat.ajax.reload()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a></div>
                <div class="box-body">
                  <table id="defectaobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>No</th>
                      <th>Barang</th>
                      <th>Stok</th>
                      <th>ROP</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
            </div>

            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Pelaporan Resep Obat</h3>
                  <div class="pull-right box-tools"><a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse"><i class="fa fa-minus"></i></a></div>
                </div>
                
                <div style="padding-top:4px;">
                      <div class="col-xs-12 col-md-1"><input type="text" size="7" class="form-control tahunbulan" id="tbresepobat" name="tahunbulan"></div>
                      <div class="col-xs-12 col-md-3" style="margin-right:1px;"><select id="dokterresepobat" class="select2 form-control"></select></div>
                    
                    <a onclick="select_resepobat()" class="btn btn-info btn-md" ><i class="fa fa-desktop"></i></a>
                    <a onclick="excel_resepobat()" class="btn btn-success btn-md"><i class="fa  fa-file-excel-o"></i></a>
                    <a onclick="select_resepobat()" class="btn btn-warning btn-md"><i class="fa  fa-refresh"></i></a>
                  </div>
                <div class="box-body" id="listresepobat">
                  <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>No</th>
                      <th>Diresepkan</th>
                      <th>Dokter</th>
                      <th>HargaJual</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
            </div>
            
            <div class="box box-default box-solid">
                <div class="box-header ui-sortable-handle">
                    <h3 class="box-title">Laporan Penggunaan Obat Generik dan Paten</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                <div style="padding-top:4px;">
                  <div class="col-xs-12 col-md-1"><input type="text" size="7" id="generikpatenmonth" class="form-control datepicker" name="tahunbulan"></div>
                  <a class="btn btn-info btn-sm" onclick="obatgenerikpaten.ajax.reload()"><i class="fa fa-desktop"></i></a>
                  <a onclick="excel_obatgenerikpaten()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i></a>
                  <a onclick="obatgenerikpaten.ajax.reload()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
                  </div>
                <div class="box-body">
                  <table id="obatgenerikpaten" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>No</th>
                      <th>Generik</th>
                      <th>Paten</th>
                      <th>Unit</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
          </div>            
            
            <div class="box box-default box-solid">
                <div class="box-header ui-sortable-handle">
                    <?php  
                        $awal = date_create(ql_bulankemarin(date('Y-m-d'))); 
                        $sekarang = date_create(date('Y-m-d')); 
                        $diff = date_diff($awal,$sekarang); 
                        $hari = $diff->format('%a');  
                    ?>
                    <h3 class="box-title">Laporan Barang Tanpa Penjualan Rawat Jalan, Dalam <?= $hari; ?> Hari Yang Lalu</h3>
                  <div class="pull-right box-tools">
                    <a class="btn btn-xs pull-right" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                    <i class="fa fa-minus"></i></a>
                  </div>
                </div>
                <div class="box-body">
                  <div class="col-md-12 row">
                    <a onclick="downloadExcel('barangslowmoving')" class=" btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Download</a>
                  </div>
                  <table id="barangslowmoving" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                    <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Kode</th>
                      <th>Nama Barang</th>
                      <th>BatchNo</th>
                      <th>Kadaluarsa</th>
                      <th>Stok Gudang</th>
                      <th>Satuan</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tfoot>
                  </table>
                </div>
          </div>
        </section>        
        <div class="clearfix"></div>
    </section>