<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2" >
                    <select class="form-control" name="pilihBangsal" id="pilihBangsal">
                        <?php 
                                echo  "<option value=''>Semua Bangsal</option>";
                                echo  "<option value='(8,22)'>Anggrek</option>";
                                echo  "<option value='(37)'>Dahlia</option>";
                                echo  "<option value='(1,2,3,4,5,38,40)'>Mawar Melati Cempaka</option>";
                                echo  "<option value='(7)'>Wijayakusuma</option>";
                                echo  "<option value='(10,26)'>Kamar Bayi</option>";
                                echo  "<option value='(9,27)'>Unit Intensive</option>";
                        ?>
                    </select>
                </div>
                <div class="col-md-5">
                  <input type="text" class="datepicker" id="tahunbulan" size="7" value="<?= ((isset($get['tahunbulan'])) ? $get['tahunbulan'] : date('M Y') ) ?>" readonly>
                  &nbsp;
                  <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                  <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
                </div>
            </div>

            <!-- Laporan Cakupan Rawat Inap >>> -->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h5 class="text-bold" id="title1">Laporan Cakupan Rawat Inap</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="tablePasienlamabaru"></div>                      
                    <div class="col-xs-12" id="tableCarabayar"></div>                      
                    <div class="col-xs-12" id="tablePerDPJP"></div>
                </div>  
            </div>

            <!-- Cakupan Diagnosis >>> -->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                  <h5 class="text-bold" id="title2">Cakupan Diagnosis</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="viewDiagnosis">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="header-table-ql"><th>No</th><th>Diagnosis</th><th>Jumlah Pasien</th></tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <<< Cakupan Diagnosis -->
            
            <!-- Cakupan Tindakan >>> -->
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h5 class="text-bold" id="title3">Cakupan Tindakan</h5>
                </div>
                <div class="box-body">
                    <div class="col-xs-12" id="viewTindakan">
                        <table class="table table-bordered table-hover table-striped">
                            <tr class="header-table-ql"><th>No</th><th>Tindakan</th><th>Jumlah Pasien</th></tr>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <<< Cakupan Tindakan -->
            
        </div>    
    </div>
</section>
        