<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judulFile.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
  <tr>
    <td colspan="6" style="text-align: center;"><b> <?= $judul; ?> </b></td>
  </tr>
  <tr>
    <td colspan="6" style="text-align: center;"><b> <?= $subJudul; ?> </b></td>
  </tr>
  <tr>
    <th>No</th>
    <th>Tanggal</th>
    <th>Keterangan</th>
    <th>Debet</th>
    <th>Kredit</th>
    <th>Saldo</th>
  </tr>
    <?php 
    if(empty($dataHistory['laporan']))
    {
        echo '<tr style="text-align: center;"><td colspan="6">Data Tidak Ada.</td></tr>';
    }
    else
    {
      $saldoawal = (($dataHistory['saldoawal'] == null) ? 0 : $dataHistory['saldoawal'] );
      echo "<tr><th colspan='3' class='text-center'> Saldo Awal </th><th></th><th></th><th>".convertToRupiah($saldoawal)."</th></tr>";
      $no    = 0;
      $saldo = 0;
      $saldo += $saldoawal;
      
      foreach ($dataHistory['laporan'] as $arr)
        {
            $saldo += $arr['saldo'];
            echo "<tr><td>". ++$no ."</td><td>".$arr['tanggal']."</td><td>".$arr['deskripsi']."</td><td>".convertToRupiah($arr['debet'])."</td><td>".convertToRupiah($arr['kredit'])."</td><td>".convertToRupiah($saldo)."</td></tr>";
        }
        echo "<tr><th colspan='3' class='text-center'> Saldo Akhir </th><th></th><th></th><th>".convertToRupiah($saldo)."</th></tr>";
    }
    ?>
</table>