<table id="waktupendaftaran" class="table ql-dt table-hover table-striped" cellspacing="0" width="100%">
    <thead>
        <tr class="bg bg-yellow-gradient">
            <th>No</th>
            <th>Nomor Antrian</th>
            <th>Tgl Pengambilan</th>
            <th>Waktu Pengambilan</th>
            <th>Estimasi</th>
            <th>Waktu Panggil</th>
            <th>Jenis Antrian</th>  
        </tr>
    </thead>
    <tbody>
    <?php
        $tanggal= date("Y-m-d", strtotime($tanggal));
        $no = 1;
        $sql=$this->db->select("*")
                        ->from('waktu_pendaftaran_admisi')
                        ->where("tanggal",$tanggal)
                        ->get()->result();

        foreach( $sql as $row ):
            $waktu_ambilantrian = $row->waktu_ambilantrian;
            $array_data1 = explode(' ', $waktu_ambilantrian);
            $tgl_ambilantrian = $array_data1[0];
            $jam_ambilantrian = $array_data1[1];
            $waktu_panggilantrian = $row->waktu_panggil_antrian;
            $array_data2 = explode(' ', $waktu_panggilantrian);
            $tgl_panggilantrian = $array_data2[0];
            $jam_panggilantrian = $array_data2[1];

            $datetime1 = new DateTime($jam_ambilantrian);//start time
            $datetime2 = new DateTime($jam_panggilantrian);//end time
            $durasi = $datetime1->diff($datetime2);
    ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $row->nomor_antrian; ?></td>
            <td><?= $tgl_ambilantrian; ?></td>
            <td><?= $jam_ambilantrian; ?></td>
            <td><?= $durasi->format('%H jam %i menit %s detik'); ?></td>
            <td><?= $jam_panggilantrian; ?></td>
            <td><?= $row->jenis_antrian; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>