<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");

$rows='';
$no  =0;
$jumlah = 0;
// $subbpjs = 0;
// $subumum = 0;
if(!empty($laporan))
{
    foreach ($laporan as $obj) 
    {
        if($obj->resep ==0 && $obj->bebas == 0)
        {
            //
        }
        else
        {
            $jumlah = (float) $obj->resep + (float) $obj->bebas;
            // $subbpjs = $jumlah * $obj->hargabpjs;
            // $subumum = $jumlah * $obj->hargaumum;
            $rows .='<tr><td>'. ++$no .'</td><td>'. $obj->namabarang .'</td><td>'. $obj->resep .'</td><td>'. $obj->bebas .'</td><td>'. $jumlah .'</td><td>'. $obj->hargabeli .'</td><td>'. $obj->hargabpjs .'</td><td>'. $obj->hargaumum .'</td></tr>';
        }
    }
}
echo '<h3>'.$judul.'</h3>';
echo '<table  border="1" cellspacing="0" width="100%">'
    . '<thead>'
        . '<tr><td>No</td><td>Nama Barang</td><td>Resep</td><td>Bebas</td><td>Jumlah</td><td>Harga Beli</td><td>Harga BPJS</td><td>Harga Umum</td></tr></thead>'
        . '<tbody>',$rows,'</tfoot>'
    . '</table>';
?>