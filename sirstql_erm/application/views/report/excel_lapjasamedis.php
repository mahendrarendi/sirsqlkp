<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$fileName.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo "<h3>".$fileName."</h3>";
echo "<table border='1'>
          <tr>
            <td>No</td>
            <td>Norm</td>
            <td>Nama Pasien</td>
            <td>Jaminan</td>
            <td>Tanggal Periksa</td>
            <td>Tindakan</td>
            <td>Jumlah</td>
            <td>Jasa Medis</td>
            <td>Subtotal Jasa Medis</td>
            <td>Jasa Dokter</td>
            <td>Subtotal Jasa Dokter</td>
            <td>Jasa RS</td>
            <td>Subtotal Jasa RS</td>
            <td>Total</td>
          </tr>";
if(empty(!$laporan))
{
    $no =0;
    foreach ($laporan as $arr)
    {
        echo "<tr><td>". ++$no ."</td><td>". $arr['norm'] ."</td><td>". $arr['namapasien'] ."</td><td>". (($arr['carabayar'] == 'mandiri') ? 'umum' : $arr['carabayar'] ) ."</td><td>". $arr['tanggalperiksa'] ."</td><td>". $arr['icd'].' '. $arr['namaicd'] ."</td><td>". $arr['jumlah'] ."</td><td>". $arr['jasaoperator'] ."</td><td>". ($arr['jasaoperator'] * $arr['jumlah']) ."</td><td>". $arr['dokter'] ."</td><td>". ($arr['dokter'] * $arr['jumlah']) ."</td><td>". $arr['jasars'] ."</td><td>". ($arr['jasars'] * $arr['jumlah']) ."</td><td>". $arr['total'] ."</td></tr>";
    }
}
else
{
    echo "<tr><td colspan='10'>Data Tidak Ada.</td></tr>";
}
echo "</table>";
?>


        
