
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <?php if($mode=='laporankeuangan'){ ?>
            <style type="text/css">.small-box>.small-box-footer {background: #795548;}.btn-unduh{text-align: left; padding: none; cursor: pointer}</style>
            <div class="col-xs-6" style="margin-top: 15px;">
              <div class="box">
              <div class="box-body">
                <table class="table table-hover">
                    <tbody><tr style="font-size: 16px;">
                      <th>No</th>
                      <th>DESKRIPSI</th>
                      <th></th>
                    </tr>
                    <tr><td>L.1</td><td>Laporan Jasa Medis</td><td><a href="<?= base_url('creport/keuangan_jasamedis');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.2</td><td>Laporan Keuangan Pendapatan Rawat Jalan</td><td><a href="<?= base_url('creport/keuangan_pendapatanrajal');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.3</td><td>Rekap Kasir Pendapatan Rawat Jalan</td><td><a href="<?= base_url('creport/kasir_pendapatanrajal');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.4</td><td>Laporan Pendapatan Penjualan Bebas</td><td><a href="<?= base_url('creport/keuangan_penjualanbebas');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.5</td><td>Dashboard Pendapatan</td><td><a href="<?= base_url('creport/dashboard_pendapatan');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.6</td><td>Laporan Pendapatan Potong Gaji</td><td><a href="<?= base_url('creport/keuangan_pendapatanpotonggaji');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                    <tr><td>L.7</td><td>Data Log Reset Pembayaran</td><td><a href="<?= base_url('creport/keuangan_logsetelulangpembayaran');?>" class="btn btn-warning btn-xs">Detail</a></td></tr>
                  </tbody>
                </table>
                </div>
              </div>
            </div>
            
            <?php }else if($mode=='logsetelulangpembayaran'){ ?>
            
            
            <section class="colo-xs-12">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pencarian</h3>
              </div>
              <div class="box-body">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <form id="formcari_resetpembayaran">
                    <input type="text" name="tahunbulan" size="8" class="datepicker tahunbulan"/>
                    &nbsp;
                    <a class="btn btn-info btn-sm" id="cari_dtlogsetelulangpembayaran"><i class="fa fa-desktop"></i> Tampil</a>
                    </form>
                </div>
              </div>
            </div>
          </section>
            
            
            <section class="colo-xs-12">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Data Log</h3>
              </div>
              <div class="box-body" id="dtresetpembayaran">
                
              </div>
            </div>
          </section>
            
            <?php }else if($mode=='pendapatanpotonggaji'){ ?>
            
            
            <section class="colo-xs-12">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Form Pencarian</h3>
              </div>
              <div class="box-body">
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <form id="formcari_tongji_ralan">
                    <input type="text" name="tanggal1" size="8" class="datepicker"/>
                    <input type="text" name="tanggal2" size="8" class="datepicker"/>
                    &nbsp;
                    <a class="btn btn-info btn-sm" id="caritongjiralan"><i class="fa fa-desktop"></i> Tampil</a>
                    </form>
                </div>
              </div>
            </div>
          </section>
            
            
            <section class="colo-xs-12">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Pendapatan Potongan Gaji - Pasien Rawat Jalan</h3>
              </div>
              <div class="box-body" id="dtralan">
                
              </div>
            </div>
          </section>
            
            
            <section class="colo-xs-12">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Pendapatan Potongan Gaji - Penjualan Obat bebas</h3>
              </div>
              <div class="box-body" id="dtobatbebas">
              </div>
            </div>
          </section>
           
           <?php }else if($mode=='dashboardpendapatan'){ ?>
            <section class="col-xs-12">
            <a href="<?= base_url('creport/laporankeuangan');?>" type="button" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            <!--<div class="btn-group"> <select name="shif" class="form-control" style="width: 100%;"> <option value="">Pilih Shif</option> <?php if(!empty($shif)){ foreach ($shif as $key => $value) { echo "<option value=".$key.">".$value."</option>"; }} ?></select></div>-->
            <!--<div class="btn-group"> <select name="jenistagihan" class="form-control" style="width: 100%;"> <option value="">Jenis Tagihan</option> <?php if(!empty($jenistagihan)){ foreach ($jenistagihan as $value) { echo "<option value=".$value.">".$value."</option>"; }} ?></select></div>-->
            <!--<div class="btn-group"> <select name="user" class="form-control" style="width: 100%;"> <option value="">User/Petugas</option> <?php if(!empty($user)){foreach ($user as $usr) { echo "<option value=".$usr->iduser.">".$usr->namauser."</option>"; }} ?></select></div>-->
            <div class="btn-group"><input style="height:34px;"  type="text" class="datepicker" id="tanggal" size="7"></div>
            <a id="tampil_dashboardpendapatan" data-toggle="tooltip" data-original-title="Tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
            <!--<a id="unduhObatBebas"  data-toggle="tooltip" data-original-title="Unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></a>-->
            <a id="refresh_dashboardpendapatan"  data-toggle="tooltip" data-original-title="Refresh" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
            </section>
             
            <section class="col-xs-12" style="margin-top:8px;">
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pendapatan Rawat Jalan All</h3>
                    </div>
                    <div class="box-body">
                      <table class="table table-hover table-bordered table-striped" id="list_pendapatanall">
                            <!--list data dari js laporankeuangan-->
                      </table>
                    </div>
                </div>
            </section>
            <!--pendapatan berdasarkan cara pembayaran--> 
            <section class="col-xs-12 col-md-6">
                
                <div class="box box-solid box-warning">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pendapatan Rawat Jalan Berdasarkan Jenis Tagihan</h3>
                    </div>
                    <div class="box-body">
                      <table class="table table-hover table-bordered table-striped">
<!--                            <thead><tr class="bg bg-gray"><th>Nominal</th><th>Dibayar</th><th>Jenis Tagihan</th><th>Cara Bayar</th><th>Status Bayar</th><tr></thead>-->
                            <tbody id="list_tagihanralan"></tbody>
                      </table>
                    </div>
                </div>
            
              <div class="box box-solid box-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Pendapatan Obat Bebas</h3>
                </div>
                <div class="box-body">
                  <table class="table table-hover table-bordered table-striped">
                        <thead><tr class="bg bg-gray"><th>Kasir</th><th>Tagihan</th><th>Dibayar</th><th>Jenis Tagihan</th><th>Carabayar</th><tr></thead>
                        <tbody id="list_tagihanobatbebas"></tbody>
                  </table>
                </div>
              </div>

        </section>
                       
        <section class="colo-xs-12 col-md-6">            
            <div class="box box-solid box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Pendapatan Rawat Jalan Berdasarkan Poliklinik</h3>
              </div>
              <div class="box-body">
                <table class="table  table-hover table-bordered table-striped">
                  <thead>
                      <tr class="bg bg-gray">
                        <th rowspan="2">POLIKLINIK</th>
                        <th rowspan="2">JUMLAH PASIEN</th>
                        <th colspan="2">SETORAN</th>
                      </tr>
                      <tr class="bg bg-gray">
                        <th>BPJS</th>
                        <th>UMUM</th>
                      </tr>
                  </thead>
                  <tbody id="list_tagihanralanpoli" >

                  </tbody>
                </table>
              </div>
            </div>
          </section>
            
            
           <?php }else if($mode=='jasamedis'){ ?>
          <div class="box" style="padding-top: 3px;">
            <div class="col-md-1 row">
                <a href="<?= base_url('creport/laporankeuangan');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
            </div>
            <div class="col-md-3">
                <select name="nmdokter" id="nmdokter" class="form-control select2" style="width: 100%;"> 
                    <option value="">Pilih Dokter</option>
                    <?php 
                        if(!empty($dokter))
                        {
                            foreach ($dokter as $arr)
                            {
                                echo "<option ".(( isset($get['idpegawaidokter']) && $get['idpegawaidokter'] == $arr['idpegawai']) ? 'selected' : '' )." value=".$arr['idpegawai']."> ".$arr['titeldepan']." ".$arr['namalengkap']." ".$arr['titelbelakang']."</option>";
                            }
                        } 
                    ?>
                </select>
            </div>
              
            <div class="col-md-1">
                <input type="text" size="7" class="datepicker" name="tanggal1" value="<?= (( isset($get['tanggal1']) && empty(!$get['tanggal1']) ) ? $get['tanggal1'] : date('Y-m-d') ); ?>">
            </div>
              
            <div class="col-md-1">
                <input type="text" size="7" class="datepicker" name="tanggal2" value="<?= (( isset($get['tanggal1']) && empty(!$get['tanggal2']) ) ? $get['tanggal2'] : date('Y-m-d') ); ?>">
            </div>
            <a onclick="getjasamedis()" data-toggle="tooltip" data-original-title="Tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
            <a onclick="unduhjasamedis()"  data-toggle="tooltip" data-original-title="Unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></a>
            <div class="box-body">
                
                <?php
                    $totjm  = 0;
                    $totjrs = 0;
                    if(isset($laporan) && empty( !$laporan ))
                    {
                        foreach ($laporan as $arr)
                        {
                            $totjm  += intval($arr['jasaoperator']);
                            $totjrs += intval($arr['jasars']);
                        }
                    }
                
                ?>
                <h3>Total Jasa Medis : <?= convertToRupiah($totjm); ?></h3>
                <h3>Total Jasa RS : <?= convertToRupiah($totjrs); ?></h3>
                
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr class="header-table-ql">
                            <th>No</th>
                            <th>Norm</th>
                            <th>Nama Pasien</th>
                            <th>Jaminan</th>
                            <th>Tanggal Periksa</th>
                            <th>Tindakan</th>                            
                            <th>Jumlah</th>
                            <th>Jasa Medis</th>
                            <th>Subtotal Jasa Medis</th>
                            <th>Jasa Dokter</th>
                            <th>Subtotal Jasa Dokter</th>
                            <th>Jasa RS</th>
                            <th>Subtotal Jasa RS</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if(empty($laporan))
                            {
                                echo '<tr class="text-center"><td colspan="11">Data Tidak Ada.</td></tr>';
                            }
                            else
                            {
                                $no = 0;
                                foreach ($laporan as $arr)
                                {
                                    echo "<tr><td>". ++$no ."</td><td>". $arr['norm'] ."</td><td>". $arr['namapasien'] ."</td><td>". (($arr['carabayar'] == 'mandiri') ? 'umum' : $arr['carabayar'] ) ."</td><td>". $arr['tanggalperiksa'] ."</td><td>". $arr['icd'] ." ". $arr['namaicd'] ."</td><td>". $arr['jumlah'] ."</td><td>". convertToRupiah($arr['jasaoperator']) ."</td><td>". convertToRupiah($arr['jasaoperator'] * $arr['jumlah']) ."</td><td>". convertToRupiah($arr['dokter']) ."</td><td>". convertToRupiah($arr['dokter'] * $arr['jumlah']) ."</td><td>". convertToRupiah($arr['jasars']) ."</td><td>". convertToRupiah($arr['jasars'] * $arr['jumlah']) ."</td><td>". convertToRupiah($arr['total']) ."</td></tr>";
                                }
                            }
                        ?>
                    </tfoot>
                    
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <?php }else if($mode=='kasirpendapatanrajal'){ ?>
            <style> .table{margin:none;padding:none;}.table>tbody>tr>td {padding: 1px 1px;} </style>
            <div class="btn-group"><a href="<?= base_url('creport/laporankeuangan');?>" type="button" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a></div>
            <div class="btn-group"><select name="shif" class="form-control" style="width: 100%;"> <option value="">Pilih Shif</option> <?php if(!empty($shif)){ foreach ($shif as $key => $value) { echo "<option value=".$key.">".$value."</option>"; }} ?></select></div>
            <div class="btn-group"><select name="carabayar" id="carabayar" class="form-control" style="width: 100%;"> <option value="">Cara Bayar</option> <?php if(!empty($carabayar)){foreach ($carabayar as $key => $value) { echo "<option value=".$value.">".$value."</option>"; }} ?></select></div>
            <div class="btn-group"><select name="unit" id="unit" class="form-control select2" style="width: 100%;"> <option value="">Pilih Poli/Unit</option> <?php if(!empty($unit)){foreach ($unit as $obj) { echo "<option value=".$obj->idunit.">".$obj->namaunit."</option>"; }} ?></select></div>
            <div class="btn-group"><select name="user" id="user" class="form-control" style="width: 100%;"> <option value="">Pilih User/Petugas</option> <?php if(!empty($user)){foreach ($user as $usr) { echo "<option value=".$usr->iduser.">".$usr->namauser."</option>"; }} ?></select></div>
            <div class="btn-group"><input style="height:34px;"  type="text" class="datepicker" name="hari" size="7"></div>
            <div class="btn-group"><input style="height:34px;" type="text" readonly class="datepicker" name="hari2" size="7" ></div>
            <a onclick="rekap_pendapatanrajal()" data-toggle="tooltip" data-original-title="Tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
            <a onclick="unduhrekappendapatanrajal()"  data-toggle="tooltip" data-original-title="Unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></a>  
            <a onclick="refresh_page()"  data-toggle="tooltip" data-original-title="Refresh" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
            <div class="box">
            <div class="box-body" id="tampil" style="max-height:500px; max-width:1293px; overflow-x: auto; overflow-y: auto;margin:4px;"></div>
            </div>
          <?php }else if($mode=='pendapatanrajal'){ ?>
            <style> .table{margin:none;padding:none;}.table>tbody>tr>td {padding: 1px 1px;} </style>
            <div class="btn-group"><a href="<?= base_url('creport/laporankeuangan');?>" type="button" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a></div>
            <div class="btn-group"><select name="carabayar" id="carabayar" class="form-control" style="width: 100%;"> <option value="">Cara Bayar</option> <?php if(!empty($carabayar)){foreach ($carabayar as $key => $value) { echo "<option value=".$value.">".$value."</option>"; }} ?></select></div>
            <div class="btn-group"><select name="unit" id="unit" class="form-control select2" style="width: 100%;"> <option value="">Pilih Poli/Unit</option> <?php if(!empty($unit)){foreach ($unit as $obj) { echo "<option value=".$obj->idunit.">".$obj->namaunit."</option>"; }} ?></select></div>
            <div class="btn-group"><input style="height:34px;"  type="text" class="datepicker" name="hari" size="7"></div>
            <div class="btn-group"><input style="height:34px;" type="text" readonly class="datepicker" name="hari2" size="7" ></div>
            <a onclick="pendapatanrajal()" data-toggle="tooltip" data-original-title="Tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
            <a onclick="unduhpendapatanrajal()"  data-toggle="tooltip" data-original-title="Unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></a>  
            <a onclick="refresh_page()"  data-toggle="tooltip" data-original-title="Refresh" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
            <div class="box">
            <div class="box-body" id="tampil" style="max-height:500px; max-width:1293px; overflow-x: auto; overflow-y: auto;margin:4px;"></div>
            </div>
          <?php }else if($mode=='pendapatanobatbebas'){ ?>
            <a href="<?= base_url('creport/laporankeuangan');?>" type="button" class="btn btn-danger"> <i class="fa fa-backward"></i> Kembali</a>
            <div class="btn-group"> <select name="shif" class="form-control" style="width: 100%;"> <option value="">Pilih Shif</option> <?php if(!empty($shif)){ foreach ($shif as $key => $value) { echo "<option value=".$key.">".$value."</option>"; }} ?></select></div>
            <div class="btn-group"> <select name="jenistagihan" class="form-control" style="width: 100%;"> <option value="">Jenis Tagihan</option> <?php if(!empty($jenistagihan)){ foreach ($jenistagihan as $value) { echo "<option value=".$value.">".$value."</option>"; }} ?></select></div>
            <div class="btn-group"> <select name="user" class="form-control" style="width: 100%;"> <option value="">User/Petugas</option> <?php if(!empty($user)){foreach ($user as $usr) { echo "<option value=".$usr->iduser.">".$usr->namauser."</option>"; }} ?></select></div>
            <div class="btn-group"> <input style="height:34px;"  type="text" class="datepicker" name="date1" size="7"></div>
            <div class="btn-group"> <input style="height:34px;"  type="text" class="datepicker" name="date2" size="7"></div>
            <a id="tampilObatBebas" data-toggle="tooltip" data-original-title="Tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i></a>
            <a id="unduhObatBebas"  data-toggle="tooltip" data-original-title="Unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i></a>
            <a onclick="refresh_page()"  data-toggle="tooltip" data-original-title="Refresh" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i></a>
            <div class="box">
                <div class="box-body" id="tampil">
                    
                </div>
            </div>
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->