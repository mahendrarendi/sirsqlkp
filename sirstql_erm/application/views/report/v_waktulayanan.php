  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
                <div class="toolbar col-md-6 row">
                    <div class="col-md-3" style="padding-right:0px;">
                        <small>Nama Unit</small>
                        <select id="idunit" name="unit"  class="select2" style="width:100%"></select>
                    </div>
                    <div class="col-md-2">
                        <small>Tgl Periksa</small>
                        <input type="text" id="tglperiksa" class="datepicker" size="8" placeholder="yyyy-mm-dd" />
                    </div>
                    <div class="col-md-6">
                        <br>
                    <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                    <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Resfresh</a>
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
                    </div>
                </div>
              <table id="waktulayanan" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th>NORM</th>
                  <th>Nama Pasien</th>
                  <th>Tanggal Periksa</th>
                  <th>Nama Unit</th>  
                  <th width="600px">Riwayat Waktu Layanan</th>  
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        