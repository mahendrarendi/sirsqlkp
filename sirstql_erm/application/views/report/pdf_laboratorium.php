<?php

            $header = '';
            $namapaketpemeriksaansebelum ='';
            $icd = '';
            $idgrup = '';
            $printData = '';
            $keteranganhasil = '';
            $identitas = $data['identitas']; 
            $gruppaket='';
            $waktu = $data['waktu'];
//            $pesan = $data['pesan'];
//            var jeniscetak = value.jeniscetak;
            $formatcetak = $data['formatcetak'];
//            var ttdokter = value.ttdokter;
            $value = $data['hasillab'];
            $header .= '<img src="'.base_url().'assets/images/headerlaboratorium.jpg" >';
            $header .= '<table cellpadding="5" cellspacing="0" style=" width:18cm;font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"">
            <tr><td>No.Periksa/No.RM </td><td>:</td> <td >'.$identitas['idpemeriksaan'] .'/'. $identitas['norm'].'</td><td>Dokter</td> <td>:</td><td>'.$identitas['dokter'].'</td></tr>
            <tr><td>Nama</td>             <td>:</td> <td>'.$identitas['namalengkap'].'</td><td>Asal</td>   <td>:</td><td>'.$identitas['namaunit'].'</td></tr>
            <tr><td>J.Kelamin</td>        <td>:</td> <td>'.$identitas['jeniskelamin'].'</td><td>Periksa</td><td>:</td><td>'.$waktu['periksa'].'</td></tr>
            <tr><td>Tgl.Lahir</td>             <td>:</td> <td>'.$identitas['tanggallahir']. (($identitas['usia'] == '') ? '' : ' ( '.$identitas['usia'].' )' ) .'</td><td>Selesai</td> <td>:</td><td>'.$waktu['selesai'].'</td></tr>
            <tr><td>Alamat</td>           <td>:</td><td colspan="4" style="font-size:12px;">'.$identitas['alamat'].'</td></tr>
            </table>';

            $header .= '<table cellpadding="5" cellspacing="0" style=" width:18cm;font-size:14px;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;"">';
            // jenis cetak kosong default, selain itu lab hiv
            $header .= $formatcetak['judul'];//((jeniscetak==null || jeniscetak=='') ? '' : '<tr><td colspan="5">IMUNOSEROLOGI</td></tr>');
            $header .= $formatcetak['header'];//((jeniscetak==null || jeniscetak=='') ? '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">PEMERIKSAAN</td><td style="border:1px solid #000;border-left:0px;">HASIL</td><td style="border:1px solid #000;border-left:0px;min-width:80px;">N DEFAULT</td><td style="border:1px solid #000;border-left:0px;min-width:90px;">N RUJUKAN</td><td style="border:1px solid #000;border-left:0px;min-width:60px;" >SATUAN</td></tr>' : '<tr style="border:1px solid #000;"><td style="border:1px solid #000;">Pemeriksaan</td><td style="border:1px solid #000;border-left:0px;">Hasil</td><td style="border:1px solid #000;border-left:0px;">Nilai Rujukan</td><td style="border:1px solid #000;border-left:0px;">Metode</td><td style="border:1px solid #000;border-left:0px;">Hasil</td></tr>');
            $printData .= $header;
            $printPaket='';
            $i=0; $nilai=''; $jenisicd=''; $paketpemeriksaanparent=''; $paketpemeriksaan=''; $nilairujukan=''; $nilaiantigen='';
            foreach ($value as $obj)
            {   
                $nilaiantigen = $obj->nilai ;
                $nilairujukan = $obj->nilaiacuanrendah .' ' .$obj->nilaiacuantinggi;
//                if($obj->idjenisicd2=='4')
//                {
//                    -----------
                    if( empty(!$paketlab) ){
//                    
                        //CETAK HASIL LABORATORIUM SESUAI PAKET YANG DIPILIH
                        $printData='';
                        
                        //tampilkan keterangan hasil laborat jika satu grup dan satu paket
                        if($gruppaket !== $obj->gruppaketperiksa && empty(!$idgrup) )
                        {
                             $printPaket += '<tr ><td colspan="5" style="padding-left:7px;">'.$keteranganhasil.'</td></tr>'; 
                             $keteranganhasil = '';
                        }
                        
                        if( $paketlab == $obj->idpaketpemeriksaan || $paketlab == $obj->idpaketpemeriksaanparent )
                        { 
                            if($gruppaket != $obj->gruppaketperiksa ){ $printPaket.=$header;}
                            
                            $paketpemeriksaan = (( $obj->namapaketpemeriksaan == $namapaketpemeriksaansebelum && $gruppaket == $obj->gruppaketperiksa )?'': '<tr><td colspan="5"><b>'.$obj->namapaketpemeriksaan.'</b></td></tr>');
                            $printPaket       = $printPaket .$paketpemeriksaan;// + ((jeniscetak==null || jeniscetak=='') ?  paketpemeriksaan : '');//jika jenis cetak null  nama paket dicetak
                            if( $icd != $obj->icd && empty(!$obj->icd) )//jika icd sama lewati
                            {
                                
                                if($formatcetak['kode']=='AHIV')
                                {
                                    $printPaket = $printPaket  .'<tr>'
                                      .'<td>'. $obj->namaicd.'</td>'
                                      .'<td>'. $obj->nilai .'</td>'
                                      .'<td>'. $nilairujukan .'</td>'
                                      .'<td>'. $obj->metode .'</td>'
                                      .'<td>'. $obj->kesimpulanhasil .'</td>'
                                      .'</tr>';
                                }
                                else if($formatcetak['kode']=='PCRS')
                                {
                                    $printPaket = $printPaket  .'<tr>'
                                      .'<td>'. $obj->namaicd.'</td>'
                                      .'<td>'. $obj->nilai .'</td>'
                                      .'<td>'. $nilairujukan .'</td>'
                                      .'<td>'. $obj->satuan .'</td>'
                                      .'<td>'. $obj->metode .'</td>'
                                      .'</tr>';                                    
                                }
                                else
                                {
                                    $printPaket = $printPaket  .'<tr>'
                                      .'<td>'.$obj->namaicd.'</td>'
                                      .'<td>'.$obj->nilai .'</td>'
                                      .'<td>'. (( empty($obj->nilaidefault) ) ? '' : $obj->nilaidefault  ) .'</td>'
                                      .'<td>'. $nilairujukan .'</td>'
                                      .'<td>'. $obj->satuan .'</td>'
                                      .'</tr>';
                                }
                                    
                            }
                                    $icd = $obj->icd;
                                    // var jenisicdsebelum=value[i].jenisicd;
                                    $namapaketpemeriksaansebelum = $obj->namapaketpemeriksaan;
                                    $gruppaket = $obj->gruppaketperiksa;
                        }
                        if( empty(!$obj->keteranganhasil) && $obj->keteranganhasil != $keteranganhasil)
                        {
                            $keteranganhasil = $obj->keteranganhasil;
                        }
                        $idgrup = $obj->idgrup;
                    }
                    else
                    {
                        if( empty($paketlab))
                        {
                            //CETAK HASIL LABORATORIUM - KETIKA JENIS PAKET YANG DIPILIH BUKAN PAKET
                            if( $obj->idpaketpemeriksaan < 1 && $obj->idpaketpemeriksaanparent < 1)
                            {
                                $printData = $printData  .'<tr>'
                                            .'<td>'. $obj->namaicd.'</td>'
                                            .'<td>'. $obj->nilai .'</td>'
                                            .'<td>'. (( empty($obj->nilaidefault) ) ? '' : $obj->nilaidefault  ) .'<input type="hidden" name="icd[]" value="'.$ojb->icd.'"><input type="hidden" name="istext[]" value="'.$obj->istext.'"></td>'
                                            .'<td>'. $obj->nilaiacuanrendah.' '.$obj->nilaiacuantinggi.'</td>'
                                            .'<td>'. $obj->satuan.'</td>'
                                            .'</tr>';
                            }
                        }
                        else
                        {
                           //CETAK HASIL LABORAT TANPA MEMILIH JENIS PAKET
                           // paketpemeriksaanparent = ((value[i].namapaketpemeriksaan==namapaketpemeriksaansebelum)?'': '<tr><td colspan="5"><b>'+value[i].namapaketpemeriksaan.toUpperCase()+'</b></td></tr>');
                           $paketpemeriksaan = (( $obj->namapaketpemeriksaan == $namapaketpemeriksaansebelum && $gruppaket == $obj->gruppaketperiksa )?'': (( empty($obj->namapaketpemeriksaan) )?'':'<tr><td colspan="5"><b>'.$obj->namapaketpemeriksaan.'</b></td></tr>'));
                           $printData = $printData  .$paketpemeriksaanparent .$paketpemeriksaan;
                            if($icd != $obj->icd && empty($obj->icd) )//jika icd sama lewati
                            {
                                $printData = $printData  .'<tr>'
                                .'<td>'. $obj->namaicd.'</td>'
                                .'<td>'. $obj->nilai .'</td>'
                                .'<td>'. (( empty($obj->nilaidefault) ) ? '' : $obj->nilaidefault  ) .'</td>'
                                .'<td>'. $obj->nilaiacuanrendah.' '.$obj->nilaiacuantinggi.'</td>'
                                .'<td>'. $obj->satuan.'</td>'
                                .'</tr>';
                            }
                            $icd = $obj->icd;
                            $namapaketpemeriksaansebelum = $obj->namapaketpemeriksaan;
                            $gruppaket = $obj->gruppaketperiksa;
                        }
                    }
//                }
            }
            
            $printData .= $printPaket;
            //tampilkan keterangan hasil laborat terakhir
            $printData .= '<tr><td colspan="5" style="padding-left:7px;">'.((empty($keteranganhasil))?'':$keteranganhasil).'</td></tr> </table> ';
$paperOrientation='potrait';
$htmlTxt  = $printData;
$fileName = $identitas['norm'].'-'.$identitas['namalengkap'];
//print_r($printData);
ob_start();
ob_end_clean();
CI()->load->library('pdf');
CI()->pdf->base_html($htmlTxt, $fileName,'', $paperOrientation);
?>