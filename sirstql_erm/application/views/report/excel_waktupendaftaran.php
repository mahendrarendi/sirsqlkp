<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo "<h3>".$judul."</h3>";
$countshow = 0;
$no     = 0;
echo  "<table border='1'>";
if(empty($laporan))
{
    echo "<tr><td colspan='5'>Data Tidak Ada</td></tr>";
}
else 
{
    foreach ($laporan as $obj) 
    {
       
       if($countshow == 0)
       {
           $no = 0;
           $countshow++;
            echo '<tr>
            <th>NO</th>
            <th>ID Pendaftaran Admisi</th>
            <th>ID Antrian</th>
            <th>Nomor Antrian</th>
            <th>Tanggal</th>
            <th>Waktu Ambil Antrian</th>
            <th>Waktu Panggil Antrian</th>
            <th>Estimasi</th>
            <th>Jenis Antrian</th>
            </tr>';
    
        }

        $ambil = $obj->waktu_ambilantrian;
        $splitambil = explode(" ",$ambil);
        $dateambil = $splitambil[0];
        $timeambil = $splitambil[1];
        $ambil = $obj->waktu_panggil_antrian;
        $splitpanggil = explode(" ",$ambil);
        $datepanggil = $splitpanggil[0];
        $timepanggil = $splitpanggil[1];

        $datetime1 = new DateTime($timeambil);//start time
        $datetime2 = new DateTime($timepanggil);//end time
        $durasi = $datetime1->diff($datetime2);

        echo  '<tr>
        <td>'.++$no.'</td>
        <td>'.$obj->id_pendaftaran_admisi.'</td>
        <td>'.$obj->idantrian.'</td>
        <td>'.$obj->nomor_antrian .'</td>
        <td>'.$obj->tanggal.'</td>
        <td>'.$timeambil.'</td>
        <td>'.$timepanggil.'</td>
        <td>'.$durasi->format('%H jam %i menit %s detik').'</td>
        <td>'.$obj->jenis_antrian .'</td>
        
        </tr>';
    }
    
}

echo "</table>";
?>
