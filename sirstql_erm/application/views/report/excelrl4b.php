<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
<tr><th colspan="26"><b>RL4B Penyakit Rawat Jalan</b></th></tr>
<tr>
  <th rowspan="3">No. Urut</th>
  <th rowspan="3">No. DTD</th> 
  <th rowspan="3">No. Daftar Terperinci</th> 
  <th rowspan="3">Golongan Sebab Penyakit</th> 
  <th colspan="18">Jumlah Pasien Kasus Baru Menurut Golongan Umur &amp; Sex</th> 
  <th colspan="2">Kasus Baru Menurut Jenis Kelamin</th>
  <th rowspan="3">Jumlah Kasus Baru</th>
  <th rowspan="3">Jumlah Kunjungan</th>
</tr>
<tr>
  <th colspan="2">0-6 hr</th>
  <th colspan="2">7-28 hr</th> 
  <th colspan="2">29hr-&lt; 1 th</th>
  <th colspan="2">1-4 th</th>
  <th colspan="2">5-14 th</th>
  <th colspan="2">15-24 th</th>
  <th colspan="2">25-44 th</th>
  <th colspan="2">45-64 th</th>
  <th colspan="2">&gt; 65</th>
  <th rowspan="2">Laki</th>
  <th rowspan="2">Perempuan</th>
</tr>
<tr>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
</tr>
  <?php
    $no=0;
    $apria = 0;
    $awanita = 0;
    $bpria = 0;
    $bwanita = 0;
    $cpria = 0;
    $cwanita = 0;
    $dpria = 0;
    $dwanita = 0;
    $epria = 0;
    $ewanita = 0;
    $fpria = 0;
    $fwanita = 0;
    $gpria = 0;
    $gwanita = 0;
    $hpria = 0;
    $hwanita = 0;
    $ipria = 0;
    $iwanita = 0;
    $kbpria = 0;
    $kbwanita = 0;
    $kunjunganbaru = 0;
    $kunjungan = 0;
    
    foreach ($jenis as $jns)
    {
        foreach ($laporan as $lp)
        {
            if($lp->idgolongansebabpenyakit == $jns->idgolongansebabpenyakit)
            {
                $apria += intval($lp->apria);
                $awanita += intval($lp->awanita);
                $bpria += intval($lp->bpria);
                $bwanita += intval($lp->bwanita);
                $cpria += intval($lp->cpria);
                $cwanita += intval($lp->cwanita);
                $dpria += intval($lp->dpria);
                $dwanita += intval($lp->dwanita);
                $epria += intval($lp->epria);
                $ewanita += intval($lp->ewanita);
                $fpria += intval($lp->fpria);
                $fwanita += intval($lp->fwanita);
                $gpria += intval($lp->gpria);
                $gwanita += intval($lp->gwanita);
                $hpria += intval($lp->hpria);
                $hwanita += intval($lp->hwanita);
                $ipria += intval($lp->ipria);
                $iwanita += intval($lp->iwanita);
                $kbpria += intval($lp->kbpria);
                $kbwanita += intval($lp->kbwanita);
                $kunjunganbaru += intval($lp->kunjunganbaru);
                $kunjungan += intval($lp->kunjungan);
            }
        }
        
        
        echo '<tr><td>'. ++$no .'</td><td>'.$jns->nodtd.'</td><td>'.$jns->daftar_terperinci.'</td><td>'.$jns->golongansebabpenyakit.'</td>
        <td>'.$apria .'</td>
        <td>'.$awanita .'</td>
        <td>'.$bpria .'</td>
        <td>'.$bwanita .'</td>
        <td>'.$cpria .'</td>
        <td>'.$cwanita .'</td>
        <td>'.$dpria .'</td>
        <td>'.$dwanita .'</td>
        <td>'.$epria .'</td>
        <td>'.$ewanita .'</td>
        <td>'.$fpria .'</td>
        <td>'.$fwanita .'</td>
        <td>'.$gpria .'</td>
        <td>'.$gwanita .'</td>
        <td>'.$hpria .'</td>
        <td>'.$hwanita .'</td>
        <td>'.$ipria .'</td>
        <td>'.$iwanita .'</td>
        <td>'.$kbpria .'</td>
        <td>'.$kbwanita .'</td>
        <td>'.$kunjunganbaru .'</td>
        <td>'.$kunjungan.'</td></tr>';
        $apria = 0;
        $awanita = 0;
        $bpria = 0;
        $bwanita = 0;
        $cpria = 0;
        $cwanita = 0;
        $dpria = 0;
        $dwanita = 0;
        $epria = 0;
        $ewanita = 0;
        $fpria = 0;
        $fwanita = 0;
        $gpria = 0;
        $gwanita = 0;
        $hpria = 0;
        $hwanita = 0;
        $ipria = 0;
        $iwanita = 0;
        $kbpria = 0;
        $kbwanita = 0;
        $kunjunganbaru = 0;
        $kunjungan = 0;
    }   
    
  ?>
</table>