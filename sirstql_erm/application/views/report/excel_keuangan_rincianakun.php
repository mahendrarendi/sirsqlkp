<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judulFile.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
  <tr>
    <td colspan="6" style="text-align: center;"><b> <?= $judul; ?> </b></td>
  </tr>
  <tr>
    <td colspan="6" style="text-align: center;"><b> <?= $subJudul; ?> </b></td>
  </tr>
  <tr>
    <th>No</th>
    <th>Tanggal</th>
    <th>Keterangan</th>
    <th>Debet</th>
    <th>Kredit</th>
    <th>Saldo</th>
  </tr>
    <?php 
    if(empty($riwayat))
    {
        echo '<tr style="text-align: center;"><td colspan="6">Data Tidak Ada.</td></tr>';
    }
    else
    {
        $no=0;
        $total = 0;
        $debit = 0;
        $kredit= 0;
        foreach ($riwayat as $arr)
        {
            $total  += (($arr['isdebit'] == 1) ? ($arr['debet'] - $arr['kredit']) : ($arr['kredit'] - $arr['debet']) ) ;
            $debit  += $arr['debet'];
            $kredit += $arr['kredit'];
            echo "<tr><td>". ++$no ."</td><td>".$arr['tanggal']."</td><td>".$arr['deskripsi']."</td><td>".convertToRupiah($arr['debet'])."</td><td>".convertToRupiah($arr['kredit'])."</td><td>".convertToRupiah($total)."</td></tr>";
        }
        
        echo "<tr><th colspan='3' class='text-center'>Total</th><th>".convertToRupiah($debit)."</th><th>".convertToRupiah($kredit)."</th><th>".convertToRupiah($total)."</th></tr>";
        
    }
    ?>
</table>