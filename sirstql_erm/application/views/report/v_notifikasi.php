  <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <?php if( $mode=='barangbelumterjual'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Kode</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>Akhir Penjualan</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    if(!empty($listbarang))
                    {
                      $no=0;
                      foreach ($listbarang as $obj) {
                        echo "<tr><td>", ++$no ,"</td><td>",$obj->kode,"</td><td>",$obj->namabarang,"</td><td>",$obj->stok,"</td><td>",$obj->tanggaljual,"</td></tr>" ;
                      }
                    }
                  ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if( $mode=='viewpobat'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body" id="tampildataobat">
              <table id="viewpobat" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>Waktu</th>
                  <th>Nama Obat</th>
                  <th>Kode Obat</th>
                  <th>Jenis</th>  
                  <th>Satuan Obat</th>
                  <th>Jumlah</th>
                  <th>Dosis Racik</th>
                  <th>Harga</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if( $mode=='viewdtpasien'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>NoRM</th>
                  <th>nojkn</th> 
                  <th>nik</th> 
                  <th>namalengkap</th> 
                  <th>tempatlahir</th> 
                  <th>tanggallahir</th> 
                  <th>jeniskelamin</th> 
                  <th>agama</th> 
                  <th>statusmenikah</th> 
                  <th>golongandarah</th> 
                  <th>rh</th> 
                  <th>namapendidikan</th> 
                  <th>namapekerjaan</th> 
                  <th>alamat</th> 
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <?php }else if($mode=='pelayananrs'){?>
          <style type="text/css">.small-box>.small-box-footer {background: #795548;}.btn-unduh{text-align: left; padding: none; cursor: pointer}</style>
          <div class="col-xs-6" style="margin-top: 15px;">
            <div class="box">
            <div class="box-body">
              <table class="table table-hover">
                  <tbody><tr style="font-size: 16px;">
                    <th>NAMA</th>
                    <th>DESKRIPSI</th>
                    <th></th>
                  </tr>
                  
                  <tr>
                    <td>Pelayanan</td>
                    <td>Laporan Diagnosa &amp; Tindakan</td>
                    <td><a href="<?= base_url('creport/lap_pelayanan');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL4A</td>
                    <td>Data Keadaan Morbiditas Pasien Rawat Inap</td>
                    <td><a href="<?= base_url('creport/rl4a_penyakitranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL4B</td>
                    <td>Data Keadaan Morbiditas Pasien Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl4b_penyakitrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.1</td>
                    <td>Kunjungan Pasien</td>
                    <td><a href="<?= base_url('creport/rl51_kunjunganpasien');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.2</td>
                    <td>Kunjungan Pasien Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl52_kunjunganpasienrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.3</td>
                    <td>10 Besar Penyakit Rawat Inap</td>
                    <td><a href="<?= base_url('creport/rl53_10besarpenyakitranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>RL5.4</td>
                    <td>10 Besar Penyakit Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/rl54_10besarpenyakitrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>ISPA/PNEUMONIA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/pneumonia_pelaporanview');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>BALITA PNEUMONIA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/balitapneumonia_pelaporanview');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>Surveilans Rajal</td>
                    <td>Surveilans Terpadu Penyakit Berbasis Rumah Sakit Rawat Jalan</td>
                    <td><a href="<?= base_url('creport/report_stprsrajal');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>Surveilans Ranap</td>
                    <td>Surveilans Terpadu Penyakit Berbasis Rumah Sakit Rawat Inap</td>
                    <td><a href="<?= base_url('creport/report_stprsranap');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>SIHA</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/report_siha');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>
                  <tr>
                    <td>CAKUPAN POLI</td>
                    <td>...</td>
                    <td><a href="<?= base_url('creport/report_cakupanpoli');?>" class="btn btn-warning btn-xs">Detail</a></td>
                  </tr>

                </tbody>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='laporanpelayanan'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <!-- <select id="dokter"><option>Pilih Dokter</option></select> -->
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampil_lappelayanan()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhlpelayanan()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;"  class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Norm</th> 
                  <th>NamaPasien</th>
                  <th>WaktuLayanan</th>
                  <th>DokterDPJP</th> 
                  <th>Poli</th>
                  <th>Diagnosa</th>
                  <th>Tindakan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl4a'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl4a()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl4a()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="3">No. Urut</th>
                  <th rowspan="3">No. DTD</th> 
                  <th rowspan="3">No. Daftar Terperinci</th> 
                  <th rowspan="3">Golongan Sebab Penyakit</th> 
                  <th colspan="18">Jumlah Pasien (Hidup &amp; Mati) Menurut Golongan Umur &amp; Jenis Kelamin</th> 
                  <th colspan="3">Pasien Keluar (Hidup &amp; Mati) Menurut Jenis Kelamin</th>
                  <th rowspan="3">Jumlah Pasien Keluar Hidup</th>
                  <th rowspan="3">Jumlah Pasien Keluar Mati</th>
                </tr>
                <tr>
                  <th colspan="2">0-6 hr</th>
                  <th colspan="2">7-28 hr</th> 
                  <th colspan="2">29hr-&lt; 1 th</th>
                  <th colspan="2">1-4 th</th>
                  <th colspan="2">5-14 th</th>
                  <th colspan="2">15-24 th</th>
                  <th colspan="2">25-44 th</th>
                  <th colspan="2">45-64 th</th>
                  <th colspan="2">&gt; 65</th>
                  <th rowspan="2">Laki</th>
                  <th rowspan="2">Perempuan</th>
                </tr>
                <tr>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
            </div>
            </div>
          </div>
          <?php } else if($mode=='rl4b'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl4b()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl4b()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="3">No. Urut</th>
                  <th rowspan="3">No. DTD</th> 
                  <th rowspan="3">No. Daftar Terperinci</th> 
                  <th rowspan="3">Golongan Sebab Penyakit</th> 
                  <th colspan="18">JML Pasien Kasus Baru Menurut Golongan Umur &amp; Sex</th> 
                  <th colspan="2">Kasus Baru Menurut Jenis Kelamin</th>
                  <th rowspan="3">JML Kasus Baru</th>
                  <th rowspan="3">JML Kunjungan</th>
                </tr>
                <tr>
                  <th colspan="2">0-6 hr</th>
                  <th colspan="2">7-28 hr</th> 
                  <th colspan="2">29hr-&lt; 1 th</th>
                  <th colspan="2">1-4 th</th>
                  <th colspan="2">5-14 th</th>
                  <th colspan="2">15-24 th</th>
                  <th colspan="2">25-44 th</th>
                  <th colspan="2">45-64 th</th>
                  <th colspan="2">&gt; 65</th>
                  <th rowspan="2">Laki</th>
                  <th rowspan="2">Perempuan</th>
                </tr>
                <tr>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                  <th>L</th>
                  <th>P</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl51'){?>
          <div class="box">
            <div class="box-body" style="margin-top: 10px;">
                <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampil_rl51()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl51()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
                <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Kode RS</th>
                    <th>Nama RS</th>
                    <th>Bulan</th>
                    <th>KAB/KOTA</th>
                    <th>Tahun</th>
                    <th>KODE PROPINSI</th>
                    <th>No. Urut</th>
                    <th>Jenis Kegiatan</th>
                    <th>Laki-laki</th>
                    <th>Wanita</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl52'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
              <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
              <input type="text" class="datepicker" id="tgl1" size="4">
              <input type="text" class="datepicker" id="tgl2" size="4">
              <a onclick="tampil_rl52()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
              <a onclick="unduhrl52()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
            </div>
            <div class="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>Kode RS</th>
                  <th>Nama RS</th>
                  <th>Bulan</th>
                  <th>KAB/KOTA</th>
                  <th>Tahun</th>
                  <th>KODE PROPINSI</th>
                  <th>No. Urut</th>
                  <th>Jenis Kegiatan</th>
                  <th>Laki-laki</th>
                  <th>Wanita</th>
                  <th>Jumlah</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl53'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl53()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl53()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="2">KODE PROPINSI</th>
                  <th rowspan="2">KAB/KOTA</th>
                  <th rowspan="2">Kode RS</th>
                  <th rowspan="2">Nama RS</th>
                  <th rowspan="2">Bulan</th>
                  <th rowspan="2">Tahun</th>
                  <th rowspan="2">No. Urut</th>
                  <th rowspan="2">KODE ICD 10</th>
                  <th rowspan="2">Descripsi</th>
                  <th colspan="2" width="20%">Pasien Keluar hidup Menurut jenis Kelamin</th>
                  <th colspan="2" width="20%">Pasien Keluar Mati Menurut Jenis Kelamin</th>
                  <th rowspan="2">TOTAL ALL</th>
                </tr>
                <tr>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php } else if($mode=='rl54'){?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="tampilrl54()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhrl54()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th rowspan="2">KODE PROPINSI</th>
                  <th rowspan="2">KAB/KOTA</th>
                  <th rowspan="2">Kode RS</th>
                  <th rowspan="2">Nama RS</th>
                  <th rowspan="2">Bulan</th>
                  <th rowspan="2">Tahun</th>
                  <th rowspan="2">No. Urut</th>
                  <th rowspan="2">KODE ICD 10</th>
                  <th rowspan="2">Descripsi</th>
                  <th colspan="2">Kasus Baru menurut Jenis Kelamin</th>
                  <th rowspan="2">TOTAL ALL</th>
                </tr>
                <tr>
                  <th>Laki-laki</th>
                  <th>Perempuan</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='pelaporanpneumonia'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="loaddt_pelaporanpneumonia()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhpelaporanpneumonia()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th class="bg bg-gray" rowspan="4">No</th>
                  <th class="bg bg-gray" rowspan="4">UNIT</th>
                  <th class="bg bg-gray" colspan="10" rowspan="1">REALISASI PENEMUAN PENDERITA</th>
                  <th class="bg bg-gray" rowspan="2" colspan="8">Jml Kematian Balita karena Pneumonia</th>
                  <th class="bg bg-gray" colspan="6">ISPA &ge; 5 Th</th>
                </tr>
                <tr>
                  <th class="bg bg-gray" colspan="5">Pneumonia</th>
                  <th class="bg bg-gray" colspan="5">Batuk Bukan Pneumonia</th>
                  <th class="bg bg-gray" rowspan="2" colspan="3">Bukan Pneumonia</th>
                  <th class="bg bg-gray" rowspan="2" colspan="3">Pneumonia</th>
                </tr>
                <tr>
                  <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
                  <th rowspan="2" class="bg bg-yellow">Total</th>
                  <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
                  <th rowspan="2" class="bg bg-yellow">Total</th>
                  <th colspan="3" class="bg bg-aqua">&lt; 1 Th</th>
                  <th colspan="3" class="bg bg-green">1 - 4 Th</th>
                  <th colspan="2" class="bg bg-yellow">Total</th>
                </tr>
                <tr>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-yellow">L</th>
                  <th class="bg bg-yellow">P</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                  <th class="bg bg-info">L</th>
                  <th class="bg bg-danger">P</th>
                  <th class="bg bg-yellow">T</th>
                </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='pelaporanbalitapneumonia'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="loaddt_pelaporanbalitapneumonia()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhpelaporanbalitapneumonia()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr class="bg-gray">
                    <th rowspan="2">No</th>
                    <th rowspan="2">NAMA LENGKAP</th>
                    <th rowspan="2">ALAMAT LENGKAP</th>
                    <th colspan="2">UMUR</th>
                    <th rowspan="2">Tanggal Masuk</th>
                    <th rowspan="2">Tanggal Keluar</th>
                    <th rowspan="2">dx</th>
                    <th rowspan="2">Kondisi Keluar (H/M)</th>
                  </tr>
                  <tr class="bg-gray"><th>L</th><th>P</th></tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='report_stprs'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a id="loaddt_report_stprs" jenis="<?= $jenispemeriksaan ;?>" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a id="unduhreport_stprs" jenis="<?= $jenispemeriksaan ;?>" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
                <a onclick="formatreport_stprs()" class="btn btn-warning btn-sm"><i class="fa  fa-file-excel-o"></i> Contoh Laporan</a>
              </div>
              <div id="listtable">
              <table  style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th rowspan="3">No</th>
                    <th rowspan="3">Kode ICD X</th>
                    <th rowspan="3">Jenis Penyakit</th>
                    <th colspan="11">RAWAT <?= strtoupper($jenispemeriksaan); ?></th>
                  </tr>
                  <tr>
                    <th colspan="8">Golongan Umur</th>
                    <th colspan="2">Total</th>
                    <th rowspan="2">Total</th>
                  </tr>
                  <tr>
                    <th>0-&lt;28 Hr</th>
                    <th>28 Hr-&lt;1 Th</th>
                    <th>1-&lt;4 Th</th>
                    <th>5-&lt;14 Th</th>
                    <th>15-&lt;24 Th</th>
                    <th>25-&lt;44 Th</th>
                    <th>45-&lt;64 Th</th>
                    <th>65+ Th</th>
                    <th>Lk</th>
                    <th>Pr</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div><?php }else if($mode=='laporan_siha'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <input type="text" class="datepicker" id="tgl2" size="4">
                <a onclick="siha_listdata()" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="siha_downloadexcel()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
              <table  style="margin-top:5px;" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>No</th><th>Nama Pasien</th><th>Jenis Kelamin</th><th>Tgl Lahir</th><th>Riwayat</th><th>Diagnosa</th>
                  </tr>
                </thead>
                <tbody id="tampildata">
                </tfoot>
              </table>
              </div>
            </div>
          </div>
          <?php }else if($mode=='report_cakupanpoli'){ ?>
          <div class="box">
            <div class="box-body">
              <div class="toolbar">
                <a href="<?php echo base_url('creport/pelayananrs');?>" class="btn btn-danger btn-sm"><i class="fa  fa-backward"></i> Kembali</a>
                <input type="text" class="datepicker" id="tgl1" size="4">
                <a onclick="loadreport_cakupanpoli()"  class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
                <a onclick="unduhreport_cakupanpoli()" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
              </div>
              <div id="listtable">
                <h3>Laporan <?= $title_page ?></h3>
              <table  style="margin-top:5px;" class="table table-hover table-bordered table-striped" >
                <thead>
                  <tr class="bg bg-gray">
                    <td width="80px"></td>
                    <td>BULAN</td>
                    <td>JUMLAH</td>
                  </tr>
                  <tr id="waktu"></tr>
                  <tr ><td colspan="3">&nbsp;</td></tr>
                  <tr class="bg bg-gray">
                    <td colspan="2">PASIEN LAMA</td>
                    <td>PASIEN BARU</td>
                  </tr>
                  <tr id="jenispasien"></tr>
                  <tr class="bg bg-gray" id="carabayar"><td colspan="3">&nbsp;</td></tr>
                  <tr id="jumlahbayar"><td colspan="3">&nbsp;</td></tr>
                </table>
                
                <table class="table table-hover table-bordered table-striped" >
                  <thead>
                    <tr style="font-size: 15px;"><td colspan="4"><b>10 BESAR PENYAKIT</b></td></tr>
                  </thead>
                    <tr class="bg bg-gray"><td>NO</td><td>DIAGNOSIS</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
                  <tbody id="penyakit">
                  </tbody>
                </table>

                  <table class="table table-hover table-bordered table-striped" >
                  <thead>
                    <tr style="font-size: 15px;"><td colspan="4"><b>10 BESAR TINDAKAN TERSERING</b></td></tr>
                  </thead>
                    <tr class="bg bg-gray"><td>NO</td><td>TINDAKAN</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
                  <tbody id="tindakan">
                  </tbody>
                </table>
              </div>
            </div>
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        