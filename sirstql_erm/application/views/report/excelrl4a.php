<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
<tr><th colspan="29"><b>RL4A Penyakit Rawat Inap</b></th></tr>
<tr>
  <th rowspan="3">No. Urut</th>
  <th rowspan="3">No. DTD</th> 
  <th rowspan="3">No. Daftar Terperinci</th> 
  <th rowspan="3">Golongan Sebab Penyakit</th> 
  <th colspan="18">Jumlah Pasien Kasus Baru Menurut Golongan Umur &amp; Sex</th> 
  <th colspan="2">Kasus Baru Menurut Jenis Kelamin</th>
  <th rowspan="3">Jumlah Kasus Baru</th>
  <th rowspan="3">Jumlah Kunjungan</th>
</tr>
<tr>
  <th colspan="2">0-6 hr</th>
  <th colspan="2">7-28 hr</th> 
  <th colspan="2">29hr-&lt; 1 th</th>
  <th colspan="2">1-4 th</th>
  <th colspan="2">5-14 th</th>
  <th colspan="2">15-24 th</th>
  <th colspan="2">25-44 th</th>
  <th colspan="2">45-64 th</th>
  <th colspan="2">&gt; 65</th>
  <th rowspan="2">Laki</th>
  <th rowspan="2">Perempuan</th>
</tr>
<tr>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
  <th>L</th>
  <th>P</th>
</tr>
  <?php
  $no=0;$total=0;
  foreach ($dtresult as $obj) {
      $total += $obj->kbwanita + $obj->kbpria;
      echo '<tr><td>'. ++$no .'</td><td>'. $no .'</td><td>'.$obj->icd .'</td><td>'.$obj->namaicd .'</td><td>'.$obj->apria .'</td><td>'.$obj->awanita .'</td><td>'.$obj->bpria .'</td><td>'.$obj->bwanita .'</td><td>'.$obj->cpria .'</td><td>'.$obj->cwanita .'</td><td>'.$obj->dpria .'</td><td>'.$obj->dwanita .'</td><td>'.$obj->epria .'</td><td>'.$obj->ewanita .'</td><td>'.$obj->fpria .'</td><td>'.$obj->fwanita .'</td><td>'.$obj->gpria .'</td><td>'.$obj->gwanita .'</td><td>'.$obj->hpria .'</td><td>'.$obj->hwanita .'</td><td>'.$obj->ipria .'</td><td>'.$obj->iwanita .'</td><td>'.$obj->kbpria .'</td><td>'.$obj->kbwanita .'</td><td>'.$total .'</td><td>'.$obj->kunjungan.'</td></tr>';
  }
  ?>
</table>