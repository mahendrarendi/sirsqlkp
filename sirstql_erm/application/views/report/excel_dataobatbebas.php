<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!-- LAPORAN PEMAKAIAN DATA OBAT BEBAS -->
    <table border="1">
      <tr bgcolor="02BD00">
        <td>No</td>
        <td>Waktu</td>
        <td>Nama Obat</td>
        <td>Kode Obat</td>
        <td>Jenis</td>
        <td>Satuan Obat</td>
        <td>Jumlah</td>
        <td>Dosis Racik</td>
        <td>Harga</td>
      </tr>
    <?php if(!empty($dataobat) && $dataobat!=null)
    {
        $no=0;
        foreach ($dataobat as $list) {
          echo '<tr>
                <td>'.++$no.'</td>
                <td>'.date('Y-m-d', strtotime($list->waktu)).'</td>
                <td>'.$list->namabarang.'</td>
                <td>'.$list->kode.'</td>
                <td>'.$list->jenistarif.'</td>
                <td>'.$list->namasatuan.'</td>
                <td>'.$list->jumlahpemakaian.'</td>
                <td>'.$list->kekuatan.'</td>
                <td>'.$list->total.'</td>
              </tr>';
        }
    }
    ?>
    </table>