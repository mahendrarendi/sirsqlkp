<?php 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$fileName.xls");
    header("Pragma: no-cache");
    header("Expires: 0");    
    
    echo "<h3>".$fileName." </h3>";
    echo "<br><br>";
    
    //pasien lama baru
    $table  = '<table border="1"><tr><td>Pasien Baru</td><td>Pasien Lama</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    foreach ($lamabaru as $arr)
    {
        $jumlah = intval($arr['pasienbaru']) + intval($arr['pasienlama']);
        $table .= "<tr><td>".$arr['pasienbaru']."</td><td>".$arr['pasienlama']."</td><td>".$jumlah."</td></tr>";
    }            
    $table .= '</table>';
    echo $table;    
    echo "<br><br>";
    
    //pasien cara bayar
    $table  = '<table border="1"><tr><td>No</td><td>Cara Bayar</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    $jumlahAll = 0;
    $no = 0;
    foreach ($carabayar as $arr)
    {
        $jumlah     = intval($arr['jumlahpasien']);
        $jumlahAll += intval($jumlah);
        $table .= "<tr><td>".++$no."</td><td>".$arr['carabayar']."</td><td>".$jumlah."</td></tr>";
    }
    $table .= "<tr><td></td><td>Total</td><td>".$jumlahAll."</td></tr>";
    $table .= '</table>';
    echo $table;  
    echo "<br><br>";
    
    //pasien per DPJP
    $table  = '<table border="1"><tr><td>No</td><td>Nama Dokter</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    $jumlahAll = 0;
    $no = 0;
    foreach ($perDPJP as $arr)
    {
        $jumlah     = intval($arr['jumlahpasien']);
        $jumlahAll += intval($jumlah);
        $table .= '<tr><td>'.++$no.'</td><td>'.$arr['dokter'].'</td><td>'.$jumlah.'</td></tr>';
    }
    $table .= '<tr><td></td><td>Total</td><td>'.$jumlahAll.'</td></tr>';
    $table .= '</table>';
    echo $table;  
    echo "<br><br>";
    
    //diagnosis
    echo "<h3>Cakupan Diagnosis Rawat Inap</h3>";
    $table  = '<table border="1"><tr><td>No</td><td>Diagnosis</td><td>Jumlah</td></tr>';
    $no=0;
    foreach($diagnosis as $arr)
    {
        $table .= '<tr><td>'.++$no.'</td><td>'.$arr['icd'].' '.$arr['namaicd'].'</td><td>'.$arr['total'].'</td></tr>';
    }
    $table .= '</table>';
    echo $table;  
    echo "<br><br>"; 
    
    //tindakan
    echo "<h3>Cakupan Tindakan Rawat Inap</h3>";
    $table  = '<table border="1"><tr><td>No</td><td>Tindakan</td><td>Jumlah</td></tr>';
    $no=0;
    foreach($tindakan as $arr)
    {
        $table .= '<tr><td>'.++$no.'</td><td>'.$arr['icd'].' '.$arr['namaicd'].'</td><td>'.$arr['total'].'</td></tr>';
    }
    $table .= '</table>';
    echo $table;  
    echo "<br><br>"; 
?>