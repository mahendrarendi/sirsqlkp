<?php 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$fileName.xls");
    header("Pragma: no-cache");
    header("Expires: 0");    
    
    echo "<h3>".$fileName." </h3>";
    //pasien lama baru
    $table  = '<table border="1"><tr><td>Nama Dokter</td><td>Pasien Baru</td><td>Pasien Lama</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    foreach ($lamabaru as $arr)
    {
        $jumlah = intval($arr['pasienlama']) + intval($arr['pasienbaru']);
        $table .= "<tr><td>".$arr['namadokter']."</td><td>".$arr['pasienlama']."</td><td>".$arr['pasienbaru']."</td><td>".$jumlah."</td></tr>";
    }            
    $table .= '</table>';
    echo $table;    
    
    echo "<br><br>";
    //pasien jenis periksa
    $table  = '<table border="1"><tr><td>Nama Dokter</td><td>Pasien Rawat Jalan</td><td>Pasien Rawat Inap</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    foreach ($jenisperiksa as $arr)
    {
        $jumlah = intval($arr['pasienranap']) + intval($arr['pasienralan']);
        $table .= "<tr><td>".$arr['namadokter']."</td><td>".$arr['pasienralan']."</td><td>".$arr['pasienranap']."</td><td>".$jumlah."</td></tr>";
    }            
    $table .= '</table>';
    echo $table;  
    
    echo "<br><br>";
    //pasien cara bayar
    $table  = '<table border="1"><tr><td>Nama Dokter</td><td>Mandiri</td><td>N.PBI</td><td>PBI</td><td>Asuransi Lain</td><td>Jamkesos</td><td>Jampersal</td><td>Jasaraharja</td><td>Bpjs Tenaga Kerja</td><td>Jumlah Pasien</td></tr>';
    $jumlah = 0;
    foreach ($carabayar as $arr)
    {
        $jumlah = intval($arr['mandiri']) + intval($arr['jknnonpbi'])+ intval($arr['jknpbi'])+ intval($arr['asuransilain']) + intval($arr['jamkesos']) + intval($arr['jampersal']) + intval($arr['jasaraharja']) + intval($arr['bpjstenagakerja']);
        $table .= "<tr><td>".$arr['namadokter']."</td><td>".$arr['mandiri']."</td><td>".$arr['jknnonpbi']."</td><td>".$arr['jknpbi']."</td><td>".$arr['asuransilain']."</td><td>".$arr['jamkesos']."</td><td>".$arr['jampersal']."</td><td>".$arr['jasaraharja']."</td><td>".$arr['bpjstenagakerja']."</td><td>".$jumlah."</td></tr>";
    }
    $table .= '</table>';
    echo $table;

    echo "<h3>Cakupan Diagnosis ".$namaPoli." </h3>";
    //diagnosis
    $table  = '<table border="1"><tr><td>No</td><td>Diagnosis</td><td>Jumlah</td></tr>';
    $no=0;
    foreach($diagnosis as $arr)
    {
        $table .= '<tr><td>'. ++$no .'</td><td>'.$arr['icd'].' '.$arr['namaicd'].'</td><td>'.$arr['total'].'</td></tr>';
    }
    $table .= '</table>';
    echo $table;  
    
    echo "<h3>Cakupan Tindakan ".$namaPoli." </h3>";
    //tindakan
    $table  = '<table border="1"><tr><td>No</td><td>Tindakan</td><td>Jumlah</td></tr>';
    $no=0;
    foreach($tindakan as $arr)
    {
        $table .= '<tr><td>'. ++$no .'</td><td>'.$arr['icd'].' '.$arr['namaicd'].'</td><td>'.$arr['total'].'</td></tr>';
    }
    $table .= '</table>';
    echo $table; 

    echo "<h3>Cakupan Tindakan Elektromedik ".$namaPoli." </h3>";
    //tindakan elektromedik
    $table  = '<table border="1"><tr><td>No</td><td>Tindakan</td><td>Jumlah</td></tr>';
    $no=0;
    foreach($tindakanElektromedik as $arr)
    {
        $table .= '<tr><td>'. ++$no .'</td><td>'.$arr['icd'].' '.$arr['namaicd'].'</td><td>'.$arr['total'].'</td></tr>';
    }
    $table .= '</table>';
    echo $table; 
?>