<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">
  <tr>
    <td colspan="8"><?= $judul; ?></td>
  </tr>
  <tr>
    <td colspan="3">Akun</td>
    <td>Umum</td>
    <td>Bpjs</td>
    <td>Jaminan</td>
    <td>Saldo</td>
    <td>Saldo Tahun Berjalan</td>
  </tr>
  <?php 
  if(empty($laporan))
  {
      echo '<tr class="text text-center"><td colspan="8">Data Tidak Ada.</td></tr>';
  }
  else
  {
    $data   = '';
    $saldo  = 0;
    $umum   = 0;
    $bpjs   = 0;
    $jaminan= 0;
    $saldoberjalan = 0;
    $coa    = '';

    $datadetail ='';
    $nama_tipe  = '';
    $totalsaldo = 0;
    $totalsaldoberjalan = 0;
    $totalumum  = 0;
    $totalbpjs  = 0;
    $totaljaminan   = 0;
    $tipe_akun = '';
    $totaltipecoa = 0;
    $x=0;

    $kode_tipe = '';

    foreach ($laporan as $arr)
    {
        $xx = 0;
        if($coa != $arr['kode_akun'] && $coa!='')
        {
            $xx = $x-1;
            echo '<tr><td></td><td>'.$laporan[$xx]['kode_akun'].'</td><td>'.$laporan[$xx]['nama_akun'].'</td><td>'.$umum.'</td><td>'.$bpjs.'</td><td>'.$jaminan.'</td><td>'.$saldo.'</td><td>'.$saldoberjalan.'</td></tr>';
        }
        
        $x++;
        //-- tampil total tipe akun
        if($nama_tipe != $arr['nama_tipe'] && $nama_tipe!='')
        {   
            echo '<tr><td colspan="2"></td><td>Total '.$nama_tipe.'</td><td>'.$totalumum.'</td><td>'.$totalbpjs.'</td><td>'.$totaljaminan.'</td><td>'.$totalsaldo.'</td><td>'.$totalsaldoberjalan.'</td></tr>';
            $totalumum = 0;
            $totalbpjs = 0;
            $totaljaminan = 0;
            $totalsaldo   = 0;
            $totalsaldoberjalan = 0;
        }

        //-- tampil total tipe coa
        if($tipe_akun != $arr['tipe_akun'] && $tipe_akun !='')
        {
            echo '<tr style="border-top:2px solid #a5a5a5;font-weight:bold;background:#fff;"><td colspan="2"></td><td>Total '.$tipe_akun.'</td><td></td><td></td><td></td><td>'.$totaltipecoa.'</td><td>'.$totaltipecoa.'</td></tr>';
            $totaltipecoa = 0;
        }
        //--

        //--Tampil Tipe Coa
        if($tipe_akun!=$arr['tipe_akun'])
        {
            echo '<tr ><td colspan="8"><p style="display:inline; font-size:14px;"><b>'.$arr['tipe_akun'].'</b></p></td></tr>';
        }
        //--

        if($kode_tipe!=$arr['kode_tipe'])
        {
            echo '<tr><td>'.$arr['kode_tipe'].'</b></td><td colspan="7"><b>'.$arr['nama_tipe'].'</b></td></tr>';
        }


        //hitung total akun
        if($coa != $arr['kode_akun'] && $coa!='')
        {
            $saldo = 0;
            $umum = 0;
            $bpjs = 0;
            $jaminan = 0;
            $saldoberjalan = 0;
            $coa = '';
        }
        $saldo += intval($arr['saldo']);
        $umum += intval($arr['umum']);
        $bpjs += intval($arr['bpjs']);
        $jaminan += intval($arr['jaminan']);
        $saldoberjalan += intval($arr['saldoberjalan']);
        $coa = $arr['kode_akun'];


        $totaltipecoa += intval($arr['saldo']);                
        $totalumum += intval($arr['umum']);
        $totalbpjs += intval($arr['bpjs']);
        $totaljaminan += intval($arr['jaminan']);                
        $totalsaldoberjalan += intval($arr['saldoberjalan']);
        $totalsaldo += intval($arr['saldo']);

        $kode_tipe = $arr['kode_tipe'];
        $tipe_akun = $arr['tipe_akun'];
        $nama_tipe = $arr['nama_tipe'];

        $tipe_akun = $arr['tipe_akun'];
  }            
}
echo '<tr><td></td><td>'.$laporan[intval($xx) + 1 ]['kode_akun'].'</td><td>'.$laporan[intval($xx) + 1]['nama_akun'].'</td><td>'.$umum.'</td><td>'.$bpjs.'</td><td>'.$jaminan.'</td><td>'.$saldo.'</td><td>'.$saldoberjalan.'</td></tr>';
echo '<tr><td colspan="2"></td><td>Total '.$nama_tipe.'</td><td>'.$totalumum.'</td><td>'.$totalbpjs.'</td><td>'.$totaljaminan.'</td><td>'.$totalsaldo.'</td><td>'.$totalsaldoberjalan.'</td></tr>';
echo '<tr><td colspan="2"></td><td>Total '.$tipe_akun.'</td><td></td><td></td><td></td><td>'.$totaltipecoa.'</td><td>'.$totaltipecoa.'</td></tr>';
?>
</table>