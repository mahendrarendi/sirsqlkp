<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$judul.xls");
header("Pragma: no-cache");
header("Expires: 0");
 ?>
<table border="1">

  <tr><td colspan="4"><h3><?= qlreplace('%20', ' ', $judul); ?></h3></td></tr>

  <tr>
    <td></td>
    <td>BULAN</td>
    <td>JUMLAH</td>
  </tr>
  <tr>
    <td></td>
    <td><?= ((empty($jmlpasien))?'Data tidak ada':date('F Y', strtotime($jmlpasien[0]->waktu))); ?></td>
    <td><?= ((empty($jmlpasien))?'Data tidak ada':$jmlpasien[0]->jumlahpasien); ?></td>
  </tr>

  <tr>
    <td></td>
    <td>PASIEN BARU</td>
    <td>PASIEN LAMA</td>
  </tr>
  <tr>
    <td></td>
    <td><?= ((empty($jmlpasien))?'Data tidak ada':$jmlpasien[0]->pasienlama); ?></td>
    <td><?= ((empty($jmlpasien))?'Data tidak ada':$jmlpasien[0]->pasienbaru); ?></td>
  </tr>

  <?php 
  if(!empty($jmlpasien))
  {
    $carabayar = '';$jmlbayar = '';
    foreach ($jmlpasien as $obj) {
      $carabayar .= "<td>".$obj->carabayar."</td>";
      $jmlbayar .= "<td>".$obj->jumlahbayar."</td>";
    }
    echo "<tr><td></td>".$carabayar."</tr>";
    echo "<tr><td></td>".$jmlbayar."</tr>";
  }
  ?>

  <tr><td colspan="4"><b>10 BESAR PENYAKIT</b></td></tr>
  <tr><td>NO</td><td>DIAGNOSIS</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
  <?php
  if(!empty($penyakit))
  {
    $no=0;
    foreach ($penyakit as $obj) {
      echo "<tr><td>". ++$no ."</td><td>".$obj->icd." ".$obj->namaicd ."</td><td>".$obj->total ."</td><td>".$obj->percentase."&#37</td></tr>";
    }
  }
  ?>

  <tr><td colspan="4"><br></td></tr>
  <tr><td colspan="4"><b>10 BESAR TINDAKAN TERSERING</b></td></tr>
  <tr><td>NO</td><td>TINDAKAN</td><td>JUMLAH</td><td>PERSENTASE</td></tr>
  <?php
  if(!empty($tindakan))
  {
    $no=0;
    foreach ($tindakan as $obj) {
      echo "<tr><td>". ++$no ."</td><td>".$obj->icd." ".$obj->namaicd ."</td><td>".$obj->total ."</td><td>".$obj->percentase."&#37</td></tr>";
    }
  }
  ?>