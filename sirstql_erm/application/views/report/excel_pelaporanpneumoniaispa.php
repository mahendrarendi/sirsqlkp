<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<table border="1">
  <thead>
    <tr>
      <th class="bg bg-gray" rowspan="4">No</th>
      <th class="bg bg-gray" rowspan="4">UNIT</th>
      <th class="bg bg-gray" colspan="10" rowspan="1">REALISASI PENEMUAN PENDERITA</th>
      <th class="bg bg-gray" rowspan="2" colspan="8">Jml Kematian Balita karena Pneumonia</th>
      <th class="bg bg-gray" colspan="6">ISPA &ge; 5 Th</th>
    </tr>
    <tr>
      <th class="bg bg-gray" colspan="5">Pneumonia</th>
      <th class="bg bg-gray" colspan="5">Batuk Bukan Pneumonia</th>
      <th class="bg bg-gray" rowspan="2" colspan="3">Bukan Pneumonia</th>
      <th class="bg bg-gray" rowspan="2" colspan="3">Pneumonia</th>
    </tr>
    <tr>
      <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
      <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
      <th rowspan="2" class="bg bg-yellow">Total</th>
      <th colspan="2" class="bg bg-aqua">&lt; 1 Th</th>
      <th colspan="2" class="bg bg-green">1 - &lt;5 Th</th>
      <th rowspan="2" class="bg bg-yellow">Total</th>
      <th colspan="3" class="bg bg-aqua">&lt; 1 Th</th>
      <th colspan="3" class="bg bg-green">1 - 4 Th</th>
      <th colspan="2" class="bg bg-yellow">Total</th>
    </tr>
    <tr>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-yellow">T</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-yellow">T</th>
      <th class="bg bg-yellow">L</th>
      <th class="bg bg-yellow">P</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-yellow">T</th>
      <th class="bg bg-info">L</th>
      <th class="bg bg-danger">P</th>
      <th class="bg bg-yellow">T</th>
    </tr>
    </thead>
<?php if(!empty($rajal) && $rajal!=null)
{
    $no=0;

    echo '<tr><td>1</td><td>RAWAT JALAN</td><td>'.$rajal['pneumonia_k1L'].'</td><td>'.$rajal['pneumonia_k1W'].'</td><td>'.$rajal['pneumonia_k5L'].'</td><td>'.$rajal['pneumonia_k5W'].'</td><td>'.$rajal['pneumonia_k5T'].'</td><td>'.$rajal['K1L'].'</td><td>'.$rajal['K1W'].'</td><td>'.$rajal['K5L'].'</td><td>'.$rajal['K5W'].'</td><td>'.$rajal['K5T'].'</td><td>'.$rajal['meninggal_k1L'].'</td><td>'.$rajal['meninggal_k1W'].'</td><td>'.$rajal['meninggal_k1T'].'</td><td>'.$rajal['meninggal_4L'].'</td><td>'.$rajal['meninggal_4W'].'</td><td>'.$rajal['meninggal_4T'].'</td><td>'.$rajal['meninggal_4TL'].'</td><td>'.$rajal['meninggal_4TW'].'</td><td>'.$rajal['L5L'].'</td><td>'.$rajal['L5W'].'</td><td>'.$rajal['L5T'].'</td><td>'.$rajal['pneumonia_5L'].'</td><td>'.$rajal['pneumonia_5W'].'</td><td>'.$rajal['pneumonia_5T'].'</td></tr>';
    echo '<tr><td>2</td><td>RAWAT INAP</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
    echo '<tr><td></td><td>Jumlah</td><td>'.$rajal['pneumonia_k1L'].'</td><td>'.$rajal['pneumonia_k1W'].'</td><td>'.$rajal['pneumonia_k5L'].'</td><td>'.$rajal['pneumonia_k5W'].'</td><td>'.$rajal['pneumonia_k5T'].'</td><td>'.$rajal['K1L'].'</td><td>'.$rajal['K1W'].'</td><td>'.$rajal['K5L'].'</td><td>'.$rajal['K5W'].'</td><td>'.$rajal['K5T'].'</td><td>'.$rajal['meninggal_k1L'].'</td><td>'.$rajal['meninggal_k1W'].'</td><td>'.$rajal['meninggal_k1T'].'</td><td>'.$rajal['meninggal_4L'].'</td><td>'.$rajal['meninggal_4W'].'</td><td>'.$rajal['meninggal_4T'].'</td><td>'.$rajal['meninggal_4TL'].'</td><td>'.$rajal['meninggal_4TW'].'</td><td>'.$rajal['L5L'].'</td><td>'.$rajal['L5W'].'</td><td>'.$rajal['L5T'].'</td><td>'.$rajal['pneumonia_5L'].'</td><td>'.$rajal['pneumonia_5W'].'</td><td>'.$rajal['pneumonia_5T'].'</td></tr>';
    echo '<tr><td colspan="26"></td></tr>';
    echo '<tr><th colspan="26">RINCIAN ASAL PASIEN</th></tr>';
}


if(!empty($rajalbykab) && $rajalbykab!=null)
{
  $no=0;
  foreach($rajalbykab as $tampil){
    $no++;
    echo '<tr><td>'.$no.'</td><td>'.$tampil->kabupatenkota.'</td><td>'.$tampil->pneumonia_k1L.'</td><td>'.$tampil->pneumonia_k1W.'</td><td>'.$tampil->pneumonia_k5L.'</td><td>'.$tampil->pneumonia_k5W.'</td><td>'.$tampil->pneumonia_k5T.'</td><td>'.$tampil->K1L.'</td><td>'.$tampil->K1W.'</td><td>'.$tampil->K5L.'</td><td>'.$tampil->K5W.'</td><td>'.$tampil->K5T.'</td><td>'.$tampil->meninggal_k1L.'</td><td>'.$tampil->meninggal_k1W.'</td><td>'.$tampil->meninggal_k1T.'</td><td>'.$tampil->meninggal_4L.'</td><td>'.$tampil->meninggal_4W.'</td><td>'.$tampil->meninggal_4T.'</td><td>'.$tampil->meninggal_4TL.'</td><td>'.$tampil->meninggal_4TW.'</td><td>'.$tampil->L5L.'</td><td>'.$tampil->L5W.'</td><td>'.$tampil->L5T.'</td><td>'.$tampil->pneumonia_5L.'</td><td>'.$tampil->pneumonia_5W.'</td><td>'.$tampil->pneumonia_5T.'</td></tr>';
   }
}
?>
</table>



