<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$title.xls");
header("Pragma: no-cache");
header("Expires: 0");

echo "<table border='1'>";
echo "<tr><td colspan='10'>FAKTUR LUNAS HUTANG USAHA, Tanggal Pelunasan: ".$tanggal1." - ".$tanggal2."</td></tr>";
echo "<tr><td colspan='10'> &nbsp;</td></tr>";

$no=0; $distributor='null'; 
$tot_tagihan=0; $tot_potongan=0; $tot_ppn=0; $tot_totaltagihan=0; $tot_dibayar=0;
$sub_tagihan=0; $sub_potongan=0; $sub_ppn=0; $sub_totaltagihan=0; $sub_dibayar=0;
foreach ($fakturlunas as $arr)
{
    // menampilkan subtotal     
    if($arr['distributor'] != $distributor && $distributor !='null')
    {
        echo "<tr><td colspan='5'>SUBTOTAL</td><td>".$sub_tagihan."</td><td>".$sub_potongan."</td><td>".$sub_ppn."</td><td>".$sub_totaltagihan."</td><td>".$sub_dibayar."</td></tr>";
        $sub_tagihan=0; $sub_potongan=0; $sub_ppn=0; $sub_totaltagihan=0; $sub_dibayar=0;
    }
    
    // menampilkan data distributor    
    if($arr['distributor'] != $distributor)
    {
        echo "<tr><td colspan='10'> &nbsp;</td></tr>";
        echo "<tr><td colspan='10'><b>".$arr['distributor']."  - Telp. ".$arr['telepon']."</b></td></tr>";
        echo "<tr><td colspan='10'><b>".$arr['alamat']."</b></td></tr>";
        echo "<tr><th>No</th><th>NO.FAKTUR</th><th>TANGGALFAKTUR</th><th>JATUHTEMPO</th><th>PELUNASAN</th><th>TAGIHAN</th><th>POTONGAN</th><th>PPN</th><th>TOT.TAGIHAN</th><th>DIBAYAR</th></tr>";
    
        $no=0;
    }
    
    //hitung sub total
    $sub_tagihan += $arr['tagihan'];
    $sub_potongan += $arr['potongan'];
    $sub_ppn += $arr['ppn'];
    $sub_totaltagihan += $arr['totaltagihan'];
    $sub_dibayar += $arr['dibayar'];
    
    //hitung total
    $tot_tagihan += $arr['tagihan'];
    $tot_potongan += $arr['potongan'];
    $tot_ppn += $arr['ppn'];
    $tot_totaltagihan += $arr['totaltagihan'];
    $tot_dibayar += $arr['dibayar'];
    
    //set distributor
    $distributor = $arr['distributor'];
    
    
    echo "<tr>";
    echo "<td>". ++$no ."</td>";
    echo "<td>".$arr['nofaktur']."</td>";
    echo "<td>".$arr['tanggalfaktur']."</td>";
    echo "<td>".$arr['tanggaljatuhtempo']."</td>";
    echo "<td>".$arr['tanggalpelunasan']."</td>";
    echo "<td>".$arr['tagihan']."</td>";
    echo "<td>".$arr['potongan']."</td>";
    echo "<td>".$arr['ppn']."</td>";
    echo "<td>".$arr['totaltagihan']."</td>";
    echo "<td>".$arr['dibayar']."</td>";
    echo "</tr>";
}
// menampilkan subtotal terakhir
echo "<tr><td colspan='5'>SUBTOTAL</td><td>".$sub_tagihan."</td><td>".$sub_potongan."</td><td>".$sub_ppn."</td><td>".$sub_totaltagihan."</td><td>".$sub_dibayar."</td></tr>";

// menampilkan Total 
echo "<tr><td colspan='10'> &nbsp;</td></tr>";
echo "<tr><td colspan='5'>TOTAL ALL</td><td>".$tot_tagihan."</td><td>".$tot_potongan."</td><td>".$tot_ppn."</td><td>".$tot_totaltagihan."</td><td>".$tot_dibayar."</td></tr>";

echo "</table>";
?>

    




