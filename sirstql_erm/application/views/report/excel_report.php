<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");
if($mode=='laporandataobat'){?>
<!-- LAPORAN PEMAKAIAN DATA OBAT -->
    <table border="1">
      <tr bgcolor="02BD00">
        <td>No</td>
        <td>Waktu</td>
        <td>Nama Obat</td>
        <td>Kode Obat</td>
        <td>Jenis</td>
        <td>Satuan Obat</td>
        <td>Jumlah</td>
        <td>Dosis Racik</td>
        <td>Harga</td>
      </tr>
    <?php if(!empty($dataobat) && $dataobat!=null)
    {
        $no=0;
        foreach ($dataobat as $list) {
          echo '<tr>
                <td>'.++$no.'</td>
                <td>'.date('Y-m-d', strtotime($list->waktu)).'</td>
                <td>'.$list->namabarang.'</td>
                <td>'.$list->kode.'</td>
                <td>'.$list->jenistarif.'</td>
                <td>'.$list->namasatuan.'</td>
                <td>'.$list->jumlahpemakaian.'</td>
                <td>'.$list->kekuatan.'</td>
                <td>'.$list->total.'</td>
              </tr>';
        }
    }
    ?>
    </table>
<?php }else if($mode=='kasir_versipasien'){
    $htmlText = ' <style>.setTable{font-size:small;font-family: "Palatino Linotype", "Book Antiqua", Palatino, serif;color: #333333;border:none;}"</style> <div style="top:0px;"><img width="50%;" src="'.base_url().'assets/images/headerkuitansismall.jpg" ></div><div style="float: none;"><table class="setTable" border="0" border-collapse: collapse">';
    $htmlText .='<tr><td colspan="2"> &nbsp;</td></tr><tr><td colspan="2"> &nbsp;</td></tr><tr><td colspan="2"> &nbsp;</td></tr><tr><td colspan="2"> &nbsp;</td></tr></table><table class="setTable" border="1" border-collapse: collapse">';
if ($infotagihan != null)
{
    foreach ($infotagihan as $obj) {
        # code...
    $htmlText .='<tr><td >Nama</td><td colspan="2" align="left">'.$obj->namalengkap.'</td></tr>';
    $htmlText .='<tr><td >Alamat</td><td colspan="2" align="left">'.$obj->alamat.'</td></tr>';
    $htmlText .='<tr><td >Penanggung</td><td colspan="2" align="left">'.$obj->penanggung.'</td></tr>';
    $htmlText .='<tr><td >No RM</td><td colspan="2" align="left">'.$obj->norm.'</td></tr>';
    }
}
$htmlText.='</table><table class="setTable" border="1" style="padding:2px;" border-collapse: collapse">';
$htmlText .= '<tr><td>No/Item</td><td align="right">Jml</td><td align="right">Nominal</td></tr>';
$no=0;$subtotal  = 0;$total=0;$tipesebelum='';
foreach ($detailtagihan as $obj) {
    if ($tipesebelum != $obj->jenistarif)
    {
        if ($tipesebelum != '') $htmlText .= '<tr><td width="30px" align="right" colspan="2">:: Subtotal ::</td><td align="right">'.$subtotal.'</td></tr>';
        $htmlText .= '<tr><td colspan="3">'.$obj->jenistarif.'</td></tr>';
        $subtotal  = 0;
    }
    $subtotal += (intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan)/intval($obj->kekuatanbarang));
    $total    += (intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan)/intval($obj->kekuatanbarang));
    $htmlText .= '<tr><td width="30px">'. ++$no .'. '.$obj->nama.((intval($obj->kekuatan)/intval($obj->kekuatanbarang)>1)? ' ['.intval($obj->kekuatan).'/'.intval($obj->kekuatanbarang).']' :'' ).'</td><td style="font-size:x-small">'.intval($obj->jumlah) .'</td><td>'. number_format_dot((intval($obj->sewa) + intval($obj->nonsewa)) * intval($obj->jumlah) * (intval($obj->kekuatan) / intval($obj->kekuatanbarang))).'</td></tr>'; 
    $tipesebelum = $obj->jenistarif;
}
    $htmlText .= '<tr><td width="30px" align="right" colspan="2">:: Subtotal ::</td><td align="right">'.number_format_dot($subtotal).'</td></tr>';
    $htmlText .= '<tr><td width="30px" align="right" colspan="2">:: Total ::</td><td>'.number_format_dot($total).'</td></tr>';
    $htmlText .= '</table><table class="setTable" border="1" border-collapse: collapse">';
    $selisih=0;$waktutagih='';
    foreach ($infotagihan as $obj) {
        $selisih   = $obj->nominal - $obj->dibayar;
        $htmlText .= '<tr><td>No Nota</td><td colspan="2" align="left">'.$obj->idtagihan.'</td></tr>';
        $htmlText .= '<tr><td>Waktu Tagih</td><td colspan="2" align="left">'.$obj->waktutagih.'</td></tr>';
        $htmlText .= '<tr><td>Waktu Bayar</td><td colspan="2" align="left">'.$obj->waktubayar.'</td></tr>';
        $htmlText .= '<tr><td>Nominal</td><td colspan="2" align="left">'.number_format_dot($obj->nominal).'</td></tr>';//$obj->nominal)
        $htmlText .= '<tr><td>Dibayar</td><td colspan="2" align="left">'.number_format_dot($obj->dibayar).'</td></tr>';
        $htmlText .= '<tr><td>Selisih</td><td colspan="2" align="left">'.number_format_dot($selisih).'</td></tr>';
        $htmlText .= '<tr><td>Jenis Tagihan</td><td colspan="2" align="left">'.$obj->jenistagihan.'</td></tr>';
    }
    $htmlText .= '<tr><td colspan="3" align="center">dicetak oleh '.$this->session->userdata('username').', pada '.date("Y-m-d H:i:s").'</td></tr>';
    $htmlText .= '</table></div>';
    echo $htmlText;
}else if($mode=='pelayananperiksa'){ ?>
<!-- pemeriksaan pasien -->
<table border="1">
  <tr bgcolor="02BD00">
    <td>No</td>
    <td>NORM</td>
    <td>Nama Pasien</td>
    <td>NoTelp</td>
    <td>NOSEP</td>
    <td>WAKTU DAFTAR</td>
    <td>DOKTER DPJP</td>
    <td>KLINIK ASAL</td>
    <td>KLINIK TUJUAN (Antrian)</td>
    <td>STATUS</td>
  </tr>
  <?php
  if(!empty($dtperiksa) && $dtperiksa!=null)
    { 
  $no=0;
  foreach ($dtperiksa as $list) {
   echo'<tr><td>'.++$no.'</td><td>'.$list->norm.'</td><td>'.$list->namalengkap.'</td><td>'.$list->notelpon.'</td><td>'.$list->nosep.'</td><td>'.$list->waktu.'</td><td>'.$list->namadokter.'</td><td>'.$list->namadokter.'</td><td>'.$list->unittujuan.' ('.$list->noantrian.')</td><td>'.$list->status.'</td></tr>';   
  }
}
  ?>
  </table>
<?php } ?>