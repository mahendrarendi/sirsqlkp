<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=$titel.xls");
header("Pragma: no-cache");
header("Expires: 0");

    echo '<h1>JASA MEDIS DOKTER</h1>';
    echo '<h2>'. (($dokter!='PilihDokter')? $dokter : '').'</h2>';
    echo "<table border='1'>";
    for( $x=0; $x<count($carabayar); $x++)
    {
        $waktu='';
        echo "<tr bg-color='#0073b7'><td colspan='18'>".$carabayar[$x]."</td></tr>";
        echo "<tr ><td>". (($dokter=='')? 'Dokter' : (($order=='bulan')?'Tgl':'') ) ."</td><td>ICD</td><td>Komponen</td><td>jumlah</td><td>JM</td><td>Tot.JM</td><td>JRS</td><td>Tot.JRS</td><td>Nakes</td><td>Tot.Nakes</td><td>BHP</td><td>Tot.BHP</td><td>Akomodasi</td><td>Tot.Akomodasi</td><td>Margin</td><td>Tot.Margin</td><td>Sewa</td><td>Tot.Sewa</td></tr>";
        for($y=0; $y<count($jasamedis[$x]);$y++)
        {
            echo "<tr bg-color='#f39c10'><td>".(($dokter=='')? $jasamedis[$x][$y]->namadokter : (($order=='bulan')? (($waktu==$jasamedis[$x][$y]->waktu)?'':$jasamedis[$x][$y]->waktu):'') )."</td><td>".$jasamedis[$x][$y]->icd."</td>"
                    . "<td>".$jasamedis[$x][$y]->namaicd."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->jumlah, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->jasaoperator, 0,'','.')."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumjasaoperator, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->jasars)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumjasars, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->nakes)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumnakes, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->bhp)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumbhp, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->akomodasi)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumakomodasi, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->margin)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->summargin, 0,'','.')."</td>"
                    . "<td>".number_format($jasamedis[$x][$y]->sewa)."</td>"
                    . "<td class='text-bold'>".number_format($jasamedis[$x][$y]->sumsewa, 0,'','.')."</td></tr>";
        
            $waktu = $jasamedis[$x][$y]->waktu;
        }
    }
    echo "</table>";