<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url('dashboard');?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b><i class="fa  fa-hospital-o"></i></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b><?= APP_NAME; ?></b></span>
    </a>
    
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <!--<span class="icon-bar"></span>-->
      </a>
      

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!--START TEXT BOX GENERATE BARCODE-->
          <li class="dropdown" style="padding-top: 2.2%; padding-right: 6px;"> <div id="qrcode" style="width:100px; height:100px; margin-top:15px;display: none;"></div> <input class="form-control" type="text" id="textqrcode" size="12" placeholder="Generate Barcode "></li>
          <!--END TEXT BOX GENERATE BARCODE-->
          
           <?php if( $this->pageaccessrightbap->checkAccessRight(NOTIF_FARMASI) ) {  ?>
           <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger" id="label_notiflayananfarmasi"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="header_notiflayananfarmasi"></li>
              <li>
                <ul class="menu" id="body_notiflayananfarmasi"></ul>
              </li>
              <li class="footer bg bg-yellow-gradient"><i class="small" style="padding-left:10px;">#Klik untuk konfirmasi sudah dilayani.</i></li>
            </ul>
          </li>
          <?php } ?>
          
          
          <?php if($this->pageaccessrightbap->checkAccessRight(V_GANTISHIF)){?>
            <li><a href="#" id="gantishif" class="bg-red" title="Ganti Shif"><b><i class="fa fa-sign-out fa-lg"></i> <span class="hidden-xs"></span></b></a></li>
          <?php } ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-user-md fa-lg"></i> <span class="hidden-xs"></span> </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header" >
                <i class="fa  fa-user-md fa-lg" style="font-size: 6em; color:#fff;"></i>
                <p> <?php echo 'Login Sebagai <br>'. $this->session->userdata('username').",". $this->session->userdata('unitterpilih')."<br>".shiftjaga(date('Y-m-d H:i:s')); ?></p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left"><a class="btn btn-default btn-flat" onclick="ubahpassword()"><i class="fa fa-key fa-default"></i> Ganti Password</a></div>
                <div class="pull-right"><a href="<?php echo base_url('clogin/log_out') ?>" class="btn btn-default btn-flat"> <i class="fa fa-sign-out fa-default"></i> Log out</a></div>
              </li>
            </ul>
          </li>
          
        </ul>
      </div>
    </nav>
    
  </header>
