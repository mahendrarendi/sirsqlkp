<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}.label-default { background-color: #676767;color: #fff;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        
        <div class="box">
            
          <div class="box-body">
            <div class="col-md-9 row"> 
                <select id="idunit" name="unit" onchange="reloadPemeriksaanByUnit()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%"></select>
                <select id="idbangsal" name="bangsal" onchange="reload_pemerisaan_by_bangsal()"  class="col-xs-12 col-md-2 select2"></select>
                <input type="text" size="7" class="datepicker" name="tanggal" >
                <input type="text" size="7" class="datepicker" name="tanggal2" >
                <a class="btn btn-info btn-sm" id="tampilkasir"> <i class="fa fa-desktop"></i> Tampil</a>
                <a class="btn btn-warning btn-sm" id="refreshkasir"><i class="fa  fa-refresh"></i> Refresh</a>
                
                <div class="form-group" style="margin-top:10px;">
                  <select class="select2 col-md-4" id="caripasien" ><option value="">Cari By Pasien</option></select> 
                  <label><input type="checkbox" name="isbelumdibayar" style="margin-left:10px;" onchange="reloadKasir()" /> List Belum Dibayar</label>
                </div>
            
            </div>
              
            <table id="dtkasir" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>NO</th>
                <th>WAKTU</th>
                <th>NO.RM</th>
                <th>NAMA PASIEN</th>
                <th>NO.SEP</th>
                <th>KELAS</th>
                <th>CARA BAYAR</th>
                <th>STATUS PULANG</th>
                <th>STATUS TAGIHAN</th>
                <th></th>
                <th>STATUS PEMERIKSAAN</th>
                <th width='105'><i class="fa  fa-print"></i> CETAK</th>
                <th width='70'>AKSI</th>
                <th class="none"></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
              <div>
                  <b>Panduan penggunaan:</b>
                  <ol>
                      <li>Panggil pasien dengan klik menu (panggilan kasir)</li>
                      <li>Informasikan Jumlah Tagihan Ke Pasien</li>
                      <li>Klik menu selesai <b>jika pasien sepakat dengan detail tagihan</b></li>
                      <li><b>Jika pasien menghendaki pengurangan biaya</b> (misal ada obat yg tidak ingin di ambil), konfirmasikan ke petugas terkait</li>
                      <li>Kembali ke proses no 2 dan no 3</li>
                      <li>Terakhir selesaikan pembayaran. Tunggu pasien sampai menyerahkan uang tunai atau menggunakan layanan debit, baru selesaikan proses pembayaran.</li>
                      
                  </ol>
              </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->