<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
            <div>
                <select id="idbangsal" name="bangsal" onchange="cariByBangsal()" style="margin-right:4px;" class="sel2 col-md-2"></select>
                <input type="text" class="datepicker" name="tanggal" id="tanggal" size="7" />
                <a class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a>
                <a class="btn btn-warning btn-sm" href="javascript:void(0)" onclick="window.location.reload()"><i class="fa fa-refresh"></i> Refresh</a>
                <a class="btn btn-success btn-sm" href="<?= base_url('cpelayananranap/tambahrencanaperawatan')?>"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan</a>
                <a class="btn btn-primary btn-sm" onclick="tambah_rencana_perawatan_paket()"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan [Paket]</a>
            </div>
            <div class="box">
                <div class="box-body">
                    <table id="dtlaboratranap" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                      <thead>
                      <tr class="header-table-ql">
                          <th></th>
                        <th>NO.RM</th>
                        <th>NAMA PASIEN</th>
                        <th>TGL. LAHIR</th>
                        <th>NO.SEP</th>
                        <th>WAKTU Masuk</th>
                        <th>DOKTER DPJP</th>
                        <th>BANGSAL</th>
                        <th></th>
                      </tr>
                      </thead>
                      <tbody>
                      </tfoot>
<!--                      <thead>
                        <tr>
                            <th></th>
                            <th>First name</th>
                            <th>Last name</th>
                            <th>Position</th>
                            <th>Office</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tfoot>-->

                    </table>
                </div>
            </div>
        
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->