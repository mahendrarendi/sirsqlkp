<style type="text/css">
        .tabriwayat{background-color: #d2d6de;}
        .nav-tabs-custom>.nav-tabs>li.active>a {
            background-color: #f39c12;
            color: #444;
        }
        .form-pelayanan
        {
            display: block;
            width: 100%;
            height: 22px;
            padding: 2px 4px;
             font-size: 12.8px; 
            line-height: 1.42857143;
            color: #222e32;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 2px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .cHidden{background: transparent;border: none; height: 0px;margin:0px;padding: 0px;display: inherit;}
        .riwayatparagraf{background-color: #ffffff;padding:6px;border-radius:2px;}
</style>

<div class="box-body" style="background-color:#fff;">
    <div class="box-header">
        <a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
        <a onclick="cetak_hasillab()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><i class="fa fa-flask"></i> Laboratorium</a>
        <a onclick="cetak_hasilelektromedik()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><img src="<?= base_url('assets/images/icons/radiasi.svg')?>"> Elektromedik</a> 
    </div>
    <div class="row col-md-12">
        <div class="col-sm-3" id="pemeriksaanklinik_profile"></div>
        <!-- /.col -->
        <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile"></div>               
    </div>
</div>
<br>
<a data-toggle="tooltip" data-original-title="VitalSign" id="periksaVitalsign" onclick="setfocusVitalsign()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-male fa-lg"></i></a>
<a data-toggle="tooltip" data-original-title="Radiologi" id="periksaElektromedik" onclick="setfocusElektromedik()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/images/icons/radiasi.svg')?>" width="20"></a>
<a data-toggle="tooltip" data-original-title="Echocardiography" id="periksaEchocardiography" onclick="setfocusEchocardiography()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-heartbeat fa"></i></a>
<a data-toggle="tooltip" data-original-title="Laboratorium" id="periksaLaboratorium" onclick="setfocuslaboratorium()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-flask"></i></a>
<a data-toggle="tooltip" data-original-title="Diagnosa" id="periksaDiagnosa" onclick="setfocusdiagnosa()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/icon/diagnoses-white.svg')?>" width="20"></a>
<a data-toggle="tooltip" data-original-title="Tindakan" id="periksaTindakan" onclick="setfocustindakan()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-medkit"></i></a>
<a data-toggle="tooltip" data-original-title="Bhp/Obat" id="periksaBhp" onclick="setfocusobatbhp()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-toggle-on"></i></a>
<section class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active tabriwayat"><a href="#activity" id="btnPemeriksaan" data-toggle="tab">Pemeriksaan</a></li>
              <li class="tabriwayat"><a href="#timeline" id="btnRiwayatPasien" data-toggle="tab">Riwayat Pasien</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
                      <input type="hidden" name="idperiksa">
                      <input type="hidden" name="idpendaftaran">
                      <input type="hidden" name="idunit">
                      <input type="hidden" name="norm">
                      <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter DPJP</label>
                        <div class="col-sm-4"><input disabled type="text" name="dokter" value="" class="form-control"></div>
                      </div>



                      <!-- VitalSign ===================== -->

                      <!-- LIST HASIL VitalSign -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title">VitalSign</h3>

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">

                        <table class="table" style="width: 98%">
                          <thead>
                            <tr class="bg-yellow">
                              <th>Parameter</th>
                              <th>Hasil</th>
                              <th>NDefault</th>
                              <th>NRujukan</th>
                              <th colspan="2">Satuan</th>
                            </tr>
                          </thead>
                          <tbody id="listVitalsign">
                          </tbody>
                        </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                        </div>
                      </div>
                      <!-- ============================== -->

                      <div id="pemeriksaanInputAnamnesa"><!-- ditampilkan di js --></div> 

                      <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Data Obyektif</label>
                        <div class="col-sm-9">
                          <textarea disabled class="form-control textarea" name="keterangan" placeholder="Keterangan Pemeriksaan" rows="4"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Rencana/Catatan</label>
                        <div class="col-sm-9">
                          <textarea disabled class="form-control textarea" name="rekomendasi"  rows="4"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Alergi Obat</label>
                        <div class="col-sm-9" >
                          <textarea disabled class="form-control textarea"  name="alergiobat" placeholder="alergi obat" rows="4"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                          <label class="col-md-2 control-label">Hasil Radiografi</label>
                          <div class="col-md-9 row">
                          <table class="table">
                            <tbody>
                              <tr>
                                    <td colspan="2" id="viewHasilRadiografi">
                                      <!--tampil dari js-->
                                    </td>
                              </tr>

                            </tbody>
                            </table>
                          </div>
                      </div>

                      <div class="form-group">
                      <label for="" class="col-md-2 control-label">Hasil Expertise</label>
                      <div class="col-md-9"><textarea class="form-control textarea" disabled name="keteranganradiologi" rows="18"></textarea></div>
                     </div>

                      <div class="form-group">
                        <label class="col-md-2 control-label">Saran</label>
                        <div class="col-md-9"><textarea disabled class="form-control textarea" name="saranradiologi" rows="12"></textarea></div>
                      </div>



                      <!-- LIST HASIL RADIOLOGI -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title">Elektromedik</h3>

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">

                          <table class="table">
                            <thead>
                              <tr class="bg-yellow">
                                <th>ICD</th>
                                <th>Nama ICD</th>
                                <th colspan="2">Biaya</th>
                              </tr>
                            </thead>
                            <tbody id="listRadiologi">
                            </tbody>
                          </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                        </div>
                      </div>
                      <!-- ============================== -->

                        <!-- FORM EKOKARDIOGRAFI -->
                       <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                        <div class="box box-default">            
                        <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>ECHOCARDIOGRAPHY</b></h3></div>
                        <!-- /.box-header -->
                        <div class="box-body">
                        <div class="form-group">

                            <div class="col-md-12">
                                <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                        <tr class="header-table-ql">
                                            <th>Measurement</th>
                                            <th>Result</th>
                                            <th>Normal (Adult)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listEkokardiografi">                            
                                    </tbody>
                                </table>
                            </div>
                          <div class="col-sm-12">
                            <label>Keterangan</label>
                            <textarea class="form-control textarea" id="keteranganekokardiografi" name="keteranganekokardiografi" rows="20" disabled></textarea>
                          </div>
                        </div>
                        </div>
                        <!-- /.box-body -->

                      </div>
                        </div>
                       </div>
                      <!-- END FORM EKOKARDIOGRAFI -->

                      <!-- LABORATORIUM ===================== -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                          <span class="label label-default">Keterangan Laboratorium</span>
                          <textarea disabled class="form-control" name="keteranganlaboratorium" rows="3"></textarea>
                        </div>
                      </div>
                      <!-- LIST HASIL LABORATORIUM -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title">Laboratorium</h3>

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">

                          <table class="table">
                          <thead>
                            <tr class="bg-yellow">
                              <th>Parameter</th>
                              <th>Hasil</th>
                              <th>NDefault</th>
                              <th>NRujukan</th>
                              <th>Satuan</th>
                              <th colspan="2">Biaya</th>
                            </tr>
                          </thead>
                          <tbody id="listLaboratorium">
                          </tbody>
                        </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                        </div>
                      </div>
                      <!-- ============================== -->

                      <!-- DIAGNOSA ===================== -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                          <span class="label label-default">Diagnosis DPJP</span>
                          <textarea class="form-control" name="diagnosa" rows="2" disabled></textarea>
                        </div>
                      </div>
                      <!-- LIST HASIL DIAGNOSA -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">

                              <!--ICD 10-->
                                <div class="box box-default box-solid">
                                    <div class="box-header with-border">
                                      <h5 class="text-bold">ICD 10 (Diagnosa)</h5>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table class="table">
                                            <thead>
                                              <tr class="bg-yellow">
                                                <th>ICD</th>
                                                <th>Nama ICD</th>
                                                <th>Alias</th>                                        
                                                <th>Level</th>
                                              </tr>
                                            </thead>
                                            <tbody id="listDiagnosa10">
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                <!--end icd 10--> 

                                <!--ICD 10-->
                                <div class="box box-default box-solid">
                                    <div class="box-header with-border">
                                      <h5 class="text-bold">ICD 9 (Tindakan)</h5>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <table class="table">
                                            <thead>
                                              <tr class="bg-yellow">
                                                <th>ICD</th>
                                                <th>Nama ICD</th>                                        
                                                <th>Alias</th>
                                                <th></th>
                                              </tr>
                                            </thead>
                                            <tbody id="listDiagnosa9">
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                <!--end icd 10--> 
                          </div>
                          <!-- /.box-body -->
                        </div>
                        </div>
                      </div>
                      <!-- ============================== -->

                      <!-- TINDAKAN ===================== -->
                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title">Tindakan</h3>

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                            <table class="table">
                              <thead>
                                <tr class="bg-yellow">
                                  <th>ICD</th>
                                  <th>Nama ICD</th>
                                  <th>Penerima JM</th>
                                  <th width="5%">Jumlah</th>
                                  <th colspan="2">Biaya</th>
                                </tr>
                              </thead>
                              <tbody id="listTindakan">
                              </tbody>
                            </table>
                          </div>

                          <div class="col-md-12 row" style="margin-top:10px;">
                            <div class="col-xs-12 col-md-3 ">
                              <label>Kondisi Keluar</label>
                              <input type="text" class="form-control" id="kondisikeluar" disabled/>
                            </div>
                          </div>

                        <div class="col-md-12 row" style="margin-top:10px;margin-bottom: 10px;">
                        <div class="col-xs-12 col-md-3">                  
                            <label>Status Pasien Pulang</label>
                            <input type="text" class="form-control" id="statuspasienpulang" disabled/>
                        </div>
                      </div>

                        </div>
                        </div>
                      </div>


                      <!-- ============================== -->

                      <!-- INPUT BHP -->
                      <div class="form-group">
                         <div class="col-sm-1"></div>
                        <div class="col-sm-4">
                           <span class="label label-default">Keterangan BHP/Obat</span>
                          <textarea disabled class="form-control" name="keteranganobat" rows="3"></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-1"></div>
                        <div class=" col-sm-10">
                          <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title">Pemakaian BHP/Obat</h3>

                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                            </div>
                            <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                         <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
                         <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
                         <table class="table table-striped table-hover table-bordered">
                          <thead>
                            <tr class="bg-yellow">
                              <th>No</th>
                              <th>NamaObat/BHP</th>
                              <th width="7%">DosisRacik</th>
                              <th>Kekuatan</th>
                              <th>Jumlah</th>
                              <th width="10%">JumlahAmbil</th>
                              <th>TotalHarga</th>
                              <th>Penggunaan</th>
                              <th>AturanPakai</th>
                              <th></th> 
                              <!-- <th>Beli</th> -->
                              <!-- <th>Jenis</th> -->
                            </tr>
                          </thead>
                          <tbody id="viewDataBhp">
                          </tbody>
                        </table>
                          </div>
                          <!-- /.box-body -->
                        </div>
                        </div>
                      </div>
                      <!-- END INPUT BHP -->
                      <center id="menuPendaftaranPoliklinik">
                        <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >Back</a>
                      </center>
                    </form>
                </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane" id="timeline">
                  <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  
                  <li id="riwayatAkhir">
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                  <input type="hidden" id="riwayattanggalpemeriksaan"/>
                </ul>
                
                <div style="text-align:center;">
                    <a class="btn btn-lg btn-success" id="loadMoreRiwayat"><i class="fa fa-search-plus"></i> Tampilkan riwayat sebelumnya</a>
                </div>
                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
