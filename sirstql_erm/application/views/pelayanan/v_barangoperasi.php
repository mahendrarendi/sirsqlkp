<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-4">
            <select class="form-control" id="pilihbarang" style="width:100%;"></select>
        </div>
        <div class="col-md-2">
            <a onclick="window.location.reload()" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Reload</a>
        </div>
        
        <div class="col-md-12" style="margin-top:6px;">
            <div class="box">
                <div class="box-body table-responsive no-padding">                
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr class="header-table-ql">
                                <th>No</th>
                                <th>Barang</th>
                                <th>Satuan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="body"></tbody>
                    </table>
                </div>
              <!-- /.box-body -->
            </div>
        </div>
        
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  

                    