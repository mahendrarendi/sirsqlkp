<style type="text/css">
    .col-sm-1, .col-sm-2{
        padding-right: 1px;margin:0px;
    }
    .tabriwayat{
        background-color: #d2d6de;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a{
        background-color: #f39c12;
        color: #444;
    }
    .riwayatparagraf{
        background-color: #ffffff;padding:6px;border-radius:2px;
    }
</style>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row col-md-12" style="padding-bottom:15px;">
                <div class="col-sm-3 row" id="ranap_profile">
                  <!-- Profile Image -->
                </div>

                <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile">
                <!-- detail profile -->
                </div>
                <div  class="col-md-4 ">
                    <div class="info-box bg-gold">
                        <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text"></span>
                          <span class="info-box-number">Waktu Pemeriksaan</span>

                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description" id="waktupemeriksaan"></span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                </div>
                <div class="col-md-1">
                    <a id="riwayatperawatan" class=" btn btn-info btn-lg" ><i class="fa fa-history"></i> Riwayat Perawatan</a><br><br>
                    <a id="btnasesmenulangrisikojatuh" class=" btn btn-success btn-lg" ><i class="fa fa-hospital-o"></i> Pengkajian Ulang Risiko Jatuh</a><br><br>
                    <a id="btnasesmenulangstatusfungsional" class=" btn btn-success btn-lg" ><i class="fa fa-hospital-o"></i> Pengkajian Ulang Status Fungsional</a><br><br>
                    <a id="btnasesmenulangnyeri" class=" btn btn-success btn-lg" ><i class="fa fa-hospital-o"></i> Pengkajian Ulang Nyeri</a>
                </div>
            </div>
          <!-- /.box -->
        </div>
          
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active tabriwayat"><a href="#activity" id="btnPemeriksaan" data-toggle="tab">Pemeriksaan</a></li>
              <li class="tabriwayat"><a href="#timeline" id="btnRiwayatPasien" data-toggle="tab">Riwayat Pasien</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">

              <div class="form-group">
                <div class="panel-group" id="awalmedriAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#awalmedriAccordion" data-target="#awalmedisri">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Pengkajian Awal Medis Rawat Inap</a>
                          </h4>
                        </div>
                        <div id="awalmedisri" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                              <form id="pengkajianawalmedisri">
                                <div class="col-md-12">
                                  <div class="panel-body">
                                    <table style="width: 60%;">
                                        <tbody>
                                          <tr>
                                              <td style="width: 20%;">
                                                <label>Tekanan Darah <span class="required"></span></label>
                                              </td>
                                              <td style="width: 25%;">
                                                <input type="text" class="form-control" name="tekanandarah">
                                              </td>
                                              <td style="width: 5%;">mmHg</td>
                                              <td style="width: 10%;">&nbsp;</td>
                                              <td style="width: 20%;"><label>Laju Pernafasan <span class="required"></span></label></td>
                                              <td style="width: 25%;"><input type="text" class="form-control" name="lajunapas"></td>
                                              <td style="width: 5%;">x/menit</td>
                                          <tr>
                                          <tr>
                                              <td>
                                                <label>Nadi <span class="required"></span></label>
                                              </td>
                                              <td>
                                                <input class="form-control" type="text" name="nadi">
                                              </td>
                                              <td style="width: 5%;">x/menit</td>
                                              <td style="width: 10%;">&nbsp;</td>
                                              <td><label>SpO<small>2</small><span class="required"></span></label></td>
                                              <td><input class="form-control" type="text"name="spo2"></td>
                                              <td style="width: 5%;">%</td>
                                          <tr>
                                          <tr>
                                              <td>
                                                <label>Suhu <span class="required"></span></label>
                                              </td>
                                              <td>
                                                <input class="form-control" type="text" name="suhu">
                                              </td>
                                              <td style="width: 5%;">C</td>
                                          <tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                          </tr>
                                        </tbody>
                                    </table>
                                    <div class="col-md-12"  >
                                        <div class="form-group">
                                          <label>Subjective<span class="required">*</span></label>
                                          <textarea class="form-control txt_subjective" rows="4" name="subjective" style="width: 1153px; height: 90px;"></textarea>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-md-12"  >
                                        <div class="form-group">
                                          <label>Objective<span class="required">*</span></label>
                                          <textarea class="form-control txt_objective" rows="4" name="objective" style="width: 1153px; height: 90px;"></textarea>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-md-12"  >
                                        <div class="form-group">
                                          <label>Assessment<span class="required">*</span></label>
                                          <textarea class="form-control txt_assessment" rows="4" name="assessment" style="width: 1153px; height: 90px;"></textarea>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="col-md-12"  >
                                        <div class="form-group">
                                          <label>Plan<span class="required">*</span></label>
                                          <textarea class="form-control txt_plan" rows="4" name="plan" style="width: 1153px; height: 90px;"></textarea>
                                        </div>
                                        <br>
                                    </div>
                                    <table sytle="width:60%;margin-top: 10px !important;">
                                      <tr>
                                        <td><label>Tanggal</label>&emsp;</td>
                                        <td><input type="date" class="form-control" name="tanggalawalmedisri"></td>
                                      </tr>
                                      <tr>
                                        <td><label>Jam</label>&emsp;</td>
                                        <td><input type="time" class="form-control" name="jamawalmedisri"></td>
                                      </tr>
                                      <tr>
                                        <td><label>DPJP</label>&emsp;</td>
                                        <td><input type="text" class="form-control" name="dpjpawalmedisri"></td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                                <div class="form-group" style="text-align:right;">
                                  <a class="btn btn-warning btn-sm" id="btnsimpanformpemeriksaanklinik" onclick="()" >SIMPAN</a>
                                  <a class="btn btn-success btn-sm" id="btnsimpanformpemeriksaanklinik" onclick="()" >SELESAI</a>
                                </div>
                              </form>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="awalkepriAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#awalkepriAccordion" data-target="#awalkeperawatanri">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Pengkajian Awal Keperawatan Rawat Inap</a>
                          </h4>
                        </div>
                        <div id="awalkeperawatanri" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="ulangAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#ulangAccordion" data-target="#ulangri">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Pengkajian Ulang</a>
                          </h4>
                        </div>
                        <div id="ulangri" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="pkhususAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#pkhususAccordion" data-target="#pkusus">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Pengkajian Populasi Khusus</a>
                          </h4>
                        </div>
                        <div id="pkusus" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="cpptAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#cpptAccordion" data-target="#cppt">
                            <h4 class="panel-title">
                                <a href="#" class="ing">CPPT</a>
                          </h4>
                        </div>
                        <div id="cppt" class="panel-collapse collapse" style="height: 0px;">
                        <div class="panel-body">
                                <div class="col-md-12">
                                  <a class="btn btn-primary btn-sm" id="addcppt"><i class="fa fa-plus-circle"></i> Tambah CPPT</a>
                                  <table id="tb-cppt" style="width: 90%;">
                                    <tbody></tbody>
                                  </table>
                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="monitoringAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#monitoringAccordion" data-target="#monitoring">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Monitoring Pasien</a>
                          </h4>
                        </div>
                        <div id="monitoring" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="rpelaksanaantinAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#rpelaksanaantinAccordion" data-target="#rpelaksanaantin">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Rencana Pelaksanaan Tindakan</a>
                          </h4>
                        </div>
                        <div id="rpelaksanaantin" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="edukasiAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#edukasiAccordion" data-target="#edukasi">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Edukasi</a>
                          </h4>
                        </div>
                        <div id="edukasi" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="lpersalinanAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#lpersalinanAccordion" data-target="#lpersalinan">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Laporan Persalinan</a>
                          </h4>
                        </div>
                        <div id="lpersalinan" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="panel-group" id="operasiAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#operasiAccordion" data-target="#loperasi">
                            <h4 class="panel-title">
                                <a href="#" class="ing">Laporan Operasi</a>
                          </h4>
                        </div>
                        <div id="loperasi" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
              </div>
                    <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
                        <input type="hidden" name="idinap">
                        <input type="hidden" name="idrencanamedispemeriksaan">
                        <input type="hidden" name="idpendaftaran">
                        <input type="hidden" name="idunit">
                        <input type="hidden" name="norm">
                        <input type="hidden" id="modepemeriksaan" name="modepemeriksaan">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Dokter Masuk</label>
                            <div class="col-sm-4"><textarea name="dokter" rows="6" class="form-control" readonly></textarea></div>

                            <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Anamnesa Masuk</label>
                            <div class="col-sm-4"><textarea  class="form-control textarea" name="anamnesa" placeholder="Anamnesa Dokter Saat Masuk" rows="6" readonly></textarea></div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Catatan Rawat Inap</label>
                            <div class="col-sm-4"><textarea class="form-control textarea" name="catatan" placeholder="Catatan Rawat Inap" rows="6"></textarea></div>

                            <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Keterangan Masuk</label>
                            <div class="col-sm-4"><textarea class="form-control textarea" name="keterangan" placeholder="Keterangan Pemeriksaan Saat Masuk" rows="6" readonly></textarea></div>
                       </div>

                        <div class="form-group"><label for="" class="col-sm-2 control-label">&nbsp;</label></div>

                        <!-- SOAP RANAP -->
                        <div class="form-group">
                          <label for="" class="col-sm-2 control-label" >SOAP</label>
                          <div class=" col-sm-10">
                              <a class="btn btn-primary btn-sm" id="inputsoap"><i class="fa fa-plus-circle"></i> Input SOAP</a>
                              <table class="table table-bordered table-bordered table-striped" style="width: 98%">
                                <thead>
                                  <tr class="bg-yellow">
                                    <th width="100px">waktu input</th>
                                    <th>Petugas</th>
                                    <th>SIP</th>
                                    <th>Profesi</th>
                                    <th>SOA</th>
                                    <th>P</th>
                                    <th></th>
                                  </tr>
                                </thead>
                                <tbody id="listsoap">
                                </tbody>
                              </table>
                          </div>
                        </div>

                        <div class="form-group"><label for="" class="col-sm-2 control-label">&nbsp;</label></div>

                        <!-- VitalSign ===================== -->
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >VitalSign</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="carivitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'')">
                                  <option value="0">Pilih</option>
                                </select>
                            </div>

                            <label for="" class="col-sm-2 control-label" >Paket VitalSign</label>
                            <div class="col-sm-3">
                                <select class="select2 form-control" name="paketvitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'ispaket')">
                                  <option value="0">Pilih</option>
                                  <?php 
                                  if(!empty($data_paketvitalsign))
                                  {
                                    foreach ($data_paketvitalsign as $data) {
                                      echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                                    }
                                  }
                                  ?>
                                </select>
                            </div>

                        </div>

                        <!-- LIST HASIL VitalSign -->
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class=" col-sm-10">
                            <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan hasil</span>
                            <table class="table" style="width: 98%">
                              <thead>
                                <tr class="bg-yellow">
                                  <th>Parameter</th>
                                  <th>Hasil</th>
                                  <th>NDefault</th>
                                  <th>NRujukan</th>
                                  <th>Satuan</th>
                                  <th>Status</th>
                                  <th width="12%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody id="listVitalsign">
                              </tbody>
                            </table>
                            </div>
                        </div>

                        <!-- DIAGNOSA ===================== -->
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Diagnosa</label>
                            <div class="col-sm-3">
                              <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="inputdelete_diagnosa(this.value,'input')">
                                <option value="0">Pilih</option>
                              </select>
                            </div>
                        </div>

                        <!-- LIST HASIL TINDAKAN -->
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class=" col-sm-10">
                            <table class="table table-striped table-hover" style="width: 98%">
                              <thead>
                                <tr class="bg-yellow">
                                  <th>ICD</th>
                                  <th>Nama ICD</th>
                                  <th>Alias ICD</th>
                                  <th width="12%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody id="listDiagnosa">
                              </tbody>
                            </table>
                            </div>
                        </div>
                      <!-- ============================== -->

                      <!-- TINDAKAN ===================== -->
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Tindakan</label>
                            <div class="col-sm-4">
                              <select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)">
                                <option value="0">Pilih</option>
                              </select>
                            </div>
                        </div>

                      <!-- LIST HASIL TINDAKAN -->
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class=" col-sm-10">
                            <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan jumlah</span>
                            <table class="table" style="width: 98%">
                                <thead>
                                    <tr class="bg-yellow">
                                      <th>ICD</th>
                                      <th>Nama ICD | Nama Obat</th>
                                      <th>Dokter (Opsional)</th>
                                      <th>Jumlah/Pemakaian</th>
                                      <th>Biaya</th>
                                      <th>Status</th>
                                      <th width="12%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="listTindakan">
                                </tbody>
                            </table>
                            </div>
                        </div>
                      <!-- ============================== -->

                      <!-- RADIOLOGI ===================== -->
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Hasil Radiografi</label>
                            <div class="col-sm-10">          
                              <table class="table" style="width: 92%; background-color: #eeeeee;" >
                                    <tbody>
                                      <tr>
                                        <td colspan="2" id="viewHasilRadiografi">
                                          <!--tampil dari js-->
                                        </td>
                                      </tr>
                                        <?php if($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI)){ ?>
                                        <tr>
                                            <td colspan="2" style="padding-top:15px;padding-left:20px;">
                                                <a id="btnRadiologiEfilm" class="btn btn-md btn-primary">Unggah hasil <i class="fa fa-upload"></i></a>
                                            </td>
                                        </tr>
                                      <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Hasil Expertise</label>

                            <div class="col-sm-9">
                              <span class="label label-default">*Input Hasil Expertise</span>
                              <a class="btn btn-xs btn-info" id="cetakexpertise"> <i class="fa fa-print"></i> Cetak Hasil Radiologi</a>
                              <textarea id="keteranganradiologi" class="form-control textarea" name="keteranganradiologi" rows="14"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Saran </label>
                            <div class="col-sm-9">
                              <span class="label label-default">*Input Saran</span>
                              <textarea id="saranradiologi" class="form-control textarea" name="saranradiologi" rows="7"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >&nbsp; </label>
                            <div class="col-sm-9">
                                <a class="btn btn-primary" id="simpanHasilExpertiseRanap"><i class="fa fa-save"></i> Simpan Hasil Expertise</a>
                                <br>
                                <small class="text text-info">#klik menu simpan hasil expertise, jika akan mencetak langsung hasil expertise.</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Elektromedik</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                                  <option value="0">Pilih</option>
                                </select>
                            </div>
                        </div>

                      <!-- LIST HASIL RADIOLOGI -->
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class=" col-sm-10">
                            <table class="table" style="width: 98%">
                              <thead>
                                <tr class="bg-yellow">
                                  <th>ICD</th>
                                  <th>Nama ICD</th>
                                  <th>Biaya</th>
                                  <th>Status</th>
                                  <th width="12%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody id="listRadiologi">
                              </tbody>
                            </table>
                            </div>
                        </div>
                      <!-- ============================== -->

                      <!-- LABORATORIUM ===================== -->
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Keterangan</label>
                            <div class="col-sm-4">
                                <span class="label label-default">*Input keterangan laboratorium</span>
                                <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Laboratorium</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)">
                                  <option value="0">Pilih</option>
                                </select>
                            </div>

                            <label for="" class="col-sm-2 control-label" >Paket Laboratorium</label>
                            <div class="col-sm-3">
                                <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
                                  <option value="0">Pilih</option>
                                  <?php 
                                  if(!empty($data_paket))
                                  {
                                    foreach ($data_paket as $data) {
                                      echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                                    }
                                  }
                                  ?>
                                </select>
                            </div>
                        </div>

                      <!-- LIST HASIL LABORATORIUM -->
                      <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class=" col-sm-10">
                        <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan hasil</span>
                        <a id="waktupengerjaanlabranap" mode="nonpaket" class="btn btn-info btn-xs" ><i class="fa fa-clock-o"></i> Waktu Pengerjaan Non Paket</a> 
                        <a id="cetakhasillaboratnonpaket" class="btn btn-info btn-xs" ><i class="fa fa-print"></i> Cetak Hasil Non Paket</a> 
                        <table class="table" style="width: 98%">
                          <thead>
                            <tr class="bg-yellow">
                              <th>Parameter</th>
                              <th>Hasil</th>
                              <th>NDefault</th>
                              <th>NRujukan</th>
                              <th>Satuan</th>
                              <th>Biaya</th>
                              <th>Status</th>
                              <th width="16%">Aksi</th>
                            </tr>
                          </thead>
                          <tbody id="listLaboratorium">
                          </tbody>
                        </table>
                        </div>
                      </div>
                    <!-- END HASIL LABORATORIUM -->

                    <!-- INPUT BHP -->
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label" >Keterangan</label>
                            <div class="col-sm-4">
                                <span class="label label-default">#Input keterangan obat</span>
                                <textarea class="form-control" name="keteranganobat" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label" >Rencana BHP</label>
                            <div class="col-sm-4">
                                <select class="select2 form-control" name="caribhp" style="width:100%;" onchange="rencanaInputBhp(this.value)">
                                    <option value="0">Pilih</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class=" col-sm-10">
                            <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan pemakaian</span>
                            <table class="table table-striped table-hover" style="width: 98%">
                              <thead>
                                <tr class="bg-yellow">
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Pemakaian</th>
                                  <th>Satuan</th>
                                  <th>@Harga</th>
                                  <th>Subtotal</th>
                                  <th>Status</th>
                                  <th width="12%"></th>
                                </tr>
                              </thead>
                              <tbody id="viewDataBhp">
                              </tbody>
                            </table>
                            </div>
                        </div>
                      <!-- END INPUT BHP -->

                        <center id="menuPendaftaranPoliklinik">
                            <input type="submit" class="btn btn-warning btn-lg" name="simpan" value="Simpan Data">
                            <input type="submit" class="btn btn-warning btn-lg" name="simpan" value="Simpan Data dan Set Terlaksana">
                            <a class="btn btn-warning btn-lg" href="javascript:void(0)" onclick="history.back()" >Batal</a>
                        </center>
                    </form>
                    <hr>
                </div>
                <div class="tab-pane" id="timeline">
                 <!-- The timeline -->
                    <ul class="timeline timeline-inverse">

                      <li id="riwayatAkhir">
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
                      <input type="hidden" id="riwayattanggalpemeriksaan"/>
                    </ul>

                    <div style="text-align:center;">
                        <a class="btn btn-lg btn-success" id="loadMoreRiwayat"><i class="fa fa-search-plus"></i> Tampilkan riwayat sebelumnya</a>
                    </div>

                </div>
             <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->