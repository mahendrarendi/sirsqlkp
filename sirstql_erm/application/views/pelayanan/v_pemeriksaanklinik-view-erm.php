<style type="text/css">
        .tabriwayat{background-color: #d2d6de;}
        .nav-tabs-custom>.nav-tabs>li.active>a {
            background-color: #f39c12;
            color: #444;
        }
        .form-pelayanan
        {
            display: block;
            width: 100%;
            height: 22px;
            padding: 2px 4px;
             font-size: 12.8px; 
            line-height: 1.42857143;
            color: #222e32;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 2px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
.cHidden{background: transparent;border: none; height: 0px;margin:0px;padding: 0px;display: inherit;}
.riwayatparagraf{background-color: #ffffff;padding:6px;border-radius:2px;}

.header-title {
  text-align: center;
  background: #566D7E;
}
.header-title h3 {
  font-size: 15px;
  font-weight: bold;
  color: white;
  padding: 10px;
}


.tab-asesmen-medis a.active {
   background: #566D7E !important;
  border-color: #566D7E !important;
/* 519500 || #966F33*/
}
.asesmenawalkeperawatan .panel-heading,.asesmenawalmedis .panel-heading,.edukasiform .panel-heading{
  background: #519500;
  color: white;
}
.asesmenawalkeperawatan .panel,.asesmenawalmedis .panel {
  margin-top: 20px;
}

.optiongroup-siki label {
  font-weight: normal;
}
.optiongroup-siki h3 {
  font-size: 12px;
  font-weight: bold;
}

body .select2.select2-container{
   width:100% !important;
}

body span.required{
   color:red !important;
}
</style>

<div class="box-body" style="background-color:#fff;">
    <div class="box-header">
        <!-- <a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a> -->
        <a onclick="pilih_riwayat_periksa()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
        <a onclick="cetak_hasillab()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><i class="fa fa-flask"></i> Laboratorium</a>
        <a onclick="cetak_hasilelektromedik()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><img src="<?= base_url('assets/images/icons/radiasi.svg')?>"> Elektromedik</a> 
    </div>
    <div class="row col-md-12">
        <div class="col-sm-3" id="pemeriksaanklinik_profile"></div>
        <!-- /.col -->
        <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile"></div>               
    </div>
</div>
<br>
<a data-toggle="tooltip" data-original-title="VitalSign" id="periksaVitalsign" onclick="setfocusVitalsign()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-male fa-lg"></i></a>
<a data-toggle="tooltip" data-original-title="Radiologi" id="periksaElektromedik" onclick="setfocusElektromedik()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/images/icons/radiasi.svg')?>" width="20"></a>
<a data-toggle="tooltip" data-original-title="Echocardiography" id="periksaEchocardiography" onclick="setfocusEchocardiography()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-heartbeat fa"></i></a>
<a data-toggle="tooltip" data-original-title="Laboratorium" id="periksaLaboratorium" onclick="setfocuslaboratorium()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-flask"></i></a>
<a data-toggle="tooltip" data-original-title="Diagnosa" id="periksaDiagnosa" onclick="setfocusdiagnosa()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/icon/diagnoses-white.svg')?>" width="20"></a>
<a data-toggle="tooltip" data-original-title="Tindakan" id="periksaTindakan" onclick="setfocustindakan()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-medkit"></i></a>
<a data-toggle="tooltip" data-original-title="Bhp/Obat" id="periksaBhp" onclick="setfocusobatbhp()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-toggle-on"></i></a>
<section class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active tabriwayat"><a href="#activity" id="btnPemeriksaan" data-toggle="tab">Pemeriksaan</a></li>
              <li class="tabriwayat"><a href="#timeline" id="btnRiwayatPasien" data-toggle="tab">Riwayat Pasien</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <div class="row col-sm-12" style="margin-top: 15px;">
                        <div class="box box-solid box-default">
                          <div class="box-header with-border">
                            <h1 class="box-title">Riwayat Pemeriksaan</h1>
                          </div>

                          <div class="row">
                            <div id="r_hasilPeriksa" class="col-sm-9" style="margin:none;padding-top:5px;max-height:500px;overflow-y: scroll;">
                            </div>

                            <div class="col-sm-3">
                              <div class="form-group col-sm-10">
                                <a class="btn bt-md btn-primary" onclick="pemeriksaanklinik_gettglriwayat()">Riwayat</a>
                              </div>
                              <label class="control-label col-sm-12">Tanggal Periksa</label>
                              <div id="r_tanggalPeriksa" class="btn-group-vertical" style="margin:none;max-height:250px;overflow-y: scroll; padding-right:50px;padding-left:15px;">

                              </div>
                            </div>
                          </div>

              </div>
            </div>

            
            <!-- <form action="base_url('cpelayanan/pemeriksaanklinik_save_periksa');" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik"> -->
            <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa_trial'); ?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
              
               <input type="hidden" name="modeperiksa" id="modeperiksa"/>

              <input type="hidden" name="idperiksa" id="idperiksa" />
              <input type="hidden" name="idpendaftaran" />
              <input type="hidden" name="idunit" />
              <input type="hidden" name="idloket" id="idloket" />
              <input type="hidden" name="idperson" />
              <input type="hidden" name="carabayar" id="carabayar" />
              <input type="hidden" name="status" />
              <input type="hidden" name="norm" />
              <input type="hidden" id="validasidataobyektif" />
              <input type="hidden" id="validasidatasubyektif" />
              <input type="hidden" id="validasiicd10" />
              <input type="hidden" name="modesave" value="<?= $mode; ?>" />

              <!-- Andri - Rekon -->
              <input type="hidden" name="NoSEP" id="NoSEP" />
              <input type="hidden" name="NoJKN" id="NoJKN" />
              <input type="hidden" name="KdDPJP" id="KdDPJP" />
              <input type="hidden" name="KdPoli" id="KdPoli" />
              <input type="hidden" name="KdNamaPoli" id="KdNamaPoli" />
              <input type="hidden" name="SIPDokterTxt" id="SIPDokterTxt" />
              <input type="hidden" name="txtDiagnosa" id="txtDiagnosa" />
              <input type="hidden" name="txtDiagnosaD PJPManual" id="txtDiagnosaDPJPManual" />
              <input type="hidden" name="tglTerbit_back" id="tglTerbit_back" />
              <input type="hidden" name="tanggal_cetak_back" id="tanggal_cetak_back" />
              <input type="hidden" name="tglCetak_QR_back" id="tglCetak_QR_back" />
              <input type="hidden" name="tglSEP_back" id="tglSEP_back" />
              <input type="hidden" name="txtNoRujukan" id="txtNoRujukan" />
              <input type="hidden" name="txtTanggalrujukan" id="txtTanggalrujukan" />


              <input type="hidden" name="noSuratKontrol" id="noSuratKontrol" />
              <input type="hidden" name="tglRencanaKontrol" id="tglRencanaKontrol" />
              <input type="hidden" name="namaDokter" id="namaDokter" />
              <input type="hidden" name="noKartuJKN" id="noKartuJKN" />
              <input type="hidden" name="namaPasien" id="namaPasien" />
              <input type="hidden" name="kelamin" id="kelamin" />
              <input type="hidden" name="tglLahir" id="tglLahir" />

              <input type="hidden" id="waktumulai" name="waktumulai" />
              <input type="hidden" id="waktulayanan" name="waktulayanan" />
              <input type="hidden" id="waktusekarang" name="waktusekarang" />

                     <!-- <input type="hidden" name="idperiksa" />
                     <input type="hidden" name="idpendaftaran" />
                     <input type="hidden" name="idunit" />
                     <input type="hidden" name="idloket" id="idloket" />
                     <input type="hidden" name="idperson" />
                     <input type="hidden" name="carabayar" />
                     <input type="hidden" name="status" />
                     <input type="hidden" name="norm" />
                     <input type="hidden" id="validasidataobyektif" />
                     <input type="hidden" id="validasidatasubyektif" />
                     <input type="hidden" id="validasiicd10" />
                     <input type="hidden" name="modesave" value="<?= $mode; ?>" />
                     <input type="hidden" name="iddokterdpjp">
                     <input type="hidden" name="sessidperson" value="<?= $_SESSION['id_person_login']; ?>" />
                     <input type="hidden" id="waktumulai" name="waktumulai" />
                     <input type="hidden" id="waktulayanan" name="waktulayanan" />
                     <input type="hidden" id="waktusekarang" name="waktusekarang" /> -->

<!-- TRIASE METODE ESI -->

                     <div class="form-group itemugd itemralanugd">
                        <label for="" class="col-sm-2 label-daftar control-label">Cara pasien datang </label>
                           <table class="itemugd itemralanugd">
                           <tbody>
                              <tr>
                                 <td style="width : 15%;"><input type="checkbox" name="chckcarapasiendatang" class="chckcarapasiendatang" id="chckcarapasiendatang0" value="0">&nbsp;Kemauan sendiri <br><br><br><br><br></td>
                                 <td style="width : 25%;">
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td colspan="3">
                                                   <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang1" value="1">&nbsp;Diantar (oleh orang lain/bukan kerabat)
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>Nama</td>
                                                <td>:&nbsp;</td>
                                                <td><input type="text" name="chckcarapasiendatangdiantarnama"></td>
                                             </tr>
                                             <tr>
                                                <td>Alamat</td>
                                                <td>:&nbsp;</td>
                                                <td><input type="text" name="chckcarapasiendatangdiantaralamat"></td>
                                             </tr>
                                             <tr>
                                                <td>No. Telp</td>
                                                <td>:&nbsp;</td>
                                                <td><input type="text" name="chckcarapasiendatangdiantarnotelp"></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                 </td>
                                 <td>
                                       <table>
                                          <tbody>
                                             <tr>
                                                <td><input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang2" value="2">&nbsp;Diantar polisi</td>
                                             </tr>
                                            <tr>
                                                <td>
                                                <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang3" value="3">&nbsp;Rujukan
                                                </td>
                                             </tr>
                                             <tr>
                                                <td>Dari</td>
                                                <td>:&nbsp;</td>
                                                <td><input type="text" name="chckcarapasiendatangrujukandari"></td>
                                             </tr>
                                             <tr>
                                                <td>Alasan</td>
                                                <td>:&nbsp;</td>
                                                <td><input type="text" name="chckcarapasiendatangrujukanalasan"></td>
                                             </tr>
                                          </tbody>
                                       </table>
                                 </td>
                              </tr>
                           </tbody>
                           </table>
                     </div>
                     <div class="form-group itemugd itemralanugd">
                        <label for="" class="col-sm-2 label-daftar control-label">Kategori kasus</label>
                           <table style="width : 45%;">
                              <tbody>
                                 <tr>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus0" value="0">&nbsp;Bedah</td>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus1" value="1">&nbsp;Non bedah</td>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus2" value="2">&nbsp;Kebidanan</td>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus3" value="3">&nbsp;Psikiatri</td>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus4" value="4">&nbsp;Anak</td>
                                    <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus5" value="5">&nbsp;KLL</td>
                                 </tr>
                              </tbody>
                           </table> 
                     </div>
                     <div class="form-group itemugd itemralanugd">
                        <label for="" class="col-sm-2 label-daftar control-label">&nbsp;</label>
                           <table style="width : 65%">
                              <thead>
                                 <tr>
                                    <th colspan="3" class="text-center"><strong>TRIASE METODE ESI <i>(Emergency Severity Index)</i></strong></th>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <th style="border: 1px solid black;" class="text-center">Level</th>
                                    <th style="border: 1px solid black;" class="text-center">Keterangan</th>
                                    <th style="border: 1px solid black;" class="text-center"><i>Response Time</i></th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme1" class="chhcktme" name="chhcktme" value="1">&nbsp;Level 1: <i>Resuscitation</i></td>
                                    <td style="border: 1px solid black;">&nbsp;Pasien sekarat, membutuhkan intervensi yang menyelamatkan nyawa</td>
                                    <td style="border: 1px solid black;">&nbsp;0 menit</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme2" class="chhcktme" name="chhcktme" value="2">&nbsp;Level 2: <i>Emergency</i></td>
                                    <td style="border: 1px solid black;">&nbsp;Pasien berisiko tinggi dan/atau dengan penurunan kesadaran akut dan/atau nyeri berat dan/atau gangguan psikis berat</td>
                                    <td style="border: 1px solid black;">&nbsp;15 menit</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme3" class="chhcktme" name="chhcktme" value="3">&nbsp;Level 3:&nbsp;<i>Urgent</i></td>
                                    <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan dua atau lebih sumber daya</td>
                                    <td style="border: 1px solid black;">&nbsp;30 menit</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme4" class="chhcktme" name="chhcktme" value="4">&nbsp;Level 4:&nbsp;<i>Non urgent</i> </td>
                                    <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan satu sumber daya</td>
                                    <td style="border: 1px solid black;">&nbsp;60 menit</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme5" class="chhcktme" name="chhcktme" value="5">&nbsp;Level 5:&nbsp;<i>False emergency</i> </td>
                                    <td style="border: 1px solid black;">&nbsp;Pasien tidak membutuhkan sumber daya</td>
                                    <td style="border: 1px solid black;">&nbsp;120 menit</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme0" class="chhcktme" name="chhcktme" value="0">&nbsp;<i>Death on arrival</i> </td>
                                    <td style="border: 1px solid black;">&nbsp;Kasus meninggal</td>
                                    <td style="border: 1px solid black;">&nbsp;Tidak perlu</td>
                                 </tr>
                              </tbody>
                           </table>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter DPJP</label>
                        <div class="col-sm-4"><input type="text" name="dokter" value="" class="form-control" readonly="readonly" /></div>
                        <div class="col-sm-2" id="viewMenuUbahDokter"></div>
                     </div>

                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter Pemeriksa</label>
                        <div class="col-sm-4">
                           <select class="form-control" id="iddokterpemeriksa" name="iddokterpemeriksa">
                           </select>
                        </div>
                        <div class="col-sm-2">
                           Risiko Tinggi: <br>
                           <input type="checkbox" class="chckrisikotinggi" value="1" name="risikotinggi1" id="risikotinggi1" readonly>&nbsp;Ya <br>
                           <input type="checkbox" class="chckrisikotinggi" value="0" name="risikotinggi0" id="risikotinggi0" readonly>&nbsp;Tidak
                        </div>
                        <div class="col-sm-2">
                           Tipe Kasus: <br>
                           <input type="radio" class="cektipekasus" value="1" name="tipekasus" >&nbsp;Baru <br>
                           <input type="radio" class="cektipekasus" value="0" name="tipekasus" >&nbsp;Lama
                        </div>
                     </div>

                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Loket Poliklinik </label>
                        <div class="col-sm-4"><input type="text" name="namaloket" value="" class="form-control" readonly="readonly" /></div>
                        <div class="col-sm-2" id="viewMenuUbahLoket"></div>
                     </div>

                     <!-- VitalSign ===================== -->
                     <div class="form-group">
                        <input class="cHidden" id="focusVitalsign" type="text">
                        <label for="" class="col-sm-2 control-label">VitalSign</label>
                        <div class="col-sm-4">
                           <select class="select2 form-control" name="carivitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'')">
                              <option value="0">Pilih</option>
                           </select>
                        </div>

                        <label for="" class="col-sm-2 control-label">Paket VitalSign</label>
                        <div class="col-sm-3">
                           <select class="select2 form-control" name="paketvitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'ispaket')">
                              <option value="0">Pilih</option>
                              <?php
                              if (!empty($data_paketvitalsign)) {
                                 foreach ($data_paketvitalsign as $data) {
                                    echo "<option value='" . $data->idpaketpemeriksaan . "'>" . $data->namapaketpemeriksaan . "</option>";
                                 }
                              }
                              ?>
                           </select>
                        </div>

                     </div>

                     <!-- LIST HASIL VitalSign -->
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class=" col-sm-10">
                           <span class="label label-default">*Tekan tab pada inputan nilai untuk menyimpan perubahan</span>
                           <table class="table" style="width: 95%">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th>Parameter</th>
                                    <th>Hasil</th>
                                    <th>NDefault</th>
                                    <th>NRujukan</th>
                                    <th>Satuan</th>
                                    <th width="8%">Aksi</th>
                                 </tr>
                              </thead>
                              <tbody id="listVitalsign">
                              </tbody>
                           </table>
                        </div>
                     </div>

                     <!-- Kategori Skrining -->
                     <div class="form-group">
                        <label for="" class="col-sm-2 label-daftar control-label">Skrining</label>
                        <div class="col-sm-9" id="skrining" style="padding-top:5px;"></div>
                     </div>

                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <label for="" class="col-md-4">
                           <input type="checkbox" name="cindikasipasien" id="cindikasipasien" onclick="setFormIndikasiPasienWabah(this.value)" /> Pasien Terindikasi Wabah (Pasien Covid-19)
                        </label>
                     </div>

                     <div id="formIndikasiPasienWabah" class="form-group hide" style="margin-left:8px;">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="row col-md-8">
                           <div class="box box-warning col-xs-6">
                              <div class="box-header with-border">
                                 <h3 class="box-title"> Formulir Pemeriksaan Indikasi Pasien Wabah, (Pasien Covid-19).</h3>

                                 <div class="box-body">
                                    <div class="form-group">
                                       <label for="">Pilih Wabah</label>
                                       <select class="form-control" id="jeniswabah" name="jeniswabah" style="width:100%;"></select>
                                    </div>

                                    <div class="form-group">
                                       <label>Status Rawat</label>
                                       <select class="form-control" id="statusrawatwabah" onchange="change_statusrawatIPW(this.value)" name="statusrawatwabah" style="width:100%;"></select>
                                    </div>

                                    <div class="form-group">
                                       <label>Penanganan</label>
                                       <select class="form-control" id="penanganan" name="penanganan" style="width:100%;"></select>
                                    </div>

                                    <div class="form-group">
                                       <label>Sebab Penularan</label>
                                       <select class="form-control" id="sebabpenularan" name="sebabpenularan" style="width:100%;"></select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <!-- Pengkajian Risiko Jatuh -->

                     <div class="form-group" style="background-color:#DADBDD;">
                     <label for="" class="col-sm-2 label-daftar control-label">Pengkajian Risiko Jatuh</label>
                           <table>
                              <tbody>
                                 <tr>
                                    <td colspan="2" style="width : 50%;">Cara berjalan pasien: tidak seimbang / jalan dengan menggunakan alat bantu</td>
                                    <td style="width : 1px;">:</td>
                                    <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprjatuh1" class="chckprjatuh" id="chckprjatuhcvptsjdmabya" value="1" name="chckprjatuh">&nbsp;Ya <input type="hidden" name="jmlhscorecbp" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" class="chckprjatuh" id="chckprjatuh0" id="chckprjatuhcvptsjdmabyatidak" value="0" name="chckprjatuh">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Menopang saat akan duduk (tampak memegang pinggiran kursi/benda lain)</td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckmsad1" class="chckmsad" id="chckmsadya" value="1" name="chckmsad">&nbsp;Ya <input type="hidden" name="txtjumlahskorpengkajianresikojatuh"><input type="hidden" name="jmlhscoremsad" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" id="chckmsad0" class="chckmsad" id="chckmsadyatidak" value="0" name="chckmsad">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td><br> Interprestasi hasil :</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh0" id="chckporjatuhtidakberisiko" value="0" onclick="return false;">&nbsp;Tidak berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh1" id="chckporjatuhrisikorendah" value="1" onclick="return false;">&nbsp;Resiko rendah</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh2" id="chckporjatuhrisikotinggi" value="2" onclick="return false;">&nbsp;Risiko tinggi</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 2 jawaban "Ya"</td>
                                 </tr>
                              </tbody>
                           </table>
                     </div>


                     <!-- <div id="pemeriksaanInputAnamnesa">
                        <!-- ditampilkan di js -->
                     <!-- </div> -->

                     <div class="kungungankurang30hari">
                        <div id="pemeriksaanInputAnamnesa">
                           <!-- ditampilkan di js -->
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Data Obyektif</label>
                           <div class="col-sm-9">
                              <textarea class="form-control textarea" id="dataobyektif" name="keterangan" placeholder="Keterangan Pemeriksaan" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTDATAOBYEKTIFRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Rencana/Catatan</label>
                           <div class="col-sm-9">
                              <textarea class="form-control textarea" name="rekomendasi" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRENCANACATATANRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Alergi</label>
                           <div class="col-sm-9" id="">
                              <!-- listAlergi -->
                              <textarea class="form-control textarea" name="alergiobat" placeholder="" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTALERGIRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <!-- Kategori Skrining -->
                        <div class="form-group">
                           <label for="" class="col-sm-2 label-daftar control-label">Skrining</label>
                           <div class="col-sm-9" id="skrining" style="padding-top:5px;"></div>
                        </div>
                     </div>
                     <br><br>

                     <div class="text-center" id="question_visit">
                        <label class="control-label" style="font-size : 130%">Apakah jarak antara kunjungan sebelumnya > 30 hari?</label>
                        <table class="table table-borderless" style="margin-left : auto; margin-right : auto; width : 20%;">
                           <tbody>
                              <tr>
                                 <td colspan="2" class="text-center">
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-center"><button type="button" class="btn btn-primary" style="width : 80%;" onclick="setAssementTab()">Ya</button></td>
                                 <td class="text-center"><button type="button" class="btn btn-primary btnTidakJarakKunjungan" style="width : 59%;" onclick="hideAssementTab()">Tidak</button></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>

                     <div class="text-center tab-asesmen-medis">
                        <a class="btn btn-md btn-warning" style="width : 16%;" id="asesmenawalmedis" href="javascript:void(0);">Pengkajian Awal Medis</a>
                        <a class="btn btn-md btn-warning" style="width : 18%;" id="navasesmenawalkeperawatan" href="javascript:void(0);">Pengkajian Awal Keperawatan</a>
                        <a class="btn btn-md btn-warning" style="width : 18%;" id="navedukasi" href="javascript:void(0);">Edukasi Interdisiplin</a>
                        <a class="btn btn-md btn-danger" style="display:none;" id="navbatalpilihnav" href="javascript:void(0);"><i class="fa fa-refresh"></i></a>
                     </div>

                     <div class="pad margin" id="viewHasilLaboratorium"></div>

                     <!-- Awal asesmen Awal Medis -->
                     <div class="asesmenawalmedis">
                        <div class="header-title bg-warning">
                           <h3>PENGKAJIAN AWAL MEDIS</h3>
                        </div>
                        <input type="hidden" name="idpendaftaran" />
                        <input type="hidden" name="idpemeriksaan" />
                        <input type="hidden" name="idunit" value="idunit"></input>
                        <table class="table table-bordered" style="width:100%">
                           <tbody>
                              <tr class="itemralanugd">
                                 <td colspan="3">
                                    <div class="col-md-10">
                                       <table style="width : 100%;">
                                          <tbody>
                                             <tr>
                                                <td style="width : 15%;"><label>Tanggal Pasien Periksa: </label></td>
                                                <td style="width : 30%;"><label id="tglpasienperiksa"></label></td>
                                                <td style="width : 15%;"><label for="">Jam Pasien Periksa: </label></td>
                                                <td style="width : 30%;"><label id="jampasienperiksa">: &nbsp;WIB</label></td>
                                             </tr>
                                             <tr>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </td>
                              </tr>
                              <!-- <tr class="itemugd itemralanugd">
                                 <td>
                                    <div class="col-md-12">
                                       <div class="panel panel-default">
                                          <div class="panel-heading">
                                             <h4>CARA PASIEN DATANG</h4>
                                          </div>
                                          <div class="panel-body">
                                             <table style="width : 100%;">
                                                <tbody>
                                                   <tr>
                                                      <td><label for=""><strong>Cara pasien datang :</strong></label></td>
                                                   </tr>
                                                   <tr>
                                                      <td style="width : 15%;"><input type="checkbox" name="chckcarapasiendatang" class="chckcarapasiendatang" id="chckcarapasiendatang0" value="0">&nbsp;Kemauan sendiri <br><br><br><br><br></td>
                                                      <td style="width : 25%;">
                                                         <table>
                                                            <tbody>
                                                               <tr>
                                                                  <td colspan="3">
                                                                     <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang1" value="1">&nbsp;Diantar (oleh orang lain/bukan kerabat)
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td>
                                                                     Nama
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantarnama">
                                                                  </td>
                                                               <tr>
                                                                  <td>
                                                                     Alamat
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantaralamat">
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td>
                                                                     No. Telp
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantarnotelp">
                                                                  </td>
                                                                                                                  </tr>
                                                </tbody>
                                             </table>
                                 </td>
                                 <td>
                                    <table>
                                       <tbody>
                                          <tr>
                                             <td><input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang2" value="2">&nbsp;Diantar polisi</td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang3" value="3">&nbsp;Rujukan
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>Dari</td>
                                             <td>:&nbsp;</td>
                                             <td><input type="text" name="chckcarapasiendatangrujukandari"></td>
                                          </tr>
                                          <tr>
                                             <td>Alasan</td>
                                             <td>:&nbsp;</td>
                                             <td><input type="text" name="chckcarapasiendatangrujukanalasan"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                        <table style="width : 80%;">
                           <tbody>
                              <tr>
                                 <td colspan="6"><strong>Kategori kasus</strong></td>
                              </tr>
                              <tr>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus0" value="0">&nbsp;Bedah</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus1" value="1">&nbsp;Non bedah</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus2" value="2">&nbsp;Kebidanan</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus3" value="3">&nbsp;Psikiatri</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus4" value="4">&nbsp;Anak</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus5" value="5">&nbsp;KLL</td>
                              </tr>
                           </tbody>
                        </table> <br>
                        <table style="width : 88%">
                           <thead>
                              <tr>
                                 <th colspan="3" class="text-center"><strong>TRIASE METODE ESI <i>(Emergency Severity Index)</i></strong></th>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <th style="border: 1px solid black;" class="text-center">Level</th>
                                 <th style="border: 1px solid black;" class="text-center">Keterangan</th>
                                 <th style="border: 1px solid black;" class="text-center"><i>Response Time</i></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme1" class="chhcktme" name="chhcktme" value="1">&nbsp;Level 1: <i>Resuscitation</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien sekarat, membutuhkan intervensi yang menyelamatkan nyawa</td>
                                 <td style="border: 1px solid black;">&nbsp;0 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme2" class="chhcktme" name="chhcktme" value="2">&nbsp;Level 2: <i>Emergency</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien berisiko tinggi dan/atau dengan penurunan kesadaran akut dan/atau nyeri berat dan/atau gangguan psikis berat</td>
                                 <td style="border: 1px solid black;">&nbsp;15 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme3" class="chhcktme" name="chhcktme" value="3">&nbsp;Level 3:&nbsp;<i>Urgent</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan dua atau lebih sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;30 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme4" class="chhcktme" name="chhcktme" value="4">&nbsp;Level 4:&nbsp;<i>Non urgent</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan satu sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;60 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme5" class="chhcktme" name="chhcktme" value="5">&nbsp;Level 5:&nbsp;<i>False emergency</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien tidak membutuhkan sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;120 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme0" class="chhcktme" name="chhcktme" value="0">&nbsp;<i>Death on arrival</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Kasus meninggal</td>
                                 <td style="border: 1px solid black;">&nbsp;Tidak perlu</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
               </div>
            </div>
            </td>
            </tr> -->
            <tr class="itemralanugd">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>SUBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <div class="col-md-12">
                              <div class="form-group">
                                 <label>&nbsp;Riwayat Penyakit Sekarang <span class="required">*</span> </label><br>
                                 <textarea class="form-control txt_asesmen" rows="4" name="riwayatpenyakitsekarang" style="width: 1153px; height: 102px;"></textarea>
                              </div>
</div>
                           <div class="col-md-12" style="background-color:#FFE4C4;">
                              <br>
                              <div class="form-group">
                                 <label>Riwayat Penyakit Dahulu <span class="required">*</span></label><br>
                                 <input type="checkbox" class="chckboxrpd" name="chckboxrpd" id="tidakriwayapenyakitdahulu" value="0">
                                 &nbsp;&nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckboxrpd yardp" name="chckboxrpd" id="yariwayatpenaykitdahulu" value="1">
                                 &nbsp;&nbsp;Ada,sebutkan :<br>
                                 <textarea class="form-control txt_asesmen txtrdp" rows="4" name="txtriwayatpenyakitdahulu" style="width: 1153px; height: 102px;"></textarea>
                              </div>
                                                            <div class="form-group">
                                 <label>Obat yang Rutin Dikonsumsi <span class="required">*</span></label><br>
                                 <input type="checkbox" class="chckobyrd" id="chcktidakobyrd" name="chckobyrd" value="0">
                                 &nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckobyrd" id="chckyaobyrd" name="chckobyrd" value="1">
                                 &nbsp;Ada, sebutkan :
                                 <textarea name="txtobyrd" class="form-control txt_asesmen" rows="4" style="width: 1153px; height: 102px;"></textarea>
                              </div>
                                                            <div class="form-group">
                                 <label>Riwayat Alergi Obat dan Makanan <span class="required">*</span></label><br>
                                 <input type="checkbox" class="form-check-input chckraodm" id="chcktidakraodm" name="chckraodm" value="0">
                                 &nbsp;Tidak <br>
                                 <input type="checkbox" class="form-check-input chckraodm" id="chckyaraodm" name="chckraodm" value="1">
                                 &nbsp;Ya, sebutkan:
                                 <textarea class="form-control txt_asesmen" rows="4" name="txtraodm" style="width: 1153px; height: 102px;"></textarea>
                              </div>
                                                         </div>
                        </div>
                        <!-- <div class="panel-footer">Panel Footer</div> -->
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemralanugd">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>OBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <table>
                              <tbody>
                                 <tr>
                                    <td style="width : 14%">
                                       <label>Keadaan Umum <span class="required">*</span></label>
                                    </td>
                                    <td>
                                       : <input type="text" style="width: 39%;" name="keadaanumum">
                                    </td>
                                    <td><label>Skala Nyeri <span class="required">*</span></label></td>
                                    <td>: <input type="text" style="width: 30%;" name="skalanyeri"></td>
                                 <tr>
                                 <tr>
                                  <td>&nbsp;</td>
                                 </tr>
                                 <!-- <tr>
                                    <td>
                                       <label>GCS</label>
                                    </td>
                                    <td> :
                                       <label>E</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcse">
                                       <label>V</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsv">
                                       <label>M</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsm">
                                    </td>
                                 <tr> -->
                              </tbody>
                           </table>
                           <div class="col-md-12"  >
                              <div class="form-group">
                                 <label>Pemeriksaan Fisik <span class="required">*</span></label>
                                 <textarea class="form-control txt_asesmen" rows="4" name="pemeriksaanfisik" style="width: 1153px; height: 102px;"></textarea>
<label>Upload Pemeriksaan Fisik </label><br>
                                 <span id="viewpemeriksaanfisik">
                                             <!--tampil dari js-->
                                 </span>

                              </div>
                              <br>
                           </div>
                           <!-- <div class="col-md-8">
                              <div class="daftargambarpemeriksaanfisik"></div>
                           </div> -->
                           <div class="col-md-12" style="background-color:#FFE4C4;">
                              <div class="form-group">
                                 <label for="">Pemeriksaan Penunjang Medik (Hasil yang abnormal) <span class="required">*</span></label><br>
                                 <input type="checkbox" class="chckppm" id="chcktidakppm" name="chckppm" value="0">&nbsp;&nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckppm" id="chckyakppm" name="chckppm" value="1">&nbsp;&nbsp;Ada, sebutkan :
                                 <textarea class="form-control txt_asesmen" rows="4" name="txtppm" style="width: 1153px; height: 102px;"></textarea>
                              </div>
                              <br>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ELEKTROMEDIK</h4>
                        </div>
                        <div class="panel-body">
                           <input class="cHidden" id="focusElektromedik" type="text">
                           <div class="form-group">
                              <!--jika mode periksa bisa menambahkan radiologi-->
                              <?php if ($mode == 'periksa') { ?>
                                 <div class="col-md-4">
                                    <select class="select2 col-md-3" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                                       <option value="0">Pilih Tindakan Radiologi</option>
                                    </select>
                                 </div>
<div class="col-md-6" style="text-align:right">
                                 <label>Upload Pemeriksaan Penunjang </label><br>
                                 <span id="viewpemeriksaanpenunjang">
                                             <!--tampil dari js-->
                                 </span>
                                 </div>
                              <?php } ?>
                              <div class="col-md-11">
                                 <table class="table" style="width: 97%">
                                    <thead>
                                       <tr class="header-table-ql">
                                          <th>ICD</th>
                                          <th style="width: 25%;"> Nama ICD </th>
                                          <th style="width: 40%;"> Dokter Penerima JM </th>
                                          <th width="5%">Jumlah</th>
                                          <th>Biaya</th>
                                          <th>Potongan</th>
                                          <th width="19%">Aksi</th>
                                       </tr>
                                    </thead>
                                    <tbody id="listRadiologi">
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-7">
                                 <label>Hasil Expertise</label>
                                 <textarea class="form-control textarea" name="keteranganradiologi" rows="25" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
                                 <label>Saran</label>
                                 <textarea class="form-control textarea" name="saranradiologi" rows="12" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
                                 <br>
                                 <div class="col-md-8">
                                    <div class="uploadgambarelektromedik"></div>
                                 </div>
                              </div>
                              <div class="col-sm-5">
                                 <label>Hasil Radiografi</label>
                                 <table class="table" style="width: 79%; background-color: #eeeeee;">
                                    <tbody>
                                       <tr>
                                          <td colspan="2" id="viewHasilRadiografi">
                                             <!--tampil dari js-->
                                          </td>
                                       </tr>
                                     
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ECHOCARDIOGRAPHY</h4>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                              <div class="col-md-11">
                                 <table class="table table-bordered table-hover table-striped">
                                    <thead>
                                       <tr class="header-table-ql">
                                          <th>Measurement</th>
                                          <th>Result</th>
                                          <th>Normal (Adult)</th>
                                       </tr>
                                    </thead>
                                    <tbody id="listEkokardiografi">
                                    </tbody>
                                 </table>
                              </div>
                              <div class="col-sm-7">
                                 <label>Keterangan</label>
                                 <textarea class="form-control textarea" id="keteranganekokardiografi" name="keteranganekokardiografi" rows="18" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI)) ? '' : 'disabled') ?>></textarea>
                              </div>
<div class="col-sm-5">
                                 <label>Hasil Expertise Echocardiography</label>
                                 <table class="table" style="width: 79%; background-color: #eeeeee;">
                                    <tbody>
                                       <tr>
                                          <td colspan="2" id="viewpemeriksaanechocardiography">
                                             <!--tampil dari js-->
                                          </td>
                                       </tr>
                                       
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>LABORATORIUM</h4>
                        </div>
                        <div class="panel-body">
                           <div class="pad margin" id="viewHasilLaboratorium"></div>

                           <div class="col-sm-7">
                              <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTKETERANGANLABORATORIUM)) ?: disabled) ?>></textarea>
                           </div>

                           <div class="col-sm-4 row">
                              <!--jika mode periksa dapat menambahkan laboratorium-->
                              <?php if ($mode == "periksa") { ?>
                                 <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)">
                                    <option value="0">Pilih Tindakan Laboratorium</option>
                                 </select>
                                 <br>
                                 <br>
                                 <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
                                    <option value="0">Pilih Paket Laboratorium</option>
                                    <?php
                                    if (!empty($data_paket)) {
                                       foreach ($data_paket as $data) {
                                          echo "<option value=" . $data->idpaketpemeriksaan . ">" . $data->namapaketpemeriksaan . "</option>";
                                       }
                                    }
                                    ?>
                                 </select>
<?php } ?><br>
                              <label>Hasil Laboratorium</label>
                              <table class="table" style="width: 100%; background-color: #eeeeee;">
                                 <tbody>
                                    <tr>
                                       <td colspan="2" id="viewpemeriksaanlaboratorium">
                                             <!--tampil dari js-->
                                       </td>
                                    </tr>
                                   
</tbody>
                              </table>
                           </div>
                           <input class="cHidden" id="focusLaboratorium" type="text">
                           <table class="table" style="width: 95%">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th>Parameter</th>
                                    <th>Hasil</th>
                                    <th>NDefault</th>
                                    <th>NRujukan</th>
                                    <th>Satuan</th>
                                    <th>Biaya</th>
                                    <th>Potongan</th>
                                    <th width="12%">Aksi</th>
                                 </tr>
                              </thead>
                              <tbody id="listLaboratorium">
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ASSESSMENT (ICD 10)</h4>
                        </div>
                        <div class="panel-body">
                           <input class="cHidden" id="focusDiagnosa" type="text">
                           <div class="col-md-4" style="display : none;">
                              <label>Diagnosa Primer</label>
                              <select class="ql-select2 select2 form-control" id="diagnosaprimer" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,primer)">
                                 <option value="0">Input Diagnosa</option>
                              </select>
                           </div>
                           <div class="col-md-4" style="display : none;">
                              <label>Diagnosa Sekunder</label>
                              <select class="ql-select2 select2 form-control" id="diagnosasekunder" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,sekunder)">
                                 <option value="0">Input Diagnosa</option>
                              </select>
                           </div>
                           <div class="col-md-8" style="margin-top:8px;">
                              <label>Diagnosa DPJP <span class="required">*</span> </label>
                              <textarea class="form-control" name="diagnosa" id="diagnosadilauricd10" rows="2" style="width: 1153px; height: 102px;"></textarea>
                           </div>
                           <div class="col-md-12" style="margin-top:4px; display:none;">
                              <table class="table">
                                 <thead>
                                    <tr class="header-table-ql">
                                       <th>ICD</th>
                                       <th>Nama ICD</th>
                                       <th>Level</th>
                                       <th>Alias</th>
                                       <th width="8%">Aksi</th>
                                    </tr>
                                 </thead>
                                 <tbody class="listDiagnosa10"></tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari" style="display:none">
               <td><!-- FORM diskon -->
               <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>DISKON</h4>
                        </div>
                        <div class="box box-default">
                            <div class="box-header ui-sortable-handle" style="cursor: move;">
                                <h3 class="box-title"><b>DISKON</b></h3>
                            </div>
                            
                            <div class="box-body">
                              <?php if($mode=='periksa'){ ?>
                              <div class="col-sm-4 row">
                                  <select class="select2 form-control" name="caridiskon" style="width:100%;" onchange="pilihdiskon(this.value)">
                                        <option value="0">Pilih</option>
                                  </select>
                              </div>
                              <?php } ?>

                                <table class="table" style="width: 95%">
                                    <thead>
                                        <tr class="header-table-ql">
                                            <th>ICD</th>
                                            <th>Nama ICD</th>
                                            <th width="10%">Diskonn</th>
                                            <th width="10%">Subtotal</th>
                                            <th width="8%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listDiskon">
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                      <!-- END FORM Diskon -->
                     </div>
               </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
               <div class="col-md-12">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4>PLAN</h4>
                     </div>
                  </div>
                  <div class="panel-body">
                     <!-- FORM BHP ATAU OBAT -->
                     <div class="box box-default asesmenawalmedis">
<!-- FORM TINDAKAN -->
                           <!-- <div class="box box-default"> -->
                              <input class="cHidden" id="focusTindakan" type="text">
                              <div class="col-sm-4" style="cursor: move;">
                                 <h4 class="box-title"><b>TINDAKAN</b></h4>
                                 <?php if($mode=='periksa'){ ?>
                                       <select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)">
                                          <option value="0">Pilih</option>
                                       </select>
                                 <?php } ?>
                              </div><br>
                              <!-- /.box-header -->
                              <div class="box-body">
                                 
                                 <table class="table" style="width: 95%">
                                       <thead>
                                          <tr class="header-table-ql">
                                             <th>ICD</th>
                                             <th>Nama ICD</th>
                                             <th width="17%">Dokter Penerima JM</th>
                                             <th width="5%">Jumlah</th>
                                             <th>Biaya</th>
                                             <th>Potongan</th>
                                             <th width="8%">Aksi</th>
                                          </tr>
                                       </thead>
                                       <tbody id="listTindakan" class="listTindakan">
                                       </tbody>
                                 </table>
                              </div>
                           <!-- </div> -->
                      <!-- END FORM TINDAKAN -->
                      <br>
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                           <h3 class="box-title"><b>OBAT/BHP</b></h3>
                        </div>
                        <div class="box-body">
                           <div class="col-sm-5">
                              <span style="margin-right:20px;"><input id="adaracikan" name="adaracikan" type="checkbox"> Ada Racikan</span><br>
                              <span class="label label-default">Keterangan Obat/Bhp</span>
                              <textarea class="form-control" id="keteranganobat" name="keteranganobat"  <?= (($mode=='verifikasi')?'':' onchange="pemeriksaanklinik_saveketobat(this.value)" ') ?> rows="9" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN)) ? '' : 'disabled' ) ?> ></textarea>
                           </div>
                           <div class="col-sm-5">
                              <?php if($mode=='periksa'){ ?>
                              <label>&nbsp;</label>
                              <?php } ?>
                                 <br>
                                 <br>
                                 <div class="pasien-prolanis <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS))? ''  : 'hide'  ); ?>">   
                                    <br>
                                    <input id="prolanis" name="prolanis" type="chasesmenawalmediseckbox" /> PROLANIS
                                    <br>
                                    <span class="label label-default">Tanggal Kunjungan Berikutnya</span>
                                       <div class="input-group date col-md-6">
                                          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                          <input type="text" readonly="readonly" placeholder="input tanggal..." class="form-control pull-right" id="kunjunganberikutnya" name="kunjunganberikutnya">
                                       </div>
                                 </div>
                           </div>
                           <div class="col-sm-12">
                              <div class="col-sm-4">
                                 <h4>Obat/BHP yang Dibawa Pulang Pasien</h4>
                                 <select class="select2 form-control" name="caribhp" style="width:30%;" onchange="inputBhpFarmasi(this.value,'resep')"></select><br>
                              </div>
                              <input class="cHidden" id="focusObat" type="text">
                              <?= table_bhpralanugd('viewBhpRalan'); ?>
                              <div class="col-md-4">
                                 <?php if($mode=='periksa'){ ?>
                                 <label>&nbsp;</label>
                                 <h4>Obat/BHP yang Sudah Terpakai di Unit</h4>
                                 <select class="select2 form-control" name="caribhp" style="width:30%;" onchange="inputBhpFarmasi(this.value,'unit')"></select><br>
                                 <?php } ?>
                              </div>
                              <br>
                              <input class="cHidden" id="focusObat" type="text">
                              <?= table_bhpralanunitugd('viewBhpRalanunit'); ?>
                           </div>
                           <br>
                           <div class="col-sm-12" style="background-color:#FFE4C4;">
                                 <label>Catatan/Rencana Lainnya</label><br>
                                 <input type="checkbox" class="form-check-input chckcl" id="chcktdkcatatanlainnya" name="chckcl" value="0">
                                 &nbsp;Tidak <br>
                                 <input type="checkbox" class="form-check-input chckcl" id="chckyacatatanlainnya" name="chckcl" value="1">
                                 &nbsp;Ya, sebutkan:<br>
                                 <textarea class="form-control" name="txtcatatanlainnya" style="width: 1153px; height: 102px;"></textarea><br>
                           </div>
                           <div class="col-sm-12 asesmenawalmedis">
                              <div class="asesmenawalmedis">
                                    <table>
                                       <tbody>
                                          <tr>
                                             <td>
                                                <table>
                                                      <tbody>
                                                      <tr>
                                                         <td><strong>Rencana Perawatan dan Tindakan Selanjutnya</strong></td>
                                                      </tr>
                                                      </tbody>
                                                   </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td style="width : 10%;">
                                                <div>
                                                   &nbsp;<input type="checkbox" class="chckrpdts" id="chckrpdts0" value="0" name="chckrencanaperawatandantindakanlainnya" >&nbsp;Rawat jalan
                                                   <table>
                                                      <tbody>
                                                         <tr>
                                                            <td style="width : 15%;"  class="col-md-12" ><input type="checkbox" class="chckrpdtskontrol" name="chckrawatjalanitem" id="chckrawatjalanitem0" value="0">&nbsp;Kontrol di : <br><br><br><br></td>
                                                            <td colspan="2">
                                                               <table>
                                                                  <tbody>
                                                                     <tr>
                                                                        <td><input type="checkbox" class="chckkontroldi" id="chckkontroldi0" name="chckkontroldi" value="0">&nbsp;UGD, tanggal&nbsp; <input type="date" name="tglugd"></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td><input type="checkbox" class="chckkontroldi" name="chckkontroldi" id="chckkontroldi1" value="1">&nbsp;Poliklinik&nbsp;<input type="text" id="txtkontroldi" name="txtkontroldi"></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td><input type="checkbox" class="chckkontroldi" name="chckkontroldi" id="chckkontroldi2" value="2">&nbsp;PPK 1</td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </td>
                                                         </tr>
                                                         <tr>
                                                            <td style="width : 25%;"  class="col-md-12" ><input type="checkbox" class="chckrpdtskontrol" name="chckrawatjalanitem" id="chckrawatjalanitem1" value="1">&nbsp;Kontrol bila ada keluhan
                                                            </td>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <div><input type="checkbox" class="chckrpdts" id="chckrpdts1" name="chckrencanaperawatandantindakanlainnya" value="1" >&nbsp;Observasi lanjutan di UGD</div>
                                             </td>
                                          </tr>
                                          <tr id="observasipasiendiugd" style="display : none;">
                                             <td>
                                                <div class="col-sm-1">
                                                </div>
                                                <div>
                                                   <div class="itemugd">
                                                      <div>
                                                         <label>Observasi Pasien di UGD</label><br>
                                                         <textarea name="txtobservasipasiendiugd" class="form-control"></textarea>
                                                      </div>
                                                   </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <div><input type="checkbox" class="chckrpdts" id="chckrpdts2" name="chckrencanaperawatandantindakanlainnya" value="2">&nbsp;Rawat inap, bangsal <input type="text" name="txtrawatinapbangsal"></div>
                                             </td>
                                          </tr>
                                          
                                          <tr id="advisebpjps" style="display : none;">
                                             <td>
                                                <div class="row">
                                                   <div class="col-md-12">
                                                   
                                                   <!-- START -->
                                                   <table>
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <table>
                                                            <tbody>
                                                               <tr>
                                                                  <td><strong>Rencana Perawatan</strong></td>
                                                               </tr>
                                                               <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp0" value="0">&nbsp;Preventif</td>
                                                               </tr>
                                                               <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp1" value="1">&nbsp;Paliatif</td>
                                                               </tr>
                                                               <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp2" value="2">&nbsp;Kuratif</td>
                                                               </tr>
                                                               <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp3" value="3">&nbsp;Rehabilitatif</td>
                                                               </tr>
                                                               <!-- <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp4" value="4">&nbsp;Rujuk <input type="text" name="txtrencanarawatrujuk"></td>
                                                               </tr> -->
                                                               <tr>
                                                                  <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp5" value="5">&nbsp;Menolak tindakan/rawat inap</td>
                                                               </tr>
                                                               <tr>
                                                                  <td>&nbsp;</td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                       
                                                   <!-- END -->

                                                   </div>
                                                   <div class="col-sm-1"></div>
                                                   <div class="col-sm-11">
                                                      <div class="row ml-5">
                                                         <div class="form-group">
                                                            <label>Advise DPJP</label>
                                                            <textarea name="advisedpjp" class="form-control"></textarea>
                                                         </div>
                                                         <table style="width : 100%;">
                                                            <tbody>
                                                               <tr>
                                                                  <td colspan="2" class="text-center"><strong>Verifikasi DPJP</strong></td>
                                                               </tr>
                                                               <tr>
                                                                  <td style=" width : 50%;">
                                                                     <div class="form-group" style="width : 95%;">
                                                                        <label for="">Dokter Penerima Advise</label>
                                                                        <input type="text" name="dokter" value="" class="form-control" readonly="readonly" />
                                                                        <!-- <select name="dokteryangmenerimaadvise" class="select2 ql-select2 form-control"></select> -->
                                                                     </div>
                                                                  </td>
                                                                  <td style=" width:50%;">
                                                                     <div class="form-group" style="width : 95%;">
                                                                        <label for="">DPJP</label>
                                                                        <!-- <input type="text" name="dokter" value="" class="form-control" readonly="readonly" /> -->
                                                                        <select name="verifikasidpjpdokterdpjd" class="select2 ql-select2 form-control"></select>
                                                                     </div>
                                                                  </td>
                                                               </tr>
                                                               <tr style="text-align:center;">
                                                                  <td colspan="2" style="padding-bottom:20px;">
                                                                     <input type="checkbox" class="raber" id="raber" name="raber" value="1">&nbsp;&nbsp;Rawat Gabung
                                                                  </td>
                                                               </tr>
                                                               <tr class="check-raber">
                                                                  <td colspan="2">
                                                                     <div class="form-group">
                                                                        <label>Advise DPJP Raber</label>
                                                                        <textarea name="advisedpjpraber" class="form-control"></textarea>
                                                                     </div>
                                                                  </td>
                                                               </tr>
                                                               <tr class="check-raber">
                                                                  <td colspan="2" class="text-center"><strong>Verifikasi Raber</strong></td>
                                                               </tr>
                                                               <tr class="check-raber">
                                                                  <td style=" width : 50%;">
                                                                     <div class="form-group" style="width : 95%;">
                                                                        <label for="">Dokter Penerima Advise Raber</label>
                                                                        <select name="dokteryangmenerimaadviseraber" class="select2 ql-select2 form-control"></select>
                                                                     </div>
                                                                  </td>
                                                                  <td style=" width:50%;">
                                                                     <div class="form-group" style="width : 95%;">
                                                                        <label for="">DPJP Raber</label>
                                                                        <!-- <input type="text" name="dokter" value="" class="form-control" readonly="readonly" /> -->
                                                                        <select name="verifikasidpjpdokterdpjdraber" class="select2 ql-select2 form-control"></select>
                                                                     </div>
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td colspan="3">
                                                                     <table width ="100%">
                                                                        <tr>
                                                                           <td>
                                                                              <input type="checkbox" class="chckpri" id="chckri0" name="chckrencanaperawatandantindakanlainnya_inap[]" value="0">&nbsp;&nbsp;Pengantar Rawat Inap
                                                                           </td>
                                                                           <td>
                                                                              <input type="checkbox" class="chckpri" id="chckri1" name="chckrencanaperawatandantindakanlainnya_inap[]" value="1">&nbsp;&nbsp;Rekonsiliasi Obat
                                                                           </td>
                                                                           <td>
                                                                              <input type="checkbox" class="chckpri" id="chckri2" name="chckrencanaperawatandantindakanlainnya_inap[]" value="2">&nbsp;&nbsp;Instruksi Pelaksanaan dan Pembeian Obat
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td>
                                                                              <input type="checkbox" class="chckpri" id="chckri3" name="chckrencanaperawatandantindakanlainnya_inap[]" value="3">&nbsp;&nbsp;Transfer Antar Ruangan
                                                                           </td>
                                                                           <td>
                                                                              <input type="checkbox" class="chckpri" id="chckri4" name="chckrencanaperawatandantindakanlainnya_inap[]" value="4">&nbsp;&nbsp;Early Warning Score
                                                                           </td>
                                                                        </tr>
                                                                     </table>
                                                                  
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                </div>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                             <!-- <td><input type="checkbox" class="chckrpdts" name="chckrencanaperawatandantindakanlainnya" id="chckrpdts4" value="4">&nbsp;Rujuk <input readonly type="text" name="txtrencanarawatrujuk"></td> -->
                                             <div><input type="checkbox" class="chckrpdts" id="chckrpdts5" name="chckrencanaperawatandantindakanlainnya" value="5">&nbsp;Rujuk <input readonly type="text" name="txtrencanarawatrujuk"></div>
                                             </td>
                                             <!-- <td><input type="checkbox" class="chckrp" name="chckrp" id="chckrp4" value="4">&nbsp;Rujuk <input type="text" name="txtrencanarawatrujuk"></td> -->
                                          </tr>
                                       </tbody>
                                    </table>
                              </div>

                           <div class="itemugd itemralanugd">
                              <table>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td><strong>Kondisi Pasien sebelum Meninggalkan UGD</strong></td>
                                                </tr>
                                                <tr>
                                                   <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu0" value="0">&nbsp;Stabil, kondisi membaik</td>
                                                </tr>
                                                <tr>
                                                   <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu1" value="1">&nbsp;Tidak ada perbaikan kondisi</td>
                                                </tr>
                                                <tr>
                                                   <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu2" value="2">&nbsp;Memburuk</td>
                                                </tr>
                                                <tr>
                                                   <td>&nbsp;</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                       <td>
                                          <br>
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td>
                                                      <input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu3" value="3">&nbsp;Meninggal dunia <br>
                                                      <table>
                                                         <tbody>
                                                            <tr>
                                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                               <td>Tanggal</td>
                                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                               <td><input type="date" name="txttanggalmeninggaldunia" id="txttanggalmeninggaldunia"></td>
                                                            </tr>
                                                            <tr>
                                                               <td>&nbsp;</td>
                                                            </tr>
                                                            <tr>
                                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                               <td>Jam</td>
                                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                               <td><input type="text" name="txtjammeninggaldunia"></td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
</div>
               </td>
            </tr> 
            <tr class="itemugd itemralanugd">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>PASIEN MENINGGALKAN UGD </h4>
                        </div>
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-4">
                                 <label>Tanggal Pasien Pulang</label><br>
                                 <input type="date" id="tglpasienpulangugd" name="tglpasienpulangugd" class="form-control">
                              </div>
                              <div class="col-md-4">
                                 <label>Jam Pasien Pulang</label><br>
                                 <input type="text" name="jampasienpulangugd" class="form-control" id="jampasienpulangugd">
                              </div>
                              <div class="col-md-4">
                                 <label for="idpersondokter">Nama Dokter</label><br>
                                 <!-- <select name="namadokterugd" id="" class="select2 ql-select2 form-control"> -->
                                 <select name="namadokterugd" id="" class="select2 ql-select2 form-control"></select>
                                 <input type="hidden" id="idnamadokterugd" name="idnamadokterugd" value="">
                                 <input type="hidden" id="namadokterhidden" name="namadokterhidden" value="">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemralan itemralanugd itemasesemenwal">
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-4">
                                 <label>Tanggal Selesai</label><input type="date" id="tglselesaiasesmen" name="tglselesaiasesmen">
                                 <input type="hidden" name="idpersondokter" id="idpersondokter" readonly>
                              </div>
                              <div class="col-md-4">
                                 <label>Jam Pasien<input type="text" name="txtselesaiasesmenawal" id="txtselesaiasesmenawal"> WIB</label>
                              </div>
                              <div class="col-md-4">
                                 <label>Nama Dokter :</label><input type="text" value="" id="namadokter" name="namadokter" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr> 
      </table>
  
   </div>
   <!-- Akhir Asesmen Awal Medis -->

   <!-- Asesmen Awal Keperawatan -->
   <div class="asesmenawalkeperawatan">
      <div class="header-title bg-warning">
         <h3>PENGKAJIAN AWAL KEPERAWATAN</h3>
      </div>
      <!-- <form id="formasesmenawalkeperawatan"> -->
      <input type="hidden" name="idperiksa" />
      <input type="hidden" name="idpendaftaran" />
      <table class="table table-bordered" style="width:100%">
         <tbody>
            <tr>
               <td colspan="2">
                  <div class="col-md-10">
                     <table style="width : 100%;">
                        <tbody>
                           <tr>
                              <td style="width : 15%;"><label>Tanggal Pasien Periksa : </label></td>
                              <td style="width : 30%;"><label id="tglperiksa"></label></td>
                              <td style="width : 15%;"><label for="">Jam Pasien Periksa : </label></td>
                              <td style="width : 30%;"><label id="waktupasienperiksa"></label></td>
                           </tr>
                           <tr>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>SUBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <div class="">
                              <label>Keluhan Utama</label>
                              <textarea class="form-control" style="width: 1153px; height: 102px;" name="txtkeluhanutama"></textarea>
                           </div><br>

                           <table>
                              <tbody>
                                 <tr>
                                    <td>Apakah pasien dengan keluhan tentang kebidanan dan kandungan?</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckapdktkdk" name="chckapdkt" value="0" id="chckapdktkdktidak">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckapdktkdk" name="chckapdkt" value="1" id="chckapdktkdktya">&nbsp;Ya</td>
                                 </tr>
                              </tbody>
                           </table>

                           <table class="asesmenobsyetridanginekologi">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><label>Asesmen Obstetri dan Ginekologi</label></td>
                                 </tr>
                                 <tr>
                                    <td width="20%">
                                       <label>Riwayat Kehamilan Sekarang</label>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : G <input type="text" style="width: 5%;" name="rksg"> P <input type="text" style="width: 5%;" name="rksp"> A <input type="text" style="width: 5%;" name="rksga"> Ah <input type="text" style="width: 5%;" name="rksah">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HPHT <input type="text" name="rksghpht"> HPL <input type="text" name="rksghpl"> UK <input type="text" name="rksguk">
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label>ANC Terpadu</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckancterpadu" id="chckanctidak" name="chckanc" value="0">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" id="chckanciya" name="chckanc" class="chckancterpadu" value="1">&nbsp;&nbsp;Ya,&nbsp; di <input type="text" name="txtanc">
                                       </div>
                                    </td>
                                 </tr>
                                 <!-- <tr> --disable nurhi rev 3
                                    <td>
                                       <label>Imunisasi TT</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckitt" id="chcktdkitt" value="0" name="chckimunisasitt">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckitt" id="chckyaitt" value="1" name="chckimunisasitt">&nbsp;&nbsp;Ya,&nbsp; TT ke <input type="text" name="txtitt">
                                       </div>
                                    </td>
                                 </tr> -->
                                 <tr>
                                    <td>
                                       <label>Pendarahan pervaginam</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckppvm" id="chcktdkppvm" name="chckppvm" value="0">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckppvm" id="chckyappvm" name="chckppvm" value="1">&nbsp;&nbsp;Ya,&nbsp;<input type="text" name="txtppvm">&nbsp;ml&nbsp;&nbsp;<strong>(Isikan Dengan Menggunakan Bilangan Bulat)</strong>
                                       </div </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>OBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <table>
                           <table class="table table-bordered itemugd">
                              <tbody>
                                 <tr>
                                    <td style="width:100px">
                                       <label><strong>Airway</strong></label>
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Bersih" id="chckairwayBersih">&nbsp;Bersih<br><input type="checkbox" class="chckairway" name="chckairway" id="chckairwayDarah" value="Darah">&nbsp;Darah
                                    </td>
                                    <td style="width:16%">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Sumbatan" id="chckairwaySumbatan">&nbsp;Sumbatan<br><input type="checkbox" class="chckairway" name="chckairway" value="Lidah jatuh" id="chckairwayLidahjatuh">&nbsp;Lidah jatuh
                                    </td>
                                    <td style="width:12%;" colspan="1">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Sputum" id="chckairwaySputum">&nbsp;Sputum<br><input type="checkbox" class="chckairway" name="chckairway" value="Benda asing" id="chckairwayBendaAsing">&nbsp;Benda asing
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Lendir" id="chckairwayLendir">&nbsp;Lendir<br><br>
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Ludah" id="chckairwayLudah">&nbsp;Ludah<br><br>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label><strong>Breathing</strong></label><br>Pernafasan
                                    </td>
                                    <td><br>
                                       <input type="checkbox" class="chckbre" name="chckbre" value="Tidak sesak" id="chckbretidaksesak">&nbsp;Tidak sesak
                                    </td>
                                    <td><br>
                                       <input type="checkbox" class="chckbre" name="chckbresesak" id="chckbresesak">&nbsp;Sesak:<br>
                                       &emsp;&emsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Saat Aktivitas" id="chckbresaataktivitas">&nbsp;Saat Aktivitas<br>
                                       &emsp;&emsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Tanpa Aktivitas" id="chckbretanpaaktivitas">&nbsp;Tanpa aktivitas
                                    </td>
                                    <td colspan="2"><br>
                                       <input type="checkbox" class="chckbre" name="chckbre" value="Tidak ada nafas" id="chckbretidakadanafas">&nbsp;Tidak ada nafas
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Suara nafas</p>
                                    </td>
                                    <td style="width:120px">
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Vesikuler" id="chcksuanavesikuler">&nbsp;Vesikuler
                                    </td>
                                    <td>
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Ronkhi" id="chcksuanaronkhi">&nbsp;Ronkhi
                                    </td>
                                    <td>
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Crakles" id="chcksuanacrakles">&nbsp;Crakles
                                    </td>
                                    <td>
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Wheezing" id="chcksuanawheezing">&nbsp;Wheezing
                                    </td>
                                    <td>
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Stridor" id="chcksuanastridor">&nbsp;Stridor
                                    </td>
                                    <td style="width : 25%;">
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Tidak ada suara nafas" id="chcksuanatidakadasuaranafas">&nbsp;Tidak ada suara nafas
                                    </td>
                                    <td></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">
                                       <strong>Circulation</strong>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pulsasi perifer</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulper" id="chckpulperteraba" value="1" name="chckpulper">&nbsp;Teraba
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulper" id="chckpulpertidakteraba" value="0" name="chckpulper">&nbsp;Tidak teraba
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pulsasi karotis</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulsasikarotis" id="chckpulsasikarotisteraba" name="chckpulsasikarotis" value="1">&nbsp;Teraba
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulsasikarotis" id="chckpulsasikarotistidakteraba" name="chckpulsasikarotis" value="0">&nbsp;Tidak teraba
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Warna kulit</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckwarnakulit" id="chckwarnakulitkemerahan" name="chckwarnakulit" value="1">&nbsp;Kemerahan
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckwarnakulit" id="chckwarnakulitpucat" name="chckwarnakulit" value="0">&nbsp;Pucat
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Akral</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckakral" id="chckakralhangat" name="chckakral" value="1">&nbsp;Hangat
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckakral" id="chckakraldingin" name="chckakral" value="0">&nbsp;Dingin
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>CRT</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckcrt" id="chckcrtkurang2dtk" name="chckcrt" value="1">&nbsp;<2 dtk </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckcrt" id="chckcrtlebih2dtk" name="chckcrt" value="0">&nbsp;>2 dtk
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <strong>Kesadaran</strong>
                                    </td>
                                    <td style="width:14%;">
                                       <input type="checkbox" class="chckkesadarans" name="chckkesadaran" id="chckkesadarancomposmentis" value="Compos mentis">&nbsp;Compos mentis
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadaras" name="chckkesadaran" id="chckkesadaranapatis" value="Apatis">&nbsp;Apatis
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadarans" name="chckkesadaran" id="chckkesadaransomnolen" value="Somnolen">&nbsp;Somnolen
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadarans" name="chckkesadaran" id="chckkesadaransupor" value="Supor">&nbsp;Supor
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadarans" name="chckkesadaran" id="chckkesadarancoma" value="Coma">&nbsp;Coma
                                    </td>
                                 </tr>
                              </tbody>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td colspan="3"><strong>Pengkajian Nyeri</strong></td>
                                    </tr>
                                    <tr>
                                       <td colspan="7" width="100%"><input type="checkbox" name="chckppnnips" class="chckppn">&nbsp;<i>NIPS (Neonatal Infant Pain Score)</i> (Digunakan pada pasien bayi usia 0-30 hari)</td>
                                    </tr>
                                    <tr>
                                       <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnflacc">&nbsp;<i>FLACC Behavioral Pain Scale</i> (Digunakan pada pasien bayi usia >30 hari sampai dengan usia <3 tahun, dan anak dengan gangguan kognitif)</td>
                                    </tr>
                                    <tr>
                                       <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnwongbaker">&nbsp;<i>Wong Baker Faces Pain Scale</i> (Digunakan pada pasien anak usia &#8805; 3 tahun dan dewasa)</td>
                                    </tr>
                                 </tbody>
                              </table>
                        </div>
                        <div id="tblnips" style="display:none;">
                           <table class="table table-bordered" style="border: 1px solid black;">
                              <thead style="border: 1px solid black;">
                                 <tr>
                                    <th class="text-center" style="border: 1px solid black;" colspan="3"><label><i>NIPS</i>&nbsp;&nbsp;<i>(Neonatal Infant Pain Score)</i></label>
                                 </tr>
                                 <tr>
                                    <th style="border: 1px solid black; width:300px;" class="text-center">Pengkajian</th>
                                    <th style="border: 1px solid black;" class="text-center">Parameter</th>
                                    <th style="border: 1px solid black;" class="text-center">Skor</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Ekspresi Wajah</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chckekw" class="chckekw" id="chckekw0" value="0">&nbsp;Otot wajah rileks, ekspresi netral</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckekw" name="chckekw" id="chckekw1" value="1">&nbsp;Otot wajah tegang, alis berkerut rahang, dan dagu mengunci</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="4" style="border: 1px solid black;" class="text-center">Tangisan</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan0" value="0">&nbsp;Tenang, tidak menangis</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan1" value="1">&nbsp;Mengerang lemah, intermitten</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan2" value="2">&nbsp;Menangis kencang, melengking dan terus-menerus</td>
                                    <td style="border: 1px solid black;" class="text-center">2</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;" colspan="2" class="text-center">(Catatan : menangis tanpa suara diberi skor bila bayi diintubasi)</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Pola pernafasan</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna0" value="0">&nbsp;Bernapas biasa</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna1" value="1">&nbsp;Pola nafas berubah: tarikan ireguler, lebih cepat dibanding biasa, menahan napas, tersedak</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Tangan</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan0" value="0">&nbsp;Rileks, tidak ada kekakuan otot, gerakan tungkai biasa</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan1" value="1">&nbsp;Tegang, kaku</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Kesadaran</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chcknipskesadaran" id="chckkesadaran0" value="0">&nbsp;Rileks, tenang, tidur lelap atau bangun</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chcknipskesadaran" id="chckkesadaran1" value="1">&nbsp;Sadar, atau gelisah</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;" class="text-center">
                                    <td style="border: 1px solid black;line-height: 12px;" colspan="2" class="text-center"><strong>Total Skor</strong></td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" name="totalskornips" value="0" style="width: 20%;" readonly>
                                       <input type="hidden" name="nipsekspresiwajah" class="nipsscore text-center" value="0">
                                       <input type="hidden" name="nipstangisan" class="nipsscore" class="nipsscore" value="0">
                                       <input type="hidden" name="nipspolapernafasan" class="nipsscore" value="0">
                                       <input type="hidden" name="nipstangan" class="nipsscore" value="0">
                                       <input type="hidden" name="nipskesadaran" class="nipsscore" value="0">
                                    </td>
                                 </tr>
                                 <tr style="line-height: 10px;">
                                    <td style="" colspan="4"><strong>Interpretasi hasil:</strong></td>
                                 </tr>
                                 <tr style="">
                                    <td style="" colspan="4">
                                       <input type="checkbox" id="chckbayi0" name="chckbayimengalaminyeri" value="0" onclick="return false;" />&nbsp;< 3 :Bayi tidak mengalami nyeri<br>
                                          <input type="checkbox" id="chckbayi1" name="chckbayimengalaminyeri" value="1" onclick="return false;" />&nbsp;> 3 :Bayi mengalami nyeri
                                    </td>
                                 </tr>
                           </table>
                        </div>

                        <div id="tbflacc" style="display:none;">
                           <table style="width : 100%;">
                              <thead>
                                 <tr style="border: 1px solid black">
                                    <th class="text-center" colspan="5"><label><i>FLACC Behavioral Pain Scale</i></label></th>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <th class="text-center" style="border : 1px solid black;width:100px;">Pengkajian</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 0</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 1</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 2</th>
                                    <th class="text-center" style="border : 1px solid black">Skor</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">
                                       <p>Wajah</p>
                                    </td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah0" value="0">&nbsp;Tersenyum / tidak ada ekspresi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah1" value="1">&nbsp;Terkadang merintih / menarik diri</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah2" value="2">&nbsp;Sering menggetarkan dagu dan menggatupkan rahang</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccwajah" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">
                                       <p>Kaki</p>
                                    </td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki0" value="0">&nbsp;Normal / relaksasi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki1" value="1">&nbsp;Tidak tenang / tegang</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki2" value="2">&nbsp;Kaki menendang/menarik diri</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflacckaki" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Aktivitas</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian0" value="0">&nbsp;Tidur posisi normal / relaksasi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian1" value="1">&nbsp;Gerakan menggeliat / bergeling kaku</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian2" value="2">&nbsp;Melengkungkan punggung / kaku / membentak</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccaktivitas" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Menangis</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis0" value="0">&nbsp;Tidak menangis</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis1" value="1">&nbsp;Mengerang / merengek</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis2" value="2">&nbsp;Menangis terus menerus / terisak / menjerit</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccmenangis" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Bersuara</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara0" value="0">&nbsp;Bersuara normal / tenang</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara1" value="1">&nbsp;Tenang bila dipeluk / digendong / diajak bicara</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara2" value="2">&nbsp;Sulit untuk menenangkan</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccbersuara" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center" style="border: 1px solid black" colspan="4"><label><strong>Total Skor</strong></label></td>
                                    <td class="text-center">
                                       <input type="text" name="totalskorflacc" value="0" style="width : 20%;" readonly>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width: 50%;">
                              <tbody>
                                 <tr>
                                    <td colspan="4">Interprestasi hasil</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" class="chckinteprestrasihasilflacc0" name="chckinteprestrasihasilflacc" id="chckflaccdibawahnol" value="0" onclick="return false;">&nbsp;0</td>
                                    <td>:&nbsp;Nyaman</td>
                                    <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaraempatsampaienam" value="2" class="chckinteprestrasihasilflacc2" onclick="return false;">&nbsp;4-6</td>
                                    <td>:&nbsp;Nyeri sedang</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantarasatusampaitiga" class="chckinteprestrasihasilflacc1" value="1" onclick="return false;">&nbsp;1-3</td>
                                    <td>:&nbsp;Nyeri Ringan</td>
                                    <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaratujuhsampaisepuluh" class="chckinteprestrasihasilflacc3" value="3" onclick="return false;">&nbsp;7-10</td>
                                    <td>:&nbsp;Nyeri berat</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                        <div id="tbwongbaker" style="display:none;">
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td class="text-center">
                                       <img src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_0.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_2.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_4.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_6.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_8.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_10.png">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker0" value="0"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker2" value="2"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker4" value="4"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker6" value="6"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker8" value="8"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker10" value="10"></td>
                                 </tr>
                              </tbody>
                           </table>

                           <table style="width: 50%;" id="tbinterprestasihasilwongbaker">
                              <tbody>
                                 <tr>
                                    <td colspan="4">Interprestasi hasil:</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" class="chckwongbakerinterpretasi0" id="chckwongbakernol" value="0" onclick="return false;">&nbsp;0</td>
                                    <td>:&nbsp;Nyaman</td>
                                    <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakerempatsampaienam" class="chckwongbakerinterpretasi2" value="2" onclick="return false;">&nbsp;4-6</td>
                                    <td>:&nbsp;Nyeri sedang</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakersatusampaitiga" class="chckwongbakerinterpretasi1" value="1" onclick="return false;">&nbsp;1-3</td>
                                    <td>:&nbsp;Nyeri Ringan</td>
                                    <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakertujuhsampaisepuluh" class="chckwongbakerinterpretasi3" value="3" onclick="return false;">&nbsp;7-10</td>
                                    <td>:&nbsp;Nyeri berat</td>
                                 </tr>
                              </tbody>
                           </table>
                           <br>
                           <table style="width: 100%;" class="trwongbakesfacespainscaleinterpretasi">
                              <tbody>
                                 <tr>
                                    <td>Faktor yang memperberat</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfym0" class="chckfym" name="chckfym" value="0">&nbsp;Cahaya</td>
                                    <td><input type="checkbox" id="chckfym1" class="chckfym" name="chckfym" value="1">&nbsp;Gerakan</td>
                                    <td><input type="checkbox" id="chckfym2" class="chckfym" name="chckfym" value="2">&nbsp;Suara</td>
                                    <td><input type="checkbox" id="chckfym3" class="chckfym" name="chckfym" value="3" colspan="2">&nbsp;Lainnya</td>
                                    <!-- <td><input type="checkbox" id="chckfym3" class="chckfym" name="chckfym" value="3" colspan="2">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyafaktoryangmemperberat" readonly></td> -->
                                 <tr>
                                 <tr>
                                    <td>Faktor yang meringankan</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfymer0" class="chckfymer" name="chckfymer" value="0">&nbsp;Istirahat</td>
                                    <td><input type="checkbox" id="chckfymer1" class="chckfymer" name="chckfymer" value="1">&nbsp;Dipijat</td>
                                    <td><input type="checkbox" id="chckfymer2" class="chckfymer" name="chckfymer" value="2">&nbsp;Minum Obat</td>
                                    <td><input type="checkbox" id="chckfymer3" class="chckfymer" name="chckfymer" value="3" colspan="2">&nbsp;Lainnya</td>
                                    <!-- <td><input type="checkbox" id="chckfymer3" class="chckfymer" name="chckfymer" value="3" colspan="2">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyafaktoryangmeringankan" readonly></td> -->
                                 <tr>
                                 <tr>
                                    <td>Kualitas nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckkualitasnyeri0" class="chckkualitasnyeri" name="chckkualitasnyeri" value="0">&nbsp;Ditusuk</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri1" class="chckkualitasnyeri" name="chckkualitasnyeri" value="1">&nbsp;Terikat</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri2" class="chckkualitasnyeri" name="chckkualitasnyeri" value="2">&nbsp;Tumpul</td>
                                    <td style="width: 192px;"><input type="checkbox" id="chckkualitasnyeri3" class="chckkualitasnyeri" name="chckkualitasnyeri" value="3">&nbsp;Berdenyut</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri4" class="chckkualitasnyeri" name="chckkualitasnyeri" value="4">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyakualitasnyeri"></td>
                                 <tr>
                                 <tr>
                                    <td>Lokasi nyeri</td>
                                    <td colspan="4">:&nbsp;<input type="text" name="txtlokasinyeri"></td>
                                 <tr>
                                 <tr>
                                    <td>Menjalar</td>
                                    <td>:&nbsp;<input type="checkbox" class="chckmenjalar" name="chckmenjalar" id="chckmenjalartidak" value="0">&nbsp;Tidak</td>
                                    <td colspan="3"><input type="checkbox" class="chckmenjalar" name="chckmenjalar" id="chckmenjalaryajelaskan" value="1">&nbsp;Ya, jelaskan&nbsp;<input type="text" name="txtjelaskanmenjalar"></td>
                                 <tr>
                                 <tr>
                                    <td>Skala nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckskalanyeri0" class="chckskalanyeri" name="chckskalanyeri" value="0">&nbsp;Ringan</td>
                                    <td><input type="checkbox" id="chckskalanyeri1" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="1">&nbsp;Sedang</td>
                                    <td><input type="checkbox" id="chckskalanyeri2" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="2">&nbsp;Berat</td>
                                    <td><input type="checkbox" id="chckskalanyeri3" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="3">&nbsp;Tidak nyeri</td>
                                 <tr>
                                 <tr>
                                    <td>Lamanya nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chcklamanyanyeridibawah30menit" name="chcklamanyanyeri" class="chcklamanyanyeri" value=0">&nbsp;< 30 menit</td>
                                    <td><input type="checkbox" colspan="2" id="chcklamanyanyeridiatas30menit" name="chcklamanyanyeri" class="chcklamanyanyeri" value="1">&nbsp;> 30 menit</td>
                                 <tr>
                                 <tr>
                                    <td>Frekuensi nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfrekuensinyeri0" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="0">&nbsp;Terus menerus</td>
                                    <td><input type="checkbox" id="chckfrekuensinyeri1" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="1">&nbsp;Hilang timbul</td>
                                    <td colspan="1"><input type="checkbox" id="chckfrekuensinyeri2" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="2">&nbsp;Jarang</td>
                                 <tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>PENGKAJIAN</h4>
                        </div>
                        <div class="panel-body">
                           <!-- <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Risiko Jatuh</strong></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2" style="width : 50%;">Cara berjalan pasien: tidak seimbang / jalan dengan menggunakan alat bantu</td>
                                    <td style="width : 1px;">:</td>
                                    <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprjatuhkpr1" class="chckprjatuhkpr" id="chckprjatuhkprcvptsjdmabya" value="1" name="chckprjatuhkpr" >&nbsp;Ya <input type="hidden" name="jmlhscorecbpkpr" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" class="chckprjatuhkpr" id="chckprjatuhkpr0" id="chckprjatuhkprcvptsjdmabyatidak" value="0" name="chckprjatuhkpr" onclick="return false">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Menopang saat akan duduk (tampak memegang pinggiran kursi/benda lain)</td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckmsadkpr1" class="chckmsadkpr" id="chckmsadkprya" value="1" name="chckmsadkpr" >&nbsp;Ya <input type="hidden" name="txtjumlahskorpengkajianresikojatuhkpr"><input type="hidden" name="jmlhscoremsadkpr" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" id="chckmsadkpr0" class="chckmsadkpr" id="chckmsadkpryatidak" value="0" name="chckmsadkpr" onclick="return false">&nbsp;Tidak</td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width : 50%;">
                              <tbody>
                                 <tr>
                                    <td><br> Interprestasi hasil :</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh0" id="chckporjatuhtidakberisikokpr" value="0" onclick="return false;">&nbsp;Tidak berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh1" id="chckporjatuhrisikorendahkpr" value="1" onclick="return false;">&nbsp;Resiko rendah</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh2" id="chckporjatuhrisikotinggikpr" value="2" onclick="return false;">&nbsp;Risiko tinggi</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 2 jawaban "Ya"</td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" /> -->
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Risiko Nutrisional</strong></td>
                                 </tr>
                                 <tr>
                                    <td style="width : 50%;">Apakah pasien mempunyai penurunan berat badan dalam 1 bulan?</td>
                                    <td style="width : 1px;">: &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnberatbadan1" class="chckprnberatbadan" name="chckprnberatbadan" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnberatbadan" value="0">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckprnberatbadan" id="chckprnberatbadan0" name="chckprnberatbadan" value="0">&nbsp;Tidak</td>
                                    <td style="width : 10%;"></td>
                                    <td style="width : 10%;"></td>
                                 </tr>
                                 <tr>
                                    <td style="width : 50%;">Apakah pasien mempunyai masalah penurunan nafsu makan dalam 1 bulan?</td>
                                    <td style="width : 1px;">: &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnnafsumakan1" class="chckprnnafsumakan" name="chckprnnafsumakan" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnnafsumakan" value="0">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckprnnafsumakan" id="chckprnnafsumakan0" name="chckprnnafsumakan" value="0">&nbsp;Tidak <input type="hidden" name="jumlahskorpengkajianrisikonutrisional"></td>
                                    <td style="width : 10%;"></td>
                                    <td style="width : 10%;"></td>
                                 </tr>
                              </tbody>
                           </table>
                           <br>
                           <table style="width : 50%;">
                              <tbody>
                                 <tr>
                                    <td>Interprestasi hasil :</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn0" value="0" onclick="return false;">&nbsp;Tidak berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn1" value="1" onclick="return false;">&nbsp;Berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Status Fungsional</strong></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Penggunaan alat bantu</p>
                                    </td>
                                    <td>:</td>
                                    <td style="width:10%;"><input type="checkbox" class="chckpealaban" id="chckpealaban0" name="chckpealaban" value="0">&nbsp;Tidak</td>
                                    <td><input type="checkbox" class="chckpealaban" id="chckpealaban1" name="chckpealaban" value="1">&nbsp;Ya, sebutkan : <input type="text" name="txtoestafu"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>ADL (<i>Activities of Daily Living</i>) </p>
                                    </td>
                                    <td>:</td>
                                    <td><input type="checkbox" name="chckadl" class="chckadl" id="chckadl1" value="1">&nbsp;Mandiri </td>
                                    <td><input type="checkbox" name="chckadl" class="chckadl" id="chckadl0" value="0">&nbsp;Dibantu </td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td><strong>Pengkajian Status Psikologi</strong></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi0" value="0">&nbsp;Tenang</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi1" value="1">&nbsp;Disorentasi</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi2" value="2">&nbsp;Sedih</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi3" value="3">&nbsp;Takut</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="8" id="chckpenstatuspsikologi8">&nbsp;Resiko melukai diri sendiri</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="5" id="chckpenstatuspsikologi5">&nbsp;Marah/tegang <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="6" id="chckpenstatuspsikologi6">&nbsp;Cemas <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="7" id="chckpenstatuspsikologi7">&nbsp;Gelisah <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="9" id="chckpenstatuspsikologi9">&nbsp;Resiko bunuh diri <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" id="chckpenstatuspsikologi4" name="chckpenstatuspsikologi" value="4"> &nbsp;Lainnya&nbsp;<input type="text" name="txtpengstatuspsikologi"></td>
                                    <td></td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%;">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Status Sosial, Ekonomi, Kultural, dan Spiritual</strong></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Status pernikahan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi0" value="0">&nbsp;Belum menikah</td>
                                    <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi1" value="1">&nbsp;Menikah</td>
                                    <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi2" value="2">&nbsp;Duda/janda</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Kendala komunikasi</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi0" name="chckkendalakomunikasi" value="0">&nbsp;Tidak ada</label /td>
                                    <td colspan="2"><input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi1" name="chckkendalakomunikasi" value="1">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtkendadakomunikasilainnya"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Yang merawat dirumah</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah0" name="chckyangmerawatdirumah" value="0">&nbsp;Tidak ada</td>
                                    <td colspan="2"><input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah1" name="chckyangmerawatdirumah" value="1">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtadasebutkanyangmerawatdirumah"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pekerjaan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpekerjaan" id="chckpekerjaan0" name="chckpekerjaanlainnya" value="0">&nbsp;Tidak bekerja</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan1" value="1">&nbsp;PNS/TNI/POLRI</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan2" value="2">&nbsp;Swasta</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan3" value="3">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyapekerjaan"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Jaminan kesehatan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan0" name="chckjaminankesehatan" value="0">&nbsp;Mandiri</td>
                                    <td><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan1" name="chckjaminankesehatan" value="1">&nbsp;BPJS</td>
                                    <td colspan="2"><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan2" name="chckjaminankesehatan" id="chckjaminankesehatanlainnya" value="2">&nbsp;Lainnya&nbsp;<input type="text" name="txtjaminankesehatanlainnya"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Apakah ada tindakan yang bertentangan dengan kepercayaan pasien?</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien0" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasien" value="0">&nbsp;Tidak</td>
                                    <td><input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien1" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasien" value="1">&nbsp;Ya</td>
                                 </tr>
                                 <tr style="display:none" class="jelaskan_tindakantersebut">
                                    <td>Jika ada jelaskan tindakan tersebut</td>
                                    <td>:</td>
                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<input type="text" name="txtjikaadajelaskantindakantersebut"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ASSESSMENT (SDKI)</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width:100%">
                              <tbody>
                                 <tr>
                                    <td style="width:33.3%;padding: 5px;">
                                       <label>Diagnosa SDKI <span class="required">*</span></label><br>
                                          <select class="ql-select2 select2 form-control" name="diagnosasdki">
                                       </select>
                                    </td>
                                    <td style="width:33.3%;padding: 5px;">
                                       <label>SLKI <span class="required">*</span></label><br>
                                          <select class="form-control slki-option" name="diagnosaslki">
                                             <option value="">Pilih SLKI</option>
                                          </select>
                                    </td>
                                    <td style="width:33.3%;padding: 5px;">
                                       <label>SIKI <span class="required">*</span></label><br>
                                          <select class="form-control siki-option" name="diagnosasiki">
                                             <option value="">Pilih SIKI</option>
                                          </select>
                                    </td>
                                 </tr>
                                 <!-- <tr>
                                    <td style="width : 100%;" colspan="2">
                                       <label>Diagnosa diluar SDKI</label><br>
                                       <input type="text" class="form-control" name="diagnosadiluarsdki"><br>
                                    </td>
                                 </tr> -->
                                 <!-- <tr>
                                    <td style="width : 100%;" colspan="2">
                                       <table style="width : 108%;">
                                          <thead>
                                             <tr class="header-table-ql">
                                                <th>Kategori</th>
                                                <th>Subkategori</th>
                                                <th>Kode Diagnosa</th>
                                                <th>Diagnosa</th>
                                                <th>Aksi</th>
                                             </tr>
                                          </thead>
                                          <tbody class="tbodydiagnosasdki"></tbody>
                                       </table>
                                    </td>
                                 </tr> -->
                              </tbody>
                           </table>
                           <table>
                              <tbody>
                                 <tr class="siki-otek">
                                    <td></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <!-- <h4>PLAN (INTERVENSI KEPERAWATAN)</h4> -->
                           <h4>Evaluasi Keperawatan</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width : 100%;display:none;">
                              <tbody>
                                 <tr class="trintervensinyeri">
                                    <td>
                                       <strong>Intervensi Nyeri</strong><br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri0" name="planintervensinyerimengajarkanteknikrelaksasi">&nbsp;Mengajarkan teknik relaksasi dan distraksi <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri1" name="planintervensinyerimegaturposisiyangnyamanbagipasien">&nbsp;Mengatur posisi yang nyaman bagi pasien <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri2" name="planintervensinyerimengontrollingkungan">&nbsp;Mengontrol lingkungan yang dapat mempengaruhi nyeri, seperti cahaya, tingkat kebisingan, dll <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri3" name="planintervensinyerimemberikanedukasikepadapasiendankeluarga">&nbsp;Memberikan edukasi kepada pasien dan keluarga <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri4" name="planintervensinyerikoloborasidengandokter">&nbsp;Koloborasi dengan dokter untuk pemberian obat analgetik<br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri5" name="planintervensinyerichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensinyerilainnya"> <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri6" name="planintervensinyeritidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="6">
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintervensirisikojatuh">
                                    <td>
                                       <strong>Intervensi Risiko Jatuh</strong> <br>
                                       <input type="checkbox" name="planintervensirisikojatuhpakaikanpitabewarnakuning" id="chckintervensirisikojatuh0" class="chckintervensirisikojatuh">&nbsp;Pakaikan pita bewarna kuning pada lengan kanan atas pasien <br>
                                       <input type="checkbox" name="planintervensirisikojatuhberikanedukasipencegahanjatuh" id="chckintervensirisikojatuh1" class="chckintervensirisikojatuh">&nbsp;Berikan edukasi pencegahan jatuh pada pasien / keluarga <br>
                                       <input type="checkbox" name="planintervensirisikojatuhchcklainnya" id="chckintervensirisikojatuh2" class="chckintervensirisikojatuh">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikojatuhtidakadaintervensi"> <br>
                                       <input type="checkbox" name="planintervensirisikojatuhtidakadaintervensi" id="chckintervensirisikojatuh3" class="chckintervensirisikojatuh">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintervensirisikonutrisional">
                                    <td>
                                       <strong>Intervensi Risiko Nutrisional</strong> <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional0" name="planintervensirisikonutrisionalkonsultasikanpadaahligizi">&nbsp;Konsultasikan pada ahli gizi dan DPJP bila pasien mempunyai risiko nutrisional <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional1" name="planintervensirisikonutrisionalchcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikonutrisionallainnya"> <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional2" name="planintervensirisikonutrisionaltidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintervensistatusfungsional">
                                    <td>
                                       <strong>Intervensi Status Fungsional</strong> <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional0" name="intervensistatusfungsionallibatkanpartisipasikeluargadalam">&nbsp;Libatkan partisipasi keluarga dalam membantu pasien untuk melakukan aktivitas <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional1" name="intervensistatusfungsionalchcklainnya" id="chckintervebsistatusfungsionallainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensistatusfungsionallainnya"> <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional2" name="intervensistatusfungsionaltidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintervensipsikologi">
                                    <td>
                                       <strong>Intervensi Psikologi</strong> <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi0" name="intervensipsikologimenenangkanpasien">&nbsp;Menenangkan pasien <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi1" name="intervensipsikologimengajarkanteknikrelaksasi">&nbsp;Mengajarkan teknik relaksasi <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi2" name="intervensipsikologiberkoloborasidengandokter">&nbsp;Berkoloborasi dengan dokter dalam memberikan terapi pada pasien yang histeris <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi3" name="intervensipsikologichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensipsikologilainnya"> <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi4" name="intervensipsikologitidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintervensisosialekonomikulturaldanspiritual">
                                    <td>
                                       <strong>Intervensi Sosial, Ekonomi, Kultural, dan Spiritual</strong> <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial0" name="planintervensisosialmenjelaskankepadapasiendankeluarga">&nbsp;Menjelaskan kepada pasien dan kelaurga terkait persyaratan asusransi kesehatan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial1" name="planintervensisosialmemfasilitasibiladahambatan">&nbsp;Memfasilitasi bila ada hambatan dalam berkomunikasi dengan melaporkan kepada tim yang bertanggung jawab <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial2" name="planintervensisosialmemberikanalternatiftindakanlain">&nbsp;Memberikan alternatif tindakan lain yang tidak bertentangan dengan kepercayaan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial3" name="planintervensisosialmenjelaskanterkaittindakan">&nbsp;Menjelaskan terkait tindakan/obat-obatan yang tidak tertanggung oleh asuransi kesehatan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial4" name="planintervensisosialchcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensisosiallainnya"> <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial5" name="planintervensisosialtidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr class="trintevensialergi">
                                    <td>
                                       <strong>Intervensi Alergi</strong> <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi0" name="planintervensialergimemberitahukankepadadokter">&nbsp;Memberitahukan kepada dokter terkait alergi pasien <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi1" name="planintervensialergiedukasikepadapasiendankeluarga">&nbsp;Edukasi kepada pasien dan keluarga tentang hal-hal yang hyarus dihindarkan dan pertolongan pertama pada reaksi alergi <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi4" name="planintervensialergichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensialergilainnya"> <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi5" name="planintervensialergitidakadaintervensi">&nbsp;Tidak ada intervensi
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width : 100%;display:none;">
                              <tbody>
                                 <tr>
                                    <td>
                                       <strong>Intervensi Keperawatan Lainnya</strong> <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyastabilisasijalannafas">&nbsp;Stabilisasi jalan nafas <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakeluarkanbendaasing">&nbsp;Keluarkan benda asing, lakukan suction <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangiripharingeal">&nbsp;Pasang Oripharingeal Airway/ stabilisasi servikal (neck collar) <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanbantuannafas">&nbsp;Berikan bantuan nafas buatan/ventilasi mekanik <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikan02">&nbsp;Berikan O<sub>2</sub> melalui nasal kanul/NRM/RM <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyadelegatifnebulizer">&nbsp;Delegatif nebulizer <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortandatandavital">&nbsp;Monitor tanda-tanda vital secara periodik<br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortingkatkesadaran">&nbsp;Monitor tingkat kesadaran secara periodik <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamontorekg">&nbsp;Monitor EKG <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanposisisemifowler">&nbsp;Berikan posisi semifowler <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangdowercatheter">&nbsp;Pasang dower catheter <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan">&nbsp;Monitor intake dan output cairan<br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasanginfus">&nbsp;Pasang infus/cairan intravena, cairan koloidm darah atau produk darah, ekspander plasma <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyalakukanperawatanluka">&nbsp;Lakukan perawatan luka <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakoloborasidengandokter">&nbsp;Koloborasi dengan dokter untuk pemberian IV/IC/SC/IM/oral/supp <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakajirturgorkulit">&nbsp;Kaji turgor kulit dan membran mukosa mulut <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangpengaman">&nbsp;Pasang pengaman, spalk, lakukan imobilisasi <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamengobservasitandatanda">&nbsp;Mengobservasi tanda-tanda adanya kompartemen sindrom (nyeri local daerah cedera, pucat, penurunan mobilitas, penurunan tekanan nadi, nyeri bertambah saat digerakkan, perubahan sensori/baal dan kesemutan) <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaoasangngt">&nbsp;Pasang NGT <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya1">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya1"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya2">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya2"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya3">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya3"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya4">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya4">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width : 100%;">
                              <tbody>
                                 <tr>
                                    <td>
                                       <strong>Evaluasi Keperawatan</strong><br>
                                       <input type="checkbox" class="chckaevaluasikeperawatan" id="chckaevaluasikeperawatan0" name="chckaevaluasikeperawatan" value="0">&nbsp;Tidak ada <br>
                                       <input type="checkbox" class="chckaevaluasikeperawatan" id="chckaevaluasikeperawatan1" name="chckaevaluasikeperawatan" value="1">&nbsp;Ada, sebutkan <br>
                                       <textarea class="form-control" style="width: 1153px; height: 102px;" name="txtevaluasikeperawatan"></textarea>
                                       <br><br>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-12">
                     <table style="width : 100%;">
                        <tbody>
                           <tr>
                              <td style="width : 34%;">
                                 <label>Tanggal Pasien Pulang :</label><input type="date" name="txttanggalpasienpulang">
                              </td>
                              <td style="width : 34%;">
                                 <label>Jam Pasien Pulang : <input type="text" name="txtjampasienpulang"> WIB</label> <br>
                              </td>
                              <td style="width : 34%;">
                                 <label>Perawat Pelaksana :</label><input type="text" name="txtperawatpelaksana">
                              </td>
                           </tr>
                           <!-- <tr class="rulejampasienpulang">
                              <td></td>
                              <td>
                                 <div>
                                    <p>Tata Cara Pengisian Jam Pasien Pulang</p>
                                    <p>1. Isi Dimulai Dengan Detik</p>
                                    <p>2. Kemudian Diisi Dengan Menit</p>
                                    <p>3. Yang Terakhir Isi Dengan Jam</p>
                                    <p>NB: Separator Akan Muncul Sendiri Ketika Angka Diketikkan</p>
                                 </div>
                              </td>
                              <td></td>
                           </tr> -->
                        </tbody>
                     </table>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>


      
      <!-- <br><br> -->
   </div>
   <!-- Akhir Asesmen Awal Keperawatan -->
   <div class="edukasiform">
      <div class="header-title bg-warning" style="margin-bottom:25px;">
         <h3><?= strtoupper('Edukasi Interdisiplin'); ?></h3>
      </div>

      <table style="width:100%;">
            <tr>
               <td>
                  <div class="col-md-12">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>EDUKASI PASIEN DAN KELUARGA (Waiting in Progress)</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width :  100%;">
                              <tbody>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Apakah pasien/keluarga bersedia menerima edukasi&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi0" name="chckapakahpasienbersediamenerimaedukasi" value="0">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi1" name="chckapakahpasienbersediamenerimaedukasi" value="1">&nbsp;Bersedia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7">
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Apakah terdapat hambatan dalam edukasi&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi0" name="chckapakahterdapathambatandalamedukasi" value="0">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi1" name="chckapakahterdapathambatandalamedukasi" value="1">&nbsp;Ya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7">
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Jika "Ya" sebutkan hambatannya&nbsp;&nbsp;<br></td>
                                    <td><input type="checkbox" name="chckpendengaran" id="chckpendengaran1">&nbsp;Pendengaran <br> <input type="checkbox" name="chckemosi" id="chckemosi1">&nbsp;Emosi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckpenglihatan" id="chckpenglihatan1">&nbsp;Penglihatan <br> <input type="checkbox" name="chckbahasa" id="chckbahasa1">&nbsp;Bahasa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckkognitif" id="chckkognitif1">&nbsp;Kognitif <br> <input type="checkbox" name="chcklainnya" id="chcklainnya1">&nbsp;Lainnya &nbsp;&nbsp;<input type="text" name="evaluasipasiendankeluargajikayahamabatan"></td>
                                    <td><input type="checkbox" name="chckfisik" id="chckfisik1">&nbsp;Fisik<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckbudaya" id="chckbudaya1">&nbsp;Budaya<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7">
                                       <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Kebutuhan edukasi (Pilih topik edukasi)&nbsp;&nbsp;<br><br><br><br></td>
                                    <td colspan="2"><input type="checkbox" name="chckdiagnosapenyakit" id="chckdiagnosapenyakit1">&nbsp;Diagnosa Penyakit <br> <input type="checkbox" name="chckrehabilitasimedik" id="chckrehabilitasimedik1">&nbsp;Rehabilitasi medik <br> <input type="checkbox" name="chckhakdankewajibanpasien" id="chckhakdankewajibanpasien1">&nbsp;Hak dan kewajiban pasien <br> <input type="checkbox" name="chcktandabahayadanperawatanbayi" id="chcktandabahayadanperawatanbayi1">&nbsp;Tanda bahaya dan perawatan bayi baru lahir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckobatobatan" id="chckobatobatan1">&nbsp;Obat-obatan <br> <input type="checkbox" name="chckmanajemennyeri" id="chckmanajemennyeri1">&nbsp;Manajemen nyeri <br> <input type="checkbox" name="chckkb" id="chckkb1">&nbsp;KB<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckdietdannutrisi" id="chckdietdannutrisi1">&nbsp;Diet dan nutrisi <br> <input type="checkbox" name="chckpenggunaanalatmedia" id="chckpenggunaanalatmedia1">&nbsp;Penggunaan alat media <br> <input type="checkbox" name="chcktandabahayamasanifas" id="chcktandabahayamasanifas1">&nbsp;Tanda bahaya masa nifas<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
      </table>
   </div>
                      <hr>
                        <center id="menuPendaftaranPoliklinik">
                           <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >KEMBALI</a>
                       </center>
                        
                    </form>
                </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <li id="riwayatAkhir">
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                  <input type="hidden" id="riwayattanggalpemeriksaan"/>
                </ul>
                
                <div style="text-align:center;display: none;" >
                    <a class="btn btn-lg btn-success" id="loadMoreRiwayat"><i class="fa fa-search-plus"></i> Tampilkan riwayat sebelumnya</a>
                </div>
                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
