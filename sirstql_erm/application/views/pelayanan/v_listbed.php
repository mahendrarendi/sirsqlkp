<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-xs-12 col-md-2"><select id="idbangsal" class="select2" style="width:100%"></select></div>  
            <div class="col-xs-12 col-md-2"><a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a></div>
            <table id="dtbed" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
                  <tr class="bg bg-yellow-gradient">
                <th>NO</th>
                <th>BANGSAL</th>
                <th>KELAS</th>
                <th>STATUS BED</th>
                <th>NO.BED</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <?php } ?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->