
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div>
            <form id="formcari">
                <input type="text" id="tanggal1" name="tanggal1" class="datepicker" size="8"/>
                <input type="text" id="tanggal2" name="tanggal2" class="datepicker" size="8"/>
                <a style="margin-left:4px;" class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a>
                <a class="btn btn-warning btn-sm" id="refresh"><i class="fa fa-refresh"></i> Refresh</a>
                <a class="btn btn-success btn-sm" id="unduh"><i class="fa fa-file-excel-o"></i> Unduh</a>
            </form>
          </div>
          <div class="box-body" >
            <table id="dt_register" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                        <th>No</th>
                        <th>NORM</th>
                        <th>Nama Pasien</th>
                        <th width="200">Alamat</th>
                        <th>Jaminan</th>
                        <th>Ruang</th>
                        <th>Waktu Rencana</th>
                        <th>Waktu Operasi</th>
                        <th>Jenis Anestesi</th>
                        <th>Obat Anestesi</th>
                        <th class="none">Operator</th>
                        <th class="none">Anestesi</th>
                        <th class="none">dr. Anak</th>
                        <th class="none">Penata</th>
                        <th class="none">Asisten</th>
                        <th class="none">Instrumen</th>
                        <th class="none">Onloop</th>
                        <th class="none">RR/P.Bayi</th>
                        <th class="none">Diagnosa</th>
                        <th class="none">Tindakan</th>
                        <th class="none">Jenis Tindakan</th>
                        <th class="none">Asesment Pra Anestesi</th>
                        <th class="none">Kelengkapan Lap Anestesi</th>
                        <th class="none">Kelengkapan Monitoring Pemulihan PSCA Anestesi</th>
                        <th class="none">Konversi Anestesi Dari Lokal/Regional ke GA</th>
                        <th class="none">Asesmen Pra Bedah</th>
                        <th class="none">Marking</th>
                        <th class="none">Kelengkapan SCC</th>
                        <th class="none">Diskrepasi Diagnosa Pre dan Post</th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>
          </div>
        </div>
        <!-- /.box -->          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->