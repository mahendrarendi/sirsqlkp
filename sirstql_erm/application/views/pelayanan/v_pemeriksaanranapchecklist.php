<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
              <div class="row">
                <div class="col-md-12" style="margin-top:10px;margin-bottom:10px;">
                    
                    <div class="col-md-2 row">
                        <small><b>Nama Bangsal</b></small>
                        <select id="tampilbangsal" name="bangsal" class="form-control">
                            <option value="0">Pilih Bangsal</option>
                        </select>
                        
                    </div>
                    
                    <div class="col-md-3">
                        <small><b>Nama Pasien</b></small>
                        <select class="form-control select2" name="id_inap" id="id_inap" ><option value="0">Pilih Pasien Ranap</option></select>
                        <small class="text text-red">#pasien yang sudah dipulangkan tidak dapat dipilih.</small>
                    </div>
                    
                    <div class="col-md-2">
                    <small><b>Status Perawatan</b></small>
                    <select id="statusperawatan" name="statusperawatan" class="form-control">
                        <option value="rencana">Rencana</option>
                        <option value="terlaksana">Terlaksana</option>
                        <option value="batal">Batal</option>
                    </select>
                    </div>
                    
                    <div class="col-md-1">
                        <small><b>Tgl. Awal</b></small>
                        <input id="tanggalawal" size="7" class="datepicker"> 
                     </div>
                     <div class="col-md-1">
                         <small><b>Tgl. Akhir</b></small>
                         <input id="tanggalakhir" size="7" class="datepicker"> 
                     </div>

                     <div class="col-md-3">
                         <small>&nbsp;</small><br>
                         <a class="btn btn-info btn-sm" id="tampilchecklist"><i class="fa fa-desktop"></i> Tampil</a>
                         <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
                         <!--<a id="tampilsekarang" class="btn btn-warning btn-sm"><i class="fa fa-chevron-circle-down"></i> Tampil Sekarang</a>--> 
                     </div>
                </div>
                  
                <div id="pasienbelumdiplot"></div>
              </div>
              
              <table class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                      <tr class="header-table-ql">
                    <th>No</th>
                    <th>BANGSAL</th>
                    <th>PASIEN</th>
                    <th>JADWAL</th>
                    <th>TINDAKAN/OBAT</th>
                    <th>STATUS</th>
                  </tr>
                  </thead>
                  <tbody id="tampildata">
                  </tfoot>
              </table>
              <div class="col-md-7 col-xs-12 row">
                <table class="table">
                    <tr><th colspan="4">#Keterangan</th></tr>                       
                    <tr>
                        <td><a class="btn btn-primary btn-xs"><i class="fa fa-check"></i></a></td><td> Menu untuk merubah perencanaan menjadi terlaksana.<br/><strong> #semua tindakan dan obat yang direncanakan akan diubah menjadi terlaksana, kecuali yang telah dibatalkan.</strong></td> 
                        <td><a class="btn btn-primary btn-xs" ><i class="fa fa-check-square"></i></a></td><td> Menu untuk merubah rencana menjadi terlaksana.</td>
                    </tr>
                    
                    <tr>
                        <td><a class="btn btn-danger btn-xs"><i class="fa  fa-minus"></i></a></td><td> Menu Untuk merubah status perencanaan menjadi batal.</td>
                        <td><a class="btn btn-danger btn-xs"><i class="fa fa-minus-square"></i></a></td><td> Menu untuk merubah status rencana menjadi batal.</strong></td>
                    </tr> 
                    
                    <tr>
                        <td><a class="btn btn-danger btn-xs"><i class="fa  fa-reply-all"></i></a></td><td> Menu untuk mengembalikan perencanaan setelah terlaksana atau batal.</td>
                        <td><a class="btn btn-danger btn-xs"><i class="fa  fa-reply"></i></a></td><td> Menu untuk mengembalikan rencana setelah terlaksana atau batal </td>
                    </tr> 
                </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->