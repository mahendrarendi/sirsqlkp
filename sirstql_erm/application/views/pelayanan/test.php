<style type="text/css">
   .tabriwayat {
      background-color: #d2d6de;
   }

   .nav-tabs-custom>.nav-tabs>li.active>a {
      background-color: #f39c12;
      color: #444;
   }

   .form-pelayanan {
      display: block;
      width: 100%;
      height: 22px;
      padding: 2px 4px;
      font-size: 12.8px;
      line-height: 1.42857143;
      color: #222e32;
      background-color: #ffffff;
      background-image: none;
      border: 1px solid #ccc;
      border-radius: 2px;
      -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
      -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
      -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
      transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
   }

   .cHidden {
      background: transparent;
      border: none;
      height: 0px;
      margin: 0px;
      padding: 0px;
      display: inherit;
   }

   .riwayatparagraf {
      background-color: #ffffff;
      padding: 6px;
      border-radius: 2px;
   }

   .btn-circle {
      width: 15px;
      height: 15px;
      padding: 0px 0px;
      border-radius: 15px;
      font-size: 11px;
      text-align: center;
   }
</style>
<div class="box-body" style="background-color:#fff;">
   <div class="box-header">
      <a onclick="riwayatpasien_rajal()" class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
      <a onclick="cetak_hasillab()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;"><i class="fa fa-flask"></i> Laboratorium</a>
      <a onclick="cetak_hasilelektromedik()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;"><img src="<?= base_url('assets/images/icons/radiasi.svg') ?>"> Elektromedik</a>
   </div>
   <div class="row col-md-12">
      <div class="col-sm-3" id="pemeriksaanklinik_profile"></div>
      <!-- /.col -->
      <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile"></div>
   </div>
</div>
<br>

<div class="navigationfastmenu">
   <a data-toggle="tooltip" data-original-title="VitalSign" id="periksaVitalsign" onclick="setfocusVitalsign()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-male fa-lg"></i></a>
   <a data-toggle="tooltip" data-original-title="Radiologi" id="periksaElektromedik" onclick="setfocusElektromedik()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/images/icons/radiasi.svg') ?>" width="20"></a>
   <a data-toggle="tooltip" data-original-title="Laboratorium" id="periksaLaboratorium" onclick="setfocuslaboratorium()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-flask"></i></a>
   <a data-toggle="tooltip" data-original-title="Diagnosa" id="periksaDiagnosa" onclick="setfocusdiagnosa()" class=" btn btn-warning btn-lg fastmenu"><img src="<?= base_url('assets/icon/diagnoses-white.svg') ?>" width="20"></a>
   <a data-toggle="tooltip" data-original-title="Tindakan" id="periksaTindakan" onclick="setfocustindakan()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-medkit"></i></a>
   <a data-toggle="tooltip" data-original-title="Bhp/Obat" id="periksaBhp" onclick="setfocusobatbhp()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-toggle-on"></i></a>
   <!-- <a data-toggle="tooltip" data-original-title="Echocardiography" id="periksaEchocardiography" onclick="setfocusEchocardiography()" class=" btn btn-warning btn-lg fastmenu"><i class="fa fa-heartbeat fa"></i></a> -->
</div>

<section class="content">
   <div class="row">
      <div class="col-md-12">
         <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
               <li class="active tabriwayat"><a href="#activity" id="btnPemeriksaan" data-toggle="tab">Pemeriksaan</a></li>
               <li class="tabriwayat"><a href="#timeline" id="btnRiwayatPasien" data-toggle="tab">Riwayat Pasien</a></li>
            </ul>
            <div class="tab-content">
               <div class="active tab-pane" id="activity">
                  <div class="row col-sm-12" style="margin-top: 15px;">
                     <div class="box box-solid box-default" id="riwayatpemeriksaan">
                        <div class="box-header with-border">
                           <h1 class="box-title">Riwayat Pemeriksaan</h1>
                        </div>
                        <div class="row">
                           <div id="r_hasilPeriksa" class="col-sm-9" style="margin:none;padding-top:5px;max-height:500px;overflow-y: scroll;">
                           </div>
                           <div class="col-sm-3">
                              <div class="form-group col-sm-10">
                                 <a class="btn bt-md btn-primary" onclick="pemeriksaanklinik_gettglriwayat()">Riwayat</a>
                              </div>
                              <label class="control-label col-sm-12">Tanggal Periksa</label>
                              <div id="r_tanggalPeriksa" class="btn-group-vertical" style="margin:none;max-height:250px;overflow-y: scroll; padding-right:50px;padding-left:15px;">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa') ?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
                     <input type="hidden" name="idperiksa" />
                     <input type="hidden" name="idpendaftaran" />
                     <input type="hidden" name="idunit" />
                     <input type="hidden" name="idloket" id="idloket" />
                     <input type="hidden" name="idperson" />
                     <input type="hidden" name="carabayar" />
                     <input type="hidden" name="status" />
                     <input type="hidden" name="norm" />
                     <input type="hidden" id="validasidataobyektif" />
                     <input type="hidden" id="validasidatasubyektif" />
                     <input type="hidden" id="validasiicd10" />
                     <input type="hidden" name="modesave" value="<?= $mode; ?>" />
                     <input type="hidden" name="iddokterdpjp">
                     <input type="hidden" name="sessidperson" value="<?= $_SESSION['id_person_login']; ?>" />
                     <input type="hidden" id="waktumulai" name="waktumulai" />
                     <input type="hidden" id="waktulayanan" name="waktulayanan" />
                     <input type="hidden" id="waktusekarang" name="waktusekarang" />
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter DPJP</label>
                        <div class="col-sm-4"><input type="text" name="dokter" value="" class="form-control" readonly="readonly" /></div>
                        <div class="col-sm-2" id="viewMenuUbahDokter"></div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter Pemeriksa</label>
                        <div class="col-sm-4">
                           <select class="form-control" id="iddokterpemeriksa" name="iddokterpemeriksa">
                           </select>
                        </div>
                        <div class="col-sm-4">
                           Risiko Tinggi <br>
                           <input type="checkbox" name="risikotinggi0" class="chckrisikotinggi" value="0">&nbsp;Tidak <br>
                           <input type="checkbox" name="risikotinggi1" class="chckrisikotinggi" value="1">&nbsp;Ya
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Loket Poliklinik </label>
                        <div class="col-sm-4"><input type="text" name="namaloket" id="namaloket" class="form-control" readonly="readonly" /></div>
                        <div class="col-sm-2" id="viewMenuUbahLoket"></div>
                     </div>
                     <!-- VitalSign ===================== -->
                     <div class="form-group">
                        <input class="cHidden" id="focusVitalsign" type="text">
                        <label for="" class="col-sm-2 control-label">VitalSign</label>
                        <div class="col-sm-4">
                           <select class="select2 form-control" name="carivitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'')">
                              <option value="0">Pilih</option>
                           </select>
                        </div>
                        <label for="" class="col-sm-2 control-label">Paket VitalSign</label>
                        <div class="col-sm-3">
                           <select class="select2 form-control" name="paketvitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'ispaket')">
                              <option value="0">Pilih</option>
                              <?php
                              if (!empty($data_paketvitalsign)) {
                                 foreach ($data_paketvitalsign as $data) {
                                    echo "<option value='" . $data->idpaketpemeriksaan . "'>" . $data->namapaketpemeriksaan . "</option>";
                                 }
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                     <!-- LIST HASIL VitalSign -->
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class=" col-sm-10">
                           <span class="label label-default">*Tekan tab pada inputan nilai untuk menyimpan perubahan</span>
                           <table class="table" style="width: 95%">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th>Parameter</th>
                                    <th>Hasil</th>
                                    <th>NDefault</th>
                                    <th>NRujukan</th>
                                    <th>Satuan</th>
                                    <th width="8%">Aksi</th>
                                 </tr>
                              </thead>
                              <tbody id="listVitalsign">
                              </tbody>
                           </table>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <label for="" class="col-md-4">
                           <input type="checkbox" name="cindikasipasien" id="cindikasipasien" onclick="setFormIndikasiPasienWabah(this.value)" /> Pasien Terindikasi Wabah (Pasien Covid-19)
                        </label>
                     </div>
                     <div id="formIndikasiPasienWabah" class="form-group hide" style="margin-left:8px;">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="row col-md-8">
                           <div class="box box-warning col-xs-6">
                              <div class="box-header with-border">
                                 <h3 class="box-title"> Formulir Pemeriksaan Indikasi Pasien Wabah, (Pasien Covid-19).</h3>
                                 <div class="box-body">
                                    <div class="form-group">
                                       <label for="">Pilih Wabah</label>
                                       <select class="form-control" id="jeniswabah" name="jeniswabah" style="width:100%;"></select>
                                    </div>
                                    <div class="form-group">
                                       <label>Status Rawat</label>
                                       <select class="form-control" id="statusrawatwabah" onchange="change_statusrawatIPW(this.value)" name="statusrawatwabah" style="width:100%;"></select>
                                    </div>
                                    <div class="form-group">
                                       <label>Penanganan</label>
                                       <select class="form-control" id="penanganan" name="penanganan" style="width:100%;"></select>
                                    </div>
                                    <div class="form-group">
                                       <label>Sebab Penularan</label>
                                       <select class="form-control" id="sebabpenularan" name="sebabpenularan" style="width:100%;"></select>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div id="btnasesmenmedis" class="text-center row"></div>
                     <!-- Asssesment Modal Tab -->
                     <!-- <div class="box box-default box-asesmen"> -->
                     <!-- /.box-header -->
                     <!-- <div class="box-body">
                        <div class="nav-tabs-custom" style="margin: 0 auto; float:none;">
                          <ul class="nav nav-tabs bg">
                            <li><a class="text-bold" href="#tab_rawatjalan" onclick="asesmenmedis_rawatjalan()">Asesmen Awal Medis</a></li>
                            <li><a class="text-bold" href="#tab_ugd" onclick="asesmenmedis_igd()" data-toggle="tab" aria-expanded="false">Asesmen Awal Keperawatan</a></li>
                          </ul>
                        </div>
                        </div>
                        </div> -->
                     <!-- End Of Asssesment Modal Tab -->
                     <div class="kungungankurang30hari">
                        <div id="pemeriksaanInputAnamnesa">
                           <!-- ditampilkan di js -->
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Data Obyektif</label>
                           <div class="col-sm-9">
                              <textarea class="form-control textarea" id="dataobyektif" name="keterangan" placeholder="Keterangan Pemeriksaan" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTDATAOBYEKTIFRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Rencana/Catatan</label>
                           <div class="col-sm-9">
                              <textarea class="form-control textarea" name="rekomendasi" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRENCANACATATANRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <div class="form-group">
                           <label for="" class="col-sm-2 control-label">Alergi</label>
                           <div class="col-sm-9" id="">
                              <!-- listAlergi -->
                              <textarea class="form-control textarea" name="alergiobat" placeholder="" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTALERGIRALAN)) ? '' : 'disabled') ?>></textarea>
                           </div>
                        </div>
                        <!-- Kategori Skrining -->
                        <div class="form-group">
                           <label for="" class="col-sm-2 label-daftar control-label">Skrining</label>
                           <div class="col-sm-9" id="skrining" style="padding-top:5px;"></div>
                        </div>
                     </div>
                     <br><br>

                     <div class="text-center" id="question_visit">
                        <label class="control-label" style="font-size : 130%">Apakah jarak kunjungan > 30 hari?</label>
                        <table class="table table-borderless" style="margin-left : auto; margin-right : auto; width : 20%;">
                           <tbody>
                              <tr>
                                 <td colspan="2" class="text-center">
                                 </td>
                              </tr>
                              <tr>
                                 <td class="text-center"><button type="button" class="btn btn-primary" style="width : 80%;" onclick="setAssementTab()">Ya</button></td>
                                 <td class="text-center"><button type="button" class="btn btn-primary btnTidakJarakKunjungan" style="width : 59%;" onclick="hideAssementTab()">Tidak</button></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>

                     <div class="text-center tab-asesmen-medis">
                        <a class="btn btn-md btn-warning" style="width : 16%;" id="asesmenawalmedis" href="#">Pengkajian Awal Medis</a>
                        <a class="btn btn-md btn-warning" style="width : 16%;" id="navasesmenawalkeperawatan" href="#">Pengkajian Awal Keperawatan</a>
                     </div>

                     <!--               
                     <div class="tab-asesmen-medis">
                        <ul class="nav nav-tabs" id="asemenralantab">
                           <li class="navasesmen" id="navasesmenawalmedis"><a href="#" id="asesmenawalmedis" class=asawalmedis">Asesmen Awal Medis</a></li>
                           <li class="navasesmen" id="navasesmenawalkeperawatan"><a href="#" id="asesmenawalkeperawatan" class="asawalkeperatan">Asesmen Awal Keperawatan</a></li>
                        </ul>
                     </div> -->


                     <!-- Awal Asesmen Awal Medis -->
                     <!-- <form id="Formasesmenawalmedisralan"> -->
                     <div class="asesmenawalmedis">
                        <input type="hidden" name="idpendaftaran" />
                        <input type="hidden" name="idpemeriksaan" />
                        <input type="hidden" name="idunit" value="idunit"></input>
                        <table class="table table-bordered" style="width:100%">
                           <tbody>
                              <tr class="itemralanugd">
                                 <td colspan="3">
                                    <div class="col-md-10">
                                       <table style="width : 100%;">
                                          <tbody>
                                             <tr>
                                                <td style="width : 15%;"><label>Tanggal Pasien Periksa: </label></td>
                                                <td style="width : 30%;"><label id="tglpasienperiksa"></label></td>
                                                <td style="width : 15%;"><label for="">Jam Pasien Periksa: </label></td>
                                                <td style="width : 30%;"><label id="jampasienperiksa">: &nbsp;WIB</label></td>
                                             </tr>
                                             <tr>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </td>
                              </tr>
                              <tr class="itemugd itemralanugd">
                                 <td>
                                    <div class="col-md-10">
                                       <div class="panel panel-default">
                                          <div class="panel-heading">CARA PASIEN DATANG</div>
                                          <div class="panel-body">
                                             <table style="width : 100%;">
                                                <tbody>
                                                   <tr>
                                                      <td><label for=""><strong>Cara pasien datang :</strong></label></td>
                                                   </tr>
                                                   <tr>
                                                      <td style="width : 15%;"><input type="checkbox" name="chckcarapasiendatang" class="chckcarapasiendatang" id="chckcarapasiendatang0" value="0">&nbsp;Kemauan sendiri <br><br><br><br><br></td>
                                                      <td style="width : 25%;">
                                                         <table>
                                                            <tbody>
                                                               <tr>
                                                                  <td colspan="3">
                                                                     <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang1" value="1">&nbsp;Diantar (oleh orang lain/bukan kerabat)
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td>
                                                                     Nama
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantarnama">
                                                                  </td>
                                                               <tr>
                                                                  <td>
                                                                     Alamat
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantaralamat">
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td>
                                                                     No. Telp
                                                                  </td>
                                                                  <td>
                                                                     :&nbsp;
                                                                  </td>
                                                                  <td>
                                                                     <input type="text" name="chckcarapasiendatangdiantarnotelp">
                                                                  </td>
                                                               </tr>
                                                   </tr>
                                                </tbody>
                                             </table>
                                 </td>
                                 <td>
                                    <table>
                                       <tbody>
                                          <tr>
                                             <td><input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang2" value="2">&nbsp;Diantar polisi</td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <input type="checkbox" class="chckcarapasiendatang" name="chckcarapasiendatang" id="chckcarapasiendatang3" value="3">&nbsp;Rujukan
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>Dari</td>
                                             <td>:&nbsp;</td>
                                             <td><input type="text" name="chckcarapasiendatangrujukandari"></td>
                                          </tr>
                                          <tr>
                                             <td>Alasan</td>
                                             <td>:&nbsp;</td>
                                             <td><input type="text" name="chckcarapasiendatangrujukanalasan"></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </td>
                              </tr>
                           </tbody>
                        </table>

                        <table style="width : 80%;">
                           <tbody>
                              <tr>
                                 <td colspan="6"><strong>Kategori kasus</strong></td>
                              </tr>
                              <tr>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus0" value="0">&nbsp;Bedah</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus1" value="1">&nbsp;Non bedah</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus2" value="2">&nbsp;Kebidanan</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus3" value="3">&nbsp;Psikiatri</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus4" value="4">&nbsp;Anak</td>
                                 <td><input type="checkbox" class="chckkategorikasus" name="chckkategorikasus" id="chckkategorikasus5" value="5">&nbsp;KLL</td>
                              </tr>
                           </tbody>
                        </table> <br>
                        <table style="width : 88%">
                           <thead>
                              <tr>
                                 <th colspan="3" class="text-center"><strong>TRIASE METODE ESI <i>(Emergency Severity Index)</i></strong></th>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <th style="border: 1px solid black;" class="text-center">Level</th>
                                 <th style="border: 1px solid black;" class="text-center">Keterangan</th>
                                 <th style="border: 1px solid black;" class="text-center"><i>Response Time</i></th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme1" class="chhcktme" name="chhcktme" value="1">&nbsp;Level 1: <i>Resuscitation</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien sekarat, membutuhkan intervensi yang menyelamatkan nyawa</td>
                                 <td style="border: 1px solid black;">&nbsp;0 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme2" class="chhcktme" name="chhcktme" value="2">&nbsp;Level 2: <i>Emergency</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien berisiko tinggi dan/atau dengan penurunan kesadaran akut dan/atau nyeri berat dan/atau gangguan psikis berat</td>
                                 <td style="border: 1px solid black;">&nbsp;15 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme3" class="chhcktme" name="chhcktme" value="3">&nbsp;Level 3:&nbsp;<i>Urgent</i></td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan dua atau lebih sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;30 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme4" class="chhcktme" name="chhcktme" value="4">&nbsp;Level 4:&nbsp;<i>Non urgent</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan satu sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;60 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme5" class="chhcktme" name="chhcktme" value="5">&nbsp;Level 5:&nbsp;<i>False emergency</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Pasien tidak membutuhkan sumber daya</td>
                                 <td style="border: 1px solid black;">&nbsp;120 menit</td>
                              </tr>
                              <tr style="border: 1px solid black;">
                                 <td style="border: 1px solid black;">&nbsp;<input type="checkbox" id="chhcktme0" class="chhcktme" name="chhcktme" value="0">&nbsp;<i>Death on arrival</i> </td>
                                 <td style="border: 1px solid black;">&nbsp;Kasus meninggal</td>
                                 <td style="border: 1px solid black;">&nbsp;Tidak perlu</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
               </div>
            </div>
            </td>
            </tr>
            <tr class="itemralanugd">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>SUBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <div class="col-md-8">
                              <div class="form-group">
                                 <label>&nbsp;Riwayat Penyakit Sekarang</label><br>
                                 <textarea class="form-control txt_asesmen" rows="4" name="riwayatpenyakitsekarang"></textarea>
                              </div>
                              <br>
                              <div class="form-group">
                                 <label>Riwayat Penyakit Dahulu</label><br>
                                 <input type="checkbox" class="chckboxrpd" name="chcktidakriwayapenyakitdahulu" id="tidakriwayapenyakitdahulu">
                                 &nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckboxrpd yardp" name="yariwayatpenaykitdahulu" id="yariwayatpenaykitdahulu">
                                 &nbsp;Ada,sebutkan :<br>
                                 <textarea class="form-control txt_asesmen txtrdp" rows="4" name="txtriwayatpenyakitdahulu"></textarea>
                              </div>
                              <br>
                              <div class="form-group">
                                 <label>Obat yang Rutin Dikonsumsi</label><br>
                                 <input type="checkbox" class="chckobyrd" id="chcktidakobyrd" name="chcktidakobyrd">
                                 &nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckobyrd" id="chckyaobyrd" name="chckyaobyrd">
                                 &nbsp;Ada, sebutkan :
                                 <textarea name="txtobyrd" class="form-control txt_asesmen" rows="4"></textarea>
                              </div>
                              <br>
                              <div class="form-group">
                                 <label>Riwayat Alergi Obat dan Makanan</label><br>
                                 <input type="checkbox" class="form-check-input chckraodm" id="chcktidakraodm" name="chcktidakraodm">
                                 &nbsp;Tidak <br>
                                 <input type="checkbox" class="form-check-input chckraodm" id="chckyaraodm" name="chckyaraodm">
                                 &nbsp;Ya, sebutkan:
                                 <textarea class="form-control txt_asesmen" rows="4" name="txtraodm"></textarea>
                              </div>
                              <br>
                           </div>
                        </div>
                        <!-- <div class="panel-footer">Panel Footer</div> -->
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemralanugd">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>OBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <table>
                              <tbody>
                                 <tr>
                                    <td style="width : 14%">
                                       <label>Keadaan Umum</label>
                                    </td>
                                    <td>
                                       : <input type="text" style="width: 39%;" name="keadaanumum">
                                    </td>
                                    <td><label>Skala Nyeri</label></td>
                                    <td>: <input type="text" style="width: 30%;" name="skalanyeri"></td>
                                 <tr>
                                 <tr>
                                    <td>
                                       <label>GCS</label>
                                    </td>
                                    <td> :
                                       <label>E</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcse">
                                       <label>V</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsv">
                                       <label>M</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsm">
                                    </td>
                                 <tr>
                              </tbody>
                           </table>
                           <br>
                           <div class="col-md-8">
                              <div class="form-group">
                                 <label>Pemeriksaan Fisik</label>
                                 <textarea class="form-control txt_asesmen" rows="4" name="pemeriksaanfisik"></textarea>
                              </div>
                              <br>
                           </div>
                           <div class="col-md-8">
                              <div class="daftargambarpemeriksaanfisik"></div>
                           </div>
                           <div class="col-md-8">
                              <div class="form-group">
                                 <label for="">Pemeriksaan Penunjang Medik (Hasil yang abnormal)</label><br>
                                 <input type="checkbox" class="chckppm" id="chcktidakppm" name="chcktidakppm">&nbsp;&nbsp;Tidak ada<br>
                                 <input type="checkbox" class="chckppm" id="chckyakppm" name="chckyakppm">&nbsp;&nbsp;Ada, sebutkan :
                                 <textarea class="form-control txt_asesmen" rows="4" name="txtppm"></textarea>
                              </div>
                              <br>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ELEKTROMEDIK</h4>
                        </div>
                        <div class="panel-body">
                           <input class="cHidden" id="focusElektromedik" type="text">
                           <div class="form-group">
                              <!--jika mode periksa bisa menambahkan radiologi-->
                              <?php if ($mode == 'periksa') { ?>
                                 <div class="col-md-4">
                                    <select class="select2 col-md-3" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                                       <option value="0">Pilih Tindakan Radiologi</option>
                                    </select>
                                 </div>
                              <?php } ?>
                              <div class="col-md-11">
                                 <table class="table" style="width: 97%">
                                    <thead>
                                       <tr class="header-table-ql">
                                          <th>ICD</th>
                                          <th>Nama ICD</th>
                                          <th width="23%">Dokter Penerima JM</th>
                                          <th width="5%">Jumlah</th>
                                          <th>Biaya</th>
                                          <th>Potongan</th>
                                          <th width="19%">Aksi</th>
                                       </tr>
                                    </thead>
                                    <tbody id="listRadiologi">
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-7">
                                 <label>Hasil Expertise</label>
                                 <textarea class="form-control textarea" name="keteranganradiologi" rows="25" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
                                 <label>Saran</label>
                                 <textarea class="form-control textarea" name="saranradiologi" rows="12" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
                              </div>
                              <div class="col-sm-5">
                                 <label>Hasil Radiografi</label>
                                 <table class="table" style="width: 92%; background-color: #eeeeee;">
                                    <tbody>
                                       <tr>
                                          <td colspan="2" id="viewHasilRadiografi">
                                             <!--tampil dari js-->
                                          </td>
                                       </tr>
                                       <?php if ($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI)) { ?>
                                          <tr>
                                             <td colspan="2" style="padding-top:15px;padding-left:20px;">
                                                <a id="btnRadiologiEfilm" class="btn btn-md btn-primary">Unggah hasil <i class="fa fa-upload"></i></a>
                                             </td>
                                          </tr>
                                       <?php } ?>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>LABORATORIUM</h4>
                        </div>
                        <div class="panel-body">
                           <div class="pad margin" id="viewHasilLaboratorium"></div>

                           <div class="col-sm-7">
                              <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTKETERANGANLABORATORIUM)) ?: disabled) ?>></textarea>
                           </div>

                           <div class="col-sm-4 row">
                              <!--jika mode periksa dapat menambahkan laboratorium-->
                              <?php if ($mode == "periksa") { ?>
                                 <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)">
                                    <option value="0">Pilih Tindakan Laboratorium</option>
                                 </select>
                                 <br>
                                 <br>
                                 <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
                                    <option value="0">Pilih Paket Laboratorium</option>
                                    <?php
                                    if (!empty($data_paket)) {
                                       foreach ($data_paket as $data) {
                                          echo "<option value=" . $data->idpaketpemeriksaan . ">" . $data->namapaketpemeriksaan . "</option>";
                                       }
                                    }
                                    ?>
                                 </select>
                              <?php } ?>
                           </div>
                           <input class="cHidden" id="focusLaboratorium" type="text">
                           <table class="table" style="width: 95%">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th>Parameter</th>
                                    <th>Hasil</th>
                                    <th>NDefault</th>
                                    <th>NRujukan</th>
                                    <th>Satuan</th>
                                    <th>Biaya</th>
                                    <th>Potongan</th>
                                    <th width="12%">Aksi</th>
                                 </tr>
                              </thead>
                              <tbody id="listLaboratorium">
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ASSESSMENT (ICD 10)</h4>
                        </div>
                        <div class="panel-body">
                           <input class="cHidden" id="focusDiagnosa" type="text">
                           <div class="col-md-4">
                              <label>Diagnosa Primer</label>
                              <select class="ql-select2 select2 form-control" id="diagnosaprimer" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,primer)">
                                 <option value="0">Input Diagnosa</option>
                              </select>
                           </div>
                           <div class="col-md-4">
                              <label>Diagnosa Sekunder</label>
                              <select class="ql-select2 select2 form-control" id="diagnosasekunder" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,sekunder)">
                                 <option value="0">Input Diagnosa</option>
                              </select>
                           </div>
                           <div class="col-md-8" style="margin-top:8px;">
                              <label>Diagnosa Diluar ICD 10</label>
                              <textarea class="form-control" name="diagnosa" id="diagnosadilauricd10" rows="2"></textarea>
                           </div>
                           <div class="col-md-12" style="margin-top:4px;">
                              <table class="table">
                                 <thead>
                                    <tr class="header-table-ql">
                                       <th>ICD</th>
                                       <th>Nama ICD</th>
                                       <th>Level</th>
                                       <th>Alias</th>
                                       <th width="8%">Aksi</th>
                                    </tr>
                                 </thead>
                                 <tbody class="listDiagnosa10"></tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemralanugd">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>PLAN</h4>
                        </div>
                        <div class="panel-body">
                           <div class="col-md-8">
                              <div class="form-group">
                                 <label for="">Terapi</label>
                                 <input type="text" id="txtterapi" class="form-control" style="width:100%;" name="txtterapi">
                              </div>
                              <div class="form-group">
                                 <label>Catatan Lainnya</label><br>
                                 <input type="checkbox" class="form-check-input chckcl" id="chcktdkcatatanlainnya" name="chcktdkcatatanlainnya">
                                 &nbsp;Tidak <br>
                                 <input type="checkbox" class="form-check-input chckcl" id="chckyacatatanlainnya" name="chckyacatatanlainnya">
                                 &nbsp;Ya, sebutkan:<br>
                                 <textarea class="form-control" name="txtcatatanlainnya"></textarea>
                              </div>
                              <div class="itemugd">
                                 <div class="form-group">
                                    <label>Observasi Pasien di UGD</label><br>
                                    <input type="checkbox" class="form-check-input chckobservasipasiendiugd" id="chckobservasipasiendiugd0" name="chckobservasipasiendiugd" value="0">
                                    &nbsp;Tidak ada <br>
                                    <input type="checkbox" class="form-check-input chckobservasipasiendiugd" id="chckobservasipasiendiugd1" name="chckobservasipasiendiugd" value="1">
                                    &nbsp;Ada, sebutkan:<br>
                                    <textarea class="form-control" name="txtobservasipasiendiugd"></textarea>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <td>
                                             <div class="col-md-8"><strong>Rencana Perawatan dan Tindakan Selanjutnya</strong></div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td style="width : 10%;">
                                             <div class="col-md-12">
                                                &nbsp;<input type="checkbox" class="chckrpdts" id="chckrpdts0" value="0" name="chckrencanaperawatandantindakanlainnya">&nbsp;Rawat jalan
                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <td style="width : 15%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckrpdtskontrol" name="chckrawatjalanitem" id="chckrawatjalanitem0" value="0">&nbsp;Kontrol di : <br><br><br><br></td>
                                                         <td colspan="2">
                                                            <table>
                                                               <tbody>
                                                                  <tr>
                                                                     <td><input type="checkbox" class="chckkontroldi" id="chckkontroldi0" name="chckkontroldi" value="0">&nbsp;UGD, tanggal&nbsp; <input type="date" name="tglugd"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><input type="checkbox" class="chckkontroldi" name="chckkontroldi" id="chckkontroldi1" value="1">&nbsp;Poliklinik&nbsp;<input type="text" id="txtkontroldi" name="txtkontroldi">&nbsp;tanggal&nbsp;<input type="date" name="txttanggalpoliklinik"></td>
                                                                  </tr>
                                                                  <tr>
                                                                     <td><input type="checkbox" class="chckkontroldi" name="chckkontroldi" id="chckkontroldi2" value="2">&nbsp;PPK 1</td>
                                                                  </tr>
                                                               </tbody>
                                                            </table>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td colspan="2">
                                                            &nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckrpdtskontrol" name="chckrawatjalanitem" id="chckrawatjalanitem1" value="1">&nbsp;Kontrol bila ada keluhan :
                                                         </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="col-md-12"><input type="checkbox" class="chckrpdts" id="chckrpdts1" name="chckrencanaperawatandantindakanlainnya" value="1">&nbsp;Observasi lanjutan di UGD</div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="col-md-12"><input type="checkbox" class="chckrpdts" id="chckrpdts2" name="chckrencanaperawatandantindakanlainnya" value="2">&nbsp;Rawat inap, bangsal <input type="text" name="txtrawatinapbangsal"></div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="col-md-12"><input type="checkbox" class="chckrpdts" id="chckrpdts3" name="chckrencanaperawatandantindakanlainnya" value="3">&nbsp;Rujuk</div>
                                          </td>
                                       </tr>
                                       <tr>
                                          <td>
                                             <div class="col-md-12"><input type="checkbox" class="chckrpdts" id="chckrpdts4" name="chckrencanaperawatandantindakanlainnya" value="4">&nbsp;Menolak tindakan/rawat inap</div>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                              <div class="form-group itemugd">
                                 <table>
                                    <tbody>
                                       <tr>
                                          <td>
                                             <table>
                                                <tbody>
                                                   <tr>
                                                      <td><strong>Kondisi Pasien sebelum Meninggalkan UGD</strong></td>
                                                   </tr>
                                                   <tr>
                                                      <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu0" value="0">&nbsp;Stabil, kondisi membaik</td>
                                                   </tr>
                                                   <tr>
                                                      <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu1" value="1">&nbsp;Tidak ada perbaikan kondisi</td>
                                                   </tr>
                                                   <tr>
                                                      <td><input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu2" value="2">&nbsp;Memburuk</td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                          <td>
                                             <br>
                                             <table>
                                                <tbody>
                                                   <tr>
                                                      <td>
                                                         <input type="checkbox" class="chckkpsmu" name="chckkpsmu" id="chckkpsmu3" value="3">&nbsp;Meninggal dunia <br>
                                                         <table>
                                                            <tbody>
                                                               <tr>
                                                                  <td>Tanggal</td>
                                                                  <td>:</td>
                                                                  <td><input type="date" name="txttanggalmeninggaldunia"></td>
                                                               </tr>
                                                               <tr>
                                                                  <td>Jam</td>
                                                                  <td>:</td>
                                                                  <td><input type="time" name="txtjammeninggaldunia"></td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <table>
                                    <tbody>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>TINDAKAN</h4>
                        </div>
                        <div class="panel-body">
                           <input class="cHidden" id="focusTindakan" type="text">
                           <div class="col-md-8">
                              <div class="form-group">
                                 <label for="">TINDAKAN</label><br>
                                 <select class="form-control select2" name="caritindakan"></select>
                              </div>
                           </div>
                           <table class="table table-bordered">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th> ICD </th>
                                    <th> Nama ICD </th>
                                    <th> Dokter Penerima JM </th>
                                    <th> Jumlah </th>
                                    <th> Biaya </th>
                                    <th> Potongan </th>
                                    <th> Aksi </th>
                                 </tr>
                              </thead>
                              <tbody class="listTindakanAsesmen"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemdibawah30hari">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>OBAT/BHP</h4>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                              <div class="row">
                                 <div class="col-sm-5">
                                    <span class="label label-default">Keterangan Obat/Bhp</span>
                                    <textarea class="form-control" id="keteranganobat" rows="9" name="keteranganobat"></textarea>
                                 </div>
                                 <div class="col-sm-5">
                                    <label>&nbsp;</label>
                                    <select class="ql-select2 form-control" name="caribhp" style="width:100%;"></select>
                                    <small class="text text-red">#stok yang tertera stok gudang.</small>
                                    <br><br>
                                    <span style="margin-right:20px;"><input id="adaracikan" name="adaracikan" type="checkbox" class=""> Ada Racikan</span>
                                    <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
                                    <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
                                    <br>
                                    <div class="pasien-prolanis <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS)) ? ''  : 'hide'); ?>">
                                       <br>
                                       <input id="prolanis" name="prolanis" type="checkbox" /> PROLANIS
                                       <br>
                                       <span class="label label-default">Tanggal Kunjungan Berikutnya</span>
                                       <div class="input-group date col-md-6">
                                          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                          <input type="text" readonly="readonly" placeholder="input tanggal..." class="form-control pull-right" id="kunjunganberikutnya" name="kunjunganberikutnya">
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <input class="cHidden" id="focusObat" type="text">
                           <table class="table table-bordered">
                              <thead>
                                 <tr class="header-table-ql">
                                    <th>No</th>
                                    <th>Obat/BHP[ExdDate]</th>
                                    <th>Resep<sup>1)</sup></th>
                                    <th>Kekuatan</th>
                                    <th>DosisRacik<sup>6)</sup></th>
                                    <th>GrupRacik<sup>6)</sup></th>
                                    <th>Diresepkan</th>
                                    <th>Tot.Harga</th>
                                    <th>Tot.Diberikan<sup>4)</sup></th>
                                    <th>Diberikan</th>
                                    <th>Penggunaan</th>
                                    <th colspan="2">AturanPakai</th>
                                 </tr>
                              </thead>
                              <tbody class="viewBhpRalan"></tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemugd itemralanugd">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-4">
                                 <label>Tanggal Pasien Pulang</label><br>
                                 <input type="date" id="tglpasienpulangugd" name="tglpasienpulangugd" class="form-control" readonly>
                              </div>
                              <div class="col-md-4">
                                 <label>Jam Pasien Pulang</label><br>
                                 <input type="time" name="jampasienpulangugd" class="form-control" id="jampasienpulangugd" readonly>
                              </div>
                              <div class="col-md-4">
                                 <label for="idpersondokter">Nama Dokter</label><br>
                                 <select name="namadokterugd" id="" class="select2 ql-select2 form-control">
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr class="itemralan itemralanugd">
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-4">
                                 <label>Tanggal Selesai As.Awal :</label><input type="date" id="tglselesaiasesmen" name="tglselesaiasesmen" readonly>
                                 <input type="hidden" name="idpersondokter" id="idpersondokter" readonly>
                              </div>
                              <div class="col-md-4">
                                 <label>Jam Pasien As.Awal : <input type="text" name="txtselesaiasesmenawal" id="txtselesaiasesmenawal" readonly> WIB</label>
                              </div>
                              <div class="col-md-4">
                                 <label>Nama Dokter :</label><input type="text" value="" id="namadokter" name="namadokter" readonly>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            </tbody>
            </table>
            <!-- </form> -->
            <!-- FORM RADIOLOGI/ELEKTROMEDIK -->
            <!-- <div class="box box-default">
                           <input class="cHidden" id="focusElektromedik" type="text">
                           <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>ELEKTROMEDIK</b></h3></div>
                           <!-- /.box-header -->
            <!-- <div class="box-body"> -->
            <!-- <div class="form-group"> -->
            <!--jika mode periksa bisa menambahkan radiologi-->
            <?php if ($mode == 'periksa') { ?>
               <!-- <div class="col-md-4"> -->
               <!-- <select class="select2 col-md-3" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)"> -->
               <!-- <option value="0">Pilih Tindakan Radiologi</option> -->
               <!-- </select> -->
               <!-- </div> -->
            <?php } ?>
            <!-- <div class="col-md-11">
                           <table class="table" style="width: 97%">
                              <thead>
                                <tr class="header-table-ql">
                                  <th>ICD</th>
                                  <th>Nama ICD</th>
                                  <th width="23%">Dokter Penerima JM</th>
                                  <th width="5%">Jumlah</th>
                                  <th>Biaya</th>
                                  <th>Potongan</th>
                                  <th width="19%">Aksi</th>
                                </tr>
                              </thead>
                              <tbody id="listRadiologi">
                              </tbody>
                            </table>
                           </div>
                           </div>
                           
                           <div class="form-group">
                           <div class="col-sm-7">
                           <!-- <label>Hasil Expertise</label> -->
            <!-- <textarea class="form-control textarea" name="keteranganradiologi" rows="25" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea> -->
            <!-- <label>Saran</label> -->
            <!-- <textarea class="form-control textarea" name="saranradiologi" rows="12" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea> -->
            <!-- </div> -->
            <!-- </div> -->
            <!-- </div> -->
            <!-- </div> -->
            <!-- END FORM RADIOLOGI/ELEKTROMEDIK -->
            <!-- FORM EKOKARDIOGRAFI -->
            <!-- <div class="box box-default">            
                           <input class="cHidden" id="focusEchocardiography" type="text">
                           <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>ECHOCARDIOGRAPHY</b></h3></div> -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
                           <div class="form-group">
                               <div class="col-md-11">
                                   <table class="table table-bordered table-hover table-striped">
                                       <thead>
                                           <tr class="header-table-ql">
                                               <th>Measurement</th>
                                               <th>Result</th>
                                               <th>Normal (Adult)</th>
                                           </tr>
                                       </thead>
                                       <tbody id="listEkokardiografi">                            
                                       </tbody>
                                   </table>
                               </div> -->
            <!-- <div class="col-sm-8">
                           <label>Keterangan</label>
                           <textarea class="form-control textarea" id="keteranganekokardiografi" name="keteranganekokardiografi" rows="18" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI)) ? '' : 'disabled') ?>></textarea>
                           </div>
                           </div>
                           </div> -->
            <!-- /.box-body -->
            <!-- </div> -->
            <!-- END FORM EKOKARDIOGRAFI -->
            <!-- FORM LABORATORIUM -->
            <!-- <div class="box box-default">
                           <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>LABORATORIUM</b></h3></div>
                           <!-- /.box-header -->
            <!-- <div class="box-body"> -->
            <!-- <div class="col-sm-7">
                           <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTKETERANGANLABORATORIUM)) ? '' : 'disabled') ?>></textarea>
                           </div> -->
            <!-- <div class="col-sm-4 row"> -->
            <!--jika mode periksa dapat menambahkan laboratorium-->
            <?php if ($mode == 'periksa') { ?>
               <!-- <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)"><option value="0">Pilih Tindakan Laboratorium</option></select> -->
               <!-- <br> -->
               <!-- <br> -->
               <!-- <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')"> -->
               <!-- <option value="0">Pilih Paket Laboratorium</option> -->
               <?php
               if (!empty($data_paket)) {
                  foreach ($data_paket as $data) {
                     // echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                  }
               }
               ?>
               <!-- </select> -->
            <?php } ?>
            <!-- </div> -->
            <input class="cHidden" id="focusLaboratorium" type="text">
            <!-- <table class="table" style="width: 95%">
                           <thead>
                             <tr class="header-table-ql">
                               <th>Parameter</th>
                               <th>Hasil</th>
                               <th>NDefault</th>
                               <th>NRujukan</th>
                               <th>Satuan</th>
                               <th>Biaya</th>
                               <th>Potongan</th>
                               <th width="12%">Aksi</th>
                             </tr>
                           </thead>
                           <tbody id="listLaboratorium">
                           </tbody>
                           </table>
                           </div>
                           </div> -->
            <!-- END FORM LABORATORIUM -->
            <!-- FORM DIAGNOSA -->
            <!-- <div class="box box-default"> -->
            <!-- <input class="cHidden" id="focusDiagnosa" type="text"> -->
            <!-- /.box-header -->
            <!-- <div class="box-body"> -->
            <!-- <div class="col-md-11"> -->
            <!-- ICD 10-->
            <!--                                 
                           <div class="box box-default box-solid">
                               <div class="box-header with-border">
                                 <h3 class="box-title">ICD 10 (Diagnosa)</h3>
                               </div>
                                -->
            <!-- /.box-header -->
            <!-- <div class="box-body">
                           <div class="col-md-4">
                               <label>Diagnosa Primer</label>
                               <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,'primer')">
                                   <option value="0">Input Diagnosa</option>
                               </select>
                           
                           </div>
                           <div class="col-md-4">
                               <label>Diagnosa Sekunder</label>
                               <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,'sekunder')">
                                   <option value="0">Input Diagnosa</option>
                               </select>
                           </div>
                           
                           <div class="col-md-8" style="margin-top:8px;">
                               <label>Diagnosa Diluar ICD 10</label>
                               <textarea class="form-control" name="diagnosa" rows="2"></textarea>
                           </div> 
                           
                           <div class="col-md-12" style="margin-top:4px;">
                               <table class="table">
                                   <thead>
                                     <tr class="header-table-ql">
                                       <th>ICD</th>
                                       <th>Nama ICD</th>
                                       <th>Level</th>
                                       <th>Alias</th>
                                       <th width="8%">Aksi</th>
                                     </tr>
                                   </thead>
                                   <tbody id="listDiagnosa10">
                                   </tbody>
                               </table>
                           </div>
                           </div>
                           </div> -->
            <!--end icd 10-->
            <!--ICD 9-->
            <!-- <div class="box box-default box-solid">
                           <div class="box-header with-border">
                             <h3 class="box-title">ICD 9 (Tindakan)</h3>
                           </div>
                           <!-- /.box-header -->
            <!-- <div class="box-body"> -->
            <!-- <div class="col-md-4">
                           <label>Input ICD 9</label>
                           <select class="select2 form-control" name="caridiagnosa9" style="width:100%;" onchange="pilihdiagnosa(this.value,9,'')">
                               <option value="0">Input</option>
                           </select>
                           </div> -->
            <!-- <div class="col-md-12" style="margin-top:4px;">
                           <table class="table">
                               <thead>
                                 <tr class="header-table-ql">
                                   <th>ICD</th>
                                   <th>Nama ICD</th>
                                   <th></th>
                                   <th>Alias</th>
                                   <th width="8%">Aksi</th>
                                 </tr>
                               </thead>
                               <tbody id="listDiagnosa9">
                               </tbody>
                           </table>
                           </div>
                           </div> -->
            <!-- </div> -->
            <!--end icd 9-->
            <!-- </div> -->
            <!-- </div> -->
            <!-- </div> -->
            <!-- END FORM DIAGNOSA -->
            <!-- FORM TINDAKAN -->
            <!-- <div class="box box-default">
                           <input class="cHidden" id="focusTindakan" type="text">
                           <div class="box-header ui-sortable-handle" style="cursor: move;">
                               <h3 class="box-title"><b>TINDAKAN</b></h3>
                           </div>
                           <!-- /.box-header -->
            <!-- <div class="box-body"> -->
            <?php if ($mode == 'periksa') { ?>
               <!-- <div class="col-sm-4 row">
                           <select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)">
                               <option value="0">Pilih</option>
                           </select> -->
               <!-- </div> -->
            <?php } ?>
            <!-- <table class="table" style="width: 95%">
                           <thead>
                               <tr class="header-table-ql">
                                   <th>ICD</th>
                                   <th>Nama ICD</th>
                                   <th width="17%">Dokter Penerima JM</th>
                                   <th width="5%">Jumlah</th>
                                   <th>Biaya</th>
                                   <th>Potongan</th>
                                   <th width="8%">Aksi</th>
                               </tr>
                           </thead>
                           <tbody class="listTindakan">
                           </tbody>
                           </table> -->
            <!-- <div class="col-md-12 row" style="margin-top:10px;">
                           <div class="col-xs-12 col-md-3 ">
                               <label>Kondisi Keluar</label>
                               <select class="form-control" id="kondisikeluar"  name="kondisikeluar" style="width:100%;" ></select>
                           </div>
                           </div>
                           
                           <div class="col-md-12 row" style="margin-top:10px;margin-bottom: 10px;">
                           <div class="col-xs-12 col-md-3">                  
                               <label>Status Pasien Pulang</label>
                               <select class="form-control" id="statuspasienpulang"  name="statuspasienpulang" style="width:100%;"></select>
                           </div>
                           </div>
                           
                           </div>
                           </div> -->
            <!-- END FORM TINDAKAN -->
            <!-- FORM diskon -->
            <!-- <div class="box box-default">
                           <div class="box-header ui-sortable-handle" style="cursor: move;">
                               <h3 class="box-title"><b>DISKON</b></h3>
                           </div>
                           
                           <div class="box-body">
                             <?php if ($mode == 'periksa') { ?>
                             <div class="col-sm-4 row">
                                 <select class="select2 form-control" name="caridiskon" style="width:100%;" onchange="pilihdiskon(this.value)">
                                       <option value="0">Pilih</option>
                                 </select>
                             </div> -->
         <?php } ?>
         <!-- <table class="table" style="width: 95%">
                           <thead>
                               <tr class="header-table-ql">
                                   <th>ICD</th>
                                   <th>Nama ICD</th>
                                   <th width="10%">Diskon</th>
                                   <th width="10%">Subtotal</th>
                                   <th width="8%">Aksi</th>
                               </tr>
                           </thead>
                           <tbody id="listDiskon">
                           </tbody>
                           </table> 
                           </div>
                           </div> -->
         <!-- END FORM Diskon -->
         <!-- FORM BHP ATAU OBAT -->
         <!-- <div class="box box-default">
                           <div class="box-header ui-sortable-handle" style="cursor: move;">
                               <h3 class="box-title"><b>OBAT/BHP</b></h3>
                           </div> -->
         <!-- /.box-header -->
         <!-- <div class="box-body">
                           <div class="col-sm-5">
                               <span class="label label-default">Keterangan Obat/Bhp</span>
                               <textarea class="form-control" id="keteranganobat" name="keteranganobat"  <?= (($mode == 'verifikasi') ? '' : ' onchange="pemeriksaanklinik_saveketobat(this.value)" ') ?> rows="9" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN)) ? '' : 'disabled') ?> ></textarea>
                           </div>
                           
                           <div class="col-sm-5">
                               <?php if ($mode == 'periksa') { ?>
                               <label>&nbsp;</label>
                               <select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value,'resep')"></select>
                               <small class="text text-red">#stok yang tertera stok gudang.</small> -->
      <?php } ?>
      <!-- <br>
                           <br> -->
      <!-- <span style="margin-right:20px;"><input id="adaracikan" name="adaracikan" type="checkbox"  class=""> Ada Racikan</span> -->
      <!--<a class="btn btn-primary btn-xs" onclick="cetakEtiketManual()"><i class="fa fa-print"></i> Etiket</a>-->
      <!-- <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a> -->
      <!-- <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a> -->
      <!-- <div class="pasien-prolanis <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS)) ? ''  : 'hide'); ?>">   
                           <br>
                           <input id="prolanis" name="prolanis" type="checkbox" /> PROLANIS
                           <br>
                           <span class="label label-default">Tanggal Kunjungan Berikutnya</span>
                               <div class="input-group date col-md-6">
                                   <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                   <input type="text" readonly="readonly" placeholder="input tanggal..." class="form-control pull-right" id="kunjunganberikutnya" name="kunjunganberikutnya">
                               </div>
                           </div>
                           </div>
                           <input class="cHidden" id="focusObat" type="text">
                           <?= table_bhpralan('viewBhpRalan'); ?>
                           </div>
                           </div> -->
      <!-- END FORM BHP ATAU OBAT -->
      <!-- </form> -->
         </div>
      </div>
   </div>

   <!-- Asesmen Awal Keperawatan -->
   <div class="asesmenawalkeperawatan">
      <!-- <form id="formasesmenawalkeperawatan"> -->
      <input type="hidden" name="idperiksa" />
      <input type="hidden" name="idpendaftaran" />
      <table class="table table-bordered" style="width:100%">
         <tbody>
            <tr>
               <td colspan="2">
                  <div class="col-md-10">
                     <table style="width : 100%;">
                        <tbody>
                           <tr>
                              <td style="width : 15%;"><label>Tanggal Pasien Periksa: </label></td>
                              <td style="width : 30%;"><label class="tglperiksa"></label></td>
                              <td style="width : 15%;"><label for="">Jam Pasien Periksa: </label></td>
                              <td style="width : 30%;"><label class="waktupasienperiksa"></label></td>
                           </tr>
                           <tr>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>SUBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <div class="form-group">
                              <label>Keluhan Utama</label>
                              <textarea class="form-control" style="width : 50%" name="txtkeluhanutama"></textarea>
                           </div>

                           <table>
                              <tbody>
                                 <tr>
                                    <td>Apakah pasien dengan keluhan tentang kebidanan dan kandungan?</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckapdktkdk" name="chckapdktkdktidak">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckapdktkdk" name="chckapdktkdkya">&nbsp;Ya</td>
                                 </tr>
                              </tbody>
                           </table>

                           <table class="asesmenobsyetridanginekologi">
                              <tbody>
                                 <tr>
                                    <td colspan="3"><label>Asesmen Obstetri dan Ginekologi</label></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label>Riwayat Kehamilan Sekarang</label>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : G<input type="text" style="width: 5%;" name="rksg"> P<input type="text" style="width: 5%;" name="rksp"> A<input type="text" style="width: 5%;" name="rksga"> Ah<input type="text" style="width: 5%;" name="rksah">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HPHT <input type="text" name="rksghpht"> HPL <input type="text" name="rksghpl"> UK <input type="text" name="rksguk">
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label>ANC Terpadu</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckancterpadu" name="chckanctidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="chckanciya" class="chckancterpadu">&nbsp;&nbsp;Ya,&nbsp; di <input type="text" name="txtanc">
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label>Imunisasi TT</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckitt" name="chcktdkitt">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckitt" name="chckyaitt">&nbsp;&nbsp;Ya,&nbsp; TT ke <input type="text" name="txtitt">
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <label>Pendarahan pervaginam</label> <br><br>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          : <input type="checkbox" class="chckppvm" name="chcktdkppvm">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckppvm" name="chckyappvm">&nbsp;&nbsp;Ya,&nbsp;<input type="text" name="txtppvm">&nbsp;ml
                                       </div>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                     </div>
                  </div>

               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>OBJECTIVE</h4>
                        </div>
                        <div class="panel-body">
                           <table class="table table-bordered">
                              <tbody>
                                 <tr>
                                    <td style="width:100px">
                                       <div class="form-group">
                                          <label><strong>Airway</strong></label>
                                       </div>
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Bersih" id="chckairwayBersih">&nbsp;Bersih<br><input type="checkbox" class="chckairway" name="chckairway" id="chckairwayDarah" value="Darah">&nbsp;Darah
                                    </td>
                                    <td style="width:16%">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Sumbatan" id="chckairwaySumbatan">&nbsp;Sumbatan<br><input type="checkbox" class="chckairway" name="chckairway" value="Lidah jatuh" id="chckairwayLidahjatuh">&nbsp;Lidah jatuh
                                    </td>
                                    <td style="width:12%;" colspan="1">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Sputum" id="chckairwaySputum">&nbsp;Sputum<br><input type="checkbox" class="chckairway" name="chckairway" value="Benda asing" id="chckairwayBendaAsing">&nbsp;Benda asing
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Lendir" id="chckairwayLendir">&nbsp;Lendir<br>
                                    </td>
                                    <td style="width:100px">
                                       <input type="checkbox" class="chckairway" name="chckairway" value="Ludah" id="chckairwayLudah">&nbsp;Ludah<br>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <div class="form-group">
                                          <label><strong>Breathing</strong></label><br>Pernafasan
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          <input type="checkbox" class="chckbre" name="chckbre" value="Tidak sesak" id="chckbretidaksesak">&nbsp;Tidak sesak
                                       </div>
                                    </td>
                                    <td>
                                       <div class="form-group">
                                          <input type="checkbox" class="chckbre" name="chckbresesak" id="chckbresesak">&nbsp;Sesak:<br>
                                          &nbsp;&nbsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Saat Aktivitas" id="chckbresaataktivitas">&nbsp;Saat Aktivitas<br>
                                          &nbsp;&nbsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Tanpa Aktivitas" id="chckbretanpaaktivitas">&nbsp;Tanpa aktivitas
                                       </div>
                                    </td>
                                    <td colspan="2">
                                       <div class="form-group">
                                          <input type="checkbox" class="chckbre" name="chckbre" value="Tidak ada nafas" id="chckbretidakadanafas">&nbsp;Tidak ada nafas
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Suara nafas</p>
                                    </td>
                                    <td style="width:120px">
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Vesikuler" id="chcksuanavesikuler">&nbsp;Vesikuler
                                    </td>
                                    <td>
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Ronkhi" id="chcksuanaronkhi">&nbsp;Ronkhi
                                    </td>
                                    <td>
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Crakles" id="chcksuanacrakles">&nbsp;Crakles
                                    </td>
                                    <td>
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Wheezing" id="chcksuanawheezing">&nbsp;Wheezing
                                    </td>
                                    <td>
                                       <input type="checkbox" name="chcksuana" class="chcksuana" value="Stridor" id="chcksuanastridor">&nbsp;Stridor
                                    </td>
                                    <td style="width : 25%;">
                                       <input type="checkbox" class="chcksuana" name="chcksuana" value="Tidak ada suara nafas" id="chcksuanatidakadasuaranafas">&nbsp;Tidak ada suara nafas
                                    </td>
                                    <td></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">
                                       <strong>Circulation</strong>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pulsasi perifer</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulper" name="chckpulperteraba">&nbsp;Teraba
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulper" name="chckpulpertidakteraba">&nbsp;Tidak teraba
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pulsasi karotis</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulsasikarotis" name="chckpulsasikarotisteraba">&nbsp;Teraba
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckpulsasikarotis" name="chckpulsasikarotistidakteraba">&nbsp;Tidak teraba
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Warna kulit</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckwarnakulit" name="chckwarnakulitkemerahan">&nbsp;Kemerahan
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckwarnakulit" name="chckwarnakulitpucat">&nbsp;Pucat
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Akral</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckakral" name="chckakralhangat">&nbsp;Hangat
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckakral" name="chckakraldingin">&nbsp;Dingin
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>CRT</p>
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckcrt" name="chckcrtkurang2dtk">&nbsp;<2 dtk </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckcrt" name="chckcrtlebih2dtk">&nbsp;>2 dtk
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <strong>Kesadaran</strong>
                                    </td>
                                    <td style="width:14%;">
                                       <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadarancomposmentis" value="Compos mentis">&nbsp;Compos mentis
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaranapatis" value="Apatis">&nbsp;Apatis
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaransomnolen" value="Somnolen">&nbsp;Somnolen
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaransupor" value="Supor">&nbsp;Supor
                                    </td>
                                    <td style="width:100px;">
                                       <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadarancoma" value="Coma">&nbsp;Coma
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="3"><strong>Pengkajian Nyeri</strong></td>
                                 </tr>
                                 <tr>
                                    <td colspan="7" width="100%"><input type="checkbox" name="chckppnnips" class="chckppn">&nbsp;<i>NIPS (Neonatal Infant Pain Score)</i> (Digunakan pada pasien bayi usia 0-30 hari)</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnflacc">&nbsp;<i>FLACC Behavioral Pain Scale</i> (Digunakan pada pasien bayi usia >30 hari sampai dengan usia <3 tahun, dan anak dengan gangguan kognitif)</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnwongbaker">&nbsp;<i>Wong Baker Faces Pain Scale</i> (Digunakan pada pasien anak usia &#8805; 3 tahun dan dewasa)</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                        <div id="tblnips">
                           <table class="table table-bordered" style="border: 1px solid black;">
                              <thead style="border: 1px solid black;">
                                 <tr>
                                    <th class="text-center" style="border: 1px solid black;" colspan="3"><label><i>NIPS</i>&nbsp;&nbsp;<i>(Neonatal Infant Pain Score)</i></label>
                                 </tr>
                                 <tr>
                                    <th style="border: 1px solid black; width:300px;" class="text-center">Pengkajian</th>
                                    <th style="border: 1px solid black;" class="text-center">Parameter</th>
                                    <th style="border: 1px solid black;" class="text-center">Skor</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Ekspresi Wajah</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chckekw" class="chckekw" id="chckekw0" value="0">&nbsp;Otot wajah rileks, ekspresi netral</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckekw" name="chckekw" id="chckekw1" value="1">&nbsp;Otot wajah tegang, alis berkerut rahang, dan dagu mengunci</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="4" style="border: 1px solid black;" class="text-center">Tangisan</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan0" value="0">&nbsp;Tenang, tidak menangis</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan1" value="1">&nbsp;Mengerang lemah, intermitten</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan2" value="2">&nbsp;Menangis kencang, melengking dan terus-menerus</td>
                                    <td style="border: 1px solid black;" class="text-center">2</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;" colspan="2" class="text-center">(Catatan : menangis tanpa suara diberi skor bila bayi diintubasi)</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Pola pernafasan</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna0" value="0">&nbsp;Bernapas biasa</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna1" value="1">&nbsp;Pola nafas berubah: tarikan ireguler, lebih cepat dibanding biasa, menahan napas, tersedak</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Tangan</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan0" value="0">&nbsp;Rileks, tidak ada kekakuan otot, gerakan tungkai biasa</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan1" value="1">&nbsp;Tegang, kaku</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td rowspan="2" style="border: 1px solid black;" class="text-center">Kesadaran</td>
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaran0" value="0">&nbsp;Rileks, tenang, tidur lelap atau bangun</td>
                                    <td style="border: 1px solid black;" class="text-center">0</td>
                                 </tr>
                                 <tr style="border: 1px solid black;">
                                    <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaran1" value="1">&nbsp;Sadar, atau gelisah</td>
                                    <td style="border: 1px solid black;" class="text-center">1</td>
                                 </tr>
                                 <tr style="border: 1px solid black;" class="text-center">
                                    <td style="border: 1px solid black;line-height: 12px;" colspan="2" class="text-center"><strong>Total Skor</strong></td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" name="totalskornips" value="0" style="width: 20%;" readonly>
                                       <input type="hidden" name="nipsekspresiwajah" class="nipsscore text-center" value="0">
                                       <input type="hidden" name="nipstangisan" class="nipsscore" class="nipsscore" value="0">
                                       <input type="hidden" name="nipspolapernafasan" class="nipsscore" value="0">
                                       <input type="hidden" name="nipstangan" class="nipsscore" value="0">
                                       <input type="hidden" name="nipskesadaran" class="nipsscore" value="0">
                                    </td>
                                 </tr>
                                 <tr style="line-height: 10px;">
                                    <td style="" colspan="4"><strong>Interpretasi hasil:</strong></td>
                                 </tr>
                                 <tr style="">
                                    <td style="" colspan="4">
                                       <input type="checkbox" id="chckbayi0" name="chckbayitidakmengalaminyeri" onclick="return false;" />&nbsp;< 3 :Bayi tidak mengalami nyeri<br>
                                          <input type="checkbox" id="chckbayi1" name="chckbayimengalaminyeri" onclick="return false;" />&nbsp;> 3 :Bayi mengalami nyeri
                                    </td>
                                 </tr>
                           </table>
                        </div>

                        <div id="tbflacc">
                           <table style="width : 100%;">
                              <thead>
                                 <tr style="border: 1px solid black">
                                    <th class="text-center" colspan="5"><label><i>FLACC Behavioral Pain Scale</i></label></th>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <th class="text-center" style="border : 1px solid black;width:100px;">Pengkajian</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 0</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 1</th>
                                    <th class="text-center" style="border : 1px solid black">Nilai 2</th>
                                    <th class="text-center" style="border : 1px solid black">Skor</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">
                                       <p>Wajah</p>
                                    </td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah0" value="0">&nbsp;Tersenyum / tidak ada ekspresi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah1" value="1">&nbsp;Terkadang merintih / menarik diri</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflaccwajah" name="chckflaccwajah" id="chckflaccwajah2" value="2">&nbsp;Sering menggetarkan dagu dan menggatupkan rahang</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccwajah" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">
                                       <p>Kaki</p>
                                    </td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki0" value="0">&nbsp;Normal / relaksasi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki1" value="1">&nbsp;Tidak tenang / tegang</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki2" value="2">&nbsp;Kaki menendang/menarik diri</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflacckaki" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Aktivitas</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian0" value="0">&nbsp;Tidur posisi normal / relaksasi</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian1" value="1">&nbsp;Gerakan menggeliat / bergeling kaku</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian2" value="2">&nbsp;Melengkungkan punggung / kaku / membentak</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccaktivitas" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Menangis</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis0" value="0">&nbsp;Tidak menangis</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis1" value="1">&nbsp;Mengerang / merengek</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis2" value="2">&nbsp;Menangis terus menerus / terisak / menjerit</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccmenangis" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center">Bersuara</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara0" value="0">&nbsp;Bersuara normal / tenang</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara1" value="1">&nbsp;Tenang bila dipeluk / digendong / diajak bicara</td>
                                    <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara2" value="2">&nbsp;Sulit untuk menenangkan</td>
                                    <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccbersuara" value="0" readonly></td>
                                 </tr>
                                 <tr style="border: 1px solid black">
                                    <td class="text-center" style="border: 1px solid black" colspan="4"><label><strong>Total Skor</strong></label></td>
                                    <td class="text-center">
                                       <input type="text" name="totalskorflacc" value="0" style="width : 20%;" readonly>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width: 50%;">
                              <tbody>
                                 <tr>
                                    <td colspan="4">Interprestasi hasil</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" class="chckinteprestrasihasilflacc0" name="chckinteprestrasihasilflacc" id="chckflaccdibawahnol" value="0" onclick="return false;">&nbsp;0</td>
                                    <td>:&nbsp;Nyaman</td>
                                    <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaraempatsampaienam" value="2" class="chckinteprestrasihasilflacc2" onclick="return false;">&nbsp;4-6</td>
                                    <td>:&nbsp;Nyeri sedang</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantarasatusampaitiga" class="chckinteprestrasihasilflacc1" value="1" onclick="return false;">&nbsp;1-3</td>
                                    <td>:&nbsp;Nyeri Ringan</td>
                                    <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaratujuhsampaisepuluh" class="chckinteprestrasihasilflacc3" value="3" onclick="return false;">&nbsp;7-10</td>
                                    <td>:&nbsp;Nyeri berat</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>

                        <div id="tbwongbaker">
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td class="text-center">
                                       <img src="<?= base_url() ?>assets/images/skalanyeri/nohurt.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/hurtslittlebit.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/hurtslittlemore.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtsevenmore.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtswholelot.png">
                                    </td>
                                    <td class="text-center">
                                       <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtsworst.png">
                                    </td>
                                 </tr>
                                 <tr>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker0" value="0"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker2" value="2"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker4" value="4"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker6" value="6"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker8" value="8"></td>
                                    <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker10" value="10"></td>
                                 </tr>
                              </tbody>
                           </table>

                           <table style="width: 50%;" id="tbinterprestasihasilwongbaker">
                              <tbody>
                                 <tr>
                                    <td colspan="4">Interprestasi hasil:</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" class="chckwongbakerinterpretasi0" id="chckwongbakernol" value="0" onclick="return false;">&nbsp;0</td>
                                    <td>:&nbsp;Nyaman</td>
                                    <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakerempatsampaienam" class="chckwongbakerinterpretasi2" value="2" onclick="return false;">&nbsp;4-6</td>
                                    <td>:&nbsp;Nyeri sedang</td>
                                 </tr>
                                 <tr>
                                    <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakersatusampaitiga" class="chckwongbakerinterpretasi1" value="1" onclick="return false;">&nbsp;1-3</td>
                                    <td>:&nbsp;Nyeri Ringan</td>
                                    <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakertujuhsampaisepuluh" class="chckwongbakerinterpretasi3" value="3" onclick="return false;">&nbsp;7-10</td>
                                    <td>:&nbsp;Nyeri berat</td>
                                 </tr>
                              </tbody>
                           </table>

                           <table style="width: 70%;" class="trwongbakesfacespainscaleinterpretasi">
                              <tbody>
                                 <tr>
                                    <td>Faktor yang memperberat</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfym0" class="chckfym" name="chckfym" value="0">&nbsp;Cahaya</td>
                                    <td><input type="checkbox" id="chckfym1" class="chckfym" name="chckfym " value="1">&nbsp;Gerakan</td>
                                    <td><input type="checkbox" id="chckfym2" class="chckfym" name="chckfym" value="2">&nbsp;Suara</td>
                                    <td><input type="checkbox" id="chckfym3" class="chckfym" name="chckfym" value="3">&nbsp;Lainnya</td>
                                 <tr>
                                 <tr>
                                    <td>Faktor yang meringankan</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfymer0" class="chckfymer" name="chckfymer" value="0">&nbsp;Istirahat</td>
                                    <td><input type="checkbox" id="chckfymer1" class="chckfymer" name="chckfymer" value="1">&nbsp;Dipijat</td>
                                    <td><input type="checkbox" id="chckfymer2" class="chckfymer" name="chckfymer" value="2">&nbsp;Minum Obat</td>
                                    <td><input type="checkbox" id="chckfymer3" class="chckfymer" name="chckfymer" value="3">&nbsp;Lainnya</td>
                                 <tr>
                                 <tr>
                                    <td>Kualitas nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckkualitasnyeri0" class="chckkualitasnyeri" name="chckkualitasnyeri" value="0">&nbsp;Ditusuk</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri1" class="chckkualitasnyeri" name="chckkualitasnyeri" value="1">&nbsp;Terikat</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri2" class="chckkualitasnyeri" name="chckkualitasnyeri" value="2">&nbsp;Tumpul</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri3" class="chckkualitasnyeri" name="chckkualitasnyeri" value="3">&nbsp;Berdenyut</td>
                                    <td><input type="checkbox" id="chckkualitasnyeri4" class="chckkualitasnyeri" name="chckkualitasnyeri" value="4">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyakualitasnyeri"></td>
                                 <tr>
                                 <tr>
                                    <td>Lokasi nyeri</td>
                                    <td colspan="4">:&nbsp;<input type="text" name="txtlokasinyeri"></td>
                                 <tr>
                                 <tr>
                                    <td>Menjalar</td>
                                    <td>:&nbsp;<input type="checkbox" class="chckmenjalar" name="chckmenjalartidak">&nbsp;Tidak</td>
                                    <td colspan="3"><input type="checkbox" class="chckmenjalar" name="chckmenjalaryajelaskan">&nbsp;Ya, jelaskan&nbsp;<input type="text" name="txtjelaskanmenjalar"></td>
                                 <tr>
                                 <tr>
                                    <td>Skala nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckskalanyeri0" class="chckskalanyeri" name="chckskalanyeri" value="0">&nbsp;Ringan</td>
                                    <td><input type="checkbox" id="chckskalanyeri1" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="1">&nbsp;Sedang</td>
                                    <td><input type="checkbox" id="chckskalanyeri2" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="2">&nbsp;Berat</td>
                                    <td><input type="checkbox" id="chckskalanyeri3" class="chckskalanyeri" name="chckskalanyeri" name="chckskalanyeri" value="3">&nbsp;Tidak nyeri</td>
                                 <tr>
                                 <tr>
                                    <td>Lamanya nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" name="chcklamanyanyeridibawah30menit" class="chcklamanyanyeri">&nbsp;<30 menit</td>
                                    <td><input type="checkbox" colspan="2" name="chcklamanyanyeridiatas30menit" class="chcklamanyanyeri">&nbsp;>30 menit</td>
                                 <tr>
                                 <tr>
                                    <td>Frekuensi nyeri</td>
                                    <td>:&nbsp;<input type="checkbox" id="chckfrekuensinyeri0" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="0">&nbsp;Terus menerus</td>
                                    <td><input type="checkbox" id="chckfrekuensinyeri1" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="1">&nbsp;Hilang timbul</td>
                                    <td><input type="checkbox" id="chckfrekuensinyeri2" class="chckfrekuensinyeri" name="chckfrekuensinyeri" value="2">&nbsp;Jarang <br><br></td>
                                 <tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>PENGKAJIAN</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Risiko Jatuh</strong></td>
                                 </tr>
                                 <tr>
                                    <td colspan="2" style="width : 50%;">Cara berjalan pasien: tidak seimbang / jalan dengan menggunakan alat bantu</td>
                                    <td style="width : 1px;">:</td>
                                    <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprjatuh1" class="chckprjatuh" name="chckprjatuhcvptsjdmabya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscorecbp" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" class="chckprjatuh" id="chckprjatuh0" name="chckprjatuhcvptsjdmabyatidak" value="0">&nbsp;Tidak</td>
                                 </tr>
                                 <tr>
                                    <td colspan="2">Menopang saat akan duduk (tampak memegang pinggiran kursi/benda lain)</td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckmsad1" class="chckmsad" name="chckmsadya" value="1">&nbsp;Ya <input type="hidden" name="txtjumlahskorpengkajianresikojatuh"><input type="hidden" name="jmlhscoremsad" value="0"></td>
                                    <td>&nbsp;<input type="checkbox" id="chckmsad0" class="chckmsad" name="chckmsadyatidak" value="0">&nbsp;Tidak</td>
                                 </tr>
                              </tbody>
                           </table>
                           <table style="width : 50%;">
                              <tbody>
                                 <tr>
                                    <td><br> Interprestasi hasil :</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh0" id="chckporjatuhtidakberisiko" value="0" onclick="return false;">&nbsp;Tidak berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh1" id="chckporjatuhrisikorendah" value="1" onclick="return false;">&nbsp;Resiko rendah</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh2" id="chckporjatuhrisikotinggi" value="2" onclick="return false;">&nbsp;Risiko tinggi</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 2 jawaban "Ya"</td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Risiko Nutrisional</strong></td>
                                 </tr>
                                 <tr>
                                    <td style="width : 50%;">Apakah pasien mempunyai penurunan berat badan dalam 1 bulan?</td>
                                    <td style="width : 1px;">: &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnberatbadan1" class="chckprnberatbadan" name="chckprnberatbadanya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnberatbadan" value="0">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckprnberatbadan" id="chckprnberatbadan0" name=chckprnberatbadantidak" value="0">&nbsp;Tidak</td>
                                    <td style="width : 10%;"></td>
                                    <td  style="width : 10%;"></td>
                                 </tr>
                                 <tr>
                                    <td  style="width : 50%;">Apakah pasien mempunyai masalah penurunan nafsu makan dalam 1 bulan?</td>
                                    <td style="width : 1px;">: &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnnafsumakan1" class="chckprnnafsumakan" name="chckprnnafsumakanya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnnafsumakan" value="0">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckprnnafsumakan" id="chckprnnafsumakan0" name="chckprnnafsumakantidak" value="0">&nbsp;Tidak <input type="hidden" name="jumlahskorpengkajianrisikonutrisional"></td>
                                    <td style="width : 10%;"></td>
                                    <td style="width : 10%;"></td>
                                 </tr>
                              </tbody>
                           </table>
                           <br>
                           <table style="width : 50%;">
                              <tbody>
                                 <tr>
                                    <td>Interprestasi hasil :</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn0" value="0" onclick="return false;">&nbsp;Tidak berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn1" value="1" onclick="return false;">&nbsp;Berisiko</td>
                                    <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Status Fungsional</strong></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Penggunaan alat bantu</p>
                                    </td>
                                    <td>:</td>
                                    <td style="width:10%;"><input type="checkbox" class="chckpealaban" id="chckpealaban0" name="chckpealabantidak">&nbsp;Tidak</td>
                                    <td><input type="checkbox" class="chckpealaban" id="chckpealaban1" name="chckpealabanya">&nbsp;Ya, sebutkan : <input type="text" name="txtoestafu"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>ADL (<i>Activities of Daily Living</i>) </p>
                                    </td>
                                    <td>:</td>
                                    <td><input type="checkbox" name="chckadlya" class="chckadl" id="chckadl1">&nbsp;Mandiri </td>
                                    <td><input type="checkbox" name="chckadltidak" class="chckadl" id="chckadl0">&nbsp;Dibantu </td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%">
                              <tbody>
                                 <tr>
                                    <td><strong>Pengkajian Status Psikologi</strong></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi0" value="0">&nbsp;Tenang</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi1" value="1">&nbsp;Disorentasi</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi2" value="2">&nbsp;Sedih</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi3" value="3">&nbsp;Takut</td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" id="chckpenstatuspsikologilainnya" id="chckpenstatuspsikologi4" name="chckpenstatuspsikologi" value="4"> &nbsp;Lainnya&nbsp;<input type="text" name="txtpengstatuspsikologi"></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="5" id="chckpenstatuspsikologi5">&nbsp;Marah/tegang <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="6" id="chckpenstatuspsikologi6">&nbsp;Cemas <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="7" id="chckpenstatuspsikologi7">&nbsp;Gelisah <br><br></td>
                                    <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="8" id="chckpenstatuspsikologi8">&nbsp;Resiko bunuh diri <br><br></td>
                                    <td></td>
                                 </tr>
                              </tbody>
                           </table>
                           <hr style="height:1px;border:none;color:#333;background-color:#333;" />
                           <table style="width : 100%;">
                              <tbody>
                                 <tr>
                                    <td colspan="4"><strong>Pengkajian Status Sosial, Ekonomi, Kultural, dan Spiritual</strong></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Status pernikahan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi0" value="0">&nbsp;Belum menikah</td>
                                    <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi1" value="1">&nbsp;Menikah</td>
                                    <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi2" value="2">&nbsp;Duda/janda</td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Kendala komunikasi</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi0" name="chckkendalakomunikasitidakada">&nbsp;Tidak ada</label /td>
                                    <td colspan="2"><input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi1" name="chckkendalakomunikasiadasebutkan">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtkendadakomunikasilainnya"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Yang merawat dirumah</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah0" name=chckyangmerawatdirumahtidakada"">&nbsp;Tidak ada</td>
                                    <td colspan="2"><input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah1" name="chckyangmerawatdirumahadasebutkan">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtadasebutkanyangmerawatdirumah"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Pekerjaan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpekerjaan" id="chckpekerjaan0" name="chckpekerjaanlainnya" value="0">&nbsp;Tidak bekerja</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan1" value="1">&nbsp;PNS/TNI/POLRI</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan2" value="2">&nbsp;Swasta</td>
                                    <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan3" value="3">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyapekerjaan"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Jaminan kesehatan</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan0" name="chckjaminankesehatan" value="0">&nbsp;Mandiri</td>
                                    <td><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan1" name="chckjaminankesehatan" value="1">&nbsp;BPJS</td>
                                    <td colspan="2"><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan2" name="chckjaminankesehatan" id="chckjaminankesehatanlainnya" value="2">&nbsp;Lainnya&nbsp;<input type="text" name="txtjaminankesehatanlainnya"></td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <p>Apakah ada tindakan yang bertentangan dengan kepercayaan pasien?</p>
                                    </td>
                                    <td>:</td>
                                    <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien0" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasientidak">&nbsp;Tidak</td>
                                    <td><input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien1" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasienya">&nbsp;Ya</td>
                                 </tr>
                                 <tr>
                                    <td>Jika ada jelaskan tindakan tersebut</td>
                                    <td>:</td>
                                    <td colspan="2">&nbsp;&nbsp;&nbsp;<input type="text" name="txtjikaadajelaskantindakantersebut"></td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>ASSESSMENT (SDKI)</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width:90%">
                              <tbody>
                                 <tr>
                                    <td style="width : 50%;">
                                       <label>Diagnosa</label><br>
                                       <select class="ql-select2 select2 form-control" name="diagnosasdki">
                                       </select>
                                    </td>
                                    <td></td>
                                 </tr>
                                 <tr>
                                    <td style="width : 50%;">
                                       <label>Diagnosa diluar SDKI</label><br>
                                       <input type="text" class="form-control" name="diagnosadiluarsdki">
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>PLAN (INTERVENSI KEPERAWATAN)</h4>
                        </div>
                        <div class="panel-body">
                           <table>
                              <tbody>
                                 <tr class="trintervensinyeri">
                                    <td>
                                       <strong>Intervensi Nyeri</strong><br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri0" name="planintervensinyerimengajarkanteknikrelaksasi">&nbsp;Mengajarkan teknik relaksasi dan distraksi <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri1" name="planintervensinyerimegaturposisiyangnyamanbagipasien">&nbsp;Mengatur posisi yang nyaman bagi pasien <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri2" name="planintervensinyerimengontrollingkungan">&nbsp;Mengontrol lingkungan yang dapat mempengaruhi nyeri, seperti cahaya, tingkat kebisingan, dll <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri3" name="planintervensinyerimemberikanedukasikepadapasiendankeluarga">&nbsp;Memberikan edukasi kepada pasien dan keluarga <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri4" name="planintervensinyerikoloborasidengandokter">&nbsp;Koloborasi dengan dokter untuk pemberian obat analgetik<br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri5" name="planintervensinyerichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensinyerilainnya"> <br>
                                       <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri6" name="planintervensinyeritidakadaintervensi">&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintervensirisikojatuh">
                                    <td>
                                       <strong>Intervensi Risiko Jatuh</strong> <br>
                                       <input type="checkbox" name="planintervensirisikojatuhpakaikanpitabewarnakuning" id="chckintervensirisikojatuh0" class="chckintervensirisikojatuh">&nbsp;Pakaikan pita bewarna kuning pada lengan kanan atas pasien <br>
                                       <input type="checkbox" name="planintervensirisikojatuhberikanedukasipencegahanjatuh" id="chckintervensirisikojatuh1" class="chckintervensirisikojatuh">&nbsp;Berikan edukasi pencegahan jatuh pada pasien / keluarga <br>
                                       <input type="checkbox" name="planintervensirisikojatuhchcklainnya" id="chckintervensirisikojatuh2" class="chckintervensirisikojatuh">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikojatuhtidakadaintervensi"> <br>
                                       <input type="checkbox" name="planintervensirisikojatuhtidakadaintervensi" id="chckintervensirisikojatuh3" class="chckintervensirisikojatuh">&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintervensirisikonutrisional">
                                    <td>
                                       <strong>Intervensi Risiko Nutrisional</strong> <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional0" name="planintervensirisikonutrisionalkonsultasikanpadaahligizi">&nbsp;Konsultasikan pada ahli gizi dan DPJP bila pasien mempunyai risiko nutrisional <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional1" name="planintervensirisikonutrisionalchcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikonutrisionallainnya"> <br>
                                       <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional2" name="planintervensirisikonutrisionaltidakadaintervensi">&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintervensistatusfungsional">
                                    <td>
                                       <strong>Intervensi Status Fungsional</strong> <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional0" name="intervensistatusfungsionallibatkanpartisipasikeluargadalam">&nbsp;Libatkan partisipasi keluarga dalam membantu pasien untuk melakukan aktivitas <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional1" name="intervensistatusfungsionalchcklainnya"  id="chckintervebsistatusfungsionallainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensistatusfungsionallainnya"> <br>
                                       <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional2" name="intervensistatusfungsionaltidakadaintervensi" >&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintervensipsikologi">
                                    <td>
                                       <strong>Intervensi Psikologi</strong> <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi0" name="intervensipsikologimenenangkanpasien">&nbsp;Menenangkan pasien <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi1" name="intervensipsikologimengajarkanteknikrelaksasi">&nbsp;Mengajarkan teknik relaksasi <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi2" name="intervensipsikologiberkoloborasidengandokter">&nbsp;Berkoloborasi dengan dokter dalam memberikan terapi pada pasien yang histeris <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi3" name="intervensipsikologichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensipsikologilainnya"> <br>
                                       <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi4" name="intervensipsikologitidakadaintervensi" >&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintervensisosialekonomikulturaldanspiritual">
                                    <td>
                                       <strong>Intervensi Sosial, Ekonomi, Kultural, dan Spiritual</strong> <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial0" name="planintervensisosialmenjelaskankepadapasiendankeluarga">&nbsp;Menjelaskan kepada pasien dan kelaurga terkait persyaratan asusransi kesehatan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial1" name="planintervensisosialmemfasilitasibiladahambatan">&nbsp;Memfasilitasi bila ada hambatan dalam berkomunikasi dengan melaporkan kepada tim yang bertanggung jawab <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial2" name="planintervensisosialmemberikanalternatiftindakanlain">&nbsp;Memberikan alternatif tindakan lain yang tidak bertentangan dengan kepercayaan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial3" name="planintervensisosialmenjelaskanterkaittindakan">&nbsp;Menjelaskan terkait tindakan/obat-obatan yang tidak tertanggung oleh asuransi kesehatan <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial4" name="planintervensisosialchcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensisosiallainnya"> <br>
                                       <input type="checkbox" class="chckintervensisosial" id="chckintervensisosial5" name="planintervensisosialtidakadaintervensi">&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                                 <tr class="trintevensialergi">
                                    <td>
                                       <strong>Intervensi Alergi</strong> <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi0" name="planintervensialergimemberitahukankepadadokter">&nbsp;Memberitahukan kepada dokter terkait alergi pasien <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi1" name="planintervensialergiedukasikepadapasiendankeluarga">&nbsp;Edukasi kepada pasien dan keluarga tentang hal-hal yang hyarus dihindarkan dan pertolongan pertama pada reaksi alergi <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi4" name="planintervensialergichcklainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensialergilainnya"> <br>
                                       <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi5" name="planintervensialergitidakadaintervensi">&nbsp;Tidak ada intervensi <br><br>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>

                           <table>
                              <tbody>
                                 <tr>
                                    <td>
                                       <strong>Intervensi Keperawatan Lainnya</strong> <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyastabilisasijalannafas">&nbsp;Stabilisasi jalan nafas <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakeluarkanbendaasing">&nbsp;Keluarkan benda asing, lakukan suction <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangiripharingeal">&nbsp;Pasang Iripharingeal Airway/ stabilisasi servikal (neck collar) <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanbantuannafas">&nbsp;Berikan bantuan nafas buatan/ventilasi mekanik <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikan02">&nbsp;Berikan O<sub>2</sub> melalui nasal kanul/NRM/RM <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyadelegatifnebulizer">&nbsp;Delegatif nebulizer <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortandatandavital">&nbsp;Monitor tanda-tanda vital secara periodik<br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortingkatkesadaran">&nbsp;Monitor tingkat kesadaran secara periodik <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamontorekg">&nbsp;Monitor EKG <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanposisisemifowler">&nbsp;Berikan posisi semifowler <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangdowercatheter">&nbsp;Pasang dower catheter <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan">&nbsp;Monitor intake dan output cairan<br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasanginfus">&nbsp;Pasang infus/cairan intravena, cairan koloidm darah atau produk darah, ekspander plasma <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyalakukanperawatanluka">&nbsp;Lakukan perawatan luka <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakoloborasidengandokter">&nbsp;Koloborasi dengan dokter untuk pemberian IV/IC/SC/IM/oral/supp <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakajirturgorkulit">&nbsp;Kaji turgor kulit dan membran mukosa mulut <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangpengaman">&nbsp;Pasang pengaman, spalk, lakukan imobilisasi <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamengobservasitandatanda">&nbsp;Mengobservasi tanda-tanda adanya kompartemen sindrom (nyeri local daerah cedera, pucat, penurunan mobilitas, <br>penurunan tekanan nadi, nyeri bertambah saat digerakkan, perubahan sensori/baal dan kesemutan) <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaoasangngt">&nbsp;Pasang NGT <br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya1">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya1"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya2">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya2"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya3">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya3"><br>
                                       <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya4">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya4"><br><br>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>
                                       <strong>Evaluasi Keperawatan</strong><br>
                                       <input type="checkbox" class="chckaevaluasikeperawatan" name="chckaevaluasikeperawatantidak">&nbsp;Tidak ada <br>
                                       <input type="checkbox" class="chckaevaluasikeperawatan" name="chckaevaluasikeperawatanya">&nbsp;Ada, sebutkan <br>
                                       <textarea class="form-control" style="width : 70%;" name="txtevaluasikeperawatan"></textarea>
                                       <br><br>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="col-md-10">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4>EVALUASI PASIEN DAN KELUARGA</h4>
                        </div>
                        <div class="panel-body">
                           <table style="width :  100%;">
                              <tbody>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Apakah pasien/keluarga bersedia menerima edukasi&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi0" name="chckapakahpasienbersediamenerimaedukasitidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi1" name="chckapakahpasienbersediamenerimaedukasibersedia">&nbsp;Bersedia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7"><hr style="height:1px;border:none;color:#333;background-color:#333;" /></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Apakah terdapat hambatan dalam edukasi&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi0" name="chckapakahterdapathambatandalamedukasitidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi1" name="chckapakahterdapathambatandalamedukasiya">&nbsp;Ya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7"><hr style="height:1px;border:none;color:#333;background-color:#333;" /></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Jika "Ya" sebutkan hambatannya&nbsp;&nbsp;<br></td>
                                    <td><input type="checkbox" name="chckpendengaran" id="chckpendengaran1">&nbsp;Pendengaran <br> <input type="checkbox" name="chckemosi" id="chckemosi1">&nbsp;Emosi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckpenglihatan" id="chckpenglihatan1">&nbsp;Penglihatan <br> <input type="checkbox" name="chckbahasa" id="chckbahasa1">&nbsp;Bahasa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckkognitif" id="chckkognitif1">&nbsp;Kognitif <br> <input type="checkbox" name="chcklainnya" id="chcklainnya1">&nbsp;Lainnya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckfisik" id="chckfisik1">&nbsp;Fisik<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckbudaya" id="chckbudaya1">&nbsp;Budaya<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td colspan="7"><hr style="height:1px;border:none;color:#333;background-color:#333;" /></td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox" style="display : none;">&nbsp;Kebutuhan edukasi (Pilih topik edukasi)&nbsp;&nbsp;<br><br><br><br></td>
                                    <td colspan="2"><input type="checkbox" name="chckdiagnosapenyakit" id="chckdiagnosapenyakit1">&nbsp;Diagnosa Penyakit <br> <input type="checkbox" name="chckrehabilitasimedik" id="chckrehabilitasimedik1">&nbsp;Rehabilitasi medik <br> <input type="checkbox" name="chckhakdankewajibanpasien" id="chckhakdankewajibanpasien1">&nbsp;Hak dan kewajiban pasien <br> <input type="checkbox" name="chcktandabahayadanperawatanbayi" id="chcktandabahayadanperawatanbayi1">&nbsp;Tanda bahaya dan perawatan bayi baru lahir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckobatobatan" id="chckobatobatan1">&nbsp;Obat-obatan <br> <input type="checkbox" name="chckmanajemennyeri" id="chckmanajemennyeri1">&nbsp;Manajemen nyeri <br> <input type="checkbox" name="chckkb" id="chckkb1">&nbsp;KB<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td><input type="checkbox" name="chckdietdannutrisi" id="chckdietdannutrisi1">&nbsp;Diet dan nutrisi <br> <input type="checkbox" name="chckpenggunaanalatmedia" id="chckpenggunaanalatmedia1">&nbsp;Penggunaan alat media <br> <input type="checkbox" name="chcktandabahayamasanifas" id="chcktandabahayamasanifas1">&nbsp;Tanda bahaya masa nifas<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
               </td>
            </tr>
            <tr>
               <td>
                  <div class="com-md-10">
                     <table style="width : 100%;">
                        <tbody>
                           <tr>
                              <td style="width : 34%;">
                                 <label>Tanggal Pasien Pulang :</label><input type="date" name="txttanggalpasienpulang">
                              </td>
                              <td style="width : 34%;">
                                 <label>Jam Pasien Pulang : <input type="time" name="txtjampasienpulang"> WIB</label> <br>
                              </td>
                              <td style="width : 34%;">
                                 <label>Perawat Pelaksana :</label><input type="text" name="txtperawatpelaksana">
                              </td>
                           </tr>
                           <!-- <tr class="rulejampasienpulang">
                              <td></td>
                              <td>
                                 <div>
                                    <p>Tata Cara Pengisian Jam Pasien Pulang</p>
                                    <p>1. Isi Dimulai Dengan Detik</p>
                                    <p>2. Kemudian Diisi Dengan Menit</p>
                                    <p>3. Yang Terakhir Isi Dengan Jam</p>
                                    <p>NB: Separator Akan Muncul Sendiri Ketika Angka Diketikkan</p>
                                 </div>
                              </td>
                              <td></td>
                           </tr> -->
                        </tbody>
                     </table>
                  </div>
               </td>
            </tr>
         </tbody>
      </table>
      <br><br>
      <!-- </form> -->
   </div>
   <!-- End of Asesment awal keperawatan -->




   <!-- tab asesmen ugd -->
   <!-- <div class="tab-asesmen-ugd">
               <ul class="nav nav-tabs" id="asemenralantab">
                  <li class="navasesmen" id="navasesmenawalmedis"><a href="#" id="asesmenawalmedis">Pengkajian Awal Medis</a></li>
                  <li class="navasesmen" id="navasesmenawalkeperawatan"><a href="#" id="asesmenawalkeperawatan">Pengkajian Awal Keperawatan</a></li>
               </ul>
               </div> -->
   <!-- end of asesmen ugd -->
   <!-- asesmen awal ugd -->
   <!-- <div class="ugdpengkajianwalmedis">
               <form action="" method="POST" id="formugdpengkajianawalmedis">
                  <input type="hidden" name="idpendaftaran" />
                  <input type="hidden" name="idpemeriksaan" />
                  <input type="hidden" name="idunit" value="idunit">
                  <table class="table table-bordered" style="width:100%">
                     <tbody>
                        <tr>
                           <td colspan="4">
                              <table style="width : 75%;">
                                 <tbody>
                                    <tr>
                                       <td class="text-center"><strong>PENGKAJIAN AWAL MEDIS GAWAT DARURAT</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label>Tanggal Pasien Periksa</label>
                                       <label id="tglpasienperiksa"></label>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="">Jam Pasien Periksa </label>
                                    <label id="jampasienperiksa">: &nbsp;WIB</label>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td><strong>Cara pasien datang :</strong></td>
                        </tr>
                        <tr>
                           <td>
                              <table style="width : 100%;">
                                 <tbody>
                                    <tr>
                                       <td style="width : 15%;"><input type="checkbox">&nbsp;Kemauan sendiri <br><br><br><br><br></td>
                                       <td style="width : 25%;">
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td colspan="3">
                                                      <input type="checkbox">&nbsp;Diantar (oleh orang lain/bukan kerabat)
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      Nama
                                                   </td>
                                                   <td>
                                                      :&nbsp;
                                                   </td>
                                                   <td>
                                                      <input type="text">
                                                   </td>
                                                <tr>
                                                   <td>
                                                      Alamat
                                                   </td>
                                                   <td>
                                                      :&nbsp;
                                                   </td>
                                                   <td>
                                                      <input type="text">
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      No. Telp
                                                   </td>
                                                   <td>
                                                      :&nbsp;
                                                   </td>
                                                   <td>
                                                      <input type="text">
                                                   </td>
                                                </tr>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                       <td>
                                          <table>
                                             <tbody>
                                                <tr>
                                                   <td><input type="checkbox">&nbsp;Diantar polisi</td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <input type="checkbox">&nbsp;Rujukan
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>Dari</td>
                                                   <td>:&nbsp;</td>
                                                   <td><input type="text"></td>
                                                </tr>
                                                <tr>
                                                   <td>Alasan</td>
                                                   <td>:&nbsp;</td>
                                                   <td><input type="text"></td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <table style="width : 80%;">
                                 <tbody>
                                    <tr>
                                       <td colspan="6"><strong>Kategori kasus :</strong></td>
                                    </tr>
                                    <tr>
                                       <td><input type="checkbox">&nbsp;Bedah</td>
                                       <td><input type="checkbox">&nbsp;Non bedah</td>
                                       <td><input type="checkbox">&nbsp;Kebidanan</td>
                                       <td><input type="checkbox">&nbsp;Psikiatri</td>
                                       <td><input type="checkbox">&nbsp;Anak</td>
                                       <td><input type="checkbox">&nbsp;KLL</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <table style="width : 75%">
                                 <thead>
                                    <tr>
                                       <th colspan="3" class="text-center"><strong>TRIASE METODE ESI <i>(Emergency Severity Index)</i></strong></th>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <th style="border: 1px solid black;" class="text-center">Level</th>
                                       <th style="border: 1px solid black;" class="text-center">Keterangan</th>
                                       <th style="border: 1px solid black;" class="text-center"><i>Response Time</i></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;Level 1: <i>Resuscitation</i></td>
                                       <td style="border: 1px solid black;">&nbsp;Pasien sekarat, membutuhkan intervensi yang menyelamatkan nyawa</td>
                                       <td style="border: 1px solid black;">&nbsp;0 menit</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;Level 2: <i>Emergency</i></td>
                                       <td style="border: 1px solid black;">&nbsp;Pasien berisiko tinggi dan/atau dengan penurunan kesadaran akut dan/atau nyeri berat dan/atau gangguan psikis berat</td>
                                       <td style="border: 1px solid black;">&nbsp;15 menit</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;Level 3:&nbsp;<i>Urgent</i></td>
                                       <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan dua atau lebih sumber daya</td>
                                       <td style="border: 1px solid black;">&nbsp;30 menit</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;Level 4:&nbsp;<i>Non urgent</i> </td>
                                       <td style="border: 1px solid black;">&nbsp;Pasien membutuhkan satu sumber daya</td>
                                       <td style="border: 1px solid black;">&nbsp;60 menit</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;Level 5:&nbsp;<i>False emergency</i> </td>
                                       <td style="border: 1px solid black;">&nbsp;Pasien tidak membutuhkan sumber daya</td>
                                       <td style="border: 1px solid black;">&nbsp;120 menit</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox">&nbsp;<i>Death on arrival</i> </td>
                                       <td style="border: 1px solid black;">&nbsp;Kasus meninggal</td>
                                       <td style="border: 1px solid black;">&nbsp;Tidak perlu</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="line-height : 1px;">
                              <div class="form-group">
                                 <h4 class="box-title"><b>A. SUBJECTIVE</b></h4>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label>&nbsp;Riwayat Penyakit Sekarang</label><br>
                                    <textarea class="form-control txt_asesmen" rows="4" name="riwayatpenyakitsekarang"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label>Riwayat Penyakit Dahulu</label><br>
                                    <input type="checkbox" class="chckboxrpd" name="chcktidakriwayapenyakitdahulu" id="tidakriwayapenyakitdahulu">
                                    &nbsp;Tidak ada<br>
                                    <input type="checkbox" class="chckboxrpd yardp" name="yariwayatpenaykitdahulu" id="yariwayatpenaykitdahulu">
                                    &nbsp;Ada,sebutkan :<br>
                                    <textarea class="form-control txt_asesmen txtrdp" rows="4" name="txtriwayatpenyakitdahulu"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label>Obat yang Rutin Dikonsumsi</label><br>
                                    <input type="checkbox" class="chckobyrd" id="chcktidakobyrd" name="chcktidakobyrd">
                                    &nbsp;Tidak ada<br>
                                    <input type="checkbox" class="chckobyrd" id="chckyaobyrd" name="chckyaobyrd">
                                    &nbsp;Ada, sebutkan : 
                                    <textarea name="txtobyrd" class="form-control txt_asesmen" rows="4"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label>Riwayat Alergi Obat dan Makanan</label><br>
                                    <input type="checkbox" class="form-check-input chckraodm" id="chcktidakraodm" name="chcktidakraodm">
                                    &nbsp;Tidak <br>
                                    <input type="checkbox" class="form-check-input chckraodm" id="chckyaraodm" name="chckyaraodm">
                                    &nbsp;Ya, sebutkan:
                                    <textarea class="form-control txt_asesmen" rows="4" name="txtraodm"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td style="line-height:1px;">
                              <div class="form-group">
                                 <h4><b>B. OBJECTIVE</b></h4>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td style="width : 14%">
                                          <label>Keadaan Umum</label>
                                       </td>
                                       <td>
                                          : <input type="text" style="width: 39%;" name="keadaanumum">
                                       </td>
                                       <td><label>Skala Nyeri</label></td>
                                       <td>: <input type="text" style="width: 30%;" name="skalanyeri"></td>
                                    <tr>
                                    <tr>
                                       <td>
                                          <label>GCS</label>
                                       </td>
                                       <td> :
                                          <label>E</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcse">
                                          <label>V</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsv">
                                          <label>M</label>&nbsp;&nbsp;<input type="text" style="width: 10%;" name="gcsm">
                                       </td>
                                    <tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label>Pemeriksaan Fisik</label>
                                    <textarea class="form-control" name="pemeriksaanfisik"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="col-md-8">
                                 <div class="form-group">
                                    <label for="">Pemeriksaan Penunjang Medik (Hasil yang abnormal)</label><br>
                                    <input type="checkbox" class="chckppm" id="chcktidakppm" name="chcktidakppm">&nbsp;&nbsp;Tidak ada<br>
                                    <input type="checkbox" class="chckppm" id="chckyakppm" name="chckyakppm">&nbsp;&nbsp;Ada, sebutkan :
                                    <textarea class="form-control txt_asesmen" rows="4" name="txtppm"></textarea>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="box box-default">
                                 <input class="cHidden" id="focusElektromedik" type="text">
                                 <div class="box-header ui-sortable-handle" style="cursor: move;">
                                    <h3 class="box-title"><b>ELEKTROMEDIK</b></h3>
                                 </div>
                                 <!-- /.box-header -->
   <!-- <div class="box-body">
               <div class="form-group"> -->
   <!--jika mode periksa bisa menambahkan radiologi-->
   <!-- <?php if ($mode == 'periksa') { ?> -->
   <!-- <div class="col-md-4">
               <select class="select2 col-md-3" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                  <option value="0">Pilih Tindakan Radiologi</option>
               </select>
               </div>
               <?php } ?>
               <div class="col-md-11">
               <table class="table" style="width: 97%">
                  <thead>
                     <tr class="header-table-ql">
                        <th>ICD</th>
                        <th>Nama ICD</th>
                        <th width="23%">Dokter Penerima JM</th>
                        <th width="5%">Jumlah</th>
                        <th>Biaya</th>
                        <th>Potongan</th>
                        <th width="19%">Aksi</th>
                     </tr>
                  </thead>
                  <tbody id="listRadiologi">
                  </tbody>
               </table>
               </div>
               </div> -->
   <!-- <div class="form-group">
               <div class="col-sm-7">
                  <label>Hasil Expertise</label>
                  <textarea class="form-control textarea" name="keteranganradiologi" rows="25" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
                  <label>Saran</label>
                  <textarea class="form-control textarea" name="saranradiologi" rows="12" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled') ?>></textarea>
               </div>
               <div class="col-sm-5">
                  <label>Hasil Radiografi</label>
                  <table class="table" style="width: 92%; background-color: #eeeeee;">
                     <tbody>
                        <tr>
                           <td colspan="2" id="viewHasilRadiografi">
                              <!--tampil dari js-->
   <!-- </td>
               </tr> -->
   <?php if ($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI)) { ?>
      <!-- <tr>
               <td colspan="2" style="padding-top:15px;padding-left:20px;">
                  <a id="btnRadiologiEfilm" class="btn btn-md btn-primary">Unggah hasil <i class="fa fa-upload"></i></a>
               </td> -->
      <!-- </tr>  -->
   <?php } ?>
   <!-- </tbody>
               </table>
               </div>
               </div>
               </div>
               </div>
               </td>
               </tr>
               <tr> -->
   <!-- <td> -->
   <!-- ` -->
   <!-- <div class="box box-default">
               <div class="box-header ui-sortable-handle" style="cursor: move;">
                  <h3 class="box-title"><b>LABORATORIUM</b></h3>
               </div> -->
   <!-- /.box-header -->
   <!-- <div class="box-body">
               <div class="col-sm-7">
                  <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTKETERANGANLABORATORIUM)) ?: disabled) ?>></textarea>
               </div>
               <div class="col-sm-4 row"> -->
   <!--jika mode periksa dapat menambahkan laboratorium-->
   <?php if ($mode == "periksa") { ?>
      <!-- <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)">
               <option value="0">Pilih Tindakan Laboratorium</option>
               </select>
               <br>
               <br> -->
      <!-- <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
               <option value="0">Pilih Paket Laboratorium</option> -->
      <?php
      if (!empty($data_paket)) {
         foreach ($data_paket as $data) {
            // echo "<option value=" . $data->idpaketpemeriksaan . ">" . $data->namapaketpemeriksaan . "</option>";
         }
      }
      ?>
      <!-- </select> -->
   <?php } ?>
   <!-- </div> -->
   <input class="cHidden" id="focusLaboratorium" type="text">
   <!-- <table class="table" style="width: 95%">
               <thead>
                  <tr class="header-table-ql">
                     <th>Parameter</th>
                     <th>Hasil</th>
                     <th>NDefault</th>
                     <th>NRujukan</th>
                     <th>Satuan</th>
                     <th>Biaya</th>
                     <th>Potongan</th>
                     <th width="12%">Aksi</th>
                  </tr>
               </thead>
               <tbody id="listLaboratorium">
               </tbody>
               </table>
               </div>
               </div>
               `
               </td>
               </tr>
               <tr> -->
   <!-- <td style="line-height : 1px;">
               <div class="form-group">
                  <h4 class="box-title"><b>C. ASSESSMENT (ICD 10)</b></h4>
               </div>
               </td>
               </tr>
               <td>
               <div class="col-md-4"> -->
   <!-- <label>Diagnosa Primer</label>
               <select class="ql-select2 select2 form-control" id="diagnosaprimer" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,primer)">
                 <option value="0">Input Diagnosa</option>
               </select>
               </div>
               <div class="col-md-4">
               <label>Diagnosa Sekunder</label>
               <select class="ql-select2 select2 form-control" id="diagnosasekunder" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,sekunder)">
                 <option value="0">Input Diagnosa</option>
               </select>
               </div>
               <div class="col-md-8" style="margin-top:8px;">
               <label>Diagnosa Diluar ICD 10</label>
               <textarea class="form-control" name="diagnosa" id="diagnosadilauricd10" rows="2"></textarea>
               </div>
               <div class="col-md-12" style="margin-top:4px;">
               <table class="table">
                 <thead>
                    <tr class="header-table-ql">
                       <th>ICD</th>
                       <th>Nama ICD</th>
                       <th>Level</th>
                       <th>Alias</th>
                       <th width="8%">Aksi</th>
                    </tr>
                 </thead>
                 <tbody class="listDiagnosa10"></tbody>
               </table>
               </div>
               </td>
               `
               </tr>
               <tr>
               <td style="line-height:1px;">
               <div class="form-group">
                 <h4><b>D. PLAN</b></h4>
               </div> -->
   <!-- </td>
               </tr>
               <tr>
                  <td>
                     <div class="col-md-8">
                        <div class="form-group">
                           <label>Terapi</label><br>
                           <input type="text" id="txtterapi" class="form-control" style="width:100%;" name="txtterapi">
                        </div>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td>
                     <div class="col-md-4">
                        <label>Tanggal Selesai As.Awal :</label><input type="date" id="tglselesaiasesmen" name="tglselesaiasesmen" readonly>
                     </div>
                     <div class="col-md-4">
                        <label>Jam Pasien As.Awal : <input type="text" name="txtselesaiasesmenawal" id="txtselesaiasesmenawal" readonly> WIB</label>
                     </div>
                     <div class="col-md-4">
                        <label>Nama Dokter :</label><input type="text" value="" id="namadokter" readonly>
                        <input type="hidden" name="idpdokter" id="idpersondokter" readonly>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td>
                     <div class="col-md-8">
                        <label>Catatan Lainnya</label><br>
                        <input type="checkbox" class="form-check-input chckcl" id="chcktdkcatatanlainnya" name="chcktdkcatatanlainnya">
                        &nbsp;Tidak <br>
                        <input type="checkbox" class="form-check-input chckcl" id="chckyacatatanlainnya" name="chckyacatatanlainnya">
                        &nbsp;Ya, sebutkan:<br>
                        <textarea class="form-control" name="txtcatatanlainnya"></textarea>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td>
                     <div class="col-md-8">
                        <label>Observasi Pasien di UGD</label><br>
                        <input type="checkbox" class="form-check-input chckcl" id="chcktdkcatatanlainnya" name="chcktdkcatatanlainnya">
                        &nbsp;Tidak ada <br>
                        <input type="checkbox" class="form-check-input chckcl" id="chckyacatatanlainnya" name="chckyacatatanlainnya">
                        &nbsp;Ada, sebutkan:<br>
                        <textarea class="form-control" name="txtcatatanlainnya"></textarea>
                     </div>
                  </td>
               </tr>
               <tr>
                  <td>
                     <table>
                        <tbody>
                           <tr>
                              <td>
                                 <div class="col-md-8"><strong>Rencana Perawatan dan Tindakan Selanjutnya</strong></div>
                              </td>
                           </tr>
                           <tr>
                              <td style="width : 10%;">
                                 <div class="col-md-12">
                                    &nbsp;<input type="checkbox">&nbsp;Rawat jalan
                                    <table>
                                       <tbody>
                                          <tr>
                                             <td style="width : 15%;">&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Kontrol di : <br><br><br><br></td>
                                             <td colspan="2">
                                                <table>
                                                   <tbody>
                                                      <tr>
                                                         <td><input type="checkbox">&nbsp;UGD, tanggal&nbsp; <input type="date"></td>
                                                      </tr>
                                                      <tr>
                                                         <td><input type="checkbox">&nbsp;Poliklinik&nbsp;<input type="text">&nbsp;tanggal&nbsp;<input type="date"></td>
                                                      </tr>
                                                      <tr>
                                                         <td><input type="checkbox">&nbsp;PPK 1</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td colspan="2">
                                                &nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;Kontrol bila ada keluhan :
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <div class="col-md-12"><input type="checkbox">&nbsp;Observasi lanjutan di UGD</div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <div class="col-md-12"><input type="checkbox">&nbsp;Rawat inap, bangsal <input type="text"></div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <div class="col-md-12"><input type="checkbox">&nbsp;Rujuk</div>
                              </td>
                           </tr>
                           <tr>
                              <td>
                                 <div class="col-md-12"><input type="checkbox">&nbsp;Menolak tindakan/rawat inap</div>
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </td>
               </tr> -->
   <!-- <tr>
               <td>
                  <div class="col-md-3">
                     <table>
                        <tbody>
                           <tr>
                              <td><strong>Kondisi Pasien sebelum Meninggalkan UGD</strong></td>
                           </tr>
                           <tr>
                              <td><input type="checkbox">&nbsp;Stabil, kondisi membaik</td>
                           </tr>
                           <tr>
                              <td><input type="checkbox">&nbsp;Tidak ada perbaikan kondisi</td>
                           </tr>
                           <tr>
                              <td><input type="checkbox">&nbsp;Memburuk</td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!-- <div class="col-md-3">
                     <br> -->
   <!-- <table>
               <tbody>
                  <tr>
                     <td>
                        <input type="checkbox">&nbsp;Meninggal dunia <br>
                        <table>
                           <tbody>
                              <tr>
                                 <td>Tanggal</td>
                                 <td><input type="text"></td>
                              </tr>
                              <tr>
                                 <td>Jam</td>
                                 <td><input type="text"></td>
                              </tr>
                           </tbody>
                        </table>
                     </td>
                  </tr>
               </tbody>
               </table>
               </div>
               </td>
               </tr>
               <tr>
               <td>
               <div class="form-group">
               <label for="">TINDAKAN</label><br>
               <div class="col-md-6">
               <select class="form-control select2" name="caritindakan">
               </select>
               </div>
               </div>
               <table class="table table-bordered">
               <!-- <thead> -->
   <!-- <tr class="header-table-ql"> -->
   <!-- <th> ICD </th> -->
   <!-- <th> Nama ICD </th> -->
   <!-- <th> Dokter Penerima JM </th> -->
   <!-- <th> Jumlah </th> -->
   <!-- <th> Biaya </th> -->
   <!-- <th> Potongan </th> -->
   <!-- <th> Aksi </th> 
               <!-- </tr> -->
   <!-- </thead> 
               <!-- <tbody class="listTindakanAsesmen"></tbody> -->
   <!-- </table> -->
   <!-- <div class="col-md-12 row" style="margin-top:10px;"> -->
   <!-- <div class="col-xs-12 col-md-3 ">
               <label>Kondisi Keluar</label>
               <select class="form-control" id="kondisikeluar"  name="kondisikeluar" style="width:100%;" ></select>
               </div>
               </div>
               <div class="col-md-12 row" style="margin-top:10px;margin-bottom: 10px;">
               <div class="col-xs-12 col-md-3">                  
               <label>Status Pasien Pulang</label>
               <select class="form-control" id="statuspasienpulang"  name="statuspasienpulang" style="width:100%;"></select>
               </div>
               </div> -->
   <!-- </td>
               </tr>
               <tr>
               <tr>
               <td><label>OBAT/BHP</td>
               </tr>
               <td>
               <div class="col-sm-5">
               <span class="label label-default">Keterangan Obat/Bhp</span>
               <textarea class="form-control" id="keteranganobat" rows="9" name="keteranganobat"></textarea>
               </div>
               <div class="col-sm-5">
               <label>&nbsp;</label>
               <select class="ql-select2 form-control" name="caribhp" style="width:100%;"></select>
               <small class="text text-red">#stok yang tertera stok gudang.</small>
               <br><br>
               <span style="margin-right:20px;"><input id="adaracikan" name="adaracikan" type="checkbox" class=""> Ada Racikan</span>
               <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
               <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
               <br>
               <div class="pasien-prolanis <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS)) ? ''  : 'hide'); ?>">
               <br>
               <input id="prolanis" name="prolanis" type="checkbox" /> PROLANIS
               <br> -->
   <!-- <span class="label label-default">Tanggal Kunjungan Berikutnya</span>
               <div class="input-group date col-md-6">
               <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
               <input type="text" readonly="readonly" placeholder="input tanggal..." class="form-control pull-right" id="kunjunganberikutnya" name="kunjunganberikutnya">
               </div>
               </div>
               </div>
               </td>
               </tr>
               <tr>
               <td>
               <table class="table table-bordered">
               <thead>
               <tr class="header-table-ql">
               <th>No</th>
               <th>Obat/BHP[ExdDate]</th>
               <th>Resep<sup>1)</sup></th>
               <th>Kekuatan</th>
               <th>DosisRacik<sup>6)</sup></th>
               <th>GrupRacik<sup>6)</sup></th>
               <th>Diresepkan</th>
               <th>Tot.Harga</th>
               <th>Tot.Diberikan<sup>4)</sup></th>
               <th>Diberikan</th>
               <th>Penggunaan</th>
               <th colspan="2">AturanPakai</th>
               </tr>
               </thead>
               <tbody class="viewBhpRalan"></tbody>
               </table> -->
   <!-- </td>
               </tr>
               <tr>
               </tr>
               <tr>
               <td>
               <div class="col-md-4">
               <label>Tanggal Pasien Pulang :</label><input type="date" id="tglselesaiasesmen" name="tglselesaiasesmen" readonly>
               </div>
               <div class="col-md-4">
               <label>Jam Pasien Pulang : <input type="time" name="txtselesaiasesmenawal" id="txtselesaiasesmenawal" readonly> WIB</label>
               </div>
               <div class="col-md-4">
               <label>Nama Dokter :</label><input type="text" value="" id="namadokter" readonly>
               <input type="hidden" name="idpdokter" id="idpersondokter" readonly>
               </div>
               </td>
               </tr>
               </tbody>
               </table>
               </form>
               </div> 
               <!-- End of asesment awal UGD -->
   <!-- awal asemenpengkajianwalkeperawatangawatdarurat  -->
   <!-- <div class="asemenpengkajianwalkeperawatangawatdarurat">
               <form id="formasesmenawalkeperawatan">
                  <input type="hidden" name="idperiksa" />
                  <input type="hidden" name="idpendaftaran" />
                  <table class="table table-bordered" style="width:100%">
                     <tbody>
                        <tr>
                           <td colspan="3">
                              <table style="width : 75%;">
                                 <tbody>
                                    <tr>
                                       <td class="text-center"><strong>PENGKAJIAN AWAL KEPERAWATAN GAWAT DARURAT</strong></td>
                                    </tr>
                                 </tbody>             
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label for="">Tanggal Pasien Periksa:</label>
                                       <label class="tglperiksa"></label>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label for="">Jam Pasien Periksa </label>
                                    <label class="waktupasienperiksa"></label>
                                 </div>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td style="line-height:1px;">
                              <div class="form-group">
                                 <label for="">A. SUBJECTIVE</label>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <div class="form-group">
                                 <label>Keluhan Utama</label>
                                 <textarea class="form-control" style="width : 50%" name="txtkeluhanutama"></textarea>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <p>Apakah pasien dengan keluhan tentang kebidanan dan kandungan?</p>
                              <input type="checkbox" class="chckapdktkdk" name="chckapdktkdktidak">&nbsp;Tidak<br><input type="checkbox" class="chckapdktkdk" name="chckapdktkdkya">&nbsp;Ya
                           </td>
                        </tr>
                        <tr class="asesmenobsyetridanginekologi">
                           <td>
                              <div class="form-group">
                                 <label>Asesmen Obstetri dan Ginekologi</label>
                              </div>
                              <table>
                                 <tbody>
                                    <tr>
                                       <td>
                                          <label>Riwayat Kehamilan Sekarang</label>
                                       </td>
                                       <td>
                                          <div class="form-group">
                                             : G<input type="text" style="width: 5%;" name="rksg"> P<input type="text" style="width: 5%;" name="rksp"> A<input type="text" style="width: 5%;" name="rksga"> Ah<input type="text" style="width: 5%;" name="rksah">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HPHT <input type="text" name="rksghpht"> HPL <input type="text" name="rksghpl"> UK <input type="text" name="rksguk">
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <label>ANC Terpadu</label> <br><br>
                                       </td>
                                       <td>
                                          <div class="form-group">
                                             : <input type="checkbox" class="chckancterpadu" name="chckanctidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" name="chckanciya" class="chckancterpadu">&nbsp;&nbsp;Ya,&nbsp; di <input type="text" name="txtanc">
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <label>Imunisasi TT</label> <br><br>
                                       </td>
                                       <td>
                                          <div class="form-group">
                                             : <input type="checkbox" class="chckitt" name="chcktdkitt">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckitt" name="chckyaitt">&nbsp;&nbsp;Ya,&nbsp; TT ke <input type="text" name="txtitt">
                                          </div>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>
                                          <label>Pendarahan pervaginam</label> <br><br>
                                       </td>
                                       <td>
                                          <div class="form-group">
                                             : <input type="checkbox" class="chckppvm" name="chcktdkppvm">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp; <input type="checkbox" class="chckppvm" name="chckyappvm">&nbsp;&nbsp;Ya,&nbsp;<input type="text" name="txtppvm">&nbsp;ml
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td style="line-height: 1px;">
                              <div class="form-group"><label>B. Objective</label></div>
                           </td>
                        </tr>
                        <tr>
                           <td>
                              <table class="">
                                 <tbody>
                                    <tr>
                                       <td>
                                          <table class="table table-bordered">
                                             <tbody>
                                                <tr>
                                                   <td style="width:100px">
                                                      <div class="form-group">
                                                         <label><strong>Airway</strong></label>
                                                      </div>
                                                   </td>
                                                   <td style="width:100px">
                                                      <input type="checkbox" class="chckairway" name="chckairway" value="Bersih" id="chckairwayBersih">&nbsp;Bersih<br><input type="checkbox" class="chckairway" name="chckairway" id="chckairwayDarah" value="Darah">&nbsp;Darah
                                                   </td>
                                                   <td style="width:16%">
                                                      <input type="checkbox" class="chckairway" name="chckairway" value="Sumbatan" id="chckairwaySumbatan">&nbsp;Sumbatan<br><input type="checkbox" class="chckairway" name="chckairway" value="Lidah jatuh" id="chckairwayLidahjatuh">&nbsp;Lidah jatuh
                                                   </td>
                                                   <td style="width:12%;" colspan="1">
                                                      <input type="checkbox" class="chckairway" name="chckairway" value="Sputum" id="chckairwaySputum">&nbsp;Sputum<br><input type="checkbox" class="chckairway" name="chckairway" value="Benda asing" id="chckairwayBendaAsing">&nbsp;Benda asing
                                                   </td>
                                                   <td style="width:100px">
                                                      <input type="checkbox" class="chckairway" name="chckairway" value="Lendir" id="chckairwayLendir">&nbsp;Lendir<br>
                                                   </td>
                                                   <td style="width:100px">
                                                      <input type="checkbox" class="chckairway" name="chckairway" value="Ludah" id="chckairwayLudah">&nbsp;Ludah<br>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <div class="form-group">
                                                         <label><strong>Breathing</strong></label><br>Pernafasan
                                                      </div>
                                                   </td>
                                                   <td>
                                                      <div class="form-group">
                                                         <input type="checkbox" class="chckbre" name="chckbre" value="Tidak sesak" id="chckbretidaksesak">&nbsp;Tidak sesak
                                                      </div>
                                                   </td>
                                                   <td>
                                                      <div class="form-group">
                                                         <input type="checkbox" class="chckbre" name="chckbresesak" id="chckbresesak">&nbsp;Sesak:<br>
                                                         &nbsp;&nbsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Saat Aktivitas" id="chckbresaataktivitas">&nbsp;Saat Aktivitas<br>
                                                         &nbsp;&nbsp;<input type="checkbox" class="chcksesak" name="chckbre" value="Sesak Tanpa Aktivitas" id="chckbretanpaaktivitas">&nbsp;Tanpa aktivitas
                                                      </div>
                                                   </td>
                                                   <td colspan="2">
                                                      <div class="form-group">
                                                         <input type="checkbox" class="chckbre" name="chckbre" value="Tidak ada nafas" id="chckbretidakadanafas">&nbsp;Tidak ada nafas
                                                      </div>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>Suara nafas</p>
                                                   </td>
                                                   <td style="width:120px">
                                                      <input type="checkbox" name="chcksuana" class="chcksuana" value="Vesikuler" id="chcksuanavesikuler">&nbsp;Vesikuler
                                                   </td>
                                                   <td>
                                                      <input type="checkbox" class="chcksuana" name="chcksuana" value="Ronkhi" id="chcksuanaronkhi">&nbsp;Ronkhi
                                                   </td>
                                                   <td>
                                                      <input type="checkbox" class="chcksuana" name="chcksuana" value="Crakles" id="chcksuanacrakles">&nbsp;Crakles
                                                   </td>
                                                   <td>
                                                      <input type="checkbox" name="chcksuana" class="chcksuana" value="Wheezing" id="chcksuanawheezing">&nbsp;Wheezing
                                                   </td>
                                                   <td>
                                                      <input type="checkbox" name="chcksuana" class="chcksuana" value="Stridor" id="chcksuanastridor">&nbsp;Stridor
                                                   </td>
                                                   <td style="width : 25%;">
                                                      <input type="checkbox" class="chcksuana" name="chcksuana" value="Tidak ada suara nafas" id="chcksuanatidakadasuaranafas">&nbsp;Tidak ada suara nafas
                                                   </td>
                                                   <td></td>
                                                </tr>
                                                <tr>
                                                   <td colspan="2">
                                                      <strong>Circulation</strong>
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>Pulsasi perifer</p>
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckpulper" name="chckpulperteraba">&nbsp;Teraba
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckpulper" name="chckpulpertidakteraba">&nbsp;Tidak teraba
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>Pulsasi karotis</p>
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckpulsasikarotis" name="chckpulsasikarotisteraba">&nbsp;Teraba
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckpulsasikarotis" name="chckpulsasikarotistidakteraba">&nbsp;Tidak teraba
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>Warna kulit</p>
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckwarnakulit" name="chckwarnakulitkemerahan">&nbsp;Kemerahan
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckwarnakulit" name="chckwarnakulitpucat">&nbsp;Pucat
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>Akral</p>
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckakral" name="chckakralhangat">&nbsp;Hangat
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckakral" name="chckakraldingin">&nbsp;Dingin
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      <p>CRT</p>
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckcrt" name="chckcrtkurang2dtk">&nbsp;<2 dtk 
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckcrt" name="chckcrtlebih2dtk">&nbsp;>2 dtk
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td>
                                                      Kesadaran
                                                   </td>
                                                   <td style="width:14%;">
                                                      <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadarancomposmentis" value="Compos mentis">&nbsp;Compos mentis
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaranapatis" value="Apatis">&nbsp;Apatis
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaransomnolen" value="Somnolen">&nbsp;Somnolen
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaransupor" value="Supor">&nbsp;Supor
                                                   </td>
                                                   <td style="width:100px;">
                                                      <input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadarancoma" value="Coma">&nbsp;Coma
                                                   </td>
                                                </tr>
                                                <tr>
                                                   <td colspan="3"><strong>Pengkajian Nyeri</strong></td>
                                                </tr>
                                                <tr>
                                                   <td colspan="7" width="100%"><input type="checkbox" name="chckppnnips" class="chckppn">&nbsp;<i>NIPS (Neonatal Infant Pain Score)</i> (Digunakan pada pasien bayi usia 0-30 hari)</td>
                                                </tr>
                                                <tr>
                                                   <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnflacc">&nbsp;<i>FLACC Behavioral Pain Scale</i> (Digunakan pada pasien bayi usia >30 hari sampai dengan usia <3 tahun, dan anak dengan gangguan kognitif)</td>
                                                </tr>
                                                <tr>
                                                   <td colspan="7"><input type="checkbox" class="chckppn" name="chckppnwongbaker">&nbsp;<i>Wong Baker Faces Pain Scale</i> (Digunakan pada pasien anak usia &#8805; 3 tahun dan dewasa)</td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr id="tblnips">
                           <td>
                              <table class="table table-bordered" style="border: 1px solid black;">
                                 <thead style="border: 1px solid black;">
                                    <tr>
                                       <th class="text-center" style="border: 1px solid black;" colspan="3"><label><i>NIPS</i>&nbsp;&nbsp;<i>(Neonatal Infant Pain Score)</i></label>
                                    </tr>
                                    <tr>
                                       <th style="border: 1px solid black; width:300px;" class="text-center">Pengkajian</th>
                                       <th style="border: 1px solid black;" class="text-center">Parameter</th>
                                       <th style="border: 1px solid black;" class="text-center">Skor</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr style="border: 1px solid black;">
                                       <td rowspan="2" style="border: 1px solid black;" class="text-center">Ekspresi Wajah</td>
                                       <td style="border: 1px solid black;"><input type="checkbox" name="chckekw" class="chckekw" id="chckekw0" value="0">&nbsp;Otot wajah rileks, ekspresi netral</td>
                                       <td style="border: 1px solid black;" class="text-center">0</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chckekw" name="chckekw" id="chckekw1" value="1">&nbsp;Otot wajah tegang, alis berkerut rahang, dan dagu mengunci</td>
                                       <td style="border: 1px solid black;" class="text-center">1</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td rowspan="4" style="border: 1px solid black;" class="text-center">Tangisan</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan0" value="0">&nbsp;Tenang, tidak menangis</td>
                                       <td style="border: 1px solid black;" class="text-center">0</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan1" value="1">&nbsp;Mengerang lemah, intermitten</td>
                                       <td style="border: 1px solid black;" class="text-center">1</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" name="chcktangisan" class="chcktangisan" id="chcktangisan2" value="2">&nbsp;Menangis kencang, melengking dan terus-menerus</td>
                                       <td style="border: 1px solid black;" class="text-center">2</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;" colspan="2" class="text-center">(Catatan : menangis tanpa suara diberi skor bila bayi diintubasi)</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td rowspan="2" style="border: 1px solid black;" class="text-center">Pola pernafasan</td>
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna0" value="0">&nbsp;Bernapas biasa</td>
                                       <td style="border: 1px solid black;" class="text-center">0</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chckpolaperna" name="chckpolaperna" id="chckpolaperna1" value="1">&nbsp;Pola nafas berubah: tarikan ireguler, lebih cepat dibanding biasa, menahan napas, tersedak</td>
                                       <td style="border: 1px solid black;" class="text-center">1</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td rowspan="2" style="border: 1px solid black;" class="text-center">Tangan</td>
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan0" value="0">&nbsp;Rileks, tidak ada kekakuan otot, gerakan tungkai biasa</td>
                                       <td style="border: 1px solid black;" class="text-center">0</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chcktangan" name="chcktangan" id="chcktangan1" value="1">&nbsp;Tegang, kaku</td>
                                       <td style="border: 1px solid black;" class="text-center">1</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td rowspan="2" style="border: 1px solid black;" class="text-center">Kesadaran</td>
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaran0" value="0">&nbsp;Rileks, tenang, tidur lelap atau bangun</td>
                                       <td style="border: 1px solid black;" class="text-center">0</td>
                                    </tr>
                                    <tr style="border: 1px solid black;">
                                       <td style="border: 1px solid black;"><input type="checkbox" class="chckkesadaran" name="chckkesadaran" id="chckkesadaran1" value="1">&nbsp;Sadar, atau gelisah</td>
                                       <td style="border: 1px solid black;" class="text-center">1</td>
                                    </tr>
                                    <tr style="border: 1px solid black;" class="text-center">
                                       <td style="border: 1px solid black;line-height: 12px;" colspan="2" class="text-center"><strong>Total Skor</strong></td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" name="totalskornips" value="0" style="width: 20%;">
                                          <input type="hidden" name="nipsekspresiwajah" class="nipsscore text-center" value="0">
                                          <input type="hidden" name="nipstangisan" class="nipsscore" class="nipsscore" value="0">
                                          <input type="hidden" name="nipspolapernafasan" class="nipsscore" value="0">
                                          <input type="hidden" name="nipstangan" class="nipsscore" value="0">
                                          <input type="hidden" name="nipskesadaran" class="nipsscore" value="0">
                                       </td>
                                    </tr>
                                    <tr style="line-height: 10px;">
                                       <td style="" colspan="4"><strong>Interpretasi hasil:</strong></td>
                                    </tr>
                                    <tr style="">
                                       <td style="" colspan="4">
                                          <input type="checkbox" id="chckbayi0" name="chckbayitidakmengalaminyeri" onclick="return false;" />&nbsp;< 3 :Bayi tidak mengalami nyeri<br>
                                          <input type="checkbox" id="chckbayi1" name="chckbayimengalaminyeri" onclick="return false;" />&nbsp;> 3 :Bayi mengalami nyeri
                                       </td>
                                    </tr>
                              </table>
                           </td>
                        </tr>
                        <tr id="tbflacc">
                           <td>
                              <table style="width : 100%;">
                                 <thead>
                                    <tr style="border: 1px solid black">
                                       <th class="text-center" colspan="5"><label><i>FLACC Behavioral Pain Scale</i></label></th>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <th class="text-center" style="border : 1px solid black;width:100px;">Pengkajian</th>
                                       <th class="text-center" style="border : 1px solid black">Nilai 0</th>
                                       <th class="text-center" style="border : 1px solid black">Nilai 1</th>
                                       <th class="text-center" style="border : 1px solid black">Nilai 2</th>
                                       <th class="text-center" style="border : 1px solid black">Skor</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center">
                                          <p>Wajah</p>
                                       </td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki0" value="0">&nbsp;Tersenyum / tidak ada ekspresi</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki1" value="1">&nbsp;Terkadang merintih / menarik diri</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki2" value="2">&nbsp;Sering menggetarkan dagu dan menggatupkan rahang</td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflacckaki" value="0" readonly></td>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center">
                                          <p>Kaki</p>
                                       </td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki0" value="0">&nbsp;Normal / relaksasi</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki1" value="1">&nbsp;Tidak tenang / tegang</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckflacckaki" name="chckflacckaki" id="chckflacckaki2" value="2">&nbsp;Kaki menendang/menarik diri</td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflacckaki" value="0" readonly></td>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center">Aktivitas</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian0" value="0">&nbsp;Tidur posisi normal / relaksasi</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian1" value="1">&nbsp;Gerakan menggeliat / bergeling kaku</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckaktivitaspengkajian" name="chckaktivitaspengkajian" id="chckaktivitaspengkajian2" value="2">&nbsp;Melengkungkan punggung / kaku / membentak</td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccaktivitas" value="0" readonly></td>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center">Menangis</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis0" value="0">&nbsp;Tidak menangis</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis1" value="1">&nbsp;Mengerang / merengek</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianmenangis" name="chckpengkajianmenangis" id="chckpengkajianmenangis2" value="2">&nbsp;Menangis terus menerus / terisak / menjerit</td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccmenangis" value="0" readonly></td>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center">Bersuara</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara0" value="0">&nbsp;Bersuara normal / tenang</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara1" value="1">&nbsp;Tenang bila dipeluk / digendong / diajak bicara</td>
                                       <td style="border: 1px solid black;">&nbsp;<input type="checkbox" class="chckpengkajianbersuara" name="chckpengkajianbersuara" id="chckpengkajianbersuara2" value="2">&nbsp;Sulit untuk menenangkan</td>
                                       <td style="border: 1px solid black;" class="text-center"><input type="text" style="width: 20%;" name="totalflaccbersuara" value="0" readonly></td>
                                    </tr>
                                    <tr style="border: 1px solid black">
                                       <td class="text-center" style="border: 1px solid black" colspan="4"><label><strong>Total Skor</strong></label></td>
                                       <td class="text-center">
                                          <input type="text" name="totalskorflacc" value="0" style="width : 20%;">
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <table style="width: 50%;">
                                 <tbody>
                                    <tr>
                                       <td colspan="4">Interprestasi hasil</td>
                                    </tr>
                                    <tr>
                                       <td style="width:10%;"><input type="checkbox" class="chckinteprestrasihasilflacc0" name="chckinteprestrasihasilflacc" id="chckflaccdibawahnol" value="0" onclick="return false;">&nbsp;0</td>
                                       <td>:&nbsp;Nyaman</td>
                                       <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaraempatsampaienam" value="2" class="chckinteprestrasihasilflacc2" onclick="return false;">&nbsp;4-6</td>
                                       <td>:&nbsp;Nyeri sedang</td>
                                    </tr>
                                    <tr>
                                       <td style="width:10%;"><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantarasatusampaitiga" class="chckinteprestrasihasilflacc1" value="1" onclick="return false;">&nbsp;1-3</td>
                                       <td>:&nbsp;Nyeri Ringan</td>
                                       <td><input type="checkbox" name="chckinteprestrasihasilflacc" id="chckflaccdiantaratujuhsampaisepuluh" class="chckinteprestrasihasilflacc3" value="3" onclick="return false;">&nbsp;7-10</td>
                                       <td>:&nbsp;Nyeri berat</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <tr id="tbwongbaker">
                           <td>
                              <table style="width : 100%">
                                 <tbody>
                                    <tr>
                                       <td class="text-center">
                                          <img src="<?= base_url() ?>assets/images/skalanyeri/nohurt.png">
                                       </td>
                                       <td class="text-center">
                                          <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/hurtslittlebit.png">
                                       </td>
                                       <td class="text-center">
                                          <img style="padding : 12%" src="<?= base_url() ?>assets/images/skalanyeri/hurtslittlemore.png">
                                       </td>
                                       <td class="text-center">
                                          <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtsevenmore.png">
                                       </td>
                                       <td class="text-center">
                                          <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtswholelot.png">
                                       </td>
                                       <td class="text-center">
                                          <img style="padding : 12%" src=" <?= base_url() ?>assets/images/skalanyeri/hurtsworst.png">
                                       </td>
                                    </tr>
                                    <tr>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker0" value="0"></td>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker2" value="2"></td>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker4" value="4"></td>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker6" value="6"></td>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker8" value="8"></td>
                                       <td class="text-center"><input type="checkbox" class="chckwongbaker" name="chckwongbaker" id="chckwongbaker10" value="10"></td>
                                    </tr>
                                 </tbody>
                              </table>
                              <br>
                              <table style="width : 100%">
                                 <tbody>
                                    <tr>
                                       <td>Faktor yang memperberat</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Cahaya</td>
                                       <td><input type="checkbox">&nbsp;Gerakan</td>
                                       <td><input type="checkbox">&nbsp;Suara</td>
                                       <td><input type="checkbox">&nbsp;Lainnya</td>
                                    </tr>
                                    <tr>
                                       <td>Faktor yang meringankan</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Istirahat</td>
                                       <td><input type="checkbox">&nbsp;Dipijat</td>
                                       <td><input type="checkbox">&nbsp;Minum obat</td>
                                       <td><input type="checkbox">&nbsp;Lainnya</td>
                                    </tr>
                                    <tr>
                                       <td>Kualitas nyeri</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Ditusuk</td>
                                       <td><input type="checkbox">&nbsp;Terikat</td>
                                       <td><input type="checkbox">&nbsp;Tumpul</td>
                                       <td><input type="checkbox">&nbsp;Berdenyut</td>
                                       <td><input type="checkbox">&nbsp;Lainnya&nbsp;<input type="text"></td>
                                    </tr>
                                    <tr>
                                       <td>Lokasi nyeri</td>
                                       <td>:</td>
                                       <td colspan="3"><input type="text"></td>
                                    </tr>
                                    <tr>
                                       <td>Menjalar</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Tidak</td>
                                       <td colspan="3"><input type="checkbox">&nbsp;Ya, jelaskan <input type="text"></td>
                                    </tr>
                                    <tr>
                                       <td>Skala nyeri</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Ringan</td>
                                       <td><input type="checkbox">&nbsp;Sedang</td>
                                       <td><input type="checkbox">&nbsp;Berat</td>
                                       <td><input type="checkbox">&nbsp;Tidak nyeri</td>
                                    </tr>
                                    <tr>
                                       <td>Lamanya nyeri</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;< 30 menit</td>
                                       <td colspan="3"><input type="checkbox">&nbsp;> 30 menit</td>
                                    </tr>
                                    <tr>
                                       <td>Frekuensi nyeri</td>
                                       <td>:</td>
                                       <td><input type="checkbox">&nbsp;Terus menerus</td>
                                       <td><input type="checkbox">&nbsp;Hilang timbul</td>
                                       <td><input type="checkbox">&nbsp;Jarang</td>
                                    </tr>
                                 </tbody>
               
                              </table>
                              <br>
                              <table style="width: 50%;">
                                 <tbody>
                                    <tr>
                                       <td colspan="4">Interprestasi hasil:</td>
                                    </tr>
                                    <tr>
                                       <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" class="chckwongbakerinterpretasi0" id="chckwongbakernol" value="0" onclick="return false;">&nbsp;0</td>
                                       <td>:&nbsp;Nyaman</td>
                                       <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakerempatsampaienam" class="chckwongbakerinterpretasi2" value="2" onclick="return false;">&nbsp;4-6</td>
                                       <td>:&nbsp;Nyeri sedang</td>
                                    </tr>
                                    <tr>
                                       <td style="width:10%;"><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakersatusampaitiga" class="chckwongbakerinterpretasi1" value="1" onclick="return false;">&nbsp;1-3</td>
                                       <td>:&nbsp;Nyeri Ringan</td>
                                       <td><input type="checkbox" name="chckinterpretasiwbfps" id="chckwongbakertujuhsampaisepuluh" class="chckwongbakerinterpretasi3" value="3" onclick="return false;">&nbsp;7-10</td>
                                       <td>:&nbsp;Nyeri berat</td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr>
                        <!-- <tr class="trwongbakesfacespainscaleinterpretasi">
                           <table style="width: 70%;">
                             <tbody>
                                 <tr>
                                   <td>Faktor yang memperberat</td>
                                   <td>:&nbsp;<input type="checkbox" name="chckfymcahaya" class="chckfym">&nbsp;Cahaya</td>
                                   <td><input type="checkbox" name="chckfymgerakan" class="chckfym">&nbsp;Gerakan</td>
                                   <td><input type="checkbox" name="chckfymsuara" class="chckfym">&nbsp;Suara</td>
                                   <td><input type="checkbox" name="chckfymlainnya" class="chckfym">&nbsp;Lainnya</td>
                                 <tr>
                                 <tr>
                                   <td>Faktor yang meringankan</td>
                                   <td>:&nbsp;<input type="checkbox" class="chckfymer" name="chckfymeristirahat">&nbsp;Istirahat</td>
                                   <td><input type="checkbox" class="chckfymer" name="chckfymerdipijat">&nbsp;Dipijat</td>
                                   <td><input type="checkbox" class="chckfymer" name="chckfymerminumobat">&nbsp;Minum Obat</td>
                                   <td><input type="checkbox" class="chckfymer" name="chckfymerlainnya">&nbsp;Lainnya</td>
                                 <tr> 
                                 <tr>
                                   <td>Kualitas nyeri</td>
                                   <td>:&nbsp;<input type="checkbox" class="chckkualitasnyeri" name="chckkualitasnyeriditusuk">&nbsp;Ditusuk</td>
                                   <td><input type="checkbox" class="chckkualitasnyeri" name="chckkualitasnyeriterikit">&nbsp;Terikat</td>
                                   <td><input type="checkbox" class="chckkualitasnyeri" name="chckkualitasnyeritumpul">&nbsp;Tumpul</td>
                                   <td><input type="checkbox" class="chckkualitasnyeri" name="chckkualitasnyeriberdenyut">&nbsp;Berdenyut</td>
                                   <td><input type="checkbox"  class="chckkualitasnyeri" name="chckkualitasnyerilainnya">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyakualitasnyeri"></td>
                                 <tr>
                                 <tr>
                                   <td>Lokasi nyeri</td>
                                   <td colspan="4">:&nbsp;<input type="text" name="txtlokasinyeri"></td>
                                 <tr>
                                 <tr>
                                   <td>Menjalar</td>
                                   <td>:&nbsp;<input type="checkbox" class="chckmenjalar" name="chckmenjalartidak">&nbsp;Tidak</td>
                                   <td colspan="3"><input type="checkbox" class="chckmenjalar" name="chckmenjalaryajelaskan">&nbsp;Ya, jelaskan&nbsp;<input type="text" name="txtjelaskanmenjalar"></td>
                                 <tr> 
                                 <tr>
                                   <td>Skala nyeri</td>
                                   <td>:&nbsp;<input type="checkbox" name="chckskalanyeriringan" class="chckskalanyeri">&nbsp;Ringan</td>
                                   <td><input type="checkbox" class="chckskalanyeri" name="chckskalanyerisedang">&nbsp;Sedang</td>
                                   <td><input type="checkbox" class="chckskalanyeri" name="chckskalanyeriberat">&nbsp;Berat</td>
                                   <td><input type="checkbox" class="chckskalanyeri" name="chckskalanyeritidanyeri">&nbsp;Tidak nyeri</td>
                                 <tr> 
                                 <tr>
                                   <td>Lamanya nyeri</td>
                                   <td>:&nbsp;<input type="checkbox" name="chcklamanyanyeridibawah30menit" class="chcklamanyanyeri">&nbsp;<30 menit</td>
                                   <td><input type="checkbox" colspan="2" name="chcklamanyanyeridiatas30menit" class="chcklamanyanyeri">&nbsp;>30 menit</td>
                                 <tr> 
                                 <tr>
                                   <td>Frekuensi nyeri</td>
                                   <td>:&nbsp;<input type="checkbox" class="chckfrekuensinyeri" name="chckfrekuensinyeriterusmenerus">&nbsp;Terus menerus</td>
                                   <td><input type="checkbox" colspan="2" class="chckfrekuensinyeri" name="chckfrekuensinyerihilangtimbul">&nbsp;Hilang timbul</td>
                                   <td><input type="checkbox" colspan="2" class="chckfrekuensinyeri" name="chckfrekuensinyerijarang">&nbsp;Jarang</td>
                                 <tr> 
                             </tbody>
                           </table>
                           </tr> -->
   <!-- <table style="width : 100%">
               <tbody>
                  <tr>
                     <td colspan="4"><strong>Pengkajian Risiko Jatuh</strong></td>
                  </tr>
                  <tr>
                     <td colspan="2" style="width : 40%;">Cara berjalan pasien: tidak seimbang / jalan dengan menggunakan alat bantu</td>
                     <td style="width : 1px;">:</td>
                     <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprjatuh1" class="chckprjatuh" name="chckprjatuhcvptsjdmabya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscorecbp" value="0"></td>
                     <td>&nbsp;<input type="checkbox" class="chckprjatuh" id="chckprjatuh0" name="chckprjatuhcvptsjdmabyatidak" value="0">&nbsp;Tidak</td>
                  </tr>
                  <tr>
                     <td colspan="2">Menopang saat akan duduk (tampak memegang pinggiran kursi/benda lain)</td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckmsad1" class="chckmsad" name="chckmsadya" value="1">&nbsp;Ya <input type="hidden" name="txtjumlahskorpengkajianresikojatuh"><input type="hidden" name="jmlhscoremsad" value="0"></td>
                     <td>&nbsp;<input type="checkbox" id="chckmsad0" class="chckmsad" name="chckmsadyatidak" value="0">&nbsp;Tidak</td>
                  </tr>
               </tbody>
               </table>
               <table style="width : 50%;"> -->
   <!-- <tbody>
               <tr>
                  <td>Interprestasi hasil :</td>
               </tr>
               <tr>
                  <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh0" id="chckporjatuhtidakberisiko" value="0">&nbsp;Tidak berisiko</td>
                  <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
               </tr>
               <tr>
                  <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh1" id="chckporjatuhrisikorendah" value="1">&nbsp;Resiko rendah</td>
                  <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
               </tr>
               <tr>
                  <td><input type="checkbox" name="chckintepretasihasilpengkajianrisikojatuh" class="chckporjatuh2" id="chckporjatuhrisikotinggi" value="2">&nbsp;Risiko tinggi</td>
                  <td>&nbsp;&nbsp;:&nbsp;Ditemukan 2 jawaban "Ya"</td>
               </tr>
               </tbody>
               </table>
               </tr>
               <tr>
               <table style="width : 100%">
               <tbody>
                  <tr>
                     <td><strong>Pengkajian Risiko Nutrisional</strong></td>
                  </tr>
                  <tr>
                     <td colspan="2" style="width : 40%;">Apakah pasien mempunyai penurunan berat badan dalam 1 bulan?</td>
                     <td style="width : 1px;">:</td>
                     <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnberatbadan1" class="chckprnberatbadan" name="chckprnberatbadanya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnberatbadan" value="0"></td>
                     <td>&nbsp;<input type="checkbox" class="chckprnberatbadan" id="chckprnberatbadan0" name=chckprnberatbadantidak" value="0">&nbsp;Tidak</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="width : 40%;">Apakah pasien mempunyai masalah penurunan nafsu makan dalam 1 bulan?</td>
                     <td style="width : 1px;">:</td>
                     <td style="width : 10%;">&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chckprnnafsumakan1" class="chckprnnafsumakan" name="chckprnnafsumakanya" value="1">&nbsp;Ya <input type="hidden" name="jmlhscoreprnnafsumakan" value="0"></td>
                     <td>&nbsp;<input type="checkbox" class="chckprnnafsumakan" id="chckprnnafsumakan0" name="chckprnnafsumakantidak" value="0">&nbsp;Tidak <input type="hidden" name="jumlahskorpengkajianrisikonutrisional"></td>
                  </tr>
               </tbody>
               </table>
               <table style="width : 50%;">
               <tbody>
                  <tr>
                     <td>Interprestasi hasil :</td>
                  </tr>
                  <tr>
                     <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn0" value="0">&nbsp;Tidak berisiko</td>
                     <td>&nbsp;&nbsp;:&nbsp;Tidak ada jawaban "Ya"</td>
                  </tr>
                  <tr>
                     <td><input type="checkbox" name="chckinterpretasiprn" class="chckinterpretasiprn" id="chckinterpretasiprn1" value="1">&nbsp;Berisiko</td>
                     <td>&nbsp;&nbsp;:&nbsp;Ditemukan 1 jawaban "Ya"</td>
                  </tr>
               </tbody>
               </table>
               </tr>
               <tr>
               <table style="width : 100%">
               <tbody>
                  <tr>
                     <td colspan="4"><strong>Pengkajian Status Fungsional</strong></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Penggunaan alat bantu</p>
                     </td>
                     <td>:</td>
                     <td style="width:10%;"><input type="checkbox" class="chckpealaban" id="chckpealaban0" name="chckpealabantidak">&nbsp;Tidak</td>
                     <td><input type="checkbox" class="chckpealaban" id="chckpealaban1" name="chckpealabanya">&nbsp;Ya, sebutkan : <input type="text" name="txtoestafu"></td>
                  </tr>
                  <tr>
                     <td>
                        <p>ADL (<i>Activities of Daily Living</i>)</p>
                     </td>
                     <td>:</td>
                     <td><input type="checkbox" name="chckadlya" class="chckadl" id="chckadl1">&nbsp;Mandiri</td>
                     <td><input type="checkbox" name="chckadltidak" class="chckadl" id="chckadl0">&nbsp;Dibantu</td>
                  </tr>
               </tbody>
               </table>
               </tr>
               <tr>
               <table style="width : 100%">
               <tbody>
                  <tr>
                     <td><strong>Pengkajian Status Psikologi</strong></td>
                  </tr>
                  <tr>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi0" value="0">&nbsp;Tenang</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi1" value="1">&nbsp;Disorentasi</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi2" value="2">&nbsp;Sedih</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" id="chckpenstatuspsikologi3" value="3">&nbsp;Takut</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" id="chckpenstatuspsikologilainnya" id="chckpenstatuspsikologi4" name="chckpenstatuspsikologi" value="4"> &nbsp;Lainnya&nbsp;<input type="text" name="txtpengstatuspsikologi"></td>
                  </tr>
                  <tr>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="5" id="chckpenstatuspsikologi5">&nbsp;Marah/tegang</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="6" id="chckpenstatuspsikologi6">&nbsp;Cemas</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="7" id="chckpenstatuspsikologi7">&nbsp;Gelisah</td>
                     <td><input type="checkbox" class="chckpenstatuspsikologi" name="chckpenstatuspsikologi" value="8" id="chckpenstatuspsikologi8">&nbsp;Resiko bunuh diri</td>
                     <td></td>
                  </tr>
               </tbody>
               </table>
               </tr>
               <tr>
               <table style="width : 100%;">
               <tbody>
                  <tr>
                     <td colspan="4"><strong>Pengkajian Status Sosial, Ekonomi, Kultural, dan Spiritual</strong></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Status pernikahan</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi0" value="0">&nbsp;Belum menikah</td>
                     <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi1" value="1">&nbsp;Menikah</td>
                     <td><input type="checkbox" class="chckpengstasoekkudspi" name="chckpengstasoekkudspi" id="chckpengstasoekkudspi2" value="2">&nbsp;Duda/janda</td>
                  </tr>
                  <tr>
                     <td>
                        <p>Kendala komunikasi</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi0" name="chckkendalakomunikasitidakada">&nbsp;Tidak ada</label /td>
                     <td colspan="2"><input type="checkbox" class="chckkendalakomunikasi" id="chckkendalakomunikasi1" name="chckkendalakomunikasiadasebutkan">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtkendadakomunikasilainnya"></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Yang merawat dirumah</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah0" name=chckyangmerawatdirumahtidakada"">&nbsp;Tidak ada</td>
                     <td colspan="2"><input type="checkbox" class="chckyangmerawatdirumah" id="chckyangmerawatdirumah1" name="chckyangmerawatdirumahadasebutkan">&nbsp;Ada, sebutkan&nbsp;<input type="text" name="txtadasebutkanyangmerawatdirumah"></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Pekerjaan</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckpekerjaan" id="chckpekerjaan0" name="chckpekerjaanlainnya" value="0">&nbsp;Tidak bekerja</td>
                     <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan1" value="1">&nbsp;PNS/TNI/POLRI</td>
                     <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan2" value="2">&nbsp;Swasta</td>
                     <td><input type="checkbox" class="chckpekerjaan" name="chckpekerjaanlainnya" id="chckpekerjaan3" value="3">&nbsp;Lainnya&nbsp;<input type="text" name="txtlainnyapekerjaan"></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Jaminan kesehatan</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan0" name="chckjaminankesehatan" value="0">&nbsp;Mandiri</td>
                     <td><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan1" name="chckjaminankesehatan" value="1">&nbsp;BPJS</td>
                     <td colspan="2"><input type="checkbox" class="chckjaminankesehatan" id="chckjaminankesehatan2" name="chckjaminankesehatan" id="chckjaminankesehatanlainnya" value="2">&nbsp;Lainnya&nbsp;<input type="text" name="txtjaminankesehatanlainnya"></td>
                  </tr>
                  <tr>
                     <td>
                        <p>Apakah ada tindakan yang bertentangan dengan kepercayaan pasien?</p>
                     </td>
                     <td>:</td>
                     <td>&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien0" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasientidak">&nbsp;Tidak</td>
                     <td><input type="checkbox" id="chcktidanakanyangbertentangandengankepercayaanpasien1" class="chcktidanakanyangbertentangandengankepercayaanpasien" name="chcktidanakanyangbertentangandengankepercayaanpasienya">&nbsp;Ya</td>
                  </tr>
                  <tr> -->
   <!-- <td style="">Jika ada jelaskan tindakan tersebut</td> -->
   <!-- <td>:</td>
               <td colspan="2">&nbsp;&nbsp;&nbsp;<input type="text" name="txtjikaadajelaskantindakantersebut"></td>
               </tr>
               </tbody>
               </table>
               </tr>
               <tr style="border: 1px solid black;">
               <td style="line-height: 1px; border: 1px solid black;">
               <div class="form-group"><label>C. ASSESSMENT (SDKI)</label></div>
               </td>
               </tr>
               <tr>
               <table style="width:90%">
               <tbody>
               <tr>
               <td style="width : 50%;">
                  <label>Diagnosa</label><br>
                  <select class="ql-select2 select2 form-control" name="diagnosasdki">
                  </select>
               </td>
               <td></td>
               </tr>
               <tr>
               <td style="width : 50%;">
                  <label>Diagnosa diluar SDKI</label><br>
                  <input type="text" class="form-control" name="diagnosadiluarsdki">
               </td>
               </tr>
               </tbody>
               </table>
               </tr>
               <tr style="border: 1px solid black;">
               <td style="line-height: 1px; border: 1px solid black;">
               <div class="form-group"><label>D. PLAN (INTERVENSI KEPERAWATAN)</label></div>
               </td>
               </tr>
               <tr>
               <td>
               <table>
               <tbody>
               <tr>
                  <td>
                     <strong>Intervensi Nyeri</strong><br>
                     <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri0" name="chckintervensinyeri" value="0">&nbsp;Mengajarkan teknik relaksasi dan distraksi <br>
                     <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri1" name="chckintervensinyeri" value="1">&nbsp;Mengontrol lingkungan yang dapat mempengaruhi nyeri, seperti cahaya, tingkat kebisingan, dll <br>
                     <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri2" name="chckintervensinyeri" value="2">&nbsp;Memberikan edukasi kepada pasien dan keluarga <br>
                     <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri3" name="chckintervensinyeri" value="3" id="chckintervensinyerilainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensinyerilainnya"> <br>
                     <input type="checkbox" class="chckintervensinyeri" id="chckintervensinyeri4" name="chckintervensinyeri" value="4">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Risiko Jatuh</strong> <br>
                     <input type="checkbox" name="chckintervensirisikojatuh" id="chckintervensirisikojatuh0" class="chckintervensirisikojatuh" value="0">&nbsp;Pakaikan pita bewarna kuning pada lengan kanan atas pasien <br>
                     <input type="checkbox" name="chckintervensirisikojatuh" id="chckintervensirisikojatuh1" class="chckintervensirisikojatuh" value="1">&nbsp;Berikan edukasi pencegahan jatuh pada pasien / keluarga <br>
                     <input type="checkbox" name="chckintervensirisikojatuh" id="chckintervensirisikojatuh2" class="chckintervensirisikojatuh" value="2">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikojatuhtidakadaintervensi"> <br>
                     <input type="checkbox" name="chckintervensirisikojatuh" id="chckintervensirisikojatuh3" class="chckintervensirisikojatuh" value="3">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Risiko Nutrisional</strong> <br>
                     <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional0" name="chckintervensirisikonutrisional" value="0">&nbsp;Konsultasikan pada ahli gizi dan DPJP bila pasien mempunyai risiko nutrisional <br>
                     <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional1" name="chckintervensirisikonutrisional" value="1">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensirisikonutrisionallainnya"> <br>
                     <input type="checkbox" class="chckintervensirisikonutrisional" id="chckintervensirisikonutrisional2" name="chckintervensirisikonutrisional" value="2">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Status Fungsional</strong> <br>
                     <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional0" name="chckintervebsistatusfungsional" value="0">&nbsp;Libatkan partisipasi keluarga dalam membantu pasien untuk melakukan aktivitas <br>
                     <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional1" name="chckintervebsistatusfungsional" value="1" id="chckintervebsistatusfungsionallainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensistatusfungsionallainnya"> <br>
                     <input type="checkbox" class="chckintervebsistatusfungsional" id="chckintervebsistatusfungsional2" name="chckintervebsistatusfungsional" value="2">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Psikologi</strong> <br>
                     <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi0" name="chckintervensipsikologi" value="0">&nbsp;Menenangkan pasien <br>
                     <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi1" name="chckintervensipsikologi" value="1">&nbsp;Mengajarkan teknik relaksasi <br>
                     <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi2" name="chckintervensipsikologi" value="2">&nbsp;Berkoloborasi dengan dokter dalam memberikan terapi pada pasien yang histeris <br>
                     <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi3" name="chckintervensipsikologi" value="3">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensipsikologilainnya"> <br>
                     <input type="checkbox" class="chckintervensipsikologi" id="chckintervensipsikologi4" name="chckintervensipsikologi" value="4">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Sosial, Ekonomi, Kultural, dan Spiritual</strong> <br>
                     <input type="checkbox" class="chckintervensisosial" value="0" id="chckintervensisosial0" name="chckintervensisosial">&nbsp;Menjelaskan kepada pasien dan kelaurga terkait persyaratan asusransi kesehatan <br>
                     <input type="checkbox" class="chckintervensisosial" value="1" id="chckintervensisosial1" name="chckintervensisosial">&nbsp;Memfasilitasi bila ada hambatan dalam berkomunikasi dengan melaporkan kepada tim yang bertanggung jawab <br>
                     <input type="checkbox" class="chckintervensisosial" value="2" id="chckintervensisosial2" name="chckintervensisosial">&nbsp;Memberikan alternatif tindakan lain yang tidak bertentangan dengan kepercayaan <br>
                     <input type="checkbox" class="chckintervensisosial" value="3" id="chckintervensisosial3" name="chckintervensisosial">&nbsp;Menjelaskan terkait tindakan/obat-obatan yang tidak tertanggung oleh asuransi kesehatan <br>
                     <input type="checkbox" class="chckintervensisosial" value="4" id="chckintervensisosial4" name="chckintervensisosial">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensisosiallainnya"> <br>
                     <input type="checkbox" class="chckintervensisosial" value="5" id="chckintervensisosial5" name="chckintervensisosial">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               <tr>
                  <td>
                     <strong>Intervensi Alergi</strong> <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi0" value="0" name="chckintervensialergimemberitahukankepadadokter">&nbsp;Memberitahukan kepada dokter terkait alergi pasien <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi1" value="1" name="chckintervensialergiedukasikepadapasien">&nbsp;Edukasi kepada pasien dan keluarga tentang hal-hal yang hyarus dihindarkan dan pertolongan pertama pada reaksi alergi <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi2" value="2" name="chckintervensialergimemberikanalternatiftindakanlain">&nbsp;Memberikan alternatif tindakan lain yang tidak bertentangan dengan kepercayaan <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi3" value="3" name="chckintervensialergimenjelaskanterkaittindakan">&nbsp;Menjelaskan terkait tindakan/obat-obatan yang tidak tertanggung oleh asuransi kesehatan <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi4" value="4" name="chckintervensialergilainnya">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensialergilainnya"> <br>
                     <input type="checkbox" class="chckintervensialergi" id="chckintervensialergi5" value="5" name="chckintervensialergitidakadaintervensi">&nbsp;Tidak ada intervensi <br>
                  </td>
               </tr>
               </tbody>
               </table>
               </td>
               </tr>
               <tr>
               <table>
               <tbody>
               <tr>
               <td>
                  <strong>Intervensi Keperawatan Lainnya</strong> <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyastabilisasijalannafas">&nbsp;Stabilisasi jalan nafas <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakeluarkanbendaasing">&nbsp;Keluarkan benda asing, lakukan suction <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangiripharingeal">&nbsp;Pasang Iripharingeal Airway/ stabilisasi servikal (neck collar) <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanbantuannafas">&nbsp;Berikan bantuan nafas buatan/ventilasi mekanik <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikan02">&nbsp;Berikan O<sub>2</sub> melalui nasal kanul/NRM/RM <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyadelegatifnebulizer">&nbsp;Delegatif nebulizer <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortandatandavital">&nbsp;Monitor tanda-tanda vital secara periodik<br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitortingkatkesadaran">&nbsp;Monitor tingkat kesadaran secara periodik <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamontorekg">&nbsp;Monitor EKG <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaberikanposisisemifowler">&nbsp;Berikan posisi semifowler <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangdowercatheter">&nbsp;Pasang dower catheter <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamonitoeintakedanoutputcairan">&nbsp;Monitor intake dan output cairan<br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasanginfus">&nbsp;Pasang infus/cairan intravena, cairan koloidm darah atau produk darah, ekspander plasma <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyalakukanperawatanluka">&nbsp;Lakukan perawatan luka <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakoloborasidengandokter">&nbsp;Koloborasi dengan dokter untuk pemberian IV/IC/SC/IM/oral/supp <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyakajirturgorkulit">&nbsp;Kaji turgor kulit dan membran mukosa mulut <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyapasangpengaman">&nbsp;Pasang pengaman, spalk, lakukan imobilisasi <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyamengobservasitandatanda">&nbsp;Mengobservasi tanda-tanda adanya kompartemen sindrom (nyeri local daerah cedera, pucat, penurunan mobilitas, <br>penurunan tekanan nadi, nyeri bertambah saat digerakkan, perubahan sensori/baal dan kesemutan) <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnyaoasangngt">&nbsp;Pasang NGT <br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya1">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya1"><br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya2">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya2"><br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya3">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya3"><br>
                  <input type="checkbox" class="chckintervensikeperawatanlainnya" name="chckintervensikeperawatanlainnya4">&nbsp;Lainnya&nbsp;&nbsp;<input type="text" name="txtintervensikeperawatanlainnya4"><br>
               </td>
               </tr>
               <tr>
               <td>
                  <strong>Evaluasi Keperawatan</strong><br>
                  <input type="checkbox" class="chckaevaluasikeperawatan" name="chckaevaluasikeperawatantidak">&nbsp;Tidak ada <br>
                  <input type="checkbox" class="chckaevaluasikeperawatan" name="chckaevaluasikeperawatanya">&nbsp;Ada, sebutkan <br>
                  <textarea class="form-control" style="width : 70%;" name="txtevaluasikeperawatan"></textarea>
                  <br>
               </td>
               </tr>
               </tbody>
               </table>
               </tr>
               <tr>
               <td>
               <strong>Evaluasi Pasien dan Keluarga</strong>
               </td>
               </tr>
               <tr>
               <td>
               <table style="width : 100%;">
               <tbody>
               <tr style="border: 1px solid black;">
                  <td><input type="checkbox" style="display : none;">&nbsp;Apakah pasien/keluarga bersedia menerima edukasi&nbsp;&nbsp;</td>
                  <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi0" name="chckapakahpasienbersediamenerimaedukasitidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" class="chckapakahpasienbersediamenerimaedukasi" id="chckapakahpasienbersediamenerimaedukasi1" name="chckapakahpasienbersediamenerimaedukasibersedia">&nbsp;Bersedia&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               </tr>
               <tr style="border: 1px solid black;">
                  <td><input type="checkbox" style="display : none;">&nbsp;Apakah terdapat hambatan dalam edukasi&nbsp;&nbsp;</td>
                  <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi0" name="chckapakahterdapathambatandalamedukasitidak">&nbsp;Tidak&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" class="chckapakahterdapathambatandalamedukasi" id="chckapakahterdapathambatandalamedukasi1" name="chckapakahterdapathambatandalamedukasiya">&nbsp;Ya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               </tr>
               <tr style="border: 1px solid black;">
                  <td><input type="checkbox" style="display : none;">&nbsp;Jika "Ya" sebutkan hambatannya&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckpendengaran" id="chckpendengaran1">&nbsp;Pendengaran <br> <input type="checkbox" name="chckemosi" id="chckemosi1">&nbsp;Emosi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckpenglihatan" id="chckpenglihatan1">&nbsp;Penglihatan <br> <input type="checkbox" name="chckbahasa" id="chckbahasa1">&nbsp;Bahasa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckkognitif" id="chckkognitif1">&nbsp;Kognitif <br> <input type="checkbox" name="chcklainnya" id="chcklainnya1">&nbsp;Lainnya &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckfisik" id="chckfisik1">&nbsp;Fisik<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckbudaya" id="chckbudaya1">&nbsp;Budaya<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               </tr>
               <tr style="border: 1px solid black;">
                  <td><input type="checkbox" style="display : none;">&nbsp;Kebutuhan edukasi (Pilih topik edukasi)&nbsp;&nbsp;</td>
                  <td colspan="2"><input type="checkbox" name="chckdiagnosapenyakit" id="chckdiagnosapenyakit1">&nbsp;Diagnosa Penyakit <br> <input type="checkbox" name="chckrehabilitasimedik" id="chckrehabilitasimedik1">&nbsp;Rehabilitasi medik <br> <input type="checkbox" name="chckhakdankewajibanpasien" id="chckhakdankewajibanpasien1">&nbsp;Hak dan kewajiban pasien <br> <input type="checkbox" name="chcktandabahayadanperawatanbayi" id="chcktandabahayadanperawatanbayi1">&nbsp;Tanda bahaya dan perawatan bayi baru lahir &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckobatobatan" id="chckobatobatan1">&nbsp;Obat-obatan <br> <input type="checkbox" name="chckmanajemennyeri" id="chckmanajemennyeri1">&nbsp;Manajemen nyeri <br> <input type="checkbox" name="chckkb" id="chckkb1">&nbsp;KB<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td><input type="checkbox" name="chckdietdannutrisi" id="chckdietdannutrisi1">&nbsp;Diet dan nutrisi <br> <input type="checkbox" name="chckpenggunaanalatmedia" id="chckpenggunaanalatmedia1">&nbsp;Penggunaan alat media <br> <input type="checkbox" name="chcktandabahayamasanifas" id="chcktandabahayamasanifas1">&nbsp;Tanda bahaya masa nifas<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
               </tr>
               </tbody>
               </table>
               </td>
               </tr>
               <tr>
               <td>
               <table style="width : 100%;">
               <tbody>
               <tr>
                  <td style="width : 34%;">
                     <label>Tanggal Pasien Pulang :</label><input type="date" name="txttanggalpasienpulang">
                  </td>
                  <td style="width : 34%;">
                     <label>Jam Pasien Pulang : <input type="time" name="txtjampasienpulang"> WIB</label> <br>
                  </td>
                  <td style="width : 34%;">
                     <label>Perawat Pelaksana :</label><input type="text" name="txtperawatpelaksana">
                  </td>
               </tr>
               <tr class="rulejampasienpulang">
                  <td></td>
                  <td>
                     <div>
                        <p>Tata Cara Pengisian Jam Pasien Pulang</p>
                        <p>1. Isi Dimulai Dengan Detik</p>
                        <p>2. Kemudian Diisi Dengan Menit</p>
                        <p>3. Yang Terakhir Isi Dengan Jam</p>
                        <p>NB: Separator Akan Muncul Sendiri Ketika Angka Diketikkan</p>
                     </div>
                  </td>
                  <td></td>
               </tr>
               </tbody>
               </table>
               </td>
               </tr>
               </tbody>
               </table>
               <br><br>
               <div class="text-center">
               <div class="row">
               <button type="button" class="btn btn-primary btn-lg" id="btnsimpanasesmenawalkeperawatan">SIMPAN</button>
               <button type="button" class="btn btn-warning btn-lg">KEMBALI</button>
               </div>
               </div>
               </form>-->
   </div>
   <!-- akhir asemenpengkajianwalkeperawatangawatdarurat -->
   <div class="text-center" id="buttonsimpansemua">
      <div class="row">
         <a type="button" class="btn btn-lg btn-primary" id="btnsimpanformpemeriksaanklinik" onclick="FormPemeriksaanKlinikSubmit()">SIMPAN</a>
         <button type="button" class="btn btn-lg btn-warning">KEMBALI</button>
      </div>
   </div>
   </form>
   <!-- /.tab-pane -->
   <div class="tab-pane" id="timeline">
      <!-- The timeline -->
      <ul class="timeline timeline-inverse">
         <li id="riwayatAkhir">
            <i class="fa fa-clock-o bg-gray"></i>
         </li>
         <input type="hidden" id="riwayattanggalpemeriksaan" />
      </ul>
      <div style="text-align:center;">
         <a class="btn btn-lg btn-success" id="loadMoreRiwayat"><i class="fa fa-search-plus"></i> Tampilkan riwayat sebelumnya</a>
      </div>
   </div>
   <!-- /.tab-pane -->
   </div>
   <!-- /.tab-content -->
   </div>
   <!-- /.nav-tabs-custom -->
   </div>
   <!-- /.col -->
   </div>
   <!-- /.row -->
   <!-- Modal  -->
   <div id="modal-pemeriksaan-klinik" class="modal fade ">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer"></div>
         </div>
      </div>
   </div>
   <!-- End Of Modal -->
</section>