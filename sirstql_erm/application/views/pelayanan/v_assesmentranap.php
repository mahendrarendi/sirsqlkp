<style type="text/css">
.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}


  /* Ikhsan css */
  body input[type="radio"],body input[type="checkbox"] {
    margin-top: 0;
  }
  body div.radio,body div.checkbox {
    margin-top: 0;
  }
  span.required{color:red;}
  
  body .form-horizontal .form-group {
  margin-left: 0;
  margin-right: 0;
  }
  .square{
    margin-top: 5px;
    margin-left: 10px;
    margin-right: 10px;
    margin-bottom: 10px;
  }
  .btn-sm {
    background-color: #FF7F50;
    border-color: #CD5C5C;
    color: white;
  }


  /* end ikhsan css */
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
          
        <!--view perencanaan ranap-->
        <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_bhp');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
            <div class="box">
                <div class="box-body">
                    <div class="row col-md-12" style="padding-bottom:15px;">
                        <div class="col-md-2">&nbsp;</div>
                        <div class="col-sm-3 row" id="ranap_profile"><!-- Profile Image --></div>
                        <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile"><!-- detail profile --></div>
                    </div>
                    
                    <input type="hidden" name="idinap">
                    <input type="hidden" name="idrencanamedispemeriksaan">
                    <input type="hidden" name="idpendaftaran">
                    <input type="hidden" name="idunit">
                </div>
            </div>
            
            <div class="col-xs-12">
            <a class="btn btn-default btn-md" id="pemeriksaanranap"><i class="fa fa-medkit "></i> Pemeriksaan</a>
            <a class="btn btn-default btn-md" id="pengkajianranap"><i class="fa fa-file"></i> Pengkajian</a>
            <a class="btn btn-default btn-md" id="rekonobat"><i class="fa fa-plus-square"></i> Rekon. Obat</a></i>
            <a class="btn btn-default btn-md" id="labor"><i class="fa fa-hospital-o"></i> Laboratorium</a></i>
            <a class="btn btn-default btn-md" id="radiolog"><i class="fa fa-heartbeat"></i> Radiologi</a></i>
            <a class="btn btn-default btn-md" id="catatan"><i class="fa fa-book"></i> Catatan Akhir</a></i>
            <div class="box pemeriksaanranap" style="border-color:  #1076ba;">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">
<div class="form-group">
                <div class="panel-group" id="vsignriAccordion">
                    <div class="panel panel-default ">
                        <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#vsignriAccordion" data-target="#vsignri">
                            <h4 class="panel-title">
                                <a href="javascript:void(0);"  class="ing">Vital Signs Monitoring</a>
                          </h4>
                        </div>
                        <div id="vsignri" class="panel-collapse collapse" style="height: 0px;">
                            <div class="panel-body">
                              <form action="" id="pengkajianvsignri" name="vsignri" class="form-horizontal">
                                <input type="hidden" name="idinap" id="idinap" value="1">
                                <input type="hidden" name="idpendaftaranranap">
                                <input type="hidden" name="norm">
                                  <div class="col-md-12">
                                    <div class="panel-body">
                                      <a class="btn btn-primary btn-sm" id="addvsign"><i class="fa fa-plus-circle"></i> Add Vital Sign</a>
                                      <table id="vsignranap" class="table table-bordered table-hover table-striped">
                                        <thead>
                                          <tr>
                                            <th style="text-align:center">No</th>
                                            <th style="text-align:center">Tanggal</th>
                                            <th style="text-align:center">Jam</th>
                                            <th style="text-align:center">Tekanan Darah(mmHg)</th>
                                            <th style="text-align:center">Nadi(x/menit)</th>
                                            <th style="text-align:center">Suhu("C)</th>
                                            <th style="text-align:center">Laju Pernapasan(x/menit)</th>
                                            <th style="text-align:center">SpO<small>2</small>(%)</th>
                                          </tr>
                                        </thead>
                                        <tbody></tbody>
                                      </table>
                                    </div>
                                  </div><br>
                                  <div class="form-group" style="text-align:center;">
                                    <a class="btn btn-warning btn-sm" id="btnsimpanvsignmonitoring" onclick="save_monitoring_vsign()" >SIMPAN</a>
                                  </div>
                              </form>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
                      <div class="form-group">
                        <div class="panel-group" id="awalmedriAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#awalmedriAccordion" data-target="#awalmedisri">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);"  class="ing">Pengkajian Awal Medis Rawat Inap</a>
                                  </h4>
                                </div>
                                <div id="awalmedisri" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                      <form action="" id="pengkajianawalmedisri" name="pengkajianri" class="form-horizontal">
                                      <input type="hidden" name="idinap" id="idinap">
                                      <input type="hidden" name="idpendaftaranranap">
                                      <input type="hidden" name="norm">
                                        <div class="col-md-12">
                                          <div class="panel-body">
                                            <table style="width: 60%;">
                                                <tbody>
                                                  <tr>
                                                      <td style="width: 20%;">
                                                        <label>Tekanan Darah <span class="required"></span></label>
                                                      </td>
                                                      <td style="width: 25%;">
                                                        <input type="text" class="form-control" name="tekanandarah" id="tekanandarah">
                                                      </td>
                                                      <td style="width: 5%;">mmHg</td>
                                                      <td style="width: 10%;">&nbsp;</td>
                                                      <td style="width: 20%;"><label>Laju Pernafasan <span class="required"></span></label></td>
                                                      <td style="width: 25%;"><input type="text" class="form-control" name="lajunapas"></td>
                                                      <td style="width: 5%;">x/menit</td>
                                                  <tr>
                                                  <tr>
                                                      <td>
                                                        <label>Nadi <span class="required"></span></label>
                                                      </td>
                                                      <td>
                                                        <input class="form-control" type="text" name="nadi">
                                                      </td>
                                                      <td style="width: 5%;">x/menit</td>
                                                      <td style="width: 10%;">&nbsp;</td>
                                                      <td><label>SpO<small>2</small><span class="required"></span></label></td>
                                                      <td><input class="form-control" type="text"name="spo2"></td>
                                                      <td style="width: 5%;">%</td>
                                                  <tr>
                                                  <tr>
                                                      <td>
                                                        <label>Suhu <span class="required"></span></label>
                                                      </td>
                                                      <td>
                                                        <input class="form-control" type="text" name="suhu">
                                                      </td>
                                                      <td style="width: 5%;">C</td>
                                                  <tr>
                                                  <tr>
                                                    <td>&nbsp;</td>
                                                  </tr>
                                                </tbody>
                                            </table>
                                            <div class="col-md-12"  >
                                                <div class="form-group">
                                                  <label>Subjective<span class="required">*</span></label>
                                                  <textarea class="form-control txt_subjective" rows="4" name="subjective" style="width: 1153px; height: 90px;"></textarea>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-md-12"  >
                                                <div class="form-group">
                                                  <label>Objective<span class="required">*</span></label>
                                                  <textarea class="form-control txt_objective" rows="4" name="objective" style="width: 1153px; height: 90px;"></textarea>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-md-12"  >
                                                <div class="form-group">
                                                  <label>Assessment<span class="required">*</span></label>
                                                  <textarea class="form-control txt_assessment" rows="4" name="assessment" style="width: 1153px; height: 90px;"></textarea>
                                                </div>
                                                <br>
                                            </div>
                                            <div class="col-md-12"  >
                                                <div class="form-group">
                                                  <label>Plan<span class="required">*</span></label>
                                                  <textarea class="form-control txt_plan" rows="4" name="plan" style="width: 1153px; height: 90px;"></textarea>
                                                </div>
                                                <br>
                                            </div>
                                            <table sytle="width:60%;margin-top: 10px !important;">
                                              <tr>
                                                <td><label>Tanggal</label>&emsp;</td>
                                                <td><input type="date" class="form-control" name="tanggalawalmedisri"></td>
                                              </tr>
                                              <tr>
                                                <td><label>Jam</label>&emsp;</td>
                                                <td><input type="time" class="form-control" name="jamawalmedisri"></td>
                                              </tr>
                                              <tr>
                                                <td><label>DPJP</label>&emsp;</td>
                                                <td><input type="text" class="form-control" name="dpjpawalmedisri"></td>
                                              </tr>
                                            </table>
                                          </div>
                                        </div>
                                        <div class="form-group" style="text-align:right;">
                                          <a class="btn btn-warning btn-sm" id="btnsimpanformpemeriksaanklinik" onclick="save_awalmedis_ri()" >SIMPAN</a>
                                          <a class="btn btn-success btn-sm" id="btnsimpanformpemeriksaanklinik" onclick="()" >SELESAI</a>
                                        </div>
                                      </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="awalkepriAccordion">
                        <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#awalkepriAccordion" data-target="#awalkeperawatanri">
                                    <h4 class="panel-title">
                                        <a href="javascript:void(0);" class="ing">Pengkajian Awal Keperawatan Rawat Inap</a>
                                  </h4>
                                </div>
                                <div id="awalkeperawatanri" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                      <!-- FORM Pengkajian Perawatan -->
                                      <form class="form" id="frm-pengkajianrawatinap">  

                                          <div class="row">

                                            <!-- vital sign -->
                                            <div class="col-md-6">
                                              <div class="head-awal">
                                                <table class="table table-striped">
                                                  <tr>
                                                    <th>Tekanan Darah</th>
                                                    <td>:</td>
                                                    <td><input type="text" name="tekanandarah" class="form-control" required></td>
                                                    <td>mmHg</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Nadi</th>
                                                    <td>:</td>
                                                    <td><input type="text" name="nadi" class="form-control" required></td>
                                                    <td>x/menit</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Suhu</th>
                                                    <td>:</td>
                                                    <td><input min="0" type="number" name="suhu" class="form-control" required></td>
                                                    <td>"C</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Laju Pernapasan</th>
                                                    <td>:</td>
                                                    <td><input type="text" name="lajupernapasan" class="form-control" required></td>
                                                    <td>x/menit</td>
                                                  </tr>
                                                  <tr>
                                                    <th>SpO2</th>
                                                    <td>:</td>
                                                    <td><input min="0" type="number" name="spo2" class="form-control" required></td>
                                                    <td>%</td>
                                                  </tr>
                                                </table>
                                              </div>
                                            </div>
                                            <div class="col-md-6">
                                              <div class="head-dua">
                                                <table class="table table-striped">
                                                  <tr>
                                                    <th>Berat Badan</th>
                                                    <td>:</td>
                                                    <td><input min="0" type="number" name="beratbadan" class="form-control" required></td>
                                                    <td>Kg/Gram</td>
                                                  </tr>
                                                  <tr>
                                                    <th>Tinggi Badan</th>
                                                    <td>:</td>
                                                    <td><input min="0" type="number" name="tinggibadan" class="form-control" required></td>
                                                    <td>Cm</td>
                                                  </tr>
                                                </table>
                                              </div>
                                            </div>
                                            <!-- end vital sign -->
                                          
                                            <div class="col-md-12">
                                                <div class="alert alert-warning">
                                                  <h4 style="margin:0;">SUBJECTIVE</h4>
                                                </div>

                                                <div class="form-group">
                                                  <label>Keluhan Utama <span class="required">*</span></label>
                                                  <textarea name="keluhanutama" placeholder="Tuliskan disini ..." class="form-control" required></textarea>
                                                </div>

                                                <table class="table table-striped">
                                                  <tr>
                                                    <th>Riwayat Alergi <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatalergi" required value="0">Tidak Ada</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatalergi" required value="1">Ada,Sebutkan</label>
                                                      </div>
                                                      <div class="ada-riwayatalegi">
                                                        <div class="form-group">
                                                          <input type="text" required class="form-control" placeholder="Tuliskan disini ..." name="text_riwayatalergi">
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Riwayat Konsumsi Alkohol <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatkonsumsiobat" required value="0">Tidak Pernah</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatkonsumsiobat" required value="1">Masih Mengkonsumsi</label>
                                                      </div>
                                                      <div class="ada-riwayatkonsusmsiobat">
                                                        <div class="form-inline">
                                                          <label>Jumlah Perhari <span class="required">*</span></label>
                                                          <input type="text" required class="form-control" placeholder="Tuliskan disini ..." name="text_riwayatkonsumsiobat">
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatkonsumsiobat" required value="2">Sudah Berhenti</label>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Riwayat Merokok <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatmerokok" required value="0">Tidak Pernah</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatmerokok" required value="1">Masih Mengkonsumsi</label>
                                                      </div>
                                                      <div class="ada-riwayatmerokok">
                                                        <div class="form-inline">
                                                          <label>Jumlah Perhari <span class="required">*</span></label>
                                                          <input type="text" required class="form-control" placeholder="Tuliskan disini ..." name="text_riwayatkonsumsiobat">
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayatmerokok" required value="2">Sudah Berhenti</label>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Riwayat Transfusi Darah <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayattransfusidarah" required value="0">Tidak pernah</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="3">
                                                      <div class="radio">
                                                        <label class="radio-inline"><input type="radio" name="riwayattransfusidarah" required value="1">Pernah</label>
                                                      </div>
                                                      <div class="ada-riwayattransfusidarah">
                                                        <div class="form-group">
                                                          <label>Reaksi Transfusi : </label>
                                                          <label class="radio-inline"><input type="radio" name="reaksitransfusi" required value="0">Tidak Ada</label>
                                                          <label class="radio-inline"><input type="radio" name="reaksitransfusi" required value="1">Ada,Sebutkan</label>
                                                          <div class="ada-reaksitransfusi">
                                                              <input type="text" name="txt_reaksitransfusi" placeholder="Tuliskan disini ..." class="form-control" required>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Riwayat Penyakit Dahulu <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="0">Peny.Jantung</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="1">Peny.Ginjal</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="2">DM</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="3">Hipertensi</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="4">Kanker</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="5">Stroke</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="6">Kejang</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="7">TBC</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="8">Gangguan Darah</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="9">Gangguan Jiwa</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="10">Tidak Ada</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitdahulu[]" required value="11">Lainya</label>
                                                      </div>
                                                      <div class="ada-riwayatpenyakitdahulu">
                                                        <input type="text" name="txt_riwayatpenyakitdahulu" required class="form-control" placeholder="Tuliskan disni ...">
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Riwayat Penyakit Keluarga <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="0">Peny.Jantung</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="1">Peny.Ginjal</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="2">DM</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="3">Hipertensi</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="4">Kanker</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="5">Stroke</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="6">Kejang</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="7">TBC</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="8">Gangguan Darah</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="9">Gangguan Jiwa</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="10">Tidak Ada</label>
                                                        <label class="checkbox"><input type="checkbox" name="riwayatpenyakitkeluarga[]" required value="11">Lainya</label>
                                                      </div>
                                                      <div class="ada-riwayatpenyakitdahulu">
                                                        <input type="text" name="txt_riwayatpenyakitdahulu" required class="form-control" placeholder="Tuliskan disni ...">
                                                      </div>
                                                    </td>
                                                  </tr>
                                                  
                                                </table>
                                                
                                            </div>
                                            <!-- end col-md-12 -->
                                            
                                            <div class="col-md-12">
                                                <div class="alert alert-warning">
                                                  <h4 style="margin:0;">OBJECTIVE</h4>
                                                </div>
                                                <table class="table table-striped">
                                                  
                                                  <tr>
                                                    <th>Keadaan Umum <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="form-inline">
                                                        <input type="text" name="keadaanumum" class="form-control" required placeholder="Tuliskan disni ...">
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Neurologis <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="0">Vertigo</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="1">Tingling</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="2">Genggaman Lemah</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="3">Nyeri Kepala</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="4">Paralisis</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="5">Pupil Tidak Reaktif</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="6">Kejang</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="7">Afasia</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="8">Tremor</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="9">Letargi</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="10">Baal</label>
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="11">Hemiparese</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="neurolois[]" required value="12">Tidak ada keluhan/Kelainan</label> 
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Kardiovaskular <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="0">Takikardi</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="1">Fatigue</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="2">Kuku Clubbing</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="3">Bradikardi</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="4">Cardiomegali</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="5">Tidak Ada Nadi</label>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="6">Irreuler</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="7">AMI</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="8">Peningkatkan JVP</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="9">Murmur</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="10">Hipertensi</label>
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="11">Gallop</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="12">Hipotensi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="kardiovaskular[]" required value="13">Tidak ada keluhan/Kelainan</label> 
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Respiratori <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="0">Sesak Nafas</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="1">Crakles</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="2">Barrel Chest</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="3">Takipnea</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="4">Batuk</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="5">Funnel Chest</label>
                                                      </div>
                                                      <div class="form-group" style="margin-top:15px;">
                                                          <label style="margin:0;padding:0;">Alat bantu nafas : <span class="required">*</span></label>
                                                          <div class="radio" style="padding:0;">
                                                            <label class="radio-inline"><input type="radio" name="alatbantunafas" required value="0">Tidak Ada</label>
                                                            <label class="radio-inline"><input type="radio" name="alatbantunafas" required value="1">Ada</label>
                                                            <div class="text-alatbantunafas">
                                                              <input placeholder="Tulisakan disini ..." type="text" name="text_alatbantunafas" required class="form-control">
                                                            </div>
                                                          </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="6">Bradipnea</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="7">Dyspnea</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="8">Pigeon Chest</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="9">Ronchi</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="10">Hemoptysis</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="11">Kyphoscoliosis</label>
                                                      </div>
                                                    </td>
                                                    <td colspan="2">
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="12">Whezzing</label>
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="13">Chest Pain</label> 
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="14">Bersin</label> 
                                                        <label class="checkbox"><input type="checkbox" name="respiratori[]" required value="15">Tidak ada keluhan/Kelainan</label> 
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Gastrointestinal <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Mual">Mual</label>
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Konstipasi">Konstipasi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Maag">Maag</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Bising usus meningkat">Bising usus meningkat</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Muntah">Muntah</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Anoreksia">Anoreksia</label> 
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Disfagia">Disfagia</label>
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Inkontinensia">Inkontinensia</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Stomatitis">Stomatitis</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Nyeri ulu hati">Nyeri ulu hati</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Bising usus meningkat">Bising usus meningkat</label> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Diare">Diare</label> 
                                                        <div class="option-diare">
                                                          <div class="form-inline">
                                                            <label>Durasi</label>
                                                            <input placeholder="Tuliskan disini ..." type="text" required class="form-control" name="xhari_diare"> x/Hari
                                                          </div>
                                                          <div class="form-inline">
                                                            <label>Lendir</label>
                                                            <input placeholder="Tuliskan disini ..." type="text" required class="form-control" name="lendir_diare">
                                                          </div>
                                                          <div class="form-inline">
                                                            <label>Darah</label>
                                                            <input placeholder="Tuliskan disini ..." type="text" required class="form-control" name="darah_diare">
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Intoleransi diet">Intoleransi diet</label>
                                                          <div class="form-group" style="margin:0;">
                                                            <label style="margin:0;padding:0;">Penurunan nafsu makan : <span class="required">*</span></label>
                                                            <div class="radio" style="padding:0;">
                                                              <label class="radio-inline"><input type="radio" name="penurunannafsumakan" required value="Tidak">Tidak</label>
                                                              <label class="radio-inline"><input type="radio" name="penurunannafsumakan" required value="Ya">Ya, Karena : </label>
                                                              <div class="text-penurunannafsumakan">
                                                                <input placeholder="Tulisakan disini ..." type="text" name="text_penurunannafsumakan" required class="form-control">
                                                              </div>
                                                            </div>
                                                          </div>
                                                          <div class="form-group" style="margin:0;">
                                                            <label style="margin:0;padding:0;">Abdomen : <span class="required">*</span></label>
                                                            <div class="radio" style="padding:0;">
                                                              <label class="radio-inline"><input type="radio" name="abdomen" required value="Distensi">Distensi</label>
                                                              <label class="radio-inline"><input type="radio" name="abdomen" required value="Ascites">Ascites</label>
                                                              <label class="radio-inline"><input type="radio" name="abdomen" required value="Lainnya">Lainnya </label>
                                                              <div class="text-abdomen">
                                                                <input placeholder="Tulisakan disini ..." type="text" name="text_abdomenlainya" required class="form-control">
                                                              </div>
                                                            </div>
                                                          </div>
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Diet khusus">Diet khusus</label>
                                                        <div class="text-dietkhusus">
                                                          <input placeholder="Tulisakan disini ..." type="text" name="text_dietkhusus" required class="form-control">
                                                        </div> 
                                                        <label class="checkbox"><input type="checkbox" name="gastrointestinal[]" required value="Tidak ada keluhan/kelainan">Tidak ada keluhan/kelainan</label> 
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Genitourinaria <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Disuria">Disuria</label>
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Foley">Foley</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Urostomi">Urostomi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Hesitansi">Hesitansi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Menopause">Menopause</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Sekret abnormal">Sekret abnormal</label> 
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Nokturia">Nokturia</label>
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Inkontinensia">Inkontinensia</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Poliuri">Poliuri</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Retensi">Retensi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Oligouri">Oligouri</label> 
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Hematuria">Hematuria</label> 
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="genitourinaria[]" required value="Tidak ada keluhan/kelainan">Tidak ada keluhan/kelainan</label>
                                                      </div>
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                    <th>Obstetri dan Ginekologi <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="form-group">
                                                        <label>Riwayat Kehamilan Sekarang :</label>
                                                        <table>
                                                          <tr>
                                                            <td>G</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="g_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>P</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="p_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>A</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="a_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>AH</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="ah_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>HPHT</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="hpht_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>HPL</td>
                                                            <td>
                                                              <input required class="form-control" type="text" placeholder="Tuliskan disini ..." name="hpl_riwayatkehamilansekarang">
                                                            </td>
                                                          </tr>
                                                        </table>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="form-group">
                                                        <label>ANC Terpadu</label>
                                                        <div class="radio" style="padding:0;">
                                                          <label class="radio-inline"><input type="radio" name="anc_terpadu" required value="Tidak">Tidak</label>
                                                          <label class="radio-inline"><input type="radio" name="anc_terpadu" required value="Ya">Ya</label>
                                                        </div>
                                                        <div class="group-ancterpadu">
                                                          <input placeholder="Tuliskan disini ..." type="text" required name="text_ancterpadu" class="form-control">
                                                        </div>
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Imunisasi TT</label>
                                                        <input placeholder="Tuliskan disini ..." type="text" required name="imunisasi_tt" class="form-control">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Pendarahan pervaginaan</label>
                                                        <div class="radio" style="padding:0;">
                                                          <label class="radio-inline"><input type="radio" name="pendarahan_pervaginaan" required value="Tidak Ada">Tidak Ada</label>
                                                          <label class="radio-inline"><input type="radio" name="pendarahan_pervaginaan" required value="Ya">Ya</label>
                                                        </div>
                                                        <div class="group-ancterpadu">
                                                          <input placeholder="Tuliskan disini ..." type="text" required name="text_pendarahan_pervaginaan" class="form-control">
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="radio">
                                                        <label class="radio"><input type="radio" name="obstetri_dan_ginekologi" required value="Tidak ada keluhan/kelainan">Tidak ada keluhan/kelainan</label>
                                                      </div> 
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <th>Muskuloskeletal, dan Kulit <span class="required">*</span></th>
                                                    <td>:</td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Deformitas/atrofi">Deformitas/atrofi</label>
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Kram">Kram</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Nodul">Nodul</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Panas">Panas</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Alat bantu">Alat bantu</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Nyeri sendi">Nyeri sendi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Fraktur">Fraktur</label>
                                                        <div class="group-fraktur">
                                                            <input type="text" placeholder="Tuliskan disini ..." name="text_fraktur" reuired class="form-control">
                                                        </div>
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Kekuatan Otot">Kekuatan Otot</label>
                                                        <div class="group-kekuatanotot">
                                                          <table>
                                                            <tr>
                                                              <td><label>Kanan Atas</label></td>
                                                              <td>
                                                                <input type="text" placeholder="Tuliskan disini ..." name="kekuatanotot_kananatas" required class="form-control">
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                              <td><label>Kanan Bawah</label></td>
                                                              <td>
                                                                <input type="text" placeholder="Tuliskan disini ..." name="kekuatanotot_kananbawah" required class="form-control">
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                              <td><label>Kiri Atas</label></td>
                                                              <td>
                                                                <input type="text" placeholder="Tuliskan disini ..." name="kekuatanotot_kiriatas" required class="form-control">
                                                              </td>
                                                            </tr>
                                                            <tr>
                                                              <td><label>Kiri Bawah</label></td>
                                                              <td>
                                                                <input type="text" placeholder="Tuliskan disini ..." name="kekuatanotot_kiribawah" required class="form-control">
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Paralisis">Paralisis</label>
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Kemerahan">Kemerahan</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Kifosis">Kifosis</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Distrofi">Distrofi</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Osteoporosis">Osteoporosis</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Turgor buruk">Turgor buruk</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Luka">Luka</label> 
                                                        <div class="group-luka">
                                                            <input type="text" placeholder="Tuliskan disini ..." name="text_luka" reuired class="form-control">
                                                        </div>
                                                      </div>
                                                    </td>
                                                    <td>
                                                      <div class="checkbox">
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Paralisis">Lembab</label>
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Kemerahan">Ikterik</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit[]" required value="Pucat">Pucat</label> 
                                                        <label class="checkbox"><input type="checkbox" name="muskuloskeletal_kulit" required value="Tidak ada keluhan/kelainan">Tidak ada keluhan/kelainan</label>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td colspan="5">
                                                      <div class="pengkajiannyeri-assesmentawal" style="padding-top:15px;">
                                                        <div class="form-group" style="margin-bottom:0;">
                                                          <label>Pengkajian Nyeri (Pilih Salah Satu Sesuai Kategori Pasien)</label>
                                                        </div>
                                                        <div class="radio" style="padding:0;">
                                                          <label class="radio"><input type="radio" name="pilihpengkajiannyeri_awal" required value="NIPS">NIPS (Neonatal Infant Pain Score) (Digunakan pada pasien bayi usia 0-30 hari)</label>
                                                          <label class="radio"><input type="radio" name="pilihpengkajiannyeri_awal" required value="FLACC">FLACC Behavioral Pain Scale (Digunakan pada pasien bayi usia >30 hari sampai dengan usia < 3 tahun, dan anak dengan gangguan kognitif)</label>
                                                          <label class="radio"><input type="radio" name="pilihpengkajiannyeri_awal" required value="WRONG">Wong Baker Faces Pain Scale (Digunakan pada pasien anak usia ≥ 3 tahun dan dewasa)</label>
                                                        </div>
                                                      </div>
                                                    </td>
                                                  </tr>

                                                  <tr>
                                                    <td colspan="5">
                                                      <!-- nips form -->
                                                      <div class="nips-form">
                                                        <div class="text-center" style="margin:15px 0px;"><b>NIPS (Neonatal Infant Pain Score)</b></div>
                                                        <table class="table table-bordered table-hover table-striped">
                                                          <tr>
                                                            <th>Pengkajian</th>
                                                            <th>Parameter</th>
                                                            <th>Nilai</th>
                                                            <th>Skor</th>
                                                          </tr>
                                                          
                                                          <tr>
                                                            <td rowspan="2">Ekspresi Wajah</td>                                               
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="ekspresiwajah_awal" required value="0">Otot Wajah rileks, ekspresi netral</label>
                                                            </td>     
                                                            <td>
                                                              <div class="nilai-keterangan text-center">0</div>
                                                            </td>
                                                            <td rowspan="2" style="vertical-align: middle;">
                                                              <div class="skor-ekspresiwajahawal text-center" data-skor="0">0</div>
                                                            </td>  
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="ekspresiwajah_awal" required value="1">Otot Wajah tegang, alis berkerut,rahang,dan dagu mengunci</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">1</div>
                                                            </td> 
                                                                                                            
                                                          </tr>

                                                          <tr>
                                                            <td rowspan="4">Tangisan</td>                                               
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="tangisan_awal" required value="0">Tenang,Tidak Menangis</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">0</div>
                                                            </td>
                                                            <td rowspan="2" style="vertical-align: middle;">
                                                              <div class="skor-tangisanawal text-center" data-skor="0">0</div>
                                                            </td>         
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="tangisan_awal" required value="1">Mengerang Lemah</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">1</div>
                                                            </td>  
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="tangisan_awal" required value="2">Menangis Kencang, Melengking dan terus Menerus</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">2</div>
                                                            </td>  
                                                          </tr>
                                                          <tr>
                                                            <td colspan="2" class="text-center">(Catatan : menangis tanpa suara diberi skor bila bayi di intubasi)</td>
                                                          </tr>

                                                          <tr>
                                                            <td rowspan="2">Pola Pernafasan</td>                                               
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="polapernafasan_awal" required value="0">Bernapas Biasa</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">0</div>
                                                            </td>
                                                            <td rowspan="2" style="vertical-align: middle;">
                                                              <div class="skor-polapernafasanawal text-center" data-skor="0">0</div>
                                                            </td>        
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="polapernafasan_awal" required value="1">Pola Nafas berubah : tarikan ireguler,lebih cepat dibanding biasa, menahan nafas, tersedak</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">1</div>
                                                            </td>  
                                                          </tr>
                                                          
                                                          <tr>
                                                            <td rowspan="2">Tangan</td>                                               
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="rileks_awal" required value="0">Rileks, Tidak ada kekakuan Otot, gerakan tungkai biasa</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">0</div>
                                                            </td>
                                                            <td rowspan="2" style="vertical-align: middle;">
                                                              <div class="skor-rileksawal text-center" data-skor="0">0</div>
                                                            </td>       
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="rileks_awal" required value="1">Tegang, Kaku</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">1</div>
                                                            </td>  
                                                          </tr>

                                                          <tr>
                                                            <td rowspan="2">Kesadaran</td>                                               
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="kesadaran_awal" required value="0">Rileks, Tenang,Tidur lelap atau bangun</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">0</div>
                                                            </td>
                                                            <td rowspan="2" style="vertical-align: middle;">
                                                              <div class="skor-kesadaranawal text-center" data-skor="0">0</div>
                                                            </td>         
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                              <label class="radio-inline" style="padding-top:0;"><input type="radio" name="kesadaran_awal" required value="1">Sadar, atau Gelisah</label>
                                                            </td>
                                                            <td>
                                                              <div class="nilai-keterangan text-center">1</div>
                                                            </td>  
                                                          </tr>

                                                          <tr>
                                                            <th colspan="3" class="text-center">
                                                              Total Skor
                                                            </th>
                                                            <td style="vertical-align: middle;">
                                                              <div class="total-skor-nips text-center" data-totalskor="0">0</div>
                                                            </td> 
                                                          </tr>
                                                                                                            
                                                        </table>
                                                        <div class="form-group">
                                                          <label>Interprestasi Hasil</label>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="interprestasinips_hasil_awal" required value="< 3"> < 3 : Bayi Tidak mengalami Nyeri</label>
                                                            <label class="radio-inline"><input type="radio" name="interprestasinips_hasil_awal" required value="> 3"> >3 : Bayi mengalami Nyeri</label>
                                                          </div>
                                                        </div>
                                                      </div> 
                                                      <!-- end nips form -->
                                                      
                                                      <hr>

                                                      <!-- FLACC Form -->
                                                      <div class="flacc-form">
                                                        <div class="text-center" style="margin:15px 0px;"><b>FLACC Behavioral Pain Scale</b></div>
                                                        <table class="table table-bordered table-hover table-striped">
                                                          <tr>
                                                            <th>Pengkajian</th>
                                                            <th>Nilai 0</th>
                                                            <th>Nilai 1</th>
                                                            <th>Nilai 2</th>
                                                            <th>Skor</th>
                                                          </tr>
                                                          <tr>
                                                            <td>Wajah</td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_wajahawal" required value="0">Tersenyum / tidak ada ekspresi</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_wajahawal" required value="1">Terkadang Merintih/menarik diri</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_wajahawal" required value="2">Sering Menggetarkan dagu dan mengatupkan rahang</label>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                              <div class="skor-flacc-wajahawal text-center" data-skor="0">0</div>
                                                              <input type="hidden" name="skor_flacc_wajahawal" value="0">
                                                            </td> 
                                                          </tr>
                                                          <tr>
                                                            <td>Kaki</td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_kakiawal" required value="0">Normal/relaksasi</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_kakiawal" required value="1">Tidak Tenang/Tegang</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_kakiawal" required value="2">Kaki Menendang/menarik diri</label>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                              <div class="skor-flacc-kakiawal text-center" data-skor="0">0</div>
                                                              <input type="hidden" name="skor_flacc_kakiawal" value="0">
                                                            </td> 
                                                          </tr>
                                                          <tr>
                                                            <td>Aktivitas</td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_aktivitasawal" required value="0">Tidur Posisi Normal/relaksasi</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_aktivitasawal" required value="1">Gerakan Menggliat/bergeling Kaku</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_aktivitasawal" required value="2">Melengkung Punggung/kaku/membentak</label>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                              <div class="skor-flacc-aktivitasawal text-center" data-skor="0">0</div>
                                                              <input type="hidden" name="skor_flacc_aktivitasawal" value="0">
                                                            </td> 
                                                          </tr>
                                                          <tr>
                                                            <td>Menangis</td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_menangisawal" required value="0">Tidak Menangis</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_menangisawal" required value="1">Mengerang/Merengek</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_menangisawal" required value="2">Menangis terus menerus/terisak/menjerit</label>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                              <div class="skor-flacc-menangisawal text-center" data-skor="0">0</div>
                                                              <input type="hidden" name="skor_flacc_menangisawal" value="0">
                                                            </td> 
                                                          </tr>
                                                          <tr>
                                                            <td>Bersuara</td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_bersuaraawal" required value="0">Bersuara Normal/tenang</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_bersuaraawal" required value="1">Tenang bila dipeluk/digendong/diajak bicara</label>
                                                            </td>
                                                            <td>
                                                              <label class="radio-inline"><input type="radio" name="flacc_bersuaraawal" required value="2">Sulit untuk menenangkan</label>
                                                            </td>
                                                            <td style="vertical-align: middle;">
                                                              <div class="skor-flacc-bersuaraawal text-center" data-skor="0">0</div>
                                                              <input type="hidden" name="skor_flacc_bersuaraawal" value="0">
                                                            </td> 
                                                          </tr>
                                                          <tr>
                                                            <th colspan="4" class="text-center">
                                                              Total Skor
                                                            </th>
                                                            <td style="vertical-align: middle;">
                                                              <div class="total-skor-flacc text-center" data-totalskor="0">0</div>
                                                              <input type="hidden" name="total_skor_flacc" value="0">
                                                            </td> 
                                                          </tr>
                                                        </table>
                                                        
                                                        <div class="form-group">
                                                          <label>Interprestasi Hasil</label>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="interprestasiflacc_hasil_awal" required value="0"> 0 : Nyaman</label>
                                                            <label class="radio-inline"><input type="radio" name="interprestasiflacc_hasil_awal" required value="1-3"> 1-3 : Nyeri Ringan</label>
                                                            <label class="radio-inline"><input type="radio" name="interprestasiflacc_hasil_awal" required value="4-6"> 4-6 : Nyeri Sedang</label>
                                                            <label class="radio-inline"><input type="radio" name="interprestasiflacc_hasil_awal" required value="7-10"> 7-10 : Nyeri Berat</label>
                                                          </div>
                                                        </div>

                                                      </div>
                                                      <!-- END FLACC Form -->

                                                      <hr>

                                                      <!-- wongbaker form -->
                                                      <div class="wongbaker-form">
                                                        <div class="text-center" style="margin:15px 0px;"><b>Wong Baker Faces pain Scale</b></div>
                                                        <table class="table table-bordered table-hover table-striped">
                                                          <tr>
                                                    <td class="text-center">
                                                      <img src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_0.png">
                                                    </td>
                                                    <td class="text-center">
                                                      <img src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_2.png">
                                                    </td>
                                                    <td class="text-center">
                                                      <img src="<?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_4.png">
                                                    </td>
                                                    <td class="text-center">
                                                      <img  src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_6.png">
                                                    </td>
                                                    <td class="text-center">
                                                      <img  src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_8.png">
                                                    </td>
                                                    <td class="text-center">
                                                      <img  src=" <?= base_url() ?>assets/images/skalanyeri/FACES_English_Blue1_10.png">
                                                    </td>
                                                  </tr>
                                                  <tr>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="0">
                                                        </div>
                                                      </td>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="2">
                                                        </div>
                                                      </td>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="4">
                                                        </div>
                                                      </td>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="6">
                                                        </div>  
                                                      </td>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="8">
                                                        </div>
                                                      </td>
                                                      <td class="text-center">
                                                        <div class="radio">
                                                          <input type="radio" class="radio-inline" name="chckwongbaker"  value="10">
                                                        </div>  
                                                      </td>
                                                  </tr>
                                                        </table>

                                                <div class="form-group">
                                                  <label>Interprestasi Hasil</label>
                                                  <div class="radio-form">
                                                    <label class="radio-inline"><input type="radio" name="wongbakker_hasil_awal" required value="0"> 0 : Nyaman</label>
                                                    <label class="radio-inline"><input type="radio" name="wongbakker_hasil_awal" required value="1-3"> 1-3 : Nyeri Ringan</label>
                                                    <label class="radio-inline"><input type="radio" name="wongbakker_hasil_awal" required value="4-6"> 4-6 : Nyeri Sedang</label>
                                                    <label class="radio-inline"><input type="radio" name="wongbakker_hasil_awal" required value="7-10"> 7-10 : Nyeri Berat</label>
                                                  </div>
                                                </div>

                                                <hr>

                                                <div class="faktor-hasil-wongbaker">
                                                  <table class="table table-striped table-hover">
                                                      <tr>
                                                        <th>Faktor yang memperberat <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktormemperberatwongbaker_awal" required value="Cahaya">Cahaya</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormemperberatwongbaker_awal" required value="Gerakan">Gerakan</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormemperberatwongbaker_awal" required value="Suara">Suara</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormemperberatwongbaker_awal" required value="Lainya">Lainya</label>
                                                          </div>
                                                          <div class="form-lainyafaktormemperberat form-inline">
                                                            <input type="text" placeholder="Tuliskan disini ..." class="form-control" name="text_faktormemperberat_awal" required>
                                                          </div>
                                                        </td>
                                                        <th>Menjalar <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktormenjalarwongbaker_awal" required value="Tidak">Tidak</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormenjalarwongbaker_awal" required value="Ya">Ya</label>
                                                          </div>
                                                          <div class="form-lainyafaktormenjalarwongbaker form-inline">
                                                            <input type="text" placeholder="Tuliskan disini ..." class="form-control" name="text_faktormenjalarwongbaker_awal" required>
                                                          </div>
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <th>Faktor yang Meringankan <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktormeringankanwongbaker_awal" required value="Istirahat">Istirahat</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormeringankanwongbaker_awal" required value="Dipijat">Dipijat</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormeringankanwongbaker_awal" required value="Minum Obat">Minum Obat</label>
                                                            <label class="radio-inline"><input type="radio" name="faktormeringankanwongbaker_awal" required value="Lainya">Lainya</label>
                                                          </div>
                                                          <div class="form-lainyafaktormeringankan form-inline">
                                                            <input type="text" placeholder="Tuliskan disini ..." class="form-control" name="text_faktormeringankan_awal" required>
                                                          </div>
                                                        </td>
                                                        <th>Skala Nyeri <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="Ringan">Ringan</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="Sedang">Sedang</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="Berat">Berat</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="Tidak Nyeri">Tidak Nyeri</label>
                                                          </div>
                                                          
                                                        </td>
                                                      </tr>
                                                      <tr>
                                                        <th>Kualitas Nyeri <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="kualitasnyeriwongbaker_awal" required value="Ditusuk">Ditusuk</label>
                                                            <label class="radio-inline"><input type="radio" name="kualitasnyeriwongbaker_awal" required value="Terikat">Terikat</label>
                                                            <label class="radio-inline"><input type="radio" name="kualitasnyeriwongbaker_awal" required value="Tumpul">Tumpul</label>
                                                            <label class="radio-inline"><input type="radio" name="kualitasnyeriwongbaker_awal" required value="Berdenyut">Berdenyut</label>
                                                            <label class="radio-inline"><input type="radio" name="kualitasnyeriwongbaker_awal" required value="Lainya">Lainya</label>
                                                          </div>
                                                          <div class="form-lainyafaktorkualitasnyeri form-inline">
                                                            <input type="text" placeholder="Tuliskan disini ..." class="form-control" name="text_faktorkualitasnyeri_awal" required>
                                                          </div>
                                                        </td>
                                                        <th>Lamanya Nyeri <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="< 30"> < 30 Menit</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorskalanyeriwongbaker_awal" required value="> 30"> > 30 Menit</label>
                                                          </div>
                                                        </td>
                                                        
                                                      </tr>
                                                      <tr>
                                                        <th>Frekuensi Nyeri <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="radio-form">
                                                            <label class="radio-inline"><input type="radio" name="faktorfrekuensinyeriwongbaker_awal" required value="Terus Menerus">Terus menerus</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorfrekuensinyeriwongbaker_awal" required value="Hilang Timbul">Hilang Timbul</label>
                                                            <label class="radio-inline"><input type="radio" name="faktorfrekuensinyeriwongbaker_awal" required value="Jarang">Jarang</label>
                                                          </div>
                                                        </td>
                                                        <th>Lokasi Nyeri <span class="required">*</span></th>
                                                        <td>:</td>
                                                        <td>
                                                          <div class="form-group">
                                                            <input type="text" name="lokasinyeriwongbaker_awal" class="form-control" required placeholder="Tuliskan disini ...">
                                                          </div>
                                                        </td>
                                                      </tr>
                                                      
                                                  </table>
                                                </div>

                                                      </div>
                                                      <!-- end wongbaker form -->
</td>
                                          </tr>

                                          <!-- Pengkajian Resiko Jatuh -->
                                          <tr>
                                            <td colspan="5">
                                              <div class="pengkajianresikojatuh-assesmentawal" style="padding-top:15px;">
                                                <div class="form-group" style="margin-bottom:0;">
                                                  <label>Pengkajian Resiko Jatuh (Pilih Salah Satu Sesuai Kategori Usia Pasien)</label>
                                                </div>
                                                <div class="radio" style="padding:0;">
                                                  <label class="radio"><input type="radio" name="pilihpengkajianresikojatuh_awal" required value="HUMPTY">Humpty Dumpty (Digunakan pada pasien anak usia 0 < 18 Tahun)</label>
                                                  <label class="radio"><input type="radio" name="pilihpengkajianresikojatuh_awal" required value="MORSE">Morse Fall Scale (Digunakan pada Pasien Usia > 18 - < 60 Tahun)</label>
                                                  <label class="radio"><input type="radio" name="pilihpengkajianresikojatuh_awal" required value="ONTARIO">Ontario Modified Stratify Sidney Scoring(Digunakan pada pasien lansia usia > 60)</label>
                                                </div>
                                              </div>
                                                    </td>
                                                  </tr>
                                          <tr>
                                            <td colspan="5">
                                              <!-- Humpty Dumpty form -->
                                              <div class="humptydumpty-form">
                                                <div class="text-center" style="margin:15px 0px;"><b>Humpty Dumpty (Digunakan pada pasien anak usia 0 < 18 Tahun)</b></div>
                                                <table class="table table-bordered table-hover table-striped">
                                                  <tr>
                                                    <th>Parameter</th>
                                                    <th>Kriteria</th>
                                                    <th>Nilai</th>
                                                    <th>Skor</th>
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="4">Usia</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_usia_awal" required value="4">< 3 Tahun</label>
                                                    </td>     
                                                    <td>
                                                      <div class="skor-resikojatuh-usia-awal text-center">4</div>
                                                    </td>
                                                    <td rowspan="4" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-usia-awal text-center" data-skor="0">0</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_usia_awal" required value="3">3-7 Tahun</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-usia-awal text-center">3</div>
                                                    </td>                                              
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_usia_awal" required value="2">7-13 Tahun</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-usia-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_usia_awal" required value="1"> > 13 Tahun</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-usia-awal text-center">1</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="2">Jenis Kelamin</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_jeniskelamin_awal" required value="2">Laki-Laki</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-jk-awal text-center">2</div>
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-jk-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh_jeniskelamin_awal" required value="1">Perempuan</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-jk-awal text-center">1</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="4">Diagnosis</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosis-awal" required value="4">Diagnosis neurologi</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosis-awal text-center">4</div>
                                                    </td>
                                                    <td rowspan="4" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-diagnosis-awal text-center" data-skor="0">0</div>
                                                    </td>        
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosis-awal" required value="3">Perubahan Oksigenasi (diagnosis respiratorik, dehidrasi, anemia, sinkop, anoreksia, pusing, dsb)</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosis-awal text-center">3</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosis-awal" required value="2">Gangguan perilaku/psikiatri</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosis-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosis-awal" required value="1">Diagnosis lainnya</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosis-awal text-center">1</div>
                                                    </td>  
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="3">Gangguan Kognitif</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-kognitif-awal" required value="3">Tidak menyadari keterbatasan dirinya</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-kognitif-awal text-center">3</div>
                                                    </td>
                                                    <td rowspan="3" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-kognitif-awal text-center" data-skor="0">0</div>
                                                    </td>       
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-kognitif-awal" required value="2">Lupa akan adanya keterbatasan</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-kognitif-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-kognitif-awal" required value="1">Orientasi baik terhadap diri sendiri</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-kognitif-awal text-center">1</div>
                                                    </td>  
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="4">Faktor Lingkungan</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-faktorlingkungan-awal" required value="4">Riwayat jatuh dari tempat tidur saat bayi-balita/bayi yang diletakkan di tempat tidur dewasa</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-faktorlingkungan-awal text-center">4</div>
                                                    </td>
                                                    <td rowspan="4" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-faktorlingkungan-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-faktorlingkungan-awal" required value="3">Pasien mengenakan alat bantu/bayi diletakkan di dalam tempat tidur bayi</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-faktorlingkungan-awal text-center">3</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-faktorlingkungan-awal" required value="2">Pasien diletakkan di tempat tidur</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-faktorlingkungan-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-faktorlingkungan-awal" required value="1">Area di luar ruang rawat</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-faktorlingkungan-awal text-center">1</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="3">Pembedahan / Sedasi / Anestesi</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-pembedahan-awal" required value="3">Dalam 24 jam</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-pembedahan-awal text-center">3</div>
                                                    </td>
                                                    <td rowspan="3" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-pembedahan-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-pembedahan-awal" required value="2">Dalam 48 jam</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-pembedahan-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-pembedahan-awal" required value="1"> > 48 jam atau tidak menjalani tindakan tersebut</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-pembedahan-awal text-center">1</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="3">Penggunaan medikamentosa</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-medikamentosa-awal" required value="3">Penggunaan multipel: sedatif, obat hipnosis, barbiturat, fenotiazin, antidepresan, pencahar, diuretik, narkoba</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-medikamentosa-awal text-center">3</div>
                                                    </td>
                                                    <td rowspan="3" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-medikamentosa-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-medikamentosa-awal" required value="2">Penggunaan salah satu obat diatas</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-medikamentosa-awal text-center">2</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-medikamentosa-awal" required value="1">Penggunaan medikasi lainnya/tidak ada medikasi</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-medikamentosa-awal text-center">1</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <th colspan="3" class="text-center">
                                                      Total Skor
                                                    </th>
                                                    <td style="vertical-align: middle;">
                                                      <div class="total-skor-humptydumpty text-center" data-totalskor="0">0</div>
                                                    </td> 
                                                  </tr>

                                                </table>
                                                <div class="form-group">
                                                  <label>Interprestasi Hasil</label>
                                                  <div class="radio-form">
                                                    <label class="radio-inline"><input type="radio" name="interprestasihumptydumty_hasil_awal" required value="12-23"> Risiko Tinggi (RT) : Total skor 12-23</label><br>
                                                    <label class="radio-inline"><input type="radio" name="interprestasihumptydumty_hasil_awal" required value="7-11"> Risiko Sedang (RS) : Total skor 7-11</label><br>
                                                    <label class="radio-inline"><input type="radio" name="interprestasihumptydumty_hasil_awal" required value="0-7"> Tidak Risiko (TR)  : Total skor 0-7</label>
                                                  </div>
                                                </div>
                                              </div> 
                                              <!-- end Humpty Dumpty form --> 
                                              <hr>
                                              <!-- Morse Fall Scale form -->
                                              <div class="morsefall-form">
                                                <div class="text-center" style="margin:15px 0px;"><b>Morse Fall Scale (Digunakan pada Pasien Usia > 18 - < 60 Tahun)</b></div>
                                                <table class="table table-bordered table-hover table-striped">
                                                  <tr>
                                                    <th>Faktor Risiko</th>
                                                    <th>Kriteria</th>
                                                    <th>Nilai</th>
                                                    <th>Skor</th>
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="2">Riwayat Jatuh (Dalam 3 bulan terakhir)</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-riwayatjatuh-awal" required value="25">Ya</label>
                                                    </td>     
                                                    <td>
                                                      <div class="skor-resikojatuh-riwayatjatuh-awal text-center">25</div>
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-riwayatjatuh-awal text-center" data-skor="0">0</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-riwayatjatuh-awal" required value="0">Tidak</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-riwayatjatuh-awal text-center">0</div>
                                                    </td>                                              
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="2">Diagnosa Sekunder (2 diagnosa medis)</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosasekunder-awal" required value="15">Ya</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosasekunder-awal text-center">15</div>
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-diagnosasekunder-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-diagnosasekunder-awal" required value="0">Tidak</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-diagnosasekunder-awal text-center">0</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="3">Alat Bantu Jalan</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-alatbantu-awal" required value="30">Berpegangan pada benda-benda di sekitar (kursi, lemari, meja, dinding)</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-alatbantu-awal text-center">30</div>
                                                    </td>
                                                    <td rowspan="3" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-alatbantu-awal text-center" data-skor="0">0</div>
                                                    </td>        
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-alatbantu-awal" required value="15">Kruk / tongkat / walker</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-alatbantu-awal text-center">15</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-alatbantu-awal" required value="0">Bed rest / dibantu perawat</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-alatbantu-awal text-center">0</div>
                                                    </td>  
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="2">Terpasang Infus</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-infus-awal" required value="20">Ya</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-infus-awal text-center">20</div>
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-infus-awal text-center" data-skor="0">0</div>
                                                    </td>       
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-infus-awal" required value="0">Tidak</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-infus-awal text-center">0</div>
                                                    </td>  
                                                  </tr>
                                                  
                                                  <tr>
                                                    <td rowspan="3">Gaya Berjalan</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-gayajalan-awal" required value="20">Terganggu / tidak normal (pincang / diseret)</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-gayajalan-awal text-center">20</div>
                                                    </td>
                                                    <td rowspan="3" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-gayajalan-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-gayajalan-awal" required value="10">Lemah (tidak bertenaga)</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-gayajalan-awal text-center">10</div>
                                                    </td>  
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-gayajalan-awal" required value="0">Normal / bed rest / immobile</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-gayajalan-awal text-center">0</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <td rowspan="2">Status Mental</td>                                               
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-mental-awal" required value="15">Mengalami keterbatasan daya ingat</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-mental-awal text-center">15</div>
                                                    </td>
                                                    <td rowspan="2" style="vertical-align: middle;">
                                                      <div class="skors-resikojatuh-mental-awal text-center" data-skor="0">0</div>
                                                    </td>         
                                                  </tr>
                                                  <tr>
                                                    <td>
                                                      <label class="radio-inline" style="padding-top:0;"><input type="radio" name="resikojatuh-mental-awal" required value="0">Orientasi baik terhadap kemampuan diri sendiri</label>
                                                    </td>
                                                    <td>
                                                      <div class="skor-resikojatuh-mental-awal text-center">0</div>
                                                    </td>  
                                                  </tr>

                                                  <tr>
                                                    <th colspan="3" class="text-center">
                                                      Total Skor
                                                    </th>
                                                    <td style="vertical-align: middle;">
                                                      <div class="total-skor-morsefall text-center" data-totalskor="0">0</div>
                                                    </td> 
                                                  </tr>

                                                </table>
                                                <div class="form-group">
                                                  <label>Interprestasi Hasil</label>
                                                  <div class="radio-form">
                                                    <label class="radio-inline"><input type="radio" name="interprestasimorsefall_hasil_awal" required value=">45"> Risiko Tinggi (RT) : Total skor > 45</label><br>
                                                    <label class="radio-inline"><input type="radio" name="interprestasimorsefall_hasil_awal" required value="25-44"> Risiko Sedang (RS) : Total skor 25-44</label><br>
                                                    <label class="radio-inline"><input type="radio" name="interprestasimorsefall_hasil_awal" required value="0-24"> Tidak Risiko (TR)  : Total skor 0-24</label>
                                                  </div>
                                                </div>
                                              </div> 
                                              <!-- end Morse Fall Scale form --> 
                                          
                                            </td>
                                          </tr>
                                          
                                          <!-- end Pengkajian resiko jatuh -->
                                        </table>
                                            </div>
                                            <!-- end col-md-12 -->

                                          </div>
                                          <!-- end row -->

                                      </form>
                                      <!-- End FORM Pengkajian Perawatan -->

                                    </div>
                                    <!-- End Panel Body -->

                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="ulangAccordion">
                          <div class="panel panel-default ">
                            <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#ulangAccordion" data-target="#ulangri">
                              <h4 class="panel-title">
                                <a href="#" class="ing">Pengkajian Ulang</a>
                              </h4>
                            </div>

                            <div id="ulangri" class="panel-collapse collapse" style="height: 0px;">

                              <div class="panel panel-default square">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#ulangri" data-target="#ulangny">
                                  <h4 class="panel-title">
                                    <a href="#" class="ing">Pengkajian Ulang Nyeri</a>
                                  </h4>
                                </div>

                                <div id="ulangny" class="panel-collapse collapse" style="height: 0px;">
                                  <div class="panel-body">
                                    <table style="width : 100%;">
                                      <thead>
                                        <tr>
                                          <th rowspan="3" class="text-center" style="border: 1px solid black; width : 10%;">Tanggal/Jam Pengkajian</th>
                                          <th rowspan="3" class="text-center" style="border: 1px solid black; width : 15%;">Jenis Pengkajian</th>
                                          <th colspan="5" class="text-center" style="border: 1px solid black;">Intervensi Nyeri</th>
                                          <th rowspan="3" class="text-center" style="border: 1px solid black; width : 15%;">Nama Petugas</th>
                                          <th rowspan="3" class="text-center" style="border: 1px solid black; width : 10%;">Rencana Waktu <br> Kaji Ulang (Tanggal/Jam)</th>
                                        </tr>
                                        <tr>
                                          <th colspan="4" class="text-center" style="border: 1px solid black;">Farmakologi</th>
                                          <th rowspan="2" class="text-center" style="border: 1px solid black; width : 10%;">Non Farmakologi</th>
                                        </tr>
                                        <tr>
                                          <th class="text-center" style="border: 1px solid black;">Jam</th>
                                          <th class="text-center" style="border: 1px solid black;">Obat</th>
                                          <th class="text-center" style="border: 1px solid black;">Dosis</th>
                                          <th class="text-center" style="border: 1px solid black;">Rute</th>
                                        </tr>
                                      </thead>
                                      <tbody style="border: 1px solid black;">
                                        <tr height="40px">
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="width : 100%;">
                                      <tr height="30px">
                                        <td align="right">
                                          <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                        <td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                              </div>

                              <div class="panel panel-default square">
                                <div class="panel-heading accordion-toggle question-toggle collapsed"  data-toggle="collapse" data-parent="#ulangri" data-target="#ulangko">
                                  <h4 class="panel-title">
                                    <a href="#" class="ing">Pengkajian Ulang Risiko Jatuh</a>
                                  </h4>
                                </div>

                                <div id="ulangko" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                  <label>Pilih salah satu sesuai kategori usia pasien</label>
                                  <div class="radio">
                                    <label class="radio" data-toggle="collapse" data-parent="#ulangko" data-target="#ulanghum"><input type="radio" class="humptydumpty" name="risikojatuh[]" required value="0"><i>Humpty Dumpty </i> (Digunakan pada pasien anak usia 0 - &le;18 tahun)</label>
                                    <label class="radio" data-toggle="collapse" data-parent="#ulangko" data-target="#ulangmor"><input type="radio" class="morsefall" name="risikojatuh[]" required value="1"><i>Morse Fall Scale </i>(Digunakan pada pasien usia >18 - &lt;60 tahun)</label>
                                    <label class="radio" data-toggle="collapse" data-parent="#ulangko" data-target="#ulangon"><input type="radio" class="ontariomodified" name="risikojatuh[]" required value="2"><i>Ontario Modified Stratify Sidney Scoring </i> (Digunakan pada pasien lansia usia &ge;60 tahun)</label>
                                  </div>
                                </div>
                                </div>
                                
                                <div id="ulanghum" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body rjhumpty">
                                  <table style="width : 100%;">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="col-md-12">
                                                  <table style="width : 100%;">
                                                    <thead>
                                                      <tr>
                                                        <th class="text-center" colspan="4"><i>Humpty Dumpty </i> (Digunakan pada pasien anak usia 0 - &le;18 tahun)</th>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <th class="text-center" style="border: 1px solid black; width : 18%;">Parameter</th>
                                                        <th class="text-center" style="border: 1px solid black;">Kriteria</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 5%;">Nilai</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 50px;">Skor Pasien</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr style="border: 1px solid black;">
                                                        <td rowspan="4" style="border: 1px solid black;" class="text-center">Usia</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="4" id="" name="usia[]">&nbsp;< 3 tahun</td>
                                                        <td style="border: 1px solid black;" class="text-center">4</td>
                                                        <td rowspan="4" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="3" id="" name="usia[]">&nbsp;3 - 7 tahun</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="2" id="" name="usia[]">&nbsp;7-13 tahun</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="1" id="" name="usia[]">&nbsp;&ge; 13 tahun</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td class="text-center" rowspan="2" style="border: 1px solid black;">Jenis Kelamin</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="2" id="" name="jk[]">&nbsp;Laki-Laki</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="1" id="" name="jk[]">&nbsp;Perempuan</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="4" style="border: 1px solid black;">Diagnosis</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="diagnosis[]" id="" value="4">&nbsp;Diagnosis neurologi</td>
                                                        <td style="border: 1px solid black;" class="text-center">4</td>
                                                        <td rowspan="4" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="diagnosis[]" id="" value="3">&nbsp;Perubahan oksigenisasi (diagnosis respiratorik, dehidrasi, anemia, sinkop, anoreksia, pusing. dsb</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="diagnosis[]" id="" value="2">&nbsp;Gangguan perilaku/ psikiatri</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="diagnosis[]" id="" value="1">&nbsp;Diagnosis lainnya</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="3" style="border: 1px solid black;">Gangguan kognitif</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="kognitif[]" id="" value="3">&nbsp;Tidak menyadari keterbatasan dirinya</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                        <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="kognitif[]" id="" value="2">&nbsp;Lupa akan adanya keterbatasan</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="kognitif[]" id="" value="1">&nbsp;Orientasi baik terhadap diri sendiri</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="4" style="border: 1px solid black;">Faktor lingkungan</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="lingkungan[]" id="" value="4">&nbsp;Riwayat jatuh dari tempat tidur saat bayi-balita / bayi yang diletakkan di tempat tidur dewasa</td>
                                                        <td style="border: 1px solid black;" class="text-center">4</td>
                                                        <td rowspan="4" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="lingkungan[]" id="" value="3">&nbsp;Pasien mengenakan alat bantu / bayi diletakkan di dalam tempat tidur bayi</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="lingkungan[]" id="" value="2">&nbsp;Pasien diletakkan di tempat tidur</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="lingkungan[]" id="" value="1">&nbsp;Area di luar ruang rawat</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="3" style="border: 1px solid black;">Pembedahan / Sedasi / Anestesi</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="psa[]" id="" value="3">&nbsp;Dalam 24 jam</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                        <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="psa[]" id="" value="2">&nbsp;Dalam 48 jam</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="psa[]" id="" value="1">&nbsp;> 48 jam atau tidak menjalani tindakan tersebut</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="3" style="border: 1px solid black;">Penggunaan medikamentosa</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="medika[]" id="" value="3">&nbsp;Penggunaan multipel: sedatif, obat hipnosis, barbiturat, fenotiazin, antidepresan, pencahar, diuretik, narkoba</td>
                                                        <td style="border: 1px solid black;" class="text-center">3</td>
                                                        <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="medika[]" id="" value="2">&nbsp;Penggunaan salah satu obat diatas</td>
                                                        <td style="border: 1px solid black;" class="text-center">2</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="medika[]" id="" value="1">&nbsp;Penggunaan medikasi lainnya/tidak ada medikasi</td>
                                                        <td style="border: 1px solid black;" class="text-center">1</td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" class="text-center" style="border: 1px solid black;"><strong>Total Skor</strong></td>
                                                        <td style="border: 1px solid black;" class="text-center"></td>
                                                        <td style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                  <table>
                                                    <tbody>
                                                      <tr>
                                                        <td colspan="3">Interpretasi hasil:</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="3" onclick="return false;">&nbsp;Risiko Tinggi (RT)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor 12-23</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="2" onclick="return false;">&nbsp;Risiko Sedang (RS)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor 7-11</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="1" onclick="return false;">&nbsp;Tidak Risiko (TR)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor 0-7</td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </td>
                                            </tr>
                                          </tbody>
                                  </table>
                                </div>
                                </div>

                                <div id="ulangmor" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body rjmorse">
                                  <table style="width : 100%;">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="col-md-12">
                                                  <table style="width : 100%;">
                                                    <thead>
                                                      <tr>
                                                        <th class="text-center" colspan="4"><i>Morse Fall Scale </i>(Digunakan pada pasien usia >18 - &lt;60 tahun)</th>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <th class="text-center" style="border: 1px solid black; width : 18%;">Faktor Risiko</th>
                                                        <th class="text-center" style="border: 1px solid black;">Kriteria</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 5%;">Nilai</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 50px;">Skor Pasien</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr style="border: 1px solid black;">
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center">Riwayat Jatuh (Dalam 3 bulan terakhir)</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="25" id="" name="jatuh[]">&nbsp;Ya</td>
                                                        <td style="border: 1px solid black;" class="text-center">25</td>
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="0" id="" name="jatuh[]">&nbsp;Tidak</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td class="text-center" rowspan="2" style="border: 1px solid black;">Diagnosa Sekunder (2 diagnosa medis)</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="15" id="" name="sekunder[]">&nbsp;Ya</td>
                                                        <td style="border: 1px solid black;" class="text-center">15</td>
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr style="border: 1px solid black;">
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="0" id="" name="sekunder[]">&nbsp;Tidak</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="3" style="border: 1px solid black;">Alat Bantu Jalan</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="alat[]" id="" value="30">&nbsp;Berpegangan pada benda-benda di sekitar (kursi, lemari, meja, dinding)</td>
                                                        <td style="border: 1px solid black;" class="text-center">30</td>
                                                        <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="alat[]" id="" value="15">&nbsp;Kruk / tongkat / walker</td>
                                                        <td style="border: 1px solid black;" class="text-center">15</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="alat[]" id="" value="0">&nbsp;Bed rest / dibantu perawat</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="2" style="border: 1px solid black;">Terpasang Infus</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="infus[]" id="" value="20">&nbsp;Ya</td>
                                                        <td style="border: 1px solid black;" class="text-center">20</td>
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="infus[]" id="" value="0">&nbsp;Tidak</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="3" style="border: 1px solid black;">Gaya Berjalan</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="gaya[]" id="" value="20">&nbsp;Terganggu / tidak normal (pincang / diseret)</td>
                                                        <td style="border: 1px solid black;" class="text-center">20</td>
                                                        <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="gaya[]" id="" value="10">&nbsp;Lemah (tidak bertenaga)</td>
                                                        <td style="border: 1px solid black;" class="text-center">10</td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="gaya[]" id="" value="0">&nbsp;Normal / bed rest / immobile</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr>
                                                        <td class="text-center" rowspan="2" style="border: 1px solid black;">Status Mental</td>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mental[]" id="" value="15">&nbsp;Mengalami keterbatasan daya ingat</td>
                                                        <td style="border: 1px solid black;" class="text-center">15</td>
                                                        <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                      <tr>
                                                        <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mental[]" id="" value="0">&nbsp;Orientasi baik terhadap kemampuan diri sendiri</td>
                                                        <td style="border: 1px solid black;" class="text-center">0</td>
                                                      </tr>
                                                      <tr>
                                                        <td colspan="2" class="text-center" style="border: 1px solid black;"><strong>Total Skor</strong></td>
                                                        <td style="border: 1px solid black;" class="text-center"></td>
                                                        <td style="border: 1px solid black;" class="text-center"></td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                  <table>
                                                    <tbody>
                                                      <tr>
                                                        <td colspan="3">Interpretasi hasil:</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="3" onclick="return false;">&nbsp;Risiko Tinggi (RT)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor &ge; 45</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="2" onclick="return false;">&nbsp;Risiko Sedang (RS)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor 25-44</td>
                                                      </tr>
                                                      <tr>
                                                        <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="1" onclick="return false;">&nbsp;Tidak Risiko (TR)</td>
                                                        <td>:</td>
                                                        <td>&nbsp;Total skor 0-24</td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </td>
                                            </tr>
                                          </tbody>
                                  </table>
                                </div>
                                </div>

                                <div id="ulangon" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body rjontario">
                                                        <table style="width : 100%;">
                                                          <thead>
                                                              <tr>
                                                                <th class="text-center" colspan="4"><i>Ontario Modified Stratify Sidney Scoring</i> (Digunakan pada pasien lansia usia &ge;60 tahun)</th>
                                                              </tr>
                                                              <tr>
                                                                <th class="text-center" style="border: 1px solid black; width: 15%;">Faktor Risiko</th>
                                                                <th class="text-center" style="border: 1px solid black; width: 70%;">Kriteria</th>
                                                                <th class="text-center" style="border: 1px solid black;" colspan="2">Nilai</th>
                                                                <th class="text-center" style="border: 1px solid black;">Skor Pasien</th>
                                                              </tr>
                                                          </thead>
                                                          <tbody>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;" rowspan="2">Riwayat Jatuh</td>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah Pasien datang ke rumah sakit karena jatuh</td>
                                                                <td style="border: 1px solid black; width : 6%;">&nbsp;&nbsp;<input type="radio" class="" name="riwayatjatuh1[]" value="1" id="">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="riwayatjatuh1[]" value="0" id="">&nbsp;Tidak</td>
                                                                <td style="border: 1px solid black; width : 4%;" rowspan="2" class="text-center"></td>
                                                                <td style="border: 1px solid black;" rowspan="2" class="text-center"></td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Jika tidak, apakah pasien mengalami jatuh dalam 2 bulan terakhir ini</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="riwayatjatuh2[]" value="1" id="">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="riwayatjatuh2[]" value="0" id="">&nbsp;Tidak</td>
                                                              </tr>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;" rowspan="3">Status Mental</td>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah Pasien delirium (tidak bisa mengambil keputusan, pola pikir, tidak teorganisir, gangguan daya ingat)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="statusmental1[]" value="1" id="">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="statusmental1[]" value="0" id="">&nbsp;Tidak</td>
                                                                <td style="border: 1px solid black;" class="text-center" rowspan="3"></td>
                                                                <td style="border: 1px solid black;" rowspan="3" class="text-center"></td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah pasien disorientasi (salah menyebutkan waktu, tempat, dan orang)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="statusmental2[]" value="1" id="">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="statusmental2[]" value="0" id="">&nbsp;Tidak</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah pasien mengalami agitasi (ketakutan, gelisah, dan cemas)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="statusmental3[]" value="1" id="">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="statusmental3[]" value="0" id="">&nbsp;Tidak</td>
                                                              </tr>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;" rowspan="3">Penglihatan</td>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah pasien memakai kacamata?</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="penglihatan1[]" id="" value="1">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="penglihatan1[]" id="" value="0">&nbsp;Tidak</td>
                                                                <td style="border: 1px solid black;" class="text-center" rowspan="3"></td>
                                                                <td style="border: 1px solid black;" rowspan="3" class="text-center"></td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah pasien mengeluhkan penglihatannya buram?</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="penglihatan2[]" id="" value="1">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="penglihatan2[]" id="" value="0">&nbsp;Tidak</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah pasien mempunyai glaukoma, katarak, atau degenerasi makula?</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="penglihatan3[]" id="" value="1">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="penglihatan3[]" id="" value="0">&nbsp;Tidak</td>
                                                              </tr>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;">Kebiasaan Berkemih</td>
                                                                <td style="border: 1px solid black;">&nbsp;Apakah terdapat perubahan perilaku berkemih? Frekuensi, urgensi, inkontinensia, nokturia</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="berkemih[]" id="" value="1">&nbsp;Ya<br>&nbsp;&nbsp;<input type="radio" class="" name="berkemih[]" id="" value="0">&nbsp;Tidak</td>
                                                                <td style="border: 1px solid black;" class="text-center"></td>
                                                                <td style="border: 1px solid black;" class="text-center"></td>
                                                              </tr>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;" rowspan="4">Transfer <br>(dari tempat tidur ke kursi, dan kembali ke tempat tidur lagi)</td>
                                                                <td style="border: 1px solid black;">&nbsp;Mandiri (boleh menggunakan alat bantu jalan)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="0" id="" name="transfer[]">&nbsp;0</td>
                                                                <td style="border: 1px solid black;" class="text-center" rowspan="4"></td>
                                                                <td style="border: 1px solid black;" class="text-center" rowspan="8"></td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Memerlukan sedikit bantuan (perawat) atau dalam pengawasan</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="1" id="" name="transfer[]">&nbsp;1</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Memerlukan bantuan yang nyata (2 orang)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="2" id="" name="transfer[]">&nbsp;2</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Tidak dapat duduk dengan seimbang, perlu bantuan total</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" value="3" id="" name="transfer[]">&nbsp;3</td>
                                                              </tr>
                                                              <tr>
                                                                <td class="text-center" style="border: 1px solid black;" rowspan="4">Mobilitas</td>
                                                                <td style="border: 1px solid black;">&nbsp;Mandiri (boleh menggunakan alat bantu)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="0">&nbsp;0</td>
                                                                <td style="border: 1px solid black;" class="text-center" rowspan="4"></td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Berjalan dengan bantuan 1 orang (verbal/fisik)</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="1">&nbsp;1</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Menggunakan kursi roda</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="2">&nbsp;2</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;">&nbsp;Imobilitas</td>
                                                                <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="3">&nbsp;3</td>
                                                              </tr>
                                                              <tr>
                                                                <td style="border: 1px solid black;" colspan="3" class="text-center"><strong>Total Skor</strong></td>
                                                                <td style="border: 1px solid black;" class="text-center"></td>
                                                                <td style="border: 1px solid black;" class="text-center"><br></td>
                                                              </tr>
                                                          </tbody>
                                                        </table>
                                                        <table>
                                                          <tbody>
                                                              <tr>
                                                                <td colspan="3">Interpretasi hasil:</td>
                                                              </tr>
                                                              <tr>
                                                                <td>&nbsp;&nbsp;<input type="checkbox" name="chckintepretasiomsss" class="chckintepretasiomsss" value="3" id="chckintepretasiomsss3">&nbsp;Risiko Tinggi (RT)</td>
                                                                <td>:</td>
                                                                <td>&nbsp;Total skor 17 - 30</td>
                                                              </tr>
                                                              <tr>
                                                                <td>&nbsp;&nbsp;<input type="checkbox" name="chckintepretasiomsss" class="chckintepretasiomsss" value="2" id="chckintepretasiomsss2">&nbsp;Risiko Sedang (RS)</td>
                                                                <td>:</td>
                                                                <td>&nbsp;Total skor 6 - 16</td>
                                                              </tr>
                                                              <tr>
                                                                <td>&nbsp;&nbsp;<input type="checkbox" name="chckintepretasiomsss" class="chckintepretasiomsss" value="1" id="chckintepretasiomsss1">&nbsp;Tidak Risiko (TR)</td>
                                                                <td>:</td>
                                                                <td>&nbsp;Total skor 0 -5</td>
                                                              </tr>
                                                          </tbody>
                                                        </table>
                                </div>
                                </div>

                                <!-- <div id="intervensirj" class="panel-collapse collapse" style="height: 0px;"> -->
                                <div class="panel-body">
                                  <table style="width : 100%;">
                                    <tbody>
                                      <tr>
                                        <td colspan="3"><label>Intervensi Risiko Jatuh</label></td>
                                      </tr>
                                      <tr>
                                        <td>Risiko Sedang</td>
                                        <td colspan="2">Risiko Tinggi</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Pastikan pasien terpasang gelang kuning</td>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Lakukan semua pencegahan jatuh risiko sedang</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Pasien diorientasikan kondisi lingkungan/ruangan</td>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Libatkan keluarga untuk selalu menunggu dan mengawasi pasien</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Pengecekan "BEL" mudah dijangkau</td>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Beri penjelasan ulang tentang pencegahan jatuh untuk penunggu yang berbeda</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Naikkan pagar pengaman tempat tidur</td>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Lakukan pengkajian ulang setiap 1x24 jam</td>
                                      </tr>
                                      <tr>
                                        <td colspan="3" style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;<i>Handrail</i> mudah dijangkau dan kokoh</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Roda tempat tidur berada pada posisi terkunci</td>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Tidak Berisiko - Tidak ada intervensi</td>
                                      </tr>
                                      <tr>
                                        <td colspan="3" style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Pastikan tanda pasien risiko jatuh terpasang pada atas tempat tidur pasien</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Pastikan lampu menyala pada saat malam hari</td>
                                        <td colspan="2">Nama Petugas <input type="text" name="" value=""></input></td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox" class="" name="" id="" value="">&nbsp;Berikan edukasi pencegahan jatuh pada pasien / keluarga</td>
                                        <td>Rencana Waktu Kaji Ulang (Tanggal dan Jam)</td>
                                        <td><input type="text" name="" value=""></input></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Lakukan pengkajian ulang setiap 1x24 jam</td>
                                        <td><input type="text" name="" value=""></input></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table style="width : 100%;">
                                    <tr height="30px">
                                      <td align="right">
                                        <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                      <td>
                                    </tr>
                                  </table>
                                </div>
                                <!-- </div> -->

                              </div>
                              <div class="panel panel-default square">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#ulangri" data-target="#ulangfu">
                                  <h4 class="panel-title">
                                    <a href="#" class="ing">Pengkajian Ulang Status Fungsional</a>
                                  </h4>
                                </div>

                                <div id="ulangfu" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                  <table style="width : 100%;">
                                          <tbody>
                                            <tr>
                                              <td>
                                                <div class="col-md-12">
                                                  <table style="width : 100%;">
                                                    <thead>
                                                      <tr style="border: 1px solid black;">
                                                        <th class="text-center" style="border: 1px solid black; width : 18%;">Item yang Diniali</th>
                                                        <th class="text-center" style="border: 1px solid black;">Kriteria</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 5%;">Point</th>
                                                        <th class="text-center" style="border: 1px solid black; width : 50px;">Skor</th>
                                                      </tr>
                                                    </thead>
                                                            <tbody>
                                                                <tr style="border: 1px solid black;">
                                                                  <td rowspan="3" style="border: 1px solid black;" class="text-center">Makan (<i>Feeding</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="makan[]" id="" value="0">&nbsp;Tidak mampu</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="makan[]" id="" value="1">&nbsp;Butuh bantuan memotong, mengoles mentega, dll</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio"  class="" name="makan[]" id="" value="2">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="2" style="border: 1px solid black;">Mandi (<i>Bathing</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mandi[]" id="" value="0">&nbsp;Tergantung pada orang lain</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mandi[]" id="" value="1">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="2" style="border: 1px solid black;">Perawatan diri (<i>Grooming</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="perawatan[]" id="" value="0">&nbsp;Membutuhkan bantuan orang lain</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="2" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="perawatan[]" id="" value="1">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="3" style="border: 1px solid black;">Berpakaian (<i>Dressing</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="berpakaian[]" id="" value="0">&nbsp;Tergantung pada orang lain</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="berpakaian[]" id="" value="1">&nbsp;Sebagian dibantu (misal mengancing baju)</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="berpakaian[]" id="" value="2">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" style="border: 1px solid black;" rowspan="3">Buang Air Kecil (<i>Bladder</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airkecil[]" id="" value="0">&nbsp;Inkontinensia atau pakai kateter dan tidak terkontrol</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td style="border: 1px solid black;" class="text-center" rowspan="3"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airkecil[]" id="" value="1">&nbsp;Kadang inkontinensia (maks 1X24 jam)</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airkecil[]" id="" value="2">&nbsp;Kontinensia teratur untuk lebih dari 7 hari</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="3" style="border: 1px solid black;">Buang Air Besar (<i>Bowel</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airbesar[]" id="" value="0">&nbsp;Inkontinensia (tidak teratur atau perlu enema)</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airbesar[]" id="" value="1">&nbsp;Kadang inkotinensia (sekali dalam seminggu)</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="airbesar[]" id="" value="2">&nbsp;Kontinensia teratur</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="3" style="border: 1px solid black;">Penggunaan Toilet (<i>Toilet Use</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="toilet[]" id="" value="0">&nbsp;Tergantung pada orang lain</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="3" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="toilet[]" id="" value="1">&nbsp;Membutuhkan bantuan tapi dapat melakukan beberapa hal sendiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr style="border: 1px solid black;">
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="toilet[]" id="" value="2">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="4" style="border: 1px solid black;">Transfer (<i>Transfer - Bed to chair and back</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="transfer[]" id="" value="0">&nbsp;Tidak mampu</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="4" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="transfer[]" id="" value="1">&nbsp;Butuh bantuan untuk bisa duduk</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="transfer[]" id="" value="2">&nbsp;Bantuan kecil</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="transfer[]" id="" value="3">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">3</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="4" style="border: 1px solid black;">Mobilitas (<i>Mobility - On level surfaces</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="0">&nbsp;<i>Immobile</i> (tidak mampu)</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="4" style="border: 1px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="1">&nbsp;Menggunakan kursi roda</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="2">&nbsp;Berjalan dengan bantuan satu orang</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="mobilitas[]" id="" value="3">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">3</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td class="text-center" rowspan="3" style="border: 1px solid black;">Naik Turun Tangga (<i>Stairs</i>)</td>
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="tangga[]" id="" value="0">&nbsp;Tidak mampu</td>
                                                                  <td style="border: 1px solid black;" class="text-center">0</td>
                                                                  <td rowspan="3" style="border: 1 px solid black;" class="text-center"></td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="tangga[]" id="" value="1">&nbsp;Membutuhkan bantuan</td>
                                                                  <td style="border: 1px solid black;" class="text-center">1</td>
                                                                </tr>
                                                                <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;">&nbsp;&nbsp;<input type="radio" class="" name="tangga[]" id="" value="2">&nbsp;Mandiri</td>
                                                                  <td style="border: 1px solid black;" class="text-center">2</td>
                                                                </tr>
                                                              <tr style="border: 1px solid black;">
                                                                  <td style="border: 1px solid black;" colspan="3" class="text-center"><strong>Total Skor</strong></td>
                                                                  <td style="border: 1px solid black;" class="text-center"></td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                                          <table>
                                                            <tbody>
                                                                <tr>
                                                                  <td colspan="3">Interpretasi hasil:</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="0" onclick="return false;">&nbsp;Mandiri</td>
                                                                  <td>:</td>
                                                                  <td>&nbsp;Skor 20</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="1" onclick="return false;">&nbsp;Ketergantungan ringan</td>
                                                                  <td>:</td>
                                                                  <td>&nbsp;Skor 12-19</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="2" onclick="return false;">&nbsp;Ketergantungan sedang</td>
                                                                  <td>:</td>
                                                                  <td>&nbsp;Skor 9-11</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="3" onclick="return false;">&nbsp;Ketergantungan berat</td>
                                                                  <td>:</td>
                                                                  <td>&nbsp;Skor 5-8</td>
                                                                </tr>
                                                                <tr>
                                                                  <td>&nbsp;&nbsp;<input type="checkbox" class="" name="" id="" value="4" onclick="return false;">&nbsp;Ketergantungan total</td>
                                                                  <td>:</td>
                                                                  <td>&nbsp;Skor 0-4</td>
                                                                </tr>
                                                            </tbody>
                                                          </table>
                                                      </div>
                                                    </td>
                                                </tr>
                                              </tbody>
                                  </table>
                                </div>
                                <div class="panel-body">
                                  <table style="width : 100%;">
                                    <tbody>
                                      <tr>
                                        <td colspan="3">Intervensi:</td>
                                      </tr>
                                      <tr>
                                        <td style="width : 60%;"><input type="checkbox" class="" name="" id="" value="">&nbsp;Edukasi keluarga dan libatkan keluarga pasien dalam membantu pasien melakukan aktivitas sehari-hari</td>
                                        <td colspan="2">Nama Petugas <input type="text" name="" value=""></input></td>
                                      </tr>
                                      <tr>
                                        <td><input type="checkbox" class="" name="" id="" value="">&nbsp;Konsultasikan pada DPJP jika skor pada indeks <i>Barthel</i> &ge; 8</td>
                                        <td>Rencana Waktu Kaji Ulang (Tanggal dan Jam)</td>
                                        <td><input type="text" name="" value=""></input></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Lainnya <input type="text" name="" value=""></input></td>
                                        <td><input type="text" name="" value=""></input></td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><input type="checkbox" class="" name="" id="" value="">&nbsp;Tidak ada intervensi</td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <table style="width : 100%;">
                                    <tr height="30px">
                                      <td align="right">
                                        <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                        <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                      <td>
                                    </tr>
                                  </table>
                                </div>
                                </div>
                              </div>

                              <div class="panel panel-default square">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#ulangri" data-target="#ulangzi">
                                  <h4 class="panel-title">
                                    <a href="#" class="ing">Pengkajian Ulang Gizi</a>
                                  </h4>
                                </div>
                              </div>
                              <br>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="pkhususAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#pkhususAccordion" data-target="#pkusus">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Pengkajian Populasi Khusus</a>
                                  </h4>
                                </div>
                                <div id="pkusus" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="cpptAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#cpptAccordion" data-target="#cppt">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">CPPT</a>
                                  </h4>
                                </div>
                                <div id="cppt" class="panel-collapse collapse" style="height: 0px;">
                                <div class="panel-body">
                                        <div class="col-md-12">
                                          <a class="btn btn-primary btn-sm" id="addnewcppt"><i class="fa fa-plus-circle"></i> Tambah CPPT</a>
                                          <table id="tb-cppt" style="width: 90%;">
                                            <tbody></tbody>                                                            
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="monitoringAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#monitoringAccordion" data-target="#monitoring">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Monitoring Pasien</a>
                                  </h4>
                                </div>
                                <div id="monitoring" class="panel-collapse collapse" style="height: 0px;">
                                  <div class="panel-body">
                                    <table style="width : 100%;">
                                      <thead height="30px">
                                        <tr>
                                          <th class="text-center" style="border: 1px solid black;">Jenis Monitoring</th>
                                          <th class="text-center" style="border: 1px solid black;">Hasil</th>
                                          <th class="text-center" style="border: 1px solid black;">Rencana Perawatan / Intervensi</th>
                                          <th class="text-center" style="border: 1px solid black;">PPA</th>
                                        </tr>
                                      </thead>
                                      <tbody style="border: 1px solid black;">
                                        <tr height="40px">
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="width : 100%;">
                                      <tr height="30px">
                                        <td align="right">
                                          <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                        <td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="rpelaksanaantinAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#rpelaksanaantinAccordion" data-target="#rpelaksanaantin">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Rencana Pelaksanaan Tindakan</a>
                                  </h4>
                                </div>
                                <div id="rpelaksanaantin" class="panel-collapse collapse" style="height: 0px;">
                                  <div class="panel-body">
                                    <table style="width : 100%;">
                                      <thead height="30px">
                                        <tr>
                                          <th class="text-center" style="border: 1px solid black;">Tanggal dan Jam</th>
                                          <th class="text-center" style="border: 1px solid black;">Masalah/Diagnosis</th>
                                          <th class="text-center" style="border: 1px solid black;">Tujuan dan Target</th>
                                          <th class="text-center" style="border: 1px solid black;">Rencana Penatalaksanaan</th>
                                        </tr>
                                      </thead>
                                      <tbody style="border: 1px solid black;">
                                        <tr height="40px">
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="width : 100%;">
                                      <tr height="30px">
                                        <td align="right">
                                          <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                        <td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="edukasiAccordion">
                            <div class="panel panel-default">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#edukasiAccordion" data-target="#edukasi">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Edukasi</a>
                                  </h4>
                                </div>
                                <div id="edukasi" class="panel-collapse collapse" style="height: 0px;">
                                  <div class="panel-body">
                                    <table style="width : 100%;">
                                      <thead height="30px">
                                        <tr>
                                          <th class="text-center" style="border: 1px solid black;">Tanggal dan Jam</th>
                                          <th class="text-center" style="border: 1px solid black;">Materi Edukasi</th>
                                          <th class="text-center" style="border: 1px solid black;">Pemberi Edukasi</th>
                                          <th class="text-center" style="border: 1px solid black;">Penerima Edukasi (Nama dan NIP)</th>
                                        </tr>
                                      </thead>
                                      <tbody style="border: 1px solid black;">
                                        <tr height="40px">
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="width : 100%;">
                                      <tr height="30px">
                                        <td align="right">
                                          <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                          <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                        <td>
                                      </tr>
                                    </table>
                                  </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="lpersalinanAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#lpersalinanAccordion" data-target="#lpersalinan">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Laporan Persalinan</a>
                                  </h4>
                                </div>
                                <div id="lpersalinan" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="panel-group" id="operasiAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed" data-toggle="collapse" data-parent="#operasiAccordion" data-target="#loperasi">
                                    <h4 class="panel-title">
                                        <a href="#" class="ing">Laporan Operasi</a>
                                  </h4>
                                </div>
                                <div id="loperasi" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    <!-- /.tab-pane -->
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="box rekonobat" style="border-color:  #1076ba;">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">

                      <div class="form-group">
                        <div class="panel-group" id="obatAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                      <table style="width : 100%;">
                                        <tbody>
                                          <tr>
                                            <td colspan="3"><label>Riwayat Alergi Obat</label></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 7%;"><input type="radio" class="" name="alergiobat[]" id="" value="0">&nbsp;Tidak</td>
                                            <td><input type="radio" class="" name="alergiobat[]" id="" value="1">&nbsp;Ya</td>
                                          </tr>
                                        </tbody>
                                      </table><br>
                                      <table style="width : 95%;" align="center">
                                        <thead height="30px">
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;">Nama Obat yang menyebabkan Alergi</th>
                                            <th class="text-center" style="border: 1px solid black;">Waktu Penggunaan Terakhir</th>
                                            <th class="text-center" style="border: 1px solid black;">Reaksi Alergi yang Muncul</th>
                                          </tr>
                                        </thead>
                                        <tbody style="border: 1px solid black;">
                                          <tr height="40px">
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table style="width : 95%;" align="center">
                                        <tr height="30px">
                                          <td align="right">
                                            <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                          <td>
                                        </tr>
                                      </table><br>
                                      <table style="width : 100%;">
                                        <tbody>
                                          <tr>
                                            <td colspan="3"><label>Riwayat Penggunaan Obat pada Pasien</label></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 7%;"><input type="radio" class="" name="penggunaanobat[]" id="" value="0">&nbsp;Tidak ada</td>
                                            <td><input type="radio" class="" name="penggunaanobat[]" id="" value="1">&nbsp;Ada</td>
                                          </tr>
                                        </tbody>
                                      </table><br>
                                      <table style="width : 95%;" align="center">
                                        <thead height="30px">
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;" rowspan="2">Nama Obat</th>
                                            <th class="text-center" style="border: 1px solid black;" colspan="3">Pemberian</th>
                                            <th class="text-center" style="border: 1px solid black;" rowspan="2">Waktu Pemberian Terakhir</th>
                                            <th class="text-center" style="border: 1px solid black;" colspan="2" rowspan="2">Obat Dilanjutkan di RS</th>
                                          </tr>
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;">Dosis</th>
                                            <th class="text-center" style="border: 1px solid black;">Frekuensi</th>
                                            <th class="text-center" style="border: 1px solid black;">Cara/Rute Pemberian</th>
                                          </tr>
                                        </thead>
                                        <tbody style="border: 1px solid black;">
                                          <tr height="40px">
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input type="radio" class="" name="rpo[]" value="1">&nbsp;Ya</input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input type="radio" class="" name="rpo[]" value="0">&nbsp;Tidak</input></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table style="width : 95%;" align="center">
                                        <tr height="30px">
                                          <td align="right">
                                            <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                          <td>
                                        </tr>
                                      </table><br>
                                      <table style="width : 100%;">
                                        <tbody>
                                          <tr>
                                            <td colspan="3"><label>Obat yang Digunakan Pasien di Rumah Sakit</label></td>
                                          </tr>
                                        </tbody>
                                      </table><br>
                                      <table style="width : 95%;" align="center">
                                        <thead height="30px">
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;" rowspan="2">Nama Obat yang Menyebabkan Alergi</th>
                                            <th class="text-center" style="border: 1px solid black;" colspan="3">Waktu Penggunaan Terakhir</th>
                                            <th class="text-center" style="border: 1px solid black;" colspan="2">Tanggal Pengobatan</th>
                                          </tr>
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;">Dosis</th>
                                            <th class="text-center" style="border: 1px solid black;">Frekuensi</th>
                                            <th class="text-center" style="border: 1px solid black;">Cara/Rute Pemberian</th>
                                            <th class="text-center" style="border: 1px solid black;">Mulai</th>
                                            <th class="text-center" style="border: 1px solid black;">Stop</th>
                                          </tr>
                                        </thead>
                                        <tbody style="border: 1px solid black;">
                                          <tr height="40px">
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"><input class="text" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <table style="width : 95%;" align="center">
                                        <tr height="30px">
                                          <td align="right">
                                            <a class="btn btn-xs btn-sm" href="#">Simpan</a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                                            <a href="javascript:void(0)" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i></a>
                                          <td>
                                        </tr>
                                      </table>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="box laboratorium" style="border-color:  #1076ba;">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                      <div class="form-group">
                        <div class="panel-group" id="laboratAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                      <table class="table table-striped" style="width : 80%;">
                                        <tbody>
                                          <tr>
                                            <td>Nama Pasien</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Dokter yang Meminta</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>No.RM</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Diagnosis</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>Tanggal Lahir</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Jenis Pemeriksaan</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>Alamat</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Tanggal/Jam</td>
                                            <td>
                                              <input class="form-control" style="width : 90%;" name="" value=""></input>
                                            </td>
                                            <td>
                                              <input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td>Ruangan</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td><input type="checkbox" class="" name="cito" id="" value="1">&nbsp;Cito</input></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table><hr>
                                      <table style="width : 100%;">
                                        <tbody>
                                          <tr>
                                            <td colspan="3" class="text-center">
                                              <label>Riwayat</label>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <br>
                                      <table style="width : 100%;" align="center">
                                        <thead height="30px">
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;">Tanggal dan Jam</th>
                                            <th class="text-center" style="border: 1px solid black;">Hasil</th>
                                            <th class="text-center" style="border: 1px solid black;">Dokter Penanggung Jawab</th>
                                            <th class="text-center" style="border: 1px solid black;">Dokter yang Meminta Pemeriksaan</th>
                                          </tr>
                                        </thead>
                                        <tbody style="border: 1px solid black;">
                                          <tr height="30px">
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="box radiologi" style="border-color:  #1076ba;">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                      <div class="form-group">
                        <div class="panel-group" id="radiologAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                      <table class="table table-striped" style="width : 80%;">
                                        <tbody>
                                          <tr>
                                            <td>Nama Pasien</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Dokter yang Meminta</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>No.RM</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Diagnosis</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>Tanggal Lahir</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Jenis Pemeriksaan</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                          </tr>
                                          <tr>
                                            <td>Alamat</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td>Tanggal/Jam</td>
                                            <td>
                                              <input class="form-control" style="width : 90%;" name="" value=""></input>
                                            </td>
                                            <td>
                                              <input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td>Ruangan</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td><input type="checkbox" class="" name="cito" id="" value="1">&nbsp;Cito</input></td>
                                            <td></td>
                                            <td></td>
                                          </tr>
                                        </tbody>
                                      </table><hr>
                                      <table style="width : 100%;">
                                        <tbody>
                                          <tr>
                                            <td colspan="3" class="text-center">
                                              <label>Riwayat</label>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                      <br>
                                      <table style="width : 100%;" align="center">
                                        <thead height="30px">
                                          <tr>
                                            <th class="text-center" style="border: 1px solid black;">Tanggal dan Jam</th>
                                            <th class="text-center" style="border: 1px solid black;">Hasil</th>
                                            <th class="text-center" style="border: 1px solid black;">Dokter Penanggung Jawab</th>
                                            <th class="text-center" style="border: 1px solid black;">Dokter yang Meminta Pemeriksaan</th>
                                          </tr>
                                        </thead>
                                        <tbody style="border: 1px solid black;">
                                          <tr height="30px">
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                            <td style="border: 1px solid black; border-bottom: none;" align="center"></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="box catatanakhir" style="border-color:  #1076ba;">
              <div class="box-body">
                <div class="col-md-12">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                      <div class="form-group">
                        <div class="panel-group" id="catatanAccordion">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                      <table class="table table-striped" style="width : 80%;">
                                        <tbody>
                                          <tr>
                                            <td>Nama</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td><input type="radio" class="" name="jk[]" id="" value="0">&nbsp;Laki-laki</input></td>
                                            <td><input type="radio" class="" name="jk[]" id="" value="1">&nbsp;Perempuan</input></td>
                                            <td>Ruang</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td>No.RM</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                            <td></td>
                                            <td>Kelas</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td>Tgl. Lahir</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tanggal Masuk</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td>Alamat</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                            <td></td>
                                            <td></td>
                                            <td>Tanggal Keluar</td>
                                            <td><input class="form-control" style="width : 90%;" name="" value=""></input></td>
                                          </tr>
                                        </tbody>
                                      </table><br>
                                      <table class="table" style="width : 100%;" cellpadding="7">
                                        <tbody>
                                          <tr>
                                            <td style="width : 15%;">Indikasi Rawat Inap</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Diagnosis</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Komorbiditas Lain</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Ringkasan Riwayat</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Ringkasan Pemeriksaan Fisik</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Hasil Pemeriksaan Penunjang</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Terapi Selama Rawat Inap</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Diagnosis Utama</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Diagnosis Sekunder</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Tindakan / Prosedur / Operasi</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Terapi Pulang</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Kondisi Saat Pulang</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Cara Keluar RS</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Anjuran / Instruksi dan Edukasi</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                          <tr>
                                            <td style="width : 15%;">Tindak Lanjut</td>
                                            <td><input class="form-control" style="width : 100%;" name="" value=""></input></td>
                                          </tr>
                                        </tbody>
                                      </table><br>
                                      <table style="width : 30%;">
                                        <tbody>
                                          <tr>
                                            <td><a class="btn btn-success" href="#" >Resume Pasien Pulang</a></td>
                                            <td><a class="btn btn-success" href="#" >Rujukan Pasien</a></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <center id="menuPendaftaranPoliklinik">
            <input type="submit" class="btn btn-primary btn-lg" name="simpan" value="Simpan Data">
            <a class="btn btn-danger btn-lg" href="<?= base_url('cpelayananranap/pemeriksaanranap');?>" >Batal</a>
            </center>
            <br>
        </form>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->