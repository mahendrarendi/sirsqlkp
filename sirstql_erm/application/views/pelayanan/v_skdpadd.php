<style>
    span.required{ color:red; }
    .btn{line-height:0;}
</style>
<div class="box">
    <div class="box-body">

        <div class="box-header" style="margin-bottom:10px;margin-top:10px;">
            <h4 style="text-align:center;font-weight:bold;">SURAT KETERANGAN DALAM PERAWATAN</h4>
        </div>

        <form action="<?= base_url('cpelayanan/simpan_skdp'); ?>" method="POST">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>SKDP <span class="required">*</span></label>
                        <input required name="skdpke" type="text" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>No Kontrol <span class="required">*</span></label>
                        <input required name="nokontrol"  type="text" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>No RM <span class="required">*</span></label>
                        <input required name="norm" disabled type="text" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jenis Kelamin <span class="required">*</span></label>
                        <input required name="jk" disabled type="text" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nama Pasien <span class="required">*</span></label>
                        <input required name="namapasien" disabled type="text" autocomplete="off" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Nomor Kartu BPJS <span class="required">*</span></label>
                        <input required name="nokartu" disabled type="text" autocomplete="off" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Diagnosa</label>
                        <textarea  rows="5" name="diagnosa" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Terapi</label>
                        <textarea rows="5" name="terapi" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Kontrol Kembali Tanggal <span class="required">*</span></label>
                        <input name="tglkontrolberikutnya"  type="text" autocomplete="off" class="form-control qldate-skdp" required>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Surat Rujukan</label>
                        <!-- <input name="mulaitglsuratrujukan"  type="text" autocomplete="off" class="form-control qldate-skdp">
                        <span class="space-ql"> s.d. </span> 
                        <input name="selesaitglsuratrujukan"  type="text" autocomplete="off" class="form-control qldate-skdp"> -->
                        <div class="input-group input-daterange">
                            <input name="mulaitglsuratrujukan" type="text" autocomplete="off" class="form-control qldate-skdp" value="2012-04-05">
                            <div class="input-group-addon">to</div>
                            <input name="selesaitglsuratrujukan" type="text" autocomplete="off" class="form-control qldate-skdp" value="2012-04-19">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top:25px;text-align:center;">
                        <button type="button" class="btn btn-warning btn-lg" onclick="history.back()"><i class="fa fa-arrow-left"> </i> Batal</button>
                        <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"> </i> Simpan</button>
                    </div>
                </div>
                
            </div>
        </form>
    </div>
</div>