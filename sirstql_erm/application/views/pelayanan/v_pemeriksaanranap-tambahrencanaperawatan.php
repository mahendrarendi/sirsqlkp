<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
          <div class="box">
              
            <form action="" id="FormRencanaPerawatan" class="form-horizontal">
              <input id="refferurl" type="hidden" value="<?= $this->agent->referrer(); ?>"/>
              <div class="form-group">
                  <div class="col-md-1">
                  </div>
                  <div class="col-md-10">
                      <div class="bg bg-info padding2 text text-sm" style="margin-top:8px;">
                          <div class="col-md-6">
                            <b>Panduan Input Rencana Perawatan</b>
                            <ol>
                                <li>Pasien Rawat Inap</li>
                                Pilih nama pasien yang akan direncanakan perawatannya.
                                <li>Dokter DPJP</li>
                                Pilih nama dokter sebagai penanggung jawab pasien pada rencana perawatan.
                                <li>Waktu Perawatan</li>
                                Waktu Perawatan akan dilakukan.
                                <li>ICD Rencana Perawatan</li>
                                Tindakan perawatan yang akan diberikan ke pasien.
                                <li>Jumlah Tindakan</li>
                                Jumlah tindakan yang diberikan pada setiap perawatan dilakukan.
                                <li>Jumlah Per Hari</li>
                                Jumlah tindakan yang direncanakan akan diberikan berapa kali dalam sehari.
                                <li>Selama [ Hari ]</li>
                                Rencana perawatan akan dilakukan selama berapa hari.
                            </ol>
                          </div>
                          <div class="col-md-6">
                              <b>Panduan Input Obat/Bhp Include Perawatan</b>
                                <ol>
                                  <li>Obat/BHP</li>
                                  Obat atau BHP yang digunakan untuk tindakan yang direncanakan.
                                  <li>Jumlah Pesan</li>
                                  Jumlah pesan Obat atau BHP.
                                  <li>Satuan Pakai</li>
                                  Satuan pemakaian Obat atau BHP.
                                  <li>Jumlah Pakai (Per Tindakan)</li>
                                  Jumlah pemakaian Obat atau BHP untuk setiap dilakukan tindakan perawatan.
                                </ol>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                  </div>
              </div>
                
              <div class="form-group">
                  <div class="col-md-1">
                  </div>
                  
                  <div class="col-offset-1 col-sm-4">
                      <label>Pasien Rawat Inap</label>
                      <select class="select2 form-control" style="width:100%;" name="idinap">
                          <option value="0">Pilih</option>
                      </select>
                  </div>
                  <div class="col-sm-3">
                      <label>Dokter DPJP (Opsional)</label>
                      <select class="select2 form-control" style="width:100%;" name="idpegawaidokter">
                            <option value="0">Pilih</option>
                      </select>
                  </div>
              </div>
                
                <div class="form-group">
                    <div class="col-md-1">
                    </div>
                    <div class="col-sm-4"> 
                        <label>Waktu Perawatan</label> <small>(Tahun-Bulan-Hari Jam / yyyy-mm-dd H)</small>
                        <input type="text" name="tgl" class="datetimepicker form-control"/>
                    </div> 
                </div> 
                
                <div class="form-group">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <h4>Rencana Perawatan</h4>
                        <table id="rencanaperawatan" width="100%" class="table table-hover table-bordered table-striped" style="margin-top:4px;">                            
                        </table>
                    </div>
                </div>
              
                <div class="form-group">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <h4>Rencana Penggunaan Obat/BHP</h4>
                        <table id="rencanapenggunaanobatbhp" width="100%" class="table table-hover table-bordered table-striped" style="margin-top:4px;">                            
                        </table>
                    </div>
                </div>

              <center>                  
                  <a id="tambahrencanapenggunaanobat" class=" btn bg-maroon btn-md"><i class="fa fa-plus-square"></i> Tambah Rencana Penggunaan Obat/Bhp</a>
                  <a id="tambahrencanaperawatan" class=" btn btn-warning btn-md"><i class="fa fa-plus-square"></i> Tambah Rencana Perawatan</a>
                  <a class="btn btn-primary" onclick="menusimpanrencana()"><i class="fa fa-save btn-md"></i> Simpan Rencana</a>
                  <a class="btn btn-danger" onclick="menukembali()"><i class="fa fa-backward btn-md"></i> Kembali </a>
              </center>
            </form>
            <hr>
            </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->