  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div class="toolbar col-md-6"> 
<!--                <div class="col-md-1" style="padding-top: 2.2px;">
                    <small>Bulan Lahir</small><br>
                    
                    <input type="text" size="7" class="datepicker" id="tanggal">
                </div>-->
                <!--<div class="col-md-6">-->
                    <!--<label>&nbsp;</label><br>-->
                    <a class="btn btn-info btn-sm" id="tampil"> <i class="fa fa-desktop"></i> Tampil</a>
                    <a class="btn btn-warning btn-sm" id="refresh"><i class="fa  fa-refresh"></i> Refresh</a>
                    <a class="btn btn-primary btn-sm" id="buatsurat"> <i class="fa fa-plus-square"></i> Buat Surat Kelahiran</a>
                <!--</div>-->
                <!--<select class="select2 form-control" id="caripasien" ><option value="">Cari By Pasien</option></select>--> 
            </div>
              
            <table id="dtsuratkelahiran" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr class="bg bg-yellow-gradient">
                <th>No</th>
                <th>No Surat</th>
                <th>No.RM</th>
                <th>Nama</th>
                <th>Tgl Lahir</th>
                <th>Jenis Kelamin</th>
                <th>TB</th>
                <th>BB</th>
                <th>Anak Ke</th>
                <th>Tindakan/Kelainan Bawaan</th>
                <th>Nama Ayah</th>
                <th>Nama Ibu</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->