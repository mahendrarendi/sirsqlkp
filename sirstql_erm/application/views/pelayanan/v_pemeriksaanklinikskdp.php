<style>
    body .btn{ line-height:0 !important; }
</style>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                
                <div class="box-header">
                    <div class="button-header form-group">
                        <a href="<?= base_url('cpelayanan/skdp_add'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus-square"></i> Tambah SKDP</a>
                    </div>    
                </div>

                <table class="table table-striped ql-datatable">
                    <thead>
                        <tr class="header-table-ql">
                            <th width="5%">No</th>
                            <th>SKDP</th>
                            <th>Tanggal Terbit</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>SKDP 1</td>
                            <td>22/01/2022</td>
                            <td>
                                <button type="button" <?= ql_tooltip('Edit'); ?> class="btn btn-warning btn-sm edit-skdp"><i class="fa fa-edit"></i></button>
                                <button type="button" <?= ql_tooltip('Hapus'); ?> class="btn btn-danger btn-sm remove-skdp"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>