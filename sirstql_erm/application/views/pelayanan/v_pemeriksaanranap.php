<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
          
        <!--view perencanaan ranap-->
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="row">
            <div class="col-md-1">
                <small><b>Tgl. Awal</b></small>
                <input id="tanggalawal" size="7" class="datepicker"> 
             </div>
             <div class="col-md-1">
                 <small><b>Tgl. Akhir</b></small>
                 <input id="tanggalakhir" size="7" class="datepicker"> 
             </div>

             <div class="col-md-2">
                 <small>&nbsp;</small><br>
                 <a class="btn btn-info btn-sm" id="tampilpemeriksaanranap"><i class="fa fa-desktop"></i> Tampil</a>
                 <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
             </div>

            <div class="col-md-7">
                <small>&nbsp;</small><br>
                <a class="btn btn-success btn-sm" href="<?= base_url('cpelayananranap/tambahrencanaperawatan')?>"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan</a>
                <a class="btn btn-primary btn-sm" onclick="tambah_rencana_perawatan_paket()"><i class="fa fa-plus-circle"></i> Tambah Rencana Perawatan [Paket]</a>
                <a class="btn btn-info btn-sm" onclick="pemberian_bhp()"><i class="fa fa-eye"></i> Detail Pemberian Obat/BHP</a>
            </div>
        </div>
        
        <div class="box">
            <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                    
                    <div class="col-md-2 row">
                        <small><b>Nama Bangsal</b></small>
                        <select id="tampilbangsal" name="bangsal" class="form-control">
                            <option value="0">Pilih Bangsal</option>
                        </select>
                        
                    </div>
                    
                    <div class="col-md-3">
                        <small><b>Nama Pasien</b></small>
                        <select class="form-control select2" name="id_inap" onchange="caribypasien(this.value)" ><option value="0">Pilih Pasien</option></select>
                    </div>
                </div>
                
                    <div id="pasienbelumdiplot" class="col-md-12">
                        
                    </div>
                </div>
                <label class="label label-danger"><i class="fa fa-info-circle"></i> Perawatan yang terbiling sistem hanya yang sudah terlaksana.</label>
                 <table id="dtranap" class="table table-bordered table-striped table-hover " style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                  <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>NO.SEP</th>
                    <th>WAKTU PERAWATAN</th>
                    <th>DOKTER DPJP</th>
                    <th>BANGSAL</th>
                    <th>STATUS</th>                    
                    <th>DETAIL</th>
                    <th style="min-width:140px;">RENCANA</th>
                    <th style="width:110px;">AKSI</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tfoot>
                 </table>
                <div class="col-md-8 col-xs-12 row">
                <table class="table">
                    <tr><th colspan="4">#Keterangan</th></tr>                       
                    <tr><td><a class="btn btn-success btn-xs"><i class="fa fa-stethoscope"></i></a></td><td> Menu untuk mengakses halaman perawatan (set terlaksana/batal prencanaan dan input perawatan)</td></tr>
                    <tr><td><a class="btn btn-success btn-xs"><i class="fa fa-toggle-off"></i></a></td><td> Menu untuk merencakan obat pasien</td></tr>
                    <tr><td><a class="btn btn-primary btn-xs"><i class="fa fa-check"></i></a></td><td> Menu Untuk mengubah status perencanaan menjadi terlaksana  sesuai waktu yang direncanakan.</td></tr>
                    <tr><td><a class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></a></td><td> Menu Untuk mengubah status perencanaan menjadi terlaksana  sesuai waktu yang direncanakan. <strong><br>#jika perencanaan dibatalkan, semua rencana yang ada didalam perencanaan akan dibatalkan.</strong></td></tr> 
                    <tr><td><a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a></td><td> Menu Untuk melihat hasil perawatan.</td></tr>
                    <tr><td><a class="btn btn-danger btn-xs"><i class="fa  fa-times"></i></a></td><td> Menu Untuk mengubah status menjadi rencana setelah dibatalkan atau terlaksana. <strong><br>#jika sebelumnya sudah terlaksana maka rencana yang sudah terlaksana tidak diubah menjadi rencana.</strong></td></tr>
                </table>
                </div>
            </div>
            
        </div>
        <!-- end mode view -->
        
        <!--view administrasi ranap-->
        <?php }else if($mode=='administrasi'){ ?>
            
            <div class="">
                <select id="idbangsal" name="bangsal" onchange="cariByBangsal()" style="margin-right:4px;" class="sel2 col-xs-12 col-md-2" style="width:100%"></select>
                <select id="status" name="status" onchange="cariByStatus()" style="margin-right:4px;" class="sel2 col-xs-12 col-md-1" style="width:100%"><?php foreach ($status as $key=>$val){ echo "<option value='".$key."'>".$val."</option>"; } ?></select>
                <a class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a>
                <a class="btn btn-warning btn-sm" id="refresh"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
            <div class="box">
            
            <div class="box-body">
                <table id="listpasienranap" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                  <thead>
                  <tr class="header-table-ql">
                    <th>No</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>
                    <th>NO.SEP</th>
                    <th>WAKTU</th>
                    <th>CARA BAYAR</th>
                    <th>DOKTER DPJP</th>
                    <th>BANGSAL</th>
                    <th>PERAWATAN</th>
                    <th>BIAYA</th>
                    <th>STATUS</th>
                    <th width="130px;">AKSI</th>
                    <th class="none">TGL. LAHIR</th>
                    <th class="none">TELEPON</th>
                    <th class="none">ALAMAT</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tfoot>
                </table>
                <div class="col-xs-12 row">
                    <table class="table">
                        <tr><th colspan="2">#Keterangan</th><th colspan="6"></th></tr>                       
                        <tr  style="background-color: #ecf0f5;">
                            <td><a class="btn btn-xs btn-warning" ><i class="fa fa-user-md"></i></a></td>
                            <td> Menu Untuk Mengubah Dokter Penanggung Jawab</td>
                            <td><a class=" btn bg-purple btn-xs"><i class="fa fa-bed"></i></a></td>
                            <td> Menu Untuk Pindah Kamar Pasien</td>
                        </tr>
                        <tr  style="background-color: #ecf0f5;">
                            <td><a class="btn btn-success btn-xs"><i class="fa fa-stethoscope"></i></a></td>
                            <td> Menu Untuk melihat daftar perencanaan perawatan</td>                            
                            <td><a class="btn btn-warning btn-xs"><i class="fa fa-bed"></i></a></td>
                            <td> Menu untuk mengubah status bed menjadi kosong setelah dipulangkan</td>
                        </tr>
                        <tr  style="background-color: #ecf0f5;">
                            <td><a class=" btn btn-info btn-xs" ><i class="fa fa-history"></i></a></td>
                            <td> Menu Untuk Melihat Riwayat Perawatan Pasien</td>
                            <td><a class="btn btn-primary btn-xs"><i class="fa fa-check"></i></a></td>
                            <td> Menu Untuk menyelesaikan status rawat inap</td>
                        </tr>
                        <tr  style="background-color: #ecf0f5;">
                            <td><a class="btn btn-warning btn-xs"><i class="fa fa-toggle-off"></i></a></td>
                            <td> Menu Untuk merencanakan obat atau bhp</td>
                            <td><a class="btn btn-danger btn-xs"><i class="fa fa-minus-circle"></i></a></td>
                            <td> Menu Untuk membatalkan status rawat inap</td>
                        </tr>
                        
                        <tr  style="background-color: #ecf0f5;">
                            <td><a class="btn bg-maroon btn-xs"><i class="fa fa-trello"></i></a></td>
                            <td> Menu Untuk Input/Cetak Ringkasan Pasien Pulang (<i>DISCARGE SUMMARY</i>)</td>
                            <td><a class=" btn btn-primary btn-xs"><i class="fa fa-delicious"></i></a></td>
                            <td> Menu Untuk Input/Cetak Formulir INA-CBG</td>
                        </tr>
                        
                        <tr  style="background-color: #ecf0f5;">
                            <td><a idinap="1" class=" btn btn-info btn-xs"><i class="fa fa-align-justify"></i></a></td>
                            <td> Menu Untuk Cetak Catatan Terintegrasi</td>
                        </tr>
                        
                    </table>
                </div>
            </div>
            </div>
        
        <!--view pemeriksaan ranap-->
        <?php }else if( $mode=='periksa'){?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row col-md-12" style="padding-bottom:15px;">
                <div class="col-sm-3 row" id="ranap_profile">
                  <!-- Profile Image -->
                </div>

                <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile">
                <!-- detail profile -->
                </div>
                <div  class="col-md-4 ">

                    <div class="info-box bg-gold">
                        <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text"></span>
                          <span class="info-box-number">Waktu Pemeriksaan</span>

                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description" id="waktupemeriksaan">
                          </span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                </div>
                <div class="col-md-1">
                    <a id="riwayatperawatan" class=" btn btn-info btn-lg" ><i class="fa fa-history"></i> Riwayat Perawatan</a>
                </div>
            </div>
          <!-- /.box -->
        </div>
          
              
            <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
              <input type="hidden" name="idinap">
              <input type="hidden" name="idrencanamedispemeriksaan">
              <input type="hidden" name="idpendaftaran">
              <input type="hidden" name="idunit">
              <input type="hidden" name="norm">
              <input type="hidden" id="modepemeriksaan" name="modepemeriksaan">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Dokter Masuk</label>
                <div class="col-sm-4"><textarea name="dokter" rows="6" class="form-control" readonly></textarea></div>
                
                <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Anamnesa Masuk</label>
                <div class="col-sm-4">
                  <textarea  class="form-control textarea" name="anamnesa" placeholder="Anamnesa Dokter Saat Masuk" rows="6" readonly></textarea>
                </div>
              </div>

               <div class="form-group">
                <label for="" class="col-sm-2 control-label">Catatan Rawat Inap</label>
                <div class="col-sm-4">
                  <textarea class="form-control textarea" name="catatan" placeholder="Catatan Rawat Inap" rows="6"></textarea>
                </div>

                <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Keterangan Masuk</label>
                <div class="col-sm-4">
                  <textarea class="form-control textarea" name="keterangan" placeholder="Keterangan Pemeriksaan Saat Masuk" rows="6" readonly></textarea>
                </div>
              </div>
              
              <div class="form-group"><label for="" class="col-sm-2 control-label">&nbsp;</label></div>
              
              <!-- SOAP RANAP -->
              <div class="form-group">
                <label for="" class="col-sm-2 control-label" >SOAP</label>
                <div class=" col-sm-10">
                    <a class="btn btn-primary btn-sm" id="inputsoap"><i class="fa fa-plus-circle"></i> Input SOAP</a>
                <table class="table table-bordered table-bordered table-striped" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th width="100px">waktu input</th>
                      <th>Petugas</th>
                      <th>SIP</th>
                      <th>Profesi</th>
                      <th>SOA</th>
                      <th>P</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="listsoap">
                  </tbody>
                </table>
                </div>
              </div>
              
              
              
              <div class="form-group"><label for="" class="col-sm-2 control-label">&nbsp;</label></div>

              <!-- VitalSign ===================== -->
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >VitalSign</label>
                <div class="col-sm-4">
                  <select class="select2 form-control" name="carivitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'')">
                    <option value="0">Pilih</option>
                  </select>
                </div>

                <label for="" class="col-sm-2 control-label" >Paket VitalSign</label>
                  <div class="col-sm-3">
                  <select class="select2 form-control" name="paketvitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'ispaket')">
                    <option value="0">Pilih</option>
                    <?php 
                    if(!empty($data_paketvitalsign))
                    {
                      foreach ($data_paketvitalsign as $data) {
                        echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                      }
                    }
                    ?>
                  </select>
                </div>

              </div>
              <!-- LIST HASIL VitalSign -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan hasil</span>
                <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th>Status</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listVitalsign">
                  </tbody>
                </table>
                </div>
              </div>
              <!-- DIAGNOSA ===================== -->
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Diagnosa</label>
                <div class="col-sm-3">
                  <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="inputdelete_diagnosa(this.value,'input')">
                    <option value="0">Pilih</option>
                  </select>
                </div>
              </div>
              <!-- LIST HASIL TINDAKAN -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <table class="table table-striped table-hover" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>ICD</th>
                      <th>Nama ICD</th>
                      <th>Alias ICD</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listDiagnosa">
                  </tbody>
                </table>
                </div>
              </div>
              <!-- ============================== -->

              <!-- TINDAKAN ===================== -->
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Tindakan</label>
                <div class="col-sm-4">
                  <select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)">
                    <option value="0">Pilih</option>
                  </select>
                </div>
              </div>
              <!-- LIST HASIL TINDAKAN -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan jumlah</span>
                <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>ICD</th>
                      <th>Nama ICD / Nama Obat</th>
                      <th>Dokter (Opsional)</th>
                      <th width="5%">Jumlah / Pemakaian</th>
                      <th>Biaya / Satuan</th>
                      <th>Status</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listTindakan">
                  </tbody>
                </table>
                </div>
              </div>
              <!-- ============================== -->
              
              <!-- RADIOLOGI ===================== -->
              <div class="form-group">
                <label for="" class="col-sm-2 control-label" >Hasil Radiografi</label>
                <div class="col-sm-10">          
                  <table class="table" style="width: 92%; background-color: #eeeeee;" >
                      <tbody>
                        <tr>
                              <td colspan="2" id="viewHasilRadiografi">
                                <!--tampil dari js-->
                              </td>
                        </tr>
                        <?php if($this->pageaccessrightbap->checkAccessRight(V_MENU_MANAJEMENFILE_HASILRADIOGRAFI)){ ?>
                        <tr>
                            <td colspan="2" style="padding-top:15px;padding-left:20px;"><a id="btnRadiologiEfilm" class="btn btn-md btn-primary">Unggah hasil <i class="fa fa-upload"></i></a></td>
                        </tr>
                        <?php } ?>

                      </tbody>
                      </table>
                </div>
              </div>
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Hasil Expertise</label>
                 
                <div class="col-sm-9">
                  <span class="label label-default">*Input Hasil Expertise</span>
                  <a class="btn btn-xs btn-info" id="cetakexpertise"> <i class="fa fa-print"></i> Cetak Hasil Radiologi</a>
                  <textarea id="keteranganradiologi" class="form-control textarea" name="keteranganradiologi" rows="14"></textarea>
                </div>
              </div>
              
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Saran </label>
                <div class="col-sm-9">
                  <span class="label label-default">*Input Saran</span>
                  <textarea id="saranradiologi" class="form-control textarea" name="saranradiologi" rows="7"></textarea>
                </div>
              </div>
              
              <div class="form-group">
                  <label for="" class="col-sm-2 control-label" >&nbsp; </label>
                  <div class="col-sm-9">
                  <a class="btn btn-primary" id="simpanHasilExpertiseRanap"><i class="fa fa-save"></i> Simpan Hasil Expertise</a>
                  <br>
                  <small class="text text-info">#klik menu simpan hasil expertise, jika akan mencetak langsung hasil expertise.</small>
                </div>
              </div>

              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Elektromedik</label>
                <div class="col-sm-4">
                  <select class="select2 form-control" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                    <option value="0">Pilih</option>
                  </select>
                </div>
              </div>
              <!-- LIST HASIL RADIOLOGI -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>ICD</th>
                      <th>Nama ICD</th>
                      <th>Biaya</th>
                      <th>Status</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listRadiologi">
                  </tbody>
                </table>
                </div>
              </div>
              <!-- ============================== -->

              <!-- LABORATORIUM ===================== -->
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Keterangan</label>
                <div class="col-sm-4">
                <span class="label label-default">*Input keterangan laboratorium</span>
                  <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3"></textarea>
                </div>
              </div>
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Laboratorium</label>
                  <div class="col-sm-4">
                  <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)">
                    <option value="0">Pilih</option>
                  </select>
                </div>

                <label for="" class="col-sm-2 control-label" >Paket Laboratorium</label>
                  <div class="col-sm-3">
                  <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
                    <option value="0">Pilih</option>
                    <?php 
                    if(!empty($data_paket))
                    {
                      foreach ($data_paket as $data) {
                        echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
              <!-- LIST HASIL LABORATORIUM -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan hasil</span>
                <a id="waktupengerjaanlabranap" mode="nonpaket" class="btn btn-info btn-xs" ><i class="fa fa-clock-o"></i> Waktu Pengerjaan Non Paket</a> 
                <a id="cetakhasillaboratnonpaket" class="btn btn-info btn-xs" ><i class="fa fa-print"></i> Cetak Hasil Non Paket</a> 
                <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th>Biaya</th>
                      <th>Status</th>
                      <th width="16%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listLaboratorium">
                  </tbody>
                </table>
                </div>
              </div>
            <!-- END HASIL LABORATORIUM -->

            <!-- INPUT BHP -->
              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Keterangan</label>
                <div class="col-sm-4">
                  <span class="label label-default">#Input keterangan obat</span>
                  <textarea class="form-control" name="keteranganobat" rows="3"></textarea>
                </div>
              </div>

              <div class="form-group">
                 <label for="" class="col-sm-2 control-label" >Rencana BHP</label>
                <div class="col-sm-4"><select class="select2 form-control" name="caribhp" style="width:100%;" onchange="rencanaInputBhp(this.value)"><option value="0">Pilih</option></select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan pemakaian</span>
                <table class="table table-striped table-hover" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>No</th>
                      <th>Nama</th>
                      <th>Pemakaian</th>
                      <th>Satuan</th>
                      <th>@Harga</th>
                      <th>Subtotal</th>
                      <th>Status</th>
                      <th width="12%"></th>
                    </tr>
                  </thead>
                  <tbody id="viewDataBhp">
                  </tbody>
                </table>
                </div>
              </div>
              <!-- END INPUT BHP -->

              <center id="menuPendaftaranPoliklinik">
                <input type="submit" class="btn btn-warning btn-lg" name="simpan" value="Simpan Data">
                <input type="submit" class="btn btn-warning btn-lg" name="simpan" value="Simpan Data dan Set Terlaksana">
                <a class="btn btn-warning btn-lg" href="javascript:void(0)" onclick="history.back()" >Batal</a>
              </center>
            </form>
            <hr>
            </div>
        
        <!--view pemberian obat/bhp ranap-->
        <?php }else if( $mode=='bhp'){?>
        <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_bhp');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
            <div class="box">
                <div class="box-body">
                    <div class="row col-md-12" style="padding-bottom:15px;">
                        <div class="col-md-2">&nbsp;</div>
                        <div class="col-sm-3 row" id="ranap_profile"><!-- Profile Image --></div>
                        <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile"><!-- detail profile --></div>
                    </div>
                    
                    <input type="hidden" name="idinap">
                    <input type="hidden" name="idrencanamedispemeriksaan">
                    <input type="hidden" name="idpendaftaran">
                    <input type="hidden" name="idunit">
                  
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dokter Masuk</label>
                        <div class="col-sm-4"><textarea name="dokter" rows="6" class="textarea form-control" readonly></textarea></div>

                        <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Anamnesa Masuk</label>
                        <div class="col-sm-4"><textarea  class="textarea form-control" name="anamnesa" placeholder="Anamnesa Dokter Saat Masuk" rows="6" readonly></textarea></div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Catatan Rawat Inap</label>
                        <div class="col-sm-4"><textarea class="textarea form-control" name="catatan" placeholder="Catatan Rawat Inap" rows="6"></textarea></div>

                        <label for="" class="col-sm-1 control-label" style="margin-right: 16px;">Keterangan Masuk</label>
                        <div class="col-sm-4"><textarea class="textarea form-control" name="keterangan" placeholder="Keterangan Pemeriksaan Saat Masuk" rows="6" readonly></textarea></div>
                    </div>
                  
                    <!-- INPUT BHP -->
                    <div class="form-group">
                       <label for="" class="col-sm-2 control-label" >Keterangan Obat</label>
                      <div class="col-sm-4">
                        <textarea class="textarea form-control" name="keteranganobat" rows="4"></textarea>
                      </div>
                    </div>
                </div>
            </div>
            
    <!--detail pemberian obat bhp-->
    <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h5 class="text-bold">Detail Pemberian Obat/BHP</h5>
        </div>
        <div class="box-body">
            <table class="table table-hover table-bordered table-striped" style="width: 100%">
        <thead>
          <tr class="header-table-ql">
            <th>No</th>
            <th>Waktu /Status Perencanaan</th>
            <th>Nama Obat/BHP</th>
            <th>Diberikan</th>
            <th>Satuan Pakai</th>
            <th>@Harga</th>
            <th>Subtotal</th>
            <th>Status Pemberian</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="viewDetailObatBHP">
        </tbody>
      </table>
        </div>
    </div>


    <!--rencana obat / bhp pasien pulang-->
    <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h5 class="text-bold">Rencana Obat/BHP Pasien Pulang</h5>
        </div>
        <div class="box-body">
            <div class="col-xs-12 row">
               <div class="col-sm-4 col-xs-12" style="margin:0px; padding:0px; margin-bottom:8px;">
                 <select class="select2  bg bg-danger" name="caribhppasienpulang" style="width:100%;" onchange="inputBhpFarmasi(this.value,'pulang')"><option value="0">Input..</option></select>
               </div> &nbsp; &nbsp;
               <a class="btn btn-primary btn-sm" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
               <a class="btn btn-primary btn-sm" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
            </div>
            
            <table class="table table-hover table-bordered table-striped" style="width: 100%">
                <thead>
                  <tr class="header-table-ql">
                    <th>No</th>
                    <th>Obat/BHP</th>
                    <th width="8%">Resep</th>
                    <th>Kekuatan</th>
                    <th width="9%">DosisRacik</th>
                    <th>GrupRacik</th>
                    <th>Diresepkan</th>
                    <th>Tot.Harga</th>
                    <th>Tot.Diberikan</th>
                    <th>Diberikan</th>
                    <th>Penggunaan</th>
                    <th>AturanPakai</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="viewBhpRalan">
                </tbody>
              </table>
        </div>
    </div>
    
     
    <center id="menuPendaftaranPoliklinik">
      <input type="submit" class="btn btn-primary btn-lg" name="simpan" value="Simpan Data">
      <a class="btn btn-danger btn-lg" href="<?= base_url('cpelayananranap/pemeriksaanranap');?>" >Batal</a>
    </center>
    <br>
  </form>
        
    <?php }else if( $mode=='viewperiksa'){?>
      <div class="box">
      <div class="box-header">
      </div>
      <div style="border-top: 1px solid #f1f1f1;"></div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row col-md-12" style="padding-bottom:15px;">
            <div class="col-sm-3 row" id="ranap_profile">
              <!-- Profile Image -->
            </div>

            <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile">
            <!-- detail profile -->
            </div>
            <div  class="col-md-4 ">

                <div class="info-box bg-gold">
                    <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                    <div class="info-box-content">
                      <span class="info-box-text"></span>
                      <span class="info-box-number">Waktu Pemeriksaan</span>

                      <div class="progress">
                        <div class="progress-bar" style="width: 100%"></div>
                      </div>
                      <span class="progress-description" id="waktupemeriksaan">
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
            </div>
        </div>


        <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
          <input type="hidden" name="idinap">
          <input type="hidden" name="idrencanamedispemeriksaan">
          <input type="hidden" name="idpendaftaran">
          <input type="hidden" name="idunit">
          <div class="form-group">
            <label for="" class="col-sm-1 control-label"></label>
            <div class="col-sm-4"><div class="label label-warning">Dokter Masuk</div>
                <textarea disabled name="dokter" rows="6" class="textarea form-control" readonly></textarea></div>
            <label class="col-sm-2"></label>
            <div class="col-sm-4">
              <div class="label label-warning">Anamnesa Masuk</div>
              <textarea  class="textarea form-control" name="anamnesa" placeholder="Anamnesa Dokter Saat Masuk" rows="6" readonly></textarea>
            </div>
          </div>

           <div class="form-group">
            <label for="" class="col-sm-1 control-label"></label>
            <div class="col-sm-4">
              <div class="label label-warning">Catatan Rawat Inap</div>
              <textarea class="textarea form-control" name="catatan" placeholder="Catatan Rawat Inap" rows="6" disabled></textarea>
            </div>

            <label for="" class="col-sm-2 control-label" ></label>
            <div class="col-sm-4">
              <div class="label label-warning">Keterangan Masuk</div>
              <textarea disabled class="textarea form-control" name="keterangan" placeholder="Keterangan Pemeriksaan Saat Masuk" rows="6" readonly></textarea>
            </div>
          </div>

          <!-- LIST HASIL VitalSign -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">VitalSign</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>Parameter</th>
                  <th>Hasil</th>
                  <th>NDefault</th>
                  <th>NRujukan</th>
                  <th>Satuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listVitalsign">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- ============================== -->


          <!-- TINDAKAN ===================== -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tindakan</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>ICD</th>
                  <th>Nama ICD</th>
                  <th>Dokter (Opsional)</th>
                  <th width="5%">Jumlah</th>
                  <th>Biaya/Satuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listTindakan">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- ============================== -->
          <!-- RADIOLOGI ===================== -->

          <div class="form-group">
            <label for="" class="col-sm-2 control-label" >Hasil Radiografi</label>
            <div class="col-sm-10">          
              <table class="table" style="width: 92%; background-color: #eeeeee;" >
                  <tbody>
                    <tr>
                          <td colspan="2" id="viewHasilRadiografi">
                            <!--tampil dari js-->
                          </td>
                    </tr>
                  </tbody>
                  </table>
            </div>
          </div>

          <div class="form-group">
             <label for="" class="col-sm-2 control-label" >Hasil Expertise</label>
            <div class="col-sm-9">
              <span class="label label-warning">*Input Hasil Expertise</span>
              <textarea class="form-control textarea" name="keteranganelektromedik" rows="14" disabled></textarea>
            </div>
          </div>

          <div class="form-group">
             <label for="" class="col-sm-2 control-label" >Saran </label>
            <div class="col-sm-9">
              <span class="label label-warning">*Input Saran</span>
              <textarea class="form-control textarea" name="saranelektromedik" rows="7" disabled></textarea>
            </div>
          </div>

          <!-- Elektromedik ===================== -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Elektromedik</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>ICD</th>
                  <th>Nama ICD</th>
                  <th>Biaya</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listRadiologi">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>

          <!-- ============================== -->

          <!-- LABORATORIUM ===================== -->
          <div class="form-group">
             <label for="" class="col-sm-1" ></label>
            <div class="col-sm-4">
            <span class="label label-warning">Keterangan Laboratorium</span>
              <textarea class="textarea form-control" name="keteranganlaboratorium" rows="3" disabled></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Laboratorium</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <a id="cetakhasillaboratnonpaket" class="btn btn-info btn-xs" ><i class="fa fa-print"></i> Cetak Hasil Non Paket</a> 
            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>Parameter</th>
                  <th>Hasil</th>
                  <th>NDefault</th>
                  <th>NRujukan</th>
                  <th>Satuan</th>
                  <th>Biaya</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listLaboratorium">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
        <!-- END HASIL LABORATORIUM -->
        <div class="form-group">
             <label for="" class="col-sm-1 control-label" ></label>
            <div class="col-sm-4">
              <span class="label label-warning">Keterangan Obat</span><textarea disabled class=" form-control" name="keteranganobat" rows="3"></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Pemakaian BHP/Obat</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table table-striped table-hover" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>No</th>
                  <th>Nama</th>
                  <th>Pemakaian</th>
                  <th>Satuan</th>
                  <th>@Harga</th>
                  <th>Subtotal</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="viewDataBhp">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- END INPUT BHP -->

          <center id="menuPendaftaranPoliklinik">
            <a class="btn btn-warning btn-lg" href="javascript:void(0)" onclick="history.back()">Kembali</a>
          </center>
        </form>
        <hr>
        </div>

    <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->