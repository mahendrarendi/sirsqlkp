
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div>
            <input style="margin-left:4px;" id="tanggalawal" size="7" class="datepicker"> 
            <input id="tanggalakhir" size="7" class="datepicker"> 
            <a class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a>
            <a class="btn btn-warning btn-sm" id="refresh"><i class="fa  fa-refresh"></i> Refresh</a>
            
          </div>
          <div class="box-body" >
            <table id="listpo" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                        <th>Tgl/Jam RO</th>
                        <th>Kode Booking</th>
                        <th>dr. Operator</th>
                        <th>Nama Poli</th>
                        <th>Jenis Tindakan</th>
                        <th>Jaminan</th>
                        <th>No.RM</th>
                        <th>Nama Pasien</th>
                        <th>Status</th>
                        <th>Aksi</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody></tfoot>
            </table>
          </div>
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode periksa -->
        <?php }else if($mode=='operasi'){?>
        <style>.marginl-20{margin-left:-20px;margin-right:20px;}</style>
        <div class="box">
          <div class="box-header">
          </div>
          <div class="box-body">
          <div class="col-md-12 " id="profilepasien" style="border:1.5px solid #d2d6de;border-radius: 2px;margin-bottom:20px; padding:0px;"> <!-- data profile ditampilkan dari js -->  </div>
        
            <form action="<?= base_url('cpelayanan/saveoperasi');?>" method="POST" class="form-horizontal" id="FormOperasi">
            <input type="hidden" name="idoperasi" value=""/>
            <input type="hidden" name="idjadwaloperasi" id="idjadwaloperasi" value=""/>
            <input type="hidden" name="idpendaftaran" value=""/>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">dr. Operator :</label>
              <div class="col-sm-3"><select id="dokteroperator" class="select2 form-control" name="iddokteroperator" style="width:100%;"></select></div>
              
              <label for="" class="col-sm-3 control-label">dr. Anestesi :</label>
              <div class="col-sm-3"><select id="dokteranestesi" class="select2 form-control" name="idpegawaidokteranestesi" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="dokteranestesi" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">dr. Anak :</label>
              <div class="col-sm-3"><select id="dokteranak" class="select2 form-control" name="idpegawaidokteranak" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="dokteranak" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
              
              <label for="" class="col-sm-2 control-label">Penata :</label>
              <div class="col-sm-3"><select id="penata" class="select2 form-control" name="idpegawaipenata" style="width:100%;" ><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="penata" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Asisten Bedah :</label>
              <div class="col-sm-3"><select id="asisten" class="select2 form-control" name="idpegawaiasisten" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="asisten" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
              
              <label for="" class="col-sm-2 control-label">Sirkuler :</label>
              <div class="col-sm-3"><select id="sirkuler" class="select2 form-control" name="idpegawaisirkuler" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="sirkuler" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Perawat Instrumen :</label>
              <div class="col-sm-3"><select id="instrumen" class="select2 form-control" name="idpegawaiinstrumen" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="instrumen" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
              
              <label for="" class="col-sm-2 control-label">Penerima Bayi :</label>
              <div class="col-sm-3"><select id="penerimabayi" class="select2 form-control" name="idpegawaipenerimabayi" style="width:100%;"><option value="0">Pilih..</option></select></div>
              <div class="col-md-1 marginl-20"><a id="hapuspetugas" idpetugas="penerimabayi" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> hapus</a></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Jenis Operasi :</label>
              <div class="col-sm-4" id="jenisoperasi" style="padding-top:5px;"></div>
              <label for="" class="col-sm-2 control-label">Jenis Anestesi :</label>
              <div class="col-sm-3" id="jenisanestesi" style="padding-top:5px;"></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Macam Operasi :</label>
              <div class="col-sm-3"><select id="idmacamoperasi" name="idmacamoperasi" class="form-control"></select></div>
              <label for="" class="col-sm-3 control-label">Obat Anestesi :</label>
              <div class="col-sm-3"><input id="obatanestesi" name="obatanestesi" class="form-control" type="text"></div>
            </div>
            
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Jumlah Perdarahan :</label>
              <div class="col-sm-3"><textarea name="jumlahperdarahan" id="jumlahperdarahan" class="form-control"></textarea></div>
              
              <label for="" class="col-sm-3 control-label">Sitologi :</label>
              <div class="col-sm-3"><textarea name="sitologi" id="sitologi" class="form-control"></textarea></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Patologi Anatomi :</label>
              <div class="col-sm-3"><textarea name="patologianatomi" id="patologianatomi" class="form-control"></textarea></div>
              
              <label for="" class="col-sm-3 control-label">Pemasangan Implant :</label>
              <div class="col-sm-3"><textarea name="pemasanganimplant" id="pemasanganimplant" class="form-control"></textarea></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Asal Jaringan:</label>
              <div class="col-sm-3"><textarea name="asaljaringan" id="asaljaringan" class="form-control"></textarea></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Resiko Tinggi :</label>
              <div class="col-sm-3" style="padding-top:5px;" id="resikotinggi">
                  <label><input type="radio" name="resikotinggi" value="0" class="flat-red" checked> Tidak </label>&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="resikotinggi" value="1" class="flat-red"> Ya </label>
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Operasi Caesar :</label>
              <div class="col-sm-3" style="padding-top:5px;" id="isoperasisc">
                  <label><input type="radio" name="isoperasisc" value="0" class="flat-red" checked> Tidak </label>&nbsp;&nbsp;&nbsp;
                  <label><input type="radio" name="isoperasisc" value="1" class="flat-red"> Ya </label>
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Tanggal Mulai :</label>
              <div class="col-sm-3" style="padding-top:5px;">
                  <input type="text" name="tanggalmulai" id="tanggalmulai" class="form-control" readonly />
              </div>
              
              <label for="" class="col-sm-2 control-label">Tanggal Selesai :</label>
              <div class="col-sm-3" style="padding-top:5px;">                  
                  <input type="text" name="tanggalselesai" id="tanggalselesai" class="form-control" readonly />
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Jam Mulai :</label>
              <div class="col-sm-3" style="padding-top:5px;">                  
                  <input type="text" name="jammulai" id="jammulai" class="form-control" readonly />
              </div>
              
              <label for="" class="col-sm-2 control-label">Jam Selesai :</label>
              <div class="col-sm-3" style="padding-top:5px;">                  
                  <input type="text" name="jamselesai" id="jamselesai" class="form-control" readonly />
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Tarif Operasi :</label>
              <div class="col-sm-3" style="padding-top:5px;">
                  <select id="tarifoperasi" name="tarifoperasi" class="form-control">
                  </select>
              </div>
              
              <label for="" class="col-sm-2 control-label">Jaminan :</label>
              <div class="col-sm-3" style="padding-top:5px;">
                  <select id="jaminan" name="jaminan" class="form-control">
                  </select>
              </div>
            </div>
            
            <div class="form-group"><label for="" class="col-sm-2 control-label">Tarif Tindakan :</label>
              <div class="col-sm-3" style="padding-top:5px;">
                  <select name="tariftindakan" id="tariftindakan" class="form-control">
                  </select>
              </div>
            </div>

            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Uraian Pembedahan :</label>
              <div class="col-sm-9">
                <textarea class="form-control textarea" id="uraianpembedahan" name="uraianpembedahan" placeholder="(Prosedur operasi yang dilakukan dan rincian temuan, ada dan tidak adanya komplikasi)" rows="12"></textarea>
              </div>
            </div>
                        
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Diagnosis Pra Operasi :</label>
              <div class="col-sm-9">
                <textarea class="form-control textarea" id="diagnosapraoperasi" name="diagnosapraoperasi" placeholder="Diagnosis Pra Operasi" rows="6"></textarea>
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Diagnosis Pasca Operasi :</label>
              <div class="col-sm-9">
                <textarea class="form-control textarea" id="diagnosapascaoperasi" name="diagnosapascaoperasi" placeholder="Diagnosis Pasca Operasi" rows="6"></textarea>
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Jenis Tindakan:</label>
              <div class="col-sm-9">
                <input class="form-control" id="jenistindakan" name="jenistindakan" >
              </div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Tindakan Operasi :</label>
              <div class="col-sm-9">
                <textarea class="form-control textarea" id="tindakanoperasi" name="tindakanoperasi" placeholder="tindakanoperasi" rows="6"></textarea>
              </div>
            </div>
            
          </div>
          </div>
          
          <!-- FORM SEWA ALAT -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>Daftar Sewa Alat</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="toolbar">
                    <a id="tambah_alat" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Alat</a>
                </div>              
              <table id="dtsewaalat" class="table table-bordered table-hover table-striped" style="width: 100%">
                <thead>
                  <tr class="bg-yellow">
                    <th>Nama Alat</th>
                    <th>Alias</th>
                    <th>Jumlah</th>
                    <th>Harga Alat</th>                    
                    <th>Subtotal</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
                
                <div class="col-md-6 col-xs-12 row">
                    <table class="" style="padding:10px;">
                    <tr><th colspan="4">#Keterangan</th></tr>  
                    <tr><td colspan="2"><small class="text text-red">*tekan tab untuk menyimpan perubahan jumlah & harga.</small></td></tr>
                    <tr><td><a class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Tambah Alat</a></td><td> &nbsp; Menu untuk menambah daftar alat.</td></tr>
                    <tr><td><a class="btn btn-xs btn-primary" style="margin-top:3px;"><i class="fa fa-plus-square"></i> Sewa</a></td><td> &nbsp; Menu untuk menambahkan sewa alat.</td></tr>
                    <tr><td><a class="btn btn-xs btn-danger" style="margin-top:3px;"><i class="fa fa-minus-circle"></i> Batal</a></td><td> &nbsp; Menu untuk membatalkan sewa alat.</td></tr>
                </table>
                </div>
            </div>
          </div>
          <!-- END FORM sewa alat -->
          
          <!-- FORM Obat/BMHP -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>OBAT/BHP</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="dtbarangpenggunaan" class="table table-striped table-hover table-bordered" style="width: 100%; margin-top: 5px;">
                  <thead>
                    <tr class="bg-yellow">
                      <th class="text-center" width="50px;" >No</th>
                      <th width="60%">Obat/BHP</th>
                      <th width="100px">Jumlah Pemakaian</th>
                      <th>Satuan</th>
                    </tr>
                  </thead>
                  <tbody id="bodybarangpenggunaan"></tbody>
                </table>
            </div>
          </div>
          
          
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>OBAT/BHP Lain-lain</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-4 row">
                  <select class="select2 form-control" id="caribhpobat" style="width:100%;">
                      <option value="0">Pilih</option>
                  </select>
              </div>              
              <table id="dtbarangpemeriksaan" class="table table-striped table-hover table-bordered" style="width: 100%; margin-top: 5px;">
                  <thead>
                    <tr class="bg-yellow">
                      <th>No</th>
                      <th>Obat/BHP</th>
                      <th>Jumlah Pemakaian</th>
                      <th>Harga</th>
                      <th>Satuan</th>
                      <th>Subtotal</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                
                <sup class="text text-red">*Tekan tab untuk menyimpan perubahan jumlah.</sup><br/>
            </div>
          </div>
            
            <hr>
             <center>
                <a class="btn btn-primary btn-lg" id="saveoperasi">SIMPAN</a>
                <a class="btn btn-warning btn-lg" id="kembali" >KEMBALI</a>
            </center>
            </br>
            </form>
        <?php } ?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->