<style type="text/css">
    .col-sm-1,
    .col-sm-2 {
        padding-right: 1px;
        margin: 0px;
    }
</style>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <a href="<?= base_url('cpelayananranap/pemeriksaanranapdetail/ringkasanpasienpulang/' . $idinap) ?>" class="btn <?= (($mode == 'ringkasanpasienpulang') ? 'bg-maroon' : 'btn-default') ?> btn-md"><i class="fa fa-trello "></i> Ringkasan Pasien Pilang</a>
            <a href="<?= base_url('cpelayananranap/pemeriksaanranapdetail/forminacbg/' . $idinap) ?>" class="btn <?= (($mode == 'forminacbg') ? 'btn-primary' : 'btn-default') ?>  btn-md"><i class="fa fa-delicious"></i> Formulir INA-CBG</a>
            <!--tampilkan list pemeriksaan ranap-->
            <div class="box" <?= (($mode == 'forminacbg') ? 'style="border-color:#3c8dbc;"' : 'style="border-color:#d81b60;"'); ?>>
                <div class="box-body">
                    <div class="col-md-12">
                        <?php if ($mode == 'ringkasanpasienpulang') { ?>
                            <form action="" id="formRingkasanPasienPulang" class="form-horizontal">
                                <input type="hidden" name="idinap" value="<?= $idinap; ?>" />
                                <div class="form-group">
                                    <span class="col-md-2">&nbsp;</span>
                                    <span for="" class="col-sm-8 text-left">
                                        <h4>Ringkasan Pasien Pulang (DISCHARGE SUMMARY)</h4>
                                    </span>
                                </div>

                                <input type="hidden" name="idpegawaidokter" value="<?= $identitas['idpegawaidokter']; ?>">
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">No.RM</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['norm']; ?>" readonly>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Dokter yang merawat</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['namadokter']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Nama</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['namalengkap']; ?>" readonly>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Resiko Tinggi</span>
                                    <div class="col-sm-3" style="margin-top:4px;">
                                        <label><input type="radio" name="resikotinggi" value="1" <?= (($identitas['resikotinggi'] == 1) ? 'checked' : ''); ?>> Ya</label> &nbsp;&nbsp;
                                        <label><input type="radio" name="resikotinggi" value="0" <?= (($identitas['resikotinggi'] == 0) ? 'checked' : ''); ?>> Tidak</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Tanggal Lahir</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['tanggallahir']; ?>" readonly>
                                    </div>



                                    <span for="" class="col-sm-2 control-label">Waktu Masuk</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['mulaipoli']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Jenis Kelamin</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['jeniskelamin']; ?>" readonly>
                                    </div>



                                    <span for="" class="col-sm-2 control-label">Waktu Keluar</span>
                                    <div class="col-sm-3">
                                        <div class='input-group date' id='datetimepicker'>
                                            <input type='text' name="waktukeluar" value="<?= $identitas['waktukeluar']; ?>" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar">
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Usia</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['usia']; ?>" readonly>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Ruang</span>
                                    <div class="col-sm-3">
                                        <?php if(empty($identitas['namabangsal'])){ ?>
                                            <input type="text" class="form-control" value=" " readonly>
                                        <?php } else { ?>
                                            <input type="text" class="form-control" value="<?= $identitas['namabangsal'] . ' ( ' . $identitas['nobed'] . ' )'; ?>" readonly>
                                        <?php } ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Alamat</span>
                                    <div class="col-sm-3">
                                        <textarea class="form-control" readonly><?= $identitas['alamat']; ?></textarea>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Kelas</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['kelas']; ?>" readonly><br>
                                        <label><input type="checkbox" name="pilihan" class="pilihan" id="pilihan1" value="1"<?= $identitas['pilihan'] == 1 ? "checked" : ""; ?>> Ganti Ruangan</label><br>
                                        <label class="inputpilihan">Ruang :</label><input type="text" class="form-control inputpilihan" placeholder="Ruang" name="pilihanruang" value="<?= $identitas['pilihanruang']; ?>"><br>
                                        <label class="inputpilihan">Kelas :</label><input type="text" class="form-control inputpilihan" placeholder="Kelas" name="pilihankelas" value="<?= $identitas['pilihankelas']; ?>">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-5">INDIKASI RAWAT INAP</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="indikasirawatinap"><?= $identitas['indikasirawatinap']; ?></textarea>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-5">DIAGNOSIS</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="diagnosis"><?= $identitas['diagnosis']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-5">KOMORDIBITAS LAIN</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="komordibitaslain"><?= $identitas['komordibitaslain']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">RINGKASAN RIWAYAT & PEMERIKSAAN FISIK (yang penting/berhubungan)</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="ringkasanriwayat_pemeriksaanfisik"><?= $identitas['ringkasanriwayat_pemeriksaanfisik']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">HASIL LABORATORIU< / PA, PA, RONTGEN, USG, dll (yang penting / berhubungan)</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="hasilpemeriksaanpenunjang"><?= $identitas['hasilpemeriksaanpenunjang']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">TERAPI / PENGOBATAN (selama rawat inap)</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="terapiselamaranap"><?= $identitas['terapiselamaranap']; ?></textarea>
                                    </div>
                                </div>

                                <!-- [Andri] -START- diagnosis DPJP -->
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">Diagnosis DPJP</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="diagnosisDPJP"><?= $identitas['diagnosisDPJP']; ?></textarea>
                                    </div>
                                </div>
                                <!-- [Andri] -END- diagnosis DPJP -->

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Kode ICD-10</span>
                                    <div class="col-sm-3">
                                        <select class="form-control select2 icd10" level="primer" modeview="ringkasanpasienpulang" name="icd10" id="icd10">
                                            <option value="">[ICD 10 Primer ]</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control select2 icd10" level="sekunder" modeview="ringkasanpasienpulang" name="icd10" id="icd10">
                                            <option value="">[ICD 10 Sekunder ]</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <table class="table table-bordered table-bordered table-striped table-hover">
                                            <thead>
                                                <tr class="header-table-ql">
                                                    <th width="10%">NO</th>
                                                    <th width="30%">KODE ICD-10</th>
                                                    <th>DIAGNOSIS</th>
                                                    <th>Level</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="icd10_view">
                                                </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="form-group">
                                        <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                        <span for="" class="col-sm-8">Tindakan/Prosedur/Operasi</span>
                                    </div>
                                    <div class="form-group" style="margin-top:-15px;">
                                        <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="tindakanproseduroperasi"><?= $identitas['tindakanproseduroperasi']; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Kode ICD-9</span>
                                    <div class="col-sm-3">
                                        <select class="form-control select2" name="icd9" modeview="ringkasanpasienpulang" id="icd9">
                                            <option value="">[ ICD-9 ]</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr class="header-table-ql">
                                                    <th width="10%">NO</th>
                                                    <th width="30%">KODE ICD-9</th>
                                                    <th>TINDAKAN/OPERASI</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="icd9_view">
                                                </tfoot>
                                        </table>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">TERAPI PULANG</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="terapipulang"><?= $identitas['terapipulang']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">KONDISI SAAT PULANG</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="kondisisaatpulang"><?= $identitas['kondisisaatpulang']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">CARA KELUAR RS</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#pilihCaraPulangModal"><i class="fa fa-list"></i> Pilih </a> &nbsp;
                                        <textarea class="form-control" id="carapulang_textArea" name="carapulang" readonly><?= $identitas['carapulang']; ?></textarea>
                                    </div>
                                </div>
                                <!-- pilih Cara Pulang Modal -->
                                <div class="modal fade" id="pilihCaraPulangModal" tabindex="-1" role="dialog" aria-labelledby="pilihCaraPulangModal" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabel"> Cara Keluar RS </h4>
                                            </div>
                                            <div class="modal-body">

                                                <input type="radio" id="BLPL" name="carapulang_modal" value="BLPL">
                                                <label for="BLPL">BLPL</label><br>

                                                <input type="radio" id="APS" name="carapulang_modal" value="APS">
                                                <label for="APS">APS</label><br>

                                                <input type="radio" id="meninggal" name="carapulang_modal" value="Meninggal">
                                                <label for="meninggal">Meninggal</label><br>

                                                <input type="radio" id="kabur" name="carapulang_modal" value="Kabur">
                                                <label for="kabur">Kabur</label><br>

                                                <input type="radio" id="dirujuk" name="carapulang_modal" value="Dirujuk">
                                                <label for="dirujuk">Dirujuk</label><br>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveCaraPulang(carapulang_modal.value)">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">ANJURAN / INTRUKSI DAN EDUKASI (TINDAK LANJUT)</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="anjuran"><?= $identitas['anjuran']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">TINDAK LANJUT</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#pilihTindakLanjutModal"><i class="fa fa-list"></i> Pilih </a> &nbsp;
                                        <textarea class="form-control" id="tindaklanjut_textArea" name="tindaklanjut" readonly><?= $identitas['tindaklanjut'] ?></textarea>
                                    </div>
                                </div>


                                <!-- pilih Tindak Lanjut Modal -->
                                <div class="modal fade" id="pilihTindakLanjutModal" tabindex="-1" role="dialog" aria-labelledby="pilihTindakLanjutModal" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title" id="myModalLabelTindakLanjut"> TINDAK LANJUT </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <span for="" class="col-sm-6 control-label" style="text-align: left;">
                                                            KONTROL RAWAT JALAN, tanggal :
                                                        </span>
                                                        <div class="col-sm-4">
                                                            <div class='input-group date' id='datetimepickerKontrol'>
                                                                <input type='text' id="kontrolTanggal" name="kontrolTanggal" class="form-control" />
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar">
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveTindakLanjut('KONTROL RAWAT JALAN, tanggal : ',kontrolTanggal.value)">Simpan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <span for="" class="col-sm-3 control-label" style="text-align: left;">
                                                            DIRUJUK ke :
                                                        </span>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" type="text" id="dirujukValue"></input>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveTindakLanjut('DIRUJUK ke : ', dirujukValue.value)">Simpan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <div class="col-sm-10" style="text-align: left;">
                                                            Lainnya :
                                                            <textarea class="form-control" id="lainnyaValue" name="lainnyaValue"></textarea>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveTindakLanjut(lainnyaValue.value)">Simpan</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> -->
                                                <!-- <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="saveTindakLanjut(tindaklanjut_modal.value)">Simpan</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-10">TANDA TANGAN DOKTER<br> <strong>(PASTIKAN DOKTER YANG MERAWAT SUDAH MEREKAM TANDA TANGAN DIGITAL SEBELUM MENGGUNAKAN FITUR INI!)</strong></span>
                                </div>

                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <?php if ($identitas['is_signature'] == 0) { ?>
                                            <a class="btn btn-sm btn-success" data-toggle="modal" data-target="#inputPasswordTandaTanganModal" idpegawaidokter="<?= $identitas['idpegawaidokter'] ?>" id="btnMasukkanLoginDPJP"><i class="fa fa-unlock-alt"></i> Masukkan </a> &nbsp;
                                            <img class="img-thumbnail form-control" style="width : 50%; height : 3%; display : none;" id="gambarttd">
                                        <?php } else { ?>
                                            <img src="http://172.16.200.3:8008/tanda_tangan/<?= $identitas['signature'] ?>" class="img-thumbnail form-control" style="width : 50%; height : 3%;" id="gambarttd">
                                        <?php } ?>
                                    </div>
                                </div> -->

                                <input type="text" name="is_signature" style="display : none;">

                                <!-- modal tanda tangan dokter -->
                                <div class="modal fade in" id="inputPasswordTandaTanganModal" tabindex="-1" role="dialog" aria-labelledby="inputPasswordTandaTanganModal" aria-hidden="true" data-backdrop="static">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title"> MASUKKAN AKUN DOKTER YANG MERAWAT </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="panel panel-default">
                                                    <div class="panel-body" id="panelbodymodallttd">
                                                        <form method="post" id="frmLoginDPJP">
                                                            <input type="hidden" name="hdnusernamedpjp">
                                                            <input type="hidden" name="modelogin" value="1">
                                                            <div class="text-right">
                                                                <button type="button" class="btn btn-sm btn-warning" id="btnBukanDPJP"><i class="fa fa-user"></i>&nbsp;BUKAN DPJP</button>
                                                                <button type="button" class="btn btn-sm btn-warning" id="btnIsDPJP"><i class="fa fa-user-md"></i>&nbsp;DPJP</button>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Username</label>
                                                                <input type="text" class="form-control" name="usernamedpjp" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Password</label>
                                                                <input type="password" name="passworddpjp" class="form-control">
                                                            </div>
                                                            <div class="text-center">
                                                                <button type="button" class="btn btn-primary btn-md" id="btnloginDPJP"><i class="fa fa-sign-in"></i> LOGIN</button>
                                                                <a type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnclosemodallogindpjp"><i class="fa fa-times"></i> BATAL</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- akhir modal tanda tangan dokter -->
                                <div class="form-group">
                                    <span for="" class="col-sm-2">
                                        &nbsp;
                                    </span>

                                    <div class="col-sm-8">
                                        <a class="btn btn-primary btn-sm" onclick="save_ringkasanpasienpulang()"><i class="fa fa-save"></i> Simpan </a> &nbsp;
                                        <a class="btn btn-danger btn-sm" href="<?= base_url('cpelayananranap/administrasiranap'); ?>"><i class="fa fa-backward"></i> Kembali</a>
                                    </div>
                                </div>
                            </form>
                            <?php
                            $idpendaftaran = $identitas['idpendaftaran'];
                            $norm = $identitas['norm'];
                            ?>
                            <a id="periksaVitalsign" onclick="getdata_riwayatperawatanranap(<?= $idinap . ',' . $idpendaftaran . ',' . $norm; ?>);" class=" btn btn-info btn-lg"><i class="fa fa-male fa-lg"></i> Riwayat</a>
                            <!-- <a id="periksaElektromedik" onclick="print_ringkasanpasienpulang(<?//= $identitas['idpendaftaran']; ?>)" class=" btn btn-primary btn-lg"><i class="fa fa-print fa-lg"></i> Cetak</a> -->
                            <a id="periksaElektromedik" onclick="print_ringkasanpasienpulang(<?= $idpendaftaran . ',' . $idinap; ?>)" class=" btn btn-primary btn-lg"><i class="fa fa-print fa-lg"></i> Cetak</a>
                            <a id="periksaLaboratorium" onclick="save_ringkasanpasienpulang()" class=" btn btn-primary btn-lg"><i class="fa fa-save fa-lg"></i> Simpan</a>
                            <a id="periksaDiagnosa" href="<?= base_url('cpelayananranap/administrasiranap'); ?>" class=" btn btn-danger btn-lg"><i class="fa fa-backward fa-lg"></i> Kembali</a>
                        <?php
                        }
                        //---------------- ** form ina cbg
                        else {
                        ?>
                            <form action="" id="formInaCbg" class="form-horizontal">
                                <input type="hidden" name="idinap" value="<?= $idinap; ?>" />
                                <div class="form-group">
                                    <span class="col-md-2">&nbsp;</span>
                                    <span for="" class="col-sm-8 text-left">
                                        <h4>FORMULIR INA-CBG</h4>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">No.RM</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['norm']; ?>" readonly>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Dokter yang merawat</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['namadokter']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Nama</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['namalengkap']; ?>" readonly>
                                    </div>


                                    <span for="" class="col-sm-2 control-label">Waktu Masuk</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['waktumasuk']; ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Tanggal Lahir</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['tanggallahir']; ?>" readonly>
                                    </div>


                                    <span for="" class="col-sm-2 control-label">Waktu Keluar</span>
                                    <div class="col-sm-3">
                                        <div class='input-group date' id='datetimepicker'>
                                            <input type='text' name="waktukeluar" value="<?= $identitas['waktukeluar']; ?>" class="form-control" />
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar">
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Jenis Kelamin</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['jeniskelamin']; ?>" readonly>
                                    </div>

                                    <span for="" class="col-sm-2 control-label">Ruang</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['namabangsal'] . ' ( ' . $identitas['nobed'] . ' )' ?>" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Usia</span>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="<?= $identitas['usia']; ?>" readonly>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <span for="" class="col-sm-8">Cara Pulang</span>
                                </div>
                                <div class="form-group" style="margin-top:-15px;">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>

                                    <div class="col-sm-8">
                                        <textarea class="form-control" name="carapulang"><?= $identitas['carapulang']; ?></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Kode ICD-10</span>
                                    <div class="col-sm-3">
                                        <select class="form-control select2 icd10" level="primer" modeview="forminacbg" name="icd10" id="icd10">
                                            <option value="">[ICD 10 Primer ]</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control select2 icd10" level="sekunder" modeview="forminacbg" name="icd10" id="icd10">
                                            <option value="">[ICD 10 Sekunder ]</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr class="header-table-ql">
                                                    <th width="10%">NO</th>
                                                    <th width="30%">KODE ICD-10</th>
                                                    <th>DIAGNOSIS</th>
                                                    <th>Level</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="icd10_view">
                                                </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Kode ICD-9</span>
                                    <div class="col-sm-3">
                                        <select class="form-control select2" name="icd9" modeview="forminacbg" id="icd9">
                                            <option value="">[ ICD-9 ]</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">&nbsp;</span>
                                    <div class="col-sm-8">
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr class="header-table-ql">
                                                    <th width="10%">NO</th>
                                                    <th width="30%">KODE ICD-9</th>
                                                    <th>TINDAKAN/OPERASI</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="icd9_view">
                                                </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <span for="" class="col-sm-2">
                                        &nbsp;
                                    </span>

                                    <div class="col-sm-8">
                                        <a class="btn btn-primary btn-sm" onclick="save_forminacbg()"><i class="fa fa-save"></i> Simpan </a> &nbsp;
                                        <a class="btn btn-danger btn-sm" href="<?= base_url('cpelayananranap/administrasiranap'); ?>"><i class="fa fa-backward"></i> Kembali</a>
                                    </div>
                                </div>

                            </form>
                            <?php
                            $idpendaftaran = $identitas['idpendaftaran'];
                            $norm = $identitas['norm'];
                            ?>
                            <a id="periksaElektromedik" onclick="getdata_riwayatperawatanranap(<?= $idinap . ',' . $idpendaftaran . ',' . $norm; ?>);" class=" btn btn-info btn-lg"><i class="fa fa-male fa-lg"></i> Riwayat</a>
                            <a id="periksaEchocardiography" onclick="print_forminacbgranap(<?= $identitas['idpendaftaran']; ?>)" class=" btn btn-primary btn-lg"><i class="fa fa-print fa-lg"></i> Cetak</a>
                            <a id="periksaLaboratorium" onclick="save_forminacbg()" class=" btn btn-primary btn-lg"><i class="fa fa-save fa-lg"></i> Simpan</a>
                            <a id="periksaDiagnosa" href="<?= base_url('cpelayananranap/administrasiranap'); ?>" class=" btn btn-danger btn-lg"><i class="fa fa-backward fa-lg"></i> Kembali</a>
                        <?php } ?>

                    </div>

                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<script>
$(document).ready(function() {
    $('.inputpilihan').hide(); 
    $('input:checkbox[name="pilihan"]').click(function() {
        if (!$(this).is(':checked')) {
            $('.inputpilihan').hide(); 
            $('.inputpilihan').val('');
        } else {
            $('.inputpilihan').show(); 
            $('.inputpilihan').removeAttr('disabled');
            $('.inputpilihan').focus();
        }
    });
});
</script>