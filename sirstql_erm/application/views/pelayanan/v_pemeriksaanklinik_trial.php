<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
 <input type="hidden" name="modehalaman" value="<?php echo $mode; ?>">
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div>
            <select id="idunit" name="unit" onchange="reloadPemeriksaanByUnit()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%"></select>
            <select id="idpegawaidokter" name="pegawaidokter" onchange="reloadPemeriksaanByDokter()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%"></select>
            <input id="tglawal" size="7" class="datepicker" placeholder="Tanggal awal"> <input id="tglakhir" size="7" class="datepicker" placeholder="Tanggal akhir"> <a class="btn btn-info btn-sm" onclick="reloadPemeriksaan()"><i class="fa fa-desktop"></i> Tampil</a> <a class="btn btn-success btn-sm" onclick="get_exceldata()"><i class="fa fa-file-excel-o"></i> Unduh</a> <a style="margin-right: 6px;" onclick="refreshPemeriksaan()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>  <input class="bg bg-red" type="checkbox" name="isfarmasi" onchange="reloadPemeriksaan()" > Farmasi Belum List
            <!--<select class="col-xs-12 col-md-3 select2" onchange="reloadPemeriksaanByNorm()" id="norm" name="norm"><option value="">Cari Pasien</option></select>--> 
          </div>
          <div class="box-body" > <!-- id="listpasien"-->
            <table id="tablepemeriksaan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                    <tr class="header-table-ql">
                    <th>ID.Periksa</th>
                    <th>NO.RM</th>
                    <th>NAMA PASIEN</th>                    
                    <th class="none">NO.SEP</th>
                    <th class="none">No.Telpon</th>
                    <th>TGL. DAFTAR</th>
                    <th>TGL. PERIKSA</th>
                    <th style="width:110px;">DOKTER DPJP</th>
                    <th>KLINIK ASAL</th>
                    <th style="min-width:80px;">KLINIK TUJUAN (No.Antri)</th>
                    <th>SKRINING</th>
                    <th>PROLANIS</th>
                    <th>Kunjungan Berikutnya</th>                    
                    <th>STATUS</th>
                    <th style="width:160px;"></th>
                  </tr>
                  </thead>
                  <tbody>
                  </tfoot>
                </table>
          </div>
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode periksa -->
        <?php }else if( $mode=='periksa' or $mode=='verifikasi'){?>
        <style type="text/css">
        .form-pelayanan
        {
            display: block;
            width: 100%;
            height: 22px;
            padding: 2px 4px;
             font-size: 12.8px; 
            line-height: 1.42857143;
            color: #222e32;
            background-color: #f5f5f5;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 2px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .cHidden{background: transparent;border: none; height: 0px;margin:0px;padding: 0px;display: inherit;}
        </style>

        <div class="box">
          <div class="box-header">
              <h2 class="box-title"><?php echo ucwords($title_page) ;?></h2>
              <a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
              <a onclick="cetak_hasillab()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><i class="fa fa-flask"></i> Laboratorium</a>
              <a onclick="cetak_hasilelektromedik()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><img src="<?= base_url('assets/images/icons/radiasi.svg')?>"> Elektromedik</a> 
          </div>
          <div style="border-top: 1px solid #f1f1f1;"></div>
          <!-- /.box-header -->
          <div class="box-body">
        <div class="row">
        <div class="col-sm-3" id="pemeriksaanklinik_profile">
          <!-- Profile Image -->
        </div>
        <!-- /.col -->
        <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile">
        <!-- detail profile -->
        </div>
        
        </div>

        <!-- RIWAYAT PEMERIKSAAN -->
        <div class="row col-sm-12" style="margin-top: 15px;">
          <div class="box box-solid box-default">
            <div class="box-header with-border">
              <h1 class="box-title">Riwayat Pemeriksaan</h1>
            </div>

            <div class="row">
              <div id="r_hasilPeriksa" class="col-sm-9" style="margin:none;padding-top:5px;max-height:500px;overflow-y: scroll;">
              </div>
      
              <div class="col-sm-3" >
                <div class="form-group col-sm-10">
                  <a class="btn bt-md btn-primary" onclick="pemeriksaanklinik_gettglriwayat()">Riwayat</a>
                </div>
                <label class="control-label col-sm-12">Tanggal Periksa</label>
                <div id="r_tanggalPeriksa" class="btn-group-vertical" style="margin:none;max-height:250px;overflow-y: scroll; padding-right:50px;padding-left:15px;">
                  
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- END RIWAYAT PEMERIKSAAN -->
            <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
            <input type="hidden" name="idperiksa"/>
            <input type="hidden" name="idpendaftaran"/>
            <input type="hidden" name="idunit"/>
            <input type="hidden" name="idloket" id="idloket"/>
            <input type="hidden" name="idperson"/>
            <input type="hidden" name="carabayar"/>
            <input type="hidden" name="status"/>
            <input type="hidden" name="norm"/>
            <input type="hidden" id="validasidataobyektif"/>
            <input type="hidden" id="validasidatasubyektif"/>
            <input type="hidden" id="validasiicd10"/>
            <input type="hidden" name="modesave" value="<?= $mode; ?>" />
            
            <input type="hidden"  id="waktumulai" name="waktumulai"/>
            <input type="hidden" id="waktulayanan" name="waktulayanan"/>
            <input type="hidden" id="waktusekarang" name="waktusekarang"/>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Dokter DPJP</label>
              <div class="col-sm-4"><input type="text" name="dokter" value="" class="form-control" readonly="readonly" /></div>
              <div class="col-sm-2" id="viewMenuUbahDokter"></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Loket Poliklinik </label>
              <div class="col-sm-4"><input type="text" name="namaloket"  value="" class="form-control" readonly="readonly" /></div>
              <div class="col-sm-2" id="viewMenuUbahLoket"></div>
            </div>
            
              <!-- VitalSign ===================== -->
              <div class="form-group">
                <input class="cHidden" id="focusVitalsign" type="text">
                 <label for="" class="col-sm-2 control-label" >VitalSign</label>
                <div class="col-sm-4">
                  <select class="select2 form-control" name="carivitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'')">
                    <option value="0">Pilih</option>
                  </select>
                </div>

                <label for="" class="col-sm-2 control-label" >Paket VitalSign</label>
                  <div class="col-sm-3">
                  <select class="select2 form-control" name="paketvitalsign" style="width:100%;" onchange="pilihvitalsign(this.value,'ispaket')">
                    <option value="0">Pilih</option>
                    <?php 
                    if(!empty($data_paketvitalsign))
                    {
                      foreach ($data_paketvitalsign as $data) {
                        echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                      }
                    }
                    ?>
                  </select>
                </div>

              </div>
              <!-- LIST HASIL VitalSign -->
              <div class="form-group">
                <div class="col-sm-2"></div>
                <div class=" col-sm-10">
                  <span class="label label-default">*Tekan tab pada inputan nilai untuk menyimpan perubahan</span>
                <table class="table" style="width: 95%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th width="8%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listVitalsign">
                  </tbody>
                </table>
                </div>
              </div>
              
            <div class="form-group">
               <label for="" class="col-sm-2 control-label"></label>
                    <input type="checkbox" name="cindikasipasien" id="cindikasipasien" onclick="setFormIndikasiPasienWabah(this.value)" /> Pasien Terindikasi Wabah  (Pasien Covid-19)
               </label>
            </div>
              
            <div id="formIndikasiPasienWabah" class="form-group hide" style="margin-left:8px;">
                <label for="" class="col-sm-2 control-label"></label>
                <div class="row col-xs-6">
                  <div class="box box-warning col-xs-6">
                    <div class="box-header with-border">
                      <h3 class="box-title"> Formulir Pemeriksaan Indikasi Pasien Wabah, (Pasien Covid-19).</h3>
                      
                      <div class="box-body">
                        <div class="form-group">
                            <label for="" >Pilih Wabah</label>
                            <select class="form-control" id="jeniswabah" name="jeniswabah" style="width:100%;" ></select>
                        </div>
                      
                       <div class="form-group">
                        <label>Status Rawat</label>
                          <select class="form-control" id="statusrawatwabah" onchange="change_statusrawatIPW(this.value)" name="statusrawatwabah" style="width:100%;" ></select>
                     </div>
                      
                     <div class="form-group">
                        <label >Penanganan</label>
                         <select class="form-control" id="penanganan" name="penanganan" style="width:100%;" ></select>
                     </div>
                      
                      <div class="form-group">
                        <label>Sebab Penularan</label>
                         <select class="form-control" id="sebabpenularan" name="sebabpenularan" style="width:100%;" ></select>
                     </div>
                  </div>
              </div>
            </div>
            </div>
            </div>

              <div id="pemeriksaanInputAnamnesa"><!-- ditampilkan di js --></div> 

              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Data Obyektif</label>
                <div class="col-sm-9">
                  <textarea class="form-control textarea" id="dataobyektif" name="keterangan" placeholder="Keterangan Pemeriksaan" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTDATAOBYEKTIFRALAN)) ? '' : 'disabled' ) ?> ></textarea>
                </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Rencana/Catatan</label>
                <div class="col-sm-9">
                  <textarea class="form-control textarea" name="rekomendasi" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRENCANACATATANRALAN)) ? '' : 'disabled' ) ?>></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Alergi</label>
                <div class="col-sm-9" id=""> <!-- listAlergi -->
                    <textarea class="form-control textarea" name="alergiobat" placeholder="" rows="4" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTALERGIRALAN)) ? '' : 'disabled' ) ?> ></textarea>                
                </div>
              </div>
              
              <!-- Kategori Skrining -->
              <div class="form-group">
                  <label for="" class="col-sm-2 label-daftar control-label">Skrining</label>
                  <div class="col-sm-9" id="skrining" style="padding-top:5px;">
                </div>
              </div>
            <!-- START HASIL LABORATORIUM -->
            <div class="pad margin" id="viewHasilLaboratorium"></div>
            <!-- END HASIL LABORATORIUM -->
          </div>
          </div>
              <a data-toggle="tooltip" data-original-title="VitalSign" id="periksaVitalsign" onclick="setfocusVitalsign()" class=" btn btn-warning btn-lg"><i class="fa fa-male fa-lg"></i></a>
              <a data-toggle="tooltip" data-original-title="Radiologi" id="periksaElektromedik" onclick="setfocusElektromedik()" class=" btn btn-warning btn-lg"><img src="<?= base_url('assets/images/icons/radiasi.svg')?>" width="20"></a>
              <a data-toggle="tooltip" data-original-title="Echocardiography" id="periksaEchocardiography" onclick="setfocusEchocardiography()" class=" btn btn-warning btn-lg"><i class="fa fa-heartbeat fa"></i></a>
              <a data-toggle="tooltip" data-original-title="Laboratorium" id="periksaLaboratorium" onclick="setfocuslaboratorium()" class=" btn btn-warning btn-lg"><i class="fa fa-flask"></i></a>
              <a data-toggle="tooltip" data-original-title="Diagnosa" id="periksaDiagnosa" onclick="setfocusdiagnosa()" class=" btn btn-warning btn-lg"><img src="<?= base_url('assets/icon/diagnoses-white.svg')?>" width="20"></a>
              <a data-toggle="tooltip" data-original-title="Tindakan" id="periksaTindakan" onclick="setfocustindakan()" class=" btn btn-warning btn-lg"><i class="fa fa-medkit"></i></a>
              <a data-toggle="tooltip" data-original-title="Bhp/Obat" id="periksaBhp" onclick="setfocusobatbhp()" class=" btn btn-warning btn-lg"><i class="fa fa-toggle-on"></i></a>


            <!-- FORM RADIOLOGI/ELEKTROMEDIK -->
            <div class="box box-default">
            <input class="cHidden" id="focusElektromedik" type="text">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>ELEKTROMEDIK</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="form-group">
              <div class="col-sm-7">
                <label>Hasil Expertise</label>
                <textarea class="form-control textarea" name="keteranganradiologi" rows="18" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled' ) ?>></textarea>
                <label>Saran</label>
                <textarea class="form-control textarea" name="saranradiologi" rows="8" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEXPERTISERADIOLOGI)) ? '' : 'disabled' ) ?>></textarea>
              </div>
            </div>
                
            <div class="col-sm-5" style="margin-top:8px;">
                <!--jika mode periksa bisa menambahkan radiologi-->
                <?php if($mode=='periksa'){ ?>
                <div class="col-sm-12 row">
                    <select class="select2" name="cariradiologi" style="width:100%;" onchange="pilihradiologi(this.value)">
                    <option value="0">Pilih Tindakan Radiologi</option>
                  </select>
                </div>
                <?php } ?>
               

               <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>ICD</th>
                      <th>Nama ICD</th>
                      <th width="5%">Jumlah</th>
                      <th>Biaya</th>
                      <th>Potongan</th>
                      <th width="19%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listRadiologi">
                  </tbody>
                </table>
              </div>

             
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- END FORM RADIOLOGI/ELEKTROMEDIK -->
          
          <!-- FORM EKOKARDIOGRAFI -->
            <div class="box box-default">
            <input class="cHidden" id="focusEchocardiography" type="text">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>ECHOCARDIOGRAPHY</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="form-group">
                <div class="col-md-12">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr class="header-table-ql">
                                <th>Measurement</th>
                                <th>Result</th>
                                <th>Normal (Adult)</th>
                            </tr>
                        </thead>
                        <tbody id="listEkokardiografi">                            
                        </tbody>
                    </table>
                </div>
              <div class="col-sm-8">
                <label>Keterangan</label>
                <textarea class="form-control textarea" id="keteranganekokardiografi" name="keteranganekokardiografi" rows="18" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTHASILEKOKARDIOGRAFI)) ? '' : 'disabled' ) ?>></textarea>
              </div>
            </div>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- END FORM EKOKARDIOGRAFI -->

            <!-- FORM LABORATORIUM -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>LABORATORIUM</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="col-sm-7">
                <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTKETERANGANLABORATORIUM)) ? '' : 'disabled' ) ?>></textarea>
              </div>

              
              <div class="col-sm-4 row">
                  <!--jika mode periksa dapat menambahkan laboratorium-->
                <?php if($mode=='periksa'){ ?>
                <select class="select2 form-control" name="carilaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value)"><option value="0">Pilih Tindakan Laboratorium</option></select>
                  <br><br>
                  <select class="select2 form-control" name="paketlaboratorium" style="width:100%;" onchange="pilihlaboratorium(this.value,'ispaket')">
                    <option value="0">Pilih Paket Laboratorium</option>
                    <?php 
                    if(!empty($data_paket))
                    {
                      foreach ($data_paket as $data) {
                        echo "<option value='".$data->idpaketpemeriksaan."'>".$data->namapaketpemeriksaan."</option>";
                      }
                    }
                    ?>
                  </select>
                <?php } ?>
              </div>

            <input class="cHidden" id="focusLaboratorium" type="text">
              

              <table class="table" style="width: 95%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th>Biaya</th>
                      <th>Potongan</th>
                      <th width="12%">Aksi</th>
                    </tr>
                  </thead>
                  <tbody id="listLaboratorium">
                  </tbody>
                </table>
            </div>
          </div>
          <!-- END FORM LABORATORIUM -->


            <!-- FORM DIAGNOSA -->
            <div class="box box-default">
            <input class="cHidden" id="focusDiagnosa" type="text">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b></b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-11">
                <!--ICD 10-->
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">ICD 10 (Diagnosa)</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <label>Diagnosa Primer</label>
                            <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,'primer')"><option value="0">Input Diagnosa</option></select>
                            
                        </div>
                        <div class="col-md-4">
                            <label>Diagnosa Sekunder</label>
                            <select class="select2 form-control" name="caridiagnosa" style="width:100%;" onchange="pilihdiagnosa(this.value,10,'sekunder')"><option value="0">Input Diagnosa</option></select>
                        </div>
                        
                        <div class="col-md-8" style="margin-top:8px;">
                            <label>Diagnosa Diluar ICD 10</label>
                            <textarea class="form-control" name="diagnosa" rows="2"></textarea>
                        </div>
                      
                        <div class="col-md-12" style="margin-top:4px;">
                        <table class="table">
                            <thead>
                              <tr class="bg-yellow">
                                <th>ICD</th>
                                <th>Nama ICD</th>
                                <th>Level</th>
                                <th>Alias</th>
                                <th width="8%">Aksi</th>
                              </tr>
                            </thead>
                            <tbody id="listDiagnosa10">
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
                <!--end icd 10--> 
                
                
                <!--ICD 9-->
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">ICD 9 (Tindakan)</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-4">
                            <label>Input ICD 9</label>
                            <select class="select2 form-control" name="caridiagnosa9" style="width:100%;" onchange="pilihdiagnosa(this.value,9,'')"><option value="0">Input</option></select>
                            
                        </div>
                      
                        <div class="col-md-12" style="margin-top:4px;">
                        <table class="table">
                            <thead>
                              <tr class="bg-yellow">
                                <th>ICD</th>
                                <th>Nama ICD</th>
                                <th></th>
                                <th>Alias</th>
                                <th width="8%">Aksi</th>
                              </tr>
                            </thead>
                            <tbody id="listDiagnosa9">
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
                <!--end icd 9--> 
                
                </div>
            </div>
          </div>
          <!-- END FORM DIAGNOSA -->


            <!-- FORM TINDAKAN -->
            <div class="box box-default">
            <input class="cHidden" id="focusTindakan" type="text">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>TINDAKAN</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($mode=='periksa'){ ?>
                <div class="col-sm-4 row">
                    <select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Pilih</option></select>
                </div>
              <?php } ?>

              <table class="table" style="width: 95%">
                <thead>
                  <tr class="bg-yellow">
                    <th>ICD</th>
                    <th>Nama ICD</th>
                    <th width="17%">Dokter Penerima JM</th>
                    <th width="5%">Jumlah</th>
                    <th>Biaya</th>
                    <th>Potongan</th>
                    <th width="8%">Aksi</th>
                  </tr>
                </thead>
                <tbody id="listTindakan">
                </tbody>
              </table>
            
              <div class="col-md-12 row" style="margin-top:10px;">
                <div class="col-xs-12 col-md-3 ">
                  <label>Kondisi Keluar</label>
                  <select class="form-control" id="kondisikeluar"  name="kondisikeluar" style="width:100%;" >
                  </select>
                </div>
              </div>
                
                <div class="col-md-12 row" style="margin-top:10px;margin-bottom: 10px;">
                <div class="col-xs-12 col-md-3">                  
                    <label>Status Pasien Pulang</label>
                  <select class="form-control" id="statuspasienpulang"  name="statuspasienpulang" style="width:100%;"></select>
                </div>
              </div>
                
                
            </div>
          </div>
          <!-- END FORM TINDAKAN -->
          
          <div class="box box-default">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs bg">
                    <li><a class="text-bold" href="#tab_rawatjalan" onclick="asesmenmedis_rawatjalan()" data-toggle="tab" aria-expanded="false">Asesmen Medis Rawat Jalan</a></li>
                    <li><a class="text-bold" href="#tab_ugd" onclick="asesmenmedis_igd()" data-toggle="tab" aria-expanded="false">Triase & Asesmen Medis Gawat Darurat</a></li>
                  </ul>
                </div>
            </div>
          </div>
          
          
          <!-- FORM diskon -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>DISKON</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php if($mode=='periksa'){ ?>
              <div class="col-sm-4 row">
                  <select class="select2 form-control" name="caridiskon" style="width:100%;" onchange="pilihdiskon(this.value)"><option value="0">Pilih</option></select>
              </div>
              <?php } ?>

              <table class="table" style="width: 95%">
                <thead>
                  <tr class="bg-yellow">
                    <th>ICD</th>
                    <th>Nama ICD</th>
                    <th width="10%">Diskon</th>
                    <th width="10%">Subtotal</th>
                    <th width="8%">Aksi</th>
                  </tr>
                </thead>
                <tbody id="listDiskon">
                </tbody>
              </table>               
                
            </div>
          </div>
          <!-- END FORM Diskon -->
          
          
            <!-- FORM BHP ATAU OBAT -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>OBAT/BHP</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <span class="label label-default">Keterangan Obat/Bhp</span>
                        <textarea class="form-control" id="keteranganobat" name="keteranganobat"  <?= (($mode=='verifikasi')?'':' onchange="pemeriksaanklinik_saveketobat(this.value)" ') ?> rows="9" <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTRESEPOBATRALAN)) ? '' : 'disabled' ) ?> ></textarea>
                    </div>

                    <div class="col-sm-6">

                      <!--<a class="btn btn-primary btn-xs" onclick="cetakEtiketManual()"><i class="fa fa-print"></i> Etiket</a>--> 

                      <div class="pasien-prolanis <?= (($this->pageaccessrightbap->checkAccessRight(V_INPUTPROLANIS))? ''  : 'hide'  ); ?>">   
                          <br>
                          <input id="prolanis" name="prolanis" type="checkbox" /> PROLANIS
                          <br>
                          <span class="label label-default">Tanggal Kunjungan Berikutnya</span>
                          <div class="input-group date col-md-6">
                              <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                              <input type="text" readonly="readonly" placeholder="input tanggal..." class="form-control pull-right" id="kunjunganberikutnya" name="kunjunganberikutnya">
                          </div>
                      </div>
                    </div>
                </div>
                
                <input class="cHidden" id="focusObat" type="text">
                <!--input resep-->
                <div class="row">
                    <div class="col-md-2">
                        <h3 class="">Resep</h3>
                    </div>
                    
                    <div class="col-md-2" style="padding-top:25px;">
                        <label style="margin-right:20px;"><input id="adaracikan" name="adaracikan" type="checkbox"  class=""> Ada Racikan</label>
                        <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
                    </div>
                
                    <div class="col-md-4" style="padding-top:5px;">
                        <?php if($mode=='periksa'){ ?>
                        <select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value,'resep')"></select>
                        <small class="text text-red">#stok yang tertera stok farmasi.</small>
                        <?php } ?>
                    </div>
                    
                    <div class="col-md-12">                        
                        <?= table_bhpralan('obatbhp_resep'); ?>
                    </div>
                </div>
                
                <!--input copy resep-->
                <div class="row">
                    <div class="col-md-3">
                        <h3 class="">Copy Resep</h3>
                    </div>
                    
                    <div class="col-md-1" style="padding-top:25px;">                        
                        <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
                    </div>
                
                    <div class="col-md-4" style="padding-top:25px;">
                        <?php if($mode=='periksa'){ ?>
                        <select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value,'copyresep')"></select>
                        <?php } ?>
                    </div>
                    
                    <div class="col-md-12">                        
                        <table class="table table-striped table-hover table-bordered" style="width: 95%; margin-top: 5px;">
                            <thead>
                              <tr class="bg-yellow">
                                <th>No</th>
                                <th>Obat/BHP[ExdDate]</th>
                                <th width="8%">Resep<sup>1)</sup></th>
                                <th>Kekuatan</th>
                                <th width="9%">DosisRacik<sup>6)</sup></th>
                                <th>GrupRacik<sup>3)</sup></th>
                                <th>Diresepkan</th>
                                <th>Tot.Harga</th>
                                <th>Tot.Diberikan<sup>4)</sup></th>
                                <th>Diberikan</th>
                                <th>Penggunaan</th>
                                <th colspan="2">AturanPakai</th>
                              </tr>
                            </thead>
                            <tbody id="obatbhp_copyresep">
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <!--input Obat/BHP Digunakan-->
                <div class="row">
                    <div class="col-md-4">
                        <h3 class="">Penggunaan Obat/BHP</h3>
                    </div>
                
                    <div class="col-md-4" style="padding-top:25px;">
                        <?php if($mode=='periksa'){ ?>
                        <select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value,'sudahdigunakan')"></select>
                        <?php } ?>
                    </div>
                    
                    <div class="col-md-12">                        
                        <table class="table table-striped table-hover table-bordered" style="width: 95%; margin-top: 5px;">
                            <thead>
                              <tr class="bg-yellow">
                                <th>No</th>
                                <th>Obat/BHP</th>
                                <th>Jumlah Diberikan</th>
                                <th>Kekuatan</th>
                                <th>Harga</th>
                                <th>Subtotal</th>
                              </tr>
                            </thead>
                            <tbody id="obatbhp_sudahdigunakan">
                            </tbody>
                          </table>
                    </div>
                </div>
                
            </div>
            </div>
          <!-- END FORM BHP ATAU OBAT -->
            
            <hr>
             <center id="menuPendaftaranPoliklinik">
                <a class="btn btn-warning btn-lg" onclick="FormPemeriksaanKlinikSubmit()" >SIMPAN</a>
                <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >KEMBALI</a>
            </center>
            </div>
            </form>
        <?php }else if( $mode=='editpemeriksaan'){?>
        <style type="text/css">
          .form-pelayanan {
            display: block;
            width: 100%;
            height: 22px;
            padding: 2px 4px;
             font-size: 12.8px; 
            line-height: 1.42857143;
            color: #222e32;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 2px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .cHidden{background: transparent;border: none; height: 0px;margin:0px;padding: 0px;display: inherit;}
        </style>
        
        
        <div class="box">
          <div class="box-header">
              <a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
              <a onclick="cetak_hasillab()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><i class="fa fa-flask"></i> Laboratorium</a>
              <a onclick="cetak_hasilelektromedik()" class="btn btn-md btn-info pull-right" style="margin-right: 8px;" ><img src="<?= base_url('assets/images/icons/radiasi.svg')?>"> Elektromedik</a> 
          </div>
          <div style="border-top: 1px solid #f1f1f1;"></div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <div class="col-sm-3" id="pemeriksaanklinik_profile">
              <!-- Profile Image -->
            </div>
            <!-- /.col -->
            <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile">
            <!-- detail profile -->
            </div>
            </div>


            <form action="<?= base_url('cpelayanan/pemeriksaanklinik_updatepenunjang');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
            <input type="hidden" name="idperiksa"/>
            <input type="hidden" name="idpendaftaran"/>
            <input type="hidden" name="idunit"/>
            <input type="hidden" name="idperson"/>
            <input type="hidden" name="status"/>
            <div class="form-group col-xs-12" style="margin-top:10px;">
                <div class="col-xs-12 col-md-4"><label for="" class="control-label">DOKTER DPJP</label><input type="text" name="dokter" value="" class="form-control" readonly="readonly" /></div>
            </div>
          </div>
          </div>
              
            <!-- FORM RADIOLOGI/ELEKTROMEDIK -->
            <div class="box box-default" style="padding:12px 0px;">
            <input class="cHidden" id="focusElektromedik" type="text">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 style="padding-left:10px;" class="box-title"><b>ELEKTROMEDIK</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="form-group">
              <div class="col-xs-12">
               <table class="table">
                  <thead>
                    <tr class="bg-yellow">
                      <th>ICD</th>
                      <th>Nama ICD</th>
                      <th width="5%">Jumlah</th>
                      <th>Biaya</th>
                    </tr>
                  </thead>
                  <tbody id="listRadiologi">
                  </tbody>
                </table>
                  
                <label>Hasil Expertise</label>
                <textarea class="form-control textarea" name="keteranganradiologi" rows="25" <?= (($this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANRADIOLOGI)) ? '' : 'readonly' ); ?> ></textarea>
                <label>Saran</label>
                <textarea class="form-control textarea" name="saranradiologi" rows="12" <?= (($this->pageaccessrightbap->checkAccessRight(V_MENUPEMERIKSAANRADIOLOGI)) ? '' : 'readonly' ); ?>></textarea>
              </div>
            </div>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- END FORM RADIOLOGI/ELEKTROMEDIK -->

            <!-- FORM LABORATORIUM -->
            <div class="box box-default" style="padding:12px 0px;">
            <input type="hidden" value="<?= $edithasillab; ?>" name="edithasillab"/>
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 style="padding-left:10px;" class="box-title"><b>LABORATORIUM</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">

              <div class="col-xs-12">
              <table class="table">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th>Biaya</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody id="listLaboratorium">
                  </tbody>
                </table>
                  
                <label>Keterangan Laboratorium</label>
                <textarea class="form-control textarea" name="keteranganlaboratorium" rows="3" <?= (($edithasillab > 0) ? '' : 'readonly' ); ?>></textarea>
                
              </div>
            </div>
          </div>
          <!-- END FORM LABORATORIUM -->
            
            <hr>
            <center>
                <a class="btn btn-primary btn-lg" onclick="FormPemeriksaanKlinikSubmit()" >SIMPAN</a>
                <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >KEMBALI</a>
            </center>
            </div>
            </form>
        <?php }else if( $mode=='viewperiksa'){?>
          <div class="box">
          <div class="box-header">
              <h2 class="box-title"><?php echo ucwords($title_page) ;?></h2>
              <a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>
              <a onclick="cetak_hasilelektromedik()" style="margin-right:4px;" class="btn btn-md btn-info pull-right" ><img src="<?= base_url('assets/images/icons/radiasi.svg')?>" width="16"> Elektromedik</a> 
              <a onclick="cetak_hasillab()" style="margin-right:4px;" class="btn btn-md btn-info pull-right" ><i class="fa fa-flask"></i> Laboratorium</a>
          </div>
          <div style="border-top: 1px solid #f1f1f1;"></div>
          <!-- /.box-header -->
          <div class="box-body">

            <div class="row">
             <!-- identitas pasien -->
            <div class="col-sm-3" id="pemeriksaanklinik_profile">
              <!-- Profile Image -->
            </div>
            <!-- /.col -->
            <div class="col-md-9" id="pemeriksaanklinik_detailprofile">
            <!-- detail profile -->
            </div>
            </div>
              <div class="col-sm-12"><hr></div>

              
            <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
              <input type="hidden" name="idperiksa">
              <input type="hidden" name="idpendaftaran">
              <input type="hidden" name="idunit">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Dokter DPJP</label>
                <div class="col-sm-4"><input disabled type="text" name="dokter" value="" class="form-control"></div>
              </div>

               

              <!-- VitalSign ===================== -->

              <!-- LIST HASIL VitalSign -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">VitalSign</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">

                <table class="table" style="width: 98%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th colspan="2">Satuan</th>
                    </tr>
                  </thead>
                  <tbody id="listVitalsign">
                  </tbody>
                </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                </div>
              </div>
              <!-- ============================== -->
              
              <div id="pemeriksaanInputAnamnesa"><!-- ditampilkan di js --></div> 
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Data Obyektif</label>
                <div class="col-sm-9">
                  <textarea disabled class="form-control textarea" name="keterangan" placeholder="Keterangan Pemeriksaan" rows="4"></textarea>
                </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Rencana/Catatan</label>
                <div class="col-sm-9">
                  <textarea disabled class="form-control textarea" name="rekomendasi"  rows="4"></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Alergi Obat</label>
                <div class="col-sm-9" >
                  <textarea disabled class="form-control textarea"  name="alergiobat" placeholder="alergi obat" rows="4"></textarea>
                </div>
              </div>
              
              <div class="form-group">
              <label for="" class="col-md-2 control-label">Hasil Expertise</label>
              <div class="col-md-9"><textarea class="form-control textarea" disabled name="keteranganradiologi" rows="18"></textarea></div>
             </div>
              
              <div class="form-group">
                <label class="col-md-2 control-label">Saran</label>
                <div class="col-md-9"><textarea disabled class="form-control textarea" name="saranradiologi" rows="12"></textarea></div>
              </div>
              
              
              <!-- LIST HASIL RADIOLOGI -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Elektromedik</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">

                  <table class="table">
                    <thead>
                      <tr class="bg-yellow">
                        <th>ICD</th>
                        <th>Nama ICD</th>
                        <th colspan="2">Biaya</th>
                      </tr>
                    </thead>
                    <tbody id="listRadiologi">
                    </tbody>
                  </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                </div>
              </div>
              <!-- ============================== -->

              <!-- LABORATORIUM ===================== -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
                  <span class="label label-default">Keterangan Laboratorium</span>
                  <textarea disabled class="form-control" name="keteranganlaboratorium" rows="3"></textarea>
                </div>
              </div>
              <!-- LIST HASIL LABORATORIUM -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Laboratorium</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">

                  <table class="table">
                  <thead>
                    <tr class="bg-yellow">
                      <th>Parameter</th>
                      <th>Hasil</th>
                      <th>NDefault</th>
                      <th>NRujukan</th>
                      <th>Satuan</th>
                      <th colspan="2">Biaya</th>
                    </tr>
                  </thead>
                  <tbody id="listLaboratorium">
                  </tbody>
                </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                </div>
              </div>
              <!-- ============================== -->

              <!-- DIAGNOSA ===================== -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class="col-sm-4">
                  <span class="label label-default">Diagnosa diluar icd 10</span>
                  <textarea class="form-control" name="diagnosa" rows="2" disabled></textarea>
                </div>
              </div>
              <!-- LIST HASIL DIAGNOSA -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                      
                      <!--ICD 10-->
                        <div class="box box-default box-solid">
                            <div class="box-header with-border">
                              <h5 class="text-bold">ICD 10 (Diagnosa)</h5>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table class="table">
                                    <thead>
                                      <tr class="bg-yellow">
                                        <th>ICD</th>
                                        <th>Nama ICD</th>
                                        <th>Alias</th>                                        
                                        <th>Level</th>
                                      </tr>
                                    </thead>
                                    <tbody id="listDiagnosa10">
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                        <!--end icd 10--> 
                        
                        <!--ICD 10-->
                        <div class="box box-default box-solid">
                            <div class="box-header with-border">
                              <h5 class="text-bold">ICD 9 (Tindakan)</h5>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table class="table">
                                    <thead>
                                      <tr class="bg-yellow">
                                        <th>ICD</th>
                                        <th>Nama ICD</th>                                        
                                        <th>Alias</th>
                                        <th></th>
                                      </tr>
                                    </thead>
                                    <tbody id="listDiagnosa9">
                                    </tbody>
                                  </table>
                                </div>
                            </div>
                        <!--end icd 10--> 
                  </div>
                  <!-- /.box-body -->
                </div>
                </div>
              </div>
              <!-- ============================== -->

              <!-- TINDAKAN ===================== -->
              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tindakan</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <table class="table">
                      <thead>
                        <tr class="bg-yellow">
                          <th>ICD</th>
                          <th>Nama ICD</th>
                          <th width="5%">Jumlah</th>
                          <th colspan="2">Biaya</th>
                        </tr>
                      </thead>
                      <tbody id="listTindakan">
                      </tbody>
                    </table>
                  </div>
                  
                  <div class="col-md-12 row" style="margin-top:10px;">
                    <div class="col-xs-12 col-md-3 ">
                      <label>Kondisi Keluar</label>
                      <input type="text" class="form-control" id="kondisikeluar" disabled/>
                    </div>
                  </div>
                
                <div class="col-md-12 row" style="margin-top:10px;margin-bottom: 10px;">
                <div class="col-xs-12 col-md-3">                  
                    <label>Status Pasien Pulang</label>
                    <input type="text" class="form-control" id="statuspasienpulang" disabled/>
                </div>
              </div>
                  
                </div>
                </div>
              </div>
              
              
              <!-- ============================== -->

              <!-- INPUT BHP -->
              <div class="form-group">
                 <div class="col-sm-1"></div>
                <div class="col-sm-4">
                   <span class="label label-default">Keterangan BHP/Obat</span>
                  <textarea disabled class="form-control" name="keteranganobat" rows="3"></textarea>
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-1"></div>
                <div class=" col-sm-10">
                  <div class="box box-default">
                  <div class="box-header with-border">
                    <h3 class="box-title">Pemakaian BHP/Obat</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                    <!-- /.box-tools -->
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                 <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
                 <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>
                 <table class="table table-striped table-hover table-bordered">
                  <thead>
                    <tr class="bg-yellow">
                      <th>No</th>
                      <th>NamaObat/BHP</th>
                      <th width="7%">DosisRacik</th>
                      <th>Kekuatan</th>
                      <th>Jumlah</th>
                      <th width="10%">JumlahAmbil</th>
                      <th>TotalHarga</th>
                      <th>Penggunaan</th>
                      <th>AturanPakai</th>
                      <th></th> 
                      <!-- <th>Beli</th> -->
                      <!-- <th>Jenis</th> -->
                    </tr>
                  </thead>
                  <tbody id="viewDataBhp">
                  </tbody>
                </table>
                  </div>
                  <!-- /.box-body -->
                </div>
                </div>
              </div>
              <!-- END INPUT BHP -->
              <center id="menuPendaftaranPoliklinik">
                <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >Back</a>
              </center>
            </form>
            <hr>
            <!-- START HASIL LABORATORIUM -->
            <div class="pad margin" id="viewHasilLaboratorium">
            </div>
            <!-- END HASIL LABORATORIUM -->
          </div>


        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->