<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box">
          <div class="box-body">
            <div class="col-md-6 row">
                <a id="reload" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
            <table id="dtpasiendiblacklist" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                <thead>
                  <tr class="header-table-ql">
                    <th>No</th>
                    <th>NORM</th>
                    <th>Nama Pasien</th>
                    <th>Unit</th>
                    <th>Alasan</th>
                  </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>
          </div>
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->