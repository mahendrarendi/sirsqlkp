<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box">
          <div class="box-body">
          <?php if( $mode=='bhp'){?>
              <div class="form-group col-xs-12">
                <div class="col-xs-12 bg bg-danger">
                  <small><b>Panduan Input Penjualan Bebas:</b> 
                    <br>1.Klik Buat Baru, untuk memulai input baru. 
                    <br>2.Jika pembeli pegawai RSQL, <span class="bg bg-info"><b>diskon pegawai</b>, di Label Inputan = Jenis Diskon Pembeli</span>.
                    <br>3.Jika pembeli pegawai RSQL Pilih nama pegawai, jika bukan pegawai masukan nama di kolom nama pembeli.
                    <br>4.Jika pembeli patner RSQL, <span class="bg bg-info"><b>diskon patner</b>, di Label Inputan = Jenis Diskon Pembeli</span>.
                    <br>5.Klik Simpan setelah mengisi nama pembeli atau memilih nama pegawai.
                    <br>6.Input Bhp/Farmasi untuk menambahkan item.
                    <br>7.Klik seleai untuk menyelesaikan transaksi.
                  </small>
                </div>
                <input type="hidden" name="labelid">
                <input type="hidden" name="labelname">
              </div>
            <div class="form-group col-xs-12">
              <label for="" class="col-xs-12 control-label" >Identitas</label>
              <div class="col-md-6"><div class="callout callout-warning" style="margin-bottom: 0!important;"> <div id="view_identitasPasien"></div></div></div>
              <div class="col-md-6">
                <center id="menuPembelianbebas">
                  <a class="btn btn-success btn-lg" id="baru"> <i class="fa fa-plus-square"></i> Buat Baru</a>
                  <a class="btn btn-info btn-lg"  id="simpan"><i class="fa fa-save"></i> Simpan</a>
                  <a class="btn btn-primary btn-lg" id="selesai"> <i class="fa fa-check-square-o"></i> Selesai</a>
                  <a class="btn btn-danger btn-lg" id="kembali"> <i class="fa fa-backward"></i> Kembali</a>
              </center>
              </div>
            </div>  

            <div id="inputbhp">
            <div class="form-group col-md-12">
              <div id="datapembeli" class="row col-md-6">
                <label for="" class="col-xs-12 control-label" >Nama Pembeli</label>
                <div class="col-xs-12"><textarea class="form-control" id="nama" name="nama" rows="2"></textarea></div>
              </div>
                
              <div class="col-md-3">
                <label for="" class="col-xs-12 control-label" >Jenis Diskon Pembeli</label>
                <div class="col-xs-12"><select class="form-control " id="jenisdiskon" name="jenisdiskon" onchange="setjenisdiskon(this.value)" style="width:100%;" ></select></div>
              </div>
                
              <div class="col-md-2">
                <label for="" class="col-xs-12 control-label" >Pasien Ranap QL</label>
                <div class="col-xs-12">
                    <select class="form-control" onchange="setpasienranap(this.value)" id="ispasienranap" name="ispasienranap" style="width:100%;">
                        <option value="0">Bukan</option>
                        <option value="1">Ya</option>
                    </select>
                </div>
              </div>
                
            </div>

            <div class="form-group col-md-12">
              <div id="datapembeli" class="row col-md-6">
                <label for="" class="col-xs-12 control-label" >Input BHP/Farmasi</label>
                  <div class="col-xs-12"><select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value)"><option value="0">Pilih</option></select></div>
                </div>
                <div class="col-md-5">
                  <label for="" class="col-xs-12 control-label" >Keterangan</label>
                <div class="col-xs-12"><textarea class="form-control" id="keterangan" name="keterangan" rows="2"></textarea></div>
              </div>
            </div>
            </div>
                
              <div class="form-group col-md-12">
                <div class=" col-sm-12">
                <span class="label label-default">*Tekan tab pada inputan yang diubah untuk menyimpan hasil</span>
                <table class="table table-hover table-bordered" style="width: 96%">
                  <thead>
                    <tr class="bg-yellow">
                      <th>No</th>
                      <th colspan="2">BHP/Obat</th>
                      <th>Pemakaian<sup>1)</sup></th>
                      <th>Kekuatan</th>
                      <th>DosisRacik</th>
                      <th>GrupRacik<sup>2)</sup></th>
                      <th>AturanPakai<sup>3)</sup></th>  
                      <th>Penggunaan</th>                    
                      <th>Catatan</th>
                      <th>JumlahAmbil</th>
                      <th>TotalHarga</th>
                      <th width="100px"></th>
                    </tr>
                  </thead>
                  <tbody id="viewDataBhp">
                  </tbody>
                </table>
                <sup>1) Banyaknya BHP/Obat yang dibeli atau Jumlah racikan</sup><br/>
                <sup>2) 0 berarti BHP/Obat tidak diracik. Selain itu, BHP/Obat akan diracik sesuai dengan grupnya. Harap diurutkan dari nomor 1, kemudian 2, dan seterusnya</sup><br/>
                <sup>3) Aturan Pemakaian Obat/BHP</sup><br/>
                </div>
              </div>
            </div>
              <!-- END INPUT BHP -->
            <!--</form>-->
        <?php }else{?>
                  <!-- <a href="<?= base_url('cpelayanan/pembelianbebas_bhp') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i>Tambah</a> -->
                  
                  <div class="form-group">
                  <div class="toolbar">
                  <input placeholder="yyyy-mm-dd" class="datepicker" autocomplete="off" type="text" name="tanggal1" size="7"> 
                  <input placeholder="yyyy-mm-dd" class="datepicker" autocomplete="off" type="text" name="tanggal2" size="7"> 
                    <a id="tampil" class="btn btn-info btn-sm" ><i class="fa fa-desktop"></i> Tampil</a> 
                    <a id="refresh" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                    <span class="bg bg-info" style="padding:8px;border-radius:4px;"><input id="ispegawai" name="ispegawai" type="checkbox"> Filter by pegawai</span>
                    <?php if($this->pageaccessrightbap->checkAccessRight(V_OBATBEBAS)){?> <a href="<?= base_url('cpelayanan/pembelianbebas_bhp')?>" class="btn btn-success btn-sm"> <i class="fa fa-plus-square"></i> Pembelian Baru</a> <?php } ?>
                  </div>
                  <table id="listpembelibebas" class="table table-bordered table-striped table-hover dt-responsive" style="width: 100%">
                    <thead>
                      <tr class="bg-yellow">
                        <th>No Nota</th>
                        <th>Nama Pembeli</th>
                        <th>Waktu Beli</th>
                        <th class="none">Waktu Bayar</th>
                        <th>Status Tagihan</th>                    
                        <th class="none">Keterangan</th>
                        <th class="none">Status</th>
                        <th width="130px"></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
        <?php } ?>
          
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->