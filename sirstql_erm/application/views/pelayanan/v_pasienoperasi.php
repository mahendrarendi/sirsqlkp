<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div>
            <a style="margin-left:4px;" class="btn btn-info btn-sm" id="tampil"><i class="fa fa-desktop"></i> Tampil</a>
            <a class="btn btn-warning btn-sm" id="refresh"><i class="fa  fa-refresh"></i> Refresh</a>
            <input style="margin-left:4px;" id="tanggalawal" size="7" class="datepicker" placeholder="Tanggal RO"> |
            <input  id="filtertanggal" class="bg bg-red" type="checkbox" name="filtertanggal"> Filter Tanggal
          </div>
          <div class="box-body" >
            <table id="listpo" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                <thead><tr><th>Jadwal</th><th>Data Pasien</th><th></th></tr></thead><tbody></tfoot>
            </table>
          </div>
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode periksa -->
        <?php }else if($mode=='operasi'){?>

        <div class="box">
          <div class="box-header">
              <!--<a onclick="riwayatpasien_rajal()"  class="btn btn-info pull-right" data-toggle="tooltip" data-original-title="Riwayat Pasien"><i class="fa fa-eye"></i> Riwayat</a>-->
              </div>
          <!-- /.box-header -->
          <div class="box-body">
        <div class="row">
        <div class="col-sm-3" id="profile">
        </div>
        <!-- /.col -->
        <div style="max-width: 72.5%" class="col-md-9" id="pemeriksaanklinik_detailprofile">
        <!-- detail profile -->
        </div>
        </div>

            <form action="<?= base_url('cpelayanan/pemeriksaanklinik_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
            <input type="hidden" name="idperiksa"/>
            <input type="hidden" name="idpendaftaran"/>
            <input type="hidden" name="idunit"/>
            <input type="hidden" name="idperson"/>
            <input type="hidden" name="status"/>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">dr. Operator</label>
              <div class="col-sm-4"><input type="text" name="dokteroperator" value="" class="form-control" readonly="readonly" /></div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">dr. Anestesi</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">dr. Anestesi</option></select></div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">dr. Anak</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">dr. Anak</option></select></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Penata</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Input</option></select></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Asisten</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Input</option></select></div>
              <label for="" class="col-sm-1 control-label">Sirkuler</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Input</option></select></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">Instrumen</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Instrumen</option></select></div>
              <label for="" class="col-sm-1 control-label">P. Bayi</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Penerima Bayi</option></select></div>
            </div>
            
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">J. Operasi</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Jenis Operasi</option></select></div>
              <label for="" class="col-sm-1 control-label">J. Anestesi</label>
              <div class="col-sm-4"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Jenis Anestesi</option></select></div>
            </div>

              <div id="pemeriksaanInputAnamnesa"><!-- ditampilkan di js --></div> 

              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Diagnosa</label>
                <div class="col-sm-9">
                  <textarea class="form-control textarea" name="diagnosa" placeholder="Diagnosa" rows="6"></textarea>
                </div>
              </div>
              
            <!-- END HASIL LABORATORIUM -->
          </div>
          </div>

            <!-- FORM TINDAKAN -->
            <div class="box box-default">
            <div class="box-header ui-sortable-handle" style="cursor: move;"><h3 class="box-title"><b>TINDAKAN</b></h3></div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-sm-4 row"><select class="select2 form-control" name="caritindakan" style="width:100%;" onchange="pilihtindakan(this.value)"><option value="0">Pilih</option></select>
              </div>

              <table class="table" style="width: 95%">
                <thead>
                  <tr class="bg-yellow">
                    <th>ICD</th>
                    <th>Nama ICD</th>
                    <th width="5%">Jumlah</th>
                    <th>Biaya</th>
                    <th width="8%">Aksi</th>
                  </tr>
                </thead>
                <tbody id="listTindakan">
                </tbody>
              </table>
            </div>
          </div>
          <!-- END FORM TINDAKAN -->
            <!-- FORM BHP ATAU OBAT -->
            <div class="box box-default">
            <!-- /.box-header -->
            <div class="box-body">
                
              <div class="col-sm-5"><label class="form-label">Keterangan Obat/Bhp</label><textarea class="form-control" name="keteranganobat" onchange="pemeriksaanklinik_saveketobat(this.value)" rows="4"></textarea></div>
              <div class="col-sm-5"><label class="form-label">Input Obat/Bhp</label><select class="select2 form-control" name="caribhp" style="width:100%;" onchange="inputBhpFarmasi(this.value)"></select>
                <!--<a class="btn btn-primary btn-xs" onclick="cetakEtiketManual()"><i class="fa fa-print"></i> Etiket</a>--> 
                <br/><br/>
                <a class="btn btn-primary btn-xs" onclick="cetakBhpResep()"><i class="fa fa-print"></i> Resep</a>
                <a class="btn btn-primary btn-xs" onclick="cetakBhpCopyResep()"><i class="fa fa-print"></i> Copy Resep</a>

              </div>
              <?= table_bhpralan('viewBhpPO'); ?>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix no-border text-center">
              <a class="btn btn-primary" onclick="FormPemeriksaanKlinikSubmit()" >SIMPAN</a>
              <a class="btn btn-warning" href="<?= base_url('cpelayanan/pemeriksaanklinik');?>" >KEMBALI</a>
            </div> -->
          <!-- END FORM BHP ATAU OBAT -->
            
            <hr>
             <center>
                <a class="btn btn-warning btn-lg" onclick="FormPasienOperasiSubmit()" >SIMPAN</a>
                <a class="btn btn-warning btn-lg" href="<?= base_url('cpelayanan/pasienoperasi');?>" >KEMBALI</a>
            </center>
            </div>
            </form>
        <?php } ?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->