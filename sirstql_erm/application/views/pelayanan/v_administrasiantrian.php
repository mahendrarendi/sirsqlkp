<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
 <div class="form-group">
 <div class="col-sm-2">
    <select class="select2 form-control" style="width:100%" id="unit" name="unit">
    <option value="0">Poli/Unit</option>
    <?php
    foreach ($dt_unit as $row) 
    {
      echo '<option value="'.$row->idunit.'">'.$row->namaunit.'</option>';
    }
    ?>
    </select>
</div>
 
<div class="col-sm-2">
    <input type="text" class="datepicker form-control" name="tanggal" >
</div>
 <div class="col-sm-2">
 <a class="btn btn-warning btn-md" onclick="tampilkanantrian()">Tampilkan</a>
 </div>
 <div class="col-sm-2">
 <input class="form-control" type="text" id="textqrcodeantrian" name="textqrcodeantrian" size="10" placeholder="Cari Pasien (50 data)">
 </div>
 
 </div>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- <div class="box-header">
            <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3>
          </div> -->
          <!-- /.box-header -->
          <div class="box-body" id="listantrian">
            <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th>NO</th>
                <th>POLI/UNIT</th>
                <th>JADWAL</th>
                <th>NO.RM</th>
                <th>NAMA PASIEN</th>
                <th>NO.SEP</th>
                <th>NO.PESAN</th>
                <th>NO.ANTRIAN</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <!-- START TAMPIL DATA-->
              <!-- data ditampilkan oleh js_administrasiantrian -->
              <!-- END TAMPIL DATA-->
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode periksa -->
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->