<style type="text/css">
    .col-sm-1, .col-sm-2{
        padding-right: 1px;margin:0px;
    }
    .tabriwayat{
        background-color: #d2d6de;
    }
    .nav-tabs-custom>.nav-tabs>li.active>a{
        background-color: #f39c12;
        color: #444;
    }
    .riwayatparagraf{
        background-color: #ffffff;padding:6px;border-radius:2px;
    }
</style>

 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row col-md-12" style="padding-bottom:15px;">
                <div class="col-sm-3 row" id="ranap_profile">
                  <!-- Profile Image -->
                </div>

                <div style="max-width: 72.5%" class="col-md-5 " id="ranap_detailprofile">
                <!-- detail profile -->
                </div>
                <div  class="col-md-4 ">

                    <div class="info-box bg-gold">
                        <span class="info-box-icon"><i class="fa fa-clock-o"></i></span>

                        <div class="info-box-content">
                          <span class="info-box-text"></span>
                          <span class="info-box-number">Waktu Pemeriksaan</span>

                          <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                          </div>
                          <span class="progress-description" id="waktupemeriksaan">
                          </span>
                        </div>
                        <!-- /.info-box-content -->
                      </div>
                </div>
            </div>
          <!-- /.box -->
        </div>
          
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active tabriwayat"><a href="#activity" id="btnPemeriksaan" data-toggle="tab">Pemeriksaan</a></li>
              <li class="tabriwayat"><a href="#timeline" id="btnRiwayatPasien" data-toggle="tab">Riwayat Pasien</a></li>
            </ul>
            <div class="tab-content">
                <div class="active tab-pane" id="activity">
                    <form action="<?= base_url('cpelayananranap/pemeriksaanranap_save_periksa');?>" method="POST" class="form-horizontal" id="FormPemeriksaanKlinik">
          <input type="hidden" name="idinap">
          <input type="hidden" name="idrencanamedispemeriksaan">
          <input type="hidden" name="idpendaftaran">
          <input type="hidden" name="idunit">
          <input type="hidden" name="norm">
          <div class="form-group">
            <label for="" class="col-sm-1 control-label"></label>
            <div class="col-sm-4"><div class="label label-warning">Dokter Masuk</div>
                <textarea disabled name="dokter" rows="6" class="textarea form-control" readonly></textarea></div>
            <label class="col-sm-2"></label>
            <div class="col-sm-4">
              <div class="label label-warning">Anamnesa Masuk</div>
              <textarea  class="textarea form-control" name="anamnesa" placeholder="Anamnesa Dokter Saat Masuk" rows="6" readonly></textarea>
            </div>
          </div>

           <div class="form-group">
            <label for="" class="col-sm-1 control-label"></label>
            <div class="col-sm-4">
              <div class="label label-warning">Catatan Rawat Inap</div>
              <textarea class="textarea form-control" name="catatan" placeholder="Catatan Rawat Inap" rows="6" disabled></textarea>
            </div>

            <label for="" class="col-sm-2 control-label" ></label>
            <div class="col-sm-4">
              <div class="label label-warning">Keterangan Masuk</div>
              <textarea disabled class="textarea form-control" name="keterangan" placeholder="Keterangan Pemeriksaan Saat Masuk" rows="6" readonly></textarea>
            </div>
          </div>

          <!-- LIST HASIL VitalSign -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">VitalSign</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>Parameter</th>
                  <th>Hasil</th>
                  <th>NDefault</th>
                  <th>NRujukan</th>
                  <th>Satuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listVitalsign">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- ============================== -->


          <!-- TINDAKAN ===================== -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Tindakan</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>ICD</th>
                  <th>Nama ICD</th>
                  <th>Dokter (Opsional)</th>
                  <th width="5%">Jumlah</th>
                  <th>Biaya/Satuan</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listTindakan">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- ============================== -->
          <!-- RADIOLOGI ===================== -->

          <div class="form-group">
            <label for="" class="col-sm-2 control-label" >Hasil Radiografi</label>
            <div class="col-sm-10">          
              <table class="table" style="width: 92%; background-color: #eeeeee;" >
                  <tbody>
                    <tr>
                          <td colspan="2" id="viewHasilRadiografi">
                            <!--tampil dari js-->
                          </td>
                    </tr>
                  </tbody>
                  </table>
            </div>
          </div>

          <div class="form-group">
             <label for="" class="col-sm-2 control-label" >Hasil Expertise</label>
            <div class="col-sm-9">
              <span class="label label-warning">*Input Hasil Expertise</span>
              <textarea class="form-control textarea" name="keteranganelektromedik" rows="14" disabled></textarea>
            </div>
          </div>

          <div class="form-group">
             <label for="" class="col-sm-2 control-label" >Saran </label>
            <div class="col-sm-9">
              <span class="label label-warning">*Input Saran</span>
              <textarea class="form-control textarea" name="saranelektromedik" rows="7" disabled></textarea>
            </div>
          </div>

          <!-- Elektromedik ===================== -->
          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Elektromedik</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>ICD</th>
                  <th>Nama ICD</th>
                  <th>Biaya</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listRadiologi">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>

          <!-- ============================== -->

          <!-- LABORATORIUM ===================== -->
          <div class="form-group">
             <label for="" class="col-sm-1" ></label>
            <div class="col-sm-4">
            <span class="label label-warning">Keterangan Laboratorium</span>
              <textarea class="textarea form-control" name="keteranganlaboratorium" rows="3" disabled></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Laboratorium</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <a id="cetakhasillaboratnonpaket" class="btn btn-info btn-xs" ><i class="fa fa-print"></i> Cetak Hasil Non Paket</a> 
            <table class="table" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>Parameter</th>
                  <th>Hasil</th>
                  <th>NDefault</th>
                  <th>NRujukan</th>
                  <th>Satuan</th>
                  <th>Biaya</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="listLaboratorium">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
        <!-- END HASIL LABORATORIUM -->
        <div class="form-group">
             <label for="" class="col-sm-1 control-label" ></label>
            <div class="col-sm-4">
              <span class="label label-warning">Keterangan Obat</span><textarea disabled class=" form-control" name="keteranganobat" rows="3"></textarea>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-1"></div>
            <div class=" col-sm-10">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Pemakaian BHP/Obat</h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                </div>
                <!-- /.box-tools -->
              </div>
              <!-- /.box-header -->
              <div class="box-body">

            <table class="table table-striped table-hover" style="width: 98%">
              <thead>
                <tr class="bg-yellow">
                  <th>No</th>
                  <th>Nama</th>
                  <th>Pemakaian</th>
                  <th>Satuan</th>
                  <th>@Harga</th>
                  <th>Subtotal</th>
                  <th>Status</th>
                </tr>
              </thead>
              <tbody id="viewDataBhp">
              </tbody>
            </table>
              </div>
              <!-- /.box-body -->
            </div>
            </div>
          </div>
          <!-- END INPUT BHP -->

          <center id="menuPendaftaranPoliklinik">
            <a class="btn btn-warning btn-lg" href="javascript:void(0)" onclick="history.back()">Kembali</a>
          </center>
        </form>
                    <hr>
                </div>
                <div class="tab-pane" id="timeline">
                 <!-- The timeline -->
                    <ul class="timeline timeline-inverse">

                      <li id="riwayatAkhir">
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
                      <input type="hidden" id="riwayattanggalpemeriksaan"/>
                    </ul>

                    <div style="text-align:center;">
                        <a class="btn btn-lg btn-success" id="loadMoreRiwayat"><i class="fa fa-search-plus"></i> Tampilkan riwayat sebelumnya</a>
                    </div>

                </div>
             <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->