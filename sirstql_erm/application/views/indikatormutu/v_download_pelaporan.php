<style>
    .layout-header .logo-l{
        display: inline-block;
        width:20%;
        text-align:center;
    }
    .layout-header .format-r{
        display: inline-block;
        width:80%;
        text-align:center;
    }
    .layout-header .format-r h4{
        margin:0;
        padding:0;
    }
    .layout-header .format-r h3{
        margin:0 0 10px 0;
        padding:0;
    }
    .note-l{
        text-align:center;
        background:red;
        padding: 5px;
    }
    .note-l p {
        font-size:12px;
        margin:0;
        color:white;
    }
    ul.checkbox li{
        list-style:none;
        margin:0;
        padding:0;
        font-size:13px;
    }
    ul.checkbox{
        padding:0 0 0 25px;
    }
    ul.checkbox label{
        margin:0;
        padding:0;
        vertical-align:text-top;
    }
    
</style>

<?php 
    $id = base64_decode($id);
    $row_pelaporan = $this->db->select('*')
                              ->from('mutu_pelaporan_ikp')
                              ->where('idpelaporanikp',$id)
                              ->get()->row();
?>

<div class="layout-print-laporan">
    <div class="layout-header">
        <div class="logo-l">
            <img width="50%" src="<?= FCPATH.'/assets/images/logors.svg'; ?>" alt="">
        </div>
        <div class="format-r">
            <h3>FORMAT LAPORAN INTERNAL INSIDEN</h3>
            <h4>Rumah Sakit Umum Queen Latifa</h4>
        </div>
    </div>
    <hr style="margin-top:-5%;">
    <div class="note-l">
        <p>RAHASIA, TIDAK BOLEH DIFOTOCOPY, DILAPORKAN MAXIMAL 2X24 JAM</p>
    </div>
    <h4 style="text-align:center;">LAPORAN INSIDEN KNC, KTC, KTD DAN KEJADIAN SENTINEL</h4>

    <div class="layout-body">
        <table class="table">
            <tr>
                <th>I.</th>
                <th>DATA PASIEN (inisial)</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td>NAMA</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->nama; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>NO RM</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->norm; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    UMUR
                    <ul class="checkbox">
                        <?php foreach( $option_data['umur'] as $umur ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="umur" value="<?= $umur; ?>" <?= $row_pelaporan->umur == $umur ? "checked" : '' ?>>
                                    <?= $umur; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Jenis Kelamin
                    <ul class="checkbox">
                        <?php foreach( $option_data['jk'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="jk" value="<?= $row; ?>" <?= $row_pelaporan->jk == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Penanggungjawab Pasien
                    <ul class="checkbox">
                        <?php foreach( $option_data['penanggungjawab_pasien'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="pp" value="<?= $row; ?>" <?= $row_pelaporan->penanggungjawab_pasien == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Tanggal Masuk RS : <?php echo $row_pelaporan->tglmasuk_rs; ?></td>
                <td>Jam Masuk RS : <?php echo $row_pelaporan->jammasuk_rs; ?></td>
            </tr>
        </table>
        <table class="table" style="margin-top:25px;">
            <tr>
                <th>II.</th>
                <th>RINCIAN KEJADIAN</th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th></th>
                <td>Tanggal Insiden : <?php echo $row_pelaporan->tglinsiden; ?></td>
                <td>Jam Insiden : <?php echo $row_pelaporan->jaminsiden; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Jenis Insiden
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['jenis_insiden'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="ji" value="<?= $row; ?>" <?= $row_pelaporan->jenis_insiden == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Orang Pertama yang melaporkan Insiden
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['orang_pertama_lapor'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="opl" value="<?= $row; ?>" <?= $row_pelaporan->orang_pertama_lapor == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Insiden terjadi pada
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['insiden_terjadi'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="it" value="<?= $row; ?>" <?= $row_pelaporan->insiden_terjadi == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Insiden Menyangkut pasien
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['insiden_menyangkut_pasien'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="imp" value="<?= $row; ?>" <?= $row_pelaporan->insiden_menyangkut_pasien == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li>
                        <?php endforeach; ?>
                        <li> <input type="text" value="<?php echo $row_pelaporan->form_insiden_menyangkut_pasien; ?>"></input>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Tempat Insiden</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->tempat_insiden; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Insiden terjadi pada pasien
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['insiden_terjadi_pada_pasien'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="itpp" value="<?= $row; ?>" <?= $row_pelaporan->insiden_terjadi_pada_pasien == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Unit/departemen terkait yang menyebabkan insiden, Unit Kerja Penyebab</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->unit_terkait; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Akibat insiden terhadap pasien
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['akibat_insiden_pasien'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="aip" value="<?= $row; ?>" <?= $row_pelaporan->akibat_insiden_pasien == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Tindakan yang dilakukan segera setelah kejadian, dan hasilnya</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->tindakan_kejadian; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Tindakan dilakukan oleh
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['tindakan_dilakukan_oleh'] as $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="tdo" value="<?= $row; ?>" <?= $row_pelaporan->tindakan_dilakukan_oleh == $row ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Apakah kejadian yang sama pernah terjadi di unit kerja lain?</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->apakah_kejadian_pernah_terjadi; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>Apabila ya, isi bagian di bawah ini. Kapan? dan Langkah/tindakan apa yang telah diambil pada Unit Kerja tersebut untuk mencegah terulangnya kejadian yang sama?</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->form_kejadian_pasien; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Grading Insiden
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['grading_dampak'] as $key => $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="gd" value="<?= $key; ?>" <?= $row_pelaporan->grading_dampak == $key ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    Grading Frequensi
                    <ul class="checkbox">
                        <?php foreach( $option_kejadian['grading_frequensi'] as $key => $row ): ?>
                            <li>
                                <label>
                                    <input type="checkbox" name="gf" value="<?= $key; ?>" <?= $row_pelaporan->grading_frequensi == $key ? "checked" : '' ?>>
                                    <?= $row; ?>
                                </label> 
                            </li> 
                        <?php endforeach; ?>
                    </ul>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>Hasil Grading</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->hasil_grading; ?></td>
            </tr>
            <tr>
                <th></th>
                <td>Kategori Grading</td>
                <td>:</td>
                <td><?php echo $row_pelaporan->kategori_grading; ?></td>
            </tr>
        </table>
    </div>
</div>