<style>
    .add-tdclass{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
 <?php
 $kalender = CAL_GREGORIAN;
 $hari = cal_days_in_month($kalender, $bulan, $tahun);
 ?>
<div class="title-content-table">
    <h3>Insiden Keselamatan Pasien (IKP RS) - <?= ql_namabulan((int) $bulan).' '.$tahun; ?></h3>
</div>
<div class="msg-mutu"></div>
<div class="table-responsive">
<div class="err-msg-pendataan"></div>
<table class="table ql-dt table-hover table-striped">
    <thead>
        <tr class="header-table-ql">
            <?php if( !$is_superuser ): ?>
                <th width="5%" rowspan="2">No</th>
                <th width="60%" rowspan="2">IKP RS</th>
                <th colspan="<?=$hari;?>" style="text-align:center;"><?= ql_namabulan((int) $bulan); ?></th>
            <?php else: ?>
                <th width="5%">No</th>
                <th width="60%">IKP RS</th>
                <th style="text-align:center;"><?= ql_namabulan((int) $bulan); ?></th>
                <th width="10%" style="text-align:center;">[ ]</th>
            <?php endif; ?>
        </tr>
        <?php if( !$is_superuser ): ?>
        <tr class="header-table-ql">
            <?php 
            for ($i=1; $i <= $hari ; $i++) {
            echo "<th>".$i."</th>";
            } 
            ?>
        </tr>
        <?php endif; ?>
    </thead>
    <tbody>
    <?php
    if( $userid == 'without_superuser' ) $daftarikprs = [];
    $no = 1;
    foreach( $daftarikprs as $row_indikator ):
        if( !$is_superuser ):
            $get_judul=$this->db->select("daftar_ikp_id")
                                ->from('mutu_pengaturanikp')
                                ->where("userid_akses",$userid)
                                ->get()->row();
        if(!in_array($row_indikator->id_daftarikp,explode(',',$get_judul->daftar_ikp_id))){
            continue;
        }
        endif;

    ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $row_indikator->judulikp; ?>
                <input type="hidden" value="<?= $row_indikator->id_daftarikp; ?>" class="hiddendaftarikp"/>
            </td>
            <?php if( !$is_superuser ):
            for ($i=1; $i <= $hari ; $i++) { 
                $get_nilai=$this->db->select("*")
                                    ->from('mutu_rekapikp')
                                    ->where("tanggal",$i)
                                    ->where("bulan",$bulan)
                                    ->where("tahun",$tahun)
                                    ->where("daftarikp_id",$row_indikator->id_daftarikp)
                                    ->where("author",$userid)
                                    ->get()->row();

                $nilai_n = ( empty($get_nilai) ) ? '' : $get_nilai->nilai_survey;               
                
       
            ?>
            <td>
                <input type="radio"  data-tgl="<?= $i; ?>" data-bln="<?= $bulan; ?>" data-thn="<?= $tahun; ?>" class="ikppilihan" name="ikpls[<?= $row_indikator->id_daftarikp .'/'. $i .'/'. $bulan .'/'. $tahun; ?>]" value="1" <?= ( ($nilai_n == '1') ? 'checked':''); ?>/> Y <br>
                <input type="radio"  data-tgl="<?= $i; ?>" data-bln="<?= $bulan; ?>" data-thn="<?= $tahun; ?>" class="ikppilihan" name="ikpls[<?= $row_indikator->id_daftarikp .'/'. $i .'/'. $bulan .'/'. $tahun; ?>]" value="0" <?= ( ($nilai_n == '0') ? 'checked':''); ?>/> T
            </td>
            <?php } ?>
            <?php else: ?>
                <?php
                $get_hasil=$this->db->select('SUM(nilai_survey) as nilai')
                                    ->from('mutu_rekapikp')
                                    ->where("bulan",$bulan)
                                    ->where("tahun",$tahun)
                                    ->where("daftarikp_id",$row_indikator->id_daftarikp)
                                    ->get()->row();
                $hasil_nilai = $get_hasil->nilai;
                ?>
            <td style="text-align:center;"><a href="javascript:void(0)" data-bln="<?= $bulan; ?>" data-th="<?= $tahun; ?>"><?php echo $hasil_nilai; ?></a></td>
            <td style="text-align:center;"><button class="btn btn-sm btn-success view-detailikp" data-id="<?= $row_indikator->id_daftarikp; ?>" type="button" data-bln="<?= $bulan; ?>" data-th="<?= $tahun; ?>"><i class="fa fa-eye"></i> Detail</button></td>
            <?php endif; ?>    
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>