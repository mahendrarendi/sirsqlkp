<style>
.required {
   color:red;
}
.ql-inline {
    display: inline-block;
    width: calc(55% - 40px);
}
.ql-group {
    margin-bottom: 15px;
}
.ql-inline .form-group {
    padding: 0 5px;
}
.ajax-table-pendataan {
    margin-top: 10px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php 
                        $judul_indikator  = isset($get_edit->judul_indikator) ? $get_edit->judul_indikator : '';
                        $cara_perbaikan   = isset($get_edit->cara_perbaikan) ? $get_edit->cara_perbaikan : '';
                        $siklus           = isset($get_edit->siklus) ? $get_edit->siklus : '';
                        $plan_rencana     = isset($get_edit->plan_rencana) ? $get_edit->plan_rencana : '';
                        $plan_harap       = isset($get_edit->plan_harap) ? $get_edit->plan_harap : '';
                        $do               = isset($get_edit->do) ? $get_edit->do : '';
                        $study            = isset($get_edit->study) ? $get_edit->study : '';
                        $act              = isset($get_edit->act) ? $get_edit->act : '';
                        $penggungjawab    = isset($get_edit->penanggungjawab) ? $get_edit->penanggungjawab : '';
                    ?>
                    <form class="form" id="frm-tambahpdsa" enctype="multipart/form-data">
                        <div class="err-msg"></div>
                        <?php if($mode == 'edit'): ?>
                            <input type="hidden" name="paramid" required value="<?= $paramid; ?>">
                        <?php endif; ?>
                        <div class="ql-group">
                            <label>JUDUL INDIKATOR <span class="required">*</span> </label>
                            <select name="judul" class="form-control ql-select2 judulindikator" required>
                                <option value="">-- Pilih --</option>
                                <?php foreach( $allindikator as $indikator ): ?>
                                    <option <?= ($indikator['id_indikator'] == $judul_indikator ) ? 'selected' : ''; ?> value="<?= $indikator['id_indikator']; ?>" ><?= $indikator['judul_indikator']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="ql-group">
                            <div class="form-group">
                                <label>CARA PERBAIKAN <span class="required">*</span></label>
                                <input value="<?= $cara_perbaikan; ?>" type="text" name="cara_perbaikan" autocomplete="off" placeholder="Cara Perbaikan" required class="form-control">
                            </div>
                        </div>
                        <div class="ql-group">
                            <div class="form-group">
                                <label>SIKLUS <span class="required">*</span></label>
                                <input value="<?= $siklus; ?>" type="text" name="siklus" autocomplete="off" placeholder="Siklus" required class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>PLAN <span class="required">*</span></label><br>
                            <label>Saya berencana:</label>
                            <textarea value="<?= $plan_rencana; ?>" required name="plan_rencana" class="form-control textarea" placeholder="Saya berencana..." ><?= $plan_rencana; ?></textarea>
                            <label>Saya berharap:</label>
                            <textarea value="<?= $plan_harap; ?>" required name="plan_harap" class="form-control textarea" placeholder="Saya berharap..." ><?= $plan_harap; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Tambah Pendataan Indikator</label>
                            <div class="panel">
                                <div class="panel-body" style="background: #e5e5e5;">
                                    
                                    <button type="button" class="btn btn-primary btn-sm tambah-tindakan"> <i class="fa fa-plus-square"></i> Tambah</button>

                                    <div class="ajax-table-tindakan">
                                        <input type="hidden" required class="cek_datatindakan" name="cek_datatindakan" value="<?= ($mode == 'edit') ? count($all_pendataan) : ''; ?>">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Tindakan</th>
                                                    <th>PIC</th>
                                                    <th>Tanggal</th>
                                                    <th>[ ]</th>
                                                </tr>
                                            </thead>
                                            <tbody class="ajax-tbody">
                                                <?php if( $mode == 'edit' ): ?>
                                                    <?php foreach($all_pendataan as $row_pendataan): ?>
                                                    <tr>
                                                        <td> <input type="hidden" name="tindakan[]" value="<?= $row_pendataan->tindakan; ?>" /><?= nl2br($row_pendataan->tindakan); ?></td>
                                                        <td> <input type="hidden" name="pic[]" value="<?= $row_pendataan->pic; ?>" /><?= $row_pendataan->pic; ?></td>
                                                        <td> <input type="hidden" name="tanggal[]" value="<?= $row_pendataan->tanggal; ?>" /><?= $row_pendataan->tanggal; ?></td>
                                                        <td>
                                                            <button class="btn btn-sm btn-danger remove-item-tindakan" type="button"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>DO <span class="required">*</span></label>
                            <textarea value="<?= $do; ?>" required name="do" class="form-control textarea" rows="4" placeholder="Yang saya amati..." ><?= $do; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>STUDY <span class="required">*</span></label>
                            <textarea value="<?= $study; ?>" required name="study" class="form-control textarea" rows="4" placeholder="Yang saya pelajari ..." ><?= $study; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>ACT <span class="required">*</span></label>
                            <textarea value="<?= $act; ?>" required name="act" class="form-control textarea" rows="4" placeholder="Yang disimpulkan dari iklus ini ..." ><?= $act; ?></textarea>
                        </div>
                        <div class="ql-group">
                            <div class="form-group">
                                <label>Nama Penaggung Jawab Perbaikan <span class="required">*</span></label>
                                <input value="<?= $penggungjawab; ?>" type="text" name="penanggungjawab" autocomplete="off" placeholder="Penanggungjawab" required class="form-control">
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label>Upload Dokumen Bukti Tindak Lanjut </label><br> -->
                            <!-- <input required type="file" class="form-control" name="file" id="file"></td> -->
                            <!-- <button type="button" class="btn btn-md btn-primary popup-upload" data-id="">Upload <i class="fa fa-upload"></i></button><br> -->
                        <!-- </div> -->

                        <!-- <div class="clik-popup-file">
                            <a href="javascript:void(0)" class="popup-upload">Upload Document</a>
                        </div>
                        <div class="ajax-table-dokumen">
                            <input type="hidden" required class="cek_datadokumen" name="cek_datadokumen" value="<?= ($mode == 'edit') ? count($all_dok) : ''; ?>">
                            <table class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Filename</th>
                                                    <th>Path</th>
                                                    <th>[ ]</th>
                                                </tr>
                                            </thead>
                                            <tbody class="ajax-tbody">
                                                <?php if( $mode == 'edit' ): ?>
                                                    <?php foreach($all_dok as $row_dok): ?>
                                                    <tr>
                                                        <td> <input type="hidden" name="filename[]" value="<?= $row_dok->filename; ?>" /><?= $row_dok->filename; ?></td>
                                                        <td> <input type="hidden" name="pathname[]" value="<?= $row_dok->pathname; ?>" /><?= $row_dok->pathname; ?></td>
                                                        <td>
                                                            <button class="btn btn-sm btn-danger remove-item-dok" type="button"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                        </div> -->

                        <div class="form-group" style="text-align:right;">
                            <a href="<?= base_url('cindikatormutu/lembarpdsa') ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-left "></i> Kembali</a>
                            <button type="submit" class="btn btn-primary btn-sm" name="upload" value="Upload"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>

</section>

<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->