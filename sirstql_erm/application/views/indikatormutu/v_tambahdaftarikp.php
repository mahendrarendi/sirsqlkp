<style>
.required {
   color:red;
}
.ql-inline {
    display: inline-block;
    width: calc(55% - 40px);
}
.ql-group {
    margin-bottom: 15px;
}
.ql-inline .form-group {
    padding: 0 5px;
}
.ajax-table-pendataan {
    margin-top: 10px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php 
                        $judulikp      = isset($get_edit->judulikp) ? $get_edit->judulikp : '';
                    ?>
                    <form class="form" id="frm-tambahdaftarikp">
                        <div class="err-msg"></div>
                        <?php if($mode == 'edit'): ?>
                            <input type="hidden" name="paramid" required value="<?= $paramid; ?>">
                        <?php endif;?>
                        <div class="ql-group">
                                <div class="form-group">
                                    <label>Judul IKP <span class="required">*</span></label>
                                    <textarea value="<?= $judulikp; ?>" required name="judulikp" class="form-control tiny" placeholder="Silahkan isi Judul ... " cols="50" rows="10"><?= $judulikp; ?></textarea>
                                </div>
                        </div>
                        <div class="form-group" style="text-align:right;">
                            <a href="<?= base_url('Cindikatormutu/daftarikp') ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-left "></i> Kembali</a>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>

</section>

<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->