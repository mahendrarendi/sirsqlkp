<style>
    .panel .panel-header {
        padding: 10px;
        background: #ededed;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
</style>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-header">
                    <a href="<?= base_url('Cindikatormutu/tambah_aksesikp'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Tambah Akses</a>
                    <a href="<?= base_url('Cindikatormutu/pengaturanikp'); ?>" data-toggle="tooltip" data-placement="bottom" title="Refresh" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="panel-body">
                    <div class="table-content">
                        <table class="table table-hover table-striped ql-dt">
                            <thead>
                                <tr class="header-table-ql">
                                    <th width="5%">No</th>
                                    <th>Unit</th>
                                    <th>Akses IKP</th>
                                    <th>Akses</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no =1;
                                foreach( $list_pengaturan as $row_pengaturan): ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row_pengaturan['unit']; ?></td>
                                    <td><?= get_judulikp_pengaturan($row_pengaturan['daftar_ikp_id']); ?></td>
                                    <td><?= get_username_login($row_pengaturan['userid_akses']); ?></td>
                                    <td>
                                        <a href="<?= base_url('Cindikatormutu/tambah_aksesikp/'.$row_pengaturan['id_pengaturanikp']); ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" data-id="<?= $row_pengaturan['id_pengaturanikp']; ?>" class="btn btn-xs btn-danger remove-pengaturanikp"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>