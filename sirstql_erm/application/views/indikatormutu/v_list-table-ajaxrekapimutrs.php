
<div class="panel" style="background: #f7f7f7;">
    <div class="panel-header" style="padding: 5px 15px 5px 15px;background: #edd38c;"><h4>Rekap IMUT Lokal RS Periode :: <?= $tahun; ?></h4></div>
    <div class="panel-body">
        
        <div class="msg-mutu"></div>
        <table class="table ql-dt table-hover table-striped">
            <thead>
                <tr class="header-table-ql">
                    <th width="5%">No</th>
                    <th width="30%">Indikator</th>
                    <?php foreach( $jumlah_bulan as $row_bulan ): ?>
                        <th><?= ql_namabulan((int) $row_bulan); ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                if( !empty($bulan) ){
                    $bulan_new = ( strlen($bulan) < 2 ) ? '0'.$bulan : $bulan;
                }

                foreach( $daftarindikator as $key=> $row_indikator ): 
                    // $get_hasil = $this->mmutu->get_hasil_rekap($row_indikator->id_indikator);
                    if( !empty($bulan) ){
                        $get_hasil = $this->mmutu->get_hasil_mutu($row_indikator->id_indikator,$bulan_new,$tahun);
                    }else{
                        $get_hasil = $this->mmutu->get_hasil_mutu($row_indikator->id_indikator,"",$tahun);
                    }


                    $hasil_nume = [];
                    $hasil_denume = [];
                    foreach($get_hasil as $row_hasil){
                        $hasil_nume []= ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : '';
                        $hasil_denume []= ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                    }
                    
                    $numerator      = remove_empty_value_array($hasil_nume);
                    $denumerator    = remove_empty_value_array($hasil_denume);

                    $c_num      = count($numerator);
                    $c_denum    = count($denumerator);
                    
                    if( $c_num != $c_denum ){
                        $c_gabung = $c_num - $c_denum;
                        for ($i=1; $i <= $c_gabung ; $i++) { 
                            $h = $c_denum + $i;
                            $g = $h - 1;
                            $denumerator[$g] = '';
                        }
                    }

                    // echo '<pre>';
                    // print_r( $numerator );
                    // print_r('--------------------------------------');
                    // print_r( $denumerator );
                    // echo '</pre>';

                    $rumus=[];
                    foreach($numerator as $key => $nemu){
                        $denum = $denumerator[$key];
                        
                        if( $denum == 0 && $nemu == 0  ){
                            $rumus [] = '';
                        }elseif($nemu != 0 && $denum == 0){
                            $rumus [] = '';
                        }else{
                            $rumus[]  = round( $nemu/$denum,2 );
                        }
                    }

                    $hasil_rekap = !empty($rumus) ? (round(array_sum($rumus)/count($rumus),2)*100).'%' : '';
                    

                ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $row_indikator->judul_indikator; ?></td>
                    <?php 
                    foreach( $jumlah_bulan as $row_bulan_td ):                                           
                    ?>
                        <td>
                            <?php 

                                if( !empty($get_hasil) || empty($bulan) ){
                                    $hasil_nume = [];
                                    $hasil_denume = [];
                                    foreach($get_hasil as $row_hasil){
                                        if( $row_hasil->bulan == $row_bulan_td ){
                                            $hasil_nume []= ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : '';
                                            $hasil_denume []= ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                                        }
                                    }

                                    $numerator      = remove_empty_value_array($hasil_nume);
                                    $denumerator    = remove_empty_value_array($hasil_denume);
                                    
                                    $c_num      = count($numerator);
                                    $c_denum    = count($denumerator);
                                    
                                    if( $c_num != $c_denum ){
                                        $c_gabung = $c_num - $c_denum;
                                        for ($i=1; $i <= $c_gabung ; $i++) { 
                                            $h = $c_denum + $i;
                                            $g = $h - 1;
                                            $denumerator[$g] = '';
                                        }
                                    }
                                    
                                    $rumus=[];
                                    foreach($numerator as $key => $nemu){
                                        $denum = $denumerator[$key];
                                        
                                        if( $denum == 0 && $nemu == 0  ){
                                            $rumus [] = '';
                                        }elseif($nemu != 0 && $denum == 0){
                                            $rumus [] = '';
                                        }else{
                                            $rumus[]  = round( $nemu/$denum,2 );
                                        }
                                    }
                
                                    $hasil_rekap = !empty($rumus) ? (round(array_sum($rumus)/count($rumus),2)*100).'%' : '';
                
                                    echo $hasil_rekap;

                                }else{
                                    if( $row_bulan_td == (int) $bulan_new ){    
                                        echo $hasil_rekap;
                                    }
                                }
                            ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>