<style>
.panel .panel-header {
    padding: 10px;
    background: #ededed;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}
.required {
   color:red;
}
.ql-inline {
    display: inline-block;
    width: calc(50% - 50px);
}

.ql-inline .select2 {
    width: 100% !important;
}
.ql-group {
    margin-bottom: 15px;
}
.cek-err small {
    background: #d52222;
    padding: 6px 10px;
    border-radius: 4px;
    color: white;
}

.cek-err {
    margin-bottom: 10px;
}
.alert-abu {
    background: #ededed;
    height: auto;
    margin-bottom: 0;
}
</style>

<?php 
     $idpengaturanikp  = empty($get_pengaturanikp) ? '' : $get_pengaturanikp['id_pengaturanikp']; 
     $unit             = empty($get_pengaturanikp) ? '' : $get_pengaturanikp['unit']; 
     $userid_akses     = empty($get_pengaturanikp) ? '' : $get_pengaturanikp['userid_akses']; 
     $daftar_ikp_id    = empty($get_pengaturanikp) ? '' : explode(',',$get_pengaturanikp['daftar_ikp_id']); 

     $count_cek_table  = empty($daftar_ikp_id) ? 0 : count($daftar_ikp_id); 
?>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form class="form" id="frm-pengaturanikpakses">
                        <?php if( $mode == 'edit' ): ?>
                            <input type="hidden" class="form-control" name="idpengaturanikp_hidden" value="<?= $idpengaturanikp; ?>">
                        <?php endif; ?>
                        <div class="err-msg"></div>
                        <div class="form-group">
                            <label>Pilih User <span class="required">*</span> </label>
                            <select name="alluser" class="form-control ql-select2 userpengturanmutu" required>
                                <option value="">-- Pilih --</option>
                                <?php foreach( $allusers as $user ): 
                                    $selected = ($user['user_id_login'] == $userid_akses) ? 'selected' : '';    
                                ?>
                                    <option <?= $selected; ?> value="<?= $user['user_id_login']; ?>"><?= get_username_login($user['user_id_login']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Nama Unit <span class="required">*</span></label>
                            <div class="unit-desc">
                                <?php $nama_unit = ( empty($unit) ) ? 'Silahkan Pilih User Unit Terlebih Dahulu' : $unit; ?>                                
                                <div class="alert alert-abu"><?= $nama_unit; ?></div>
                            </div>
                            <input value="<?= $unit; ?>" required type="hidden" autocomplete="off" class="form-control" name="unit" placeholder="Masukan Nama Unit ...">
                        </div>
                        <div class="form-group">
                            <label>Tambahkan Daftar IKP Akses <span class="required">*</span></label>
                            <div class="ql-group">
                                <div class="ql-inline">
                                    <select class="form-control ql-select2 allindikator-akses">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach( $allindikator as $row ): ?>
                                            <option value="<?= $row->id_daftarikp; ?>"><?= $row->judulikp; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="ql-inline">
                                    <button class="btn btn-primary btn-sm tambah-item-ikp" type="button"><i class="fa fa-plus-square"></i> Tambah IKP</button>
                                </div>
                            </div>
                            <div class="ql-tabledata">
                                <table class="table table-striped table-bordered">
                                    <thead style="background: #ecf0f5;">
                                        <tr>
                                            <th width="80%">Judul IKP</th>
                                            <th>[X]</th>
                                        </tr>
                                    </thead>
                                    <tbody class="ajax-tableselect-ikp">
                                        <?php if( !empty($daftar_ikp_id) ): ?>
                                            <?php 
                                            foreach( $allindikator as $row_indikator ): 
                                                if( !in_array($row_indikator->id_daftarikp,$daftar_ikp_id) ) continue;
                                            ?>
                                            <tr>
                                                <td> <input type="hidden" name="idindikator[]" value="<?= $row_indikator->id_daftarikp; ?>"/><?= $row_indikator->judulikp; ?></td>
                                                <td><a data-toggle="tooltip" data-placement="bottom" title="Hapus IKP" class="remove-ikp-item btn btn-xs btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                            </tr>

                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group" style="text-align:right;">
                            <a href="<?= base_url('Cindikatormutu/pengaturanikp'); ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
                            <button <?= ($count_cek_table > 0) ? '' : 'disabled'; ?> type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>