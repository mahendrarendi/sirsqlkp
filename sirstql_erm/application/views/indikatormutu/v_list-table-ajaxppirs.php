<style>
    .add-tdclass{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
 <?php
 $kalender = CAL_GREGORIAN;
 $hari = cal_days_in_month($kalender, $bulan, $tahun);
 ?>
<div class="title-content-table">
    <h3>Pencegahan Pengendalian Infeksi (PPI RS) - <?= ql_namabulan((int) $bulan).' '.$tahun; ?></h3>
</div>
<div class="msg-mutu"></div>
<div class="table-responsive">
<table class="table ql-dt table-hover table-striped">
    <thead>
        <tr class="header-table-ql">
            <?php if( !$is_superuser ): ?>
                <th width="5%" rowspan="2">No</th>
                <th width="60%" rowspan="2">PPI RS</th>
                <th colspan="<?=$hari;?>"><?= ql_namabulan((int) $bulan); ?></th>
            <?php else: ?>
                <th width="5%">No</th>
                <th width="60%">PPI RS</th>
                <th style="text-align:center;"><?= ql_namabulan((int) $bulan); ?></th>
                <th width="10%" style="text-align:center;">[ ]</th>
            <?php endif; ?>
        </tr>
        <?php if( !$is_superuser ): ?>
        <tr class="header-table-ql">
            <?php 
            for ($tanggal=1; $tanggal <= $hari ; $tanggal++) {
            echo "<th>".$tanggal."</th>";
            } 
            ?>
        </tr>
        <?php endif; ?>
    </thead>
    <tbody>
    <?php
    if( $userid == 'without_superuser' ) $daftarppirs = [];
    $no = 1;
    foreach( $daftarppirs as $row_indikator ):
        if( !$is_superuser ):
            $get_judul=$this->db->select("daftarppi_id")
                                ->from('mutu_pengaturanppi')
                                ->where("userid_akses",$userid)
                                ->get()->row();
        if(!in_array($row_indikator->id_daftarppi,explode(',',$get_judul->daftarppi_id))){
            continue;
        }
        endif;

        $add_class = ($is_superuser) ? 'add-tdclass' : 'no-class';
        $add_style = ($is_superuser) ? 'color:black;' : '';
      
    ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $row_indikator->judul_ppi; ?>
                <input type="hidden" value="<?= $row_indikator->id_daftarppi; ?>" class="hiddendaftarppi"/>
            </td>
            <?php if(!$is_superuser): ?>
            <?php 
            for ($tanggal=1; $tanggal <= $hari ; $tanggal++) {
                $get_pendataan = $this->mmutu->get_editpendataanppi_row($row_indikator->id_daftarppi);

                $idpendataan = $get_pendataan->id_pendataan;
                
                $get_hasil = $this->mmutu->get_hasil_ppi($row_indikator->id_daftarppi,$tanggal,$bulan,$tahun,$userid);

                $hasil_nume     = [];
                $hasil_denume   = [];
                
                foreach($get_hasil as $row_hasil){
                    $hasil_nume[]    = ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : ''; 
                    $hasil_denume[]  = ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                }
                $sum_allnume       = array_sum($hasil_nume);
                $sum_alldenume     = array_sum($hasil_denume);

                    $sum_nume      = remove_empty_value_array($hasil_nume);
                    $sum_denume    = remove_empty_value_array($hasil_denume);
                
                if( $sum_allnume != 0 && $sum_alldenume != 0 ){
 
                    $rumus=[];
                    foreach($sum_nume as $key => $nemu){
                        $denum = $sum_denume[$key];
                                    
                        if( $denum == 0 && $nemu == 0  ){
                            $rumus [] = '';
                        }elseif($nemu != 0 && $denum == 0){
                            $rumus [] = '';
                        }else{
                            $rumus[]  = round( $nemu/$denum,2 );
                        }
                    }
                    $hasil = (round(array_sum($rumus)/count($rumus),2)*100).'%';

                }else{
                    $hasil = 0;
                }
                echo "<td style='text-align:center;'>
                        <table class='table table-bordered'>
                            <tr>
                                <td colspan='2'>
                                <a href='javascript:void(0)' class='input-hasil-pendataanppi' data-id=".$row_indikator->id_daftarppi." data-tgl=".$tanggal." data-bln=".$bulan." data-th=".$tahun." >$hasil</a>
                                </td>
                            </tr>
                            <tr>
                                <td data-id=".$row_indikator->id_daftarppi." data-tgl=".$tanggal." data-bln=".$bulan." data-th=".$tahun." >$sum_allnume</td>
                                <td data-id=".$row_indikator->id_daftarppi." data-tgl=".$tanggal." data-bln=".$bulan." data-th=".$tahun." >$sum_alldenume</td>
                            </tr>
                        </table>
                      </td>";
            }
            ?>
            <?php else:
                $get_pendataan2 = $this->mmutu->get_editpendataanppi_row($row_indikator->id_daftarppi);

                $idpendataan = $get_pendataan2->id_pendataan;
                
                $get_hasil2 = $this->mmutu->get_hasil_ppi2($row_indikator->id_daftarppi,$bulan,$tahun);

                $hasil_nume     = [];
                $hasil_denume   = [];
                
                foreach($get_hasil2 as $row_hasil){
                    $hasil_nume[]    = ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : ''; 
                    $hasil_denume[]  = ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                }
                $sum_allnume2       = array_sum($hasil_nume);
                $sum_alldenume2     = array_sum($hasil_denume);

                    $sum_nume      = remove_empty_value_array($hasil_nume);
                    $sum_denume    = remove_empty_value_array($hasil_denume);
                
                if( $sum_allnume2 != 0 && $sum_alldenume2 != 0 ){
        
                    $rumus=[];
                    foreach($sum_nume as $key => $nemu){
                        $denum = $sum_denume[$key];
                                    
                        if( $denum == 0 && $nemu == 0  ){
                            $rumus [] = '';
                        }elseif($nemu != 0 && $denum == 0){
                            $rumus [] = '';
                        }else{
                            $rumus[]  = round( $nemu/$denum,2 );
                        }
                    }
                    $hasil2 = (round(array_sum($rumus)/count($rumus),2)*100).'%';

                }else{
                    $hasil2 = 0;
                }
                echo "<td style='text-align:center;'>
                        <table class='table table-bordered'>
                            <tr>
                                <td colspan='2'>
                                <a href='javascript:void(0)' data-id=".$row_indikator->id_daftarppi." data-bln=".$bulan." data-th=".$tahun." >$hasil2</a>
                                </td>
                            </tr>
                            <tr>
                                <td data-id=".$row_indikator->id_daftarppi." data-bln=".$bulan." data-th=".$tahun." >$sum_allnume2</td>
                                <td data-id=".$row_indikator->id_daftarppi." data-bln=".$bulan." data-th=".$tahun." >$sum_alldenume2</td>
                            </tr>
                        </table>
                      </td>"; 
            ?>
            <td class="<?= $add_class; ?>"><button class="btn btn-sm btn-success view-detailppi" data-id="<?= $row_indikator->id_daftarppi; ?>" type="button" data-bln="<?= $bulan; ?>" data-th="<?= $tahun; ?>"><i class="fa fa-eye"></i> Detail</button></td>
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>