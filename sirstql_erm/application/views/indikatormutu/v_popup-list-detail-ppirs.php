<div class="table-responsive">
<div class="popup-detail">
    <table class="table ql-dt table-striped table-hover">
        <thead>
            <tr class="header-table-ql">
                <th>Unit</th>
                <th style="text-align:center;">Hasil</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list_detail_ppirs as $row_detail):
            $get_pendataan = $this->mmutu->get_editpendataanppi_row($row_detail->daftarppi_id,$bulan,$tahun);

            $idpendataan = $get_pendataan->id_pendataan;
            
            $get_hasil = $this->mmutu->get_hasil_ppi3($row_detail->daftarppi_id,$row_detail->bulan,$tahun,$row_detail->author);
            
            print_r($get_hasil);

            $hasil_nume     = [];
            $hasil_denume   = [];
            
            foreach($get_hasil as $row_hasil){
                $hasil_nume[]    = ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : ''; 
                $hasil_denume[]  = ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
            }
            $sum_allnume       = array_sum($hasil_nume);
            $sum_alldenume     = array_sum($hasil_denume);

                $sum_nume      = remove_empty_value_array($hasil_nume);
                $sum_denume    = remove_empty_value_array($hasil_denume);
            
            if( $sum_allnume != 0 && $sum_alldenume != 0 ){
    
                $rumus=[];
                foreach($sum_nume as $key => $nemu){
                    $denum = $sum_denume[$key];
                                
                    if( $denum == 0 && $nemu == 0  ){
                        $rumus [] = '';
                    }elseif($nemu != 0 && $denum == 0){
                        $rumus [] = '';
                    }else{
                        $rumus[]  = round( $nemu/$denum,2 );
                    }
                }
                $hasil2 = (round(array_sum($rumus)/count($rumus),2)*100).'%';

            }else{
                $hasil2 = 0;
            }
            ?>
            <tr>
                <td><?= $this->mmutu->get_nama_unit_pengaturanppi($row_detail->author); ?></td>
                <td style="text-align:center;">
                    <table class='table table-bordered'>
                        <tr>
                            <td colspan='2'>
                            <a href='javascript:void(0)' data-bln="<?php $bulan; ?>" data-th="<?php $tahun; ?>" ><?php echo $hasil2; ?></a>
                            </td>
                        </tr>
                        <tr>
                            <td data-bln="<?php $bulan; ?>" data-th="<?php $tahun; ?>" ><?php echo $sum_allnume; ?></td>
                            <td data-bln="<?php $bulan; ?>" data-th="<?php $tahun; ?>" ><?php echo $sum_alldenume; ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>