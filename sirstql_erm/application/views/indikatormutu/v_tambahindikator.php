<style>
.required {
   color:red;
}
.ql-inline {
    display: inline-block;
    width: calc(55% - 40px);
}
.ql-group {
    margin-bottom: 15px;
}
.ql-inline .form-group {
    padding: 0 5px;
}
.ajax-table-pendataan {
    margin-top: 10px;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <?php 
                        $judul      = isset($get_edit->judul_indikator) ? $get_edit->judul_indikator : '';
                        $defenisi   = isset($get_edit->defenisi_operasional) ? $get_edit->defenisi_operasional : '';
                        $inklusi    = isset($get_edit->kriteria_inklusi) ? $get_edit->kriteria_inklusi : '';
                        $ekslusi    = isset($get_edit->kriteria_ekslusi) ? $get_edit->kriteria_ekslusi : '';
                        $target     = isset($get_edit->target) ? $get_edit->target : '';
                        $satuan     = isset($get_edit->satuan) ? $get_edit->satuan : '';
                    ?>
                    <form class="form" id="frm-tambahindikator">
                        <div class="err-msg"></div>
                        <?php if($mode == 'edit'): ?>
                            <input type="hidden" name="paramid" required value="<?= $paramid; ?>">
                        <?php endif; ?>
                        <div class="ql-group">
                            <div class="ql-inline">
                                <div class="form-group">
                                    <label>Judul Indikator <span class="required">*</span></label>
                                    <textarea value="<?= $judul; ?>" required name="judul_indikator" class="form-control tiny" placeholder="Silahkan isi Judul ... " cols="30" rows="10"><?= $judul; ?></textarea>
                                </div>
                            </div>
                            <div class="ql-inline">
                                <div class="form-group">
                                    <label>Defenisi Operasional <span class="required">*</span></label>
                                    <textarea value="<?= $defenisi; ?>" required name="defenisi_operasional" class="form-control tiny" placeholder="Silahkan isi Defenisi ..." cols="30" rows="10"><?= $defenisi; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="ql-group">
                            <div class="ql-inline">
                                <div class="form-group">
                                    <label>Kriteria Inklusi <span class="required">*</span></label>
                                    <textarea value="<?= $inklusi; ?>" required name="kriteria_inklusi" class="form-control" placeholder="Silahkan isi kriteria inklusi ..." ><?= $inklusi; ?></textarea>
                                </div>
                            </div>
                            <div class="ql-inline">
                                <div class="form-group">
                                    <label>Kriteria Ekslusi <span class="required">*</span></label>
                                    <textarea value="<?= $ekslusi; ?>" required name="kriteria_ekslusi" class="form-control" placeholder="Silahkan isi kriteria ekslusi ..." ><?= $ekslusi; ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tambah Pendataan Indikator</label>
                            <div class="panel">
                                <div class="panel-body" style="background: #e5e5e5;">
                                    
                                    <?php 
                                        $disable_button = ( $mode == 'edit' ) ? ( count($all_pendataan) == 2 ) ? 'disabled' : '' : ''; 
                                    ?>
                                    <button <?= $disable_button; ?> type="button" class="btn btn-primary btn-sm tambah-pendataan"> <i class="fa fa-plus-square"></i> Tambah</button>

                                    <div class="ajax-table-pendataan">
                                        <input type="hidden" required class="cek_dataindikator" name="cek_dataindikator" value="<?= ($mode == 'edit') ? count($all_pendataan) : ''; ?>">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Tipe Variable</th>
                                                    <th>Indikator</th>
                                                    <th>Satuan</th>
                                                    <th>[ ]</th>
                                                </tr>
                                            </thead>
                                            <tbody class="ajax-tbody">
                                                <?php if( $mode == 'edit' ): ?>
                                                    <?php foreach($all_pendataan as $row_pendataan): ?>
                                                    <tr>
                                                        <td> <input type="hidden" name="tipe_variable[]" value="<?= $row_pendataan->tipe_variabel; ?>" /><?= $row_pendataan->variabel; ?></td>
                                                        <td> <input type="hidden" name="indkator_pendataan[]" value="<?= $row_pendataan->indikator; ?>" /><?= $row_pendataan->indikator; ?></td>
                                                        <td> <input type="hidden" name="satuan[]" value="<?= $row_pendataan->satuan; ?>" /><?= $row_pendataan->satuan; ?></td>
                                                        <td>
                                                            <button class="btn btn-sm btn-danger remove-item-pendataan" type="button"><i class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Target <span class="required">*</span></label>
                            <input value="<?= $target; ?>" type="number" name="target" step="any" min="0" value="0" placeholder="Silahkan Isi Target ... " required class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Satuan <span class="required">*</span></label>
                            <input value="<?= $satuan; ?>" type="text" name="satuan_indikator" autocomplete="off" placeholder="Silahkan Isi satuan ... " required class="form-control">
                        </div>
                        <div class="form-group" style="text-align:right;">
                            <a href="<?= base_url('cindikatormutu/listdaftarimutrs') ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-left "></i> Kembali</a>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>

</section>

<!-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->