<style>
    .panel .panel-header {
        padding: 10px;
        background: #ededed;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
    
</style>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <a href="<?= base_url('cindikatormutu/tambah_indikator'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Tambah Indikator</a>
                    <a href="<?= base_url('cindikatormutu/listdaftarimutrs'); ?>" data-toggle="tooltip" data-placement="bottom" title="Refresh" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="panel-body">
                    <div class="table-content">
                        <table class="table table-hover table-striped ql-dt">
                            <thead>
                                <tr class="header-table-ql">
                                    <th>No</th>
                                    <th width="20%">Judul Indikator</th>
                                    <th width="20%">Definisi Operasional</th>
                                    <th width="20%">Kriteria Inklusi</th>
                                    <th width="20%">Kriteria Ekslusi</th>
                                    <th>Target</th>
                                    <th>Satuan</th>
                                    <th>[ ]</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach( $list as $row ): ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row->judul_indikator; ?></td>
                                    <td><?= $row->defenisi_operasional; ?></td>
                                    <td><?= $row->kriteria_inklusi; ?></td>
                                    <td><?= $row->kriteria_ekslusi; ?></td>
                                    <td><?= $row->target; ?></td>
                                    <td><?= $row->satuan; ?></td>
                                    <td>
                                        <a href="javascript:void(0)" data-id="<?= $row->id_indikator; ?>" class="btn btn-xs btn-success view-indikatormutu"><i class="fa fa-eye"></i></a>
                                        <a href="<?= base_url('cindikatormutu/tambah_indikator/'.$row->id_indikator); ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="javascript:void(0)" data-id="<?= $row->id_indikator; ?>" class="btn btn-xs btn-danger remove-indikatormutu"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>
    
</section>