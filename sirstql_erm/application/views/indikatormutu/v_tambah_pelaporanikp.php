<style>
    .umur-list,.jk-list,.penanggungjawab_pasien-list {
        padding-left: 0;
    }
    .umur-list .radio,.jk-list .radio,.penanggungjawab_pasien-list .radio {
        margin-top: 0;
    }

    .tb-form-survey tr th, .tb-form-survey tr td,.tb-form-survey tr th, .tb-form-survey {
        border: none !important;
    }
    .form-table {
        margin-top: 35px;
    }

    .bg-pp {
        background: #eee;
        padding: 15px;
        border-radius: 5px;
    }
    
    span.required{
        color:red;
    }

    #frm-laporan-internal-inseden ul li {
        list-style: none;
    }
    #frm-laporan-internal-inseden ul {
        margin: ;
        margin-left: 0;
        padding-left: 0;
        margin-bottom: 0;
    }

    #frm-laporan-internal-inseden ul .radio {
        margin: ;
        margin-top: 0;
    }

    .wg-text {
        display: inline-block;
        vertical-align: middle;
        font-size: 14px;
        height: 16px;
        margin: 0 4px;
    }
    .wg-circle {
        height: 30px;
        width: 30px;
        border-radius: 100px;
        display: inline-block;
        vertical-align: middle;
    }

</style>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="title-pp" style="text-align:center;">
                        <h4 style="text-transform:uppercase;font-weight:bold;">Format Laporan Internal Insiden</h4>
                        <p>Rumah Sakit Umum Queen Latifa</p>
                        <p style="text-transform:uppercase;font-size:12px;font-weight:bold;">Rahasia, Tidak Boleh difotocopy, dilaporkan maximal 2x24 Jam</p>
                        <p>( Laporan Insiden KNC, KTC, KTD dan Kejadian Sentinel )</p>
                    </div>

                    <hr>

                    <div class="form-table">
                        <form id="frm-laporan-internal-inseden">
                            <?php if( $mode == 'edit' ){ ?>
                                <input type="hidden" name="idpelaporan" value="<?= $_GET['ids']; ?>">
                            <?php } ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="title-pp">
                                        <p style="text-transform:uppercase;font-weight:bold;">I. Data Pasien (inisial)</p>
                                    </div>
                                    <div class="bg-pp">
                                        <table class="table table-bordered tb-form-survey">
                                            <tr>
                                                <td>NO RM <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <div class="form-group">
                                                        <!-- <select name="norm" class="form-control ql-select2-mutu" required></select> -->
                                                        <input value="<?= $getquery_get->norm; ?>" placeholder="No RM" type="text" autocomplete="off" class="form-control" name="norm" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">NAMA <span class="required">*</span></td>
                                                <td width="1%">:</td>
                                                <td width="50%">
                                                    <div class="form-group">
                                                        <input value="<?= $getquery_get->nama; ?>" placeholder="Nama Pasien" type="text" autocomplete="off" class="form-control" name="nama" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <!-- <tr>
                                                <td>UMUR SEKARANG</td>
                                                <td>:</td>
                                                <td>
                                                    <div class="form-group">
                                                        <input disabled placeholder="Umur Sekarang" type="text" autocomplete="off" class="form-control" name="umur_sekarang" required>
                                                    </div>
                                                </td> 
                                            </tr> -->
                                            <tr>
                                                <td>UMUR <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="umur-list">
                                                        <?php 
                                                        foreach( $form_datapasien['umur'] as $umur ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->umur == $umur ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="umur" value="<?= $umur; ?>"><?= $umur; ?></label>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="jk-list">
                                                        <?php 
                                                        foreach( $form_datapasien['jk'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->jk == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="jk" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Penanggungjawab Pasien <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="penanggungjawab_pasien-list">
                                                        <?php 
                                                        foreach( $form_datapasien['penanggungjawab_pasien'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->penanggungjawab_pasien == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="penanggungjawab_pasien" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tanggal/Jam Masuk RS <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <div class="form-inline" style="margin-bottom:5px;">
                                                        <input type="text" readonly required placeholder="Tgl RS" autocomplete="off" class="form-control datepicker-ql" name="tglmasuk_rs">
                                                    </div>
                                                    <div class="form-inline" style="margin-bottom:5px;">
                                                        <input type="text" value="<?= $getquery_get->jammasuk_rs; ?>" readonly required placeholder="Jam RS" autocomplete="off" class="form-control timepicker-ql" name="jammasuk_rs">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="title-pp">
                                        <p style="text-transform:uppercase;font-weight:bold;">II. RINCIAN KEJADIAN</p>
                                    </div>
                                    <div class="bg-pp">
                                        <table class="table table-bordered tb-form-survey">
                                            <tr>
                                                <td>Tanggal/Waktu Insiden <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <div class="form-inline" style="margin-bottom:5px;">
                                                        <input type="text" readonly required placeholder="Tgl Kejadian" autocomplete="off" class="form-control datepicker-ql" name="tglinsiden">
                                                    </div>
                                                    <div class="form-inline" style="margin-bottom:5px;">
                                                        <input type="text" value="<?= $getquery_get->jaminsiden; ?>" readonly required placeholder="Jam Kejadian" autocomplete="off" class="form-control timepicker-ql" name="jaminsiden">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">Insiden <span class="required">*</span></td>
                                                <td width="1%">:</td>
                                                <td width="50%">
                                                    <div class="form-group">
                                                        <textarea value="<?= $getquery_get->insiden; ?>" placeholder="Masukan Insiden" autocomplete="off" class="form-control" name="insiden" required cols="30" rows="5"><?= $getquery_get->insiden; ?></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="10%">Kronologi <span class="required">*</span></td>
                                                <td width="1%">:</td>
                                                <td width="50%">
                                                    <div class="form-group">
                                                        <textarea value="<?= $getquery_get->kronologi; ?>" placeholder="Masukan Kronologis" autocomplete="off" class="form-control" name="kronologi" required cols="30" rows="5"><?= $getquery_get->kronologi; ?></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Insiden <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="jenis_insiden-list">
                                                        <?php 
                                                        foreach( $form_insiden['jenis_insiden'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->jenis_insiden == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="jenis_insiden" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Orang Pertama yang melaporkan Insiden <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="orang_pertama_lapor-list">
                                                        <?php 
                                                        foreach( $form_insiden['orang_pertama_lapor'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->orang_pertama_lapor == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="orang_pertama_lapor" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Insiden terjadi pada <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="insiden_terjadi-list">
                                                        <?php 
                                                        foreach( $form_insiden['insiden_terjadi'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->insiden_terjadi == $row ) ? 'checked' : ''; ?>  required type="radio" class="pp-form-input" name="insiden_terjadi" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                    <?php if( $getquery_get->insiden_terjadi == 'Lain-Lain' ){ ?>
                                                        <div class="insiden-form">
                                                            <textarea value="<?= $getquery_get->insiden_terjadi_lainlain; ?>" class="form-control" placeholder="Mis: Karyawan/Pengunjung/Pendamping?Keluarga pasien lapor ke K3 RS" name="insiden_terjadi_lainlain" required=""><?= $getquery_get->insiden_terjadi_lainlain; ?></textarea>
                                                        </div>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Insiden Menyangkut pasien <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="insiden_menyangkut_pasien-list">
                                                        <?php 
                                                        foreach( $form_insiden['insiden_menyangkut_pasien'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->insiden_menyangkut_pasien == $row ) ? 'checked' : ''; ?>  required type="radio" class="pp-form-input" name="insiden_menyangkut_pasien" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                    <?php if($getquery_get->insiden_menyangkut_pasien == 'Lain-Lain'){ ?>
                                                        <div class="insiden-form-menyangkut-pasien">
                                                            <textarea value="<?= $getquery_get->form_insiden_menyangkut_pasien; ?>" class="form-control" placeholder="Tuliskan ..." name="form_insiden_menyangkut_pasien" required=""><?= $getquery_get->form_insiden_menyangkut_pasien; ?></textarea>
                                                        </div>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tempat Insiden <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <textarea  value="<?= $getquery_get->tempat_insiden; ?>"  required class="form-control" name="tempat_insiden"  cols="30" rows="5"><?= $getquery_get->tempat_insiden; ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Insiden terjadi pada pasien	: (sesuai kasus penyakit/spesialisasi) <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="insiden_terjadi_pada_pasien-list">
                                                        <?php 
                                                        foreach( $form_insiden['insiden_terjadi_pada_pasien'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->insiden_terjadi_pada_pasien == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="insiden_terjadi_pada_pasien" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Unit/departemen terkait yang menyebabkan insiden, Unit Kerja Penyebab <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <textarea value="<?= $getquery_get->unit_terkait; ?>"  required class="form-control" name="unit_terkait"  cols="30" rows="5"><?= $getquery_get->unit_terkait; ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Akibat insiden terhadap pasien <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="akibat_insiden_pasien-list">
                                                        <?php 
                                                        foreach( $form_insiden['akibat_insiden_pasien'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->akibat_insiden_pasien == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="akibat_insiden_pasien" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tindakan yang dilakukan segera setelah kejadian, dan hasilnya: <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <textarea value="<?= $getquery_get->tindakan_kejadian; ?>" required class="form-control" name="tindakan_kejadian"  cols="30" rows="5"><?= $getquery_get->tindakan_kejadian; ?></textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tindakan dilakukan oleh <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="tindakan_dilakukan_oleh-list">
                                                        <?php 
                                                        foreach( $form_insiden['tindakan_dilakukan_oleh'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->tindakan_dilakukan_oleh == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="tindakan_dilakukan_oleh" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Apakah kejadian yang sama pernah terjadi di unit kerja lain <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="apakah_kejadian_pernah_terjadi-list">
                                                        <?php 
                                                        foreach( ['Ya','Tidak'] as $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->apakah_kejadian_pernah_terjadi == $row ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="apakah_kejadian_pernah_terjadi" value="<?= $row; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="note-kejadianterjadi">
                                                        <p>Apabila ya, isi bagian di bawah ini. Kapan?dan Langkah/tindakan apa yang telah diambil pada Unit Kerja tersebut untuk mencegah terulangnya kejadian yang sama?</p>
                                                        <?php if( $getquery_get->apakah_kejadian_pernah_terjadi == 'Ya' ){ ?>
                                                            <div class="insiden-form-kejadian-pasien">
                                                                <textarea value="<?= $getquery_get->form_kejadian_pasien; ?>" class="form-control" placeholder="Tuliskan ..." name="form_kejadian_pasien" required=""><?= $getquery_get->form_kejadian_pasien; ?></textarea>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Grading Insiden <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="grading_dampak-list">
                                                        <?php 
                                                        foreach( $form_insiden['grading_dampak'] as $key => $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->grading_dampak == $key ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="grading_dampak" value="<?= $key; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Grading Frequensi <span class="required">*</span></td>
                                                <td>:</td>
                                                <td>
                                                    <ul class="grading_frequensi-list">
                                                        <?php 
                                                        foreach( $form_insiden['grading_frequensi'] as $key => $row ){ ?>
                                                            <li>
                                                                <div class="radio">
                                                                    <label><input <?= ($getquery_get->grading_frequensi == $key ) ? 'checked' : ''; ?> required type="radio" class="pp-form-input" name="grading_frequensi" value="<?= $key; ?>"><?= $row; ?></label>
                                                                </div>
                                                            </li>
            
                                                        <?php } ?>
                                                    </ul>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">
                                                            
                                                <div class="hasil_grading">
                                                    <div class="keterangan_grading">
                                                        <p>Hasil Grading : <input type="text" value="<?= $getquery_get->hasil_grading; ?>" readonly required name="hasil_grading" autocomplete="off" class="form-control"></p>
                                                    </div>
                                                    <div class="kategori_grading">
                                                        <p>Kategori Grading : 
                                                            <span class="kat_warna_grading">
                                                                <?php if( !empty( $getquery_get->kategori_grading) ){ 
                                                                    $explode = explode( '|',$getquery_get->kategori_grading );
                                                                    $warna = $explode['0'];
                                                                    $ket = $explode['1'];
                                                                ?>
                                                                    <div class="wg">
                                                                        <div class="wg-circle" style="background-color:<?= $warna; ?>;"></div>
                                                                        <div class="wg-text"><?= $ket; ?></div>
                                                                    </div>
                                                                <?php } ?>
                                                            </span>

                                                            <input value="<?= $getquery_get->kategori_grading; ?>" type="hidden" readonly required name="kategori_grading" autocomplete="off" class="form-control">
                                                        </p>
                                                    </div>
                                                </div>
                                                </td>
                                            </tr>
                                            <?php if( $mode == 'edit' ){ ?>
                                            <tr>
                                                <td colspan="3">
                                                    <a href="javascript:void(0)" data-id="<?= $getquery_get->idpelaporanikp; ?>" class="btn btn-success investigasi_sederhana">Investigasi</a>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <tr>
                                                <td colspan="3" style="text-align:center;">
                                                    
                                                    <hr style="border: 1px solid #c6c6c6;">
                                                    <button type="button" onclick="history.back()" class="btn btn-warning btn-lg"><i class="fa fa-arrow-left"></i> Kembali</button>
                                                    <button class="btn btn-primary btn-lg"><i class="fa fa-paper-plane"></i> Kirim</button>
                                                </td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>
   <!-- End Of Modal -->
</section>
<!-- <script>
    $('.investigasi_sederhana').attr('disabled', true);
    $("#submit").click(function() {
        $('.investigasi_sederhana').attr('disabled', false);
    });
</script> -->