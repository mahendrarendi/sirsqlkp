<div class="table-responsive">
<div class="popup-detail">
    <table class="table ql-dt table-striped table-hover">
        <thead>
            <tr class="header-table-ql">
                <th>Unit</th>
                <th>Hasil</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list_detail_ikprs as $row_detail):
            $no = 1; 
            $hasil=$this->db->select('SUM(nilai_survey) as nilai,bulan,tahun')
                            ->from('mutu_rekapikp')
                            ->where("daftarikp_id",$row_detail->daftarikp_id)
                            ->where("bulan",$row_detail->bulan)
                            ->where("tahun",$row_detail->tahun)
                            ->where("author",$row_detail->author)
                            ->get()->row();
            $hasil_nilai = $hasil->nilai;
            ?>
            <tr>
                <td><?= $this->mmutu->get_nama_unit_pengaturanikp($row_detail->author); ?></td>
                <td><?= $hasil_nilai; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>