<style>
    .panel .panel-header {
        padding: 10px;
        background: #ededed;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
    }
    
</style>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-header">
                    <a href="<?= base_url('Cindikatormutu/tambah_datapdsa'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Tambah PDSA</a>
                    <a href="<?= base_url('Cindikatormutu/lembarpdsa'); ?>" data-toggle="tooltip" data-placement="bottom" title="Refresh" class="btn btn-danger btn-sm"><i class="fa fa-refresh"></i></a>
                </div>
                <div class="panel-body">
                    <div class="table-content">
                        <table class="table table-hover table-striped ql-dt">
                            <thead>
                                <tr class="header-table-ql">
                                    <th width="5%">No</th>
                                    <th>Judul Indikator</th>
                                    <th>Penanggungjawab</th>
                                    <th width="10%" style="text-align:center;">[ ]</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach( $list as $row ): 
                                $encode_id = base64_encode($row->idpdsa);
                                $rows=$this->db->select("*")
                                                ->from('mutu_indikator')
                                                ->where("id_indikator",$row->judul_indikator)
                                                ->get()->row();
                                ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $rows->judul_indikator; ?></td>
                                    <td><?= $row->penanggungjawab; ?></td>
                                    <td>
                                        <a href="<?= base_url('Cindikatormutu/tambah_datapdsa/'.$row->idpdsa); ?>" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i></a>
                                        <a href="javascript:void(0)" data-id="<?= $row->idpdsa; ?>" class="btn btn-xs btn-danger remove-datapdsa"><i class="fa fa-trash"></i></a>
                                        <a href="<?= base_url('Cindikatormutu/printlembarpdsa/'.$encode_id); ?>" data-id="<?= $row->idpdsa; ?>" class="print-lembarpdsa btn btn-success btn-xs"><i class="fa fa-file-pdf-o"></i></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>
    
</section>