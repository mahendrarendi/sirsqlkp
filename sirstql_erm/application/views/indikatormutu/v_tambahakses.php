<style>
.panel .panel-header {
    padding: 10px;
    background: #ededed;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}
.required {
   color:red;
}
.ql-inline {
    display: inline-block;
    width: calc(50% - 50px);
}

.ql-inline .select2 {
    width: 100% !important;
}
.ql-group {
    margin-bottom: 15px;
}
.cek-err small {
    background: #d52222;
    padding: 6px 10px;
    border-radius: 4px;
    color: white;
}

.cek-err {
    margin-bottom: 10px;
}
.alert-abu {
    background: #ededed;
    height: auto;
    margin-bottom: 0;
}
</style>

<?php 
     $idpengaturan  = empty($get_pengaturan) ? '' : $get_pengaturan['id_pengaturan']; 
     $unit          = empty($get_pengaturan) ? '' : $get_pengaturan['unit']; 
     $userid_akses  = empty($get_pengaturan) ? '' : $get_pengaturan['userid_akses']; 
     $indikator_id  = empty($get_pengaturan) ? '' : explode(',',$get_pengaturan['indikator_id']); 

     $count_cek_table  = empty($indikator_id) ? 0 : count($indikator_id); 
?>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form class="form" id="frm-pengaturanimutakses">
                        <?php if( $mode == 'edit' ): ?>
                            <input type="hidden" class="form-control" name="idpengaturan_hidden" value="<?= $idpengaturan; ?>">
                        <?php endif; ?>
                        <div class="err-msg"></div>
                        <div class="form-group">
                            <label>Pilih User <span class="required">*</span> </label>
                            <select name="alluser" class="form-control ql-select2 userpengturanmutu" required>
                                <option value="">-- Pilih --</option>
                                <?php foreach( $allusers as $user ): 
                                    $selected = ($user['user_id_login'] == $userid_akses) ? 'selected' : '';    
                                ?>
                                    <option <?= $selected; ?> value="<?= $user['user_id_login']; ?>"><?= get_username_login($user['user_id_login']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label >Nama Unit <span class="required">*</span></label>
                            <div class="unit-desc">
                                <?php $nama_unit = ( empty($unit) ) ? 'Silahkan Pilih User Unit Terlebih Dahulu' : $unit; ?>                                
                                <div class="alert alert-abu"><?= $nama_unit; ?></div>
                            </div>
                            <input value="<?= $unit; ?>" required type="hidden" autocomplete="off" class="form-control" name="unit" placeholder="Masukan Nama Unit ...">
                        </div>
                        <div class="form-group">
                            <label>Tambahkan Indikator Akses <span class="required">*</span></label>
                            <div class="ql-group">
                                <div class="ql-inline">
                                    <select class="form-control ql-select2 allindikator-akses">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach( $allindikator as $row ): ?>
                                            <option value="<?= $row->id_indikator; ?>"><?= namefile_maxlength_string( $row->judul_indikator ); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="ql-inline">
                                    <button class="btn btn-primary btn-sm tambah-item-indikator" type="button"><i class="fa fa-plus-square"></i> Tambah Indikator</button>
                                </div>
                            </div>
                            <div class="ql-tabledata">
                                <table class="table table-striped table-bordered">
                                    <thead style="background: #ecf0f5;">
                                        <tr>
                                            <th width="80%">Nama Indikator</th>
                                            <th>[X]</th>
                                        </tr>
                                    </thead>
                                    <tbody class="ajax-tableselect-indikator">
                                        <?php if( !empty($indikator_id) ): ?>
                                            <?php 
                                            foreach( $allindikator as $row_indikator ): 
                                                if( !in_array($row_indikator->id_indikator,$indikator_id) ) continue;
                                            ?>
                                            <tr>
                                                <td> <input type="hidden" name="idindikator[]" value="<?= $row_indikator->id_indikator; ?>"/><?= namefile_maxlength_string($row_indikator->judul_indikator); ?></td>
                                                <td><a data-toggle="tooltip" data-placement="bottom" title="Hapus Indikator" class="remove-indikator-item btn btn-xs btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                            </tr>

                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-group" style="text-align:right;">
                            <a href="<?= base_url('cindikatormutu/pengaturan'); ?>" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
                            <button <?= ($count_cek_table > 0) ? '' : 'disabled'; ?> type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>