<div class="popup-detail">
    <table class="table ql-dt table-striped table-hover">
        <thead>
            <tr class="header-table-ql">
                <th>Unit</th>
                <th>Username</th>
                <th>Analisa</th>
                <th>Rencana Tindak Lanjut</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($list_detailanalisarencana as $row_detail): ?>
            <tr>
                <td><?= $this->mmutu->get_nama_unit_pengaturanmutu($row_detail->author); ?></td>
                <td><?= get_username_login($row_detail->author); ?></td>
                <td><?= $row_detail->analisa; ?></td>
                <td><?= $row_detail->rencana_tindaklanjut; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>