
<div class="panel" style="background: #f7f7f7;">
    <div class="panel-header" style="padding: 5px 15px 5px 15px;background: #edd38c;"><h4>Rekap PPI RS Periode :: <?= $tahun; ?></h4></div>
    <div class="panel-body">
        
        <div class="msg-mutu"></div>
        <table class="table ql-dt table-hover table-striped">
            <thead>
                <tr class="header-table-ql">
                <?php if( !$is_superuser ): ?>
                    <th width="5%">No</th>
                    <th width="30%">PPI</th>
                    <?php foreach( $jumlah_bulan as $row_bulan ): ?>
                        <th><?= ql_namabulan((int) $row_bulan); ?></th>
                    <?php endforeach; ?>
                <?php else: ?>
                    <th width="5%">No</th>
                    <th width="30%">PPI</th>
                    <?php foreach( $jumlah_bulan as $row_bulan ): ?>
                        <th><?= ql_namabulan((int) $row_bulan); ?></th>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                if( !empty($bulan) ){
                    $bulan_new = ( strlen($bulan) < 2 ) ? '0'.$bulan : $bulan;
                }

                foreach( $rekapppirs as $key=> $row_indikator ): 
                    if( !$is_superuser ):
                        $get_judul=$this->db->select("daftarppi_id")
                                            ->from('mutu_pengaturanppi')
                                            ->where("userid_akses",$userid)
                                            ->get()->row();
                    if(!in_array($row_indikator->id_daftarppi,explode(',',$get_judul->daftarppi_id))){
                        continue;
                    }
                    endif;
                        if( !empty($bulan) ){
                            $get_hasil = $this->mmutu->get_hasil_ppirekap($row_indikator->id_daftarppi,$bulan_new,$tahun);
                        }else{
                            $get_hasil = $this->mmutu->get_hasil_ppirekap($row_indikator->id_daftarppi,"",$tahun);
                        }

                        $hasil_nume = [];
                        $hasil_denume = [];
                        foreach($get_hasil as $row_hasil){
                            $hasil_nume []= ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : '';
                            $hasil_denume []= ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                        }
                        
                        $numerator      = remove_empty_value_array($hasil_nume);
                        $denumerator    = remove_empty_value_array($hasil_denume);
    
                        $c_num      = count($numerator);
                        $c_denum    = count($denumerator);
                        
                        if( $c_num != $c_denum ){
                            $c_gabung = $c_num - $c_denum;
                            for ($i=1; $i <= $c_gabung ; $i++) { 
                                $h = $c_denum + $i;
                                $g = $h - 1;
                                $denumerator[$g] = '';
                            }
                        }
    
                        $rumus=[];
                        foreach($numerator as $key => $nemu){
                            $denum = $denumerator[$key];
                            
                            if( $denum == 0 && $nemu == 0  ){
                                $rumus [] = '';
                            }elseif($nemu != 0 && $denum == 0){
                                $rumus [] = '';
                            }else{
                                $rumus[]  = round( $nemu/$denum,2 );
                            }
                        }
    
                        $hasil_rekap = !empty($rumus) ? (round(array_sum($rumus)/count($rumus),2)*100).'%' : '';
                ?>
                <tr>
                <?php if( !$is_superuser ): ?>
                    <td><?= $no++; ?></td>
                    <td><?= $row_indikator->judul_ppi; ?></td>
                    <?php 
                    foreach( $jumlah_bulan as $row_bulan ):            
                    ?>
                        <td>
                            <?php 
                                if( !empty($bulan) ){
                                    $get_hasil = $this->mmutu->get_hasil_ppirekap2($row_indikator->id_daftarppi,$bulan_new,$tahun,$userid);
                                }else{
                                    $get_hasil = $this->mmutu->get_hasil_ppirekap2($row_indikator->id_daftarppi,"",$tahun,$userid);
                                }
                                if( !empty($get_hasil) || empty($bulan) ){
                                    $hasil_nume = [];
                                    $hasil_denume = [];
                                    foreach($get_hasil as $row_hasil){
                                        if( $row_hasil->bulan == $row_bulan ){
                                            $hasil_nume []= ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : '';
                                            $hasil_denume []= ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                                        }
                                    }

                                    $numerator      = remove_empty_value_array($hasil_nume);
                                    $denumerator    = remove_empty_value_array($hasil_denume);
                                    
                                    $c_num      = count($numerator);
                                    $c_denum    = count($denumerator);
                                    
                                    if( $c_num != $c_denum ){
                                        $c_gabung = $c_num - $c_denum;
                                        for ($i=1; $i <= $c_gabung ; $i++) { 
                                            $h = $c_denum + $i;
                                            $g = $h - 1;
                                            $denumerator[$g] = '';
                                        }
                                    }
                                    
                                    $rumus=[];
                                    foreach($numerator as $key => $nemu){
                                        $denum = $denumerator[$key];
                                        
                                        if( $denum == 0 && $nemu == 0  ){
                                            $rumus [] = '';
                                        }elseif($nemu != 0 && $denum == 0){
                                            $rumus [] = '';
                                        }else{
                                            $rumus[]  = round( $nemu/$denum,2 );
                                        }
                                    }
                
                                    $hasil_rekap = !empty($rumus) ? (round(array_sum($rumus)/count($rumus),2)*100).'%' : '';
                
                                    echo $hasil_rekap;

                                }else{
                                    if( $row_bulan == (int) $bulan_new ){    
                                        echo $hasil_rekap;
                                    }
                                }
                            ?>
                        </td>
                    <?php endforeach; ?>
                <?php else: ?>
                    <td><?= $no++; ?></td>
                    <td><?= $row_indikator->judul_ppi; ?></td>
                    <?php 
                    foreach( $jumlah_bulan as $row_bulan ):            
                    ?>
                        <td>
                            <?php 
                                if( !empty($get_hasil) || empty($bulan) ){
                                    $hasil_nume = [];
                                    $hasil_denume = [];
                                    foreach($get_hasil as $row_hasil){
                                        if( $row_hasil->bulan == $row_bulan ){
                                            $hasil_nume []= ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : '';
                                            $hasil_denume []= ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 
                                        }
                                    }

                                    $numerator      = remove_empty_value_array($hasil_nume);
                                    $denumerator    = remove_empty_value_array($hasil_denume);
                                    
                                    $c_num      = count($numerator);
                                    $c_denum    = count($denumerator);
                                    
                                    if( $c_num != $c_denum ){
                                        $c_gabung = $c_num - $c_denum;
                                        for ($i=1; $i <= $c_gabung ; $i++) { 
                                            $h = $c_denum + $i;
                                            $g = $h - 1;
                                            $denumerator[$g] = '';
                                        }
                                    }
                                    
                                    $rumus=[];
                                    foreach($numerator as $key => $nemu){
                                        $denum = $denumerator[$key];
                                        
                                        if( $denum == 0 && $nemu == 0  ){
                                            $rumus [] = '';
                                        }elseif($nemu != 0 && $denum == 0){
                                            $rumus [] = '';
                                        }else{
                                            $rumus[]  = round( $nemu/$denum,2 );
                                        }
                                    }
                
                                    $hasil_rekap = !empty($rumus) ? (round(array_sum($rumus)/count($rumus),2)*100).'%' : '';
                
                                    echo $hasil_rekap;

                                }else{
                                    if( $row_bulan == (int) $bulan_new ){    
                                        echo $hasil_rekap;
                                    }
                                }
                            ?>
                        </td>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>