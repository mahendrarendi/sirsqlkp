
<div class="panel" style="background: #f7f7f7;">
    <div class="panel-header" style="padding: 5px 15px 5px 15px;background: #edd38c;"><h4>Rekap IKP RS Periode :: <?= $tahun; ?></h4></div>
    <div class="panel-body">
        <div class="msg-mutu"></div>
        <table class="table ql-dt table-hover table-striped">
            <thead>
                <tr class="header-table-ql">
                <?php if( !$is_superuser ): ?>
                    <th width="5%">No</th>
                    <th width="30%">IKP</th>
                    <?php foreach( $jumlah_bulan as $row_bulan ): ?>
                        <th><?= ql_namabulan((int) $row_bulan); ?></th>
                    <?php endforeach; ?>
                <?php else: ?>
                    <th width="5%">No</th>
                    <th width="30%">IKP</th>
                    <?php foreach( $jumlah_bulan as $row_bulan ): ?>
                        <th><?= ql_namabulan((int) $row_bulan); ?></th>
                    <?php endforeach; ?>
                <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                if( !empty($bulan) ){
                    $bulan_new = ( strlen($bulan) < 2 ) ? '0'.$bulan : $bulan;
                }
                foreach( $rekapikprs as $key=> $row_indikator ): 
                    if( !$is_superuser ):
                        $get_judul=$this->db->select("daftar_ikp_id")
                                            ->from('mutu_pengaturanikp')
                                            ->where("userid_akses",$userid)
                                            ->get()->row();
                    if(!in_array($row_indikator->id_daftarikp,explode(',',$get_judul->daftar_ikp_id))){
                        continue;
                    }
                    endif; 
                ?>
                <?php if( !$is_superuser ): ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $row_indikator->judulikp; ?></td>
                    <?php 
                    foreach( $jumlah_bulan as $row_bulan ):   
                    $get_row_rekap=$this->db->select('SUM(nilai_survey) as nilai')
                                            ->from('mutu_rekapikp')
                                            ->where("bulan",$row_bulan)
                                            ->where("tahun",$tahun)
                                            ->where("daftarikp_id",$row_indikator->id_daftarikp)
                                            ->where("author",$userid)
                                            ->get()->row();
                    $hasil_nilai = $get_row_rekap->nilai;                       
                    ?>
                        <td style="text-align:center;" data-bln="<?= $row_bulan; ?>" data-th="<?= $tahun; ?>"><?php echo $hasil_nilai; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php else: ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $row_indikator->judulikp; ?></td>
                    <?php 
                    foreach( $jumlah_bulan as $row_bulan ):   
                    $get_row_rekap=$this->db->select('SUM(nilai_survey) as nilai')
                                            ->from('mutu_rekapikp')
                                            ->where("bulan",$row_bulan)
                                            ->where("tahun",$tahun)
                                            ->where("daftarikp_id",$row_indikator->id_daftarikp)
                                            ->get()->row();
                    $hasil_nilai = $get_row_rekap->nilai;                       
                    ?>
                        <td style="text-align:center;" data-bln="<?= $row_bulan; ?>" data-th="<?= $tahun; ?>"><?php echo $hasil_nilai; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>