<style>
    .layout-header .logo-l{
        display: inline-block;
        width:20%;
        text-align:center;
    }
    .layout-header .format-r{
        display: inline-block;
        width:80%;
        text-align:center;
    }
    .layout-header .format-r h4{
        margin:0;
        padding:0;
    }
    .layout-header .format-r h3{
        margin:0 0 10px 0;
        padding:0;
    }
    .note-l{
        text-align:center;
        background:red;
        padding: 5px;
    }
    .note-l p {
        font-size:12px;
        margin:0;
        color:white;
    }
    ul.checkbox li{
        list-style:none;
        margin:0;
        padding:0;
        font-size:13px;
    }
    ul.checkbox{
        padding:0 0 0 25px;
    }
    ul.checkbox label{
        margin:0;
        padding:0;
        vertical-align:text-top;
    }
    
</style>

<?php 
    $id = base64_decode($idpelaporanikp);
    $row_investigasi = $this->db->select('*')
                              ->from('mutu_investigasi_sederhana')
                              ->where('idpelaporanikp',$id)
                              ->get()->row();
?>

<div class="layout-print-laporan">
    <div class="row">
        <div class="col-md-12">
            <div class="layout-header">
                <div class="logo-l">
                    <img width="50%" src="<?= FCPATH.'/assets/images/logors.svg'; ?>" alt="">
                </div>
                <div class="format-r">
                    <h3>LEMBAR KERJA INVESTIGASI SEDERHANA</h3>
                    <h4>RS UMUM QUEEN LATIFA</h4>
                </div>
            </div>
            <div class="layout-body">
                <table style="width:100%" class="table" border="1" cellspacing="0">
                    <tr>
                        <td colspan="3" height="70" align="left" valign="top">
                            <strong><u>PENYEBAB LANGSUNG INSIDEN:</u></strong><br>
                            <?php echo nl2br($row_investigasi->penyebab_langsung); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" height="70" align="left" valign="top">
                            <strong><u>PENYEBAB YANG MELATARBELAKANGI / AKAR MASALAH INSIDEN:</u></strong><br>
                            <?php echo nl2br($row_investigasi->penyebab_latarbelakang); ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong><u>REKOMENDASI:</u></strong>
                        </td>
                        <td style="text-align:center;">
                            <label>PENANGGUNG JAWAB:</label>
                        </td>
                        <td style="text-align:center;">
                            <label>TANDA TANGAN:</label>
                        </td>
                    </tr>
                    <tr>
                        <td height="70" align="left" valign="top">
                            <?php echo nl2br($row_investigasi->rekomendasi); ?>
                        </td>
                        <td height="70" style="text-align:center;">
                            <?php echo $row_investigasi->pj_rekomendasi; ?>
                        </td>
                        <td height="70"></td>
                    </tr>
                    <tr>
                        <td>
                            <strong><u>TINDAKAN YANG AKAN DILAKUKAN:</u></strong>
                        </td>
                        <td style="text-align:center;">
                            <label>PENANGGUNG JAWAB:</label>
                        </td>
                        <td style="text-align:center;">
                            <label>TANDA TANGAN:</label>
                        </td>
                    </tr>
                    <tr>
                        <td height="70" align="left" valign="top">
                            <?php echo nl2br($row_investigasi->tindakan_dilakukan); ?>
                        </td>
                        <td height="70" style="text-align:center;">
                            <?php echo $row_investigasi->pj_tindakan; ?>
                        </td>
                        <td height="70"></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <strong><u>KEPALA RUANG/KEPALA INSTALASI:</u></strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>NAMA : </label><?php echo $row_investigasi->nama_kepalaruang; ?><br>
                            <label>TANDA TANGAN :</label>
                        </td>
                        <td colspan="2">
                            <label>TANGGAL MULAI INVESTIGASI : </label><?php echo $row_investigasi->tglmulai_investigasi; ?><br>
                            <label>TANGGAL SELESAI INVESTIGASI : </label><?php echo $row_investigasi->tglselesai_investigasi; ?>
                        </td>
                    </tr>
                </table>
                <table style="width:100%" class="table">
                    <tr>
                        <td colspan="2">
                            <strong>DI ISI OLEH SUB KOMITE KESELAMATAN PASIEN RS (KPRS)</strong>
                        </td>
                    </tr>
                </table>
                <table style="width:100%" class="table" border="1"  cellspacing="0">
                    <tr>
                        <td rowspan="2" colspan="1" style="text-align:center;">
                            <strong>MANAJEMEN RESIKO</strong>
                        </td>
                        <td rowspan="1" colspan="5">
                            <label>INVESTIGASI LENGKAP:</label>
                            <input type="radio" class="investigasi_lengkap" name="investigasi_lengkap" value="1" <?= ($row_investigasi->kronologi_kejadian == 1 ? "checked" : "") ?> > Ya &nbsp; </input>
                            <input type="radio" class="investigasi_lengkap" name="investigasi_lengkap" value="0" <?= ($row_investigasi->kronologi_kejadian == 0 ? "checked" : "") ?> > Tidak &nbsp; &nbsp; </input><br>
                            <label>TANGGAL: <?php echo $row_investigasi->tglinvestigasi_lengkap; ?></label>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="1" colspan="5">
                            <label>DIPERLUKAN INVESTIGASI LANJUT:</label>
                            <input type="radio" class="investigasi_lanjut" name="investigasi_lanjut" value="1" <?= ($row_investigasi->investigasi_lanjut == 1 ? "checked" : "") ?> > Ya &nbsp; </input>
                            <input type="radio" class="investigasi_lanjut" name="investigasi_lanjut" value="0" <?= ($row_investigasi->investigasi_lanjut == 0 ? "checked" : "") ?> > Tidak </input><br>
                            <label>INVESTIGASI SETELAH GRADING ULANG:</label><br>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="0" <?= ($row_investigasi->investigasi_grading == 0 ? "checked" : "") ?> > Biru &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="1" <?= ($row_investigasi->investigasi_grading == 1 ? "checked" : "") ?> > Hijau &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="2" <?= ($row_investigasi->investigasi_grading == 2 ? "checked" : "") ?> > Kuning &nbsp; </input>
                            <input type="radio" class="investigasi_grading" name="investigasi_grading" value="3" <?= ($row_investigasi->investigasi_grading == 3 ? "checked" : "") ?> > Merah </input>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align:center;">
                            <strong>KRONOLOGI KEJADIAN HASIL INVESTIGASI SEDERHANA</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" height="150" align="left" valign="top">
                            <?php echo nl2br($row_investigasi->kronologi_kejadian); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>