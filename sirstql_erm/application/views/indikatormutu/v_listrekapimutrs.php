<style>
.ql-inline {
    display: inline-block;
    width: calc(55% - 40px);
}
.ql-group {
    margin-bottom: 15px;
}
.ql-inline .form-group {
    padding: 0 5px;
}
.title-content-table {
    text-align: center;
}

.title-content-table h3 {
    margin-top: 0;
    margin-bottom: 15px;
}
.wrap-button button {
    margin: 0 2px;
}
.animasi-loading{
  -webkit-animation: fa-spin 2s infinite linear;
  animation: fa-spin 2s infinite linear;
}
</style>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form" id="frm-filterrekapmutu">
                                <div class="filter-imut">
                                    <label>Filter : </label>
                                    <div class="ql-group">
                                        <div class="ql-inline">
                                            <div class="form-group">
                                                <input name="filterrekapmutu" value="<?= date('m-Y'); ?>" type="text" autocomplete="off" class="form-control ql-datemonth">
                                            </div>
                                        </div>
                                        <div class="ql-inline">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
                                                <a href="<?= base_url('Cindikatormutu/listrekapimutrs'); ?>" class="btn btn-sm btn-danger"><i class="fa fa-refresh"></i> Refresh</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="table-content">
                        <?= $list_rekap; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>