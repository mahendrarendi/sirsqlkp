<style>
    .layout-header .logo-l{
        display: inline-block;
        width:20%;
        text-align:center;
    }
    .layout-header .format-r{
        display: inline-block;
        width:80%;
        text-align:center;
    }
    .layout-header .format-r h4{
        margin:0;
        padding:0;
    }
    .layout-header .format-r h3{
        margin:0 0 10px 0;
        padding:0;
    }
    .note-l{
        text-align:center;
        background:red;
        padding: 5px;
    }
    .note-l p {
        font-size:12px;
        margin:0;
        color:white;
    }
    ul.checkbox li{
        list-style:none;
        margin:0;
        padding:0;
        font-size:13px;
    }
    ul.checkbox{
        padding:0 0 0 25px;
    }
    ul.checkbox label{
        margin:0;
        padding:0;
        vertical-align:text-top;
    }
    
</style>

<?php 
    $id = base64_decode($idpdsa);
    $row = $this->db->select('*')
                    ->from('mutu_lembarpdsa')
                    ->where('idpdsa',$id)
                    ->get()->row();
?>

<div class="layout-print-laporan">
    <div class="row">
        <div class="col-md-12">
            <div class="layout-header">
                <div class="logo-l">
                    <img width="50%" src="<?= FCPATH.'/assets/images/logors.svg'; ?>" alt="">
                </div>
                <div class="format-r">
                    <h3>LEMBAR KERJA PDSA (PLAN, DO, STUDY, ACTION)</h3>
                    <h4>RSU QUEEN LATIFA YOGYAKARTA</h4>
                </div>
            </div>
            <div class="layout-body">
                <table class="table">
                    <tr>
                    <?php
                    $rows=$this->db->select("*")
                        ->from('mutu_indikator')
                        ->where("id_indikator",$row->judul_indikator)
                        ->get()->row();
                    ?> 
                        <td>JUDUL INDIKATOR</td>
                        <td>:</td>
                        <td><?php echo $rows->judul_indikator; ?></td>
                    </tr>
                    <tr>
                        <td>CARA PERBAIKAN</td>
                        <td>:</td>
                        <td><?php echo $row->cara_perbaikan; ?></td>
                    </tr>
                    <tr>
                        <td>SIKLUS</td>
                        <td>:</td>
                        <td><?php echo $row->siklus; ?></td>
                    </tr>
                <table>
                <table style="width:100%" class="table" border="1" cellspacing="0">
                    <tr>
                        <td style="background-color:#F4A460;" colspan="4">PLAN</td>
                    </tr>
                    <tr>
                        <td colspan="4" height="70" align="left" valign="top">
                            <label>Saya berencana : </label><br><?php echo nl2br($row->plan_rencana); ?><br>
                            <label>Saya berharap : </label><br><?php echo nl2br($row->plan_harap); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%">No</td>
                        <td width="55%">Tindakan/Langkah-langkah yang dilakukan:</td>
                        <td width="25%">PIC</td>
                        <td width="15%">Tanggal</td>
                    </tr>
                    <?php
                    $sql=$this->db->select("*")
                        ->from('mutu_tindakan')
                        ->where("idpdsa",$id)
                        ->get()->result();
                    $no=1;
                    foreach( $sql as $row2 ):
                    ?>   
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $row2->tindakan; ?></td>
                        <td><?= $row2->pic; ?></td>
                        <td><?= $row2->tanggal; ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tabel>
                <table style="width:100%" class="table" border="1" cellspacing="0">
                    <tr>
                        <td style="background-color:#F4A460;">DO</td>
                    </tr>
                    <tr>
                        <td height="70" align="left" valign="top">
                            <label>Yang saya amati :</label><br><?php echo nl2br($row->do); ?><br>
                        </td>
                    </tr>     
                    <tr>
                        <td style="background-color:#F4A460;" >STUDY</td>
                    </tr>
                    <tr>
                        <td height="70" align="left" valign="top">
                            <label>Yang saya pelajari :</label><br><?php echo nl2br($row->study); ?><br>
                        </td>
                    </tr>     
                    <tr>
                        <td style="background-color:#F4A460;">ACT</td>
                    </tr>
                    <tr>
                        <td height="70" align="left" valign="top">
                            <label>Yang disimpulkan dari siklus ini :</label><br><?php echo nl2br($row->act); ?><br>
                        </td>
                    </tr>
                </tabel>
                <table style="width:40%" class="table" border="1" cellspacing="0" align="right">
                    <tr>
                        <td style="background-color:#F4A460;" align="center">Nama Penanggung Jawab Perbaikan</td>
                    </tr>
                    <tr>
                        <td height="50"></td>
                    </tr> 
                    <tr>
                        <td align="center"><?php echo $row->penanggungjawab; ?></td>
                    </tr>      
                </tabel>
            </div>
        </div>
    </div>
</div>