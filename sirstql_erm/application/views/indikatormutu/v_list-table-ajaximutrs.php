<style>
    .add-tdclass{
        text-align: center;
        vertical-align: middle !important;
    }
</style>
<div class="title-content-table">
    <h3>Indikator Mutu (Lokal/Internal RS) - <?= ql_namabulan((int) $bulan).' '.$tahun; ?></h3>
</div>
<div class="msg-mutu"></div>
<table class="table ql-dt table-hover table-striped">
    <thead>
        <tr class="header-table-ql">
            <th width="5%">No</th>
            <th width="30%">Indikator</th>
            <th><?= ql_namabulan((int) $bulan); ?></th>
            <?php if( !$is_superuser ): ?>
                <th>Analisa</th>
                <th>Rencana Tindak Lanjut</th>
            <?php else: ?>
                <th>Detail Analisa dan Rencana Tindak Lanjut</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
    <?php 
    
    if( $userid == 'without_superuser' ) $daftarindikator = [];
    
    $no = 1;
    foreach( $daftarindikator as $row_indikator ): 
        // $get_pendataan = $this->mmutu->get_editpendataan($row_indikator->id_indikator);
        $get_pendataan = $this->mmutu->get_editpendataan_row($row_indikator->id_indikator);

        $idpendataan = $get_pendataan->id_pendataan;

        // $get_row_hasil  = $this->mmutu->get_hasil_mutu_row($idpendataan,$row_indikator->id_indikator,$userid);
        $get_row_hasil = $this->mmutu->get_analisarencana_row($row_indikator->id_indikator,$userid,$bulan,$tahun);
        $analisa        = empty( $get_row_hasil ) ? '' : $get_row_hasil->analisa ;
        $rencana_tindaklanjut = empty( $get_row_hasil ) ? '' : $get_row_hasil->rencana_tindaklanjut ;
        
        $get_hasil = $this->mmutu->get_hasil_mutu_listrs($row_indikator->id_indikator,$bulan,$tahun,$userid);
       
        $hasil_nume     = [];
        $hasil_denume   = [];
        
        $indikatorid_hasil = [];
        foreach($get_hasil as $row_hasil){

            $hasil_nume[]    = ( $row_hasil->variabel == 'Numerator' ) ? (float)$row_hasil->hasil : ''; 
            $hasil_denume[]  = ( $row_hasil->variabel == 'Denumerator' ) ? (float)$row_hasil->hasil : ''; 


            $indikatorid_hasil[] = $row_hasil->indikator_id; 
        }        

        // if( !in_array( $row_indikator->id_indikator,$indikatorid_hasil ) ) continue;
        if( !empty($indikator_id_pengaturan) ){
            if( !in_array( $row_indikator->id_indikator,$indikator_id_pengaturan ) ) continue;
        }

        $sum_allnume       = array_sum($hasil_nume);
        $sum_alldenume     = array_sum($hasil_denume);

        // if( $sum_denume == 0 && $sum_nume == 0  ){
        //     $rumus = 0;
        // }elseif($sum_nume == 0 && $sum_denume != 0){
        //     $rumus = '0%';
        // }else{
        //     $rumus = round( $sum_denume/$sum_nume,2 );
        // }

        // $hasil = ( $rumus == 0 ) ? $rumus : $rumus.'%'; 

            $sum_nume      = remove_empty_value_array($hasil_nume);
            $sum_denume    = remove_empty_value_array($hasil_denume);
        
        if( $sum_allnume != 0 && $sum_alldenume != 0 ){

            $rumus=[];
            foreach($sum_nume as $key => $nemu){
                // $denum = $sum_denume[$key];
                $denum = empty($sum_denume[$key])? 0 : $sum_denume[$key];
                if( $denum == 0 && $nemu == 0  ){
                    $rumus [] = '';
                }elseif($nemu != 0 && $denum == 0){
                    $rumus [] = '';
                }else{
                    $rumus[]  = round( $nemu/$denum,2 );
                }
            }
            $hasil = (round(array_sum($rumus)/count($rumus),2)*100).'%';

        }else{
            $hasil = 0;
        }

        $add_class = ($is_superuser) ? 'add-tdclass' : 'no-class';
        $add_style = ($is_superuser) ? 'color:black;' : '';
    ?>
        <tr>
            <td><?= $no++; ?></td>
            <td><?= $row_indikator->judul_indikator; ?></td>
            <td style="text-align:center;">
                <?php if( $sum_allnume == 0 && $sum_alldenume == 0 ): ?>
                    <a href="javascript:void(0)" style="<?= $add_style; ?>" class="<?= ( $is_superuser ) ? '' : 'input-hasil-pendataan'; ?>" data-id="<?= $row_indikator->id_indikator; ?>" data-bln="<?= $bulan; ?>" data-th="<?= $tahun; ?>"><?= $hasil; ?></a>
                <?php else: ?>
                    <table class="table table-bordered">
                        <tr>
                            <td colspan="2">
                                <a href="javascript:void(0)" style="<?= $add_style; ?>" class="<?= ( $is_superuser ) ? '' : 'input-hasil-pendataan'; ?>" data-id="<?= $row_indikator->id_indikator; ?>" data-bln="<?= $bulan; ?>" data-th="<?= $tahun; ?>"><?= $hasil; ?></a>
                            </td>
                        </tr>
                        <tr>
                            <td><?= $sum_allnume; ?></td>
                            <td><?= $sum_alldenume; ?></td>
                        </tr>
                    </table>
                <?php endif; ?>
                
            </td>
            <?php if($is_superuser): ?>
                <td class="<?= $add_class; ?>"><button class="btn btn-sm btn-success view-detail" data-id="<?= $row_indikator->id_indikator; ?>" type="button"><i class="fa fa-eye"></i> Detail</button></td>
            <?php else: ?>
                <td><textarea data-idpendataan="<?= $idpendataan; ?>" data-id="<?= $row_indikator->id_indikator; ?>" name="analisa"  class="form-control analisa" placeholder="Isi Analisa ..." cols="50" rows="5" value="<?= $analisa; ?>"><?= $analisa; ?></textarea></td>
                <td><textarea data-idpendataan="<?= $idpendataan; ?>" data-id="<?= $row_indikator->id_indikator; ?>" name="rencana-tindaklanjut" class="form-control rencana" placeholder="Isi Rencana Tindak Lanjut ..." cols="50" rows="5" value="<?= $rencana_tindaklanjut; ?>"><?= $rencana_tindaklanjut; ?></textarea></td> 
            <?php endif; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>