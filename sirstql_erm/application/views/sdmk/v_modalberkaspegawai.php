<table class="table table-bordered" id="tbl-berkas">
   <tbody>
      <?php foreach($datas as $data) { ?>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Riwayat 
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-daftarberkas" type="button" class="btntambahriwayat" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['riwayat'])) { ?>
                        <?php if(is_array(unserialize($data['riwayat']))) { ?>
                           <?php foreach(unserialize($data['riwayat']) as $riwayat) { ?>
                           <ul>
                              <li>
                                 <a href="http://172.16.200.3:8008/berkas_pegawai/riwayat/<?= $riwayat ?>" style="color: black;"><?= $riwayat ?></a>&nbsp;
                                 <a type="button" class="btn btn-circle btn-warning btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="riwayat" namaberkas="<?= $riwayat ?>" idberkas="<?= $data['id_berkas'] ?>">
                                 <i class="fa fa-trash-o"></i></a>
                              </li>
                           </ul>
                           <?php } ?>  
                        <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Ijazah Terlegalisir <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahijazahterlegalisir" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['ijazahterlegalisir'])) { ?>
                     <?php if(is_array(unserialize($data['ijazahterlegalisir']))) { ?>
                        <?php foreach(unserialize($data['ijazahterlegalisir']) as $ijazahterlegalisir) { ?>
                        <ul>
                           <a href="http://172.16.200.3:8008/berkas_pegawai/ijazahterlegalisir/<?= $ijazahterlegalisir ?>" style="color: black;"><?= $ijazahterlegalisir ?></a>
                           <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="ijazahterlegalisir" namaberkas="<?= $ijazahterlegalisir ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  STR <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahstr" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['str'])) { ?>
                     <?php if(is_array(unserialize($data['str']))) { ?>
                        <?php foreach(unserialize($data['str']) as $str) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/str/<?= $str ?>" style="color: black;"><?= $str ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="str" namaberkas="<?= $str ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  SIK/SIP <a href="" id="btntambahsiksip" data-toggle="modal" data-target="#modal-daftarberkas" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['siksip'])) { ?>
                     <?php if(is_array(unserialize($data['siksip']))) { ?>
                        <?php foreach(unserialize($data['siksip']) as $siksip) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/siksip/<?= $siksip ?>" style="color: black;"><?= $siksip ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="siksip" namaberkas="<?= $siksip ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Bukti Verifikasi Ijazah <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahbuktiverifijazah" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['buktiverifijazah'])) { ?>
                     <?php if(is_array(unserialize($data['buktiverifijazah']))) { ?>
                        <?php foreach(unserialize($data['buktiverifijazah']) as $buktiverifijazah) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/buktiverifikasiijazah/<?= $buktiverifijazah ?>" style="color: black;"><?= $buktiverifijazah ?></a>
                              <a class="btn btn-circle btn-warning btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="buktiverifikasiijazah" namaberkas="<?= $buktiverifijazah ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  SIK Surat Keputusan <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahsiksuratkeputusan" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['suratkeputusansuratperjanjian'])) { ?>
                     <?php if(is_array(unserialize($data['suratkeputusansuratperjanjian']))) { ?>
                        <?php foreach(unserialize($data['suratkeputusansuratperjanjian']) as $suratkeputusansuratperjanjian) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/sksuratkeputusan/<?= $suratkeputusansuratperjanjian ?>" style="color: black;"><?= $suratkeputusansuratperjanjian ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="suratkeputusansuratperjanjian" namaberkas="<?= $suratkeputusansuratperjanjian ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Uraian Tugas Staff Masing-Masing <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahuraiantugasstaff" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['uraiantugas'])) { ?>
                      <?php if(is_array(unserialize($data['uraiantugas']))) { ?>
                        <?php foreach(unserialize($data['uraiantugas']) as $uraiantugas) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/uraiantugas/<?= $uraiantugas?>" style="color: black;"><?= $uraiantugas ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="uraiantugas" namaberkas="<?= $uraiantugas ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  RKK & SPKK <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahrkkspkk" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['rkkspkk'])) { ?>
                     <?php if(is_array(unserialize($data['rkkspkk']))) { ?>
                        <?php foreach(unserialize($data['rkkspkk']) as $rkkspkk) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/rkkspkk/<?= $rkkspkk ?>" style="color: black;"><?= $rkkspkk ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="rkkspkk" namaberkas="<?= $rkkspkk ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
            </ul>
            </li>   
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Bukti Penilaian Kinerja <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahbuktipenilaiankinerja" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['buktipenilaiankinerja'])) { ?>
                     <?php if(is_array(unserialize($data['buktipenilaiankinerja']))) { ?>
                        <?php foreach(unserialize($data['buktipenilaiankinerja']) as $buktipenilaiankinerja) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/buktipenilaiankinerja/<?= $buktipenilaiankinerja ?>" style="color: black;"><?= $buktipenilaiankinerja ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="buktipenilaiankinerja" namaberkas="<?= $buktipenilaiankinerja ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
               </li>
            </ul>
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Data Pelatihan <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahdatapelatihan" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['datapelatihan'])) { ?>
                     <?php if(is_array(unserialize($data['datapelatihan']))) { ?>
                        <?php foreach(unserialize($data['datapelatihan']) as $datapelatihan) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/datapelatihan/<?= $datapelatihan ?>" style="color: black;"><?= $datapelatihan ?></a>
                              <a type="button" class="btn btn-warning btn-circle btn-hapus-peritem" data-toggle="tooltip" title="Hapus Berkas" modehapus="datapelatihan" namaberkas="<?= $datapelatihan ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
            </ul>
            </li>   
         </td>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                  Bukti Lamaran Kerja <a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahbuktilamarankerja" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['buktilamarankerja'])) { ?>
                     <?php if(is_array(unserialize($data['buktilamarankerja']))) { ?>
                        <?php foreach(unserialize($data['buktilamarankerja']) as $buktilamarankerja) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/buktilamarankerja/<?= $buktilamarankerja ?>" style="color: black;"><?= $buktilamarankerja ?></a>
                              <a class="btn btn-circle btn-warning btn-hapus-peritem" type="button" data-toggle="tooltip" title="Hapus Berkas" modehapus="buktilamarankerja" namaberkas="<?= $buktilamarankerja ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
            </ul>
            </li>   
         </td>
      </tr>
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                 Bukti Orientasi Kerja<a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahbuktiorientasikerja" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['buktiorientasi'])) { ?>
                     <?php if(is_array(unserialize($data['buktiorientasi']))) { ?>
                        <?php foreach(unserialize($data['buktiorientasi']) as $buktiorientasi) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/buktiorientasi/<?= $buktiorientasi ?>" style="color: black;"><?= $buktiorientasi ?></a>
                              <a class="btn btn-circle btn-warning btn-hapus-peritem" type="button" data-toggle="tooltip" title="Hapus Berkas" modehapus="buktiorientasi" namaberkas="<?= $buktiorientasi ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
            </ul>
            </li>   
         </td>
      </tr>           
      <tr style="border: 1px solid black;">
         <td>
            <ul>
               <li>
                 Sertifikat Vaksinasi<a href="" data-toggle="modal" data-target="#modal-daftarberkas" id="btntambahsertifikatvaksin" alt="<?= $data['id_berkas'] ?>">(Tambah Lagi)</a><br>
                  <?php if(!empty($data['sertifikatvaksin'])) { ?>
                     <?php if(is_array(unserialize($data['sertifikatvaksin']))) { ?>
                        <?php foreach(unserialize($data['sertifikatvaksin']) as $sertifikatvaksin) { ?>
                        <ul>
                           <li>
                              <a href="http://172.16.200.3:8008/berkas_pegawai/sertifikatvaksin/<?= $sertifikatvaksin ?>" style="color: black;"><?= $sertifikatvaksin ?></a>
                              <a class="btn btn-circle btn-warning btn-hapus-peritem" type="button" data-toggle="tooltip" title="Hapus Berkas" modehapus="sertifikatvaksin" namaberkas="<?= $sertifikatvaksin ?>" idberkas="<?= $data['id_berkas'] ?>"><i class="fa fa-trash-o"></i></a>
                           </li>
                        </ul>
                        <?php } ?>
                     <?php } ?>
                  <?php } ?>
            </ul>
            </li>   
         </td>
      </tr>     
      <?php } ?>
   </tbody>
</table>
