<style>
   .btn-circle {
   width: 25px;
   height: 25px;
   padding: 6px 0px;
   border-radius: 15px;
   font-size: 11px;
   text-align: center;
   }
</style>
<table id="dtberkaspegawai" class="table dt-responsive" cellspacing="0" width="100%">
   <thead>
      <tr class="header-table-ql">
         <th style="width:10px;">No</th>
         <th style="width:50px">Nama Pegawai</th>
         <th style="width:12px;">Profesi</th>
         <th style="width:45px">Berkas</th>
         <th style="width:50px;">Aksi</th>
      </tr>
   </thead>
   <tbody>
      <?php $i = 1; ?>
      <?php foreach($datas as $data) { ?>
      <tr>
         <td><?= $i++ ?></td>
         <td><?= $data['namalengkap']?></td>
         <td><?= $data['profesi'] ?></td>
         <td><a href="" data-toggle="modal" data-target="#modal-berkas" idberkas="<?= $data['id_berkas']?>" class="btntampilberkas">(Tampil Berkas)</a></td>
         <td><button data-toggle="tooltip" idberkas="<?= $data['id_berkas']?>" title="Hapus Berkas Keseluruhan" type="button" class="btn btn-danger btnhapusberkas"><i class="fa fa-trash"></i></b></td>
      </tr>
      <?php } ?>
   </tbody>
   <tbody>
   </tbody>
</table>
