
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body" >
            <div class="col-md-6 row">
                <a href="<?= base_url();?>csdmk/add_datapegawai" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Pegawai</a>
                <a onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
            </div>
            <table id="dtpegawai" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
                <tr class="header-table-ql">
                  <th>NO</th>
                  <th>NIK</th>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Jenis Tenaga</th>
                  <th>Status Pegawai</th>
                  <th>Jenis Kelamin</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <style>
            .nav-tabs-custom>.nav-tabs>li.active>a {
                background-color: #f0f0f0;
                color: #444;
            }
            .nav-tabs-custom>.nav-tabs>li.hover>a {
                background-color: #ecf0f5;
                color: #444;
            }
            .nav-tabs-custom>.nav-tabs>li>a {
                background-color: #d2d6de;
                color: #444;
            }
        .nav-tabs-custom>.nav-tabs {
            margin-top: 5px;
            border-bottom-color: #d2d6de;
            border-top-right-radius: 3px;
            border-top-left-radius: 3px;
        }
        .nav-tabs-custom>.nav-tabs>li {
            border-top: 3px solid transparent;
             margin-bottom: 0.1px; 
             margin-right: 0px; 
        }
        </style>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                  <!-- Custom Tabs -->
                  <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab_biodata" data-toggle="tab">Biodata</a></li>
                      <li><a href="#tab_pekerjaan" data-toggle="tab">Pekerjaan</a></li>
                      <li><a href="#tab_pendidikan" data-toggle="tab">Pendidikan</a></li>
                      <li><a href="#tab_pelatihan" data-toggle="tab">Pelatihan</a></li>
                      <li><a href="#tab_sertifikasi" data-toggle="tab">Sertifikasi</a></li>
                      <li><a href="#tab_datakesehatan" data-toggle="tab">Data Kesehatan</a></li>
                      <li><a href="#tab_infokontak" data-toggle="tab">Info Kontak</a></li>
                    </ul>
                    <div class="tab-content">
                        
                        <!--Biodata-->
                        <div class="tab-pane active" id="tab_biodata">
                            <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal" id="FormPegawai" autocomplete="off">
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">NIK *</span>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Nama Lengkap *</span>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Tempat Lahir *</span>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Tanggal Lahir *</span>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Jenis Kelamin *</span>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">
                                        <h3>Alamat Rumah</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Provinsi *</span>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Kabupaten *</span>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Alamat Sesuai E-KTP *</span>
                                    <div class="col-sm-4">
                                        <textarea name="alamat" class="form-control">
                                            
                                        </textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span for="" class="col-sm-3 control-label"></span>
                                    <div class="col-sm-2">
                                        <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()">Update Data</a>
                                    </div>
                                    
                                </div>
                                
                            </form>
                        </div>
                        <!-- /Biodata-->
                      
                        <!--Pekerjaan-->
                        <div class="tab-pane" id="tab_pekerjaan">
                              <h3>Daftar Pekerjaan Saat Ini</h3>
                              <table id="dtpegawai" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                  <tr class="header-table-ql">
                                    <th>NO</th>
                                    <th>SDMK</th>
                                    <th>NIP</th>
                                    <th>Status Kepegawaian</th>
                                    <th>Status</th>
                                    <th>Mulai</th>
                                    <th>Akhir</th>
                                    <th>Edit</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tfoot>
                              </table>

                              <div class="form-group row">
                                  <div class="col-sm-2" id="pekerjaan_mnTambahTutup">
                                      <a id="pekerjaan_tambah" class="btn btn-warning btn-md btn-block"><i class="fa fa-plus-square"></i> Tambah Pekerjaan</a>
                                  </div>
                              </div>

                              <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal hidden" id="FormPekerjaan" autocomplete="off">
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Kategori SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Kelompok SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Sub Kelompok SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Grup Status Kepegawaian SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>                                

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Status Kepegawaian SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jenis SDMK</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jenjang JabFung</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">ID Pegawai / NIP *</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Mulai</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"></span>
                                      <div class="col-sm-2">
                                          <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()"> <i class="fa fa-save"></i> Simpan Data</a>
                                      </div>

                                  </div>


                              </form>
                        </div>
                        <!-- /Pekerjaan-->
                        
                        
                        <!--Pendidikan-->
                        <div class="tab-pane" id="tab_pendidikan">
                              <h3>Daftar Riwayat Pendidikan</h3>
                              <table id="dtpendidikan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                  <tr class="header-table-ql">
                                    <th>NO</th>
                                    <th>Sekolah/PT</th>
                                    <th>Program Studi</th>
                                    <th>Sumber Dana</th>
                                    <th>Status Pendidikan</th>
                                    <th>Tahun Lulus</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tfoot>
                              </table>

                              <div class="form-group row">
                                  <div class="col-sm-2" id="pendidikan_mnTambahTutup">
                                      <a id="pendidikan_tambah" class="btn btn-warning btn-md btn-block"><i class="fa fa-plus-square"></i> Tambah Pendidikan</a>
                                  </div>
                              </div>

                              <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal hidden" id="FormPendidikan" autocomplete="off">
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Nama Sekolah / Perguruan Tinggi</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Akreditasi</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jenjang</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Program Studi</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>                                

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Sumber Dana</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Status Pendidikan</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tahun Lulus</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"></span>
                                      <div class="col-sm-2">
                                          <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()"> <i class="fa fa-save"></i> Simpan Data</a>
                                      </div>
                                  </div>
                              </form>
                        </div>
                        <!-- /Pendidikan-->
                        
                        <!--Pelatihan-->
                        <div class="tab-pane" id="tab_pelatihan">
                              <h3>Daftar Pelatihan Diklat</h3>
                              <table id="dtpelatihan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                  <tr class="header-table-ql">
                                    <th>NO</th>
                                    <th>Kelompok Diklat</th>
                                    <th>Nama Diklat</th>
                                    <th>Akreditasi</th>
                                    <th>Tempat Pelaksanaan</th>
                                    <th>Tanggal Awal Pelaksanaan</th>
                                    <th>Tanggal Akhir Pelaksanaan</th>
                                    <th>Lama Pelatihan (hari)</th>
                                    <th>Jumlah Jam Pelajaran (jam)</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tfoot>
                              </table>

                              <div class="form-group row">
                                  <div class="col-sm-2" id="pelatihan_mnTambahTutup">
                                      <a id="pelatihan_tambah" class="btn btn-warning btn-md btn-block"><i class="fa fa-plus-square"></i> Tambah Pelatihan</a>
                                  </div>
                              </div>

                              <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal hidden" id="FormPelatihan" autocomplete="off">
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Kelompok Diklat</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jenis Diklat</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Nama Diklat</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">disek sepi bange
                                      <span for="" class="col-sm-2 control-label">Akreditasi</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>                                

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tempat</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tanggal Awal Pelaksanaan</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tanggal Akhir Pelaksanaan</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Lama Pelatihan (hari)</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jumlah Jam Pelajaran (jam)</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"></span>
                                      <div class="col-sm-2">
                                          <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()"> <i class="fa fa-save"></i> Simpan Data</a>
                                      </div>
                                  </div>
                              </form>
                        </div>
                        <!-- /Pelatihan-->
                        
                        <!--Sertifikasi-->
                        <div class="tab-pane" id="tab_sertifikasi">
                            <h3>Daftar Sertifikasi</h3>
                              <table id="dtpelatihan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                  <tr class="header-table-ql">
                                    <th>No</th>
                                    <th>No.STR</th>
                                    <th>Tahun Terbit STR</th>
                                    <th>Jenis Profesi</th>
                                    <th>No SIP</th>
                                    <th>Nama Unit</th>
                                    <th>Tanggal Terbit SIP</th>
                                    <th>Edit</th>
                                    <th>Hapus</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tfoot>
                              </table>

                              <div class="form-group row">
                                  <div class="col-sm-2" id="sertifikasi_mnTambahTutup">
                                      <a id="sertifikasi_tambah" class="btn btn-warning btn-md btn-block"><i class="fa fa-plus-square"></i> Tambah Sertifikasi</a>
                                  </div>
                              </div>

                              <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal hidden" id="FormSertifikasi" autocomplete="off">
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Jenis Profesi</span>
                                      <div class="col-sm-4">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">STR</span>
                                      <div class="col-sm-4">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tanggal STR</span>
                                      <div class="col-sm-2">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                      
                                      <span for="" class="col-sm-2 control-label">Tanggal Berakhir STR</span>
                                      <div class="col-sm-2">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">No SIP</span>
                                      <div class="col-sm-4">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>                                

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Tanggal Izin Praktek</span>
                                      <div class="col-sm-2">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"></span>
                                      <div class="col-sm-2">
                                          <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()"> <i class="fa fa-save"></i> Simpan Data</a>
                                      </div>
                                  </div>
                              </form>
                        </div>
                        <!-- /Sertifikasi-->
                        
                        
                        <!--Data Kesehatan-->
                        <div class="tab-pane" id="tab_datakesehatan">                          
                                <div class="row">
                                    <div class="col-md-3">
                                        <h3>Riwayat Kesehatan</h3> 
                                        <table id="dtpelatihan" class="table table-bordered table-striped table-hover" cellspacing="0" width="100%">
                                            <thead>
                                              <tr class="header-table-ql">
                                                <th>Pertanyaan</th>
                                                <th>Jawaban</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                                <tr><td>Pernah Positif Covid-19</td>	<td>Tidak</td></tr>
                                                <tr><td>Hipertensi</td>	<td>Tidak</td></tr>
                                                <tr><td>Diabetes Melitus</td>	<td>Tidak</td></tr>
                                                <tr><td>Penyakit Imunologi</td>	<td>Tidak</td></tr>
                                                <tr><td>Penyakit Jantung</td>	<td>Tidak</td></tr>
                                                <tr><td>PPOK</td>	<td>Tidak</td></tr>
                                                <tr><td>Kanker</td>	<td>Tidak</td></tr>
                                                <tr><td>Penyakit Kronis Lainnya</td>	<td>Tidak</td></tr>
                                                <tr><td>Hamil</td>	<td>Tidak</td></tr>
                                                <tr><td>Menyusui</td>	<td>Tidak</td></tr>
                                                
                                            </tfoot>
                                        </table>
                                        
                                        <label class="label label-info">Terakhir diperbarui pada : ....</label>
                                    </div>
                                    <div class="col-md-1"> &nbsp;</div>
                                    <div class="col-md-5">
                                        
                                        <h3>Update Data Kesehatan</h3>  
                                        <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal" style="background-color: #f0f0f08c !important; padding: 15px;" id="FormSertifikasi" autocomplete="off">
                                            
                                            <?php 
                                                foreach ($datakesehatan as $arr)
                                                {
                                                    echo '<div class="col-md-12 row">
                                                            <label for="" class="col-sm-6">'.$arr['kategori'].'</label>
                                                            <div class="col-sm-4">
                                                                <select class="form-control" name="nik">
                                                                    <option>Tidak</option>
                                                                    <option>Ya</option>
                                                                </select>
                                                            </div>
                                                        </div>';
                                                }
                                            ?>

                                            <div class="form-group">
                                                <div class="col-sm-6">
                                                    <a class="btn btn-primary btn-md" onclick="save_pegawai()"> <i class="fa fa-save"></i> Update Data</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                        <!-- /Data Kesehatan-->
                        
                        <!--Informasi Kontak-->
                        <div class="tab-pane" id="tab_infokontak">
                            
                                <form action="<?= base_url('csdmk/save_datapegawai');?>" method="POST" class="form-horizontal" id="FormSertifikasi" autocomplete="off">
                                    
                                    <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"><h4>Informasi Kontak</h4></span>
                                    </div>
                                    
                                    <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Alamat E-mail</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Nomor Telepon</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"><h4>Informasi Kontak Darurat</h4></span>
                                    </div>
                                    
                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Nama Kontak</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>
                                    <div class="form-group">
                                    <span for="" class="col-sm-2 control-label">Hubungan</span>
                                      <div class="col-sm-3">
                                          <select class="form-control" name="nik">
                                              <option>Pilih</option>
                                          </select>
                                      </div>
                                    </div>

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label">Nomor Telepon</span>
                                      <div class="col-sm-3">
                                          <input type="text" class="form-control" name="nik" value="<?= (!empty($data_edit)) ? $data_edit['nik'] : '' ; ?>">
                                      </div>
                                  </div>             

                                  <div class="form-group">
                                      <span for="" class="col-sm-2 control-label"></span>
                                      <div class="col-sm-2">
                                          <a class="btn btn-primary btn-md btn-block" onclick="save_pegawai()"> <i class="fa fa-save"></i> Update Data</a>
                                      </div>
                                  </div>
                              </form>
                        </div>
                        <!-- /Informasi Kontak-->
                      
                    </div>
                    <!-- /.tab-content -->
                  </div>
                  <!-- nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
        </div>
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->