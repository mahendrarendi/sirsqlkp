<!-- Style -->
<style>
   /* kategori */
   .list-kategori .list-group a {
   display: inline-block;
   border-radius: 5px;
   line-height: normal;
   }

   /* .modal:nth-of-type(even) {
    z-index: 1052 !important;
   }

   .modal-backdrop.show:nth-of-type(even) {
      z-index: 1051 !important;
   }
     */

</style>
<!-- End Style --> 

<section class="content">
   <div class="ql-akreditasi">
   <div class="row">
         <?php if($mode == "viewberkas"){ ?>
         <div class="col-md-12">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <a href="<?= base_url()?>csdmk/tambahberkas" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                  <a href="#" class="btn btn-danger refresh-akre" id="btnrefresh"><i class="fa fa-refresh"></i> Refresh</a>
               </div>
               <div class="panel-body">
                  <div class="list-kategori">
                     <div class="list-group">
                     </div>
                  </div>
                  <div class="err-msg"></div>
                  <div class="data-berkas-pegawai">
                  </div>
               </div>
            </div>
         </div>
         <?php } ?>
         <?php if($mode == "tambahberkas"){ ?>
         <div class="col-md-7">
            <div class="box">
               <!-- <div class="box-header">Tambah Berkas Pegawai</div> -->
               <!-- box body -->
               <div class="box-body">
                  <div class="err-msg"></div>
                  <form action="#" method="post" enctype="multipart/form-data">
                     <div>
                        <label for="slctpilihpegawai">Pilih Pegawai *</label>
                        <select name="idpegawai" id="slctpilihpegawai" class="ql-select2 form-control">
                           <option value="0">--  PILIH PEGAWAI --</option>
                           <?php foreach($datapegawai as $row) {?><option value="<?= $row['idpegawai']?>"><?= $row['namalengkap']?></option><?php } ?>
                        </select>
                     </div><br>
                     
                     <div class="">
                        <label for="">Riwayat</label>
                        <input type="file" class="form-control riwayat" name="riwayat[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Ijazah Terlegalisir</label>
                        <input type="file" class="form-control ijazahterlegalisir" name="ijazahterlegalisir[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">STR</label>
                        <input type="file" class="form-control str" name="str[]" accept="application/pdf" multiple>
                     </div><br>


                     <div class="">
                        <label for="">SIK/SIP</label>
                        <input type="file" class="form-control siksip" name="siksip[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Bukti Verifikasi Ijazah</label>
                        <input type="file" class="form-control buktiverifikasiijazah" name="buktiverifikasiijazah[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Surat Keterangan Surat Keputusan/ Surat Perjanjian Kerja Sama Kekaryawanan</label>
                        <input type="file" class="form-control skskspksk" name="skskspksk[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Uraian Tugas Staff Masing-Masing</label>
                        <input type="file" class="form-control uraiantugas" name="uraiantugas[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">RKK & SPKK</label>
                        <input type="file" class="form-control rkkspkk" name="rkkspkk[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Bukti Penilaian Kinerja</label>
                        <input type="file" class="form-control buktipenilaiankinerja" name="buktipenilaiankinerja[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Data Pelatihan</label>
                        <input type="file" class="form-control datapelatihan" name="datapelatihan[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Bukti Lamaran Kerja</label>
                        <input type="file" class="form-control buktilamarankerja" name="buktilamarankerja[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Bukti Orientasi Kerja</label>
                        <input type="file" class="form-control buktiorientasi" name="buktiorientasi[]" accept="application/pdf" multiple>
                     </div><br>

                     <div class="">
                        <label for="">Sertifikat Vaksinasi</label>
                        <input type="file" class="form-control sertifikatvaksinasi" name="sertifikatvaksinasi[]" accept="application/pdf" multiple>
                     </div><br>
                  </div>
                  <center>
                     <button type="button" class="btn btn-primary btn-md" id="btnsimpan"><i class="fa fa-floppy-o"></i> SIMPAN</button>
                     <a href="<?= base_url('csdmk/berkas')?>" type="button" class="btn btn-danger btn-md"> <i class="fa fa-chevron-left"></i> KEMBALI</a>
                  </center>
               </form>
            </div>
         </div>
         <?php } ?>
      </div>
   </div>

   <!-- Modal 1 -->
   <div id="modal-berkas" class="modal fade modal-berkas">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body" id="modalbodyberkaspegawai"></div>
            <div class="modal-footer"></div>
         </div>
      </div>
   </div>

   <!-- Modal 2 -->
   <div id="modal-daftarberkas" class="modal fade">
      <div class="modal-dialog">
         <!-- Modal content-->
         <div class="modal-content">
            <div class="modal-header modal-header-daftar-berkas">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body modal-body-daftar-berkas" id="modalbodyberkaspegawai"></div>
            <div class="modal-footer modal-footer-daftar-berkas"></div>
         </div>
      </div>
   </div>
</section>
</section>