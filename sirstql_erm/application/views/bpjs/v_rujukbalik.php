<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .padding2{padding:2px;}
</style>
 <!-- Main content -->
  <section class="content">
      <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="box box-default">
                <div class="box-body" style="padding-bottom:6px;">
                  <div class="col-md-10">
                    <input type="text" class="form-control" placeholder="ketik nomor SRB"  id="nomorsrb" autocomplete="off">                    
                    <input type="text" class="form-control" placeholder="ketik nomor SEP"  id="nomorsep" autocomplete="off">
                  </div>
                  <div class="col-md-2" style="margin-left:-14px;">
                    <a id="caripeserta" jenis="nomorsrb" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                  </div>
                    <small class="col-xs-11 padding2" style="color:#686868;">#Pencarian data PRB Berdasarkan Nomor SRB</small>
                </div>
           </div>
        </div>
          
        <div class="col-xs-12 col-md-5">
            <div class="box box-default">
                <div class="box-body" style="padding-bottom:6px;">
                  <div class="form-group">
                    <label class="col-md-2 col-sm-3 col-xs-12 control-label">Tanggal</label>
                    <div class="col-md-8">
                        <div class="input-group date">
                            <input type="text" class="form-control datepicker" id="tglMulai" placeholder="yyyy-mm-dd" maxlength="10" readonly>
                            <span class="input-group-addon">
                                s.d
                            </span>
                            <input type="text" class="form-control datepicker" id="tglAkhir" placeholder="yyyy-mm-dd" maxlength="10" readonly>
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-left:-14px;">
                        <a id="caripeserta" jenis="tanggalsrb" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Cari</a>
                    </div>
                </div>
                    <small class="col-xs-11 padding2" style="color:#686868;">#Pencarian data PRB Berdasarkan tanggal SRB</small>
                </div>
           </div>
        </div>
      </div>
      
      
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div class="box-body" >
            <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                    <tr class="header-table-ql">
                    <th>No</th>
                    <th>No.SRB</th>
                    <th>No.Kartu</th>
                    <th>Nama Peserta</th>
                    <th>Program PRB</th>
                    <th>No.SEP</th>
                    <th>Status</th>
                  </tr>
                  </thead>
                  <tbody id="bodypeserta">
                  </tfoot>
                </table>
          </div>
        </div>
        <!-- /.box -->
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->