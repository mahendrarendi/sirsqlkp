<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_antrol" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'referensi_poli') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_referensi?tabActive=referensi_poli') ?>">Referensi Poli</a></li>              
              <li class="<?= (($tabActive == 'referensi_dokter') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_referensi?tabActive=referensi_dokter') ?>">Referensi Dokter</a></li>
            </ul>
            <div class="tab-content">
                <!-- $tabActive == 'referensi_poli >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'referensi_poli') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'referensi_poli'){ ?>
                        
                        <div class="col-md-12"> </div>
                        <table id="tblReferensi" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>Nama Poli</th>
                                <th>Nama Sub Spesialis</th>
                                <th>Kode Sub Spesialis</th>
                                <th>Kode Poli</th>
                            </tr>
                            </thead>
                            <tbody id="bodyReferensi">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'referensi_poli -->  

                <!-- $tabActive == 'referensi_dokter >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'referensi_dokter') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'referensi_dokter'){ ?>                        
                        <div class="col-md-12"> </div>
                        <table id="tblReferensi" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>Nama Dokter</th>
                                <th>Kode Dokter</th>
                            </tr>
                            </thead>
                            <tbody id="bodyReferensi">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'referensi_dokter -->  
               
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->