<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_bridging" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'form_prb') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rujukbalik?tabActive=form_prb') ?>">Form PRB</a></li>
              <li class="<?= (($tabActive == 'list_prb') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rujukbalik?tabActive=list_prb') ?>">List PRB</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= (($tabActive == 'form_prb') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'form_prb'){ $this->load->view('bpjs/v_prb_form');} ?>
                </div>
                <!-- /.tab-pane -->
              
                <div class="tab-pane <?= (($tabActive == 'list_prb') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'list_prb'){ $this->load->view('bpjs/v_prb_list'); } ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->