<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
</style>
 <!-- Main content -->
  <section class="content">
      
    <div class="row" style="margin-bottom:4px;">
        <div class="col-md-5" id="col_referensi">
            <select name="idreferensi" class="form-control" id="idreferensi">
                <option value="0">Pilih Referensi</option>
                <?php
                    foreach ($ref as $arr)
                    {
                        echo "<option value='".$arr['kode']."'>".$arr['referensi']."</option>";
                    }
                ?>
            </select>
        </div>
    </div>
      
    <div class="row hide" id="row_table">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body" id="boxbody_table">
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->