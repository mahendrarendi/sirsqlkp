<div class="form-horizontal">
  <div class="form-group">
      <label class="col-md-2 control-label">Aksi</label>
      <div class="col-md-2">
          <select id="jenis_rencana_kontrol" class="form-control">
              <option value="1">Buat Rencana</option>
              <option value="2">Ubah Rencana</option>
          </select>
      </div>
  </div>

  <div class="form-group cari_sep">
      <label class="col-md-2 control-label">No.SEP</label>
      <div class="col-md-4">
          <div class="input-group">
              <input type="text" class="form-control" id="txtCariNosep" placeholder="ketik nomor sep">
              <span class="input-group-btn">
                  <button type="button" id="btnrencana_kontrol" class="btn btn-info" style="height:32px;">
                      <i class="fa fa-search"></i> Cari
                  </button>
              </span>
          </div>
      </div>
  </div>

  <div class="form-group cari_rencanakontrol hide">
      <label class="col-md-2 control-label">No.Rencana Kontrol/Inap</label>
      <div class="col-md-4">
          <div class="input-group">
              <input type="text" class="form-control" id="txtCariNoRencanaKontrol" placeholder="ketik nomor rencana kontrol/inap">
              <span class="input-group-btn">
                  <button type="button" id="btnrencana_kontrol" class="btn btn-info" style="height:32px;">
                      <i class="fa fa-search"></i> Cari
                  </button>
              </span>
          </div>
      </div>
  </div>
</div>
<form class="form-horizontal" id="formRencanakontrol">
  <input type="hidden" name="modesave" id="modesave" value="insert"/>
  <div class="col-md-12">&nbsp;</div>
      <section class="col-sm-2">
          <div class="box box-solid box-default">
              <div class="box-header">
                <h3 class="box-title">History</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="input-group">
                    <input type="text" class="form-control" id="cariHistoriRekon" name="cariHistoriRekon" placeholder="ketik no.JKN">
                    <span class="input-group-btn">
                        <button type="button" id="btnCariHistoriRekon" class="btn btn-info" style="height:32px;">
                            <i class="fa fa-search"></i> Cari
                        </button>
                    </span>
                </div>
                <div class="input-group">
                    <b><small>History Pasien Dalam 90 Hari</small></b>
                </div>
                <div class="input-group">
                    <div>&nbsp</div>
                </div>
                <div class="input-group" id="dataTampilHistoryRekon">
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>
                </div>
              </div>
          </div>
      </section> 
      <section class="col-sm-4">
          <!--sep-->
          <div class="box box-solid box-default">
              <div class="box-header">
                <h3 class="box-title">SEP</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">No.SEP</span>
                    <div class="col-md-8"><input type="text" id="nosep" name="nosep" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Tgl.SEP</span>
                    <div class="col-md-8"><input type="text" id="tanggal_sep" name="tanggal_sep" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Jns.Layanan</span>
                    <div class="col-md-8"><input type="text" id="jenis_layanan" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Poli</span>
                    <div class="col-md-8"><input type="text" id="txtPoli" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Diagnosa
                        </br><small><b>(Bridging BPJS)</b></small>
                    </span>
                    <div class="col-md-8"><input type="text" id="txtDiagnosa" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Diagnosa DPJP
                        </br><small><b>(catatan manual)</b></small></span>
                    <div class="col-md-8"><input type="text" id="txtDiagnosaDPJPManual" class="form-control"/></div>
                  </div>

              </div>
          </div>

          <!--asal rujukan-->
          <div class="box box-solid box-default">
              <div class="box-header">
                <h3 class="box-title">Asal Rujukan SEP</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">No.Rujukan</span>
                    <div class="col-md-8"><input type="text" id="txtNoRujukan" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Tanggal Rujukan</span>
                    <div class="col-md-8"><input type="text" id="txtTanggalrujukan" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Faskes Asal Rujukan</span>
                    <div class="col-md-8"><input type="text" id="txtFaskesAssal" class="form-control" readonly/></div>
                  </div>
              </div>
          </div>


          <!--Peserta-->
          <div class="box box-solid box-default">
              <div class="box-header">
                <h3 class="box-title">Peserta</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">No.Kartu</span>
                    <div class="col-md-8"><input type="text" id="txtNoKartu" name="nokartu" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Nama Peserta</span>
                    <div class="col-md-8"><input type="text" id="txtNamaPeserta" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Tgl. Lahir</span>
                    <div class="col-md-8"><input type="text" id="txtTanggalLahir" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Jns. Kelamin</span>
                    <div class="col-md-8"><input type="text" id="txtKelamin" class="form-control" readonly/></div>
                  </div>

                  <div class="form-group">
                    <span for="" class="col-md-4 label-daftar control-label">Kelas Peserta</span>
                    <div class="col-md-8"><input type="text" id="txtKelasPeserta" class="form-control" readonly/></div>
                  </div>
              </div>
          </div>
      </section>

      <section class="col-sm-6">
          <div class="box box-solid box-default">
              <div class="box-header">
                <h3 class="box-title" id="title_rencanakontrol">Data Rencana Kontrol/Inap</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">Tgl. Rencana Kontrol</span>
                      <div class="col-md-3">
                          <div class="input-group">
                              <input type="text" id="tanggal_rencana_kontrol" name="tanggal_rencana_kontrol" class="form-control" readonly>
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                          </div>
                          <sub>format tanggal : yyyy-mm-dd</sub>
                      </div>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">Pelayanan</span>
                      <div class="col-md-3">
                          <select class="form-control" id="pelayanan" name="pelayanan">
                              <option value="2">Rawat Jalan</option>
                              <option value="1">Rawat Inap</option>
                          </select>
                      </div>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">No.Surat Kontrol</span>
                      <div class="col-md-6">
                          <input type="text" class="form-control" name="no_surat_kontrol" id="no_surat_kontrol" readonly/>
                      </div>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">Tanggal Terbit</span>
                      <div class="col-md-6">
                          <input type="text" class="form-control" name="tglTerbit" id="tglTerbit" readonly/>
                      </div>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">Tanggal Terakhir Cetak</span>
                      <div class="col-md-6">
                          <input type="text" class="form-control" name="tanggal_cetak" id="tanggal_cetak" readonly/>
                      </div>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label">Spesialis/ SubSpesialis</span>
                      <div class="col-md-7">
                          <select name="kode_poli" id="kode_poli" class="form-control"></select>                          
                      </div>
                  </div>
      
                  <div class="form-group">
                    <span for="" class="col-md-3 label-daftar control-label">DPJP Tujuan</span>
                    <div class="col-md-6">
                        <select class="form-control" id="dpjp" name="dpjp">
                            <option value="">Pilih</option>
                        </select>
                    </div>
                    <div class="col-md-2"><a id="caridokter" class="btn btn-sm btn-info"><i class="fa fa-search"></i> Cari Dokter</a></div>
                  </div>
                  
                  <!-- create hidden item here -->
                  <div class="form-group">
                    <input type="hidden" class="form-control" name="kodeDokterPembuat" id="kodeDokterPembuat" readonly/>
                    <input type="hidden" class="form-control" name="namaDokterPembuat" id="namaDokterPembuat" readonly/>
                    <input type="hidden" class="form-control" name="SIPDokter" id="SIPDokter" readonly/>
                    <input type="hidden" class="form-control" name="tglCetak_QR" id="tglCetak_QR" readonly/>
                  </div>

                  <div class="form-group">
                      <span for="" class="col-md-3 label-daftar control-label"></span>
                      <div class="col-md-6">
                          <a class="btn btn-primary btn-md" id="btnSimpan"><i class="fa fa-save"></i> Simpan</a>
                          <a class="btn btn-danger btn-md hide" id="btnPrintRencanaKontrol"><i class="fa fa-print"></i> Print</a>
                          <a class="btn btn-danger btn-md hide" id="btnHapus"><i class="fa fa-trash"></i> Hapus</a>
                      </div>
                  </div>

              </div>
          </div>
      </section>
</form>