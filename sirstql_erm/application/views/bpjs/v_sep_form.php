<div class="row col-md-12" style="margin-top:13px;">
        <div class="col-md-4" >
            <label>Pasien Periksa</label>
            <select id="idpendaftaran" name="idpendaftaran" class="form-control">
                <option>Pilih Pasien Periksa</option>
            </select>
        </div>

        <div class="col-md-4">
            <label>Ket. Pasien Periksa</label>
            <p id="ket_pasien_periksa" style="background-color:#fff;padding:6px;"></p>
        </div>

        </div>
        <div class="col-md-12">&nbsp;</div>
            <section class="col-sm-5">
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title">Data Pasien</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <input type="hidden" id="hiddenjenispelayanan">
                        <input type="hidden" id="hiddenprob">
                        <input type="hidden" id="hiddenkelasrawat">
                        <input type="hidden" id="hiddenFaskesPerujuk">
                        <input type="hidden" id="hiddenJenisPesertaByRujukan">
                        <input type="hidden" id="hiddenNoTeleponByRujukan">
                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Nama Pasien</span>
                          <div class="col-md-8"><input type="text" id="namapasien" name="namapasien" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">No.Kartu</span>
                          <div class="col-md-8"><input type="text" id="nokartu" name="nokartu" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">NIK</span>
                          <div class="col-md-8"><input type="text" id="nik" name="nik" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Jenis Kelamin</span>
                          <div class="col-md-4">
                              <select class="form-control" name="jeniskelamin" id="jeniskelamin">
                                  <option value="">-</option>
                                  <option value="laki-laki">Laki-laki</option>
                                  <option value="wanita">Perempuan</option>
                              </select>
                          </div>

                          <span for="" class="col-md-1 label-daftar control-label" style="margin-left:-7px; margin-right:7px;">Status</span>
                          <div class="col-md-3">
                              <input type="hidden" id="idstatuspeserta" name="idstatuspeserta" class="form-control" readonly/>
                              <input type="text" id="statuspeserta" name="statuspeserta" class="form-control" readonly/>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Tgl. Lahir</span>
                          <div class="col-md-8">
                              <div class="input-group">                                                    
                                    <input type="text" id="tanggallahir" name="tanggallahir" class="form-control" readonly/>
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Hak Kelas</span>
                          <div class="col-md-8">
                              <input type="hidden" id="idhakkelas" name="idhakkelas" class="form-control"/>
                              <input type="text" id="hakkelas" name="hakkelas" class="form-control" readonly/>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Jenis Peserta</span>
                          <div class="col-md-8">
                              <input type="hidden" id="idjenispeserta" name="idjenispeserta" class="form-control"/>
                              <input type="text" id="jenispeserta" name="jenispeserta" class="form-control" readonly/>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Tgl. Cetak Kartu</span>
                          <div class="col-md-8">
                              <div class="input-group">                                                    
                                    <input type="text" id="tanggal_cetakkartu" name="tanggal_cetakkartu" class="form-control" readonly/>
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title">History Pasien Dalam 90 Hari      </h3>
                      <a class="btn btn-primary btn-sm text-right" id="tampilHistorySEP"><i class="fa fa-history"></i> Tampilkan</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                          <div class="col-md-12" id="dataTampilHistorySEP">
                          </div>
                        </div>
                    </div>   
                </div>  
            </section>

            <section class="col-sm-7">
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title">Kelengkapan Data SEP</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group hide_ranap">
                            <span for="" class="col-md-3 label-daftar control-label">Spesialis/SubSpesialis</span>
                            <div class="col-md-6">
                                <select name="kode_poli" id="kode_poli" class="form-control" style="width:100%;"></select>
                            </div>
                            <div class="checkbox col-md-2" style="margin-left:-20px;">
                                <label><input type="checkbox" name="eksekutif" id="eksekutif" value="1" class="flat-red" /> Eksekutif</label>
                            </div>
                        </div>

                        <div class="form-group hide_ranap">
                          <span for="" class="col-md-3 label-daftar control-label">DPJP yang melayani</span>
                          <div class="col-md-6">
                              <select class="form-control" name="dpjp" id="dpjp">
                                  <option value="">-</option>
                              </select>
                          </div>
                          <div class="col-md-2">
                              <a id="cari_dokter" class="btn btn-xs btn-info"><i class="fa fa-search"></i> Cari Dokter</a>
                          </div>
                        </div>

                        <div class="form-group hide_edit">
                          <span for="" class="col-md-3 label-daftar control-label">PPK Asal Rujukan</span>
                          <input type="hidden" id="txtKodeFaskes" name="txtKodeFaskes" class="form-control" readonly/>
                          <div class="col-md-8">
                              <input type="hidden" id="hdnNamaFaskes">
                            <input type="text" id="txtFaskes" name="txtFaskes" class="form-control" readonly /></div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Tgl.Rujukan</span>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" id="txtTanggalRujukan" name="txtTanggalRujukan" class="form-control" readonly>
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <sub>format tanggal : yyyy-mm-dd</sub>
                            </div>

                            <span for="" class="col-md-1 label-daftar control-label">Tgl.SEP</span>
                            <div class="col-md-3">
                                <div class="input-group">
                                      <input type="text" id="txtTanggalSep" name="txtTanggalSep" class="form-control" readonly>
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                  </div>
                                <sub>format tanggal : yyyy-mm-dd</sub>
                            </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">No.Rujukan</span>
                          <div class="col-md-8"> 
                              <input type="text" id="txtNorujukan" name="txtNorujukan" class="form-control" />
                              <sub>Nomor Rujukan Harap Diisikan Dengan Minimal 6 Digit Angka</sub>
                            </div>
                          </div> 

                          <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">No.SEP</span>
                          <div class="col-md-8"><input type="text" id="txtnosep" name="nosep" class="form-control" readonly/></div>
                        </div>
                        
                        <!--pembuatan sep ralan (sep rujukan)-->
                        <?php if($tabActive == 'pembuatan_sep'){ ?>
                        <hr>
                        
                        <!-- <div class="form-group" data-form="1,2"> -->
                        <div class="form-group">
                          <span for="" class="col-md-5 label-daftar control-label">No.Surat Kontrol/SKDP</span>
                          <div class="col-md-5"><input type="text" id="no_surat_kontrol" autocomplete="off" name="no_surat_kontrol" class="form-control" placeholder="ketik no surat kontrol"/></div>
                        </div>
                        
                        <!-- <div class="form-group" data-form="1,2"> -->
                        <div class="form-group">
                          <span for="" class="col-md-5 label-daftar control-label">DPJP pemberi Surat SKDP/SPRI</span>
                          <div class="col-md-5">                              
                              <input type="hidden" id="dpjp_kontrol" name="dpjp_kontrol" class="form-control"/>
                              <!-- <input class="form-control" id="cari_dpjp_kontrol"/>
                              <datalist id="list_dpjp_kontrol"></datalist> -->
                              <input type="hidden" id="kodeDokterPembuat_text" name="kodeDokterPembuat_text" class="form-control"/>
                              <input type="text" id="namaDokterPembuat_text" name="namaDokterPembuat_text" class="form-control"/>
                          </div>
                        </div>
                        <!-- <hr data-form="1,2"> -->
                        <hr>
                        
                        <!--form tujuan kunjungan-->
                        <div class="form-group hide_ranap">
                          <span for="" class="col-md-5 label-daftar control-label"><b>Apa tujuan kunjungan ini?</b></span>
                          <div class="col-md-3">
                              <select class="form-control" id="tujuan_kunjungan" name="tujuan_kunjungan">
                                  <option value="0">-</option>
                                  <option value="1">Prosedur</option>
                                  <option value="2">Konsul Dokter</option>
                              </select>
                          </div>
                        </div>
                        
                        <div class="form-group" data-form="1">
                          <span for="" class="col-md-5 label-daftar control-label">Flag Procedure</span>
                          <div class="col-md-4">
                              <select class="form-control" id="flag_procedure" name="flag_procedure">
                                  <option value="">-</option>
                                  <option value="0">Prosedur Tidak Berkelanjutan</option>
                                  <option value="1">Prosedur dan Terapi Berkelanjutan</option>
                              </select>
                          </div>
                        </div>
                        
                        <div class="form-group" data-form="1,2">
                          <span for="" class="col-md-5 label-daftar control-label">Penunjang</span>
                          <div class="col-md-4">
                              <select class="form-control" id="kdpenunjang" name="kdpenunjang">
                                  <option value="">-</option>
                                  <option value="1">Radioterapi</option>
                                  <option value="2">Kemoterapi</option>                                  
                                  <option value="3">Rehabilitasi Medik</option>                                  
                                  <option value="4">Rehabilitasi Psikososial</option>                                  
                                  <option value="5">Transfusi Darah</option>
                                  <option value="6">Pelayanan Gigi</option>
                                  <option value="7">Laboratorium</option>
                                  <option value="8">USG</option>
                                  <option value="9">Farmasi</option>                                  
                                  <option value="10">Lain-lain</option>
                                  <option value="11">MRI</option>                                  
                                  <option value="12">HEMODIALISA</option>
                              </select>
                          </div>
                        </div>
                        
                        <div class="form-group" data-form="2">
                          <!-- <span for="" class="col-md-5 label-daftar control-label"><b>Mengapa pelayanan ini tidak diselesaikan pada hari yang sama sebelumnya?</b></span> -->
                          <div class="col-md-5 label-daftar control-label"><b>Mengapa pelayanan ini tidak diselesaikan pada hari yang sama sebelumnya?</b></div>
                          <div class="col-md-7">
                              <select class="form-control" id="assesment_pel" name="assesment_pel">
                                  <option value="">-</option>
                                  <option value="1">Poli spesialis tidak tersedia pada hari sebelumnya</option>
                                  <option value="2">Jam Poli telah berakhir pada hari sebelumnya</option>
                                  <option value="3">Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya</option>
                                  <option value="4">Atas Instruksi Rumah Sakit</option>
                                  <option value="5">Tujuan Kontrol</option>
                              </select>
                          </div>
                        </div>
                        
                        <!--end form tujuan kunjungan-->
                        
                        <?php }else{?> 
                        <!--pembuatan sep ranap-->   
                            <hr data-forminap="1">                        
                            <div class="form-group" data-forminap="1">
                              <span for="" class="col-md-3 label-daftar control-label">No.SPRI</span>
                              <div class="col-md-6"><input type="text" id="no_surat_kontrol" autocomplete="off" name="no_surat_kontrol" class="form-control" placeholder="ketik no SPRI"/></div>
                            </div>

                            <div class="form-group" data-forminap="1">
                              <span for="" class="col-md-3 label-daftar control-label">DPJP Pemberi Surat SKDP/SPRI</span>
                              <div class="col-md-6">                              
                                  <input type="hidden" id="dpjp_kontrol" name="dpjp_kontrol" class="form-control"/>
                                  <!-- <input class="form-control" id="cari_dpjp_kontrol"/>
                                  <datalist id="list_dpjp_kontrol"></datalist> -->
                                  <input type="hidden" id="kodeDokterPembuat_text" name="kodeDokterPembuat_text" class="form-control"/>
                                  <input type="text" id="namaDokterPembuat_text" name="namaDokterPembuat_text" class="form-control"/>
                              </div>
                            </div>
                        <?php } ?>
                            
                        <!--kelas ranap-->
                        <div class="form-group" data-forminap="1">
                            <span class="col-md-3 col-sm-3 col-xs-12 control-label">Kelas Rawat</span>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <select class="form-control" id="idkelasrawat" name="idkelasrawat">
                                    <option value="1">Kelas 1</option>
                                    <option value="2">Kelas 2</option>
                                    <option value="3">Kelas 3</option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <input type="hidden" id="naikkelas" name="naikkelas" value="0"/>
                                <label> <span class="text-muted" onclick="setcheck_naikkelas('add')" style="cursor:pointer;"> <i id="icon_naikkelas" class="fa fa-square-o fa-lg"></i> Naik Kelas</span></label>
                            </div>
                        </div>
                        
                        <!--naik kelas-->
                        <div class="text-muted well well-sm no-shadow naik_kelas" data-forminap="1">
                            <div class="form-group">
                                <span class="col-md-3 col-sm-3 col-xs-12 control-label">Kelas Rawat Inap</span>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select class="form-control" id="klsRawatNaik" name="klsRawatNaik">
                                        <option value=""> - </option>
                                        <option value="1">VVIP</option>
                                        <option value="2">VIP</option>
                                        <option value="3">Kelas 1</option>
                                        <option value="4">Kelas 2</option>
                                        <option value="5">Kelas 3</option>
                                        <option value="6">ICCU</option>
                                        <option value="7">ICU</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-3 col-sm-3 col-xs-12 control-label">Pembiayaan</span>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <select class="form-control" id="pembiayaan" name="pembiayaan">
                                        <option value=""> - </option>
                                        <option value="1">Pribadi</option>
                                        <option value="2">Pemberi Kerja</option>
                                        <option value="3">Asuransi Kesehatan Tambahan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <span class="col-md-3 col-sm-3 col-xs-12 control-label">Nama Penanggung Jawab</span>
                                <div class="col-md-8 col-sm-4 col-xs-12">
                                    <textarea class="form-control" id="penanggungJawab" name="penanggungJawab" rows="2" placeholder="Jika Pembiayaan Oleh [Pemberi Kerja] atau [Asuransi Kesehatan Tambahan]" maxlength="255"></textarea>
                                </div>
                            </div>
                        </div>
                        
                        <hr>
                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">No.MR</span>
                            <div class="col-md-3"><input type="text" autocomplete="off" name="txtNoMR" id="txtNoMR" class="form-control"/></div>
                            <div class="checkbox col-md-3">
                                <label><input type="checkbox" id="txtcob" name="txtcob" value="1" class="flat-red" /> Peserta COB</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Diagnosa</span>
                            <div class="col-md-8">
                                <select class="form-control" name="diagnosa" id="diagnosa" style="width:100%;"></select>
                            </div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">No.Telepon</span>
                            <div class="col-md-8"><input type="text" autocomplete="off" id="notelepon" name="notelepon" class="form-control"/></div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Catatan</span>
                            <div class="col-md-8"><textarea class="form-control" id="catatan" name="catatan" placeholder="ketik catatan apabila ada"></textarea></div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox col-md-3 label-daftar control-label">
                                <label>Katarak <input type="checkbox" id="katarak" name="katarak" value="1" class="flat-red" /></label>
                            </div>
                            <div class="col-md-8"><small class="form-control col-md-12 bg bg-gray">Centang, Jika Peserta Mendapatkan Surat Perintah Operasi Katarak.</small></div>
                        </div>
                        
                        <hr>
                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Status Kecelakaan</span>
                            <div class="col-md-8">
                                <select name="lakalantas" id="lakalantas" class="form-control">
                                    <?php
                                      foreach ($lakalantas as $arr)
                                      {
                                          echo "<option value=".$arr['idstatus'].">".$arr['status']."</option>";
                                      }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <!--form kll-->
                        <div class="text-muted well well-sm no-shadow hide" id="div_kll">
                            <div class="form-group">
                                <span for="" class="col-md-3 label-daftar control-label">Tanggal Kejadian</span>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" id="tanggal_kejadian" name="tanggal_kejadian" class="form-control" readonly>
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <span for="" class="col-md-3 label-daftar control-label">No.LP</span>
                                <div class="col-md-8">
                                    <input type="text" name="nolp" class="form-control" id="nolp" placeholder=" no laporan polisi"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <span for="" class="col-md-3 label-daftar control-label">Suplesi</span>
                                <div class="col-md-2">       
                                    <input type="hidden" id="suplesi" name="suplesi" value="0"/>
                                    <label> <span class="text-muted" onclick="setcheck_suplesi('add')" style="cursor:pointer;"> <i id="icon_suplesi" class="fa fa-square-o fa-lg"></i> Suplesi</span></label>
                                </div>
                            </div>

                            <div class="form-group nosep_suplesi hide">
                                <span for="" class="col-md-3 label-daftar control-label">No.SEP Suplesi</span>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="nosepsuplesi" name="nosepsuplesi" placeholder="no sep suplesi" />
                                </div>
                            </div>

                            <div class="form-group">
                                <span for="" class="col-md-3 label-daftar control-label">Lokasi Kejadian</span>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <select class="form-control" id="kdPropinsi" name="kdPropinsi"><option value="">Pilih Propinsi</option></select>
                                        <span class="input-group-btn">
                                            <button type="button" onclick="cari_prop_kll()" class="btn btn-info">
                                                <i class="fa fa-search"></i> cari
                                            </button>
                                        </span>
                                    </div>
                                    
                                    <div class="input-group">
                                        <select class="form-control" id="kdKabupaten" name="kdKabupaten"><option value="">-</option></select>
                                        <span class="input-group-btn">
                                            <button type="button" onclick="cari_kab_kll()" class="btn btn-info">
                                                <i class="fa fa-search"></i> cari
                                            </button>
                                        </span>
                                    </div>
                                    
                                    <div class="input-group">
                                        <select class="form-control" id="kdKecamatan" name="kdKecamatan"><option value="">-</option></select>
                                        <span class="input-group-btn">
                                            <button type="button" onclick="cari_kec_kll()" class="btn btn-info">
                                                <i class="fa fa-search"></i> cari
                                            </button>
                                        </span>
                                    </div>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <span for="" class="col-md-3 label-daftar control-label">Keterangan Kejadian</span>
                                <div class="col-md-8"><textarea class="form-control" id="keterangan_kll" name="keterangan_kll" placeholder="ketik keterangan kejadian"></textarea></div>
                            </div>
                        </div>
                        <!--end form kll-->

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label"></span>
                            <div class="col-md-8">
                                <a class="btn btn-primary btn-md" id="btnSimpanSEP"><i class="fa fa-save"></i> Simpan</a>
                                <a class="btn btn-danger btn-md" nosep="" id="btnPrintSEP"><i class="fa fa-print"></i> Print</a>
                                <a class="btn btn-warning btn-md hide" id="btnUbahSEP"><i class="fa fa-edit"></i> Ubah</a>
                                <a class="btn btn-danger btn-md hide" id="btnHapusSEP"><i class="fa fa-trash"></i> Hapus</a>
                                <!-- <a class="btn btn-primary btn-md hide" id="btnKirimAntrol"><i class="fa fa-street-view"></i> Kirim Antrian Online</a> -->
                            </div>
                        </div>

                    </div>
                </div>
            </section>                           
    </form>
</div>


<!--modal Data Suplesi-->
<div class="modal fade" id="modal-datasuplesi">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">                        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="modal-suplesi_title">Data Penjaminan an.</h4>
        </div>
        <div class="modal-body">
            <div class="form-horizontal">
                <div class="callout callout-info">
                    <p style="margin-top: 14px;">
                        Daftar ini merupakan penjaminan kecelakaan lalu lintas dari PT. Jasa Raharja.
                        <br><b>Silahkan Pilih Data tersebut sesuai dengan berkas kasus sebelumnya(SUPLESI) dengan klik <i class="fa fa-check-circle"></i> warna hijau.</b>
                    </p>
                    <p style="margin-top: 12px;">
                        <b>
                            1. Jika Kasus Kecelakaan baru, Silahkan Klik tombol kasus KLL Baru.<br>
                            2. Jika Kasus Kecelakaan lama, Silahkan Pilih No.SEP Sebagai SEP Kasus Sebelumnya.<br>
                            <!--3. Untuk Melihat Detail Jumlah Dibayar. Silahkan Pilih dengan klik NO.SEP.-->
                        </b>
                    </p>
                </div>
                <div class="row">
                    <div class="col-sm-12" id="divViewDataSuplesi">
                        <table id="tblDataSuplesi" class="table table-bordered table-striped dataTable no-footer" cellpadding="0"  style="width: 100%; font-size: small;">
                            <thead><tr><th>No.SEP</th><th>No.SEP Awal</th><th>Tgl.SEP</th><th>Tgl.Kejadian</th><th>No.Register</th><th>Surat Jaminan</th><th>Aksi</th></tr></thead>
                            <tbody><tr><td colspan="7">Data Tidak Ada</td></tr></tbody>
                        </table>
                    </div>
                </div>
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->