<style>
.btn-warning {
    background-color: #666;
    border-color: #e08e0b;
}
</style>
<!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <!-- search category >>>>> -->
                    <div class="col-md-3"></div>
                    <div class="col-md-2">
                        <select class="form-control" id="kodePoliBPJS" placeholder="Kode Poli BPJP"> 
                            <?php
                                foreach($poli as $arr)
                                {
                                    echo "<option value=".$arr['kodebpjs'].">".$arr['namaunit']."</option>";
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group date">
                            <input type="text" class="form-control datepicker" id="tanggalJadwalDokter" placeholder="yyyy-MM-dd" maxlength="10" readonly>
                            <span class="input-group-addon">
                                <span class="fa fa-calendar">
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a id="cari_jadwal_dokter" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>            
                    </div>
                    <div class="col-md-3"></div>
                    <!-- <<<<< search category -->
                    
                    <div class="col-md-12"> </div>
                    <table id="tblReferensiJadwalDokter" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                        <thead>
                            <tr class="bg bg-gray">
                            <th>No</th>                
                            <th>Poli</th>
                            <th>Sub Spesialis</th>                
                            <th>Dokter</th>                            
                            <th>Jadwal</th>
                            <th>Kapasitas Pasien</th>
                            <th>Libur</th>
                        </tr>
                        </thead>
                        <tbody id="bodyReferensiJadwalDokter"></tbody>
                        <tfoot></tfoot>
                    </table>
                    <p>
                    <b>Catatan:<br/></b>
                    - Data yang berhasil disimpan menunggu aproval dari BPJS atau otomatis approve jadwal dokter oleh sistem, misal pengajuan perubahan jadwal oleh RS diantara jam 00.00 - 20.00 , kemudian alokasi approve manual oleh BPJS/cabang di jam 20.01-00.00. Jika pukul 00.00 belum dilakukan aproval oleh kantor cabang, maka otomatis approve by sistem akan dilaksanakan setelah jam 00.00 dan yang berubahnya esoknya (H+1).
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->


<!--form update tanggal pulang-->
<div class="modal fade" id="modal-jadwal">
    <div class="modal-dialog">
        <div class="modal-content">                        
        <form id="formJadwal" autocomplete="off">
            <input type="hidden" name="kodedokter" id="kodedokter"/>
            <input type="hidden" name="kodepoli" id="kodepoli"/>
            <input type="hidden" name="kodesubpoli" id="kodesubpoli"/>

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">UBAH JADWAL DOKTER</h4>
        </div>
        <div class="modal-body">

            <div class="form-group">
                <label>Poli</label>
                <input type="text" id="txtPoli" class="form-control" disabled/>
            </div>

            <div class="form-group">
                <label>Sub Spesialis</label>
                <input type="text" id="txtSubspesialis" class="form-control" disabled/>
            </div>

            <div class="form-group">
                <label>Dokter</label>
                <input type="text" id="txtDokter" class="form-control" disabled/>
            </div>


            <div class="form-group">
                <label>Hari</label>
                <select class="form-control" name="hari" id="hari">
                    <?php
                        $hari = harinasional();
                        foreach($hari as $key=>$value)
                        {
                            echo "<option value='".$key."'>".$value."</option>";
                        }
                    ?>
                </select>
            </div>
            
            <div class="form-group">
                <label>Jam Mulai </label>
                <input type="text" name="mulai" id="mulai" class="form-control timepicker" placeholder="jam mulai" readonly/>
            </div>

            <div class="form-group">
                <label>Jam Selesai </label>
                <input type="text" name="selesai" id="selesai" class="form-control timepicker" placeholder="jam mulai" readonly/>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Batal</button>
            <button type="button" id="btnSimpanJadwal" class="btn btn-primary">Simpan</button>
        </div>
        </form>
        </div>
        <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->