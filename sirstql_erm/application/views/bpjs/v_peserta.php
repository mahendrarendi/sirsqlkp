<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        
        <div class="box">
          <div class="box-body" >              
            <div class="col-xs-12 col-md-3 ">
                  <label>NIK:</label> 
                  <input type="text" placeholder="ketik nik"  id="nik" autocomplete="off">
                <a id="caripeserta" jenis="nik" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
            </div>
              
            <div class="col-xs-12 col-md-4 ">
                  <label>No.Peserta:</label> 
                  <input type="text" id="nokartu" placeholder="ketik nomor kartu" autocomplete="off">
                <a id="caripeserta" jenis="nokartu" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Cari</a>
            </div>
              
            <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                    <tr class="header-table-ql">
                    <th>NIK</th>
                    <th>No.Kartu</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Jenis Peserta</th>
                    <th>Status</th>
                    <th>Informasi</th>
                  </tr>
                  </thead>
                  <tbody id="bodypeserta">
                  </tfoot>
                </table>
          </div>
        </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->