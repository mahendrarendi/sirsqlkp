
<div class="col-md-2">
    <span>Filter :</span>
    <select class="form-control" id="rl_filter_tanggal">                                        
        <option value="1">Tanggal Entri</option>
        <option value="2">Tanggal Rencana Kontrol</option>
    </select>
</div>
<div class="col-md-2">
    <span>Tgl.Awal :</span>
    <div class="input-group">
        <input type="text" class="form-control" id="rl_tanggalawal" placeholder="Tanggal Awal" autocomplete="off" readonly>
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    </div>
</div>
<div class="col-md-2">
    <span>Tgl.Akhir :</span>
    <div class="input-group">
        <input type="text" class="form-control" id="rl_tanggalakhir" placeholder="Tanggal akhir" autocomplete="off" readonly>
        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    </div>
</div>
<div class="col-md-1">
    <span> &nbsp;</span><br>
    <a id="rl_cari" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
</div>
<div class="col-md-12"> </div>
<table id="rl_table" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
    <thead>
        <tr class="bg bg-gray">
        <th>No</th>
        <th>No.Surat</th>
        <th>Kontrol/Inap</th>
        <th>Tgl.Rencana Kontrol</th>                                        
        <th>Tgl.Terbit</th>
        <th>No.SEP Asal</th>
        <th>Poli Asal</th>
        <th>Poli Tujuan</th>
        <th>Nama DPJP</th>
        <th>Tgl. SEP</th>                                     
        <th>No.Kartu</th>
        <th>Nama Lengkap</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody id="rl_tablebody">
    </tfoot>
</table>