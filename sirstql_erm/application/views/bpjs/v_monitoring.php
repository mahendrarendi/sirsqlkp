<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_bridging" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'data_kunjungan') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/monitoring?tabActive=data_kunjungan') ?>">Data Kunjungan</a></li>              
              <li class="<?= (($tabActive == 'data_klaim') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/monitoring?tabActive=data_klaim') ?>">Data Klaim</a></li>
              <li class="<?= (($tabActive == 'data_histori') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/monitoring?tabActive=data_histori') ?>">Data Histori Pelayanan Peserta</a></li>
              <li class="<?= (($tabActive == 'data_klaim_jasaraharja') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/monitoring?tabActive=data_klaim_jasaraharja') ?>">Data Klaim Jaminan Jasa Raharja</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= (($tabActive == 'data_kunjungan') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'data_kunjungan'){ ?>
                        <!--Data Kunjungan-->
                        <div class="col-md-2">
                            <select class="form-control" id="jenislayanan">                                        
                                <option value="2">Rawat Jalan</option>
                                <option value="1">Rawat Inap</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" id="tanggalsep" placeholder="Tanggal SEP" autocomplete="off" readonly>
                        </div>
                        <div class="col-md-1"><a id="carikunjungan" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a></div>
                        <div class="col-md-12"> </div>
                        <table id="tblKunjungan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No.Kartu</th>
                                <th>No.SEP</th>
                                <th>No.Rujukan</th>
                                <th>Tgl.SEP</th>
                                <th>Tgl.Pulang SEP</th>                                        
                                <th>Diagnosa</th>
                                <th>Jenis Pelayanan</th>
                                <th>Kelas Rawat</th>
                            </tr>
                            </thead>
                            <tbody id="bodykunjungan">
                            </tfoot>
                        </table>
                    <?php } ?>
                </div>
              <!-- /.tab-pane -->       
              
                <div class="tab-pane <?= (($tabActive == 'data_klaim') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'data_klaim'){ ?>
                        <div class="form-horizontal col-md-12">
                            <div class="form-group">
                                <div class="col-xs-12 col-md-2">
                                    <label>Tgl.Pulang</label> 
                                    <input type="text" class="form-control" id="tanggalpulang" autocomplete="off" readonly>
                                </div>

                                <div class="col-md-2">
                                    <label>Jns Layanan</label> 
                                    <select class="form-control" id="jenislayananklaim">
                                        <option value="1">Rawat Inap</option>
                                        <option value="2">Rawat Jalan</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label>Status</label> 
                                    <select class="form-control" id="statusklaim">
                                        <option value="1">Proses Verifikasi</option>
                                        <option value="2">Pending Verifikasi</option>
                                        <option value="3">Klaim</option>
                                    </select>
                                </div>

                                <div class="col-md-1">
                                    <label>&nbsp;</label><br>
                                    <a id="cariklaim" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                                </div>
                            </div>
                        </div>
                        <table id="tblKlaim" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                    <th colspan="2">Inacbg</th>
                                    <th colspan="5">Biaya</th>
                                    <th rowspan="2">Kelas Rawat</th>
                                    <th rowspan="2">No.FPK</th>
                                    <th rowspan="2">No.SEP</th>                                        
                                    <th colspan="3">Peserta</th>
                                    <th rowspan="2">Poli</th>
                                    <th rowspan="2">Status</th>
                                    <th rowspan="2">Tgl.Pulang</th>
                                    <th rowspan="2">Tgl.SEP</th>
                                </tr>
                                <tr class="bg bg-gray">
                                    <th>kode</th>
                                    <th>nama</th>

                                    <th>Pengajuan</th>
                                    <th>Disetujui</th>
                                    <th>Tarif Gruper</th>                                        
                                    <th>Tarif Rs</th>
                                    <th>Topup</th>

                                    <th>Nama</th>
                                    <th>No.Kartu</th>
                                    <th>No.MR</th>
                                </tr>
                            </thead>
                            <tbody id="bodyklaim">
                            </tfoot>
                        </table>
                    <?php } ?>
                </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane <?= (($tabActive == 'data_histori') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'data_histori'){ ?>
                    <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-md-1 control-label">No.Kartu</label> 
                          <div class="col-md-2">
                              <input type="text" class="form-control" id="nokartu" autocomplete="off" placeholder="ketik no kartu peserta">
                          </div>

                          <label class="col-md-1 control-label">Tanggal</label>
                          <div class="col-md-4">
                              <div class="input-group date">
                                  <input type="text" class="form-control datepicker" id="tanggal1" placeholder="yyyy-MM-dd" maxlength="10">
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar">
                                      </span>
                                  </span>
                                  <span class="input-group-addon">
                                      s.d
                                  </span>
                                  <input type="text" class="form-control datepicker" id="tanggal2" placeholder="yyyy-MM-dd" maxlength="10">
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar">
                                      </span>
                                  </span>
                              </div>
                          </div>

                          <div class="col-md-1">
                              <a id="caridatahistori" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                          </div>
                      </div>
                    </div>
                  
                        <table id="tableHistori" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                    <th>Diagnosa</th>
                                    <th>Jenis Pelayanan</th>
                                    <th>Kelas Rawat</th>
                                    <th>Nama Peserta</th>
                                    <th>No.Kartu</th>
                                    <th>No.SEP</th>
                                    <th>No.Rujukan</th>
                                    <th>Poli</th>
                                    <th>PPK Pelayanan</th>
                                    <th>Tgl. Pulang SEP</th>
                                    <th>Tgl. SEP</th>
                                </tr>
                            </thead>
                            <tbody id="bodyHistori">
                            </tfoot>
                        </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane <?= (($tabActive == 'data_klaim_jasaraharja') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'data_klaim_jasaraharja'){ ?>
                  <div class="form-horizontal">
                      <div class="form-group">
                          <label class="col-md-1 control-label">Pelayanan</label> 
                          <div class="col-md-2">
                                <select class="form-control" id="jenislayanan">
                                    <option value="1">Rawat Inap</option>
                                    <option value="2">Rawat Jalan</option>
                                </select>
                          </div>

                          <label class="col-md-1 control-label">Tanggal</label>
                          <div class="col-md-4">
                              <div class="input-group date">
                                  <input type="text" class="form-control datepicker" id="tanggal1" placeholder="yyyy-MM-dd" maxlength="10">
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar">
                                      </span>
                                  </span>
                                  <span class="input-group-addon">
                                      s.d
                                  </span>
                                  <input type="text" class="form-control datepicker" id="tanggal2" placeholder="yyyy-MM-dd" maxlength="10">
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar">
                                      </span>
                                  </span>
                              </div>
                          </div>

                          <div class="col-md-1">
                              <a id="cari_klaim_jaminan_jasaraharja" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                          </div>
                      </div>
                    </div>
                  
                  <div style="overflow-x:scroll;">
                        <table id="table" class="table table-bordered table-striped table-hover" cellspacing="0">
                            <thead>
                                <tr class="bg bg-gray">
                                    <th colspan="6">SEP</th>
                                    <th colspan="3">Peserta</th>
                                    <th colspan="8">Jasa Raharja</th>
                                </tr>
                                <tr class="bg bg-gray">
                                    <th>No.SEP</th>
                                    <th>Tgl.SEP</th>                                    
                                    <th>Tgl.Pulang SEP</th>
                                    <th>Pelayanan</th>
                                    <th>Poli</th>
                                    <th>Diagnosa</th>
                                    
                                    <th>No.Kartu</th>
                                    <th>Nama</th>
                                    <th>NoMR</th> 
                                    
                                    <th>Tgl.Kejadian</th>
                                    <th>No.Register</th>
                                    <th>Status Dijamin</th>
                                    <th>Status Dikirim</th>
                                    <th>Dijamin</th>                                    
                                    <th>Plafon</th>
                                    <th>Dibayar</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="body">
                            </tfoot>
                        </table>
                  </div>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->