<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
    .modal-lg {width: 90%;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_bridging" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'pembuatan_sep') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/sep?tabActive=pembuatan_sep') ?>">SEP Rujukan</a></li>              
              <li class="<?= (($tabActive == 'pembuatan_sep_manual') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/sep?tabActive=pembuatan_sep_manual') ?>">SEP Rujukan Manual/IGD</a></li>
              <li class="<?= (($tabActive == 'persetujuan_sep') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/sep?tabActive=persetujuan_sep') ?>">Persetujuan SEP</a></li>
              <li class="<?= (($tabActive == 'updatepulang_sep') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/sep?tabActive=updatepulang_sep') ?>">Update Pulang SEP</a></li>              
              <li class="<?= (($tabActive == 'finger_print') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/sep?tabActive=finger_print') ?>">List Finger Print</a></li>
            </ul>
            <div class="tab-content">
              <!--pembuatan sep-->
                <div class="tab-pane <?= (($tabActive == 'pembuatan_sep') ? 'active' : '' ); ?>" id="tab_pembuatansep">
                    <?php if($tabActive == 'pembuatan_sep'){ ?>
                    <div class="form-group">      
                        <form class="form-horizontal" id="formBuatanSep">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Asal Rujukan</label>
                                <div class="col-md-2">
                                    <select name="asalrujukan" id="asalrujukan" class="form-control">
                                        <option value="1">Faskes Tingkat 1</option>
                                        <option value="2">Faskes Tingkat 2</option>
                                    </select>
                                </div>
                                <div class="col-md-2"> </div>
                                <label class="col-md-2 control-label">No.SEP</label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="txtCariNosep" placeholder="ketik nomor sep" maxlength="19">
                                        <span class="input-group-btn">
                                            <button type="button" id="btnCariNosep" class="btn btn-info">
                                                <i class="fa fa-search"></i> Cari SEP
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">No.Rujukan</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="txtTab2Norujukan" placeholder="ketik nomor rujukan" maxlength="19">
                                        <span class="input-group-btn">
                                            <button type="button" id="btnTab2Carirujukan" jenis="Rujukan" class="btn btn-info">
                                                <i class="fa fa-search"></i> Cari Rujukan
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>

                           <div class="form-group">
                                <label class="col-md-2 control-label">No.Kartu</label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="txtTab2Nokartu" placeholder="ketik nomor kartu" maxlength="19">
                                        <span class="input-group-btn">
                                            <button type="button" id="btnTab2Carirujukan" jenis="Kartu" class="btn btn-info">
                                                <i class="fa fa-search"></i> Cari Rujukan
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                    <?php $this->load->view('bpjs/v_sep_form');?>
                    <?php } ?>
                </div>
              <!--end pembuatan sep-->
              <!-- /.tab-pane -->
              
              <!--pembuatan sep-->
                <div class="tab-pane <?= (($tabActive == 'pembuatan_sep_manual') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'pembuatan_sep_manual'){ ?>
                    <div class="form-group">   
                        <form class="form-horizontal" id="formBuatanSep">
                            <!--pencarian jenis rujukan 2-->
                            <div class="form-group">
                            <div class="col-md-2"></div>
                                <div class="col-md-7 bg bg-info" style="border-radius:4px;">
                                    <small>
                                        Pembuatan SEP rawat jalan menggunakan no.kartu hanya bisa :
                                        <br>1. Untuk PPK yang tidak menggunakan jaringan komunikasi dapat manual.
                                        <br>2. Untuk PPK yang mempunyai jaringan komunikasi data hanya bisa menerbitkan SEP Gawat Darurat.
                                    </small>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Pelayanan</label>
                                <div class="col-md-2">
                                    <select name="jenispelayanan" id="jenispelayanan" class="form-control">                                            
                                        <option value="2">Rawat Jalan</option>
                                        <option value="1">Rawat Inap</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Jenis Faskes</label>
                                <div class="col-md-2">
                                    <select id="asalrujukan" name="asalrujukan" class="form-control">                                            
                                        <option value="1">Faskes 1</option>
                                        <option value="2">Faskes 2/RS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">PPK Asal Rujukan</label>
                                <div class="col-md-4">
                                    <select  id="cari_ppkasal" class="form-control" style="width:100%;"></select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-2 control-label">No.Rujukan</label>
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="txtTab2Norujukan" placeholder="ketik nomor rujukan" maxlength="19">
                                        <span class="input-group-btn">
                                            <button type="button" id="btnTab2Carirujukan" jenis="Rujukan" class="btn btn-info">
                                                <i class="fa fa-search"></i> Cari Rujukan
                                            </button>
                                        </span>
                                    </div>
                                </div>

                                <label class="col-md-2 control-label">No.Kartu</label>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="txt_carikartu" placeholder="ketik nomor kartu" maxlength="19">
                                        <span class="input-group-btn">
                                            <button type="button" id="cari_peserta" jenis="kartu" class="btn btn-info">
                                                <i class="fa fa-search"></i> Cari Peserta
                                            </button>
                                        </span>
                                    </div>  
                                </div>
                            </div>
                            
                            
                    <?php $this->load->view('bpjs/v_sep_form') ?>
                    <?php } ?>
                </div>
              <!--end pembuatan sep-->
              <!-- /.tab-pane -->
              
              <!--persetujuan sep-->
              <div class="tab-pane <?= (($tabActive == 'persetujuan_sep') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'persetujuan_sep'){ ?>
                <!--Data Kunjungan-->
                <div class="col-md-2">
                    <select class="form-control" id="ps_jenislayanan">                                        
                        <option value="2">Rawat Jalan</option>
                        <option value="1">Rawat Inap</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <div class="input-group date">
                        <input type="text" class="form-control" id="ps_txttanggalsep" placeholder="Tanggal SEP" autocomplete="off" readonly>
                        <span class="input-group-addon">
                            <span class="fa fa-calendar">
                            </span>
                        </span>
                    </div>
                </div>
                <div class="col-md-3">
                    <a id="ps_cari" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                    <a id="btnTambahPengajuanSEP" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modal-tambahpengajuan"><i class="fa fa-plus"></i> Tambah Pengajuan</a>
                </div>
                <div class="col-md-12"><br></div>
                <table id="ps_tblSep" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                    <thead>
                        <tr class="bg bg-gray">
                        <th>No.Kartu</th>
                        <th>No.SEP</th>
                        <th>No.Rujukan</th>
                        <th>Tgl.SEP</th>
                        <th>Tgl.Pulang SEP</th>                                        
                        <th>Diagnosa</th>
                        <th>Jenis Pelayanan</th>
                        <th>Kelas Rawat</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody id="ps_tablebody">
                    </tfoot>
                </table>
                
                <!--modal pengajuan-->
                <div class="modal fade" id="modal-tambahpengajuan">
                    <div class="modal-dialog">
                      <div class="modal-content">                        
                        <form id="formPengajuanSEP" autocomplete="off">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Persetujuan SEP</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Tgl. SEP</label>
                                <div class="input-group">
                                    <input type="text" class="form-control datepicker" name="tanggal_sep" placeholder="yyyy-MM-dd" maxlength="10">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar">
                                        </span>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>No.Kartu</label>
                                <div class="input-group">
                                    <input type="text" name="nokartu" class="form-control" placeholder="ketik nomor kartu bpjs"/>
                                    <span class="input-group-btn">
                                        <button type="button" id="persetujuan_carisep" class="btn btn-info">
                                            <i class="fa fa-search"></i> cari
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" id="persetujuan_tampilnama" class="form-control" disabled/>
                            </div>
                            <div class="form-group">
                                <label>Pelayanan</label>
                                <select name="jenis_pelayanan" class="form-control"><option value="1">Rawat Inap</option><option value="2">Rawat Jalan</option></select>
                            </div>
                            <div class="form-group">
                                <label>Pengajuan</label>
                                <select name="jenis_pengajuan" class="form-control"><option value="1">Pengajuan Backdate</option><option value="2">Pengajuan Finger Print</option></select>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
                          <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        </form>
                      </div>
                      <!-- /.modal-content -->
                    </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.modal -->
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
                <!--update tanggal pulang sep-->
                <div class="tab-pane <?= (($tabActive == 'updatepulang_sep') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'updatepulang_sep'){ ?>
                    <!--Data Kunjungan-->
                    <div class="col-md-5">
                        <label class="col-md-2 control-label"></label>
                        <div class="col-sm-3">
                        <input type="text" class="form-control" id="ups_txttahunbulan" autocomplete="off" readonly>
                            <!-- <input type="text" class="form-control" id="ups_filter" placeholder="ketik nomor kartu" maxlength="13"> -->
                        </div>                    
                        <div class="col-md-1"">
                        <a id="ups_cari" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>                    
                    </div>
                    <div class="col-md-12"> </div>
                    <table id="ups_tblSep" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                        <thead>
                            <tr class="bg bg-gray">
                            <th>No</th>
                            <th>No.SEP</th>
                            <th>No.SEP Updating</th>
                            <th>PPK Tujuan</th>                                        
                            <th>No.Kartu</th>
                            <th>Nama Peserta</th>
                            <th>Tgl.SEP</th>
                            <th>Tgl.Pulang</th>
                            <th>Status</th>
                            <th>Tgl.Meninggal</th>
                            <th>No.Surat</th>
                            <th>Keterangan</th>
                            <th>User</th>
                        </tr>
                        </thead>
                        <tbody id="ups_tablebody">
                        </tfoot>
                    </table>
                
                    
                    <!--form update tanggal pulang-->
                        <div class="modal fade" id="modal-updatetglpulang">
                            <div class="modal-dialog">
                              <div class="modal-content">                        
                                <form id="formUpdateSEPTglPulang" autocomplete="off">
                                    <input type="hidden" name="nosep" id="update_tglpulang_sep"/>
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Persetujuan SEP</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label>Status Pulang</label>
                                        <select class="form-control" onchange="change_status_pulang(this.value)" name="status_pulang">
                                            <option value="1">Atas Persetujuan Dokter</option>
                                            <option value="3">Atas Permintaan Sendiri</option>
                                            <option value="4">Meninggal</option>
                                            <option value="5">Lain-lain</option>
                                        </select>
                                    </div>
                                    
                                    <div class="form-group" data-form="4">
                                        <label>No.Surat Ket.Meninggal</label>
                                        <input type="text" name="no_surat" class="form-control" placeholder="no surat keterangan meniggal"/>
                                    </div>
                                    
                                    <div class="form-group" data-form="4">
                                        <label>Tgl.Meninggal</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" name="tgl_meninggal" placeholder="yyyy-MM-dd" maxlength="10">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Tgl.Pulang</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker" name="tgl_pulang" placeholder="yyyy-MM-dd" maxlength="10">
                                            <span class="input-group-addon">
                                                <span class="fa fa-calendar">
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>No.LP Manual</label>
                                        <input type="text" name="nolp" class="form-control" placeholder="no LP Manual"/>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Keluar</button>
                                  <button type="submit" class="btn btn-primary">Simpan</button>
                                </div>
                                </form>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                        <!-- /.modal-dialog -->
                      </div>
                      <!-- /.modal -->
                <?php } ?>
                
                </div>
                <!-- /.tab-pane -->
              
              <!--list finger print-->
              <div class="tab-pane <?= (($tabActive == 'finger_print') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'finger_print'){ ?>
                
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-1 control-label">No.Kartu</label>
                        <div class="col-md-2"><input type="text" class="form-control" id="nokartu" placeholder="ketik nomor kartu" maxlength="13"></div>

                        <label class="col-md-1 control-label">Tgl.Pelayanan</label>
                        <div class="col-md-2">
                            <div class="input-group date">
                                <input type="text" class="form-control datepicker" id="tanggal" placeholder="yyyy-MM-dd" maxlength="10">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-1">
                            <a id="cari_fingerprint" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>
                    </div>
                </div>
                
                <table id="dtfingerprint" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                    <thead>
                        <tr class="bg bg-gray">
                        <th>No</th>
                        <th>No.Kartu</th>
                        <th>No.SEP</th>
                    </tr>
                    </thead>
                    <tbody id="table_body">
                    </tfoot>
                </table>
                
                <?php } ?>
                
              </div>
              <!-- /.tab-pane -->
              
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->