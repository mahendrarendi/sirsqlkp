<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-1"></div>
        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Tanggal</label>

        <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="input-group date">
                <input type="text" class="form-control datepicker" id="txtTgl1" placeholder="yyyy-MM-dd" maxlength="10">
                <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
                <span class="input-group-addon">
                    s.d
                </span>
                <input type="text" class="form-control datepicker" id="txtTgl2" placeholder="yyyy-MM-dd" maxlength="10">
                <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
            </div>

        </div>
        <div class="col-md-1">
            <a id="cari_rujukankeluar" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Cari</a>
        </div>
        
        <div class="col-md-1">
            <a id="download_list_rujukankeluar" alt="" class="btn btn-info btn-sm" data-toggle="tooltip" data-original-title="Download" href="#"><i class="fa fa-file-excel-o"></i> Download </a>
        </div>
    </div>

    <!-- Resume -->
    <div class="form-group">
        <table class="table table-bordered table-striped table-hover" style="font-size: 11.4px;" cellspacing="0" width="100%">
            <thead>
                <tr class="p-3 mb-2 bg-info text-white" id="infoResume">
                    <th>Jumlah Rujukan Penuh = 0</th>
                    <th>Jumlah Rujukan Partial = 0</th>
                    <th>Jumlah Rujukan Balik(Non PRB) = 0</th>
                    <th>TOTAL Semua Rujukan = 0</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<table id="dttable" class="table table-bordered table-striped table-hover dt-responsive dtql" style="font-size: 11.4px;" cellspacing="0" width="100%">
    <thead>
        <tr class="bg bg-gray">
        <th>No.</th>
        <th>No.Rujukan</th>
        <th>Tgl.Rujukan</th>
        <th>RI/RJ</th>
        <th>No.Sep</th>                                        
        <th>No.Kartu</th>
        <th>Nama</th>
        <th>Kode PPK Rujuk</th>
        <th>Nama PPK Rujuk</th>
        <th>Tipe Rujuk</th>
        <th>Diagnosa</th>
        <th>Spes/Sub</th>
        <th>Catatan</th>
        <th>Aksi</th>
    </tr>
    </thead>
    <tbody id="tablebody"></tfoot>
</table>