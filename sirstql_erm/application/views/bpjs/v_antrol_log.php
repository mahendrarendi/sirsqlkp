<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_antrol_log" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="<?= (($tabActive == 'log_taskId') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_log?tabActive=log_taskId') ?>">Log Task Id</a></li>
                <li class="<?= (($tabActive == 'log_kode_booking') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_log?tabActive=log_kode_booking') ?>">Log Kode Booking</a></li>              
                <li class="<?= (($tabActive == 'log_antrian') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_log?tabActive=log_antrian') ?>">Log Antrian</a></li>              
            </ul>
            <div class="tab-content">
                
                <div class="row ">
                    <div class="col-md-12" style="margin-bottom:15px;">
                        <input onchange="setLocalStorageTgl1(this.value)" id="tanggalAwal" style="margin-right:6px;" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal">
                        <input onchange="setLocalStorageTgl2(this.value)" id="tanggalAkhir" style="margin-right:6px;" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal akhir" > 
                        <a class="btn btn-info btn-sm" id="tampildata"><i class="fa fa-desktop"></i> Tampil</a>
                        <a style="margin-right: 6px;" id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                    </div>
                </div>
                
                <div class="tab-pane <?= (($tabActive == 'log_taskId') ? 'active' : (($tabActive == 'log_kode_booking') ? 'active' : (($tabActive == 'log_antrian') ? 'active' : '')) ); ?>">
                    <div class="col-md-12"> </div>
                        <table id="dtlog" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                        
                            <?php if($tabActive == 'log_taskId'){ ?>
                                <!-- $tabActive == 'log_antrian >>>>> -->
                                <thead>
                                    <tr class="bg bg-gray">
                                        <th>No</th>
                                        <th>Identitas</th>
                                        <th>Nama</th>
                                        <th>Waktu</th>
                                        <th>Task ID</th>
                                        <th>Keterangan</th>
                                    </tr>
                                </thead>
                            <?php } else if($tabActive == 'log_kode_booking'){ ?>
                                <!-- $tabActive == 'log_kode_booking >>>>> -->
                                <thead>
                                    <tr class="bg bg-gray">
                                        <th>No</th>
                                        <th>Identitas</th>
                                        <th>Nama</th>
                                        <th>Waktu</th>
                                        <th>Keterangan</th>
                                        <th>Jenis Kunjungan</th>
                                        <th>No Rujukan</th>
                                        <th>Cara Bayar/Daftar</th>
                                        <th>User</th>
                                        <th>Sinkron BPJS</th>
                                    </tr>
                                </thead>
                            <?php }  else if($tabActive == 'log_antrian'){ ?>
                                <!-- $tabActive == 'log_antrian >>>>> -->
                                <thead>
                                    <tr class="bg bg-gray">
                                        <th>No</th>
                                        <th>Task Id</th>
                                        <th>Waktu Task Id</th>
                                        <th>Sudah Sinkron BPJS</th>
                                        <th>Kode Booking</th>
                                        <th>ID Pendaftaran</th>
                                    </tr>
                                </thead>
                            <?php } ?>
                            <tbody></tbody>
                            <tfoot></tfoot>
                        </table>
                </div>
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->