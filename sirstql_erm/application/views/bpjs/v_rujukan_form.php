<div class="form-group">      
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-md-2 control-label">Aksi</label>
            <div class="col-md-2">
                <select id="jenis_rujukan" onchange="init_formcari()" class="form-control">
                    <option value="1">Buat Rujukan</option>
                    <option value="2">Ubah Rujukan</option>
                </select>
            </div>
        </div>
        
        <div class="form-group" form-jenisrujukan="1">        
            <label class="col-md-2 control-label">No.SEP</label>
            <div class="col-md-3">
                <div class="input-group">
                    <input type="text" class="form-control" id="txtCariNosep" placeholder="ketik nomor sep" maxlength="19">
                    <span class="input-group-btn">
                        <button type="button" id="btnCariNosep" class="btn btn-info">
                            <i class="fa fa-search"></i> Cari SEP
                        </button>
                    </span>
                </div>
            </div>
        </div>
        
        <div class="form-group" form-jenisrujukan="2">        
            <label class="col-md-2 control-label">No.Rujukan</label>
            <div class="col-md-5">
                <div class="input-group">
                    <input type="text" class="form-control" id="txtCariRujukan" placeholder="ketik nomor rujukan" maxlength="19">
                    <span class="input-group-btn">
                        <button type="button" id="btnCariRujukan" class="btn btn-info">
                            <i class="fa fa-search"></i> Cari Rujukan
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <form class="form-horizontal" id="formBuatanRujukan">
        <input type="hidden" id="modesave" value="insert" />
        <input type="hidden" id="tglrencanakunjungan" name="tglrencanakunjungan" class="form-control" readonly>
        <input type="hidden" id="tglberlakukunjungan" name="tglberlakukunjungan" class="form-control" readonly>
        <div class="col-md-12">&nbsp;</div>
            <section class="col-sm-4">
                <!--SEP-->
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title">Data SEP</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <input type="hidden" id="hdnTipeRujukan">
                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">No.Sep</span>
                          <div class="col-md-8"><input type="text" id="nosep" name="nosep" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Tgl.Sep</span>
                          <div class="col-md-8"><input type="text" id="tglsep" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Pelayanan</span>
                          <div class="col-md-8"><input type="text" id="pelayanan" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Diagnosa</span>
                          <div class="col-md-8"><input type="text" id="diagnosa" class="form-control" readonly/></div>
                        </div>

                    </div>
                </div>
                
                <!--Pasien/Peserta-->
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title">Peserta</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">No.Kartu</span>
                          <div class="col-md-8"><input type="text" id="nokartu" class="form-control" readonly/></div>
                        </div>
                        
                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Nama Pasien</span>
                          <div class="col-md-8"><input type="text" id="namapasien" class="form-control" readonly/></div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Tgl. Lahir</span>
                          <div class="col-md-8">
                              <div class="input-group">                                                    
                                    <input type="text" id="tanggallahir" class="form-control" readonly/>
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Jenis Kelamin</span>
                          <div class="col-md-8">
                              <select class="form-control" id="jeniskelamin">
                                  <option value="Laki-laki">Laki-laki</option>
                                  <option value="Perempuan">Perempuan</option>
                              </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">Hak Kelas</span>
                          <div class="col-md-8">
                              <input type="text" id="hakkelas" class="form-control" readonly/>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <span for="" class="col-md-4 label-daftar control-label">PPK Asal Peserta</span>
                          <div class="col-md-8">
                              <input type="hidden" id="kodeppkasal" name="kodeppkasal" class="form-control"/>
                              <input type="text" id="ppkasalpeserta" class="form-control" readonly/>
                          </div>
                        </div>

                    </div>
                </div>
            </section>

            <section class="col-sm-8">
                <div class="box box-solid box-default">
                    <div class="box-header">
                      <h3 class="box-title" id="judulRujukan">Kelengkapan Data Rujukan</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">No.Rujukan</span>
                          <div class="col-md-4">
                              <input type="text" value="" class="form-control" id="norujukan" name="norujukan" readonly/>
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Tgl.Rujukan</span>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <input type="text" id="tglrujukan" name="tglrujukan" class="form-control datepicker">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <sub>format tanggal : yyyy-mm-dd</sub>
                            </div>
                        </div>
                        
                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Pelayanan</span>
                          <div class="col-md-3">
                              <select class="form-control" id="jenislayanan" name="jenislayanan">
                                  <option value="2">Rawat Jalan</option>
                                  <option value="1">Rawat Inap</option>
                              </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <span for="" class="col-md-3 label-daftar control-label">Tipe</span>
                          <div class="col-md-4">
                              <select class="form-control" id="tiperujukan" name="tiperujukan">
                                  <option value="0">Penuh</option>
                                  <option value="1">Partial</option>
                                  <option value="2">Rujuk Balik (Non PRB)</option>
                              </select>
                          </div>
                        </div>
                        
                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Diagnosa Rujukan</span>
                            <div class="col-md-7">
                                <select id="dropdown_caridiagnosa" name="kode_diagnosa" class="form-control"></select>
                            </div>
                        </div>
                        
                        <div class="form-group" data-form="0,1">
                            <span for="" class="col-md-3 label-daftar control-label">Dirujuk Ke</span>
                            <div class="col-md-7">
                                <div class="input-group">
                                    <input type="hidden" id="kodefaskes" name="kodefaskes" value="">
                                    <input type="text" class="form-control" id="namafaskes" placeholder="" disabled>
                                    <span class="input-group-btn"><a id="carifaskes" class="btn btn-sm btn-info"><i class="fa fa-search"></i> cari ppk</a></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group" data-form="2">
                            <span for="" class="col-md-3 label-daftar control-label">Dirujuk Ke</span>
                            <div class="col-md-6">
                                <input id="txtppkasalpeserta" class="form-control" disabled>
                            </div>
                        </div>
                        

                        <div class="form-group" class="form-group" data-form="0">
                            <span for="" class="col-md-3 label-daftar control-label">Spesialis/SubSpesialis</span>
                            <div class="col-md-5">
                                <input type="hidden" class="form-control" name="polirujukan" id="polirujukan" readonly/>
                                <input type="text" class="form-control" name="poli" id="poli" readonly/>
                            </div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label">Catatan Rujukan</span>
                            <div class="col-md-7"><textarea class="form-control" id="catatan" name="catatan" placeholder="ketik catatan apabila ada"></textarea></div>
                        </div>

                        <div class="form-group">
                            <span for="" class="col-md-3 label-daftar control-label"></span>
                            <div class="col-md-8">
                                <a class="btn btn-primary btn-md" id="btnSimpanRujukan"><i class="fa fa-save"></i> Simpan</a>
                                <a class="btn btn-danger btn-md" id="btnPrintRujukan"><i class="fa fa-print"></i>Print</a>
                            </div>
                        </div>

                    </div>
                </div>
            </section>                           
    </form>
</div>



<style type="text/css">
    .modal-lg {width: 85%;}
</style>
<!--modal cari ppk dan jadwal praktek-->
<div class="modal fade" id="modal_carippk_dan_jadwalpraktek">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">                        
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"> <i class="fa fa-calendar"></i> Jadwal Praktek dan Sarana Rumah Sakit Rujukan</h4>
        </div>
        <div class="modal-body">
            
            <div class="form-horizontal">                
                <div class="form-group">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">PPK Rujuk</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <select id="dropdow_carippk" class="form-control" style="width:100%;"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">Tgl.Rencana Rujukan</label>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class="input-group date">
                            <input type="text" class="form-control datepicker" id="txttglrencanarujukan" placeholder="yyyy-MM-dd" maxlength="10">
                            <span class="input-group-addon">
                                <span class="fa fa-calendar">
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3 col-sm-3 col-xs-12"></div>
                    <div class="col-md-5 col-sm-5 col-xs-12">                        
                        <button class="btn bg-maroon" id="btnCariPoli" type="button"> <i class="fa fa-user-md"></i> Spesialis/SubSpesialis</button>
                        <button class="btn bg-purple" id="btnCariSarana" type="button"> <i class="fa fa-medkit"></i> Sarana</button>
                        <button class="btn btn-danger" id="btnBatalJadwal" type="button"> <i class="fa fa-undo"></i> Batal</button>
                    </div>
                </div>
            </div>            
            
            <!--list jadwal atau fasilitas-->
            <div class="col-md-12">
            <div class="box">
                <div class="box-body" id="boxBody">
                    <!--tampipl dari js - vclaim_rujukan-->
                </div>
            </div>
        </div>
            
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-right" data-dismiss="modal">Keluar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->