 <style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'rencana_kontrol') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rencanakontrol?tabActive=rencana_kontrol') ?>">Rencana Kontrol/Inap</a></li>
              <li class="<?= (($tabActive == 'list_rencana_kontrol') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rencanakontrol?tabActive=list_rencana_kontrol') ?>">List Rencana Kontrol / Inap</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane <?= (($tabActive == 'rencana_kontrol') ? 'active' : '' ); ?>">
                  <?php if($tabActive == 'rencana_kontrol'){ $this->load->view('bpjs/v_rencanakontrol_create');} ?>
              </div>
              <!-- /.tab-pane -->
              
              <!--persetujuan sep-->
              <div class="tab-pane <?= (($tabActive == 'list_rencana_kontrol') ? 'active' : '' ); ?>">
                  <?php if($tabActive == 'list_rencana_kontrol'){ $this->load->view('bpjs/v_rencanakontrol_list');} ?>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->