<form class="form-horizontal">
	<div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">No.SRB Peserta</label>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" class="form-control" id="prb_txtnosrb" placeholder="ketik Nomor SRB Peserta" maxlength="19" />
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-3 col-sm-3 col-xs-12 control-label">No.SEP</label>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <input type="text" class="form-control" id="prb_txtnosep" placeholder="ketik nomor SEP" maxlength="19" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-3 col-sm-3 col-xs-12"></div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <button class="btn btn-primary" id="prb_SearchBySRB_SEP" type="button"> <i class="fa fa-search"></i> Cari</button>
        </div>
    </div>
</form>

<!-- result table -->
<table id="prb_table" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
    <thead>
        <tr class="bg bg-gray">
        <th>No</th>
        <th>No.SRB</th>                                        
        <th>No.SEP</th>
        <th>Kode DPJP</th>
        <th>Nama Peserta</th>
        <th>Program PRB</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody id="prb_table_tablebody">
    </tfoot>
</table>