<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'form_rujukan') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rujukan?tabActive=form_rujukan') ?>">Form Rujukan</a></li>
              <li class="<?= (($tabActive == 'list_rujukan') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/rujukan?tabActive=list_rujukan') ?>">List Rujukan</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= (($tabActive == 'form_rujukan') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'form_rujukan'){ $this->load->view('bpjs/v_rujukan_form'); } ?>
                </div>
                <!-- /.tab-pane -->
              
                <!--pembuatan sep-->
                <div class="tab-pane <?= (($tabActive == 'list_rujukan') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'list_rujukan'){ $this->load->view('bpjs/v_rujukan_list');} ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->