<div class="form-group">       
    <div class="form-horizontal">
		<div class="form-group">
			<label class="col-md-3 col-sm-3 col-xs-12 control-label">Aksi</label>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<select id="jenis_rujukan" onchange="formSelectAction()" class="form-control">
                    <option value="1">Buat Rujukan Balik</option>
                    <option value="2">Ubah Rujukan Balik</option>
                </select>
            </div>
		</div>
        
        <div class="form-group" form-jenisrujukan="1"> 			
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-12 control-label">No.SEP</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" class="form-control" id="txtCariNosep" placeholder="ketik nomor SEP" maxlength="19" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-12"></div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<button class="btn btn-primary" id="btnCariNosep" type="button"> <i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
			<div class="col-md-12">&nbsp;</div>
        </div>
        
        <div class="form-group" form-jenisrujukan="2">
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-12 control-label">No.SRB Peserta</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" class="form-control" id="txtSearchBySRB" name="txtSearchBySRB" placeholder="ketik Nomor SRB Peserta" maxlength="19" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-sm-3 col-xs-12 control-label">No.SEP</label>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<input type="text" class="form-control" id="txtSearchBySEP" name="txtSearchBySEP" placeholder="ketik nomor SEP" maxlength="19" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3 col-sm-3 col-xs-12"></div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<button class="btn btn-primary" id="prb_SearchBySRB_SEP" type="button"> <i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
			<div class="col-md-12">&nbsp;</div>
        </div>
    </div>

    <form class="form-horizontal" id="formBuatPRB">	
        <input type="hidden" id="modesave" value="insert" />
        	
       <section class="col-sm-4">
			<!--SEP-->
			<div class="box box-solid box-default">
				<div class="box-header">
				  <h3 class="box-title"> SEP</h3>
				  <div class="box-tools">
					  <button type="button" class="btn btn-box-tool" data-widget="collapse">
						  <i class="fa fa-minus"></i>
					  </button>
				  </div>
				</div>                  
				<!-- /.box-header -->
				<div class="box-body no-padding">
					<div class="form-group">
						
					</div>
					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label">No.SEP</span>
					  <div class="col-md-8"><input type="text" id="txtnosep" name="txtnosep" class="form-control" readonly></div>
					</div>
					
					<div class="form-group" form-jenisrujukan="1">
					  <span for="" class="col-md-4 label-daftar control-label">Tgl. SEP</span>
					  <div class="col-md-8"><input type="text" id="txtTanggalSep" name="txtTanggalSep" class="form-control" readonly></div>
					</div>

					<div class="form-group" form-jenisrujukan="1">
					  <span for="" class="col-md-4 label-daftar control-label">Jns.Pelayanan</span>
					  <div class="col-md-8"><input type="text" id="jnsPelayanan" name="jnsPelayanan" class="form-control"readonly></div>
					</div>
					
					<div class="form-group" form-jenisrujukan="1">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">Spesialis/ Subspesialis</span>
					  <div class="col-md-8"><input type="text" id="poli" name="poli" class="form-control" readonly /></div>
					</div>
					
					<div class="form-group" form-jenisrujukan="1">
					  <span for="" class="col-md-4 label-daftar control-label">Diagnosa</span>
					  <div class="col-md-8"><input type="text" id="diagnosa" name="diagnosa" class="form-control" readonly /></div>
					</div>
					
				</div>
			<!-- /.box-body -->
			</div>
			
			<!--Pasien/Peserta-->
			<div class="box box-solid box-default">
				<div class="box-header">
				  <h3 class="box-title">Peserta</h3>
				  <div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">
						<i class="fa fa-minus"></i>
					</button>
				  </div>
				</div>
				<!-- /.box-header -->
				<div class="box-body no-padding">
						
					<div class="form-group">
					 
					</div>                        
					
					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">No.Kartu</span>
					  <div class="col-md-8"><input type="text" id="nokartu" name="nokartu" class="form-control" readonly /></div>
					</div>

					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">No.Rujukan</span>
					  <div class="col-md-8"><input type="text" id="norujukan" name="norujukan" class="form-control" readonly /></div>
					</div>

					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">Nama Peserta</span>
					  <div class="col-md-8"><input type="text" id="namapasien" name="namapasien" class="form-control" readonly /></div>                        
					</div>

					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label">Tgl.Lahir</span>
					  <div class="col-md-8"><input type="text" id="tanggallahir" name="tanggallahir" class="form-control" readonly /></div>  
					</div>

					<div class="form-group">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">Kelamin</span>
					  <div class="col-md-8"><input type="text" id="jeniskelamin" name="jeniskelamin" class="form-control" readonly /></div>
					</div>

					<div class="form-group" form-jenisrujukan="1">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">No.MR</span>
					  <div class="col-md-8"><input type="text" id="txtNoMR" name="txtNoMR" class="form-control" readonly /></div>
					</div>

					<div class="form-group" form-jenisrujukan="2">
					  <span for="" class="col-md-4 label-daftar control-label" style="word-wrap:break-word;">Asal Faskes</span>
					  <div class="col-md-8"><input type="text" id="txtFaskesNama" name="txtFaskesNama" class="form-control" readonly /> </div>
					</div>
			  </div>
			<!-- /.box-body -->
			</div>
		</section>

		<section class="col-sm-8">
			<!-- Kelengkapan Data Rujukan -->
			<div class="box box-solid box-default">
				<div class="box-header">
				  <h3 class="box-title" id="judulRujukan">Kelengkapan Data Rujuk Balik</h3>
				</div>
				<!-- /.box-header -->

				<div class="box-body">
					<div class="form-group" form-jenisrujukan="2">
					  <span for="" class="col-md-3 label-daftar control-label">No. SRB</span>
					  	<div class="col-md-8"><input type="text" id="txtnosrb" name="txtnosrb" class="form-control" readonly /></div>  
					</div>

					<div class="form-group" form-jenisrujukan="2">
					  <span for="" class="col-md-3 label-daftar control-label">Tgl.Rujukan</span>
					  	<div class="col-md-8"><input type="text" id="tanggalrujukan" name="tanggalrujukan" class="form-control" readonly /></div>  
					</div>
					
					<div class="form-group">
					  <span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Alamat Peserta<label style="color:red;font-size:small">*</label></span>
					  <div class="col-md-9"><input type="text" id="txtalamat" name="txtalamat" class="form-control" placeholder="ketik alamat rumah tinggal peserta" maxlength="200"></div>
					</div>     
					
					<div class="form-group">
					  <span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Kontak/Email Peserta/Keluarga<label style="color:red;font-size:small">*</label></span>
					  <div class="col-md-9"><input type="text" id="txtemail" name="txtemail" class="form-control" placeholder="ketik kontak/alamat email peserta/keluarga peserta" maxlength="200"></div>
					</div>   
					
					<div class="form-group" >
						<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Program PRB<label style="color:red;font-size:small">*</label></span>
						<div class="col-md-9" form-jenisrujukan="1">							
							<select class="form-control" name="idprogramprb" id="idprogramprb">
								<option value="">-</option>
							</select>
						</div>
						<div class="col-md-3" form-jenisrujukan="1">
							<a id="refresh_programprb" class="btn btn-xs btn-success"><i class="fa fa-search"></i> Refresh Data</a>
						</div>
						<div class="col-md-9" form-jenisrujukan="2">							
							<input type="text" class="form-control" name="txtidprogramprb" id="txtidprogramprb" value="" readonly>
						</div>
					</div>
					<div class="form-group">
						<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">DPJP Pemberi Pelayanan<label style="color:red;font-size:small">*</label></span>
						<div class="col-md-9">
							<input type="text" class="form-control" id="nmdpjp" name="nmdpjp" readonly>
							<input type="hidden" class="form-control" id="kddpjp" name="kddpjp">
						</div>
					</div>
					
					<div class="form-group">
						<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Keterangan</span>
						<div class="col-md-9">
							<input type="text" class="form-control" id="txtdeskripsi" name="txtdeskripsi" placeholder="ketik keterangan maksimal 150 karakter" maxlength="150">
						</div>
					</div>

					<div class="form-group">
						<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Saran</span>
						<div class="col-md-9">
							<input type="text" class="form-control" id="txtsaran" name="txtsaran" placeholder="ketik saran maksimal 150 karakter" maxlength="150">
						</div>
					</div>                        

					<div class="form-group">
						<div class="col-md-9">
							<input type="hidden" class="form-control" id="txtuser">
						</div>
					</div>
				
					<!-- obat -->
					<div class="row">
						<div class="col-xs-12">
							<h4 class="page-header">
								<i class="fa fa-cart-plus"></i> Obat
							</h4>
						</div>
							
							<div class="form-group">
								<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Nama Obat <label style="color:red;font-size:small">*</label></span>
								<div class="col-md-8">
									<input type="hidden" class="form-control" name="kdObat" id="kdObat"/>
									<input type="hidden" class="form-control" name="nmObat" id="nmObat"/>
									<input list="listObat" type="text" autocomplete="off" class="form-control" id="obat" placeholder="ketik kode atau nama Obat min 3 karakter" />
									<datalist id="listObat"></datalist>
								</div>
							</div>

							<div class="form-group">
								<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Signa <label style="color:red;font-size:small">*</label></span>
								<div class="col-md-4">
									<div class='input-group'>
										<input type='number' class="form-control" id="txtsigna1" value="0" max="9"  min="0" placeholder="Signa 1"
												onKeyUp="if(this.value>99){this.value='9';}else if(this.value<0){this.value='0';}"/>
										<span class="input-group-addon">
											x
										</span>
										<input type='number' class="form-control" id="txtsigna2" value="0" max="9" min="0" placeholder="Signa 2"
												onKeyUp="if(this.value>99){this.value='9';}else if(this.value<0){this.value='0';}"/>
									</div>
								</div>
							</div>

							<div class="form-group">
								<span for="" class="col-md-3 label-daftar control-label" style="word-wrap:break-word;">Jumlah <label style="color:red;font-size:small">*</label></span>
								<div class="col-md-4 col-sm-4 col-xs-12">
									<div class="input-group">
										<input type="number" class="form-control" id="txtjmlobat" placeholder="jumlah obat" value="0" min="0">
										<span class="input-group-btn">
											<button type="button" id="btnAddObat" title="Tambah Obat" class="btn btn-default">                                               
												&nbsp;
												<i class="fa fa-plus"></i>
											</button>
										</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-2 col-sm-2 col-xs-12"></div>
								<div class="col-md-9 col-sm-9 col-xs-12">
									<table class="table table-bordered table-striped table-hover dt-responsive" style="font-size:small" width="100%" cellspacing="0" id="tbObat">
										<thead>
											<tr class="bg bg-gray">
												<th width="20%">Kode Obat</th>
												<th width="50%">Nama Obat</th>
												<th width="5%">S1</th>    
												<th width="5%">S2</th>                                                
												<th width="10%">Jumlah</th>                                                
												<th width="10%">Action</th>
											</tr>                                           
										</thead>										
										<tbody id="tbObatBody">
                    					</tfoot>
									</table>
								</div>
							</div>
					</div>
					<div>
						<label style="color:red;font-size:x-small">* Wajib Diisi</label>
					</div>
					<!-- /.obat -->
				</div>

				<div class="box-footer">
					<div class="form-group">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-8">
                                <a class="btn btn-primary btn-md" id="btnSimpanRujukBalik"><i class="fa fa-save"></i> Simpan</a>
								<a class="btn btn-danger btn-md" id="btnPrintRujukBalik"><i class="fa fa-print"></i> Print </a>
                            </div>
							<!-- <button id="btnInsertPRB" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Tambah PRB</button>
							
							<button id="btnSavePRB" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<button id="btnEditPRB" type="button" class="btn btn-warning"><i class="fa fa-edit"></i> Simpan Edit</button>
							<button id="btnDeletePRB" type="button" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus SRB</button>
							<button id="btnCetak" type="button" class="btn btn-info"><i class="fa fa-print"></i> Cetak</button>
							<button id="btnBatal" type="button" class="btn btn-default pull-right"><i class="fa fa-undo"></i> Batal</button> -->
						</div>
					</div>
			  </div>
			</div>                
			<!-- /. Kelengkapan Data Rujukan -->
		</section>                           
    </form>
	

	<table class="table table-bordered table-striped table-hover dt-responsive" style="font-size:small" width="100%" cellspacing="0" id="tbObatPrint">			
	<thead>
											<tr>
												<th>Nomor</th>    
												<th>Signa</th>                                                
												<th>Nama Obat</th>                                                
												<th>Jumlah Obat</th>
											</tr>                                           
										</thead>											
			<tbody id="tbObatBodyPrint">

			</tfoot>
	</table>
									
	


</div>







<!-- /.content -->
<script src="/VClaim/bundles/srb?v=evrwwCaKTunbcShm0yG7k8o1dkhLzMC7J7uYM1zHVWg1"></script>