<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_antrol" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'dashboardPerBulan') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_dashboard?tabActive=dashboardPerBulan') ?>">Dashboard Per Bulan</a></li>              
              <li class="<?= (($tabActive == 'dashboardPerTanggal') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_dashboard?tabActive=dashboardPerTanggal') ?>">Dashboard Per Tanggal</a></li>
              <li class="<?= (($tabActive == 'antreanPerTanggal') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_dashboard?tabActive=antreanPerTanggal') ?>">Antrean Per Tanggal</a></li>
              <li class="<?= (($tabActive == 'antreanBelumDilayani') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_dashboard?tabActive=antreanBelumDilayani') ?>">Antrean Belum Dilayani</a></li>
              <li class="<?= (($tabActive == 'listWaktuTaskID') ? 'active' : '' ); ?>"><a href="<?= base_url('cbpjs/antrol_dashboard?tabActive=listWaktuTaskID') ?>">List Waktu Task ID</a></li>
            </ul>
            <div class="tab-content">
                <!-- $tabActive == 'dashboardPerBulan >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'dashboardPerBulan') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'dashboardPerBulan'){ ?>
                        
                        <!-- search category >>>>> -->
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                            <div class="input-group date" >
                                <input type="text" class="datepicker" id="tahunbulanAntrol"  readonly>
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group date" >
                                <select id="jenisWaktuPerBulan" style="height: 30px;">
                                    <option value="rs" selected>Waktu RS</option>
                                    <option value="server">Waktu SERVER</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2"><a id="cari_dashboardPerBulan" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a></div>
                        <!-- <<<<< search category -->

                        <!-- Resume start-->
                        <div class="col-md-3">
                            <table class="table table-bordered table-striped table-hover" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="p-3 mb-2 bg-info text-white" id="infoResumeAntrol">
                                        <th>
                                            Total Antrean Lengkap = 0<br>
                                            Total Waktu = 0 jam, 0 menit, 0 detik<br>
                                            Rata-rata Waktu = -
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>    
                        <!-- Resume end -->
                                                
                        <div class="col-md-12"> </div>
                        <table id="tblDashboard" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>Poli</th>
                                <th>Tanggal</th>
                                <th>Jumlah Antrean</th>
                                <th>Waktu Tiap Task</th>
                                <th>Rata-Rata Waktu</th>
                            </tr>
                            </thead>
                            <tbody id="bodyDashboard">
                            </tbody>
                            <tfoot></tfoot>
                        </table>

                        <!-- chart dashboard per bulan >>>>> -->
                        <!-- <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6">
                                <div class="panel panel-default" id="chartDashboard1"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard2"></div>
                            </div>
                        </div>
                        
                        <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard3"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard4"></div>
                            </div>
                        </div>
                                                
                        <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard5"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard6"></div>
                            </div>
                        </div> -->
                        <!-- <<<<< chart dashboard per bulan -->
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'dashboardPerBulan -->  

                <!-- $tabActive == 'dashboardPerTanggal >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'dashboardPerTanggal') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'dashboardPerTanggal'){ ?>  

                        <!-- search category >>>>> -->
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                            <div class="input-group date" >
                                <input type="text" class="datepicker" id="tanggalAntrol"  readonly="">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group date" >
                                <select id="jenisWaktuPerTanggal" style="height: 30px;">
                                    <option value="rs" selected>Waktu RS</option>
                                    <option value="server">Waktu SERVER</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2"><a id="cari_dashboarPerTanggal" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a></div>
                        <!-- <<<<< search category -->

                        <!-- Resume start-->
                        <div class="col-md-3">
                            <table class="table table-bordered table-striped table-hover" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="p-3 mb-2 bg-info text-white" id="infoResumeSEPTerbitRajal">
                                        <th>
                                            Total SEP Terbit Rajal = <label id="jumlahsepterbit"></label>
                                        </th>
                                    </tr>
                                    <tr class="p-3 mb-2 bg-info text-white" id="infoResumeAntrolTanggal">
                                        <th>
                                            Total Antrean Lengkap = 0<br>     
                                            Total Waktu = 0 jam, 0 menit, 0 detik<br>
                                            Rata-rata Waktu = -
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>    
                        <!-- Resume end -->

                        <!-- chart dashboard per bulan >>>>> -->
                        <!-- <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6">
                                <div class="panel panel-default" id="chartDashboard1"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard2"></div>
                            </div>
                        </div>
                        
                        <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard3"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard4"></div>
                            </div>
                        </div>
                                                
                        <div class="row" style="padding-top:50px">
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard5"></div>
                            </div>
                            <div class="col-xs-12 col-md-6" >
                                <div class="panel panel-default" id="chartDashboard6"></div>
                            </div>
                        </div> -->
                        <!-- <<<<< chart dashboard per bulan -->
                        
                        <div class="col-md-12"> </div>
                        <table id="tblDashboard" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>Poli</th>
                                <th>Jumlah Antrean</th>
                                <th>Waktu Tiap Task</th>
                                <th>Rata-Rata Waktu</th>
                            </tr>
                            </thead>
                            <tbody id="bodyDashboard">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'dashboardPerTanggal -->  

                <!-- $tabActive == 'antreanPerTanggal >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'antreanPerTanggal') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'antreanPerTanggal'){ ?>  

                        <!-- search category >>>>> -->
                        <div class="col-md-4"></div>
                        <div class="col-md-2">
                            <div class="input-group date" >
                                <input type="text" class="datepicker" id="tanggalAntrean"  readonly="">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2"><a id="cari_antreanPerTanggal" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a></div>
                        <div class="col-md-4"></div>
                        <!-- <<<<< search category -->
                        
                        <div class="col-md-12"> </div>
                        <table id="tblAntreanPerTanggal" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>Identitas</th>
                                <th>Dokter</th>
                                <th>nohp</th>
                                <th>jeniskunjungan</th>
                                <th>nomorreferensi</th>
                                <th>sumberdata</th>
                                <th>ispeserta</th>
                                <th>antrean</th>
                                <th>createdtime</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody id="bodyAntreanPerTanggal">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'dashboardPerTanggal -->  

                <!-- $tabActive == 'antreanBelumDilayani >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'antreanBelumDilayani') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'antreanBelumDilayani'){ ?>                          
                        <div class="col-md-12"> </div>
                        <table id="tblAntreanBelumDilayani" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                <th>No</th>
                                <th>kodebooking</th>
                                <th>tanggal</th>
                                <th>Poli</th>
                                <th>Pasien</th>
                                <th>jeniskunjungan</th>
                                <th>nomorreferensi</th>
                                <th>sumberdata</th>
                                <th>ispeserta</th>
                                <th>antrean</th>
                                <th>status</th>
                            </tr>
                            </thead>
                            <tbody id="bodyAntreanBelumDilayani">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'antreanBelumDilayani --> 

                <!-- $tabActive == 'List Waktu Task ID' >>>>> -->
                <div class="tab-pane <?= (($tabActive == 'listWaktuTaskID') ? 'active' : '' ); ?>">
                    <?php if($tabActive == 'listWaktuTaskID'){ ?>  

                        <!-- search category >>>>> -->
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                            <label for="txt_kodebooking">Kode Booking : </label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="txt_kodebooking" name="txt_kodebooking"><br><br>                            
                        </div>
                        <div class="col-md-1"><a id="cari_listWaktuTaskID" class="btn btn-info btn-sm"><i class="fa fa-search"></i> Cari</a></div>
                        <div class="col-md-4"></div>
                        <!-- <<<<< search category -->
                        
                        <div class="col-md-12"> </div>
                        <table id="tblDashboard" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                            <thead>
                                <tr class="bg bg-gray">
                                    <th>Task ID</th>
                                    <th>Waktu</th>
                                    <th>Task Name</th>
                                </tr>
                            </thead>
                            <tbody id="bodyDashboard">
                            </tbody>
                            <tfoot></tfoot>
                        </table>
                    <?php } ?>
                </div>
                <!-- <<<<< $tabActive == 'List Waktu Task ID' --> 
               
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->