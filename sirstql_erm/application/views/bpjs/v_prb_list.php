<div class="form-horizontal">
    <div class="form-group">
        <label class="col-md-2 col-sm-3 col-xs-12 control-label">Tanggal</label>

        <!-- date input -->
        <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="input-group date">
                <input type="text" class="form-control datepicker" id="prb_txtDateStart" placeholder="yyyy-MM-dd" maxlength="10" readonly>
                <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
                <span class="input-group-addon">
                    s.d
                </span>
                <input type="text" class="form-control datepicker" id="prb_txtDateEnd" placeholder="yyyy-MM-dd" maxlength="10" readonly>
                <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
            </div>
        </div>

        <!-- search button -->
        <div class="col-md-1">
            <a id="prb_SearchByDate" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Cari</a>
        </div>

    </div>
</div>

<!-- result table -->
<table id="prb_table" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
    <thead>
        <tr class="bg bg-gray">
        <th>No</th>
        <th>No.SRB</th> 
        <th>No.Kartu</th>
        <th>Nama Peserta</th>
        <th>Program PRB</th>                                   
        <th>No.SEP</th> 
        <th>Action</th>
    </tr>
    </thead>
    <tbody id="prb_table_tablebody">
    </tfoot>
</table>