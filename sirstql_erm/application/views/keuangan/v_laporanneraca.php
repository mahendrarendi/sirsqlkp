<!-- Main content -->
<style>
    .table>tbody>tr>td{padding:6px;font-size: 13px;color:#575757;}
    .table-bordered>tbody>tr>td{border:0px;border-bottom: 1px solid #cacaca;}
    .table-striped>tbody>tr:nth-of-type(odd) {background-color: #f3f3f3d6;
    }
</style>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-md-10">
                <div class="row">
                    <div class="col-md-1">&nbsp;</div>
                  <div class="col-sm-2">
                    <label>Periode</label>
                    <select class="form-control" id="bulan" name="bulan">
                        <?php 
                            $no=0;
                            $bulannow = intval(date('m'));
                            foreach ($bulan as $arr)
                            {
                                ++$no;
                                echo '<option value="'. $no .'"  '.(($bulannow == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                            }
                        ?>
                    </select>
                  </div>
                    
                  <div class="col-sm-1">
                      <label>&nbsp;</label><br/>
                      <input type="text" id="tahun" name="tahun" class="form-control" />
                  </div>
                   
                    <div class="col-sm-3" style="width:20%;">
                      <label>&nbsp;</label><br/>
                      <input style="margin-top:15px;" type="checkbox" id="saldo" name="saldo" checked /> Tampil yang bersaldo
                  </div>
                  <div class="col-sm-3">
                    <label>&nbsp;</label><br>
                    <a id="reload" class="btn btn-primary btn-sm"><i class="fa fa-desktop"></i> Tampil Data</a> 
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a> 
                  </div>
                  
                  
                </div>
            </div>
            <div class="col-sm-12">&nbsp;
              <p style="text-align:center;font-size:20px;">Laporan Neraca</p>
              <p style="text-align:center;font-size:16px;display:none;" id="periode"> </span></p>
            </div>
                <table id="tbl_laporanneraca" class="table table-bordered table-hover dt-responsive" cellspacing="0" width="100%">
              <tbody id="tampildata">
              </tbody>
            </table>
          </div>
            <!-- /.box-body -->
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->