<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <?php if($mode == 'viewcoakasir'){ ?>
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6">
              <a id="tampil" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
              <table id="dttable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Kode</th>
                      <th>Akun</th>
                      <th>Cara Bayar</th>
                      <th>Kasir</th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            
          <?php }else if($mode == 'viewcoabank'){ ?>
            
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="dttable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="bg bg-yellow-gradient">
                      <th>Kode</th>
                      <th>Akun</th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>            
          <?php }else{ ?>
            
                <div class="row">
                    <div class="col-md-3">
                      <div class="box box-warning">
                        <div class="box-header with-border">
                            <span>Periode Laporan</span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <form class="" method="POST" action="<?= base_url('ckeuangan/update_periode')?>"  id="formperiode">
                                <div class="form-group">
                                    <label class="col-md-12">Bulan</label>
                                    <div class="col-md-12">
                                        <select class="form-control" name="periodebulan">
                                            <?php 
                                                $no=0;
                                                foreach ($bulan as $arr)
                                                {
                                                    ++$no;
                                                    echo '<option value="'. $no .'"  '.(($periode['bulan'] == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Tahun</label>
                                    <div class="col-md-12"><input type="text" class="form-control" name="periodetahun" value="<?= $periode['tahun']; ?>" /></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12" style="margin: 7px 0px;">
                                        <a class="btn btn-sm btn-primary" id="ubahperiode">Ubah Periode</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                    
                    <div class="col-md-4">
                      <div class="box box-warning">
                        <div class="box-header with-border">
                            <span>Tutup Buku</span>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <form class="" method="POST" action="<?= base_url('ckeuangan/create_tutup_buku')?>" id="formtutupbuku">
                                <input type="hidden" name="bulansekarang" value="<?= $periode['bulan'] ?>" />
                                <input type="hidden" name="tahunsekarang" value="<?= $periode['tahun'] ?>" />
                                <div class="form-group">
                                    <label class="col-md-12">Periode Sekarang</label>
                                    <div class="col-md-12 row">
                                        <div class="col-md-8">
                                            <small>Bulan</small>
                                            <select class="form-control" disabled>
                                                <?php 
                                                    $no=0;
                                                    foreach ($bulan as $arr)
                                                    {
                                                        ++$no;
                                                        echo '<option value="'. $no .'"  '.(($periode['bulan'] == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <small>Tahun</small>
                                            <input type="text" class="form-control" value="<?= $periode['tahun']; ?>" disabled />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12" style="margin-top:10px;">Periode Selanjutnya</label>
                                    <div class="col-md-12 row">
                                        <div class="col-md-8">
                                            <small>Bulan</small>
                                            <select class="form-control" name="bulanselanjutnya">
                                                <option value="0">Pilih</option>
                                                <?php 
                                                    $no=0;
                                                    foreach ($bulan as $arr)
                                                    {
                                                        ++$no;
                                                        echo '<option value="'. $no .'"  >'.$arr.'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <small>Tahun</small>
                                            <input type="text" class="form-control" name="tahunselanjutnya" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12" style="margin: 7px 0px;">
                                        <a class="btn btn-sm btn-primary" id="tutupbuku">Tutup Buku</a>
                                    </div>
                                </div>
                            
                            </form>
                        </div>
                      </div>
                    </div>
                    
                </div>
          <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->