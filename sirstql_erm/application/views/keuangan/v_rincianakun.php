<!-- Main content -->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-12">
                <div class="row">
                  
                  <div class="col-sm-1">
                    <label>&nbsp;</label><br>
                    <a class="btn btn-sm btn-danger" href="javascript.void(0)" onclick="window.close();"><i class="fa fa-backward"></i> Kembali</a>
                  </div>
                  <div class="col-md-1">                    
                    <label>&nbsp;</label><br>
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Excel</a>
                  </div>
                  <div class="col-sm-5">
                    <label>Akun</label>
                    <input name="akun" id="akun" class="form-control" type="text" value="<?= $akun['kode'].' '.$akun['nama']; ?>" readonly />
                  </div>
                  <div class="col-sm-2">
                    <label>Dari</label>
                    <input name="tanggal_mulai" id="tanggal_mulai" class="form-control" type="text" value="<?= $awal; ?>" readonly />
                  </div>
                  <div class="col-sm-2">
                    <label>Sampai</label>
                    <input name="tanggal_selesai" id="tanggal_selesai" class="form-control" type="text" value="<?= $selesai; ?>" readonly/>
                    <input name="myidakun" id="myidakun" class="form-control" type="hidden" value="<?= $myidakun; ?>" readonly/>
                  
                  </div>                  
                </div>
            </div>
                <div class="col-sm-12">&nbsp;<br>
            </div>
            <table id="dtrincianakun" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr class="bg bg-yellow-gradient">
                  <th style="width:50px;">No</th>
                  <th>Tanggal</th>
                  <th>Keterangan</th>
                  <th>Debet</th>
                  <th>Kredit</th>
                  <th>Saldo</th>
                  <th></th>
                </tr>
               </thead>
              <tbody>
                  <?php 
                    if(empty(!$riwayat))
                    {
                        $no=0;
                        $total = 0;
                        $debit = 0;
                        $kredit= 0;
                        foreach ($riwayat as $arr)
                        {
                            $total  += (($arr['isdebit'] == 1) ? ($arr['debet'] - $arr['kredit']) : ($arr['kredit'] - $arr['debet']) ) ;
                            $debit  += $arr['debet'];
                            $kredit += $arr['kredit'];
                            echo "<tr><td>". ++$no ."</td><td>".$arr['tanggal']."</td><td>".$arr['deskripsi']."</td><td>".convertToRupiah($arr['debet'])."</td><td>".convertToRupiah($arr['kredit'])."</td><td>".convertToRupiah($total)."</td><td><a id='ubahjurnal' kdjurnal='".$arr['kdjurnal']."' class='btn btn-xs btn-warning'><i class='fa fa-edit'></i> ubah jurnal</a></td></tr>";
                        }
                        
                        echo "<tr><th colspan='3' class='text-center'>Total</th><th>".convertToRupiah($debit)."</th><th>".convertToRupiah($kredit)."</th><th>".convertToRupiah($total)."</th></tr>";
                        
                    }
                  ?>
              </tbody>
            </table>
          </div>
            <!-- /.box-body -->
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->