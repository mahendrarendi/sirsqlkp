<!-- Main content -->
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-12">
                <div class="row">
                  <div class="col-sm-2">
                    <label>Nama Kasir</label>
                    <select name="idkasir" id="idkasir" class="select2 form-control" readonly disabled></select>
                    <input type="hidden" class="form-control" name="kasir" id="kasir" value="<?php echo $idkasir;?>" readonly >
                    <input type="hidden" class="form-control" name="bank" id="bank" value="<?php echo $bank;?>" readonly >
                  </div>
                  <div class="col-sm-2">
                    <label>Jenis Bayar</label>
                    <input type="text" class="form-control" name="jenisbayar" id="jenisbayar" value="<?php echo $jenistagihan;?>" readonly>
                  </div>
                  <div class="col-sm-2">
                    <label>Bank</label><br>
                    <select name="nama_bank" id="nama_bank" class="select2 form-control" readonly disabled></select>
                  </div>
                  <div class="col-sm-2">
                    <label>Tanggal</label><br>
                    <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?php echo $tanggal;?>" readonly>
                  </div>
                  <!-- <div class="col-sm-1">
                    <label></label><br>
                    <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                  </div> -->
                  <div class="col-sm-1">
                    <label></label><br>
                    <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
                  </div>
                </div>
              </div>
              <table id="dtrekaptransaksi" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th>NO</th>
                <th>WAKTU TAGIH</th>
                <th>NO.RM</th>
                <th>NAMA PASIEN</th>
                <th>TOTAL</th>
                <th>SALDO</th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->