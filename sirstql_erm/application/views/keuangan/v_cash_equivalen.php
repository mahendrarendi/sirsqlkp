<!-- Main content -->
    <section class="content">
        
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-xs-12" style="padding-bottom:4px;">
                <div class="nav-tabs-custom">
                   <!-- Tabs within a box -->
                   <ul class="nav nav-tabs">
                     <li class="active"><a href="#riwayattransaksi" data-toggle="tab">Riwayat Transaksi</a></li>
                     <li><a href="#saldoawal" id="view_saldoawal" data-toggle="tab">Saldo Awal</a></li>
                   </ul>
                   <div class="tab-content no-padding">
                     <!-- Morris chart - Sales -->
                     <div class="chart tab-pane active" id="riwayattransaksi" style="padding: 8px 0px;">
                         
                         <div class="row" style="margin-bottom: 4px;">
                           <form id="formCariEquivalen">
                               <div class="col-md-1">
                                    <label>Tahun </label><br>
                                    <input type="text" class="datepicker" name="tahun" size="5" value="<?= date('Y'); ?>"/> 
                               </div>
                                <div class="col-md-2">
                                     <label>Bulan</label><br>
                                     <select class="form-control" name="bulan">
                                         <?php 
                                             $no=0;
                                             $sekarang = date('n');
                                             foreach ($bulan as $arr)
                                             {
                                                 ++$no;
                                                 echo '<option value="'. $no .'"  '.(($sekarang == $no) ? 'selected' : '' ).' >'.$arr.'</option>';
                                             }
                                         ?>
                                     </select>
                                 </div>
                               <div class="col-md-1">
                                    <label>Awal </label><br>
                                    <select name="tgl1" class="form-control">
                                        <?php 
                                            $tglnow = date('d');
                                            for($x=1;$x<=31;$x++)
                                            {
                                                $tgl = (($x < 10) ? '0' :'' );
                                                echo "<option value=". $tgl.$x." ".(($tglnow == $tgl.$x) ? 'selected' : '' )."  >". $tgl.$x."</option>";
                                            }
                                        ?>
                                    </select>
                               </div>
                               <div class="col-md-1">
                                    <label>Akhir </label><br>
                                    <select name="tgl2" class="form-control">
                                        <?php 
                                            for($x=1;$x<=31;$x++)
                                            {
                                                $tgl = (($x < 10) ? '0' :'' );
                                                echo "<option value=". $tgl.$x." ".(($tglnow == $tgl.$x) ? 'selected' : '' ).">". $tgl.$x."</option>";
                                            }
                                        ?>
                                    </select>
                               </div>
                               <div class="col-md-">
                                <label>&nbsp;</label><br>
                                <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                                <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
                              </div>
                           </form>
                         </div>
                         
                         <div class="box box-default box-solid">
                            <div class="box-header with-border">
                              <h5 class="text-bold">Riwayat Transaksi</h5>
                            </div>
                            <div class="box-body">
                              <table id="tbl_cashequivalen" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                
                              </table>
                            </div>
                        </div>                      
                         
                         
                     </div>
                     <div class="chart tab-pane" id="saldoawal" style="padding: 8px 0px;">
                         
                         <div class="row" style="margin-bottom: 4px;">
                           <form id="formCariSaldoawalEquivalen" class="col-md-12 row">
                            <div class="col-md-4">
                                <label>Tahun </label><br>
                                <input type="text" class="datepicker" name="tahun" size="5" value="<?= date('Y'); ?>"/> 
                                 <a id="tampilsaldoawal" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                                 <a id="adddata_saldoawal" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Saldo Awal</a>
                           </div>
                           </form>
                         </div>
                         
                         <div class="box box-default box-solid">
                            <div class="box-header with-border">
                              <h5 class="text-bold">Pengaturan Saldo Awal</h5>
                            </div>
                            <div class="box-body">
                              <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                                <thead>
                                 <tr style="background:#edd38c;">                
                                  <th>Bulan</th>
                                  <th>Tahun</th>
                                  <th>Saldo Awal</th>
                                  <th></th>
                                </tr>
                                </thead>
                                <tbody id="dtsaldoawal">
                                </tfoot>
                              </table>
                            </div>
                        </div>   
                         
                         
                     </div>
                   </div>
                 </div>
                </div>
                
              
              
            </div>
          </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->