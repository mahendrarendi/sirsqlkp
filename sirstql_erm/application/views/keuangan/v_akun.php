<!-- Main content -->
<style>
    .table>tbody>tr>td{padding:6px;font-size: 13px;color:#575757;}
    .table-bordered>tbody>tr>td{border:0px;border-bottom: 1px solid #cacaca;}
    .table-striped>tbody>tr:nth-of-type(odd) {background-color: #f3f3f3d6;
    }
</style>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <input type="hidden" id="tahun" name="tahun" value="<?= $tahun; ?>" />
                <input type="hidden" id="bulan" name="bulan" value="<?= $bulan; ?>" />
                <div class="toolbar col-sm-6 row" style="margin-bottom: 4px;">
              <a id="add-tipe" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Tipe Akun </a>
              <a id="add-akun" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Akun </a>            
              &nbsp;
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
            <div id="tampildata"></div>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->