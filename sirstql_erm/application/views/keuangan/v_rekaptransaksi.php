<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-xs-12 col-md-8" style="padding-bottom:4px;">
                <div class="row">
                  <div class="col-md-3">
                    <label>Nama Kasir</label>
                    <select name="idkasir" id="idkasir" class="select2 form-control"></select>
                  </div>
                  <div class="col-md-3">
                    <label>Status</label>
                    <select name="status" id="status" class="select2 form-control">
                      <option value="">Pilih Status</option>
                      <option value="1">Rekap</option>
                      <option value="0">Belum Rekap</option>
                    </select>
                  </div>
                  <div class="col-md-2">
                    <label>Tanggal</label><br>
                    <input type="text" class="datepicker" size="13" name="tanggal" >
                  </div>
                  <div class="col-md-3">
                    <label>&nbsp;</label><br>
                    <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                    <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
                  </div>
                </div>
              </div>
              <table id="dtrekaptransaksi" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
               <tr style="background:#edd38c;">
                <th>WAKTU TAGIH</th>
                <th>NO.RM</th>
                <th>NAMA PASIEN</th>
                <th>KELAS</th>
                <th>CARA BAYAR</th>
                <th>PEMBAYARAN</th>
                <th>STATUS TAGIHAN</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->