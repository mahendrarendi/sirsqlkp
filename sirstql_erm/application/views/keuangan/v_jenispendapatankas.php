
<section class="content">
    
    <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
    <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
    &nbsp;
    <a id="tambah" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i> Tambah</a> 

  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <table id="dtdatatable" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
          <thead>
          <tr class="header-table-ql">
            <th width="20">No</th>
            <th>Jenis Pendapatan</th>
            <th width="120"></th>
          </tr>
          </thead>
          <tbody>
          </tfoot>
        </table>
        </div>
        <!-- /.box-body -->
      </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->