
<section class="content">
    
                
    <div class="col-md-2">
      <label>Nama Kasir</label>
      <select name="idkasir" id="idkasir" class="select2 form-control idkasir"></select>
    </div>
    <div class="col-md-1">
      <label>Awal</label><br>
      <input type="text" size="7" class="datepicker" name="tanggal" id="tanggal" >      
    </div>
    <div class="col-md-1">
      <label>Akhir</label><br>
      <input type="text" size="7" class="datepicker" name="tanggal2" id="tanggal2" >
    </div>
    <div class="col-md-6">
        <label>&nbsp;</label><br>
        <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
        <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
         | 
        <a id="tambah" class="btn btn-primary btn-sm" <?= ql_tooltip('Tambah Pendapatan'); ?>><i class="fa fa-plus-square"></i> Tambah</a>
        <a id="generate" class="btn btn-success btn-sm"><i class="fa fa-gear"></i> Generate</a>
        <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Detail Pembayaran EDC</a>
        <a id="unduhdetail" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Detail</a>
    </div>
    
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
<!--              <div class="toolbar col-md-6 row">
                    
              </div>-->
              <table id="dtpenerimaan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>Jenis Pendapatan</th>
                <th>Tanggal</th>
                <th>Nama Pendapatan</th>
                <th>Penyetor</th>
                <th>Keterangan</th>
                <th>Nominal</th>
                <th>Jenis Transaksi</th>
                <th>Shif</th>
                <th>Kasir</th>
                <th width="90"></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    
    <div class="row">
        <div class="col-md-12">
          <div class="box box-default box-solid">
            <!-- /.box-header -->
            <div class="box-header with-border">
                <h5 class="text-bold">Dashboard 1</h5>
           </div>
            <div class="box-body">
                <div class="col-sm-2">
                    <label>Nama Kasir</label>
                    <select name="idkasir" id="idkasir_dashboard" class="select2 form-control idkasir"></select>
                  </div>
                  <div class="col-sm-2">
                    <label>Tanggal</label><br>
                    <input type="text" class="datepicker form-control" name="tanggal" id="tanggal_dashboard" >
                  </div>
                  <div class="col-md-6">
                      <label>&nbsp;</label><br>
                      <a id="tampil_dashboard" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                      <a id="reload_dashboard" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                  </div>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr class="header-table-ql"><th>Shif</th><th>Nama Kasir</th><th>Jenis Transaksi</th><th>Nominal</th></tr>
                    </thead>
                    <tbody id="bodydashboard">
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
    </div>
    <!-- /.row -->
    
    <div class="row">
        <div class="col-xs-12">
          <div class="box box-default box-solid">
            <div class="box-header with-border">
                <h5 class="text-bold">Dashboard 2</h5>
           </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-sm-2">
                    <label>Nama Kasir</label>
                    <select name="idkasir" id="idkasir_dashboard2" class="select2 form-control idkasir"></select>
                  </div>
                  <div class="col-sm-2">
                    <label>Tanggal</label><br>
                    <input type="text" class="datepicker form-control" name="tanggal" id="tanggal_dashboard2" >
                  </div>
                  <div class="col-md-6">
                      <label>&nbsp;</label><br>
                      <a id="tampil_dashboard2" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                      <a id="reload_dashboard2" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                  </div>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr class="header-table-ql"><th>Jenis Pendapatan</th><th>Transfer</th><th>Tunai</th><th>JKN-PBI</th><th>JKN-NonPBI</th><th>Asuransi</th><th colspan="2"></th></tr>
                    </thead>
                    <tbody id="bodydashboard2">
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
  </section>
  <!-- /.content -->