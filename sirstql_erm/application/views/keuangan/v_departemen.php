<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6">
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah </a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="dtdepartemen" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr style="background:#edd38c;">
                      <th>No</th>
                      <th>Departemen</th>
                      <th>Keterangan</th>
                      <th>Dibuat</th>
                      <th width="60px"></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->