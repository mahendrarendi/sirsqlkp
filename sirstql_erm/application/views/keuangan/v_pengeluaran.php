

    
<section class="content">
      <div class="row">          
        <div class="col-xs-12">
            <a href="<?= base_url('ckeuangan/dashboard_pengeluaran') ?>" class="btn <?= (($active_menu_level == 'dashboard') ? 'bg-gray' : 'btn-default' ) ?> btn-sm"><i class="fa fa-dashboard"></i> Dashboard</a> 
            <a href="<?= base_url('ckeuangan/pengeluaran') ?>" class="btn <?= (($active_menu_level == 'pengeluaran') ? 'bg-gray' : 'btn-default' ) ?> btn-sm"><i class="fa fa-asterisk "></i> Pengeluaran</a> 
            <a href="<?= base_url('ckeuangan/kategoribiaya') ?>" class="btn <?= (($active_menu_level == 'kategoribiaya') ? 'bg-gray' : 'btn-default' ) ?>  btn-sm"><i class="fa fa-list"></i> Kategori</a> 
                
            <?php if($mode == 'dashboard'){  ?>
              <div class="box" style="margin-top:4px;">
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="col-xs-12 col-md-8" style="padding-bottom:4px;">
                    <div class="row">
                      <div class="col-md-2">
                        <label>Tanggal Awal</label><br>
                        <input type="text" class="datepicker" size="13" name="awal" >
                      </div>
                      <div class="col-md-2">
                        <label>Tanggal Akhir</label><br>
                        <input type="text" class="datepicker" size="13" name="akhir" >
                      </div>
                      <div class="col-md-6">
                        <label>&nbsp;</label><br>
                        <a onclick="listdashboard()" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                      </div>
                    </div>
                  </div>

                  <table id="dtdashboard" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                      <tr><th colspan="2"><h4>Total Pengeluaran</h4></th><th><h3 id="txtTotalPengeluaran"></h3></th></tr>
                   <tr style="background:#edd38c;">  
                    <th>No</th>
                    <th>Kategori</th>
                    <th>Total</th>
                  </tr>
                  </thead>
                  <tbody id="bodydashboard">
                  </tfoot>
                </table>
                </div>
                <!-- /.box-body -->
              </div>
            <?php } else if($mode == 'kategori'){  ?>
              <div class="box" style="margin-top:4px;">
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="col-xs-12 col-md-8" style="padding-bottom:4px;">
                    <div class="row">
                        <a id="reloadkategroi" class="btn btn-warning btn-sm"> <i class="fa fa-refresh"></i> Refresh</a>
                        <a id="tambahkategori" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah</a> 
                    </div>
                  </div>
                    
                  <table id="dtkategori" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                  <thead>
                   <tr style="background:#edd38c;">                
                    <th>Kategori</th>
                    <th width="100"></th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php
                        foreach ($dtkategori as $arr)
                        {
                            echo "<tr><td>".$arr['kategoribiaya']."</td><td><a id='ubahkategori' alt='".$arr['idkategoribiaya']."' alt2='".$arr['kategoribiaya']."'  class='btn btn-warning btn-xs'><i class='fa fa-edit'></i> ubah</a> <a id='hapuskategori' alt='".$arr['idkategoribiaya']."' alt2='".$arr['kategoribiaya']."' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> hapus</a></td></tr>";
                        }
                      ?>
                  </tfoot>
                </table>
                    
                </div>
                <!-- /.box-body -->
              </div>
          <?php }else if($mode =='view'){  ?>
          <div class="box" style="margin-top:4px;">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-xs-12 col-md-8" style="padding-bottom:4px;">
                <div class="row">
                    
                    
                  <div class="col-md-2">
                    <label>Tanggal Awal</label><br>
                    <input type="text" class="datepicker" size="13" name="awal" >
                  </div>
                  <div class="col-md-2">
                    <label>Tanggal Akhir</label><br>
                    <input type="text" class="datepicker" size="13" name="akhir" >
                  </div>
                  <div class="col-md-8">
                    <label>&nbsp;</label><br>
                    <a id="tampil" class="btn btn-info btn-sm"> <i class="fa fa-desktop"></i> Tampil</a>
                    <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
                    <a href="<?= base_url('ckeuangan/add_pengeluaran'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Pengeluaran</a> 
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Pengeluaran</a>
                    
                    <label style="cursor:pointer;">&nbsp;&nbsp;<input id="statusrekap" type="checkbox" name="statusjurnal" value="1" /> Belum Jurnal</label>
                    
                  </div>
                </div>
              </div>
                
                
              <table id="dtpengeluaran" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
               <tr style="background:#edd38c;">                
                <th>Kategori</th>
                <th>Tgl. Transaksi</th>
                <th>No.Transaksi/Nota</th>
                <th>Penjual</th>
                <th>Total Biaya</th>
                <th>Cara Bayar</th>
                <th>PPN</th>
                <th>Catatan</th>
                <th>Keterangan</th>
                <th>Pembeli</th>
                <th width="100"></th>
              </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
            </div>
            <!-- /.box-body -->
          </div>
          <?php }else if($mode =='add'){  ?>
            
            <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
            <form role="form" class="form-horizontal" id="formPengeluaran" method="post" action="<?= base_url('ckeuangan/save_pengeluaran') ?>" autocomplete="off">
                <br>
            <input type="hidden" name="idpengeluaran" id="idpengeluaran" value="<?= $this->uri->segment(3); ?>" />
            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label">Penjual <span class="asterisk">*</span></label>

              <div class="col-sm-3">
                  <select class="select2 form-control" name="idpenjual" id="idpenjual"><option value="">Pilih</option></select>
                  <a class="btn btn-xs btn-primary" onclick="formtambahpenjual()"><i class="fa fa-plus-square"></i> Tambah Penjual</a>
              </div>

              <label for="total_amount_txt" class="col-sm-2 control-label">Total </label>
              <div class="col-sm-4">
                <span id="total_amount_txt" class="control-label text-primary" style="font-size: 25px;font-weight: bold;">0</span>
                <input type="hidden" id="total_amount_input_txt" name="total" value="0">
              </div>
            </div>

            <div class="form-group">
              <label for="inv_date_txt" class="col-sm-2 control-label">Tanggal Beli</label>

              <div class="col-sm-3">
                  <input type="text" class="form-control datepicker" id="tanggalbeli" name="tanggalbeli" placeholder="yyyy-mm-dd" readonly="">
              </div>

              <label for="inv_no_txt" class="col-sm-2 control-label">No.Pembelian </label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="notransaksi"  name="notransaksi" maxlength="100" placeholder="" value="">
              </div>
            </div>

            <div class="form-group">
              <label for="memo_txt" class="col-sm-2 control-label">Catatan</label>
              <div class="col-sm-3"><textarea name="catatan" id="catatan" class="form-control" rows="3"></textarea></div>

              <label for="description_txt" class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-3"><textarea name="keterangan" id="keterangan" class="form-control" rows="3"></textarea></div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-2 control-label">Kategori</label>

              <div class="col-sm-3">
                <select class="select2 form-control" id="idkatgeori" name="idkatgeori">
                  <option value="">Pilih</option>
                  <?php
                    if(empty(!$kategori))
                    {
                        foreach ($kategori as $obj)
                        {
                            echo "<option value=".$obj->idkategoribiaya.">".$obj->kategoribiaya."</option>";
                        }
                    }
                  ?>
                </select>
              </div>
              
              <label  class="col-sm-2 control-label">Potongan</label>
              <div class="col-sm-3">
                  <input type="text" name="potongan" class="form-control" id="potongan" onkeyup="setrupiahpotongan()" />
              </div>
            </div>
                    
            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label">Pembeli</label>

              <div class="col-sm-3">
                <select class="select2 form-control" id="idpegawai" name="idpegawai">
                  <option value="">Pilih</option>
                </select>
              </div>

              <label  class="col-sm-2 control-label">PPN</label>
              <div class="col-sm-3" id="editppn">
                  <input type="radio" name="is_ppn" value="11" /> &nbsp;PPN 11%</input><br>
                  <input type="radio" name="is_ppn" value=""> &nbsp;Tanpa PPN</input>
              </div>

            </div>

            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label"></label>
              <div class="col-sm-3">
              </div>
              <label  class="col-sm-2 control-label"></label>
              <div class="col-sm-3">
              <input type="checkbox" name="istf" id="istf">&nbsp;<b>Transfer/EDC</b></input>

              </div>
            </div>

            <table id="tableFormDetail" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="80%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th width="280">Barang/Item</th>
                  <th>Jumlah</th>
                  <th>Satuan</th>
                  <th>Harga Satuan</th>
                  <!-- <th>Cara Bayar</th> -->
                  <th>PPN</th>
                  <th>Subtotal</th>
                  <th></th>
                </tr>
                </thead>
                <tbody id="bodyFormDetail">                    
                </tbody>
                <tfoot>
                    <tr class="bg bg-gray-light"><th colspan="5" >Total</th><th colspan="2" id="total_amount_footer"></th></tr>
                </tfoot>
              </table>
                    
                <a style="margin-left:28px;" href="javascript:void(0)" class="btn btn-success btn-md" id="tambahItem"> Tambah Item</a>


            <center>
            <div class="row" style="padding-bottom: 20px;">
              <a class="btn btn-primary btn-lg" id="simpanPengeluaran"> Simpan</a>
              <a class="btn btn-danger btn-lg" id="batalPengeluaran" > Batal</a>
            </div>
            </center>
          </form>
            </div>
            <!-- /.box-body -->
          </div>
          <?php } else{ ?>
            <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
            <form role="form" class="form-horizontal">
            <br>
            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label">Penjual <span class="asterisk">*</span></label>

              <div class="col-sm-3">
                  <?= $dtpengeluaran[0]['penjual']; ?>
              </div>

              <label for="total_amount_txt" class="col-sm-2 control-label">Total </label>
              <div class="col-sm-4">
                  <span id="total_amount_txt" class="control-label text-primary" style="font-size: 25px;font-weight: bold;"><?= convertToRupiah($dtpengeluaran[0]['total']); ?></span>
              </div>
            </div>

            <div class="form-group">
              <label for="inv_date_txt" class="col-sm-2 control-label">Tanggal Beli</label>

              <div class="col-sm-3">
                  <input type="text" class="form-control" value="<?= $dtpengeluaran[0]['tanggal']; ?>" disabled="" >
              </div>

              <label for="inv_no_txt" class="col-sm-2 control-label">No.Pembelian </label>
              <div class="col-sm-3">
                  <input type="text" class="form-control" value="<?= $dtpengeluaran[0]['notransaksi']; ?>" disabled="">
              </div>
            </div>

            <div class="form-group">
              <label for="memo_txt" class="col-sm-2 control-label">Catatan</label>
              <div class="col-sm-3"><textarea class="form-control" rows="3" disabled=""><?= $dtpengeluaran[0]['catatan']; ?></textarea></div>

              <label for="description_txt" class="col-sm-2 control-label">Keterangan</label>
              <div class="col-sm-3"><textarea  class="form-control" disabled="" rows="3"><?= $dtpengeluaran[0]['keterangan']; ?></textarea></div>
            </div>
            
            <div class="form-group">
              <label class="col-sm-2 control-label">Kategori</label>

              <div class="col-sm-3">
                  <input type="text" class="form-control" value="<?= $dtpengeluaran[0]['kategoribiaya']; ?>" disabled="">
              </div>
            </div>
                    
            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label">Pembeli</label>

              <div class="col-sm-3">
                  <input type="text" class="form-control" value="<?= $dtpengeluaran[0]['pembeli']; ?>" disabled="">
              </div>
              <label  class="col-sm-2 control-label">PPN</label>
              <div class="col-sm-3">
                  <?php 
                  $chck = ($dtpengeluaran[0]['ppn']) ? 'checked' : '' ;
                  ?>
                  <input type="radio" name="is_ppn" value="11" <?=$chck?> disabled/> &nbsp;PPN 11%</input><br>
                  <input type="radio" name="is_ppn" value="" disabled> &nbsp;Tanpa PPN</input>
              </div>

            </div>

            <div class="form-group">
              <label for="id_vendor_txt" class="col-sm-2 control-label"></label>
              <div class="col-sm-3">
              </div>
              <label  class="col-sm-2 control-label"></label>
              <div class="col-sm-3">
              <?php 
                  $cx = ($dtpengeluaran[0]['is_transfer']) ? 'checked' : '' ;
                  ?>
              <input type="checkbox" name="istf" id="istf" <?=$cx?> disabled>&nbsp;<b>Transfer/EDC</b></input>
              </div>
            </div>

            <table id="tableFormDetail" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="80%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th>Barang/Item</th>
                  <th>Jumlah</th>
                  <th>Satuan</th>
                  <th>Harga Satuan</th>
                  <th>PPN</th>
                  <!-- <th>Cara Bayar</th> -->
                  <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>      
                    <?php
                        $total = 0;
                        foreach ($dtdetail as $arr)
                        {
                            $total += $arr['subtotal'];
                            echo "<tr><td>".$arr['namaitem']."</td><td>".convertToRupiah($arr['jumlah'])."</td><td>".$arr['satuan']."</td><td>".convertToRupiah($arr['harga'])."</td><td>".convertToRupiah($arr['nilaippn'])."</td><td>".convertToRupiah($arr['subtotal'])."</td></tr>";
                        }
                        echo "<tr><th class='text-center' colspan='5'>Total</th><th>".convertToRupiah($total)."</th></tr>";
                    ?>
                </tbody>
              </table>


            <center>
            <div class="row" style="padding-bottom: 20px;">
              <a href="<?= base_url('ckeuangan/pengeluaran') ?>" class="btn btn-warning btn-lg" > Kembali</a>
            </div>
            </center>
          </form>
            </div>
            <!-- /.box-body -->
          </div>
          <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->