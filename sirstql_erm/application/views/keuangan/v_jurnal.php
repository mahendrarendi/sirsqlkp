<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-4">
                  <label>Tanggal:</label><br>
              <input type="text" class="datepicker" name="tanggal" id="tanggal" size="8" autocomplete="off" />
              <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
              &nbsp;
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Jurnal Baru </a>
            </div>
            <div class="col-sm-2">
                <label>Referensi:</label><br>
                <select id="referensi" name="referensi" class="form-control">
                    <option value="Pengeluaran">Pengeluaran</option>
                    <option value="Penjualan">Penjualan</option>
                </select>
            </div>
              <!-- <div id="tampildata"></div> -->
              <table id="dtjurnal" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr style="background:#edd38c;">
                      <th>Tanggal</th>
                      <th>Keterangan</th>                      
                      <th>Debet</th>
                      <th>Kredit</th>
                      <th>Ref</th>
                      <th>No Referensi</th>
                      <th>Departemen</th>
                      <th>Status</th>
                      <th width="60px"></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->