<!-- Main content -->
<style>
    .table>tbody>tr>td{padding:6px;font-size: 13px;color:#575757;}
    .table-bordered>tbody>tr>td{border:0px;border-bottom: 1px solid #cacaca;}
    .table-striped>tbody>tr:nth-of-type(odd) {background-color: #f3f3f3d6;
    }
    .col-md-2 {
        width: 10.66666667%;
    }
</style>
<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-12">
                <div class="row">
                  <div class="col-md-2">
                    <label>Dari</label>
                    <input name="tanggal_mulai" id="tanggal_mulai" class="form-control" type="text" readonly="readonly"/>
                  </div>
                  <div class="col-md-2">
                    <label>Sampai</label>
                    <input name="tanggal_selesai" id="tanggal_selesai" class="form-control" type="text" readonly="readonly"/>
                  </div>

                  <div class="col-md-4">
                    <label>&nbsp;</label><br>
                    <a id="reload" class="btn btn-primary btn-sm"><i class="fa fa-desktop"></i> Tampil Data</a> 
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Excel</a>
                  </div>
                </div>
            </div>
            <div class="col-sm-12">&nbsp;
              <p style="text-align:center;font-size:25px;">Laporan Laba Rugi</p>
              <p style="text-align:center;font-size:18px;display:none;" id="periode">Periode  <span id="periode_awal"></span> s/d <span id="periode_akhir"></span> </p>
            </div>
            <table id="tabel_laporanralabarugi" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr style="background:#edd38c;">
                  <th colspan="3">Akun</th>
                  <th>Umum</th>
                  <th>Bpjs</th>
                  <th>Jaminan</th>
                  <th>Saldo</th>
                  <th>Saldo Tahun Berjalan</th>
                </tr>
              </thead>
              <tbody id="tampildata">
              </tbody>
            </table>
          </div>
            <!-- /.box-body -->
        </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->