<div id="infopanggilan"></div> <!-- untuk diisi ketika pemanggilan pemberitauan order rm, ugd dll-->

<footer class="main-footer" style="font-size: 12.5px;">
    <div class="pull-right hidden-xs">
      <b>Version</b> <?php echo APP_VERSION; ?> || Page rendered in <strong>{elapsed_time}</strong> seconds
    </div><?= COPY_RIGHT; ?>  
</footer>
<!--set baseu url untuk digunakan di javascript-->
<script> var base_url ='<?= base_url(); ?>';</script>

<!--set session, sesuai keperluan-->
<script>sessionStorage.panggilSemuaAntrian='<?= $this->pageaccessrightbap->checkAccessRight(V_PEMANGGILANANSEMUATRIAN); ?>';</script>
<script>sessionStorage.user='<?= $this->session->userdata('username'); ?>';</script>
<script>sessionStorage.pesannotiffarmasi='<?= $this->pageaccessrightbap->checkAccessRight(NOTIF_FARMASI); ?>';</script>
<script>sessionStorage.pesanorderugd='<?= $this->pageaccessrightbap->checkAccessRight(NOTIF_ORDER_UGD); ?>';</script>
<script>sessionStorage.pesanorderrm='<?= $this->pageaccessrightbap->checkAccessRight(NOTIF_ORDER_RM); ?>';</script>
<script>sessionStorage.farmasigenerateantrian='<?= $this->pageaccessrightbap->checkAccessRight(V_FARMASIGENERATEANTRIAN); ?>';</script>
<script>sessionStorage.belanjabarang='<?= $this->pageaccessrightbap->checkAccessRight(V_BELANJABARANG); ?>';</script>
<script>sessionStorage.unitterpilih='<?= $this->session->userdata('unitterpilih'); ?>';</script>
<script>sessionStorage.idunitterpilih='<?= $this->session->userdata('idunitterpilih'); ?>';</script>
<script>sessionStorage.v_pemeriksaandokter='<?= $this->pageaccessrightbap->checkAccessRight(V_PEMERIKSAANDOKTER); ?>';</script>
<script>sessionStorage.v_menukasir='<?= $this->pageaccessrightbap->checkAccessRight(V_MENUKASIR); ?>';</script>
<script>sessionStorage.v_inputdatasubyektifralan='<?= $this->pageaccessrightbap->checkAccessRight(V_INPUTDATASUBYEKTIFRALAN); ?>';</script>
<script>sessionStorage.akses_salinriwayatperiksaralan='<?= $this->pageaccessrightbap->checkAccessRight(AKSES_SALINRIWAYATPERIKSARALAN); ?>';</script>

