    <section class="content">
      <br>
      <div class="row" style="margin-bottom: 5px;">
      <div class="col-xs-4">
          <label>Nama Poli</label>
        <select id="tampilpoli" name="unit" class="form-control select2" style="margin-right:4px;"></select> 
      </div>
      
      <div class="col-md-1">
          <label>Tahun</label>
          <input type="text" id="tahun" class="form-control" />

      </div>
      <div class="col-md-6">
        <label>&nbsp;</label><br>
        <a class="btn btn-primary" id="tampilcakupanpoli"><i class="fa fa-bar-chart"></i> Cakupan Poli</a>
        <a class="btn btn-info" onclick="tampildiahnosa10besar()"><i class="fa fa-desktop"></i> Diagnosa/Tindakan 10 Besar</a>
        <a class="btn btn-warning" id="unduhcakupanpoli"><i class="fa fa-file-excel-o"></i> Unduh Data</a>

      </div>
      <!-- <div class="col-xs-8">&bnsp;</div> -->
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-4" >
          <div class="box" id="cakupan">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:13px;">CAKUPAN</h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="cakupanPoli" style="height:250px"></canvas>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-4">
          <div class="box" id="jaminan">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:13px;">PROPORSI JAMINAN PASIEN</h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="proporsiJaminan" style="height:250px"></canvas>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-4">
          <div class="box" id="pasien">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:13px;">PROPORSI PASIEN LAMA BARU</h3>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="proporsiLamaBaru" style="height:250px"></canvas>
              </div>
            </div>
          </div>

        </div>

    <div class="col-xs-12" >
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:13px;" id="judulicd10"><b>10 DIAGNOSIS TERBANYAK</b></h3> 
            </div>
            <div class="box-body" style="overflow-x: scroll;overflow-y: hidden; padding-bottom:10px;" id="diagnosa10besar"><span style="font-size: 11px;padding-left: 20px; text-align: center;">DATA BELUM DITAMPILKAN..!</span></div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title" style="font-size:13px;" id="judulicd9"><b>10 TINDAKAN TERBANYAK</b></h3>
            </div>
            <div class="box-body" style="overflow-x: scroll;overflow-y: hidden; padding-bottom:10px;" id="tindakan10besar"><span style="font-size: 11px;padding-left: 20px; text-align: center;">DATA BELUM DITAMPILKAN..!</span></div>
        </div>
    </div>
          
    <?php if($this->pageaccessrightbap->checkAccessRight(V_BERANDA_TINDAKANRAWATJALAN)){  ?>      
    <div class="col-xs-12">
        <div class="row" style="margin-bottom:4px;">
            <div class="col-md-3">
              <input type="text" class="datepicker" id="tahunbulan" size="7" value="2021" readonly="">
              <a id="tampiltindakanall" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
              <a id="unduhtindakanall" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
            </div>
        </div>
        
        <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title text-bold" style="font-size:13px;" id="judultindakanall">TINDAKAN RAWAT JALAN</h3>
            </div>
            <div class="box-body" style="padding-bottom:10px;" id="tindakanall"><span style="font-size: 11px;padding-left: 20px; text-align: center;">DATA BELUM DITAMPILKAN..!</span></div>
        </div>
    </div>
    <?php } ?>
</div>









