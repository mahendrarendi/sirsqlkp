<style>
    .select2-container {
        width: 100% !important;
    }
</style>

<section class="content">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#datamaster">Data Master</a></li>
        <li><a data-toggle="tab" href="#tautansdkislki">Tautan SDKI - SIKI</a></li>
    </ul>

    <div class="tab-content">
        <div id="datamaster" class="tab-pane fade active in">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Tambah Data</strong></div>
                        <div class="panel-body">
                            <form action="<?= base_url('cmasterdata/tambah_siki') ?>" method="POST">
                                <div class="form-group">
                                    <label>Kode SIKI</label>
                                    <input type="text" class="form-control" name="kodesiki">
                                </div>
                                <div class="form-group">
                                    <label>Nama SIKI</label>
                                    <input type="text" class="form-control" name="namasiki">
                                </div>
                                <div class="form-group">
                                    <label>Definisi</label>
                                    <textarea class="form-control" name="definisi"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Observasi</label>
                                    <textarea rows="5" class="form-control" name="observasi"></textarea>
                                    <div class="keterangan-catatan"><small style="color:red;"> Catatan: Jika lebih dari satu Silahkan pisahkan menggunakan tanda <b style="color:black;">|</b> </small></div>
                                </div>
                                <div class="form-group">
                                    <label>Terapeutik</label>
                                    <textarea rows="5" class="form-control" name="terapeutik"></textarea>
                                    <div class="keterangan-catatan"><small style="color:red;"> Catatan: Jika lebih dari satu Silahkan pisahkan menggunakan tanda <b style="color:black;">|</b> </small></div>

                                </div>
                                <div class="form-group">
                                    <label>Edukasi</label>
                                    <textarea rows="5" class="form-control" name="edukasi"></textarea>
                                    <div class="keterangan-catatan"><small style="color:red;"> Catatan: Jika lebih dari satu Silahkan pisahkan menggunakan tanda <b style="color:black;">|</b> </small></div>

                                </div>
                                <div class="form-group">
                                    <label>Kolaborasi</label>
                                    <textarea rows="5" class="form-control" name="koloborasi"></textarea>
                                    <div class="keterangan-catatan"><small style="color:red;"> Catatan: Jika lebih dari satu Silahkan pisahkan menggunakan tanda <b style="color:black;">|</b> </small></div>

                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-md">SIMPAN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Data SIKI</strong></div>
                        <div class="panel-body">
                            <table style="width : 100%;" class="table table-bordered table-hovered dt-siki">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th>Nomor</th>
                                        <th>Kode SIKI</th>
                                        <th>Nama SIKI</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($datasiki as $item) {
                                        echo "<tr>";
                                        echo "<td>" . $i++ . "</td>";
                                        echo "<td>" . $item['kode_siki'] . "</td>";
                                        echo "<td>" . $item['nama_siki'] . "</td>";
                                        echo "<td class='text-center'>
                                                <button type='button' class='btn btn-primary editsiki-modal' data-id='" . $item['id_siki'] . "' style='border-radius:100%;'><i class='fa fa-edit'></i></button>
                                                <button type='button' data-toggle='modal' data-target='#modalSIKI' class='btn btn-danger btn-primary btnlihatdata' data-toggle='tooltip' title='Lihat Detail Data' idsiki='" . $item['id_siki'] . "' style='border-radius: 150px;'><i class='fa fa-search'></i> </button></td>";
                                        echo "</tr>";
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tautansdkislki" class="tab-pane fade">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Tautan SDKI-SIKI</strong></div>
                        <div class="panel-body">
                            <form action="<?= base_url('cmasterdata/save_tautan_sdki_siki') ?>" method="POST">
                                <div class="form-group">
                                    <label>Pilih SDKI</label>
                                    <select id="" class="form-control idsdki" name="idsdki"></select>
                                </div>
                                <div class="form-group">
                                    <label>Intervensi Utama</label>
                                    <select class="form-control idiki" name="intervensiutama[]"></select>
                                    <div class="divintervensiutama"></div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-light btn-md btntambahintervensiutama"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Intervensi Pendukung</label>
                                    <select class="form-control idiki" name="intervensipendukung[]"></select>
                                    <div class="divintervensipendukung">
                                    </div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-light btn-md btntambahintervensipendukung"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-md">SIMPAN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Data Tautan SDKI-SLKI</strong></div>
                        <div class="panel-body">
                            <table style="width : 100%;" class="table table-bordered table-hover table-striped dt-siki">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th style="width : 38px;">Nomor</th>
                                        <th style="width : 38px;">Kode SDKI</th>
                                        <th>Nama SDKI</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($tautanslkisdki as $item) {
                                        echo "<tr>";
                                        echo "<td>" . $i++ . "</td>";
                                        echo "<td>" . $item['kode_childkategori'] . "</td>";
                                        echo "<td>" . $item['nama_childkategori'] . "</td>";
                                        echo "<td class='text-center'><button type='button' data-toggle='modal' data-target='#modalSIKI' class='btn btn-danger btn-primary btnlihatdatatautansdkisiki' data-toggle='tooltip' title='Lihat Detail Data' idtautansdkisiki='" . $item['id_tautan_sdki_siki'] . "' style='border-radius: 150px;'><i class='fa fa-search'></i> </button></td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="modalSIKI" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius : 14px;">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>