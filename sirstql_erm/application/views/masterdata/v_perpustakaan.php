<!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if($jsmode=='buku'){ ?>
        <div class="box">
          
        <div class="box-body" id="listbuku">
          <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Judul</th>
                <th>Kode</th>
                <th>Jumlah</th>
                <th>ISBN</th>
                <th>Penulis</th>
                <th>Penerbit</th>
                <th>Kategori</th>
                <th></th>
              </tr>
              </thead>
              <tbody></tfoot>
              
            </table>
          </div>
          </div>
        
        <?php }else if($jsmode=='kategori'){ ?>
        <div class="box">
          
        <div class="box-body" id="listkategori"> 
          <table id="example" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Kategori</th>
                <th></th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          </div>
        
        <?php }else if($jsmode=='penulis'){ ?>
        <div class="box">
          
        <div class="box-body" id="listpenulis">
          <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Penulis</th>
                <th></th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          </div>
        
        <?php }else if($jsmode=='penerbit'){ ?>
        <div class="box">
          
        <div class="box-body" id="listpenerbit">
          <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Penerbit</th>
                <th></th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          </div>

        <!-- start mode add or edit -->
        <?php }else if( $jsmode=='editbuku' || $jsmode=='addbuku'){?>
        <div class="box">
          <div class="box-header">
            <center>
              <br>
            <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
            </center>
            <br>
            
          </div>
        <!-- /.box-header -->
        <div class="box-body">
        </div>
        <!-- /.box-body -->
      </div>
      <?php } ?>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->