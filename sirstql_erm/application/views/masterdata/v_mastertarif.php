
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            <div class="box-body">
                <div class="col-md-6 row">
                    <a href="<?= base_url() ?>cmasterdata/add_mastertarif" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
                    <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                    <a onclick="downloadExcel('<?php echo $modeTarif ?>')" class=" btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Download</a>
                </div>
              <table id="example" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th>ICD</th>
                  <th>Layanan</th>
                  <th>Kelas</th>
                  <th>JenisTarif</th>
                  <th>Tipe COA</th>
                  <th>COA</th>
                  <th>Operator</th>
                  <th>Dokter</th>
                  <th>RS</th>
                  <th>Bhp</th>
                  <th>Akomodasi</th>
                  <th>Margin</th>
                  <th>Sewa</th>
                  <th>Total</th>
                  <th style="width:110px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
          </div>
        <?php }else if( $mode=='edit' || $mode=='add' || $mode=='salin'){?>
        <div class="box">
            <div class="box-header">
                <center>
                    <h2 class="box-title"></h2>
                </center>
            </div>
          <!-- /.box-header -->
          <div class="box-body">
              <form action="<?= base_url('cmasterdata/save_mastertarif');?>" method="POST" class="form-horizontal" id="Formmastertarif">
              <input type="hidden" name="modetarif" value="<?= $modetarif; ?>" />
              <input type="hidden" name="idmastertarif" placeholder="idmastertarif"  value="<?= (!empty($data_edit) && ($mode!='salin')) ? $this->encryptbap->encrypt_urlsafe($data_edit["idmastertarif"], "json") : '' ; ?>">

            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kelas</label>
                <div class="col-sm-6">
                    <select class="select2 form-control" style="width:100%" id="idkelas" name="idkelas">
                        <option value="0">Pilih</option>
                        <?php
                          foreach($data_list1 as $obj) 
                          {
                            echo '<option '.dropdown_selected($data_edit, $obj->idkelas, 'idkelas').' value="'.$obj->idkelas.'">'.$obj->kelas.'</option>';
                          }
                        ?>
                    </select>
                </div>
            </div>
              
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">ICD/Layanan</label>
                <div class="col-sm-6">
                    <select class="select2 form-control" style="width:100%" id="icd" name="icd">
                        <option value="0">Pilih</option>
                        <?php
                            if(!empty($icd) && $data_edit['icd']!='')
                            {
                                foreach ($icd as $obj)
                                {
                                    echo '<option value="'.$data_edit['icd'].'" '.dropdown_selected($data_edit,$obj->icd,'icd').' > '.$data_edit['icd'].' | '.$obj->namaicd.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
            </div>
              
            <div class="form-group">
                <label for="_name_txt" class="col-sm-3 control-label"> Is Boleh Edit <span class="asterisk">*</span></label>
                <div class="col-sm-6">
                    <select name="isbolehedit" class="form-control" style="width:100%;" id="isbolehedit">
                        <option value="0" <?= dropdown_selected($data_edit, 0, 'isbolehedit'); ?> >Tarif TIDAK Boleh Diubah di Pemeriksaan</option>
                        <option value="1" <?= dropdown_selected($data_edit, 1, 'isbolehedit'); ?> >Tarif Boleh Diubah di Pemeriksaan</option>
                    </select>
                </div>
            </div>
              
            <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jenis Tarif</label>
                <div class="col-sm-6">
                    <select class="select2 form-control" style="width:100%" id="idjenistarif" name="idjenistarif">
                        <option value="0">Pilih</option>
                        <?php
                            foreach ($data_list3 as $obj) 
                            {
                              echo '<option '.  dropdown_selected($data_edit, $obj->idjenistarif, 'idjenistarif').' value="'.$obj->idjenistarif.'">'.$obj->jenistarif.'</option>';
                            }
                        ?>
                    </select>
                </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Tipe COA</label>
                <div class="col-sm-6">
                    <select class="select2 form-control" style="width:100%" id="kodetipeakun" name="kodetipeakun">
                        <option value="0">Pilih</option>
                        <?php 
                            if(!empty($tipeakun) && $data_edit['tipecoapendapatan']!='')
                            {
                                foreach ($tipeakun as $obj)
                                {
                                    echo '<option value="'.$data_edit['tipecoapendapatan'].'"'.  dropdown_selected($data_edit,$obj->kode,'tipecoapendapatan').'> '.$data_edit['tipecoapendapatan'].' | '.$obj->nama.'</option>';
                                }
                            }
                        ?>
                  </select>
                </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">COA</label>
                <div class="col-sm-6">
                    <select class="select2 form-control" style="width:100%" id="kodeakun" name="kodeakun">
                        <option value="0">Pilih</option>
                        <?php 
                            if(!empty($akun) && $data_edit['coapendapatan']!='')
                            {
                                foreach ($akun as $obj)
                                {
                                    echo '<option value="'.$data_edit['coapendapatan'].'"'. dropdown_selected($data_edit,$obj->kode,'coapendapatan').'> '.$data_edit['coapendapatan'].' | '.$obj->nama.'</option>';
                                }
                            }
                        ?>
                    </select>
                </div>
                <a class="btn btn-warning btn-sm" onclick="hapusakunpendapatan()" <?= ql_tooltip('Hapus COA Pendapatan'); ?>><i class="fa fa-minus-circle"></i></a>
              </div>
              
               <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jasa Operator*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text"  class="form-control" name="jasaoperator" value="<?= (!empty($data_edit)) ? $data_edit['jasaoperator'] : '' ; ?>" placeholder="jasaoperator"></div>
              </div>   
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Dokter*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="nakes" value="<?= (!empty($data_edit)) ? $data_edit['nakes'] : '' ; ?>" placeholder="nakes"></div>
              </div>   
              
                <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jasa RS*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="jasars" value="<?= (!empty($data_edit)) ? $data_edit['jasars'] : '' ; ?>" placeholder="jasars"></div>
              </div>  
              
               <div class="form-group">
                <label for="" class="col-sm-3 control-label">BHP*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="bhp" value="<?= (!empty($data_edit)) ? $data_edit['bhp'] : '' ; ?>" placeholder="bhp"></div>
              </div>  
              
               <div class="form-group">
                <label for="" class="col-sm-3 control-label">AKOMODASI*</label>
                <div class="col-sm-6"><input onkeypress="converToRupiahInput('akomodasi')" onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="akomodasi" value="<?= (!empty($data_edit)) ? $data_edit['akomodasi'] : '' ; ?>" placeholder="akomodasi"></div>
              </div>  
              
               <div class="form-group">
                <label for="" class="col-sm-3 control-label">MARGIN*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="margin" value="<?= (!empty($data_edit)) ? $data_edit['margin'] : '' ; ?>" placeholder="margin"></div>
              </div> 
              
               <div class="form-group">
                <label for="" class="col-sm-3 control-label">SEWA*</label>
                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="sewa" value="<?= (!empty($data_edit)) ? $data_edit['sewa'] : '' ; ?>" placeholder="sewa"></div>
              </div> 
              
                <div class="form-group">
                    <label for="" class="col-sm-3 control-label">TOTAL*</label>
                    <label for="" class="col-sm-6 control-label"><input id="totaltarif" disabled class="form-control"  value="<?= (!empty($data_edit)) ? $data_edit['total'] : '' ; ?>" placeholder="[Auto]"></label>
                </div> 
              
              <br>
              <center>
                <a class="btn btn-primary btn-lg" onclick="simpan_mastertarif()">SAVE</a>
                <a class="btn btn-warning btn-lg" href="<?= base_url('cmasterdata/'.$active_menu_level); ?>" >Back</a>
              </div>
              </center>
            </form>      
          </div>
        <?php } ?>
      </div>
      <!-- /.col -->
</div>
    <!-- /.row -->
  </section>
  <!-- /.content -->