
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar">
              <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
              <a href="<?php echo base_url('cmasterdata/add_golongan'); ?>" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data Golongan</a>
              <a style="margin-left: 18px;" href="<?php echo base_url('cmasterdata/barang'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Barang</a>
              <a href="<?php echo base_url('cmasterdata/sediaan'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Sediaan</a>
              <a href="<?php echo base_url('cmasterdata/satuan'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Satuan</a>
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm pull-right"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th style="width:60px;">No</th>
                  <th>Jenis</th>
                  <th>Golongan</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA GOLONGAN -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idgolongan, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('rs_barang_golongan', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_GOLONGAN, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->jenis.'</td>
                            <td>'.$obj->golongan.'</td>   
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Golongan" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_golongan/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Golongan" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA GOLONGAN -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT GOLONGAN-->
            <?php 
            echo form_open('cmasterdata/save_golongan', 'class="form-horizontal" id="FormGolongan"'); 
            $this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'idgolongan','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idgolongan'],'json') :'']);
            styleformgrup1kolom('jenis',form_input(['name'=>'jenis', 'type'=>'text', 'class'=>'form-control','id'=>'jenis','value'=>empty(!$data_edit) ? $data_edit['jenis'] :'']));
            styleformgrup1kolom('golongan',form_input(['name'=>'golongan', 'type'=>'text', 'class'=>'form-control','id'=>'golongan','value'=>empty(!$data_edit) ? $data_edit['golongan'] :'']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_golongan()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/golongan').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->