<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6 row">
              <a id="addinventaris" href="#" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Inventaris</a>
              <a id="reload" href="#" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
              <a id="unduh" href="#" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a> 
            </div>
              <table id="dtinventaris" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                      <th>Nama Alat</th>
                      <th>Merek</th>
                      <th>Tipe</th>
                      <th>No.Seri</th>
                      <th>Ruangan</th>
                      <th>No.Inventaris</th>
                      <th>No.Sertifikat</th>
                      <th>Tanggal Pembelian</th>
                      <th>Nilai Aset</th>
                      <th>Penyusutan</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->