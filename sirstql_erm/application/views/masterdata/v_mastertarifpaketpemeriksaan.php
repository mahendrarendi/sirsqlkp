<!--Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
          <div class="col-md-6 row">
            <a href="<?php echo base_url('cmasterdata/add_mastertarifpaketpemeriksaan'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
            <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
            <a onclick="downloadExcel('')" class=" btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download</a>
          </div>
            <table id="dttable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr class="header-table-ql">
                      <th>Nama Paket</th>
                      <th>Jenis ICD</th>
                      <th>Kelas</th>
                      <th>JenisTarif</th>
                      <th>COA</th>
                      <th>Operator</th>
                      <th>Nakes</th>
                      <th>Jasars</th>
                      <th>Bhp</th>
                      <th>Akomodasi</th>
                      <th>Margin</th>
                      <th>Sewa</th>
                      <th>Total</th>
                      <th>Asuransi</th>
                      <th width="50px">Aksi</th>
                  </tr>
              </thead>
              <tbody>
              <!-- END TAMPIL DATA -->
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
            </br>
            <form action="" class="form-horizontal" id="FormTarifPaketPemeriksaan" method="post" accept-charset="utf-8">
                <div class="col-xs-12">
                    <div class="box box-default box-solid">
                        <div class="box-header with-border">
                          <h3 class="box-title">Tarif Paket Pemeriksaan</h3>

                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body form-horizontal">
                            <input type="hidden" name="idmastertarifpaketpemeriksaan" value="<?= (($idmastertarifpaketpemeriksaan)? $idmastertarifpaketpemeriksaan :'') ?>" />
                            <div class="form-group">
                                <label for="_name_txt" class="col-sm-3 control-label"> Paket Pemeriksaan <span class="asterisk">*</span></label>
                                <div class="col-sm-6"><select id="idpaketpemeriksaan" name="idpaketpemeriksaan" class="select form-control"><option value="0">Pilih</option></select></div>
                            </div>    

                            <div class="form-group">
                                <label for="_name_txt" class="col-sm-3 control-label"> Kelas <span class="asterisk">*</span></label>
                                <div class="col-sm-6"><select id="idkelas" name="idkelas" class="form-control"><option value="0">Pilih</option></select></div>
                            </div>

                            <div class="form-group">
                                <label for="_name_txt" class="col-sm-3 control-label"> Is Boleh Edit <span class="asterisk">*</span></label>
                                <div class="col-sm-6"><select name="isbolehedit" class="form-control" style="width:100%;" id="isbolehedit">
                                    <option value="0">Tarif TIDAK Boleh Diubah di Pemeriksaan</option>
                                    <option value="1">Tarif Boleh Diubah di Pemeriksaan</option>
                                    </select>
                                </div>
                            </div>                 
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jenis Tarif</label>
                                <div class="col-sm-6"><select class="select form-control" style="width:100%" id="idjenistarif"  name="idjenistarif" ><option value="0">Pilih</option></select></div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">COA Pendapatan</label>
                                <div class="col-sm-6"><select class="select form-control" style="width:100%" id="coapendapatan"  name="coapendapatan" ><option value="0">Pilih</option></select></div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jasa Operator*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="jasaoperator" value="" placeholder="jasaoperator"/></div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Nakes*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="nakes" value="" placeholder="nakes"/></div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Jasa RS*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="jasars" value="" placeholder="jasars"/></div>
                            </div>  

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Bhp*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="bhp" value="" placeholder="bhp"/></div>
                            </div>  

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Akomodasi*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="akomodasi" value="" placeholder="akomodasi"/></div>
                            </div>  

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Margin*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="margin" value="" placeholder="margin"/></div>
                            </div> 

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Sewa*</label>
                                <div class="col-sm-6"><input onkeyup="sum_tarifrs(this.value)" type="text" class="form-control" name="sewa" value="" placeholder="sewa"/></div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label">Total*</label>
                                <div class="col-xs-12 col-md-6"><input id="totaltarif" disabled class="form-control" name="total" value="" placeholder="Total"/></div>
                            </div> 
                        </div>
                    </div>
                </div>
                
                <center style="padding: 8px 0px">
                <div class="row">
                  <a class="btn btn-primary btn-lg" onclick="menuSimpanPaket()">Simpan</a>
                  <a class="btn btn-danger btn-lg" onclick="menuKembali()">Kembali</a>
                </div>
                </center>
            </form>
        <!-- /.box-header -->
      <!--</div>-->
      <?php } ?>
    </div>
    <!-- /.col -->
  </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content