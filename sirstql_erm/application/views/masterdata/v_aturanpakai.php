
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6 row">
              <a href="<?php echo base_url('cmasterdata/add_aturanpakai'); ?>" class="btn btn-default btn-sm"><i class="fa  fa-plus-square"></i> Add Aturan Pakai</a>
              <a href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th style="width:60px;">No</th>
                  <th>Signa</th>
                  <th>Aturan Pakai</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA GOLONGAN -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idbarangaturanpakai, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('rs_barang_aturanpakai', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_ATURANPAKAI, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->signa.'</td>
                            <td>'.$obj->aturanpakai.'</td>  
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Aturanpakai" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_aturanpakai/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Aturanpakai" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA GOLONGAN -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT GOLONGAN-->
            <?php 
            echo form_open('cmasterdata/save_aturanpakai', 'class="form-horizontal" id="FormAturanpakai"'); 
            $this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'idaturanpakai','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idbarangaturanpakai'],'json') :'']);
            styleformgrup1kolom('Signa',form_input(['name'=>'signa', 'type'=>'text', 'class'=>'form-control','id'=>'signa','placeholder'=>'Signa','value'=>empty(!$data_edit) ? $data_edit['signa'] :'']));
            // pemakaian
            $pemakaian_edit='';
              empty($data_edit) ? $pemakaian_edit='' :  $pemakaian_edit =  $data_edit['pemakaian'];
            if(!empty($pemakaian)) { $loop_pemakaian=[]; foreach ($pemakaian as $key => $value){$loop_pemakaian += [$pemakaian[$key]=>$pemakaian[$key]];} } 
              styleformgrup1kolom('Pemakaian', form_dropdown(['onchange'=>'setKeterangan()','name'=>'pemakaian','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'pemakaian'],$loop_pemakaian,$pemakaian_edit));
            // waktu pemakaian
            $waktupemakaian_edit='';
              empty($data_edit) ? $waktupemakaian_edit='' :  $waktupemakaian_edit =  $data_edit['waktupemakaian'];
            if(!empty($waktupemakaian)) { $loop_waktupemakaian=[]; foreach ($waktupemakaian as $key => $value){$loop_waktupemakaian += [$waktupemakaian[$key]=>$waktupemakaian[$key]];} } 
              styleformgrup1kolom('Waktu Pemakaian', form_dropdown(['onchange'=>'setKeterangan()','name'=>'waktupakai','class'=>'form-control','style'=>'width:100%;', 'id'=>'waktupemakaian'],$loop_waktupemakaian,$waktupemakaian_edit));
            styleformgrup1kolom('Kali Sehari',form_input(['onchange'=>'setKeterangan()','name'=>'kalisehari', 'type'=>'text','placeholder'=>'Jumlah pakai', 'class'=>'form-control','id'=>'kalisehari','value'=>empty(!$data_edit) ? $data_edit['kalisehari'] :'']));
            styleformgrup1kolom('Jumlah',form_input(['onchange'=>'setKeterangan()','name'=>'jumlah', 'type'=>'text','placeholder'=>'Aturan pakai', 'class'=>'form-control','id'=>'jumlah','value'=>empty(!$data_edit) ? $data_edit['jumlah'] :'']));
            styleformgrup1kolom('Keterangan',form_textarea(['disabled'=>'disabled','rows'=>'2', 'class'=>'form-control','id'=>'keterangan']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_aturanpakai()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/aturanpakai').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->