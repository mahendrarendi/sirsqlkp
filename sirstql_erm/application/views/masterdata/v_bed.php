<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <!-- mode view -->
       <?php if( $mode=='view'){ ?>
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
           <div class="col-md-6 row">
           <a href="<?php echo base_url('cmasterdata/add_bed'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Bed</a>
           <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
         </div>
           <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
             <tr class="header-table-ql">
               <th style="width:60px;">No</th>
               <th>Nama Bangsal</th>
               <th>No Bed</th>
               <th>Status Bed</th>  
               <th style="width:170px;">Aksi</th>
             </tr>
             </thead>
             <tbody>
            <!-- START TAMPIL DATA BED -->
             <?php
             if (!empty($data_list))
             {
               $no=0;
               foreach ($data_list as $obj) 
               {
                 $this->encryptbap->generatekey_once("HIDDENTABEL");
                 $id    =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idbed));
                 $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('rs_bed'));
                 $idhalaman = $this->encryptbap->encrypt_urlsafe(V_BED, "json");
                 echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                 echo '  <td>'.$no.'</td>
                         <td>'.$obj->namabangsal.'</td>
                         <td>'.$obj->nobed.'</td>
                         <td>'.$obj->statusbed.'</td> 
                         <td>
                           <a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_bed/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                           <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                          <i class="fa fa-trash"></i> Delete</a></td>
                       </tr>';
               }
             }
             ?>
             <!-- END TAMPIL DATA BED -->
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
       <!-- start mode add or edit -->
       <?php }else if( $mode=='edit' || $mode=='add'){?>
       <div class="box">
         <div class="box-header">
           <center>
             <br>
           <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
           </center>
           <br>
         </div>
         <!-- /.box-header -->
         <div class="box-body">
             <form action="<?= base_url('cmasterdata/save_bed'); ?>" class="form-horizontal" id="Formbed" method="post" accept-charset="utf-8">
                <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                <input type="hidden" name="idbed" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idbed'])) ); ?>">
                <div class="form-group">
                    <label for="_name_txt" class="col-sm-3 control-label"> Pilih Bangsal <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <select name="idbangsal" class="select2 form-control" style="width:100%;" id="bangsal">
                            <?php
                                foreach ($data_bangsal as $obj)
                                {
                                    echo "<option ".dropdown_selected($data_edit, $obj->idbangsal, 'idbangsal')." value='".$obj->idbangsal."'>".$obj->namabangsal."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="_name_txt" class="col-sm-3 control-label"> No. Bed <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" name="nobed" value="<?= forminput_setvalue($data_edit, 'nobed'); ?>" onkeyup="setnobed_tonumber(this.value)" placeholder="input nomor bed" class="form-control" id="nobed">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="_name_txt" class="col-sm-3 control-label"> Statusbed <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <select name="idstatusbed" class="select2 form-control" style="width:100%;" id="statusbed">
                            <?php
                                foreach ($data_status as $obj)
                                {
                                    echo "<option ".dropdown_selected($data_edit, $obj->idstatusbed, 'idstatusbed')." value='".$obj->idstatusbed."'>".$obj->statusbed."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <center style="padding-top: 8px">
                    <div class="row">
                        <a class="btn btn-primary btn-lg" onclick="simpan_bed()">SAVE</a>
                        <a class="btn btn-danger btn-lg" href="<?= base_url('cmasterdata/bed');?>">BACK</a>
                    </div>
                </center>
            </form>
         </div>
         <!-- /.box-body -->
       </div>

       <?php } ?>
     </div>
     <!-- /.col -->
   </div>
   <!-- /.row -->
 </section>
 <!-- /.content -->
        