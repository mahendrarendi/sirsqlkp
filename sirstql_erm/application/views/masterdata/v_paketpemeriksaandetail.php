<!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="toolbar">
            <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
            <a href="<?php echo base_url('cmasterdata/add_paketpemeriksaandetail'); ?>" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data</a>
            <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm pull-right"><i class="fa  fa-refresh"></i> Refresh</a> 
          </div>
            <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th style="width:60px;">No</th>
                <th>Paket Pemeriksaan</th>
                <th>ICD</th>
                <th>Jenis ICD</th>
                <th>Nilai Acuan</th>
                <th style="width:170px;">Aksi</th>
              </tr>
              </thead>
              <tbody>
              <!-- START TAMPIL DATA -->
              <?php
              if (!empty($data_list))
              {
                $no=0;
                foreach ($data_list as $obj) 
                {
                  $this->encryptbap->generatekey_once("HIDDENTABEL");
                  $id           = $this->encryptbap->encrypt_urlsafe($obj->idpaketpemeriksaandetail, "json");
                  $tabel        = $this->encryptbap->encrypt_urlsafe('rs_paket_pemeriksaan_detail', "json");
                  $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_PAKETPEMERIKSAANDETAIL, "json");
                  echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                  echo '  <td>'.$no.'</td>
                          <td>'.$obj->namapaketpemeriksaan.'</td>
                          <td>'.$obj->icd.'-'.$obj->namaicd.'</td>
                          <td>'.$obj->jenisicd.'</td>
                          <td>'.$obj->nilaiacuanrendah.ql_penghubungstring($obj->nilaiacuantinggi,'-').$obj->nilaiacuantinggi.' '.$obj->satuan.'</td>
                          <td>
                             <a data-toggle="tooltip" title="" data-original-title="Edit Paket Pemeriksaan" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_paketpemeriksaandetail/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                             <a data-toggle="tooltip" title="" data-original-title="Delete Paket Pemeriksaan" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                           <i class="fa fa-trash"></i> Delete</a></td>
                        </tr>';
                }
              }
              
              ?>
              <!-- END TAMPIL DATA -->
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <div class="box-header">
            <center>
              <br>
            <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
            </center>
            <br>
            <?php echo validation_errors(); ?>
            <!-- FORM ADD/EDIT-->
            <?php
            (!empty($data_edit)) ? $optioneditpaket=$data_edit['idpaketpemeriksaan'] : $optioneditpaket='';//jika mode add data option kosong selain itu isi data option
            (!empty($data_edit)) ? $optionediticd=$data_edit['icd'] : $optionediticd='';//jika mode add data option kosong selain itu isi data option
            (!empty($data_edit)) ? $optioneditnameicd=$data_edit['namaicd'] : $optioneditnameicd='';
            // SET DATA OPTION
            $optionpaket=[]; 
            if(!empty($data_paket))
            {
              foreach ($data_paket as $obj){ $optionpaket += [$obj->idpaketpemeriksaan=> $obj->namapaketpemeriksaan];}
            }

            echo form_open('cmasterdata/save_paketpemeriksaandetail', 'class="form-horizontal" id="FormPaketPemeriksaandetail"'); 
  		      $this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'id','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idpaketpemeriksaandetail'],'json') :'']);
            styleformgrup1kolom('Paket Pemeriksaan',form_dropdown(['name'=>'idpaket','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'idpaket'],$optionpaket,$optioneditpaket));
            echo form_input(['id'=>'hideidicd','value'=>$optionediticd,'type'=>'hidden']);
            echo form_input(['id'=>'hideidnameicd','value'=>$optioneditnameicd,'type'=>'hidden']);
            styleformgrup1kolom('ICD',form_dropdown(['name'=>'icd','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'icd']));


            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_paketpemeriksaandetail()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/paketpemeriksaandetail').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); 
            ?>
          </div>
        <!-- /.box-header -->
        <div class="box-body">
        </div>
        <!-- /.box-body -->
      </div>
      <?php } ?>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->