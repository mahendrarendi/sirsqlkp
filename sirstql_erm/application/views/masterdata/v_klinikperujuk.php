   <!-- Main content -->;
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <div class="box-body">
              <table id="tableklinikperujuk" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                  <th>No</th>
                  <th>Kode</th>
                  <th>Nama</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($datalist))
                {
                    $no=0;
                    foreach ($datalist as $arr)
                    {
                        echo "<tr><td>".++$no."</td><td>".$arr['kodeklinik']."</td><td>".$arr['namaklinik']."</td></tr>";
                    }
                }
                ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /mode view -->         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->