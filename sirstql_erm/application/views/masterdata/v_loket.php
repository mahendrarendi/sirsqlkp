
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6 row">
              <a href="<?php echo base_url('cmasterdata/add_loket'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
              <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Stasiun</th>
                  <th>Loket</th>
                  <th>Unit</th>
                  <th>Jenis Loket</th>
                  <th>Tampil Ambil Antrian</th>                  
                  <th>Bridging JKN</th>
                  <th>Tarif Otomatis</th>
                  <th>Estimasi Layanan @pasien</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA LOKET -->
                <?php
                if (!empty($data_list))
                { 
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id     =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idloket));
                    $tabel  = $this->encryptbap->encrypt_urlsafe(json_encode('antrian_loket'));
                    $idhalaman = $this->encryptbap->encrypt_urlsafe(V_LOKET, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namastasiun.'</td>
                            <td>'. ((empty($obj->kodeloket)) ? '' : $obj->kodeloket .' - ' ) .$obj->namaloket.' <i class="'.$obj->fontawesome.'"></i></td>
                            <td>'.$obj->namaunit.'</td> 
                            <td>'.$obj->jenisloket.'</td> 
                            <td>'.(($obj->isambil==1) ? 'Tampil' : 'Tidak Tampil' ) .'</td>                                
                            <td>'.(($obj->bridging_jkn==1) ? 'Bridging' : 'Tidak' ) .'</td>
                            <td>'.$obj->icdtarifpemeriksaan.'</td> 
                            <td>'.$obj->lamapelayanan.' menit</td> 
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_loket/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                               <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA LOKET -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
            </div>
              
            <!-- /.box-header -->
            <div class="box-body">
                <form action="<?= base_url('cmasterdata/save_loket'); ?>" class="form-horizontal" id="Formloket" method="post" accept-charset="utf-8">
                    <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                    <input type="hidden" name="idloket" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idloket'])) ); ?>">
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Stasiun <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idstasiun" class="dropdownloket form-control" style="width:100%;" id="namastasiun">
                                <?php
                                    foreach ($dtstasiun as $obj)
                                    {
                                        echo "<option value='".$obj->idstasiun."' ".dropdown_selected($data_edit, $obj->idstasiun, 'idstasiun')." >".$obj->namastasiun."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Unit <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idunit" class="dropdownloket form-control" style="width:100%;" id="idunit">
                                <option value="">Pilih</option>
                                <?php
                                    foreach ($dtunit as $obj)
                                    {
                                        echo "<option value='".$obj->idunit."' ".dropdown_selected($data_edit, $obj->idunit, 'idunit')." >".$obj->namaunit."</option>";
                                    }
                                ?>
                            </select>
                            <small class="text-red">Harap diisi dengan sesuai, digunakan untuk bridging sistem. </small>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode Loket</label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="cek_kodeloket(this.value)" name="kodeloket" style="text-transform:uppercase;" value="<?= forminput_setvalue($data_edit, 'kodeloket') ?>" class="form-control" id="kodeloket" maxlength="5" placeholder="input kode loket">
                            <small class="text-red">Kosongkan kode loket apabila unit sudah dipilih, untuk keperluan bridging. Kode maksimal 5 digit.</small>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Loket</label>
                        <div class="col-sm-6">
                            <input type="text" name="namaloket" value="<?= forminput_setvalue($data_edit, 'namaloket') ?>" class="form-control" id="namaloket">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label">  <span class="asterisk">*</span></label>
                        <div class="col-sm-6">Jika Loket lebih dari satu, maka ditambahkan ID: A, B, F, I, O, X, atau Z. Contoh: Poli Mata A. Karena B mirip dengan C, D, G, J, P, T, dan W; dan seterusnya</div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Ambil Antrian <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="isambil" class="form-control" style="width:100%;" id="isambil">
                                <option value="0" <?= dropdown_selected($data_edit, 0, 'isambil') ?> >Tidak Boleh Ambil</option>
                                <option value="1" <?= dropdown_selected($data_edit, 1, 'isambil') ?>>Boleh Ambil</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Bridging JKN <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="bridging_jkn" class="form-control" style="width:100%;" id="bridging_jkn">
                                <option value="0" <?= dropdown_selected($data_edit, 0, 'bridging_jkn') ?>>Tidak</option>
                                <option value="1" <?= dropdown_selected($data_edit, 1, 'bridging_jkn') ?>>Ya Bridging</option>
                            </select>
                            <small class="bg bg-danger" style="padding:3px;">#Harap Diisi Dengan Sesuai, Untuk Kebutuhan Bridging Antrean.</small><br>
                            <small class="text text-red">Pilih Bridging jika antrian pada Loket yang dibuat harus Bridging dengan Sistem Antrean JKN.</small>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Jenis Loket <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idjenisloket" class="form-control" style="width:100%;" id="idjenisloket">
                                <?php
                                    foreach ($dtjenisloket as $obj)
                                    {
                                        echo "<option value=".$obj->kode." ".dropdown_selected($data_edit, $obj->kode, 'idjenisloket')." >".$obj->jenis." (".$obj->antrean_taskid.") </option>";
                                    }
                                ?>
                            </select>
                            <small class="bg bg-danger" style="padding:3px;">#Harap Diisi Dengan Sesuai, Untuk Kebutuhan Bridging Antrean.</small><br>
                            <p>Kode : (1,2,3,4,5,6,7)<br>
                                1 (mulai waktu tunggu admisi),<br>
                                2 (akhir waktu tunggu admisi/mulai waktu layan admisi), <br>
                                3 (akhir waktu layan admisi/mulai waktu tunggu poli), <br>
                                4 (akhir waktu tunggu poli/mulai waktu layan poli),  <br>
                                5 (akhir waktu layan poli/mulai waktu tunggu farmasi), <br>
                                6 (akhir waktu tunggu farmasi/mulai waktu layan farmasi membuat obat), <br>
                                7 (akhir waktu obat selesai dibuat),<br>
                                99 (tidak hadir/batal)</p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Tarif Otomatis <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select class="form-control select2" name="icd" id="icd">
                                <?= ((empty($data_edit)) ? '<option value="">-Pilih-</option>' : '<option value="'.$data_edit['icdtarifpemeriksaan']).'"><b>'.$data_edit['icdtarifpemeriksaan'].'</b> - '.$data_edit['namaicd'].'</option>'; ?>
                            </select>
                            <small class="bg bg-danger" style="padding:3px;">#Jika ICD di isi, sistem menambahkan otomatis ICD tersebut setelah pasien mendapat antrian periksa di Loket Ini.</small>
                        </div>
                        
                        <div class="col-xs-1">
                            <a id="loket_hapusicd" class="btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Hapus ICD"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label">Estimasi Layanan @pasien <span class="asterisk">*</span></label>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                <input type="text" name="estimasi" class="form-control" value="<?= forminput_setvalue($data_edit, 'lamapelayanan'); ?>">
                                    <span class="input-group-addon bg bg-gray">menit</span>
                            </div>
                        </div>
                    </div>
                    <center style="padding-top: 8px">
                        <div class="row">
                          <a class="btn btn-primary btn-lg" onclick="simpan_loket()">SAVE</a>
                          <a class="btn btn-danger btn-lg" href="<?= base_url('cmasterdata/loket');?>">BACK</a>
                        </div>
                    </center>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->