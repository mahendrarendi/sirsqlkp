<!--Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-6 row">
            <a id="tambahdata" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
            <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
          </div>
            <table id="paketpemeriksaanrule" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th width="100px">No</th>
                      <th>Nama Rule</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content