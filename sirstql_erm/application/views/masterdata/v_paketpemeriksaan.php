<!--Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-6 row">
            <a href="<?php echo base_url('cmasterdata/add_paketpemeriksaan'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
            <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
            <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Data</a>
          </div>
            <table id="dttable" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr class="header-table-ql">
                      <th>Nama Paket</th>
                      <th>Nama Paket Print</th>
                      <th>Jenis ICD</th>
                      <th>Paketpemeriksaan Rule Pesan</th>
                      <th>Format Hasil Cetak</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
            </br>
            <form action="<?= base_url('cmasterdata/save_paketpemeriksaan');?>" class="form-horizontal" id="FormPaketPemeriksaan" method="post" accept-charset="utf-8">
                <input type="hidden" name="saveitem" value="" />
                <input type="hidden" name="insertupdateitem" value="" />
            <div class="col-xs-12">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                      <h3 class="box-title">Paket Pemeriksaan</h3>
                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <input type="hidden" name="idpaketpemeriksaan" value="<?= (($idpaket)? $idpaket :'') ?>" />
                    <div class="box-body form-horizontal">
                        <div class="form-group">
                            <label for="_name_txt" class="col-sm-3 control-label"> Paket Pemeriksaan <span class="asterisk">*</span></label>
                            <div class="col-sm-6"><input type="text" name="namapaketpemeriksaan" placeholder="Nama paket pemeriksaan" value="" class="form-control" /></div>
                        </div>
                        <div class="form-group">
                            <label for="_name_txt" class="col-sm-3 control-label"> Nama Hasil Cetak<span class="asterisk">*</span></label>
                            <div class="col-sm-6"><input type="text" name="namahasilcetak" placeholder="Nama Hasil Cetak" value="" class="form-control" /></div>
                        </div>
                        <div class="form-group">
                            <label for="_name_txt" class="col-sm-3 control-label"> Jenis ICD <span class="asterisk">*</span></label>
                            <div class="col-sm-6"><select id="idjenisicd" name="idjenisicd" class="form-control"></select></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Child</label>
                            <div class="col-sm-6">
                                <select onchange="hideShowPaketChild(this.value)" class="form-control" name="ispunyachild"><option value="0">Tidak</option><option value="1">Punya</option></select>
                                <small class="text text-red">#jika paket memiliki sub paket, isi dengan punya jika tidak isi dengan tidak.</small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Paketpemeriksaan Rule Pesan</label>
                            <div class="col-sm-6"><select class="form-control select2" id="idpaketpemeriksaanrule" name="idpaketpemeriksaanrule"><option value="">Pilih</option></select></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Format Hasil Cetak</label>
                            <div class="col-sm-6"><select class="form-control select2" id="idjeniscetak" name="idjeniscetak"><option value="">Pilih</option></select></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Cetak Hasil QRCode</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="iscetakqrcode" name="iscetakqrcode">
                                    <option value="0">Tidak</option>
                                    <option value="1">Cetak</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Ubah Tanggal/Waktu Hasil</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="isubahwaktuhasil" name="isubahwaktuhasil">
                                    <option value="0">Tidak</option>
                                    <option value="1">Bisa Diubah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="_name_txt" class="col-sm-3 control-label"> Pilih Paket <span class="asterisk">*</span></label>
                            <div class="col-sm-6" id="checkbox_pilihpaket"></div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
                
            <center style="padding: 8px 0px">
            <div class="row">
                <a class="btn btn-primary btn-lg" onclick="menuSimpanPaket()">Simpan</a>
                <a class="btn btn-danger btn-lg" onclick="menuKembali()">Kembali</a>
            </div>
            </center>
           
            </form>
        <!-- /.box-header -->
      <!--</div>-->
      <?php }else if( $mode=='detail'){?>
            <div class="col-md-12">
                <a class="btn btn-primary" onclick="edit_rs_paket_pemeriksaan_detail('','','tambahpaket')"><i class="fa fa-plus-square"></i> Tambah Paket</a>
                <a class="btn btn-primary" onclick="edit_rs_paket_pemeriksaan_detail('','','tambahicd')"><i class="fa fa-plus-square"></i> Tambah ICD</a>
                <a class="btn btn-danger" onclick="menuKembali()"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
            </div>
            <div class="col-xs-12" id="hideDetailPaket" style="padding-top:4px;">
                <div class="box box-default box-solid">
                    <div class="box-header with-border">

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body form-horizontal">
                    <div class="form-group">
                        <input type="hidden" name="idpaketpemeriksaan" value="<?= (($idpaket)? $idpaket :'') ?>" />
                        <label for="_name_txt" class="control-label pull-left col-md-2"> Paket Pemeriksaan :</label>
                        <div class="col-md-5"><input type="text" name="namapaketpemeriksaan"  value="" class="form-control" readonly /></div>
                    </div>
                        
                        <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                            <thead>
                            <tr class="bg bg-yellow-gradient">
                              <th>Parameter</th>
                              <th>Nilai Default</th>
                              <th>Nilai Acuan</th>
                              <th>Satuan</th>
                              <th style="width:70px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody id="listdetail">
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
      
      <?php } ?>
    </div>
    <!-- /.col -->
  </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content