
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar">
              <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
              <a href="<?php echo base_url('cmasterdata/add_distributor'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th style="width:60px;">No</th>
                  <th>Distributor</th>
                  <th>Alamat</th>
                  <th>Telepon</th>
                  <th>No.Rek</th>
                  <th>Email</th>
                  <th>NPWP</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA JENIS ICD -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idbarangdistributor, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('rs_barang_distributor', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_DISTRIBUTOR, "json");
                    $jenisNPWP    = "NPWP";
                    $URLimage    = '';
                    if(!empty($obj->path))
                    {
                      $URLimage = '<div>
                                      <a href="'.URLNASSIMRS.$obj->path.'" target="_blank">
                                        <b><i class="fa fa-file-text" aria-hidden="true"></i> NPWP </b>
                                      </a>
                                      </br> </br>
                                  </div>
                                  <div>
                                      <a data-toggle="tooltip" title="" data-original-title="Hapus NPWP" id="btnDeleteNPWP" onclick="btnDeleteNPWP('.$obj->idberkas.',`'.$obj->path.'`)" class="btn btn-danger btn-xs">Hapus <i class="fa fa-trash-o"></i></a>
                                  </div>';
                                  
                    } else {
                      $URLimage = '<div>
                                    <a data-toggle="tooltip" title="" data-original-title="Upload NPWP" id="btnUploadNPWP" onclick="btnUploadNPWP('.$obj->idbarangdistributor.',`'.$jenisNPWP.'`)" class="btn btn-primary btn-xs">NPWP <i class="fa fa-upload"></i></a>
                                  </div>';
                    }
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>                    
                            <td>'.$obj->distributor.'</td>
                            <td>'.$obj->alamat.'</td>
                            <td>'.$obj->telepon.'</td>
                            <td>'.$obj->norekening.'</td>
                            <td>'.$obj->email.'</td>
                            <td>
                              '.$URLimage.'                             
                            </td>
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Ubah Distributor" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_distributor/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                            </td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA JENIS ICD -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
              <!-- FORM ADD/EDIT JENIS ICD-->
              <?php 
              echo form_open('cmasterdata/save_distributor', 'class="form-horizontal" id="Formdistributor"'); 
              $this->encryptbap->generatekey_once("HIDDENTABEL");
              echo form_input(['name'=>'iddistributor','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idbarangdistributor'],'json') :'']);
              // SET DATA OPTION
              $options=['0'=>'Pilih PBF'];
              if(!empty($datapbf))
              {
//                $options=[];
                foreach ($datapbf as $obj){$options += [$obj->idbarangpbf=> $obj->namapbf];}
              } 
	            styleformgrup1kolom('PBF', form_dropdown(['name'=>'idbarangpbf','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'idbarangpbf'],$options, empty($data_edit) ? 0 : $data_edit['idbarangpbf']));		
              styleformgrup1kolom('Distributor',form_input(['placeholder'=>'nama distributor','name'=>'distributor', 'type'=>'text', 'class'=>'form-control','id'=>'distributor','value'=>empty(!$data_edit) ? $data_edit['distributor'] :'']));
              styleformgrup1kolom('Alamat',form_textarea(['placeholder'=>'alamat distributor','name'=>'alamat', 'type'=>'text', 'class'=>'form-control','rows'=>'4','id'=>'alamat','value'=>empty(!$data_edit) ? $data_edit['alamat'] :'']));
              styleformgrup1kolom('Telepon',form_input(['placeholder'=>'no telepon','name'=>'telepon', 'type'=>'text', 'class'=>'form-control','id'=>'telepon','value'=>empty(!$data_edit) ? $data_edit['telepon'] :'']));
              styleformgrup1kolom('No.Rek',form_input(['placeholder'=>'no rekening','name'=>'norekening', 'type'=>'text', 'class'=>'form-control','id'=>'norekening','value'=>empty(!$data_edit) ? $data_edit['norekening'] :'']));
              styleformgrup1kolom('Email',form_input(['placeholder'=>'email','name'=>'email', 'type'=>'text', 'class'=>'form-control','id'=>'email','value'=>empty(!$data_edit) ? $data_edit['email'] :'']));
              
              echo '<center style="padding-top: 8px">
                    <div class="row">
                      <a class="btn btn-primary btn-lg" onclick="simpan_distributor()">SAVE</a>
                      <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/distributor').'">BACK</a>
                    </div>
                    </center>';
              echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->