
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar">
              <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
              <a href="<?php echo base_url('cmasterdata/add_kelurahan'); ?>" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data</a>
              <a style="margin-left: 18px;" href="<?php echo base_url('cmasterdata/propinsi'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Propinsi</a>
              <a href="<?php echo base_url('cmasterdata/kabupaten'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Kabupaten</a>
              <a href="<?php echo base_url('cmasterdata/kecamatan'); ?>" class="btn btn-default btn-sm pull-left"><i class="fa  fa-file-o"></i> Kecamatan</a>
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm pull-right"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th style="width:60px;">No</th>
                  <th>Propinsi</th>
                  <th>Kabupaten</th>
                  <th>Kecamatan</th>  
                  <th>Kode Pos</th>
                  <th>Kelurahan</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA KECAMATAN -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->iddesakelurahan));
                    $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('geografi_desakelurahan'));
                    $idhalaman = $this->encryptbap->encrypt_urlsafe(V_KELURAHAN, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
					        <td>'.$obj->namapropinsi.'</td>
                            <td>'.$obj->namakabupatenkota.'</td>
                            <td>'.$obj->namakecamatan.'</td>
                            <td>'.$obj->kodepos.'</td>
							<td>'.$obj->namadesakelurahan.'</td>
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_kelurahan/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                               <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA KELURAHAN -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
              <!-- FORM ADD/EDIT KELURAHAN-->
              <?php 
              echo form_open('cmasterdata/save_kelurahan', 'class="form-horizontal" id="Formkelurahan"');
              //---Load Data untuk mode edit
              $options_edit='';
              $checked='';
              if(!empty($data_edit))
              {
                $options_edit=$data_edit['idkecamatan'];
                $this->encryptbap->generatekey_once("HIDDENTABEL");
                echo form_hidden('iddesakelurahan', $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['iddesakelurahan'])));
              }
			  
			   // SET DATA OPTION
              $options=[];
              if(!empty($data_list1))
              {
                $options=[];
                foreach ($data_list1 as $obj) 
                {
					 $options += [$obj->idpropinsi=> $obj->namapropinsi];
                }
              } 
			  styleformgrup1kolom('namapropinsi', form_dropdown(['name'=>'idpropinsi','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'namapropinsi'],$options,$options_edit));
			  
			   // SET DATA OPTION
              $options=[];
              if(!empty($data_list2))
              {
                $options=[];
                foreach ($data_list2 as $obj) 
                {
					 $options += [$obj->idkabupatenkota=> $obj->namakabupatenkota];
                }
              } 
			  styleformgrup1kolom('namakabupatenkota', form_dropdown(['name'=>'idkabupatenkota','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'namakabupatenkota'],$options,$options_edit));

			   // SET DATA OPTION
              $options=[];
              if(!empty($data_list3))
              {
                $options=[];
                foreach ($data_list3 as $obj) 
                {
					 $options += [$obj->idkecamatan=> $obj->namakecamatan];
                }
              } 
			  styleformgrup1kolom('namakecamatan', form_dropdown(['name'=>'idkecamatan','class'=>'select2 form-control','style'=>'width:100%;', 'id'=>'namakecamatan'],$options,$options_edit));
			// SET DATA OPTION
              $options=[];
              if(!empty($data_list))
              {
                $options=[];
                foreach ($data_list as $obj) 
                {
					 $options += [$obj->iddesakelurahan=> $obj->namadesakelurahan];
                }
              } 			  
			  styleformgrup1kolom('namadesakelurahan',form_input(['name'=>'namadesakelurahan', 'type'=>'text', 'class'=>'form-control','id'=>'namadesakelurahan','value'=>empty(!$data_edit) ? $data_edit['namadesakelurahan'] :''])); 
              echo '<center style="padding-top: 8px">
                    <div class="row">
                      <a class="btn btn-primary btn-lg" onclick="simpan_kelurahan()">SAVE</a>
                      <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/kelurahan').'">BACK</a>
                    </div>
                    </center>';
              echo form_close(); 
              ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->