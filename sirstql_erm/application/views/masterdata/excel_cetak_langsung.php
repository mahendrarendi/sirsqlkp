<?php 
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$fileName.xls");
    header("Pragma: no-cache");
    header("Expires: 0");    
?>

<html>
    <head>
        <style>
            /* table, td, th {
                border: 0;
            } */

            table {
                border-collapse: collapse;
                width:30%;
                font-family: "Palatino Linotype", "Book Antiqua", "Palatino", "serif";
                color: #333333;
            }

            /* th {
                height: 70px;
            } */

            img {
                opacity:0.9;
                margin-bottom:-2px;
                width:50%;
            }

            div {
                width:10%;
                float: none;
            }
        </style>
    </head>
    <body>
        <?php    
            echo $listData;  
        ?>
    </body>
</html>
