
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6 row">
              <a href="<?php echo base_url('cmasterdata/add_bangsal'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
              <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Nama Bangsal</th>
                  <th>Dasar Kelas Tarif</th>
                  <th>Kepala Bangsal</th>
                  <th>Kelas</th>  
                  <th>Fasilitas</th>
                  <th>Jenis Kelamin</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idbangsal));
                    $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('rs_bangsal'));
                    $idhalaman = $this->encryptbap->encrypt_urlsafe(V_BANGSAL, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namabangsal.'</td>
                            <td>'.$obj->dasarkelastarif.'</td>   
                            <td>'.$obj->namalengkap.'</td>
                            <td>'.$obj->kelas.'</td>
                            <td>'.$obj->fasilitas.'</td> 
                            <td>'.$obj->jeniskelaminbangsal.'</td>     
                            <td>
                              <a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_bangsal/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                              <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="<?= base_url('cmasterdata/save_bangsal'); ?>" class="form-horizontal" id="Formbangsal" method="post" accept-charset="utf-8">
                    <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idbangsal'])) ); ?>" name="idbangsal" />
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label">Nama Bangsal <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="namabangsal" value="<?= forminput_setvalue($data_edit,'namabangsal'); ?>" class="form-control" id="namabangsal">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Dasar Kelas Tarif <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="dasarkelastarif" class="form-control" style="width:100%;" id="dasarkelastarif">
                                <?php
                                    foreach ($dasarkelastarif as $key=>$value)
                                    {
                                        echo "<option ".dropdown_selected($data_edit,$data_edit['dasarkelastarif'],$dasarkelastarif[$key])." value='".$dasarkelastarif[$key]."'>".$dasarkelastarif[$key]."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Pegawai <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idpegawaika" class="select2 form-control" style="width:100%;" id="namalengkap">
                                <?php
                                    foreach ($data_pegawai as $obj)
                                    {
                                        echo "<option ".dropdown_selected($data_edit,$data_edit['idpegawaika'],$obj->idpegawai)." value='".$obj->idpegawai."'>".$obj->namalengkap."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kelas <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idkelas" class="select2 form-control" style="width:100%;" id="kelas">
                                <?php
                                    foreach ($data_kelas as $obj)
                                    {
                                        echo "<option ".dropdown_selected($data_edit,$obj->idkelas,'idkelas')." value='".$obj->idkelas."'>".$obj->kelas."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Fasilitas <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="fasilitas" value="<?= forminput_setvalue($data_edit,'fasilitas'); ?>" class="form-control" id="fasilitas">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Jenis Kelamin Bangsal <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="jeniskelaminbangsal" class="form-control" style="width:100%;" id="jeniskelaminbangsal">
                                <?php
                                    foreach ($jeniskelaminbangsal as $key=>$value)
                                    {
                                        echo "<option ".dropdown_selected($data_edit,$jeniskelaminbangsal[$key],'jeniskelaminbangsal')." value='".$jeniskelaminbangsal[$key]."'>".$jeniskelaminbangsal[$key]."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode Ruang JKN <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="koderuangjkn" value="<?= forminput_setvalue($data_edit,'koderuangjkn'); ?>" placeholder="input kode ruang (unik)" class="form-control" id="koderuangjkn">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <label  class="label text-red">*Sudah bridging dengan Aplicare, butuh waktu untuk proses bridging.</label>
                        </div>               
                    </div>
                    
                    <center style="padding-top: 8px">
                        <div class="row">
                          <a class="btn btn-primary btn-lg" onclick="simpan_bangsal()">SAVE</a>
                          <a class="btn btn-danger btn-lg" href="<?= base_url('cmasterdata/bangsal'); ?>">BACK</a>
                        </div>
                    </center>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->