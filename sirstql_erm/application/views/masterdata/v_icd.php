<!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if($mode=='view1'){ ?>
        <div class="box">
          
        <div class="box-body">
          <table id="example" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="bg bg-yellow-gradient">
                <th>KodeICD</th>
                <th>NamaICD</th>
                <th>Alias ICD</th>
                <th>Nilai Acuan</th>
                <th>Status Kronis</th>
                <th>Gol.Icd</th>
                <th>Jenis Penyakit <sup>STPRS</sup></th>
                <th>Gol.Sebab Penyakit</th>
                <th>Klasifikasi ICD</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          </div>
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <div class="box-header">
            <center>
              <br>
            <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
            </center>
            <br>
            <?php echo validation_errors(); ?>
            <!-- FORM ADD/EDIT-->
            <?php 
            ($mode=='edit') ? $readonly='readonly' : $readonly='' ; //jika mode edit set kode icd readonly
            echo form_open('cmasterdata/save_icd', 'class="form-horizontal" id="FormICD"'); 
  		      $this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'id','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['icd'],'json') :'']);
            ?>
            <div class="form-group">
                <label for="_name_txt" class="col-sm-3 control-label"> Klasifikasi ICD <span class="asterisk">*</span></label>
                <div class="col-sm-6">
                    <select name="klasifikasiicd" class="form-control">
                        <?php                        
                            foreach ($klasifikasi as $key => $value) { echo '<option  ' . (($key==$data_edit['idicd']) ? 'selected' : '' ) . ' value="'.$key.'" >'.$value.'</option>';}
                        ?>
                    </select>
                    <small class="text text-red">*penting, isi sesuai klasifikasi.</small>
                </div>
             </div>
            
            <div class="form-group">
                <label for="_name_txt" class="col-sm-3 control-label"> Kode ICD <span class="asterisk">*</span></label>
                <div class="col-sm-6">
                    <input type="text" name="kodeicd" value="<?= ((empty(!$data_edit)) ? $data_edit['icd'] :'') ?>" id="inputkodeicd" class="form-control" <?= $readonly ?> >
                    <small class="text text-red" id="maxicd"></small>
                    <small class="text text-red" id="uniqicd"></small>
                </div>
            </div>
            
            <?php
//            styleformgrup1kolom('Kode ICD',form_input([ $readonly=>'' ,'name'=>'kodeicd','id'=>'inputkodeicd', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['icd'] :'']));
            styleformgrup1kolom('Nama ICD',form_input(['name'=>'namaicd', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['namaicd'] :'']));
            styleformgrup1kolom('Alias ICD',form_input(['name'=>'aliasicd', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['aliasicd'] :'']));
            styleformgrup1kolom('Satuan',form_input(['name'=>'satuan', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['satuan'] :'']));
            styleformgrup1kolom('Nilai Acuan Rendah',form_input(['name'=>'nilaiacuanrendah', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['nilaiacuanrendah'] :'']));
            styleformgrup1kolom('Nilai Acuan Tinggi',form_input(['name'=>'nilaiacuantinggi', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['nilaiacuantinggi'] :'']));
            
            $jenisicd_list = [];
            if(!empty($jenisicd)){foreach ($jenisicd as $data){$jenisicd_list += [$data->idjenisicd=>$data->jenisicd];}}
            $jenisicd_edit = empty(!$data_edit) ? $data_edit['idjenisicd'] :'';
            styleformgrup1kolom('Jenis ICD', form_dropdown(['name'=>'jenisicd','class'=>'form-control','style'=>'width:100%;', 'id'=>'jenisicd'],$jenisicd_list,$jenisicd_edit));
            
            styleformgrup1kolom('NilaiDefault',form_input(['name'=>'nilaidefault', 'type'=>'text', 'class'=>'form-control','value'=>empty(!$data_edit) ? $data_edit['nilaidefault'] :'']));

            $istext_list = [];
            if(!empty($istext)){foreach ($istext as $key => $value){$istext_list += [$istext[$key]=>$istext[$key]];}}
            $istext_edit = empty(!$data_edit) ? $data_edit['istext'] :'';
            styleformgrup1kolom('Nilai ICD', form_dropdown(['name'=>'istext','class'=>'form-control','style'=>'width:100%;', 'id'=>'istext'],$istext_list,$istext_edit));
            
            $statuskronis_list = [];
            if(!empty($statuskronis)){foreach ($statuskronis as $key => $value){$statuskronis_list += [$statuskronis[$key]=>$statuskronis[$key]];}}
            $statuskronis_edit = empty(!$data_edit) ? $data_edit['statuskronis'] :'';
            styleformgrup1kolom('Status kronis', form_dropdown(['name'=>'statuskronis','class'=>'form-control','style'=>'width:100%;', 'id'=>'statuskronis'],$statuskronis_list,$statuskronis_edit));
            
            $golicd_list = [];
            if(!empty($golonganicd)){foreach ($golonganicd as $key => $value){$golicd_list += [$golonganicd[$key]=>$golonganicd[$key]];}}
            $golicd_edit = empty(!$data_edit) ? $data_edit['golonganicd'] :'';
            styleformgrup1kolom('Golongan Icd', form_dropdown(['name'=>'golonganicd','class'=>'form-control','style'=>'width:100%;', 'id'=>'golonganicd'],$golicd_list,$golicd_edit));
            
            $idgolongansebabpenyakit = [''=>'Pilih'];
            if(!empty($sebabpenyakit)){foreach ($sebabpenyakit as $data){$idgolongansebabpenyakit += [$data->idgolongansebabpenyakit=>$data->nodtd.', '.$data->golongansebabpenyakit];}}
            $idgolongansebabpenyakit_edit = empty(!$data_edit) ? $data_edit['idgolongansebabpenyakit'] :'';
            styleformgrup1kolom('Golongan Sebab Penyakit', form_dropdown(['name'=>'idgolongansebabpenyakit','class'=>'form-control select2','style'=>'width:100%;', 'id'=>'idgolonganpenyebabpenyakit'],$idgolongansebabpenyakit,$idgolongansebabpenyakit_edit));

            $idjenispenyakit = [''=>'Pilih'];
            if(!empty($jenispenyakit)){foreach ($jenispenyakit as $data){$idjenispenyakit += [$data->idjenispenyakit=>$data->jenispenyakit];}}
            $idjenispenyakit_edit = empty(!$data_edit) ? $data_edit['idjenispenyakit'] :'';
            styleformgrup1kolom('Jenis Penyakit <sup>STPRS</sup>', form_dropdown(['name'=>'idjenispenyakit','class'=>'form-control select2','style'=>'width:100%;', 'id'=>'idjenispenyakit'],$idjenispenyakit,$idjenispenyakit_edit));
            
            styleformgrup1kolom('JM Bukan Dokter Penunjang?', form_checkbox(['name'=>'isjmbukanpenunjang','id'=>'isjmbukanpenunjang','value'=>'1','checked'=>empty(!$data_edit) ? $data_edit['isjmbukanpenunjang']==1:false]));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_icd()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/icd').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); 
            ?>
          </div>
        <!-- /.box-header -->
        <div class="box-body">
        </div>
        <!-- /.box-body -->
      </div>
      <?php } ?>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->