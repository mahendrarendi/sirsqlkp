
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar">
              <!-- <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> -->
              <!--<a href="<?php echo base_url('cmasterdata/add_jenisicd'); ?>" class="btn btn-default btn-sm pull-right"><i class="fa  fa-plus-square"></i> Add Data</a>-->
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-default btn-sm pull-right"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th style="width:60px;">No</th>
                  <th>Jenis ICD</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA JENIS ICD -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idjenisicd, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('rs_jenisicd', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_JENISICD, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->jenisicd.'</td>
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Jenis Icd" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_jenisicd/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA JENIS ICD -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT JENIS ICD-->
            <?php 
            echo form_open('cmasterdata/save_jenisicd', 'class="form-horizontal" id="Formjenisicd"'); 
			$this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'idjenisicd','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idjenisicd'],'json') :'']);
            styleformgrup1kolom('jenisicd',form_input(['name'=>'jenisicd', 'type'=>'text', 'class'=>'form-control','id'=>'jenisicd','value'=>empty(!$data_edit) ? $data_edit['jenisicd'] :'']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_jenisicd()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/jenisicd').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->