<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6 row">
            <!-- <select id="idunit" name="unit" onchange="reloadPemeriksaanByUnit()" style="margin-right:4px;" class="col-xs-12 col-md-2 select2" style="width:100%"></select> -->
              <!-- <select id="idbangsal" name="bangsal" onchange="cariByBangsal()" style="margin-right:4px;" class="sel2 col-xs-12 col-md-2" style="width:100%"></select> -->
              <select id="idunit" name="idunit" onchange="reloadINMByUnit()" style="margin-right:4px;" class="col-xs-12 col-md-3 select2" style="width:100%"></select>
              <a id="addinventaris" href="#" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Inventaris</a>
              <a id="reload" href="#" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
              <a id="excel" href="#" class="btn btn-success btn-sm"><i class="fa fa-file"></i> Unduh</a>
            </div>
              <table id="dtinventarisnonmedis" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                      <th>No</th>
                      <th>Nama Unit</th>
                      <th>Nama Item</th>
                      <th>No Inventory</th>
                      <th>Kondisi</th>
                      <th>Agen/Distributor</th>
                      <th>Tahun Pengadaan</th>
                      <th>Harga</th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->