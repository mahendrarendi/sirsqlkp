<style>
    .btn-circle {
        width: 27px;
        height: 27px;
        padding: 1px 0px;
        border-radius: 15px;
        font-size: 14px;
        text-align: center;
    }

    /* table,
    th,
    td {
        border: 1px solid black;
    } */

    .select2-container {
        width: 100% !important;
    }

    .btn-detail {
        border-radius: 6px;
    }

    .modal-content {
        -webkit-border-radius: 0px !important;
        -moz-border-radius: 0px !important;
        border-radius: 10px !important;
    }
</style>

<section class="content">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#datamaster">Data Master</a></li>
        <li><a data-toggle="tab" href="#tautansdkislki">Tautan SDKI-SLKI</a></li>
        <li><a data-toggle="tab" href="#kriteriahasil">Kriteria Hasil</a></li>
    </ul>

    <div class="tab-content">
        <div id="datamaster" class="tab-pane fade active in">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Tambah Data</strong></div>
                        <div class="panel-body">
                            <form id="frmSLKI">
                                <div class="form-group">
                                    <label for="">Kode SLKI</label>
                                    <input type="text" class="form-control" name="kodeslki">
                                </div>
                                <div class="form-group">
                                    <label for="">Nama SLKI</label>
                                    <input type="text" class="form-control" name="namaslki">
                                </div>
                                <div class="form-group">
                                    <label for="">Ekspetasi</label>
                                    <select name="ekspetasi" id="" class="form-control">
                                        <option value="Meningkat">Meningkat</option>
                                        <option value="Menurun">Menurun</option>
                                        <option value="Memburuk">Memburuk</option>
                                        <option value="Membaik">Membaik</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Definisi</label>
                                    <textarea name="definisislki" class="form-control"></textarea>
                                </div>
                                <div class="text-right">
                                    <button type="button" class="btn btn-primary btn-md" id="btnsimpanslki">SIMPAN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>DATA SLKI</strong></div>
                        <div class="panel-body">
                            <table class="table table-bordered table-hovered dt-slki" style="width : 100%;">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th>Nomor</th>
                                        <th>Kode SLKI</th>
                                        <th>Nama SLKI</th>
                                        <th>Ekspetasi</th>
                                        <th>Definisi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="listslki">
                                    <?php $i = 1; ?>
                                    <?php foreach ($dataslki as $data) { ?>
                                        <tr>
                                            <td><?php echo $i++; ?></td>
                                            <td><?= $data['kode_slki'] ?></td>
                                            <td><?= $data['nama_slki'] ?></td>
                                            <td><?= $data['ekspektasi'] ?></td>
                                            <td><?= $data['definisislki'] ?></td>
                                            <td>
                                                <div class="text-center">
                                                    <a href="#" type="button" class="btn btn-danger btn-sm btn-circle btnhapusslki" data-toggle="tooltip" title="Hapus Data" iddatasdki="<?= $data['id_slki'] ?>"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tautansdkislki" class="tab-pane fade">
            <div class="row">
                <div class="col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Tautan SDKI-SLKI</strong></div>
                        <div class="panel-body">
                            <form action="<?= base_url('cmasterdata/save_tautan_sdki_slki') ?>" method="POST">
                                <div class="form-group">
                                    <label for="">Pilih SDKI</label>
                                    <select id="" class="form-control idsdki" name="idsdki"></select>
                                </div>
                                <div class="form-group">
                                    <label for="">Luaran Utama</label>
                                    <select id="" class="form-control idslki" name="idluaranutama"></select>
                                </div>
                                <div class="form-group">
                                    <label for="">Luaran Tambahan</label>
                                    <select id="" class="form-control idslki" name="luarantambahan[]"></select>
                                    <div class="selectluarantambahan"></div>
                                    <div class="text-right">
                                        <button type="button" class="btn btn-light btn-md btntambahselectluarantambahan" data-toggle="tooltip" title="Tambah Luaran Tambahan"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary btn-md">SIMPAN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="panel panel-default">
                        <div class="panel-heading"><strong>Data Tautan SDKI-SLKI</strong></div>
                        <div class="panel-body">
                            <table style="width : 100%;" class="table table-bordered table-hover table-striped dt-slki">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th style="width : 37px;">Nomor</th>
                                        <th>Nama SDKI</th>
                                        <th>Luaran Utama</th>
                                        <th>Luaran Tambahan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($listdatatautansdkislki as $item) {
                                        echo "<tr>";
                                        echo "<td>".$i++."</td>";
                                        echo "<td>" . $item['namasdki'] . " </td>";
                                        echo "<td>" . $item['luaranutama'] . " </td>";
                                        echo "<td><ol>" . $item['luarantambahan'] . "</ol></td>";
                                        echo '<td><ol><button type="button" class="btn btn-danger btn-md btnhapustautansdkislki" style="border-radius : 77%;" data-toggle="tooltip" title="Hapus Data" idtautansdkislki="'.$item['id_tautan_slki_sdki'].'"><i class="fa fa-trash"></i></button></td></tr>';
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="kriteriahasil" class="tab-pane fade">
            <div class="panel panel-default">
                <div class="panel-heading"><strong>Kriteria Hasil</strong></div>
                <div class="panel-body">
                    <div class="text-left">
                        <a data-toggle="tab" href="#tambahkriteriahasil" type="button" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;Tambah Data</a>
                    </div> <br>
                    <table class="table table-bordered table-hovered dt-slki" style="width : 100%;">
                        <thead>
                            <tr class="header-table-ql">
                                <th class="text-center" style="width : 12%;">Nomor</th>
                                <th class="text-center" style="width : 100px;">Kode SLKI</th>
                                <th class="text-center" style="width : 100px;">Nama SLKI</th>
                                <th class="text-center" style="width : 123px;">Ekspetasi</th>
                                <th class="text-center" style="width : 3px;">Jumlah Tingkatan</th>
                                <th class="text-center" style="width : 150px;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $j = 1; ?>
                            <?php foreach ($listdatakriteriahasilslki as $data) { ?>
                                <tr>
                                    <td><?= $j++ ?></td>
                                    <td><?= $data['kode_slki'] ?></td>
                                    <td><?= $data['nama_slki'] ?></< /td>
                                    <td><?= $data['ekspektasi'] ?></td>
                                    <td><?= $data['jumlahtingkatan'] ?></td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-danger btn-detail" data-toggle="modal" data-target="#modalSLKI" idslki="<?= $data['id_slki'] ?>"><i class="fa fa-info-circle"></i>&nbsp;DETAIL</button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="tambahkriteriahasil" class="tab-pane fade in">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Tambah Kriteria Hasil</h4>
                </div>
                <div class="panel-body">
                    <form action="<?= base_url('cmasterdata/simpanKriteriaHasilSLKI'); ?>" method="POST">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">KODE/NAMA SLKI</label><br>
                                <select name="idslki" class="form-control idslki"></select> <br>
                            </div>
                            <div class="form-group">
                                <label for="">Jumlah Tingkatan</label>
                                <input type="number" class="form-control" name="jumlahtingkatan">
                            </div>
                        </div><br>
                        <div id="tblkriteria">
                        </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-lg">SIMPAN</button>
                    <a data-toggle="tab" href="#kriteriahasil" href="#" type="button" class="btn btn-danger btn-lg">KEMBALI</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="modalSLKI" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <div class="text-left">
                    <span class="badge badge-secondary" id="kodeslki"></span><br>
                    <span class="badge badge-secondary" id="namaslki"></span><br>
                    <span class="badge badge-secondary" id="ekspetasi"></span><br>
                    <span class="badge badge-secondary" id="jumlahtingkatan"></span>
                </div><br>
                <div class="tbltingkatankriteriahasil"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-md" data-dismiss="modal" style="border-radius : 8px;"><i class="fa fa-times-circle"></i>&nbsp;Close</button>
                <button type="button" class="btn btn-danger btn-md btnhapuskriteriaslki" style="border-radius : 8px;"><i class="fa fa-trash"></i>&nbsp;Hapus</button>
            </div>
        </div>
    </div>
</div>