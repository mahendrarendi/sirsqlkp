<style>
   .add-elemen {
      line-height: normal;
   }

   #tbl-elemen tr td {
      padding-left: 0 !important;
      padding-top: 0 !important;
      padding-bottom: 10px;
   }

   span.required {
      color: red;
   }

   #tb-addelement .form-group {
      margin-bottom: 0;
      padding: 10px 10px;
   }

   #tb-addelement tr td:last-child {
      text-align: center;
      vertical-align: middle;
   }

   .btn-cicle {
      width: 35px;
      height: 35px;
      padding: 6px 0px;
      border-radius: 35px;
   }
</style>
<section class="content">
   <div class="row">
      <div class="col-md-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Tambah
            </div>
            <form action="" class="form" id="frm-dokumentasi">
               <div class="panel-body">
                  <div class="err-msg"></div>
                  <div class="form-group">
                     <label>Pilih Kategori <span class="required">*</span> </label>
                     <select name="kodekategori" class="form-control ql-select2" id="selectkategoriparent" required>
                        <option value="">Pilih Kategori</option>
                        <option value="0" jenis="parent">PARENT CATEGORY</option>
                        <?php foreach ($datas['kategori'] as $data) {
                           echo '<option jenis="subkategori" value="' . $data['id_kategori_sdki'] . '">' . $data['nama_kategori_sdki'] . '</option>';
                        } ?>}
                        <?php foreach ($datas['subkategori'] as $data) {
                           echo '<option jenis="childsubkategori" value="' . $data['id_subkategori_sdki'] . '">__' . $data['nama_subkategori_sdki'] . '&nbsp;(SUBKATEGORI = ' . $data['nama_kategori_sdki'] . ')</option>';
                        } ?>}
                     </select>
                  </div>
                  <div class="form-group" id="namakategori">
                     <label for="">Nama *</label>
                     <input type="text" name="namakategori" class="form-control" placeholder="Nama Kategori">
                  </div>
                  <div class="form-group" id="namakategori">
                     <label for="">Definisi *</label>
                     <textarea name="definisisdki" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                     <div class="row" id="rowdiagnosa"></div>
                  </div>
               </div>
               <div class="form-group text-right" style="margin-bottom:0;">
                  <button type="button" class="btn btn-primary" id="save-dokumentasi-sdki">Simpan</button>
               </div>

            </form>
         </div>
      </div>
      <div class="col-md-6">
         <div class="panel panel-default">
            <div class="panel-heading">
               Data
            </div>
            <table class="table table-bordered dt-responsive" id="dt-sdki" style="width : 100%;">
               <thead>
                  <th>Nomor</th>
                  <th>Kode/Nama Diagnosa</th>
                  <th>Kategori/Kode Diagnosa</th>
                  <th>Jenis Kategori</th>
                  <th>Aksi</th>
               </thead>
               <tbody>
                  <?php $i = 1; ?>
                  <?php foreach ($datas['kategori'] as $data) { ?>
                     <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $data['nama_kategori_sdki'] ?></td>
                        <td></td>
                        <td>KATEGORI</td>
                        <td><button type="button" jenis="kategori" idkat="<?= $data['id_kategori_sdki'] ?>" data-toggle="tooltip" title="Hapus Kategori" class="btn btn-danger btn-md btnhapus btn-cicle"><i class="fa fa-trash"></i></button></td>
                     </tr>
                  <?php } ?>
                  <?php foreach ($datas['subkategori'] as $data) { ?>
                     <tr>
                        <td><?= $i++ ?></td>
                        <td>__<?= $data['nama_subkategori_sdki'] ?> </td>
                        <td><?= $data['nama_kategori_sdki'] ?></td>
                        <td>SUBKATEGORI</td>
                        <td><button type="button" jenis="subkategori" idkat="<?= $data['id_subkategori_sdki'] ?>" data-toggle="tooltip" title="Hapus Kategori" class="btn btn-danger btn-md btnhapus btn-cicle"><i class="fa fa-trash"></i></td>
                     </tr>
                  <?php } ?>
                  <?php foreach ($datas['childkategori'] as $data) { ?>
                     <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $data['nama_childkategori'] ?> </td>
                        <td><?= $data['kode_childkategori'] ?>&nbsp;(<?= $data['nama_subkategori_sdki'] ?>)</td>
                        <td>CHILD KATEGORI</td>
                        <td>
                           <button type="button" class="btn btn-primary btn-cicle btn-detail" data-toggle="modal" data-target="#modalSDKI" onclick="detailchildkategori(<?= $data['id_sdki_childkategori'] ?>)"><i class="fa fa-eye"></i></button>
                           <!-- <button type="button" jenis="childkategori" idkat ="<?= $data['id_sdki_childkategori'] ?>"  data-toggle="tooltip" title="Hapus Kategori" class="btn btn-danger btn-md btnhapus btn-cicle"><i class="fa fa-trash"></i> -->
                        </td>
                     </tr>
                  <?php } ?>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</section>

<!-- modal -->
<div id="modalSDKI" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
         </div>
         <div class="modal-body">
            <table class="table" style="width : 100%;">
               <tbody>
                  <tr>
                     <td style="width : 86px;"><strong>Kode SDKI</strong></td>
                     <td style="width : 1px;"><strong>:</strong></td>
                     <td style="">D.0001</td>
                  </tr>
                  <tr>
                     <td><strong>Nama SDKI</strong></td>
                     <td><strong>:</strong></td>
                     <td>Bersihan Jalan Napas Tidak Efektif</td>
                  </tr>
               </tbody>
            </table>
            <div class="panel panel-default">
               <div class="panel-heading">Defenisi</div>
               <div class="panel-body">Panel Content</div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>