
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            <!-- <div class="box-header"> -->
            <!-- </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="dtpropinsi" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Propinsi</th>
                  <th style="width:120px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT PROPINSI-->
            <?php 
            echo form_open('cmasterdata/save_propinsi', 'class="form-horizontal" id="FormPropinsi"'); 
			$this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'idpropinsi','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idpropinsi'],'json') :'']);
            styleformgrup1kolom('propinsi',form_input(['name'=>'namapropinsi', 'type'=>'text', 'class'=>'form-control','id'=>'namapropinsi','value'=>empty(!$data_edit) ? $data_edit['namapropinsi'] :'']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_propinsi()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/propinsi').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->