<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6">
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Barang</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="dtbarang" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Satuan</th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->