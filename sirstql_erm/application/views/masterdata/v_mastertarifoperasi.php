<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <!-- mode view -->
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
           <div class="col-md-6 row">
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
              <a id="formtarif" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah</a> 
            </div>
           <table id="dtmastertarifoperasi" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
             <tr class="header-table-ql">
               <th>Jenis Tarif Operasi</th>
               <th>Tindakan</th>
               <th>Jenis Tarif</th>
               <th>Jasa Medis</th>
               <th>Jasa RS</th>
               <th>Total</th>
               <th></th>
             </tr>
             </thead>
             <tbody>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
       <!-- start mode add or edit -->

   </div>
       
    <div class="col-xs-12 col-md-4">
        <!-- mode view -->
        <div class="box">
            <div class="box-header with-border">
               <h4>Tarif Operasi NON SC BPJS</h4>
               <!-- /.box-tools -->
             </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table  class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>Plafon BPJS</th>
                <th>Tarif</th>
              </tr>
              </thead>
              <tbody>
                <?php 
                    foreach ($tarif_nonsc as $arr)
                    {
                        echo '<tr id="'.$arr['idplafon'].'">
                            <td><input onchange="update_tarifnonsc('.$arr['idplafon'].')" type="text" name="plafon'.$arr['idplafon'].'" value="'.$arr['plafon'].'" class="form-control" placeholder="input plafon"/></td>
                            <td><input onchange="update_tarifnonsc('.$arr['idplafon'].')" type="text" name="tarif'.$arr['idplafon'].'" value="'.convertToRupiah($arr['tarif']).'" class="form-control" placeholder="input nominal tarif"/></td>
                        </tr>';
                    }
                ?>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
    </div>
       
    <div class="col-xs-12 col-md-4">
        <!-- mode view -->
        <div class="box">
            <div class="box-header with-border">
               <h4>Tarif Operasi SC BPJS</h4>
               <!-- /.box-tools -->
             </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table  class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql">
                <th>Kelas BPJS</th>
                <th>Tarif</th>
              </tr>
              </thead>
              <tbody>
                  <?php
                    foreach ($tarif_sc as $arr)
                    {
                        echo '<tr id="'.$arr['idkelas'].'">
                                <td><select onchange="update_tarifsc('.$arr['idkelas'].')" name="kelas'.$arr['idkelas'].'" class="form-control" disabled>'.list_dropdown($kelas,$arr['idkelas']).'</select></td>
                                <td><input  onchange="update_tarifsc('.$arr['idkelas'].')" type="text" name="tarifsc'.$arr['idkelas'].'"  value="'. convertToRupiah($arr['tarif']).'" class="form-control" placeholder="input nominal tarif"/></td>
                            </tr>';
                    }
                    
                    function list_dropdown($data,$pilih='')
                    {
                        $opt = '';
                        foreach ($data as $arr)
                        {
                            $opt .= "<option value='".$arr['idkelas']."' ".(($arr['idkelas'] == $pilih) ? 'selected' : '' ).">".$arr['kelas']."</option>";
                        }
                        return $opt;
                    }
                  ?>
                
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
    </div>       
       
    <div class="col-xs-12 col-md-2">
        <!-- mode view -->
        <div class="box">
            <div class="box-header with-border">
               <h4>Tarif Orthopedi</h4>
             </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table  class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                <tr class="header-table-ql">
                  <th>Persentase</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td><div class="input-group">
                            <input type="number" onkeyup="settarif_operasi_ortopedi(this.value)" value="<?= $tarif_orto['persentase']; ?>" name="nominal" class="form-control" placeholder="input persentase">
                            <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                        </div>
                    </td>
                </tr>  
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
    </div>
 </div>
 <!-- /.row -->
</section>
<!-- /.content -->