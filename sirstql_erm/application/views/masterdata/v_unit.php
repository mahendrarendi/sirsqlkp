
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6 row">
              <a href="<?php echo base_url('cmasterdata/add_unit'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a> 
              <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th style="width:60px;">No</th>
                  <th>Nama Unit</th>
                  <th>Akronim Unit</th>
                  <th>Kode BPJS</th>
                  <th>Kepala Unit</th>
                  <th>Nama Instalasi</th>
                  <th>Nama Paket Pemeriksaan</th>
                  <th>Hak Input ID Jenis ICD</th>
                  <th style="width:170px;">Aksi</th>
                </tr>
                </thead>
                <tbody>
               <!-- START TAMPIL DATA UNIT -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idunit));
                    $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('rs_unit'));
                    $idhalaman = $this->encryptbap->encrypt_urlsafe(V_UNIT, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namaunit.'</td>
                            <td>'.$obj->akronimunit.'</td>
                            <td>'.$obj->kodebpjs.'</td>
                            <td>'.$obj->namalengkap.'</td>
                            <td>'.$obj->namainstalasi.'</td>
                            <td>'.$obj->namapaketpemeriksaan.'</td>   
                            <td>'.$obj->hakinputidjenisicd.'</td>    
                            <td>
                              <a data-toggle="tooltip" title="" data-original-title="Edit Akses" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_unit/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                              <a data-toggle="tooltip" title="" data-original-title="Delete Akses" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA UNIT -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- /mode view -->
          
          <!-- mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="<?= base_url('cmasterdata/save_unit'); ?>" class="form-horizontal" id="Formunit" method="post" accept-charset="utf-8">
                    <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idunit'])) ); ?>" name="idunit"/>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Nama Unit <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="namaunit" value="<?= forminput_setvalue($data_edit,'namaunit'); ?>" class="form-control" id="namaunit">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode Unit <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="cek_kodeunit(this.value)" name="akronimunit" value="<?= forminput_setvalue($data_edit,'akronimunit'); ?>" class="form-control" id="akronimunit" style="text-transform:uppercase;" maxlength="5" placeholder="input kode unit">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode BPJS <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" name="kodebpjs" value="<?= forminput_setvalue($data_edit,'kodebpjs'); ?>" class="form-control" id="kodebpjs">
                            <sub class="text-red">#Isi Sesuai Referensi dari BPJS, digunakan untuk Bridging Sistem.</sub>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label">Kepala Unit <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idpegawai" class="select2 form-control" style="width:100%;" id="namalengkap">
                                <?php
                                    foreach ($data_pegawai as $obj)
                                    {
                                        echo "<option ".(( empty(!$data_edit) && $data_edit['idpegawai'] == $obj->idpegawai ) ? 'selected' : '' )." value='".$obj->idpegawai."'>".$obj->namalengkap."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Instalasi <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idinstalasi" class="select2 form-control" style="width:100%;" id="namalengkap">
                                <?php
                                    foreach ($data_instalasi as $obj)
                                    {
                                        echo "<option ".(( empty(!$data_edit) && $data_edit['idinstalasi'] == $obj->idinstalasi ) ? 'selected' : '' )." value='".$obj->idinstalasi."'>".$obj->namainstalasi."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Paket Pemeriksaan <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="idpaketpemeriksaan" class="select2 form-control" style="width:100%;" id="namalengkap">                                
                            <option value="">Pilih</option>
                                <?php 
                                    foreach ($data_paketpemeriksaan as $obj)
                                    {
                                        echo "<option ".(( empty(!$data_edit) && $data_edit['idpaketpemeriksaan'] == $obj->idpaketpemeriksaan ) ? 'selected' : '' )." value='".$obj->idpaketpemeriksaan."'>".$obj->namapaketpemeriksaan."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Hak Input Jenis ICD <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <?php
                                foreach ($data_jenisicd as $arr)
                                {
                                    echo '<label><input type="checkbox" name="hakinputidjenisicd[]" value="'.$arr['idjenisicd'].'" '. (( empty(!$data_edit) && in_array($arr['idjenisicd'], explode(',', $data_edit['hakinputidjenisicd']) )) ? 'checked' : '')   .' id="hakinputidjenisicd" class="flat-red"> '.$arr['jenisicd'].'</label><br>';
                                }
                            ?>
                        </div>
                    </div>
                    <center style="padding-top: 8px">
                        <div class="row">
                            <a class="btn btn-primary btn-lg" onclick="simpan_unit()">SAVE</a>
                            <a class="btn btn-danger btn-lg" href="<?= base_url('cmasterdata/unit'); ?>">BACK</a>
                        </div>
                    </center>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->