<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <div class="toolbar col-sm-6 row">
              <a id="addinventaris" href="#" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah Inventaris</a>
              <a id="reload" href="#" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="dtinventarisnonmedis" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                      <th>No</th>
                      <th>Kode Blok</th>
                      <th>Nama Blok</th>
                      <th>Kode Ruang</th>
                      <th>Nama Ruang</th>
                      <th>Jumlah Inventaris</th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->