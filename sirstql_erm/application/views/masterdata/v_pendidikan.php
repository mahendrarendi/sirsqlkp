
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-6 row">
              <a href="<?php echo base_url('cmasterdata/add_pendidikan'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a>
              <a style="margin-right: 14px;" href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
              <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                        <th style="width:60px;">No</th>
                        <th>Jenjang Pendidikan</th>
                        <th>Level KKNI</th>
                        <th style="width:170px;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <!-- START TAMPIL DATA JENJANG PENDIDIKAN -->
                <?php
                if (!empty($data_list))
                {
                  $no=0;
                  foreach ($data_list as $obj) 
                  {
                    $this->encryptbap->generatekey_once("HIDDENTABEL");
                    $id           = $this->encryptbap->encrypt_urlsafe($obj->idpendidikan, "json");
                    $tabel        = $this->encryptbap->encrypt_urlsafe('demografi_pendidikan', "json");
                    $idhalaman    = $this->encryptbap->encrypt_urlsafe(V_PENDIDIKAN, "json");
                    echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                    echo '  <td>'.$no.'</td>
                            <td>'.$obj->namapendidikan.'</td>
							<td>'.$obj->levelkkni.'</td>
                            <td>
                               <a data-toggle="tooltip" title="" data-original-title="Edit Pendidikan" class="btn btn-warning btn-xs" href="'.base_url('cmasterdata/edit_pendidikan/'.$id).'" ><i class="fa fa-pencil"></i> Edit</a>
                               <a data-toggle="tooltip" title="" data-original-title="Delete Pendidikan" id="delete_data" nobaris="'.$no.'" class="btn btn-danger btn-xs" href="#" alt="'.$tabel.'" alt2="'.$id.'" alt3="'.$idhalaman.'">
                             <i class="fa fa-trash"></i> Delete</a></td>
                          </tr>';
                  }
                }
                ?>
                <!-- END TAMPIL DATA PROPINSI -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          <?php }else if( $mode=='edit' || $mode=='add'){?>
          <div class="box">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
              <?php echo validation_errors(); ?>
             <!-- FORM ADD/EDIT PENDIDIKAN-->
            <?php 
            echo form_open('cmasterdata/save_pendidikan', 'class="form-horizontal" id="Formpendidikan"'); 
			$this->encryptbap->generatekey_once("HIDDENTABEL");
            echo form_input(['name'=>'idpendidikan','type'=>'hidden', 'value'=>empty(!$data_edit) ? $this->encryptbap->encrypt_urlsafe($data_edit['idpendidikan'],'json') :'']);
            styleformgrup1kolom('pendidikan',form_input(['name'=>'namapendidikan', 'type'=>'text', 'class'=>'form-control','id'=>'namapendidikan','value'=>empty(!$data_edit) ? $data_edit['namapendidikan'] :'']));
			styleformgrup1kolom('levelkkni',form_input(['name'=>'levelkkni', 'type'=>'text', 'class'=>'form-control','id'=>'levelkkni','value'=>empty(!$data_edit) ? $data_edit['levelkkni'] :'']));
            echo '<center style="padding-top: 8px">
                  <div class="row">
                    <a class="btn btn-primary btn-lg" onclick="simpan_pendidikan()">SAVE</a>
                    <a class="btn btn-danger btn-lg" href="'.base_url('cmasterdata/pendidikan').'">BACK</a>
                  </div>
                  </center>';
            echo form_close(); ?>
        </div>
          <!-- /.box-header -->
          <div class="box-body">
          </div>
          <!-- /.box-body -->
        </div>
         
        <?php } ?>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->