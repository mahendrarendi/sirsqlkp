<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <!-- mode view -->
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
           <div class="col-md-6 row">
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
           <table id="member" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
             <tr class="header-table-ql">
               <th>No</th>
               <th>Member</th>
               <th style="max-width: 500px;">Keterangan</th>
               <th>Hari Berlaku</th>
               <th></th>
             </tr>
             </thead>
             <tbody>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->
       <!-- start mode add or edit -->

   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</section>
<!-- /.content -->