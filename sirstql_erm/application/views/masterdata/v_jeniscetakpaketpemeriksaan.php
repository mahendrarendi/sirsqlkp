<!--Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-6 row">
            <a id="add" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data</a>
            <a id="reload" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
          </div>
            <table id="jeniscetakpaketpemeriksaan" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
                  <tr class="header-table-ql">
                      <th>No</th>
                      <th>Kode</th>
                      <th>Jenis Cetak</th>
                      <th>Judul Utama</th>
                      <th>Judul Detail</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content