<style>
.panel-header {
    padding: 15px;
}

.panel-header h5 {
    /* margin: 0; */
    font-weight: bold;
}
span.select2.select2-container {
    width: 100% !important;
}
ul.select2-selection__rendered {
    max-height: 100%;
}
</style>

<!-- .content -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-body">
                    <form class="form" id="frm-pengaturanpic">
                        <div class="err-msg"></div>
                        <div class="form-group">
                            <label>Pengaturan Users PIC <span class="required">*</span></label>
                            <select  name="allusers[]" required class="form-control qlselect-multi" multiple="multiple">
                            <?php 
                                foreach( $alluser as $row): 
                                $author_id  = explode(',', $pengaturan_pic['author'] );
                                $selected   = ( in_array( $row['iduser'],$author_id ) ) ? 'selected' : '';
                            ?>
                                <option value="<?= $row['iduser'] ?>" <?= $selected;?> ><?= $row['namauser']; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                    
                    <?php /** [+] Update 21092022 */ ?>
                    <div class="group-body">
                        <form id="frm-simpan-pengaturan-tab">
                            <div class="form-group">
                                <label>Menyembunyikan Tab Kategori/BAB <span class="required">*</span></label>
                                <select name="hidden_akses_tab[]" class="form-control qlselect-multi" multiple="multiple">
                                <?php foreach( $allpic as $row): 
                                    $hidden_id  = explode(',', $iduserhiddenbab['value_pengaturan'] );
                                    $selected   = ( in_array( $row['iduser'],$hidden_id ) ) ? 'selected' : '';
                                    
                                    ?>
                                    <option value="<?= $row['iduser'] ?>" <?= $selected;?> ><?= $row['namauser']; ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-sm ">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> <!-- end col-md-6 -->
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-header">
                    <h5>Pengaturan Unit</h5>
                    <div class="tambah-unit">
                        <button class="btn btn-primary" type="button"><i class="fa fa-plus-circle"></i> Tambah</button>
                    </div>
                </div>
                <div class="panel-body unit-ql">
                    <div class="err-msg"></div>
                    <table class="table ql-dt table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Unit</th>
                                <th>Akses PIC Dokumen</th>
                                <th>User Akses</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php 
                            $no = 1;
                            foreach( $list_unit as $row ): ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $row['nama_unit']; ?></td>
                                <td>
                                    <ul style="padding-left: 20px;">
                                    <?php
                                     foreach($row['user_pic'] as $pic_row){
                                        echo '<li>'.$pic_row['namauser'].'</li>';
                                     } 
                                    ?>
                                    </ul>
                                </td>
                                <td>
                                    <!-- <ul style="padding-left: 20px;"> -->
                                    <?php
                                    //  foreach($row['user_login'] as $login_row){
                                        // echo '<li>'. $login_row['namauser'].'</li>';
                                        echo $row['user_login'];
                                    //  } 
                                    ?>
                                    <!-- </ul> -->
                                </td>
                                <td>
                                    <div class="btn-action">
                                        <a href="javascript:void(0);" style="color:blue;" class="edit-popup" data-id="<?= $row['idakre_unit']; ?>"><i class="fa fa-edit"></i></a>
                                        |
                                        <a href="javascript:void(0);" style="color:red;" class="delete-popup" data-id="<?= $row['idakre_unit']; ?>"><i class="fa fa-trash"></i></a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>


</section>