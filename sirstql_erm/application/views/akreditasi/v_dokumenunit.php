<style>
.table-item-regulasi tr td {
    padding: 10px !important;
    /* margin-bottom: 0; */
}

.table-item-regulasi {
    margin-bottom: 0 !important;
}

.uploadfile-unit {
    text-align: center;
}
.button-action button {
    border-radius: 100px;
}

table.item-file-table {
    width: 100%;
}

table.item-file-table tr td {
    padding: 0 0 5px 0 !important;
}

a.link-file-unit {
    padding: 5px;
    border-radius: 100px;
    text-align: center;
    margin: 0 5px;
}

</style>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <?php if( $cek_akses || $cek_userlogin_akre ): ?>
                <div class="panel-header" style="padding: 15px 15px 0px 15px;">
                    <a href="<?= base_url().'cakreditasi/tambah_regulasi'; ?>" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah</a>
                </div>
                <?php endif; ?>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Pilih Unit</label>
                                <select name="unit" class="form-control ql-select2 search-unit-akreditasi">
                                    <option value="">All</option>
                                    <?php foreach( $pic_regulasi as $row_picunit ): 
                                        $selected = ( $row_picunit['iduser'] == $userid_akre ) ? 'selected' : '';    
                                    ?>
                                        <option value="<?= $row_picunit['iduser']; ?>" <?= $selected; ?>><?= $row_picunit['nama_unit']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="err-msg-dok"></div>
                    <div class="replace-ajax">
                        <?= $listdokumen; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>