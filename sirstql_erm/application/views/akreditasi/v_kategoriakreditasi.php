<style>
    span.required{color:red;}
</style>
<!-- .content -->
<section class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="form-kat">
                <div class="panel panel-default">
                    <div class="panel-heading">Tambah Kategori</div>
                    <div class="panel-body">
                       
                            <div class="form-group">
                                <label>Nama <span class="required">*</span></label>
                                <input type="text" class="form-control" name="nama-kategori" id="nama-kategori" placeholder="Masukan Nama Kategori" autocomplete="off" required>
                                <input type="hidden" value="<?= $author; ?>"  class="form-control" name="username" id="username"  autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <select name="kat_parent" required id="kat_parent" class="form-control ql-select2">
                                    <option value="0" rolecategory="parent">Parent</option>"
            
                                   <?php foreach($select_level as $key => $row): ?>
                                        <option value="<?=$row["id"];?>" rolecategory="subparent"><?= $row["nama_kategori_title"];?></option>
                                        <?php 
                                            if( !empty( $row['sub'] ) ): 
                                                foreach($row['sub'] as $sub ): ?>
                                                <option value="<?= $sub['id']; ?>" rolecategory="childcategory">__<?= $sub['nama_kategori']; ?></option>
                                            <?php endforeach; // end.foreach sub ?>
                                        <?php endif // end.if sub ?>
                                    <?php endforeach; // end.foreach parent ?> 
                                       
                                </select>
                            </div>
                            
                            <?php if($author == 'superuser'): ?>
                            <div class="form-group">
                                <label>Pilih PIC <span class="required">*</span></label>
                                <select name="user-pic" class="form-control ql-select2" required>
                                    <option value="">Pilih PIC</option>
                                    <?php foreach($alluser as $row_user): ?>
                                        <option value="<?= $row_user['iduser']; ?>"><?= $row_user['namauser']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php endif; ?>

                            <div class="form-group">
                                <button class="btn btn-primary" type="button" id="btnsimpan" >Simpan</button>
                            </div>
                   
                    </div> <!-- end.panel-body -->
                </div> <!-- end.panel -->
            </div>
        </div> <!-- end.col-md-4 -->
        <div class="col-md-8">
            <div class="list-kat">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped ql-dt">
                            <thead>
                                <tr>
                                    <th width="10%" class="nomor">No</th>
                                    <th>Nama</th>
                                    <th>Level</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="tbodybagankategori">
                            
                            <?php 
                            $no = 1;
                            foreach( $list_data as $parent_kat ): ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $parent_kat['nama_kategori_title']; ?></td>
                                    <td> PARENT </td>
                                    <td> <button type="button" id="<?= $parent_kat['id']; ?>" role="parent" class="btnhapus btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Hapus"><i class="fa fa-trash"></i></button></td>
                                </tr>
                                <?php if( !empty($parent_kat['sub']) ): ?>
                                    <?php foreach( $parent_kat['sub'] as $sub_kat ): ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= '__'.$sub_kat['nama_kategori']; ?></td>
                                        <td><?= 'SUB - '.$parent_kat['nama_kategori_title']; ?></td>
                                        <td> <button type="button" id="<?= $sub_kat['id']; ?>" role="subparent" class="btnhapus btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Hapus"><i class="fa fa-trash"></i></button></td>
                                    </tr>
                                        <?php if( !empty( $sub_kat['child'] ) ): ?>
                                            <?php foreach( $sub_kat['child'] as $child_kat ): ?>
                                                <tr>
                                                    <td><?= $no++; ?></td>
                                                    <td><?= '____'.$child_kat['nama_kategori']; ?></td>
                                                    <td><?= 'CHILD - '.$sub_kat['nama_kategori']; ?></td>
                                                    <td> <button type="button" id="<?= $child_kat['id']; ?>" role="child" class="btnhapus btn btn-danger btn-xs" data-toggle="tooltip" data-original-title="Hapus"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                            <?php endforeach; // end.foreach child ?>
                                        <?php endif; // end.if child ?>
                                    <?php endforeach; // end.foreach sub ?>
                                <?php endif; // end.if sub ?>
                            <?php endforeach; // end.foreach parent ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> <!-- end.col-md-6 --> 
    </div> <!-- end.row -->
</section> 
<!-- end.content -->