<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="title">
                                <h5 style="font-weight:bold;">List Data Total Nilai Akreditasi</h5>
                            </div>
                            <div class="list-content">
                                <table class="ql-dt table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>BAB Akreditasi</th>
                                            <th>Presentase Nilai</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach($data_nilai as $row): 
                                            $average    =  (int) $row['total_nilai']/(int) $row['count_element'];
                                        ?>
                                        <tr>
                                            <td><?= $row['name']; ?></td>
                                            <td><?= round( $average*10,2 ).' %'; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="title">
                                <h5 style="font-weight:bold;">Chart Data Nilai Akreditasi</h5>
                            </div>
                            <div class="panel">
                                <div class="panel-body bg-info">
                                    <canvas id="akreditasichart" width="600" height="400"></canvas>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>