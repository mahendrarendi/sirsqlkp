<!-- Style -->
<style>
/* kategori */
.list-kategori .list-group a {
    display: inline-block;
    border-radius: 5px;
    line-height: normal;
}
</style>
<!-- End Style -->

<section class="content">
    <div class="ql-akreditasi">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <?php if( $cek_user_akses && $asesor == false   ): ?>
                        <div class="panel-heading">
                            <a href="<?= base_url().'cakreditasi/tambahdata'; ?>" class="btn btn-primary"><i class="fa fa-plus-square"></i> Tambah Data</a>
                            <a href="<?= base_url().'cakreditasi/uploaddocumen'; ?>" class="btn btn-warning"><i class="fa fa-plus"></i> Tambah Elemen Penilaian</a>
                            <a href="javascript:void(0);" class="btn btn-danger refresh-akre"><i class="fa fa-refresh"></i> Refresh</a>
                        </div>
                    <?php endif; ?>
                    <div class="panel-body">
                        <div class="list-kategori">
                            <div class="list-group">
                                <a href="javascript:void(0)" data-id="<?= 0 ?>" class="list-group-item"><?= strtoupper('All'); ?></a>
                                <?php foreach( $kategori as $row_kat ): ?>
                                    <a href="javascript:void(0)" data-id="<?= $row_kat['id']; ?>" class="list-group-item"><?= $row_kat['nama_kategori_title']; ?></a>
                                <?php endforeach; //end.foreach kategori ?>
                            </div>
                        </div>
                        <div class="data-dokumen">
                            <table class="table table-striped ql-dt">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Standar</th>
                                        <th>Elemen Penilaian</th>
                                        <th>Data</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="wrap-data"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div id="ql-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Mohon Tunggu Sebentar ...</h4>
            </div>
            <div class="modal-body">
                <p>Mohon Tunggu Sebentar ...</p>
            </div>
            <div class="modal-footer">...</div>
            </div>

        </div>
    </div>

</section>

