<style>
.regulasi-form tr td {
    padding-bottom: 15px !important;
}
</style>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form" id="frm-tambahregulasi">
                        
                        <div class="err-msg"></div>
                            
                        <div class="form-group">
                            <label>Pilih PIC UNIT <span class="required">*</span></label>
                            <select name="pic-unit" id="pic-change-regulasi" class="form-control ql-select2" required>
                                <option value="">Pilih</option>
                                <?php foreach( $pic_regulasi as $row_picregulasi ): 
                                    if( !empty($user_login) ): 
                                        if( $user_login == $row_picregulasi['iduser'] ){ ?>
                                            <option value="<?= $row_picregulasi['iduser']; ?>"><?= $row_picregulasi['nama_unit'] ?></option>
                                        <?php } ?>
                                    <?php else: ?>
                                        <option value="<?= $row_picregulasi['iduser']; ?>"><?= $row_picregulasi['nama_unit'] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        

                        <div class="form-group">
                            <label>Pilih Standar Regulasi <span class="required">*</span> </label>
                            <select name="parentkat"  class="form-control parentkat ql-select2" required>
                                <option value="">Pilih</option>
                            </select>
                            <input type="hidden" name="hiddenrole" class="hiddenrole">
                        </div>
                        <!-- <div class="form-group">
                            <button class="btn btn-primary btn-xs add-regulasi" type="button"><i class="fa fa-plus-circle"></i> Regulasi</button>
                        </div>
                        <div class="form-group">
                            <label>Regulasi <span class="required">*</span> </label>
                            <div class="regulasi-form">
                                <table width="100%">
                                    <tr colspan="2">
                                        <td><textarea name="regulasi[]" class="form-control regulasi" required placeholder="Masukan Point Regulasi ..."></textarea></td>
                                    </tr>
                                </table>
                            </div>
                        </div> -->
                        
                        <div class="form-group" style="text-align: right;">
                            <a class="btn btn-warning" href="<?= base_url('cakreditasi/dokumenunit'); ?>">Kembali</a>
                            <button class="btn btn-primary" type="submit"> <i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>