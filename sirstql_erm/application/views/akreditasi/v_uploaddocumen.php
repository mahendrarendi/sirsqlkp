<style>
.add-elemen {
    line-height: normal;
}
#tbl-elemen tr td {
    padding-left: 0 !important;
    padding-top: 0 !important;
    padding-bottom: 10px;
}
span.required{
    color:red;
}
</style>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Upload File
                </div>
                <input type="hidden" class="mode" value="<?= $mode; ?>">
                <form action="" method="post" class="form" id="frm-uploaddocumen" enctype="multipart/form-data">
                    
                    <div class="panel-body">
                        <div class="err-msg"></div>
                        <!-- <div class="row"> -->
                            <!-- <div class="col-md-6"> -->
                                <div class="form-group">
                                    <label>Pilih Kategori <span class="required">*</span> </label>
                                    <select name="parent-kategori" class="form-control ql-select2" id="selectkategoriparent" required>
                                        <option value="">Pilih Kategori</option>
                                        <?php foreach($select_parent as $parent_kat): ?>
                                            <option value="<?= $parent_kat['idparent'] ?>"><?= $parent_kat['nama_kategori_title']; ?></option>
                                        <?php endforeach; //end.foreach parent ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Sub Kategori <span class="required">*</span></label>
                                    <select required name="sub-kategori" class="form-control subkategori ql-select2">
                                        <option value="">Pilih Sub Kategori</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Child Sub Kategori <em>(Opsional)</em></label>
                                    <select name="child-sub-kategori" class="form-control child-subkategori ql-select2">
                                        <option value="">Pilih Child Sub Kategori</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Judul Elemen Penilaian <span class="required">*</span></label>
                                    <textarea name="judul_penilaian" class="form-control" placeholder="Masukan Judul Penilaian" required cols="30" rows="10"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Upload Files <span class="required">*</span></label>
                                    <input required type="file" class="form-control file_upload" name="file_upload[]" id="fileupload" accept="application/pdf"  multiple></td>
                                </div>
                            <!-- </div> -->
                        <!-- </div> -->

                    </div>
                    <div class="panel-footer">
                        <div class="form-group text-right" style="margin-bottom:0;">
                            <a href="<?= base_url().'cakreditasi/dokumenakreditasi'; ?>" class="btn btn-warning">Kembali</a>
                            <button type="submit" class="btn btn-primary" id="save-dokumentasi-file">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>