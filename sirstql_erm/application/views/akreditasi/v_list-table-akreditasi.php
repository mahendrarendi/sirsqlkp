<style>
.wrap-data ul.list-element {
    list-style: none;
    padding: 0;
}
table.table.list-element {
    margin-bottom: 0;
    width: 100%;
    max-width: 100%;
}

table.table.list-element td {
    padding: 10px;
}

table.table.list-element .link-file {
    color: black;
}

table.table.list-element .link-file i {
    color: #009688;
}
.ql-button-delete button {
    line-height: normal;
}
.link-file .fa-download {
    padding: 4px 6px 2px 6px;
    background: #009688;
    border-radius: 4px;
    line-height: normal;
    color: white !important;
}
.link-file .fa-eye {
    padding: 4px 6px 2px 6px;
    background: #ff9800;
    border-radius: 4px;
    line-height: normal;
    color: white !important;
}
.list-element td.no-available {
    text-align: center;
    vertical-align: middle;
}
ul.listfilename {
    padding-left: 15px;
}
a.delete-link-file {
    padding: 2px 6px;
    background: red;
    border-radius: 4px;
    color: white;
}
</style>

<table id="testtb" class="table table-bordered table-striped table-hover dt-responsive ql-dt">
    <thead class="header-table-ql">
        <tr>
            <th width="5%">No</th>
            <th>Standar</th>
            <th>Elemen Penilaian</th>
            <td>Aksi</td>
        </tr>
    </thead>
    <tbody class="wrap-data">
    <?php 
        $no = 1;
        foreach( $query_listbykat as $list ): 
            
            if( !empty($author) ){
                $cek_pic_akses  = ( $list['author'] == $author ) ? true : false;
            }else{
                $cek_pic_akses = true;
            }

            $judul_parent   = empty($list['judul']) ? 'Data Parent Belum Di Masukan, Silahkan Masukan Data Parent Terlebih dahulu' : $list['judul'];
        ?>  
        <tr>
            <td><?= $no++; ?></td>
            <td>
                <?= $list['subparent'].' : '.$judul_parent; ?>
                <?php if( $cek_pic_akses ): ?>
                    <div class="group-edit-standar">
                        <a href="javascript:void(0);" data-id="<?= $list['data_id']; ?>" class="edit-standar">[ Edit ]</a>
                    </div>
                <?php endif; ?>
            </td>
            <td>
                <?php 
                    if( !empty($list['files']) ): ?> 
                    <table class="table list-element table-bordered">
                <?php $a = 'a';
                    foreach( $list['files'] as $file ):
                        $replace_path   = str_replace("/sirstql","",$file['pathname']);
                        $urlnas         = URLNASSIMRS.$replace_path;
                        $class_file     = !empty($file['filename']) ? 'available-list':'no-available';

                    ?>
                        <tr>
                            <td width="30%">
                                <?= '<strong>'.$a++.'.</strong> '.$file['elemen_penilaian']; ?>
                                <?php if( $cek_pic_akses ): ?>
                                    <div class="group-edit-element">
                                        <a href="javascript:void(0);" data-id="<?= $file['id']; ?>" class="edit-element">[ Edit ]</a>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td width="40%" class="<?= $class_file; ?>">
                                <?php if( !empty($file['filename']) ): ?>
                                    <strong>Data : </strong>
                                    <ul class="listfilename">
                                    <?php foreach ( unserialize( $file['filename'] ) as $key => $row_file): ?>
                                        <li>
                                            <a data-toggle="tooltip" data-original-title="<?= $row_file; ?>" target="_blank" class="link-file" href="<?= $urlnas.$row_file; ?>"> <?= $this->namefile_maxlength($row_file); ?> <i class="fa fa-eye"></i> </a> 
                                            <?php if( $cek_pic_akses ): ?>
                                            <a data-toggle="tooltip" data-id="<?= $file['id']; ?>" data-key="<?= $key; ?>" data-original-title="Hapus File" class="delete-link-file" href="javascript:void(0)"><i class="fa fa-trash"></i> </a> 
                                            <?php endif; ?>
                                        </li>
                                    <?php endforeach; ?>
                                    </ul>
                                    <?php if( $cek_pic_akses ): ?>
                                        <div class="tambahan-upload-file" style="text-align:center;">
                                            <a href="javascript:void(0)" class="tambahan-popup-upload" data-id="<?= $file['id']; ?>"> [ Tambah Document ]</a>
                                        </div>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php if( $cek_pic_akses ): ?>
                                    <div class="clik-popup-file">
                                        <a href="javascript:void(0)" class="popup-upload" data-id="<?= $file['id']; ?>">Upload Document ()</a>
                                    </div>
                                    <?php else: ?>
                                        <p>File Dokumen Belum Tersedia</p>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </td>
                            <td width="20%">
                                <strong>Nilai : <?= $file['nilai']; ?></strong>
                                <?php if( $cek_pic_akses || $author_asesor ): ?>
                                    <div class="edit-nilai-popup">
                                        <a href="javascript:void(0)" class="edit-nilai" data-id="<?= $file['id']; ?>" data-nilai="<?= $file['nilai']; ?>">[ Edit Nilai ]</a>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <td>
                                <strong>Rekomendasi</strong>
                                <?php if( $cek_pic_akses || $author_asesor ): ?>
                                    <div class="edit-rekomendasi">
                                        <?php if(!empty($file['rekomendasi'])): ?>
                                            <div class="alert alert-success" role="alert" style="margin-bottom:2px;padding: 5px;">
                                                <?= $file['rekomendasi']; ?>
                                            </div>
                                        <?php endif; ?>
                                        <a href="javascript:void(0)" class="edit-rekomendasi-element" data-id="<?= $file['id']; ?>" > 
                                            <?= ( empty($file['rekomendasi']) ) ? '[ Add ]' :'[ Edit ]'; ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </td>
                            <?php if( $cek_pic_akses ): ?>
                                <td width="20%">
                                    <div class="ql-button-delete">
                                        <button type="button" data-toggle="tooltip" data-original-title="Hapus Elemen" data-id="<?= $file['id']; ?>" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </div>                                   
                                </td>
                            <?php endif; ?>
                        </tr>
                   <?php endforeach; ?>
                    </table>
                <?php else: ?>
                    <div class="file-upload-action">
                        <p>Data Element Belum di Input</p>
                    </div>
                <?php endif; ?>
            </td>
            <td>
                <?php if( $cek_pic_akses ): ?>
                    <?php if(empty($list['child'])): ?>
                        <button type="button" data-toggle="tooltip" data-original-title="Hapus Sub Kategori" data-id="<?= $list['data_id']; ?>" class="btn btn-danger delete-group-akreditasi"><i class="fa fa-minus-circle"></i></button>
                    <?php endif; ?>
                <?php endif; ?>
            </td>
        </tr>
        <?php if(!empty($list['child'])): ?>
            <?php foreach( $list['child'] as $row_child ): ?>
            <tr>
                <td><?= $no++; ?></td>
                <td>
                    <?= $row_child['childname'].' : '.$row_child['judul']; ?>
                    <?php if( $cek_pic_akses ): ?>
                        <div class="group-edit-standar">
                            <a href="javascript:void(0);" data-id="<?= $row_child['data_id']; ?>" class="edit-standar">[ Edit ]</a>
                        </div>
                    <?php endif; ?>
                </td>
                <td>
                    <?php 
                        if( !empty($row_child['files']) ): ?> 
                        <table class="table list-element table-bordered">
                    <?php $a = 'a';
                        foreach( $row_child['files'] as $file ):
                            $replace_path   = str_replace("/sirstql","",$file['pathname']);
                            $urlnas         = URLNASSIMRS.$replace_path;
                            $class_file     = !empty($file['filename']) ? 'available-list':'no-available';
                        ?>
                            <tr>
                                <td width="30%">
                                    <?= '<strong>'.$a++.'.</strong> '.$file['elemen_penilaian']; ?>
                                    <?php if( $cek_pic_akses ): ?>
                                        <div class="group-edit-element">
                                            <a href="javascript:void(0);" data-id="<?= $file['id']; ?>" class="edit-element">[ Edit ]</a>
                                        </div>
                                    <?php endif; ?>
                                </td>
                                <td width="40%" class="<?= $class_file; ?>">
                                    <?php if( !empty($file['filename']) ): ?>
                                        <strong>Data : </strong>
                                        <ul class="listfilename">
                                        <?php foreach ( unserialize( $file['filename'] ) as $key => $row_file): ?>
                                                <li>
                                                    <a data-toggle="tooltip" data-original-title="<?= $row_file; ?>" class="link-file" href="<?= $urlnas.$row_file; ?>"> <?= $this->namefile_maxlength($row_file); ?> <i class="fa fa-eye"></i> </a> 
                                                    <?php if( $cek_pic_akses ): ?>
                                                    <a data-toggle="tooltip" data-id="<?= $file['id']; ?>" data-key="<?= $key; ?>" data-original-title="Hapus File" class="delete-link-file" href="javascript:void(0)"><i class="fa fa-trash"></i> </a> 
                                                    <?php endif; ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php else: ?>
                                        <?php if( $cek_pic_akses ): ?>
                                            <div class="clik-popup-file">
                                                <a href="javascript:void(0)" class="popup-upload" data-id=<?= $file['id']; ?>>Upload Document ()</a>
                                            </div>
                                        <?php else: ?>
                                            <p>File Dokumen Belum Tersedia</p>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </td>
                                
                                <td width="20%">
                                    <strong>Nilai : <?= $file['nilai']; ?></strong>
                                    <?php if( $cek_pic_akses ): ?>
                                        <div class="edit-nilai-popup">
                                            <a href="javascript:void(0)" class="edit-nilai" data-id="<?= $file['id']; ?>" data-nilai="<?= $file['nilai']; ?>">[ Edit Nilai ]</a>
                                        </div>
                                    <?php endif; ?>
                                    </td>
                                <?php if( $cek_pic_akses ): ?>
                                    <td width="20%">
                                        <div class="ql-button-delete">
                                            <button type="button" data-toggle="tooltip" data-original-title="Hapus Elemen" data-id="<?= $file['id']; ?>" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus Elemen</button>
                                        </div>                                   
                                    </td>
                                <?php endif; ?>
                            </tr>
                    <?php endforeach; ?>
                        </table>
                    <?php else: ?>
                        Data Element Belum di Input
                    <?php endif; ?>
                </td>
                <td>
                    <?php if( $cek_pic_akses ): ?>
                        <button type="button" data-toggle="tooltip" data-original-title="Hapus Child Kategori" data-id="<?= $row_child['data_id']; ?>" class="btn btn-danger delete-group-akreditasi"><i class="fa fa-minus-circle"></i></button>
                    <?php endif; ?>
                </td>
            </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    <?php  endforeach; ?>
    </tbody>
</table>
