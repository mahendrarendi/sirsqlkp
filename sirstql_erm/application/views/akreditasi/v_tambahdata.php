<style>
.add-elemen {
    line-height: normal;
}
#tbl-elemen tr td {
    padding-left: 0 !important;
    padding-top: 0 !important;
    padding-bottom: 10px;
}
span.required{
    color:red;
}
#tb-addelement .form-group {
    margin-bottom: 0;
    padding: 10px 10px;
}

#tb-addelement tr td:last-child {
    text-align: center;
    vertical-align: middle;
}
</style>

<section class="content">

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tambah
                </div>
                <form action="" class="form" id="frm-dokumentasi">
                    <div class="panel-body">
                        <div class="err-msg"></div>
                        <input type="hidden" name="username" value="<?= $author; ?>">
                        <!-- <div class="row"> -->
                            <!-- <div class="col-md-6"> -->
                                
                            <?php if($author == 'superuser'): ?>
                            <div class="form-group">
                                <label>Pilih PIC <span class="required">*</span></label>
                                <select name="user-pic" class="form-control ql-select2" required>
                                    <option value="">Pilih PIC</option>
                                    <?php foreach($alluser as $row_user): ?>
                                        <option value="<?= $row_user['iduser']; ?>"><?= $row_user['namauser']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php endif; ?>

                                <div class="form-group">
                                    <label>Pilih Kategori <span class="required">*</span> </label>
                                    <select name="parent-kategori" class="form-control ql-select2" id="selectkategoriparent" required>
                                        <option value="">Pilih Kategori</option>
                                        <?php foreach($select_parent as $parent_kat): ?>
                                            <option value="<?= $parent_kat['idparent']; ?>"><?= $parent_kat['nama_kategori_title']; ?></option>
                                        <?php endforeach; //end.foreach parent ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Sub Kategori <span class="required">*</span></label>
                                    <select name="sub-kategori" class="form-control subkategori ql-select2" required>
                                        <option value="">Pilih Sub Kategori</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Child Sub Kategori</label>
                                    <select name="child-sub-kategori" class="form-control child-subkategori ql-select2">
                                        <option value="">Pilih Child Sub Kategori</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Standar Judul <span class="required">*</span> </label>
                                    <textarea required name="standar-judul" placeholder="Masukan Standar Judul" class="form-control" cols="30" rows="10"></textarea>
                                </div>

                                <div class="form-group">
                                    <label style="display:block;"> [+] Element Penilaian </label>
                                    <button class="btn btn-info add-element" type="button"><i class="fa fa-plus-square"></i> Tambah </button>
                                </div>
                                
                                <table class="table table-bordered" id="tb-addelement">
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <div class="form-group">
                                                    <textarea placeholder="Masukan Elemen Penilaian ..." class="form-control item-elemen-txt" name="itemelemen[]" required></textarea>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            <!-- </div> -->
                            
                        <!-- </div> -->

                    </div>
                    <div class="panel-footer">
                        <div class="form-group text-right" style="margin-bottom:0;">
                            <a href="<?= base_url().'cakreditasi/dokumenakreditasi'; ?>" class="btn btn-warning">Kembali</a>
                            <button  class="btn btn-primary" id="save-dokumentasi">Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>