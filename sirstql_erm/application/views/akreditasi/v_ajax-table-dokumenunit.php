<table class="table table-striped table-hover ql-dt">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Unit</th>
                                    <th>Standar Judul</th>
                                    <th>Regulasi</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no = 1;
                                foreach( $listdokumen as $row ): 
                                    $picarray = $row['picarray'];
                                    $cek_akses_by_pic_set = ( in_array($author,$picarray) ) ? true : false;
                                ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['nama_unitpelayanan']; ?></td>
                                    <td><?= $row['namakategori']; ?></td>
                                    <td>
                                        <table class="table table-striped table-item-regulasi">
                                            <?php
                                            $a = 'a'; 
                                            foreach( $row['item_regulasi'] as  $row_item ): 
                                                $replace_path   = str_replace("/sirstql","",$row_item['pathname']);
                                                $urlnas         = URLNASSIMRS.$replace_path;
                                            ?>
                                            <tr>
                                                <td width="50%">
                                                    <?php if( !empty( $row_item['filename'] ) ): ?>
                                                        <table class="item-file-table table-hover">
                                                            <?php foreach(  unserialize( $row_item['filename'] ) as $key => $file ): ?>
                                                            <tr>
                                                                <td><?= namefile_maxlength_string($file); ?></td>
                                                                <td style="text-align:center;">
                                                                    <a data-toggle="tooltip" data-placement="top" title="<?= 'Lihat Dokumentasi' ?>" target="_blank" class="link-file-unit btn-success btn-xs" href="<?= $urlnas.$file; ?>"><i class="fa fa-eye"></i> </a> 
                                                                    <?php if( $row['author'] == $author || $cek_pic_superuser == true || $cek_akses_by_pic_set == true ): ?>
                                                                    <button style="border-radius:100px;" data-keyfile=<?= $key; ?>  data-toggle="tooltip" data-placement="top" title="Hapus Dokumentasi" class="delete-file-unit btn-danger btn-xs" type="button" data-id="<?= $row_item['id']; ?>"><i class="fa fa-close"></i></button>
                                                                    <?php endif; ?>
                                                                </td>
                                                            </tr>
                                                            <?php endforeach; ?>
                                                        </table>
                                                    <?php else: ?>
                                                        <table class="item-file-table table-hover">
                                                            <tr>
                                                                <td style="text-align:center;">Maaf Data Belum Tersedia</td>
                                                            </tr>
                                                        </table>
                                                    <?php endif; ?>
                                                    <div class="uploadfile-unit">
                                                        <?php if( $row['author'] == $author || $cek_pic_superuser == true || $cek_akses_by_pic_set == true ): ?>
                                                            <a href="javascript:void(0);" data-id="<?= $row_item['id']; ?>" class="link-upload-popup">Upload File ()</a>
                                                        <?php endif; ?>
                                                    </div>
                                                </td>
                                                <td width="20%" style="vertical-align:middle;text-align:center;">
                                                    <div class="button-action">
                                                    <?php if( $row['author'] == $author || $cek_pic_superuser == true || $cek_akses_by_pic_set == true ): ?>
                                                        <button type="button" data-id="<?= $row_item['id']; ?>"  data-toggle="tooltip" data-placement="top" title="Hapus Item Regulasi" class="btn btn-danger delete-item-regulasi btn-sm"><i class="fa fa-trash"></i></button>
                                                    <?php endif; ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    </td>
                                    <td style="vertical-align:middle;text-align:center;">
                                        <div class="button-action">
                                            <?php if( $row['author'] == $author || $cek_pic_superuser == true ||  $cek_akses_by_pic_set == true ): ?>
                                                <button type="button" data-id="<?= $row['id']; ?>" data-toggle="tooltip" data-placement="top" title="Hapus Regulasi" class="btn btn-danger delete-regulasi btn-sm"><i class="fa fa-close"></i></button>
                                            <?php endif; ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>