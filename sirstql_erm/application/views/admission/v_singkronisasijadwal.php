<!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <div class="box" style="padding-top:4px;">
            <table style="font-size: 10px;" id="dtpendaftaran" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="header-table-ql"> 
                <th>No</th>
                <th>Nama Unit</th>
                <th>Belum Diunggah</th>
                <th>Diunggah</th>
                <th width="200"></th>
              </tr>
              </thead>
              <tbody>
                  <?php 
                    if(isset($unit))
                    {
                        $no=0;
                        foreach ($unit as $arr)
                        {
                            echo "<tr>"
                                    . "<td>". ++$no ."</td>"
                                    . "<td>".$arr['namaunit']."</td>"
                                    . "<td>".$arr['jumlah_jadwal'].(($arr['jumlah_jadwal'] == 0) ? '' : ' jadwal' )." </td>"
                                    . "<td>".$arr['jumlah_singkron'].(($arr['jumlah_singkron'] == 0) ? '' : ' jadwal' )." </td>"
                                    . "<td> ".(($arr['jumlah_jadwal'] == 0) ? "" : " <a ".  ql_tooltip('Unggah ke Website')." id='singkronisasi_unggahjadwal' idunit='".$arr['idunit']."' unit='".ucwords(strtolower($arr['namaunit']))."' class='btn btn-primary btn-xs'><i class='fa fa-upload'></i> unggah</a> <a ".  ql_tooltip('Hapus Jadwal di Website')." id='singkronisasi_hapusjadwal' idunit='".$arr['idunit']."' unit='".ucwords(strtolower($arr['namaunit']))."' class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> hapus</a> " )." </td></tr>";
                        }
                    }
                  ?>
              </tfoot>
            </table>
        </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->