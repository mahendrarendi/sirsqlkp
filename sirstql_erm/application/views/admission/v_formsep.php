<style>
.inline-header {
  display: inline-block;
  margin: 0 10px;
  vertical-align: middle;
}
.inline-header h3 {
  margin-top: 0px;
}
.form-group.pasien-dt {
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.form-group.pasien-dt .form-inline input[name="status"] {
  width: 100%;
}

.form-group.tgl-group {
  display: flex;
  justify-content: space-between;
}
</style>

    <?php 
        if( $getpeserta_bpjs['code'] != 200 ): ?>
            <div class="alert alert-warning"><?= $getpeserta_bpjs['code'].'-'.$getpeserta_bpjs['response']; ?></div>
    <?php return false; endif; ?>

    <?php 

        $row_response = $getpeserta_bpjs['response'];
        $peserta      = $row_response['peserta'];

        // $data_pendaftaran
        // $getdatarencanakontrol_db
        echo '<pre>';
        print_r($data_pendaftaran);
        print_r( $getdatarencanakontrol_db );
        echo '</pre>';

    ?>

<div class="row">
    <div class="col-md-2">
        <!-- panel -->
        <div class="panel panel-default">
            <div class="panel-heading">
                Riwayat Periksa
            </div>
            <div class="panel-body">
                Body TEST
            </div>
        </div>
        <!-- end panel -->
    </div>
    <div class="col-md-10">
        <form class="form" id="frm-buatsep">
            <div class="row">
                <div class="col-md-4">
                    <!-- panel -->
                    <div class="panel panel-default data-pasien">
                        <div class="panel-heading">
                            Data Pasien
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Norm</label>
                                <input value="<?= $peserta['mr']['noMR']; ?>" type="text" required readonly class="form-control" autocomplete="off" name="norm">
                            </div>
                            <div class="form-group">
                                <label>Nama Pasien</label>
                                <input type="text" value="<?= $peserta['nama']; ?>" required readonly class="form-control" autocomplete="off" name="namapasien">
                            </div>
                            <div class="form-group">
                                <label>No.Kartu</label>
                                <input type="text" value="<?= $peserta['noKartu']; ?>" required readonly class="form-control" autocomplete="off" name="noka">
                            </div>
                            <div class="form-group">
                                <label>NIK</label>
                                <input type="text" value="<?= $peserta['nik']; ?>" required readonly class="form-control" autocomplete="off" name="nik">
                            </div>
                            <div class="form-group pasien-dt">
                                <div class="form-inline">
                                    <label>Jenis Kelamin</label>
                                    <select name="jeniskelamin" class="form-control" required>
                                        <option value="">-</option>
                                        <option value="laki-laki" <?= (($peserta['sex'] == 'L') ? 'selected' : ''); ?>>Laki-Laki</option>
                                        <option value="wanita"<?= (($peserta['sex'] == 'P') ? 'selected' : ''); ?>>Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-inline">
                                    <label>Status</label>
                                    <input type="text" value="<?= $peserta['statusPeserta']['keterangan']; ?>" required readonly class="form-control" autocomplete="off" name="status">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tgl.Lahir</label>
                                <input type="text" value="<?= $peserta['tglLahir']; ?>" readonly required class="form-control" autocomplete="off" name="tangallahir">
                            </div>
                            <div class="form-group">
                                <label>Hak Kelas</label>
                                <input type="text" value="<?= $peserta['hakKelas']['keterangan']; ?>" readonly required class="form-control" autocomplete="off" name="hakkelas">
                            </div>
                            <div class="form-group">
                                <label>Jenis Peserta</label>
                                <input type="text" value="<?= $peserta['jenisPeserta']['keterangan']; ?>" readonly required class="form-control" autocomplete="off" name="jenispeserta">
                            </div>
                            <div class="form-group">
                                <label>Tgl. Cetak Kartu</label>
                                <input type="text" value="<?= $peserta['tglCetakKartu']; ?>" readonly required class="form-control" autocomplete="off" name="tanggal_cetakkartu">
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <div class="col-md-8">
                    <!-- panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kelengkapan Data SEP
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>Spesialis/SubSpesialis</label>
                                <select name="kode_poli" class="form-control ql-select2-poli" style="width: 100%"></select>
                            </div>
                            <div class="form-group">
                                <div class="checkbox-inline">
                                        <label><input type="checkbox" value="1" name="eksekutif">Eksekutif</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" value="1" name="pesertacob">Peserta COB</label>
                                    </div>
                                    <div class="checkbox-inline">
                                        <label><input type="checkbox" value="1" name="katarak">Pasien Katarak</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>DPJP yang melayani</label>
                                <select name="dpjp" class="form-control" required>
                                    <option value="">-</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>PPK Asal Rujukan</label>
                                <input type="text" readonly required class="form-control" autocomplete="off" name="asalrujukan">
                            </div>
                            <div class="form-group tgl-group">
                                <div class="form-inline">
                                    <label>Tgl.Rujukan</label>
                                    <input type="text" readonly name="tanggalrujukan" class="form-control date-ql" required>
                                    <div style="text-align: right;"><small>Format Tanggal Y-m-d</small></div>
                                </div>
                                <div class="form-inline">
                                    <label>Tgl.SEP</label>
                                    <input type="text" readonly name="tanggalsep" class="form-control date-ql" required>
                                    <div style="text-align: right;"><small>Format Tanggal Y-m-d</small></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>No.Rujukan</label>
                                <input type="text" class="form-control" placeholder="Masukan Nomor Rujukan ..." autocomplete="off" name="norujukan">
                            </div>
                            <div class="form-group">
                                <label>No.SEP</label>
                                <input type="text" readonly class="form-control" autocomplete="off" name="nosep">
                            </div>
                            <div class="form-group">
                                <label>No.Surat Kontrol</label>
                                <input type="text" placeholder="Masukan Nomor Surat Kontrol ..." class="form-control" autocomplete="off" name="nosuratkontrol">
                            </div>
                            <div class="form-group">
                                <label>DPJP pemberi Surat SKDP/SPRI</label>
                                <input type="text" class="form-control" autocomplete="off" name="namaDokterPembuat_spri">
                            </div>
                            <div class="form-group">
                                <label>Apa tujuan kunjungan ini?</label>
                                <select name="tujuan_kunjungan" class="form-control" required>
                                    <option value="0">-</option>
                                    <option value="1">Prosedur</option>
                                    <option value="2">Konsul Dokter</option>
                                </select>
                            </div>
                            <div class="panel panel-default tujuan-prosedur hide">
                                <div class="panel-body">
                                    <div class="form-group ">
                                        <label>Flag Procedure</label>
                                        <select class="form-control" required name="flag_procedure">
                                          <option value="">-</option>
                                          <option value="0">Prosedur Tidak Berkelanjutan</option>
                                          <option value="1">Prosedur dan Terapi Berkelanjutan</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Penunjang</label>
                                        <select class="form-control" required name="kdpenunjang">
                                          <option value="">-</option>
                                          <option value="1">Radioterapi</option>
                                          <option value="2">Kemoterapi</option>                                  
                                          <option value="3">Rehabilitasi Medik</option>                                  
                                          <option value="4">Rehabilitasi Psikososial</option>                                  
                                          <option value="5">Transfusi Darah</option>
                                          <option value="6">Pelayanan Gigi</option>
                                          <option value="7">Laboratorium</option>
                                          <option value="8">USG</option>
                                          <option value="9">Farmasi</option>                                  
                                          <option value="10">Lain-lain</option>
                                          <option value="11">MRI</option>                                  
                                          <option value="12">HEMODIALISA</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default tujuan-konsul-dokter hide">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label>Penunjang</label>
                                        <select class="form-control" required name="kdpenunjang">
                                          <option value="">-</option>
                                          <option value="1">Radioterapi</option>
                                          <option value="2">Kemoterapi</option>                                  
                                          <option value="3">Rehabilitasi Medik</option>                                  
                                          <option value="4">Rehabilitasi Psikososial</option>                                  
                                          <option value="5">Transfusi Darah</option>
                                          <option value="6">Pelayanan Gigi</option>
                                          <option value="7">Laboratorium</option>
                                          <option value="8">USG</option>
                                          <option value="9">Farmasi</option>                                  
                                          <option value="10">Lain-lain</option>
                                          <option value="11">MRI</option>                                  
                                          <option value="12">HEMODIALISA</option>
                                        </select>
                                    </div>
                                    <div class="form-group ">
                                        <label>Mengapa pelayanan ini tidak diselesaikan pada hari yang sama sebelumnya?</label>
                                        <select class="form-control" required name="assesment_pel">
                                            <option value="">-</option>
                                            <option value="1">Poli spesialis tidak tersedia pada hari sebelumnya</option>
                                            <option value="2">Jam Poli telah berakhir pada hari sebelumnya</option>
                                            <option value="3">Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya</option>
                                            <option value="4">Atas Instruksi Rumah Sakit</option>
                                            <option value="5">Tujuan Kontrol</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Diagnosa</label>
                                <select name="diagnosa" class="form-control select2ql" style="width: 100%"></select>
                            </div>
                            <div class="form-group">
                                <label>No.Telpon</label>
                                <input type="text" required class="form-control" autocomplete="off" name="notelpon">
                            </div>
                            <div class="form-group">
                                <label>Catatan</label>
                                <textarea name="catatan" class="textarea form-control" placeholder="Ketik Catatan Apabila Ada"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Status Kecelakaan</label>
                                <select name="status_kecelakaan" class="form-control" required>
                                    <option value="0">Bukan Kecelakaan</option>
                                    <option value="1">Kecelakaan Lalu Lintas</option>
                                    <option value="2">Kecelakaan Lalu Lintas dan Kecelakaan Kerja</option>
                                    <option value="3">Kecelakaan Kerja</option>
                                </select>
                            </div>
                            <div class="form-group" style="text-align:right;">
                                <button type="button" class="btn btn-danger" data-dismiss="modal"> <i class="fa fa-close"></i> Batal</button>
                                <button class="btn btn-success" type="submit"><i class="fa fa-plus-circle"></i> Buat SEP</button>
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
            </div>
            
        </form>
    </div>
</div>