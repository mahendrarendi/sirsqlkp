<style type="text/css">
  .form-group { margin-bottom: 10px;}
  .col-sm-1{padding: 4px;margin:0px; margin-left:20px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="col-md-6 row">
                <input id="tglawal" type="text" size="7" class="datepicker" name="tanggal" placeholder="Tanggal awal">
                <input id="tglakhir" type="text" size="7" placeholder="Tanggal akhir" class="datepicker" name="tanggal">
                <a class="btn btn-info btn-sm" id="tampil" ><i class="fa fa-desktop"></i> Tampil</a>
                <a class="btn btn-warning btn-sm" id="reload"><i class="fa fa-refresh"></i> Refresh</a>
            </div>
            <table id="listdata" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">
                  <th>No</th>
                  <th>NO.RM</th>
                  <th>NAMA PASIEN</th>
                  <th>WAKTU DAFTAR</th>
                  <th><i class="fa fa-clock-o"></i> DIPERIKSA DOKTER</th>
                  <th>CARA DAFTAR</th>
                  <th>POLI</th>
                  <th>JENIS PERIKSA</th>
                  <th>CARA BAYAR</th>
                  <th width="120px">CETAK</th>
                  <th>AKSI</th>
                </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->