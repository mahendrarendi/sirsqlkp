<section class="content">
  <div class="row">
      
    <div class="col-xs-12">
       <div class="callout callout-info col-md-6 row">
        <p>Data Jadwal Pendaftaran digunakan untuk pilihan jadwal saat mendaftarkan pasien rawat jalan, apabila data jadwal masih kosong silakan di generate.</p>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="box">
        <!-- <div class="box-header">
          <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> 
        </div> -->
        <!-- /.box-header -->
        <div class="box-body" >
          <div class="col-md-6 row">
          <a onclick="generate_jadwal()" class="btn btn-primary btn-sm" data-toggle="tooltip" data-original-title="Generate Jadwal" href="#"><i class="fa fa-plus-square"></i> Generate</a>
          <input type="text" class="datepicker caritanggal" id="cariawal" size="7"> <input type="text" class="datepicker caritanggal" id="cariakhir" size="7"> 
          <a id="tampil" class="btn btn-info btn-sm"><i class="fa  fa-desktop"></i> Tampil</a>
          <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
          <a id="hapus" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus Jadwal</a>
          </div>
          <table id="tableMasterJadwal" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
            <thead>
            <tr class="header-table-ql">
              <th>POLIKLINIK</th>
              <th>LOKET</th>
              <th>DOKTER</th>
              <th>MULAI</th>
              <th>AKHIR</th>
              <th>QUOTA</th>
              <th>STATUS</th>
              <th>CATATAN</th>
              <th>GRUP</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->