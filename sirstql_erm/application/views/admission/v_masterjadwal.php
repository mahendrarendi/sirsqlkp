<section class="content">
  <div class="row">
      
    <div class="col-xs-12">
       <div class="callout callout-info col-md-6 row">
        <p>Data Master Jadwal digunakan untuk mengatur jadwal, yang akan digunakan untuk hasil generate di Halaman Input Jadwal.</p>
      </div>
    </div>
      
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body" >
          <div class="col-md-6 row">
            <a onclick="masterjadwalAdd()" class="btn btn-primary btn-sm" data-toggle="tooltip" data-original-title="Tambah Jadwal baru" href="#"><i class="fa fa-plus-square"></i> Jadwal Baru</a>
            <a onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
          </div>
          <table id="dtmasterjadwal" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
            <thead>
            <tr class="header-table-ql">
              <th width="17%">POLIKLINIK</th>
              <th width="17%">DOKTER</th>
              <th>Ahad</th>
              <th>Senin</th>
              <th>Selasa</th>
              <th>Rabu</th>
              <th>Kamis</th>
              <th>Jum'at</th>
              <th>Sabtu</th>
            </tr>
            </thead>
            <tbody>            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->