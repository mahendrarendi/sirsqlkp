<!-- Main content -->
 <section class="content">
   <div class="row">
     <div class="col-xs-12">
       <!-- mode view -->
       <div class="box">
         <!-- /.box-header -->
         <div class="box-body">
           <div class="col-md-6 row">
              <a id="add" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Tambah</a>
              <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> 
            </div>
           <table id="personmember" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
             <thead>
             <tr class="header-table-ql">
               <th>No</th>
               <th>No.RM</th>
               <th>Nama Pasien</th>
               <th>Tgl Lahir</th>
               <th>Alamat</th>
               <th></th>
             </tr>
             </thead>
             <tbody>
             </tfoot>
           </table>
         </div>
         <!-- /.box-body -->
       </div>
       <!-- /.box -->
       <!-- end mode view -->

   </div>
   <!-- /.col -->
 </div>
 <!-- /.row -->
</section>
<!-- /.content -->