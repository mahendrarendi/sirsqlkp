<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if($mode=='view'){ ?>
        <div class="box">
          <!-- <div class="box-header">
            <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3> 
          </div> -->
        <div class="box-body">
          <div class="col-md-6 row">
            <?php if($this->pageaccessrightbap->checkAccessRight(V_MENUAKSIDATAINDUK)){?>
              <a href="<?= base_url('cadmission/add_datainduk')?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Entri Data</a>
              <a onclick="get_dataindukexcel()" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh</a>
              <a id="rmganda" class="btn btn-danger btn-sm"><i class="fa fa-book"></i> Pindah RM</a>
              <a id="pindahriwayat" class="btn btn-danger btn-sm"><i class="fa fa-book"></i> Pindah Riwayat Periksa</a>
            <?php } ?>
            <a onclick="refresh_page()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
            <!-- <div class="input-group"><input onchange="get_dataindukbytgllahir(this.value)" size="6" placeholder=" Tanggal lahir" type="text" class="form-control datepicker"><span class="input-group-btn"><button type="button" class="btn btn-info btn-flat" onclick="$.alert(\'Untuk mencari pasien berdasarkan tanggallahir, masukan tanggal pada inputan.\')"><i class="fa fa-question"></i></button></span></div> -->
          </div>
          <table id="datainduk" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
              <thead>
              <tr class="bg bg-yellow-gradient">
                <th>NIK</th>
                <th>No.RM</th>
                <th>NamaLengkap</th>
                <th>TanggalLahir</th>
                <th>Kelamin</th>
                <th>NoTelpon</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody id="displayBytgllahir"></tbody>
            </table>
          </div>
          </div>

        <?php } else if($mode=='edit' OR $mode=='add'){ ?>
        <!-- jika mode ubah data induk -->
        <div class="box">
        <div class="box-body">
          <form action="<?= base_url('cadmission/save_datainduk');?>" method="POST" class="form-horizontal" id="FormDatainduk">
            <?php 
              $this->encryptbap->generatekey_once("HIDDENTABEL"); 
              $id = $this->encryptbap->encrypt_urlsafe(((!empty($data_edit['idperson']))? $data_edit['idperson'] : ''), "json");
            ?>
            <input type="hidden" name="idperson" value="<?=$id;?>">
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">No.Identitas*</label>
              <div class="col-sm-3">
                <input type="text" class="form-control" id="noidentitas" name="noidentitas" placeholder="No. Identitas" value="<?= ((!empty($data_edit['nik']))? $data_edit['nik'] : ''); ?>">
              </div>
              <label for="" class="col-sm-1 control-label">Kelamin*</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="jeniskelamin" name="jeniskelamin">
                <option value="0">Pilih</option>
                <?php foreach($dt_jeniskelamin as $row){ echo '<option value="'.$row.'" '. (($row==$data_edit['jeniskelamin'] && !empty($data_edit['jeniskelamin']) ) ? 'selected' : '' ) .'>'.$row.'</option>'; }?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">No.RM*</label>
              <div class="col-sm-2">
                <input type="text" readonly name="norm" value="<?= ((!empty($data_edit['norm']))?$data_edit['norm']:'') ?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">NamaPasien*</label>
              <div class="col-sm-3"><input type="text" class="form-control" name="namalengkap" placeholder="Nama Pasien" value="<?= ((!empty($data_edit['namalengkap']))?$data_edit['namalengkap']:''); ?>"></div>
              <label for="" class="col-sm-1 control-label">Agama</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="agama" name="agama">
                <option value="0">Pilih</option>
                <?php foreach ($dt_agama as $row){echo '<option value="'.$row.'" '. (($row==$data_edit['agama']) ? 'selected' : '' ) .'>'.$row.'</option>';} ?>
                </select>
              </div>
              <label for="" class="col-sm-2 control-label">No.JKN</label>
              <div class="col-sm-2">
                <input type="text" name="nojkn" value="<?= ((!empty($data_edit['nojkn']))?$data_edit['nojkn']:''); ?>" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">NamaPemanggilan</label>
               <div class="col-sm-3"><input type="text" class="form-control" name="namalengkappemanggilan" placeholder="Nama Pasien" value="<?= ((!empty($data_edit['namalengkappemanggilan']))?$data_edit['namalengkappemanggilan']:''); ?>"></div>
              <label for="" class="col-sm-1 control-label">GolDarah</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="golongandarah" name="golongandarah">
                <option value="0">Pilih</option>
                <?php foreach ($dt_golongandarah as $row){ echo '<option value="'.$row.'" '. (($row==$data_edit['golongandarah']) ? 'selected' : '' ) .'>'.$row.'</option>';}?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">Jenis Peserta</label>
              <div class="col-sm-2">
                <input type="text" name="jenispeserta" value="" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">TanggaLahir*</label>
              <div class="col-sm-3"><input type="text" id="tanggallahir" class="form-control" name="tanggallahir" placeholder="Tanggal Lahir" value="<?= ((!empty($data_edit['tanggallahir']))?$data_edit['tanggallahir']:'');?>"></div>
              <label for="" class="col-sm-1 control-label">Rhesus</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="rhesus" name="rhesus">
                <option value="0">Pilih</option>
                <?php foreach ($dt_rhesus as $row){ echo '<option value="'.$row.'" '. (($row==$data_edit['rh'] && !empty($data_edit['rh'])) ? 'selected' : '' ) .'>'.$row.'</option>';}?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">Kelas</label>
              <div class="col-sm-2">
                <input type="text" name="kelasjkn" value="" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">TempatLahir</label>
              <div class="col-sm-3"><input type="text" class="form-control" name="tempatlahir" placeholder="Tempat Lahir" value="<?= ((!empty($data_edit['tempatlahir']))?$data_edit['tempatlahir']:'');?>"></div>
              <label for="" class="col-sm-1 control-label">Status</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="statusmenikah" name="statusmenikah">
                <option value="0">Pilih</option>
                <?php foreach ($dt_perkawinan as $row){ echo '<option value="'.$row.'" '.(($row==$data_edit['statusmenikah'] && !empty($data_edit['statusmenikah'])) ? 'selected' : '').'>'.$row.'</option>';}?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">Status</label>
              <div class="col-sm-2">
                <input type="text" name="statusjkn" value="" class="form-control" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="" class="col-sm-2 control-label">No. Telp</label>
              <div class="col-sm-3"><input type="text" class="form-control" name="notelpon" placeholder="No. Telepon" value="<?=((!empty($data_edit['notelpon']))?$data_edit['notelpon']:'');?>"></div>
              <label for="" class="col-sm-1 control-label">Pendidikan</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="pendidikan" name="pendidikan">
                <option value="0">Pilih</option>
                <?php foreach ($dt_pendidikan as $row){ echo '<option value="'.$row->idpendidikan.'" '. (($row->idpendidikan==$data_edit['idpendidikan'] && !empty($data_edit['idpendidikan'])) ? 'selected' : '' ) .'>'.$row->namapendidikan.'</option>';}?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">CetakKartu</label>
              <div class="col-sm-2">
                <input type="text" name="tanggalcetakkartu" value="<?= ((!empty($data_bpjs['tglcetakkartu']))?$data_bpjs['tglcetakkartu']:'') ?>" class="form-control" readonly>
              </div>
            </div>

            <div class="form-group" >
              <label for="" class="col-sm-2 control-label">Alamat*</label>
             <div class="col-sm-3"><textarea class="form-control" rows="2" placeholder="Alamat sesuai identitas" name="alamat"><?=((!empty($data_edit['alamat']))?$data_edit['alamat']:'');?></textarea></div>
             <label for="" class="col-sm-1 control-label">Pekerjaan</label>
              <div class="col-sm-2">
                <select class="form-control" style="width:100%" id="pekerjaan" name="pekerjaan">
                <option value="0">Pilih</option>
                <?php foreach ($dt_pekerjaan as $row){ echo '<option value="'.$row->idpekerjaan.'" '. (($row->idpekerjaan==$data_edit['idpekerjaan'] && !empty($data_edit['idpekerjaan'])) ? 'selected' : '' ) .'>'.$row->namapekerjaan.'</option>';}?>
                </select>
              </div>

              <label for="" class="col-sm-2 control-label">Informasi</label>
              <div class="col-sm-2">
                <input type="text" name="informasi" value="<?= ((!empty($data_bpjs['informasi']))?$data_bpjs['informasi']:'') ?>" class="form-control datepicker" readonly>
              </div>
            </div>
            <center id="menuPendaftaranPoliklinik">
              <a class="btn btn-primary btn-md" id="menusaveadmisi" onclick="savedatainduk()">Save</a>
              <a class="btn btn-warning btn-md" href="<?php echo base_url('cadmission/datainduk');?>" >Back</a>
            </center>
          </form>
        </div>
        </div>
        <?php } ?>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content