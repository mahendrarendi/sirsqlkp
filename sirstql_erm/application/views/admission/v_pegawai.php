
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body" >
            <div class="col-md-6 row">
                <a href="<?= base_url();?>cadmission/add_pegawai" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Pegawai</a>
                <a onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
            </div>
            <table id="dtpegawai" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
                <tr class="bg bg-yellow-gradient">
                  <th>NO</th>
                  <th>NIP</th>
                  <th>SIP</th>
                  <th>Kode DPJP BPJS</th>
                  <th>NAMA PEGAWAI</th>
                  <th>PROFESI</th>
                  <th>GRUP PEGAWAI</th>
                  <th>IHS Practitioner</th>
                  <th>STATUS</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        
        <!-- start mode add or edit -->
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body">
              <form action="<?= base_url('cadmission/save_pegawai');?>" method="POST" class="form-horizontal" id="FormPegawai" autocomplete="off">
              <input type="hidden" name="idpegawai" placeholder="idpegawai"  value="<?= (!empty($data_edit)) ? $this->encryptbap->encrypt_urlsafe($data_edit["idpegawai"], "json") : '' ; ?>">
              <input type="hidden" name="iduser" value="<?=((empty($data_edit))?'':$data_edit['iduser'])?>">
              <input type="hidden" name="idhakaksesuser" value="<?=((empty($data_edit))?'':$data_edit['idhakaksesuser'])?>">
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">NIP*</label>
                <div class="col-sm-5"><input type="text" class="form-control" name="nip" value="<?= (!empty($data_edit)) ? $data_edit['nip'] : '' ; ?>" placeholder="NIP"></div>
              </div>  
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">SIP*</label>
                <div class="col-sm-5"><input type="text" class="form-control" name="sip" value="<?= (!empty($data_edit)) ? $data_edit['sip'] : '' ; ?>" placeholder="SIP"></div>
              </div>
              <div class="form-group">
                    <label for="" class="col-sm-3 control-label">Kode DPJP BPJS</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="bpjs_kodedokter" value="<?= (!empty($data_edit)) ? $data_edit['bpjs_kodedokter'] : '' ; ?>" placeholder="Kode Dokter (BPJS)">
                        <span class="label label-warning">Isi Kode DPJP Sesuai Referensi dari BPJS, digunakan untuk Bridging Sistem.</span>
                    </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Pegawai*</label>
                <div class="col-sm-5">
                  <select class="select2 form-control" id="listPerson" name="listPerson">
                    <?= (($data_edit['idpegawai'])) ? '<option selected value="'.$data_edit[idperson].'">'.$data_edit[namalengkap].'</option>' : '<option value="0" selected>Pilih</option>' ; ?>
                  </select>
                </div>
                <a onclick="tambah_person()" data-toggle="tooltip" data-original-title="Tambah Person" class="btn btn-default btn-sm"><i class="fa fa-plus-square"></i></a>
                <?=(($data_edit)?'<a onclick="ubah_person('.$data_edit['idperson'].')" data-toggle="tooltip" data-original-title="Ubah Person" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></a>':'')?>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Status*</label>
                <div class="col-sm-5">
                <select class="form-control" style="width:100%" id="statuskeaktifan" name="statuskeaktifan">
                <option value="0">Pilih</option>
                <?php
                foreach ($dt_statuskeaktifan as $row) 
                {
                  if(!empty($data_edit) && $row==$data_edit['statuskeaktifan']) { echo '<option selected value="'.$row.'">'.$row.'</option>';}
                  else {echo '<option value="'.$row.'">'.$row.'</option>';}
                }
                ?>
                </select>
                </div>
              </div> 

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Titel*</label>
                <div class="col-sm-2"><input type="text" class="form-control" value="<?= (!empty($data_edit)) ? $data_edit['titeldepan'] : '' ; ?>" name="titeldepan" placeholder="Titel Depan"></div>
                <div class="col-sm-2" ><input type="text" class="form-control" value="<?= (!empty($data_edit)) ? $data_edit['titelbelakang'] : '' ; ?>" name="titelbelakang" placeholder="Titel Belakang"></div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Profesi*</label>
                <div class="col-sm-5">
                  <select class="select2 form-control" style="width:100%" id="profesi" name="profesi">
                  <option value="0" style="color:blue;">Pilih</option>
                  <?php
                  foreach ($dt_profesi as $row) 
                  {
                    (!empty($data_edit) && $row->idprofesi==$data_edit['idprofesi']) ? $select='selected' : $select='' ;
                    echo '<option '.$select.' value="'.$row->idprofesi.'">'.$row->profesi.'</option>';
                  }
                  
                  ?>
                  </select>
                </div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Grup*</label>
                <div class="col-sm-5">
                  <select class="select2 form-control" style="width:100%" id="gruppegawai" name="gruppegawai">
                  <option value="0" style="color:blue;">Pilih</option>
                  <?php
                  foreach ($dt_gruppegawai as $row) 
                  {
                    (!empty($data_edit) && $row->idgruppegawai==$data_edit['idgruppegawai']) ? $select='selected' : $select='' ;
                    echo '<option '.$select.' value="'.$row->idgruppegawai.'">'.$row->namagruppegawai.'</option>';
                  }
                  
                  ?>
                  </select>
                </div><a onclick="tambah_gruppegawai()" data-toggle="tooltip" data-original-title="Tambah Grup Pegawai" class="btn btn-default btn-sm"><i class="fa fa-plus-square"></i></a>
              </div> 
              
               <div class="form-group">
                   <label for="" class="col-sm-3 control-label">Username<br/><sub>(opsional)</sub></label>
                <div class="col-sm-5"><input type="text" onchange="uniqusername(this.value)" name="username" class="form-control" value="<?= ((!$data_edit)?'':$data_edit['namauser']); ?>"  placeholder="Username" <?= ((!empty($data_edit['namauser']))?'readonly':'') ?>></div>
              </div> 
               <div class="form-group">
                   <label for="" class="col-sm-3 control-label">Password<br/><sub>(opsional)</sub></label>
                <div class="col-sm-5"><input type="password" name="password" class="form-control" value=""  placeholder="password" <?= ((!empty($data_edit['namauser']))?'readonly':'') ?>></div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Unit Farmasi</label>
                <div class='col-sm-2'>
                    <!--$unitunit= ((!$data_edit)?[]:explode(',',$data_edit['unituser]));-->
                <?php if(!empty($unit)){foreach ( $unit as $u ){echo "<input value='".$u->idunit."' ".((($data_edit) && in_array($u->idunit,explode(',',$data_edit['unituser'])))?'checked':'')." name='unituser[]' type='checkbox' class='flat-red'> ".$u->namaunit.'<br>'; } }  ?></div>
                
                <label for="" class="col-sm-1 control-label">Peran* </label>
                <div class='col-sm-2'>
                <?php if(!empty($peran)){foreach ( $peran as $p ){echo "<input value='".$p->idperan."' ".((($data_edit) && in_array($p->idperan,explode(',',$data_edit['peranperan'])))?'checked':'')." name='peranperan[]' type='checkbox' class='flat-red'> ".$p->namaperan.'<br>'; } }  ?></div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label"> </label>
                <div class='col-sm-2 align-center'> 
                    <a class="btn btn-primary btn-lg" onclick="save_pegawai()">Save</a>
                    <a class="btn btn-warning btn-lg" href="<?= base_url('cadmission/pegawai'); ?>" >Back</a> 
                </div>
              </div>
              
            </form>      
          </div>
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->