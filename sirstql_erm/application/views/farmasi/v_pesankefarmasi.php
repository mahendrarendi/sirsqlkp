   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1 "><label>Tgl Awal:</label><br/><input name="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-3">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <a href="<?= base_url("cfarmasi/addpesankefarmasi")?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Pesan</a>
              </div>
              <div class="col-md-3 pull-right"><label>Cari:</label>
                  <br/><input class="form-control" type="text" name="cari" id="cari">
              </div>
            </div>
            <table id="dtpesankefarmasi" class="table table-striped table-bordered table-hover  dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="bg-yellow">
                        <td>Faktur</td>
                        <td>Tanggal</td>
                        <td>Unit Asal/Tujuan</td>
                        <td>Status</td>
                        <td>Jenis Transaksi</td>
                        <td width="80px"></td>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>  
          </div>
        </div>
      <?php }else if($mode=='tambahpesanan'){?>
        <style>.col-sm-2{width:11%;}</style>
        <form action="<?= base_url('cfarmasi/save_pesankefarmasi');?>" method="POST" class="form-horizontal" id="Formbarangpesankefarmasi">
        <div class="box">
          <input name="modeubah" type="hidden">
          <input name="ip" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Tanggal Faktur</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="tanggalfaktur" value="<?= date('Y-m-d'); ?>" readonly></div>
                
              </div> 
              <div class="form-group">
              <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-3"><select id="idunitasal" name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Tujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
            <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <!--<label for="" class="col-sm-1 control-label">Cari Barang/Obat</label>-->
                    <div class="col-sm-4">
                        <select name='idbarang' onchange="addbarangpembelian(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th>Kode</th>
                            <th>Barang/Obat</th>
                            <th>Harga@</th>
                            <th width="150">Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodydistribusi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="simpanbarangpembelian()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/pesankefarmasi'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            <!-- /.box-body -->
          </div>
            
            
        </div>
        </form>
        
        <?php }?> 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  