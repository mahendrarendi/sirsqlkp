   <!-- Main content -->
    <section class="content">
      <div class="row" id="masterobat">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
          <div class="box">
            <div class="box-body" id="tablebarang">
                <?php if( empty(!$this->session->userdata('levelgudang')) ){?>
                <h3>Total Kekayaan  <span id="totalkekayaan"> </span> </h3>
                <label class="label label-info" style="font-size:16px;"> Unit <?= ucwords($this->session->userdata('unitterpilih')); ?> </label>
                <hr>
                <?php } ?>
                <div class="col-md-6 row">
                    <a href="<?= base_url();?>cfarmasi/add_barang" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data</a> 
                    <a onclick="table.ajax.reload()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Unduh Barang</a>
					          <input type="hidden" name="totalkaya" id="totalkaya" value=""/>

                </div>
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="header-table-ql">                  
                  <th>Kode</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>ROP</th>
                  <th>JenisTarif</th>
                  <th>Satuan</th>
                  <th>Sediaan</th>
                  <th>Jenis</th>
                  <th>TipeObat</th>
                  <th>Harga Beli</th>
                  <th>Harga BPJS</th>
                  <th>Harga Umum/Asuransi</th>                  
                  <th>HET</th>
                  <th>Kekuatan</th>
                  <th>Status</th>
                  <th width="90px">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <!-- list barang dari js_masterbarang -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <!-- end mode view -->
          <!-- start mode add or edit -->
          
        <?php }else if( $mode=='edit' || $mode=='add'){?>
        <div class="box">
          <div class="box-body">
              <form action="<?= base_url('cfarmasi/save_barang');?>" method="POST" class="form-horizontal" id="Formbarang">
              <input type="hidden" name="idbarang" placeholder="idbarang"  value="<?= (!empty($data_edit)) ? $this->encryptbap->encrypt_urlsafe($data_edit["idbarang"], "json") : '' ; ?>">

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kode*</label>
                <div class="col-sm-6"><input type="text" class="form-control" name="kode" value="<?= (!empty($data_edit)) ? $data_edit['kode'] : '' ; ?>" placeholder="[otomatis]" disabled></div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Nama Barang*</label>
                <div class="col-sm-6"><input type="text" class="form-control" name="namabarang" value="<?= (!empty($data_edit)) ? $data_edit['namabarang'] : '' ; ?>" placeholder="namabarang"></div>
              </div>  
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Keluhan</label>
                <div class="col-sm-6"><textarea class="form-control" name="keluhan" ><?= (!empty($data_edit)) ? $data_edit['kandungan'] : '' ; ?></textarea></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kandungan/Komposisi</label>
                <div class="col-sm-6"><textarea class="form-control" name="kandungan" ><?= (!empty($data_edit)) ? $data_edit['kandungan'] : '' ; ?></textarea></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Indikasi</label>
                <div class="col-sm-6"><textarea class="form-control" name="indikasi" ><?= (!empty($data_edit)) ? $data_edit['indikasi'] : '' ; ?></textarea></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kontra Indikasi</label>
                <div class="col-sm-6"><textarea class="form-control" name="kontraindikasi" ><?= (!empty($data_edit)) ? $data_edit['kontraindikasi'] : '' ; ?></textarea></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Efeksamping</label>
                <div class="col-sm-6"><textarea class="form-control" name="efeksamping" ><?= (!empty($data_edit)) ? $data_edit['efeksamping'] : '' ; ?></textarea></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Pabrik Pembuat</label>
                <div class="col-sm-6">
                  <select class="select2 form-control" style="width:100%" id="idbarangpabrik" name="idbarangpabrik">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_pabrik as $row){( (!empty($data_edit) && $row->idbarangpabrik==$data_edit['idbarangpabrik']) ? $selected='selected'  : $selected=''  );echo '<option '.$selected.' value="'.$row->idbarangpabrik.'">'.$row->namapabrik.'</option>';}?>
                  </select>
                </div>
              </div>

             
              <div class="form-group">
                  <label for="" class="col-sm-3 control-label">ROP</label>
                <div class="col-sm-6"><input type="text" class="form-control" name="rop" value="<?= (!empty($data_edit)) ? $data_edit['rop'] : '' ; ?>" placeholder="rop"></div>
              </div>
              <div class="form-group">
                  <label for="" class="col-sm-3 control-label"><span class="bg-yellow padding2">Stok Minimal*</span></label> 
                  <div class="col-sm-6"><input type="text" name="stokminimal" value="<?= (!empty($data_edit)) ? $data_edit['stokminimal'] : '' ; ?>" class="form-control"></div>
              </div>
              <div class="form-group">
                  <label for="" class="col-sm-3 control-label"><span class="bg-red padding2">Stok Habis*</span></label>
                `<div class="col-sm-6"><input type="text" name="stokhabis" value="<?= (!empty($data_edit)) ? $data_edit['stokhabis'] : '' ; ?>" class="form-control"></div>
              </div>
              <div class="form-group">
                  <label for="" class="col-sm-3 control-label"><span class="bg-green padding2">Stok Aman*</span></label>
                  <div class="col-sm-6"><input type="text" name="stokaman" value="<?= (!empty($data_edit)) ? $data_edit['stokaman'] : '' ; ?>" class="form-control"></div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jenis Tarif*</label>
                <div class="col-sm-6">
                  <select class="select2 form-control" style="width:100%" id="idjenistarif" name="idjenistarif">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_jenistarif as $row){( (!empty($data_edit) && $row->idjenistarif==$data_edit['idjenistarif']) ? $selected='selected'  : $selected=''  );echo '<option '.$selected.' value="'.$row->idjenistarif.'">'.$row->jenistarif.'</option>';}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Satuan*</label>
                <div class="col-sm-6">
                  <select class="select2 form-control" style="width:100%" id="idsatuan" name="idsatuan">
                  <option value="0">Pilih</option>
                  <?php foreach ($dt_satuan as $row){( (!empty($data_edit) && $row->idsatuan==$data_edit['idsatuan']) ? $selected='selected'  : $selected=''  );echo '<option '.$selected.' value="'.$row->idsatuan.'">'.$row->namasatuan.'</option>';}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Sediaan*</label>
                <div class="col-sm-6">
                  <select class="select2 form-control" style="width:100%" id="idsediaan" name="idsediaan">
                  <option value="0">Pilih</option>
                  <?php foreach ($data_sediaan as $row){( (!empty($data_edit) && $row->idsediaan==$data_edit['idsediaan']) ? $selected='selected'  : $selected=''  );echo '<option '.$selected.' value="'.$row->idsediaan.'">'.$row->namasediaan.'</option>';}?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jenis*</label>
                <div class="col-sm-6">
                <select class="form-control" style="width:100%" id="jenis" name="jenis" onchange="barang_carigolbyjenis(this.value)">
                <option value="0">Pilih</option>
                <?php foreach ($dt_jenis as $row) {if(!empty($data_edit) && $row==$data_edit['jenis']) { echo '<option selected value="'.$row.'">'.$row.'</option>';}else {echo '<option value="'.$row.'">'.$row.'</option>';}}?>
                </select>
                </div>
              </div> 

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Golongan*</label>
                <div class="col-sm-6">
                <select class="select2 form-control" style="width:100%" id="golongan" name="golongan">
                <option value="0">Pilih</option>
                <?php if($mode=='edit'){foreach ($data_golongan as $obj) {echo "<option ". (($obj->idgolongan==$data_edit['idgolongan']) ? "selected" : "" ) ." value=".$obj->idgolongan.">".$obj->golongan."</option>";}}?>
                </select>
                </div>
              </div> 
              
              <div class="form-group">
                  <label for="" class="col-sm-3 control-label"><label class="bg-yellow padding2">Obat Fornas*</label></label>
                  <div class="col-sm-6"><input id="isfornas" name="isfornas" type="checkbox" <?= ( empty(!$data_edit) && $data_edit['isfornas']==1 ) ? 'checked' : '' ; ?> class="flat-red"></div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Tipeobat*</label>
                <div class="col-sm-6">
                <select class="form-control" style="width:100%" id="tipeobat" name="tipeobat">
                <option value="0">Pilih</option>
                <?php foreach ($dt_tipeobat as $row) {if(!empty($data_edit) && $row==$data_edit['tipeobat']) { echo '<option selected value="'.$row.'">'.$row.'</option>';}else {echo '<option value="'.$row.'">'.$row.'</option>';}}?>
                </select>
                </div>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Jenis Jaminan*</label>
                <div class="col-sm-6">
                <select class="form-control" style="width:100%" id="jenisjaminan" name="jenisjaminan">
                  <option value="">Pilih</option>
                  <option <?= (!empty($data_edit) && $data_edit['jenisjaminan'] == 'Umum') ? 'selected'  : '' ; ?> value="Umum">Umum</option>
                  <option <?= (!empty($data_edit) && $data_edit['jenisjaminan'] == 'BPJS') ? 'selected'  : '' ; ?> value="BPJS">BPJS</option>
                </select>
                </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Harga Beli*</label>
                <div class="col-sm-6"><input id="hargabeli" onkeyup="barang_sethargajual()" type="text" class="form-control" name="hargabeli" value="<?= (!empty($data_edit)) ? $data_edit['hargabeli'] : '' ; ?>" placeholder="Harga beli"></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Margin Harga BPJS*</label>
                <div class="col-sm-3"><div class="input-group">
                  <input id="persenmargin" onkeyup="barang_sethargajual()" type="text" class="form-control" name="persenmargin" value="<?= (!empty($data_edit)) ? $data_edit['persenmargin'] : '' ; ?>" placeholder="Persen margin ">
                <div class="input-group-addon">%</div> 
                </div>
                </div>
              </div>   
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Harga BPJS*</label>
                <div class="col-sm-6"><input id="hargajual"  readonly class="form-control" name="hargajual" value="<?= (!empty($data_edit)) ? $data_edit['hargajual'] : '' ; ?>" placeholder="Harga jual"></div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">HET*</label>
                <div class="col-sm-6"><input id="het"  class="form-control" name="het" value="<?= (!empty($data_edit)) ? $data_edit['het'] : '' ; ?>" placeholder="Harga Eceran Tertinggi"></div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Pengaturan Margin</label>
                <div class="col-sm-3">
                    <select class="form-control" onchange="setPengaturanMargin(this.value)" name="margin_manual" id="margin_manual">
                        <option <?= (!empty($data_edit) && $data_edit['margin_manual'] == 0) ? 'selected'  : '' ; ?> value="0">Otomatis</option>
                        <option <?= (!empty($data_edit) && $data_edit['margin_manual'] == 1) ? 'selected'  : '' ; ?> value="1">Manual</option>
                    </select>
                </div>
              </div> 
              
              <div class="form-group" data-setmargin="1">
                <label for="" class="col-sm-3 control-label">Margin Harga Umum/Asuransi*</label>
                <div class="col-sm-3">
                    <div class="input-group">
                        <input id="persenmargin_hargaumum" onkeyup="barang_sethargaumum()" type="text" class="form-control" name="persenmargin_hargaumum" value="<?= (!empty($data_edit)) ? $data_edit['persenmargin_hargaumum'] : '' ; ?>" placeholder="Persen margin harga umum / asuransi">
                        <div class="input-group-addon">%</div> 
                    </div>
                </div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Harga Umum/Asuransi*</label>
                <div class="col-sm-6">
                    <input id="hargaumum"  readonly class="form-control" name="hargaumum" value="<?= (!empty($data_edit)) ? $data_edit['hargaumum'] : '' ; ?>" placeholder="Harga Umum/Asuransi">
                </div>
              </div> 
              
              <div class="form-group">
                <label for="" class="col-sm-3 control-label">Kekuatan*</label>
                <div class="col-sm-6"><input type="text" class="form-control" name="kekuatan" value="<?= (!empty($data_edit)) ? $data_edit['kekuatan'] : '' ; ?>" placeholder="kekuatan"></div>
              </div>
              
              <br>
              <center>
                    <a class="btn btn-primary btn-lg" onclick="simpan_barang()">SAVE</a>
                    <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/barang'); ?>" >Back</a>
                  </div>
                  </center>
            </form> 
          </div>
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    
  </section>
  <!-- /.content -->