   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            
              <a style="margin-bottom:5px;" id="create_stokopname" class="btn btn-primary btn-md"><i class="fa fa-plus-square"></i> Stok Opname</a> 
              <!--<a style="margin-bottom:5px;" class="btn btn-success btn-md"><i class="fa fa-file-excel-o"></i> Unduh Stok</a>--> 
              
            <div class="box box-default box-solid">
               <div class="box-header with-border">
                 <h5 class="text-bold">Data Perbandingan Stok Lama dan Stok Baru</h5>
               </div>
               <div class="box-body">
                 <table id="dtperbandingan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                   <thead>
                    <tr style="background:#edd38c;">    
                     <th>Nama Barang</th>
                     <th>Kode</th>
                     <th>Stok Lama</th>
                     <th>Stok Baru</th>
                     <th>Satuan</th>
                   </tr>
                   </thead>
                   <tbody>
                   </tfoot>
                 </table>
               </div>
           </div>
                  
          </div>
        </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  