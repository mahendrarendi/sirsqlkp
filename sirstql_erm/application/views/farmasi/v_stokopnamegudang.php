   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
              <a class="btn btn-primary"><i class="fa fa-plus-square"></i> Stok Opname</a>
              <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#perbandinganstok" id="view_perbandinganstok" data-toggle="tab">Perbandingan Stok</a></li>
                  <li><a href="#stoklama" id="view_stoklama" data-toggle="tab">Stok Lama</a></li>
                  <li><a href="#stokbaru" id="view_stokbaru" data-toggle="tab">Stok Baru</a></li>
                </ul>
                <div class="tab-content no-padding">
                    
                  <!-- stok perbandingan -->
                  <div class="chart tab-pane active" id="perbandinganstok" style="padding: 8px 0px;">
                      <div class="box box-default box-solid">
                         <div class="box-header with-border">
                           <h5 class="text-bold">Data Perbandingan Stok Lama dan Stok Baru</h5>
                         </div>
                         <div class="box-body">
                           <table id="dtperbandingan" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                             <thead>
                              <tr style="background:#edd38c;">    
                               <th>Nama Barang</th>
                               <th>Kode</th>
                               <th>Stok Lama</th>
                               <th>Stok Baru</th>
                               <th>Satuan</th>
                             </tr>
                             </thead>
                             <tbody>
                             </tfoot>
                           </table>
                         </div>
                     </div>
                  </div>
                  <!--end stok perbandingan-->
                  
                  <!-- stok lama -->
                  <div class="chart tab-pane" id="stoklama" style="padding: 8px 0px;">
                      <div class="box box-default box-solid">
                         <div class="box-header with-border">
                           <h5 class="text-bold">Data Stok Lama</h5>
                         </div>
                         <div class="box-body">
                           <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                             <thead>
                              <tr style="background:#edd38c;">                
                               <th>No</th>
                               <th>Nama Barang</th>
                               <th>Kode</th>
                               <th>Stok</th>
                               <th>Satuan</th>
                             </tr>
                             </thead>
                             <tbody>
                             </tfoot>
                           </table>
                         </div>
                     </div>
                  </div>
                  <!--end stok lama-->
                  
                  <!-- stok baru -->
                  <div class="chart tab-pane" id="stokbaru" style="padding: 8px 0px;">
                      <div class="box box-default box-solid">
                         <div class="box-header with-border">
                           <h5 class="text-bold">Data Stok Baru</h5>
                         </div>
                         <div class="box-body">
                           <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                             <thead>
                              <tr style="background:#edd38c;">                
                               <th>No</th>
                               <th>Nama Barang</th>
                               <th>Kode</th>
                               <th>Stok</th>
                               <th>Satuan</th>
                             </tr>
                             </thead>
                             <tbody>
                             </tfoot>
                           </table>
                         </div>
                     </div>
                  </div>
                  <!--end stok baru-->
                  
                </div>
              </div>
          </div>
        </div>

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  