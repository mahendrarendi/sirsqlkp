   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- mode view -->
        <?php if($mode=='kirimpermintaanfarmasi'){?>
        <style>.table>tbody>tr>th{padding:1px;}.table>thead>tr>th{padding:3px;}</style>
        <form action="<?= base_url('cfarmasi/savekirimpesananfarmasi');?>" method="POST" class="form-horizontal" id="Formbarangpesanan">
        <div class="box">
            <input type="hidden" name="idbarangpemesanan" />
            <input name="modeubah" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">NoFaktur</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="nofaktur" readonly/></div>
                <label for="" class="col-sm-1 control-label">UnitAsal</label>
                <div class="col-sm-3"><select name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div> 
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">Waktu</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="waktu" readonly></div>
                <label for="" class="col-sm-1 control-label">UnitTujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
           <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Pesanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header --> 
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        
                        <tbody id="tbodybarangpesanan"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="savekirimbarangpesanan()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/permintaanfarmasi'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            
            <!-- /.box-body -->
          </div>
        </div>
        </form>
        <?php }?> 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  