   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='distribusibarang'){ ?>
        <div class="box">

          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1"><label>Tgl Awal:</label><br/><input name="tanggal1" id="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" id="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-4">
                <label>&nbsp;</label><br/>
                <a id="tampilbarangdatang" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reloadbarangdatang" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <?php if( $this->pageaccessrightbap->checkAccessRight(V_TAMBAHITEMBELANJAFARMASI) ) { ?>
                    <a href="<?=base_url('cfarmasi/tambahbarangdatang');?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Barang Datang</a>
                    <a id="unduh" class="btn btn-success btn-sm"><i class="fa  fa-file-excel-o"></i> Unduh</a>
                <?php } ?>
              </div>
              <div class="col-md-4"> <label>Filter Tanggal</label><br/> <span class="bg bg-info" style="padding:8px;"><input id="radio1" type="radio" name="filtertanggal" value="tanggalfaktur"> Tanggal Faktur</span> <span class="bg bg-info" style="padding:8px;"> <input type="radio" id="radio2" checked name="filtertanggal" value="tanggalinput"> Tanggal Input</span></div>
              <div class="col-md-2"><label>Cari:</label><br/><input class="form-control" type="text" name="cari" id="cari"></div>
            </div>
            <div>
                <table id="listbarangdatang" class="table table-hover table-bordered  dt-responsive"  width="100%">
                    <thead>
                        <tr class="bg bg-yellow-gradient">
                            <td>No</td>
                            <td>PBF - Distributor</td>
                            <td>No.Faktur</td>
                            <td>Tgl.Faktur</td>
                            <td>Tgl.Pelunasan</td>
                            <td>Jatuh Tempo</td>
                            <td>Masa Hutang</td>
                            <td>Status</td>
                            <td>N.Inkaso</td>
                            <td>Dibayar</td>
                            <td>J.Hutang</td>
                            <td>Keterangan</td>
                            <td width="100px"></td>
                        </tr>
                    </thead>
                    <tbody></tfoot>
                </table>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        
      <?php }else if( $mode=='permintaanfarmasi'){ ?>
        <div class="box">
          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1 "><label>Tgl Awal:</label><br/><input name="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-3">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <?= '<a href="'. base_url("cfarmasi/addpesankegudang") .'" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Permintaan</a>'; ?>
              </div>
              <div class="col-md-3 pull-right"><label>Cari:</label>
                  <br/><input class="form-control" type="text" name="cari" id="cari">
              </div>
            </div>
            <table id="dtpermintaanfarmasi" class="table table-striped table-bordered table-hover  dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="bg-yellow">
                        <td>Faktur</td>
                        <td>Tanggal</td>
                        <td>Unit Asal/Tujuan</td>
                        <td>Status</td>
                        <td>Jenis Transaksi</td>
                        <td width="80px"></td>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>  
          </div>
        </div>

      <?php }else if( $mode=='pesanbarangkegudang'){ ?>
        <div class="box">
          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1 "><label>Tgl Awal:</label><br/><input name="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-3">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <?= '<a href="'. base_url("cfarmasi/addpesankegudang") .'" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Permintaan</a>'; ?>
              </div>
              <div class="col-md-3 pull-right"><label>Cari:</label>
                  <br/><input class="form-control" type="text" name="cari" id="cari">
              </div>
            </div>
            <table id="dtpesanankegudang" class="table table-striped table-bordered table-hover  dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="bg-yellow">
                        <td>Faktur</td>
                        <td>Tanggal</td>
                        <td>Unit Asal/Tujuan</td>
                        <td>Status</td>
                        <td>Jenis Transaksi</td>
                        <td width="80px"></td>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>  
          </div>
        </div>
        
       <!--farmasi tambah pesan barang kegudang-->
      <?php }else if($mode=='addpesankegudang'){?>
        <style>.col-sm-2{width:11%;}</style>
        <form action="<?= base_url('cfarmasi/save_pesankegudang');?>" method="POST" class="form-horizontal" id="Formbarangpembelian">
        <div class="box">
          <input name="modeubah" type="hidden">
          <input name="ip" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Tanggal Faktur</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="tanggalfaktur" value="<?= date('Y-m-d'); ?>" readonly></div>
                
              </div> 
              <div class="form-group">
              <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-3"><select id="idunitasal" name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Tujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
            <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <!--<label for="" class="col-sm-1 control-label">Cari Barang/Obat</label>-->
                    <div class="col-sm-4">
                        <select name='idbarang' onchange="addbarangpembelian(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th>Kode</th>
                            <th>Barang/Obat</th>
                            <th>Harga@</th>
                            <th width="150">Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodydistribusi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="simpanbarangpembelian()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/pesankegudang'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            <!-- /.box-body -->
          </div>
            
            
        </div>
        </form>
        
      <?php }else if( $mode=='distribusireturbarang'){ ?>
        <div class="box">
          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1 "><label>Tgl Awal:</label><br/><input name="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-3">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <?= ($this->session->userdata('unitterpilih')!='GUDANG') ? '<a href="'. base_url("tambahpengembalian") .'" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Pengembalian</a>' : '' ?>
              </div>
              <div class="col-md-3 pull-right"><label>Cari:</label><br/><input class="form-control" type="text" name="cari" id="cari"></div>
            </div>
            <table id="listretur" class="table table-striped table-bordered table-hover  dt-responsive" cellspacing="0" width="100%"><thead><tr class="bg-yellow"><td>Faktur</td><td>Tanggal</td><td>Unit Asal/Tujuan</td><td>Status</td><td width="80px"></td></tr></thead><tbody></tfoot></table>  
          </div>
        </div>
        
        <!--barang datang-->
        <?php }else if($mode=='belanja'){?>
        <style>.col-sm-1{margin-left:0px;padding-left:0px;}</style>
        <form action="<?= base_url('cfarmasi/save_barangpembelianfaktur');?>" method="POST" class="form-horizontal" id="Formbarangpembelian">
        <input type="hidden" name="idbarangfaktur"/>
        <input type="hidden" name="statusfaktur"/>
        <div class="box">
            
          <div class="box-body">
              <div class="col-xs-12 bg bg-info" style="margin-bottom:10px;">
                <small>
                  <b>Panduan Input Barang Datang:</b>
                  <ol>
                    <li>Lengkapi data (no faktur, no pesan, tanggal faktur, tanggal jatuh tempo dan distributor).</li>
                    <li>Unit asal dan Jenis Distribusi otomatis terisi, tidak perlu diinput ulang.</li>
                    <li>Ceklis Barang Sudah Diterima, jika barang sudah diterima, maka harap ceklist pada inputan ini.</li>
                    <li>Untuk menambahkan Item (obat/bhp), silakan melakukan pencarian di detail barang dengan klik label cari barang kemudian ketik nama barang dan pilih.</li>
                    <li>Jika barang berhasil ditambahkan, lengkapi data (batch no, kadaluarsa, jumlah, harga beli, bominal diskon).</li>
                    <li>Fungsi menu (hitung diskon, salin, batal) di detail barang. </li>
                      <ul>
                        <li>Hitung diskon (warna biru muda,icon kalkulator) = untuk menghitung ulang diskon, apabila belum sesuai setelah melakukan perubahan data (jumlah,harga beli,diskon)</li>
                        <li>Salin (warna orange, icon dua tumpukan dokumen) = untuk menyalin barang yang sama dengan isian (batch no, kadaluarsa, jumlah, harga beli, bominal diskon) yang berbeda.</li>
                        <li>Batal (warna merah, icon mines didalam lingkaran)  untuk membatalkan/menghapus barang yang sudah terinput.</li>
                      </ul>
                    <li>Perhitungan Pada Detail Barang</li>
                    <ul>
                      <li>Diskon(%) : (NominalDiskon / (HargaAsli * jumlah)) * 100 </li>
                      <li>Nominal Diskon : (HargaAsli * jumlah) * (Diskon(%) / 100)</li>
                      <li>HargaBeli : ((HargaAsli-(HargaAsli*Diskon(%))) * 1.1)</li>
                    </ul>
                    <li>Ubah /Hapus Detail Faktur (jumlah,hargabeli,diskon..)</li>
                    <ul>
                      <li>Ubah:  Ubah isian pada item yang akan dirubah, kemudian simpan kembali</li>
                      <li>Hapus: Klik menu batalkan pada detail barang.</li>
                    </ul>
                    <li>Hapus/Batalkan Faktur: Klik menu batalkan faktur, menu Batalkan faktur akan keluar jika faktur telah dibuat sebelumnya dan belum diterima/selesaikan.</li>
                  </ol>
                </small>
              </div>

              <div class="form-group">
                <label for="" class="col-sm-2 control-label">No.Faktur</label>
                <div class="col-sm-3"><input type="text" class="form-control" id="nofaktur" name="nofaktur"  placeholder="Nomor Faktur" /></div>
                <label for="" class="col-sm-2 control-label">Jenis Distribusi</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="jenisdistribusi" value="<?= $jenisdistribusi; ?>" readonly='readonly' /></div>
                
              </div> 
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">No.Pesan</label>
                <div class="col-sm-3"><input size="2"  type="text" class="form-control" id="nopesan" name="nopesan" placeholder="Nomor Pesan" /></div>
                <label for="" class="col-sm-2 control-label">Tagihan</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="tagihan" placeholder="nominal tagihan" onkeyup="hitungHutangBelanja()" /></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Tanggal Faktur</label>
                <div class="col-sm-3"><input size="2"  type="text" class="form-control datepicker" id="tanggalfaktur" name="tanggalfaktur" /></div>
                <label for="" class="col-sm-2 control-label">Potongan</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="potongan"  placeholder="potongan" /> </div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Jatuh Tempo</label>
                <div class="col-sm-3"><input size="2"  type="text" class="form-control datepicker" id="tanggaljatuhtempo" name="tanggaljatuhtempo" /></div>
                <label for="" class="col-sm-2 control-label">PPN</label>
                <div class="col-sm-2"><input type="text" id="ppnbelanja" class="form-control" name="ppn"  placeholder="ppn" /></div>
                </div>
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-3"><select name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
                <label for="" class="col-sm-2 control-label">Total Tagihan</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="totaltagihan"  placeholder="totaltagihan" /></div>
               </div>
              
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Disributor</label>
                <div class="col-sm-3">
                    <select id="idbarangdistributor" name='idbarangdistributor' class="form-control select2">
                    </select>
                </div>
                <label for="" class="col-sm-2 control-label"></label>
                <div class="col-sm-2 bg-yellow"><input name="setterimabarang" type="checkbox" /> Barang Sudah Diterima</div>
                
              </div>  
          </div>
            
          <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body">
                <div class="form-group">
                    <!--<label for="" class="col-sm-1 control-label">Cari Barang/Obat</label>-->
                    <div class="col-sm-4">
                        <select name='idbarang' onchange="addbarangbarangdatang(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th rowspan="2">Kode</th>
                            <th rowspan="2">Barang/Obat</th>
                            <th rowspan="2">Batch.No</th>
                            <th rowspan="2">Kadaluarsa</th>
                            <th rowspan="2">Jumlah</th>
                            <th colspan="3">Harga@</th>
                            <th colspan="2">Nominal Diskon@</th>
                            <th rowspan="2">Satuan</th>
                            <th rowspan="2">Subtotal</th>
                            <th rowspan="2"></th>
                          </tr>
                          <tr class="bg bg-yellow">
                            <th>HargaBeli</th>
                            <th>Harga Asli</th>
                            <th>HET</th>
                            <th>Persen(%)</th>
                            <th>Nominal(Rp)</th>
                          </tr>
                        </thead>
                        <tbody id="tbodydistribusi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-primary btn-lg" onclick="simpanbarangpembelian()"> <i class="fa fa-save"></i> SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/barangdatang'); ?>" id="menubatal" ><i class="fa  fa-step-backward"></i> KEMBALI</a>
                </center>
                <br>      
            </div>
            </div>
            
          </div>
            
            
        </div>
        </form>

        <?php }else if($mode=='pesan'){?>
        <style>.col-sm-2{width:11%;}</style>
        <form action="<?= base_url('cmasterdata/save_barangpemesanan');?>" method="POST" class="form-horizontal" id="Formbarangpembelian">
        <div class="box">
          <input name="modeubah" type="hidden">
          <input name="ip" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Tanggal Faktur</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="tanggalfaktur" value="<?= date('Y-m-d'); ?>" readonly></div>
                
              </div> 
              <div class="form-group">
              <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-3"><select id="idunitasal" name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Tujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
            <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <!--<label for="" class="col-sm-1 control-label">Cari Barang/Obat</label>-->
                    <div class="col-sm-4">
                        <select name='idbarang' onchange="addbarangpembelian(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th>Kode</th>
                            <th>Barang/Obat</th>
                            <th>Harga@</th>
                            <th width="150">Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodydistribusi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="simpanbarangpembelian()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('permintaanbarang'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            <!-- /.box-body -->
          </div>
            
            
        </div>
        </form>

        <?php }else if($mode=='retur'){?>
        <style>.col-sm-2{width:11%;}</style>
        <form action="<?= base_url('cmasterdata/save_barangretur');?>" method="POST" class="form-horizontal" id="Formbarangpembelian">
        <div class="box">
          <input name="modeubah" type="hidden">
          <input name="ip" type="hidden">
          <input type="hidden" name="mode" value="<?= $mode; ?>">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Tanggal Faktur</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="tanggalfaktur" value="<?= date('Y-m-d'); ?>" readonly></div>
                
              </div> 
              <div class="form-group">
              <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-3"><select name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div>
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Tujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
            <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <!--<label for="" class="col-sm-1 control-label">Cari Barang/Obat</label>-->
                    <div class="col-sm-4">
                        <select name='idbarang' onchange="addbarangpembelian(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th>Kode</th>
                            <th>Barang/Obat</th>
                            <th>Harga@</th>
                            <th>BatchNo</th>
                            <th>Kadaluarsa</th>
                            <th width="150">Jumlah</th>
                            <th>Satuan</th>
                            <th>Keterangan</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodydistribusi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="simpanbarangpembelian()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('pengembalianbarang'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            <!-- /.box-body -->
          </div>
            
            
        </div>
        </form>
        
        
        
        
        <?php }else if($mode=='kirimpermintaanfarmasi'){?>
        <style>.table>tbody>tr>th{padding:1px;}.table>thead>tr>th{padding:3px;}</style>
        <form action="<?= base_url('cfarmasi/savekirimpesananfarmasi');?>" method="POST" class="form-horizontal" id="Formbarangpesanan">
        <div class="box">
            <input type="hidden" name="idbarangpemesanan" />
            <input name="modeubah" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">NoFaktur</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="nofaktur" readonly/></div>
                <label for="" class="col-sm-1 control-label">UnitAsal</label>
                <div class="col-sm-3"><select name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div> 
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">Waktu</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="waktu" readonly></div>
                <label for="" class="col-sm-1 control-label">UnitTujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
           <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Pesanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header --> 
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        
                        <tbody id="tbodybarangpesanan"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="savekirimbarangpesanan()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/permintaanfarmasi'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            
            <!-- /.box-body -->
          </div>
        </div>
        </form>
        
        <?php }else if($mode=='kirim'){?>
        <style>.table>tbody>tr>th{padding:1px;}.table>thead>tr>th{padding:3px;}</style>
        <form action="<?= base_url('cmasterdata/savepesananbarangunit');?>" method="POST" class="form-horizontal" id="Formbarangpesanan">
        <div class="box">
            <input type="hidden" name="idbarangpemesanan" />
            <input name="modeubah" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">NoFaktur</label>
                <div class="col-sm-2"><input type="text" class="form-control" name="nofaktur" readonly/></div>
                <label for="" class="col-sm-1 control-label">UnitAsal</label>
                <div class="col-sm-3"><select name="idunitasal" class="form-control"><?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?></select></div>
              </div> 
              <div class="form-group">
                <label for="" class="col-sm-1 control-label">Waktu</label>
                <div class="col-sm-2"><input  type="text" class="form-control" name="waktu" readonly></div>
                <label for="" class="col-sm-1 control-label">UnitTujuan</label>
                <div class="col-sm-3"><select name="idunittujuan" class="form-control"><?php  if(!empty($unittujuan)){foreach ($unittujuan as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?></select></div>
              </div>
          </div>
            
           <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Pesanan</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header --> 
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        
                        <tbody id="tbodybarangpesanan"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="savekirimbarangpesanan()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/permintaanfarmasi'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            
            <!-- /.box-body -->
          </div>
        </div>
        </form>
        <?php }else if($mode=='stokbarang'){?>
        
        <div class="box">

          <div class="box-body" id='tampilstokbarang'>
              <table id="listdatatable" class="table table-bordered table-striped table-hover dt-responsive dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="listdatatable_info" style="width: 100%;">
                <thead>
                <tr role="row"><th>No</th><th>NamaBarang</th><th>Kode</th><th>Stok</th><th>Kadaluarsa</th><th>JenisTarif</th><th>Satuan</th><th>Sediaan</th><th>Jenis</th><th>TipeObat</th><th>Kekuatan</th></tr>
                </thead>
                <tbody>
                    <?php 
                        if(!empty($stokbarang))
                        {
                            $no=0;
                            foreach ($stokbarang as $obj)
                            {
                                echo '<tr><td>',++$no,'</td><td>',$obj->namabarang,'</td><td>',$obj->kode,'</td><td>',$obj->stok,'</td><td>',$obj->kadaluarsa,'</td><td>',$obj->jenistarif,'</td><td>',$obj->namasatuan,'</td><td>',$obj->namasediaan,'</td><td>',$obj->jenis,'</td><td>',$obj->tipeobat,'</td><td>',$obj->kekuatan,'</td></tr>';
                            }
                        } 
                    ?>
                </tbody>
              </table>
          </div>
          <!-- /.box-body -->
        </div>
         <?php }else if($mode=='transformasibarang'){?>
        
        <div class="box">

          <div class="box-body">
            <div class="col-xs-12 row">
              <div class="col-md-1 "><label>Tgl Awal:</label><br/><input name="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-1"><label>Tgl Akhir:</label><br/><input name="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-3">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <a href="<?= base_url('cfarmasi/addtransformasibarang') ?>" class="btn btn-primary btn-sm"><i class="fa fa-random"></i> Transformasi</a>
              </div>
              <div class="col-md-3 pull-right"><label>Cari:</label><br/><input class="form-control" type="text" name="cari" id="cari"></div>
            </div>
              <table id="transformasi" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                  <thead>
                      <tr><th>No</th><th>Transformasi Asal</th><th>Transformasi Hasil</th><th>Waktu</th><th></th></tr>
                  </thead>
                  <tbody></tfoot>
              </table>
          </div>
          <!-- /.box-body -->
        </div>
        <?php }else if($mode=='addtransformasibarang'){?>
        <style>.col-sm-1{margin-left:0px;padding-left:0px;}</style>
        <form action="<?= base_url('cfarmasi/save_transformasibarang');?>" method="POST" class="form-horizontal" id="Formtransformasi">
        <div class="box">
            
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-sm-2 control-label">Unit Asal</label>
                <div class="col-sm-2">
                    <select name="idunitasal" class="form-control">
                        <?php foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>"; } ?>
                    </select>
                </div>
                
                <label for="" class="col-sm-2 control-label">Unit Tujuan</label>
                <div class="col-sm-2">
                    <select name="idunittujuan" class="form-control">
                        <?php  if(!empty($unit)){foreach ($unit as $list){ echo "<option value=",$list->idunit," >",$list->namaunit,"</option>";}}else{echo "<option value='NULL'>NULL</option>";} ?>
                    </select>
                </div>
              </div> 
              <div class="form-group">
                
                <label for="" class="col-sm-2 control-label">Jenis Distribusi</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" name="jenisdistribusi" value="<?= $jenisdistribusi; ?>" readonly />
                </div>
              </div>  
          </div>
            
             <div class="form-group" style="padding-left:4%;padding-right:2%;">
                    <div class="col-sm-4">
                        <select id='selectbarang' name='idbarang' onchange="addtransformasibarang(this.value)" class="form-control select2">
                            <option value="0">Cari barang</option>
                        </select>
                    </div>
                </div>
            
            <label class="label-default col-sm-12 text-center"><i class="fa  fa-quote-left"></i> Data Transformasi Barang<i class="fa  fa-quote-right"></i></label>
                
        <style>.table>tbody>tr>th{padding:1px;}.table>thead>tr>th{padding:1.5px;}</style>
        
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        
                        <tbody id="tbodybarangtransformasi"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a id="btnsimpantransformasibarang" class="btn btn-warning btn-lg" onclick="savetransformasibarang()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/transformasibarang'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
        </div>
        </form>
        
        <?php }?> 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  