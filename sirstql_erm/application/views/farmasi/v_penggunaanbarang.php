   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- mode view -->
        <?php if($mode=='view'){ ?>
        <div class="box">
          <div class="box-body">
            <div class="col-xs-6 row">
              <div class="col-md-3">
                  <label>Bulan</label><br/>
                  <input name="tahunbulan" id="tahunbulan" class="form-control" size="7"/>
              </div>
              <div class="col-md-8">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="reload" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a> |
                <a href="<?= base_url('cfarmasi/add_penggunaanbarang') ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-circle"></i> Add</a>
              </div>
            </div>
              
            <table id="dtpenggunaan" class="table table-striped table-bordered table-hover  dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                        <td>No</td>    
                        <td>Keterangan</td>    
                        <td>Nama Barang</td>
                        <td>Jumlah</td>
                        <td>Harga Jual</td>
                        <td>Total Harga</td>
                        <td>Satuan</td>
                        <td>Dibuat Oleh</td>
                        <td>Waktu Dibuat</td>
                        <td width="80px"></td>
                    </tr>
                </thead>
                <tbody>
                </tfoot>
            </table>  
              
          </div>
        </div>   
        <?php }else{ ?>
        <style>.table>tbody>tr>th{padding:1px;}.table>thead>tr>th{padding:3px;}</style>
        <form  method="POST" class="form-horizontal" id="Formpenggunaan">
        <div class="box">
            <input type="hidden" id="idbarangpenggunaan" name="idbarangpenggunaan" value="" />
            <input type="hidden" id="iddetail" value="" />
            <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Keterangan</label>
                  <div class="col-sm-5">
                      <textarea class="form-control" name="keterangan" rows="5"></textarea>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="" class="col-sm-1 control-label">Pilih Barang</label>
                  <div class="col-sm-5">
                      <select class="form-control" id="pilihbarang">
                          <option value="0">Pilih</option>
                      </select>
                  </div>
                </div>
            </div>
            
           <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Penggunaan Barang</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header --> 
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <table class="table table-striped table-bordered table-hover">                        
                            <thead>
                                <tr class="header-table-ql"><td>Kode</td><td>Nama Barang</td><td>Kadaluarsa</td><td>No.Batch</td><td width="200px">Jumlah</td><td>Satuan</td><td></td></tr>
                            </thead>
                          <tbody id="bodyDetailPenggunaan">                              
                          </tfoot>
                        </table>
                    </div>
                </div>
                <center>
                  <button type="submit" class="btn btn-primary btn-lg">SIMPAN</button>
                  <a class="btn btn-danger btn-lg" href="<?= base_url('cfarmasi/penggunaanbarang'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            
            <!-- /.box-body -->
          </div>
        </div>
        </form>
        <?php }?> 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  