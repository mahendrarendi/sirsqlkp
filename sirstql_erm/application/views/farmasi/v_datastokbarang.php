   <!-- Main content -->
   
    
   
    <section class="content">
      <div class="row">
          <div class="col-md-4" style="margin-bottom:4px;">
              <?php if(isset($unitfarmasi) && empty(!$unitfarmasi)){ ?>
                <select class="form-control col-md-4" name="idunitfarmasi" id="idunitfarmasi">
                    <option value="<?= $this->session->userdata('idunitterpilih'); ?>"><?= ucwords($this->session->userdata('unitterpilih')); ?></option>
                    <?php
                        foreach ($unitfarmasi as $arr)
                        {
                            echo "<option value='".$arr['idunit']."'>".$arr['namaunit']."</option>";
                        }
                    ?>
                </select>
                <?php } ?>
          </div>
        <div class="col-xs-12">
            
            
          <div class="box">
            <div class="box-body" id="tablebarang">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                    <tr class="header-table-ql">
                  <th>No</th>
                  <th>Kode</th>
                  <th>Nama Barang</th>
                  <th>Stok</th>
                  <th>Satuan</th>
                  <th>Sediaan</th>
                  <th>Jenis</th>
                  <th>Tipe Obat</th>
                  <th>Kekuatan</th>
                </tr>
                </thead>
                <tbody>
                <!-- list barang dari js_masterbarang -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    
  </section>
  <!-- /.content -->