   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">

          <div class="box-body">
            <div class="col-md-6 row">
              <div class="col-md-2"><label>Tgl Awal:</label><br/><input name="tanggal1" id="tanggal1" class="datepicker" size="7"/></div>
              <div class="col-md-2"><label>Tgl Akhir:</label><br/><input name="tanggal2" id="tanggal2" class="datepicker" size="7"/></div>
              <div class="col-md-8">
                <label>&nbsp;</label><br/>
                <a id="tampil" class="btn btn-info btn-sm"><i class="fa fa-desktop"></i> Tampil</a>
                <a id="refresh" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Refresh</a>
                <a href="<?=base_url('cfarmasi/tambahreturkedistributor');?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Retur Barang</a>
              </div>
            </div>
            <table id="dt_returkedistributor" class="table table-hover table-bordered  dt-responsive"  width="100%">
                <thead>
                    <tr class="bg bg-yellow-gradient">
                        <td>Distributor</td>
                        <td>No.Faktur</td>
                        <td>Tgl.Faktur</td>
                        <td>Tgl.Retur</td>
                        <td>Status</td>
                        <td>Keterangan</td>
                        <td>Detail</td>
                        <td width="100px"></td>
                    </tr>
                </thead>
                <tbody></tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        
      <?php }else if( $mode=='tambah'){ ?>
        <style>.col-md-2{width:12%;}</style>
        <form action="<?= base_url('cfarmasi/save_returkedistributor');?>" method="POST" class="form-horizontal" id="Formretur">
        <div class="box">
          <input name="modeubah" type="hidden">
          <input name="ip" type="hidden">
          <div class="box-body">
              <div class="form-group">
                <label for="" class="col-xs-12 col-md-2 control-label">Tanggal Retur</label>
                <div class="col-xs-12 col-md-2"><input id="tanggalretur" type="text" class="form-control" name="tanggalretur" value=""></div>                
              </div>
              
              <div class="form-group">
                <label for="" class="col-xs-12 col-md-2 control-label">Faktur Pembelian</label>
                <div class="col-xs-12 col-md-3"><select id="idbarangfaktur" name="idbarangfaktur" class="form-control select2"></select></div>
              </div>
              
              <div class="form-group">
                <label for="" class="col-xs-12 col-md-2 control-label">Keterangan</label>
                <div class="col-xs-12 col-md-3"><textarea class="form-control" name="keterangan" placeholder="keterangan retur"></textarea></div>
              </div>
          </div>
            
            <div class="box box-default box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail Barang Retur</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-12">
                      <table class="table table-striped table-bordered table-hover">
                        <thead>
                          <tr class="bg bg-yellow">
                            <th>Kode</th>
                            <th>Barang/Obat</th>
                            <th>Batch.No</th>
                            <th>Kadaluarsa</th>
                            <th style="width:90px;">Jumlah</th>
                            <th>Satuan</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="dtbodydetailretur"><!-- ditampilkan dari js --></tfoot>
                      </table>
                    </div>
                </div>
                <center>
                  <a class="btn btn-warning btn-lg" onclick="simpanretur()">SIMPAN</a>
                  <a class="btn btn-warning btn-lg" href="<?= base_url('cfarmasi/returkedistributor'); ?>" >KEMBALI</a>
                </center>
                <br>      
            </div>
            <!-- /.box-body -->
          </div>
            
            
        </div>
        </form>
        
        <?php }?> 
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
  
  
  
  