<!-- Main content -->
<br>
<br>
<style type="text/css">.input-lg {
  margin-top: 12px;
    height: 82px;
    padding: 10px 16px;
    font-size: 30px;
    line-height: 1.3333333;
    border-radius: 12px;
}</style>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-warning">
        <div class="box-header with-border">
          <h2 ><b>MASUKAN No.NIK / No.RM / No. BPJS</b></h2>
        </div>
        <div class="box-body text-center">
          <input onkeyup="$('#noidentitas').css('background-color','#fff');" onchange="verifikasi_pendaftaran(this.value)" class="form-control input-lg" id="noidentitas" autofocus type="text" placeholder=".input nomor identitas"> 
          <br><a onclick="verifikasi_pendaftaran($('#noidentitas').val())" class="btn btn-warning btn-block btn-lg">VERIFIKASI</a>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>
<!-- /.content -->