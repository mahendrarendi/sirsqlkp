<style type="text/css">.col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}</style>
 <!-- Main content -->
  <section class="content">
    <br>
 <div class="row" style="margin-bottom:5px; ">
     
     <div class="col-sm-2">
        Stasiun <select class="select2 form-control" style="width:100%" id="stasiun" name="stasiun" onchange="pilihstasiun()">
        <?php
        foreach ($stasiun as $row) 
        {
          echo '<option value="'.$row->idstasiun.'">'.$row->namastasiun.'</option>';
        }
        ?>
        </select>
    </div>
    
    <!-- loket -->
    <?php if($this->pageaccessrightbap->checkAccessRight(V_PEMANGGILANANSEMUATRIAN)){?> 
    <div class="col-sm-2">Loket <select class="select2 form-control" style="width:100%" id="pilihloket"></select></div>
    <?php } ?>
    <div class="col-xs-1" style="width: auto;margin: none;padding: 0px;">All <br><input type="checkbox" id="semuaantrian" ></div>
    
    <div class="col-sm-2">
        Tanggal<input type="text" name="" id="tanggal" class="datepicker form-control"> 
    </div>

    <div class="col-sm-2">
        <br>
        <a class="btn btn-warning btn-md" onclick="buattombolloket();">Tampilkan/Refresh</a>
     </div>
     <div class="col-sm-8">&nbsp;</div>
 </div>
  <div  class="row" id="tombolberikutnya"> </div>
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <div class="box">
          <!-- <div class="box-header">
            <h3 class="box-title pull-left"><i class="fa fa-table"></i> <?php echo $table_title; ?></h3>
          </div> -->
          <!-- /.box-header -->
          <div class="box-body" id="listantrian">
            <table class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
              <thead>
              <tr>
                <th>LOKET - NO.ANTRIAN - NAMA PASIEN</th>
                <th></th>
              </tr>
              </thead>
              <tbody>
              <!-- START TAMPIL DATA-->
              <!-- data ditampilkan oleh js_administrasipemanggilanantrian -->
              <!-- END TAMPIL DATA-->
              </tfoot>
            </table>
          </div>
          
          <div class="bg bg-info padding2 text text-sm" style="margin:8px;">
              <b>Panduan Penggunaan</b>
              <ol>
                  <li>Poli Spesialis</li>
                  Urutan proses : Antrian - Panggil - Sedang Proses - OK
                  <li>Non Poli / Penunjang</li>
                  Urutan proses : Antrian - Sedang Proses - Panggil (untuk pengambilan hasil) - OK
              </ol>
              <label class="label label-danger">
                  #Waktu pelayanan terhadap pasien dihitung dari <strong>Sedang Proses</strong> sampai <strong>OK</strong>
              </label>
              
        
          </div>
          <div class="clearfix"></div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <!-- end mode view -->
        <!-- start mode periksa -->
        <?php } ?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->