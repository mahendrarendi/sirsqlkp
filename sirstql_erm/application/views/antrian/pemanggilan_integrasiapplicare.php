<style type="text/css">.col-xs-2{position: relative;padding-right: 4px;padding-left: 4px;}</style>
<section >
  <div class="row" style="margin:none; ">
    <div class="col-xs-12">
    
    <div class="row" style="padding: none; margin: top-5px;max-height: 300px;">
    <div class="col-sm-2" >
    </div>
    <div class="col-sm-8" align="center">
      <span style="font-size: 48px; text-align:center;">Ketersediaan Kamar</span>
      <div class="row" style="font-size: 20px;">
        <div style="background-color: #fff; width: -moz-fit-content;">
            <table class="table table-bordered table-striped div-center" style="font-size: 20px;">
            <tbody>
              <tr>
                <th style="vertical-align:middle; text-align:center;" rowspan="2">Ruang</th>
                <th style="vertical-align:middle; text-align:center;" rowspan="2">Kapasitas</th>
                <th style="vertical-align:middle; text-align:center;" rowspan="2">Tersedia</th>
                <th style="text-align:center;" colspan="3">Kategori Tersedia</th>
              </tr>
              <tr>
                <th>Campuran</th>
                <th>Pria</th>
                <th>Wanita</th>
              </tr>
            </tbody>
            <tbody>
              <?php
              if(!empty($applicare))
              {
                foreach ($applicare as $arr)
                { echo
                '<tr>
                  <td style="padding-top: 4px; padding-bottom: 4px;" >'.$arr['namaruang'].'</td>'.
                  '<td style="padding-top: 4px; padding-bottom: 4px; text-align:center;" >'.$arr['kapasitas'].'</td>'.
                  '<td style="padding-top: 4px; padding-bottom: 4px; text-align:center;">'.$arr['tersedia'].'</td>'.
                  '<td style="padding-top: 4px; padding-bottom: 4px; text-align:center;">'.$arr['tersediapriawanita'].'</td>'.
                  '<td style="padding-top: 4px; padding-bottom: 4px; text-align:center;">'.$arr['tersediawanita'].'</td>'.
                  '<td style="padding-top: 4px; padding-bottom: 4px; text-align:center;">'.$arr['tersediapria'].'</td>'.
                '</tr>';
                }
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row" style="font-size: 14px;text-align:right;"><b>Data diupdate pada : <span id="datetime"></span></b></div>
    </div>
    <div class="col-sm-2" >
    </div>
</div>
</div>
</section>

<script>
  function refreshTime(){
    let timeDisplay = document.getElementById("datetime");
    let dateString = new Date().toLocaleString();
    let formattedString = dateString.replace(", ", " - ");
    timeDisplay.textContent = formattedString;
  }
     
  refreshTime();
  setTimeout(function(){ 
    window.location.reload(1);    
  }, 28877000);
</script>