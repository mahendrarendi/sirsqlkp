
   <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                <thead>
                <tr class="bg bg-yellow-gradient">
                  <th>Nama Stasiun</th>
                  <th>Ambil Antrian</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($stasiun))
                {
                  foreach ($stasiun as $obj) 
                  {
                    
                    echo '<tr>';
                    echo '  <td>'.$obj->namastasiun.'</td>
                            <td><a data-toggle="tooltip" title="" data-original-title="'.$obj->namastasiun.'" class="btn btn-warning btn-xs" href="'.base_url('cantrian/pengambilanantrian/'.$obj->idstasiun).'" ><i class="fa fa-desktop"></i></a></td>
                          </tr>';
                  }
                }
                ?>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
        