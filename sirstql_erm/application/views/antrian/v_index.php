<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIRSTQL <?php echo $title_page==='' ? '' : '- ' ;  echo $title_page; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"><!-- Tell the browser to be responsive to screen width -->
  <link href="<?php echo base_url()?>assets/plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"><!-- Bootstrap 3.3.7 -->
  <link href="<?php echo base_url()?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"><!-- Font Awesome -->
  <link href="<?php echo base_url()?>assets/dist/css/AdminLTE.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet">
  <link href="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/dist/css/animate.min.css" rel="stylesheet"/><!-- Animation library for notifications   -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon-16x16.png"><!--icon--> 
<script src="<?php echo base_url()?>assets/plugins/jquery/dist/jquery.min.js"></script> <!-- jQuery 3 -->
  <?php 
  if(!empty($plugins))
  {
    foreach ($plugins as $i => $value) 
    {
      $this->load->view('plugins/'.$plugins[$i].'css');//<!-- Plugins css tambahan-->
    }
  }
  ?>
  <link href="<?php echo base_url(); ?>assets/dist/css/sirstql.css" rel="stylesheet"/>
</head>
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="skin-yellow sidebar-mini" style="background-color: #ecf0f5;">
  <div id="qrcode" style="width:100px; height:100px; margin-top:15px;display: none;"></div>
               
<!-- Site wrapper -->
<div class="container " >
      <div class="container" style=" padding-left: 1.5%;">
        <p><h1><?php echo ucwords($title_page);?> </h1> </p>
      </div>
      <div class="row " style="font-size: 9px; padding-left: 3%;"><?= COPY_RIGHT; ?></div>
      <div id="notif"></div> <!-- Set notifikasi -->
      <input type="hidden" id="set_status" value="<?= $this->session->flashdata('message')['status'] ?>">
      <input type="hidden" id="set_message" value="<?= $this->session->flashdata('message')['message'] ?>">
      <input type="hidden" id="page_view" value="<?= $title_page; ?>" /> <!-- set isi page view -->
      <?php $this->load->view($content_view); ?>  
      
    <!-- END CONTENT -->
</div>
<script>sessionStorage.user='<?php echo $this->session->userdata('username'); ?>';</script>
<script type="text/javascript"> var base_url ='<?php echo base_url(); ?>';</script> <!-- inisialisasi base url javascript -->
<script src="<?php echo base_url()?>assets/dist/js/qrcode.min.js"></script><!-- QRCODE -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script><!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url()?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script><!-- SlimScroll -->
<script src="<?php echo base_url()?>assets/plugins/fastclick/lib/fastclick.js"></script><!-- FastClick -->
<script src="<?php echo base_url()?>assets/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/jquery-confirm/dist/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/dist/js/bootstrap-notify.js"></script><!--  Notifications Plugin    -->

<script src="<?php echo base_url(); ?>assets/dist/js/sirstql.min.js"></script>
<?php 
 if(!empty($plugins))
  {
    foreach ($plugins as $i => $value) 
    {
      $this->load->view('plugins/'.$plugins[$i].'js'); //<!-- Plugins js tambahan-->
    }
  }
if(isset($script_js)){
if(empty(!$script_js))
{
    foreach ($script_js as $key => $value) {
        echo '<script src="'. base_url() .'assets/pages/'.$value.'.min.js?ver=1"></script>';
    }
}}
?>

</body>
</html>

   