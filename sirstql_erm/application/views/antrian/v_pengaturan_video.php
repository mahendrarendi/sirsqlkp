<style> 
    .bg-video{ 
        background-color:#eee; 
        color:#000; 
        padding:12px; 
        border-radius: 6px; 
        border: 2px solid #dd4b39 !important; 
        font-size: 9px; 
        margin-top:-5px; 
        margin-bottom:-3px; 
        min-height: 100px; 
    } 
    .title-body h5 {padding-bottom: 10px !important;} 
    .title-body em {margin-bottom: 15px !important;display: block;font-size: 12px;} 
    .vd-col { 
        height: 390px !important; 
        max-height: 390px !important; 
        overflow: hidden; 
    } 
</style> 
<!-- Main content --> 
 <section class="content"> 
   <div class="row"> 
     <div class="col-xs-12"> 
       <!-- mode view --> 
       <div class="box"> 
 
         <div class="box-body"> 
           <div class="toolbar"> 
                <a id="addvideo" class="btn btn-primary btn-sm"><i class="fa  fa-upload"></i> Upload Video</a>  
                <a onclick="window.location.reload()" class="btn btn-warning btn-sm"><i class="fa  fa-refresh"></i> Reload</a>  
                &nbsp;&nbsp; 
                <labe>Halaman :  
                    <select id="halaman" name="halaman" style="padding:4px;"> 
                        <?php 
                            for($x=1; $x<=$halaman['jumlah'];$x++) 
                            { 
                               echo ' <option '.((isset($get['halaman']) && $get['halaman'] == $x ) ? 'selected' : '' ).' value="'.$x.'">'.$x.'</option>'; 
                            } 
                        ?> 
                    </select> 
                </label> 
                &nbsp;&nbsp; 
                <label>Semua Video : <?= $halaman['total']; ?></label> 
            </div> 
             <br> 
           
             <div class="row"> 
                <!--tampilkan video--> 
                <?php foreach ($video as $arr) : ?> 
                    <div class="col-md-3 col-xs-6 vd-col" id="display_video<?= $arr['idvideo']; ?>"> 
                        <!-- <div class="small-box bg-red"> --> 
                            <div class="panel panel-default" style="line-height:0;"> 
                                <video src="<?= URLNASSIMRS.$arr['dokumen']; ?>" controls  style="height: 212px; width:100%; max-height: 1000px;"></video> 
                                <div class="panel-body" style="background: #f1f1f1;"> 
                                    <div class="title-body"> 
                                        <div class="bg bg-videos"> 
                                            <h5 class="text-bold"> <?= $arr['namavideo']; ?></h5> 
                                            <em> Published : <?= empty($arr['date_created']) ? '-' : format_date($arr['date_created'],'d M Y, H:i'); ?></em> 
                                        </div> 
                                    </div> 
                                </div> 
                                <div class="panel-footer text-center bg-red"> 
                                    <a href="javascript:void(0)"  id="hapusvideo" idvideo="<?= $arr['idvideo']; ?>" namavideo="<?= $arr['namavideo']; ?>" dokumen="<?= $arr['dokumen']; ?>" class="small-box-footer" style="color:white;">Hapus <i class="fa fa-trash"></i></a> 
                                </div> 
                            </div> 
                        <!-- </div> --> 
                    </div>    
                <?php endforeach; ?> 
                </div> 
            </div> 
        </div> 
   </div> 
   <!-- /.col --> 
 </div> 
 <!-- /.row --> 
</section> 
<!-- /.content -->