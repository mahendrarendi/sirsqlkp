<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_location" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'by_identifier') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_location?tabActive=by_identifier') ?>">Cari berdasarkan Identifier</a></li>              
              <li class="<?= (($tabActive == 'by_name') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_location?tabActive=by_name') ?>">Cari berdasarkan Nama</a></li>
              <li class="<?= (($tabActive == 'by_orgid') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_location?tabActive=by_orgid') ?>">Cari berdasarkan Id Organisasi</a></li>
              <li class="<?= (($tabActive == 'by_id') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_location?tabActive=by_id') ?>">Cari berdasarkan Id Lokasi</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane <?= (($tabActive == 'by_identifier') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_identifier'){ ?>
                  <!--By ID-->
                  <div class="form-horizontal">
                    <div class="form-group byident_loc">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="ident" name="ident" type="text" autocomplete="off" class="form-control ident" placeholder="Masukkan Identifier">
                        </div> 
                        <div class="col-md-1">
                          <a id="cari_ident" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a> 
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbyidentifier" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                      </tr>
                    </thead>
                    <tbody id="bodybyidentifier">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->       
              
              <div class="tab-pane <?= (($tabActive == 'by_name') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_name'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group byname_loc">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="name_loc" name="name_loc" type="text" autocomplete="off" class="form-control name_loc" placeholder="Masukkan Name">
                        </div>   
                        <div class="col-md-1">
                          <a id="cari_name" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbyname" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                      </tr>
                    </thead>
                    <tbody id="bodybyname">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane <?= (($tabActive == 'by_orgid') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_orgid'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group byorgid_loc">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="orgid" name="orgid" type="text" autocomplete="off" class="form-control orgid" placeholder="Masukkan OrgID">
                        </div> 
                        <div class="col-md-1">
                          <a id="cari_orgid" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbyorgid" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                      </tr>
                    </thead>
                    <tbody id="bodybyorgid">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane --> 
              
              <div class="tab-pane <?= (($tabActive == 'by_id') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_id'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group byid_loc">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="id_loc" name="id_loc" type="text" autocomplete="off" class="form-control id_loc" placeholder="Masukkan ID">
                        </div>   
                        <div class="col-md-1">
                          <a id="cari_idloc" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbyid" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Alamat</th> 
                      </tr>
                    </thead>
                    <tbody id="bodybyid">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->  
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->