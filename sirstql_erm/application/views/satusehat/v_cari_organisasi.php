<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="modepage_organization" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'by_id') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_organization?tabActive=by_id') ?>">Cari berdasarkan Id Organisasi</a></li>              
              <li class="<?= (($tabActive == 'by_name') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_organization?tabActive=by_name') ?>">Cari berdasarkan Nama</a></li>
              <li class="<?= (($tabActive == 'by_partof') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_organization?tabActive=by_partof') ?>">Cari berdasarkan Id Part Of</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane <?= (($tabActive == 'by_id') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_id'){ ?>
                  <!--By ID-->
                  <div class="form-horizontal">
                    <div class="form-group byname_org">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="id_org" name="id_org" type="text" autocomplete="off" class="form-control" placeholder="Masukkan ID">
                        </div>   
                        <div class="col-md-1">
                          <a id="cari_id" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbyid" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Phone</th>
                        <th>Email</th>                                        
                        <th>Department</th>
                      </tr>
                    </thead>
                    <tbody id="bodybyid">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->       
              
              <div class="tab-pane <?= (($tabActive == 'by_name') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_name'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group byname_org">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="name_org" name="name_org" type="text" autocomplete="off" class="form-control name_org" placeholder="Masukkan Name">
                        </div>   
                        <div class="col-md-1">
                          <a id="cari_name" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>           
                      </div>         
                    </div>
                  </div>
                  <table id="tblbyname" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Phone</th>
                        <th>Email</th>
                      </tr>
                    </thead>
                    <tbody id="bodybyname">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane <?= (($tabActive == 'by_partof') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_partof'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group bypartof_org">              
                      <div class="ql-inline">    
                        <div class="col-md-3">
                          <input id="partof" name="partof" type="text" autocomplete="off" class="form-control partof" placeholder="Masukkan PartOf">
                        </div>   
                        <div class="col-md-1">
                          <a id="cari_partof" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a> 
                        </div>           
                      </div>
                    </div>
                  </div>
                  <table id="tblbypartof" class="table table-bordered table-striped table-hover dt-responsive" style="font-size: 11.4px;margin-top:2px;" cellspacing="0" width="100%">
                    <thead>
                      <tr class="bg bg-gray">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Alamat</th> 
                      </tr>
                    </thead>
                    <tbody id="bodybypartof">
                    </tfoot>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->              
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->