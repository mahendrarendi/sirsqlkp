
<style type="text/css">
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
    a#kirimsatusehat-disable{
    background-color: gray;
    cursor: not-allowed;
    pointer-events: none;}
</style>
   <!-- Main content -->
   <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- mode view -->
          <?php if( $mode=='view'){ ?>
        <input type="hidden" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'data_lokasi') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/location?tabActive=data_lokasi') ?>">Lokasi</a></li>              
              <li class="<?= (($tabActive == 'data_sub_lokasi') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/location?tabActive=data_sub_lokasi') ?>">Sub Lokasi</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= (($tabActive == 'data_lokasi') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'data_lokasi'){ ?>
                        <div class="col-md-6 row">
                        <a href="<?php echo base_url('csatusehat/add_location'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data Lokasi</a> 
                        <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                        </div>
                        <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                            <thead>
                            <tr class="header-table-ql">
                            <th style="width:10px;">No</th>
                            <th>IHS</th>
                            <th>Nama Ruang</th>
                            <th>Status</th>
                            <th>Tipe Fungsi</th>
                            <th style="width:170px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                        <?php
                            if (!empty($location))
                            {

                                $lokasiMap = [];
                                foreach ($location as $lokasi) {
                                    $lokasiMap[$lokasi->idlocation] = $lokasi;
                                }            
                                $DeleteButton = true;
                                foreach ($sub_location as $sublokasi) {
                                    if (isset($lokasiMap[$sublokasi->idlocation])) {
                                        $DeleteButton = false;
                                        break; // Keluar dari loop jika ada sublokasi yang terkait dengan lokasi
                                    }
                                }
                            $no=0;

                            foreach ($location as $obj) 
                            {
                                $this->encryptbap->generatekey_once("HIDDENTABEL");
                                $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idlocation));
                                $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('satusehat_location'));
                                $idhalaman = $this->encryptbap->encrypt_urlsafe(V_LOKASI_RUANG, "json");
                                $ihs = empty($obj->ihs) ? '<label class="label label-warning label-xs">Data belum teregistrasi ke Satusehat</label>' : $obj->ihs;
                                $btnEdit = !empty($obj->ihs) ? '<a data-toggle="tooltip" title="" data-original-title="Edit" class="btn btn-warning btn-xs" href="'.base_url('csatusehat/edit_location/'.$id).'" ><i class="fa fa-pencil"></i></a>' : '';                
                                $btnSend = empty($obj->ihs) ? '<a href="#" data-toggle="tooltip" title="Integrasikan" onclick="sendData(\'' . htmlspecialchars($id, ENT_QUOTES) . '\', \'loc\')" class="btn btn-info btn-xs"><i class="fa fa-paper-plane"></i></a></td>' : '';
                                echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                                echo '  <td>'.$no.'</td>
                                        <td>'.$ihs.'</td>
                                        <td>'.$obj->namaruang.'</td>
                                        <td>'.get_array_value($status[$obj->status],'Location.status').'</td>
                                        <td>'.get_array_value($type[$obj->type],'Location.type.display').'</td>
                                        <td>'.$btnEdit.'
                                        <a href="#" data-id="'.$id.'" data-toggle="tooltip" data-original-title="Hapus" class="btn btn-xs btn-danger remove-location"><i class="fa fa-trash"></i></a>
                                        '.$btnSend.'
                                    </tr>';
                            }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                <?php } ?>
                </div>

                <div class="tab-pane <?= (($tabActive == 'data_sub_lokasi') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'data_sub_lokasi'){ ?>
                        <div class="col-md-6 row">
                        <a href="<?php echo base_url('csatusehat/add_sub_location'); ?>" class="btn btn-primary btn-sm"><i class="fa  fa-plus-square"></i> Add Data Sub Lokasi</a> 
                        <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                        </div>
                        <table id="table" class="table table-bordered table-striped table-hover dt-responsive" cellspacing="0" width="100%">
                            <thead>
                            <tr class="header-table-ql">
                            <th style="width:10px;">No</th>
                            <th>IHS</th>
                            <th>Nama Ruang</th>
                            <th>Status</th>
                            <th>Tipe Fungsi</th>
                            <th>IHS Induk</th>
                            <th style="width:170px;">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                        <?php
                            if (!empty($sub_location))
                            {
                            $no=0;
                            foreach ($sub_location as $obj) 
                            {
                                $this->encryptbap->generatekey_once("HIDDENTABEL");
                                $partOf = !empty($obj->ihsParent) ? $obj->ihsParent : '<span class="label label-info">Ruang Utama belum teregistrasi</span>';
                                $ihs = empty($obj->ihs) ? '<label class="label label-warning label-xs">Data belum teregistrasi ke Satusehat</label>' : $obj->ihs;  
                                $id =  $this->encryptbap->encrypt_urlsafe(json_encode($obj->idsublocation));
                                $btnSend = empty($obj->ihs) ? '<a href="#" data-toggle="tooltip" title="Integrasikan" onclick="sendData(\'' . htmlspecialchars($id, ENT_QUOTES) . '\', \'subloc\')" class="btn btn-info btn-xs"><i class="fa fa-paper-plane"></i></a></td>' : '';     
                                $tabel = $this->encryptbap->encrypt_urlsafe(json_encode('satusehat_sub_location'));
                                $btnEdit = !empty($obj->ihs) ? '<a data-toggle="tooltip" title="" data-original-title="Edit" class="btn btn-warning btn-xs" href="'.base_url('csatusehat/edit_sub_location/'.$id).'" ><i class="fa fa-pencil"></i></a>' : ''; 
                                $idhalaman = $this->encryptbap->encrypt_urlsafe(V_LOKASI_RUANG, "json");
                                echo '<tr id="row'.++$no.'">'; //--baris diberi nomor agar dapat dihapus langsung tanpa perlu reload halaman (menggunakan ajax). Attribute nobaris harus ada di menu/tombol delete_data
                                echo '  <td>'.$no.'</td>
                                        <td>'.$ihs.'</td>
                                        <td>'.$obj->namaruang.'</td>
                                        <td>'.get_array_value($status[$obj->status],'Location.status').'</td>
                                        <td>'.get_array_value($type[$obj->type],'Location.type.display').'</td>
                                        <td>'.$partOf.'</td>
                                        <td>'.$btnEdit.' 
                                        <a href="#" data-id="'.$id.'" data-toggle="tooltip" data-original-title="Hapus" class="btn btn-xs btn-danger remove-sublocation"><i class="fa fa-trash"></i></a>
                                        '.$btnSend.'
                                    </tr>';
                            }
                            }
                            ?>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                <?php } ?>
                </div>
            <!-- /.box-body -->
          
            </div>
        </div>
          <!-- /.box -->
          <!-- /mode view -->
          
          <!-- mode add or edit -->
          <?php }else if( $mode=='edit_lokasi' || $mode=='add_lokasi'){?>
          <div class="box" id="location">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form class="form-horizontal" id="frm-tambahlocation" method="post" accept-charset="utf-8">
                <!-- <form action="<? // base_url('csatusehat/save_location'); ?>" class="form-horizontal" id="frm-tambahlocation" method="post" accept-charset="utf-8"> -->

                <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idlocation'])) ); ?>" name="id" />
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $data_edit['ihs']); ?>" name="ihs" />
                                <input type="hidden" name="x" required value="loc">

                    <div class="form-group">
                        <?php if($mode==='add_lokasi'){?>
                        <label for="_name_txt" class="col-sm-3 control-label"> Tipe Ruang <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="tiperuang" class="select2 form-control" style="width:100%;" id="tiperuang">
                                <option value="" selected>PILIH TIPE RUANG</option>    
                                <option value="get_unit">Unit</option>
                                <option value="get_bangsal">Bangsal</option>
                                <option value="get_stasiun">Stasiun</option>
                            </select>
                        </div>
                            <?php }else{?>
                                <?php if (!is_null($data_edit['kelas'])){?>
                                    <label for="_name_txt" class="col-sm-3 control-label"  style="display:none;"> Tipe Ruang <span class="asterisk">*</span></label>
                                    <div class="col-sm-6" style="display:none;">
                                        <select name="tiperuang" class="select2 form-control" style="width:100%;" id="tiperuang" readonly>  
                                            <option value="get_bangsal" selected>Bangsal</option>
                                        </select>
                                    </div>
                                <?php }?>
                            <?php } ?>
                    </div>
                                
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Nama Ruangan <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                        <?php if($mode==='add_lokasi'){?>
                            <select name="namaruang" class="select2 form-control" style="width:100%;" id="ruangloc">
                                <option value="">PILIH RUANG</option> 
                            </select>
                            <?php }else{?>
                                <input type="text" onkeyup="" name="namaruang" value="<?= forminput_setvalue($data_edit,'namaruang')?>" class="form-control" id="ruangloc" readonly>
                            <?php } ?>
                        </div>
                    </div>
                    
                    <div class="form-group" style="" id="get_kelas">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kelas </label>
                        <div class="col-sm-6">
                            <select name="kelas" class="select2 form-control" style="width:100%;" id="kelas">
                                <option value="">PILIH KELAS</option> 
                                <?php 
                                    foreach($location_get_service_class as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['kelas']) && $key == $data_edit['kelas']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.extension.serviceClass.display']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Deskripsi / detail alamat ruang <br><small>*untuk generate kode ruangan</small></label>
                        <div class="col-sm-6">
                          <textarea name="deskripsi" id="deskripsi" cols="30" rows="3" class="form-control" ><?= forminput_setvalue($data_edit,'deskripsi')?></textarea>
                          <small class="labeldeskripsi"></small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Telepon Penanggung Jawab </label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="" name="telepon" value="<?= forminput_setvalue($data_edit,'telepon')?>" class="form-control" id="telepon"  placeholder="MASUKKAN NOMOR TELEPON">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode Ruangan </label>
                        <div class="col-sm-6">  
                            <input type="text" name="koderuang" class="form-control" id="koderuang" style="text-transform:uppercase;" value="<?= forminput_setvalue($data_edit,'koderuangan')?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Status Ruangan <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                        <select name="status" class="select2 form-control" style="width:100%;" id="status">
                            <option value="" selected>PILIH STATUS</option>    
                            <?php 
                            foreach($location_get_status as $key => $item){
                                $selected = ($data_edit && isset($data_edit['status']) && $key == $data_edit['status']) ? 'selected' : '';
                            ?>
                            <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.status']?></option>
                            <?php } ?>
                        </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Tipe Fungsi </label>
                        <div class="col-sm-6">
                            <select name="tipefungsi" class="select2 form-control" style="width:100%;" id="tipefungsi">
                                <option value="" selected>PILIH TIPE FUNGSI RUANGAN</option>    
                                <?php 
                                    foreach($location_get_type as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['type']) && $key == $data_edit['type']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.type.display']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Status Operasional </label>
                        <div class="col-sm-6">
                            <select name="statusoperasional" class="select2 form-control" style="width:100%;" id="statusoperasional">
                                <option value="" selected>PILIH STATUS OPERASIONAL</option>
                                <?php 
                                    foreach($location_get_operational_status as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['operationalstatus']) && $key == $data_edit['operationalstatus']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Tipe Fisik Lokasi<span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="tipefisik" class="select2 form-control" style="width:100%;" id="tipefisik">
                                <option value="" selected>PILIH TIPE FISIK LOKASI</option>
                                <?php 
                                    foreach($location_get_physical_type as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['physicaltype']) && $key == $data_edit['physicaltype']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php  echo $selected; ?>><?php echo $item['Location.physicalType.coding.display']?> - <?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Lokasi Spesifik / Kelompok Ruang <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="mode" class="select2 form-control" style="width:100%;" id="mode">
                                <option value="" selected>PILIH LOKASI SPESIFIK / KELOMPOK RUANG</option> 
                                <?php 
                                    foreach($location_get_mode as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['mode']) && $key == $data_edit['mode']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.mode']?> - <?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <center style="padding-top: 8px;margin-bottom: 20px;">
                        <div class="row">
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                            <a class="btn btn-danger btn-sm" href="<?= base_url('csatusehat/location'); ?>">Kembali</a>
                        </div>
                    </center>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php }else if( $mode=='edit_sub_lokasi' || $mode=='add_sub_lokasi'){ ?>
          <div class="box" id="sub-location">
            <div class="box-header">
              <center>
                <br>
              <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
              </center>
              <br>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form class="form-horizontal" id="frm-tambahsublocation" method="post" accept-charset="utf-8">
                <!-- <form action="<?// base_url('csatusehat/save_sub_location'); ?>" class="form-horizontal" id="frm-tambahsublocation" method="post" accept-charset="utf-8"> -->

                    <?php $this->encryptbap->generatekey_once("HIDDENTABEL"); ?>
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $this->encryptbap->encrypt_urlsafe(json_encode($data_edit['idsublocation'])) ); ?>" name="id"/>
                    <input type="hidden" value="<?= ((empty($data_edit)) ? '' : $data_edit['ihs']); ?>" name="ihs" />
                    <input type="hidden" name="x" required value="subloc">
                    <?php if($mode== 'add_sub_lokasi'){?>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Ruang Utama <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="ruangutama" class="select2 form-control" style="width:100%;" id="ruangutama">
                                <option value="">PILIH RUANG UTAMA</option> 
                            </select>
                        </div>
                    </div>
                    <?php }else {?>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Ruang Utama <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="" name="ruangutama" value="<?= forminput_setvalue($data_edit,'ruangutama')?>" class="form-control" id="ruangutama" readonly>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if($mode== 'add_sub_lokasi'){?>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Nama Sub Ruang <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="namasubruang" class="select2 form-control" style="width:100%;" id="namasubruang">
                                <option value="">PILIH NAMA SUB RUANG</option> 
                            </select>
                        </div>
                    </div>
                    <?php }else {?>
                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Nama Sub Ruang <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="" name="namasubruang" value="<?= forminput_setvalue($data_edit,'namaruang')?>" class="form-control" readonly>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Deskripsi / detail alamat ruang </label>
                        <div class="col-sm-6">
                          <textarea name="deskripsi" id="deskripsi" cols="30" rows="3" class="form-control" ><?= forminput_setvalue($data_edit,'deskripsi')?>Ruang Poli ..............., Lantai ..., Gedung Utara/Selatan, RSU Queen Latifa</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Telepon Penanggung Jawab </label>
                        <div class="col-sm-6">
                            <input type="text" onkeyup="" name="telepon" value="<?= forminput_setvalue($data_edit,'telepon')?>" class="form-control" id="telepon"   placeholder="MASUKKAN NOMOR TELEPON">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Alamat Lantai Gedung <br><small>*untuk generate kode ruangan</small></label>
                        <div class="col-sm-6">
                            <select name="" class="select2 form-control" style="width:100%; margin-bottom: 15px;" id="lantai">
                                <option value="" selected>PILIH LANTAI</option> 
                                <option value="Lantai 1">Lantai 1</option>
                                <option value="Lantai 2">Lantai 2</option>
                                <option value="Lantai 3">Lantai 3</option>
                                <option value="Lantai 4">Lantai 4</option>
                                <option value="Lantai 5">Lantai 5</option>
                                <option value="Lantai 6">Lantai 6</option>
                            </select>
                            <select name="" class="select2 form-control" style="width:100%;" id="gedung">
                                <option value="" selected>PILIH GEDUNG</option>    
                                <option value="Gedung Utara">Gedung Utara</option>
                                <option value="Gedung Selatan">Gedung Selatan</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Kode Ruangan </label>
                        <div class="col-sm-6">
                            <input type="text" value ="<?= forminput_setvalue($data_edit,'koderuangan')?>" onkeyup="" name="koderuang" class="form-control" id="koderuang" style="text-transform:uppercase;" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Status Ruangan <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="status" class="select2 form-control" style="width:100%;" id="status">
                                <option value="" selected>PILIH STATUS</option>    
                                <?php 
                                    foreach($location_get_status as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['status']) && $key == $data_edit['status']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>"  <?php echo $selected; ?>><?php echo $item['Location.status']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Tipe Fungsi </label>
                        <div class="col-sm-6">
                            <select name="tipefungsi" class="select2 form-control" style="width:100%;" id="tipefungsi">
                                <option value="" selected>PILIH TIPE FUNGSI RUANGAN</option>    
                                <?php 
                                    foreach($location_get_type as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['type']) && $key == $data_edit['type']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.type.display']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Status Operasional </label>
                        <div class="col-sm-6">
                            <select name="statusoperasional" class="select2 form-control" style="width:100%;" id="statusoperasional">
                                <option value="" selected>PILIH STATUS OPERASIONAL</option>
                                <?php 
                                    foreach($location_get_operational_status as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['operationalstatus']) && $key == $data_edit['operationalstatus']) ? 'selected' : '';

                                ?>
                                <option value="<?php echo $key?>"  <?php echo $selected; ?>><?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Tipe Fisik Lokasi<span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="tipefisik" class="select2 form-control" style="width:100%;" id="tipefisik">
                                <option value="" selected>PILIH TIPE FISIK LOKASI</option>
                                <?php 
                                    foreach($location_get_physical_type as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['physicaltype']) && $key == $data_edit['physicaltype']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>"<?php echo $selected; ?>><?php echo $item['Location.physicalType.coding.display']?> - <?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="_name_txt" class="col-sm-3 control-label"> Lokasi Spesifik / Kelompok Ruang <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <select name="mode" class="select2 form-control" style="width:100%;" id="mode">
                                <option value="" selected>PILIH LOKASI SPESIFIK / KELOMPOK RUANG</option> 
                                <?php 
                                    foreach($location_get_mode as $key => $item){
                                        $selected = ($data_edit && isset($data_edit['mode']) && $key == $data_edit['mode']) ? 'selected' : '';
                                ?>
                                <option value="<?php echo $key?>" <?php echo $selected; ?>><?php echo $item['Location.mode']?> - <?php echo $item['Keterangan']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <center style="padding-top: 8px; margin-bottom: 20px;" >
                        <div class="row">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                        <a class="btn btn-danger btn-sm" href="<?= base_url('csatusehat/location?tabActive=data_sub_lokasi'); ?>">Kembali</a>
                        </div>
                    </center>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
           
          <?php } ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->