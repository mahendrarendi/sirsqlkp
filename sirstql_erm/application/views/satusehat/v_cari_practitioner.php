<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
    
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'by_id') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_practitioner?tabActive=by_id') ?>">Cari berdasarkan Id</a></li>              
              <li class="<?= (($tabActive == 'by_nik') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_practitioner?tabActive=by_nik') ?>">Cari berdasarkan NIK</a></li>              
              <li class="<?= (($tabActive == 'by_ngb') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/find_practitioner?tabActive=by_ngb') ?>">Cari berdasarkan Nama, Jenis Kelamin, dan Tanggal Lahir</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane <?= (($tabActive == 'by_nik') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_nik'){ ?>
                  <!--By ID-->
                  <div class="form-horizontal">
                    <div class="form-group byname_org">              
                      <div class="ql-inline">
                        <div class="col-xs-12 col-md-4">
                            <label>NIK:</label> 
                            <input type="text" placeholder="ketik nik"  id="nik" autocomplete="off" class="form-control" name="nik">
                        </div>
                        <div class="col-md-1">
                          <label>&nbsp;</label><br>
                          <a id="caripractitioner" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <h3 style="display:none;" id="hasilpencarian">Hasil Pencarian</h3>
                  <table class="table table-striped" cellspacing="0" style="font-size: 1.3rem;">
                    <tbody id="result" ></tbody>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->       
              
              <div class="tab-pane <?= (($tabActive == 'by_ngb') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_ngb'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group byname_org">              
                      <div class="ql-inline">
                        <div class="col-md-4">
                          <label for="nameGenderDOBInput">Nama:</label>
                          <input type="text" id="name" name="name" class="form-control" required>
                        </div>
                        <div class="col-md-4">
                          <label for="genderInput">Jenis Kelamin:</label>
                          <select id="gender" name="gender" class="form-control" required>
                            <option value="">Pilih</option>
                            <option value="male">Laki-laki</option>
                            <option value="female">Perempuan</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <label for="tanggalLahirInput">Tanggal Lahir:</label>
                          <input type="date" id="birth" name="birth" class="form-control" required>
                        </div>
                        <div class="col-md-1">
                          <label>&nbsp;</label><br>
                          <a id="caripractitioner" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                        </div>
                      </div>         
                    </div>
                  </div>
                  
                  <br>
                  <h3 style="display:none;" id="hasilpencarian">Hasil Pencarian</h3>
                  <table class="table table-striped" cellspacing="0" style="font-size: 1.3rem;">
                    <tbody id="result" ></tbody>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->
              
              <div class="tab-pane <?= (($tabActive == 'by_id') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'by_id'){ ?>
                  <div class="form-horizontal">
                    <div class="form-group bypartof_org">              
                      <div class="ql-inline">
                          <div class="col-xs-12 col-md-4">
                              <label>Id:</label> 
                              <input type="text" placeholder="Ketik Id IHS"  id="id" autocomplete="off" class="form-control">
                          </div>
                          <div class="col-md-1">
                            <label>&nbsp;</label><br>
                            <a id="caripractitioner" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> Cari</a>
                          </div>
                      </div>
                    </div>
                  </div>
                  
                  <br>
                  <h3 style="display:none;" id="hasilpencarian">Hasil Pencarian</h3>
                  <table class="table table-striped" cellspacing="0" style="font-size: 1.3rem;">
                    <tbody id="result" ></tbody>
                  </table>
                <?php } ?>
              </div>
              <!-- /.tab-pane -->              
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }?>
          
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->