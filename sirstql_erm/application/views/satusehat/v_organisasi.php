<style type="text/css">
    .col-sm-1, .col-sm-2{padding-right: 1px;margin:0px;}
    input{height: 30px;}
    .nav-tabs-custom>.nav-tabs>li.active {border-top-color: #f39c12;}
    .tab-content{margin-top:12px;}
    .ql-group {
        margin-bottom: 15px;
    }
</style>
 <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <!-- mode view -->
        <?php if( $mode=='view'){ ?>
        <input type="hidden" id="tabactive" value="<?= $tabActive; ?>"/>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="<?= (($tabActive == 'org') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/organization?tabActive=org') ?>">Organisasi</a></li>              
              <li class="<?= (($tabActive == 'suborg') ? 'active' : '' ); ?>"><a href="<?= base_url('csatusehat/organization?tabActive=suborg') ?>">Sub Organisasi</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane <?= (($tabActive == 'org') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'org'){ ?>
                        <div class="row" style="margin-bottom:10px;">
                            <div class="col-md-6">
                                <a href="<?= base_url('csatusehat/add_edit_organization'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data Organisasi</a>
                                <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <table id="tblOrg" class="table table-bordered table-striped table-hover dt-responsive">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th>No</th>
                                        <th>IHS</th>
                                        <th>Nama Organisasi</th>
                                        <th>Status Organisasi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="bodyorg">
                                </tbody>
                            </table>
                            </div>
                        </div>
                <?php } ?>
                </div>
                <!-- /.tab-pane -->
                
                <div class="tab-pane <?= (($tabActive == 'suborg') ? 'active' : '' ); ?>">
                <?php if($tabActive == 'suborg'){ ?>
                    <div class="row" style="margin-bottom:10px;">
                            <div class="col-md-6">
                                <a href="<?= base_url('csatusehat/add_edit_sub_organization'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-square"></i> Add Data Organisasi</a>
                                <a href="#" onclick="window.location.reload(true);" class="btn btn-warning btn-sm"><i class="fa fa-refresh"></i> Refresh</a> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <table id="tblSubOrg" class="table table-bordered table-striped table-hover dt-responsive">
                                <thead>
                                    <tr class="header-table-ql">
                                        <th>No</th>
                                        <th>IHS</th>
                                        <th>Nama Organisasi</th>
                                        <th>Status Organisasi</th>
                                        <th>Organisasi Induk</th>
                                        <th>IHS Induk</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody id="bodysuborg">
                                </tbody>
                            </table>
                            </div>
                        </div>
                <?php } ?>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
        <?php }else if($mode == 'edit_organisasi' || $mode == 'add_organisasi'){?>
        
            <div class="box">
                <div class="box-header">
                    <center>
                        <br>
                    <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
                    </center>
                    <br>
                </div>
            <!-- /.box-header -->
                <div class="box-body">
                <?php 
                    $name       = isset($get_edit->name) ? $get_edit->name : '';
                    $alias      = isset($get_edit->alias) ? $get_edit->alias : '';
                    $status     = isset($get_edit->status) ? $get_edit->status : '';
                    $phone      = isset($get_edit->phone) ? $get_edit->phone : '';
                    $email      = isset($get_edit->email) ? $get_edit->email : '';    
                    $ihs        = isset($get_edit->ihs) ? $get_edit->ihs : '';                
                    $tp          = isset($get_edit->type) ? $get_edit->type : '';

                ?>
                        <form class="form-horizontal" id="frm-tambahorganization">
                            <div class="err-msg"></div>
                            <?php if($mode == 'edit_organisasi'): ?>
                                <input type="hidden" name="paramid" required value="<?= $paramid; ?>">
                                <input type="hidden" name="ihs" required value="<?= $ihs; ?>">
                                <input type="hidden" name="x" required value="org">
                            <?php endif; ?>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Status Keaktifan Organisasi <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <select name="status" class="form-control" required>
                                        <?php if($mode == "add_organisasi"){?>  
                                            <option value="">--- Pilih ---</option>  
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                            <?php }else{?>
                                                <?php if($status == 1){?>  
                                                    <option value="">--- Pilih ---</option>  
                                                    <option value="1" selected>Aktif</option>
                                                    <option value="0">Tidak Aktif</option>
                                                    <?php }else{?>
                                                    <option value="">--- Pilih ---</option>  
                                                    <option value="1">Aktif</option>
                                                    <option value="0" selected>Tidak Aktif</option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Type <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <select name="type" class="form-control" required>
                                            <option value="">--- Pilih ---</option>
                                            <?php 
                                            foreach($type as $key => $item){
                                                $selected = (isset($tp) && $key == $tp) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $key?>" <?= $mode=='add_organisasi' ? '' : $selected;?>><?php echo $item['Keterangan']?></option>
                                            <?php } ?>
                                        
                                        <?php 
                                        foreach( $type as $row ){ ?>
                                            <option value="<?= $row['Organization.type.code']; ?>"><?= $row['Keterangan']; ?></option>
                                        <?php 
                                        } ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input value="<?= $name; ?>" required name="name" class="form-control" placeholder="Nama Organiasi"></input>
                                    </div>
                                    </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Phone <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input value="<?= $phone; ?>" required name="phone" class="form-control" placeholder="Phone"></input>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Email <span class="required">*</span></label>
                                    <div class="col-sm-6">    
                                        <input value="<?= $email; ?>" required name="email" class="form-control" placeholder="Email"></input>
                                    </div>
                                </div>
                            <center style="padding-top: 8px; margin-bottom: 30px;">
                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="<?= base_url('csatusehat/organization'); ?>">BACK</a>
                                </div>
                            </center>
                        </form>
                    </div>
                </div>
        <?php }else if($mode == 'edit_sub_organisasi'|| $mode == 'add_sub_organisasi'){?>
            <div class="box">
                <div class="box-header">
                    <center>
                        <br>
                    <h2 class="box-title"><?php echo strtoupper($title_page) ;?></h2>
                    </center>
                    <br>
                </div>
            <!-- /.box-header -->
                <div class="box-body">
                <?php 
                    $name           = isset($get_edit->name) ? $get_edit->name : '';
                    $status         = isset($get_edit->status) ? $get_edit->status : '';
                    $phone          = isset($get_edit->phone) ? $get_edit->phone : '';
                    $email          = isset($get_edit->email) ? $get_edit->email : '';
                    $ihs            = isset($get_edit->ihs) ? $get_edit->ihs : '';
                    $organisasiinduk= isset($get_edit->organisasiinduk) ? $get_edit->organisasiinduk : '';
                    $idorganization = isset($get_edit->idorganization) ? $get_edit->idorganization : '';
                    $tp          = isset($get_edit->type) ? $get_edit->type : '';
                ?>
                        <form class="form-horizontal" id="frm-tambahsuborganization">
                            <div class="err-msg"></div>
                            <?php if($mode == 'edit_sub_organisasi'): ?>
                                <input type="hidden" name="paramid" required value="<?= $paramid; ?>">
                                <input type="hidden" name="ihs" required value="<?= $ihs; ?>">
                                <input type="hidden" name="x" required value="suborg">

                            <?php endif; ?>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Organisasi Induk <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <?php if($mode == 'add_sub_organisasi'){?>
                                        <select id="organisasi_induk" name="organisasi_induk" class="form-control" required>
                                            <option value="">--- Pilih ---</option>    
                                            <?php foreach ($org_induk as $row) : ?>
                                            <option value="<?= $row->idorganization?>"><?= $row->name?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php }else{ ?>
                                            <input value="<?= $organisasiinduk; ?>" required name="organisasi_induk" class="form-control" placeholder="" readonly></input>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Status Keaktifan Organisasi <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <select id="status" name="status" class="form-control" required>
                                        <?php if($mode == "add_sub_organisasi"){?>  
                                            <option value="">--- Pilih ---</option>  
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                            <?php }else{?>
                                                <?php if($status == 1){?>  
                                                    <option value="">--- Pilih ---</option>  
                                                    <option value="1" selected>Aktif</option>
                                                    <option value="0">Tidak Aktif</option>
                                                    <?php }else{?>
                                                    <option value="">--- Pilih ---</option>  
                                                    <option value="1">Aktif</option>
                                                    <option value="0" selected>Tidak Aktif</option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Type <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <select id="type" name="type" class="form-control" required>
                                            <option value="">--- Pilih ---</option>
                                            <?php 
                                            foreach($type as $key => $item){
                                                $selected = (isset($tp) && $key == $tp) ? 'selected' : '';
                                            ?>
                                            <option value="<?php echo $key?>" <?= $mode=='add_sub_organisasi' ? '' : $selected;?>><?php echo $item['Keterangan']?></option>
                                            <?php } ?>
                                        </select> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Name <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input value="<?= $name; ?>" required name="name" class="form-control" placeholder="Nama Organiasi" id="nama"></input>
                                    </div>
                                    </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Phone <span class="required">*</span></label>
                                    <div class="col-sm-6">
                                        <input value="<?= $phone; ?>" required name="phone" class="form-control" placeholder="Phone" id="telepon"></input>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="_name_txt" class="col-sm-3 control-label">Email <span class="required">*</span></label>
                                    <div class="col-sm-6">    
                                        <input value="<?= $email; ?>" required name="email" class="form-control" placeholder="Email" id="email"></input>
                                    </div>
                                </div>
                            <center style="padding-top: 8px; margin-bottom: 30px;">
                                <div class="row">
                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Simpan</button>
                                    <a class="btn btn-danger btn-sm" href="<?= base_url('csatusehat/organization?tabActive=suborg'); ?>">BACK</a>
                                </div>
                            </center>
                        </form>
                    </div>
                </div>
        <?php }?>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->