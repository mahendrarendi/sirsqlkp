<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>REST Server Tests</title>

    <style>
    ::selection { background-color: #E13300; color: white; }
    ::-moz-selection { background-color: #E13300; color: white; }
    body {
        background-color: #FFF;
        margin: 40px;
        font: 16px/20px normal Helvetica, Arial, sans-serif;
        color: #4F5155;
        word-wrap: break-word;
    }
    a {
        color: #039;
        background-color: transparent;
        font-weight: normal;
    }
    h1 {
        color: #444;
        background-color: transparent;
        border-bottom: 1px solid #D0D0D0;
        font-size: 24px;
        font-weight: normal;
        margin: 0 0 14px 0;
        padding: 14px 15px 10px 15px;
    }
    code {
        font-family: Consolas, Monaco, Courier New, Courier, monospace;
        font-size: 16px;
        background-color: #f9f9f9;
        border: 1px solid #D0D0D0;
        color: #002166;
        display: block;
        margin: 14px 0 14px 0;
        padding: 12px 10px 12px 10px;
    }
    #body {
        margin: 0 15px 0 15px;
    }
    p.footer {
        text-align: right;
        font-size: 16px;
        border-top: 1px solid #D0D0D0;
        line-height: 32px;
        padding: 0 10px 0 10px;
        margin: 20px 0 0 0;
    }
    #container {
        margin: 10px;
        border: 1px solid #D0D0D0;
        box-shadow: 0 0 8px #D0D0D0;
    }
    </style>
</head>
<body>

<div id="container">
    <h1>WEB Service Laboratorium</h1>

    <div id="body">

<!--        <h2><a href="<?php echo site_url(); ?>">Home</a></h2>

        <p>
            See the article
            <a href="http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/" target="_blank">
                http://net.tutsplus.com/tutorials/php/working-with-restful-services-in-codeigniter-2/
            </a>
        </p>

        <p>
            The master project repository is
            <a href="https://github.com/chriskacerguis/codeigniter-restserver" target="_blank">
                https://github.com/chriskacerguis/codeigniter-restserver
            </a>
        </p>-->

        <p>Klik pada tautan untuk memeriksa apakah server REST berfungsi.</p>

        <ol>
            <li>ICD Lab - <?php echo site_url('api/webservice/icdlab'); ?> - tanpa parameter, contoh: <a href="<?php echo site_url('api/webservice/icdlab'); ?>" target="_blank">ICD Lab</a></li>
            <li><span style="background-color:#ff9800;color:#fff;">Pemeriksaan Lab Rawat Jalan </span>- <?php echo site_url('api/webservice/pemeriksaanlab/'); ?> 
                <br/>- Method GET; parameter: idpemeriksaan* (/id/idpemeriksaan), contoh: <a href="<?php echo site_url('api/webservice/pemeriksaanlab/id/12'); ?>" target="_blank">Pemeriksaan</a>
                <br/>- Method PUT; parameter: idhasilpemeriksaan* (/id/idhasilpemeriksaan), nilai (/n/nilai), nilaitext (/txt/nilaitext), remark (/rmk/remark), contoh: <a href="<?php echo site_url('api/webservice/pemeriksaanlab/id/-1/n/10'); ?>" target="_blank">Pemeriksaan</a>
                <br/>Dokumen contoh dapat diunduh di <a href="<?php base_url()?>/sirstql/assets/dokumen/sampel_pemeriksaanlab.txt" target="_blank">sini</a>
            </li>
            <li><span style="background-color:#4caf50;color:#fff;">Pemeriksaan Lab Rawat Inap </span>- <?php echo site_url('api/webservice/pemeriksaanlabranap/'); ?> 
                <br/>- Method GET; parameter: idpemeriksaan* (/id/idpemeriksaan), contoh: <a href="<?php echo site_url('api/webservice/pemeriksaanlabranap/id/20734'); ?>" target="_blank">Pemeriksaan</a>
                <br/>- Method PUT; parameter: idhasilpemeriksaan* (/id/idhasilpemeriksaan), nilai (/n/nilai), nilaitext (/txt/nilaitext), remark (/rmk/remark), contoh: <a href="<?php echo site_url('api/webservice/pemeriksaanlabranap/id/-1/n/10'); ?>" target="_blank">Pemeriksaan</a>
                <br/>Dokumen contoh dapat diunduh di <a href="<?php base_url()?>/sirstql/assets/dokumen/sampel_pemeriksaanlab.txt" target="_blank">sini</a>
            </li>
            <li>Master Jadwal - <?php echo site_url('api/webservice/masterjadwal'); ?> - tanpa parameter, contoh: <a href="<?php echo site_url('api/webservice/masterjadwal'); ?>" target="_blank">Master Jadwal</a></li>
            <li>Jadwal 1 Pekan- <?php echo site_url('api/webservice/jadwalsepekan'); ?> - tanpa parameter, contoh: <a href="<?php echo site_url('api/webservice/jadwalsepekan'); ?>" target="_blank">Jadwal 1 Pekan</a></li>
            <li>Antrian- <?php echo site_url('api/webservice/antrian'); ?> - parameter: tanggal (/tgl/yyyymmdd), contoh: <a href="<?php echo site_url('api/webservice/antrian/tgl/20191231'); ?>" target="_blank">Antrian</a></li>
            <li>Riwayat  Diagnosa <?php echo site_url('api/webservice/riwayatdiagnosa'); ?> - parameter: idpendaftaran* , contoh: <a href="<?php echo site_url('api/webservice/riwayatdiagnosa/id/10094731'); ?>" target="_blank">Riwayat Diagnosa</a></li>
        </ol>
        <ul>
            <li>*: wajib, jika tidak ada berarti opsional</li>
            <li>Format JSON - default</li>
            <li>Format CSV: /format/csv, contoh: <a href="<?php echo site_url('api/webservice/icdlab/format/csv'); ?>" target="_blank">CSV ICD Lab</a></li>
            <li>Format XML: /format/xml, contoh: <a href="<?php echo site_url('api/webservice/icdlab/format/xml'); ?>" target="_blank">XML ICD Lab</a></li>
            <li>Format HTML: /format/html, contoh: <a href="<?php echo site_url('api/webservice/icdlab/format/html'); ?>" target="_blank">HTML ICD Lab</a></li>
            <li>Format Serialized: /format/serialized, contoh: <a href="<?php echo site_url('api/webservice/icdlab/format/json'); ?>" target="_blank">Serialized ICD Lab</a></li>
        </ul>

    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>'.CI_VERSION.'</strong>' : '' ?></p>
</div>

<script src="https://code.jquery.com/jquery-1.12.0.js"></script>

<script>
    // Create an 'App' namespace
    var App = App || {};
    // Basic rest module using an IIFE as a way of enclosing private variables
    App.rest = (function restModule(window) {
        // Fields
        var _alert = window.alert;
        var _JSON = window.JSON;
        // Cache the jQuery selector
        var _$ajax = null;
        // Cache the jQuery object
        var $ = null;
        // Methods (private)
        /**
         * Called on Ajax done
         *
         * @return {undefined}
         */
        function _ajaxDone(data) {
            // The 'data' parameter is an array of objects that can be iterated over
            _alert(_JSON.stringify(data, null, 2));
        }
        /**
         * Called on Ajax fail
         *
         * @return {undefined}
         */
        function _ajaxFail() {
            _alert('Oh no! A problem with the Ajax request!');
        }
        /**
         * On Ajax request
         *
         * @param {jQuery} $element Current element selected
         * @return {undefined}
         */
        function _ajaxEvent($element) {
            $.ajax({
                    // URL from the link that was 'clicked' on
                    url: $element.attr('href')
                })
                .done(_ajaxDone)
                .fail(_ajaxFail);
        }
        /**
         * Bind events
         *
         * @return {undefined}
         */
        function _bindEvents() {
            // Namespace the 'click' event
            _$ajax.on('click.app.rest.module', function (event) {
                event.preventDefault();
                // Pass this to the Ajax event function
                _ajaxEvent($(this));
            });
        }
        /**
         * Cache the DOM node(s)
         *
         * @return {undefined}
         */
        function _cacheDom() {
            _$ajax = $('#ajax');
        }
        // Public API
        return {
            /**
             * Initialise the following module
             *
             * @param {object} jQuery Reference to jQuery
             * @return {undefined}
             */
            init: function init(jQuery) {
                $ = jQuery;
                // Cache the DOM and bind event(s)
                _cacheDom();
                _bindEvents();
            }
        };
    }(window));
    // DOM ready event
    $(function domReady($) {
        // Initialise the App module
        App.rest.init($);
    });
</script>

</body>
</html>