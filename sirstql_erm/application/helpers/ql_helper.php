<?php
defined('BASEPATH') OR exit('No direct script access allowed');


//beranda
defined('V_BERANDA_TINDAKANRAWATJALAN') OR define('V_BERANDA_TINDAKANRAWATJALAN', 147);

//administrasi
defined('V_USER') OR define('V_USER', 1);
defined('V_USERAKSES')  OR define('V_USERAKSES', 2);
defined('V_PERAN')  OR define('V_PERAN', 3);
defined('V_HALAMAN')  OR define('V_HALAMAN', 4);
defined('V_HAKAKSESHALAMAN')  OR define('V_HAKAKSESHALAMAN', 5);
defined('V_HUBUNGAN')  OR define('V_HUBUNGAN', 15);
defined('V_KONFIGURASI') OR define('V_KONFIGURASI', 105);

//DEMOSISTEM
defined('V_DEMOSISTEM') OR define('V_DEMOSISTEM', 101);

//admission
defined('V_PENDAFTARANPOLIKLINIK') OR define('V_PENDAFTARANPOLIKLINIK', 13);
defined('V_PEGAWAI') OR define('V_PEGAWAI', 14);
defined('V_MASTERJADWAL') OR define('V_MASTERJADWAL', 22);
defined('V_INPUTJADWAL') OR define('V_INPUTJADWAL', 23);
defined('V_DATAINDUK') OR define('V_DATAINDUK', 38);
defined('V_VERIFPASIENBPJS') OR define('V_VERIFPASIENBPJS', 49);
defined('V_ARUSREKAMMEDIS') OR define('V_ARUSREKAMMEDIS', 50);
defined('V_MENUAKSIDATAINDUK') OR define('V_MENUAKSIDATAINDUK', 87);
defined('V_PERSONMEMBER') OR define('V_PERSONMEMBER', 118);
defined('V_BLACKLIST') OR define('V_BLACKLIST', 143);
defined('MENU_UBAHDATAPENDAFTARAN_SELESAI') OR define('MENU_UBAHDATAPENDAFTARAN_SELESAI', 146);

//menu header
defined('V_GANTISHIF') OR define('V_GANTISHIF', 95);

//antrian
defined('V_ADMINISTRASIPEMANGGILANANTRIAN') OR define('V_ADMINISTRASIPEMANGGILANANTRIAN', 37);
defined('V_FARMASIGENERATEANTRIAN') OR define('V_FARMASIGENERATEANTRIAN', 62);
defined('V_PENGATURAN_VIDEO') OR define('V_PENGATURAN_VIDEO', 167);

//master data
defined('V_PROPINSI') OR define('V_PROPINSI', 7);
defined('V_KABUPATEN')  OR define('V_KABUPATEN', 9);
defined('V_KECAMATAN')  OR define('V_KECAMATAN', 10);
defined('V_KELURAHAN')  OR define('V_KELURAHAN', 11);
defined('V_PENDIDIKAN')  OR define('V_PENDIDIKAN', 12);
defined('V_ICD')  OR define('V_ICD', 16);
defined('V_PAKETPEMERIKSAAN')  OR define('V_PAKETPEMERIKSAAN', 17);
defined('V_PAKETPEMERIKSAANDETAIL')  OR define('V_PAKETPEMERIKSAANDETAIL', 18);
defined('V_GRUPPEGAWAI')  OR define('V_GRUPPEGAWAI', 19);
defined('V_UNIT')  OR define('V_UNIT', 20);
defined('V_INSTALASI')  OR define('V_INSTALASI', 21);
defined('V_BARANG')  OR define('V_BARANG', 25);
defined('V_JENISICD')  OR define('V_JENISICD', 26);
defined('V_JENISTARIF')  OR define('V_JENISTARIF', 30);
defined('V_MASTERTARIF')  OR define('V_MASTERTARIF', 31);
defined('V_KELAS')  OR define('V_KELAS', 32);
defined('V_MASTERTARIFPAKETPEMERIKSAAN')  OR define('V_MASTERTARIFPAKETPEMERIKSAAN', 33);
defined('V_STASIUN')  OR define('V_STASIUN', 35);
defined('V_LOKET')  OR define('V_LOKET', 36);
defined('V_GOLONGAN')  OR define('V_GOLONGAN', 39);
defined('V_BANGSAL')  OR define('V_BANGSAL', 40);
defined('V_BED')  OR define('V_BED', 41);
defined('V_APLICARE')  OR define('V_APLICARE', 52);
defined('V_JENISPENYAKIT')  OR define('V_JENISPENYAKIT', 60);
defined('v_PERPUSTAKAAN') OR define('v_PERPUSTAKAAN', 77);
defined('V_KLINIKPERUJUK')  OR define('V_KLINIKPERUJUK', 82);
defined('V_PANELSKDP')  OR define('V_PANELSKDP', 83);
defined('V_BARANGPABRIK') OR define('V_BARANGPABRIK', 84);
defined('V_MANAGEMENASET') OR define('V_MANAGEMENASET', 88);
defined('V_MODULPENGELUARAN') OR define('V_MODULPENGELUARAN', 89);
defined('V_PEDAGANGBESARFARMASI') OR define('V_PEDAGANGBESARFARMASI', 94);
defined('V_BELANJAFARMASI') or define('V_BELANJAFARMASI', 104);
defined('V_TAMBAHITEMBELANJAFARMASI') or define('V_TAMBAHITEMBELANJAFARMASI', 105);
defined('V_MASTERDISKON') or define('V_MASTERDISKON', 115);
defined('V_MASTERMEMBER') or define('V_MASTERMEMBER', 116);
defined('V_MASTERTARIFOPERASI') or define('V_MASTERTARIFOPERASI', 132);
defined('V_MENU_HAPUS_DATABARANG') or define('V_MENU_HAPUS_DATABARANG', 151);
defined('V_SDKI') or define('V_SDKI',177);


//FARMASI
defined('V_SEDIAAN')  OR define('V_SEDIAAN', 27);
defined('V_SATUAN')  OR define('V_SATUAN', 28);
defined('V_ATURANPAKAI')  OR define('V_ATURANPAKAI', 55);
defined('V_BARANGDISTRIBUSI') OR define('V_BARANGDISTRIBUSI', 66);
defined('V_DISTRIBUTOR') OR define('V_DISTRIBUTOR', 67);
defined('V_BELANJABARANG') OR define('V_BELANJABARANG', 70);
//defined('V_STOKBARANGUNIT') OR define('V_STOKBARANGUNIT',72);
defined('V_STOKBARANG') OR define('V_STOKBARANG',73);
defined('V_TRANSFORMASIBARANG') OR define('V_TRANSFORMASIBARANG',74);

//pelayanan
defined('V_PEMERIKSAANKLINIK') OR define('V_PEMERIKSAANKLINIK', 24);
defined('V_ADMINISTRASIANTRIAN') OR define('V_ADMINISTRASIANTRIAN', 29);
defined('V_KASIR') OR define('V_KASIR', 34);
defined('V_PEMERIKSAANRANAP') OR define('V_PEMERIKSAANRANAP', 47);
defined('V_PEMERIKSAANRANAPCHECKLIST') OR define('V_PEMERIKSAANRANAPCHECKLIST', 63);
defined('V_ANJUNGANPASIEN') OR define('V_ANJUNGANPASIEN', 46);
defined('V_SEWAKAMAR') OR define('V_SEWAKAMAR', 51);
defined('V_ADMINISTRASIRANAP') OR define('V_ADMINISTRASIRANAP', 58);
defined('V_LISBED') OR define('V_LISBED', 61);
defined('V_PEMERIKSAANDOKTER') OR define('V_PEMERIKSAANDOKTER',76);
defined('V_OBATBEBAS') OR define('V_OBATBEBAS',79);
defined('V_KASIROBATBEBAS') OR define('V_KASIROBATBEBAS',80);
defined('V_MENUKASIROBATBEBAS') OR define('V_MENUKASIROBATBEBAS',85);
defined('V_JADWALOPERASI') OR define('V_JADWALOPERASI', 81);
defined('V_MENUKASIR') OR define('V_MENUKASIR', 86);
defined('V_MENUPEMERIKSAANRADIOLOGI') OR define('V_MENUPEMERIKSAANRADIOLOGI',90);
defined('V_MENUPEMERIKSAANLABORAT') OR define('V_MENUPEMERIKSAANLABORAT',91);
defined('V_MENUVERIFIKATORRALAN') OR define('V_MENUVERIFIKATORRALAN',92);
defined('V_MENUPEMERIKSAANKLINIK') OR define('V_MENUPEMERIKSAANKLINIK',93);
defined('V_PEMERIKSAANOPERASI') OR define('V_PEMERIKSAANOPERASI', 99);
defined('V_MENUBATALPEMERIKSAANRALAN') OR define('V_MENUBATALPEMERIKSAANRALAN', 117);
defined('V_INPUTRESEPOBATRALAN') OR define('V_INPUTRESEPOBATRALAN', 119);
defined('V_INPUTHASILEXPERTISERADIOLOGI') OR define('V_INPUTHASILEXPERTISERADIOLOGI', 120);
defined('V_INPUTDATAOBYEKTIFRALAN') OR define('V_INPUTDATAOBYEKTIFRALAN', 123);
defined('V_INPUTDATASUBYEKTIFRALAN') OR define('V_INPUTDATASUBYEKTIFRALAN', 124);
defined('V_INPUTRENCANACATATANRALAN') OR define('V_INPUTRENCANACATATANRALAN', 125);
defined('V_INPUTALERGIRALAN') OR define('V_INPUTALERGIRALAN', 126);
defined('V_INPUTKETERANGANLABORATORIUM') OR define('V_INPUTKETERANGANLABORATORIUM', 128);
defined('V_MENUPEMANGGILANPASIENDIPOLI') OR define('V_MENUPEMANGGILANPASIENDIPOLI', 131);
defined('V_INPUTPROLANIS') OR define('V_INPUTPROLANIS', 133);
defined('V_PEMERIKSAAN_WAKTULAYANANLABORAT') OR define('V_PEMERIKSAAN_WAKTULAYANANLABORAT', 134);
defined('V_PEMERIKSAAN_WAKTULAYANANRADIOLOGI') OR define('V_PEMERIKSAAN_WAKTULAYANANRADIOLOGI', 135);
defined('V_PEMERIKSAAN_WAKTULAYANANFARMASI') OR define('V_PEMERIKSAAN_WAKTULAYANANFARMASI', 136);
defined('V_PEMERIKSAAN_WAKTULAYANANPOLI') OR define('V_PEMERIKSAAN_WAKTULAYANANPOLI', 137);
defined('V_PEMERIKSAAN_WAKTULAYANANDOKTER') OR define('V_PEMERIKSAAN_WAKTULAYANANDOKTER', 138);
defined('V_PEMERIKSAAN_WAKTULAYANANUGD') OR define('V_PEMERIKSAAN_WAKTULAYANANUGD', 139);
defined('V_MENUKASIRBATALSELESAI') OR define('V_MENUKASIRBATALSELESAI', 141);
defined('V_LABORATORIUMRANAP') OR define('V_LABORATORIUMRANAP', 142);
defined('V_REGISTERKAMARBEDAH') OR define('V_REGISTERKAMARBEDAH',145);
defined('V_DATAPASIENDIBLACKLIST') OR define('V_DATAPASIENDIBLACKLIST',150);
defined('V_INPUTHASILEKOKARDIOGRAFI') OR define('V_INPUTHASILEKOKARDIOGRAFI', 152);
defined('V_MENU_MANAJEMENFILE_HASILRADIOGRAFI') OR define('V_MENU_MANAJEMENFILE_HASILRADIOGRAFI', 159);
defined('V_MENUPEMERIKSAANEKOKARDIOGRAFI') OR define('V_MENUPEMERIKSAANEKOKARDIOGRAFI',160);
defined('V_MENUKASIRBATALSELESAINONBPJS') OR define('V_MENUKASIRBATALSELESAINONBPJS', 164);
defined('V_BARANGOPERASI') OR define('V_BARANGOPERASI', 166);

//laporan
defined('V_LOG') OR define('V_LOG', 42);
defined('V_LPOBAT') OR define('V_LPOBAT', 43);
defined('V_LDPASIEN') OR define('V_LDPASIEN', 44);
defined('V_LPELAYANRS') OR define('V_LPELAYANRS', 45);
defined('V_DEMOGRAFIKUNJUNGANPASIEN') OR define('V_DEMOGRAFIKUNJUNGANPASIEN', 64);
defined('V_LAPORANKEUANGAN') OR define('V_LAPORANKEUANGAN', 65);
defined('V_LAPORANFARMASI') OR define('V_LAPORANFARMASI',69);
defined('V_LAPORAN_CAKUPANPERPOLI') OR define('V_LAPORAN_CAKUPANPERPOLI',78);
defined('V_LAPORANSTATUSPEMERIKSAANPASIENRALAN') OR define('V_LAPORANSTATUSPEMERIKSAANPASIENRALAN', 130);
defined('V_WAKTULAYANAN') OR define('V_WAKTULAYANAN', 140);
defined('V_WAKTUPENDAFTARAN') OR define('V_WAKTUPENDAFTARAN', 141);
defined('V_LAPORANCAKUPANPOLISPESIALIS') OR define('V_LAPORANCAKUPANPOLISPESIALIS',153);
defined('V_LAPORANCAKUPANPOLIPENUNJANG') OR define('V_LAPORANCAKUPANPOLIPENUNJANG',165);
defined('V_LAPORANCAKUPANPOLIKHUSUS') OR define('V_LAPORANCAKUPANPOLIKHUSUS', 154);
defined('V_LAPORANCAKUPANKAMAROPERASI') OR define('V_LAPORANCAKUPANKAMAROPERASI', 155);
defined('V_LAPORANCAKUPANRAWATINAP') OR define('V_LAPORANCAKUPANRAWATINAP', 158);


//SDMK
defined('V_SDMKDASHBOARD') OR define('V_SDMKDASHBOARD', 149);
defined('V_SDMKBERKAS') OR define('V_SDMKBERKAS', 178);

///// BPJS //////
defined('BPJS_KODEPPK') OR define('BPJS_KODEPPK', '0179R014');
defined('BPJS_PESERTA') OR define('BPJS_PESERTA', 1);
defined('BPJS_RUJUKAN') OR define('BPJS_RUJUKAN', 2);
defined('BPJS_SEP') OR define('BPJS_SEP', 3);
defined('BPJS_APLICARE') OR define('BPJS_APLICARE', 4);
defined('BPJS_MONITORING') OR define('BPJS_MONITORING', 5);
defined('KMK_SIRANAP') OR define('KMK_SIRANAP', 11);
defined('COVID19') OR define('COVID19', 12);

defined('JENISAPLIKASI_VCLAIM') OR define('JENISAPLIKASI_VCLAIM', 1);

defined('JENISAPLIKASI_APLICARE') OR define('JENISAPLIKASI_APLICARE', 2);
defined('JENISAPLIKASI_SIRANAP') OR define('JENISAPLIKASI_SIRANAP', 11);
defined('JENISAPLIKASI_WEBQL') OR define('JENISAPLIKASI_WEBQL', 13);
defined('JENISAPLIKASI_VCLAIM_V2') OR define('JENISAPLIKASI_VCLAIM_V2', 14);
defined('JENISAPLIKASI_BPJS_ANTROL') OR define('JENISAPLIKASI_BPJS_ANTROL', 15);
defined('JENISAPLIKASI_ICARE') OR define('JENISAPLIKASI_ICARE', 16);

defined('JENISCONSID_DEVELOPMENT') OR define('JENISCONSID_DEVELOPMENT', 1);
defined('JENISCONSID_PRODUCTION') OR define('JENISCONSID_PRODUCTION', 2);

///// NOTIFIKASI ////
defined('NOTIF_ORDER_UGD') OR define('NOTIF_ORDER_UGD', 53);
defined('NOTIF_ORDER_RM') OR define('NOTIF_ORDER_RM', 54);
defined('NOTIF_FARMASI') OR define ('NOTIF_FARMASI', 75);
defined('V_PEMANGGILANANSEMUATRIAN') OR define('V_PEMANGGILANANSEMUATRIAN', 59);
defined('V_BARANGNOTIFIKASI') or define('V_BARANGNOTIFIKASI', 68);
defined('V_PEMANGGILANHASIL') or define('V_PEMANGGILANHASIL', 112);


//Lain-lain
defined('AKSES_SALINRIWAYATPERIKSARALAN') or define('AKSES_SALINRIWAYATPERIKSARALAN', 127);


//keuangan
defined('V_DEPARTEMEN') or define('V_DEPARTEMEN', 96);
defined('V_AKUN') or define('V_AKUN', 97);
defined('V_JURNAL') or define('V_JURNAL', 98);
defined('V_HISTORYAKUN') or define('V_HISTORYAKUN', 100);
defined('V_LAPORANKAS') or define('V_LAPORANKAS', 107);
defined('V_LAPORANLABARUGI') or define('V_LAPORANLABARUGI', 108);
defined('V_REKAPTRANSAKSI') or define('V_REKAPTRANSAKSI', 109);
defined('V_PENDAPATANKAS') or define('V_PENDAPATANKAS', 111);
defined('V_PENDAPATANDET') or define('V_PENDAPATANDET', 121);
defined('V_JURNALHPP') or define('V_JURNALHPP', 122);
defined('V_PENGELUARAN') or define('V_PENGELUARAN', 144);
defined('V_CASHEQUIVALEN') or define('V_CASHEQUIVALEN', 148);


//TAGIHAN
defined('V_HUTANGFARMASI') or define('V_HUTANGFARMASI', 103);
defined('V_HUTANGUSAHA') or define('V_HUTANGUSAHA', 110);
defined('V_FAKTURLUNASUSAHA') or define('V_FAKTURLUNASUSAHA', 113);
defined('V_FAKTURLUNASFARMASI') or define('V_FAKTURLUNASFARMASI', 114);


//Modul BPJS
defined('V_BPJS') or define('V_BPJS', 161);
defined('V_BPJSPESERTA') or define('V_BPJSPESERTA', 162);
defined('V_BPJSSEP') or define('V_BPJSSEP', 163);

//AKreditasi
defined('V_AKREDITASI') or define('V_AKREDITASI', 168);
defined('v_TAMBAHDATA') or define('v_TAMBAHDATA', 169);
defined('V_LISTTABLEAKREDITASI') or define('v_LISTTABLEAKREDITASI', 170);
defined('V_KATEGORIAKREDITASI') or define('V_KATEGORIAKREDITASI', 171);
defined('V_DOKUMENAKREDITASI') OR define('V_DOKUMENAKREDITASI',172);
defined('V_PENGATURANPIC') OR define('V_PENGATURANPIC',174);
defined('V_DOKUMENUNIT') OR define('V_DOKUMENUNIT',176);

//INDIKATOR MUTU
defined('V_INDIKATORMUTU') or define('V_INDIKATORMUTU', 179);

//SATUSEHAT
defined('V_LOKASI_RUANG') or define('V_LOKASI_RUANG', 151); //
defined('V_ORGANISASI') or define('V_ORGANISASI', 151); //
defined('V_PASIEN') or define('V_PASIEN', 151); //
defined('V_NAKES') or define('V_NAKES', 151); //cad

//PENGKAJIAN MEDIS
defined('V_PENGKAJIANMEDIS') or define('V_PENGKAJIANMEDIS',180);

//// ANTRIAN PASIEN ////
// defined('ANTRIAN_FARMASI') OR define('ANTRIAN_FARMASI', 56);
// defined('ANTRIAN_KASIR') OR define('ANTRIAN_KASIR', 57);
/**
 * Generate Random
 * @param type $panjang Panjang Hasil Generate
 * @return string
 */
function generaterandom($panjang)
{
    $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
    $hasil  = '';
    for($i=0; $i< $panjang; $i++)
    {
        $str = rand(0, strlen($char)-1);
        $hasil .= $char[$str];
    }

    return $hasil;
}
/**
 * Generate Random Angka
 * @param type $panjang Panjang Hasil Generate
 * @return string
 */
function generaterandomangka($panjang)
{
    $char = '0123456789';
    $hasil  = '';
    for($i=0; $i< $panjang; $i++)
    {
        $str = rand(0, strlen($char)-1);
        $hasil .= $char[$str];
    }

    return $hasil;
}
/**
 * Dropdown Set Selected
 * @param type $editdata
 * @param type $id
 * @param type $selectid
 * @return type
 */
function dropdown_selected($editdata,$id,$selectid)
{
    $result = ((empty(!$editdata) && $id == $editdata[$selectid]) ? 'selected' : '' );
    return $result;
}

/**
 * Set Value form Input
 * @param type $data
 * @param type $value
 * @return type
 */
function forminput_setvalue($data,$value)
{
    $result = ((empty($data)) ? '' : $data[$value] );
    return $result;
}

function format_date($date,$format="Y/m/d H:i:s") 
{ 
    $date   = date_create( $date ); 
    return  date_format( $date,$format ); 
}

/**
 * dev ganteng
 * qrcode
 */
function convert_to_qrcode($string_url,$ukuran="80x80"){
    #generate QR Code
    $chs = urlencode( $ukuran );
    $cht = 'qr';
    $chl = urlencode( $string_url );
    $link_qrcode = "https://chart.googleapis.com/chart?chs=".$chs."&cht=".$cht."&chl=".$chl."&choe=UTF-8";
    $html_qrcode = '<img src="'.$link_qrcode.'" title="Tanda Tangan Dokter" />';
    return $html_qrcode;
}

/** mr.ganteng */
function ql_umursekarang($tanggal_lahir){
    $birthDate = new \DateTime($tanggal_lahir);
    $today = new \DateTime("today");
    if ($birthDate > $today) {
        return "0 tahun 0 bulan 0 hari";
    }
    $y = $today->diff($birthDate)->y;
    // dd($y);
    $m = $today->diff($birthDate)->m;
    $d = $today->diff($birthDate)->d;
    return $y . " tahun " . $m . " bulan " . $d . " hari";
}

function countIntervalDate($date1,$date2){

    $diff = abs(strtotime($date2) - strtotime($date1));

    $years = floor($diff / (365*60*60*24));
    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

    $return = $months.' Bulan'.', '.$days.' Hari';

    return $return;
}

function get_session_userid(){
    CI()->load->library('session');

    $iduser = json_decode( CI()->encryptbap->decrypt_urlsafe("SESSUSER",  CI()->session->userdata('iduser')));
    return $iduser;
}

function get_session_peran(){
    CI()->load->library('session');
    $peran = CI()->session->userdata('peranperan');
    $ex_peran = explode(',',$peran);
    return $ex_peran;
}

function get_session_username(){
    CI()->load->library('session');
    $username = CI()->session->userdata('username');
    return $username;
}

function get_username_login($user_id){
    CI()->load->model('msqlbasic');
    $tb_login_user = 'login_user';
    
    $query = CI()->msqlbasic->row_db($tb_login_user,['iduser'=>$user_id]);
    return $query['namauser'];
}

function get_judulindikator_pengaturan($idindikator=""){
    CI()->load->model('msqlbasic');
    $tb_mutu_indikator = 'mutu_indikator';
    $sql = "SELECT id_indikator,judul_indikator FROM ".$tb_mutu_indikator." WHERE id_indikator IN (".$idindikator.")";
    $query = CI()->msqlbasic->custom_query($sql);
    
    $ul = '<ol class="list-judul-pengaturan">';
    foreach( $query as $row ){
        $ul .= '<li>'.namefile_maxlength_string($row['judul_indikator']).'</li>';
    }
    $ul .= '</ol>'; 

    return $ul;

}

function get_judulikp_pengaturan($idindikator=""){
    CI()->load->model('msqlbasic');
    $tb_mutu_daftar_ikp = 'mutu_daftar_ikp';
    $sql = "SELECT id_daftarikp,judulikp FROM ".$tb_mutu_daftar_ikp." WHERE id_daftarikp IN (".$idindikator.")";
    $query = CI()->msqlbasic->custom_query($sql);

    $ul = '<ol class="list-judul-pengaturan">';
    foreach( $query as $row ){
        $ul .= '<li>'.$row['judulikp'].'</li>';
    }
    $ul .= '</ol>'; 

    return $ul;

}

function get_judulppi_pengaturan($idindikator=""){
    CI()->load->model('msqlbasic');
    $tb_mutu_daftarppi = 'mutu_daftarppi';
    $sql = "SELECT id_daftarppi,judul_ppi FROM ".$tb_mutu_daftarppi." WHERE id_daftarppi IN (".$idindikator.")";
    $query = CI()->msqlbasic->custom_query($sql);
    
    $ul = '<ol class="list-judul-pengaturan">';
    foreach( $query as $row ){
        $ul .= '<li>'.$row['judul_ppi'].'</li>';
    }
    $ul .= '</ol>'; 

    return $ul;

}

function all_pic(){
    $sql = CI()->db->query("SELECT * FROM akre_pengaturan_pic");
    $query = $sql->row_array();
        
    $author_ids = explode(',', $query['author']);

    $userakses = [];
    foreach( $author_ids as $id ){
        $getrow = CI()->db->query("SELECT * FROM login_user WHERE iduser='".$id."' LIMIT 1")->row_array();
        $userakses[] = $getrow;
    }

    return $userakses;
}

function get_hidden_kategori_per_pic(){
    CI()->load->model('msqlbasic');
    $pengaturan_umum_hidden    = CI()->msqlbasic->row_db('akre_pengaturan_umum',['key_pengaturan'=>'hidden_data_pic']);
    return $pengaturan_umum_hidden['value_pengaturan'];
}

function cek_peran_superuser($peran=array()){
    CI()->load->model('msqlbasic');
    $tb_peran    = 'login_peran';
    
    $sql         = "SELECT namaperan FROM ".$tb_peran." WHERE idperan IN (".implode(',',$peran).")" ;
    $query  = CI()->msqlbasic->custom_query($sql);

    $nama_peran = [];
    foreach( $query as $row_peran ){
        $nama_peran[] = strtolower( $row_peran['namaperan'] );
    }

    return $nama_peran;
}

function is_superuser(){
    $cek_peran_superuser = cek_peran_superuser( get_session_peran() ); //array

    $is_superuser = (in_array('superuser',$cek_peran_superuser)) ? true : false;

    return $is_superuser;
}

function remove_empty_value_array($array){
    return array_values(array_filter($array, function($value) { return !is_null($value) && $value !== ''; }) );
}

function indonesia_date(){
    date_default_timezone_set("Asia/Jakarta");
    return date('Y-m-d H:i:s');
}

// parent,subparent,childparent

function role_parent(){
    return 'parent';
}

function role_subparent(){
    return 'subparent';
}

function role_childparent(){
    return 'childparent';
}

function namefile_maxlength_string($str){
    if (strlen($str) > 10) return substr($str, 0, 100) . '...';
}

/**
 * get_allusers
 *
 * @return array all user
 */
function get_allusers(){
    CI()->load->model('msqlbasic');

    $sql    = "SELECT iduser,namauser FROM login_user";
    $query  = CI()->msqlbasic->custom_query($sql);

    return $query;
}


/**
 * Style Form Grup 1 Kolom
 * @param type $label Nama Label
 * @param type $item  Form Input
 */
function styleformgrup1kolom($label,$item)
{
    echo '<div class="form-group">
            <label for="_name_txt" class="col-sm-3 control-label"> '.$label.' <span class="asterisk">*</span></label>
            <div class="col-sm-6">'.$item.'</div>
          </div>';
}
/**
 * Style Form Grup 2 Kolom
 * @param type $label Nama Label
 * @param type $item  Form INput
 * @param type $label2 Nama Label 2
 * @param type $item2 From Input 2
 */
function styleformgrup2kolom($label='',$item='',$label2='',$item2='')
{
    echo '<div class="form-group">
            <label for="_name_txt" class="col-sm-2 control-label">'.$label.'</label>
            <div class="col-sm-3">'.$item.'</div>
            <label for="_name_txt" class="col-sm-3 control-label">'.$label2.'</label>
            <div class="col-sm-3">'.$item2.'</div>
          </div>';
}
/**
 * Validasi Input
 * @return boolean
 */
function validation_input()
{
    if(!CI()->form_validation->run())
    {   
        CI()->load->library('user_agent');
        pesan_danger('Please fill all required field.!');
        redirect(CI()->agent->referrer());
        return false;
    }
    else
    {
        return true;
    }
}
function aksesditolak()
{
    return redirect('../../sirstql_login_erm');
}
/**
 * Pesan Success Danger
 * @param type $arrMsg Pesan Hasil Eksekusi
 * @param type $pesansuccess Pesan Success
 * @param type $pesandanger  Pesan Danger
 * @param type $tipe Tipe Pesan
 */
function pesan_success_danger($arrMsg, $pesansuccess, $pesandanger, $tipe = "")
{
    if ($tipe == "js")
    {
        echo json_encode(($arrMsg) ? ['status' => 'success', 'message' => $pesansuccess] : ['status' => 'danger', 'message' => $pesandanger]);
    }
    else
    {
        CI()->session->set_flashdata('message', ($arrMsg) ? ['status' => 'success', 'message' => $pesansuccess] : ['status' => 'danger', 'message' => $pesandanger]);
    }
}
/**
 * Pesan Danger
 * @param type $pesandanger Pesan Danger
 * @param type $tipe Tipe Pesan
 */
function pesan_danger($pesandanger, $tipe = "")
{
    if ($tipe == "js")
    {
        echo json_encode(['status' => 'danger', 'message' => $pesandanger]);
    }
    else
    {
        CI()->session->set_flashdata('message', ['status' => 'danger', 'message' => $pesandanger]);
    }
}

/**
 * Pesan Success Danger Web Service
 * @param type $kode   Kode
 * @param type $pesan  Pesan
 * @param type $tipe   Tipe Pesan
 */
function pesan_success_danger_ws($kode,$pesan,$tipe = "")
{
    $status = '';
    if($kode == '200')
    {
        $status = 'success';
    }
    else if($kode == '203')
    {
        $status = 'danger';
    }
    else if($kode == '401')
    {
        $status = 'warning';
    }
        
        
    if ($tipe == "js")
    {
        echo json_encode(['status' => $status, 'message' => $pesan]);
    }
    else
    {
        CI()->session->set_flashdata('message', ['status' => $status, 'message' => $pesan]);
    }
}

function CI()
{
    $ci =& get_instance();
    return $ci;
}

/**
 * Format Waktu Indonesia
 * @param type $value
 * @return type
 */
function waktuindo($value)
{
    $result = date('H:i:s', strtotime($value));
    return $result;
}
/**
 * Hilangkan Tanda Kutip Ganda
 * @param type $value
 * @return type
 */
function hilangkan_kutipganda($value)
{
    $result = str_replace('"', '', $value);
    return $result;
}
/**
 * Hari Indonesia
 * @param type $hari
 * @return type
 */
function hariindo($hari='')
{
  $days = ['1'=>'Ahad', '2'=>'Senin','3'=>'Selasa','4'=>'Rabu','5'=>'Kamis','6'=>'Jumat','7'=>'Sabtu'];

  return ((empty($hari))? $days : $days[$hari]);
}

/**
 * Hari Nasional
 * @param type $hari
 * @return type
 */
function harinasional($hari='')
{
  $days = ['1'=>'Senin', '2'=>'Selasa','3'=>'Rabu','4'=>'Kamis','5'=>'Jumat','6'=>'Sabtu','7'=>'Minggu','8'=>'Hari Libur Nasional'];

  return ((empty($hari))? $days : $days[$hari]);
}

/**
 * Format Hari sistem ke Indo
 * @param type $value
 * @return type
 */
function harisistem($value)
{
    $hari =  intval($value) + 1;
    return hariindo((($hari==8)?1:$hari));
}
/**
 * Cek Apakah Tanggal Lebih Kecil sekarang
 * @param type $date
 * @return Boolean
 */
function tanggalkurangdarisekarang($date)
{
    $sekarang = strtotime(date('Y-m-d').'00:00:00');
    $newDate = strtotime($date);
    return ($newDate < $sekarang) ? true : false ;
}
/**
 * Cek Tanggal Lebih Dari Jumlah Hari Yang Ditentukan
 * @param type $awal Tanggal Awal
 * @param type $akhir Tanggal Akhir
 * @param type $hari Jumlah Hari
 * @return Boolean
 */
function tanggallebihdari_hari($awal,$akhir,$hari)
{
    $tglawal  = new DateTime($awal);
    $tglakhir = new DateTime($akhir);
    $selisih = $tglawal->diff($tglakhir)->days + 1;
    return ($selisih > $hari) ? true : false ;
}

/**
 * Ambil Tanggal Sekarang
 * @param type $format Format Tanggal
 * @return type
 */
function ql_tanggalsekarang($format='Y-m-d')
{
    return date($format);
}
/**
 * Tanggal Kemarin
 * @param type $date Tanggal
 * @param type $format Format Tanggal
 * @return type
 */
function ql_tanggalkemarin($date = '', $format='Y-m-d')
{
    return ql_tanggaltujuan('-1 day', $date, $format);
}
/**
 * Bulan Kemarin
 * @param type $date Tanggal
 * @param type $format Format
 * @return type
 */
function ql_bulankemarin($date = '', $format='Y-m-d')
{
    return ql_tanggaltujuan('-1 month', $date, $format);
}
/**
 * Cari Tanggal Berdasarkan jarak hari
 * @param type $jarak Jarak Hari yang ditentukan (sebelum[misal:-10 => -10 day] atau sesudah[misal:+10 => 10 day])
 * @param type $date Tanggal Pembatas
 * @param type $format Format Tanggal
 * @return type
 */
function ql_tanggaltujuan($jarak, $date='', $format='Y-m-d')
{
    return date($format, strtotime(((empty($date))? ql_tanggalsekarang('Y-m-d') : $date) .' '.$jarak));
}
/**
 * Date ke "Hari, Tanggal Bulan Tahun"
 * @param type Date
 * [Danang]
 */
function ql_hari_tanggalbulantahun($tanggal) {
    $namaHari = array(
        'Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'
    );
    $namaBulan = array(
        'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
        'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
    );
    $timestamp = strtotime($tanggal);
    if ($timestamp === false) {
        return "Format tanggal tidak valid";
    };
    $hari = date('w', $timestamp);
    $tanggal = date('j', $timestamp);
    $bulan = date('n', $timestamp);
    $tahun = date('Y', $timestamp);
    $formatTanggal = $namaHari[$hari] . ', ' . $tanggal . ' ' . $namaBulan[$bulan - 1] . ' ' . $tahun;
    return $formatTanggal;
}

//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'      =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                     =>  14 Days
// '%h Hours %i Minute %s Seconds'                              =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                       =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                     =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
   
    $interval = date_diff($datetime1, $datetime2);
   
    return $interval->format($differenceFormat);
}

function ql_bulans(){
    $bulan = [];
    for ($x = 1; $x <= 12; $x++) {
        $bulan[] = $x; 
    }

    return $bulan;
}

/**
 * Nama Bulan
 * @param type $bulan Nama Bulan
 * @return string
 */
function ql_namabulan($bulan='')
{
  $namabulan = ['1'=>'Januari', '2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'];
  $result = '';
  if(empty($bulan)){
      $result = $namabulan;
  }else{
      $result = $namabulan[$bulan];
  }
  return $result;
}
/**
 * Bulan dalam Angka
 * @param type $bulan
 * @return string
 */
function ql_angkabulan($bulan)
{
  $namabulan = ['01'=>'1','02'=>'2','03'=>'3','04'=>'4','05'=>'5','06'=>'6','07'=>'7','08'=>'8','09'=>'9','10'=>'10','11'=>'11','12'=>'12',];

  return $namabulan[$bulan];
}
/**
 * Mendapatkan Warna dari angka
 * @param type $x banyaknya nilai untuk diulang
 * @return string
 */
function qlwarna($x)
{
    while ( $x > 9) {  $x = intval($x)-9;}
    $color = ['1'=>'green', '2'=>'red','3'=>'maroon','4'=>'blue','5'=>'purple','6'=>'aqua','7'=>'orange','8'=>'olive','9'=>'yellow'] ;
    return  $color[$x];
}
/**
 * Buat Status Warna
 * @param type $status
 * @return string
 */
function qlstatuswarna($status)
{
    $color = ['selesai'=>'background-color:#e0f0d8;','batal'=>'background-color:#f0D8D8;','tunda'=>'background-color:#F7E59E;'] ;   
    return  $color[$status];
}

/**
 * Buat Status Periksa
 * @param type $sts Status
 * @return string
 */
function ql_statusperiksa($sts)
{
    //0:pesan,1: periksa, 2: selesai, 3: batal, 4:kadaluarsa
    $idstatus =  intval($sts);
    $status = ['Pesan','Periksa','Selesai','Batal','Kadaluarsa'];
    return $status[$idstatus];
}
function tanggalwaktu($date)
{
  $hasil = date('d-m-Y, H:i:s', strtotime($date));
  return $hasil;
}

function ql_helpertanggal($date)
{
    return date('Y-m-d', strtotime($date));
}
function ql_masterjadwal_editdelete($string)
{
    $x = str_replace("#", "' class='btn btn-danger btn-xs' id='masterjadwalDelete' data-toggle='tooltip' data-original-title='hapus jadwal'> <i class='fa fa-trash'></i> </a><br>",str_replace("$", "' class='btn btn-warning btn-xs' id='masterjadwalEdit' data-toggle='tooltip' data-original-title='ubah jadwal'> <i class='fa fa-edit'></i> </a>", str_replace("&", "' class='btn btn-primary btn-xs' id='masterjadwalCopy' data-toggle='tooltip' data-original-title='salin jadwal'> <i class='fa fa-copy'></i> </a>", str_replace("@", " <a alt='", $string))));
    return $x;
}
function ql_removeaftercharacters($string,$character)
{
    $x = strtok($string, $character);
    return $x;
}
function ql_penghubungstring($value,$tanda)
{
    if(!empty($value))
    {
      return $x=$tanda;
    }
    else
    {
      return $x='';
    }
}

function array_msort($array, $cols)
{
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
    }
    $eval = 'array_multisort(';
    foreach ($cols as $col => $order) {
        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
    }
    $eval = substr($eval,0,-1).');';
    eval($eval);
    $ret = array();
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            $k = substr($k,1);
            if (!isset($ret[$k])) $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
    }
    return $ret;

}

function tahunbulan($tanggal)
{
    $t = explode("-", $tanggal);
    return $t[0].((int)$t[1]);
}

function tahunbulansetahunkebelakang($field, $tanggal)
{
    $t = explode("-", $tanggal);
    $current = ["y" => $t[0], "m" => (int)$t[1] + 1];
    for ($i=0; $i < 12; $i++)
    {
        if ($current["m"] == 1)
        {
            $current["m"] = 13;
            $current["y"]--;
        }
        $list[] = $field."=".$current["y"].($current["m"] - 1);
    }
    return implode(" or ", $list);
}

function format_date_to_sql($date)
{
    if(!empty($date))
    {
         $x = explode("/", $date); // 21/01/2020
        $xx= $x[2].'-'.$x[1].'-'.$x[0];
        return $xx;
    }
    else
    {
        return $date;
    }  
}
function gettahunbulan($date) // Y-m-d (2020-05-01) result Yj (20205)
{
    $result='';
    if(empty($date))
    {
       return date('Yj');
    }
    else
    {
        $result = explode('-', $date);
        return $result[0].intval($result[1]);
    }
}
function statuskeluar($index)
{
    $sts = ["0"=>"pesan","1"=>"periksa","2"=>"selesai","3"=>"batal","4"=>"kadaluarsa"];
    return $sts[$index];
}
function enum_rs_barang_pemesanan($jenistransaksi)
{
    $arr = ['pesan'=>'Pesan','retur'=>'Retur','pesanobatranap'=>'Pesan Obat Ranap','returobatranap'=>'Retur Obat Ranap','pesanobatpulang'=>'Pesan Obat Pulang','returobatpulang'=>'Retur Obat Pulang'];
    return $arr[$jenistransaksi];
}
function get_url()
{
    return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI] -- $_SERVER[HTTP_REFERER]";
}
function pesan_belumlogin()
{
   CI()->session->set_flashdata('message', ['status' => 'danger', 'message' => "Silakan login dahulu...!"]); 
   return aksesditolak();
}
function ql_formatsqldatetime($date)
{
    $result = date('Y-m-d H:i:s',strtotime($date));
    return $result;
}
function ql_formatsqldate($date)
{
    $result = date('Y-m-d',strtotime($date));
    return $result;
}
function formatdate_indotosistem($date) // dd/mm/yyyy
{
    $tgl = explode('/', $date);
    $result = $tgl[2].'-'.$tgl[1].'-'.$tgl[0];
    return $result;

}
function ql_formatdatetime($date)
{
    $result = date('Y-m-d H:i:s',strtotime($date));
    return $result;
}
function ql_formatdate($date)
{
    $result = date('Y-m-d',strtotime($date));
    return $result;
}
function ql_identitas($nama)
{
    $result = ltrim(strtolower($nama),'by ny');
    $left = substr($nama,0,3);
    $right = substr($nama,-3);
    if($left=='an ' OR $left=='nn ' OR $left=='ny ' OR $left=='sdr ' OR $left=='tn ') { $result = ltrim(strtolower($result),$left); }
    if($right==' an' OR $right==' nn' OR $right==' ny' OR $right==' sdr' OR $right==' tn') { $result =  rtrim(strtolower($result),$right);}
    $result = $result;
    return $result;
}
function ql_getmonth($date)
{
    return date('m',strtotime($date));
}
function ql_getmonth_withoutzero($date)
{
    return date('n',strtotime($date));
}
function ql_getyear($date)
{
    return date('Y',strtotime($date));
}
function ql_lastmonth($date)
{
    $datestring=$date.' first day of last month';
    $tgl=date_create($datestring);
    return $tgl->format('m');
}
function ql_nextmonth($date)
{
    $datestring=$date.' first day of next month';
    $tgl=date_create($datestring);
    return $tgl->format('m');
}
function ql_lastyear($date)
{
    $datestring=$date.' first day of last year';
    $tgl=date_create($datestring);
    return $tgl->format('Y');
}
function ql_nextyear($date)
{
    $datestring=$date.' first day of next year';
    $tgl=date_create($datestring);
    return $tgl->format('Y');
}
function ql_datetime_iso8601($datetime){
    return $datetime->format('Y-m-d\TH:i:sP');
}
/**
 * Pembulatan Ratusan
 * @param type $nominal Nominal
 * @return type
 */
function pembulatanratusan($nominal)
{
    $pembulatan = (ceil(intval($nominal) / 100) ) * 100;
    return $pembulatan;
}

/**
 * Pembulatan Lima Ratusan
 * @param type $nominal Nominal
 * @return type
 */
function pembulatan_limaratusan($nominal)
{
    $pembulatan = (ceil(intval($nominal) / 500) ) * 500;
    return $pembulatan;
}
function limit_text_length($string,$limit)
{
    $string = strip_tags($string);
    if (strlen($string) > $limit) {

        // truncate string
        $stringCut = substr($string, 0, $limit);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
    }
    return $string;
}
function convertToRupiah($angka)
{
    $number = number_format($angka,'0','','.');
    return $number;
}
function unconvertToRupiah($angka)
{
    $number = str_replace('.','', $angka);
    return $number;
}
function convertToRupiahKoma($angka)
{
    $number = number_format($angka,'0','',',');
    return $number;
}
function qlreplace($search,$replace,$var){return str_replace($search, $replace, $var);}
function qlreplace_quote($text){ return str_replace("'", "`", $text); }
function qlset_null($data){return ((empty($data))?null:$data);}

//shift jaga
function shiftjaga($datetime)
{
    $datetime=substr($datetime,11,2);
    $shift='';
    if ($datetime > 7 && $datetime < 15){ $shift='Shift 1'; }
    else if ($datetime > 14 && $datetime < 21) { $shift='Shift 2'; }
    else { $shift='Shift 3'; }
    return $shift;
}
function shiftwaktujaga($shift, $date)
{
   return (($shift==1)?  "'".$date." 07:00:01' AND '".$date." 14:00:00'":  (($shift==2)? "'".$date." 14:00:01' AND '".$date." 21:00:00'" : "'".$date." 21:00:01' AND '".date("Y-m-d", strtotime("+1 Days", strtotime($date)))." 07:00:00'"));
}
function statuspendaftaran($status)
{
    $sts = [0=>'Pesan',1=>'Dalam Pemeriksaan',2=>'Selesai',3=>'Batal',4=>'Kadaluarsa'];
    return $sts[$status];
}
function passwordhash($password)
{
    return password_hash($password, PASSWORD_DEFAULT);
}

function encrypt($keyname,$plaintext)
{
   CI()->encryptbap->generatekey_once($keyname);
   $encrypt= CI()->encryptbap->encrypt_urlsafe($plaintext, "json");
   return $encrypt; 
}
function decrypt($keyname, $ciphertext)
{
   $decrypt= json_decode(CI()->encryptbap->decrypt_urlsafe($keyname,$ciphertext));
   return $decrypt; 
}
function in_array_any($needles, $haystack) {
	return (bool)array_intersect($needles, $haystack);
}
/**
 * Fill Push Array
 * @param type $inputArray
 * @param type $data
 * @param type $value_id
 * @param type $value_txt
 * @return type
 */
function fill_push_array($inputArray, $data,$value_id,$value_txt)
{
    $newArray = array();
    $newArray[] = $inputArray;
    foreach ($data as $arr)
    {
        $row=array();
        $row[$value_id] = $arr[$value_id];
        $row[$value_txt] = $arr[$value_txt];
        $newArray[] = $row;
    }
    return $newArray;
}
/**
 * Is Isset
 * @param type $data
 * @param type $alternatif
 * @return type
 */
function is_isset($data,$alternatif)
{
    $result = ((isset($data)) ? $data : $alternatif );
    return $result;
}

function status_ROP($stok,$aman,$minimal)
{
    // $stok=1;
    $index = (($stok > $aman) ? 0 : (($stok > $minimal) ? 2 : 1) );
    $rop = ['aman','habis','minimal'];
    return $rop[$index];
}
function ql_tooltip($title)
{
    $result = 'data-toggle="tooltip" data-original-title="'.$title.'"';
    return $result;
}
/**
 * @param type $date Date Validate
 * @param type $format Format Date Validate
 * @return true or false
 */
function validateDate($date, $format = '')
{
    $d = DateTime::createFromFormat($format, $date);
    // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
    return $d && $d->format($format) === $date;
}

/**
 * Tabel Bhp Ralan
 * @param type $bodyid Tag Id Body Table
 */
function table_bhpralanugd($bodyid)
{
    echo '<table class="table table-striped table-hover table-bordered" style="width: 95%; margin-top: 5px;">
                  <thead>
                    <tr class="header-table-ql">
                    <th width="30%">Obat/BHP</th>
                    <th width="10%">Jumlah</th>
                    <th width="10%">AturanPakai</th>
                    <th width="10%">Penggunaan</th>
                    <th width="20%">Keterangan Lain <i>*max 100 huruf</i></th>
                    <th width="10%">Grup Racik <i>*Diisi farmasi</i></th>
                    <th width="10%" colspan="2">Total Harga</th>
                    </tr>
                  </thead>
                  <tbody id="'.$bodyid.'">
                  </tbody>
                </table>';
}

function table_bhpralanunitugd($bodyid)
{
    echo '<table class="table table-striped table-hover table-bordered" style="width: 75%; margin-top: 5px;">
                  <thead>
                    <tr class="header-table-ql">
                    <th width="40%">Obat/BHP</th>
                    <th width="12%">Jumlah</th>
                    <th width="12%">Aturan Pakai</th>
                    <th width="25%">Keterangan Lain <i>*max 100 huruf</i></th>
                    <th width="11%"colspan="2">Total Harga</th>
                    </tr>
                  </thead>
                  <tbody id="'.$bodyid.'">
                  </tbody>
                </table>';
}

function table_bhpralan($bodyid)
{
    echo '<table class="table table-striped table-hover table-bordered" style="width: 95%; margin-top: 5px;">
                  <thead>
                    <tr class="header-table-ql">
                      <th>No</th>
                      <th>Obat/BHP[ExdDate]</th>
                      <th width="8%">Resep<sup>1)</sup></th>
                      <th>Kekuatan</th>
                      <th width="9%">DosisRacik<sup>6)</sup></th>
                      <th>GrupRacik<sup>3)</sup></th>
                      <th>Diresepkan</th>
                      <th>Tot.Harga</th>
                      <th>Tot.Diberikan<sup>4)</sup></th>
                      <th>Diberikan</th>
                      <th>Penggunaan</th>
                      <th colspan="2">AturanPakai</th>
                    </tr>
                  </thead>
                  <tbody id="'.$bodyid.'">
                  </tbody>
                </table>
                <sup>1) Banyaknya BHP/Obat yang diresepkan ke Farmasi</sup><br/>
                <sup>2) Banyaknya jumlah racikan yang diresepkan ke Farmasi</sup><br/>
                <sup>3) 0 berarti BHP/Obat tidak diracik. Selain itu, BHP/Obat akan diracik sesuai dengan grupnya</sup><br/>
                <sup>4) Banyaknya BHP/Obat yang diberikan Farmasi ke pasien</sup><br/>
                <sup>5) Banyaknya jumlah racikan yang diberikan Farmasi ke pasien</sup><br/>
                <sup>6) Dosis obat yang diberikan Farmasi ke pasien</sup>';
}

function sql_clean($value)
{
    $relpace = str_replace("'","", htmlspecialchars($value, ENT_QUOTES));
    return $relpace;
}
function xss_clean($string)
{
    $result = htmlentities($string,ENT_QUOTES,'UTF-8');
    return $result;
}
function tandaPenghubung($nilai,$tanda){ return (($nilai!='') ? $tanda : '' );}
function fakturpermintaanbarang(){ return 'PF'.date('Ymd');}
function fakturpengembalianbarang(){ return 'RF'.date('Ymd');}
function floattostr( $val )
{
    preg_match( "#^([\+\-]|)([0-9]*)(\.([0-9]*?)|)(0*)$#", trim($val), $o );
    return $o[1].sprintf('%d',$o[2]).($o[3]!='.'?$o[3]:'');
}

function statusoperasi($status)
{
    $arrStatus = array(
        0 => 'Rencana',
        1 => 'Terlaksana',
        2 => 'Batal',
        3 => 'Hapus',
        4 => 'Sedang Ditangani'
    );
    
    $result = $arrStatus[$status];
    return $result;
}



function UUIDv1()
{
    return UUID::generate(UUID::UUID_TIME, UUID::FMT_STRING, "\x00\x00\x00\x00\x00\x00");
}

/*-
 * Copyright (c) 2011 Fredrik Lindberg - http://www.shapeshifter.se
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *	  notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *	  notice, this list of conditions and the following disclaimer in the
 *	  documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Alternative this software might be licensed under the following license
 *
 *  Copyright 2011 Fredrik Lindberg
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

/*
 * UUID (RFC4122) Generator
 * http://tools.ietf.org/html/rfc4122
 *
 * Implements version 1, 3, 4 and 5
 */
class UUID {
	/* UUID versions */
	const UUID_TIME	 = 1;	/* Time based UUID */
	const UUID_NAME_MD5	 = 3;	/* Name based (MD5) UUID */
	const UUID_RANDOM	 = 4;	/* Random UUID */
	const UUID_NAME_SHA1	 = 5;	/* Name based (SHA1) UUID */

	/* UUID formats */
	const FMT_FIELD	 = 100;
	const FMT_STRING	 = 101;
	const FMT_BINARY	 = 102;
	const FMT_QWORD	 = 1;	/* Quad-word, 128-bit (not impl.) */
	const FMT_DWORD	 = 2;	/* Double-word, 64-bit (not impl.) */
	const FMT_WORD		 = 4;	/* Word, 32-bit (not impl.) */
	const FMT_SHORT		= 8;	/* Short (not impl.) */
	const FMT_BYTE		= 16;	/* Byte */
	const FMT_DEFAULT	 = 16;

	/* Field UUID representation */
	static private $m_uuid_field = array(
		'time_low' => 0,		/* 32-bit */
		'time_mid' => 0,		/* 16-bit */
		'time_hi' => 0,			/* 16-bit */
		'clock_seq_hi' => 0,		/*  8-bit */
		'clock_seq_low' => 0,		/*  8-bit */
		'node' => array()		/* 48-bit */
	);

	static private $m_generate = array(
		self::UUID_TIME => "generateTime",
		self::UUID_RANDOM => "generateRandom",
		self::UUID_NAME_MD5 => "generateNameMD5",
		self::UUID_NAME_SHA1 => "generateNameSHA1"
	);

	static private $m_convert = array(
		self::FMT_FIELD => array(
			self::FMT_BYTE => "conv_field2byte",
			self::FMT_STRING => "conv_field2string",
			self::FMT_BINARY => "conv_field2binary"
		),
		self::FMT_BYTE => array(
			self::FMT_FIELD => "conv_byte2field",
			self::FMT_STRING => "conv_byte2string",
			self::FMT_BINARY => "conv_byte2binary"
		),
		self::FMT_STRING => array(
			self::FMT_BYTE => "conv_string2byte",
			self::FMT_FIELD => "conv_string2field",
			self::FMT_BINARY => "conv_string2binary"
		),
	);

	/* Swap byte order of a 32-bit number */
	static private function swap32($x) {
		return (($x & 0x000000ff) << 24) | (($x & 0x0000ff00) << 8) |
			(($x & 0x00ff0000) >> 8) | (($x & 0xff000000) >> 24);
	}

	/* Swap byte order of a 16-bit number */
	static private function swap16($x) {
		return (($x & 0x00ff) << 8) | (($x & 0xff00) >> 8);
	}

	/* Auto-detect UUID format */
	static private function detectFormat($src) {
		if (is_string($src))
			return self::FMT_STRING;
		else if (is_array($src)) {
			$len = count($src);
			if ($len == 1 || ($len % 2) == 0)
				return $len;
			else
				return (-1);
		}
		else
			return self::FMT_BINARY;
	}

	/*
	 * Public API, generate a UUID of 'type' in format 'fmt' for
	 * the given namespace 'ns' and node 'node'
	 */
	static public function generate($type, $fmt = self::FMT_BYTE,
		$node = "", $ns = "") {
		$func = self::$m_generate[$type];
		if (!isset($func))
			return null;
		$conv = self::$m_convert[self::FMT_FIELD][$fmt];

		$uuid = self::$func($ns, $node);
		return self::$conv($uuid);
	}

	/*
	 * Public API, convert a UUID from one format to another
	 */
	static public function convert($uuid, $from, $to) {
		$conv = self::$m_convert[$from][$to];
		if (!isset($conv))
			return ($uuid);

		return (self::$conv($uuid));
	}

	/*
	 * Generate an UUID version 4 (pseudo random)
	 */
	static private function generateRandom($ns, $node) {
		$uuid = self::$m_uuid_field;

		$uuid['time_hi'] = (4 << 12) | (mt_rand(0, 0x1000));
		$uuid['clock_seq_hi'] = (1 << 7) | mt_rand(0, 128);
		$uuid['time_low'] = mt_rand(0, 0xffff) + (mt_rand(0, 0xffff) << 16);
		$uuid['time_mid'] = mt_rand(0, 0xffff);
		$uuid['clock_seq_low'] = mt_rand(0, 255);
		for ($i = 0; $i < 6; $i++)
			$uuid['node'][$i] = mt_rand(0, 255);
		return ($uuid);
	}

	/*
	 * Generate UUID version 3 and 5 (name based)
	 */
	static private function generateName($ns, $node, $hash, $version) {
		$ns_fmt = self::detectFormat($ns);
		$field = self::convert($ns, $ns_fmt, self::FMT_FIELD);

		/* Swap byte order to keep it in big endian on all platforms */
		$field['time_low'] = self::swap32($field['time_low']);
		$field['time_mid'] = self::swap16($field['time_mid']);
		$field['time_hi'] = self::swap16($field['time_hi']);

		/* Convert the namespace to binary and concatenate node */
		$raw = self::convert($field, self::FMT_FIELD, self::FMT_BINARY);
		$raw .= $node;

		/* Hash the namespace and node and convert to a byte array */
		$val = $hash($raw, true);	
		$tmp = unpack('C16', $val);
		foreach (array_keys($tmp) as $key)
			$byte[$key - 1] = $tmp[$key];

		/* Convert byte array to a field array */
		$field = self::conv_byte2field($byte);

		$field['time_low'] = self::swap32($field['time_low']);
		$field['time_mid'] = self::swap16($field['time_mid']);
		$field['time_hi'] = self::swap16($field['time_hi']);

		/* Apply version and constants */
		$field['clock_seq_hi'] &= 0x3f;
		$field['clock_seq_hi'] |= (1 << 7);
		$field['time_hi'] &= 0x0fff;
		$field['time_hi'] |= ($version << 12);

		return ($field);	
	}
	static private function generateNameMD5($ns, $node) {
		return self::generateName($ns, $node, "md5",
			self::UUID_NAME_MD5);
	}
	static private function generateNameSHA1($ns, $node) {
		return self::generateName($ns, $node, "sha1",
			self::UUID_NAME_SHA1);
	}

	/*
	 * Generate UUID version 1 (time based)
	 */
	static private function generateTime($ns, $node) {
		$uuid = self::$m_uuid_field;

		/*
		 * Get current time in 100 ns intervals. The magic value
		 * is the offset between UNIX epoch and the UUID UTC
		 * time base October 15, 1582.
		 */
		$tp = gettimeofday();
		$time = ($tp['sec'] * 10000000) + ($tp['usec'] * 10) +
			0x01B21DD213814000;

		$uuid['time_low'] = $time & 0xffffffff;
		/* Work around PHP 32-bit bit-operation limits */
		$high = intval($time / 0xffffffff);
		$uuid['time_mid'] = $high & 0xffff;
		$uuid['time_hi'] = (($high >> 16) & 0xfff) | (self::UUID_TIME << 12);
		
		/*
		 * We don't support saved state information and generate
		 * a random clock sequence each time.
		 */
		$uuid['clock_seq_hi'] = 0x80 | mt_rand(0, 64);
		$uuid['clock_seq_low'] = mt_rand(0, 255);

		/*
		 * Node should be set to the 48-bit IEEE node identifier, but
		 * we leave it for the user to supply the node.
		 */
		for ($i = 0; $i < 6; $i++)
			$uuid['node'][$i] = ord(substr($node, $i, 1));

		return ($uuid);
	}

	/* Assumes correct byte order */
	static private function conv_field2byte($src) {
		$uuid[0] = ($src['time_low'] & 0xff000000) >> 24;
		$uuid[1] = ($src['time_low'] & 0x00ff0000) >> 16;
		$uuid[2] = ($src['time_low'] & 0x0000ff00) >> 8;
		$uuid[3] = ($src['time_low'] & 0x000000ff);
		$uuid[4] = ($src['time_mid'] & 0xff00) >> 8;
		$uuid[5] = ($src['time_mid'] & 0x00ff);
		$uuid[6] = ($src['time_hi'] & 0xff00) >> 8;
		$uuid[7] = ($src['time_hi'] & 0x00ff);
		$uuid[8] = $src['clock_seq_hi'];
		$uuid[9] = $src['clock_seq_low'];

		for ($i = 0; $i < 6; $i++)
			$uuid[10+$i] = $src['node'][$i];

		return ($uuid);
	}

	static private function conv_field2string($src) {
		$str = sprintf(
			'%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x',
			($src['time_low']), ($src['time_mid']), ($src['time_hi']),
			$src['clock_seq_hi'], $src['clock_seq_low'],
			$src['node'][0], $src['node'][1], $src['node'][2],
			$src['node'][3], $src['node'][4], $src['node'][5]);
		return ($str);
	}

	static private function conv_field2binary($src) {
		$byte = self::conv_field2byte($src);
		return self::conv_byte2binary($byte);
	}

	static private function conv_byte2field($uuid) {
		$field = self::$m_uuid_field;
		$field['time_low'] = ($uuid[0] << 24) | ($uuid[1] << 16) |
			($uuid[2] << 8) | $uuid[3];
		$field['time_mid'] = ($uuid[4] << 8) | $uuid[5];
		$field['time_hi'] = ($uuid[6] << 8) | $uuid[7];
		$field['clock_seq_hi'] = $uuid[8];
		$field['clock_seq_low'] = $uuid[9];

		for ($i = 0; $i < 6; $i++)
			$field['node'][$i] = $uuid[10+$i];
		return ($field);
	}

	static public function conv_byte2string($src) {
		$field = self::conv_byte2field($src);
		return self::conv_field2string($field);
	}

	static private function conv_byte2binary($src) {
		$raw = pack('C16', $src[0], $src[1], $src[2], $src[3],
			$src[4], $src[5], $src[6], $src[7], $src[8], $src[9],
			$src[10], $src[11], $src[12], $src[13], $src[14], $src[15]);
		return ($raw);
	}

	static private function conv_string2field($src) {
		$parts = sscanf($src, '%x-%x-%x-%x-%02x%02x%02x%02x%02x%02x');
		$field = self::$m_uuid_field;
		$field['time_low'] = ($parts[0]);
		$field['time_mid'] = ($parts[1]);
		$field['time_hi'] = ($parts[2]);
		$field['clock_seq_hi'] = ($parts[3] & 0xff00) >> 8;
		$field['clock_seq_low'] = $parts[3] & 0x00ff;
		for ($i = 0; $i < 6; $i++)
			$field['node'][$i] = $parts[4+$i];

		return ($field);
	}

	static private function conv_string2byte($src) {
		$field = self::conv_string2field($src);
		return self::conv_field2byte($field);
	}

	static private function conv_string2binary($src) {
		$byte = self::conv_string2byte($src);
		return self::conv_byte2binary($byte);
	}
}