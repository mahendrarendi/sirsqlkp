<?php
    function applicare_updatedata_idbed($idbed)
    {
        CI()->load->model('mviewql');
        $bangsal = CI()->mviewql->viewbangsalaplicare_idbed($idbed)[0];
        $uploadedJSON = json_encode([   'koderuang'=>$bangsal["koderuangjkn"],
                                        'namaruang'=>$bangsal["namabangsal"],
                                        'kapasitas'=>$bangsal["kapasitas"],
                                        'tersedia'=>$bangsal["tersedialakilaki"]+$bangsal["tersediaperempuan"]+$bangsal["tersediacampur"],
                                        'tersediapria'=>$bangsal["tersedialakilaki"],
                                        'tersediawanita'=>$bangsal["tersediaperempuan"],
                                        'tersediapriawanita'=>$bangsal["tersediacampur"],
                                        'kodekelas'=>$bangsal["kodekelasjkn"]
                                    ]);
       echo json_encode(CI()->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }
    
    function applicare_updatedata($idbangsal)
    {
        CI()->load->model('mviewql');
        $bangsal = CI()->mviewql->viewbangsalaplicare($idbangsal)[0];
        $uploadedJSON = json_encode([   'koderuang'=>$bangsal["koderuangjkn"],
                                        'namaruang'=>$bangsal["namabangsal"],
                                        'kapasitas'=>$bangsal["kapasitas"],
                                        'tersedia'=>$bangsal["tersedialakilaki"]+$bangsal["tersediaperempuan"]+$bangsal["tersediacampur"],
                                        'tersediapria'=>$bangsal["tersedialakilaki"],
                                        'tersediawanita'=>$bangsal["tersediaperempuan"],
                                        'tersediapriawanita'=>$bangsal["tersediacampur"],
                                        'kodekelas'=>$bangsal["kodekelasjkn"]
                                    ]);
       echo json_encode(CI()->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }
    
    function applicare_adddata($databangsal)
    {
        $uploadedJSON = json_encode([   'koderuang'=>$databangsal["koderuangjkn"],
                                        'namaruang'=>$databangsal["namabangsal"],
                                        'kapasitas'=>$databangsal["kapasitas"],
                                        'tersedia'=>$databangsal["tersedialakilaki"]+$databangsal["tersediaperempuan"]+$databangsal["tersediacampur"],
                                        'tersediapria'=>$databangsal["tersedialakilaki"],
                                        'tersediawanita'=>$databangsal["tersediaperempuan"],
                                        'tersediapriawanita'=>$databangsal["tersediacampur"],
                                        'kodekelas'=>$databangsal["kodekelasjkn"]
                                    ]);
       echo json_encode(CI()->bpjsbap->createBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }
    
    function applicare_deletedata($idbangsal)
    {
        CI()->load->model('mviewql');
        $bangsal = CI()->mviewql->viewbangsalaplicare($idbangsal)[0];
        $uploadedJSON = json_encode([   'koderuang'=>$bangsal["koderuangjkn"],
                                        'kodekelas'=>$bangsal["kodekelasjkn"]
                                    ]);
        echo json_encode(CI()->bpjsbap->deleteBed(JENISCONSID_PRODUCTION, $uploadedJSON));
    }
    
    function aplicare_updatebed($idbangsal = '')
    {
        if ($idbangsal == '')
        {
            $uploadedJSON = json_encode([    'koderuang'=>CI()->input->post('krj'),
                                             'namaruang'=>CI()->input->post('nr'),
                                             'kapasitas'=>CI()->input->post('k'),
                                             'tersedia'=>CI()->input->post('tl')+CI()->input->post('tp')+CI()->input->post('tc'),
                                             'tersediapria'=>CI()->input->post('tl'),
                                             'tersediawanita'=>CI()->input->post('tp'),
                                             'tersediapriawanita'=>CI()->input->post('tc'),
                                             'kodekelas'=>CI()->input->post('kkj')
                                         ]);
            echo json_encode(CI()->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON));
        }
        else 
        {
            CI()->load->model('mviewql');
            $b = CI()->mviewql->viewbangsalaplicare($idbangsal);
            if (array_key_exists(0, $b))
            {
                $bangsal = $b[0];
                $uploadedJSON = json_encode([   'koderuang'=>$bangsal["koderuangjkn"],
                                                'namaruang'=>$bangsal["namabangsal"],
                                                'kapasitas'=>$bangsal["kapasitas"],
                                                'tersedia'=>$bangsal["tersedialakilaki"]+$bangsal["tersediaperempuan"]+$bangsal["tersediacampur"],
                                                'tersediapria'=>$bangsal["tersedialakilaki"],
                                                'tersediawanita'=>$bangsal["tersediaperempuan"],
                                                'tersediapriawanita'=>$bangsal["tersediacampur"],
                                                'kodekelas'=>$bangsal["kodekelasjkn"]
                                            ]);
                CI()->bpjsbap->updateBed(JENISCONSID_PRODUCTION, $uploadedJSON);
            }
        }
   }
