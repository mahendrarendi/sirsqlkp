<?php 
function f_next_month($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year + 1;
    }else{
      $next_month=$this_month + 1;
      $next_year=$this_year;
    }
    
    return date('F-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function f_next_month_number($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year + 1;
    }else{
      $next_month=$this_month + 1;
      $next_year=$this_year;
    }
    
    return date('m-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function f_last_day($month){
    $day = date('Y-m-d', strtotime($month.'- 1 day'));

    return $day;
}

function f_last_month_number($month){
    $this_month = date('m', strtotime('01-'.$month));
    $this_year = date('Y', strtotime('01-'.$month));
    
    if($this_month=='12'){
      $next_month='01';
      $next_year=$this_year - 1;
    }else{
      $next_month=$this_month - 1;
      $next_year=$this_year;
    }
    
    return date('m-Y', strtotime('01-'.$next_month.'-'.$next_year));
}

function echoDate( $start, $end ){

            $current = $start;
            $ret = array();

            while( $current<$end ){
                
                $next = @date('Y-M-01', $current) . "+1 month";
                $current = @strtotime($next);
                $ret[] = $current;
            }

            return array_reverse($ret);
        }


