<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function location_get_identifier_use(){
    $data = [
        [
            "Code System" => "http://hl7.org/fhir/identifier-use",
            "Location.identifier[i].use" => "usual",
            "Keterangan" => "Identifier yang direkomendasikan digunakan untuk interaksi dunia nyata"
        ],
        [
            "Code System" => "http://hl7.org/fhir/identifier-use",
            "Location.identifier[i].use" => "official",
            "Keterangan" => "Identifier yang dianggap paling terpercaya. Terkadang juga dikenal sebagai 'primer' dan 'utama'. Penentuan 'resmi' bersifat subyektif dan panduan implementasi seringkali memberikan panduan tambahan untuk digunakan."
        ],
        [
            "Code System" => "http://hl7.org/fhir/identifier-use",
            "Location.identifier[i].use" => "temp",
            "Keterangan" => "Identifier sementara"
        ],
        [
            "Code System" => "http://hl7.org/fhir/identifier-use",
            "Location.identifier[i].use" => "secondary",
            "Keterangan" => "Identifier yang ditugaskan dalam penggunaan sekunder - ini berfungsi untuk mengidentifikasi objek dalam konteks relatif, tetapi tidak dapat secara konsisten ditugaskan ke objek yang sama lagi dalam konteks yang berbeda."
        ],
        [
            "Code System" => "http://hl7.org/fhir/identifier-use",
            "Location.identifier[i].use" => "old",
            "Keterangan" => "Id identifier sudah dianggap tidak valid, tetapi masih memungkinkan relevan untuk kebutuhan pencarian."
        ]
    ];
    return $data;
}

function location_get_status(){
    $data = [
        [
            "Code System" => "http://hl7.org/fhir/location-status",
            "Location.status" => "active",
            "Keterangan" => "Lokasi sedang beroperasi"
        ],
        [
            "Code System" => "http://hl7.org/fhir/location-status",
            "Location.status" => "suspended",
            "Keterangan" => "Lokasi ditutup sementara"
        ],
        [
            "Code System" => "http://hl7.org/fhir/location-status",
            "Location.status" => "inactive",
            "Keterangan" => "Lokasi tidak lagi digunakan"
        ]
    ];
    return $data;    
}

function location_get_operational_status(){
    $data = [
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "C",
            "Location.operationalStatus.display" => "Closed",
            "Keterangan" => "Tutup"
        ],
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "H",
            "Location.operationalStatus.display" => "Housekeeping",
            "Keterangan" => "Dalam pembersihan"
        ],
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "I",
            "Location.operationalStatus.display" => "Isolated",
            "Keterangan" => "Isolasi"
        ],
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "K",
            "Location.operationalStatus.display" => "Contaminated",
            "Keterangan" => "Terkontaminasi"
        ],
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "O",
            "Location.operationalStatus.display" => "Occupied",
            "Keterangan" => "Terisi"
        ],
        [
            "Location.operationalStatus.system" => "http://terminology.hl7.org/CodeSystem/v2-0116",
            "Location.operationalStatus.code" => "U",
            "Location.operationalStatus.display" => "Unoccupied",
            "Keterangan" => "Tidak terisi"
        ]
    ];
    return $data; 
}

function location_get_mode(){
    $data = [
        [
            "Code System" => "http://hl7.org/fhir/location-mode",
            "Location.mode" => "instance",
            "Keterangan" => "Merepresentasikan lokasi spesifik"
        ],
        [
            "Code System" => "http://hl7.org/fhir/location-mode",
            "Location.mode" => "kind",
            "Keterangan" => "Merepresentasikan kelompok/kelas lokasi"
        ]
    ];
    return $data;    
}

function location_get_physical_type(){
    $data = [
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "si",
            "Location.physicalType.coding.display" => "Site",
            "Keterangan" => "Kumpulan bangunan atau lokasi lain seperti kompleks atau kampus."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "bu",
            "Location.physicalType.coding.display" => "Building",
            "Keterangan" => "Setiap Bangunan atau struktur."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "wi",
            "Location.physicalType.coding.display" => "Wing",
            "Keterangan" => "Sayap di dalam Gedung, sering berisi lantai, kamar, dan koridor."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "wa",
            "Location.physicalType.coding.display" => "Ward",
            "Keterangan" => "Bangsal adalah bagian dari fasilitas medis yang mungkin berisi kamar dan jenis lokasi lainnya."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "lvl",
            "Location.physicalType.coding.display" => "Level",
            "Keterangan" => "Lantai di Gedung/Struktur."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "co",
            "Location.physicalType.coding.display" => "Corridor",
            "Keterangan" => "Setiap koridor di dalam Gedung, yang dapat menghubungkan kamar-kamar."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "ro",
            "Location.physicalType.coding.display" => "Room",
            "Keterangan" => "Sebuah ruang yang dialokasikan sebagai ruangan."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "bd",
            "Location.physicalType.coding.display" => "Bed",
            "Keterangan" => "Tempat tidur yang dapat ditempati."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "ve",
            "Location.physicalType.coding.display" => "Vehicle",
            "Keterangan" => "Alat transportasi."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "ho",
            "Location.physicalType.coding.display" => "House",
            "Keterangan" => "Rumah."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "ca",
            "Location.physicalType.coding.display" => "Cabinet",
            "Keterangan" => "Wadah yang dapat menyimpan barang, peralatan, obat-obatan, atau barang lainnya."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "rd",
            "Location.physicalType.coding.display" => "Road",
            "Keterangan" => "Jalan."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "area",
            "Location.physicalType.coding.display" => "Area",
            "Keterangan" => "Area (contoh: zona risiko banjir, wilayah, wilayah kodepos)."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.hl7.org/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "jdn",
            "Location.physicalType.coding.display" => "Jurisdiction",
            "Keterangan" => "Negara, Provinsi."
        ],
        [
            "Location.physicalType.coding.system" => "http://terminology.kemkes.go.id/CodeSystem/location-physical-type",
            "Location.physicalType.coding.code" => "vir",
            "Location.physicalType.coding.display" => "Virtual",
            "Keterangan" => "Virtual."
        ]
    ];    
    return $data;    
}

function location_get_service_class(){
    $data = [
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
            "Location.extension.serviceClass.code" => "1",
            "Location.extension.serviceClass.display" => "Kelas 1",
            "Keterangan" => "Perawatan Kelas 1"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
            "Location.extension.serviceClass.code" => "2",
            "Location.extension.serviceClass.display" => "Kelas 2",
            "Keterangan" => "Perawatan Kelas 2"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
            "Location.extension.serviceClass.code" => "3",
            "Location.extension.serviceClass.display" => "Kelas 3",
            "Keterangan" => "Perawatan Kelas 3"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
            "Location.extension.serviceClass.code" => "vip",
            "Location.extension.serviceClass.display" => "Kelas VIP",
            "Keterangan" => "Perawatan Kelas VIP"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Inpatient",
            "Location.extension.serviceClass.code" => "vvip",
            "Location.extension.serviceClass.display" => "Kelas VVIP",
            "Keterangan" => "Perawatan Kelas VVIP"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Outpatient",
            "Location.extension.serviceClass.code" => "reguler",
            "Location.extension.serviceClass.display" => "Kelas Reguler",
            "Keterangan" => "Perawatan Kelas Reguler"
        ],
        [
            "Location.extension.serviceClass.system" => "http://terminology.kemkes.go.id/CodeSystem/locationServiceClass-Outpatient",
            "Location.extension.serviceClass.code" => "eksekutif",
            "Location.extension.serviceClass.display" => "Kelas Eksekutif",
            "Keterangan" => "Perawatan Kelas Eksekutif"
        ],
    ];
    return $data;    
}
function location_get_type() {
    $data = [
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0001",
            "Location.type.display" => "Wahana PIDI",
            "Keterangan" => "Wahana PIDI"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0002",
            "Location.type.display" => "Wahana PIDGI",
            "Keterangan" => "Wahana PIDGI"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0003",
            "Location.type.display" => "RS Pendidikan",
            "Keterangan" => "RS Pendidikan"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0004",
            "Location.type.display" => "Tempat Tidur",
            "Keterangan" => "Tempat Tidur"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0005",
            "Location.type.display" => "Bank Darah",
            "Keterangan" => "Bank Darah"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0006",
            "Location.type.display" => "Instalasi Gawat Darurat",
            "Keterangan" => "Instalasi Gawat Darurat"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0007",
            "Location.type.display" => "Ruang Perawatan Intensif Umum (ICU)",
            "Keterangan" => "Ruang Perawatan Intensif Umum (ICU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0008",
            "Location.type.display" => "Ruangan Persalinan",
            "Keterangan" => "Ruangan Persalinan"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0009",
            "Location.type.display" => "Ruang Perawatan Intensif",
            "Keterangan" => "Ruang Perawatan Intensif"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0010",
            "Location.type.display" => "Daerah Rawat Pasien ICU/ICCU/HCU/PICU",
            "Keterangan" => "Daerah Rawat Pasien ICU/ICCU/HCU/PICU"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0011",
            "Location.type.display" => "Ruangan Perawatan Intensif Pediatrik (PICU)",
            "Keterangan" => "Ruangan Perawatan Intensif Pediatrik (PICU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0012",
            "Location.type.display" => "Ruangan Perawatan Intensif Neonatus(NICU)",
            "Keterangan" => "Ruangan Perawatan Intensif Neonatus(NICU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0013",
            "Location.type.display" => "High Care Unit (HCU)",
            "Keterangan" => "High Care Unit (HCU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0014",
            "Location.type.display" => "Intensive Cardiology Care Unit (ICCU)",
            "Keterangan" => "Intensive Cardiology Care Unit (ICCU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0015",
            "Location.type.display" => "Respiratory Intensive Care Unit (RICU)",
            "Keterangan" => "Respiratory Intensive Care Unit (RICU)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0016",
            "Location.type.display" => "Ruang Rawat Inap",
            "Keterangan" => "Ruang Rawat Inap"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0017",
            "Location.type.display" => "Ruangan Perawatan (Post Partum)",
            "Keterangan" => "Ruangan Perawatan (Post Partum)"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0018",
            "Location.type.display" => "Ruangan Perawatan Isolasi",
            "Keterangan" => "Ruangan Perawatan Isolasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0019",
            "Location.type.display" => "Ruangan Perawatan Neonatus Infeksius/Isolasi",
            "Keterangan" => "Ruangan Perawatan Neonatus Infeksius/Isolasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0020",
            "Location.type.display" => "Ruangan Perawatan Neonatus Non-Infeksius",
            "Keterangan" => "Ruangan Perawatan Neonatus Non-Infeksius"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0021",
            "Location.type.display" => "Ruangan Perawatan Pasien Paska Terapi",
            "Keterangan" => "Ruangan Perawatan Pasien Paska Terapi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0022",
            "Location.type.display" => "Ruangan Rawat Pasca Persalinan",
            "Keterangan" => "Ruangan Rawat Pasca Persalinan"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0023",
            "Location.type.display" => "Ruangan/Daerah Rawat Pasien Isolasi",
            "Keterangan" => "Ruangan/Daerah Rawat Pasien Isolasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0024",
            "Location.type.display" => "Ruangan/Daerah Rawat Pasien Non-Isolasi",
            "Keterangan" => "Ruangan/Daerah Rawat Pasien Non-Isolasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0025",
            "Location.type.display" => "Ruang Operasi",
            "Keterangan" => "Ruang Operasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0026",
            "Location.type.display" => "Ruangan Observasi",
            "Keterangan" => "Ruangan Observasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0027",
            "Location.type.display" => "Ruangan Resusitasi",
            "Keterangan" => "Ruangan Resusitasi"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0028",
            "Location.type.display" => "Ruangan Tindakan Anak",
            "Keterangan" => "Ruangan Tindakan Anak"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0029",
            "Location.type.display" => "Ruangan Tindakan Bedah",
            "Keterangan" => "Ruangan Tindakan Bedah"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0030",
            "Location.type.display" => "Ruangan Tindakan Kebidanan",
            "Keterangan" => "Ruangan Tindakan Kebidanan"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0031",
            "Location.type.display" => "Ruangan Tindakan Non-Bedah",
            "Keterangan" => "Ruangan Tindakan Non-Bedah"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0032",
            "Location.type.display" => "Ruangan Triase",
            "Keterangan" => "Ruangan Triase"
        ],
        [
            "Location.type.system" => "http://terminology.kemkes.go.id/CodeSystem/location-type",
            "Location.type.code" => "RT0033",
            "Location.type.display" => "Ruangan Ultra Sonograﬁ (USG)",
            "Keterangan" => "Ruangan Ultra Sonograﬁ (USG)"
        ]
    ];

    return $data;
}

function organization_get_type(){
    $data = [
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "prov",
            "Organization.type.display" => "Healthcare Provider",
            "Keterangan" => "Fasilitas Pelayanan Kesehatan"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "dept",
            "Organization.type.display" => "Hospital Department",
            "Keterangan" => "Departemen dalam Rumah Sakit"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "team",
            "Organization.type.display" => "Organizational team",
            "Keterangan" => "Kelompok praktisi/tenaga kesehatan yang menjalankan fungsi tertentu dalam suatu organisasi"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "govt",
            "Organization.type.display" => "Government",
            "Keterangan" => "Organisasi Pemerintah"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "ins",
            "Organization.type.display" => "Insurance Company",
            "Keterangan" => "Perusahaan Asuransi"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "pay",
            "Organization.type.display" => "Payer",
            "Keterangan" => "Badan Penjamin"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "edu",
            "Organization.type.display" => "Educational Institute",
            "Keterangan" => "Institusi Pendidikan/Penelitian"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "reli",
            "Organization.type.display" => "Religious Institution",
            "Keterangan" => "Organisasi Keagamaan"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "crs",
            "Organization.type.display" => "Clinical Research Sponsor",
            "Keterangan" => "Sponsor penelitian klinis"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "cg",
            "Organization.type.display" => "Community Group",
            "Keterangan" => "Kelompok Masyarakat"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "bus",
            "Organization.type.display" => "Non-Healthcare Business or Corporation",
            "Keterangan" => "Perusahaan diluar bidang kesehatan"
        ],
        [
            "Organization.type.system" => "http://terminology.hl7.org/CodeSystem/organization-type",
            "Organization.type.code" => "other",
            "Organization.type.display" => "Other",
            "Keterangan" => "Lain-lain"
        ]
    ];
    return $data;    
}
function get_array_value($array, $index, $default = null) {
    if (is_array($array) && array_key_exists($index, $array)) {
        return $array[$index];
    }
    return $default;
}
/**
 * Function untuk generate UUID pengiriman data bundle satusehat
 */
function guidv4($data = null) {
    // Generate 16 bytes (128 bits) of random data or use the data passed into the function.
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);

    // Set version to 0100
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);

    // Output the 36 character UUID.
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}
?>