<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= APP_NAME; ?> | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/AdminLTE.min.css">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>assets/favicon-16x16.png"><!--icon--> 
  <!-- Animation library for notifications   -->
  <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet"/>
  <style type="text/css">
  .login-page{background-color: #6b6555;} 
  .login-box-body {background: #fff; padding: 35px; border-radius: 4px; border-top: 0; color: #666;}
  input#password{ -webkit-text-security: disc !important;}
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box" style="width: 410px">
  <div class="login-logo">
    <a href="<?php echo base_url();?>">
        <b class="text-yellow"><?= APP_NAME; ?></b>
    </a>
  </div>
  <!-- /.login-logo -->
  
  <!-- Set notifikasi -->
  <div id="notif"></div>   
  <input type="hidden" id="set_status" value="<?= ((isset($this->session->flashdata('message')['status'])) ? $this->session->flashdata('message')['status'] : '' ) ?>">
  <input type="hidden" id="set_message" value="<?= ((isset($this->session->flashdata('message')['message'])) ? $this->session->flashdata('message')['message'] : '' ) ?>">  
  <input type="hidden" class="txt_csrfname" name="<?= $this->security->get_csrf_token_name(); ?>" value="<?= $this->security->get_csrf_hash(); ?>">
  
  <div class="login-box-body">
    <p class="login-box-msg"></p>
    <?= form_open('login/plocess',['autocomplete'=>'off']); ?>
      <div class="form-group has-feedback">
        <?= form_input(['name'=>'username', 'autocomplete'=>'off', 'value'=>$this->session->flashdata('username'), 'type'=>'text', 'class'=>'form-control input-lg','placeholder'=>'Username', 'id'=>'username','autofocus'=>'auto']); ?>
        <span class="fa fa-user-secret fa-lg form-control-feedback"></span>
      </div>
    
      <div class="form-group has-feedback">
        <?= form_input(['name'=>'password','autocomplete'=>'off', 'type'=>'text', 'class'=>'form-control input-lg','placeholder'=>'Password', 'id'=>'password']); ?>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
    
        <div class="row" style="margin-bottom:10px;">
            <label class="col-md-12">Poli/Unit :</label>
            <div class="col-md-12">
                <select class="form-control" placeholder="slcunit" id="slcunit" name="slcunit" style="margin-right:4px;">
                    <option value="0">Pilih</option>
                </select>
            </div>
        </div>
    
        <div class="row" style="margin-bottom:10px;">
            <label class="col-md-12">Loket Pemanggilan :</label>
            <div class="col-md-12">
                <select class="form-control" placeholder="" id="idloket" name="idloket" style="margin-right:4px;">
                    <option value="0">Pilih</option>
                    <?php
                        foreach ($loket as $arr)
                        {
                            echo "<option value='".$arr['idloket']."'>".$arr['loket']."</option>";
                        }
                    ?>
                </select>
            </div>
        </div>

      <div class="row">
        <div class=" col-xs-12">
          <?= form_input(['name'=>'save', 'type'=>'submit', 'class'=>'btn btn-warning btn-block btn-flat btn-lg','value'=>'Log In']); ?>
        </div>
        <!-- /.col -->
      </div>

    <?= form_close(); ?>
  </div>
  <!-- /.login-box-body -->
   <center style="color:#fff;margin-top: 5px; font-size: 12.5px;"> <?= COPY_RIGHT; ?> </center>
</div>
<!-- /.login-box -->

<!--set baseu url untuk digunakan di javascript-->
<script> var base_url ='<?= base_url(); ?>';</script>
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>
<script type="text/javascript">
    //call js saat halaman di akses
    $(function () {
        if (typeof(Storage) !== "undefined")
        {
            //--hapus local storage
            localStorage.clear();
        } else {
            //jika browser tidak support webstorage
            pesanUndefinedLocalStorage();
        }
        
        catatanpengembang();
        var set_status = $('#set_status').val();//set status message
        var set_message = $('#set_message').val();//set pesan message
        if(set_status !==''){notif(set_status, set_message);}
    })
    
    function catatanpengembang()
    {
        consoleWithNoSource("%cSTOP!", "color: red; text-shadow: 0 0 3px #FF0000, 0 0 5px #0000FF;font-weight:bold;font-size:75px;");
        consoleWithNoSource("%c Ini adalah fitur browser yang ditujukan untuk pengembang. Jika seseorang meminta Anda untuk menyalin dan menempelkan sesuatu di sini untuk mengaktifkan fitur Sitiql atau meretas akun seseorang, itu adalah penipuan dan akan memberi mereka akses ke akun Sitiql Anda.", "font-size:15px;");
    }
    function consoleWithNoSource(...params) {
      setTimeout(console.log.bind(console, ...params));
    }
    
    
    //---seting notifikasi
    function notif(status, message)
    {
        $.notify({icon: 'fa fa-bell fa-lg',message: '<strong>&nbsp;'+message+'</strong>'},{type: status,delay: 1100,timer: 400});
        return true;
    }
    
    $(document).on("change","#username", function()
    {
        var csrfName = $('.txt_csrfname').attr('name'); // Value specified in $config['csrf_token_name']
        var csrfHash = $('.txt_csrfname').val(); // CSRF hash
    
        $.ajax({
            type: "POST", //tipe pengiriman data
            url: '<?php echo base_url(); ?>login/get_unit', //alamat controller yang dituju (di js base url otomatis)
            dataType: "JSON", //tipe data yang dikirim
            data: { u: $('input[name="username"]').val() , [csrfName]: csrfHash},
            success: function(result) {
                // Update CSRF hash
                $('input[name="'+csrfName+'"]').val(result.token);
                $("#slcunit").empty();
                
                var result = result.unit;
                var tampildt='<option value="0">Pilih</option>';
                for (i in result)
                {
                    tampildt +='<option value="'+result[i].idunit+','+result[i].namaunit+','+result[i].levelgudang+',">'+result[i].namaunit+'</option>';
                }
                $("#slcunit").html(tampildt);
            },
            //jika error
            error: function(result) {
            }
        });
    });
</script>
</body>
</html>
