<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    
    //login view
    public function index()
    {
        $data['loket'] = $this->db->get('pemanggilan_loket')->result_array();
        $this->load->view('view_login',$data);
    }
    
    //proses login
    public function plocess()
    {
        //siapkan data
        $username = str_replace('"','',str_replace("'","", htmlspecialchars($this->input->post("username"), ENT_QUOTES)));
        $password = $this->input->post('password');
        $unit     = str_replace('"','',str_replace("'","", htmlspecialchars($this->input->post('slcunit'), ENT_QUOTES)));
        $idloket  = str_replace('"','',str_replace("'","", htmlspecialchars($this->input->post('idloket'), ENT_QUOTES)));
        
        //jika username kosong
        if($username=='' && empty($username))
        {
            $arrMsg = ['status'=>'danger','message'=>'Username Harap Dilengkapi.']; 
        }
        //jika password kosong
        else if($password =='' && empty($password))
        {
            $arrMsg = ['status'=>'danger','message'=>'Password Harap Dilengkapi.'];
        }
        //selain itu cek username di table login_user
        else
        {
            $data = $this->db
                    ->join('login_hakaksesuser lh','lh.iduser = lu.iduser')
                    ->get_where('login_user lu',['lu.namauser'=>$username]);
            
            //jika user benar
            if($data->num_rows() > 0)
            {
                $data = $data->row_array();
                if (password_verify($password,$data['password'])) //jika passwor benar 
                {
                    $this->db->update('login_user',['loginterakhir'=>date('Y-m-d H:i:s')],['iduser'=>$data['iduser'] ]);
                    
                    $u                      = explode(",", $unit);
                    $data['idunitterpilih'] = $u[0];
                    $data['unitterpilih']   = $u[1];
                    $data['levelgudang']    = $u[2];
                    $data['idloketpemanggilan'] = $idloket;
                    $data['loketpemanggilan']   = $this->db->select('loket')->get_where('pemanggilan_loket',['idloket'=>$idloket]);
                    $this->create_session($data);
                }
                else //jika password salah
                {
                    $arrMsg = ['status'=>'danger','message'=>'Password Salah.' ] ;
                }
            }
            else //jika username salah
            {
                $arrMsg = ['status'=>'danger','message'=>'Username Salah.' ] ;
            }
        }
        $this->session->set_flashdata('message', $arrMsg);
        $this->session->set_flashdata('username', $username);
        return redirect('login');
    }
    
    //kirim data login ke aplikasi simrs
    public function create_session($data)
    {
        $username = urlencode($this->input->post('username'));
        $password = urlencode($this->input->post('password'));
        $idunitterpilih = urlencode($data['idunitterpilih']);
        $unitterpilih   = urlencode($data['unitterpilih']);
        $levelgudang    = urlencode($data['levelgudang']);
        $idloketpemanggilan = urlencode($data['idloketpemanggilan']);
        $loketpemanggilan   = urlencode($data['loketpemanggilan']);
                            
        $myvars = '123456='.$username.'&7891011='.$password.'&234567='.$idunitterpilih.'&345678='.$unitterpilih.'&456789='.$levelgudang.'&56789101='.$idloketpemanggilan.'&6789101112='.$loketpemanggilan;
        $url = '../../sirstql_erm/clogin/rsuQL12345698678657546543543HJVJHGCHF?';
        return redirect($url.$myvars);
    }
        
    //get data unit by username
    public function get_unit()
    {
        $username = str_replace('"','',str_replace("'","", htmlspecialchars($this->input->post("u"), ENT_QUOTES)));
        
        $this->db->select("unituser");
        $this->db->from("login_hakaksesuser hau, login_user u");
        $this->db->where("hau.iduser = u.iduser");
        $this->db->where('namauser', $username);
        $query = $this->db->get()->result();
        $response = [];
        
        foreach ($query as $row)
        {
            if(empty(!$row->unituser))
            {
                $response['unit'] = $this->db->query("select idunit, namaunit, levelgudang from rs_unit_farmasi where idunit in (".$row->unituser.")")->result_array();
            }
        }
        
        $response['token'] = $this->security->get_csrf_hash();
        
        echo json_encode($response);
    }
}
